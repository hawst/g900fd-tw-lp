.class public final Lbne;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Z

.field private static final b:Ljava/lang/String;

.field private static final c:Landroid/os/Handler;

.field private static final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lbmr;",
            ">;"
        }
    .end annotation
.end field

.field private static e:I

.field private static f:I

.field private static g:Z

.field private static final h:Ljava/lang/Object;

.field private static i:Landroid/graphics/Bitmap;

.field private static final j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static k:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 104
    sget-object v0, Lbys;->k:Lcyp;

    sput-boolean v2, Lbne;->a:Z

    .line 130
    const-class v0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbne;->b:Ljava/lang/String;

    .line 132
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lbne;->c:Landroid/os/Handler;

    .line 133
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lbne;->d:Ljava/util/Set;

    .line 138
    sput-boolean v2, Lbne;->g:Z

    .line 140
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lbne;->h:Ljava/lang/Object;

    .line 147
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    sput-object v0, Lbne;->j:Ljava/util/Map;

    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/content/SharedPreferences;)Lbxz;
    .locals 2

    .prologue
    .line 1390
    sget v0, Lh;->hU:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1391
    const-string v1, ""

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1392
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 1393
    invoke-static {v0}, Lbxz;->a(Ljava/lang/String;)Lbxz;

    move-result-object v0

    .line 1395
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static a(Ljava/lang/String;I)Ljava/lang/CharSequence;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1233
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    .line 1234
    new-instance v2, Landroid/text/style/TextAppearanceSpan;

    sget v0, Lf;->hS:I

    invoke-direct {v2, v1, v0}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    .line 1236
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1237
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1238
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    sget v4, Lf;->hR:I

    invoke-direct {v0, v1, v4}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    .line 1240
    invoke-virtual {v3, p0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1241
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v3, v0, v6, v4, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1242
    sget v0, Lh;->hM:I

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1243
    invoke-virtual {v3, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1245
    :cond_0
    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    .line 1248
    sget v0, Lh;->hE:I

    .line 1249
    const/4 v5, 0x2

    if-ne p1, v5, :cond_2

    .line 1250
    sget v0, Lh;->hB:I

    .line 1258
    :cond_1
    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1260
    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    .line 1259
    invoke-virtual {v3, v2, v4, v0, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1261
    return-object v3

    .line 1251
    :cond_2
    const/4 v5, 0x3

    if-ne p1, v5, :cond_3

    .line 1252
    sget v0, Lh;->hS:I

    goto :goto_0

    .line 1253
    :cond_3
    const/4 v5, 0x4

    if-ne p1, v5, :cond_4

    .line 1254
    sget v0, Lh;->hC:I

    goto :goto_0

    .line 1255
    :cond_4
    const/4 v5, 0x6

    if-ne p1, v5, :cond_1

    .line 1256
    sget v0, Lh;->hR:I

    goto :goto_0
.end method

.method protected static a(Ljava/lang/String;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 1190
    const/4 v0, 0x0

    const/4 v1, 0x0

    sget v2, Lh;->hQ:I

    invoke-static {p0, p1, v0, v1, v2}, Lbne;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;II)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;I)Ljava/lang/CharSequence;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1156
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    .line 1157
    new-instance v1, Landroid/text/style/TextAppearanceSpan;

    sget v2, Lf;->hR:I

    invoke-direct {v1, v0, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    .line 1160
    new-instance v2, Landroid/text/style/TextAppearanceSpan;

    sget v3, Lf;->hT:I

    invoke-direct {v2, v0, v3}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    .line 1163
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1164
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1165
    invoke-virtual {v3, p0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1166
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v3, v1, v5, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1168
    :cond_0
    sget v1, Lh;->hM:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1170
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1171
    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 1172
    invoke-virtual {v3, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1174
    :cond_1
    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 1175
    invoke-virtual {v3, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1177
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v4

    add-int/2addr v4, v1

    .line 1176
    invoke-virtual {v3, v2, v1, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1179
    :cond_2
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1180
    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_3

    .line 1181
    invoke-virtual {v3, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1183
    :cond_3
    const/4 v0, 0x0

    invoke-static {v0, p3}, Lbne;->a(Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1185
    :cond_4
    return-object v3
.end method

.method private static a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;II)Ljava/lang/CharSequence;
    .locals 6

    .prologue
    .line 1207
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    .line 1208
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1211
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1212
    invoke-virtual {v1, p0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1213
    new-instance v2, Landroid/text/style/StyleSpan;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v5, 0x21

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1216
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1217
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 1218
    invoke-virtual {v0, p4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1220
    :cond_1
    invoke-virtual {v1, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1222
    :cond_2
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1223
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_3

    .line 1224
    sget v2, Lh;->hM:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1225
    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1227
    :cond_3
    const/4 v0, 0x0

    invoke-static {v0, p3}, Lbne;->a(Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1229
    :cond_4
    return-object v1
.end method

.method private static a(Lyj;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 487
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    .line 488
    if-eqz p2, :cond_0

    .line 489
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lyj;->j()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 491
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lyj;->j()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static declared-synchronized a(Lyj;Ljava/util/ArrayList;)Ljava/util/Set;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lyj;",
            "Ljava/util/ArrayList",
            "<",
            "Lbmr;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1276
    const-class v5, Lbne;

    monitor-enter v5

    .line 1277
    :try_start_0
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lf;->k(Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 1279
    const-string v0, "active_hangouts_list"

    const-string v1, "\\|"

    invoke-static {v6, v0, v1}, Lcxg;->a(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v2

    .line 1284
    if-eqz p1, :cond_c

    .line 1285
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v1, v3

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmr;

    .line 1286
    iget v7, v0, Lbmr;->s:I

    const/4 v8, 0x1

    if-ne v7, v8, :cond_1

    .line 1287
    if-nez v1, :cond_0

    .line 1288
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 1290
    :cond_0
    iget-object v0, v0, Lbmr;->n:Lbxz;

    invoke-virtual {v0}, Lbxz;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    move-object v0, v1

    move-object v1, v0

    .line 1292
    goto :goto_0

    :cond_2
    move-object v4, v1

    .line 1295
    :goto_1
    if-eqz v2, :cond_3

    .line 1296
    if-nez v4, :cond_5

    move-object v3, v2

    .line 1310
    :cond_3
    :goto_2
    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1311
    const-string v1, "active_hangouts_list"

    const-string v6, "|"

    invoke-static {v0, v1, v4, v6}, Lcxg;->a(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;)V

    .line 1313
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1314
    sget-boolean v0, Lbne;->a:Z

    if-eqz v0, :cond_4

    .line 1315
    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1316
    :goto_3
    if-eqz v4, :cond_9

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 1317
    :goto_4
    if-eqz v3, :cond_a

    .line 1318
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1319
    :goto_5
    const-string v4, "Babel"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "updateActiveHangouts: lastActive="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "\nnewActive="

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nnotActiveAnymore="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1325
    :cond_4
    monitor-exit v5

    return-object v3

    .line 1299
    :cond_5
    :try_start_1
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-object v1, v3

    :goto_6
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1300
    invoke-interface {v4, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 1301
    if-nez v1, :cond_6

    .line 1302
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 1304
    :cond_6
    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_7
    move-object v0, v1

    move-object v1, v0

    .line 1306
    goto :goto_6

    .line 1315
    :cond_8
    const-string v0, "empty"

    move-object v2, v0

    goto :goto_3

    .line 1316
    :cond_9
    const-string v0, "empty"

    move-object v1, v0

    goto :goto_4

    .line 1318
    :cond_a
    const-string v0, "empty"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_5

    .line 1276
    :catchall_0
    move-exception v0

    monitor-exit v5

    throw v0

    :cond_b
    move-object v3, v1

    goto/16 :goto_2

    :cond_c
    move-object v4, v3

    goto/16 :goto_1
.end method

.method static synthetic a(Landroid/app/Notification;Lyj;ILbxz;ZZ)V
    .locals 0

    .prologue
    .line 101
    invoke-static/range {p0 .. p5}, Lbne;->b(Landroid/app/Notification;Lyj;ILbxz;ZZ)V

    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/content/SharedPreferences;Lbxz;)V
    .locals 3

    .prologue
    .line 1404
    invoke-static {p0, p1}, Lbne;->a(Landroid/content/Context;Landroid/content/SharedPreferences;)Lbxz;

    move-result-object v0

    .line 1405
    invoke-virtual {p2, v0}, Lbxz;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1410
    :goto_0
    return-void

    .line 1408
    :cond_0
    sget v0, Lh;->hU:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1409
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-virtual {p2}, Lbxz;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method

.method static synthetic a(Lbk;Lbmr;)V
    .locals 0

    .prologue
    .line 101
    invoke-static {p0, p1}, Lbne;->b(Lbk;Lbmr;)V

    return-void
.end method

.method static synthetic a(Lbmr;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 101
    invoke-static {p0, p1}, Lbne;->b(Lbmr;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private static a(Lbmr;ZZ)V
    .locals 13

    .prologue
    .line 551
    new-instance v3, Lbk;

    .line 552
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v3, v0}, Lbk;-><init>(Landroid/content/Context;)V

    .line 554
    iget-object v0, p0, Lbmr;->n:Lbxz;

    invoke-virtual {v0}, Lbxz;->a()Ljava/lang/String;

    move-result-object v1

    .line 556
    iget-object v0, p0, Lbmr;->m:Lyj;

    invoke-virtual {p0}, Lbmr;->l()I

    move-result v2

    invoke-static {v0, v2}, Lbne;->d(Lyj;I)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Landroid/provider/Settings$System;->DEFAULT_NOTIFICATION_URI:Landroid/net/Uri;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p0}, Lbmr;->m()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v0, v4, v5

    const/4 v0, 0x2

    sget v5, Lf;->hO:I

    invoke-static {v5}, Lf;->g(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v2, v4}, Lf;->a(Landroid/net/Uri;[Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 559
    if-eqz p2, :cond_3

    .line 560
    sget-boolean v0, Lbne;->a:Z

    if-eqz v0, :cond_0

    .line 561
    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "processAndSend: fromConversationId == sCurrentlyDisplayedConversationId so NOT showing notification, but playing soft sound. conversationId: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    :cond_0
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "audio"

    .line 566
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 567
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v0, 0x1

    .line 568
    :goto_0
    if-nez v0, :cond_1

    .line 569
    new-instance v0, Lbyv;

    const-string v1, "Babel"

    invoke-direct {v0, v1}, Lbyv;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    const/4 v2, 0x5

    const/high16 v3, 0x3e800000    # 0.25f

    invoke-virtual {v0, v4, v1, v2, v3}, Lbyv;->a(Landroid/net/Uri;ZIF)V

    sget-object v1, Lbne;->c:Landroid/os/Handler;

    new-instance v2, Lbng;

    invoke-direct {v2, v0}, Lbng;-><init>(Lbyv;)V

    const-wide/16 v3, 0x1388

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 694
    :cond_1
    :goto_1
    return-void

    .line 567
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 575
    :cond_3
    const/4 v0, 0x0

    .line 576
    iget v2, p0, Lbmr;->s:I

    const/4 v5, 0x1

    if-eq v2, v5, :cond_4

    instance-of v2, p0, Lbmg;

    if-eqz v2, :cond_5

    :cond_4
    move-object v0, v1

    .line 581
    :cond_5
    invoke-virtual {p0}, Lbmr;->h()I

    move-result v1

    .line 582
    iget-object v2, p0, Lbmr;->m:Lyj;

    iget v5, p0, Lbmr;->s:I

    invoke-static {v2, v1, v5, v0}, Lbzb;->a(Lyj;IILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lbmr;->t:I

    .line 588
    instance-of v0, p0, Lbmg;

    if-nez v0, :cond_6

    .line 589
    invoke-virtual {p0}, Lbmr;->a()Landroid/content/Intent;

    move-result-object v0

    .line 590
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    .line 591
    invoke-virtual {p0}, Lbmr;->q()I

    move-result v2

    const/high16 v5, 0x8000000

    .line 590
    invoke-static {v1, v2, v0, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v3, v0}, Lbk;->b(Landroid/app/PendingIntent;)Lbk;

    .line 595
    :cond_6
    iget-object v5, p0, Lbmr;->m:Lyj;

    iget-object v0, p0, Lbmr;->n:Lbxz;

    .line 596
    invoke-virtual {p0}, Lbmr;->l()I

    move-result v6

    .line 595
    const/4 v2, 0x4

    invoke-virtual {v0}, Lbxz;->a()Ljava/lang/String;

    move-result-object v1

    if-nez p1, :cond_13

    sget-object v0, Lbne;->j:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    sget v7, Lbne;->k:I

    if-nez v7, :cond_7

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "babel_notification_time_between_rings"

    const/16 v9, 0xa

    invoke-static {v7, v8, v9}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    mul-int/lit16 v7, v7, 0x3e8

    sput v7, Lbne;->k:I

    :cond_7
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v7

    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v9

    sub-long v9, v7, v9

    if-eqz v0, :cond_8

    sget v0, Lbne;->k:I

    int-to-long v11, v0

    cmp-long v0, v9, v11

    if-lez v0, :cond_13

    :cond_8
    sget-object v0, Lbne;->j:Ljava/util/Map;

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v0, v1, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v3, v4}, Lbk;->a(Landroid/net/Uri;)Lbk;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    packed-switch v6, :pswitch_data_0

    :pswitch_0
    sget v1, Lh;->ao:I

    sget v0, Lf;->bG:I

    :goto_2
    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v7

    if-nez v6, :cond_d

    const-string v0, "smsmms"

    const/4 v5, 0x0

    invoke-virtual {v4, v0, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    :goto_3
    invoke-interface {v0, v1, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_13

    const/4 v0, 0x6

    :goto_4
    invoke-virtual {v3, v0}, Lbk;->b(I)Lbk;

    .line 599
    invoke-virtual {p0}, Lbmr;->d()Landroid/content/Intent;

    move-result-object v0

    .line 600
    const-string v1, "processAndSend notification"

    invoke-static {v1, v0}, Lbxm;->a(Ljava/lang/String;Landroid/content/Intent;)V

    .line 601
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcu;->a(Landroid/content/Context;)Lcu;

    move-result-object v1

    .line 603
    invoke-virtual {v1, v0}, Lcu;->b(Landroid/content/Intent;)Lcu;

    move-result-object v1

    .line 606
    invoke-virtual {v1}, Lcu;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    :goto_5
    if-ltz v0, :cond_9

    .line 607
    invoke-virtual {v1, v0}, Lcu;->a(I)Landroid/content/Intent;

    move-result-object v2

    .line 608
    sget-object v4, Lbne;->b:Ljava/lang/String;

    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 609
    const-string v0, "com.google.android.apps.hangouts.phone.conversationlist"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 615
    :cond_9
    invoke-virtual {p0}, Lbmr;->p()I

    move-result v0

    .line 614
    invoke-virtual {v1, v0}, Lcu;->b(I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 616
    invoke-virtual {v3, v0}, Lbk;->a(Landroid/app/PendingIntent;)Lbk;

    .line 619
    invoke-virtual {p0}, Lbmr;->b()I

    move-result v0

    invoke-virtual {v3, v0}, Lbk;->c(I)Lbk;

    .line 625
    invoke-virtual {p0, v3}, Lbmr;->a(Lbk;)Lbu;

    move-result-object v1

    .line 627
    instance-of v0, v1, Lbj;

    if-eqz v0, :cond_f

    .line 628
    invoke-static {}, Lf;->u()I

    move-result v0

    const/4 v2, 0x1

    if-le v0, v2, :cond_a

    move-object v0, v1

    .line 631
    check-cast v0, Lbj;

    iget-object v2, p0, Lbmr;->m:Lyj;

    .line 632
    invoke-virtual {v2}, Lyj;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lbj;->a(Ljava/lang/CharSequence;)Lbj;

    .line 643
    :cond_a
    :goto_6
    iput-object v3, p0, Lbmr;->q:Lbk;

    .line 644
    iput-object v1, p0, Lbmr;->p:Lbu;

    .line 645
    iget-object v0, p0, Lbmr;->o:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    .line 646
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 647
    const-string v2, "android.people"

    iget-object v0, p0, Lbmr;->o:Ljava/util/HashSet;

    iget-object v4, p0, Lbmr;->o:Ljava/util/HashSet;

    .line 648
    invoke-virtual {v4}, Ljava/util/HashSet;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 647
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 649
    invoke-virtual {v3, v1}, Lbk;->a(Landroid/os/Bundle;)Lbk;

    .line 652
    :cond_b
    invoke-static {p0}, Lbne;->b(Lbmr;)Z

    move-result v10

    .line 653
    iget-object v0, p0, Lbmr;->u:Ljava/util/ArrayList;

    if-eqz v0, :cond_12

    .line 654
    if-eqz v10, :cond_10

    .line 655
    invoke-static {}, Lbtf;->k()Z

    move-result v0

    if-nez v0, :cond_10

    const/4 v9, 0x1

    .line 656
    :goto_7
    iget-object v0, p0, Lbmr;->u:Ljava/util/ArrayList;

    const/4 v1, 0x0

    iget-object v2, p0, Lbmr;->m:Lyj;

    .line 658
    invoke-static {}, Lyn;->b()I

    move-result v3

    const/4 v4, 0x0

    new-instance v5, Lbnh;

    const/4 v6, 0x0

    invoke-direct {v5, v6}, Lbnh;-><init>(B)V

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v6, p0

    .line 656
    invoke-static/range {v0 .. v9}, Lxy;->a(Ljava/util/List;ILyj;ILjava/lang/String;Laac;Ljava/lang/Object;Ljava/lang/String;ZZ)Lzx;

    move-result-object v1

    .line 662
    if-eqz v1, :cond_c

    .line 664
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x14

    if-le v0, v2, :cond_11

    const/4 v0, 0x1

    :goto_8
    invoke-virtual {v1, v0}, Lzx;->a(Z)V

    .line 667
    :cond_c
    sget-object v2, Lbne;->d:Ljava/util/Set;

    monitor-enter v2

    .line 668
    :try_start_0
    sget-object v0, Lbne;->d:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 669
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 673
    sget-object v0, Lbne;->c:Landroid/os/Handler;

    new-instance v2, Lbnf;

    invoke-direct {v2, p0, v10}, Lbnf;-><init>(Lbmr;Z)V

    const-wide/16 v3, 0x7d0

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 689
    invoke-static {}, Lbsn;->c()Lbsn;

    move-result-object v0

    invoke-virtual {v0, v1}, Lbsn;->c(Lbrv;)V

    goto/16 :goto_1

    .line 595
    :pswitch_1
    sget v1, Lh;->lu:I

    sget v0, Lf;->bV:I

    goto/16 :goto_2

    :pswitch_2
    sget v1, Lh;->dw:I

    sget v0, Lf;->bG:I

    goto/16 :goto_2

    :pswitch_3
    sget v1, Lh;->dz:I

    sget v0, Lf;->bG:I

    goto/16 :goto_2

    :cond_d
    invoke-virtual {v5}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lf;->k(Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v0

    goto/16 :goto_3

    .line 606
    :cond_e
    add-int/lit8 v0, v0, -0x1

    goto/16 :goto_5

    .line 634
    :cond_f
    instance-of v0, v1, Lbm;

    if-eqz v0, :cond_a

    .line 635
    invoke-static {}, Lf;->u()I

    move-result v0

    const/4 v2, 0x1

    if-le v0, v2, :cond_a

    move-object v0, v1

    .line 638
    check-cast v0, Lbm;

    iget-object v2, p0, Lbmr;->m:Lyj;

    .line 639
    invoke-virtual {v2}, Lyj;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lbm;->a(Ljava/lang/CharSequence;)Lbm;

    goto/16 :goto_6

    .line 655
    :cond_10
    const/4 v9, 0x0

    goto :goto_7

    .line 664
    :cond_11
    const/4 v0, 0x0

    goto :goto_8

    .line 669
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 693
    :cond_12
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lbne;->b(Lbmr;Landroid/graphics/Bitmap;)V

    goto/16 :goto_1

    :cond_13
    move v0, v2

    goto/16 :goto_4

    .line 595
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic a(Lbv;Lbmr;)V
    .locals 0

    .prologue
    .line 101
    invoke-static {p0, p1}, Lbne;->c(Lbv;Lbmr;)V

    return-void
.end method

.method public static a(Lyj;)V
    .locals 2

    .prologue
    .line 238
    sget-object v1, Lbne;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 239
    :try_start_0
    invoke-static {p0}, Lbmf;->c(Lyj;)V

    .line 240
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Lyj;I)V
    .locals 4

    .prologue
    .line 223
    sget-object v1, Lbne;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 224
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 226
    const/4 v0, 0x1

    const/4 v2, 0x7

    :try_start_0
    invoke-static {p0, v0, v2}, Lbne;->a(Lyj;ZI)V

    .line 234
    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    .line 227
    :cond_1
    const/4 v0, 0x3

    if-ne p1, v0, :cond_2

    .line 229
    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x7

    invoke-static {p0, v0, v2, v3}, Lbne;->a(Lyj;ZZI)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 234
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 230
    :cond_2
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 232
    const/4 v0, 0x0

    const/4 v2, 0x7

    :try_start_1
    invoke-static {p0, v0, v2}, Lbne;->a(Lyj;ZI)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private static declared-synchronized a(Lyj;ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 304
    const-class v0, Lbne;

    monitor-enter v0

    const/4 v1, 0x0

    :try_start_0
    invoke-static {p0, p1, p2, v1}, Lbne;->a(Lyj;ILjava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 305
    monitor-exit v0

    return-void

    .line 304
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private static declared-synchronized a(Lyj;ILjava/lang/String;Z)V
    .locals 8

    .prologue
    .line 322
    const-class v1, Lbne;

    monitor-enter v1

    .line 323
    :try_start_0
    invoke-static {p0, p1, p2, p3}, Lbne;->b(Lyj;ILjava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 325
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcc;->a(Landroid/content/Context;)Lcc;

    move-result-object v3

    .line 328
    sget-object v4, Lbne;->d:Ljava/util/Set;

    monitor-enter v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 329
    :try_start_1
    sget-object v0, Lbne;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 330
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 331
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmr;

    .line 332
    iget-object v6, v0, Lbmr;->m:Lyj;

    invoke-virtual {v6}, Lyj;->j()I

    move-result v6

    invoke-virtual {p0}, Lyj;->j()I

    move-result v7

    if-ne v6, v7, :cond_0

    iget v6, v0, Lbmr;->s:I

    if-ne v6, p1, :cond_0

    .line 334
    const/4 v6, 0x1

    iput-boolean v6, v0, Lbmr;->r:Z

    .line 335
    sget-boolean v0, Lbne;->a:Z

    if-eqz v0, :cond_1

    .line 336
    const-string v0, "Babel"

    const-string v6, "cancel found a notif and setting it to cancel"

    invoke-static {v0, v6}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 341
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 322
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    .line 341
    :cond_2
    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 342
    :try_start_4
    invoke-virtual {v3, v2, p1}, Lcc;->a(Ljava/lang/String;I)V

    .line 343
    sget-boolean v0, Lbne;->a:Z

    if-eqz v0, :cond_3

    .line 344
    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "RealTimeChatNotifications.cancel type="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " conversationId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    :cond_3
    if-nez p1, :cond_4

    .line 352
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v2

    .line 353
    invoke-virtual {p0}, Lyj;->u()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "smsmms"

    .line 355
    :goto_1
    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 356
    invoke-static {v2, v0}, Lbne;->a(Landroid/content/Context;Landroid/content/SharedPreferences;)Lbxz;

    move-result-object v3

    .line 358
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lbxz;->size()I

    move-result v4

    if-lez v4, :cond_4

    .line 361
    if-eqz p2, :cond_6

    .line 362
    invoke-virtual {v3, p2}, Lbxz;->remove(Ljava/lang/Object;)Z

    .line 363
    invoke-static {v2, v0, v3}, Lbne;->a(Landroid/content/Context;Landroid/content/SharedPreferences;Lbxz;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 371
    :cond_4
    :goto_2
    monitor-exit v1

    return-void

    .line 354
    :cond_5
    :try_start_5
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 365
    :cond_6
    const/4 v0, 0x0

    invoke-static {p0, v3, v0}, Lbne;->a(Lyj;Lbxz;Lbmr;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_2
.end method

.method public static a(Lyj;Lbxz;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 261
    sget-object v1, Lbne;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 262
    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Lbxz;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 264
    :cond_0
    invoke-static {p0}, Lbne;->e(Lyj;)V

    .line 275
    :goto_0
    monitor-exit v1

    return-void

    .line 265
    :cond_1
    invoke-virtual {p1}, Lbxz;->size()I

    move-result v0

    if-ne v0, v2, :cond_2

    .line 266
    invoke-virtual {p1}, Lbxz;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lbne;->a(Lyj;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 268
    const/4 v0, 0x1

    .line 269
    invoke-virtual {p1}, Lbxz;->a()Ljava/lang/String;

    move-result-object v2

    .line 268
    invoke-static {p0, v0, v2}, Lbne;->a(Lyj;ILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 275
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 272
    :cond_2
    const/4 v0, 0x2

    const/4 v2, 0x0

    :try_start_1
    invoke-static {p0, v0, v2}, Lbne;->a(Lyj;ILjava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private static a(Lyj;Lbxz;Lbmr;)V
    .locals 5

    .prologue
    .line 771
    new-instance v1, Lbxz;

    invoke-direct {v1}, Lbxz;-><init>()V

    .line 772
    instance-of v0, p2, Lbmm;

    if-eqz v0, :cond_1

    .line 773
    check-cast p2, Lbmm;

    iget-object v0, p2, Lbmm;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmr;

    .line 774
    iget-object v3, v0, Lbmr;->n:Lbxz;

    if-eqz v3, :cond_0

    .line 775
    iget-object v0, v0, Lbmr;->n:Lbxz;

    invoke-virtual {v0}, Lbxz;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lbxz;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 779
    :cond_1
    invoke-virtual {p1}, Lbxz;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 780
    invoke-virtual {v1, v0}, Lbxz;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 781
    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {p0, v3, v0, v4}, Lbne;->a(Lyj;ILjava/lang/String;Z)V

    goto :goto_1

    .line 785
    :cond_3
    return-void
.end method

.method private static a(Lyj;Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lyj;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 294
    if-eqz p1, :cond_0

    .line 295
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 296
    const/4 v2, 0x1

    invoke-static {p0, v2, v0}, Lbne;->a(Lyj;ILjava/lang/String;)V

    goto :goto_0

    .line 300
    :cond_0
    return-void
.end method

.method private static a(Lyj;Z)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 790
    invoke-static {p0}, Lbly;->a(Lyj;)Ljava/util/ArrayList;

    move-result-object v3

    .line 793
    if-eqz v3, :cond_0

    .line 794
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmr;

    .line 795
    instance-of v0, v0, Lblz;

    if-eqz v0, :cond_6

    .line 796
    const/4 v0, 0x1

    :goto_1
    move v1, v0

    .line 798
    goto :goto_0

    :cond_0
    move v1, v2

    .line 800
    :cond_1
    if-nez v1, :cond_2

    .line 801
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lbne;->a(Lyj;ILjava/lang/String;)V

    .line 804
    :cond_2
    invoke-static {p0, v3}, Lbne;->a(Lyj;Ljava/util/ArrayList;)Ljava/util/Set;

    move-result-object v0

    .line 805
    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 806
    invoke-static {p0, v0}, Lbne;->a(Lyj;Ljava/util/Set;)V

    .line 809
    :cond_3
    if-nez v3, :cond_5

    .line 816
    :cond_4
    return-void

    .line 813
    :cond_5
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmr;

    .line 814
    invoke-static {v0, p1, v2}, Lbne;->a(Lbmr;ZZ)V

    goto :goto_2

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method public static a(Lyj;ZI)V
    .locals 3

    .prologue
    .line 168
    sget-boolean v0, Lbne;->a:Z

    if-eqz v0, :cond_0

    .line 169
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RealTimeChatNotifications.update silent: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " coverage: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, p1, v0, p2}, Lbne;->a(Lyj;ZZI)V

    .line 173
    return-void
.end method

.method private static a(Lyj;ZZ)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 720
    invoke-static {p0}, Lbmf;->a(Lyj;)Lbmr;

    move-result-object v0

    .line 721
    if-nez v0, :cond_0

    .line 722
    const/4 v0, 0x0

    invoke-static {p0, v3, v0}, Lbne;->a(Lyj;ILjava/lang/String;)V

    .line 757
    :goto_0
    return-void

    .line 725
    :cond_0
    invoke-static {v0, p1, p2}, Lbne;->a(Lbmr;ZZ)V

    .line 735
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v2

    .line 736
    invoke-virtual {p0}, Lyj;->u()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "smsmms"

    .line 738
    :goto_1
    invoke-virtual {v2, v1, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 739
    invoke-static {v2, v1}, Lbne;->a(Landroid/content/Context;Landroid/content/SharedPreferences;)Lbxz;

    move-result-object v3

    .line 740
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lbxz;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 741
    invoke-static {p0, v3, v0}, Lbne;->a(Lyj;Lbxz;Lbmr;)V

    .line 745
    :cond_1
    new-instance v3, Lbxz;

    invoke-direct {v3}, Lbxz;-><init>()V

    .line 746
    instance-of v4, v0, Lbmm;

    if-eqz v4, :cond_4

    .line 747
    check-cast v0, Lbmm;

    iget-object v0, v0, Lbmm;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmr;

    .line 748
    const/4 v5, 0x1

    invoke-static {v0, v5, p2}, Lbne;->a(Lbmr;ZZ)V

    .line 749
    iget-object v5, v0, Lbmr;->n:Lbxz;

    if-eqz v5, :cond_2

    .line 750
    iget-object v0, v0, Lbmr;->n:Lbxz;

    invoke-virtual {v0}, Lbxz;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lbxz;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 737
    :cond_3
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 756
    :cond_4
    invoke-static {v2, v1, v3}, Lbne;->a(Landroid/content/Context;Landroid/content/SharedPreferences;Lbxz;)V

    goto :goto_0
.end method

.method public static a(Lyj;ZZI)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 188
    sget-boolean v1, Lbne;->a:Z

    if-eqz v1, :cond_0

    .line 189
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "RealTimeChatNotifications.update silent: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " coverage: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    :cond_0
    sget-object v1, Lbne;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 193
    :try_start_0
    sget-boolean v2, Lbne;->g:Z

    if-nez v2, :cond_1

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lf;->dy:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sput v3, Lbne;->e:I

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lf;->dx:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lbne;->f:I

    const/4 v2, 0x1

    sput-boolean v2, Lbne;->g:Z

    .line 194
    :cond_1
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lh;->al:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v4, Lf;->bF:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lf;->k(Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4, v3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->d()Lcom/google/android/apps/hangouts/phone/EsApplication;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->b(Lyj;)J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v2, v4, v2

    if-lez v2, :cond_5

    :goto_0
    if-nez v0, :cond_6

    .line 195
    sget-boolean v0, Lbne;->a:Z

    if-eqz v0, :cond_2

    .line 196
    const-string v0, "Babel"

    const-string v2, "notifications disabled"

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    :cond_2
    invoke-static {p0}, Lbne;->c(Lyj;)V

    .line 208
    :cond_3
    :goto_1
    and-int/lit8 v0, p3, 0x4

    if-eqz v0, :cond_4

    .line 209
    invoke-static {p0}, Lbmf;->b(Lyj;)V

    .line 211
    :cond_4
    monitor-exit v1

    return-void

    .line 194
    :cond_5
    const/4 v0, 0x0

    goto :goto_0

    .line 200
    :cond_6
    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_7

    .line 201
    invoke-static {p0, p1, p2}, Lbne;->a(Lyj;ZZ)V

    .line 204
    :cond_7
    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_3

    .line 205
    invoke-static {p0, p1}, Lbne;->a(Lyj;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 211
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static a()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1373
    :try_start_0
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.google.android.wearable.app"

    const/4 v3, 0x0

    .line 1374
    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1375
    const/4 v0, 0x1

    .line 1379
    :goto_0
    sget-boolean v1, Lbne;->a:Z

    if-eqz v1, :cond_0

    .line 1380
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "isWearCompanionAppInstalled returned "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1382
    :cond_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method static synthetic a(Lbmr;)Z
    .locals 1

    .prologue
    .line 101
    invoke-static {p0}, Lbne;->b(Lbmr;)Z

    move-result v0

    return v0
.end method

.method private static declared-synchronized a(Lyj;Ljava/lang/String;)Z
    .locals 5

    .prologue
    .line 1331
    const-class v1, Lbne;

    monitor-enter v1

    .line 1332
    :try_start_0
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lf;->k(Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1334
    const-string v2, "active_hangouts_list"

    const-string v3, "\\|"

    invoke-static {v0, v2, v3}, Lcxg;->a(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v2

    .line 1336
    if-eqz v2, :cond_1

    invoke-interface {v2, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1338
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1339
    const-string v3, "active_hangouts_list"

    const-string v4, "|"

    invoke-static {v0, v3, v2, v4}, Lcxg;->a(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;)V

    .line 1341
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1342
    sget-boolean v0, Lbne;->a:Z

    if-eqz v0, :cond_0

    .line 1343
    const-string v0, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "updateActiveHangouts: removed conversation="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " current active="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1344
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1343
    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1346
    :cond_0
    const/4 v0, 0x1

    .line 1348
    :goto_0
    monitor-exit v1

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1331
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static b(Lyj;I)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 461
    invoke-static {p0, p1}, Lbne;->d(Lyj;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lf;->p(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method protected static b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;I)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1196
    sget v0, Lh;->hP:I

    invoke-static {p0, p1, p2, p3, v0}, Lbne;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;II)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private static b(Lyj;ILjava/lang/String;Z)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 516
    packed-switch p1, :pswitch_data_0

    .line 536
    :goto_0
    :pswitch_0
    return-object v0

    .line 518
    :pswitch_1
    if-eqz p3, :cond_0

    .line 519
    const-string v0, ":chat:"

    invoke-static {p0, v0, p2}, Lbne;->a(Lyj;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 521
    :cond_0
    const-string v1, ":chat:"

    invoke-static {p0, v1, v0}, Lbne;->a(Lyj;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 525
    :pswitch_2
    const-string v1, ":missed_ho:"

    invoke-static {p0, v1, v0}, Lbne;->a(Lyj;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 528
    :pswitch_3
    const-string v0, ":active_ho:"

    invoke-static {p0, v0, p2}, Lbne;->a(Lyj;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 533
    :pswitch_4
    const-string v1, ":error:"

    invoke-static {p0, v1, v0}, Lbne;->a(Lyj;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 516
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private static declared-synchronized b(Landroid/app/Notification;Lyj;ILbxz;ZZ)V
    .locals 6

    .prologue
    .line 1069
    const-class v1, Lbne;

    monitor-enter v1

    if-eqz p0, :cond_2

    .line 1071
    :try_start_0
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcc;->a(Landroid/content/Context;)Lcc;

    move-result-object v2

    .line 1073
    const/4 v0, 0x0

    .line 1074
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lbxz;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 1075
    invoke-virtual {p3}, Lbxz;->a()Ljava/lang/String;

    move-result-object v0

    .line 1077
    :cond_0
    invoke-static {p1, p2, v0, p5}, Lbne;->b(Lyj;ILjava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 1080
    iget v3, p0, Landroid/app/Notification;->flags:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Landroid/app/Notification;->flags:I

    .line 1081
    iget v3, p0, Landroid/app/Notification;->defaults:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Landroid/app/Notification;->defaults:I

    .line 1082
    if-eqz p4, :cond_3

    .line 1083
    const-string v3, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "doNotify for sms: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1087
    :cond_1
    :goto_0
    invoke-virtual {v2, v0, p2, p0}, Lcc;->a(Ljava/lang/String;ILandroid/app/Notification;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1089
    :cond_2
    monitor-exit v1

    return-void

    .line 1084
    :cond_3
    :try_start_1
    sget-boolean v3, Lbne;->a:Z

    if-eqz v3, :cond_1

    .line 1085
    const-string v3, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "RealTimeChatNotifications.doNotify: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1069
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static b(Lbk;Lbmr;)V
    .locals 6

    .prologue
    .line 944
    iget-object v0, p1, Lbmr;->m:Lyj;

    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    .line 945
    sget-boolean v1, Lbne;->a:Z

    if-eqz v1, :cond_0

    .line 946
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Group key (for wearables)="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 948
    :cond_0
    instance-of v1, p1, Lbmm;

    if-eqz v1, :cond_2

    .line 949
    invoke-virtual {p0, v0}, Lbk;->a(Ljava/lang/String;)Lbk;

    move-result-object v0

    invoke-virtual {v0}, Lbk;->c()Lbk;

    .line 958
    :cond_1
    :goto_0
    return-void

    .line 950
    :cond_2
    instance-of v1, p1, Lbmg;

    if-eqz v1, :cond_1

    .line 951
    check-cast p1, Lbmg;

    iget v1, p1, Lbmg;->k:I

    .line 955
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%02d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 956
    invoke-virtual {p0, v0}, Lbk;->a(Ljava/lang/String;)Lbk;

    move-result-object v0

    invoke-virtual {v0, v1}, Lbk;->b(Ljava/lang/String;)Lbk;

    goto :goto_0
.end method

.method private static b(Lbmr;Landroid/graphics/Bitmap;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 856
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v3

    .line 857
    invoke-static {p0}, Lbne;->b(Lbmr;)Z

    move-result v4

    .line 858
    iget-boolean v0, p0, Lbmr;->r:Z

    if-eqz v0, :cond_1

    .line 859
    if-eqz v4, :cond_0

    .line 860
    const-string v0, "Babel"

    const-string v1, "sendNotification for sms: notification already cancelled!"

    invoke-static {v0, v1}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 940
    :cond_0
    :goto_0
    return-void

    .line 864
    :cond_1
    iput-boolean v1, p0, Lbmr;->r:Z

    .line 865
    sget-object v5, Lbne;->d:Ljava/util/Set;

    monitor-enter v5

    .line 866
    :try_start_0
    sget-object v0, Lbne;->d:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 867
    sget-object v0, Lbne;->d:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 869
    :cond_2
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 871
    iget-object v5, p0, Lbmr;->q:Lbk;

    if-eqz p1, :cond_4

    move-object v0, p1

    .line 872
    :goto_1
    invoke-virtual {v5, v0}, Lbk;->a(Landroid/graphics/Bitmap;)Lbk;

    move-result-object v0

    .line 875
    invoke-virtual {p0}, Lbmr;->e()I

    move-result v5

    invoke-virtual {v0, v5}, Lbk;->a(I)Lbk;

    .line 876
    invoke-virtual {p0}, Lbmr;->f()Ljava/lang/String;

    move-result-object v5

    .line 877
    invoke-virtual {p0}, Lbmr;->g()I

    move-result v0

    .line 878
    if-eqz v5, :cond_6

    iget-object v6, p0, Lbmr;->p:Lbu;

    if-eqz v6, :cond_6

    iget-object v6, p0, Lbmr;->p:Lbu;

    instance-of v6, v6, Lbi;

    if-eqz v6, :cond_6

    const/4 v6, 0x2

    if-eq v0, v6, :cond_6

    .line 882
    const/4 v3, 0x3

    if-ne v0, v3, :cond_5

    move v0, v1

    .line 883
    :goto_2
    new-instance v3, Lzx;

    new-instance v4, Lbyq;

    iget-object v6, p0, Lbmr;->m:Lyj;

    invoke-direct {v4, v5, v6}, Lbyq;-><init>(Ljava/lang/String;Lyj;)V

    sget v6, Lbne;->e:I

    sget v7, Lbne;->f:I

    .line 885
    invoke-virtual {v4, v6, v7}, Lbyq;->a(II)Lbyq;

    move-result-object v4

    .line 886
    invoke-virtual {v4, v0}, Lbyq;->c(Z)Lbyq;

    move-result-object v0

    invoke-virtual {v0, v1}, Lbyq;->d(Z)Lbyq;

    move-result-object v0

    new-instance v4, Lbni;

    invoke-direct {v4, v2}, Lbni;-><init>(B)V

    invoke-direct {v3, v0, v4, v1, p0}, Lzx;-><init>(Lbyq;Laac;ZLjava/lang/Object;)V

    .line 890
    sget-boolean v0, Lbne;->a:Z

    if-eqz v0, :cond_3

    .line 891
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sendNotification created ImageRequest on: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 896
    :cond_3
    invoke-static {}, Lbsn;->c()Lbsn;

    move-result-object v0

    invoke-virtual {v0, v3}, Lbsn;->c(Lbrv;)V

    goto :goto_0

    .line 869
    :catchall_0
    move-exception v0

    monitor-exit v5

    throw v0

    .line 873
    :cond_4
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 874
    invoke-virtual {p0}, Lbmr;->e()I

    move-result v6

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1

    :cond_5
    move v0, v2

    .line 882
    goto :goto_2

    .line 898
    :cond_6
    iget-object v1, p0, Lbmr;->q:Lbk;

    .line 899
    iget-object v0, p0, Lbmr;->p:Lbu;

    invoke-virtual {v1, v0}, Lbk;->a(Lbu;)Lbk;

    .line 901
    new-instance v2, Lbv;

    invoke-direct {v2}, Lbv;-><init>()V

    .line 902
    if-nez p1, :cond_8

    .line 903
    sget-object v0, Lbne;->i:Landroid/graphics/Bitmap;

    if-nez v0, :cond_7

    .line 905
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v5, Lcom/google/android/apps/hangouts/R$drawable;->cR:I

    invoke-static {v0, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lbne;->i:Landroid/graphics/Bitmap;

    .line 907
    :cond_7
    sget-object v0, Lbne;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v0}, Lbv;->a(Landroid/graphics/Bitmap;)Lbv;

    .line 909
    :cond_8
    invoke-static {v1, p0}, Lbne;->b(Lbk;Lbmr;)V

    .line 910
    invoke-static {v2, p0}, Lbne;->c(Lbv;Lbmr;)V

    .line 911
    invoke-static {v2, p0}, Lbne;->d(Lbv;Lbmr;)V

    .line 913
    instance-of v0, p0, Lbmn;

    if-eqz v0, :cond_b

    move-object v0, p0

    .line 914
    check-cast v0, Lbmn;

    .line 917
    iget-object v5, p0, Lbmr;->m:Lyj;

    iget-object v6, p0, Lbmr;->n:Lbxz;

    .line 918
    invoke-virtual {v6}, Lbxz;->a()Ljava/lang/String;

    move-result-object v6

    .line 917
    invoke-static {v5, v6}, Lbbl;->b(Lyj;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    .line 919
    invoke-virtual {v0}, Lbmn;->j()I

    move-result v0

    .line 920
    const/high16 v6, 0x8000000

    invoke-static {v3, v0, v5, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 922
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 923
    const-string v5, "on_read"

    invoke-virtual {v3, v5, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 924
    invoke-virtual {v1, v3}, Lbk;->a(Landroid/os/Bundle;)Lbk;

    .line 928
    :cond_9
    :goto_3
    invoke-virtual {v1, v2}, Lbk;->a(Lbl;)Lbk;

    .line 930
    invoke-virtual {v1}, Lbk;->e()Landroid/app/Notification;

    move-result-object v0

    .line 932
    instance-of v1, p0, Lbmf;

    if-eqz v1, :cond_a

    .line 933
    if-eqz v0, :cond_a

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x14

    if-le v1, v2, :cond_a

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "category"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    const-string v3, "CATEGORY_MESSAGE"

    invoke-virtual {v1, v3}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1

    .line 936
    :cond_a
    :goto_4
    iget-object v1, p0, Lbmr;->m:Lyj;

    iget v2, p0, Lbmr;->s:I

    iget-object v3, p0, Lbmr;->n:Lbxz;

    instance-of v5, p0, Lbmg;

    invoke-static/range {v0 .. v5}, Lbne;->b(Landroid/app/Notification;Lyj;ILbxz;ZZ)V

    goto/16 :goto_0

    .line 925
    :cond_b
    instance-of v0, p0, Lbmm;

    if-eqz v0, :cond_9

    .line 926
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v3

    move-object v0, p0

    check-cast v0, Lbmm;

    invoke-virtual {v0}, Lbmm;->i()I

    move-result v0

    iget-object v5, p0, Lbmr;->m:Lyj;

    invoke-static {v5}, Lbbl;->g(Lyj;)Landroid/content/Intent;

    move-result-object v5

    const/high16 v6, 0x10000000

    invoke-static {v3, v0, v5, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    sget v5, Lcom/google/android/apps/hangouts/R$drawable;->ca:I

    sget v6, Lh;->ch:I

    invoke-virtual {v3, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6, v0}, Lbk;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Lbk;

    new-instance v5, Lbh;

    sget v6, Lcom/google/android/apps/hangouts/R$drawable;->bw:I

    sget v7, Lh;->ch:I

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7, v0}, Lbh;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->i()Ljava/util/List;

    move-result-object v0

    new-instance v6, Lcm;

    const-string v7, "dnd_duration_choice"

    invoke-direct {v6, v7}, Lcm;-><init>(Ljava/lang/String;)V

    sget v7, Lh;->cg:I

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Lcm;->a(Ljava/lang/CharSequence;)Lcm;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v6

    new-array v6, v6, [Ljava/lang/String;

    invoke-interface {v0, v6}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    invoke-virtual {v3, v0}, Lcm;->a([Ljava/lang/CharSequence;)Lcm;

    move-result-object v0

    invoke-virtual {v0}, Lcm;->a()Lcm;

    move-result-object v0

    invoke-virtual {v0}, Lcm;->b()Lcl;

    move-result-object v0

    invoke-virtual {v5, v0}, Lbh;->a(Lcl;)Lbh;

    invoke-virtual {v5}, Lbh;->a()Lbg;

    move-result-object v0

    invoke-virtual {v2, v0}, Lbv;->a(Lbg;)Lbv;

    goto/16 :goto_3

    .line 933
    :catch_0
    move-exception v1

    const-string v2, "Babel"

    const-string v3, "Failed to set category for notification."

    invoke-static {v2, v3, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_4

    :catch_1
    move-exception v1

    const-string v2, "Babel"

    const-string v3, "Failed to set category for notification."

    invoke-static {v2, v3, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_4
.end method

.method static synthetic b(Lbv;Lbmr;)V
    .locals 0

    .prologue
    .line 101
    invoke-static {p0, p1}, Lbne;->d(Lbv;Lbmr;)V

    return-void
.end method

.method public static b(Lyj;)V
    .locals 2

    .prologue
    .line 244
    sget-object v1, Lbne;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 245
    :try_start_0
    invoke-static {p0}, Lbmf;->d(Lyj;)V

    .line 246
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static b(Lbmr;)Z
    .locals 1

    .prologue
    .line 850
    if-eqz p0, :cond_0

    .line 851
    invoke-virtual {p0}, Lbmr;->l()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static c(Lyj;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 501
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Lbne;->b(Lyj;ILjava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static c(Lbv;Lbmr;)V
    .locals 4

    .prologue
    .line 962
    instance-of v0, p1, Lbmn;

    if-nez v0, :cond_1

    .line 977
    :cond_0
    :goto_0
    return-void

    .line 965
    :cond_1
    invoke-static {}, Lbne;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 968
    iget-object v0, p1, Lbmr;->n:Lbxz;

    invoke-virtual {v0}, Lbxz;->a()Ljava/lang/String;

    move-result-object v0

    .line 969
    iget-object v1, p1, Lbmr;->m:Lyj;

    invoke-static {v1, v0}, Lyc;->b(Lyj;Ljava/lang/String;)I

    move-result v1

    .line 971
    iget-object v2, p1, Lbmr;->m:Lyj;

    .line 973
    invoke-virtual {p1}, Lbmr;->c()Z

    move-result v3

    .line 971
    invoke-static {v2, v0, v1, v3}, Lbmf;->a(Lyj;Ljava/lang/String;IZ)Landroid/app/Notification;

    move-result-object v0

    .line 974
    if-eqz v0, :cond_0

    .line 975
    invoke-virtual {p0, v0}, Lbv;->a(Landroid/app/Notification;)Lbv;

    goto :goto_0
.end method

.method public static c(Lyj;)V
    .locals 3

    .prologue
    .line 253
    sget-object v1, Lbne;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 254
    const/4 v0, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-static {p0, v0, v2}, Lbne;->a(Lyj;ILjava/lang/String;)V

    .line 255
    invoke-static {p0}, Lbne;->e(Lyj;)V

    .line 256
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static d(Lyj;I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 456
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    sget v0, Lh;->am:I

    :goto_0
    sget v2, Lf;->hO:I

    if-nez p1, :cond_0

    const/4 v1, 0x1

    .line 455
    :goto_1
    invoke-static {p0, v0, v2, v1}, Lf;->a(Lyj;IIZ)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 456
    :pswitch_1
    sget v0, Lh;->lt:I

    goto :goto_0

    :pswitch_2
    sget v0, Lh;->dv:I

    goto :goto_0

    :pswitch_3
    sget v0, Lh;->dy:I

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static d(Lbv;Lbmr;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 981
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v4

    .line 982
    instance-of v0, p1, Lbmn;

    if-nez v0, :cond_1

    instance-of v0, p1, Lblz;

    if-nez v0, :cond_1

    .line 1037
    :cond_0
    :goto_0
    return-void

    .line 987
    :cond_1
    iget-object v0, p1, Lbmr;->m:Lyj;

    iget-object v1, p1, Lbmr;->n:Lbxz;

    .line 988
    invoke-virtual {v1}, Lbxz;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lbmr;->c()Z

    move-result v5

    .line 987
    invoke-static {v0, v1, v5}, Lbbl;->a(Lyj;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v5

    .line 991
    instance-of v0, p1, Lbmn;

    if-eqz v0, :cond_6

    move-object v0, p1

    .line 992
    check-cast v0, Lbmn;

    .line 994
    invoke-virtual {p1}, Lbmr;->l()I

    move-result v1

    if-nez v1, :cond_2

    move v1, v2

    .line 996
    :goto_1
    const-string v6, "is_sms"

    invoke-virtual {v5, v6, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 997
    const-string v6, "wearable_watermark"

    iget-wide v7, v0, Lbmn;->l:J

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 999
    iget-object v0, v0, Lbmn;->j:Lbmh;

    iget-object v0, v0, Lbmh;->b:Ljava/util/List;

    .line 1000
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmj;

    iget-boolean v0, v0, Lbmj;->c:Z

    .line 1001
    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    invoke-static {}, Lbvx;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1002
    :goto_2
    const-string v0, "requires_mms"

    invoke-virtual {v5, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1004
    if-eqz v2, :cond_4

    .line 1005
    sget v0, Lh;->hI:I

    .line 1024
    :goto_3
    invoke-virtual {p1}, Lbmr;->o()I

    move-result v1

    .line 1025
    const/high16 v2, 0x8000000

    invoke-static {v4, v1, v5, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 1027
    new-instance v2, Lbh;

    sget v3, Lcom/google/android/apps/hangouts/R$drawable;->bx:I

    .line 1028
    invoke-virtual {v4, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0, v1}, Lbh;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 1029
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lf;->bz:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 1031
    new-instance v1, Lcm;

    const-string v3, "android.intent.extra.TEXT"

    invoke-direct {v1, v3}, Lcm;-><init>(Ljava/lang/String;)V

    sget v3, Lh;->hF:I

    .line 1032
    invoke-virtual {v4, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcm;->a(Ljava/lang/CharSequence;)Lcm;

    move-result-object v1

    .line 1033
    invoke-virtual {v1, v0}, Lcm;->a([Ljava/lang/CharSequence;)Lcm;

    move-result-object v0

    .line 1034
    invoke-virtual {v0}, Lcm;->b()Lcl;

    move-result-object v0

    .line 1035
    invoke-virtual {v2, v0}, Lbh;->a(Lcl;)Lbh;

    .line 1036
    invoke-virtual {v2}, Lbh;->a()Lbg;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbv;->a(Lbg;)Lbv;

    goto/16 :goto_0

    :cond_2
    move v1, v3

    .line 994
    goto :goto_1

    :cond_3
    move v2, v3

    .line 1001
    goto :goto_2

    .line 1006
    :cond_4
    if-eqz v1, :cond_5

    .line 1007
    sget v0, Lh;->hJ:I

    goto :goto_3

    .line 1009
    :cond_5
    sget v0, Lh;->hH:I

    goto :goto_3

    :cond_6
    move-object v0, p1

    .line 1013
    check-cast v0, Lblz;

    .line 1014
    iget-object v0, v0, Lblz;->n:Lbxz;

    invoke-virtual {v0}, Lbxz;->size()I

    move-result v0

    if-gt v0, v2, :cond_0

    .line 1019
    const-string v0, "is_sms"

    invoke-virtual {v5, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1020
    const-string v0, "requires_mms"

    invoke-virtual {v5, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1021
    sget v0, Lh;->hG:I

    goto :goto_3
.end method

.method public static d(Lyj;)V
    .locals 3

    .prologue
    .line 279
    sget-object v1, Lbne;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 280
    const/4 v0, 0x7

    const/4 v2, 0x0

    :try_start_0
    invoke-static {p0, v0, v2}, Lbne;->a(Lyj;ILjava/lang/String;)V

    .line 281
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static e(Lyj;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 288
    invoke-static {p0, v1}, Lbne;->a(Lyj;Ljava/util/ArrayList;)Ljava/util/Set;

    move-result-object v0

    invoke-static {p0, v0}, Lbne;->a(Lyj;Ljava/util/Set;)V

    .line 289
    const/4 v0, 0x2

    invoke-static {p0, v0, v1}, Lbne;->a(Lyj;ILjava/lang/String;)V

    .line 290
    return-void
.end method
