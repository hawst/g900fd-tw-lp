.class public Labk;
.super Lakq;
.source "PG"

# interfaces
.implements Labi;
.implements Ladw;
.implements Laeo;
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Landroid/widget/AbsListView$RecyclerListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Law;
.implements Lbsb;
.implements Lbsi;
.implements Lbzl;
.implements Lcfv;
.implements Lcfw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lakq",
        "<",
        "Landroid/widget/ListView;",
        ">;",
        "Labi;",
        "Ladw;",
        "Laeo;",
        "Landroid/widget/AbsListView$OnScrollListener;",
        "Landroid/widget/AbsListView$RecyclerListener;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Law",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lbsb;",
        "Lbsi;",
        "Lbzl;",
        "Lcfv;",
        "Lcfw;"
    }
.end annotation


# instance fields
.field private Y:Laoe;

.field private Z:Lxm;

.field private a:Lcvi;

.field private aa:Z

.field private final ab:Lbor;

.field private b:Labg;

.field private c:Z

.field private d:Z

.field private e:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcvo;

.field private g:Lbsc;

.field private i:Laen;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Lakq;-><init>()V

    .line 95
    const/4 v0, 0x0

    iput-boolean v0, p0, Labk;->d:Z

    .line 427
    new-instance v0, Labl;

    invoke-direct {v0, p0}, Labl;-><init>(Labk;)V

    iput-object v0, p0, Labk;->ab:Lbor;

    return-void
.end method

.method static synthetic a(Labk;)Landroid/util/SparseArray;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Labk;->e:Landroid/util/SparseArray;

    return-object v0
.end method

.method static synthetic a(Labk;I)V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Labk;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    iget-object v0, p0, Labk;->e:Landroid/util/SparseArray;

    invoke-static {v0}, Lf;->a(Landroid/util/SparseArray;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Labk;->q()V

    :cond_0
    return-void
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 4

    .prologue
    .line 595
    if-nez p1, :cond_0

    .line 605
    :goto_0
    return-void

    .line 598
    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 599
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 600
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_1

    .line 601
    invoke-interface {p1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 602
    const/4 v3, 0x2

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 600
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 604
    :cond_1
    invoke-static {}, Lbzh;->b()Lbzh;

    move-result-object v0

    invoke-direct {p0}, Labk;->s()Lyj;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lbzh;->a(Ljava/util/List;Lyj;)V

    goto :goto_0
.end method

.method private f()V
    .locals 4

    .prologue
    .line 153
    iget-boolean v0, p0, Labk;->aa:Z

    if-eqz v0, :cond_0

    .line 186
    :goto_0
    return-void

    .line 157
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Labk;->aa:Z

    .line 159
    invoke-direct {p0}, Labk;->s()Lyj;

    move-result-object v0

    .line 160
    if-nez v0, :cond_1

    .line 162
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "account null in maybeInitialize"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 165
    :cond_1
    invoke-virtual {p0}, Labk;->getActivity()Ly;

    move-result-object v1

    invoke-virtual {v1}, Ly;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "transport_type"

    .line 167
    invoke-virtual {v0}, Lyj;->W()I

    move-result v3

    .line 165
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 169
    new-instance v2, Labg;

    invoke-virtual {p0}, Labk;->getActivity()Ly;

    move-result-object v3

    invoke-direct {v2, v3, v0, p0, v1}, Labg;-><init>(Landroid/content/Context;Lyj;Lt;I)V

    iput-object v2, p0, Labk;->b:Labg;

    .line 170
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Labk;->e:Landroid/util/SparseArray;

    .line 172
    iget-object v0, p0, Labk;->Z:Lxm;

    if-eqz v0, :cond_2

    .line 173
    iget-object v0, p0, Labk;->Z:Lxm;

    invoke-virtual {p0, v0}, Labk;->a(Lxm;)V

    .line 174
    const/4 v0, 0x0

    iput-object v0, p0, Labk;->Z:Lxm;

    .line 177
    :cond_2
    new-instance v0, Lcvi;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0, p0}, Lcvi;-><init>(Landroid/content/Context;Lcfv;Lcfw;)V

    iput-object v0, p0, Labk;->a:Lcvi;

    .line 179
    iget-object v0, p0, Labk;->a:Lcvi;

    invoke-virtual {v0}, Lcvi;->a()V

    .line 180
    iget-object v0, p0, Labk;->g:Lbsc;

    if-nez v0, :cond_3

    .line 181
    invoke-static {}, Lbsc;->a()Lbsc;

    move-result-object v0

    iput-object v0, p0, Labk;->g:Lbsc;

    .line 182
    iget-object v0, p0, Labk;->g:Lbsc;

    invoke-virtual {v0, p0}, Lbsc;->a(Lbsi;)V

    .line 185
    :cond_3
    invoke-static {}, Lbzh;->b()Lbzh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbzh;->a(Lbzl;)V

    goto :goto_0
.end method

.method private q()V
    .locals 1

    .prologue
    .line 470
    iget-boolean v0, p0, Labk;->d:Z

    if-eqz v0, :cond_0

    .line 471
    iget-object v0, p0, Labk;->ab:Lbor;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lbor;)V

    .line 472
    const/4 v0, 0x0

    iput-boolean v0, p0, Labk;->d:Z

    .line 474
    :cond_0
    return-void
.end method

.method private r()V
    .locals 1

    .prologue
    .line 545
    iget-object v0, p0, Labk;->Y:Laoe;

    if-eqz v0, :cond_0

    .line 546
    iget-object v0, p0, Labk;->Y:Laoe;

    invoke-interface {v0}, Laoe;->a()V

    .line 549
    :cond_0
    iget-object v0, p0, Labk;->b:Labg;

    if-eqz v0, :cond_1

    .line 550
    iget-object v0, p0, Labk;->b:Labg;

    invoke-virtual {v0}, Labg;->notifyDataSetChanged()V

    .line 552
    :cond_1
    return-void
.end method

.method private s()Lyj;
    .locals 2

    .prologue
    .line 632
    invoke-virtual {p0}, Labk;->getActivity()Ly;

    move-result-object v0

    check-cast v0, Lakn;

    .line 634
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lakn;->k()Lyj;

    move-result-object v1

    if-nez v1, :cond_1

    .line 635
    :cond_0
    const-string v0, "Babel"

    const-string v1, "Account is no longer valid in AudienceListFragment."

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 636
    const/4 v0, 0x0

    .line 639
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v0}, Lakn;->k()Lyj;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 744
    iget-object v0, p0, Labk;->f:Lcvo;

    if-eqz v0, :cond_0

    .line 745
    iget-object v0, p0, Labk;->f:Lcvo;

    invoke-virtual {v0, p1}, Lcvo;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 747
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 0

    .prologue
    .line 556
    invoke-direct {p0}, Labk;->r()V

    .line 557
    return-void
.end method

.method public a(I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 255
    invoke-direct {p0}, Labk;->f()V

    .line 256
    invoke-virtual {p0}, Labk;->getLoaderManager()Lav;

    move-result-object v1

    .line 257
    invoke-virtual {v1, v5}, Lav;->b(I)Ldg;

    move-result-object v2

    .line 261
    if-eqz v2, :cond_0

    iget-object v0, p0, Labk;->b:Labg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Labk;->b:Labg;

    .line 263
    invoke-virtual {v0}, Labg;->i()Lyj;

    move-result-object v0

    invoke-direct {p0}, Labk;->s()Lyj;

    move-result-object v3

    invoke-virtual {v0, v3}, Lyj;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Labk;->b:Labg;

    .line 264
    invoke-virtual {v0}, Labg;->h()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 265
    invoke-virtual {p0}, Labk;->ai()V

    .line 293
    :goto_0
    return-void

    .line 269
    :cond_0
    new-instance v0, Labg;

    invoke-virtual {p0}, Labk;->getActivity()Ly;

    move-result-object v3

    invoke-direct {p0}, Labk;->s()Lyj;

    move-result-object v4

    invoke-direct {v0, v3, v4, p0, p1}, Labg;-><init>(Landroid/content/Context;Lyj;Lt;I)V

    iput-object v0, p0, Labk;->b:Labg;

    .line 270
    iget-object v0, p0, Labk;->h:Landroid/widget/AbsListView;

    if-eqz v0, :cond_1

    .line 271
    iget-object v0, p0, Labk;->h:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    iget-object v3, p0, Labk;->b:Labg;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 274
    :cond_1
    if-eqz v2, :cond_2

    .line 275
    invoke-virtual {v1, v5}, Lav;->a(I)V

    .line 278
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {v1, v5, v0, p0}, Lav;->a(ILandroid/os/Bundle;Law;)Ldg;

    .line 280
    iget-object v0, p0, Labk;->a:Lcvi;

    if-eqz v0, :cond_3

    iget-object v0, p0, Labk;->a:Lcvi;

    invoke-virtual {v0}, Lcvi;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 281
    new-instance v0, Lcvo;

    iget-object v1, p0, Labk;->a:Lcvi;

    invoke-direct {p0}, Labk;->s()Lyj;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0}, Lcvo;-><init>(Lcvi;Lyj;Lbsb;)V

    iput-object v0, p0, Labk;->f:Lcvo;

    .line 282
    iget-object v0, p0, Labk;->f:Lcvo;

    invoke-virtual {v0}, Lcvo;->a()V

    .line 286
    :cond_3
    iget-object v0, p0, Labk;->g:Lbsc;

    if-eqz v0, :cond_4

    .line 287
    iget-object v0, p0, Labk;->g:Lbsc;

    invoke-virtual {v0, p0}, Lbsc;->b(Lbsi;)V

    .line 288
    iget-object v0, p0, Labk;->g:Lbsc;

    invoke-virtual {v0}, Lbsc;->b()V

    .line 291
    :cond_4
    invoke-static {}, Lbsc;->a()Lbsc;

    move-result-object v0

    iput-object v0, p0, Labk;->g:Lbsc;

    .line 292
    iget-object v0, p0, Labk;->g:Lbsc;

    invoke-virtual {v0, p0}, Lbsc;->a(Lbsi;)V

    goto :goto_0
.end method

.method public a(Laen;)V
    .locals 0

    .prologue
    .line 697
    iput-object p1, p0, Labk;->i:Laen;

    .line 698
    return-void
.end method

.method public a(Laoe;)V
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Labk;->Y:Laoe;

    .line 120
    return-void
.end method

.method public a(Lcvx;Lcwf;)V
    .locals 1

    .prologue
    .line 575
    iget-object v0, p0, Labk;->b:Labg;

    invoke-virtual {v0, p1, p2}, Labg;->a(Lcvx;Lcwf;)V

    .line 576
    invoke-virtual {p0}, Labk;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 577
    invoke-virtual {p0}, Labk;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Labk;->a_(Landroid/view/View;)V

    .line 579
    :cond_0
    return-void
.end method

.method public a(Lcvz;)V
    .locals 1

    .prologue
    .line 566
    invoke-virtual {p1}, Lcvz;->d()V

    .line 567
    invoke-virtual {p0}, Labk;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Labk;->a_(Landroid/view/View;)V

    .line 568
    return-void
.end method

.method public a(Ldg;Landroid/database/Cursor;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 609
    invoke-direct {p0}, Labk;->s()Lyj;

    move-result-object v0

    .line 610
    if-eqz v0, :cond_0

    if-nez p2, :cond_1

    invoke-virtual {v0}, Lyj;->z()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Labk;->c:Z

    .line 612
    invoke-virtual {p1}, Ldg;->l()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 620
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown loader: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ldg;->l()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 610
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 614
    :pswitch_0
    iget-object v0, p0, Labk;->b:Labg;

    invoke-virtual {v0, p2}, Labg;->a(Landroid/database/Cursor;)Ljava/util/HashSet;

    move-result-object v0

    .line 615
    iget-object v1, p0, Labk;->b:Labg;

    invoke-virtual {v1}, Labg;->k()Landroid/database/Cursor;

    move-result-object v1

    invoke-direct {p0, v1}, Labk;->a(Landroid/database/Cursor;)V

    .line 616
    iget-object v1, p0, Labk;->b:Labg;

    invoke-virtual {v1}, Labg;->l()Landroid/database/Cursor;

    move-result-object v1

    invoke-direct {p0, v1}, Labk;->a(Landroid/database/Cursor;)V

    .line 617
    iget-object v1, p0, Labk;->a:Lcvi;

    invoke-virtual {v1}, Lcvi;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    if-lez v1, :cond_2

    new-instance v1, Lcvm;

    invoke-direct {v1}, Lcvm;-><init>()V

    invoke-virtual {v1, v0}, Lcvm;->a(Ljava/util/Collection;)Lcvm;

    iget-object v0, p0, Labk;->a:Lcvi;

    iget-object v2, p0, Labk;->b:Labg;

    invoke-virtual {v2}, Labg;->p()Lcvr;

    move-result-object v2

    invoke-direct {p0}, Labk;->s()Lyj;

    move-result-object v3

    invoke-virtual {v3}, Lyj;->ak()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0}, Labk;->s()Lyj;

    move-result-object v4

    invoke-virtual {v4}, Lyj;->al()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4, v1}, Lcvi;->a(Lcvr;Ljava/lang/String;Ljava/lang/String;Lcvm;)V

    .line 622
    :cond_2
    invoke-virtual {p0}, Labk;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Labk;->a_(Landroid/view/View;)V

    .line 624
    invoke-virtual {p0}, Labk;->ai()V

    .line 625
    return-void

    .line 612
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public a(Ljava/lang/String;Lbdn;)V
    .locals 0

    .prologue
    .line 304
    return-void
.end method

.method public a(Lxm;)V
    .locals 1

    .prologue
    .line 708
    iget-object v0, p0, Labk;->b:Labg;

    if-eqz v0, :cond_0

    .line 709
    iget-object v0, p0, Labk;->b:Labg;

    invoke-virtual {v0, p1}, Labg;->a(Lxm;)V

    .line 710
    invoke-direct {p0}, Labk;->r()V

    .line 712
    :cond_0
    return-void
.end method

.method protected a_(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 511
    if-nez p1, :cond_0

    .line 539
    :goto_0
    return-void

    .line 517
    :cond_0
    const v0, 0x102000a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 518
    sget v1, Lg;->gF:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 519
    iget-boolean v2, p0, Labk;->c:Z

    if-eqz v2, :cond_1

    .line 520
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 521
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 522
    invoke-virtual {p0, p1}, Labk;->b(Landroid/view/View;)V

    .line 538
    :goto_1
    invoke-direct {p0}, Labk;->r()V

    goto :goto_0

    .line 523
    :cond_1
    invoke-virtual {p0}, Labk;->d()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 524
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 525
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 526
    invoke-virtual {p0, p1}, Labk;->e(Landroid/view/View;)V

    goto :goto_1

    .line 527
    :cond_2
    invoke-virtual {p0}, Labk;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 528
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 529
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 530
    sget v0, Lh;->hz:I

    invoke-virtual {p0, p1, v0}, Labk;->a(Landroid/view/View;I)V

    .line 531
    invoke-virtual {p0, p1}, Labk;->c(Landroid/view/View;)V

    goto :goto_1

    .line 533
    :cond_3
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 534
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 535
    invoke-virtual {p0, p1}, Labk;->b(Landroid/view/View;)V

    goto :goto_1
.end method

.method public c()Laen;
    .locals 1

    .prologue
    .line 689
    iget-object v0, p0, Labk;->i:Laen;

    return-object v0
.end method

.method protected d()Z
    .locals 1

    .prologue
    .line 498
    iget-object v0, p0, Labk;->b:Labg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Labk;->b:Labg;

    invoke-virtual {v0}, Labg;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Lxm;
    .locals 1

    .prologue
    .line 720
    iget-object v0, p0, Labk;->b:Labg;

    if-nez v0, :cond_0

    .line 721
    invoke-static {}, Lxm;->newBuilder()Lxn;

    move-result-object v0

    invoke-virtual {v0}, Lxn;->a()Lxm;

    move-result-object v0

    .line 723
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Labk;->b:Labg;

    invoke-virtual {v0}, Labg;->o()Lxm;

    move-result-object v0

    goto :goto_0
.end method

.method public f_()V
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Labk;->b:Labg;

    if-eqz v0, :cond_0

    .line 298
    iget-object v0, p0, Labk;->b:Labg;

    invoke-virtual {v0}, Labg;->notifyDataSetChanged()V

    .line 300
    :cond_0
    return-void
.end method

.method protected isEmpty()Z
    .locals 1

    .prologue
    .line 503
    invoke-virtual {p0}, Labk;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Labk;->b:Labg;

    invoke-virtual {v0}, Labg;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 124
    invoke-super {p0, p1}, Lakq;->onAttach(Landroid/app/Activity;)V

    .line 125
    return-void
.end method

.method public onConnected(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 210
    const-string v0, "Babel"

    const-string v1, "People client connected."

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    invoke-virtual {p0}, Labk;->getActivity()Ly;

    move-result-object v0

    if-nez v0, :cond_1

    .line 215
    const-string v0, "Babel"

    const-string v1, "People client connected but AudienceListFragment is detached."

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    :cond_0
    :goto_0
    return-void

    .line 218
    :cond_1
    invoke-direct {p0}, Labk;->s()Lyj;

    move-result-object v0

    if-nez v0, :cond_2

    .line 219
    const-string v0, "Babel"

    const-string v1, "People client connected but account is null."

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 223
    :cond_2
    iget-object v0, p0, Labk;->a:Lcvi;

    invoke-static {v0}, Lf;->a(Lcvi;)V

    .line 226
    iget-object v0, p0, Labk;->f:Lcvo;

    if-nez v0, :cond_0

    .line 227
    new-instance v0, Lcvo;

    iget-object v1, p0, Labk;->a:Lcvi;

    invoke-direct {p0}, Labk;->s()Lyj;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0}, Lcvo;-><init>(Lcvi;Lyj;Lbsb;)V

    iput-object v0, p0, Labk;->f:Lcvo;

    .line 228
    iget-object v0, p0, Labk;->f:Lcvo;

    invoke-virtual {v0}, Lcvo;->a()V

    goto :goto_0
.end method

.method public onConnectionFailed(Lcft;)V
    .locals 3

    .prologue
    .line 244
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "People client connection failure: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 406
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 407
    const/16 v2, 0xc8

    if-ne v0, v2, :cond_2

    .line 408
    invoke-interface {p1}, Landroid/view/MenuItem;->getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v0

    .line 409
    instance-of v2, v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    if-eqz v2, :cond_1

    .line 410
    check-cast v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 412
    iget-object v0, v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;->targetView:Landroid/view/View;

    check-cast v0, Lceb;

    .line 413
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lceb;->isDismissable()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 414
    invoke-virtual {v0}, Lceb;->getContactName()Ljava/lang/String;

    move-result-object v2

    .line 415
    iget-boolean v3, p0, Labk;->d:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Labk;->ab:Lbor;

    invoke-static {v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbor;)V

    iput-boolean v1, p0, Labk;->d:Z

    .line 416
    :cond_0
    invoke-direct {p0}, Labk;->s()Lyj;

    move-result-object v3

    .line 417
    invoke-virtual {v0}, Lceb;->getInviteeId()Lbcx;

    move-result-object v0

    iget-object v0, v0, Lbcx;->a:Ljava/lang/String;

    .line 416
    invoke-static {v3, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->l(Lyj;Ljava/lang/String;)I

    move-result v0

    .line 418
    iget-object v3, p0, Labk;->e:Landroid/util/SparseArray;

    invoke-virtual {v3, v0, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    :cond_1
    move v0, v1

    .line 424
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 129
    invoke-super {p0, p1}, Lakq;->onCreate(Landroid/os/Bundle;)V

    .line 131
    if-eqz p1, :cond_0

    .line 132
    const-string v0, "audience"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lxm;

    iput-object v0, p0, Labk;->Z:Lxm;

    .line 138
    :cond_0
    invoke-direct {p0}, Labk;->s()Lyj;

    move-result-object v0

    if-nez v0, :cond_1

    .line 143
    :goto_0
    return-void

    .line 142
    :cond_1
    invoke-direct {p0}, Labk;->f()V

    goto :goto_0
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 4

    .prologue
    const/16 v1, 0xc8

    const/4 v3, 0x0

    .line 383
    invoke-super {p0, p1, p2, p3}, Lakq;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 384
    const/4 v2, -0x1

    .line 385
    iget-object v0, p0, Labk;->h:Landroid/widget/AbsListView;

    if-ne p2, v0, :cond_1

    .line 386
    check-cast p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 388
    iget-object v0, p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;->targetView:Landroid/view/View;

    instance-of v0, v0, Lceb;

    if-eqz v0, :cond_1

    .line 389
    iget-object v0, p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;->targetView:Landroid/view/View;

    check-cast v0, Lceb;

    .line 391
    invoke-virtual {v0}, Lceb;->isDismissable()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 397
    :goto_0
    if-lez v0, :cond_0

    .line 398
    sget v0, Lh;->gp:I

    invoke-interface {p1, v3, v1, v3, v0}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 399
    sget v0, Lh;->gl:I

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->setHeaderTitle(I)Landroid/view/ContextMenu;

    .line 401
    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Ldg;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 583
    packed-switch p1, :pswitch_data_0

    .line 590
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown loader: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 585
    :pswitch_0
    new-instance v0, Lanv;

    .line 586
    invoke-virtual {p0}, Labk;->getActivity()Ly;

    move-result-object v1

    .line 587
    invoke-direct {p0}, Labk;->s()Lyj;

    move-result-object v2

    sget-object v3, Labg;->a:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lanv;-><init>(Landroid/content/Context;Lyj;[Ljava/lang/String;)V

    return-object v0

    .line 583
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 342
    sget v0, Lf;->ee:I

    invoke-super {p0, p1, p2, p3, v0}, Lakq;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v7

    .line 344
    const v0, 0x102000a

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Labk;->h:Landroid/widget/AbsListView;

    .line 345
    iget-object v0, p0, Labk;->h:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 346
    iget-object v0, p0, Labk;->h:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFocusable(Z)V

    .line 347
    iget-object v0, p0, Labk;->h:Landroid/widget/AbsListView;

    invoke-virtual {p0, v0}, Labk;->registerForContextMenu(Landroid/view/View;)V

    .line 349
    iget-object v0, p0, Labk;->h:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    iget-object v1, p0, Labk;->b:Labg;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 350
    iget-object v0, p0, Labk;->h:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 351
    iget-object v0, p0, Labk;->h:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 352
    iget-object v0, p0, Labk;->h:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    .line 354
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 369
    invoke-virtual {p0}, Labk;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lf;->dk:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 370
    iget-object v0, p0, Labk;->h:Landroid/widget/AbsListView;

    move-object v6, v0

    check-cast v6, Landroid/widget/ListView;

    new-instance v0, Landroid/graphics/drawable/InsetDrawable;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const v4, -0x333334

    invoke-direct {v1, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    move v4, v2

    move v5, v3

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    invoke-virtual {v6, v0}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 375
    invoke-virtual {p0}, Labk;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lf;->cS:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 376
    iget-object v0, p0, Labk;->h:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 378
    :cond_0
    return-object v7
.end method

.method public bridge synthetic onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;
    .locals 1

    .prologue
    .line 73
    invoke-super {p0, p1, p2, p3, p4}, Lakq;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 308
    iget-object v0, p0, Labk;->g:Lbsc;

    if-eqz v0, :cond_0

    .line 309
    iget-object v0, p0, Labk;->g:Lbsc;

    invoke-virtual {v0, p0}, Lbsc;->b(Lbsi;)V

    .line 310
    iget-object v0, p0, Labk;->g:Lbsc;

    invoke-virtual {v0}, Lbsc;->b()V

    .line 311
    iput-object v1, p0, Labk;->g:Lbsc;

    .line 314
    :cond_0
    iget-object v0, p0, Labk;->f:Lcvo;

    if-eqz v0, :cond_1

    .line 315
    iget-object v0, p0, Labk;->f:Lcvo;

    invoke-virtual {v0}, Lcvo;->b()V

    .line 316
    iput-object v1, p0, Labk;->f:Lcvo;

    .line 319
    :cond_1
    iget-object v0, p0, Labk;->a:Lcvi;

    if-eqz v0, :cond_3

    iget-object v0, p0, Labk;->a:Lcvi;

    invoke-virtual {v0}, Lcvi;->d()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Labk;->a:Lcvi;

    invoke-virtual {v0}, Lcvi;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 320
    :cond_2
    iget-object v0, p0, Labk;->a:Lcvi;

    invoke-virtual {v0}, Lcvi;->b()V

    .line 321
    iput-object v1, p0, Labk;->a:Lcvi;

    .line 323
    :cond_3
    invoke-static {}, Lbzh;->b()Lbzh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbzh;->b(Lbzl;)V

    .line 325
    iget-object v0, p0, Labk;->b:Labg;

    if-eqz v0, :cond_4

    .line 326
    iget-object v0, p0, Labk;->b:Labg;

    invoke-static {}, Labg;->g()V

    .line 328
    :cond_4
    iput-object v1, p0, Labk;->e:Landroid/util/SparseArray;

    .line 329
    invoke-super {p0}, Lakq;->onDestroy()V

    .line 330
    invoke-direct {p0}, Labk;->q()V

    .line 331
    return-void
.end method

.method public bridge synthetic onDestroyView()V
    .locals 0

    .prologue
    .line 73
    invoke-super {p0}, Lakq;->onDestroyView()V

    return-void
.end method

.method public onDisconnected()V
    .locals 2

    .prologue
    .line 234
    const-string v0, "Babel"

    const-string v1, "People client disconnected."

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    iget-object v0, p0, Labk;->f:Lcvo;

    if-eqz v0, :cond_0

    .line 237
    const-string v0, "Babel"

    const-string v1, "Resetting mGmsCirclesLoader after people client disconnect"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    const/4 v0, 0x0

    iput-object v0, p0, Labk;->f:Lcvo;

    .line 240
    :cond_0
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 646
    instance-of v0, p2, Lcax;

    if-eqz v0, :cond_0

    move-object v0, p2

    .line 647
    check-cast v0, Lcax;

    .line 648
    iget-object v1, p0, Labk;->Y:Laoe;

    invoke-virtual {v0, v1}, Lcax;->isLocked(Laoe;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 649
    invoke-virtual {v0, p2}, Lcax;->onClick(Landroid/view/View;)V

    .line 650
    iget-object v0, p0, Labk;->b:Labg;

    invoke-virtual {v0}, Labg;->notifyDataSetChanged()V

    .line 653
    :cond_0
    return-void
.end method

.method public synthetic onLoadFinished(Ldg;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 73
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Labk;->a(Ldg;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Ldg;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 629
    return-void
.end method

.method public onMovedToScrapHeap(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 681
    iget-object v0, p0, Labk;->b:Labg;

    invoke-static {}, Labg;->n()V

    .line 682
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 478
    invoke-super {p0}, Lakq;->onResume()V

    .line 481
    invoke-direct {p0}, Labk;->s()Lyj;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 482
    invoke-virtual {p0}, Labk;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Labk;->a_(Landroid/view/View;)V

    .line 486
    :cond_0
    iget-object v0, p0, Labk;->b:Labg;

    invoke-virtual {v0}, Labg;->k()Landroid/database/Cursor;

    move-result-object v0

    .line 487
    if-eqz v0, :cond_1

    .line 488
    invoke-direct {p0, v0}, Labk;->a(Landroid/database/Cursor;)V

    .line 491
    :cond_1
    iget-object v0, p0, Labk;->b:Labg;

    invoke-virtual {v0}, Labg;->l()Landroid/database/Cursor;

    move-result-object v0

    .line 492
    if-eqz v0, :cond_2

    .line 493
    invoke-direct {p0, v0}, Labk;->a(Landroid/database/Cursor;)V

    .line 495
    :cond_2
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 335
    invoke-super {p0, p1}, Lakq;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 336
    const-string v0, "audience"

    invoke-virtual {p0}, Labk;->e()Lxm;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 337
    return-void
.end method

.method public bridge synthetic onScroll(Landroid/widget/AbsListView;III)V
    .locals 0

    .prologue
    .line 73
    invoke-super {p0, p1, p2, p3, p4}, Lakq;->onScroll(Landroid/widget/AbsListView;III)V

    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 3

    .prologue
    .line 657
    invoke-super {p0, p1, p2}, Lakq;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    .line 661
    packed-switch p2, :pswitch_data_0

    .line 673
    :goto_0
    return-void

    .line 664
    :pswitch_0
    iget-object v0, p0, Labk;->b:Labg;

    invoke-static {}, Labg;->m()V

    .line 666
    invoke-virtual {p1}, Landroid/widget/AbsListView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    .line 667
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 668
    invoke-virtual {p1}, Landroid/widget/AbsListView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0

    .line 672
    :pswitch_1
    iget-object v0, p0, Labk;->b:Labg;

    invoke-static {}, Labg;->m()V

    goto :goto_0

    .line 661
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 190
    invoke-super {p0}, Lakq;->onStart()V

    .line 191
    invoke-direct {p0}, Labk;->f()V

    .line 195
    iget-object v0, p0, Labk;->a:Lcvi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Labk;->a:Lcvi;

    invoke-virtual {v0}, Lcvi;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Labk;->a:Lcvi;

    invoke-virtual {v0}, Lcvi;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 196
    const-string v0, "Babel"

    const-string v1, "Reconnecting people client for AudienceListFragment."

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    iget-object v0, p0, Labk;->a:Lcvi;

    invoke-virtual {v0}, Lcvi;->a()V

    .line 199
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Labk;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 204
    invoke-direct {p0}, Labk;->q()V

    .line 205
    invoke-super {p0}, Lakq;->onStop()V

    .line 206
    return-void
.end method
