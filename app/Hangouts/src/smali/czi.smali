.class public final Lczi;
.super Lcyw;
.source "PG"


# static fields
.field private static g:Lczh;


# instance fields
.field private h:Ljava/lang/String;

.field private i:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/security/PrivateKey;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 174
    new-instance v0, Lczh;

    invoke-direct {v0}, Lczh;-><init>()V

    sput-object v0, Lczi;->g:Lczh;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 306
    new-instance v0, Lczj;

    invoke-direct {v0}, Lczj;-><init>()V

    invoke-direct {p0, v0}, Lczi;-><init>(Lczj;)V

    .line 307
    return-void
.end method

.method private constructor <init>(Lczj;)V
    .locals 1

    .prologue
    .line 315
    invoke-direct {p0, p1}, Lcyw;-><init>(Lcyy;)V

    .line 316
    iget-object v0, p1, Lczj;->k:Ljava/security/PrivateKey;

    if-nez v0, :cond_1

    .line 317
    iget-object v0, p1, Lczj;->i:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p1, Lczj;->j:Ljava/util/Collection;

    if-nez v0, :cond_0

    iget-object v0, p1, Lczj;->m:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lh;->a(Z)V

    .line 326
    :goto_1
    return-void

    .line 317
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 320
    :cond_1
    iget-object v0, p1, Lczj;->i:Ljava/lang/String;

    invoke-static {v0}, Lg;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lczi;->h:Ljava/lang/String;

    .line 321
    iget-object v0, p1, Lczj;->j:Ljava/util/Collection;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    iput-object v0, p0, Lczi;->i:Ljava/util/Collection;

    .line 322
    iget-object v0, p1, Lczj;->k:Ljava/security/PrivateKey;

    iput-object v0, p0, Lczi;->j:Ljava/security/PrivateKey;

    .line 323
    iget-object v0, p1, Lczj;->l:Ljava/lang/String;

    iput-object v0, p0, Lczi;->k:Ljava/lang/String;

    .line 324
    iget-object v0, p1, Lczj;->m:Ljava/lang/String;

    iput-object v0, p0, Lczi;->l:Ljava/lang/String;

    goto :goto_1
.end method


# virtual methods
.method public bridge synthetic a(Lcze;)Lcyw;
    .locals 1

    .prologue
    .line 168
    invoke-super {p0, p1}, Lcyw;->a(Lcze;)Lcyw;

    move-result-object v0

    check-cast v0, Lczi;

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Long;)Lcyw;
    .locals 1

    .prologue
    .line 168
    invoke-super {p0, p1}, Lcyw;->a(Ljava/lang/Long;)Lcyw;

    move-result-object v0

    check-cast v0, Lczi;

    return-object v0
.end method

.method public synthetic a(Ljava/lang/String;)Lcyw;
    .locals 1

    .prologue
    .line 168
    invoke-virtual {p0, p1}, Lczi;->c(Ljava/lang/String;)Lczi;

    move-result-object v0

    return-object v0
.end method

.method protected a()Lcze;
    .locals 8

    .prologue
    const-wide/16 v6, 0x3e8

    .line 361
    iget-object v0, p0, Lczi;->j:Ljava/security/PrivateKey;

    if-nez v0, :cond_0

    .line 362
    invoke-super {p0}, Lcyw;->a()Lcze;

    move-result-object v0

    .line 384
    :goto_0
    return-object v0

    .line 365
    :cond_0
    new-instance v0, Lczs;

    invoke-direct {v0}, Lczs;-><init>()V

    .line 366
    const-string v1, "RS256"

    invoke-virtual {v0, v1}, Lczs;->b(Ljava/lang/String;)Lczs;

    .line 367
    const-string v1, "JWT"

    invoke-virtual {v0, v1}, Lczs;->a(Ljava/lang/String;)Lczs;

    .line 368
    iget-object v1, p0, Lczi;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lczs;->c(Ljava/lang/String;)Lczs;

    .line 369
    new-instance v1, Lczv;

    invoke-direct {v1}, Lczv;-><init>()V

    .line 370
    iget-object v2, p0, Lcyw;->b:Ldap;

    invoke-interface {v2}, Ldap;->a()J

    move-result-wide v2

    .line 371
    iget-object v4, p0, Lczi;->h:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lczv;->a(Ljava/lang/String;)Lczv;

    .line 372
    iget-object v4, p0, Lcyw;->f:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lczv;->a(Ljava/lang/Object;)Lczv;

    .line 373
    div-long v4, v2, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v4}, Lczv;->b(Ljava/lang/Long;)Lczv;

    .line 374
    div-long/2addr v2, v6

    const-wide/16 v4, 0xe10

    add-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lczv;->a(Ljava/lang/Long;)Lczv;

    .line 375
    iget-object v2, p0, Lczi;->l:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lczv;->b(Ljava/lang/String;)Lczv;

    .line 376
    const-string v2, "scope"

    new-instance v3, Ldbd;

    new-instance v4, Lczw;

    const-string v5, " "

    invoke-direct {v4, v5}, Lczw;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v4}, Ldbd;-><init>(Lczw;)V

    iget-object v4, p0, Lczi;->i:Ljava/util/Collection;

    iget-object v3, v3, Ldbd;->a:Lczw;

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v5, v4}, Lczw;->a(Ljava/lang/StringBuilder;Ljava/util/Iterator;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lczv;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 378
    :try_start_0
    iget-object v2, p0, Lczi;->j:Ljava/security/PrivateKey;

    .line 379
    iget-object v3, p0, Lcyw;->e:Lcom/google/api/client/json/JsonFactory;

    .line 378
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Lcom/google/api/client/json/JsonFactory;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v0

    invoke-static {v0}, Lczz;->b([B)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "."

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3, v1}, Lcom/google/api/client/json/JsonFactory;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v1

    invoke-static {v1}, Lczz;->b([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ldbr;->a(Ljava/lang/String;)[B

    move-result-object v1

    const-string v3, "SHA256withRSA"

    invoke-static {v3}, Ljava/security/Signature;->getInstance(Ljava/lang/String;)Ljava/security/Signature;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/security/Signature;->initSign(Ljava/security/PrivateKey;)V

    invoke-virtual {v3, v1}, Ljava/security/Signature;->update([B)V

    invoke-virtual {v3}, Ljava/security/Signature;->sign()[B

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v1}, Lczz;->b([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 380
    new-instance v1, Lczb;

    .line 381
    iget-object v2, p0, Lcyw;->c:Lcom/google/api/client/http/HttpTransport;

    iget-object v3, p0, Lcyw;->e:Lcom/google/api/client/json/JsonFactory;

    new-instance v4, Lcom/google/api/client/http/GenericUrl;

    iget-object v5, p0, Lcyw;->f:Ljava/lang/String;

    invoke-direct {v4, v5}, Lcom/google/api/client/http/GenericUrl;-><init>(Ljava/lang/String;)V

    const-string v5, "urn:ietf:params:oauth:grant-type:jwt-bearer"

    invoke-direct {v1, v2, v3, v4, v5}, Lczb;-><init>(Lcom/google/api/client/http/HttpTransport;Lcom/google/api/client/json/JsonFactory;Lcom/google/api/client/http/GenericUrl;Ljava/lang/String;)V

    .line 383
    const-string v2, "assertion"

    invoke-virtual {v1, v2, v0}, Lczb;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 384
    invoke-virtual {v1}, Lczb;->a()Lcze;
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto/16 :goto_0

    .line 385
    :catch_0
    move-exception v0

    .line 386
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    .line 387
    invoke-virtual {v1, v0}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 388
    throw v1
.end method

.method public bridge synthetic b(Ljava/lang/Long;)Lcyw;
    .locals 1

    .prologue
    .line 168
    invoke-super {p0, p1}, Lcyw;->b(Ljava/lang/Long;)Lcyw;

    move-result-object v0

    check-cast v0, Lczi;

    return-object v0
.end method

.method public synthetic b(Ljava/lang/String;)Lcyw;
    .locals 2

    .prologue
    .line 168
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcyw;->e:Lcom/google/api/client/json/JsonFactory;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcyw;->c:Lcom/google/api/client/http/HttpTransport;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcyw;->d:Lcom/google/api/client/http/HttpExecuteInterceptor;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Please use the Builder and call setJsonFactory, setTransport and setClientSecrets"

    invoke-static {v0, v1}, Lh;->a(ZLjava/lang/Object;)V

    :cond_0
    invoke-super {p0, p1}, Lcyw;->b(Ljava/lang/String;)Lcyw;

    move-result-object v0

    check-cast v0, Lczi;

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)Lczi;
    .locals 1

    .prologue
    .line 330
    invoke-super {p0, p1}, Lcyw;->a(Ljava/lang/String;)Lcyw;

    move-result-object v0

    check-cast v0, Lczi;

    return-object v0
.end method
