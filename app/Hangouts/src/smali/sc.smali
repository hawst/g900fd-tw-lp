.class public final Lsc;
.super Lrj;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 28
    invoke-direct {p0}, Lrj;-><init>()V

    .line 31
    const/16 v0, 0x80

    :try_start_0
    invoke-virtual {p0, v0}, Lsc;->a(I)V

    .line 32
    invoke-virtual {p0}, Lsc;->c()V

    .line 35
    const-string v0, "application/vnd.wap.multipart.related"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    iget-object v1, p0, Lsc;->a:Lrs;

    const/16 v2, 0x84

    invoke-virtual {v1, v0, v2}, Lrs;->a([BI)V

    .line 36
    new-instance v0, Lrh;

    const-string v1, "insert-address-token"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lrh;-><init>([B)V

    invoke-virtual {p0, v0}, Lsc;->a(Lrh;)V

    .line 37
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "T"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    iget-object v1, p0, Lsc;->a:Lrs;

    const/16 v2, 0x98

    invoke-virtual {v1, v0, v2}, Lrs;->a([BI)V
    :try_end_0
    .catch Lrb; {:try_start_0 .. :try_end_0} :catch_0

    .line 42
    return-void

    .line 38
    :catch_0
    move-exception v0

    .line 40
    const-string v1, "SendReq"

    const-string v2, "Unexpected InvalidHeaderValueException."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 41
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method constructor <init>(Lrs;Lrm;)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0, p1, p2}, Lrj;-><init>(Lrs;Lrm;)V

    .line 89
    return-void
.end method


# virtual methods
.method public a([B)V
    .locals 2

    .prologue
    .line 246
    iget-object v0, p0, Lsc;->a:Lrs;

    const/16 v1, 0x8a

    invoke-virtual {v0, p1, v1}, Lrs;->a([BI)V

    .line 247
    return-void
.end method

.method public a([Lrh;)V
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lsc;->a:Lrs;

    invoke-virtual {v0, p1}, Lrs;->a([Lrh;)V

    .line 276
    return-void
.end method

.method public b(J)V
    .locals 2

    .prologue
    .line 225
    iget-object v0, p0, Lsc;->a:Lrs;

    const/16 v1, 0x8e

    invoke-virtual {v0, p1, p2, v1}, Lrs;->a(JI)V

    .line 226
    return-void
.end method

.method public h()J
    .locals 2

    .prologue
    .line 216
    iget-object v0, p0, Lsc;->a:Lrs;

    const/16 v1, 0x8e

    invoke-virtual {v0, v1}, Lrs;->e(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public i()V
    .locals 3

    .prologue
    .line 184
    iget-object v0, p0, Lsc;->a:Lrs;

    const/16 v1, 0x81

    const/16 v2, 0x86

    invoke-virtual {v0, v1, v2}, Lrs;->a(II)V

    .line 185
    return-void
.end method

.method public j()V
    .locals 4

    .prologue
    .line 205
    iget-object v0, p0, Lsc;->a:Lrs;

    const-wide/32 v1, 0x93a80

    const/16 v3, 0x88

    invoke-virtual {v0, v1, v2, v3}, Lrs;->a(JI)V

    .line 206
    return-void
.end method

.method public k()V
    .locals 3

    .prologue
    .line 265
    iget-object v0, p0, Lsc;->a:Lrs;

    const/16 v1, 0x81

    const/16 v2, 0x90

    invoke-virtual {v0, v1, v2}, Lrs;->a(II)V

    .line 266
    return-void
.end method
