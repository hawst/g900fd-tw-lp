.class public final Lbv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbl;


# instance fields
.field private a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lbg;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field private c:Landroid/app/PendingIntent;

.field private d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/Notification;",
            ">;"
        }
    .end annotation
.end field

.field private e:Landroid/graphics/Bitmap;

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1868
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1852
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbv;->a:Ljava/util/ArrayList;

    .line 1853
    const/4 v0, 0x1

    iput v0, p0, Lbv;->b:I

    .line 1855
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbv;->d:Ljava/util/ArrayList;

    .line 1858
    const v0, 0x800005

    iput v0, p0, Lbv;->g:I

    .line 1859
    const/4 v0, -0x1

    iput v0, p0, Lbv;->h:I

    .line 1860
    const/4 v0, 0x0

    iput v0, p0, Lbv;->i:I

    .line 1862
    const/16 v0, 0x50

    iput v0, p0, Lbv;->k:I

    .line 1869
    return-void
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 2363
    iget v0, p0, Lbv;->b:I

    or-int/2addr v0, p1

    iput v0, p0, Lbv;->b:I

    return-void
.end method


# virtual methods
.method public a(Lbk;)Lbk;
    .locals 5

    .prologue
    .line 1911
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1913
    iget-object v0, p0, Lbv;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1914
    const-string v2, "actions"

    invoke-static {}, Lbf;->a()Lbn;

    move-result-object v3

    iget-object v0, p0, Lbv;->a:Ljava/util/ArrayList;

    iget-object v4, p0, Lbv;->a:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Lbg;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbg;

    invoke-virtual {v3, v0}, Lbn;->a([Lbg;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1918
    :cond_0
    iget v0, p0, Lbv;->b:I

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    .line 1919
    const-string v0, "flags"

    iget v2, p0, Lbv;->b:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1921
    :cond_1
    iget-object v0, p0, Lbv;->c:Landroid/app/PendingIntent;

    if-eqz v0, :cond_2

    .line 1922
    const-string v0, "displayIntent"

    iget-object v2, p0, Lbv;->c:Landroid/app/PendingIntent;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1924
    :cond_2
    iget-object v0, p0, Lbv;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1925
    const-string v2, "pages"

    iget-object v0, p0, Lbv;->d:Ljava/util/ArrayList;

    iget-object v3, p0, Lbv;->d:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Landroid/app/Notification;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 1928
    :cond_3
    iget-object v0, p0, Lbv;->e:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_4

    .line 1929
    const-string v0, "background"

    iget-object v2, p0, Lbv;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1931
    :cond_4
    iget v0, p0, Lbv;->f:I

    if-eqz v0, :cond_5

    .line 1932
    const-string v0, "contentIcon"

    iget v2, p0, Lbv;->f:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1934
    :cond_5
    iget v0, p0, Lbv;->g:I

    const v2, 0x800005

    if-eq v0, v2, :cond_6

    .line 1935
    const-string v0, "contentIconGravity"

    iget v2, p0, Lbv;->g:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1937
    :cond_6
    iget v0, p0, Lbv;->h:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_7

    .line 1938
    const-string v0, "contentActionIndex"

    iget v2, p0, Lbv;->h:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1941
    :cond_7
    iget v0, p0, Lbv;->i:I

    if-eqz v0, :cond_8

    .line 1942
    const-string v0, "customSizePreset"

    iget v2, p0, Lbv;->i:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1944
    :cond_8
    iget v0, p0, Lbv;->j:I

    if-eqz v0, :cond_9

    .line 1945
    const-string v0, "customContentHeight"

    iget v2, p0, Lbv;->j:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1947
    :cond_9
    iget v0, p0, Lbv;->k:I

    const/16 v2, 0x50

    if-eq v0, v2, :cond_a

    .line 1948
    const-string v0, "gravity"

    iget v2, p0, Lbv;->k:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1951
    :cond_a
    invoke-virtual {p1}, Lbk;->d()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "android.wearable.EXTENSIONS"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1952
    return-object p1
.end method

.method public a()Lbv;
    .locals 1

    .prologue
    .line 2289
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lbv;->a(I)V

    .line 2290
    return-object p0
.end method

.method public a(Landroid/app/Notification;)Lbv;
    .locals 1

    .prologue
    .line 2075
    iget-object v0, p0, Lbv;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2076
    return-object p0
.end method

.method public a(Landroid/graphics/Bitmap;)Lbv;
    .locals 0

    .prologue
    .line 2125
    iput-object p1, p0, Lbv;->e:Landroid/graphics/Bitmap;

    .line 2126
    return-object p0
.end method

.method public a(Lbg;)Lbv;
    .locals 1

    .prologue
    .line 1985
    iget-object v0, p0, Lbv;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1986
    return-object p0
.end method

.method public b()Lbv;
    .locals 1

    .prologue
    .line 2349
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lbv;->a(I)V

    .line 2350
    return-object p0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1769
    new-instance v0, Lbv;

    invoke-direct {v0}, Lbv;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lbv;->a:Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, Lbv;->a:Ljava/util/ArrayList;

    iget v1, p0, Lbv;->b:I

    iput v1, v0, Lbv;->b:I

    iget-object v1, p0, Lbv;->c:Landroid/app/PendingIntent;

    iput-object v1, v0, Lbv;->c:Landroid/app/PendingIntent;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lbv;->d:Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, Lbv;->d:Ljava/util/ArrayList;

    iget-object v1, p0, Lbv;->e:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lbv;->e:Landroid/graphics/Bitmap;

    iget v1, p0, Lbv;->f:I

    iput v1, v0, Lbv;->f:I

    iget v1, p0, Lbv;->g:I

    iput v1, v0, Lbv;->g:I

    iget v1, p0, Lbv;->h:I

    iput v1, v0, Lbv;->h:I

    iget v1, p0, Lbv;->i:I

    iput v1, v0, Lbv;->i:I

    iget v1, p0, Lbv;->j:I

    iput v1, v0, Lbv;->j:I

    iget v1, p0, Lbv;->k:I

    iput v1, v0, Lbv;->k:I

    return-object v0
.end method
