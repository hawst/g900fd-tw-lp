.class public final Ldsc;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldsc;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4447
    const/4 v0, 0x0

    new-array v0, v0, [Ldsc;

    sput-object v0, Ldsc;->a:[Ldsc;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4448
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 4489
    const/4 v0, 0x0

    .line 4490
    iget-object v1, p0, Ldsc;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 4491
    const/4 v0, 0x1

    iget-object v1, p0, Ldsc;->b:Ljava/lang/String;

    .line 4492
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4494
    :cond_0
    iget-object v1, p0, Ldsc;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 4495
    const/4 v1, 0x2

    iget-object v2, p0, Ldsc;->c:Ljava/lang/Integer;

    .line 4496
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4498
    :cond_1
    iget-object v1, p0, Ldsc;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 4499
    const/4 v1, 0x3

    iget-object v2, p0, Ldsc;->d:Ljava/lang/Integer;

    .line 4500
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4502
    :cond_2
    iget-object v1, p0, Ldsc;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 4503
    const/4 v1, 0x4

    iget-object v2, p0, Ldsc;->e:Ljava/lang/String;

    .line 4504
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4506
    :cond_3
    iget-object v1, p0, Ldsc;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 4507
    const/4 v1, 0x5

    iget-object v2, p0, Ldsc;->f:Ljava/lang/String;

    .line 4508
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4510
    :cond_4
    iget-object v1, p0, Ldsc;->g:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 4511
    const/4 v1, 0x6

    iget-object v2, p0, Ldsc;->g:Ljava/lang/String;

    .line 4512
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4514
    :cond_5
    iget-object v1, p0, Ldsc;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4515
    iput v0, p0, Ldsc;->cachedSize:I

    .line 4516
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 4444
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldsc;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldsc;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldsc;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldsc;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldsc;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldsc;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldsc;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldsc;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldsc;->g:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 4465
    iget-object v0, p0, Ldsc;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 4466
    const/4 v0, 0x1

    iget-object v1, p0, Ldsc;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 4468
    :cond_0
    iget-object v0, p0, Ldsc;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 4469
    const/4 v0, 0x2

    iget-object v1, p0, Ldsc;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 4471
    :cond_1
    iget-object v0, p0, Ldsc;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 4472
    const/4 v0, 0x3

    iget-object v1, p0, Ldsc;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 4474
    :cond_2
    iget-object v0, p0, Ldsc;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 4475
    const/4 v0, 0x4

    iget-object v1, p0, Ldsc;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 4477
    :cond_3
    iget-object v0, p0, Ldsc;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 4478
    const/4 v0, 0x5

    iget-object v1, p0, Ldsc;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 4480
    :cond_4
    iget-object v0, p0, Ldsc;->g:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 4481
    const/4 v0, 0x6

    iget-object v1, p0, Ldsc;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 4483
    :cond_5
    iget-object v0, p0, Ldsc;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 4485
    return-void
.end method
