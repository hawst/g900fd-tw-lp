.class final Lbni;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Laac;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 1108
    invoke-direct {p0}, Lbni;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lbzn;Lbyd;ZLzx;Z)V
    .locals 7

    .prologue
    .line 1112
    invoke-static {p2}, Lcwz;->a(Ljava/lang/Object;)V

    .line 1114
    if-eqz p3, :cond_2

    invoke-virtual {p1}, Lbzn;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v1, v0

    .line 1116
    :goto_0
    sget-boolean v0, Lbne;->a:Z

    if-eqz v0, :cond_0

    .line 1117
    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setImageBitmap: success "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " bitmap: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1120
    :cond_0
    invoke-virtual {p4}, Lzx;->k()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lbmr;

    .line 1121
    iget-object v2, v5, Lbmr;->q:Lbk;

    .line 1122
    iget-object v0, v5, Lbmr;->p:Lbu;

    check-cast v0, Lbi;

    .line 1123
    invoke-virtual {v0, v1}, Lbi;->a(Landroid/graphics/Bitmap;)Lbi;

    .line 1124
    iget-object v0, v5, Lbmr;->p:Lbu;

    invoke-virtual {v2, v0}, Lbk;->a(Lbu;)Lbk;

    .line 1127
    new-instance v0, Lbv;

    invoke-direct {v0}, Lbv;-><init>()V

    .line 1128
    invoke-static {v2, v5}, Lbne;->a(Lbk;Lbmr;)V

    .line 1131
    new-instance v3, Lbk;

    .line 1132
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lbk;-><init>(Landroid/content/Context;)V

    .line 1133
    new-instance v4, Lbv;

    invoke-direct {v4}, Lbv;-><init>()V

    .line 1134
    invoke-virtual {v4}, Lbv;->b()Lbv;

    .line 1135
    instance-of v6, v5, Lbmg;

    if-eqz v6, :cond_1

    .line 1136
    invoke-virtual {v4, v1}, Lbv;->a(Landroid/graphics/Bitmap;)Lbv;

    .line 1138
    :cond_1
    invoke-virtual {v3, v4}, Lbk;->a(Lbl;)Lbk;

    .line 1139
    invoke-virtual {v3}, Lbk;->e()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbv;->a(Landroid/app/Notification;)Lbv;

    .line 1141
    invoke-static {v0, v5}, Lbne;->a(Lbv;Lbmr;)V

    .line 1142
    invoke-static {v0, v5}, Lbne;->b(Lbv;Lbmr;)V

    .line 1143
    invoke-virtual {v2, v0}, Lbk;->a(Lbl;)Lbk;

    .line 1146
    invoke-virtual {v2}, Lbk;->e()Landroid/app/Notification;

    move-result-object v0

    iget-object v1, v5, Lbmr;->m:Lyj;

    iget v2, v5, Lbmr;->s:I

    iget-object v3, v5, Lbmr;->n:Lbxz;

    .line 1147
    invoke-static {v5}, Lbne;->a(Lbmr;)Z

    move-result v4

    instance-of v5, v5, Lbmg;

    .line 1146
    invoke-static/range {v0 .. v5}, Lbne;->a(Landroid/app/Notification;Lyj;ILbxz;ZZ)V

    .line 1149
    return-void

    .line 1115
    :cond_2
    invoke-static {}, Lyn;->c()Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_0
.end method
