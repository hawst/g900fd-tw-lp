.class public Lpb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lmb;
.implements Lmq;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lma;

.field private c:Landroid/view/View;

.field private d:Lmn;

.field private e:Lpd;

.field private f:Lpc;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p1, p0, Lpb;->a:Landroid/content/Context;

    .line 67
    new-instance v0, Lma;

    invoke-direct {v0, p1}, Lma;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lpb;->b:Lma;

    .line 68
    iget-object v0, p0, Lpb;->b:Lma;

    invoke-virtual {v0, p0}, Lma;->a(Lmb;)V

    .line 69
    iput-object p2, p0, Lpb;->c:Landroid/view/View;

    .line 70
    new-instance v0, Lmn;

    iget-object v1, p0, Lpb;->b:Lma;

    invoke-direct {v0, p1, v1, p2}, Lmn;-><init>(Landroid/content/Context;Lma;Landroid/view/View;)V

    iput-object v0, p0, Lpb;->d:Lmn;

    .line 71
    iget-object v0, p0, Lpb;->d:Lmn;

    invoke-virtual {v0, p0}, Lmn;->a(Lmq;)V

    .line 72
    return-void
.end method


# virtual methods
.method public a()Landroid/view/Menu;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lpb;->b:Lma;

    return-object v0
.end method

.method public a(Lma;)V
    .locals 0

    .prologue
    .line 182
    return-void
.end method

.method public a(Lma;Z)V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lpb;->f:Lpc;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lpb;->f:Lpc;

    invoke-interface {v0}, Lpc;->k_()V

    .line 155
    :cond_0
    return-void
.end method

.method public a(Lpc;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lpb;->f:Lpc;

    .line 136
    return-void
.end method

.method public a(Lpd;)V
    .locals 0

    .prologue
    .line 126
    iput-object p1, p0, Lpb;->e:Lpd;

    .line 127
    return-void
.end method

.method public a_(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lpb;->e:Lpd;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lpb;->e:Lpd;

    invoke-interface {v0, p1}, Lpd;->a(Landroid/view/MenuItem;)Z

    move-result v0

    .line 145
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Landroid/view/MenuInflater;
    .locals 2

    .prologue
    .line 92
    new-instance v0, Lli;

    iget-object v1, p0, Lpb;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lli;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public b(Lma;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 161
    if-nez p1, :cond_1

    const/4 v0, 0x0

    .line 169
    :cond_0
    :goto_0
    return v0

    .line 163
    :cond_1
    invoke-virtual {p1}, Lma;->hasVisibleItems()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 168
    new-instance v1, Lmn;

    iget-object v2, p0, Lpb;->a:Landroid/content/Context;

    iget-object v3, p0, Lpb;->c:Landroid/view/View;

    invoke-direct {v1, v2, p1, v3}, Lmn;-><init>(Landroid/content/Context;Lma;Landroid/view/View;)V

    invoke-virtual {v1}, Lmn;->a()V

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lpb;->d:Lmn;

    invoke-virtual {v0}, Lmn;->a()V

    .line 110
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lpb;->d:Lmn;

    invoke-virtual {v0}, Lmn;->c()V

    .line 118
    return-void
.end method
