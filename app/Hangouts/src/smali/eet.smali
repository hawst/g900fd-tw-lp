.class final Leet;
.super Ljava/util/AbstractMap;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/concurrent/ConcurrentMap;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractMap",
        "<TK;TV;>;",
        "Ljava/io/Serializable;",
        "Ljava/util/concurrent/ConcurrentMap",
        "<TK;TV;>;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private final a:Lft;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lft",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private final b:Leeu;


# direct methods
.method constructor <init>(Lees;)V
    .locals 1

    .prologue
    .line 764
    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    .line 765
    invoke-virtual {p1}, Lees;->a()Lft;

    move-result-object v0

    iput-object v0, p0, Leet;->a:Lft;

    .line 766
    iget-object v0, p1, Lees;->j:Leeu;

    iput-object v0, p0, Leet;->b:Leeu;

    .line 767
    return-void
.end method


# virtual methods
.method public containsKey(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 773
    const/4 v0, 0x0

    return v0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 778
    const/4 v0, 0x0

    return v0
.end method

.method public entrySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 827
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 783
    const/4 v0, 0x0

    return-object v0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 794
    invoke-static {p1}, Lm;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 795
    invoke-static {p2}, Lm;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 796
    new-instance v0, Lefa;

    iget-object v1, p0, Leet;->b:Leeu;

    invoke-direct {v0, p1, p2, v1}, Lefa;-><init>(Ljava/lang/Object;Ljava/lang/Object;Leeu;)V

    iget-object v0, p0, Leet;->a:Lft;

    .line 797
    const/4 v0, 0x0

    return-object v0
.end method

.method public putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 801
    invoke-virtual {p0, p1, p2}, Leet;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 806
    const/4 v0, 0x0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 810
    const/4 v0, 0x0

    return v0
.end method

.method public replace(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 814
    invoke-static {p1}, Lm;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 815
    invoke-static {p2}, Lm;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 816
    const/4 v0, 0x0

    return-object v0
.end method

.method public replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;TV;)Z"
        }
    .end annotation

    .prologue
    .line 820
    invoke-static {p1}, Lm;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 821
    invoke-static {p3}, Lm;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 822
    const/4 v0, 0x0

    return v0
.end method
