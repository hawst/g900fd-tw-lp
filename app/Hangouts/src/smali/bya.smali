.class public final Lbya;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/Object;

.field private static volatile b:Z

.field private static c:Ljava/io/BufferedOutputStream;

.field private static d:Landroid/os/PowerManager;

.field private static volatile e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 223
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lbya;->a:Ljava/lang/Object;

    .line 224
    const/4 v0, 0x0

    sput-boolean v0, Lbya;->b:Z

    .line 296
    invoke-static {}, Lbya;->g()V

    .line 297
    new-instance v0, Lbyb;

    invoke-direct {v0}, Lbyb;-><init>()V

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/Runnable;)V

    .line 303
    return-void
.end method

.method public static a()V
    .locals 1

    .prologue
    .line 235
    const/4 v0, 0x0

    sput-object v0, Lbya;->e:Ljava/lang/String;

    .line 236
    return-void
.end method

.method public static a(Lbyc;)V
    .locals 4

    .prologue
    .line 317
    sget-object v1, Lbya;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 318
    :try_start_0
    sget-boolean v0, Lbya;->b:Z

    if-nez v0, :cond_0

    .line 319
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 327
    :goto_0
    return-void

    .line 322
    :cond_0
    :try_start_1
    sget-object v0, Lbya;->c:Ljava/io/BufferedOutputStream;

    invoke-static {p0}, Lbyc;->a(Lbyc;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 323
    sget-object v0, Lbya;->c:Ljava/io/BufferedOutputStream;

    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->flush()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 327
    :goto_1
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 324
    :catch_0
    move-exception v0

    .line 325
    :try_start_3
    const-string v2, "Babel"

    const-string v3, "error writing to datalog output stream"

    invoke-static {v2, v3, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 231
    sput-object p0, Lbya;->e:Ljava/lang/String;

    .line 232
    return-void
.end method

.method public static b()Z
    .locals 1

    .prologue
    .line 309
    sget-boolean v0, Lbya;->b:Z

    return v0
.end method

.method public static c()V
    .locals 4

    .prologue
    .line 335
    sget-object v1, Lbya;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 336
    :try_start_0
    sget-boolean v0, Lbya;->b:Z

    if-nez v0, :cond_0

    .line 337
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 344
    :goto_0
    return-void

    .line 340
    :cond_0
    :try_start_1
    sget-object v0, Lbya;->c:Ljava/io/BufferedOutputStream;

    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->flush()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 344
    :goto_1
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 341
    :catch_0
    move-exception v0

    .line 342
    :try_start_3
    const-string v2, "Babel"

    const-string v3, "error flushing datalog output stream"

    invoke-static {v2, v3, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method static synthetic d()Landroid/os/PowerManager;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lbya;->d:Landroid/os/PowerManager;

    return-object v0
.end method

.method static synthetic e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lbya;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f()V
    .locals 0

    .prologue
    .line 34
    invoke-static {}, Lbya;->g()V

    return-void
.end method

.method private static g()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 244
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v4

    .line 245
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "babel_data_logging"

    invoke-static {v0, v1, v3}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v1

    .line 248
    sget-object v5, Lbya;->a:Ljava/lang/Object;

    monitor-enter v5

    .line 249
    :try_start_0
    sget-boolean v0, Lbya;->b:Z

    if-eq v1, v0, :cond_0

    .line 250
    if-eqz v1, :cond_2

    .line 252
    const-string v0, "power"

    invoke-virtual {v4, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    sput-object v0, Lbya;->d:Landroid/os/PowerManager;

    .line 254
    const-string v0, "datalogs.csv"

    invoke-virtual {v4, v0}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 255
    invoke-virtual {v0}, Ljava/io/File;->exists()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    .line 259
    :try_start_1
    const-string v0, "datalogs.csv"

    const v7, 0x8000

    invoke-virtual {v4, v0, v7}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 266
    :goto_0
    if-eqz v0, :cond_1

    .line 267
    :try_start_2
    new-instance v2, Ljava/io/BufferedOutputStream;

    invoke-direct {v2, v0}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    sput-object v2, Lbya;->c:Ljava/io/BufferedOutputStream;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 268
    if-nez v6, :cond_1

    .line 270
    :try_start_3
    sget-object v0, Lbya;->c:Ljava/io/BufferedOutputStream;

    invoke-static {}, Lbyc;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 271
    sget-object v0, Lbya;->c:Ljava/io/BufferedOutputStream;

    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->flush()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v1

    .line 290
    :goto_1
    :try_start_4
    sput-boolean v0, Lbya;->b:Z

    .line 292
    :cond_0
    monitor-exit v5

    return-void

    .line 261
    :catch_0
    move-exception v0

    .line 262
    const-string v1, "Babel"

    const-string v4, "error opening datalog output stream"

    invoke-static {v1, v4, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v2

    move v1, v3

    .line 264
    goto :goto_0

    .line 272
    :catch_1
    move-exception v0

    .line 273
    const-string v2, "Babel"

    const-string v3, "error writing header to datalog output stream"

    invoke-static {v2, v3, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    move v0, v1

    .line 278
    goto :goto_1

    .line 280
    :cond_2
    const/4 v0, 0x0

    sput-object v0, Lbya;->d:Landroid/os/PowerManager;

    .line 281
    sget-object v0, Lbya;->c:Ljava/io/BufferedOutputStream;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v0, :cond_3

    .line 283
    :try_start_5
    sget-object v0, Lbya;->c:Ljava/io/BufferedOutputStream;

    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 287
    :goto_2
    const/4 v0, 0x0

    :try_start_6
    sput-object v0, Lbya;->c:Ljava/io/BufferedOutputStream;

    :cond_3
    move v0, v1

    goto :goto_1

    .line 284
    :catch_2
    move-exception v0

    .line 285
    const-string v2, "Babel"

    const-string v3, "error closing datalog output stream"

    invoke-static {v2, v3, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_2

    .line 292
    :catchall_0
    move-exception v0

    monitor-exit v5

    throw v0
.end method
