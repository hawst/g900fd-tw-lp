.class public final Lfac;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lfac;

.field public static final b:Lepo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lepo",
            "<",
            "Lfac;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Long;

.field public e:Ljava/lang/Long;

.field public f:Ljava/lang/Long;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:[Lfac;

.field public q:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lfac;

    sput-object v0, Lfac;->a:[Lfac;

    .line 13
    const v0, 0x1f44423

    new-instance v1, Lfad;

    invoke-direct {v1}, Lfad;-><init>()V

    .line 14
    invoke-static {v0, v1}, Lepo;->a(ILepp;)Lepo;

    move-result-object v0

    sput-object v0, Lfac;->b:Lepo;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 23
    iput-object v1, p0, Lfac;->c:Ljava/lang/Integer;

    .line 50
    sget-object v0, Lfac;->a:[Lfac;

    iput-object v0, p0, Lfac;->p:[Lfac;

    .line 53
    iput-object v1, p0, Lfac;->q:Ljava/lang/Integer;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 114
    iget-object v0, p0, Lfac;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_f

    .line 115
    const/4 v0, 0x1

    iget-object v2, p0, Lfac;->c:Ljava/lang/Integer;

    .line 116
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 118
    :goto_0
    iget-object v2, p0, Lfac;->d:Ljava/lang/Long;

    if-eqz v2, :cond_0

    .line 119
    const/4 v2, 0x2

    iget-object v3, p0, Lfac;->d:Ljava/lang/Long;

    .line 120
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 122
    :cond_0
    iget-object v2, p0, Lfac;->f:Ljava/lang/Long;

    if-eqz v2, :cond_1

    .line 123
    const/4 v2, 0x3

    iget-object v3, p0, Lfac;->f:Ljava/lang/Long;

    .line 124
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 126
    :cond_1
    iget-object v2, p0, Lfac;->g:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 127
    const/4 v2, 0x4

    iget-object v3, p0, Lfac;->g:Ljava/lang/String;

    .line 128
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 130
    :cond_2
    iget-object v2, p0, Lfac;->h:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 131
    const/4 v2, 0x5

    iget-object v3, p0, Lfac;->h:Ljava/lang/String;

    .line 132
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 134
    :cond_3
    iget-object v2, p0, Lfac;->i:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 135
    const/4 v2, 0x6

    iget-object v3, p0, Lfac;->i:Ljava/lang/String;

    .line 136
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 138
    :cond_4
    iget-object v2, p0, Lfac;->p:[Lfac;

    if-eqz v2, :cond_6

    .line 139
    iget-object v2, p0, Lfac;->p:[Lfac;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 140
    if-eqz v4, :cond_5

    .line 141
    const/4 v5, 0x7

    .line 142
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 139
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 146
    :cond_6
    iget-object v1, p0, Lfac;->o:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 147
    const/16 v1, 0x8

    iget-object v2, p0, Lfac;->o:Ljava/lang/String;

    .line 148
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 150
    :cond_7
    iget-object v1, p0, Lfac;->m:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 151
    const/16 v1, 0x9

    iget-object v2, p0, Lfac;->m:Ljava/lang/String;

    .line 152
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 154
    :cond_8
    iget-object v1, p0, Lfac;->j:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 155
    const/16 v1, 0xa

    iget-object v2, p0, Lfac;->j:Ljava/lang/String;

    .line 156
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 158
    :cond_9
    iget-object v1, p0, Lfac;->k:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 159
    const/16 v1, 0xb

    iget-object v2, p0, Lfac;->k:Ljava/lang/String;

    .line 160
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 162
    :cond_a
    iget-object v1, p0, Lfac;->l:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 163
    const/16 v1, 0xc

    iget-object v2, p0, Lfac;->l:Ljava/lang/String;

    .line 164
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 166
    :cond_b
    iget-object v1, p0, Lfac;->n:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 167
    const/16 v1, 0xd

    iget-object v2, p0, Lfac;->n:Ljava/lang/String;

    .line 168
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 170
    :cond_c
    iget-object v1, p0, Lfac;->q:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    .line 171
    const/16 v1, 0xe

    iget-object v2, p0, Lfac;->q:Ljava/lang/Integer;

    .line 172
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 174
    :cond_d
    iget-object v1, p0, Lfac;->e:Ljava/lang/Long;

    if-eqz v1, :cond_e

    .line 175
    const/16 v1, 0xf

    iget-object v2, p0, Lfac;->e:Ljava/lang/Long;

    .line 176
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 178
    :cond_e
    iget-object v1, p0, Lfac;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 179
    iput v0, p0, Lfac;->cachedSize:I

    .line 180
    return v0

    :cond_f
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lfac;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lfac;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lfac;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    if-eq v0, v4, :cond_2

    if-ne v0, v5, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lfac;->c:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lfac;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lfac;->d:Ljava/lang/Long;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lfac;->f:Ljava/lang/Long;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfac;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfac;->h:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfac;->i:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lfac;->p:[Lfac;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lfac;

    iget-object v3, p0, Lfac;->p:[Lfac;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lfac;->p:[Lfac;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iput-object v2, p0, Lfac;->p:[Lfac;

    :goto_2
    iget-object v2, p0, Lfac;->p:[Lfac;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Lfac;->p:[Lfac;

    new-instance v3, Lfac;

    invoke-direct {v3}, Lfac;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lfac;->p:[Lfac;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lfac;->p:[Lfac;

    array-length v0, v0

    goto :goto_1

    :cond_6
    iget-object v2, p0, Lfac;->p:[Lfac;

    new-instance v3, Lfac;

    invoke-direct {v3}, Lfac;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lfac;->p:[Lfac;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfac;->o:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfac;->m:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfac;->j:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfac;->k:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfac;->l:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfac;->n:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_7

    if-eq v0, v4, :cond_7

    if-eq v0, v5, :cond_7

    const/4 v2, 0x3

    if-ne v0, v2, :cond_8

    :cond_7
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lfac;->q:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_8
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lfac;->q:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lfac;->e:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 58
    iget-object v0, p0, Lfac;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 59
    const/4 v0, 0x1

    iget-object v1, p0, Lfac;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 61
    :cond_0
    iget-object v0, p0, Lfac;->d:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 62
    const/4 v0, 0x2

    iget-object v1, p0, Lfac;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 64
    :cond_1
    iget-object v0, p0, Lfac;->f:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 65
    const/4 v0, 0x3

    iget-object v1, p0, Lfac;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 67
    :cond_2
    iget-object v0, p0, Lfac;->g:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 68
    const/4 v0, 0x4

    iget-object v1, p0, Lfac;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 70
    :cond_3
    iget-object v0, p0, Lfac;->h:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 71
    const/4 v0, 0x5

    iget-object v1, p0, Lfac;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 73
    :cond_4
    iget-object v0, p0, Lfac;->i:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 74
    const/4 v0, 0x6

    iget-object v1, p0, Lfac;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 76
    :cond_5
    iget-object v0, p0, Lfac;->p:[Lfac;

    if-eqz v0, :cond_7

    .line 77
    iget-object v1, p0, Lfac;->p:[Lfac;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_7

    aget-object v3, v1, v0

    .line 78
    if-eqz v3, :cond_6

    .line 79
    const/4 v4, 0x7

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 77
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 83
    :cond_7
    iget-object v0, p0, Lfac;->o:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 84
    const/16 v0, 0x8

    iget-object v1, p0, Lfac;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 86
    :cond_8
    iget-object v0, p0, Lfac;->m:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 87
    const/16 v0, 0x9

    iget-object v1, p0, Lfac;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 89
    :cond_9
    iget-object v0, p0, Lfac;->j:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 90
    const/16 v0, 0xa

    iget-object v1, p0, Lfac;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 92
    :cond_a
    iget-object v0, p0, Lfac;->k:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 93
    const/16 v0, 0xb

    iget-object v1, p0, Lfac;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 95
    :cond_b
    iget-object v0, p0, Lfac;->l:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 96
    const/16 v0, 0xc

    iget-object v1, p0, Lfac;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 98
    :cond_c
    iget-object v0, p0, Lfac;->n:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 99
    const/16 v0, 0xd

    iget-object v1, p0, Lfac;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 101
    :cond_d
    iget-object v0, p0, Lfac;->q:Ljava/lang/Integer;

    if-eqz v0, :cond_e

    .line 102
    const/16 v0, 0xe

    iget-object v1, p0, Lfac;->q:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 104
    :cond_e
    iget-object v0, p0, Lfac;->e:Ljava/lang/Long;

    if-eqz v0, :cond_f

    .line 105
    const/16 v0, 0xf

    iget-object v1, p0, Lfac;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 107
    :cond_f
    iget-object v0, p0, Lfac;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 109
    return-void
.end method
