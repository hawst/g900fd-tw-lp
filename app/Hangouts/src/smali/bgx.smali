.class public Lbgx;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lbjw;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lesz;)V
    .locals 12

    .prologue
    const/4 v7, 0x0

    .line 4818
    invoke-direct {p0}, Lbfz;-><init>()V

    .line 4819
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbgx;->g:Ljava/util/ArrayList;

    .line 4820
    iget-object v9, p1, Lesz;->b:[Lesx;

    array-length v10, v9

    move v8, v7

    :goto_0
    if-ge v8, v10, :cond_2

    aget-object v6, v9, v8

    .line 4821
    new-instance v0, Lbjw;

    iget-object v1, v6, Lesx;->b:Ljava/lang/String;

    iget-object v2, v6, Lesx;->c:Ljava/lang/Boolean;

    .line 4823
    invoke-static {v2, v7}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v2

    iget-object v3, v6, Lesx;->d:Ljava/lang/Boolean;

    .line 4824
    invoke-static {v3, v7}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v3

    iget-object v4, v6, Lesx;->e:Ljava/lang/Boolean;

    .line 4825
    invoke-static {v4, v7}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v4

    iget-object v5, v6, Lesx;->f:Lesy;

    if-eqz v5, :cond_0

    iget-object v5, v6, Lesx;->f:Lesy;

    iget-object v5, v5, Lesy;->c:Ljava/lang/Integer;

    .line 4826
    invoke-static {v5, v7}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v5

    :goto_1
    iget-object v11, v6, Lesx;->g:Leta;

    if-eqz v11, :cond_1

    iget-object v6, v6, Lesx;->g:Leta;

    iget-object v6, v6, Leta;->b:Ljava/lang/String;

    :goto_2
    invoke-direct/range {v0 .. v6}, Lbjw;-><init>(Ljava/lang/String;ZZZILjava/lang/String;)V

    .line 4828
    iget-object v1, p0, Lbgx;->g:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4820
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_0

    :cond_0
    move v5, v7

    .line 4826
    goto :goto_1

    :cond_1
    const/4 v6, 0x0

    goto :goto_2

    .line 4830
    :cond_2
    return-void
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 4834
    if-eqz p0, :cond_0

    .line 4835
    new-instance v0, Lesz;

    invoke-direct {v0}, Lesz;-><init>()V

    .line 4836
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Lesz;

    .line 4837
    if-eqz v0, :cond_0

    .line 4838
    new-instance v1, Lbgx;

    invoke-direct {v1, v0}, Lbgx;-><init>(Lesz;)V

    move-object v0, v1

    .line 4842
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public f()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lbjw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4846
    iget-object v0, p0, Lbgx;->g:Ljava/util/ArrayList;

    return-object v0
.end method
