.class public final Ldqd;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldqd;


# instance fields
.field public b:Ldui;

.field public c:Ldqf;

.field public d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6754
    const/4 v0, 0x0

    new-array v0, v0, [Ldqd;

    sput-object v0, Ldqd;->a:[Ldqd;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 6755
    invoke-direct {p0}, Lepn;-><init>()V

    .line 6758
    iput-object v0, p0, Ldqd;->b:Ldui;

    .line 6761
    iput-object v0, p0, Ldqd;->c:Ldqf;

    .line 6755
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 6783
    const/4 v0, 0x0

    .line 6784
    iget-object v1, p0, Ldqd;->b:Ldui;

    if-eqz v1, :cond_0

    .line 6785
    const/4 v0, 0x1

    iget-object v1, p0, Ldqd;->b:Ldui;

    .line 6786
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6788
    :cond_0
    iget-object v1, p0, Ldqd;->c:Ldqf;

    if-eqz v1, :cond_1

    .line 6789
    const/4 v1, 0x2

    iget-object v2, p0, Ldqd;->c:Ldqf;

    .line 6790
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6792
    :cond_1
    iget-object v1, p0, Ldqd;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 6793
    const/4 v1, 0x3

    iget-object v2, p0, Ldqd;->d:Ljava/lang/String;

    .line 6794
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6796
    :cond_2
    iget-object v1, p0, Ldqd;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6797
    iput v0, p0, Ldqd;->cachedSize:I

    .line 6798
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 6751
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldqd;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldqd;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldqd;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldqd;->b:Ldui;

    if-nez v0, :cond_2

    new-instance v0, Ldui;

    invoke-direct {v0}, Ldui;-><init>()V

    iput-object v0, p0, Ldqd;->b:Ldui;

    :cond_2
    iget-object v0, p0, Ldqd;->b:Ldui;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldqd;->c:Ldqf;

    if-nez v0, :cond_3

    new-instance v0, Ldqf;

    invoke-direct {v0}, Ldqf;-><init>()V

    iput-object v0, p0, Ldqd;->c:Ldqf;

    :cond_3
    iget-object v0, p0, Ldqd;->c:Ldqf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldqd;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 6768
    iget-object v0, p0, Ldqd;->b:Ldui;

    if-eqz v0, :cond_0

    .line 6769
    const/4 v0, 0x1

    iget-object v1, p0, Ldqd;->b:Ldui;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 6771
    :cond_0
    iget-object v0, p0, Ldqd;->c:Ldqf;

    if-eqz v0, :cond_1

    .line 6772
    const/4 v0, 0x2

    iget-object v1, p0, Ldqd;->c:Ldqf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 6774
    :cond_1
    iget-object v0, p0, Ldqd;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 6775
    const/4 v0, 0x3

    iget-object v1, p0, Ldqd;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 6777
    :cond_2
    iget-object v0, p0, Ldqd;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 6779
    return-void
.end method
