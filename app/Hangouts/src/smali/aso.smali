.class public abstract Laso;
.super Lasw;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

.field final synthetic b:Lcom/google/android/apps/hangouts/hangout/ToastView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/hangouts/hangout/ToastView;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V
    .locals 1

    .prologue
    .line 144
    iput-object p1, p0, Laso;->b:Lcom/google/android/apps/hangouts/hangout/ToastView;

    .line 145
    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lasw;-><init>(Lcom/google/android/apps/hangouts/hangout/ToastView;I)V

    .line 146
    iput-object p2, p0, Laso;->a:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 147
    return-void
.end method


# virtual methods
.method protected abstract a()I
.end method

.method b()Ljava/lang/String;
    .locals 4

    .prologue
    .line 151
    iget-object v0, p0, Laso;->a:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    .line 152
    iget-object v1, p0, Laso;->b:Lcom/google/android/apps/hangouts/hangout/ToastView;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/hangout/ToastView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0}, Laso;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 153
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
