.class final Lapi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Laph;


# direct methods
.method constructor <init>(Laph;)V
    .locals 0

    .prologue
    .line 369
    iput-object p1, p0, Lapi;->a:Laph;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x4

    const/4 v2, 0x1

    .line 373
    iget-object v5, p0, Lapi;->a:Laph;

    invoke-static {}, Lf;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Babel"

    const-string v1, "Waiting before evaluating hangout support status..."

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v0, 0xbb8

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-static {}, Lape;->h()Lapf;

    move-result-object v6

    iget-boolean v0, v6, Lapf;->a:Z

    if-eqz v0, :cond_12

    iget-boolean v0, v6, Lapf;->b:Z

    if-eqz v0, :cond_12

    iget-boolean v0, v6, Lapf;->c:Z

    if-eqz v0, :cond_12

    move v1, v2

    :goto_1
    if-nez v1, :cond_1

    const-string v4, "Babel"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v0, "Calls not supported due to: "

    invoke-direct {v7, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v0, v6, Lapf;->a:Z

    if-eqz v0, :cond_13

    const-string v0, ""

    :goto_2
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v0, v6, Lapf;->b:Z

    if-eqz v0, :cond_14

    const-string v0, ""

    :goto_3
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v0, v6, Lapf;->c:Z

    if-eqz v0, :cond_15

    const-string v0, ""

    :goto_4
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    if-eqz v1, :cond_18

    const-string v0, "babel_hangout_supported"

    invoke-static {v0, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v1, "Babel"

    const-string v4, "GServices override - disabling hangouts"

    invoke-static {v1, v4}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    :goto_5
    iget-boolean v1, v6, Lapf;->f:Z

    if-eqz v1, :cond_16

    const-string v1, "babel_hangout_hardware_decode"

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_16

    move v1, v2

    :goto_6
    invoke-static {v1}, Lape;->a(Z)Z

    iget-boolean v1, v6, Lapf;->g:Z

    if-eqz v1, :cond_17

    const-string v1, "babel_hangout_hardware_encode"

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_17

    :goto_7
    invoke-static {v2}, Lape;->b(Z)Z

    invoke-static {}, Lape;->i()Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v1

    invoke-static {}, Lape;->j()Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v2

    invoke-static {}, Lape;->i()Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v3

    invoke-static {}, Lape;->i()Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v4

    if-eqz v0, :cond_10

    iget v7, v6, Lapf;->d:I

    const/4 v8, 0x2

    if-lt v7, v8, :cond_c

    iget-boolean v7, v6, Lapf;->e:Z

    if-eqz v7, :cond_c

    invoke-static {}, Lape;->k()Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v1

    iget v2, v6, Lapf;->d:I

    if-lt v2, v9, :cond_3

    invoke-static {}, Lape;->l()Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v1

    :cond_3
    invoke-static {}, Lape;->m()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-static {}, Lape;->n()Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v1

    :cond_4
    invoke-static {}, Lape;->j()Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v2

    invoke-static {}, Lape;->m()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-static {}, Lape;->i()Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v2

    :cond_5
    invoke-static {}, Lape;->o()Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v3

    iget v4, v6, Lapf;->d:I

    if-ge v4, v9, :cond_6

    invoke-static {}, Lape;->m()Z

    move-result v4

    if-eqz v4, :cond_7

    :cond_6
    invoke-static {}, Lape;->k()Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v3

    :cond_7
    iget v4, v6, Lapf;->d:I

    if-lt v4, v9, :cond_8

    invoke-static {}, Lape;->m()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-static {}, Lape;->l()Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v3

    :cond_8
    invoke-static {}, Lape;->p()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-static {}, Lape;->n()Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v3

    :cond_9
    invoke-static {}, Lape;->i()Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v4

    iget v7, v6, Lapf;->d:I

    if-lt v7, v9, :cond_a

    invoke-static {}, Lape;->o()Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v4

    :cond_a
    invoke-static {}, Lape;->m()Z

    move-result v7

    if-eqz v7, :cond_b

    invoke-static {}, Lape;->k()Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v4

    :cond_b
    invoke-static {}, Lape;->p()Z

    move-result v7

    if-eqz v7, :cond_c

    invoke-static {}, Lape;->l()Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v4

    :cond_c
    const-string v7, "babel_hangout_max_in_primary_video"

    invoke-static {v7, v10}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_d

    invoke-static {v7}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->getFromString(Ljava/lang/String;)Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v1

    :cond_d
    const-string v7, "babel_hangout_max_in_secondary_video"

    invoke-static {v7, v10}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_e

    invoke-static {v7}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->getFromString(Ljava/lang/String;)Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v2

    :cond_e
    const-string v7, "babel_hangout_max_out_nofx_video"

    invoke-static {v7, v10}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_f

    invoke-static {v7}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->getFromString(Ljava/lang/String;)Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v3

    :cond_f
    const-string v7, "babel_hangout_max_out_fx_video"

    invoke-static {v7, v10}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_10

    invoke-static {v7}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->getFromString(Ljava/lang/String;)Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v4

    :cond_10
    invoke-static {v1}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->setMaxIncomingPrimary(Lcom/google/android/libraries/hangouts/video/VideoSpecification;)V

    invoke-static {v2}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->setMaxIncomingSecondary(Lcom/google/android/libraries/hangouts/video/VideoSpecification;)V

    invoke-static {v3}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->setMaxOutgoingNoEffects(Lcom/google/android/libraries/hangouts/video/VideoSpecification;)V

    invoke-static {v4}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->setMaxOutgoingWithEffects(Lcom/google/android/libraries/hangouts/video/VideoSpecification;)V

    const-string v1, "Babel"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_11

    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "is armv7: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, v6, Lapf;->a:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "core count: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, v6, Lapf;->d:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "has NEON: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, v6, Lapf;->e:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "has microphone: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, v6, Lapf;->c:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "has opengl2: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, v6, Lapf;->b:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "hangout support: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_11
    invoke-static {}, Lf;->n()V

    invoke-static {v0}, Lape;->c(Z)Z

    iget-object v0, v5, Laph;->a:Landroid/os/Handler;

    new-instance v1, Lapj;

    invoke-direct {v1, v5}, Lapj;-><init>(Laph;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 374
    return-void

    :cond_12
    move v1, v3

    .line 373
    goto/16 :goto_1

    :cond_13
    const-string v0, "no_armv7 "

    goto/16 :goto_2

    :cond_14
    const-string v0, "no_glv2 "

    goto/16 :goto_3

    :cond_15
    const-string v0, "no_mic"

    goto/16 :goto_4

    :cond_16
    move v1, v3

    goto/16 :goto_6

    :cond_17
    move v2, v3

    goto/16 :goto_7

    :catch_0
    move-exception v0

    goto/16 :goto_0

    :cond_18
    move v0, v1

    goto/16 :goto_5
.end method
