.class public Lbfr;
.super Lbep;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 4375
    invoke-direct {p0}, Lbep;-><init>()V

    .line 4376
    iput-object p1, p0, Lbfr;->a:Ljava/lang/String;

    .line 4377
    iput-object p2, p0, Lbfr;->b:Ljava/lang/String;

    .line 4378
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 4

    .prologue
    .line 4382
    sget-boolean v0, Lbea;->d:Z

    if-eqz v0, :cond_0

    .line 4383
    const-string v0, "Babel_RequestWriter"

    const-string v1, "StartPhoneVerificationRequest build protobuf "

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4385
    :cond_0
    new-instance v0, Leir;

    invoke-direct {v0}, Leir;-><init>()V

    .line 4386
    iget-object v1, p0, Lbfr;->a:Ljava/lang/String;

    iput-object v1, v0, Leir;->b:Ljava/lang/String;

    .line 4388
    new-instance v1, Ldwt;

    invoke-direct {v1}, Ldwt;-><init>()V

    .line 4390
    iget-object v2, p0, Lbfr;->b:Ljava/lang/String;

    iput-object v2, v1, Ldwt;->b:Ljava/lang/String;

    .line 4392
    new-instance v2, Ldws;

    invoke-direct {v2}, Ldws;-><init>()V

    .line 4394
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Ldws;->d:Ljava/lang/Integer;

    .line 4396
    iput-object v0, v2, Ldws;->c:Leir;

    .line 4397
    iput-object v1, v2, Ldws;->e:Ldwt;

    .line 4398
    return-object v2
.end method

.method public a(Lyj;Lbph;)V
    .locals 3

    .prologue
    .line 4410
    invoke-static {}, Lbmz;->a()Lbmz;

    move-result-object v0

    const/16 v1, 0x69

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lbmz;->a(IZ)V

    .line 4411
    return-void
.end method

.method public a(JI)Z
    .locals 1

    .prologue
    .line 4405
    const/4 v0, 0x0

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4415
    const-string v0, "devices/startphonenumberverification"

    return-object v0
.end method
