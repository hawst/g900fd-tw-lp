.class public final Laqi;
.super Ls;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field private Y:Landroid/widget/CheckBox;

.field private Z:Landroid/widget/CheckBox;

.field private aa:Landroid/widget/TextView;

.field private ab:Landroid/widget/TextView;

.field private ac:Z

.field private ad:Z

.field private ae:Z

.field private af:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

.field private final ag:Lapk;

.field private final ah:Laqj;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    invoke-direct {p0}, Ls;-><init>()V

    .line 46
    iput-boolean v1, p0, Laqi;->ac:Z

    .line 47
    iput-boolean v1, p0, Laqi;->ad:Z

    .line 48
    iput-boolean v1, p0, Laqi;->ae:Z

    .line 50
    invoke-static {}, Lapk;->b()Lapk;

    move-result-object v0

    iput-object v0, p0, Laqi;->ag:Lapk;

    .line 51
    new-instance v0, Laqj;

    invoke-direct {v0, p0, v1}, Laqj;-><init>(Laqi;B)V

    iput-object v0, p0, Laqi;->ah:Laqj;

    .line 54
    return-void
.end method

.method public static a(Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;)Laqi;
    .locals 4

    .prologue
    .line 71
    new-instance v0, Laqi;

    invoke-direct {v0}, Laqi;-><init>()V

    .line 74
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 75
    const-string v2, "key_endpoint_muc_jid"

    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;->getEndpointMucJid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    invoke-virtual {v0, v1}, Laqi;->setArguments(Landroid/os/Bundle;)V

    .line 77
    return-object v0
.end method

.method static synthetic a(Laqi;)Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Laqi;->af:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    return-object v0
.end method

.method static synthetic b(Laqi;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Laqi;->q()V

    return-void
.end method

.method private q()V
    .locals 2

    .prologue
    .line 163
    iget-boolean v0, p0, Laqi;->ad:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Laqi;->ac:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Laqi;->ae:Z

    if-nez v0, :cond_0

    .line 164
    const/4 v0, 0x1

    iput-boolean v0, p0, Laqi;->ae:Z

    .line 165
    invoke-static {}, Lapk;->b()Lapk;

    move-result-object v0

    invoke-virtual {v0}, Lapk;->c()Lapx;

    move-result-object v0

    .line 166
    if-eqz v0, :cond_0

    .line 167
    iget-object v1, p0, Laqi;->af:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getJidNickname()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lapx;->i(Ljava/lang/String;)V

    .line 170
    :cond_0
    return-void
.end method


# virtual methods
.method public f()Landroid/app/Dialog;
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/16 v7, 0x8

    .line 82
    iget-object v0, p0, Laqi;->ag:Lapk;

    invoke-virtual {v0}, Lapk;->d()Lcom/google/android/libraries/hangouts/video/CallState;

    move-result-object v0

    .line 83
    invoke-virtual {p0}, Laqi;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "key_endpoint_muc_jid"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 82
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/CallState;->getEndpoint(Ljava/lang/String;)Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    move-result-object v0

    iput-object v0, p0, Laqi;->af:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 84
    invoke-virtual {p0}, Laqi;->getActivity()Ly;

    move-result-object v1

    .line 85
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 86
    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 87
    sget v0, Lf;->fr:I

    const/4 v4, 0x0

    invoke-static {v1, v0, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 88
    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 89
    sget v0, Lh;->hZ:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 90
    sget v0, Lh;->ab:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 91
    sget v0, Lg;->cz:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Laqi;->aa:Landroid/widget/TextView;

    .line 92
    sget v0, Lg;->cC:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Laqi;->ab:Landroid/widget/TextView;

    .line 93
    iget-object v0, p0, Laqi;->aa:Landroid/widget/TextView;

    const-string v5, "abuse_android"

    sget v6, Lh;->dA:I

    invoke-static {v0, v1, v3, v5, v6}, Lf;->a(Landroid/widget/TextView;Landroid/app/Activity;Landroid/content/res/Resources;Ljava/lang/String;I)V

    .line 95
    iget-object v0, p0, Laqi;->ab:Landroid/widget/TextView;

    const-string v5, "blocking"

    sget v6, Lh;->dH:I

    invoke-static {v0, v1, v3, v5, v6}, Lf;->a(Landroid/widget/TextView;Landroid/app/Activity;Landroid/content/res/Resources;Ljava/lang/String;I)V

    .line 97
    sget v0, Lg;->cA:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Laqi;->Z:Landroid/widget/CheckBox;

    .line 98
    sget v0, Lg;->cD:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Laqi;->Y:Landroid/widget/CheckBox;

    .line 99
    iget-object v0, p0, Laqi;->Z:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 100
    iget-object v0, p0, Laqi;->Y:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 101
    iget-object v0, p0, Laqi;->aa:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 102
    iget-object v0, p0, Laqi;->ab:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 103
    sget v0, Lg;->cM:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 104
    sget v1, Lh;->eh:I

    new-array v5, v9, [Ljava/lang/Object;

    iget-object v6, p0, Laqi;->af:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 105
    invoke-virtual {v6}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getDisplayName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    .line 104
    invoke-virtual {v3, v1, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    sget v0, Lg;->cL:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 107
    sget v1, Lh;->eg:I

    new-array v4, v9, [Ljava/lang/Object;

    iget-object v5, p0, Laqi;->af:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 108
    invoke-virtual {v5}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    .line 107
    invoke-virtual {v3, v1, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    invoke-static {}, Lapk;->b()Lapk;

    move-result-object v0

    invoke-virtual {v0}, Lapk;->c()Lapx;

    move-result-object v0

    .line 111
    invoke-virtual {v0}, Lapx;->h()Z

    move-result v0

    iput-boolean v0, p0, Laqi;->ad:Z

    .line 112
    iget-boolean v0, p0, Laqi;->ad:Z

    if-nez v0, :cond_0

    .line 113
    iget-object v0, p0, Laqi;->Z:Landroid/widget/CheckBox;

    invoke-virtual {v0, v7}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 119
    :goto_0
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 116
    :cond_0
    invoke-static {}, Lapk;->b()Lapk;

    move-result-object v0

    invoke-virtual {v0}, Lapk;->c()Lapx;

    move-result-object v0

    iget-object v1, p0, Laqi;->af:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 117
    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getJidNickname()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lapx;->j(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Laqi;->Z:Landroid/widget/CheckBox;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Laqi;->aa:Landroid/widget/TextView;

    move-object v1, v0

    .line 175
    :goto_0
    if-eqz p2, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 176
    return-void

    .line 174
    :cond_0
    iget-object v0, p0, Laqi;->ab:Landroid/widget/TextView;

    move-object v1, v0

    goto :goto_0

    .line 175
    :cond_1
    const/16 v0, 0x8

    goto :goto_1
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    .line 136
    invoke-static {}, Lapk;->b()Lapk;

    move-result-object v0

    invoke-virtual {v0}, Lapk;->c()Lapx;

    move-result-object v2

    .line 137
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 139
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;

    move-result-object v0

    iget-object v1, p0, Laqi;->af:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/VideoChat;->blockMedia(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V

    .line 142
    invoke-virtual {p0}, Laqi;->getActivity()Ly;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    .line 143
    iget-object v1, p0, Laqi;->Y:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 144
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->k()Lyj;

    move-result-object v3

    iget-object v1, p0, Laqi;->af:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 145
    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    iget-object v1, p0, Laqi;->af:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    check-cast v1, Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    .line 146
    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;->getObfuscatedGaiaId()Ljava/lang/String;

    move-result-object v1

    const/4 v5, 0x0

    .line 144
    invoke-static {v0, v3, v4, v1, v5}, Lf;->a(Ly;Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    :cond_0
    iget-object v0, p0, Laqi;->Z:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 150
    iget-object v0, p0, Laqi;->af:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getJidNickname()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lapx;->h(Ljava/lang/String;)V

    .line 151
    const/4 v0, 0x1

    iput-boolean v0, p0, Laqi;->ac:Z

    .line 154
    :cond_1
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 158
    invoke-direct {p0}, Laqi;->q()V

    .line 159
    invoke-super {p0, p1}, Ls;->onDismiss(Landroid/content/DialogInterface;)V

    .line 160
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 124
    invoke-super {p0}, Ls;->onStart()V

    .line 125
    iget-object v0, p0, Laqi;->ag:Lapk;

    iget-object v1, p0, Laqi;->ah:Laqj;

    invoke-virtual {v0, v1}, Lapk;->a(Lapo;)V

    .line 126
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 130
    invoke-super {p0}, Ls;->onStop()V

    .line 131
    iget-object v0, p0, Laqi;->ag:Lapk;

    iget-object v1, p0, Laqi;->ah:Laqj;

    invoke-virtual {v0, v1}, Lapk;->b(Lapo;)V

    .line 132
    return-void
.end method
