.class public final Lcdl;
.super Lcdm;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/views/MultiLineLayout;

.field private c:I

.field private d:I

.field private e:I

.field private f:I


# direct methods
.method private constructor <init>(Lcom/google/android/apps/hangouts/views/MultiLineLayout;)V
    .locals 1

    .prologue
    .line 87
    iput-object p1, p0, Lcdl;->a:Lcom/google/android/apps/hangouts/views/MultiLineLayout;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcdm;-><init>(Lcom/google/android/apps/hangouts/views/MultiLineLayout;B)V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/hangouts/views/MultiLineLayout;B)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcdl;-><init>(Lcom/google/android/apps/hangouts/views/MultiLineLayout;)V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 4

    .prologue
    .line 114
    invoke-super {p0, p1}, Lcdm;->a(I)V

    .line 116
    iget v0, p0, Lcdl;->c:I

    iget-object v1, p0, Lcdl;->a:Lcom/google/android/apps/hangouts/views/MultiLineLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/views/MultiLineLayout;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcdl;->c:I

    .line 117
    iget v0, p0, Lcdl;->d:I

    iget-object v1, p0, Lcdl;->a:Lcom/google/android/apps/hangouts/views/MultiLineLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/views/MultiLineLayout;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcdl;->d:I

    .line 118
    iget-object v0, p0, Lcdl;->a:Lcom/google/android/apps/hangouts/views/MultiLineLayout;

    iget v1, p0, Lcdl;->c:I

    iget v2, p0, Lcdl;->e:I

    invoke-static {v1, v2}, Landroid/view/View;->resolveSize(II)I

    move-result v1

    iget v2, p0, Lcdl;->d:I

    iget v3, p0, Lcdl;->f:I

    .line 119
    invoke-static {v2, v3}, Landroid/view/View;->resolveSize(II)I

    move-result v2

    .line 118
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/hangouts/views/MultiLineLayout;->a(Lcom/google/android/apps/hangouts/views/MultiLineLayout;II)V

    .line 120
    return-void
.end method

.method public a(II)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 94
    iput v0, p0, Lcdl;->c:I

    .line 95
    iput v0, p0, Lcdl;->d:I

    .line 96
    iput p1, p0, Lcdl;->e:I

    .line 97
    iput p2, p0, Lcdl;->f:I

    .line 98
    return-void
.end method

.method protected a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 102
    iget-object v0, p0, Lcdl;->a:Lcom/google/android/apps/hangouts/views/MultiLineLayout;

    iget v1, p0, Lcdl;->e:I

    iget v2, p0, Lcdl;->f:I

    invoke-static {v0, p1, v1, v2}, Lcom/google/android/apps/hangouts/views/MultiLineLayout;->a(Lcom/google/android/apps/hangouts/views/MultiLineLayout;Landroid/view/View;II)V

    .line 103
    return-void
.end method

.method protected a(Landroid/view/View;IIII)V
    .locals 2

    .prologue
    .line 108
    iget v0, p0, Lcdl;->c:I

    add-int v1, p2, p4

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcdl;->c:I

    .line 109
    iget v0, p0, Lcdl;->d:I

    add-int v1, p3, p5

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcdl;->d:I

    .line 110
    return-void
.end method
