.class public final Ldpa;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldpa;


# instance fields
.field public b:Ljava/lang/String;

.field public c:[Ljava/lang/Integer;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/Boolean;

.field public g:Ljava/lang/Boolean;

.field public h:Ljava/lang/Boolean;

.field public i:Ljava/lang/Integer;

.field public j:Ljava/lang/Boolean;

.field public k:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    new-array v0, v0, [Ldpa;

    sput-object v0, Ldpa;->a:[Ldpa;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 108
    invoke-direct {p0}, Lepn;-><init>()V

    .line 113
    sget-object v0, Lept;->m:[Ljava/lang/Integer;

    iput-object v0, p0, Ldpa;->c:[Ljava/lang/Integer;

    .line 108
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 173
    iget-object v0, p0, Ldpa;->b:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 174
    const/4 v0, 0x1

    iget-object v2, p0, Ldpa;->b:Ljava/lang/String;

    .line 175
    invoke-static {v0, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 177
    :goto_0
    iget-object v2, p0, Ldpa;->e:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 178
    const/4 v2, 0x2

    iget-object v3, p0, Ldpa;->e:Ljava/lang/String;

    .line 179
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 181
    :cond_0
    iget-object v2, p0, Ldpa;->f:Ljava/lang/Boolean;

    if-eqz v2, :cond_1

    .line 182
    const/4 v2, 0x3

    iget-object v3, p0, Ldpa;->f:Ljava/lang/Boolean;

    .line 183
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 185
    :cond_1
    iget-object v2, p0, Ldpa;->g:Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    .line 186
    const/4 v2, 0x4

    iget-object v3, p0, Ldpa;->g:Ljava/lang/Boolean;

    .line 187
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 189
    :cond_2
    iget-object v2, p0, Ldpa;->h:Ljava/lang/Boolean;

    if-eqz v2, :cond_3

    .line 190
    const/4 v2, 0x5

    iget-object v3, p0, Ldpa;->h:Ljava/lang/Boolean;

    .line 191
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 193
    :cond_3
    iget-object v2, p0, Ldpa;->i:Ljava/lang/Integer;

    if-eqz v2, :cond_4

    .line 194
    const/4 v2, 0x6

    iget-object v3, p0, Ldpa;->i:Ljava/lang/Integer;

    .line 195
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 197
    :cond_4
    iget-object v2, p0, Ldpa;->j:Ljava/lang/Boolean;

    if-eqz v2, :cond_5

    .line 198
    const/4 v2, 0x7

    iget-object v3, p0, Ldpa;->j:Ljava/lang/Boolean;

    .line 199
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 201
    :cond_5
    iget-object v2, p0, Ldpa;->c:[Ljava/lang/Integer;

    if-eqz v2, :cond_7

    iget-object v2, p0, Ldpa;->c:[Ljava/lang/Integer;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 203
    iget-object v3, p0, Ldpa;->c:[Ljava/lang/Integer;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_6

    aget-object v5, v3, v1

    .line 205
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5}, Lepl;->f(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 203
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 207
    :cond_6
    add-int/2addr v0, v2

    .line 208
    iget-object v1, p0, Ldpa;->c:[Ljava/lang/Integer;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 210
    :cond_7
    iget-object v1, p0, Ldpa;->k:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 211
    const/16 v1, 0x9

    iget-object v2, p0, Ldpa;->k:Ljava/lang/Boolean;

    .line 212
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 214
    :cond_8
    iget-object v1, p0, Ldpa;->d:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 215
    const/16 v1, 0xa

    iget-object v2, p0, Ldpa;->d:Ljava/lang/String;

    .line 216
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 218
    :cond_9
    iget-object v1, p0, Ldpa;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 219
    iput v0, p0, Ldpa;->cachedSize:I

    .line 220
    return v0

    :cond_a
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 104
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldpa;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldpa;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldpa;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldpa;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldpa;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldpa;->f:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldpa;->g:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldpa;->h:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldpa;->i:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldpa;->j:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_8
    const/16 v0, 0x40

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Ldpa;->c:[Ljava/lang/Integer;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Integer;

    iget-object v2, p0, Ldpa;->c:[Ljava/lang/Integer;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Ldpa;->c:[Ljava/lang/Integer;

    :goto_1
    iget-object v1, p0, Ldpa;->c:[Ljava/lang/Integer;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Ldpa;->c:[Ljava/lang/Integer;

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Ldpa;->c:[Ljava/lang/Integer;

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldpa;->k:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldpa;->d:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 134
    iget-object v0, p0, Ldpa;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 135
    const/4 v0, 0x1

    iget-object v1, p0, Ldpa;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 137
    :cond_0
    iget-object v0, p0, Ldpa;->e:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 138
    const/4 v0, 0x2

    iget-object v1, p0, Ldpa;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 140
    :cond_1
    iget-object v0, p0, Ldpa;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 141
    const/4 v0, 0x3

    iget-object v1, p0, Ldpa;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 143
    :cond_2
    iget-object v0, p0, Ldpa;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 144
    const/4 v0, 0x4

    iget-object v1, p0, Ldpa;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 146
    :cond_3
    iget-object v0, p0, Ldpa;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 147
    const/4 v0, 0x5

    iget-object v1, p0, Ldpa;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 149
    :cond_4
    iget-object v0, p0, Ldpa;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 150
    const/4 v0, 0x6

    iget-object v1, p0, Ldpa;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 152
    :cond_5
    iget-object v0, p0, Ldpa;->j:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 153
    const/4 v0, 0x7

    iget-object v1, p0, Ldpa;->j:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 155
    :cond_6
    iget-object v0, p0, Ldpa;->c:[Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 156
    iget-object v1, p0, Ldpa;->c:[Ljava/lang/Integer;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_7

    aget-object v3, v1, v0

    .line 157
    const/16 v4, 0x8

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v4, v3}, Lepl;->a(II)V

    .line 156
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 160
    :cond_7
    iget-object v0, p0, Ldpa;->k:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 161
    const/16 v0, 0x9

    iget-object v1, p0, Ldpa;->k:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 163
    :cond_8
    iget-object v0, p0, Ldpa;->d:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 164
    const/16 v0, 0xa

    iget-object v1, p0, Ldpa;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 166
    :cond_9
    iget-object v0, p0, Ldpa;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 168
    return-void
.end method
