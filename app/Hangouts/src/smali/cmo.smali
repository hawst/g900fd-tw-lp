.class final Lcmo;
.super Ljava/lang/Object;

# interfaces
.implements Lcvw;


# instance fields
.field final synthetic a:Lcml;

.field private final b:I

.field private c:Z

.field private d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcwb;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcwg;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcwb;

.field private final h:Z


# direct methods
.method public constructor <init>(Lcml;I)V
    .locals 1

    iput-object p1, p0, Lcmo;->a:Lcml;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcmo;->b:I

    invoke-virtual {p0}, Lcmo;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcmo;->h:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcwb;)Ljava/lang/Iterable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcwb;",
            ")",
            "Ljava/lang/Iterable",
            "<",
            "Lcwb;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method private a(Landroid/database/Cursor;Lcmp;)Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcmo;->a:Lcml;

    invoke-static {v0}, Lcml;->e(Lcml;)Landroid/database/Cursor;

    move-result-object v0

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p2, v0}, Lcmp;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lcmo;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcmo;->a:Lcml;

    invoke-static {v0}, Lcml;->a(Lcml;)Lclf;

    move-result-object v0

    iget v1, p0, Lcmo;->b:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lclf;->a(II)I

    move-result v0

    iget-object v1, p0, Lcmo;->a:Lcml;

    invoke-static {v1}, Lcml;->d(Lcml;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    iget-object v2, p0, Lcmo;->a:Lcml;

    invoke-static {v2}, Lcml;->d(Lcml;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/gms/common/data/DataHolder;->a(I)I

    move-result v2

    invoke-virtual {v1, p1, v0, v2}, Lcom/google/android/gms/common/data/DataHolder;->b(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()I
    .locals 2

    iget-object v0, p0, Lcmo;->a:Lcml;

    invoke-static {v0}, Lcml;->b(Lcml;)Lclf;

    move-result-object v0

    iget v1, p0, Lcmo;->b:I

    invoke-virtual {v0, v1}, Lclf;->b(I)I

    move-result v0

    return v0
.end method

.method private k()V
    .locals 8

    const/4 v4, 0x0

    const/4 v7, 0x3

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcmo;->c:Z

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcmo;->c:Z

    invoke-direct {p0}, Lcmo;->j()I

    move-result v2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcmo;->d:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcmo;->e:Ljava/util/ArrayList;

    iput-object v4, p0, Lcmo;->f:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcmo;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcmo;->a:Lcml;

    invoke-static {v0}, Lcml;->g(Lcml;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcmo;->a:Lcml;

    invoke-static {v0}, Lcml;->h(Lcml;)Lcnd;

    move-result-object v0

    const-string v3, "v_phones"

    invoke-direct {p0, v3}, Lcmo;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcnd;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcmo;->f:Ljava/util/ArrayList;

    :cond_2
    iget-object v0, p0, Lcmo;->f:Ljava/util/ArrayList;

    if-nez v0, :cond_3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcmo;->f:Ljava/util/ArrayList;

    :cond_3
    iput-object v4, p0, Lcmo;->g:Lcwb;

    invoke-virtual {p0}, Lcmo;->e()Ljava/lang/String;

    move-result-object v3

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_0

    iget-object v4, p0, Lcmo;->a:Lcml;

    invoke-static {v4}, Lcml;->b(Lcml;)Lclf;

    move-result-object v4

    iget v5, p0, Lcmo;->b:I

    invoke-virtual {v4, v5, v0}, Lclf;->a(II)I

    move-result v4

    iget-object v5, p0, Lcmo;->a:Lcml;

    invoke-static {v5}, Lcml;->e(Lcml;)Landroid/database/Cursor;

    move-result-object v5

    invoke-interface {v5, v4}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v4

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcmo;->a:Lcml;

    invoke-static {v4}, Lcml;->e(Lcml;)Landroid/database/Cursor;

    move-result-object v4

    invoke-interface {v4, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iget-object v6, p0, Lcmo;->d:Ljava/util/ArrayList;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    iget-object v4, p0, Lcmo;->a:Lcml;

    invoke-static {v4}, Lcml;->e(Lcml;)Landroid/database/Cursor;

    move-result-object v4

    const/4 v5, 0x2

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "vnd.android.cursor.item/email_v2"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    iget-object v5, p0, Lcmo;->g:Lcwb;

    if-nez v5, :cond_8

    iget-object v4, p0, Lcmo;->a:Lcml;

    invoke-static {v4}, Lcml;->e(Lcml;)Landroid/database/Cursor;

    move-result-object v4

    iget-object v5, p0, Lcmo;->a:Lcml;

    invoke-static {v5}, Lcml;->i(Lcml;)Lcmp;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcmo;->a(Landroid/database/Cursor;Lcmp;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcmo;->a:Lcml;

    invoke-static {v5}, Lcml;->e(Lcml;)Landroid/database/Cursor;

    move-result-object v5

    invoke-interface {v5, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_5

    new-instance v6, Lcms;

    invoke-direct {v6, v4, v5}, Lcms;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcmo;->e:Ljava/util/ArrayList;

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    if-eqz v3, :cond_7

    iget-object v4, p0, Lcmo;->a:Lcml;

    invoke-static {v4}, Lcml;->j(Lcml;)Ljava/util/HashMap;

    move-result-object v4

    invoke-interface {v6}, Lcwb;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcmo;->a:Lcml;

    invoke-static {v4}, Lcml;->j(Lcml;)Ljava/util/HashMap;

    move-result-object v4

    invoke-interface {v6}, Lcwb;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    iput-object v6, p0, Lcmo;->g:Lcwb;

    iget-object v4, p0, Lcmo;->e:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    :cond_5
    :goto_1
    iget-object v4, p0, Lcmo;->a:Lcml;

    invoke-static {v4}, Lcml;->e(Lcml;)Landroid/database/Cursor;

    move-result-object v4

    invoke-static {v4}, Lcmq;->b(Landroid/database/Cursor;)Z

    move-result v4

    if-nez v4, :cond_4

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_7
    iget-object v4, p0, Lcmo;->e:Ljava/util/ArrayList;

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_8
    const-string v5, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcmo;->a:Lcml;

    invoke-static {v4}, Lcml;->e(Lcml;)Landroid/database/Cursor;

    move-result-object v4

    iget-object v5, p0, Lcmo;->a:Lcml;

    invoke-static {v5}, Lcml;->k(Lcml;)Lcmp;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcmo;->a(Landroid/database/Cursor;Lcmp;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcmo;->a:Lcml;

    invoke-static {v5}, Lcml;->e(Lcml;)Landroid/database/Cursor;

    move-result-object v5

    invoke-interface {v5, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_5

    new-instance v6, Lcne;

    invoke-direct {v6, v4, v5}, Lcne;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcmo;->f:Ljava/util/ArrayList;

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    iget-object v4, p0, Lcmo;->f:Ljava/util/ArrayList;

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcmo;->a:Lcml;

    invoke-static {v0}, Lcml;->c(Lcml;)V

    invoke-virtual {p0}, Lcmo;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "name"

    invoke-direct {p0, v0}, Lcmo;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcmo;->a:Lcml;

    invoke-static {v0}, Lcml;->e(Lcml;)Landroid/database/Cursor;

    move-result-object v0

    iget-object v1, p0, Lcmo;->a:Lcml;

    invoke-static {v1}, Lcml;->b(Lcml;)Lclf;

    move-result-object v1

    iget v2, p0, Lcmo;->b:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lclf;->a(II)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    iget-object v0, p0, Lcmo;->a:Lcml;

    invoke-static {v0}, Lcml;->e(Lcml;)Landroid/database/Cursor;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public b()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcmo;->a:Lcml;

    invoke-static {v0}, Lcml;->c(Lcml;)V

    invoke-direct {p0}, Lcmo;->k()V

    iget-object v0, p0, Lcmo;->d:Ljava/util/ArrayList;

    return-object v0
.end method

.method public c()Ljava/lang/Iterable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Lcwb;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcmo;->a:Lcml;

    invoke-static {v0}, Lcml;->c(Lcml;)V

    invoke-virtual {p0}, Lcmo;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lclx;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcms;

    const-string v2, ""

    invoke-direct {v1, v2, v0}, Lcms;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lcmo;->a(Lcwb;)Ljava/lang/Iterable;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcmo;->k()V

    iget-boolean v0, p0, Lcmo;->h:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcmo;->g:Lcwb;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcmo;->g:Lcwb;

    invoke-static {v0}, Lcmo;->a(Lcwb;)Ljava/lang/Iterable;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v0, Lcwb;->a:Ljava/lang/Iterable;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcmo;->a:Lcml;

    invoke-static {v0}, Lcml;->c(Lcml;)V

    invoke-direct {p0}, Lcmo;->j()I

    move-result v0

    if-lez v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_5

    const-string v0, "PeopleService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Row should have a contact: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcmo;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    sget-object v0, Lcwb;->a:Ljava/lang/Iterable;

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcmo;->e:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public d()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Lcwg;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcmo;->a:Lcml;

    invoke-static {v0}, Lcml;->c(Lcml;)V

    invoke-virtual {p0}, Lcmo;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lclx;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcwg;->a:Ljava/lang/Iterable;

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcmo;->k()V

    iget-object v0, p0, Lcmo;->f:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public e()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcmo;->a:Lcml;

    invoke-static {v0}, Lcml;->c(Lcml;)V

    iget-object v0, p0, Lcmo;->a:Lcml;

    invoke-static {v0}, Lcml;->f(Lcml;)Ljava/util/ArrayList;

    move-result-object v0

    iget v1, p0, Lcmo;->b:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public f()Z
    .locals 2

    iget-object v0, p0, Lcmo;->a:Lcml;

    invoke-static {v0}, Lcml;->c(Lcml;)V

    iget-object v0, p0, Lcmo;->a:Lcml;

    invoke-static {v0}, Lcml;->a(Lcml;)Lclf;

    move-result-object v0

    iget v1, p0, Lcmo;->b:I

    invoke-virtual {v0, v1}, Lclf;->b(I)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcmo;->a:Lcml;

    invoke-static {v0}, Lcml;->c(Lcml;)V

    const-string v0, "qualified_id"

    invoke-direct {p0, v0}, Lcmo;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcmo;->a:Lcml;

    invoke-static {v0}, Lcml;->c(Lcml;)V

    sget-object v0, Lcli;->a:Lcli;

    const-string v1, "avatar"

    invoke-direct {p0, v1}, Lcmo;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcli;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public i()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcmo;->a:Lcml;

    invoke-static {v0}, Lcml;->c(Lcml;)V

    const-string v0, "v_circle_ids"

    invoke-direct {p0, v0}, Lcmo;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lclx;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
