.class public final Lczx;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Lczy;

.field private c:Lczy;

.field private d:Z


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 197
    new-instance v0, Lczy;

    invoke-direct {v0, v1}, Lczy;-><init>(B)V

    iput-object v0, p0, Lczx;->b:Lczy;

    .line 198
    iget-object v0, p0, Lczx;->b:Lczy;

    iput-object v0, p0, Lczx;->c:Lczy;

    .line 199
    iput-boolean v1, p0, Lczx;->d:Z

    .line 205
    invoke-static {p1}, Lg;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lczx;->a:Ljava/lang/String;

    .line 206
    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;B)V
    .locals 0

    .prologue
    .line 195
    invoke-direct {p0, p1}, Lczx;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/Object;)Lczx;
    .locals 2

    .prologue
    .line 227
    new-instance v1, Lczy;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lczy;-><init>(B)V

    iget-object v0, p0, Lczx;->c:Lczy;

    iput-object v1, v0, Lczy;->c:Lczy;

    iput-object v1, p0, Lczx;->c:Lczy;

    iput-object p2, v1, Lczy;->b:Ljava/lang/Object;

    invoke-static {p1}, Lg;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lczy;->a:Ljava/lang/String;

    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 385
    iget-boolean v2, p0, Lczx;->d:Z

    .line 386
    const-string v1, ""

    .line 387
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v3, 0x20

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    iget-object v3, p0, Lczx;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v3, 0x7b

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 388
    iget-object v0, p0, Lczx;->b:Lczy;

    iget-object v0, v0, Lczy;->c:Lczy;

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    :goto_0
    if-eqz v1, :cond_3

    .line 389
    if-eqz v2, :cond_0

    iget-object v4, v1, Lczy;->b:Ljava/lang/Object;

    if-eqz v4, :cond_2

    .line 390
    :cond_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 391
    const-string v0, ", "

    .line 393
    iget-object v4, v1, Lczy;->a:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 394
    iget-object v4, v1, Lczy;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x3d

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 396
    :cond_1
    iget-object v4, v1, Lczy;->b:Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 388
    :cond_2
    iget-object v1, v1, Lczy;->c:Lczy;

    goto :goto_0

    .line 399
    :cond_3
    const/16 v0, 0x7d

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
