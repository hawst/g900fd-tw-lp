.class public final Lesx;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lesx;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Boolean;

.field public d:Ljava/lang/Boolean;

.field public e:Ljava/lang/Boolean;

.field public f:Lesy;

.field public g:Leta;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 959
    const/4 v0, 0x0

    new-array v0, v0, [Lesx;

    sput-object v0, Lesx;->a:[Lesx;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 960
    invoke-direct {p0}, Lepn;-><init>()V

    .line 971
    iput-object v0, p0, Lesx;->f:Lesy;

    .line 974
    iput-object v0, p0, Lesx;->g:Leta;

    .line 960
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1003
    const/4 v0, 0x0

    .line 1004
    iget-object v1, p0, Lesx;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1005
    const/4 v0, 0x1

    iget-object v1, p0, Lesx;->b:Ljava/lang/String;

    .line 1006
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1008
    :cond_0
    iget-object v1, p0, Lesx;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 1009
    const/16 v1, 0x8

    iget-object v2, p0, Lesx;->c:Ljava/lang/Boolean;

    .line 1010
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1012
    :cond_1
    iget-object v1, p0, Lesx;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 1013
    const/16 v1, 0xa

    iget-object v2, p0, Lesx;->d:Ljava/lang/Boolean;

    .line 1014
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1016
    :cond_2
    iget-object v1, p0, Lesx;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 1017
    const/16 v1, 0xc

    iget-object v2, p0, Lesx;->e:Ljava/lang/Boolean;

    .line 1018
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1020
    :cond_3
    iget-object v1, p0, Lesx;->f:Lesy;

    if-eqz v1, :cond_4

    .line 1021
    const/16 v1, 0xd

    iget-object v2, p0, Lesx;->f:Lesy;

    .line 1022
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1024
    :cond_4
    iget-object v1, p0, Lesx;->g:Leta;

    if-eqz v1, :cond_5

    .line 1025
    const/16 v1, 0x65

    iget-object v2, p0, Lesx;->g:Leta;

    .line 1026
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1028
    :cond_5
    iget-object v1, p0, Lesx;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1029
    iput v0, p0, Lesx;->cachedSize:I

    .line 1030
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 956
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lesx;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lesx;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lesx;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lesx;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lesx;->c:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lesx;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lesx;->e:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lesx;->f:Lesy;

    if-nez v0, :cond_2

    new-instance v0, Lesy;

    invoke-direct {v0}, Lesy;-><init>()V

    iput-object v0, p0, Lesx;->f:Lesy;

    :cond_2
    iget-object v0, p0, Lesx;->f:Lesy;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lesx;->g:Leta;

    if-nez v0, :cond_3

    new-instance v0, Leta;

    invoke-direct {v0}, Leta;-><init>()V

    iput-object v0, p0, Lesx;->g:Leta;

    :cond_3
    iget-object v0, p0, Lesx;->g:Leta;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x40 -> :sswitch_2
        0x50 -> :sswitch_3
        0x60 -> :sswitch_4
        0x6a -> :sswitch_5
        0x32a -> :sswitch_6
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 979
    iget-object v0, p0, Lesx;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 980
    const/4 v0, 0x1

    iget-object v1, p0, Lesx;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 982
    :cond_0
    iget-object v0, p0, Lesx;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 983
    const/16 v0, 0x8

    iget-object v1, p0, Lesx;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 985
    :cond_1
    iget-object v0, p0, Lesx;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 986
    const/16 v0, 0xa

    iget-object v1, p0, Lesx;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 988
    :cond_2
    iget-object v0, p0, Lesx;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 989
    const/16 v0, 0xc

    iget-object v1, p0, Lesx;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 991
    :cond_3
    iget-object v0, p0, Lesx;->f:Lesy;

    if-eqz v0, :cond_4

    .line 992
    const/16 v0, 0xd

    iget-object v1, p0, Lesx;->f:Lesy;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 994
    :cond_4
    iget-object v0, p0, Lesx;->g:Leta;

    if-eqz v0, :cond_5

    .line 995
    const/16 v0, 0x65

    iget-object v1, p0, Lesx;->g:Leta;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 997
    :cond_5
    iget-object v0, p0, Lesx;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 999
    return-void
.end method
