.class public final Lape;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/Object;

.field private static b:Z

.field private static c:Z

.field private static d:Z

.field private static e:Z

.field private static final f:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

.field private static final g:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

.field private static final h:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

.field private static final i:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

.field private static final j:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

.field private static final k:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

.field private static final l:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

.field private static final m:[[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/16 v8, 0x1e

    const/16 v7, 0xf

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 88
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lape;->a:Ljava/lang/Object;

    .line 98
    new-instance v0, Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    new-instance v1, Lcom/google/android/libraries/hangouts/video/Size;

    const/16 v2, 0xa0

    const/16 v3, 0x64

    invoke-direct {v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/Size;-><init>(II)V

    invoke-direct {v0, v1, v7}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;-><init>(Lcom/google/android/libraries/hangouts/video/Size;I)V

    sput-object v0, Lape;->f:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    .line 100
    new-instance v0, Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    new-instance v1, Lcom/google/android/libraries/hangouts/video/Size;

    const/16 v2, 0x140

    const/16 v3, 0xc8

    invoke-direct {v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/Size;-><init>(II)V

    invoke-direct {v0, v1, v7}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;-><init>(Lcom/google/android/libraries/hangouts/video/Size;I)V

    sput-object v0, Lape;->g:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    .line 102
    new-instance v0, Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    new-instance v1, Lcom/google/android/libraries/hangouts/video/Size;

    const/16 v2, 0x1e0

    const/16 v3, 0x12c

    invoke-direct {v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/Size;-><init>(II)V

    invoke-direct {v0, v1, v7}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;-><init>(Lcom/google/android/libraries/hangouts/video/Size;I)V

    sput-object v0, Lape;->h:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    .line 104
    new-instance v0, Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    new-instance v1, Lcom/google/android/libraries/hangouts/video/Size;

    const/16 v2, 0x280

    const/16 v3, 0x190

    invoke-direct {v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/Size;-><init>(II)V

    invoke-direct {v0, v1, v8}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;-><init>(Lcom/google/android/libraries/hangouts/video/Size;I)V

    sput-object v0, Lape;->i:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    .line 106
    new-instance v0, Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    new-instance v1, Lcom/google/android/libraries/hangouts/video/Size;

    const/16 v2, 0x3c0

    const/16 v3, 0x258

    invoke-direct {v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/Size;-><init>(II)V

    invoke-direct {v0, v1, v8}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;-><init>(Lcom/google/android/libraries/hangouts/video/Size;I)V

    sput-object v0, Lape;->j:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    .line 108
    new-instance v0, Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    new-instance v1, Lcom/google/android/libraries/hangouts/video/Size;

    const/16 v2, 0x500

    const/16 v3, 0x320

    invoke-direct {v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/Size;-><init>(II)V

    invoke-direct {v0, v1, v8}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;-><init>(Lcom/google/android/libraries/hangouts/video/Size;I)V

    sput-object v0, Lape;->k:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    .line 110
    new-instance v0, Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    new-instance v1, Lcom/google/android/libraries/hangouts/video/Size;

    const/16 v2, 0x780

    const/16 v3, 0x4b0

    invoke-direct {v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/Size;-><init>(II)V

    invoke-direct {v0, v1, v8}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;-><init>(Lcom/google/android/libraries/hangouts/video/Size;I)V

    sput-object v0, Lape;->l:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    .line 115
    const/16 v0, 0x12

    new-array v0, v0, [[Ljava/lang/String;

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "babel_hangout_blocked_interface_names"

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/android/libraries/hangouts/video/VideoChat;->BLOCK_INTERFACE_NAMES:Ljava/lang/String;

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "babel_hangout_ec_comfort_noise_generation"

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/android/libraries/hangouts/video/VideoChat;->EC_COMFORT_NOISE_GENERATION:Ljava/lang/String;

    aput-object v2, v1, v5

    aput-object v1, v0, v5

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "babel_hangout_agc_mode"

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/android/libraries/hangouts/video/VideoChat;->AGC_MODE:Ljava/lang/String;

    aput-object v2, v1, v5

    aput-object v1, v0, v6

    const/4 v1, 0x3

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "babel_hangout_agc_config_target_level"

    aput-object v3, v2, v4

    sget-object v3, Lcom/google/android/libraries/hangouts/video/VideoChat;->AGC_CONFIG_TARGET_LEVEL:Ljava/lang/String;

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "babel_hangout_agc_config_comp_gain"

    aput-object v3, v2, v4

    sget-object v3, Lcom/google/android/libraries/hangouts/video/VideoChat;->AGC_CONFIG_COMP_GAIN:Ljava/lang/String;

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "babel_hangout_agc_config_limiter_enable"

    aput-object v3, v2, v4

    sget-object v3, Lcom/google/android/libraries/hangouts/video/VideoChat;->AGC_CONFIG_LIMITER_ENABLE:Ljava/lang/String;

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "babel_hangout_enable_rx_auto_gain_control"

    aput-object v3, v2, v4

    sget-object v3, Lcom/google/android/libraries/hangouts/video/VideoChat;->ENABLE_RX_AUTO_GAIN_CONTROL:Ljava/lang/String;

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "babel_hangout_rx_agc_config_target_level"

    aput-object v3, v2, v4

    sget-object v3, Lcom/google/android/libraries/hangouts/video/VideoChat;->RX_AGC_CONFIG_TARGET_LEVEL:Ljava/lang/String;

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "babel_hangout_rx_agc_config_comp_gain"

    aput-object v3, v2, v4

    sget-object v3, Lcom/google/android/libraries/hangouts/video/VideoChat;->RX_AGC_CONFIG_COMP_GAIN:Ljava/lang/String;

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "babel_hangout_rx_agc_config_limiter_enable"

    aput-object v3, v2, v4

    sget-object v3, Lcom/google/android/libraries/hangouts/video/VideoChat;->RX_AGC_CONFIG_LIMITER_ENABLE:Ljava/lang/String;

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "babel_hangout_audio_record_sampling_rate"

    aput-object v3, v2, v4

    sget-object v3, Lcom/google/android/libraries/hangouts/video/VideoChat;->AUDIO_RECORDING_SAMPLING_RATE:Ljava/lang/String;

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "babel_hangout_audio_playback_sampling_rate"

    aput-object v3, v2, v4

    sget-object v3, Lcom/google/android/libraries/hangouts/video/VideoChat;->AUDIO_PLAYBACK_SAMPLING_RATE:Ljava/lang/String;

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "babel_hangout_audio_recording_device"

    aput-object v3, v2, v4

    sget-object v3, Lcom/google/android/libraries/hangouts/video/VideoChat;->AUDIO_RECORDING_DEVICE:Ljava/lang/String;

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "babel_hangout_xmpp_hostname"

    aput-object v3, v2, v4

    sget-object v3, Lcom/google/android/libraries/hangouts/video/VideoChat;->XMPP_HOSTNAME:Ljava/lang/String;

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xe

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "babel_hangout_xmpp_port"

    aput-object v3, v2, v4

    sget-object v3, Lcom/google/android/libraries/hangouts/video/VideoChat;->XMPP_PORT:Ljava/lang/String;

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "babel_hangout_enter_step_timeout"

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/android/libraries/hangouts/video/VideoChat;->CALL_ENTER_STEP_TIMEOUT_MILLIS:Ljava/lang/String;

    aput-object v2, v1, v5

    aput-object v1, v0, v7

    const/16 v1, 0x10

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "babel_hangout_log_file_size"

    aput-object v3, v2, v4

    sget-object v3, Lcom/google/android/libraries/hangouts/video/VideoChat;->DIAGNOSTIC_RAW_LOG_FILE_SIZE_BYTES:Ljava/lang/String;

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x11

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "babel_hangout_use_mesi"

    aput-object v3, v2, v4

    sget-object v3, Lcom/google/android/libraries/hangouts/video/VideoChat;->USE_MESI:Ljava/lang/String;

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    sput-object v0, Lape;->m:[[Ljava/lang/String;

    return-void
.end method

.method public static a()V
    .locals 1

    .prologue
    .line 157
    new-instance v0, Laph;

    invoke-direct {v0}, Laph;-><init>()V

    .line 158
    invoke-virtual {v0}, Laph;->a()V

    .line 159
    return-void
.end method

.method public static a(Ljava/lang/Runnable;Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 218
    sget-boolean v0, Lape;->b:Z

    if-eqz v0, :cond_1

    .line 219
    sget-boolean v0, Lape;->c:Z

    if-eqz v0, :cond_0

    .line 220
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    .line 233
    :goto_0
    return-void

    .line 224
    :cond_0
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 229
    :cond_1
    new-instance v0, Lapg;

    invoke-direct {v0, p0, p1}, Lapg;-><init>(Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    .line 231
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lapg;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method static synthetic a(Z)Z
    .locals 0

    .prologue
    .line 43
    sput-boolean p0, Lape;->d:Z

    return p0
.end method

.method public static b()Z
    .locals 2

    .prologue
    .line 172
    invoke-static {}, Lcwz;->b()V

    .line 173
    sget-object v1, Lape;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 174
    :try_start_0
    sget-boolean v0, Lape;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 176
    :try_start_1
    sget-object v0, Lape;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 183
    :cond_0
    :try_start_2
    sget-boolean v0, Lape;->b:Z

    invoke-static {v0}, Lcwz;->a(Z)V

    .line 184
    sget-boolean v0, Lape;->c:Z

    monitor-exit v1

    :goto_0
    return v0

    .line 178
    :catch_0
    move-exception v0

    const-string v0, "Unexpected"

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    .line 179
    const/4 v0, 0x0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 185
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic b(Z)Z
    .locals 0

    .prologue
    .line 43
    sput-boolean p0, Lape;->e:Z

    return p0
.end method

.method public static c()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 194
    sget-boolean v0, Lape;->b:Z

    if-eqz v0, :cond_0

    .line 195
    sget-boolean v0, Lape;->c:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 197
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Z)Z
    .locals 0

    .prologue
    .line 43
    sput-boolean p0, Lape;->c:Z

    return p0
.end method

.method public static d()Z
    .locals 1

    .prologue
    .line 205
    sget-boolean v0, Lape;->d:Z

    return v0
.end method

.method public static e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 240
    sget-boolean v0, Lape;->b:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lape;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 243
    invoke-static {}, Lape;->v()Lapf;

    move-result-object v0

    .line 244
    iget-boolean v0, v0, Lapf;->c:Z

    if-nez v0, :cond_1

    .line 245
    sget v0, Lh;->eU:I

    .line 249
    :goto_1
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 240
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 247
    :cond_1
    sget v0, Lh;->eT:I

    goto :goto_1
.end method

.method public static f()V
    .locals 3

    .prologue
    .line 253
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "deviceCapabilities"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 254
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 255
    const-string v1, "dirty"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 256
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 257
    return-void
.end method

.method static synthetic g()Z
    .locals 1

    .prologue
    .line 43
    sget-boolean v0, Lape;->b:Z

    return v0
.end method

.method static synthetic h()Lapf;
    .locals 1

    .prologue
    .line 43
    invoke-static {}, Lape;->v()Lapf;

    move-result-object v0

    return-object v0
.end method

.method static synthetic i()Lcom/google/android/libraries/hangouts/video/VideoSpecification;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lape;->g:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    return-object v0
.end method

.method static synthetic j()Lcom/google/android/libraries/hangouts/video/VideoSpecification;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lape;->f:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    return-object v0
.end method

.method static synthetic k()Lcom/google/android/libraries/hangouts/video/VideoSpecification;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lape;->i:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    return-object v0
.end method

.method static synthetic l()Lcom/google/android/libraries/hangouts/video/VideoSpecification;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lape;->j:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    return-object v0
.end method

.method static synthetic m()Z
    .locals 1

    .prologue
    .line 43
    sget-boolean v0, Lape;->d:Z

    return v0
.end method

.method static synthetic n()Lcom/google/android/libraries/hangouts/video/VideoSpecification;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lape;->k:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    return-object v0
.end method

.method static synthetic o()Lcom/google/android/libraries/hangouts/video/VideoSpecification;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lape;->h:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    return-object v0
.end method

.method static synthetic p()Z
    .locals 1

    .prologue
    .line 43
    sget-boolean v0, Lape;->e:Z

    return v0
.end method

.method static synthetic q()Z
    .locals 1

    .prologue
    .line 43
    sget-boolean v0, Lape;->c:Z

    return v0
.end method

.method static synthetic r()[[Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lape;->m:[[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic s()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lape;->a:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic t()Z
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x1

    sput-boolean v0, Lape;->b:Z

    return v0
.end method

.method private static u()Z
    .locals 9

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 263
    const-string v2, "armeabi-v7a"

    sget-object v3, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-static {v2, v3}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 268
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    const-string v2, "/proc/cpuinfo"

    invoke-direct {v3, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_3

    .line 270
    :try_start_1
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    new-instance v5, Ljava/io/DataInputStream;

    invoke-direct {v5, v3}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v4, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 274
    :cond_0
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 275
    const-string v5, "[ \t]*:[ \t]*"

    const/4 v6, 0x2

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v4

    .line 276
    if-eqz v4, :cond_0

    array-length v5, v4

    if-ne v5, v7, :cond_0

    const-string v5, "Features"

    const/4 v6, 0x0

    aget-object v6, v4, v6

    .line 277
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 278
    const/4 v2, 0x1

    aget-object v2, v4, v2

    const-string v4, "[ \t]"

    invoke-virtual {v2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 279
    array-length v5, v4

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_2

    aget-object v6, v4, v2

    .line 280
    const-string v7, "neon"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v6

    if-eqz v6, :cond_1

    .line 293
    :goto_1
    :try_start_3
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_2

    .line 301
    :goto_2
    return v0

    .line 279
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 285
    goto :goto_1

    :cond_3
    move v0, v1

    .line 290
    goto :goto_1

    .line 288
    :catch_0
    move-exception v0

    .line 289
    :try_start_4
    const-string v2, "Babel"

    const-string v4, "Exception in hasNEON:"

    invoke-static {v2, v4, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move v0, v1

    goto :goto_1

    .line 294
    :catch_1
    move-exception v1

    .line 295
    :try_start_5
    const-string v2, "Babel"

    const-string v3, "Exception in hasNEON:"

    invoke-static {v2, v3, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_2

    .line 300
    :catch_2
    move-exception v1

    .line 299
    :goto_3
    const-string v2, "Babel"

    const-string v3, "Exception in hasNEON:"

    invoke-static {v2, v3, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 292
    :catchall_0
    move-exception v0

    .line 293
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_3

    .line 296
    :goto_4
    :try_start_7
    throw v0

    .line 300
    :catch_3
    move-exception v0

    move-object v8, v0

    move v0, v1

    move-object v1, v8

    goto :goto_3

    .line 294
    :catch_4
    move-exception v2

    .line 295
    const-string v3, "Babel"

    const-string v4, "Exception in hasNEON:"

    invoke-static {v3, v4, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_4
.end method

.method private static v()Lapf;
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 305
    new-instance v3, Lapf;

    invoke-direct {v3, v1}, Lapf;-><init>(B)V

    .line 306
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v4

    .line 307
    const-string v0, "deviceCapabilities"

    invoke-virtual {v4, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 310
    invoke-static {}, Lcxl;->b()I

    move-result v0

    iput v0, v3, Lapf;->d:I

    .line 314
    const-string v0, "armeabi-v7a"

    sget-object v6, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "armeabi-v7a"

    sget-object v6, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    .line 315
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "arm64-v8a"

    sget-object v6, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    .line 316
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    iput-boolean v0, v3, Lapf;->a:Z

    .line 318
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/DecoderManager;->supportsHardwareAcceleration()Z

    move-result v0

    iput-boolean v0, v3, Lapf;->f:Z

    .line 320
    iput-boolean v1, v3, Lapf;->g:Z

    .line 322
    const-string v0, "dirty"

    invoke-interface {v5, v0, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 323
    iput-boolean v1, v3, Lapf;->e:Z

    .line 324
    iget-boolean v0, v3, Lapf;->a:Z

    if-nez v0, :cond_3

    .line 325
    const-string v0, "Babel"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Not arm7 (ABI) "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v7, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    const-string v0, "Babel"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Not arm7 (ABI2) "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v7, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    :cond_1
    :goto_1
    sget v0, Lbxp;->a:F

    const/high16 v6, 0x40000000    # 2.0f

    cmpl-float v0, v0, v6

    if-ltz v0, :cond_5

    :goto_2
    iput-boolean v2, v3, Lapf;->b:Z

    .line 337
    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v2, "android.hardware.microphone"

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, v3, Lapf;->c:Z

    .line 339
    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Microphone: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v4, v3, Lapf;->c:Z

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 342
    const-string v2, "dirty"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 343
    const-string v1, "microphone"

    iget-boolean v2, v3, Lapf;->c:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 344
    const-string v1, "glv2"

    iget-boolean v2, v3, Lapf;->b:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 345
    const-string v1, "armv7"

    iget-boolean v2, v3, Lapf;->a:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 346
    const-string v1, "neon"

    iget-boolean v2, v3, Lapf;->e:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 348
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 356
    :goto_3
    return-object v3

    :cond_2
    move v0, v1

    .line 316
    goto/16 :goto_0

    .line 328
    :cond_3
    const-string v0, "armeabi-v7a"

    sget-object v6, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 329
    invoke-static {}, Lape;->u()Z

    move-result v0

    iput-boolean v0, v3, Lapf;->e:Z

    goto :goto_1

    .line 330
    :cond_4
    const-string v0, "arm64-v8a"

    sget-object v6, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 331
    iput-boolean v2, v3, Lapf;->e:Z

    goto :goto_1

    :cond_5
    move v2, v1

    .line 335
    goto :goto_2

    .line 351
    :cond_6
    const-string v0, "microphone"

    invoke-interface {v5, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, v3, Lapf;->c:Z

    .line 352
    const-string v0, "glv2"

    invoke-interface {v5, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, v3, Lapf;->b:Z

    .line 353
    const-string v0, "neon"

    invoke-interface {v5, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, v3, Lapf;->e:Z

    goto :goto_3
.end method
