.class final Lbyy;
.super Ljava/lang/Thread;
.source "PG"


# instance fields
.field public a:Lbyx;

.field final synthetic b:Lbyv;


# direct methods
.method public constructor <init>(Lbyv;Lbyx;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lbyy;->b:Lbyv;

    .line 78
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 79
    iput-object p2, p0, Lbyy;->a:Lbyx;

    .line 80
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 84
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 85
    iget-object v0, p0, Lbyy;->b:Lbyv;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v0, v1}, Lbyv;->a(Lbyv;Landroid/os/Looper;)Landroid/os/Looper;

    .line 86
    monitor-enter p0

    .line 87
    :try_start_0
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "audio"

    .line 88
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 90
    :try_start_1
    new-instance v2, Landroid/media/MediaPlayer;

    invoke-direct {v2}, Landroid/media/MediaPlayer;-><init>()V

    .line 91
    iget-object v1, p0, Lbyy;->a:Lbyx;

    iget v1, v1, Lbyx;->d:I

    invoke-virtual {v2, v1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 92
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lbyy;->a:Lbyx;

    iget-object v3, v3, Lbyx;->b:Landroid/net/Uri;

    invoke-virtual {v2, v1, v3}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 93
    iget-object v1, p0, Lbyy;->a:Lbyx;

    iget-boolean v1, v1, Lbyx;->c:Z

    invoke-virtual {v2, v1}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 94
    iget-object v1, p0, Lbyy;->a:Lbyx;

    iget v1, v1, Lbyx;->e:F

    iget-object v3, p0, Lbyy;->a:Lbyx;

    iget v3, v3, Lbyx;->e:F

    invoke-virtual {v2, v1, v3}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 95
    invoke-virtual {v2}, Landroid/media/MediaPlayer;->prepare()V

    .line 96
    iget-object v1, p0, Lbyy;->a:Lbyx;

    iget-object v1, v1, Lbyx;->b:Landroid/net/Uri;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbyy;->a:Lbyx;

    iget-object v1, v1, Lbyx;->b:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getEncodedPath()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbyy;->a:Lbyx;

    iget-object v1, v1, Lbyx;->b:Landroid/net/Uri;

    .line 97
    invoke-virtual {v1}, Landroid/net/Uri;->getEncodedPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 98
    const/4 v3, 0x0

    iget-object v1, p0, Lbyy;->a:Lbyx;

    iget v4, v1, Lbyx;->d:I

    iget-object v1, p0, Lbyy;->a:Lbyx;

    iget-boolean v1, v1, Lbyx;->c:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x2

    :goto_0
    invoke-virtual {v0, v3, v4, v1}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 103
    :cond_0
    iget-object v1, p0, Lbyy;->b:Lbyv;

    invoke-virtual {v2, v1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 104
    invoke-virtual {v2}, Landroid/media/MediaPlayer;->start()V

    .line 105
    iget-object v1, p0, Lbyy;->b:Lbyv;

    invoke-static {v1}, Lbyv;->a(Lbyv;)Landroid/media/MediaPlayer;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 106
    iget-object v1, p0, Lbyy;->b:Lbyv;

    invoke-static {v1}, Lbyv;->a(Lbyv;)Landroid/media/MediaPlayer;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    .line 108
    :cond_1
    iget-object v1, p0, Lbyy;->b:Lbyv;

    invoke-static {v1, v2}, Lbyv;->a(Lbyv;Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 112
    :goto_1
    :try_start_2
    iget-object v1, p0, Lbyy;->b:Lbyv;

    invoke-static {v1, v0}, Lbyv;->a(Lbyv;Landroid/media/AudioManager;)Landroid/media/AudioManager;

    .line 113
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 114
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 115
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 116
    return-void

    .line 98
    :cond_2
    const/4 v1, 0x3

    goto :goto_0

    .line 109
    :catch_0
    move-exception v1

    .line 110
    :try_start_3
    iget-object v2, p0, Lbyy;->b:Lbyv;

    invoke-static {v2}, Lbyv;->b(Lbyv;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "error loading sound for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lbyy;->a:Lbyx;

    iget-object v4, v4, Lbyx;->b:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 114
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
