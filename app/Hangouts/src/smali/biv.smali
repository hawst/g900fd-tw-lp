.class public final Lbiv;
.super Lbjg;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbiz;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbiw;",
            ">;"
        }
    .end annotation
.end field

.field private u:I


# direct methods
.method public constructor <init>(Ldrr;IJLdpq;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 482
    invoke-direct {p0, p1, p2, p3, p4}, Lbjg;-><init>(Ldrr;IJ)V

    .line 483
    iget-object v0, p5, Ldpq;->b:Ldtx;

    iget-object v1, v0, Ldtx;->b:[Lesg;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    array-length v4, v1

    move v0, v2

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v1, v0

    new-instance v6, Lbiz;

    invoke-direct {v6, v5}, Lbiz;-><init>(Lesg;)V

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iput-object v3, p0, Lbiv;->b:Ljava/util/List;

    .line 485
    iget-object v0, p5, Ldpq;->b:Ldtx;

    iget-object v0, v0, Ldtx;->c:[Lesc;

    invoke-static {v0}, Lbiw;->a([Lesc;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbiv;->f:Ljava/util/List;

    .line 489
    iget-object v0, p0, Lbiv;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbiz;

    .line 490
    iget-object v0, v0, Lbiz;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 491
    goto :goto_1

    .line 493
    :cond_1
    iget-object v0, p0, Lbiv;->f:Ljava/util/List;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lbiv;->f:Ljava/util/List;

    .line 494
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbiw;

    iget-object v0, v0, Lbiw;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbiv;->f:Ljava/util/List;

    .line 495
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbiw;

    iget-object v0, v0, Lbiw;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    const/4 v2, 0x1

    .line 496
    :cond_3
    if-nez v1, :cond_4

    if-nez v2, :cond_4

    .line 497
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Message without text or image: eventId "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Ldrr;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " conversationId "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Ldrr;->b:Ldqf;

    iget-object v2, v2, Ldqf;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " num segments = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p5, Ldpq;->b:Ldtx;

    iget-object v2, v2, Ldtx;->b:[Lesg;

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " num attachments = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p5, Ldpq;->b:Ldtx;

    iget-object v2, v2, Ldtx;->c:[Lesc;

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    :cond_4
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/util/List;Lbdk;JLjava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lbiz;",
            ">;",
            "Lbdk;",
            "J",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-wide v3, p4

    move-object v5, p6

    .line 513
    invoke-direct/range {v0 .. v5}, Lbjg;-><init>(Ljava/lang/String;Lbdk;JLjava/lang/String;)V

    .line 514
    iput-object p2, p0, Lbiv;->b:Ljava/util/List;

    .line 515
    const/4 v0, 0x0

    iput-object v0, p0, Lbiv;->f:Ljava/util/List;

    .line 516
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/util/List;Lbdk;JLjava/lang/String;)Lbiv;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lbiz;",
            ">;",
            "Lbdk;",
            "J",
            "Ljava/lang/String;",
            ")",
            "Lbiv;"
        }
    .end annotation

    .prologue
    .line 508
    new-instance v0, Lbiv;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lbiv;-><init>(Ljava/lang/String;Ljava/util/List;Lbdk;JLjava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 540
    iput p1, p0, Lbiv;->u:I

    .line 541
    return-void
.end method

.method public b()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbdk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 520
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 521
    iget-object v1, p0, Lbiv;->d:Lbdk;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 522
    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 530
    iget-object v0, p0, Lbiv;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbiv;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 531
    iget-object v0, p0, Lbiv;->b:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbiz;

    iget-object v0, v0, Lbiz;->b:Ljava/lang/String;

    .line 532
    invoke-static {v0}, Lf;->m(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 536
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 544
    iget v0, p0, Lbiv;->u:I

    return v0
.end method
