.class public final Ldqy;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldqy;


# instance fields
.field public b:Ljava/lang/Boolean;

.field public c:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15705
    const/4 v0, 0x0

    new-array v0, v0, [Ldqy;

    sput-object v0, Ldqy;->a:[Ldqy;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15706
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 15727
    const/4 v0, 0x0

    .line 15728
    iget-object v1, p0, Ldqy;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 15729
    const/4 v0, 0x1

    iget-object v1, p0, Ldqy;->b:Ljava/lang/Boolean;

    .line 15730
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Lepl;->g(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 15732
    :cond_0
    iget-object v1, p0, Ldqy;->c:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 15733
    const/4 v1, 0x2

    iget-object v2, p0, Ldqy;->c:Ljava/lang/Long;

    .line 15734
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 15736
    :cond_1
    iget-object v1, p0, Ldqy;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15737
    iput v0, p0, Ldqy;->cachedSize:I

    .line 15738
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 15702
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldqy;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldqy;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldqy;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldqy;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldqy;->c:Ljava/lang/Long;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 15715
    iget-object v0, p0, Ldqy;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 15716
    const/4 v0, 0x1

    iget-object v1, p0, Ldqy;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 15718
    :cond_0
    iget-object v0, p0, Ldqy;->c:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 15719
    const/4 v0, 0x2

    iget-object v1, p0, Ldqy;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 15721
    :cond_1
    iget-object v0, p0, Ldqy;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 15723
    return-void
.end method
