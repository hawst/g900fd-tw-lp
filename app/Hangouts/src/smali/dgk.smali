.class public final Ldgk;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldgk;


# instance fields
.field public b:Ldfj;

.field public c:Ldfn;

.field public d:Ldfk;

.field public e:Ldex;

.field public f:Ldgc;

.field public g:Ldew;

.field public h:Ldeu;

.field public i:Ldfh;

.field public j:Lded;

.field public k:Ldgc;

.field public l:Ldfs;

.field public m:Ldfr;

.field public n:Ljava/lang/Boolean;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/Boolean;

.field public q:Ldec;

.field public r:Ldes;

.field public s:Ldgc;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5638
    const/4 v0, 0x0

    new-array v0, v0, [Ldgk;

    sput-object v0, Ldgk;->a:[Ldgk;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5639
    invoke-direct {p0}, Lepn;-><init>()V

    .line 5642
    iput-object v0, p0, Ldgk;->b:Ldfj;

    .line 5645
    iput-object v0, p0, Ldgk;->c:Ldfn;

    .line 5648
    iput-object v0, p0, Ldgk;->d:Ldfk;

    .line 5651
    iput-object v0, p0, Ldgk;->e:Ldex;

    .line 5654
    iput-object v0, p0, Ldgk;->f:Ldgc;

    .line 5657
    iput-object v0, p0, Ldgk;->g:Ldew;

    .line 5660
    iput-object v0, p0, Ldgk;->h:Ldeu;

    .line 5663
    iput-object v0, p0, Ldgk;->i:Ldfh;

    .line 5666
    iput-object v0, p0, Ldgk;->j:Lded;

    .line 5669
    iput-object v0, p0, Ldgk;->k:Ldgc;

    .line 5672
    iput-object v0, p0, Ldgk;->l:Ldfs;

    .line 5675
    iput-object v0, p0, Ldgk;->m:Ldfr;

    .line 5684
    iput-object v0, p0, Ldgk;->q:Ldec;

    .line 5687
    iput-object v0, p0, Ldgk;->r:Ldes;

    .line 5690
    iput-object v0, p0, Ldgk;->s:Ldgc;

    .line 5639
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 5755
    const/4 v0, 0x0

    .line 5756
    iget-object v1, p0, Ldgk;->b:Ldfj;

    if-eqz v1, :cond_0

    .line 5757
    const/4 v0, 0x1

    iget-object v1, p0, Ldgk;->b:Ldfj;

    .line 5758
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5760
    :cond_0
    iget-object v1, p0, Ldgk;->c:Ldfn;

    if-eqz v1, :cond_1

    .line 5761
    const/4 v1, 0x2

    iget-object v2, p0, Ldgk;->c:Ldfn;

    .line 5762
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5764
    :cond_1
    iget-object v1, p0, Ldgk;->d:Ldfk;

    if-eqz v1, :cond_2

    .line 5765
    const/4 v1, 0x3

    iget-object v2, p0, Ldgk;->d:Ldfk;

    .line 5766
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5768
    :cond_2
    iget-object v1, p0, Ldgk;->e:Ldex;

    if-eqz v1, :cond_3

    .line 5769
    const/4 v1, 0x4

    iget-object v2, p0, Ldgk;->e:Ldex;

    .line 5770
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5772
    :cond_3
    iget-object v1, p0, Ldgk;->f:Ldgc;

    if-eqz v1, :cond_4

    .line 5773
    const/4 v1, 0x5

    iget-object v2, p0, Ldgk;->f:Ldgc;

    .line 5774
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5776
    :cond_4
    iget-object v1, p0, Ldgk;->g:Ldew;

    if-eqz v1, :cond_5

    .line 5777
    const/4 v1, 0x6

    iget-object v2, p0, Ldgk;->g:Ldew;

    .line 5778
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5780
    :cond_5
    iget-object v1, p0, Ldgk;->h:Ldeu;

    if-eqz v1, :cond_6

    .line 5781
    const/4 v1, 0x7

    iget-object v2, p0, Ldgk;->h:Ldeu;

    .line 5782
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5784
    :cond_6
    iget-object v1, p0, Ldgk;->i:Ldfh;

    if-eqz v1, :cond_7

    .line 5785
    const/16 v1, 0x8

    iget-object v2, p0, Ldgk;->i:Ldfh;

    .line 5786
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5788
    :cond_7
    iget-object v1, p0, Ldgk;->j:Lded;

    if-eqz v1, :cond_8

    .line 5789
    const/16 v1, 0x9

    iget-object v2, p0, Ldgk;->j:Lded;

    .line 5790
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5792
    :cond_8
    iget-object v1, p0, Ldgk;->k:Ldgc;

    if-eqz v1, :cond_9

    .line 5793
    const/16 v1, 0xa

    iget-object v2, p0, Ldgk;->k:Ldgc;

    .line 5794
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5796
    :cond_9
    iget-object v1, p0, Ldgk;->l:Ldfs;

    if-eqz v1, :cond_a

    .line 5797
    const/16 v1, 0xb

    iget-object v2, p0, Ldgk;->l:Ldfs;

    .line 5798
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5800
    :cond_a
    iget-object v1, p0, Ldgk;->m:Ldfr;

    if-eqz v1, :cond_b

    .line 5801
    const/16 v1, 0xc

    iget-object v2, p0, Ldgk;->m:Ldfr;

    .line 5802
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5804
    :cond_b
    iget-object v1, p0, Ldgk;->n:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    .line 5805
    const/16 v1, 0xd

    iget-object v2, p0, Ldgk;->n:Ljava/lang/Boolean;

    .line 5806
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5808
    :cond_c
    iget-object v1, p0, Ldgk;->o:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 5809
    const/16 v1, 0xe

    iget-object v2, p0, Ldgk;->o:Ljava/lang/String;

    .line 5810
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5812
    :cond_d
    iget-object v1, p0, Ldgk;->p:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    .line 5813
    const/16 v1, 0x10

    iget-object v2, p0, Ldgk;->p:Ljava/lang/Boolean;

    .line 5814
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5816
    :cond_e
    iget-object v1, p0, Ldgk;->q:Ldec;

    if-eqz v1, :cond_f

    .line 5817
    const/16 v1, 0x11

    iget-object v2, p0, Ldgk;->q:Ldec;

    .line 5818
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5820
    :cond_f
    iget-object v1, p0, Ldgk;->r:Ldes;

    if-eqz v1, :cond_10

    .line 5821
    const/16 v1, 0x12

    iget-object v2, p0, Ldgk;->r:Ldes;

    .line 5822
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5824
    :cond_10
    iget-object v1, p0, Ldgk;->s:Ldgc;

    if-eqz v1, :cond_11

    .line 5825
    const/16 v1, 0x13

    iget-object v2, p0, Ldgk;->s:Ldgc;

    .line 5826
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5828
    :cond_11
    iget-object v1, p0, Ldgk;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5829
    iput v0, p0, Ldgk;->cachedSize:I

    .line 5830
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 5635
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldgk;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldgk;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldgk;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldgk;->b:Ldfj;

    if-nez v0, :cond_2

    new-instance v0, Ldfj;

    invoke-direct {v0}, Ldfj;-><init>()V

    iput-object v0, p0, Ldgk;->b:Ldfj;

    :cond_2
    iget-object v0, p0, Ldgk;->b:Ldfj;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldgk;->c:Ldfn;

    if-nez v0, :cond_3

    new-instance v0, Ldfn;

    invoke-direct {v0}, Ldfn;-><init>()V

    iput-object v0, p0, Ldgk;->c:Ldfn;

    :cond_3
    iget-object v0, p0, Ldgk;->c:Ldfn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldgk;->d:Ldfk;

    if-nez v0, :cond_4

    new-instance v0, Ldfk;

    invoke-direct {v0}, Ldfk;-><init>()V

    iput-object v0, p0, Ldgk;->d:Ldfk;

    :cond_4
    iget-object v0, p0, Ldgk;->d:Ldfk;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldgk;->e:Ldex;

    if-nez v0, :cond_5

    new-instance v0, Ldex;

    invoke-direct {v0}, Ldex;-><init>()V

    iput-object v0, p0, Ldgk;->e:Ldex;

    :cond_5
    iget-object v0, p0, Ldgk;->e:Ldex;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ldgk;->f:Ldgc;

    if-nez v0, :cond_6

    new-instance v0, Ldgc;

    invoke-direct {v0}, Ldgc;-><init>()V

    iput-object v0, p0, Ldgk;->f:Ldgc;

    :cond_6
    iget-object v0, p0, Ldgk;->f:Ldgc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Ldgk;->g:Ldew;

    if-nez v0, :cond_7

    new-instance v0, Ldew;

    invoke-direct {v0}, Ldew;-><init>()V

    iput-object v0, p0, Ldgk;->g:Ldew;

    :cond_7
    iget-object v0, p0, Ldgk;->g:Ldew;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Ldgk;->h:Ldeu;

    if-nez v0, :cond_8

    new-instance v0, Ldeu;

    invoke-direct {v0}, Ldeu;-><init>()V

    iput-object v0, p0, Ldgk;->h:Ldeu;

    :cond_8
    iget-object v0, p0, Ldgk;->h:Ldeu;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Ldgk;->i:Ldfh;

    if-nez v0, :cond_9

    new-instance v0, Ldfh;

    invoke-direct {v0}, Ldfh;-><init>()V

    iput-object v0, p0, Ldgk;->i:Ldfh;

    :cond_9
    iget-object v0, p0, Ldgk;->i:Ldfh;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Ldgk;->j:Lded;

    if-nez v0, :cond_a

    new-instance v0, Lded;

    invoke-direct {v0}, Lded;-><init>()V

    iput-object v0, p0, Ldgk;->j:Lded;

    :cond_a
    iget-object v0, p0, Ldgk;->j:Lded;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Ldgk;->k:Ldgc;

    if-nez v0, :cond_b

    new-instance v0, Ldgc;

    invoke-direct {v0}, Ldgc;-><init>()V

    iput-object v0, p0, Ldgk;->k:Ldgc;

    :cond_b
    iget-object v0, p0, Ldgk;->k:Ldgc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Ldgk;->l:Ldfs;

    if-nez v0, :cond_c

    new-instance v0, Ldfs;

    invoke-direct {v0}, Ldfs;-><init>()V

    iput-object v0, p0, Ldgk;->l:Ldfs;

    :cond_c
    iget-object v0, p0, Ldgk;->l:Ldfs;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Ldgk;->m:Ldfr;

    if-nez v0, :cond_d

    new-instance v0, Ldfr;

    invoke-direct {v0}, Ldfr;-><init>()V

    iput-object v0, p0, Ldgk;->m:Ldfr;

    :cond_d
    iget-object v0, p0, Ldgk;->m:Ldfr;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldgk;->n:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldgk;->o:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldgk;->p:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_10
    iget-object v0, p0, Ldgk;->q:Ldec;

    if-nez v0, :cond_e

    new-instance v0, Ldec;

    invoke-direct {v0}, Ldec;-><init>()V

    iput-object v0, p0, Ldgk;->q:Ldec;

    :cond_e
    iget-object v0, p0, Ldgk;->q:Ldec;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_11
    iget-object v0, p0, Ldgk;->r:Ldes;

    if-nez v0, :cond_f

    new-instance v0, Ldes;

    invoke-direct {v0}, Ldes;-><init>()V

    iput-object v0, p0, Ldgk;->r:Ldes;

    :cond_f
    iget-object v0, p0, Ldgk;->r:Ldes;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_12
    iget-object v0, p0, Ldgk;->s:Ldgc;

    if-nez v0, :cond_10

    new-instance v0, Ldgc;

    invoke-direct {v0}, Ldgc;-><init>()V

    iput-object v0, p0, Ldgk;->s:Ldgc;

    :cond_10
    iget-object v0, p0, Ldgk;->s:Ldgc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x68 -> :sswitch_d
        0x72 -> :sswitch_e
        0x80 -> :sswitch_f
        0x8a -> :sswitch_10
        0x92 -> :sswitch_11
        0x9a -> :sswitch_12
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 5695
    iget-object v0, p0, Ldgk;->b:Ldfj;

    if-eqz v0, :cond_0

    .line 5696
    const/4 v0, 0x1

    iget-object v1, p0, Ldgk;->b:Ldfj;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 5698
    :cond_0
    iget-object v0, p0, Ldgk;->c:Ldfn;

    if-eqz v0, :cond_1

    .line 5699
    const/4 v0, 0x2

    iget-object v1, p0, Ldgk;->c:Ldfn;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 5701
    :cond_1
    iget-object v0, p0, Ldgk;->d:Ldfk;

    if-eqz v0, :cond_2

    .line 5702
    const/4 v0, 0x3

    iget-object v1, p0, Ldgk;->d:Ldfk;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 5704
    :cond_2
    iget-object v0, p0, Ldgk;->e:Ldex;

    if-eqz v0, :cond_3

    .line 5705
    const/4 v0, 0x4

    iget-object v1, p0, Ldgk;->e:Ldex;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 5707
    :cond_3
    iget-object v0, p0, Ldgk;->f:Ldgc;

    if-eqz v0, :cond_4

    .line 5708
    const/4 v0, 0x5

    iget-object v1, p0, Ldgk;->f:Ldgc;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 5710
    :cond_4
    iget-object v0, p0, Ldgk;->g:Ldew;

    if-eqz v0, :cond_5

    .line 5711
    const/4 v0, 0x6

    iget-object v1, p0, Ldgk;->g:Ldew;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 5713
    :cond_5
    iget-object v0, p0, Ldgk;->h:Ldeu;

    if-eqz v0, :cond_6

    .line 5714
    const/4 v0, 0x7

    iget-object v1, p0, Ldgk;->h:Ldeu;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 5716
    :cond_6
    iget-object v0, p0, Ldgk;->i:Ldfh;

    if-eqz v0, :cond_7

    .line 5717
    const/16 v0, 0x8

    iget-object v1, p0, Ldgk;->i:Ldfh;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 5719
    :cond_7
    iget-object v0, p0, Ldgk;->j:Lded;

    if-eqz v0, :cond_8

    .line 5720
    const/16 v0, 0x9

    iget-object v1, p0, Ldgk;->j:Lded;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 5722
    :cond_8
    iget-object v0, p0, Ldgk;->k:Ldgc;

    if-eqz v0, :cond_9

    .line 5723
    const/16 v0, 0xa

    iget-object v1, p0, Ldgk;->k:Ldgc;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 5725
    :cond_9
    iget-object v0, p0, Ldgk;->l:Ldfs;

    if-eqz v0, :cond_a

    .line 5726
    const/16 v0, 0xb

    iget-object v1, p0, Ldgk;->l:Ldfs;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 5728
    :cond_a
    iget-object v0, p0, Ldgk;->m:Ldfr;

    if-eqz v0, :cond_b

    .line 5729
    const/16 v0, 0xc

    iget-object v1, p0, Ldgk;->m:Ldfr;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 5731
    :cond_b
    iget-object v0, p0, Ldgk;->n:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    .line 5732
    const/16 v0, 0xd

    iget-object v1, p0, Ldgk;->n:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 5734
    :cond_c
    iget-object v0, p0, Ldgk;->o:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 5735
    const/16 v0, 0xe

    iget-object v1, p0, Ldgk;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 5737
    :cond_d
    iget-object v0, p0, Ldgk;->p:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    .line 5738
    const/16 v0, 0x10

    iget-object v1, p0, Ldgk;->p:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 5740
    :cond_e
    iget-object v0, p0, Ldgk;->q:Ldec;

    if-eqz v0, :cond_f

    .line 5741
    const/16 v0, 0x11

    iget-object v1, p0, Ldgk;->q:Ldec;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 5743
    :cond_f
    iget-object v0, p0, Ldgk;->r:Ldes;

    if-eqz v0, :cond_10

    .line 5744
    const/16 v0, 0x12

    iget-object v1, p0, Ldgk;->r:Ldes;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 5746
    :cond_10
    iget-object v0, p0, Ldgk;->s:Ldgc;

    if-eqz v0, :cond_11

    .line 5747
    const/16 v0, 0x13

    iget-object v1, p0, Ldgk;->s:Ldgc;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 5749
    :cond_11
    iget-object v0, p0, Ldgk;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 5751
    return-void
.end method
