.class public final Lavr;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 301
    iput-object p1, p0, Lavr;->d:Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;

    iput-object p2, p0, Lavr;->a:Ljava/lang/String;

    iput-object p3, p0, Lavr;->b:Ljava/lang/String;

    iput-object p4, p0, Lavr;->c:Ljava/lang/String;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 301
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "name"

    iget-object v0, p0, Lavr;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_1

    iget-object v0, p0, Lavr;->d:Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v3, Lh;->nG:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "mmsproxy"

    iget-object v2, p0, Lavr;->d:Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;

    iget-object v2, p0, Lavr;->d:Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;

    invoke-static {v2}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->e(Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;)Landroid/preference/EditTextPreference;

    move-result-object v2

    invoke-virtual {v2}, Landroid/preference/EditTextPreference;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "mmsport"

    iget-object v2, p0, Lavr;->d:Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;

    iget-object v2, p0, Lavr;->d:Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;

    invoke-static {v2}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->f(Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;)Landroid/preference/EditTextPreference;

    move-result-object v2

    invoke-virtual {v2}, Landroid/preference/EditTextPreference;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "mmsc"

    iget-object v2, p0, Lavr;->d:Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;

    iget-object v2, p0, Lavr;->d:Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;

    invoke-static {v2}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->g(Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;)Landroid/preference/EditTextPreference;

    move-result-object v2

    invoke-virtual {v2}, Landroid/preference/EditTextPreference;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "type"

    const-string v2, "mms"

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "mcc"

    iget-object v2, p0, Lavr;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "mnc"

    iget-object v2, p0, Lavr;->c:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "numeric"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lavr;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lavr;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lavr;->d:Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->h(Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lavr;->d:Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->i(Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lavr;->d:Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->h(Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lavr;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lavr;->d:Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->i(Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lavr;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "current"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_0
    iget-object v0, p0, Lavr;->d:Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->j(Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lavr;->d:Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->b(Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v2, "apn"

    invoke-virtual {v0, v2, v5, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    :goto_1
    return-object v5

    :cond_1
    iget-object v0, p0, Lavr;->a:Ljava/lang/String;

    goto/16 :goto_0

    :cond_2
    const-string v0, "_id =?"

    new-array v2, v4, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lavr;->d:Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;

    invoke-static {v4}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->a(Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    iget-object v3, p0, Lavr;->d:Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;

    invoke-static {v3}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->b(Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v4, "apn"

    invoke-virtual {v3, v4, v1, v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1
.end method
