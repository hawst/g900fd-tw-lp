.class public Lbhd;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final g:Z

.field private final h:Z

.field private final i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private final n:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lesv;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 4486
    invoke-direct {p0}, Lbfz;-><init>()V

    .line 4487
    iget-object v0, p1, Lesv;->c:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    iput-boolean v0, p0, Lbhd;->g:Z

    .line 4489
    iget-object v0, p1, Lesv;->d:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    iput-boolean v0, p0, Lbhd;->h:Z

    .line 4491
    iget-object v0, p1, Lesv;->h:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    iput-boolean v0, p0, Lbhd;->i:Z

    .line 4495
    iget-object v0, p1, Lesv;->i:[Letb;

    if-eqz v0, :cond_5

    iget-object v4, p1, Lesv;->i:[Letb;

    array-length v5, v4

    move v3, v1

    :goto_0
    if-ge v3, v5, :cond_5

    aget-object v0, v4, v3

    iget-object v6, v0, Letb;->b:Ljava/lang/Integer;

    invoke-static {v6, v1}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v6

    if-ne v6, v7, :cond_1

    :goto_1
    if-eqz v0, :cond_3

    iget-object v3, v0, Letb;->c:Ljava/lang/Boolean;

    invoke-static {v3, v1}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v3

    iput-boolean v3, p0, Lbhd;->j:Z

    iget-boolean v3, p0, Lbhd;->j:Z

    if-nez v3, :cond_3

    iget-object v3, v0, Letb;->d:Letc;

    if-eqz v3, :cond_3

    iget-object v3, v0, Letb;->d:Letc;

    iget-object v3, v3, Letc;->b:Ljava/lang/Boolean;

    invoke-static {v3, v1}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, v0, Letb;->d:Letc;

    iget-object v3, v3, Letc;->c:[Letd;

    if-eqz v3, :cond_3

    iput-boolean v7, p0, Lbhd;->k:Z

    iget-object v0, v0, Letb;->d:Letc;

    iget-object v3, v0, Letc;->c:[Letd;

    array-length v4, v3

    move v0, v1

    :goto_2
    if-ge v0, v4, :cond_3

    aget-object v5, v3, v0

    iget-object v5, v5, Letd;->b:Ljava/lang/Integer;

    invoke-static {v5, v1}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_2

    iput-boolean v7, p0, Lbhd;->l:Z

    :cond_0
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_2
    if-ne v5, v7, :cond_0

    iput-boolean v7, p0, Lbhd;->m:Z

    goto :goto_3

    .line 4497
    :cond_3
    iget-object v0, p1, Lesv;->b:Lesw;

    if-eqz v0, :cond_4

    .line 4498
    iget-object v0, p1, Lesv;->b:Lesw;

    iget-object v0, v0, Lesw;->d:Ljava/lang/String;

    iput-object v0, p0, Lbhd;->n:Ljava/lang/String;

    .line 4502
    :goto_4
    return-void

    .line 4500
    :cond_4
    iput-object v2, p0, Lbhd;->n:Ljava/lang/String;

    goto :goto_4

    :cond_5
    move-object v0, v2

    goto :goto_1
.end method

.method synthetic constructor <init>(Lesv;B)V
    .locals 0

    .prologue
    .line 4472
    invoke-direct {p0, p1}, Lbhd;-><init>(Lesv;)V

    return-void
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 4539
    if-eqz p0, :cond_0

    .line 4540
    new-instance v0, Lesv;

    invoke-direct {v0}, Lesv;-><init>()V

    .line 4541
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Lesv;

    .line 4542
    if-eqz v0, :cond_0

    .line 4543
    new-instance v1, Lbhd;

    invoke-direct {v1, v0}, Lbhd;-><init>(Lesv;)V

    move-object v0, v1

    .line 4547
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lyt;Lbnl;)V
    .locals 9

    .prologue
    .line 4553
    invoke-super {p0, p1, p2}, Lbfz;->a(Lyt;Lbnl;)V

    .line 4555
    invoke-virtual {p1}, Lyt;->f()Lyj;

    move-result-object v0

    invoke-virtual {p0}, Lbhd;->f()Z

    move-result v1

    .line 4556
    invoke-virtual {p0}, Lbhd;->g()Z

    move-result v2

    invoke-virtual {p0}, Lbhd;->h()Z

    move-result v3

    invoke-virtual {p0}, Lbhd;->i()Ljava/lang/String;

    move-result-object v4

    .line 4557
    invoke-virtual {p0}, Lbhd;->j()Z

    move-result v5

    invoke-virtual {p0}, Lbhd;->k()Z

    move-result v6

    invoke-virtual {p0}, Lbhd;->l()Z

    move-result v7

    .line 4558
    invoke-virtual {p0}, Lbhd;->m()Z

    move-result v8

    .line 4555
    invoke-static/range {v0 .. v8}, Lbkb;->a(Lyj;ZZZLjava/lang/String;ZZZZ)V

    .line 4559
    return-void
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 4568
    iget-boolean v0, p0, Lbhd;->g:Z

    return v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 4579
    iget-boolean v0, p0, Lbhd;->h:Z

    return v0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 4590
    iget-boolean v0, p0, Lbhd;->i:Z

    return v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4599
    iget-object v0, p0, Lbhd;->n:Ljava/lang/String;

    return-object v0
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 4608
    iget-boolean v0, p0, Lbhd;->j:Z

    return v0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 4617
    iget-boolean v0, p0, Lbhd;->k:Z

    return v0
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 4626
    iget-boolean v0, p0, Lbhd;->l:Z

    return v0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 4635
    iget-boolean v0, p0, Lbhd;->m:Z

    return v0
.end method
