.class public final Lbld;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Z

.field private static final b:[Ljava/lang/String;

.field private static volatile i:Landroid/content/Context;

.field private static volatile j:Lbld;


# instance fields
.field private c:Ljava/lang/String;

.field private volatile d:I

.field private e:J

.field private f:I

.field private volatile g:I

.field private volatile h:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 31
    sget-object v0, Lbys;->k:Lcyp;

    sput-boolean v2, Lbld;->a:Z

    .line 47
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Uninit"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string v2, "RegSent"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "Registered"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "RegFailed"

    aput-object v2, v0, v1

    sput-object v0, Lbld;->b:[Ljava/lang/String;

    .line 84
    sput-object v3, Lbld;->i:Landroid/content/Context;

    .line 102
    sput-object v3, Lbld;->j:Lbld;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput v3, p0, Lbld;->f:I

    .line 159
    invoke-static {}, Lbld;->l()Landroid/content/Context;

    move-result-object v0

    const-string v1, "gcm"

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 161
    const-string v1, "gcm_registration_id"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lbld;->c:Ljava/lang/String;

    .line 162
    const-string v1, "gcm_registration_state"

    const/16 v2, 0x64

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lbld;->d:I

    .line 163
    const-string v1, "gcm_retries"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lbld;->g:I

    .line 164
    const-string v1, "gcm_retry_strategy"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lbld;->h:I

    .line 165
    const-string v1, "gcm_android_id"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lbld;->e:J

    .line 166
    return-void
.end method

.method public static a()J
    .locals 3

    .prologue
    .line 109
    const-wide/16 v0, 0x0

    .line 110
    sget-object v2, Lbld;->j:Lbld;

    .line 111
    if-eqz v2, :cond_0

    .line 112
    monitor-enter v2

    .line 113
    :try_start_0
    iget-wide v0, v2, Lbld;->e:J

    .line 114
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 116
    :cond_0
    return-wide v0

    .line 114
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method private static a(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 466
    const/16 v0, 0x64

    if-lt p0, v0, :cond_0

    const/16 v0, 0x67

    if-gt p0, v0, :cond_0

    .line 467
    sget-object v0, Lbld;->b:[Ljava/lang/String;

    add-int/lit8 v1, p0, -0x64

    aget-object v0, v0, v1

    .line 469
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(II)V
    .locals 3

    .prologue
    .line 348
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 349
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GcmRegistration: set retryStrategy="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    :cond_0
    iput p1, p0, Lbld;->g:I

    .line 353
    iput p2, p0, Lbld;->h:I

    .line 354
    invoke-static {}, Lbld;->l()Landroid/content/Context;

    move-result-object v0

    const-string v1, "gcm"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 356
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 357
    const-string v1, "gcm_retries"

    iget v2, p0, Lbld;->g:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 358
    const-string v1, "gcm_retry_strategy"

    iget v2, p0, Lbld;->h:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 359
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 360
    return-void
.end method

.method public static a(Ljava/io/PrintWriter;)V
    .locals 3

    .prologue
    .line 481
    sget-object v0, Lbld;->j:Lbld;

    if-nez v0, :cond_0

    .line 482
    const-string v0, "GCM Registration not inited"

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 485
    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lbld;->c()Lbld;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "GCM Registration state: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v1, Lbld;->d:I

    invoke-static {v2}, Lbld;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", reg: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, v1, Lbld;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 454
    iput-object p1, p0, Lbld;->c:Ljava/lang/String;

    .line 455
    iput p2, p0, Lbld;->d:I

    .line 456
    invoke-static {}, Lbld;->l()Landroid/content/Context;

    move-result-object v0

    const-string v1, "gcm"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 458
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 459
    const-string v1, "gcm_android_id"

    iget-wide v2, p0, Lbld;->e:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 460
    const-string v1, "gcm_registration_id"

    iget-object v2, p0, Lbld;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 461
    const-string v1, "gcm_registration_state"

    iget v2, p0, Lbld;->d:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 462
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 463
    return-void
.end method

.method public static declared-synchronized b()V
    .locals 3

    .prologue
    .line 123
    const-class v1, Lbld;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lbld;->a:Z

    if-eqz v0, :cond_0

    .line 124
    const-string v0, "Babel"

    const-string v2, "Initializing GcmRegistration"

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    :cond_0
    sget-object v0, Lbld;->j:Lbld;

    if-eqz v0, :cond_1

    .line 130
    const-string v0, "Babel"

    const-string v2, "GcmRegistration.initialize() called twice"

    invoke-static {v0, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 138
    :goto_0
    monitor-exit v1

    return-void

    .line 134
    :cond_1
    :try_start_1
    new-instance v2, Lbld;

    invoke-direct {v2}, Lbld;-><init>()V

    .line 135
    sput-object v2, Lbld;->j:Lbld;

    monitor-enter v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 137
    :try_start_2
    sget-object v0, Lbld;->j:Lbld;

    invoke-direct {v0}, Lbld;->m()Z

    .line 138
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 123
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static c()Lbld;
    .locals 2

    .prologue
    .line 147
    sget-object v0, Lbld;->j:Lbld;

    if-nez v0, :cond_0

    .line 148
    const-string v0, "Babel"

    const-string v1, "GcmRegistration.initialize() should be called called first"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    :cond_0
    sget-object v0, Lbld;->j:Lbld;

    return-object v0
.end method

.method public static j()V
    .locals 3

    .prologue
    .line 440
    invoke-static {}, Lbld;->c()Lbld;

    move-result-object v1

    .line 441
    monitor-enter v1

    .line 443
    :try_start_0
    iget v0, v1, Lbld;->d:I

    const/16 v2, 0x66

    if-ne v0, v2, :cond_0

    .line 444
    const-string v0, "Babel"

    const-string v2, "GcmRegistration: Forcing re-registration"

    invoke-static {v0, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    const/4 v0, 0x0

    const/16 v2, 0x64

    invoke-direct {v1, v0, v2}, Lbld;->a(Ljava/lang/String;I)V

    .line 448
    invoke-direct {v1}, Lbld;->m()Z

    .line 450
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static k()V
    .locals 6

    .prologue
    .line 490
    invoke-static {}, Lbld;->c()Lbld;

    move-result-object v1

    .line 491
    monitor-enter v1

    .line 492
    :try_start_0
    iget v0, v1, Lbld;->d:I

    const/16 v2, 0x66

    if-ne v0, v2, :cond_0

    iget-wide v2, v1, Lbld;->e:J

    .line 493
    invoke-static {}, Lbxp;->a()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    .line 494
    const-string v0, "Babel"

    const-string v2, "Android Id mismatch. Force new GcmRegistration"

    invoke-static {v0, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 495
    const/4 v0, 0x0

    const/16 v2, 0x64

    invoke-direct {v1, v0, v2}, Lbld;->a(Ljava/lang/String;I)V

    .line 496
    invoke-direct {v1}, Lbld;->m()Z

    .line 498
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static l()Landroid/content/Context;
    .locals 1

    .prologue
    .line 95
    sget-object v0, Lbld;->i:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 96
    sget-object v0, Lbld;->i:Landroid/content/Context;

    .line 99
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    goto :goto_0
.end method

.method private m()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 411
    const-string v2, "Babel"

    const-string v3, "GcmRegistration: Checking GCM registration"

    invoke-static {v2, v3}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    iget v2, p0, Lbld;->d:I

    const/16 v3, 0x66

    if-eq v2, v3, :cond_1

    .line 415
    const-string v2, "Babel"

    const-string v3, "GcmRegistration: Requesting GCM registration"

    invoke-static {v2, v3}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    invoke-static {}, Lbkb;->d()V

    .line 418
    invoke-static {}, Lbkb;->s()V

    .line 421
    invoke-static {}, Lbkb;->u()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 424
    const/4 v0, 0x2

    .line 426
    :cond_0
    invoke-direct {p0, v1, v0}, Lbld;->a(II)V

    .line 428
    invoke-static {}, Lbxp;->a()J

    move-result-wide v2

    iput-wide v2, p0, Lbld;->e:J

    .line 429
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/GcmIntentService;->b()V

    .line 430
    const/16 v0, 0x65

    iput v0, p0, Lbld;->d:I

    move v0, v1

    .line 435
    :cond_1
    return v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 212
    const/4 v0, 0x1

    .line 213
    monitor-enter p0

    .line 214
    :try_start_0
    iget v2, p0, Lbld;->d:I

    const/16 v3, 0x65

    if-eq v2, v3, :cond_0

    .line 215
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Setting GCM registration. Expected,Actual state=101,"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lbld;->d:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    :cond_0
    invoke-static {}, Lbxp;->a()J

    move-result-wide v2

    .line 220
    iget-wide v4, p0, Lbld;->e:J

    cmp-long v2, v4, v2

    if-nez v2, :cond_1

    .line 221
    const/16 v1, 0x66

    invoke-direct {p0, p1, v1}, Lbld;->a(Ljava/lang/String;I)V

    .line 222
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lbld;->a(II)V

    .line 227
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 230
    if-nez v0, :cond_2

    .line 231
    const-string v0, "Babel"

    const-string v1, "Ignoring GCM registration due to android_id mismatch. Retrying"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    invoke-virtual {p0}, Lbld;->i()V

    .line 238
    :goto_1
    return-void

    .line 224
    :cond_1
    const/4 v0, 0x0

    const/16 v2, 0x67

    :try_start_1
    invoke-direct {p0, v0, v2}, Lbld;->a(Ljava/lang/String;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 225
    goto :goto_0

    .line 227
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 235
    :cond_2
    invoke-static {}, Lbkb;->q()V

    .line 236
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->i()V

    goto :goto_1
.end method

.method public d()Z
    .locals 2

    .prologue
    .line 174
    monitor-enter p0

    .line 175
    :try_start_0
    iget v0, p0, Lbld;->d:I

    const/16 v1, 0x66

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 176
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 185
    monitor-enter p0

    .line 186
    :try_start_0
    iget v0, p0, Lbld;->d:I

    const/16 v1, 0x67

    if-eq v0, v1, :cond_0

    iget v0, p0, Lbld;->d:I

    const/16 v1, 0x64

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 187
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public f()Ljava/lang/String;
    .locals 3

    .prologue
    .line 194
    monitor-enter p0

    .line 195
    :try_start_0
    iget v0, p0, Lbld;->d:I

    const/16 v1, 0x66

    if-eq v0, v1, :cond_0

    .line 196
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Getting GCM registcd pration. Expected,Actual state=102,"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lbld;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    const/4 v0, 0x0

    monitor-exit p0

    .line 201
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lbld;->c:Ljava/lang/String;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 202
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public g()V
    .locals 2

    .prologue
    .line 246
    const-string v0, "Babel"

    const-string v1, "Resetting GCM"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    monitor-enter p0

    .line 248
    :try_start_0
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/GcmIntentService;->c()V

    .line 249
    const/4 v0, 0x0

    const/16 v1, 0x64

    invoke-direct {p0, v0, v1}, Lbld;->a(Ljava/lang/String;I)V

    .line 250
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lbld;->a(II)V

    .line 251
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public h()V
    .locals 2

    .prologue
    .line 335
    monitor-enter p0

    .line 337
    :try_start_0
    iget v0, p0, Lbld;->h:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 338
    iget v0, p0, Lbld;->g:I

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lbld;->a(II)V

    .line 339
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->i()V

    .line 341
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public i()V
    .locals 4

    .prologue
    const/16 v3, 0x65

    .line 392
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 393
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Retry GcmRegistration. Current state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lbld;->d:I

    invoke-static {v2}, Lbld;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    :cond_0
    monitor-enter p0

    .line 398
    :try_start_0
    iget v0, p0, Lbld;->d:I

    if-eq v0, v3, :cond_1

    .line 399
    const/16 v0, 0x65

    iput v0, p0, Lbld;->d:I

    .line 400
    invoke-static {}, Lbxp;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lbld;->e:J

    .line 401
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/GcmIntentService;->b()V

    .line 403
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
