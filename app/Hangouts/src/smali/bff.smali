.class public Lbff;
.super Lbfq;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:J

.field public final h:Ljava/lang/String;

.field public final i:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;JJ)V
    .locals 0

    .prologue
    .line 1029
    invoke-direct {p0, p2, p1, p10, p11}, Lbfq;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    .line 1030
    iput-object p3, p0, Lbff;->a:Ljava/lang/String;

    .line 1031
    iput-object p4, p0, Lbff;->b:Ljava/lang/String;

    .line 1032
    iput-wide p5, p0, Lbff;->c:J

    .line 1033
    iput-object p7, p0, Lbff;->h:Ljava/lang/String;

    .line 1034
    iput-wide p8, p0, Lbff;->i:J

    .line 1035
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lbpp;Lbpk;)Lbfz;
    .locals 8

    .prologue
    const/16 v7, 0x75

    const/4 v4, 0x0

    .line 1061
    :try_start_0
    iget-object v1, p0, Lbff;->a:Ljava/lang/String;

    iget-object v2, p0, Lbff;->b:Ljava/lang/String;

    iget-object v3, p0, Lbff;->h:Ljava/lang/String;

    .line 1065
    invoke-static {}, Lbvm;->a()Lsm;

    move-result-object v0

    invoke-virtual {v0}, Lsm;->r()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    iget-wide v5, p0, Lbff;->i:J

    move-object v0, p1

    .line 1061
    invoke-static/range {v0 .. v6}, Lbwd;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ)Lbwe;

    move-result-object v0

    .line 1067
    invoke-virtual {v0}, Lbwe;->a()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1069
    invoke-virtual {v0}, Lbwe;->b()I

    move-result v0

    .line 1070
    packed-switch v0, :pswitch_data_0

    .line 1108
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 1065
    :cond_0
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const-string v5, "smsmms"

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sget v5, Lh;->lo:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget v6, Lf;->bT:I

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    invoke-interface {v0, v5, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    goto :goto_0

    .line 1073
    :pswitch_0
    iget-object v1, p0, Lbff;->a:Ljava/lang/String;

    iget-object v2, p0, Lbff;->b:Ljava/lang/String;

    iget-wide v3, p0, Lbff;->p:J

    const-wide/16 v5, 0x3e8

    div-long/2addr v3, v5

    iget-wide v5, p0, Lbff;->c:J

    move-object v0, p1

    invoke-static/range {v0 .. v6}, Lbvx;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JJ)Landroid/net/Uri;

    move-result-object v1

    .line 1079
    if-eqz v1, :cond_1

    .line 1082
    const/4 v0, 0x0

    .line 1083
    invoke-static {v1}, Lbvx;->c(Landroid/net/Uri;)J

    move-result-wide v2

    .line 1082
    invoke-static {v0, v2, v3}, Lbwf;->a(IJ)V

    .line 1087
    :goto_2
    new-instance v0, Lbhz;

    iget-wide v2, p0, Lbff;->p:J

    invoke-direct {v0, v1, v2, v3}, Lbhz;-><init>(Landroid/net/Uri;J)V
    :try_end_0
    .catch Lbwa; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1107
    :catch_0
    move-exception v0

    .line 1104
    const-string v1, "Babel_RequestWriter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SendSmsRequest: failed to send message "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1105
    new-instance v1, Lbph;

    invoke-direct {v1, v7, v0}, Lbph;-><init>(ILjava/lang/Exception;)V

    throw v1

    .line 1085
    :cond_1
    :try_start_1
    const-string v0, "Babel_RequestWriter"

    const-string v2, "SendSmsRequest: sms provider returning null"

    invoke-static {v0, v2}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1089
    :pswitch_1
    const-string v0, "Babel_RequestWriter"

    const-string v1, "SendSmsRequest: temporary failure"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 1090
    new-instance v0, Lbph;

    const/16 v1, 0x74

    invoke-direct {v0, v1}, Lbph;-><init>(I)V

    throw v0

    .line 1093
    :pswitch_2
    const-string v0, "Babel_RequestWriter"

    const-string v1, "SendSmsRequest: permanent failure"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 1094
    new-instance v0, Lbph;

    const/16 v1, 0x75

    invoke-direct {v0, v1}, Lbph;-><init>(I)V

    throw v0

    .line 1098
    :cond_2
    const-string v0, "Babel_RequestWriter"

    const-string v1, "SendSmsRequest: sending timed out"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 1100
    new-instance v0, Lbph;

    const/16 v1, 0x75

    invoke-direct {v0, v1}, Lbph;-><init>(I)V

    throw v0
    :try_end_1
    .catch Lbwa; {:try_start_1 .. :try_end_1} :catch_0

    .line 1070
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(JI)Z
    .locals 1

    .prologue
    .line 1113
    const/4 v0, 0x3

    if-ge p3, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1041
    const-string v0, "sms_queue"

    return-object v0
.end method
