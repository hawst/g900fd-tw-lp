.class final Lk;
.super Landroid/graphics/drawable/InsetDrawable;
.source "PG"

# interfaces
.implements Landroid/graphics/drawable/Drawable$Callback;


# instance fields
.field final synthetic a:Le;

.field private final b:Z

.field private final c:Landroid/graphics/Rect;

.field private d:F

.field private e:F


# direct methods
.method private constructor <init>(Le;Landroid/graphics/drawable/Drawable;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 420
    iput-object p1, p0, Lk;->a:Le;

    .line 421
    invoke-direct {p0, p2, v0}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 414
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-le v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lk;->b:Z

    .line 415
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lk;->c:Landroid/graphics/Rect;

    .line 422
    return-void
.end method

.method synthetic constructor <init>(Le;Landroid/graphics/drawable/Drawable;B)V
    .locals 0

    .prologue
    .line 413
    invoke-direct {p0, p1, p2}, Lk;-><init>(Le;Landroid/graphics/drawable/Drawable;)V

    return-void
.end method


# virtual methods
.method public a()F
    .locals 1

    .prologue
    .line 435
    iget v0, p0, Lk;->d:F

    return v0
.end method

.method public a(F)V
    .locals 0

    .prologue
    .line 430
    iput p1, p0, Lk;->d:F

    .line 431
    invoke-virtual {p0}, Lk;->invalidateSelf()V

    .line 432
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 446
    const v0, 0x3eaaaaab

    iput v0, p0, Lk;->e:F

    .line 447
    invoke-virtual {p0}, Lk;->invalidateSelf()V

    .line 448
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x1

    .line 452
    iget-object v1, p0, Lk;->c:Landroid/graphics/Rect;

    invoke-virtual {p0, v1}, Lk;->copyBounds(Landroid/graphics/Rect;)V

    .line 453
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 456
    iget-object v1, p0, Lk;->a:Le;

    invoke-static {v1}, Le;->a(Le;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Lgz;->d(Landroid/view/View;)I

    move-result v1

    if-ne v1, v0, :cond_2

    move v1, v0

    .line 458
    :goto_0
    if-eqz v1, :cond_0

    const/4 v0, -0x1

    .line 459
    :cond_0
    iget-object v2, p0, Lk;->c:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    .line 460
    iget v3, p0, Lk;->e:F

    neg-float v3, v3

    int-to-float v4, v2

    mul-float/2addr v3, v4

    iget v4, p0, Lk;->d:F

    mul-float/2addr v3, v4

    int-to-float v0, v0

    mul-float/2addr v0, v3

    invoke-virtual {p1, v0, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 463
    if-eqz v1, :cond_1

    iget-boolean v0, p0, Lk;->b:Z

    if-nez v0, :cond_1

    .line 464
    int-to-float v0, v2

    invoke-virtual {p1, v0, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 465
    const/high16 v0, -0x40800000    # -1.0f

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    .line 468
    :cond_1
    invoke-super {p0, p1}, Landroid/graphics/drawable/InsetDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 469
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 470
    return-void

    .line 456
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method
