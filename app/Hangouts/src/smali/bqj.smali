.class public final Lbqj;
.super Lbmu;
.source "PG"


# static fields
.field private static final a:Z

.field private static final d:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lbqj;",
            ">;"
        }
    .end annotation
.end field

.field private static e:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static f:Z


# instance fields
.field private g:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 22
    sget-object v0, Lbys;->k:Lcyp;

    sput-boolean v1, Lbqj;->a:Z

    .line 27
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lbqj;->d:Ljava/util/concurrent/ConcurrentHashMap;

    .line 34
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lbqj;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 71
    invoke-static {}, Lbqj;->l()V

    .line 73
    new-instance v0, Lbqk;

    invoke-direct {v0}, Lbqk;-><init>()V

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/Runnable;)V

    .line 79
    return-void
.end method

.method private constructor <init>(Lyj;JJ)V
    .locals 0

    .prologue
    .line 82
    invoke-direct/range {p0 .. p5}, Lbmu;-><init>(Lyj;JJ)V

    .line 83
    return-void
.end method

.method public static a(Lyj;)Lbqj;
    .locals 7

    .prologue
    .line 40
    invoke-virtual {p0}, Lyj;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 41
    const/4 v0, 0x0

    .line 58
    :cond_0
    :goto_0
    return-object v0

    .line 43
    :cond_1
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v6

    .line 44
    sget-object v0, Lbqj;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v6}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqj;

    .line 45
    if-nez v0, :cond_0

    .line 46
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "babel_tickle_gcm_lowmark_seconds"

    const/16 v3, 0x10e

    invoke-static {v1, v2, v3}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v2, v1

    .line 50
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "babel_tickle_gcm_highmark_seconds"

    const/16 v4, 0x258

    invoke-static {v0, v1, v4}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v4, v0

    .line 54
    new-instance v0, Lbqj;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lbqj;-><init>(Lyj;JJ)V

    .line 55
    sget-object v1, Lbqj;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, v6, v0}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object v0, Lbqj;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v6}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqj;

    goto :goto_0
.end method

.method static synthetic k()V
    .locals 0

    .prologue
    .line 21
    invoke-static {}, Lbqj;->l()V

    return-void
.end method

.method private static l()V
    .locals 2

    .prologue
    .line 66
    const-string v0, "babel_tickle_gcm_enabled2"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lbqj;->f:Z

    .line 68
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 107
    iget-object v0, p0, Lbqj;->b:Lyj;

    invoke-virtual {v0}, Lyj;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 114
    :cond_1
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcjh;->a(Landroid/content/Context;)Lcjh;

    move-result-object v0

    .line 116
    :try_start_0
    const-string v1, "hangouts@google.com"

    sget-object v2, Lbqj;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0, v1, v2, v3}, Lcjh;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 119
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lbqj;->a(I)V

    .line 120
    sget-boolean v0, Lbqj;->a:Z

    if-eqz v0, :cond_0

    .line 121
    const-string v0, "Babel"

    const-string v1, "gcm tickle sent"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 123
    :catch_0
    move-exception v0

    .line 124
    const-string v1, "Babel"

    const-string v2, "Unable to tickle gcm"

    invoke-static {v1, v2, v0}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected a(J)V
    .locals 0

    .prologue
    .line 92
    iput-wide p1, p0, Lbqj;->g:J

    .line 93
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 97
    sget-boolean v0, Lbqj;->f:Z

    if-eqz v0, :cond_0

    invoke-super {p0}, Lbmu;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 102
    sget-boolean v0, Lbqj;->f:Z

    if-eqz v0, :cond_0

    invoke-super {p0}, Lbmu;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected i()J
    .locals 2

    .prologue
    .line 87
    iget-wide v0, p0, Lbqj;->g:J

    return-wide v0
.end method
