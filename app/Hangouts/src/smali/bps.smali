.class public final Lbps;
.super Lbnj;
.source "PG"


# static fields
.field private static final a:Z


# instance fields
.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:I

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/Long;

.field private final j:Z

.field private k:I

.field private l:I

.field private m:I

.field private n:Ljava/lang/String;

.field private o:Z

.field private p:Lcom/google/android/gms/maps/model/MarkerOptions;

.field private q:Lcom/google/android/gms/maps/model/CameraPosition;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:I

.field private u:J

.field private final v:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lbys;->k:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lbps;->a:Z

    return-void
.end method

.method public constructor <init>(Lyj;Ljava/lang/String;JJ)V
    .locals 4

    .prologue
    .line 127
    invoke-direct {p0, p1}, Lbnj;-><init>(Lyj;)V

    .line 93
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lbps;->v:J

    .line 129
    const-wide/16 v0, -0x1

    cmp-long v0, p3, v0

    if-eqz v0, :cond_0

    .line 130
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lbps;->i:Ljava/lang/Long;

    .line 132
    :cond_0
    iput-object p2, p0, Lbps;->d:Ljava/lang/String;

    .line 133
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbps;->j:Z

    .line 134
    iput-wide p5, p0, Lbps;->u:J

    .line 135
    return-void
.end method

.method public constructor <init>(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lbnj;-><init>(Lyj;)V

    .line 93
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lbps;->v:J

    .line 143
    iput-object p2, p0, Lbps;->d:Ljava/lang/String;

    .line 144
    iput-object p3, p0, Lbps;->s:Ljava/lang/String;

    .line 145
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbps;->j:Z

    .line 146
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbps;->o:Z

    .line 147
    iput p5, p0, Lbps;->t:I

    .line 148
    iput-object p4, p0, Lbps;->n:Ljava/lang/String;

    .line 149
    return-void
.end method

.method public constructor <init>(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IILjava/lang/String;Ljava/lang/String;ZLcom/google/android/gms/maps/model/MarkerOptions;Lcom/google/android/gms/maps/model/CameraPosition;JI)V
    .locals 6

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lbnj;-><init>(Lyj;)V

    .line 93
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    iput-wide v2, p0, Lbps;->v:J

    .line 105
    iput-object p2, p0, Lbps;->d:Ljava/lang/String;

    .line 106
    iput-object p3, p0, Lbps;->e:Ljava/lang/String;

    .line 107
    iput-object p4, p0, Lbps;->f:Ljava/lang/String;

    .line 108
    iput p5, p0, Lbps;->g:I

    .line 109
    iput-object p6, p0, Lbps;->h:Ljava/lang/String;

    .line 110
    iput p7, p0, Lbps;->l:I

    .line 111
    iput p8, p0, Lbps;->m:I

    .line 112
    iput-object p9, p0, Lbps;->r:Ljava/lang/String;

    .line 113
    move-object/from16 v0, p10

    iput-object v0, p0, Lbps;->n:Ljava/lang/String;

    .line 114
    const/4 v2, 0x0

    iput-boolean v2, p0, Lbps;->j:Z

    .line 115
    move/from16 v0, p11

    iput-boolean v0, p0, Lbps;->o:Z

    .line 116
    move-object/from16 v0, p12

    iput-object v0, p0, Lbps;->p:Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 117
    move-object/from16 v0, p13

    iput-object v0, p0, Lbps;->q:Lcom/google/android/gms/maps/model/CameraPosition;

    .line 118
    move-wide/from16 v0, p14

    iput-wide v0, p0, Lbps;->u:J

    .line 119
    move/from16 v0, p16

    iput v0, p0, Lbps;->k:I

    .line 120
    return-void
.end method

.method private a(Lyt;)J
    .locals 27

    .prologue
    .line 314
    invoke-virtual/range {p1 .. p1}, Lyt;->a()V

    .line 315
    :try_start_0
    invoke-static {}, Lyt;->h()Ljava/lang/String;

    move-result-object v6

    .line 318
    move-object/from16 v0, p0

    iget-object v1, v0, Lbps;->d:Ljava/lang/String;

    const/16 v2, 0x3e8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Lyt;->b(Ljava/lang/String;I)J

    move-result-wide v4

    .line 321
    move-object/from16 v0, p0

    iget-object v1, v0, Lbps;->s:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 328
    move-object/from16 v0, p0

    iget v1, v0, Lbps;->t:I

    if-lez v1, :cond_2

    move-object/from16 v0, p0

    iget v1, v0, Lbps;->t:I

    .line 329
    :goto_0
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lf;->hs:I

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    .line 332
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    .line 330
    invoke-virtual {v2, v3, v1, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lbps;->e:Ljava/lang/String;

    .line 338
    :cond_0
    :goto_1
    invoke-direct/range {p0 .. p0}, Lbps;->d()Ljava/util/List;

    move-result-object v9

    .line 345
    new-instance v1, Lbqs;

    move-object/from16 v0, p0

    iget-object v2, v0, Lbps;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbps;->b:Lyj;

    .line 347
    invoke-virtual {v3}, Lyj;->c()Lbdk;

    move-result-object v3

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lbps;->e:Ljava/lang/String;

    const/4 v10, 0x0

    const/4 v11, 0x1

    const/4 v12, 0x0

    const/4 v13, 0x3

    const/4 v14, 0x0

    const/4 v15, 0x0

    const-wide/16 v16, 0x0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lbps;->o:Z

    move/from16 v18, v0

    if-eqz v18, :cond_4

    const/16 v18, 0x81

    :goto_2
    const-wide/16 v19, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbps;->n:Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x6

    move-object/from16 v0, p0

    iget-object v0, v0, Lbps;->s:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lbps;->t:I

    move/from16 v24, v0

    const-wide/16 v25, 0x0

    invoke-direct/range {v1 .. v26}, Lbqs;-><init>(Ljava/lang/String;Lbdk;JLjava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;ZIIILjava/lang/String;Ljava/lang/String;JIJLjava/lang/String;ILjava/lang/String;IJ)V

    .line 373
    move-object/from16 v0, p0

    iget-object v2, v0, Lbps;->d:Ljava/lang/String;

    .line 374
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lyp;->f(Lyt;Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 375
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lbps;->o:Z

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :goto_3
    invoke-virtual {v1, v2}, Lbqs;->a(I)Lbqs;

    .line 377
    invoke-virtual {v1}, Lbqs;->b()V

    .line 378
    const-string v2, ","

    .line 379
    invoke-static {v2, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    .line 378
    invoke-virtual {v1, v2}, Lbqs;->b(Ljava/lang/String;)Lbqs;

    .line 381
    move-object/from16 v0, p0

    iget-object v2, v0, Lbps;->c:Lbnl;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0, v2}, Lbqs;->a(Lyt;Lbnl;)V

    .line 382
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lbps;->o:Z

    if-eqz v2, :cond_7

    .line 383
    move-object/from16 v0, p0

    iget-object v2, v0, Lbps;->s:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 385
    new-instance v7, Lbfc;

    move-object/from16 v0, p0

    iget-object v9, v0, Lbps;->d:Ljava/lang/String;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    .line 388
    invoke-interface {v3, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lbps;->n:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lbps;->s:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v13, v0, Lbps;->t:I

    .line 392
    invoke-virtual {v1}, Lbqs;->a()J

    move-result-wide v14

    move-object v8, v6

    move-wide/from16 v16, v4

    invoke-direct/range {v7 .. v17}, Lbfc;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJ)V

    .line 411
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lbps;->c:Lbnl;

    invoke-virtual {v2, v7}, Lbnl;->a(Lbea;)V

    .line 427
    :cond_1
    invoke-virtual {v1}, Lbqs;->a()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 428
    invoke-virtual/range {p1 .. p1}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 430
    invoke-virtual/range {p1 .. p1}, Lyt;->c()V

    .line 432
    move-object/from16 v0, p0

    iget-object v2, v0, Lbps;->d:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lyp;->d(Lyt;Ljava/lang/String;)V

    .line 433
    if-nez v1, :cond_8

    const-wide/16 v1, -0x1

    :goto_5
    return-wide v1

    .line 328
    :cond_2
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 333
    :cond_3
    :try_start_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lbps;->p:Lcom/google/android/gms/maps/model/MarkerOptions;

    if-eqz v1, :cond_0

    .line 335
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lbps;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lbps;->p:Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-static {v2}, Lbps;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lbps;->e:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 430
    :catchall_0
    move-exception v1

    invoke-virtual/range {p1 .. p1}, Lyt;->c()V

    throw v1

    .line 347
    :cond_4
    const/16 v18, 0x0

    goto/16 :goto_2

    .line 375
    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 398
    :cond_6
    :try_start_2
    new-instance v7, Lbfc;

    move-object/from16 v0, p0

    iget-object v9, v0, Lbps;->d:Ljava/lang/String;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    .line 401
    invoke-interface {v3, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lbps;->n:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lbps;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lbps;->f:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, Lbps;->r:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v15, v0, Lbps;->l:I

    move-object/from16 v0, p0

    iget v0, v0, Lbps;->m:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lbps;->g:I

    move/from16 v17, v0

    move-object v8, v6

    move-wide/from16 v18, v4

    invoke-direct/range {v7 .. v19}, Lbfc;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIJ)V

    goto/16 :goto_4

    .line 416
    :cond_7
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v3}, Lbvx;->a(Landroid/content/Context;Ljava/util/List;)J

    move-result-wide v12

    .line 417
    move-object/from16 v0, p0

    iget-object v2, v0, Lbps;->d:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lyt;->Z(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 419
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 420
    move-object/from16 v0, p0

    iget-object v3, v0, Lbps;->c:Lbnl;

    new-instance v7, Lbff;

    move-object/from16 v0, p0

    iget-object v9, v0, Lbps;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lbps;->e:Ljava/lang/String;

    .line 422
    invoke-virtual {v1}, Lbqs;->a()J

    move-result-wide v15

    move-object v8, v6

    move-wide/from16 v17, v4

    invoke-direct/range {v7 .. v18}, Lbff;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;JJ)V

    .line 420
    invoke-virtual {v3, v7}, Lbnl;->a(Lbea;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_6

    .line 433
    :cond_8
    invoke-static {v1}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v1

    goto/16 :goto_5
.end method

.method private a(Lyt;ILjava/lang/String;)J
    .locals 49

    .prologue
    .line 438
    invoke-virtual/range {p1 .. p1}, Lyt;->a()V

    .line 439
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long v47, v2, v4

    .line 440
    invoke-static {}, Lyt;->h()Ljava/lang/String;

    move-result-object v7

    .line 441
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lbps;->d:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lyt;->e(Ljava/lang/String;)Lyv;

    move-result-object v2

    .line 445
    if-nez v2, :cond_2

    .line 449
    move-object/from16 v0, p0

    iget-object v3, v0, Lbps;->d:Ljava/lang/String;

    invoke-static {v3}, Lyt;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 450
    move-object/from16 v0, p0

    iget-object v3, v0, Lbps;->d:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 451
    if-eqz v3, :cond_1

    .line 452
    move-object/from16 v0, p0

    iput-object v3, v0, Lbps;->d:Ljava/lang/String;

    .line 453
    sget-boolean v2, Lbps;->a:Z

    if-eqz v2, :cond_0

    .line 454
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sendMessageLocally conversationId changed: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lbps;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lbps;->d:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lyt;->e(Ljava/lang/String;)Lyv;

    move-result-object v2

    .line 466
    :cond_1
    if-nez v2, :cond_2

    .line 467
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to find conversation: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lbps;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    invoke-virtual/range {p1 .. p1}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 469
    invoke-virtual/range {p1 .. p1}, Lyt;->c()V

    const-wide/16 v2, -0x1

    .line 629
    :goto_0
    return-wide v2

    :cond_2
    move-object/from16 v46, v2

    .line 473
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lbps;->d:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lyt;->Q(Ljava/lang/String;)J

    move-result-wide v5

    .line 475
    move-object/from16 v0, p0

    iget-object v2, v0, Lbps;->e:Ljava/lang/String;

    invoke-static {v2}, Lf;->h(Ljava/lang/String;)Ljava/util/List;

    move-result-object v8

    .line 476
    invoke-direct/range {p0 .. p0}, Lbps;->d()Ljava/util/List;

    move-result-object v10

    .line 479
    move-object/from16 v0, p0

    iget v2, v0, Lbps;->k:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_5

    .line 480
    const/4 v11, 0x1

    .line 489
    :goto_1
    new-instance v2, Lbqs;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbps;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lbps;->b:Lyj;

    .line 491
    invoke-virtual {v4}, Lyj;->c()Lbdk;

    move-result-object v4

    const/4 v9, 0x0

    const/4 v12, 0x1

    const/4 v13, 0x0

    const/16 v16, 0x0

    const-wide/16 v17, 0x0

    const/16 v19, 0x0

    const-wide/16 v20, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbps;->n:Ljava/lang/String;

    move-object/from16 v22, v0

    const/16 v23, 0x6

    const/16 v24, 0x0

    const/16 v25, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lbps;->u:J

    move-wide/from16 v26, v0

    move/from16 v14, p2

    move-object/from16 v15, p3

    invoke-direct/range {v2 .. v27}, Lbqs;-><init>(Ljava/lang/String;Lbdk;JLjava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;ZIIILjava/lang/String;Ljava/lang/String;JIJLjava/lang/String;ILjava/lang/String;IJ)V

    .line 519
    move-object/from16 v0, p0

    iget-object v3, v0, Lbps;->c:Lbnl;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0, v3}, Lbqs;->a(Lyt;Lbnl;)V

    .line 521
    const/16 v26, 0x0

    .line 522
    const-wide/16 v27, 0x0

    .line 523
    const-wide/16 v29, 0x0

    .line 524
    const/16 v21, 0x0

    .line 525
    const/16 v31, 0x0

    .line 526
    move-object/from16 v0, p0

    iget-object v3, v0, Lbps;->p:Lcom/google/android/gms/maps/model/MarkerOptions;

    if-eqz v3, :cond_3

    .line 530
    move-object/from16 v0, p0

    iget-object v3, v0, Lbps;->p:Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-virtual {v3}, Lcom/google/android/gms/maps/model/MarkerOptions;->d()Ljava/lang/String;

    move-result-object v26

    .line 531
    move-object/from16 v0, p0

    iget-object v3, v0, Lbps;->p:Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-virtual {v3}, Lcom/google/android/gms/maps/model/MarkerOptions;->c()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v3

    iget-wide v0, v3, Lcom/google/android/gms/maps/model/LatLng;->a:D

    move-wide/from16 v27, v0

    .line 532
    move-object/from16 v0, p0

    iget-object v3, v0, Lbps;->p:Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-virtual {v3}, Lcom/google/android/gms/maps/model/MarkerOptions;->c()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v3

    iget-wide v0, v3, Lcom/google/android/gms/maps/model/LatLng;->b:D

    move-wide/from16 v29, v0

    .line 533
    move-object/from16 v0, p0

    iget-object v3, v0, Lbps;->p:Lcom/google/android/gms/maps/model/MarkerOptions;

    move-object/from16 v0, p0

    iget-object v4, v0, Lbps;->q:Lcom/google/android/gms/maps/model/CameraPosition;

    invoke-static {v3, v4}, Lbps;->a(Lcom/google/android/gms/maps/model/MarkerOptions;Lcom/google/android/gms/maps/model/CameraPosition;)Ljava/lang/String;

    move-result-object v21

    .line 534
    move-object/from16 v0, p0

    iget-object v3, v0, Lbps;->p:Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-static {v3}, Lbps;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)Ljava/lang/String;

    move-result-object v31

    .line 537
    :cond_3
    const/16 v34, -0x1

    .line 538
    invoke-static/range {p2 .. p2}, Lf;->c(I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 539
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lbps;->o:Z

    if-eqz v3, :cond_7

    const/4 v3, 0x1

    :goto_2
    move/from16 v34, v3

    .line 542
    :cond_4
    new-instance v8, Lzg;

    move-object/from16 v0, p0

    iget-object v10, v0, Lbps;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbps;->b:Lyj;

    .line 545
    invoke-virtual {v3}, Lyj;->c()Lbdk;

    move-result-object v3

    iget-object v3, v3, Lbdk;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lbps;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lbps;->e:Ljava/lang/String;

    const/4 v14, 0x1

    const/4 v15, 0x1

    if-eqz v11, :cond_8

    const-wide v18, 0x7fffffffffffffffL

    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lbps;->f:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbps;->h:Ljava/lang/String;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    const/16 v24, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbps;->r:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lbps;->l:I

    move/from16 v35, v0

    move-object/from16 v0, p0

    iget v0, v0, Lbps;->m:I

    move/from16 v36, v0

    const/16 v37, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lbps;->g:I

    move/from16 v38, v0

    const/16 v39, 0x0

    const/16 v40, 0x0

    const/16 v41, 0x0

    const/16 v42, 0x0

    const/16 v43, 0x0

    const/16 v44, 0x0

    const/16 v45, 0x0

    move-object v9, v7

    move-object v11, v3

    move-wide/from16 v16, v5

    move/from16 v32, p2

    move-object/from16 v33, p3

    invoke-direct/range {v8 .. v45}, Lzg;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;DDLjava/lang/String;ILjava/lang/String;IIILjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 576
    move-object/from16 v0, p0

    iget-object v3, v0, Lbps;->c:Lbnl;

    move-object/from16 v0, p1

    move-object/from16 v1, v46

    invoke-static {v0, v3, v8, v1}, Lbps;->a(Lyt;Lbnl;Lzg;Lyv;)V

    .line 577
    invoke-virtual {v2}, Lbqs;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    .line 578
    invoke-virtual/range {p1 .. p1}, Lyt;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 580
    invoke-virtual/range {p1 .. p1}, Lyt;->c()V

    .line 583
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long v15, v2, v4

    .line 586
    invoke-static {v7}, Lyt;->d(Ljava/lang/String;)J

    move-result-wide v10

    .line 587
    move-object/from16 v0, p0

    iget-object v2, v0, Lbps;->b:Lyj;

    move-object/from16 v0, p0

    iget-wide v3, v0, Lbps;->u:J

    const/16 v5, 0xb

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Lbps;->d:Ljava/lang/String;

    const/4 v13, 0x0

    invoke-static/range {v2 .. v13}, Lf;->a(Lyj;JIILjava/lang/String;JJLjava/lang/String;Lyu;)V

    .line 597
    move-object/from16 v0, p0

    iget-object v2, v0, Lbps;->b:Lyj;

    move-object/from16 v0, p0

    iget-wide v3, v0, Lbps;->v:J

    const/16 v5, 0xa

    const/16 v6, 0xc9

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Lbps;->d:Ljava/lang/String;

    const/4 v13, 0x0

    invoke-static/range {v2 .. v13}, Lf;->a(Lyj;JIILjava/lang/String;JJLjava/lang/String;Lyu;)V

    .line 607
    move-object/from16 v0, p0

    iget-object v2, v0, Lbps;->b:Lyj;

    const/16 v5, 0xa

    const/16 v6, 0x65

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Lbps;->d:Ljava/lang/String;

    const/4 v13, 0x0

    move-wide/from16 v3, v47

    invoke-static/range {v2 .. v13}, Lf;->a(Lyj;JIILjava/lang/String;JJLjava/lang/String;Lyu;)V

    .line 617
    move-object/from16 v0, p0

    iget-object v2, v0, Lbps;->b:Lyj;

    const/16 v5, 0xa

    const/16 v6, 0x66

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Lbps;->d:Ljava/lang/String;

    const/4 v13, 0x0

    move-wide v3, v15

    invoke-static/range {v2 .. v13}, Lf;->a(Lyj;JIILjava/lang/String;JJLjava/lang/String;Lyu;)V

    .line 628
    move-object/from16 v0, p0

    iget-object v2, v0, Lbps;->d:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lyp;->d(Lyt;Ljava/lang/String;)V

    .line 629
    if-nez v14, :cond_9

    const-wide/16 v2, -0x1

    goto/16 :goto_0

    .line 481
    :cond_5
    :try_start_2
    move-object/from16 v0, p0

    iget v2, v0, Lbps;->k:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_6

    .line 482
    move-object/from16 v0, v46

    iget v2, v0, Lyv;->l:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v3, 0x1

    if-ne v2, v3, :cond_6

    const/4 v11, 0x1

    goto/16 :goto_1

    :cond_6
    const/4 v11, 0x0

    goto/16 :goto_1

    .line 539
    :cond_7
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 545
    :cond_8
    const-wide/16 v18, 0x0

    goto/16 :goto_3

    .line 580
    :catchall_0
    move-exception v2

    invoke-virtual/range {p1 .. p1}, Lyt;->c()V

    throw v2

    .line 629
    :cond_9
    invoke-static {v14}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v2

    goto/16 :goto_0
.end method

.method private static a(Lcom/google/android/gms/maps/model/MarkerOptions;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 893
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 894
    const-string v1, "https://maps.google.com/maps"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 897
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "?q="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/maps/model/MarkerOptions;->c()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v2

    iget-wide v2, v2, Lcom/google/android/gms/maps/model/LatLng;->a:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 898
    invoke-virtual {p0}, Lcom/google/android/gms/maps/model/MarkerOptions;->c()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v2

    iget-wide v2, v2, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 897
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 900
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/android/gms/maps/model/MarkerOptions;Lcom/google/android/gms/maps/model/CameraPosition;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 867
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 868
    const-string v1, "https://maps.googleapis.com/maps/api/staticmap"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 871
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "?center="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/google/android/gms/maps/model/CameraPosition;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, v2, Lcom/google/android/gms/maps/model/LatLng;->a:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/gms/maps/model/CameraPosition;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, v2, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 875
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "&zoom="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Lcom/google/android/gms/maps/model/CameraPosition;->b:F

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 878
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "&markers=color:red%7C"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 879
    invoke-virtual {p0}, Lcom/google/android/gms/maps/model/MarkerOptions;->c()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v2

    iget-wide v2, v2, Lcom/google/android/gms/maps/model/LatLng;->a:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 880
    invoke-virtual {p0}, Lcom/google/android/gms/maps/model/MarkerOptions;->c()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v2

    iget-wide v2, v2, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 878
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 883
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/ShareLocationActivity;->o()I

    move-result v1

    .line 884
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "&size="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 887
    const-string v1, "&sensor=false&visual_refresh=true"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 889
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lyt;Lbnl;Lzg;Lyv;)V
    .locals 32

    .prologue
    .line 635
    move-object/from16 v0, p2

    iget-object v5, v0, Lzg;->b:Ljava/lang/String;

    .line 636
    move-object/from16 v0, p2

    iget-object v4, v0, Lzg;->a:Ljava/lang/String;

    .line 637
    invoke-static {v5}, Lyt;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_a

    move-object/from16 v0, p3

    iget v3, v0, Lyv;->i:I

    if-nez v3, :cond_a

    .line 640
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lyt;->ab(Ljava/lang/String;)Z

    move-result v3

    .line 641
    if-eqz v3, :cond_2

    .line 643
    invoke-virtual/range {p0 .. p0}, Lyt;->f()Lyj;

    move-result-object v3

    const/4 v6, 0x5

    invoke-static {v3, v5, v6}, Lyp;->b(Lyj;Ljava/lang/String;I)V

    .line 645
    sget-boolean v3, Lbps;->a:Z

    if-eqz v3, :cond_0

    .line 646
    const-string v3, "Babel"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Messaging: delay sending message pending converting conversation to be permanent. ConversationId:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " messageId: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 651
    :cond_0
    new-instance v3, Lbek;

    new-instance v4, Lbee;

    .line 653
    invoke-static {}, Lyt;->h()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 657
    invoke-virtual/range {p0 .. p0}, Lyt;->f()Lyj;

    move-result-object v9

    invoke-static {v9, v5}, Lyp;->a(Lyj;Ljava/lang/String;)Ljava/util/List;

    move-result-object v9

    invoke-direct {v4, v6, v7, v8, v9}, Lbee;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/util/List;)V

    invoke-direct {v3, v4, v5}, Lbek;-><init>(Lbee;Ljava/lang/String;)V

    .line 651
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lbnl;->a(Lbea;)V

    .line 661
    const-string v3, "Babel"

    const-string v4, "Checking the server to see if we can move this conversation fromthe contingency state"

    invoke-static {v3, v4}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 797
    :cond_1
    :goto_0
    return-void

    .line 669
    :cond_2
    sget-boolean v3, Lbps;->a:Z

    if-eqz v3, :cond_3

    .line 670
    const-string v3, "Babel"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Messaging: creating SendChatMessageRequest for message. ConversationId:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " messageId: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 675
    :cond_3
    sget-boolean v3, Lbps;->a:Z

    if-eqz v3, :cond_4

    move-object/from16 v0, p2

    iget-object v3, v0, Lzg;->e:Ljava/lang/String;

    invoke-static {v3}, Lf;->m(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 676
    const-string v3, "Babel_Stress"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Sending stress message from SendMessageGeneralOperation:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    iget-object v7, v0, Lzg;->e:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 680
    :cond_4
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lyt;->j(Ljava/lang/String;)J

    move-result-wide v7

    .line 682
    move-object/from16 v0, p2

    iget-object v3, v0, Lzg;->e:Ljava/lang/String;

    invoke-static {v3}, Lf;->h(Ljava/lang/String;)Ljava/util/List;

    move-result-object v9

    .line 686
    const-string v3, "hangouts/location"

    move-object/from16 v0, p2

    iget-object v6, v0, Lzg;->o:Ljava/lang/String;

    .line 687
    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    move-object/from16 v0, p2

    iget-object v13, v0, Lzg;->k:Ljava/lang/String;

    .line 691
    :goto_1
    move-object/from16 v0, p2

    iget v3, v0, Lzg;->y:I

    packed-switch v3, :pswitch_data_0

    .line 700
    move-object/from16 v0, p2

    iget v3, v0, Lzg;->w:I

    const/4 v6, 0x1

    if-ne v3, v6, :cond_6

    .line 701
    const/16 v27, 0x1

    .line 708
    :goto_2
    new-instance v3, Lbfb;

    .line 709
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lyt;->X(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-wide/16 v10, 0x1

    add-long v28, v7, v10

    move-object/from16 v0, p2

    iget-object v10, v0, Lzg;->l:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v11, v0, Lzg;->o:Ljava/lang/String;

    move-object/from16 v0, p2

    iget v12, v0, Lzg;->C:I

    move-object/from16 v0, p2

    iget v14, v0, Lzg;->z:I

    move-object/from16 v0, p2

    iget v15, v0, Lzg;->A:I

    move-object/from16 v0, p2

    iget-object v0, v0, Lzg;->p:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p2

    iget-object v0, v0, Lzg;->q:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Lzg;->r:D

    move-wide/from16 v18, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Lzg;->s:D

    move-wide/from16 v20, v0

    move-object/from16 v0, p2

    iget-object v0, v0, Lzg;->t:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p2

    iget-object v0, v0, Lzg;->u:Ljava/lang/String;

    move-object/from16 v23, v0

    .line 716
    move-object/from16 v0, p3

    iget v0, v0, Lyv;->m:I

    move/from16 v24, v0

    const/16 v25, 0x2

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_8

    move-object/from16 v0, p3

    iget v0, v0, Lyv;->l:I

    move/from16 v24, v0

    const/16 v25, 0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_7

    const/16 v24, 0x1

    :goto_3
    move-object/from16 v0, p2

    iget v0, v0, Lzg;->w:I

    move/from16 v25, v0

    move-object/from16 v0, p2

    iget-object v0, v0, Lzg;->x:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-direct/range {v3 .. v27}, Lbfb;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/util/List;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IILjava/lang/String;Ljava/lang/String;DDLjava/lang/String;Ljava/lang/String;ZILjava/lang/String;I)V

    .line 708
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lbnl;->a(Lbea;)V

    .line 720
    const/4 v3, 0x2

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v4, v3, v6}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;II)V

    .line 722
    move-object/from16 v0, p0

    move-wide/from16 v1, v28

    invoke-virtual {v0, v5, v1, v2}, Lyt;->b(Ljava/lang/String;J)V

    goto/16 :goto_0

    .line 687
    :cond_5
    move-object/from16 v0, p2

    iget-object v13, v0, Lzg;->j:Ljava/lang/String;

    goto/16 :goto_1

    .line 694
    :pswitch_0
    const/16 v27, 0xb

    .line 695
    goto/16 :goto_2

    .line 697
    :pswitch_1
    const/16 v27, 0x2

    .line 698
    goto/16 :goto_2

    .line 703
    :cond_6
    const/16 v27, 0x0

    goto/16 :goto_2

    .line 716
    :cond_7
    const/16 v24, 0x0

    goto :goto_3

    :cond_8
    move-object/from16 v0, p2

    iget-wide v0, v0, Lzg;->i:J

    move-wide/from16 v24, v0

    const-wide/16 v30, 0x0

    cmp-long v24, v24, v30

    if-lez v24, :cond_9

    const/16 v24, 0x1

    goto :goto_3

    :cond_9
    const/16 v24, 0x0

    goto :goto_3

    .line 724
    :cond_a
    move-object/from16 v0, p3

    iget v3, v0, Lyv;->i:I

    const/4 v6, 0x2

    if-eq v3, v6, :cond_b

    move-object/from16 v0, p3

    iget v3, v0, Lyv;->k:I

    const/4 v6, 0x1

    if-ne v3, v6, :cond_e

    .line 728
    :cond_b
    sget-boolean v3, Lbps;->a:Z

    if-eqz v3, :cond_c

    .line 729
    const-string v3, "Babel"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Messaging: delay sending message pending re-accept an invite. ConversationId:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " messageId: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 742
    :cond_c
    move-object/from16 v0, p3

    iget v3, v0, Lyv;->i:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_d

    .line 744
    const-string v3, "Babel"

    const-string v4, "Retry invitation accept reply since it permenantly failed"

    invoke-static {v3, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 749
    :goto_4
    new-instance v3, Lbey;

    invoke-direct {v3, v5}, Lbey;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lbnl;->a(Lbea;)V

    goto/16 :goto_0

    .line 746
    :cond_d
    const-string v3, "Babel"

    const-string v4, "Send invitation accept reply since it is invited"

    invoke-static {v3, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 753
    :cond_e
    move-object/from16 v0, p3

    iget v3, v0, Lyv;->i:I

    const/4 v6, 0x4

    if-ne v3, v6, :cond_10

    .line 756
    sget-boolean v3, Lbps;->a:Z

    if-eqz v3, :cond_f

    .line 757
    const-string v3, "Babel"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Messaging: delay sending message pending re-creation of conversation on server. ConversationId:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " messageId: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 762
    :cond_f
    invoke-virtual/range {p0 .. p0}, Lyt;->f()Lyj;

    move-result-object v3

    invoke-static {v3, v5}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->h(Lyj;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 776
    :cond_10
    sget-boolean v3, Lbps;->a:Z

    if-eqz v3, :cond_1

    .line 777
    move-object/from16 v0, p3

    iget v3, v0, Lyv;->i:I

    const/4 v6, 0x1

    if-ne v3, v6, :cond_11

    .line 779
    const-string v3, "Babel"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Messaging: delay sending message pending an in-flight accept invite request. ConversationId:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " messageId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 782
    :cond_11
    move-object/from16 v0, p3

    iget v3, v0, Lyv;->i:I

    const/4 v6, 0x3

    if-ne v3, v6, :cond_12

    .line 784
    const-string v3, "Babel"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Messaging: delay sending message pending an in-flight conversation creation request. ConversationId:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " messageId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 787
    :cond_12
    move-object/from16 v0, p3

    iget v3, v0, Lyv;->i:I

    const/4 v6, 0x5

    if-ne v3, v6, :cond_13

    .line 789
    const-string v3, "Babel"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Messaging: delay sending message pending an in-flight persist conversation request. ConversationId:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " messageId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 793
    :cond_13
    const-string v3, "Babel"

    const-string v4, "Invalid state for conversation disposition"

    invoke-static {v3, v4}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 691
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 830
    const-string v1, "Babel"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 831
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Check readiness for location image: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move v1, v0

    .line 835
    :goto_0
    const/4 v2, 0x2

    if-ge v1, v2, :cond_2

    .line 837
    :try_start_0
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 838
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "r"

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    .line 839
    if-eqz v2, :cond_1

    .line 840
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->getStatSize()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 841
    const/4 v0, 0x1

    .line 858
    :goto_1
    return v0

    .line 844
    :cond_1
    const-wide/16 v2, 0x3e8

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 854
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 846
    :catch_0
    move-exception v2

    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Location image not ready, will retry after 1 second: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 849
    :catch_1
    move-exception v1

    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Location image check interrupted."

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 857
    :cond_2
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to get location image. Skipping it: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private d()Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbiw;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 225
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 227
    const-string v0, "hangouts/location"

    iget-object v1, p0, Lbps;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 228
    iget-object v0, p0, Lbps;->p:Lcom/google/android/gms/maps/model/MarkerOptions;

    if-nez v0, :cond_0

    .line 229
    const-string v0, "Babel"

    const-string v1, "mMarkerOptions should not be null"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v9

    .line 265
    :goto_0
    return-object v0

    .line 234
    :cond_0
    iget-object v0, p0, Lbps;->p:Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/MarkerOptions;->d()Ljava/lang/String;

    move-result-object v2

    .line 235
    iget-object v0, p0, Lbps;->p:Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/MarkerOptions;->c()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    iget-wide v3, v0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    .line 236
    iget-object v0, p0, Lbps;->p:Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/MarkerOptions;->c()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    iget-wide v5, v0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    .line 237
    iget-object v0, p0, Lbps;->p:Lcom/google/android/gms/maps/model/MarkerOptions;

    iget-object v1, p0, Lbps;->q:Lcom/google/android/gms/maps/model/CameraPosition;

    invoke-static {v0, v1}, Lbps;->a(Lcom/google/android/gms/maps/model/MarkerOptions;Lcom/google/android/gms/maps/model/CameraPosition;)Ljava/lang/String;

    move-result-object v7

    .line 238
    iget-object v0, p0, Lbps;->p:Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-static {v0}, Lbps;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)Ljava/lang/String;

    move-result-object v8

    .line 241
    new-instance v0, Lbix;

    new-array v1, v11, [I

    aput v10, v1, v10

    invoke-direct/range {v0 .. v8}, Lbix;-><init>([ILjava/lang/String;DDLjava/lang/String;Ljava/lang/String;)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_1
    move-object v0, v9

    .line 265
    goto :goto_0

    .line 244
    :cond_2
    iget-object v0, p0, Lbps;->h:Ljava/lang/String;

    if-nez v0, :cond_3

    iget-object v0, p0, Lbps;->f:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 245
    :cond_3
    iget-object v0, p0, Lbps;->h:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 246
    const-string v0, "Babel"

    const-string v1, "[SendMessageOp] photo: sending photo with photo id"

    invoke-static {v0, v1}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    :goto_2
    iget-object v0, p0, Lbps;->r:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 252
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lbps;->f:Ljava/lang/String;

    .line 253
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    .line 254
    :cond_4
    new-instance v0, Lbiy;

    new-array v1, v11, [I

    aput v10, v1, v10

    iget-object v2, p0, Lbps;->h:Ljava/lang/String;

    iget-object v3, p0, Lbps;->f:Ljava/lang/String;

    iget v4, p0, Lbps;->l:I

    iget v5, p0, Lbps;->m:I

    iget-object v6, p0, Lbps;->r:Ljava/lang/String;

    iget v7, p0, Lbps;->g:I

    invoke-direct/range {v0 .. v7}, Lbiy;-><init>([ILjava/lang/String;Ljava/lang/String;IILjava/lang/String;I)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 248
    :cond_5
    const-string v0, "Babel"

    const-string v1, "[SendMessageOp] photo: sending photo with url"

    invoke-static {v0, v1}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method


# virtual methods
.method public a()V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x3

    .line 153
    iget-boolean v0, p0, Lbps;->j:Z

    if-eqz v0, :cond_1

    .line 154
    const-string v0, "Babel"

    invoke-static {v0, v5}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Babel"

    const-string v1, "retrySendMessage"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    new-instance v0, Lyt;

    iget-object v1, p0, Lbps;->b:Lyj;

    invoke-direct {v0, v1}, Lyt;-><init>(Lyj;)V

    iget-object v1, p0, Lbps;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lyt;->e(Ljava/lang/String;)Lyv;

    move-result-object v2

    iget-object v1, p0, Lbps;->d:Ljava/lang/String;

    iget-object v3, p0, Lbps;->i:Ljava/lang/Long;

    invoke-static {v3}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v3

    iget-object v5, p0, Lbps;->c:Lbnl;

    iget-wide v6, p0, Lbps;->u:J

    iget-wide v8, p0, Lbps;->v:J

    invoke-static/range {v0 .. v9}, Lyp;->a(Lyt;Ljava/lang/String;Lyv;JLbnl;JJ)V

    .line 158
    :goto_0
    return-void

    .line 156
    :cond_1
    const-string v0, "Babel"

    invoke-static {v0, v5}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "sendOriginalMessage: text="

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lbps;->e:Ljava/lang/String;

    if-eqz v0, :cond_6

    const-string v0, "..."

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", mAttachmentUri="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lbps;->f:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", rotation="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lbps;->g:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    new-instance v0, Lyt;

    iget-object v2, p0, Lbps;->b:Lyj;

    invoke-direct {v0, v2}, Lyt;-><init>(Lyj;)V

    const-string v2, "Babel"

    invoke-static {v2, v5}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sendMessageLocally conversationId: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lbps;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v2, p0, Lbps;->r:Ljava/lang/String;

    invoke-static {v2}, Lf;->d(Ljava/lang/String;)Z

    move-result v2

    iget-object v3, p0, Lbps;->h:Ljava/lang/String;

    if-eqz v3, :cond_7

    const-string v2, "Babel"

    invoke-static {v2, v5}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sending image picasaPhotoId "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lbps;->h:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    :goto_2
    iget-object v2, p0, Lbps;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lyt;->f(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Lf;->b(I)Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-static {}, Lbzd;->c()Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lbps;->b:Lyj;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->i(Lyj;)V

    :cond_5
    invoke-direct {p0, v0}, Lbps;->a(Lyt;)J

    move-result-wide v0

    :goto_3
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lbps;->i:Ljava/lang/Long;

    goto/16 :goto_0

    :cond_6
    move-object v0, v1

    goto/16 :goto_1

    :cond_7
    if-nez v2, :cond_4

    iget-object v2, p0, Lbps;->f:Ljava/lang/String;

    if-eqz v2, :cond_4

    const-string v2, "Babel"

    invoke-static {v2, v5}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sending image "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lbps;->f:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    iget-object v2, p0, Lbps;->f:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/hangouts/content/EsProvider;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_b

    const-string v2, "Babel"

    invoke-static {v2, v5}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_9

    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sending attachment "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lbps;->f:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    const-string v2, "hangouts/location"

    iget-object v3, p0, Lbps;->r:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lbps;->f:Ljava/lang/String;

    invoke-static {v2}, Lbps;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    const-string v2, "image/jpeg"

    iput-object v2, p0, Lbps;->r:Ljava/lang/String;

    goto/16 :goto_2

    :cond_a
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sending location failed because image not ready "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lbps;->f:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lbps;->f:Ljava/lang/String;

    goto/16 :goto_2

    :cond_b
    const-string v2, "Babel"

    const-string v3, "trying to send an attachment that doesn\'t exist"

    invoke-static {v2, v3}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lbps;->f:Ljava/lang/String;

    goto/16 :goto_2

    :cond_c
    packed-switch v2, :pswitch_data_0

    :goto_4
    invoke-direct {p0, v0, v2, v1}, Lbps;->a(Lyt;ILjava/lang/String;)J

    move-result-wide v0

    goto/16 :goto_3

    :pswitch_0
    iget-object v1, p0, Lbps;->b:Lyj;

    invoke-virtual {v1}, Lyj;->J()Ljava/lang/String;

    move-result-object v1

    goto :goto_4

    :pswitch_1
    invoke-static {}, Lbzd;->g()Ljava/lang/String;

    move-result-object v1

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 928
    iget-object v0, p0, Lbps;->i:Ljava/lang/Long;

    return-object v0
.end method
