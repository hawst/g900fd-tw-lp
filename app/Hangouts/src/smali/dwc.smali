.class public final Ldwc;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldwc;


# instance fields
.field public b:Ldqf;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12093
    const/4 v0, 0x0

    new-array v0, v0, [Ldwc;

    sput-object v0, Ldwc;->a:[Ldwc;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 12094
    invoke-direct {p0}, Lepn;-><init>()V

    .line 12097
    iput-object v0, p0, Ldwc;->b:Ldqf;

    .line 12100
    iput-object v0, p0, Ldwc;->c:Ljava/lang/Integer;

    .line 12094
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 12127
    const/4 v0, 0x0

    .line 12128
    iget-object v1, p0, Ldwc;->b:Ldqf;

    if-eqz v1, :cond_0

    .line 12129
    const/4 v0, 0x1

    iget-object v1, p0, Ldwc;->b:Ldqf;

    .line 12130
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 12132
    :cond_0
    iget-object v1, p0, Ldwc;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 12133
    const/4 v1, 0x2

    iget-object v2, p0, Ldwc;->c:Ljava/lang/Integer;

    .line 12134
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 12136
    :cond_1
    iget-object v1, p0, Ldwc;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 12137
    const/4 v1, 0x3

    iget-object v2, p0, Ldwc;->d:Ljava/lang/Integer;

    .line 12138
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 12140
    :cond_2
    iget-object v1, p0, Ldwc;->e:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 12141
    const/4 v1, 0x4

    iget-object v2, p0, Ldwc;->e:Ljava/lang/Long;

    .line 12142
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 12144
    :cond_3
    iget-object v1, p0, Ldwc;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12145
    iput v0, p0, Ldwc;->cachedSize:I

    .line 12146
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 12090
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldwc;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldwc;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldwc;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldwc;->b:Ldqf;

    if-nez v0, :cond_2

    new-instance v0, Ldqf;

    invoke-direct {v0}, Ldqf;-><init>()V

    iput-object v0, p0, Ldwc;->b:Ldqf;

    :cond_2
    iget-object v0, p0, Ldwc;->b:Ldqf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_3

    const/16 v1, 0x14

    if-eq v0, v1, :cond_3

    const/16 v1, 0x1e

    if-ne v0, v1, :cond_4

    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldwc;->c:Ljava/lang/Integer;

    goto :goto_0

    :cond_4
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldwc;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldwc;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldwc;->e:Ljava/lang/Long;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 12109
    iget-object v0, p0, Ldwc;->b:Ldqf;

    if-eqz v0, :cond_0

    .line 12110
    const/4 v0, 0x1

    iget-object v1, p0, Ldwc;->b:Ldqf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 12112
    :cond_0
    iget-object v0, p0, Ldwc;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 12113
    const/4 v0, 0x2

    iget-object v1, p0, Ldwc;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 12115
    :cond_1
    iget-object v0, p0, Ldwc;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 12116
    const/4 v0, 0x3

    iget-object v1, p0, Ldwc;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 12118
    :cond_2
    iget-object v0, p0, Ldwc;->e:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 12119
    const/4 v0, 0x4

    iget-object v1, p0, Ldwc;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 12121
    :cond_3
    iget-object v0, p0, Ldwc;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 12123
    return-void
.end method
