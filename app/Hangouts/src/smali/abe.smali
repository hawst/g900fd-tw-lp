.class public Labe;
.super Ls;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field protected Y:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ls;-><init>()V

    .line 38
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Labe;
    .locals 2

    .prologue
    .line 83
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 84
    if-eqz p0, :cond_0

    .line 85
    const-string v1, "title"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    :cond_0
    const-string v1, "message"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    if-eqz p2, :cond_1

    .line 90
    const-string v1, "positive"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    :cond_1
    if-eqz p3, :cond_2

    .line 94
    const-string v1, "negative"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    :cond_2
    new-instance v1, Labe;

    invoke-direct {v1}, Labe;-><init>()V

    .line 98
    invoke-virtual {v1, v0}, Labe;->setArguments(Landroid/os/Bundle;)V

    .line 99
    return-object v1
.end method


# virtual methods
.method public f()Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 151
    invoke-virtual {p0}, Labe;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 153
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Labe;->getActivity()Ly;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 154
    const-string v2, "title"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 155
    const-string v2, "title"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 158
    :cond_0
    const-string v2, "message"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 160
    const-string v2, "positive"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 161
    const-string v2, "positive"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 164
    :cond_1
    const-string v2, "negative"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 165
    const-string v2, "negative"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 168
    :cond_2
    const-string v2, "neutral"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 169
    const-string v2, "neutral"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 172
    :cond_3
    const-string v2, "edit_text"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 173
    new-instance v2, Landroid/widget/EditText;

    invoke-virtual {p0}, Labe;->getActivity()Ly;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Labe;->Y:Landroid/widget/EditText;

    .line 174
    iget-object v2, p0, Labe;->Y:Landroid/widget/EditText;

    const-string v3, "edit_text"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 175
    iget-object v0, p0, Labe;->Y:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 178
    :cond_4
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 3

    .prologue
    .line 225
    const/4 v1, 0x0

    .line 226
    invoke-virtual {p0}, Labe;->getTargetFragment()Lt;

    move-result-object v0

    .line 227
    if-eqz v0, :cond_1

    instance-of v2, v0, Labf;

    if-eqz v2, :cond_1

    .line 228
    check-cast v0, Labf;

    .line 235
    :goto_0
    if-eqz v0, :cond_0

    .line 236
    invoke-virtual {p0}, Labe;->getArguments()Landroid/os/Bundle;

    invoke-virtual {p0}, Labe;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Labf;->b(Ljava/lang/String;)V

    .line 238
    :cond_0
    return-void

    .line 230
    :cond_1
    invoke-virtual {p0}, Labe;->getActivity()Ly;

    move-result-object v0

    .line 231
    if-eqz v0, :cond_2

    instance-of v2, v0, Labf;

    if-eqz v2, :cond_2

    .line 232
    check-cast v0, Labf;

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 193
    const/4 v1, 0x0

    .line 194
    invoke-virtual {p0}, Labe;->getTargetFragment()Lt;

    move-result-object v0

    .line 195
    if-eqz v0, :cond_2

    instance-of v2, v0, Labf;

    if-eqz v2, :cond_2

    .line 196
    check-cast v0, Labf;

    .line 203
    :goto_0
    if-eqz v0, :cond_1

    .line 204
    invoke-virtual {p0}, Labe;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 205
    const-string v2, "edit_text"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Labe;->Y:Landroid/widget/EditText;

    if-eqz v2, :cond_0

    .line 206
    const-string v2, "edit_text"

    iget-object v3, p0, Labe;->Y:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 220
    :cond_1
    :goto_1
    return-void

    .line 198
    :cond_2
    invoke-virtual {p0}, Labe;->getActivity()Ly;

    move-result-object v0

    .line 199
    if-eqz v0, :cond_3

    instance-of v2, v0, Labf;

    if-eqz v2, :cond_3

    .line 200
    check-cast v0, Labf;

    goto :goto_0

    .line 210
    :pswitch_0
    invoke-virtual {p0}, Labe;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p0}, Labe;->getTag()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Labf;->a(Landroid/os/Bundle;Ljava/lang/String;)V

    goto :goto_1

    .line 213
    :pswitch_1
    invoke-virtual {p0}, Labe;->getArguments()Landroid/os/Bundle;

    invoke-virtual {p0}, Labe;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Labf;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 216
    :pswitch_2
    invoke-virtual {p0}, Labe;->getArguments()Landroid/os/Bundle;

    invoke-virtual {p0}, Labe;->getTag()Ljava/lang/String;

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_0

    .line 208
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 183
    invoke-super {p0}, Ls;->onStart()V

    .line 186
    invoke-virtual {p0}, Labe;->b()Landroid/app/Dialog;

    move-result-object v0

    const v1, 0x102000b

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 187
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    .line 186
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 188
    return-void
.end method
