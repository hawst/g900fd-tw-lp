.class public final Lazm;
.super Landroid/widget/BaseAdapter;
.source "PG"


# instance fields
.field final synthetic a:Landroid/database/Cursor;

.field final synthetic b:[I

.field final synthetic c:Lcom/google/android/apps/hangouts/phone/DebugActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/phone/DebugActivity;Landroid/database/Cursor;[I)V
    .locals 0

    .prologue
    .line 525
    iput-object p1, p0, Lazm;->c:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    iput-object p2, p0, Lazm;->a:Landroid/database/Cursor;

    iput-object p3, p0, Lazm;->b:[I

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 564
    iget-object v0, p0, Lazm;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 569
    iget-object v0, p0, Lazm;->a:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 570
    iget-object v0, p0, Lazm;->a:Landroid/database/Cursor;

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 575
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 528
    if-nez p2, :cond_0

    .line 529
    iget-object v0, p0, Lazm;->c:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/phone/DebugActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lf;->fP:I

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 532
    :cond_0
    sget v0, Lg;->dY:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 533
    sget v1, Lg;->ia:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 535
    iget-object v3, p0, Lazm;->a:Landroid/database/Cursor;

    invoke-interface {v3, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 537
    const-string v3, ""

    .line 538
    iget-object v4, p0, Lazm;->b:[I

    array-length v5, v4

    move v9, v2

    move-object v2, v3

    move v3, v9

    :goto_0
    if-ge v3, v5, :cond_1

    aget v6, v4, v3

    .line 540
    :try_start_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lazm;->a:Landroid/database/Cursor;

    invoke-interface {v8, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 538
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 542
    :catch_0
    move-exception v6

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "???, "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 545
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "@ "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 546
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 548
    const/4 v0, -0x1

    invoke-virtual {p2, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 549
    iget-object v0, p0, Lazm;->c:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/DebugActivity;->i(Lcom/google/android/apps/hangouts/phone/DebugActivity;)Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lazm;->c:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/DebugActivity;->i(Lcom/google/android/apps/hangouts/phone/DebugActivity;)Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;->b(Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 550
    iget-object v0, p0, Lazm;->a:Landroid/database/Cursor;

    const-string v1, "conversation_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 552
    if-ltz v0, :cond_2

    .line 553
    iget-object v1, p0, Lazm;->a:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 554
    iget-object v1, p0, Lazm;->c:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/phone/DebugActivity;->i(Lcom/google/android/apps/hangouts/phone/DebugActivity;)Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;->b(Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 555
    const v0, -0x1fff0f20

    invoke-virtual {p2, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 559
    :cond_2
    return-object p2
.end method
