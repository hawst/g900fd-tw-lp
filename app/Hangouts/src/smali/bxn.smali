.class public final Lbxn;
.super Lbyo;
.source "PG"


# instance fields
.field private b:Z

.field private c:Z

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 29
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x7

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-direct {p0, p1, v0}, Lbyo;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 31
    iget-boolean v0, p0, Lbxn;->c:Z

    iget-boolean v1, p0, Lbxn;->b:Z

    iget-boolean v2, p0, Lbxn;->d:Z

    invoke-direct {p0, v0, v1, v2}, Lbxn;->b(ZZZ)V

    .line 32
    return-void
.end method

.method private a(Ljava/lang/String;II)V
    .locals 1

    .prologue
    .line 67
    new-instance v0, Lbxo;

    invoke-direct {v0, p1, p2, p3}, Lbxo;-><init>(Ljava/lang/String;II)V

    .line 68
    invoke-virtual {p0, v0}, Lbxn;->add(Ljava/lang/Object;)V

    .line 69
    return-void
.end method

.method private b(ZZZ)V
    .locals 4

    .prologue
    .line 43
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    .line 44
    sget v1, Lh;->gy:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/android/apps/hangouts/R$drawable;->bb:I

    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, v3}, Lbxn;->a(Ljava/lang/String;II)V

    .line 47
    if-eqz p2, :cond_0

    .line 48
    sget v1, Lh;->gh:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/android/apps/hangouts/R$drawable;->cu:I

    const/4 v3, 0x3

    invoke-direct {p0, v1, v2, v3}, Lbxn;->a(Ljava/lang/String;II)V

    .line 52
    :cond_0
    sget v1, Lh;->gk:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/android/apps/hangouts/R$drawable;->by:I

    const/4 v3, 0x2

    invoke-direct {p0, v1, v2, v3}, Lbxn;->a(Ljava/lang/String;II)V

    .line 55
    if-eqz p1, :cond_1

    .line 56
    sget v1, Lh;->gj:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/android/apps/hangouts/R$drawable;->cg:I

    const/4 v3, 0x1

    invoke-direct {p0, v1, v2, v3}, Lbxn;->a(Ljava/lang/String;II)V

    .line 60
    :cond_1
    if-eqz p3, :cond_2

    .line 61
    sget v1, Lh;->jP:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->bN:I

    const/4 v2, 0x4

    invoke-direct {p0, v0, v1, v2}, Lbxn;->a(Ljava/lang/String;II)V

    .line 64
    :cond_2
    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lbxn;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxo;

    .line 36
    invoke-virtual {v0}, Lbxo;->a()I

    move-result v0

    return v0
.end method

.method public a(ZZZ)V
    .locals 3

    .prologue
    .line 90
    iget-boolean v0, p0, Lbxn;->c:Z

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lbxn;->b:Z

    if-ne p2, v0, :cond_0

    iget-boolean v0, p0, Lbxn;->d:Z

    if-eq p3, v0, :cond_1

    .line 92
    :cond_0
    iput-boolean p1, p0, Lbxn;->c:Z

    .line 93
    iput-boolean p2, p0, Lbxn;->b:Z

    .line 94
    iput-boolean p3, p0, Lbxn;->d:Z

    .line 95
    invoke-virtual {p0}, Lbxn;->clear()V

    .line 96
    iget-boolean v0, p0, Lbxn;->c:Z

    iget-boolean v1, p0, Lbxn;->b:Z

    iget-boolean v2, p0, Lbxn;->d:Z

    invoke-direct {p0, v0, v1, v2}, Lbxn;->b(ZZZ)V

    .line 98
    :cond_1
    return-void
.end method
