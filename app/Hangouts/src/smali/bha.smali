.class public Lbha;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private final A:Ljava/lang/String;

.field private B:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbzo",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private C:[Ljava/lang/String;

.field private final g:Lbdh;

.field private final h:Z

.field private i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lbdm;",
            ">;"
        }
    .end annotation
.end field

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:Z

.field private final x:Z

.field private final y:Z

.field private final z:J


# direct methods
.method private constructor <init>(Ldsq;)V
    .locals 13

    .prologue
    const-wide/16 v11, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1677
    iget-object v0, p1, Ldsq;->b:Ldvn;

    const-wide/16 v1, -0x1

    invoke-direct {p0, v0, v1, v2}, Lbfz;-><init>(Ldvn;J)V

    .line 1678
    iget-object v0, p1, Ldsq;->c:Ldro;

    if-eqz v0, :cond_c

    .line 1679
    iget-object v0, p1, Ldsq;->c:Ldro;

    .line 1681
    new-instance v1, Lbdh;

    invoke-direct {v1, v0, v5}, Lbdh;-><init>(Ldro;Ljava/lang/String;)V

    iput-object v1, p0, Lbha;->g:Lbdh;

    .line 1682
    iget-object v0, p1, Ldsq;->d:Ljava/lang/Boolean;

    invoke-static {v0, v4}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    iput-boolean v0, p0, Lbha;->h:Z

    .line 1683
    iget-object v0, p0, Lbha;->g:Lbdh;

    iget-boolean v0, v0, Lbdh;->o:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0, v4}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    iput-boolean v0, p0, Lbha;->n:Z

    .line 1684
    iget-object v0, p1, Ldsq;->i:Lduk;

    const-string v1, "Babel"

    const-string v2, "parsePhoneVerificationStates."

    invoke-static {v1, v2}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lbha;->i:Ljava/util/Map;

    iget-object v1, v0, Lduk;->b:[Lduj;

    array-length v2, v1

    move v0, v4

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v5, v1, v0

    sget-boolean v6, Lbfz;->a:Z

    if-eqz v6, :cond_0

    const-string v6, "Babel"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "phoneNumber="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, v5, Lduj;->b:Leir;

    iget-object v8, v8, Leir;->b:Ljava/lang/String;

    invoke-static {v8}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " verificationStatus="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v5, Lduj;->d:Ljava/lang/Integer;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v6, v5, Lduj;->b:Leir;

    iget-object v6, v6, Leir;->b:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    const-string v5, "Babel"

    const-string v6, "skipping empty phone number"

    invoke-static {v5, v6}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    new-instance v6, Lbdm;

    invoke-direct {v6, v5}, Lbdm;-><init>(Lduj;)V

    invoke-virtual {v6}, Lbdm;->b()Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lbha;->i:Ljava/util/Map;

    invoke-virtual {v6}, Lbdm;->a()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v7, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1685
    :cond_3
    iget-object v0, p0, Lbha;->g:Lbdh;

    iget-boolean v0, v0, Lbdh;->s:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0, v4}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    iput-boolean v0, p0, Lbha;->t:Z

    .line 1687
    iget-object v0, p1, Ldsq;->j:[Ldpw;

    array-length v6, v0

    .line 1688
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_4

    .line 1689
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetSelfInfoResponse bitCount status: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    move v5, v4

    move v0, v4

    move v1, v4

    move v2, v4

    .line 1696
    :goto_2
    if-ge v5, v6, :cond_6

    .line 1697
    iget-object v7, p1, Ldsq;->j:[Ldpw;

    aget-object v7, v7, v5

    .line 1698
    sget-boolean v8, Lbfz;->a:Z

    if-eqz v8, :cond_5

    .line 1699
    const-string v8, "Babel"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "GetSelfInfoResponse configBit: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, v7, Ldpw;->b:Ljava/lang/Integer;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v7, Ldpw;->c:Ljava/lang/Boolean;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1703
    :cond_5
    iget-object v8, v7, Ldpw;->b:Ljava/lang/Integer;

    invoke-static {v8, v4}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v8

    packed-switch v8, :pswitch_data_0

    .line 1696
    :goto_3
    :pswitch_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 1705
    :pswitch_1
    iget-object v7, v7, Ldpw;->c:Ljava/lang/Boolean;

    invoke-static {v7, v4}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v7

    iput-boolean v7, p0, Lbha;->j:Z

    goto :goto_3

    .line 1709
    :pswitch_2
    iget-object v7, v7, Ldpw;->c:Ljava/lang/Boolean;

    invoke-static {v7, v4}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v7

    iput-boolean v7, p0, Lbha;->k:Z

    goto :goto_3

    .line 1713
    :pswitch_3
    iget-object v7, v7, Ldpw;->c:Ljava/lang/Boolean;

    invoke-static {v7, v4}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v7

    iput-boolean v7, p0, Lbha;->l:Z

    goto :goto_3

    .line 1717
    :pswitch_4
    iget-object v7, v7, Ldpw;->c:Ljava/lang/Boolean;

    invoke-static {v7, v4}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v7

    iput-boolean v7, p0, Lbha;->m:Z

    goto :goto_3

    .line 1721
    :pswitch_5
    iget-object v7, v7, Ldpw;->c:Ljava/lang/Boolean;

    invoke-static {v7, v4}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v7

    iput-boolean v7, p0, Lbha;->o:Z

    goto :goto_3

    .line 1724
    :pswitch_6
    iget-object v2, v7, Ldpw;->c:Ljava/lang/Boolean;

    invoke-static {v2, v4}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v2

    goto :goto_3

    .line 1727
    :pswitch_7
    iget-object v1, v7, Ldpw;->c:Ljava/lang/Boolean;

    invoke-static {v1, v4}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v1

    goto :goto_3

    .line 1730
    :pswitch_8
    iget-object v0, v7, Ldpw;->c:Ljava/lang/Boolean;

    invoke-static {v0, v4}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    goto :goto_3

    .line 1733
    :pswitch_9
    iget-object v7, v7, Ldpw;->c:Ljava/lang/Boolean;

    invoke-static {v7, v4}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v7

    iput-boolean v7, p0, Lbha;->q:Z

    goto :goto_3

    .line 1736
    :pswitch_a
    iget-object v7, v7, Ldpw;->c:Ljava/lang/Boolean;

    invoke-static {v7, v4}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v7

    iput-boolean v7, p0, Lbha;->r:Z

    goto :goto_3

    .line 1739
    :pswitch_b
    iget-object v7, v7, Ldpw;->c:Ljava/lang/Boolean;

    invoke-static {v7, v4}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v7

    iput-boolean v7, p0, Lbha;->s:Z

    goto :goto_3

    .line 1742
    :pswitch_c
    iget-object v7, v7, Ldpw;->c:Ljava/lang/Boolean;

    invoke-static {v7, v4}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v7

    iput-boolean v7, p0, Lbha;->u:Z

    goto :goto_3

    .line 1745
    :pswitch_d
    iget-object v7, v7, Ldpw;->c:Ljava/lang/Boolean;

    invoke-static {v7, v4}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v7

    iput-boolean v7, p0, Lbha;->v:Z

    goto :goto_3

    .line 1748
    :pswitch_e
    iget-object v7, v7, Ldpw;->c:Ljava/lang/Boolean;

    invoke-static {v7, v4}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v7

    iput-boolean v7, p0, Lbha;->w:Z

    goto :goto_3

    .line 1753
    :cond_6
    if-eqz v2, :cond_7

    if-eqz v1, :cond_7

    if-eqz v0, :cond_7

    move v0, v3

    :goto_4
    iput-boolean v0, p0, Lbha;->p:Z

    .line 1756
    iget-object v0, p1, Ldsq;->f:Ldrg;

    if-eqz v0, :cond_8

    :goto_5
    iput-boolean v3, p0, Lbha;->x:Z

    .line 1757
    iget-boolean v0, p0, Lbha;->x:Z

    if-eqz v0, :cond_9

    .line 1758
    iget-object v0, p1, Ldsq;->f:Ldrg;

    iget-object v0, v0, Ldrg;->b:Ljava/lang/Boolean;

    invoke-static {v0, v4}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    iput-boolean v0, p0, Lbha;->y:Z

    .line 1760
    iget-object v0, p1, Ldsq;->f:Ldrg;

    iget-object v0, v0, Ldrg;->c:Ljava/lang/Long;

    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v0

    iput-wide v0, p0, Lbha;->z:J

    .line 1767
    :goto_6
    const-string v0, ""

    .line 1768
    iget-object v2, p1, Ldsq;->n:Ldvq;

    .line 1769
    if-eqz v2, :cond_b

    .line 1770
    iget-object v1, v2, Ldvq;->d:[Ldvo;

    array-length v1, v1

    if-lez v1, :cond_a

    .line 1771
    new-instance v1, Ljava/util/ArrayList;

    iget-object v3, v2, Ldvq;->d:[Ldvo;

    array-length v3, v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lbha;->B:Ljava/util/List;

    .line 1776
    iget-object v3, v2, Ldvq;->d:[Ldvo;

    array-length v5, v3

    move v1, v4

    :goto_7
    if-ge v1, v5, :cond_a

    aget-object v6, v3, v1

    .line 1777
    iget-object v7, p0, Lbha;->B:Ljava/util/List;

    new-instance v8, Lbzo;

    iget-object v9, v6, Ldvo;->b:Ljava/lang/Integer;

    iget-object v6, v6, Ldvo;->c:Ljava/lang/Boolean;

    invoke-direct {v8, v9, v6}, Lbzo;-><init>(Ljava/io/Serializable;Ljava/io/Serializable;)V

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1776
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    :cond_7
    move v0, v4

    .line 1753
    goto :goto_4

    :cond_8
    move v3, v4

    .line 1756
    goto :goto_5

    .line 1763
    :cond_9
    iput-boolean v4, p0, Lbha;->y:Z

    .line 1764
    iput-wide v11, p0, Lbha;->z:J

    goto :goto_6

    .line 1782
    :cond_a
    iget-object v1, v2, Ldvq;->b:Ldue;

    if-eqz v1, :cond_b

    .line 1783
    iget-object v1, v2, Ldvq;->b:Ldue;

    .line 1784
    iget-object v2, v1, Ldue;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 1785
    iget-object v0, v1, Ldue;->b:Ljava/lang/String;

    .line 1789
    :cond_b
    iput-object v0, p0, Lbha;->A:Ljava/lang/String;

    .line 1791
    iget-object v0, p1, Ldsq;->r:[Ldui;

    if-eqz v0, :cond_d

    .line 1792
    iget-object v0, p1, Ldsq;->r:[Ldui;

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lbha;->C:[Ljava/lang/String;

    .line 1793
    :goto_8
    iget-object v0, p1, Ldsq;->r:[Ldui;

    array-length v0, v0

    if-ge v4, v0, :cond_d

    .line 1794
    iget-object v0, p0, Lbha;->C:[Ljava/lang/String;

    iget-object v1, p1, Ldsq;->r:[Ldui;

    aget-object v1, v1, v4

    iget-object v1, v1, Ldui;->c:Ljava/lang/String;

    aput-object v1, v0, v4

    .line 1793
    add-int/lit8 v4, v4, 0x1

    goto :goto_8

    .line 1798
    :cond_c
    iput-object v5, p0, Lbha;->g:Lbdh;

    .line 1799
    iput-boolean v4, p0, Lbha;->h:Z

    .line 1800
    iput-object v5, p0, Lbha;->i:Ljava/util/Map;

    .line 1801
    iput-boolean v4, p0, Lbha;->x:Z

    .line 1802
    iput-boolean v4, p0, Lbha;->y:Z

    .line 1803
    iput-wide v11, p0, Lbha;->z:J

    .line 1804
    const-string v0, ""

    iput-object v0, p0, Lbha;->A:Ljava/lang/String;

    .line 1806
    :cond_d
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_e

    .line 1807
    const-string v0, "Babel_protos"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetSelfInfoResponse from:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1809
    :cond_e
    return-void

    .line 1703
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_e
        :pswitch_d
    .end packed-switch
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 1838
    new-instance v0, Ldsq;

    invoke-direct {v0}, Ldsq;-><init>()V

    .line 1839
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldsq;

    .line 1840
    iget-object v1, v0, Ldsq;->b:Ldvn;

    invoke-static {v1}, Lbha;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1841
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldsq;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 1843
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbha;

    invoke-direct {v1, v0}, Lbha;-><init>(Ldsq;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public f()Lbdh;
    .locals 1

    .prologue
    .line 1848
    iget-object v0, p0, Lbha;->g:Lbdh;

    return-object v0
.end method

.method public g()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lbdm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1856
    iget-object v0, p0, Lbha;->i:Ljava/util/Map;

    return-object v0
.end method

.method public h()Lyk;
    .locals 2

    .prologue
    .line 1860
    new-instance v0, Lyk;

    invoke-direct {v0}, Lyk;-><init>()V

    .line 1861
    iget-boolean v1, p0, Lbha;->j:Z

    iput-boolean v1, v0, Lyk;->b:Z

    .line 1862
    iget-boolean v1, p0, Lbha;->k:Z

    iput-boolean v1, v0, Lyk;->c:Z

    .line 1863
    iget-boolean v1, p0, Lbha;->l:Z

    iput-boolean v1, v0, Lyk;->d:Z

    .line 1864
    iget-boolean v1, p0, Lbha;->m:Z

    iput-boolean v1, v0, Lyk;->e:Z

    .line 1865
    iget-boolean v1, p0, Lbha;->n:Z

    iput-boolean v1, v0, Lyk;->k:Z

    .line 1866
    iget-boolean v1, p0, Lbha;->o:Z

    iput-boolean v1, v0, Lyk;->f:Z

    .line 1867
    iget-boolean v1, p0, Lbha;->p:Z

    iput-boolean v1, v0, Lyk;->g:Z

    .line 1868
    iget-boolean v1, p0, Lbha;->q:Z

    iput-boolean v1, v0, Lyk;->h:Z

    .line 1869
    iget-boolean v1, p0, Lbha;->s:Z

    iput-boolean v1, v0, Lyk;->i:Z

    .line 1870
    iget-boolean v1, p0, Lbha;->r:Z

    iput-boolean v1, v0, Lyk;->j:Z

    .line 1871
    iget-boolean v1, p0, Lbha;->t:Z

    iput-boolean v1, v0, Lyk;->l:Z

    .line 1872
    iget-boolean v1, p0, Lbha;->u:Z

    iput-boolean v1, v0, Lyk;->m:Z

    .line 1873
    iget-boolean v1, p0, Lbha;->v:Z

    iput-boolean v1, v0, Lyk;->n:Z

    .line 1874
    iget-boolean v1, p0, Lbha;->w:Z

    iput-boolean v1, v0, Lyk;->o:Z

    .line 1875
    return-object v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 1883
    iget-boolean v0, p0, Lbha;->y:Z

    return v0
.end method

.method public j()J
    .locals 2

    .prologue
    .line 1887
    iget-wide v0, p0, Lbha;->z:J

    return-wide v0
.end method

.method public k()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbzo",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1891
    iget-object v0, p0, Lbha;->B:Ljava/util/List;

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1895
    iget-object v0, p0, Lbha;->A:Ljava/lang/String;

    return-object v0
.end method

.method public m()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 1904
    iget-object v0, p0, Lbha;->C:[Ljava/lang/String;

    return-object v0
.end method
