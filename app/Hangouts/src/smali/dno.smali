.class public final Ldno;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldno;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1471
    const/4 v0, 0x0

    new-array v0, v0, [Ldno;

    sput-object v0, Ldno;->a:[Ldno;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1472
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1498
    const/4 v0, 0x0

    .line 1499
    iget-object v1, p0, Ldno;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1500
    const/4 v0, 0x1

    iget-object v1, p0, Ldno;->b:Ljava/lang/String;

    .line 1501
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1503
    :cond_0
    iget-object v1, p0, Ldno;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 1504
    const/4 v1, 0x2

    iget-object v2, p0, Ldno;->c:Ljava/lang/Integer;

    .line 1505
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1507
    :cond_1
    iget-object v1, p0, Ldno;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1508
    const/4 v1, 0x3

    iget-object v2, p0, Ldno;->d:Ljava/lang/String;

    .line 1509
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1511
    :cond_2
    iget-object v1, p0, Ldno;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1512
    iput v0, p0, Ldno;->cachedSize:I

    .line 1513
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 1468
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldno;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldno;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldno;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldno;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldno;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldno;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 1483
    iget-object v0, p0, Ldno;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1484
    const/4 v0, 0x1

    iget-object v1, p0, Ldno;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1486
    :cond_0
    iget-object v0, p0, Ldno;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1487
    const/4 v0, 0x2

    iget-object v1, p0, Ldno;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1489
    :cond_1
    iget-object v0, p0, Ldno;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1490
    const/4 v0, 0x3

    iget-object v1, p0, Ldno;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1492
    :cond_2
    iget-object v0, p0, Ldno;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1494
    return-void
.end method
