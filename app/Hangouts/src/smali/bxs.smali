.class public final Lbxs;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbxv;
.implements Lcxf;


# static fields
.field protected static final a:Z


# instance fields
.field public final b:Lbxu;

.field private c:I

.field private final d:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lbzn;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lbys;->r:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lbxs;->a:Z

    return-void
.end method

.method public constructor <init>(II)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lbxs;->e:Ljava/lang/Object;

    .line 57
    if-gtz p1, :cond_0

    .line 58
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "maxSize <= 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 60
    :cond_0
    iput p1, p0, Lbxs;->c:I

    .line 61
    new-instance v2, Ljava/util/LinkedHashMap;

    const/high16 v3, 0x3f400000    # 0.75f

    invoke-direct {v2, v1, v3, v0}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    iput-object v2, p0, Lbxs;->d:Ljava/util/LinkedHashMap;

    .line 63
    if-ltz p2, :cond_1

    :goto_0
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 64
    const/4 v0, 0x5

    const/16 v1, 0x1e

    const/16 v2, 0x64

    const-string v3, "Image"

    invoke-static {v0, v1, v2, p2, v3}, Lbxu;->a(IIIILjava/lang/String;)Lbxu;

    move-result-object v0

    iput-object v0, p0, Lbxs;->b:Lbxu;

    .line 66
    return-void

    :cond_1
    move v0, v1

    .line 63
    goto :goto_0
.end method

.method private a(Z)V
    .locals 7

    .prologue
    .line 148
    iget-object v2, p0, Lbxs;->e:Ljava/lang/Object;

    monitor-enter v2

    .line 151
    :try_start_0
    iget-object v0, p0, Lbxs;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    .line 152
    if-eqz p1, :cond_0

    add-int/lit8 v1, v0, -0x28

    .line 153
    :goto_0
    if-gtz v1, :cond_1

    .line 154
    monitor-exit v2

    .line 192
    :goto_1
    return-void

    :cond_0
    move v1, v0

    .line 152
    goto :goto_0

    .line 157
    :cond_1
    sget-boolean v3, Lbxs;->a:Z

    if-eqz v3, :cond_2

    .line 158
    const-string v3, "Babel_medialoader"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "BitmapCache prune starting evictions cache size="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " onlyAboveCapacity="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " desiredToRemove="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    :cond_2
    iget-object v0, p0, Lbxs;->d:Ljava/util/LinkedHashMap;

    .line 164
    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 166
    :goto_2
    if-lez v1, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_5

    .line 167
    :try_start_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;
    :try_end_1
    .catch Ljava/util/NoSuchElementException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 174
    :try_start_2
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzn;

    .line 175
    invoke-virtual {v0}, Lbzn;->i()I

    move-result v4

    if-nez v4, :cond_7

    .line 176
    sget-boolean v4, Lbxs;->a:Z

    if-eqz v4, :cond_3

    .line 177
    const-string v4, "Babel_medialoader"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "BitmapCache prune evicting: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lbzn;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    :cond_3
    invoke-virtual {v0}, Lbzn;->c()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 180
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 181
    if-eqz v0, :cond_4

    .line 182
    iget-object v4, p0, Lbxs;->b:Lbxu;

    invoke-virtual {v4, v0}, Lbxu;->a(Landroid/graphics/Bitmap;)V

    .line 184
    :cond_4
    add-int/lit8 v0, v1, -0x1

    :goto_3
    move v1, v0

    .line 186
    goto :goto_2

    .line 171
    :catch_0
    move-exception v0

    const-string v0, "Babel_medialoader"

    const-string v3, "Pruning went off the tracks"

    invoke-static {v0, v3}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    :cond_5
    sget-boolean v0, Lbxs;->a:Z

    if-eqz v0, :cond_6

    .line 188
    const-string v0, "Babel_medialoader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "BitmapCache prune finished evictions cache size="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lbxs;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v4}, Ljava/util/LinkedHashMap;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " onlyAboveCapacity="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " desiredToRemove="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    :cond_6
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_7
    move v0, v1

    goto :goto_3
.end method

.method private b(I)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 120
    sget-boolean v1, Lbxs;->a:Z

    if-eqz v1, :cond_0

    .line 121
    const-string v1, "Babel_medialoader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "BitmapCache evictSome="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 138
    const/4 v0, 0x0

    :goto_0
    :pswitch_0
    return v0

    .line 128
    :pswitch_1
    invoke-virtual {p0}, Lbxs;->C_()V

    goto :goto_0

    .line 133
    :pswitch_2
    sget-object v1, Lcxe;->a:Lcxe;

    invoke-virtual {v1}, Lcxe;->a()V

    goto :goto_0

    .line 123
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public C_()V
    .locals 2

    .prologue
    .line 105
    sget-boolean v0, Lbxs;->a:Z

    if-eqz v0, :cond_0

    .line 106
    const-string v0, "Babel_medialoader"

    const-string v1, "BitmapCache trimMemory."

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbxs;->a(Z)V

    .line 109
    return-void
.end method

.method public a(II)Landroid/graphics/Bitmap;
    .locals 6

    .prologue
    .line 198
    const/4 v1, 0x0

    .line 199
    iget-object v2, p0, Lbxs;->e:Ljava/lang/Object;

    monitor-enter v2

    .line 200
    :try_start_0
    sget-boolean v0, Lbxs;->a:Z

    if-eqz v0, :cond_0

    .line 201
    const-string v0, "Babel_medialoader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "BitmapCache getBitmap starting evictions cache size="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lbxs;->d:Ljava/util/LinkedHashMap;

    .line 202
    invoke-virtual {v4}, Ljava/util/LinkedHashMap;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " width="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " height="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 201
    invoke-static {v0, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    :cond_0
    iget-object v0, p0, Lbxs;->d:Ljava/util/LinkedHashMap;

    .line 206
    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 208
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_2

    .line 209
    :try_start_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;
    :try_end_1
    .catch Ljava/util/NoSuchElementException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 216
    :try_start_2
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzn;

    .line 217
    invoke-virtual {v0}, Lbzn;->i()I

    move-result v4

    if-nez v4, :cond_4

    invoke-virtual {v0, p1, p2}, Lbzn;->a(II)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v0}, Lbzn;->g()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 218
    sget-boolean v1, Lbxs;->a:Z

    if-eqz v1, :cond_1

    .line 219
    const-string v1, "Babel_medialoader"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "BitmapCache getBitmap evicting: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 220
    invoke-virtual {v0}, Lbzn;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 219
    invoke-static {v1, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    :cond_1
    invoke-virtual {v0}, Lbzn;->c()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 223
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 224
    if-nez v0, :cond_5

    :goto_1
    move-object v1, v0

    .line 225
    goto :goto_0

    .line 213
    :catch_0
    move-exception v0

    const-string v0, "Babel"

    const-string v3, "BitmapCache getBitmap went off the tracks"

    invoke-static {v0, v3}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    :cond_2
    :goto_2
    sget-boolean v0, Lbxs;->a:Z

    if-eqz v0, :cond_3

    .line 230
    const-string v0, "Babel_medialoader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "BitmapCache returnOneBitmapToPool finished evictions cache size="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lbxs;->d:Ljava/util/LinkedHashMap;

    .line 232
    invoke-virtual {v4}, Ljava/util/LinkedHashMap;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " width="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " height="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 230
    invoke-static {v0, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    :cond_3
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 236
    return-object v1

    .line 235
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_4
    move-object v0, v1

    goto :goto_1

    :cond_5
    move-object v1, v0

    goto :goto_2
.end method

.method public a([BIII)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 251
    :try_start_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lbxs;->b([BIII)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 253
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Lbzn;
    .locals 3

    .prologue
    .line 89
    iget-object v1, p0, Lbxs;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 90
    :try_start_0
    iget-object v0, p0, Lbxs;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzn;

    .line 91
    if-nez v0, :cond_0

    .line 92
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lbxs;->a(Z)V

    .line 96
    :goto_0
    monitor-exit v1

    return-object v0

    .line 94
    :cond_0
    invoke-virtual {v0}, Lbzn;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 97
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public a(Ljava/lang/String;Lbzn;)Lbzn;
    .locals 2

    .prologue
    .line 74
    invoke-static {p2}, Lcwz;->b(Ljava/lang/Object;)V

    .line 75
    sget-boolean v0, Lbxs;->a:Z

    if-eqz v0, :cond_0

    .line 76
    invoke-virtual {p2}, Lbzn;->e()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbxs;->b(Landroid/graphics/Bitmap;)Z

    move-result v0

    invoke-static {v0}, Lcwz;->b(Z)V

    .line 77
    iget-object v0, p0, Lbxs;->b:Lbxu;

    invoke-virtual {p2}, Lbzn;->e()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbxu;->b(Landroid/graphics/Bitmap;)Z

    move-result v0

    invoke-static {v0}, Lcwz;->b(Z)V

    .line 79
    :cond_0
    iget-object v1, p0, Lbxs;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 80
    :try_start_0
    iget-object v0, p0, Lbxs;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzn;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 81
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 113
    return-void
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 377
    if-eqz p1, :cond_0

    .line 378
    iget-object v0, p0, Lbxs;->b:Lbxu;

    invoke-virtual {v0, p1}, Lbxu;->a(Landroid/graphics/Bitmap;)V

    .line 380
    :cond_0
    return-void
.end method

.method public b(II)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lbxs;->b:Lbxu;

    invoke-virtual {v0, p1, p2, p0}, Lbxu;->a(IILbxv;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public b([BIII)Landroid/graphics/Bitmap;
    .locals 11

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 271
    if-ltz p2, :cond_3

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 272
    if-ltz p3, :cond_4

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 273
    rem-int/lit8 v0, p4, 0x5a

    if-nez v0, :cond_5

    move v0, v1

    :goto_2
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 275
    rem-int/lit16 v0, p4, 0xb4

    if-eqz v0, :cond_e

    .line 280
    :goto_3
    invoke-static {v6, v6, v6}, Lbxu;->a(ZII)Landroid/graphics/BitmapFactory$Options;

    move-result-object v2

    .line 282
    iput-boolean v1, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 284
    const/4 v0, 0x0

    :try_start_0
    array-length v3, p1

    invoke-static {p1, v0, v3, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 290
    iput-boolean v6, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 292
    iget v3, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 293
    iget v0, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 296
    sget-boolean v4, Lbxs;->a:Z

    if-eqz v4, :cond_0

    .line 297
    const-string v4, "Babel_medialoader"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "BitmapCache.decodeByteArray: bitmap.w="

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " bitmap.h="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", limit.w="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " limit.h="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    :cond_0
    if-gt v3, p3, :cond_1

    if-le v0, p2, :cond_9

    .line 304
    :cond_1
    :goto_4
    div-int/lit8 v4, v3, 0x2

    if-gt v4, p3, :cond_2

    div-int/lit8 v4, v0, 0x2

    if-le v4, p2, :cond_7

    .line 305
    :cond_2
    div-int/lit8 v3, v3, 0x2

    .line 306
    div-int/lit8 v0, v0, 0x2

    .line 307
    shl-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_3
    move v0, v6

    .line 271
    goto :goto_0

    :cond_4
    move v0, v6

    .line 272
    goto :goto_1

    :cond_5
    move v0, v6

    .line 273
    goto :goto_2

    .line 285
    :catch_0
    move-exception v0

    .line 286
    const-string v1, "Babel"

    const-string v2, "can not decode bitmap dimensions"

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 287
    const/4 v0, 0x0

    .line 356
    :cond_6
    :goto_5
    return-object v0

    .line 309
    :cond_7
    sget-boolean v4, Lbxs;->a:Z

    if-eqz v4, :cond_8

    .line 310
    const-string v4, "Babel_medialoader"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "BitmapCache.decodeByteArray: sample size="

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    :cond_8
    iput v1, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    :cond_9
    move v4, v0

    .line 314
    sget-boolean v0, Lbxs;->a:Z

    if-eqz v0, :cond_a

    .line 315
    const-string v0, "Babel_medialoader"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "BitmapCache.decodeByteArray: decode to w="

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " h="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    move v7, v6

    .line 323
    :goto_6
    :try_start_1
    iget-object v0, p0, Lbxs;->b:Lbxu;

    move-object v1, p1

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lbxu;->a([BLandroid/graphics/BitmapFactory$Options;IILbxv;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 338
    if-eqz v0, :cond_6

    rem-int/lit16 v1, p4, 0x168

    if-eqz v1, :cond_6

    move v7, v6

    .line 342
    :goto_7
    :try_start_2
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    int-to-float v1, p4

    int-to-float v2, v3

    div-float/2addr v2, v9

    int-to-float v6, v4

    div-float/2addr v6, v9

    invoke-virtual {v5, v1, v2, v6}, Landroid/graphics/Matrix;->setRotate(FFF)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v6, 0x1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_b

    if-eq v0, v1, :cond_b

    invoke-virtual {p0, v0}, Lbxs;->a(Landroid/graphics/Bitmap;)V
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_2

    :cond_b
    move-object v0, v1

    .line 343
    goto :goto_5

    .line 326
    :catch_1
    move-exception v0

    .line 327
    const-string v1, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "out of memory for decoding a "

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v8, p1

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " sized bitmap"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    add-int/lit8 v1, v7, 0x1

    .line 330
    invoke-direct {p0, v1}, Lbxs;->b(I)Z

    move-result v5

    if-nez v5, :cond_c

    .line 331
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "out of memory: gave up on decoding a "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v3, p1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " sized bitmap"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    throw v0

    :cond_c
    move v7, v1

    .line 335
    goto :goto_6

    .line 344
    :catch_2
    move-exception v1

    .line 345
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "out of memory for rotating a "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v4, p1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " sized bitmap"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    add-int/lit8 v6, v7, 0x1

    .line 348
    invoke-direct {p0, v6}, Lbxs;->b(I)Z

    move-result v2

    if-nez v2, :cond_d

    .line 349
    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "out of memory: gave up on rotating a "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v3, p1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " sized bitmap"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    throw v1

    :cond_d
    move v7, v6

    .line 353
    goto/16 :goto_7

    :cond_e
    move v10, p3

    move p3, p2

    move p2, v10

    goto/16 :goto_3
.end method

.method b()Ljava/util/LinkedHashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lbzn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 399
    iget-object v0, p0, Lbxs;->d:Ljava/util/LinkedHashMap;

    return-object v0
.end method

.method b(Landroid/graphics/Bitmap;)Z
    .locals 3

    .prologue
    .line 384
    iget-object v1, p0, Lbxs;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 385
    :try_start_0
    iget-object v0, p0, Lbxs;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzn;

    .line 386
    invoke-virtual {v0}, Lbzn;->f()Landroid/graphics/Bitmap;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 387
    const/4 v0, 0x1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 391
    :goto_0
    return v0

    .line 390
    :cond_1
    monitor-exit v1

    .line 391
    const/4 v0, 0x0

    goto :goto_0

    .line 390
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method c()Lbxu;
    .locals 1

    .prologue
    .line 403
    iget-object v0, p0, Lbxs;->b:Lbxu;

    return-object v0
.end method
