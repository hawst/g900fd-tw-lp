.class public final Lbxw;
.super Lbxu;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# static fields
.field private static volatile e:I


# instance fields
.field private final f:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lbxx;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/lang/Object;

.field private final h:I

.field private i:I

.field private j:I

.field private k:Z

.field private final l:I

.field private final m:I

.field private n:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    sput v0, Lbxw;->e:I

    return-void
.end method

.method protected constructor <init>(IIIILjava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 75
    invoke-direct {p0, p5}, Lbxu;-><init>(Ljava/lang/String;)V

    .line 50
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lbxw;->g:Ljava/lang/Object;

    .line 58
    iput-boolean v0, p0, Lbxw;->k:Z

    .line 62
    iput v0, p0, Lbxw;->n:I

    .line 76
    iput p1, p0, Lbxw;->h:I

    .line 77
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    iput-object v2, p0, Lbxw;->f:Landroid/util/SparseArray;

    .line 79
    if-ltz p4, :cond_0

    move v0, v1

    :cond_0
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 80
    iput p4, p0, Lbxw;->l:I

    .line 81
    invoke-static {p4, p4}, Lbxw;->b(II)I

    move-result v0

    iput v0, p0, Lbxw;->m:I

    .line 82
    if-eqz p4, :cond_1

    .line 83
    iput-boolean v1, p0, Lbxw;->k:Z

    .line 84
    iput p2, p0, Lbxw;->i:I

    .line 85
    iput p3, p0, Lbxw;->j:I

    .line 87
    :cond_1
    return-void
.end method

.method private a(Landroid/graphics/BitmapFactory$Options;IILbxv;)V
    .locals 1

    .prologue
    .line 214
    iget-boolean v0, p1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    if-eqz v0, :cond_0

    .line 218
    :goto_0
    return-void

    .line 217
    :cond_0
    invoke-direct {p0, p2, p3, p4}, Lbxw;->b(IILbxv;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p1, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method private static b(II)I
    .locals 1

    .prologue
    const v0, 0xffff

    .line 106
    if-gt p0, v0, :cond_0

    if-le p1, v0, :cond_1

    .line 107
    :cond_0
    const/4 v0, 0x0

    .line 109
    :goto_0
    return v0

    :cond_1
    shl-int/lit8 v0, p0, 0x10

    or-int/2addr v0, p1

    goto :goto_0
.end method

.method private b(IILbxv;)Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 127
    invoke-static {p1, p2}, Lbxw;->b(II)I

    move-result v3

    .line 128
    if-eqz v3, :cond_9

    .line 129
    iget-object v4, p0, Lbxw;->g:Ljava/lang/Object;

    monitor-enter v4

    .line 130
    :try_start_0
    iget-boolean v2, p0, Lbxw;->k:Z

    if-eqz v2, :cond_1

    .line 131
    :goto_0
    iget v2, p0, Lbxw;->i:I

    if-ge v0, v2, :cond_0

    .line 132
    iget v2, p0, Lbxw;->l:I

    iget v5, p0, Lbxw;->l:I

    const/4 v6, 0x0

    invoke-super {p0, v2, v5, v6}, Lbxu;->a(IILbxv;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 134
    iget v5, p0, Lbxw;->n:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lbxw;->n:I

    .line 135
    invoke-virtual {p0, v2}, Lbxw;->a(Landroid/graphics/Bitmap;)V

    .line 131
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 137
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbxw;->k:Z

    .line 139
    :cond_1
    iget-object v0, p0, Lbxw;->f:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxx;

    .line 140
    if-eqz v0, :cond_7

    iget v2, v0, Lbxx;->a:I

    if-lez v2, :cond_7

    .line 141
    sget-boolean v2, Lbxw;->b:Z

    if-eqz v2, :cond_2

    .line 142
    const-string v2, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "BitmapPoolICS("

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lbxw;->c:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") found bitmap from pool for size="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 143
    invoke-static {v3}, Lbxw;->b(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " numAvailable="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v0, Lbxx;->a:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " Bitmap="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Lbxx;->b:[Landroid/graphics/Bitmap;

    iget v7, v0, Lbxx;->a:I

    add-int/lit8 v7, v7, -0x1

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 142
    invoke-static {v2, v5}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    :cond_2
    iget v2, v0, Lbxx;->a:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v0, Lbxx;->a:I

    .line 148
    iget-object v2, v0, Lbxx;->b:[Landroid/graphics/Bitmap;

    iget v5, v0, Lbxx;->a:I

    aget-object v2, v2, v5

    .line 149
    iget-object v5, v0, Lbxx;->b:[Landroid/graphics/Bitmap;

    iget v0, v0, Lbxx;->a:I

    const/4 v6, 0x0

    aput-object v6, v5, v0

    move-object v0, v2

    .line 156
    :goto_1
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164
    :goto_2
    if-nez v0, :cond_4

    iget v2, p0, Lbxw;->m:I

    if-ne v3, v2, :cond_4

    .line 165
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v2, "activity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    new-instance v2, Landroid/app/ActivityManager$MemoryInfo;

    invoke-direct {v2}, Landroid/app/ActivityManager$MemoryInfo;-><init>()V

    invoke-virtual {v0, v2}, Landroid/app/ActivityManager;->getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V

    iget-boolean v0, v2, Landroid/app/ActivityManager$MemoryInfo;->lowMemory:Z

    if-nez v0, :cond_3

    iget v0, p0, Lbxw;->n:I

    iget v2, p0, Lbxw;->j:I

    if-lt v0, v2, :cond_b

    :cond_3
    move-object v0, v1

    .line 168
    :cond_4
    :goto_3
    if-nez v0, :cond_5

    if-eqz p3, :cond_5

    .line 169
    invoke-interface {p3, p1, p2}, Lbxv;->a(II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 170
    sget-boolean v1, Lbxw;->b:Z

    if-eqz v1, :cond_5

    .line 171
    if-eqz v0, :cond_c

    .line 172
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "BitmapPoolICS("

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lbxw;->c:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ") found bitmap from provider for size: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 173
    invoke-static {v3}, Lbxw;->b(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 172
    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    :cond_5
    :goto_4
    sget-boolean v1, Lbxw;->b:Z

    if-eqz v1, :cond_6

    .line 181
    const-string v2, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "BitmapPoolICS("

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lbxw;->c:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ") final findPoolBitmap: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 182
    invoke-static {v3}, Lbxw;->b(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " bitmap="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez v0, :cond_d

    const-string v1, "To Be Allocated"

    .line 183
    :goto_5
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 181
    invoke-static {v2, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    :cond_6
    return-object v0

    .line 151
    :cond_7
    :try_start_1
    sget-boolean v0, Lbxw;->b:Z

    if-eqz v0, :cond_8

    .line 152
    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "BitmapPoolICS("

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lbxw;->c:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ") no bitmaps in pool for size: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 153
    invoke-static {v3}, Lbxw;->b(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 152
    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_8
    move-object v0, v1

    goto/16 :goto_1

    .line 156
    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    .line 158
    :cond_9
    sget-boolean v0, Lbxw;->b:Z

    if-eqz v0, :cond_a

    .line 159
    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "BitmapPoolICS("

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lbxw;->c:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ") unsupported size: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "x"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    move-object v0, v1

    goto/16 :goto_2

    .line 165
    :cond_b
    iget v0, p0, Lbxw;->l:I

    iget v2, p0, Lbxw;->l:I

    invoke-super {p0, v0, v2, v1}, Lbxu;->a(IILbxv;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget v1, p0, Lbxw;->n:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lbxw;->n:I

    goto/16 :goto_3

    .line 175
    :cond_c
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "BitmapPoolICS("

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lbxw;->c:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ") no bitmaps in provider for size: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 176
    invoke-static {v3}, Lbxw;->b(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 175
    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 183
    :cond_d
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_5
.end method

.method static b(ZII)Landroid/graphics/BitmapFactory$Options;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 91
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 92
    iput-boolean p0, v0, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 93
    iput p1, v0, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    .line 94
    iput p2, v0, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    .line 95
    iput v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 96
    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 98
    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    .line 99
    return-object v0
.end method

.method private static b(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 116
    ushr-int/lit8 v0, p0, 0x10

    .line 117
    const v1, 0xffff

    and-int/2addr v1, p0

    .line 118
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "x"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(IILbxv;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 294
    invoke-direct {p0, p1, p2, p3}, Lbxw;->b(IILbxv;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 295
    if-nez v0, :cond_1

    .line 296
    invoke-static {p1, p2}, Lbxw;->b(II)I

    move-result v0

    .line 297
    iget v1, p0, Lbxw;->m:I

    if-ne v0, v1, :cond_0

    .line 298
    iget v0, p0, Lbxw;->n:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbxw;->n:I

    .line 300
    :cond_0
    const/4 v0, 0x0

    invoke-super {p0, p1, p2, v0}, Lbxu;->a(IILbxv;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 305
    :goto_0
    return-object v0

    .line 302
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 303
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V

    goto :goto_0
.end method

.method public a(ILandroid/graphics/BitmapFactory$Options;II)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 226
    invoke-static {p2}, Lcwz;->b(Ljava/lang/Object;)V

    .line 227
    invoke-direct {p0, p2, p3, p4, v1}, Lbxw;->a(Landroid/graphics/BitmapFactory$Options;IILbxv;)V

    .line 230
    :try_start_0
    iget-object v0, p0, Lbxw;->a:Landroid/content/res/Resources;

    invoke-static {v0, p1, p2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 246
    :cond_0
    :goto_0
    sget-boolean v2, Lbxw;->b:Z

    if-eqz v2, :cond_1

    .line 247
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "BitmapPoolICS("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lbxw;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") decodeSampledBitmapFromResource ICS for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " bytes gave: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez v0, :cond_3

    .line 249
    :goto_1
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 247
    invoke-static {v2, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    :cond_1
    return-object v0

    .line 233
    :catch_0
    move-exception v0

    iget-object v0, p2, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 234
    iput-object v1, p2, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    .line 235
    iget-object v0, p0, Lbxw;->a:Landroid/content/res/Resources;

    invoke-static {v0, p1, p2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 236
    sget v2, Lbxw;->e:I

    add-int/lit8 v2, v2, 0x1

    .line 237
    sput v2, Lbxw;->e:I

    rem-int/lit8 v2, v2, 0x64

    if-nez v2, :cond_0

    .line 238
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Pooled bitmap consistently not being reused count = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v4, Lbxw;->e:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 243
    :catch_1
    move-exception v0

    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Oom decoding resource "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    sget-object v0, Lcxe;->a:Lcxe;

    invoke-virtual {v0}, Lcxe;->a()V

    :cond_2
    move-object v0, v1

    goto :goto_0

    .line 249
    :cond_3
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public a([BLandroid/graphics/BitmapFactory$Options;IILbxv;)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 260
    invoke-static {p2}, Lcwz;->b(Ljava/lang/Object;)V

    .line 261
    invoke-direct {p0, p2, p3, p4, p5}, Lbxw;->a(Landroid/graphics/BitmapFactory$Options;IILbxv;)V

    .line 264
    const/4 v0, 0x0

    :try_start_0
    array-length v2, p1

    invoke-static {p1, v0, v2, p2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 282
    :cond_0
    :goto_0
    sget-boolean v2, Lbxw;->b:Z

    if-eqz v2, :cond_1

    .line 283
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "BitmapPoolICS("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lbxw;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") decodeByteArray ICS for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v4, p1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " bytes gave: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez v0, :cond_3

    .line 284
    :goto_1
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 283
    invoke-static {v2, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    :cond_1
    return-object v0

    .line 266
    :catch_0
    move-exception v0

    sget-boolean v0, Lbxw;->b:Z

    if-eqz v0, :cond_2

    .line 267
    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "BitmapPoolICS("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lbxw;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") Unable to use pool bitmap"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    :cond_2
    iget-object v0, p2, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_4

    .line 273
    iput-object v1, p2, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    .line 274
    array-length v0, p1

    invoke-static {p1, v4, v0, p2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 275
    sget v2, Lbxw;->e:I

    add-int/lit8 v2, v2, 0x1

    .line 276
    sput v2, Lbxw;->e:I

    rem-int/lit8 v2, v2, 0x64

    if-nez v2, :cond_0

    .line 277
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Pooled bitmap consistently not being reused count = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v4, Lbxw;->e:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 284
    :cond_3
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_4
    move-object v0, v1

    goto/16 :goto_0
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 6

    .prologue
    .line 313
    invoke-static {p1}, Lyn;->a(Landroid/graphics/Bitmap;)Z

    move-result v0

    invoke-static {v0}, Lcwz;->b(Z)V

    .line 314
    sget-boolean v0, Lbxw;->b:Z

    if-eqz v0, :cond_0

    .line 315
    invoke-virtual {p0, p1}, Lbxw;->b(Landroid/graphics/Bitmap;)Z

    move-result v0

    invoke-static {v0}, Lcwz;->b(Z)V

    .line 316
    invoke-static {}, Lbyr;->a()Lbxs;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbxs;->b(Landroid/graphics/Bitmap;)Z

    move-result v0

    invoke-static {v0}, Lcwz;->b(Z)V

    .line 319
    :cond_0
    sget-boolean v0, Lbxw;->b:Z

    if-eqz v0, :cond_1

    .line 320
    const-string v1, "Babel"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "BitmapPoolICS("

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbxw;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") put bitmap b="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez p1, :cond_2

    const/4 v0, 0x0

    .line 321
    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 320
    invoke-static {v1, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    :cond_1
    if-nez p1, :cond_3

    .line 324
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "BitmapPoolICS("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbxw;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") receiving null bitmap"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-static {v0, v1, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 361
    :goto_1
    return-void

    .line 321
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 329
    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Lbxw;->b(II)I

    move-result v2

    .line 330
    if-eqz v2, :cond_4

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v0

    if-nez v0, :cond_5

    .line 332
    :cond_4
    invoke-virtual {p0, p1}, Lbxw;->c(Landroid/graphics/Bitmap;)V

    goto :goto_1

    .line 335
    :cond_5
    iget-object v3, p0, Lbxw;->g:Ljava/lang/Object;

    monitor-enter v3

    .line 336
    :try_start_0
    iget-object v0, p0, Lbxw;->f:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxx;

    .line 337
    if-nez v0, :cond_6

    .line 338
    new-instance v0, Lbxx;

    iget v1, p0, Lbxw;->m:I

    if-ne v2, v1, :cond_8

    iget v1, p0, Lbxw;->i:I

    :goto_2
    invoke-direct {v0, v1}, Lbxx;-><init>(I)V

    .line 340
    iget-object v1, p0, Lbxw;->f:Landroid/util/SparseArray;

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 342
    :cond_6
    iget v1, v0, Lbxx;->a:I

    iget-object v4, v0, Lbxx;->b:[Landroid/graphics/Bitmap;

    array-length v4, v4

    if-ge v1, v4, :cond_9

    .line 343
    iget-object v1, v0, Lbxx;->b:[Landroid/graphics/Bitmap;

    iget v4, v0, Lbxx;->a:I

    aput-object p1, v1, v4

    .line 344
    iget v1, v0, Lbxx;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lbxx;->a:I

    .line 345
    sget-boolean v1, Lbxw;->b:Z

    if-eqz v1, :cond_7

    .line 346
    const-string v1, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "BitmapPoolICS("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lbxw;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") putting bitmap into size pool "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 347
    invoke-static {v2}, Lbxw;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " which now has "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v0, v0, Lbxx;->a:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " items."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 346
    invoke-static {v1, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    :cond_7
    :goto_3
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    .line 338
    :cond_8
    :try_start_1
    iget v1, p0, Lbxw;->h:I

    goto :goto_2

    .line 351
    :cond_9
    sget-boolean v1, Lbxw;->b:Z

    if-eqz v1, :cond_a

    .line 352
    const-string v1, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "BitmapPoolICS("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lbxw;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") tried putting bitmap into size pool "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 353
    invoke-static {v2}, Lbxw;->b(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " but it was full with "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v0, v0, Lbxx;->a:I

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " items."

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 352
    invoke-static {v1, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    :cond_a
    iget v0, p0, Lbxw;->m:I

    if-ne v2, v0, :cond_b

    .line 357
    iget v0, p0, Lbxw;->n:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lbxw;->n:I

    .line 359
    :cond_b
    invoke-virtual {p0, p1}, Lbxw;->c(Landroid/graphics/Bitmap;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3
.end method

.method public a(II)Z
    .locals 3

    .prologue
    .line 387
    invoke-static {p1, p2}, Lbxw;->b(II)I

    move-result v0

    .line 388
    if-nez v0, :cond_0

    .line 390
    invoke-super {p0, p1, p2}, Lbxu;->a(II)Z

    move-result v0

    .line 398
    :goto_0
    return v0

    .line 392
    :cond_0
    iget-object v1, p0, Lbxw;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 393
    :try_start_0
    iget-object v2, p0, Lbxw;->f:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxx;

    .line 394
    if-eqz v0, :cond_1

    iget v2, v0, Lbxx;->a:I

    iget-object v0, v0, Lbxx;->b:[Landroid/graphics/Bitmap;

    array-length v0, v0

    if-ne v2, v0, :cond_1

    .line 396
    const/4 v0, 0x1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 399
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 398
    :cond_1
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0
.end method

.method public b()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 369
    iget-object v4, p0, Lbxw;->g:Ljava/lang/Object;

    monitor-enter v4

    move v3, v2

    .line 370
    :goto_0
    :try_start_0
    iget-object v0, p0, Lbxw;->f:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v3, v0, :cond_1

    .line 371
    iget-object v0, p0, Lbxw;->f:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxx;

    move v1, v2

    .line 372
    :goto_1
    iget v5, v0, Lbxx;->a:I

    if-ge v1, v5, :cond_0

    .line 373
    iget-object v5, v0, Lbxx;->b:[Landroid/graphics/Bitmap;

    aget-object v5, v5, v1

    invoke-virtual {p0, v5}, Lbxw;->c(Landroid/graphics/Bitmap;)V

    .line 374
    iget-object v5, v0, Lbxx;->b:[Landroid/graphics/Bitmap;

    const/4 v6, 0x0

    aput-object v6, v5, v1

    .line 372
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 376
    :cond_0
    const/4 v1, 0x0

    iput v1, v0, Lbxx;->a:I

    .line 370
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 378
    :cond_1
    iget-object v0, p0, Lbxw;->f:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 379
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0
.end method

.method b(Landroid/graphics/Bitmap;)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 404
    if-nez p1, :cond_1

    .line 416
    :cond_0
    :goto_0
    return v2

    .line 407
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Lbxw;->b(II)I

    move-result v0

    .line 408
    iget-object v1, p0, Lbxw;->f:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxx;

    .line 409
    if-eqz v0, :cond_0

    move v1, v2

    .line 410
    :goto_1
    iget v3, v0, Lbxx;->a:I

    if-ge v1, v3, :cond_0

    .line 411
    iget-object v3, v0, Lbxx;->b:[Landroid/graphics/Bitmap;

    aget-object v3, v3, v1

    if-ne v3, p1, :cond_2

    .line 412
    const/4 v2, 0x1

    goto :goto_0

    .line 410
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method c()Landroid/util/SparseArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray",
            "<",
            "Lbxx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 420
    iget-object v0, p0, Lbxw;->f:Landroid/util/SparseArray;

    return-object v0
.end method
