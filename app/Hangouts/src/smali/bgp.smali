.class public Lbgp;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private g:I

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbgq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ldda;)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 1187
    iget-object v0, p1, Ldda;->b:Ldcw;

    invoke-direct {p0, v0}, Lbfz;-><init>(Ldcw;)V

    .line 1188
    iget-object v0, p1, Ldda;->c:Lddp;

    iget-object v0, v0, Lddp;->b:Lddl;

    .line 1189
    sget-boolean v2, Lbfz;->a:Z

    if-eqz v2, :cond_0

    .line 1190
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GetChatAclSettingsResponse.processResponse: retrieved chat ACLs "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Ldda;->c:Lddp;

    iget-object v4, v4, Lddp;->b:Lddl;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1193
    :cond_0
    if-eqz v0, :cond_3

    .line 1194
    iget-object v2, v0, Lddl;->b:Ljava/lang/Integer;

    invoke-static {v2, v1}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v2

    iput v2, p0, Lbgp;->g:I

    .line 1195
    iget-object v2, v0, Lddl;->d:[Lddm;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 1196
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lbgp;->h:Ljava/util/List;

    .line 1197
    iget-object v2, v0, Lddl;->d:[Lddm;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 1198
    new-instance v5, Lbgq;

    invoke-direct {v5}, Lbgq;-><init>()V

    .line 1199
    iget-object v6, v4, Lddm;->c:Ljava/lang/String;

    iput-object v6, v5, Lbgq;->b:Ljava/lang/String;

    .line 1200
    iget-object v6, v4, Lddm;->b:Ljava/lang/String;

    iput-object v6, v5, Lbgq;->a:Ljava/lang/String;

    .line 1201
    iget-object v4, v4, Lddm;->d:Ljava/lang/Integer;

    invoke-static {v4, v1}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v4

    iput v4, v5, Lbgq;->c:I

    .line 1202
    iget-object v4, p0, Lbgp;->h:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1197
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1205
    :cond_1
    iput-object v5, p0, Lbgp;->h:Ljava/util/List;

    .line 1210
    :cond_2
    :goto_1
    return-void

    .line 1208
    :cond_3
    iput-object v5, p0, Lbgp;->h:Ljava/util/List;

    goto :goto_1
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 1214
    new-instance v0, Ldda;

    invoke-direct {v0}, Ldda;-><init>()V

    .line 1215
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldda;

    .line 1216
    iget-object v1, v0, Ldda;->b:Ldcw;

    invoke-static {v1}, Lbgp;->a(Ldcw;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1217
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldda;->b:Ldcw;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldcw;)V

    move-object v0, v1

    .line 1219
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbgp;

    invoke-direct {v1, v0}, Lbgp;-><init>(Ldda;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Lyt;Lbnl;)V
    .locals 3

    .prologue
    .line 1226
    invoke-super {p0, p1, p2}, Lbfz;->a(Lyt;Lbnl;)V

    .line 1227
    invoke-virtual {p1}, Lyt;->f()Lyj;

    move-result-object v0

    iget v1, p0, Lbgp;->g:I

    iget-object v2, p0, Lbgp;->h:Ljava/util/List;

    invoke-static {v0, v1, v2}, Laai;->a(Lyj;ILjava/util/List;)V

    .line 1228
    return-void
.end method
