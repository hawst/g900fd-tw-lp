.class public final Ldwk;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldwk;


# instance fields
.field public b:Ldvm;

.field public c:Ldux;

.field public d:Ldrf;

.field public e:Ldqx;

.field public f:Ldud;

.field public g:Ldta;

.field public h:Ldwz;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14573
    const/4 v0, 0x0

    new-array v0, v0, [Ldwk;

    sput-object v0, Ldwk;->a:[Ldwk;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 14574
    invoke-direct {p0}, Lepn;-><init>()V

    .line 14577
    iput-object v0, p0, Ldwk;->b:Ldvm;

    .line 14580
    iput-object v0, p0, Ldwk;->c:Ldux;

    .line 14583
    iput-object v0, p0, Ldwk;->d:Ldrf;

    .line 14586
    iput-object v0, p0, Ldwk;->e:Ldqx;

    .line 14589
    iput-object v0, p0, Ldwk;->f:Ldud;

    .line 14592
    iput-object v0, p0, Ldwk;->g:Ldta;

    .line 14595
    iput-object v0, p0, Ldwk;->h:Ldwz;

    .line 14574
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 14627
    const/4 v0, 0x0

    .line 14628
    iget-object v1, p0, Ldwk;->b:Ldvm;

    if-eqz v1, :cond_0

    .line 14629
    const/4 v0, 0x1

    iget-object v1, p0, Ldwk;->b:Ldvm;

    .line 14630
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 14632
    :cond_0
    iget-object v1, p0, Ldwk;->c:Ldux;

    if-eqz v1, :cond_1

    .line 14633
    const/4 v1, 0x2

    iget-object v2, p0, Ldwk;->c:Ldux;

    .line 14634
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14636
    :cond_1
    iget-object v1, p0, Ldwk;->d:Ldrf;

    if-eqz v1, :cond_2

    .line 14637
    const/4 v1, 0x3

    iget-object v2, p0, Ldwk;->d:Ldrf;

    .line 14638
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14640
    :cond_2
    iget-object v1, p0, Ldwk;->e:Ldqx;

    if-eqz v1, :cond_3

    .line 14641
    const/4 v1, 0x5

    iget-object v2, p0, Ldwk;->e:Ldqx;

    .line 14642
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14644
    :cond_3
    iget-object v1, p0, Ldwk;->f:Ldud;

    if-eqz v1, :cond_4

    .line 14645
    const/4 v1, 0x6

    iget-object v2, p0, Ldwk;->f:Ldud;

    .line 14646
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14648
    :cond_4
    iget-object v1, p0, Ldwk;->g:Ldta;

    if-eqz v1, :cond_5

    .line 14649
    const/4 v1, 0x7

    iget-object v2, p0, Ldwk;->g:Ldta;

    .line 14650
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14652
    :cond_5
    iget-object v1, p0, Ldwk;->h:Ldwz;

    if-eqz v1, :cond_6

    .line 14653
    const/16 v1, 0x8

    iget-object v2, p0, Ldwk;->h:Ldwz;

    .line 14654
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14656
    :cond_6
    iget-object v1, p0, Ldwk;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14657
    iput v0, p0, Ldwk;->cachedSize:I

    .line 14658
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 14570
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldwk;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldwk;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldwk;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldwk;->b:Ldvm;

    if-nez v0, :cond_2

    new-instance v0, Ldvm;

    invoke-direct {v0}, Ldvm;-><init>()V

    iput-object v0, p0, Ldwk;->b:Ldvm;

    :cond_2
    iget-object v0, p0, Ldwk;->b:Ldvm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldwk;->c:Ldux;

    if-nez v0, :cond_3

    new-instance v0, Ldux;

    invoke-direct {v0}, Ldux;-><init>()V

    iput-object v0, p0, Ldwk;->c:Ldux;

    :cond_3
    iget-object v0, p0, Ldwk;->c:Ldux;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldwk;->d:Ldrf;

    if-nez v0, :cond_4

    new-instance v0, Ldrf;

    invoke-direct {v0}, Ldrf;-><init>()V

    iput-object v0, p0, Ldwk;->d:Ldrf;

    :cond_4
    iget-object v0, p0, Ldwk;->d:Ldrf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldwk;->e:Ldqx;

    if-nez v0, :cond_5

    new-instance v0, Ldqx;

    invoke-direct {v0}, Ldqx;-><init>()V

    iput-object v0, p0, Ldwk;->e:Ldqx;

    :cond_5
    iget-object v0, p0, Ldwk;->e:Ldqx;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ldwk;->f:Ldud;

    if-nez v0, :cond_6

    new-instance v0, Ldud;

    invoke-direct {v0}, Ldud;-><init>()V

    iput-object v0, p0, Ldwk;->f:Ldud;

    :cond_6
    iget-object v0, p0, Ldwk;->f:Ldud;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Ldwk;->g:Ldta;

    if-nez v0, :cond_7

    new-instance v0, Ldta;

    invoke-direct {v0}, Ldta;-><init>()V

    iput-object v0, p0, Ldwk;->g:Ldta;

    :cond_7
    iget-object v0, p0, Ldwk;->g:Ldta;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Ldwk;->h:Ldwz;

    if-nez v0, :cond_8

    new-instance v0, Ldwz;

    invoke-direct {v0}, Ldwz;-><init>()V

    iput-object v0, p0, Ldwk;->h:Ldwz;

    :cond_8
    iget-object v0, p0, Ldwk;->h:Ldwz;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 14600
    iget-object v0, p0, Ldwk;->b:Ldvm;

    if-eqz v0, :cond_0

    .line 14601
    const/4 v0, 0x1

    iget-object v1, p0, Ldwk;->b:Ldvm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 14603
    :cond_0
    iget-object v0, p0, Ldwk;->c:Ldux;

    if-eqz v0, :cond_1

    .line 14604
    const/4 v0, 0x2

    iget-object v1, p0, Ldwk;->c:Ldux;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 14606
    :cond_1
    iget-object v0, p0, Ldwk;->d:Ldrf;

    if-eqz v0, :cond_2

    .line 14607
    const/4 v0, 0x3

    iget-object v1, p0, Ldwk;->d:Ldrf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 14609
    :cond_2
    iget-object v0, p0, Ldwk;->e:Ldqx;

    if-eqz v0, :cond_3

    .line 14610
    const/4 v0, 0x5

    iget-object v1, p0, Ldwk;->e:Ldqx;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 14612
    :cond_3
    iget-object v0, p0, Ldwk;->f:Ldud;

    if-eqz v0, :cond_4

    .line 14613
    const/4 v0, 0x6

    iget-object v1, p0, Ldwk;->f:Ldud;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 14615
    :cond_4
    iget-object v0, p0, Ldwk;->g:Ldta;

    if-eqz v0, :cond_5

    .line 14616
    const/4 v0, 0x7

    iget-object v1, p0, Ldwk;->g:Ldta;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 14618
    :cond_5
    iget-object v0, p0, Ldwk;->h:Ldwz;

    if-eqz v0, :cond_6

    .line 14619
    const/16 v0, 0x8

    iget-object v1, p0, Ldwk;->h:Ldwz;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 14621
    :cond_6
    iget-object v0, p0, Ldwk;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 14623
    return-void
.end method
