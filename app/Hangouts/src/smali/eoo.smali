.class public final Leoo;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leoo;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Leml;

.field public d:Ljava/lang/Integer;

.field public e:Leop;

.field public f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Leoo;

    sput-object v0, Leoo;->a:[Leoo;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 24
    iput-object v0, p0, Leoo;->c:Leml;

    .line 27
    iput-object v0, p0, Leoo;->d:Ljava/lang/Integer;

    .line 30
    iput-object v0, p0, Leoo;->e:Leop;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 58
    const/4 v0, 0x0

    .line 59
    iget-object v1, p0, Leoo;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 60
    const/4 v0, 0x1

    iget-object v1, p0, Leoo;->b:Ljava/lang/String;

    .line 61
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 63
    :cond_0
    iget-object v1, p0, Leoo;->c:Leml;

    if-eqz v1, :cond_1

    .line 64
    const/4 v1, 0x2

    iget-object v2, p0, Leoo;->c:Leml;

    .line 65
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 67
    :cond_1
    iget-object v1, p0, Leoo;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 68
    const/4 v1, 0x3

    iget-object v2, p0, Leoo;->d:Ljava/lang/Integer;

    .line 69
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 71
    :cond_2
    iget-object v1, p0, Leoo;->e:Leop;

    if-eqz v1, :cond_3

    .line 72
    const/4 v1, 0x4

    iget-object v2, p0, Leoo;->e:Leop;

    .line 73
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 75
    :cond_3
    iget-object v1, p0, Leoo;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 76
    const/4 v1, 0x5

    iget-object v2, p0, Leoo;->f:Ljava/lang/String;

    .line 77
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 79
    :cond_4
    iget-object v1, p0, Leoo;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 80
    iput v0, p0, Leoo;->cachedSize:I

    .line 81
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Leoo;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Leoo;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Leoo;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leoo;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Leoo;->c:Leml;

    if-nez v0, :cond_2

    new-instance v0, Leml;

    invoke-direct {v0}, Leml;-><init>()V

    iput-object v0, p0, Leoo;->c:Leml;

    :cond_2
    iget-object v0, p0, Leoo;->c:Leml;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x5

    if-eq v0, v1, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leoo;->d:Ljava/lang/Integer;

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leoo;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Leoo;->e:Leop;

    if-nez v0, :cond_5

    new-instance v0, Leop;

    invoke-direct {v0}, Leop;-><init>()V

    iput-object v0, p0, Leoo;->e:Leop;

    :cond_5
    iget-object v0, p0, Leoo;->e:Leop;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leoo;->f:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Leoo;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 38
    const/4 v0, 0x1

    iget-object v1, p0, Leoo;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 40
    :cond_0
    iget-object v0, p0, Leoo;->c:Leml;

    if-eqz v0, :cond_1

    .line 41
    const/4 v0, 0x2

    iget-object v1, p0, Leoo;->c:Leml;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 43
    :cond_1
    iget-object v0, p0, Leoo;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 44
    const/4 v0, 0x3

    iget-object v1, p0, Leoo;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 46
    :cond_2
    iget-object v0, p0, Leoo;->e:Leop;

    if-eqz v0, :cond_3

    .line 47
    const/4 v0, 0x4

    iget-object v1, p0, Leoo;->e:Leop;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 49
    :cond_3
    iget-object v0, p0, Leoo;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 50
    const/4 v0, 0x5

    iget-object v1, p0, Leoo;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 52
    :cond_4
    iget-object v0, p0, Leoo;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 54
    return-void
.end method
