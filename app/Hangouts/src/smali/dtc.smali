.class public final Ldtc;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldtc;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Long;

.field public d:[Ldqr;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9542
    const/4 v0, 0x0

    new-array v0, v0, [Ldtc;

    sput-object v0, Ldtc;->a:[Ldtc;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9543
    invoke-direct {p0}, Lepn;-><init>()V

    .line 9550
    sget-object v0, Ldqr;->a:[Ldqr;

    iput-object v0, p0, Ldtc;->d:[Ldqr;

    .line 9543
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 9575
    iget-object v0, p0, Ldtc;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 9576
    const/4 v0, 0x1

    iget-object v2, p0, Ldtc;->b:Ljava/lang/Integer;

    .line 9577
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Lepl;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 9579
    :goto_0
    iget-object v2, p0, Ldtc;->c:Ljava/lang/Long;

    if-eqz v2, :cond_0

    .line 9580
    const/4 v2, 0x2

    iget-object v3, p0, Ldtc;->c:Ljava/lang/Long;

    .line 9581
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 9583
    :cond_0
    iget-object v2, p0, Ldtc;->d:[Ldqr;

    if-eqz v2, :cond_2

    .line 9584
    iget-object v2, p0, Ldtc;->d:[Ldqr;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 9585
    if-eqz v4, :cond_1

    .line 9586
    const/4 v5, 0x3

    .line 9587
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 9584
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 9591
    :cond_2
    iget-object v1, p0, Ldtc;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9592
    iput v0, p0, Ldtc;->cachedSize:I

    .line 9593
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 9539
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldtc;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldtc;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldtc;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->l()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldtc;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldtc;->c:Ljava/lang/Long;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldtc;->d:[Ldqr;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldqr;

    iget-object v3, p0, Ldtc;->d:[Ldqr;

    if-eqz v3, :cond_2

    iget-object v3, p0, Ldtc;->d:[Ldqr;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Ldtc;->d:[Ldqr;

    :goto_2
    iget-object v2, p0, Ldtc;->d:[Ldqr;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Ldtc;->d:[Ldqr;

    new-instance v3, Ldqr;

    invoke-direct {v3}, Ldqr;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldtc;->d:[Ldqr;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ldtc;->d:[Ldqr;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Ldtc;->d:[Ldqr;

    new-instance v3, Ldqr;

    invoke-direct {v3}, Ldqr;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldtc;->d:[Ldqr;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 9555
    iget-object v0, p0, Ldtc;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 9556
    const/4 v0, 0x1

    iget-object v1, p0, Ldtc;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->c(II)V

    .line 9558
    :cond_0
    iget-object v0, p0, Ldtc;->c:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 9559
    const/4 v0, 0x2

    iget-object v1, p0, Ldtc;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 9561
    :cond_1
    iget-object v0, p0, Ldtc;->d:[Ldqr;

    if-eqz v0, :cond_3

    .line 9562
    iget-object v1, p0, Ldtc;->d:[Ldqr;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 9563
    if-eqz v3, :cond_2

    .line 9564
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 9562
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 9568
    :cond_3
    iget-object v0, p0, Ldtc;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 9570
    return-void
.end method
