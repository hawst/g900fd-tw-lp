.class final Lzy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lvg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lvg",
        "<",
        "Lbsm;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:J

.field final synthetic b:Lbyu;

.field final synthetic c:I

.field final synthetic d:Lbsv;

.field final synthetic e:Lzx;


# direct methods
.method constructor <init>(Lzx;JLbyu;ILbsv;)V
    .locals 0

    .prologue
    .line 226
    iput-object p1, p0, Lzy;->e:Lzx;

    iput-wide p2, p0, Lzy;->a:J

    iput-object p4, p0, Lzy;->b:Lbyu;

    iput p5, p0, Lzy;->c:I

    iput-object p6, p0, Lzy;->d:Lbsv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic a(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 226
    check-cast p1, Lbsm;

    invoke-virtual {p1}, Lbsm;->a()[B

    move-result-object v1

    invoke-static {}, Lzx;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lzy;->a:J

    sub-long v4, v2, v4

    const-string v6, "Babel_medialoader"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, "Volley: url="

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lzy;->b:Lbyu;

    invoke-virtual {v7}, Lbyu;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, ", entryCount= "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v7, p0, Lzy;->c:I

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, " received="

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-eqz v1, :cond_2

    array-length v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, ", clock="

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", dur="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "ms"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    if-eqz v1, :cond_1

    array-length v0, v1

    if-nez v0, :cond_3

    :cond_1
    iget-object v0, p0, Lzy;->b:Lbyu;

    invoke-virtual {v0}, Lbyu;->p()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lavk;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lzy;->e:Lzx;

    iget-object v1, p0, Lzy;->b:Lbyu;

    iget v2, p0, Lzy;->c:I

    add-int/lit8 v2, v2, 0x1

    invoke-static {v0, v1, v2}, Lzx;->a(Lzx;Lbyu;I)Z

    :goto_1
    return-void

    :cond_2
    const-string v0, "null"

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lzy;->d:Lbsv;

    invoke-static {v0, p1}, Lbsn;->a(Lbsv;Lbsm;)V

    goto :goto_1
.end method
