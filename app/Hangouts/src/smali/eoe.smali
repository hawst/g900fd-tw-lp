.class public final Leoe;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leoe;


# instance fields
.field public b:Ljava/lang/String;

.field public c:[Lens;

.field public d:[Lelf;

.field public e:[Lemp;

.field public f:[Leob;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 233
    const/4 v0, 0x0

    new-array v0, v0, [Leoe;

    sput-object v0, Leoe;->a:[Leoe;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 234
    invoke-direct {p0}, Lepn;-><init>()V

    .line 239
    sget-object v0, Lens;->a:[Lens;

    iput-object v0, p0, Leoe;->c:[Lens;

    .line 242
    sget-object v0, Lelf;->a:[Lelf;

    iput-object v0, p0, Leoe;->d:[Lelf;

    .line 245
    sget-object v0, Lemp;->a:[Lemp;

    iput-object v0, p0, Leoe;->e:[Lemp;

    .line 248
    sget-object v0, Leob;->a:[Leob;

    iput-object v0, p0, Leoe;->f:[Leob;

    .line 234
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 291
    iget-object v0, p0, Leoe;->b:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 292
    const/4 v0, 0x1

    iget-object v2, p0, Leoe;->b:Ljava/lang/String;

    .line 293
    invoke-static {v0, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 295
    :goto_0
    iget-object v2, p0, Leoe;->c:[Lens;

    if-eqz v2, :cond_1

    .line 296
    iget-object v3, p0, Leoe;->c:[Lens;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 297
    if-eqz v5, :cond_0

    .line 298
    const/4 v6, 0x2

    .line 299
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 296
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 303
    :cond_1
    iget-object v2, p0, Leoe;->d:[Lelf;

    if-eqz v2, :cond_3

    .line 304
    iget-object v3, p0, Leoe;->d:[Lelf;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    .line 305
    if-eqz v5, :cond_2

    .line 306
    const/4 v6, 0x3

    .line 307
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 304
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 311
    :cond_3
    iget-object v2, p0, Leoe;->e:[Lemp;

    if-eqz v2, :cond_5

    .line 312
    iget-object v3, p0, Leoe;->e:[Lemp;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_5

    aget-object v5, v3, v2

    .line 313
    if-eqz v5, :cond_4

    .line 314
    const/4 v6, 0x4

    .line 315
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 312
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 319
    :cond_5
    iget-object v2, p0, Leoe;->f:[Leob;

    if-eqz v2, :cond_7

    .line 320
    iget-object v2, p0, Leoe;->f:[Leob;

    array-length v3, v2

    :goto_4
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    .line 321
    if-eqz v4, :cond_6

    .line 322
    const/4 v5, 0x5

    .line 323
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 320
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 327
    :cond_7
    iget-object v1, p0, Leoe;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 328
    iput v0, p0, Leoe;->cachedSize:I

    .line 329
    return v0

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 230
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Leoe;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Leoe;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Leoe;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leoe;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leoe;->c:[Lens;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lens;

    iget-object v3, p0, Leoe;->c:[Lens;

    if-eqz v3, :cond_2

    iget-object v3, p0, Leoe;->c:[Lens;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Leoe;->c:[Lens;

    :goto_2
    iget-object v2, p0, Leoe;->c:[Lens;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Leoe;->c:[Lens;

    new-instance v3, Lens;

    invoke-direct {v3}, Lens;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leoe;->c:[Lens;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Leoe;->c:[Lens;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Leoe;->c:[Lens;

    new-instance v3, Lens;

    invoke-direct {v3}, Lens;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leoe;->c:[Lens;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leoe;->d:[Lelf;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lelf;

    iget-object v3, p0, Leoe;->d:[Lelf;

    if-eqz v3, :cond_5

    iget-object v3, p0, Leoe;->d:[Lelf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iput-object v2, p0, Leoe;->d:[Lelf;

    :goto_4
    iget-object v2, p0, Leoe;->d:[Lelf;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Leoe;->d:[Lelf;

    new-instance v3, Lelf;

    invoke-direct {v3}, Lelf;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leoe;->d:[Lelf;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Leoe;->d:[Lelf;

    array-length v0, v0

    goto :goto_3

    :cond_7
    iget-object v2, p0, Leoe;->d:[Lelf;

    new-instance v3, Lelf;

    invoke-direct {v3}, Lelf;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leoe;->d:[Lelf;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leoe;->e:[Lemp;

    if-nez v0, :cond_9

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lemp;

    iget-object v3, p0, Leoe;->e:[Lemp;

    if-eqz v3, :cond_8

    iget-object v3, p0, Leoe;->e:[Lemp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    iput-object v2, p0, Leoe;->e:[Lemp;

    :goto_6
    iget-object v2, p0, Leoe;->e:[Lemp;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    iget-object v2, p0, Leoe;->e:[Lemp;

    new-instance v3, Lemp;

    invoke-direct {v3}, Lemp;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leoe;->e:[Lemp;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_9
    iget-object v0, p0, Leoe;->e:[Lemp;

    array-length v0, v0

    goto :goto_5

    :cond_a
    iget-object v2, p0, Leoe;->e:[Lemp;

    new-instance v3, Lemp;

    invoke-direct {v3}, Lemp;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leoe;->e:[Lemp;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leoe;->f:[Leob;

    if-nez v0, :cond_c

    move v0, v1

    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Leob;

    iget-object v3, p0, Leoe;->f:[Leob;

    if-eqz v3, :cond_b

    iget-object v3, p0, Leoe;->f:[Leob;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_b
    iput-object v2, p0, Leoe;->f:[Leob;

    :goto_8
    iget-object v2, p0, Leoe;->f:[Leob;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_d

    iget-object v2, p0, Leoe;->f:[Leob;

    new-instance v3, Leob;

    invoke-direct {v3}, Leob;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leoe;->f:[Leob;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_c
    iget-object v0, p0, Leoe;->f:[Leob;

    array-length v0, v0

    goto :goto_7

    :cond_d
    iget-object v2, p0, Leoe;->f:[Leob;

    new-instance v3, Leob;

    invoke-direct {v3}, Leob;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leoe;->f:[Leob;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 253
    iget-object v1, p0, Leoe;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 254
    const/4 v1, 0x1

    iget-object v2, p0, Leoe;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 256
    :cond_0
    iget-object v1, p0, Leoe;->c:[Lens;

    if-eqz v1, :cond_2

    .line 257
    iget-object v2, p0, Leoe;->c:[Lens;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 258
    if-eqz v4, :cond_1

    .line 259
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 257
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 263
    :cond_2
    iget-object v1, p0, Leoe;->d:[Lelf;

    if-eqz v1, :cond_4

    .line 264
    iget-object v2, p0, Leoe;->d:[Lelf;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 265
    if-eqz v4, :cond_3

    .line 266
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 264
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 270
    :cond_4
    iget-object v1, p0, Leoe;->e:[Lemp;

    if-eqz v1, :cond_6

    .line 271
    iget-object v2, p0, Leoe;->e:[Lemp;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 272
    if-eqz v4, :cond_5

    .line 273
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 271
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 277
    :cond_6
    iget-object v1, p0, Leoe;->f:[Leob;

    if-eqz v1, :cond_8

    .line 278
    iget-object v1, p0, Leoe;->f:[Leob;

    array-length v2, v1

    :goto_3
    if-ge v0, v2, :cond_8

    aget-object v3, v1, v0

    .line 279
    if-eqz v3, :cond_7

    .line 280
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 278
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 284
    :cond_8
    iget-object v0, p0, Leoe;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 286
    return-void
.end method
