.class public final Lws;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field private a:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static varargs a(Ljava/lang/String;[F)Lws;
    .locals 3

    .prologue
    .line 50
    new-instance v0, Lws;

    invoke-direct {v0}, Lws;-><init>()V

    .line 51
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_0

    .line 52
    invoke-static {p0, p1}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    iput-object v1, v0, Lws;->a:Ljava/lang/Object;

    .line 58
    :goto_0
    return-object v0

    .line 55
    :cond_0
    invoke-static {p0, p1}, Lxh;->a(Ljava/lang/String;[F)Lxh;

    move-result-object v1

    iput-object v1, v0, Lws;->a:Ljava/lang/Object;

    goto :goto_0
.end method

.method public static varargs a(Ljava/lang/String;[I)Lws;
    .locals 3

    .prologue
    .line 23
    new-instance v0, Lws;

    invoke-direct {v0}, Lws;-><init>()V

    .line 24
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_0

    .line 25
    invoke-static {p0, p1}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    iput-object v1, v0, Lws;->a:Ljava/lang/Object;

    .line 31
    :goto_0
    return-object v0

    .line 28
    :cond_0
    invoke-static {p0, p1}, Lxh;->a(Ljava/lang/String;[I)Lxh;

    move-result-object v1

    iput-object v1, v0, Lws;->a:Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lws;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 7
    new-instance v0, Ljava/lang/CloneNotSupportedException;

    invoke-direct {v0}, Ljava/lang/CloneNotSupportedException;-><init>()V

    throw v0
.end method
