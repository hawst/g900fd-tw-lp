.class public final Lesg;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lesg;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/String;

.field public d:Lesd;

.field public e:Lesf;

.field public f:Lesi;

.field public g:Lese;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x0

    new-array v0, v0, [Lesg;

    sput-object v0, Lesg;->a:[Lesg;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 140
    invoke-direct {p0}, Lepn;-><init>()V

    .line 143
    iput-object v0, p0, Lesg;->b:Ljava/lang/Integer;

    .line 148
    iput-object v0, p0, Lesg;->d:Lesd;

    .line 151
    iput-object v0, p0, Lesg;->e:Lesf;

    .line 154
    iput-object v0, p0, Lesg;->f:Lesi;

    .line 157
    iput-object v0, p0, Lesg;->g:Lese;

    .line 140
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 186
    const/4 v0, 0x0

    .line 187
    iget-object v1, p0, Lesg;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 188
    const/4 v0, 0x1

    iget-object v1, p0, Lesg;->b:Ljava/lang/Integer;

    .line 189
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 191
    :cond_0
    iget-object v1, p0, Lesg;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 192
    const/4 v1, 0x2

    iget-object v2, p0, Lesg;->c:Ljava/lang/String;

    .line 193
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 195
    :cond_1
    iget-object v1, p0, Lesg;->d:Lesd;

    if-eqz v1, :cond_2

    .line 196
    const/4 v1, 0x3

    iget-object v2, p0, Lesg;->d:Lesd;

    .line 197
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 199
    :cond_2
    iget-object v1, p0, Lesg;->e:Lesf;

    if-eqz v1, :cond_3

    .line 200
    const/4 v1, 0x4

    iget-object v2, p0, Lesg;->e:Lesf;

    .line 201
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 203
    :cond_3
    iget-object v1, p0, Lesg;->f:Lesi;

    if-eqz v1, :cond_4

    .line 204
    const/4 v1, 0x5

    iget-object v2, p0, Lesg;->f:Lesi;

    .line 205
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 207
    :cond_4
    iget-object v1, p0, Lesg;->g:Lese;

    if-eqz v1, :cond_5

    .line 208
    const/4 v1, 0x6

    iget-object v2, p0, Lesg;->g:Lese;

    .line 209
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 211
    :cond_5
    iget-object v1, p0, Lesg;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 212
    iput v0, p0, Lesg;->cachedSize:I

    .line 213
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 136
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lesg;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lesg;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lesg;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lesg;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lesg;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lesg;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lesg;->d:Lesd;

    if-nez v0, :cond_4

    new-instance v0, Lesd;

    invoke-direct {v0}, Lesd;-><init>()V

    iput-object v0, p0, Lesg;->d:Lesd;

    :cond_4
    iget-object v0, p0, Lesg;->d:Lesd;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lesg;->e:Lesf;

    if-nez v0, :cond_5

    new-instance v0, Lesf;

    invoke-direct {v0}, Lesf;-><init>()V

    iput-object v0, p0, Lesg;->e:Lesf;

    :cond_5
    iget-object v0, p0, Lesg;->e:Lesf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lesg;->f:Lesi;

    if-nez v0, :cond_6

    new-instance v0, Lesi;

    invoke-direct {v0}, Lesi;-><init>()V

    iput-object v0, p0, Lesg;->f:Lesi;

    :cond_6
    iget-object v0, p0, Lesg;->f:Lesi;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lesg;->g:Lese;

    if-nez v0, :cond_7

    new-instance v0, Lese;

    invoke-direct {v0}, Lese;-><init>()V

    iput-object v0, p0, Lesg;->g:Lese;

    :cond_7
    iget-object v0, p0, Lesg;->g:Lese;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Lesg;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 163
    const/4 v0, 0x1

    iget-object v1, p0, Lesg;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 165
    :cond_0
    iget-object v0, p0, Lesg;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 166
    const/4 v0, 0x2

    iget-object v1, p0, Lesg;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 168
    :cond_1
    iget-object v0, p0, Lesg;->d:Lesd;

    if-eqz v0, :cond_2

    .line 169
    const/4 v0, 0x3

    iget-object v1, p0, Lesg;->d:Lesd;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 171
    :cond_2
    iget-object v0, p0, Lesg;->e:Lesf;

    if-eqz v0, :cond_3

    .line 172
    const/4 v0, 0x4

    iget-object v1, p0, Lesg;->e:Lesf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 174
    :cond_3
    iget-object v0, p0, Lesg;->f:Lesi;

    if-eqz v0, :cond_4

    .line 175
    const/4 v0, 0x5

    iget-object v1, p0, Lesg;->f:Lesi;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 177
    :cond_4
    iget-object v0, p0, Lesg;->g:Lese;

    if-eqz v0, :cond_5

    .line 178
    const/4 v0, 0x6

    iget-object v1, p0, Lesg;->g:Lese;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 180
    :cond_5
    iget-object v0, p0, Lesg;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 182
    return-void
.end method
