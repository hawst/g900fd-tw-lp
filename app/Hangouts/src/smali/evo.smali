.class public final Levo;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Levo;


# instance fields
.field public b:Levm;

.field public c:Levm;

.field public d:Levu;

.field public e:Levu;

.field public f:Levl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 307
    const/4 v0, 0x0

    new-array v0, v0, [Levo;

    sput-object v0, Levo;->a:[Levo;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 308
    invoke-direct {p0}, Lepn;-><init>()V

    .line 311
    iput-object v0, p0, Levo;->b:Levm;

    .line 314
    iput-object v0, p0, Levo;->c:Levm;

    .line 317
    iput-object v0, p0, Levo;->d:Levu;

    .line 320
    iput-object v0, p0, Levo;->e:Levu;

    .line 323
    iput-object v0, p0, Levo;->f:Levl;

    .line 308
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 349
    const/4 v0, 0x0

    .line 350
    iget-object v1, p0, Levo;->b:Levm;

    if-eqz v1, :cond_0

    .line 351
    const/4 v0, 0x1

    iget-object v1, p0, Levo;->b:Levm;

    .line 352
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 354
    :cond_0
    iget-object v1, p0, Levo;->c:Levm;

    if-eqz v1, :cond_1

    .line 355
    const/4 v1, 0x2

    iget-object v2, p0, Levo;->c:Levm;

    .line 356
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 358
    :cond_1
    iget-object v1, p0, Levo;->e:Levu;

    if-eqz v1, :cond_2

    .line 359
    const/4 v1, 0x3

    iget-object v2, p0, Levo;->e:Levu;

    .line 360
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 362
    :cond_2
    iget-object v1, p0, Levo;->f:Levl;

    if-eqz v1, :cond_3

    .line 363
    const/4 v1, 0x4

    iget-object v2, p0, Levo;->f:Levl;

    .line 364
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 366
    :cond_3
    iget-object v1, p0, Levo;->d:Levu;

    if-eqz v1, :cond_4

    .line 367
    const/4 v1, 0x5

    iget-object v2, p0, Levo;->d:Levu;

    .line 368
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 370
    :cond_4
    iget-object v1, p0, Levo;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 371
    iput v0, p0, Levo;->cachedSize:I

    .line 372
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 304
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Levo;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Levo;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Levo;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Levo;->b:Levm;

    if-nez v0, :cond_2

    new-instance v0, Levm;

    invoke-direct {v0}, Levm;-><init>()V

    iput-object v0, p0, Levo;->b:Levm;

    :cond_2
    iget-object v0, p0, Levo;->b:Levm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Levo;->c:Levm;

    if-nez v0, :cond_3

    new-instance v0, Levm;

    invoke-direct {v0}, Levm;-><init>()V

    iput-object v0, p0, Levo;->c:Levm;

    :cond_3
    iget-object v0, p0, Levo;->c:Levm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Levo;->e:Levu;

    if-nez v0, :cond_4

    new-instance v0, Levu;

    invoke-direct {v0}, Levu;-><init>()V

    iput-object v0, p0, Levo;->e:Levu;

    :cond_4
    iget-object v0, p0, Levo;->e:Levu;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Levo;->f:Levl;

    if-nez v0, :cond_5

    new-instance v0, Levl;

    invoke-direct {v0}, Levl;-><init>()V

    iput-object v0, p0, Levo;->f:Levl;

    :cond_5
    iget-object v0, p0, Levo;->f:Levl;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Levo;->d:Levu;

    if-nez v0, :cond_6

    new-instance v0, Levu;

    invoke-direct {v0}, Levu;-><init>()V

    iput-object v0, p0, Levo;->d:Levu;

    :cond_6
    iget-object v0, p0, Levo;->d:Levu;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 328
    iget-object v0, p0, Levo;->b:Levm;

    if-eqz v0, :cond_0

    .line 329
    const/4 v0, 0x1

    iget-object v1, p0, Levo;->b:Levm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 331
    :cond_0
    iget-object v0, p0, Levo;->c:Levm;

    if-eqz v0, :cond_1

    .line 332
    const/4 v0, 0x2

    iget-object v1, p0, Levo;->c:Levm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 334
    :cond_1
    iget-object v0, p0, Levo;->e:Levu;

    if-eqz v0, :cond_2

    .line 335
    const/4 v0, 0x3

    iget-object v1, p0, Levo;->e:Levu;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 337
    :cond_2
    iget-object v0, p0, Levo;->f:Levl;

    if-eqz v0, :cond_3

    .line 338
    const/4 v0, 0x4

    iget-object v1, p0, Levo;->f:Levl;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 340
    :cond_3
    iget-object v0, p0, Levo;->d:Levu;

    if-eqz v0, :cond_4

    .line 341
    const/4 v0, 0x5

    iget-object v1, p0, Levo;->d:Levu;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 343
    :cond_4
    iget-object v0, p0, Levo;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 345
    return-void
.end method
