.class public final Ldhu;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldhu;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ldhv;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Ldhu;

    sput-object v0, Ldhu;->a:[Ldhu;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 95
    iput-object v0, p0, Ldhu;->b:Ljava/lang/Integer;

    .line 98
    iput-object v0, p0, Ldhu;->c:Ldhv;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 115
    const/4 v0, 0x0

    .line 116
    iget-object v1, p0, Ldhu;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 117
    const/4 v0, 0x1

    iget-object v1, p0, Ldhu;->b:Ljava/lang/Integer;

    .line 118
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 120
    :cond_0
    iget-object v1, p0, Ldhu;->c:Ldhv;

    if-eqz v1, :cond_1

    .line 121
    const/4 v1, 0x2

    iget-object v2, p0, Ldhu;->c:Ldhv;

    .line 122
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 124
    :cond_1
    iget-object v1, p0, Ldhu;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 125
    iput v0, p0, Ldhu;->cachedSize:I

    .line 126
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldhu;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldhu;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldhu;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldhu;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldhu;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldhu;->c:Ldhv;

    if-nez v0, :cond_4

    new-instance v0, Ldhv;

    invoke-direct {v0}, Ldhv;-><init>()V

    iput-object v0, p0, Ldhu;->c:Ldhv;

    :cond_4
    iget-object v0, p0, Ldhu;->c:Ldhv;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Ldhu;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 104
    const/4 v0, 0x1

    iget-object v1, p0, Ldhu;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 106
    :cond_0
    iget-object v0, p0, Ldhu;->c:Ldhv;

    if-eqz v0, :cond_1

    .line 107
    const/4 v0, 0x2

    iget-object v1, p0, Ldhu;->c:Ldhv;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 109
    :cond_1
    iget-object v0, p0, Ldhu;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 111
    return-void
.end method
