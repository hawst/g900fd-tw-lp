.class public final Ldji;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldji;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ldkj;

.field public d:Ldjj;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 252
    const/4 v0, 0x0

    new-array v0, v0, [Ldji;

    sput-object v0, Ldji;->a:[Ldji;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 253
    invoke-direct {p0}, Lepn;-><init>()V

    .line 258
    iput-object v0, p0, Ldji;->c:Ldkj;

    .line 261
    iput-object v0, p0, Ldji;->d:Ldjj;

    .line 253
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 281
    const/4 v0, 0x0

    .line 282
    iget-object v1, p0, Ldji;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 283
    const/4 v0, 0x2

    iget-object v1, p0, Ldji;->b:Ljava/lang/String;

    .line 284
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 286
    :cond_0
    iget-object v1, p0, Ldji;->c:Ldkj;

    if-eqz v1, :cond_1

    .line 287
    const/4 v1, 0x3

    iget-object v2, p0, Ldji;->c:Ldkj;

    .line 288
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 290
    :cond_1
    iget-object v1, p0, Ldji;->d:Ldjj;

    if-eqz v1, :cond_2

    .line 291
    const/4 v1, 0x4

    iget-object v2, p0, Ldji;->d:Ldjj;

    .line 292
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 294
    :cond_2
    iget-object v1, p0, Ldji;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 295
    iput v0, p0, Ldji;->cachedSize:I

    .line 296
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 249
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldji;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldji;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldji;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldji;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldji;->c:Ldkj;

    if-nez v0, :cond_2

    new-instance v0, Ldkj;

    invoke-direct {v0}, Ldkj;-><init>()V

    iput-object v0, p0, Ldji;->c:Ldkj;

    :cond_2
    iget-object v0, p0, Ldji;->c:Ldkj;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldji;->d:Ldjj;

    if-nez v0, :cond_3

    new-instance v0, Ldjj;

    invoke-direct {v0}, Ldjj;-><init>()V

    iput-object v0, p0, Ldji;->d:Ldjj;

    :cond_3
    iget-object v0, p0, Ldji;->d:Ldjj;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 266
    iget-object v0, p0, Ldji;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 267
    const/4 v0, 0x2

    iget-object v1, p0, Ldji;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 269
    :cond_0
    iget-object v0, p0, Ldji;->c:Ldkj;

    if-eqz v0, :cond_1

    .line 270
    const/4 v0, 0x3

    iget-object v1, p0, Ldji;->c:Ldkj;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 272
    :cond_1
    iget-object v0, p0, Ldji;->d:Ldjj;

    if-eqz v0, :cond_2

    .line 273
    const/4 v0, 0x4

    iget-object v1, p0, Ldji;->d:Ldjj;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 275
    :cond_2
    iget-object v0, p0, Ldji;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 277
    return-void
.end method
