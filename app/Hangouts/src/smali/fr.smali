.class public abstract Lfr;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Lfs;

.field private c:Lft;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput-object p1, p0, Lfr;->a:Landroid/content/Context;

    .line 80
    return-void
.end method


# virtual methods
.method public a()Landroid/content/Context;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lfr;->a:Landroid/content/Context;

    return-object v0
.end method

.method public a(Landroid/view/SubMenu;)V
    .locals 0

    .prologue
    .line 208
    return-void
.end method

.method public a(Lfs;)V
    .locals 0

    .prologue
    .line 226
    iput-object p1, p0, Lfr;->b:Lfs;

    .line 227
    return-void
.end method

.method public a(Lft;)V
    .locals 2

    .prologue
    .line 236
    iget-object v0, p0, Lfr;->c:Lft;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 237
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setVisibilityListener: Setting a new ActionProvider.VisibilityListener when one is already set. Are you reusing this "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " instance while it is still in use somewhere else?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 241
    :cond_0
    iput-object p1, p0, Lfr;->c:Lft;

    .line 242
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lfr;->b:Lfs;

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lfr;->b:Lfs;

    invoke-interface {v0, p1}, Lfs;->a(Z)V

    .line 220
    :cond_0
    return-void
.end method

.method public abstract b()Landroid/view/View;
.end method

.method public c()Landroid/view/View;
    .locals 1

    .prologue
    .line 109
    invoke-virtual {p0}, Lfr;->b()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lfr;->c:Lft;

    .line 146
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 195
    const/4 v0, 0x0

    return v0
.end method
