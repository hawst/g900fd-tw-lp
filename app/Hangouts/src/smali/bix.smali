.class public final Lbix;
.super Lbiw;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final i:D

.field public final j:D


# direct methods
.method protected constructor <init>(Lesc;Leqs;)V
    .locals 9

    .prologue
    const-wide/16 v7, 0x0

    .line 899
    iget-object v2, p2, Leqs;->f:Ljava/lang/String;

    iget-object v3, p2, Leqs;->g:Ljava/lang/String;

    iget-object v0, p2, Leqs;->x:Lepu;

    .line 900
    invoke-static {v0}, Lbix;->a(Lepu;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p2, Leqs;->d:Ljava/lang/String;

    const-string v6, "hangouts/location"

    move-object v0, p0

    move-object v1, p1

    .line 899
    invoke-direct/range {v0 .. v6}, Lbiw;-><init>(Lesc;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 903
    invoke-static {}, Lbiq;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 904
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Received location: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbix;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 908
    :cond_0
    iget-object v0, p2, Leqs;->p:Lepu;

    .line 909
    sget-object v1, Leqf;->b:Lepo;

    invoke-virtual {v0, v1}, Lepu;->getExtension(Lepo;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leqf;

    .line 912
    if-nez v0, :cond_1

    .line 913
    iput-wide v7, p0, Lbix;->i:D

    .line 914
    iput-wide v7, p0, Lbix;->j:D

    .line 920
    :goto_0
    return-void

    .line 918
    :cond_1
    iget-object v1, v0, Leqf;->o:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    iput-wide v1, p0, Lbix;->i:D

    .line 919
    iget-object v0, v0, Leqf;->p:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    iput-wide v0, p0, Lbix;->j:D

    goto :goto_0
.end method

.method public constructor <init>([ILjava/lang/String;DDLjava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 925
    const-string v5, "hangouts/location"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p7

    move-object v4, p8

    invoke-direct/range {v0 .. v5}, Lbiw;-><init>([ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 927
    iput-wide p3, p0, Lbix;->i:D

    .line 928
    iput-wide p5, p0, Lbix;->j:D

    .line 929
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 933
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "id: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lbix;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " name: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbix;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " latitude: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lbix;->i:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " longitude: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lbix;->j:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " staticMapUrl: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbix;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
