.class public final Lelc;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lelc;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Integer;

.field public d:[Leld;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 938
    const/4 v0, 0x0

    new-array v0, v0, [Lelc;

    sput-object v0, Lelc;->a:[Lelc;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 939
    invoke-direct {p0}, Lepn;-><init>()V

    .line 1153
    const/4 v0, 0x0

    iput-object v0, p0, Lelc;->c:Ljava/lang/Integer;

    .line 1156
    sget-object v0, Leld;->a:[Leld;

    iput-object v0, p0, Lelc;->d:[Leld;

    .line 939
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1181
    iget-object v0, p0, Lelc;->b:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1182
    const/4 v0, 0x1

    iget-object v2, p0, Lelc;->b:Ljava/lang/String;

    .line 1183
    invoke-static {v0, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1185
    :goto_0
    iget-object v2, p0, Lelc;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 1186
    const/4 v2, 0x2

    iget-object v3, p0, Lelc;->c:Ljava/lang/Integer;

    .line 1187
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1189
    :cond_0
    iget-object v2, p0, Lelc;->d:[Leld;

    if-eqz v2, :cond_2

    .line 1190
    iget-object v2, p0, Lelc;->d:[Leld;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 1191
    if-eqz v4, :cond_1

    .line 1192
    const/4 v5, 0x3

    .line 1193
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1190
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1197
    :cond_2
    iget-object v1, p0, Lelc;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1198
    iput v0, p0, Lelc;->cachedSize:I

    .line 1199
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 935
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lelc;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lelc;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lelc;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lelc;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_2

    const/4 v2, 0x5

    if-ne v0, v2, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lelc;->c:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lelc;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lelc;->d:[Leld;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Leld;

    iget-object v3, p0, Lelc;->d:[Leld;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lelc;->d:[Leld;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iput-object v2, p0, Lelc;->d:[Leld;

    :goto_2
    iget-object v2, p0, Lelc;->d:[Leld;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Lelc;->d:[Leld;

    new-instance v3, Leld;

    invoke-direct {v3}, Leld;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lelc;->d:[Leld;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lelc;->d:[Leld;

    array-length v0, v0

    goto :goto_1

    :cond_6
    iget-object v2, p0, Lelc;->d:[Leld;

    new-instance v3, Leld;

    invoke-direct {v3}, Leld;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lelc;->d:[Leld;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 1161
    iget-object v0, p0, Lelc;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1162
    const/4 v0, 0x1

    iget-object v1, p0, Lelc;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1164
    :cond_0
    iget-object v0, p0, Lelc;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1165
    const/4 v0, 0x2

    iget-object v1, p0, Lelc;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1167
    :cond_1
    iget-object v0, p0, Lelc;->d:[Leld;

    if-eqz v0, :cond_3

    .line 1168
    iget-object v1, p0, Lelc;->d:[Leld;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 1169
    if-eqz v3, :cond_2

    .line 1170
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 1168
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1174
    :cond_3
    iget-object v0, p0, Lelc;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1176
    return-void
.end method
