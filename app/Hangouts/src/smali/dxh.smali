.class public final Ldxh;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldxh;


# instance fields
.field public b:Ldvm;

.field public c:Ljava/lang/Long;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Integer;

.field public f:[I

.field public g:Ljava/lang/Integer;

.field public h:Ljava/lang/Boolean;

.field public i:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9062
    const/4 v0, 0x0

    new-array v0, v0, [Ldxh;

    sput-object v0, Ldxh;->a:[Ldxh;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 9063
    invoke-direct {p0}, Lepn;-><init>()V

    .line 9066
    iput-object v1, p0, Ldxh;->b:Ldvm;

    .line 9075
    sget-object v0, Lept;->e:[I

    iput-object v0, p0, Ldxh;->f:[I

    .line 9078
    iput-object v1, p0, Ldxh;->g:Ljava/lang/Integer;

    .line 9083
    sget-object v0, Lept;->e:[I

    iput-object v0, p0, Ldxh;->i:[I

    .line 9063
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 9123
    iget-object v0, p0, Ldxh;->b:Ldvm;

    if-eqz v0, :cond_9

    .line 9124
    const/4 v0, 0x1

    iget-object v2, p0, Ldxh;->b:Ldvm;

    .line 9125
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 9127
    :goto_0
    iget-object v2, p0, Ldxh;->c:Ljava/lang/Long;

    if-eqz v2, :cond_0

    .line 9128
    const/4 v2, 0x2

    iget-object v3, p0, Ldxh;->c:Ljava/lang/Long;

    .line 9129
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 9131
    :cond_0
    iget-object v2, p0, Ldxh;->d:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 9132
    const/4 v2, 0x3

    iget-object v3, p0, Ldxh;->d:Ljava/lang/Integer;

    .line 9133
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 9135
    :cond_1
    iget-object v2, p0, Ldxh;->e:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    .line 9136
    const/4 v2, 0x4

    iget-object v3, p0, Ldxh;->e:Ljava/lang/Integer;

    .line 9137
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 9139
    :cond_2
    iget-object v2, p0, Ldxh;->f:[I

    if-eqz v2, :cond_4

    iget-object v2, p0, Ldxh;->f:[I

    array-length v2, v2

    if-lez v2, :cond_4

    .line 9141
    iget-object v4, p0, Ldxh;->f:[I

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_3

    aget v6, v4, v2

    .line 9143
    invoke-static {v6}, Lepl;->f(I)I

    move-result v6

    add-int/2addr v3, v6

    .line 9141
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 9145
    :cond_3
    add-int/2addr v0, v3

    .line 9146
    iget-object v2, p0, Ldxh;->f:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 9148
    :cond_4
    iget-object v2, p0, Ldxh;->g:Ljava/lang/Integer;

    if-eqz v2, :cond_5

    .line 9149
    const/4 v2, 0x6

    iget-object v3, p0, Ldxh;->g:Ljava/lang/Integer;

    .line 9150
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 9152
    :cond_5
    iget-object v2, p0, Ldxh;->h:Ljava/lang/Boolean;

    if-eqz v2, :cond_6

    .line 9153
    const/4 v2, 0x7

    iget-object v3, p0, Ldxh;->h:Ljava/lang/Boolean;

    .line 9154
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 9156
    :cond_6
    iget-object v2, p0, Ldxh;->i:[I

    if-eqz v2, :cond_8

    iget-object v2, p0, Ldxh;->i:[I

    array-length v2, v2

    if-lez v2, :cond_8

    .line 9158
    iget-object v3, p0, Ldxh;->i:[I

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v1, v4, :cond_7

    aget v5, v3, v1

    .line 9160
    invoke-static {v5}, Lepl;->f(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 9158
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 9162
    :cond_7
    add-int/2addr v0, v2

    .line 9163
    iget-object v1, p0, Ldxh;->i:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 9165
    :cond_8
    iget-object v1, p0, Ldxh;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9166
    iput v0, p0, Ldxh;->cachedSize:I

    .line 9167
    return v0

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 9059
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldxh;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldxh;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldxh;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldxh;->b:Ldvm;

    if-nez v0, :cond_2

    new-instance v0, Ldvm;

    invoke-direct {v0}, Ldvm;-><init>()V

    iput-object v0, p0, Ldxh;->b:Ldvm;

    :cond_2
    iget-object v0, p0, Ldxh;->b:Ldvm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldxh;->c:Ljava/lang/Long;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldxh;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldxh;->e:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x28

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Ldxh;->f:[I

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [I

    iget-object v2, p0, Ldxh;->f:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Ldxh;->f:[I

    :goto_1
    iget-object v1, p0, Ldxh;->f:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Ldxh;->f:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    aput v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v1, p0, Ldxh;->f:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    :cond_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldxh;->g:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_5
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldxh;->g:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldxh;->h:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_8
    const/16 v0, 0x40

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Ldxh;->i:[I

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [I

    iget-object v2, p0, Ldxh;->i:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Ldxh;->i:[I

    :goto_2
    iget-object v1, p0, Ldxh;->i:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_6

    iget-object v1, p0, Ldxh;->i:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    aput v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_6
    iget-object v1, p0, Ldxh;->i:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    aput v2, v1, v0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 9088
    iget-object v1, p0, Ldxh;->b:Ldvm;

    if-eqz v1, :cond_0

    .line 9089
    const/4 v1, 0x1

    iget-object v2, p0, Ldxh;->b:Ldvm;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 9091
    :cond_0
    iget-object v1, p0, Ldxh;->c:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 9092
    const/4 v1, 0x2

    iget-object v2, p0, Ldxh;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Lepl;->b(IJ)V

    .line 9094
    :cond_1
    iget-object v1, p0, Ldxh;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 9095
    const/4 v1, 0x3

    iget-object v2, p0, Ldxh;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 9097
    :cond_2
    iget-object v1, p0, Ldxh;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 9098
    const/4 v1, 0x4

    iget-object v2, p0, Ldxh;->e:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 9100
    :cond_3
    iget-object v1, p0, Ldxh;->f:[I

    if-eqz v1, :cond_4

    iget-object v1, p0, Ldxh;->f:[I

    array-length v1, v1

    if-lez v1, :cond_4

    .line 9101
    iget-object v2, p0, Ldxh;->f:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    aget v4, v2, v1

    .line 9102
    const/4 v5, 0x5

    invoke-virtual {p1, v5, v4}, Lepl;->a(II)V

    .line 9101
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 9105
    :cond_4
    iget-object v1, p0, Ldxh;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 9106
    const/4 v1, 0x6

    iget-object v2, p0, Ldxh;->g:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 9108
    :cond_5
    iget-object v1, p0, Ldxh;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 9109
    const/4 v1, 0x7

    iget-object v2, p0, Ldxh;->h:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 9111
    :cond_6
    iget-object v1, p0, Ldxh;->i:[I

    if-eqz v1, :cond_7

    iget-object v1, p0, Ldxh;->i:[I

    array-length v1, v1

    if-lez v1, :cond_7

    .line 9112
    iget-object v1, p0, Ldxh;->i:[I

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_7

    aget v3, v1, v0

    .line 9113
    const/16 v4, 0x8

    invoke-virtual {p1, v4, v3}, Lepl;->a(II)V

    .line 9112
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 9116
    :cond_7
    iget-object v0, p0, Ldxh;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 9118
    return-void
.end method
