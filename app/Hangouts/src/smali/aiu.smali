.class public final Laiu;
.super Landroid/widget/BaseAdapter;
.source "PG"

# interfaces
.implements Landroid/widget/WrapperListAdapter;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

.field private final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lait;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Laip;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;Laip;)V
    .locals 2

    .prologue
    .line 1033
    iput-object p1, p0, Laiu;->a:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 1030
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Laiu;->b:Ljava/util/ArrayList;

    .line 1034
    iput-object p2, p0, Laiu;->c:Laip;

    .line 1035
    invoke-static {p1}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1039
    invoke-virtual {p2}, Laip;->a()Landroid/database/Cursor;

    move-result-object v0

    invoke-direct {p0, v0}, Laiu;->a(Landroid/database/Cursor;)V

    .line 1041
    :cond_0
    iget-object v0, p0, Laiu;->c:Laip;

    new-instance v1, Laiv;

    invoke-direct {v1, p0, p1}, Laiv;-><init>(Laiu;Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)V

    invoke-virtual {v0, v1}, Laip;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1054
    return-void
.end method

.method static synthetic a(Laiu;)Laip;
    .locals 1

    .prologue
    .line 1029
    iget-object v0, p0, Laiu;->c:Laip;

    return-object v0
.end method

.method static synthetic a(Laiu;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 1029
    invoke-direct {p0, p1}, Laiu;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1150
    iget-object v0, p0, Laiu;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1151
    iget-object v0, p0, Laiu;->a:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    if-eqz p1, :cond_2

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 1152
    iget-object v0, p0, Laiu;->b:Ljava/util/ArrayList;

    new-instance v1, Lait;

    sget v2, Lh;->fC:I

    sget v3, Lf;->fK:I

    invoke-direct {v1, v2, v3, v4}, Lait;-><init>(III)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1154
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1156
    :cond_0
    const/16 v0, 0x1b

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 1158
    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    if-nez v0, :cond_1

    .line 1160
    iget-object v0, p0, Laiu;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1162
    :cond_1
    iget-object v0, p0, Laiu;->b:Ljava/util/ArrayList;

    new-instance v1, Lait;

    sget v2, Lh;->gb:I

    sget v3, Lf;->fK:I

    .line 1165
    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Lait;-><init>(III)V

    .line 1162
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1171
    :cond_2
    :goto_0
    return-void

    .line 1168
    :cond_3
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 1063
    iget-object v0, p0, Laiu;->c:Laip;

    invoke-virtual {v0}, Laip;->getCount()I

    move-result v0

    iget-object v1, p0, Laiu;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1068
    iget-object v0, p0, Laiu;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lait;

    .line 1069
    iget v2, v0, Lait;->a:I

    if-ne v2, p1, :cond_1

    .line 1075
    :goto_1
    return-object v0

    .line 1071
    :cond_1
    iget v0, v0, Lait;->a:I

    if-ge v0, p1, :cond_0

    .line 1072
    add-int/lit8 p1, p1, -0x1

    goto :goto_0

    .line 1075
    :cond_2
    iget-object v0, p0, Laiu;->c:Laip;

    invoke-virtual {v0, p1}, Laip;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_1
.end method

.method public getItemId(I)J
    .locals 3

    .prologue
    .line 1097
    iget-object v0, p0, Laiu;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lait;

    .line 1098
    iget v2, v0, Lait;->a:I

    if-ne v2, p1, :cond_1

    .line 1099
    const-wide/16 v0, -0x1

    .line 1104
    :goto_1
    return-wide v0

    .line 1100
    :cond_1
    iget v0, v0, Lait;->a:I

    if-ge v0, p1, :cond_0

    .line 1101
    add-int/lit8 p1, p1, -0x1

    goto :goto_0

    .line 1104
    :cond_2
    iget-object v0, p0, Laiu;->c:Laip;

    invoke-virtual {v0, p1}, Laip;->getItemId(I)J

    move-result-wide v0

    goto :goto_1
.end method

.method public getItemViewType(I)I
    .locals 3

    .prologue
    .line 1080
    iget-object v0, p0, Laiu;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lait;

    .line 1081
    iget v2, v0, Lait;->a:I

    if-ne v2, p1, :cond_1

    .line 1082
    iget-object v0, p0, Laiu;->c:Laip;

    invoke-virtual {v0}, Laip;->getViewTypeCount()I

    move-result v0

    .line 1087
    :goto_1
    return v0

    .line 1083
    :cond_1
    iget v0, v0, Lait;->a:I

    if-ge v0, p1, :cond_0

    .line 1084
    add-int/lit8 p1, p1, -0x1

    goto :goto_0

    .line 1087
    :cond_2
    iget-object v0, p0, Laiu;->c:Laip;

    invoke-virtual {v0, p1}, Laip;->getItemViewType(I)I

    move-result v0

    goto :goto_1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 1110
    iget-object v0, p0, Laiu;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lait;

    .line 1111
    iget v2, v0, Lait;->a:I

    if-ne v2, p1, :cond_2

    .line 1112
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    iget v2, v0, Lait;->c:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    sget v1, Lg;->gz:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget v0, v0, Lait;->b:I

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    sget v0, Lg;->bw:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    move-object v0, v2

    .line 1117
    :goto_1
    return-object v0

    .line 1113
    :cond_2
    iget v0, v0, Lait;->a:I

    if-ge v0, p1, :cond_0

    .line 1114
    add-int/lit8 p1, p1, -0x1

    goto :goto_0

    .line 1117
    :cond_3
    iget-object v0, p0, Laiu;->c:Laip;

    invoke-virtual {v0, p1, p2, p3}, Laip;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_1
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 1092
    iget-object v0, p0, Laiu;->c:Laip;

    invoke-virtual {v0}, Laip;->getViewTypeCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getWrappedAdapter()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 1175
    iget-object v0, p0, Laiu;->c:Laip;

    return-object v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 1058
    const/4 v0, 0x1

    return v0
.end method
