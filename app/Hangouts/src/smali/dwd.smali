.class public final Ldwd;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldwd;


# instance fields
.field public b:Ldvm;

.field public c:Ldqf;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11901
    const/4 v0, 0x0

    new-array v0, v0, [Ldwd;

    sput-object v0, Ldwd;->a:[Ldwd;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 11902
    invoke-direct {p0}, Lepn;-><init>()V

    .line 11905
    iput-object v0, p0, Ldwd;->b:Ldvm;

    .line 11908
    iput-object v0, p0, Ldwd;->c:Ldqf;

    .line 11911
    iput-object v0, p0, Ldwd;->d:Ljava/lang/Integer;

    .line 11902
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 11936
    const/4 v0, 0x0

    .line 11937
    iget-object v1, p0, Ldwd;->b:Ldvm;

    if-eqz v1, :cond_0

    .line 11938
    const/4 v0, 0x1

    iget-object v1, p0, Ldwd;->b:Ldvm;

    .line 11939
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 11941
    :cond_0
    iget-object v1, p0, Ldwd;->c:Ldqf;

    if-eqz v1, :cond_1

    .line 11942
    const/4 v1, 0x2

    iget-object v2, p0, Ldwd;->c:Ldqf;

    .line 11943
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11945
    :cond_1
    iget-object v1, p0, Ldwd;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 11946
    const/4 v1, 0x3

    iget-object v2, p0, Ldwd;->d:Ljava/lang/Integer;

    .line 11947
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11949
    :cond_2
    iget-object v1, p0, Ldwd;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 11950
    const/4 v1, 0x4

    iget-object v2, p0, Ldwd;->e:Ljava/lang/Integer;

    .line 11951
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11953
    :cond_3
    iget-object v1, p0, Ldwd;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11954
    iput v0, p0, Ldwd;->cachedSize:I

    .line 11955
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 11898
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldwd;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldwd;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldwd;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldwd;->b:Ldvm;

    if-nez v0, :cond_2

    new-instance v0, Ldvm;

    invoke-direct {v0}, Ldvm;-><init>()V

    iput-object v0, p0, Ldwd;->b:Ldvm;

    :cond_2
    iget-object v0, p0, Ldwd;->b:Ldvm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldwd;->c:Ldqf;

    if-nez v0, :cond_3

    new-instance v0, Ldqf;

    invoke-direct {v0}, Ldqf;-><init>()V

    iput-object v0, p0, Ldwd;->c:Ldqf;

    :cond_3
    iget-object v0, p0, Ldwd;->c:Ldqf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_4

    const/16 v1, 0x14

    if-eq v0, v1, :cond_4

    const/16 v1, 0x1e

    if-ne v0, v1, :cond_5

    :cond_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldwd;->d:Ljava/lang/Integer;

    goto :goto_0

    :cond_5
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldwd;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldwd;->e:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 11918
    iget-object v0, p0, Ldwd;->b:Ldvm;

    if-eqz v0, :cond_0

    .line 11919
    const/4 v0, 0x1

    iget-object v1, p0, Ldwd;->b:Ldvm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 11921
    :cond_0
    iget-object v0, p0, Ldwd;->c:Ldqf;

    if-eqz v0, :cond_1

    .line 11922
    const/4 v0, 0x2

    iget-object v1, p0, Ldwd;->c:Ldqf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 11924
    :cond_1
    iget-object v0, p0, Ldwd;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 11925
    const/4 v0, 0x3

    iget-object v1, p0, Ldwd;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 11927
    :cond_2
    iget-object v0, p0, Ldwd;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 11928
    const/4 v0, 0x4

    iget-object v1, p0, Ldwd;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 11930
    :cond_3
    iget-object v0, p0, Ldwd;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 11932
    return-void
.end method
