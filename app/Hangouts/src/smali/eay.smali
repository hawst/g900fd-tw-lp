.class final Leay;
.super Leaj;
.source "PG"


# instance fields
.field final p:Leaj;

.field final q:Leaj;


# direct methods
.method constructor <init>(Leaj;Leaj;)V
    .locals 2

    .prologue
    .line 769
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CharMatcher.or("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Leay;-><init>(Leaj;Leaj;Ljava/lang/String;)V

    .line 770
    return-void
.end method

.method private constructor <init>(Leaj;Leaj;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 763
    invoke-direct {p0, p3}, Leaj;-><init>(Ljava/lang/String;)V

    .line 764
    invoke-static {p1}, Lm;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leaj;

    iput-object v0, p0, Leay;->p:Leaj;

    .line 765
    invoke-static {p2}, Lm;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leaj;

    iput-object v0, p0, Leay;->q:Leaj;

    .line 766
    return-void
.end method


# virtual methods
.method a(Ljava/lang/String;)Leaj;
    .locals 3

    .prologue
    .line 786
    new-instance v0, Leay;

    iget-object v1, p0, Leay;->p:Leaj;

    iget-object v2, p0, Leay;->q:Leaj;

    invoke-direct {v0, v1, v2, p1}, Leay;-><init>(Leaj;Leaj;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(C)Z
    .locals 1

    .prologue
    .line 781
    iget-object v0, p0, Leay;->p:Leaj;

    invoke-virtual {v0, p1}, Leaj;->a(C)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Leay;->q:Leaj;

    invoke-virtual {v0, p1}, Leaj;->a(C)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
