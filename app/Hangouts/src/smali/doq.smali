.class public final Ldoq;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldoq;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2471
    const/4 v0, 0x0

    new-array v0, v0, [Ldoq;

    sput-object v0, Ldoq;->a:[Ldoq;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2472
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 2503
    const/4 v0, 0x0

    .line 2504
    iget-object v1, p0, Ldoq;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2505
    const/4 v0, 0x1

    iget-object v1, p0, Ldoq;->b:Ljava/lang/String;

    .line 2506
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2508
    :cond_0
    iget-object v1, p0, Ldoq;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 2509
    const/4 v1, 0x2

    iget-object v2, p0, Ldoq;->c:Ljava/lang/String;

    .line 2510
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2512
    :cond_1
    iget-object v1, p0, Ldoq;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 2513
    const/4 v1, 0x3

    iget-object v2, p0, Ldoq;->d:Ljava/lang/String;

    .line 2514
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2516
    :cond_2
    iget-object v1, p0, Ldoq;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 2517
    const/4 v1, 0x4

    iget-object v2, p0, Ldoq;->e:Ljava/lang/Integer;

    .line 2518
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2520
    :cond_3
    iget-object v1, p0, Ldoq;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2521
    iput v0, p0, Ldoq;->cachedSize:I

    .line 2522
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 2468
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldoq;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldoq;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldoq;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldoq;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldoq;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldoq;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldoq;->e:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 2485
    iget-object v0, p0, Ldoq;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2486
    const/4 v0, 0x1

    iget-object v1, p0, Ldoq;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 2488
    :cond_0
    iget-object v0, p0, Ldoq;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2489
    const/4 v0, 0x2

    iget-object v1, p0, Ldoq;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 2491
    :cond_1
    iget-object v0, p0, Ldoq;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2492
    const/4 v0, 0x3

    iget-object v1, p0, Ldoq;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 2494
    :cond_2
    iget-object v0, p0, Ldoq;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 2495
    const/4 v0, 0x4

    iget-object v1, p0, Ldoq;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 2497
    :cond_3
    iget-object v0, p0, Ldoq;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 2499
    return-void
.end method
