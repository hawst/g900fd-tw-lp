.class public final Ldil;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldil;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 311
    const/4 v0, 0x0

    new-array v0, v0, [Ldil;

    sput-object v0, Ldil;->a:[Ldil;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 312
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 348
    const/4 v0, 0x0

    .line 349
    iget-object v1, p0, Ldil;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 350
    const/4 v0, 0x1

    iget-object v1, p0, Ldil;->b:Ljava/lang/String;

    .line 351
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 353
    :cond_0
    iget-object v1, p0, Ldil;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 354
    const/4 v1, 0x2

    iget-object v2, p0, Ldil;->c:Ljava/lang/String;

    .line 355
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 357
    :cond_1
    iget-object v1, p0, Ldil;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 358
    const/4 v1, 0x3

    iget-object v2, p0, Ldil;->d:Ljava/lang/String;

    .line 359
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 361
    :cond_2
    iget-object v1, p0, Ldil;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 362
    const/4 v1, 0x4

    iget-object v2, p0, Ldil;->e:Ljava/lang/Integer;

    .line 363
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 365
    :cond_3
    iget-object v1, p0, Ldil;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 366
    const/4 v1, 0x5

    iget-object v2, p0, Ldil;->f:Ljava/lang/String;

    .line 367
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 369
    :cond_4
    iget-object v1, p0, Ldil;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 370
    iput v0, p0, Ldil;->cachedSize:I

    .line 371
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 308
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldil;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldil;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldil;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldil;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldil;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldil;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldil;->e:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldil;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 327
    iget-object v0, p0, Ldil;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 328
    const/4 v0, 0x1

    iget-object v1, p0, Ldil;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 330
    :cond_0
    iget-object v0, p0, Ldil;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 331
    const/4 v0, 0x2

    iget-object v1, p0, Ldil;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 333
    :cond_1
    iget-object v0, p0, Ldil;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 334
    const/4 v0, 0x3

    iget-object v1, p0, Ldil;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 336
    :cond_2
    iget-object v0, p0, Ldil;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 337
    const/4 v0, 0x4

    iget-object v1, p0, Ldil;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 339
    :cond_3
    iget-object v0, p0, Ldil;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 340
    const/4 v0, 0x5

    iget-object v1, p0, Ldil;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 342
    :cond_4
    iget-object v0, p0, Ldil;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 344
    return-void
.end method
