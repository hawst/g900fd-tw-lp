.class public final Lcby;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Laac;
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/views/EasterEggView;

.field private b:Lzx;

.field private c:Landroid/widget/ImageView;

.field private d:Landroid/view/animation/Animation;

.field private e:Lccm;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/views/EasterEggView;Lbyq;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 104
    iput-object p1, p0, Lcby;->a:Lcom/google/android/apps/hangouts/views/EasterEggView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    new-instance v0, Lzx;

    const/4 v1, 0x1

    invoke-direct {v0, p2, p0, v1, v2}, Lzx;-><init>(Lbyq;Laac;ZLjava/lang/Object;)V

    iput-object v0, p0, Lcby;->b:Lzx;

    .line 107
    iput-object v2, p0, Lcby;->c:Landroid/widget/ImageView;

    .line 108
    invoke-virtual {p1}, Lcom/google/android/apps/hangouts/views/EasterEggView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcby;->d:Landroid/view/animation/Animation;

    .line 109
    iget-object v0, p0, Lcby;->d:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 110
    invoke-static {}, Lbsn;->c()Lbsn;

    move-result-object v0

    iget-object v1, p0, Lcby;->b:Lzx;

    invoke-virtual {v0, v1}, Lbsn;->c(Lbrv;)V

    .line 111
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 142
    iget-object v0, p0, Lcby;->b:Lzx;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcby;->b:Lzx;

    invoke-virtual {v0}, Lzx;->b()V

    .line 144
    iput-object v2, p0, Lcby;->b:Lzx;

    .line 147
    :cond_0
    iget-object v0, p0, Lcby;->d:Landroid/view/animation/Animation;

    if-eqz v0, :cond_1

    .line 148
    iget-object v0, p0, Lcby;->d:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    .line 149
    iput-object v2, p0, Lcby;->d:Landroid/view/animation/Animation;

    .line 152
    :cond_1
    iget-object v0, p0, Lcby;->c:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 153
    iget-object v0, p0, Lcby;->a:Lcom/google/android/apps/hangouts/views/EasterEggView;

    iget-object v1, p0, Lcby;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/EasterEggView;->removeView(Landroid/view/View;)V

    .line 154
    iget-object v0, p0, Lcby;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 155
    iget-object v0, p0, Lcby;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 156
    iget-object v0, p0, Lcby;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 157
    iput-object v2, p0, Lcby;->c:Landroid/widget/ImageView;

    .line 160
    :cond_2
    iget-object v0, p0, Lcby;->e:Lccm;

    if-eqz v0, :cond_3

    .line 161
    iget-object v0, p0, Lcby;->e:Lccm;

    invoke-virtual {v0}, Lccm;->c()V

    .line 162
    iput-object v2, p0, Lcby;->e:Lccm;

    .line 164
    :cond_3
    return-void
.end method

.method public a(Lbzn;Lbyd;ZLzx;Z)V
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcby;->b:Lzx;

    invoke-virtual {p4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 120
    const/4 v0, 0x0

    iput-object v0, p0, Lcby;->b:Lzx;

    .line 121
    if-nez p3, :cond_0

    .line 122
    const-string v0, "Babel"

    const-string v1, "Failed to download easter egg image."

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    iget-object v0, p0, Lcby;->a:Lcom/google/android/apps/hangouts/views/EasterEggView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/EasterEggView;->a(Lcom/google/android/apps/hangouts/views/EasterEggView;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 129
    :cond_0
    invoke-static {p2}, Lcwz;->b(Ljava/lang/Object;)V

    .line 131
    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcby;->a:Lcom/google/android/apps/hangouts/views/EasterEggView;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/views/EasterEggView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcby;->c:Landroid/widget/ImageView;

    .line 132
    iget-object v0, p0, Lcby;->c:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 133
    new-instance v0, Lccm;

    invoke-direct {v0, p2}, Lccm;-><init>(Lbyd;)V

    iput-object v0, p0, Lcby;->e:Lccm;

    .line 134
    iget-object v0, p0, Lcby;->c:Landroid/widget/ImageView;

    iget-object v1, p0, Lcby;->e:Lccm;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 135
    iget-object v0, p0, Lcby;->e:Lccm;

    invoke-virtual {v0}, Lccm;->a()V

    .line 136
    iget-object v0, p0, Lcby;->c:Landroid/widget/ImageView;

    iget-object v1, p0, Lcby;->d:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 137
    iget-object v0, p0, Lcby;->a:Lcom/google/android/apps/hangouts/views/EasterEggView;

    iget-object v1, p0, Lcby;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/EasterEggView;->addView(Landroid/view/View;)V

    .line 139
    :cond_1
    return-void
.end method

.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 169
    iget-object v0, p0, Lcby;->c:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcby;->c:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 177
    :cond_0
    iget-object v0, p0, Lcby;->a:Lcom/google/android/apps/hangouts/views/EasterEggView;

    new-instance v1, Lcbz;

    invoke-direct {v1, p0, p0}, Lcbz;-><init>(Lcby;Lcby;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/EasterEggView;->post(Ljava/lang/Runnable;)Z

    .line 184
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 192
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 188
    return-void
.end method
