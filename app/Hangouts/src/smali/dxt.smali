.class public final Ldxt;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldxt;


# instance fields
.field public b:Ldui;

.field public c:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3162
    const/4 v0, 0x0

    new-array v0, v0, [Ldxt;

    sput-object v0, Ldxt;->a:[Ldxt;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3163
    invoke-direct {p0}, Lepn;-><init>()V

    .line 3166
    const/4 v0, 0x0

    iput-object v0, p0, Ldxt;->b:Ldui;

    .line 3163
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 3185
    const/4 v0, 0x0

    .line 3186
    iget-object v1, p0, Ldxt;->b:Ldui;

    if-eqz v1, :cond_0

    .line 3187
    const/4 v0, 0x1

    iget-object v1, p0, Ldxt;->b:Ldui;

    .line 3188
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3190
    :cond_0
    iget-object v1, p0, Ldxt;->c:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 3191
    const/4 v1, 0x2

    iget-object v2, p0, Ldxt;->c:Ljava/lang/Long;

    .line 3192
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3194
    :cond_1
    iget-object v1, p0, Ldxt;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3195
    iput v0, p0, Ldxt;->cachedSize:I

    .line 3196
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 3159
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldxt;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldxt;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldxt;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldxt;->b:Ldui;

    if-nez v0, :cond_2

    new-instance v0, Ldui;

    invoke-direct {v0}, Ldui;-><init>()V

    iput-object v0, p0, Ldxt;->b:Ldui;

    :cond_2
    iget-object v0, p0, Ldxt;->b:Ldui;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldxt;->c:Ljava/lang/Long;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 3173
    iget-object v0, p0, Ldxt;->b:Ldui;

    if-eqz v0, :cond_0

    .line 3174
    const/4 v0, 0x1

    iget-object v1, p0, Ldxt;->b:Ldui;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 3176
    :cond_0
    iget-object v0, p0, Ldxt;->c:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 3177
    const/4 v0, 0x2

    iget-object v1, p0, Ldxt;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 3179
    :cond_1
    iget-object v0, p0, Ldxt;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 3181
    return-void
.end method
