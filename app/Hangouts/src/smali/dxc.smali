.class public final Ldxc;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldxc;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Long;

.field public d:Ljava/lang/Integer;

.field public e:Lesc;

.field public f:Ldrn;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24657
    const/4 v0, 0x0

    new-array v0, v0, [Ldxc;

    sput-object v0, Ldxc;->a:[Ldxc;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 24658
    invoke-direct {p0}, Lepn;-><init>()V

    .line 24684
    iput-object v0, p0, Ldxc;->d:Ljava/lang/Integer;

    .line 24687
    iput-object v0, p0, Ldxc;->e:Lesc;

    .line 24690
    iput-object v0, p0, Ldxc;->f:Ldrn;

    .line 24658
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 24716
    const/4 v0, 0x0

    .line 24717
    iget-object v1, p0, Ldxc;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 24718
    const/4 v0, 0x1

    iget-object v1, p0, Ldxc;->b:Ljava/lang/String;

    .line 24719
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 24721
    :cond_0
    iget-object v1, p0, Ldxc;->c:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 24722
    const/4 v1, 0x2

    iget-object v2, p0, Ldxc;->c:Ljava/lang/Long;

    .line 24723
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 24725
    :cond_1
    iget-object v1, p0, Ldxc;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 24726
    const/4 v1, 0x3

    iget-object v2, p0, Ldxc;->d:Ljava/lang/Integer;

    .line 24727
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 24729
    :cond_2
    iget-object v1, p0, Ldxc;->e:Lesc;

    if-eqz v1, :cond_3

    .line 24730
    const/4 v1, 0x6

    iget-object v2, p0, Ldxc;->e:Lesc;

    .line 24731
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24733
    :cond_3
    iget-object v1, p0, Ldxc;->f:Ldrn;

    if-eqz v1, :cond_4

    .line 24734
    const/4 v1, 0x7

    iget-object v2, p0, Ldxc;->f:Ldrn;

    .line 24735
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24737
    :cond_4
    iget-object v1, p0, Ldxc;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24738
    iput v0, p0, Ldxc;->cachedSize:I

    .line 24739
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 24654
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldxc;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldxc;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldxc;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldxc;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldxc;->c:Ljava/lang/Long;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/16 v1, 0x11

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf

    if-eq v0, v1, :cond_2

    const/16 v1, 0x10

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldxc;->d:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldxc;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldxc;->e:Lesc;

    if-nez v0, :cond_4

    new-instance v0, Lesc;

    invoke-direct {v0}, Lesc;-><init>()V

    iput-object v0, p0, Ldxc;->e:Lesc;

    :cond_4
    iget-object v0, p0, Ldxc;->e:Lesc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Ldxc;->f:Ldrn;

    if-nez v0, :cond_5

    new-instance v0, Ldrn;

    invoke-direct {v0}, Ldrn;-><init>()V

    iput-object v0, p0, Ldxc;->f:Ldrn;

    :cond_5
    iget-object v0, p0, Ldxc;->f:Ldrn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x32 -> :sswitch_4
        0x3a -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 24695
    iget-object v0, p0, Ldxc;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 24696
    const/4 v0, 0x1

    iget-object v1, p0, Ldxc;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 24698
    :cond_0
    iget-object v0, p0, Ldxc;->c:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 24699
    const/4 v0, 0x2

    iget-object v1, p0, Ldxc;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 24701
    :cond_1
    iget-object v0, p0, Ldxc;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 24702
    const/4 v0, 0x3

    iget-object v1, p0, Ldxc;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 24704
    :cond_2
    iget-object v0, p0, Ldxc;->e:Lesc;

    if-eqz v0, :cond_3

    .line 24705
    const/4 v0, 0x6

    iget-object v1, p0, Ldxc;->e:Lesc;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 24707
    :cond_3
    iget-object v0, p0, Ldxc;->f:Ldrn;

    if-eqz v0, :cond_4

    .line 24708
    const/4 v0, 0x7

    iget-object v1, p0, Ldxc;->f:Ldrn;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 24710
    :cond_4
    iget-object v0, p0, Ldxc;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 24712
    return-void
.end method
