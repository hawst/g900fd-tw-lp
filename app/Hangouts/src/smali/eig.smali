.class public final Leig;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leig;


# instance fields
.field public b:Leij;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Leig;

    sput-object v0, Leig;->a:[Leig;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Leig;->b:Leij;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 2

    .prologue
    .line 27
    const/4 v0, 0x0

    .line 28
    iget-object v1, p0, Leig;->b:Leij;

    if-eqz v1, :cond_0

    .line 29
    const/16 v0, 0xf

    iget-object v1, p0, Leig;->b:Leij;

    .line 30
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 32
    :cond_0
    iget-object v1, p0, Leig;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 33
    iput v0, p0, Leig;->cachedSize:I

    .line 34
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Leig;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Leig;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Leig;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Leig;->b:Leij;

    if-nez v0, :cond_2

    new-instance v0, Leij;

    invoke-direct {v0}, Leij;-><init>()V

    iput-object v0, p0, Leig;->b:Leij;

    :cond_2
    iget-object v0, p0, Leig;->b:Leij;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x7a -> :sswitch_1
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 18
    iget-object v0, p0, Leig;->b:Leij;

    if-eqz v0, :cond_0

    .line 19
    const/16 v0, 0xf

    iget-object v1, p0, Leig;->b:Leij;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 21
    :cond_0
    iget-object v0, p0, Leig;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 23
    return-void
.end method
