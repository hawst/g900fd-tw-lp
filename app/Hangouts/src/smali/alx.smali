.class public final Lalx;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Ly;

.field private final b:Lyj;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;


# direct methods
.method constructor <init>(Ly;Lyj;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 138
    iput-object p1, p0, Lalx;->a:Ly;

    .line 139
    iput-object p2, p0, Lalx;->b:Lyj;

    .line 140
    iput-object p3, p0, Lalx;->c:Ljava/lang/String;

    .line 141
    iput-object p4, p0, Lalx;->d:Ljava/lang/String;

    .line 142
    return-void
.end method


# virtual methods
.method public a()V
    .locals 14

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 145
    iget-object v0, p0, Lalx;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 147
    new-instance v0, Lalz;

    iget-object v1, p0, Lalx;->a:Ly;

    iget-object v3, p0, Lalx;->b:Lyj;

    iget-object v4, p0, Lalx;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v3, v4, v2}, Lalz;-><init>(Landroid/content/Context;Lyj;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v2}, Lacc;->a(Laci;Lach;)Lacc;

    move-result-object v0

    .line 151
    invoke-virtual {v0}, Lacc;->a()V

    .line 161
    :goto_0
    return-void

    .line 152
    :cond_0
    iget-object v0, p0, Lalx;->d:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 155
    new-instance v5, Lacc;

    invoke-direct {v5, v2}, Lacc;-><init>(Lach;)V

    .line 156
    const-string v0, "babel_offnetwork_invite_sms_attempt_sequence"

    const-string v1, "local"

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v0, "Babel"

    const-string v1, "OffnetworkInvite: empty sms attempt sequence!"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    :cond_1
    :goto_1
    invoke-virtual {v5}, Lacc;->a()V

    goto :goto_0

    .line 156
    :cond_2
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    array-length v0, v6

    if-gtz v0, :cond_3

    const-string v0, "Babel"

    const-string v1, "OffnetworkInvite: invalid sms attempt sequence!"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    array-length v7, v6

    move v3, v4

    :goto_2
    if-ge v3, v7, :cond_1

    aget-object v0, v6, v3

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, "server"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    new-instance v0, Lalz;

    iget-object v1, p0, Lalx;->a:Ly;

    iget-object v8, p0, Lalx;->b:Lyj;

    iget-object v9, p0, Lalx;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v8, v2, v9}, Lalz;-><init>(Landroid/content/Context;Lyj;Ljava/lang/String;Ljava/lang/String;)V

    :goto_3
    invoke-virtual {v5, v0}, Lacc;->a(Laci;)V

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_4
    const-string v1, "local"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lalx;->b:Lyj;

    invoke-virtual {v0}, Lyj;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v0, p0, Lalx;->b:Lyj;

    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    :cond_5
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, "babel_offnetwork_invite_canned_text_hangouts_website"

    const-string v8, "hangouts.google.com"

    invoke-static {v1, v8}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    new-instance v1, Laly;

    iget-object v9, p0, Lalx;->a:Ly;

    iget-object v10, p0, Lalx;->d:Ljava/lang/String;

    iget-object v11, p0, Lalx;->a:Ly;

    invoke-virtual {v11}, Ly;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    sget v12, Lh;->hW:I

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    aput-object v0, v13, v4

    const/4 v0, 0x1

    aput-object v8, v13, v0

    invoke-virtual {v11, v12, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v9, v10, v0}, Laly;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_3

    :cond_6
    const-string v0, "Babel"

    const-string v1, "OffnetworkInvite: don\'t know who sends this sms"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    move-object v0, v2

    goto :goto_3

    .line 159
    :cond_8
    const-string v0, "Babel"

    const-string v1, "OffnetworkInvite: email and phone can\'t be both null"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
