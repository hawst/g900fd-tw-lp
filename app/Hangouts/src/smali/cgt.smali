.class final Lcgt;
.super Ljava/lang/Object;

# interfaces
.implements Lcgl;


# instance fields
.field final a:Ljava/util/concurrent/locks/Lock;

.field final b:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcha",
            "<*>;>;"
        }
    .end annotation
.end field

.field c:Lcft;

.field d:I

.field volatile e:I

.field volatile f:I

.field g:J

.field final h:Landroid/os/Handler;

.field final i:Landroid/os/Bundle;

.field j:Z

.field final k:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcha",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final l:Ljava/util/concurrent/locks/Condition;

.field private final m:Lchz;

.field private final n:I

.field private final o:Landroid/os/Looper;

.field private p:Z

.field private q:I

.field private final r:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcgg",
            "<*>;",
            "Lcge;",
            ">;"
        }
    .end annotation
.end field

.field private final s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final t:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lsr",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final u:Lcgy;

.field private final v:Lcgn;

.field private final w:Lcib;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/ClientSettings;Ljava/util/Map;Ljava/util/Set;Ljava/util/Set;I)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/os/Looper;",
            "Lcom/google/android/gms/common/internal/ClientSettings;",
            "Ljava/util/Map",
            "<",
            "Lcgd",
            "<*>;",
            "Lft;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcgn;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcfw;",
            ">;I)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v1}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v1, p0, Lcgt;->a:Ljava/util/concurrent/locks/Lock;

    iget-object v1, p0, Lcgt;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v1

    iput-object v1, p0, Lcgt;->l:Ljava/util/concurrent/locks/Condition;

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcgt;->b:Ljava/util/Queue;

    const/4 v1, 0x4

    iput v1, p0, Lcgt;->e:I

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcgt;->p:Z

    const-wide/16 v1, 0x1388

    iput-wide v1, p0, Lcgt;->g:J

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iput-object v1, p0, Lcgt;->i:Landroid/os/Bundle;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcgt;->r:Ljava/util/Map;

    new-instance v1, Ljava/util/WeakHashMap;

    invoke-direct {v1}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v1}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lcgt;->t:Ljava/util/Set;

    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v1}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lcgt;->k:Ljava/util/Set;

    new-instance v1, Lcgu;

    invoke-direct {v1, p0}, Lcgu;-><init>(Lcgt;)V

    iput-object v1, p0, Lcgt;->u:Lcgy;

    new-instance v1, Lcgv;

    invoke-direct {v1, p0}, Lcgv;-><init>(Lcgt;)V

    iput-object v1, p0, Lcgt;->v:Lcgn;

    new-instance v1, Lcgw;

    invoke-direct {v1, p0}, Lcgw;-><init>(Lcgt;)V

    iput-object v1, p0, Lcgt;->w:Lcib;

    new-instance v1, Lchz;

    iget-object v2, p0, Lcgt;->w:Lcib;

    invoke-direct {v1, p2, v2}, Lchz;-><init>(Landroid/os/Looper;Lcib;)V

    iput-object v1, p0, Lcgt;->m:Lchz;

    iput-object p2, p0, Lcgt;->o:Landroid/os/Looper;

    new-instance v1, Lcgz;

    invoke-direct {v1, p0, p2}, Lcgz;-><init>(Lcgt;Landroid/os/Looper;)V

    iput-object v1, p0, Lcgt;->h:Landroid/os/Handler;

    move/from16 v0, p7

    iput v0, p0, Lcgt;->n:I

    invoke-interface/range {p5 .. p5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcgn;

    iget-object v3, p0, Lcgt;->m:Lchz;

    invoke-virtual {v3, v1}, Lchz;->a(Lcgn;)V

    goto :goto_0

    :cond_0
    invoke-interface/range {p6 .. p6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcfw;

    iget-object v3, p0, Lcgt;->m:Lchz;

    invoke-virtual {v3, v1}, Lchz;->a(Lcfw;)V

    goto :goto_1

    :cond_1
    invoke-interface {p4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcgd;

    invoke-virtual {v1}, Lcgd;->a()Lcgf;

    move-result-object v3

    invoke-interface {p4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    iget-object v9, p0, Lcgt;->r:Ljava/util/Map;

    invoke-virtual {v1}, Lcgd;->c()Lcgg;

    move-result-object v10

    iget-object v4, p0, Lcgt;->v:Lcgn;

    new-instance v5, Lcgx;

    invoke-direct {v5, p0, v3}, Lcgx;-><init>(Lcgt;Lcgf;)V

    check-cast v2, Lcvf;

    const-string v1, "Must provide valid PeopleOptions!"

    invoke-static {v2, v1}, Lg;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lclj;

    iget v2, v2, Lcvf;->a:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p3}, Lcom/google/android/gms/common/internal/ClientSettings;->b()Ljava/lang/String;

    move-result-object v7

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v7}, Lclj;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcgn;Lcfw;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v9, v10, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_2
    invoke-virtual {p3}, Lcom/google/android/gms/common/internal/ClientSettings;->a()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcgt;->s:Ljava/util/List;

    return-void
.end method

.method static synthetic a(Lcgt;)V
    .locals 5

    const/4 v4, 0x0

    iget v0, p0, Lcgt;->q:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcgt;->q:I

    iget v0, p0, Lcgt;->q:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcgt;->c:Lcft;

    if-eqz v0, :cond_2

    iput-boolean v4, p0, Lcgt;->p:Z

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcgt;->a(I)V

    invoke-virtual {p0}, Lcgt;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcgt;->h:Landroid/os/Handler;

    iget-object v1, p0, Lcgt;->h:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    iget-wide v2, p0, Lcgt;->g:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :goto_0
    iput-boolean v4, p0, Lcgt;->j:Z

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lcgt;->m:Lchz;

    iget-object v1, p0, Lcgt;->c:Lcft;

    invoke-virtual {v0, v1}, Lchz;->a(Lcft;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x2

    iput v0, p0, Lcgt;->e:I

    invoke-direct {p0}, Lcgt;->g()V

    iget-object v0, p0, Lcgt;->l:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    invoke-direct {p0}, Lcgt;->f()V

    iget-boolean v0, p0, Lcgt;->p:Z

    if-eqz v0, :cond_3

    iput-boolean v4, p0, Lcgt;->p:Z

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcgt;->a(I)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcgt;->i:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    :goto_2
    iget-object v1, p0, Lcgt;->m:Lchz;

    invoke-virtual {v1, v0}, Lchz;->a(Landroid/os/Bundle;)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcgt;->i:Landroid/os/Bundle;

    goto :goto_2
.end method

.method private a(Lcha;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::",
            "Lcge;",
            ">(",
            "Lcha",
            "<TA;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcgt;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    invoke-interface {p1}, Lcha;->d()Lcgg;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "This task can not be executed or enqueued (it\'s probably a Batch or malformed)"

    invoke-static {v0, v1}, Lg;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcgt;->k:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcgt;->u:Lcgy;

    invoke-interface {p1, v0}, Lcha;->a(Lcgy;)V

    invoke-virtual {p0}, Lcgt;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {p1, v0}, Lcha;->b(Lcom/google/android/gms/common/api/Status;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcgt;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-interface {p1}, Lcha;->d()Lcgg;

    move-result-object v0

    iget-object v1, p0, Lcgt;->r:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcge;

    const-string v1, "Appropriate Api was not requested."

    invoke-static {v0, v1}, Lg;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p1, v0}, Lcha;->a(Lcge;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcgt;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcgt;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method private b(Lcgj;)Lcgj;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::",
            "Lcge;",
            "T:",
            "Lcgj",
            "<+",
            "Lcgq;",
            "TA;>;>(TT;)TT;"
        }
    .end annotation

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcgt;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcgt;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    const-string v2, "GoogleApiClient is not connected yet."

    invoke-static {v0, v2}, Lg;->a(ZLjava/lang/Object;)V

    invoke-direct {p0}, Lcgt;->f()V

    :try_start_0
    invoke-direct {p0, p1}, Lcgt;->a(Lcha;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-object p1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {p0, v1}, Lcgt;->a(I)V

    goto :goto_1
.end method

.method private f()V
    .locals 2

    iget-object v0, p0, Lcgt;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    invoke-virtual {p0}, Lcgt;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcgt;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "GoogleApiClient is not connected yet."

    invoke-static {v0, v1}, Lg;->a(ZLjava/lang/Object;)V

    :goto_1
    iget-object v0, p0, Lcgt;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    :try_start_1
    iget-object v0, p0, Lcgt;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcha;

    invoke-direct {p0, v0}, Lcgt;->a(Lcha;)V
    :try_end_1
    .catch Landroid/os/DeadObjectException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcgt;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcgt;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method private g()V
    .locals 2

    iget-object v0, p0, Lcgt;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lcgt;->f:I

    iget-object v0, p0, Lcgt;->h:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcgt;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcgt;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method


# virtual methods
.method public a(Lcgj;)Lcgj;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::",
            "Lcge;",
            "R::",
            "Lcgq;",
            "T:",
            "Lcgj",
            "<TR;TA;>;>(TT;)TT;"
        }
    .end annotation

    iget-object v0, p0, Lcgt;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    new-instance v0, Lcgi;

    iget-object v1, p0, Lcgt;->o:Landroid/os/Looper;

    invoke-direct {v0, v1}, Lcgi;-><init>(Landroid/os/Looper;)V

    invoke-virtual {p1, v0}, Lcgj;->a(Lcgi;)V

    invoke-virtual {p0}, Lcgt;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcgt;->b(Lcgj;)Lcgj;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    iget-object v0, p0, Lcgt;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object p1

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcgt;->b:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcgt;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public a()V
    .locals 2

    iget-object v0, p0, Lcgt;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcgt;->p:Z

    invoke-virtual {p0}, Lcgt;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcgt;->d()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcgt;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcgt;->j:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcgt;->c:Lcft;

    const/4 v0, 0x1

    iput v0, p0, Lcgt;->e:I

    iget-object v0, p0, Lcgt;->i:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V

    iget-object v0, p0, Lcgt;->r:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iput v0, p0, Lcgt;->q:I

    iget-object v0, p0, Lcgt;->r:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcge;

    invoke-interface {v0}, Lcge;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcgt;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_2
    iget-object v0, p0, Lcgt;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0
.end method

.method a(I)V
    .locals 5

    const/4 v1, 0x3

    const/4 v4, -0x1

    iget-object v0, p0, Lcgt;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget v0, p0, Lcgt;->e:I

    if-eq v0, v1, :cond_a

    if-ne p1, v4, :cond_4

    invoke-virtual {p0}, Lcgt;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcgt;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcha;

    invoke-interface {v0}, Lcha;->a()V

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcgt;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcgt;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    :cond_1
    iget-object v0, p0, Lcgt;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcha;

    invoke-interface {v0}, Lcha;->a()V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcgt;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lcgt;->t:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsr;

    invoke-virtual {v0}, Lsr;->e()V

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcgt;->t:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lcgt;->c:Lcft;

    if-nez v0, :cond_4

    iget-object v0, p0, Lcgt;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcgt;->p:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcgt;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_3
    return-void

    :cond_4
    :try_start_2
    invoke-virtual {p0}, Lcgt;->d()Z

    move-result v0

    invoke-virtual {p0}, Lcgt;->c()Z

    move-result v1

    const/4 v2, 0x3

    iput v2, p0, Lcgt;->e:I

    if-eqz v0, :cond_6

    if-ne p1, v4, :cond_5

    const/4 v0, 0x0

    iput-object v0, p0, Lcgt;->c:Lcft;

    :cond_5
    iget-object v0, p0, Lcgt;->l:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    :cond_6
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcgt;->j:Z

    iget-object v0, p0, Lcgt;->r:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_7
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcge;

    invoke-interface {v0}, Lcge;->c()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v0}, Lcge;->b()V

    goto :goto_4

    :cond_8
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcgt;->j:Z

    const/4 v0, 0x4

    iput v0, p0, Lcgt;->e:I

    if-eqz v1, :cond_a

    if-eq p1, v4, :cond_9

    iget-object v0, p0, Lcgt;->m:Lchz;

    invoke-virtual {v0, p1}, Lchz;->a(I)V

    :cond_9
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcgt;->j:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_a
    iget-object v0, p0, Lcgt;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_3
.end method

.method public a(Lcfw;)V
    .locals 1

    iget-object v0, p0, Lcgt;->m:Lchz;

    invoke-virtual {v0, p1}, Lchz;->a(Lcfw;)V

    return-void
.end method

.method public a(Lcgn;)V
    .locals 1

    iget-object v0, p0, Lcgt;->m:Lchz;

    invoke-virtual {v0, p1}, Lchz;->a(Lcgn;)V

    return-void
.end method

.method public b()V
    .locals 1

    invoke-direct {p0}, Lcgt;->g()V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcgt;->a(I)V

    return-void
.end method

.method public b(Lcfw;)V
    .locals 1

    iget-object v0, p0, Lcgt;->m:Lchz;

    invoke-virtual {v0, p1}, Lchz;->b(Lcfw;)V

    return-void
.end method

.method public b(Lcgn;)V
    .locals 1

    iget-object v0, p0, Lcgt;->m:Lchz;

    invoke-virtual {v0, p1}, Lchz;->b(Lcgn;)V

    return-void
.end method

.method public c()Z
    .locals 2

    iget v0, p0, Lcgt;->e:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcgt;->e:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method e()Z
    .locals 1

    iget v0, p0, Lcgt;->f:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
