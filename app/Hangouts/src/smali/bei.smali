.class public abstract Lbei;
.super Lbed;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final i:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 763
    invoke-direct {p0, p1}, Lbed;-><init>(Ljava/lang/String;)V

    .line 764
    iput-object p2, p0, Lbei;->i:Ljava/lang/String;

    .line 765
    return-void
.end method


# virtual methods
.method public a(Lyj;Lbph;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 769
    sget-boolean v0, Lbea;->d:Z

    if-eqz v0, :cond_0

    .line 770
    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onFailed "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lbei;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lbei;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 773
    :cond_0
    invoke-super {p0, p1, p2}, Lbed;->a(Lyj;Lbph;)V

    .line 774
    iget-boolean v0, p0, Lbei;->h:Z

    if-nez v0, :cond_2

    .line 775
    sget-boolean v0, Lbea;->d:Z

    if-eqz v0, :cond_1

    instance-of v0, p0, Lbfb;

    if-eqz v0, :cond_1

    move-object v0, p0

    .line 776
    check-cast v0, Lbfb;

    .line 777
    invoke-virtual {v0}, Lbfb;->h()Ljava/lang/String;

    move-result-object v0

    .line 778
    if-eqz v0, :cond_1

    .line 779
    const-string v2, "Babel_Stress"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Sending stress message permanently failed with error="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 780
    invoke-virtual {p2}, Lbph;->e()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 779
    invoke-static {v2, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 785
    :cond_1
    iget-object v2, p0, Lbei;->c:Ljava/lang/String;

    iget-object v3, p0, Lbei;->i:Ljava/lang/String;

    if-eqz p2, :cond_3

    .line 789
    invoke-virtual {p2}, Lbph;->e()I

    move-result v0

    .line 785
    :goto_0
    invoke-static {p1, v2, v3, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ljava/lang/String;Ljava/lang/String;I)I

    .line 790
    const-wide/16 v2, 0x0

    const/4 v0, 0x4

    invoke-static {p1, v2, v3, v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;JIZ)V

    .line 793
    :cond_2
    return-void

    :cond_3
    move v0, v1

    .line 789
    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 797
    const-string v0, "event_queue"

    return-object v0
.end method
