.class public final Ldsg;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldsg;


# instance fields
.field public b:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25959
    const/4 v0, 0x0

    new-array v0, v0, [Ldsg;

    sput-object v0, Ldsg;->a:[Ldsg;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25960
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 2

    .prologue
    .line 25976
    const/4 v0, 0x0

    .line 25977
    iget-object v1, p0, Ldsg;->b:[B

    if-eqz v1, :cond_0

    .line 25978
    const/4 v0, 0x1

    iget-object v1, p0, Ldsg;->b:[B

    .line 25979
    invoke-static {v0, v1}, Lepl;->b(I[B)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 25981
    :cond_0
    iget-object v1, p0, Ldsg;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25982
    iput v0, p0, Ldsg;->cachedSize:I

    .line 25983
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 25956
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldsg;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldsg;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldsg;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v0

    iput-object v0, p0, Ldsg;->b:[B

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 25967
    iget-object v0, p0, Ldsg;->b:[B

    if-eqz v0, :cond_0

    .line 25968
    const/4 v0, 0x1

    iget-object v1, p0, Ldsg;->b:[B

    invoke-virtual {p1, v0, v1}, Lepl;->a(I[B)V

    .line 25970
    :cond_0
    iget-object v0, p0, Ldsg;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 25972
    return-void
.end method
