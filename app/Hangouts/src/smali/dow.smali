.class public final Ldow;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldow;

.field public static final b:Lepo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lepo",
            "<",
            "Ldow;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public c:Ljava/lang/String;

.field public d:[Ldpa;

.field public e:[Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/Integer;

.field public h:Ljava/lang/Integer;

.field public i:Ljava/lang/Long;

.field public j:Ljava/lang/Long;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/Integer;

.field public o:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 302
    const/4 v0, 0x0

    new-array v0, v0, [Ldow;

    sput-object v0, Ldow;->a:[Ldow;

    .line 306
    const v0, 0x205afce

    new-instance v1, Ldox;

    invoke-direct {v1}, Ldox;-><init>()V

    .line 307
    invoke-static {v0, v1}, Lepo;->a(ILepp;)Lepo;

    move-result-object v0

    sput-object v0, Ldow;->b:Lepo;

    .line 306
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 303
    invoke-direct {p0}, Lepn;-><init>()V

    .line 312
    sget-object v0, Ldpa;->a:[Ldpa;

    iput-object v0, p0, Ldow;->d:[Ldpa;

    .line 315
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Ldow;->e:[Ljava/lang/String;

    .line 334
    const/4 v0, 0x0

    iput-object v0, p0, Ldow;->n:Ljava/lang/Integer;

    .line 303
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 390
    const/4 v0, 0x1

    iget-object v2, p0, Ldow;->c:Ljava/lang/String;

    .line 392
    invoke-static {v0, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 393
    iget-object v2, p0, Ldow;->e:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Ldow;->e:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 395
    iget-object v4, p0, Ldow;->e:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_0
    if-ge v2, v5, :cond_0

    aget-object v6, v4, v2

    .line 397
    invoke-static {v6}, Lepl;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 395
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 399
    :cond_0
    add-int/2addr v0, v3

    .line 400
    iget-object v2, p0, Ldow;->e:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 402
    :cond_1
    iget-object v2, p0, Ldow;->f:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 403
    const/16 v2, 0x8

    iget-object v3, p0, Ldow;->f:Ljava/lang/String;

    .line 404
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 406
    :cond_2
    iget-object v2, p0, Ldow;->i:Ljava/lang/Long;

    if-eqz v2, :cond_3

    .line 407
    const/16 v2, 0x9

    iget-object v3, p0, Ldow;->i:Ljava/lang/Long;

    .line 408
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 410
    :cond_3
    iget-object v2, p0, Ldow;->d:[Ldpa;

    if-eqz v2, :cond_5

    .line 411
    iget-object v2, p0, Ldow;->d:[Ldpa;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 412
    if-eqz v4, :cond_4

    .line 413
    const/16 v5, 0xa

    .line 414
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 411
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 418
    :cond_5
    iget-object v1, p0, Ldow;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 419
    const/16 v1, 0xb

    iget-object v2, p0, Ldow;->h:Ljava/lang/Integer;

    .line 420
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 422
    :cond_6
    iget-object v1, p0, Ldow;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 423
    const/16 v1, 0xc

    iget-object v2, p0, Ldow;->g:Ljava/lang/Integer;

    .line 424
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 426
    :cond_7
    iget-object v1, p0, Ldow;->j:Ljava/lang/Long;

    if-eqz v1, :cond_8

    .line 427
    const/16 v1, 0xd

    iget-object v2, p0, Ldow;->j:Ljava/lang/Long;

    .line 428
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 430
    :cond_8
    iget-object v1, p0, Ldow;->k:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 431
    const/16 v1, 0xe

    iget-object v2, p0, Ldow;->k:Ljava/lang/String;

    .line 432
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 434
    :cond_9
    iget-object v1, p0, Ldow;->l:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 435
    const/16 v1, 0xf

    iget-object v2, p0, Ldow;->l:Ljava/lang/String;

    .line 436
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 438
    :cond_a
    iget-object v1, p0, Ldow;->m:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 439
    const/16 v1, 0x10

    iget-object v2, p0, Ldow;->m:Ljava/lang/String;

    .line 440
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 442
    :cond_b
    iget-object v1, p0, Ldow;->n:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    .line 443
    const/16 v1, 0x11

    iget-object v2, p0, Ldow;->n:Ljava/lang/Integer;

    .line 444
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 446
    :cond_c
    iget-object v1, p0, Ldow;->o:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    .line 447
    const/16 v1, 0x12

    iget-object v2, p0, Ldow;->o:Ljava/lang/Boolean;

    .line 448
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 450
    :cond_d
    iget-object v1, p0, Ldow;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 451
    iput v0, p0, Ldow;->cachedSize:I

    .line 452
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 299
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldow;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldow;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldow;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldow;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldow;->e:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Ldow;->e:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Ldow;->e:[Ljava/lang/String;

    :goto_1
    iget-object v2, p0, Ldow;->e:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Ldow;->e:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v2, p0, Ldow;->e:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldow;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldow;->i:Ljava/lang/Long;

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x52

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldow;->d:[Ldpa;

    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    add-int/2addr v2, v0

    new-array v2, v2, [Ldpa;

    iget-object v3, p0, Ldow;->d:[Ldpa;

    if-eqz v3, :cond_3

    iget-object v3, p0, Ldow;->d:[Ldpa;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Ldow;->d:[Ldpa;

    :goto_3
    iget-object v2, p0, Ldow;->d:[Ldpa;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Ldow;->d:[Ldpa;

    new-instance v3, Ldpa;

    invoke-direct {v3}, Ldpa;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldow;->d:[Ldpa;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    iget-object v0, p0, Ldow;->d:[Ldpa;

    array-length v0, v0

    goto :goto_2

    :cond_5
    iget-object v2, p0, Ldow;->d:[Ldpa;

    new-instance v3, Ldpa;

    invoke-direct {v3}, Ldpa;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldow;->d:[Ldpa;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->l()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldow;->h:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->l()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldow;->g:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldow;->j:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldow;->k:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldow;->l:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldow;->m:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_6

    const/4 v2, 0x1

    if-ne v0, v2, :cond_7

    :cond_6
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldow;->n:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_7
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldow;->n:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldow;->o:Ljava/lang/Boolean;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x22 -> :sswitch_2
        0x42 -> :sswitch_3
        0x48 -> :sswitch_4
        0x52 -> :sswitch_5
        0x58 -> :sswitch_6
        0x60 -> :sswitch_7
        0x68 -> :sswitch_8
        0x72 -> :sswitch_9
        0x7a -> :sswitch_a
        0x82 -> :sswitch_b
        0x88 -> :sswitch_c
        0x90 -> :sswitch_d
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 341
    const/4 v1, 0x1

    iget-object v2, p0, Ldow;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 342
    iget-object v1, p0, Ldow;->e:[Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 343
    iget-object v2, p0, Ldow;->e:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 344
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Lepl;->a(ILjava/lang/String;)V

    .line 343
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 347
    :cond_0
    iget-object v1, p0, Ldow;->f:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 348
    const/16 v1, 0x8

    iget-object v2, p0, Ldow;->f:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 350
    :cond_1
    iget-object v1, p0, Ldow;->i:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 351
    const/16 v1, 0x9

    iget-object v2, p0, Ldow;->i:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Lepl;->a(IJ)V

    .line 353
    :cond_2
    iget-object v1, p0, Ldow;->d:[Ldpa;

    if-eqz v1, :cond_4

    .line 354
    iget-object v1, p0, Ldow;->d:[Ldpa;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 355
    if-eqz v3, :cond_3

    .line 356
    const/16 v4, 0xa

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 354
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 360
    :cond_4
    iget-object v0, p0, Ldow;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 361
    const/16 v0, 0xb

    iget-object v1, p0, Ldow;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->c(II)V

    .line 363
    :cond_5
    iget-object v0, p0, Ldow;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 364
    const/16 v0, 0xc

    iget-object v1, p0, Ldow;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->c(II)V

    .line 366
    :cond_6
    iget-object v0, p0, Ldow;->j:Ljava/lang/Long;

    if-eqz v0, :cond_7

    .line 367
    const/16 v0, 0xd

    iget-object v1, p0, Ldow;->j:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 369
    :cond_7
    iget-object v0, p0, Ldow;->k:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 370
    const/16 v0, 0xe

    iget-object v1, p0, Ldow;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 372
    :cond_8
    iget-object v0, p0, Ldow;->l:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 373
    const/16 v0, 0xf

    iget-object v1, p0, Ldow;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 375
    :cond_9
    iget-object v0, p0, Ldow;->m:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 376
    const/16 v0, 0x10

    iget-object v1, p0, Ldow;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 378
    :cond_a
    iget-object v0, p0, Ldow;->n:Ljava/lang/Integer;

    if-eqz v0, :cond_b

    .line 379
    const/16 v0, 0x11

    iget-object v1, p0, Ldow;->n:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 381
    :cond_b
    iget-object v0, p0, Ldow;->o:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    .line 382
    const/16 v0, 0x12

    iget-object v1, p0, Ldow;->o:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 384
    :cond_c
    iget-object v0, p0, Ldow;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 386
    return-void
.end method
