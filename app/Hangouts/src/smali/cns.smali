.class public final Lcns;
.super Lchr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lchr",
        "<",
        "Lcnk;",
        ">;"
    }
.end annotation


# instance fields
.field private final f:Ljava/lang/String;

.field private final g:Lcnq;

.field private final h:Lcnn;

.field private final i:Ljava/lang/Object;

.field private j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcnq;)V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-direct {p0, p1, p2, p2, v0}, Lchr;-><init>(Landroid/content/Context;Lcfv;Lcfw;[Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcns;->f:Ljava/lang/String;

    invoke-static {p2}, Lg;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnq;

    iput-object v0, p0, Lcns;->g:Lcnq;

    iget-object v0, p0, Lcns;->g:Lcnq;

    invoke-virtual {v0, p0}, Lcnq;->a(Lcns;)V

    new-instance v0, Lcnn;

    invoke-direct {v0}, Lcnn;-><init>()V

    iput-object v0, p0, Lcns;->h:Lcnn;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcns;->i:Ljava/lang/Object;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcns;->j:Z

    return-void
.end method

.method private b(Lcom/google/android/gms/internal/qr;Lcom/google/android/gms/internal/qn;)V
    .locals 1

    iget-object v0, p0, Lcns;->h:Lcnn;

    invoke-virtual {v0, p1, p2}, Lcnn;->a(Lcom/google/android/gms/internal/qr;Lcom/google/android/gms/internal/qn;)V

    return-void
.end method

.method private k()V
    .locals 9

    iget-boolean v0, p0, Lcns;->j:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcns;->h:Lcnn;

    invoke-virtual {v0}, Lcnn;->c()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :try_start_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcns;->h:Lcnn;

    invoke-virtual {v1}, Lcnn;->a()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v2, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcno;

    iget-object v1, v0, Lcno;->c:Leti;

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcns;->i()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lcnk;

    iget-object v5, p0, Lcns;->f:Ljava/lang/String;

    iget-object v6, v0, Lcno;->a:Lcom/google/android/gms/internal/qr;

    iget-object v0, v0, Lcno;->c:Leti;

    invoke-virtual {v0}, Lcob;->d()I

    move-result v7

    new-array v7, v7, [B

    array-length v8, v7

    invoke-static {v0, v7, v8}, Lcob;->a(Lcob;[BI)V

    invoke-interface {v1, v5, v6, v7}, Lcnk;->a(Ljava/lang/String;Lcom/google/android/gms/internal/qr;[B)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v0, "PlayLoggerImpl"

    const-string v1, "Couldn\'t send cached log events to AndroidLog service.  Retaining in memory cache."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    :goto_2
    return-void

    :cond_3
    :try_start_1
    iget-object v1, v0, Lcno;->a:Lcom/google/android/gms/internal/qr;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/internal/qr;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v0, v0, Lcno;->b:Lcom/google/android/gms/internal/qn;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {p0}, Lcns;->i()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lcnk;

    iget-object v5, p0, Lcns;->f:Ljava/lang/String;

    invoke-interface {v1, v5, v2, v3}, Lcnk;->a(Ljava/lang/String;Lcom/google/android/gms/internal/qr;Ljava/util/List;)V

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    :cond_5
    iget-object v1, v0, Lcno;->a:Lcom/google/android/gms/internal/qr;

    iget-object v0, v0, Lcno;->b:Lcom/google/android/gms/internal/qn;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v2, v1

    goto :goto_1

    :cond_6
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {p0}, Lcns;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcnk;

    iget-object v1, p0, Lcns;->f:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lcnk;->a(Ljava/lang/String;Lcom/google/android/gms/internal/qr;Ljava/util/List;)V

    :cond_7
    iget-object v0, p0, Lcns;->h:Lcnn;

    invoke-virtual {v0}, Lcnn;->b()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method


# virtual methods
.method protected synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-static {p1}, Lcnl;->a(Landroid/os/IBinder;)Lcnk;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcim;Lchv;)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const v1, 0x5cc600

    iget-object v2, p0, Lchr;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lcim;->f(Lcij;ILjava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public a(Lcom/google/android/gms/internal/qr;Lcom/google/android/gms/internal/qn;)V
    .locals 3

    iget-object v1, p0, Lcns;->i:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcns;->j:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2}, Lcns;->b(Lcom/google/android/gms/internal/qr;Lcom/google/android/gms/internal/qn;)V

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcns;->k()V

    invoke-virtual {p0}, Lcns;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcnk;

    iget-object v2, p0, Lcns;->f:Ljava/lang/String;

    invoke-interface {v0, v2, p1, p2}, Lcnk;->a(Ljava/lang/String;Lcom/google/android/gms/internal/qr;Lcom/google/android/gms/internal/qn;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "PlayLoggerImpl"

    const-string v2, "Couldn\'t send log event.  Will try caching."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1, p2}, Lcns;->b(Lcom/google/android/gms/internal/qr;Lcom/google/android/gms/internal/qn;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_1
    move-exception v0

    :try_start_3
    const-string v0, "PlayLoggerImpl"

    const-string v2, "Service was disconnected.  Will try caching."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1, p2}, Lcns;->b(Lcom/google/android/gms/internal/qr;Lcom/google/android/gms/internal/qn;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method a(Z)V
    .locals 2

    iget-object v1, p0, Lcns;->i:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcns;->j:Z

    iput-boolean p1, p0, Lcns;->j:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcns;->j:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcns;->k()V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected e()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.playlog.service.START"

    return-object v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.playlog.internal.IPlayLogService"

    return-object v0
.end method

.method public j()V
    .locals 2

    iget-object v1, p0, Lcns;->i:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lcns;->g()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcns;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    monitor-exit v1

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcns;->g:Lcnq;

    invoke-virtual {v0}, Lcnq;->a()V

    invoke-virtual {p0}, Lcns;->a()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
