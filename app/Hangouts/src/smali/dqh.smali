.class public final Ldqh;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldqh;


# instance fields
.field public b:Ldui;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Integer;

.field public e:Leir;

.field public f:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1770
    const/4 v0, 0x0

    new-array v0, v0, [Ldqh;

    sput-object v0, Ldqh;->a:[Ldqh;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1771
    invoke-direct {p0}, Lepn;-><init>()V

    .line 1774
    iput-object v0, p0, Ldqh;->b:Ldui;

    .line 1779
    iput-object v0, p0, Ldqh;->d:Ljava/lang/Integer;

    .line 1782
    iput-object v0, p0, Ldqh;->e:Leir;

    .line 1785
    iput-object v0, p0, Ldqh;->f:Ljava/lang/Integer;

    .line 1771
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1811
    const/4 v0, 0x0

    .line 1812
    iget-object v1, p0, Ldqh;->b:Ldui;

    if-eqz v1, :cond_0

    .line 1813
    const/4 v0, 0x1

    iget-object v1, p0, Ldqh;->b:Ldui;

    .line 1814
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1816
    :cond_0
    iget-object v1, p0, Ldqh;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1817
    const/4 v1, 0x2

    iget-object v2, p0, Ldqh;->c:Ljava/lang/String;

    .line 1818
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1820
    :cond_1
    iget-object v1, p0, Ldqh;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 1821
    const/4 v1, 0x3

    iget-object v2, p0, Ldqh;->d:Ljava/lang/Integer;

    .line 1822
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1824
    :cond_2
    iget-object v1, p0, Ldqh;->e:Leir;

    if-eqz v1, :cond_3

    .line 1825
    const/4 v1, 0x4

    iget-object v2, p0, Ldqh;->e:Leir;

    .line 1826
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1828
    :cond_3
    iget-object v1, p0, Ldqh;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 1829
    const/4 v1, 0x5

    iget-object v2, p0, Ldqh;->f:Ljava/lang/Integer;

    .line 1830
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1832
    :cond_4
    iget-object v1, p0, Ldqh;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1833
    iput v0, p0, Ldqh;->cachedSize:I

    .line 1834
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1767
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldqh;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldqh;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldqh;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldqh;->b:Ldui;

    if-nez v0, :cond_2

    new-instance v0, Ldui;

    invoke-direct {v0}, Ldui;-><init>()V

    iput-object v0, p0, Ldqh;->b:Ldui;

    :cond_2
    iget-object v0, p0, Ldqh;->b:Ldui;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldqh;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_3

    if-eq v0, v3, :cond_3

    if-ne v0, v4, :cond_4

    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqh;->d:Ljava/lang/Integer;

    goto :goto_0

    :cond_4
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqh;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldqh;->e:Leir;

    if-nez v0, :cond_5

    new-instance v0, Leir;

    invoke-direct {v0}, Leir;-><init>()V

    iput-object v0, p0, Ldqh;->e:Leir;

    :cond_5
    iget-object v0, p0, Ldqh;->e:Leir;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_6

    if-eq v0, v3, :cond_6

    if-eq v0, v4, :cond_6

    const/4 v1, 0x3

    if-eq v0, v1, :cond_6

    const/4 v1, 0x4

    if-eq v0, v1, :cond_6

    const/4 v1, 0x5

    if-ne v0, v1, :cond_7

    :cond_6
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqh;->f:Ljava/lang/Integer;

    goto :goto_0

    :cond_7
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqh;->f:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 1790
    iget-object v0, p0, Ldqh;->b:Ldui;

    if-eqz v0, :cond_0

    .line 1791
    const/4 v0, 0x1

    iget-object v1, p0, Ldqh;->b:Ldui;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1793
    :cond_0
    iget-object v0, p0, Ldqh;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1794
    const/4 v0, 0x2

    iget-object v1, p0, Ldqh;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1796
    :cond_1
    iget-object v0, p0, Ldqh;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 1797
    const/4 v0, 0x3

    iget-object v1, p0, Ldqh;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1799
    :cond_2
    iget-object v0, p0, Ldqh;->e:Leir;

    if-eqz v0, :cond_3

    .line 1800
    const/4 v0, 0x4

    iget-object v1, p0, Ldqh;->e:Leir;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1802
    :cond_3
    iget-object v0, p0, Ldqh;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 1803
    const/4 v0, 0x5

    iget-object v1, p0, Ldqh;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1805
    :cond_4
    iget-object v0, p0, Ldqh;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1807
    return-void
.end method
