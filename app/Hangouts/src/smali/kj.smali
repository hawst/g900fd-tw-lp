.class public Lkj;
.super Ly;
.source "PG"

# interfaces
.implements Lcv;
.implements Lj;
.implements Lke;


# instance fields
.field n:Lkk;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ly;-><init>()V

    return-void
.end method


# virtual methods
.method a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 221
    invoke-super {p0, p1}, Ly;->setContentView(Landroid/view/View;)V

    .line 222
    return-void
.end method

.method a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    .prologue
    .line 225
    invoke-super {p0, p1, p2}, Ly;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 226
    return-void
.end method

.method public a(Lcu;)V
    .locals 0

    .prologue
    .line 338
    invoke-virtual {p1, p0}, Lcu;->a(Landroid/app/Activity;)Lcu;

    .line 339
    return-void
.end method

.method a(ILandroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 233
    invoke-super {p0, p1, p2}, Ly;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method a(ILandroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 245
    invoke-super {p0, p1, p2}, Ly;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method a(ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 237
    invoke-super {p0, p1, p2, p3}, Ly;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public a(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 436
    invoke-static {p0, p1}, Laz;->a(Landroid/app/Activity;Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method protected a(Landroid/view/View;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lkj;->n:Lkk;

    invoke-virtual {v0, p1, p2}, Lkk;->a(Landroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method a_(I)V
    .locals 0

    .prologue
    .line 217
    invoke-super {p0, p1}, Ly;->setContentView(I)V

    .line 218
    return-void
.end method

.method public addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lkj;->n:Lkk;

    invoke-virtual {v0, p1, p2}, Lkk;->b(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 93
    return-void
.end method

.method public b(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 452
    invoke-static {p0, p1}, Laz;->b(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 453
    return-void
.end method

.method b(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    .prologue
    .line 229
    invoke-super {p0, p1, p2}, Ly;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 230
    return-void
.end method

.method b(Landroid/view/View;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 241
    invoke-super {p0, p1, p2}, Ly;->a(Landroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public g()Lkd;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lkj;->n:Lkk;

    invoke-virtual {v0}, Lkk;->b()Lkd;

    move-result-object v0

    return-object v0
.end method

.method public getMenuInflater()Landroid/view/MenuInflater;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lkj;->n:Lkk;

    invoke-virtual {v0}, Lkk;->c()Landroid/view/MenuInflater;

    move-result-object v0

    return-object v0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lkj;->n:Lkk;

    invoke-virtual {v0}, Lkk;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 251
    invoke-super {p0}, Ly;->onBackPressed()V

    .line 253
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 104
    invoke-super {p0, p1}, Ly;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 105
    iget-object v0, p0, Lkj;->n:Lkk;

    invoke-virtual {v0}, Lkk;->d()V

    .line 106
    return-void
.end method

.method public final onContentChanged()V
    .locals 1

    .prologue
    .line 464
    iget-object v0, p0, Lkj;->n:Lkk;

    invoke-virtual {v0}, Lkk;->j()V

    .line 465
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 97
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x14

    if-lt v0, v1, :cond_0

    new-instance v0, Lkm;

    invoke-direct {v0, p0}, Lkm;-><init>(Lkj;)V

    :goto_0
    iput-object v0, p0, Lkj;->n:Lkk;

    .line 98
    invoke-super {p0, p1}, Ly;->onCreate(Landroid/os/Bundle;)V

    .line 99
    iget-object v0, p0, Lkj;->n:Lkk;

    invoke-virtual {v0, p1}, Lkk;->a(Landroid/os/Bundle;)V

    .line 100
    return-void

    .line 97
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_1

    new-instance v0, Lku;

    invoke-direct {v0, p0}, Lku;-><init>(Lkj;)V

    goto :goto_0

    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_2

    new-instance v0, Lkt;

    invoke-direct {v0, p0}, Lkt;-><init>(Lkj;)V

    goto :goto_0

    :cond_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_3

    new-instance v0, Lkr;

    invoke-direct {v0, p0}, Lkr;-><init>(Lkj;)V

    goto :goto_0

    :cond_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_4

    new-instance v0, Lkq;

    invoke-direct {v0, p0}, Lkq;-><init>(Lkj;)V

    goto :goto_0

    :cond_4
    new-instance v0, Lko;

    invoke-direct {v0, p0}, Lko;-><init>(Lkj;)V

    goto :goto_0
.end method

.method public onCreatePanelMenu(ILandroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lkj;->n:Lkk;

    invoke-virtual {v0, p1, p2}, Lkk;->a(ILandroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onCreatePanelView(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 122
    if-nez p1, :cond_0

    .line 123
    iget-object v0, p0, Lkj;->n:Lkk;

    invoke-virtual {v0, p1}, Lkk;->b(I)Landroid/view/View;

    move-result-object v0

    .line 125
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Ly;->onCreatePanelView(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 131
    iget-object v0, p0, Lkj;->n:Lkk;

    invoke-virtual {v0, p1, p2}, Lkk;->a(ILandroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    const/4 v0, 0x1

    .line 140
    :goto_0
    return v0

    .line 135
    :cond_0
    invoke-virtual {p0}, Lkj;->g()Lkd;

    move-result-object v0

    .line 136
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lkd;->b()I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1

    .line 138
    invoke-virtual {p0}, Lkj;->v_()Z

    move-result v0

    goto :goto_0

    .line 140
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onPostResume()V
    .locals 1

    .prologue
    .line 116
    invoke-super {p0}, Ly;->onPostResume()V

    .line 117
    iget-object v0, p0, Lkj;->n:Lkk;

    invoke-virtual {v0}, Lkk;->f()V

    .line 118
    return-void
.end method

.method public onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lkj;->n:Lkk;

    invoke-virtual {v0, p1, p2, p3}, Lkk;->a(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 110
    invoke-super {p0}, Ly;->onStop()V

    .line 111
    iget-object v0, p0, Lkj;->n:Lkk;

    invoke-virtual {v0}, Lkk;->e()V

    .line 112
    return-void
.end method

.method protected onTitleChanged(Ljava/lang/CharSequence;I)V
    .locals 1

    .prologue
    .line 145
    invoke-super {p0, p1, p2}, Ly;->onTitleChanged(Ljava/lang/CharSequence;I)V

    .line 146
    iget-object v0, p0, Lkj;->n:Lkk;

    invoke-virtual {v0, p1}, Lkk;->a(Ljava/lang/CharSequence;)V

    .line 147
    return-void
.end method

.method public p_()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 419
    invoke-static {p0}, Laz;->b(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public setContentView(I)V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lkj;->n:Lkk;

    invoke-virtual {v0, p1}, Lkk;->a(I)V

    .line 78
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lkj;->n:Lkk;

    invoke-virtual {v0, p1}, Lkk;->a(Landroid/view/View;)V

    .line 83
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lkj;->n:Lkk;

    invoke-virtual {v0, p1, p2}, Lkk;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 88
    return-void
.end method

.method public u_()V
    .locals 2

    .prologue
    .line 170
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 171
    invoke-super {p0}, Ly;->u_()V

    .line 173
    :cond_0
    iget-object v0, p0, Lkj;->n:Lkk;

    invoke-virtual {v0}, Lkk;->g()V

    .line 174
    return-void
.end method

.method public v_()Z
    .locals 2

    .prologue
    .line 383
    invoke-virtual {p0}, Lkj;->p_()Landroid/content/Intent;

    move-result-object v0

    .line 385
    if-eqz v0, :cond_2

    .line 386
    invoke-virtual {p0, v0}, Lkj;->a(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 387
    invoke-static {p0}, Lcu;->a(Landroid/content/Context;)Lcu;

    move-result-object v0

    .line 388
    invoke-virtual {p0, v0}, Lkj;->a(Lcu;)V

    .line 389
    invoke-virtual {v0}, Lcu;->b()V

    .line 393
    :try_start_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->finishAffinity()V

    .line 404
    :goto_0
    const/4 v0, 0x1

    .line 406
    :goto_1
    return v0

    .line 393
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 397
    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lkj;->finish()V

    goto :goto_0

    .line 402
    :cond_1
    invoke-virtual {p0, v0}, Lkj;->b(Landroid/content/Intent;)V

    goto :goto_0

    .line 406
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final w_()Li;
    .locals 3

    .prologue
    .line 457
    iget-object v0, p0, Lkj;->n:Lkk;

    new-instance v1, Lkl;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lkl;-><init>(Lkk;B)V

    return-object v1
.end method

.method public x_()V
    .locals 0

    .prologue
    .line 472
    return-void
.end method
