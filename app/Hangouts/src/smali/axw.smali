.class public final Laxw;
.super Lal;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

.field private b:[Laxx;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;Lae;)V
    .locals 0

    .prologue
    .line 279
    iput-object p1, p0, Laxw;->a:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

    .line 280
    invoke-direct {p0, p2}, Lal;-><init>(Lae;)V

    .line 281
    invoke-virtual {p0}, Laxw;->e()V

    .line 282
    return-void
.end method


# virtual methods
.method public a(I)Lt;
    .locals 2

    .prologue
    .line 286
    iget-object v0, p0, Laxw;->b:[Laxx;

    aget-object v0, v0, p1

    .line 287
    iget-boolean v1, v0, Laxx;->b:Z

    invoke-static {v1}, Lcwz;->a(Z)V

    .line 288
    iget-object v1, p0, Laxw;->a:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

    iget-object v0, v0, Laxx;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Lt;->instantiate(Landroid/content/Context;Ljava/lang/String;)Lt;

    move-result-object v0

    return-object v0
.end method

.method public b()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 294
    iget-object v2, p0, Laxw;->b:[Laxx;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 295
    iget-boolean v4, v4, Laxx;->b:Z

    if-eqz v4, :cond_0

    .line 296
    add-int/lit8 v0, v0, 0x1

    .line 294
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 299
    :cond_1
    return v0
.end method

.method public e()V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 303
    const/4 v0, 0x3

    new-array v0, v0, [Laxx;

    new-instance v1, Laxx;

    const-class v2, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    .line 304
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2, v3, v6}, Laxx;-><init>(Laxw;Ljava/lang/String;ZB)V

    aput-object v1, v0, v6

    new-instance v1, Laxx;

    const-class v2, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    .line 305
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2, v3, v6}, Laxx;-><init>(Laxw;Ljava/lang/String;ZB)V

    aput-object v1, v0, v3

    const/4 v1, 0x2

    new-instance v2, Laxx;

    const-class v3, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;

    .line 306
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Laxw;->a:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

    .line 307
    invoke-virtual {v4}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Laxw;->a:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

    invoke-static {v5}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->f(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)Lyj;

    move-result-object v5

    invoke-static {v4, v5}, Lf;->a(Landroid/content/Context;Lyj;)Z

    move-result v4

    invoke-direct {v2, p0, v3, v4, v6}, Laxx;-><init>(Laxw;Ljava/lang/String;ZB)V

    aput-object v2, v0, v1

    iput-object v0, p0, Laxw;->b:[Laxx;

    .line 309
    invoke-virtual {p0}, Laxw;->d()V

    .line 310
    return-void
.end method
