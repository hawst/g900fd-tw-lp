.class public final Lejc;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lejc;


# instance fields
.field public b:[Leje;

.field public c:Lejb;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 864
    const/4 v0, 0x0

    new-array v0, v0, [Lejc;

    sput-object v0, Lejc;->a:[Lejc;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 865
    invoke-direct {p0}, Lepn;-><init>()V

    .line 868
    sget-object v0, Leje;->a:[Leje;

    iput-object v0, p0, Lejc;->b:[Leje;

    .line 871
    const/4 v0, 0x0

    iput-object v0, p0, Lejc;->c:Lejb;

    .line 865
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 893
    iget-object v1, p0, Lejc;->b:[Leje;

    if-eqz v1, :cond_1

    .line 894
    iget-object v2, p0, Lejc;->b:[Leje;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 895
    if-eqz v4, :cond_0

    .line 896
    const/4 v5, 0x1

    .line 897
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 894
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 901
    :cond_1
    iget-object v1, p0, Lejc;->c:Lejb;

    if-eqz v1, :cond_2

    .line 902
    const/4 v1, 0x2

    iget-object v2, p0, Lejc;->c:Lejb;

    .line 903
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 905
    :cond_2
    iget-object v1, p0, Lejc;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 906
    iput v0, p0, Lejc;->cachedSize:I

    .line 907
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 861
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lejc;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lejc;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lejc;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lejc;->b:[Leje;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Leje;

    iget-object v3, p0, Lejc;->b:[Leje;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lejc;->b:[Leje;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lejc;->b:[Leje;

    :goto_2
    iget-object v2, p0, Lejc;->b:[Leje;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lejc;->b:[Leje;

    new-instance v3, Leje;

    invoke-direct {v3}, Leje;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lejc;->b:[Leje;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lejc;->b:[Leje;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lejc;->b:[Leje;

    new-instance v3, Leje;

    invoke-direct {v3}, Leje;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lejc;->b:[Leje;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lejc;->c:Lejb;

    if-nez v0, :cond_5

    new-instance v0, Lejb;

    invoke-direct {v0}, Lejb;-><init>()V

    iput-object v0, p0, Lejc;->c:Lejb;

    :cond_5
    iget-object v0, p0, Lejc;->c:Lejb;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 876
    iget-object v0, p0, Lejc;->b:[Leje;

    if-eqz v0, :cond_1

    .line 877
    iget-object v1, p0, Lejc;->b:[Leje;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 878
    if-eqz v3, :cond_0

    .line 879
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 877
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 883
    :cond_1
    iget-object v0, p0, Lejc;->c:Lejb;

    if-eqz v0, :cond_2

    .line 884
    const/4 v0, 0x2

    iget-object v1, p0, Lejc;->c:Lejb;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 886
    :cond_2
    iget-object v0, p0, Lejc;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 888
    return-void
.end method
