.class public Lbgh;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method private constructor <init>(Ldrm;)V
    .locals 3

    .prologue
    .line 4456
    iget-object v0, p1, Ldrm;->b:Ldvn;

    iget-object v1, p1, Ldrm;->c:Ljava/lang/Long;

    .line 4457
    invoke-static {v1}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v1

    .line 4456
    invoke-direct {p0, v0, v1, v2}, Lbfz;-><init>(Ldvn;J)V

    .line 4458
    return-void
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 4462
    new-instance v0, Ldrm;

    invoke-direct {v0}, Ldrm;-><init>()V

    .line 4463
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldrm;

    .line 4464
    iget-object v1, v0, Ldrm;->b:Ldvn;

    invoke-static {v1}, Lbgh;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4465
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldrm;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 4467
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbgh;

    invoke-direct {v1, v0}, Lbgh;-><init>(Ldrm;)V

    move-object v0, v1

    goto :goto_0
.end method
