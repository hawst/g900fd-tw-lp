.class public final Leos;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leos;


# instance fields
.field public b:Leot;

.field public c:[I

.field public d:Ljava/lang/Integer;

.field public e:Leox;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [Leos;

    sput-object v0, Leos;->a:[Leos;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 19
    invoke-direct {p0}, Lepn;-><init>()V

    .line 27
    iput-object v1, p0, Leos;->b:Leot;

    .line 30
    sget-object v0, Lept;->e:[I

    iput-object v0, p0, Leos;->c:[I

    .line 33
    iput-object v1, p0, Leos;->d:Ljava/lang/Integer;

    .line 36
    iput-object v1, p0, Leos;->e:Leox;

    .line 19
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 62
    iget-object v0, p0, Leos;->b:Leot;

    if-eqz v0, :cond_4

    .line 63
    const/4 v0, 0x1

    iget-object v2, p0, Leos;->b:Leot;

    .line 64
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 66
    :goto_0
    iget-object v2, p0, Leos;->c:[I

    if-eqz v2, :cond_1

    iget-object v2, p0, Leos;->c:[I

    array-length v2, v2

    if-lez v2, :cond_1

    .line 68
    iget-object v3, p0, Leos;->c:[I

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_0

    aget v5, v3, v1

    .line 70
    invoke-static {v5}, Lepl;->f(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 68
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 72
    :cond_0
    add-int/2addr v0, v2

    .line 73
    iget-object v1, p0, Leos;->c:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 75
    :cond_1
    iget-object v1, p0, Leos;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 76
    const/4 v1, 0x3

    iget-object v2, p0, Leos;->d:Ljava/lang/Integer;

    .line 77
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 79
    :cond_2
    iget-object v1, p0, Leos;->e:Leox;

    if-eqz v1, :cond_3

    .line 80
    const/4 v1, 0x4

    iget-object v2, p0, Leos;->e:Leox;

    .line 81
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 83
    :cond_3
    iget-object v1, p0, Leos;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 84
    iput v0, p0, Leos;->cachedSize:I

    .line 85
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 15
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Leos;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Leos;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Leos;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Leos;->b:Leot;

    if-nez v0, :cond_2

    new-instance v0, Leot;

    invoke-direct {v0}, Leot;-><init>()V

    iput-object v0, p0, Leos;->b:Leot;

    :cond_2
    iget-object v0, p0, Leos;->b:Leot;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x10

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Leos;->c:[I

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [I

    iget-object v2, p0, Leos;->c:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Leos;->c:[I

    :goto_1
    iget-object v1, p0, Leos;->c:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Leos;->c:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    aput v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v1, p0, Leos;->c:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    :cond_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leos;->d:Ljava/lang/Integer;

    goto :goto_0

    :cond_5
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leos;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Leos;->e:Leox;

    if-nez v0, :cond_6

    new-instance v0, Leox;

    invoke-direct {v0}, Leox;-><init>()V

    iput-object v0, p0, Leos;->e:Leox;

    :cond_6
    iget-object v0, p0, Leos;->e:Leox;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 41
    iget-object v0, p0, Leos;->b:Leot;

    if-eqz v0, :cond_0

    .line 42
    const/4 v0, 0x1

    iget-object v1, p0, Leos;->b:Leot;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 44
    :cond_0
    iget-object v0, p0, Leos;->c:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Leos;->c:[I

    array-length v0, v0

    if-lez v0, :cond_1

    .line 45
    iget-object v1, p0, Leos;->c:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget v3, v1, v0

    .line 46
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->a(II)V

    .line 45
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 49
    :cond_1
    iget-object v0, p0, Leos;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 50
    const/4 v0, 0x3

    iget-object v1, p0, Leos;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 52
    :cond_2
    iget-object v0, p0, Leos;->e:Leox;

    if-eqz v0, :cond_3

    .line 53
    const/4 v0, 0x4

    iget-object v1, p0, Leos;->e:Leox;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 55
    :cond_3
    iget-object v0, p0, Leos;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 57
    return-void
.end method
