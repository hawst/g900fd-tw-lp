.class public final Lauc;
.super Latu;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Latu",
        "<",
        "Lcom/google/android/libraries/hangouts/video/SelfRendererGB;",
        ">;"
    }
.end annotation


# instance fields
.field private final N:Lcom/google/android/libraries/hangouts/video/SelfViewGB;


# direct methods
.method public constructor <init>(Latd;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;Lasx;Ljava/lang/Object;IILandroid/view/ViewGroup;)V
    .locals 3

    .prologue
    .line 46
    invoke-direct/range {p0 .. p8}, Latu;-><init>(Latd;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;Lasx;Ljava/lang/Object;IILandroid/view/ViewGroup;)V

    .line 48
    invoke-virtual {p1}, Latd;->e()Lcom/google/android/libraries/hangouts/video/RendererManager;

    move-result-object v0

    iget-object v1, p0, Lauc;->G:Latz;

    .line 49
    invoke-virtual {p0}, Lauc;->B()Lcom/google/android/libraries/hangouts/video/CameraSpecification;

    move-result-object v2

    .line 48
    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/hangouts/video/RendererManager;->createSelfRendererGB(Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;Lcom/google/android/libraries/hangouts/video/CameraSpecification;)Lcom/google/android/libraries/hangouts/video/SelfRendererGB;

    move-result-object v0

    iput-object v0, p0, Lauc;->H:Lcom/google/android/libraries/hangouts/video/SelfRenderer;

    .line 51
    new-instance v0, Lcom/google/android/libraries/hangouts/video/SelfViewGB;

    iget-object v1, p0, Lauc;->e:Latd;

    invoke-virtual {v1}, Latd;->a()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/libraries/hangouts/video/SelfViewGB;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lauc;->N:Lcom/google/android/libraries/hangouts/video/SelfViewGB;

    .line 52
    iget-object v0, p0, Lauc;->N:Lcom/google/android/libraries/hangouts/video/SelfViewGB;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/SelfViewGB;->setZOrderMediaOverlay(Z)V

    .line 53
    iget-object v0, p0, Lauc;->B:Latg;

    iget-object v0, v0, Latg;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, Lauc;->N:Lcom/google/android/libraries/hangouts/video/SelfViewGB;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 54
    return-void
.end method

.method static synthetic a(Lauc;)Lcom/google/android/libraries/hangouts/video/SelfViewGB;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lauc;->N:Lcom/google/android/libraries/hangouts/video/SelfViewGB;

    return-object v0
.end method


# virtual methods
.method protected a(Landroid/content/Context;)Latg;
    .locals 1

    .prologue
    .line 58
    new-instance v0, Laud;

    invoke-direct {v0, p0, p1}, Laud;-><init>(Lauc;Landroid/content/Context;)V

    return-object v0
.end method

.method protected a(I)V
    .locals 3

    .prologue
    .line 70
    iget-object v1, p0, Lauc;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 71
    :try_start_0
    invoke-super {p0, p1}, Latu;->a(I)V

    .line 72
    iget-object v2, p0, Lauc;->N:Lcom/google/android/libraries/hangouts/video/SelfViewGB;

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/libraries/hangouts/video/SelfViewGB;->setVisibility(I)V

    .line 73
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    .line 72
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method e()Z
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    return v0
.end method

.method protected z()Latz;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Latu",
            "<",
            "Lcom/google/android/libraries/hangouts/video/SelfRendererGB;",
            ">.atz;"
        }
    .end annotation

    .prologue
    .line 78
    new-instance v0, Laue;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Laue;-><init>(Lauc;B)V

    return-object v0
.end method
