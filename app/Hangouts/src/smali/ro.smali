.class final Lro;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:I

.field final synthetic b:Lrn;

.field private c:Lrp;

.field private d:Lrp;


# direct methods
.method private constructor <init>(Lrn;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1065
    iput-object p1, p0, Lro;->b:Lrn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1066
    iput-object v0, p0, Lro;->c:Lrp;

    .line 1067
    iput-object v0, p0, Lro;->d:Lrp;

    .line 1069
    const/4 v0, 0x0

    iput v0, p0, Lro;->a:I

    return-void
.end method

.method synthetic constructor <init>(Lrn;B)V
    .locals 0

    .prologue
    .line 1065
    invoke-direct {p0, p1}, Lro;-><init>(Lrn;)V

    return-void
.end method


# virtual methods
.method a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1078
    iget-object v0, p0, Lro;->d:Lrp;

    if-eqz v0, :cond_0

    .line 1079
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "BUG: Invalid newbuf() before copy()"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1082
    :cond_0
    new-instance v0, Lrp;

    invoke-direct {v0, v2}, Lrp;-><init>(B)V

    .line 1084
    iget-object v1, p0, Lro;->b:Lrn;

    iget-object v1, v1, Lrn;->a:Ljava/io/ByteArrayOutputStream;

    iput-object v1, v0, Lrp;->a:Ljava/io/ByteArrayOutputStream;

    .line 1085
    iget-object v1, p0, Lro;->b:Lrn;

    iget v1, v1, Lrn;->b:I

    iput v1, v0, Lrp;->b:I

    .line 1087
    iget-object v1, p0, Lro;->c:Lrp;

    iput-object v1, v0, Lrp;->c:Lrp;

    .line 1088
    iput-object v0, p0, Lro;->c:Lrp;

    .line 1090
    iget v0, p0, Lro;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lro;->a:I

    .line 1092
    iget-object v0, p0, Lro;->b:Lrn;

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v1, v0, Lrn;->a:Ljava/io/ByteArrayOutputStream;

    .line 1093
    iget-object v0, p0, Lro;->b:Lrn;

    iput v2, v0, Lrn;->b:I

    .line 1094
    return-void
.end method

.method b()V
    .locals 4

    .prologue
    .line 1100
    iget-object v0, p0, Lro;->b:Lrn;

    iget-object v0, v0, Lrn;->a:Ljava/io/ByteArrayOutputStream;

    .line 1101
    iget-object v1, p0, Lro;->b:Lrn;

    iget v1, v1, Lrn;->b:I

    .line 1103
    iget-object v2, p0, Lro;->b:Lrn;

    iget-object v3, p0, Lro;->c:Lrp;

    iget-object v3, v3, Lrp;->a:Ljava/io/ByteArrayOutputStream;

    iput-object v3, v2, Lrn;->a:Ljava/io/ByteArrayOutputStream;

    .line 1104
    iget-object v2, p0, Lro;->b:Lrn;

    iget-object v3, p0, Lro;->c:Lrp;

    iget v3, v3, Lrp;->b:I

    iput v3, v2, Lrn;->b:I

    .line 1106
    iget-object v2, p0, Lro;->c:Lrp;

    iput-object v2, p0, Lro;->d:Lrp;

    .line 1109
    iget-object v2, p0, Lro;->c:Lrp;

    iget-object v2, v2, Lrp;->c:Lrp;

    iput-object v2, p0, Lro;->c:Lrp;

    .line 1110
    iget v2, p0, Lro;->a:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lro;->a:I

    .line 1112
    iget-object v2, p0, Lro;->d:Lrp;

    iput-object v0, v2, Lrp;->a:Ljava/io/ByteArrayOutputStream;

    .line 1113
    iget-object v0, p0, Lro;->d:Lrp;

    iput v1, v0, Lrp;->b:I

    .line 1114
    return-void
.end method

.method c()V
    .locals 3

    .prologue
    .line 1120
    iget-object v0, p0, Lro;->b:Lrn;

    iget-object v1, p0, Lro;->d:Lrp;

    iget-object v1, v1, Lrp;->a:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    iget-object v2, p0, Lro;->d:Lrp;

    iget v2, v2, Lrp;->b:I

    invoke-virtual {v0, v1, v2}, Lrn;->a([BI)V

    .line 1123
    const/4 v0, 0x0

    iput-object v0, p0, Lro;->d:Lrp;

    .line 1124
    return-void
.end method

.method d()Lrq;
    .locals 3

    .prologue
    .line 1130
    new-instance v0, Lrq;

    iget-object v1, p0, Lro;->b:Lrn;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lrq;-><init>(Lrn;B)V

    .line 1132
    iget-object v1, p0, Lro;->b:Lrn;

    iget v1, v1, Lrn;->b:I

    iput v1, v0, Lrq;->a:I

    .line 1133
    iget v1, p0, Lro;->a:I

    iput v1, v0, Lrq;->b:I

    .line 1135
    return-object v0
.end method
