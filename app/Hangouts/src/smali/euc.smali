.class public final Leuc;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leuc;


# instance fields
.field public b:Ljava/lang/String;

.field public c:[Ljava/lang/String;

.field public d:Levt;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/Double;

.field public h:Ljava/lang/Double;

.field public i:Ljava/lang/Integer;

.field public j:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    new-array v0, v0, [Leuc;

    sput-object v0, Leuc;->a:[Leuc;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 22
    invoke-direct {p0}, Lepn;-><init>()V

    .line 27
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Leuc;->c:[Ljava/lang/String;

    .line 30
    iput-object v1, p0, Leuc;->d:Levt;

    .line 33
    iput-object v1, p0, Leuc;->e:Ljava/lang/Integer;

    .line 22
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 84
    iget-object v0, p0, Leuc;->b:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 85
    const/4 v0, 0x1

    iget-object v2, p0, Leuc;->b:Ljava/lang/String;

    .line 86
    invoke-static {v0, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 88
    :goto_0
    iget-object v2, p0, Leuc;->d:Levt;

    if-eqz v2, :cond_0

    .line 89
    const/4 v2, 0x2

    iget-object v3, p0, Leuc;->d:Levt;

    .line 90
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 92
    :cond_0
    iget-object v2, p0, Leuc;->e:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 93
    const/4 v2, 0x3

    iget-object v3, p0, Leuc;->e:Ljava/lang/Integer;

    .line 94
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 96
    :cond_1
    iget-object v2, p0, Leuc;->f:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 97
    const/4 v2, 0x4

    iget-object v3, p0, Leuc;->f:Ljava/lang/String;

    .line 98
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 100
    :cond_2
    iget-object v2, p0, Leuc;->g:Ljava/lang/Double;

    if-eqz v2, :cond_3

    .line 101
    const/4 v2, 0x5

    iget-object v3, p0, Leuc;->g:Ljava/lang/Double;

    .line 102
    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    add-int/2addr v0, v2

    .line 104
    :cond_3
    iget-object v2, p0, Leuc;->c:[Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Leuc;->c:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 106
    iget-object v3, p0, Leuc;->c:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_4

    aget-object v5, v3, v1

    .line 108
    invoke-static {v5}, Lepl;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 106
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 110
    :cond_4
    add-int/2addr v0, v2

    .line 111
    iget-object v1, p0, Leuc;->c:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 113
    :cond_5
    iget-object v1, p0, Leuc;->h:Ljava/lang/Double;

    if-eqz v1, :cond_6

    .line 114
    const/4 v1, 0x7

    iget-object v2, p0, Leuc;->h:Ljava/lang/Double;

    .line 115
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 117
    :cond_6
    iget-object v1, p0, Leuc;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 118
    const/16 v1, 0x8

    iget-object v2, p0, Leuc;->i:Ljava/lang/Integer;

    .line 119
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 121
    :cond_7
    iget-object v1, p0, Leuc;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 122
    const/16 v1, 0x9

    iget-object v2, p0, Leuc;->j:Ljava/lang/Integer;

    .line 123
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 125
    :cond_8
    iget-object v1, p0, Leuc;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 126
    iput v0, p0, Leuc;->cachedSize:I

    .line 127
    return v0

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 18
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Leuc;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Leuc;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Leuc;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leuc;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Leuc;->d:Levt;

    if-nez v0, :cond_2

    new-instance v0, Levt;

    invoke-direct {v0}, Levt;-><init>()V

    iput-object v0, p0, Leuc;->d:Levt;

    :cond_2
    iget-object v0, p0, Leuc;->d:Levt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v4, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leuc;->e:Ljava/lang/Integer;

    goto :goto_0

    :cond_4
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leuc;->e:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leuc;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->b()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Leuc;->g:Ljava/lang/Double;

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Leuc;->c:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    iget-object v2, p0, Leuc;->c:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Leuc;->c:[Ljava/lang/String;

    :goto_1
    iget-object v1, p0, Leuc;->c:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_5

    iget-object v1, p0, Leuc;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    iget-object v1, p0, Leuc;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->b()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Leuc;->h:Ljava/lang/Double;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->h()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leuc;->i:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->h()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leuc;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x29 -> :sswitch_5
        0x32 -> :sswitch_6
        0x39 -> :sswitch_7
        0x45 -> :sswitch_8
        0x4d -> :sswitch_9
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 48
    iget-object v0, p0, Leuc;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 49
    const/4 v0, 0x1

    iget-object v1, p0, Leuc;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 51
    :cond_0
    iget-object v0, p0, Leuc;->d:Levt;

    if-eqz v0, :cond_1

    .line 52
    const/4 v0, 0x2

    iget-object v1, p0, Leuc;->d:Levt;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 54
    :cond_1
    iget-object v0, p0, Leuc;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 55
    const/4 v0, 0x3

    iget-object v1, p0, Leuc;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 57
    :cond_2
    iget-object v0, p0, Leuc;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 58
    const/4 v0, 0x4

    iget-object v1, p0, Leuc;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 60
    :cond_3
    iget-object v0, p0, Leuc;->g:Ljava/lang/Double;

    if-eqz v0, :cond_4

    .line 61
    const/4 v0, 0x5

    iget-object v1, p0, Leuc;->g:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(ID)V

    .line 63
    :cond_4
    iget-object v0, p0, Leuc;->c:[Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 64
    iget-object v1, p0, Leuc;->c:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 65
    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, Lepl;->a(ILjava/lang/String;)V

    .line 64
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 68
    :cond_5
    iget-object v0, p0, Leuc;->h:Ljava/lang/Double;

    if-eqz v0, :cond_6

    .line 69
    const/4 v0, 0x7

    iget-object v1, p0, Leuc;->h:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(ID)V

    .line 71
    :cond_6
    iget-object v0, p0, Leuc;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 72
    const/16 v0, 0x8

    iget-object v1, p0, Leuc;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->b(II)V

    .line 74
    :cond_7
    iget-object v0, p0, Leuc;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 75
    const/16 v0, 0x9

    iget-object v1, p0, Leuc;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->b(II)V

    .line 77
    :cond_8
    iget-object v0, p0, Leuc;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 79
    return-void
.end method
