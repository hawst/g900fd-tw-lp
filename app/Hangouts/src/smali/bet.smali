.class public Lbet;
.super Lbep;
.source "PG"


# static fields
.field private static final c:[I

.field private static final h:[I

.field private static final i:[I

.field private static final j:[I

.field private static final serialVersionUID:J = 0x2L


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3941
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lbet;->c:[I

    .line 3946
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lbet;->h:[I

    .line 3952
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lbet;->i:[I

    .line 3960
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lbet;->j:[I

    return-void

    .line 3941
    nop

    :array_0
    .array-data 4
        0x1
        0x2
    .end array-data

    .line 3946
    :array_1
    .array-data 4
        0x1
        0x2
        0xa
    .end array-data

    .line 3952
    :array_2
    .array-data 4
        0x1
        0x2
        0x6
        0x5
        0x7
    .end array-data

    .line 3960
    :array_3
    .array-data 4
        0x1
        0x2
        0x6
        0x5
        0x7
        0xa
    .end array-data
.end method

.method public constructor <init>(Ljava/util/List;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 3970
    invoke-direct {p0}, Lbep;-><init>()V

    .line 3971
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lbet;->a:Ljava/util/List;

    .line 3972
    iput-boolean p2, p0, Lbet;->b:Z

    .line 3973
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 3977
    new-instance v3, Lduz;

    invoke-direct {v3}, Lduz;-><init>()V

    .line 3979
    iget-object v0, p0, Lbet;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    .line 3980
    new-array v0, v4, [Ldui;

    iput-object v0, v3, Lduz;->c:[Ldui;

    move v1, v2

    .line 3981
    :goto_0
    if-ge v1, v4, :cond_0

    .line 3982
    new-instance v5, Ldui;

    invoke-direct {v5}, Ldui;-><init>()V

    .line 3983
    iget-object v0, p0, Lbet;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v5, Ldui;->c:Ljava/lang/String;

    .line 3985
    iget-object v0, v5, Ldui;->c:Ljava/lang/String;

    iput-object v0, v5, Ldui;->b:Ljava/lang/String;

    .line 3986
    iget-object v0, v3, Lduz;->c:[Ldui;

    aput-object v5, v0, v1

    .line 3981
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 3989
    :cond_0
    invoke-virtual {p0, p1, p2}, Lbet;->c(Ljava/lang/String;I)Ldvm;

    move-result-object v0

    iput-object v0, v3, Lduz;->b:Ldvm;

    .line 3991
    const-string v0, "babel_enable_last_seen"

    invoke-static {v0, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3993
    iget-boolean v0, p0, Lbet;->b:Z

    if-eqz v0, :cond_1

    sget-object v0, Lbet;->j:[I

    :goto_1
    iput-object v0, v3, Lduz;->d:[I

    .line 3999
    :goto_2
    return-object v3

    .line 3993
    :cond_1
    sget-object v0, Lbet;->h:[I

    goto :goto_1

    .line 3996
    :cond_2
    iget-boolean v0, p0, Lbet;->b:Z

    if-eqz v0, :cond_3

    sget-object v0, Lbet;->i:[I

    :goto_3
    iput-object v0, v3, Lduz;->d:[I

    goto :goto_2

    :cond_3
    sget-object v0, Lbet;->c:[I

    goto :goto_3
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4004
    const-string v0, "presence/querypresence"

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4015
    const-string v0, "background_queue"

    return-object v0
.end method

.method public d()J
    .locals 3

    .prologue
    .line 4010
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x1e

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    return-wide v0
.end method
