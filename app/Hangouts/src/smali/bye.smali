.class final Lbye;
.super Landroid/os/HandlerThread;
.source "PG"

# interfaces
.implements Landroid/os/Handler$Callback;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1001
    const-string v0, "GifDecoder"

    invoke-direct {p0, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 1002
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1006
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lbyd;

    .line 1007
    if-eqz v0, :cond_0

    invoke-static {v0}, Lbyd;->a(Lbyd;)Landroid/graphics/Bitmap;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {v0}, Lbyd;->b(Lbyd;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    move v0, v1

    .line 1044
    :goto_0
    return v0

    .line 1011
    :cond_1
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    move v0, v2

    .line 1044
    goto :goto_0

    .line 1016
    :cond_2
    :pswitch_1
    :try_start_0
    invoke-static {v0}, Lbyd;->c(Lbyd;)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1022
    :goto_1
    invoke-static {v0}, Lbyd;->e(Lbyd;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1023
    invoke-static {v0}, Lbyd;->f(Lbyd;)I

    move-result v3

    if-nez v3, :cond_5

    .line 1024
    invoke-static {v0}, Lbyd;->g(Lbyd;)Z

    .line 1025
    const-string v3, "Babel"

    const-string v4, "Could not read first frame of the gif."

    invoke-static {v3, v4}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1034
    :cond_3
    :goto_2
    invoke-static {v0}, Lbyd;->e(Lbyd;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-static {v0}, Lbyd;->k(Lbyd;)Z

    move-result v3

    if-nez v3, :cond_4

    invoke-static {v0}, Lbyd;->l(Lbyd;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1035
    :cond_4
    invoke-static {v0}, Lbyd;->n(Lbyd;)Landroid/os/Handler;

    move-result-object v3

    invoke-static {v0}, Lbyd;->n(Lbyd;)Landroid/os/Handler;

    move-result-object v4

    const/16 v5, 0xb

    .line 1036
    invoke-static {v0}, Lbyd;->m(Lbyd;)I

    move-result v0

    .line 1035
    invoke-virtual {v4, v5, v0, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    move v0, v1

    .line 1037
    goto :goto_0

    .line 1018
    :catch_0
    move-exception v3

    invoke-static {v0}, Lbyd;->d(Lbyd;)Z

    goto :goto_1

    .line 1026
    :cond_5
    invoke-static {v0}, Lbyd;->f(Lbyd;)I

    move-result v3

    if-le v3, v1, :cond_6

    invoke-static {v0}, Lbyd;->h(Lbyd;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1028
    invoke-static {v0}, Lbyd;->i(Lbyd;)V

    goto :goto_2

    .line 1031
    :cond_6
    invoke-static {v0}, Lbyd;->j(Lbyd;)Z

    goto :goto_2

    .line 1040
    :pswitch_2
    invoke-static {v0}, Lbyd;->i(Lbyd;)V

    move v0, v1

    .line 1041
    goto :goto_0

    .line 1011
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
