.class public Lbdq;
.super Lbdp;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final a:[B


# direct methods
.method public constructor <init>([B)V
    .locals 0

    .prologue
    .line 223
    invoke-direct {p0}, Lbdp;-><init>()V

    .line 224
    iput-object p1, p0, Lbdq;->a:[B

    .line 225
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 3

    .prologue
    .line 229
    new-instance v1, Ldyf;

    invoke-direct {v1}, Ldyf;-><init>()V

    .line 230
    invoke-virtual {p0, p1, p2}, Lbdq;->c(Ljava/lang/String;I)Ldvm;

    move-result-object v0

    iput-object v0, v1, Ldyf;->b:Ldvm;

    .line 232
    :try_start_0
    new-instance v0, Ldxv;

    invoke-direct {v0}, Ldxv;-><init>()V

    iget-object v2, p0, Lbdq;->a:[B

    invoke-static {v0, v2}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldxv;

    iput-object v0, v1, Ldyf;->c:Ldxv;
    :try_end_0
    .catch Lepq; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 237
    :goto_0
    return-object v0

    .line 233
    :catch_0
    move-exception v0

    .line 234
    const-string v1, "Babel"

    const-string v2, "Parse failed"

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 235
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 242
    const-string v0, "broadcasts/add"

    return-object v0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 247
    invoke-static {}, Lapx;->A()J

    move-result-wide v0

    return-wide v0
.end method
