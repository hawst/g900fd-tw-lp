.class public final Lbqo;
.super Lbnj;
.source "PG"


# instance fields
.field private final a:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/Integer;

.field private f:Z

.field private g:Ljava/lang/String;

.field private h:Z

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lyj;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 30
    invoke-direct {p0, p1}, Lbnj;-><init>(Lyj;)V

    .line 21
    iput-object v1, p0, Lbqo;->d:Ljava/lang/String;

    .line 22
    iput-object v1, p0, Lbqo;->e:Ljava/lang/Integer;

    .line 23
    iput-boolean v0, p0, Lbqo;->f:Z

    .line 25
    iput-boolean v0, p0, Lbqo;->h:Z

    .line 31
    iput-object p2, p0, Lbqo;->a:Ljava/lang/String;

    .line 32
    return-void
.end method

.method private a(Lyt;Ljava/lang/String;)Ljava/lang/String;
    .locals 10

    .prologue
    .line 69
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateConversationNameLocally conversationId: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbqo;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    :cond_0
    invoke-virtual {p1}, Lyt;->a()V

    .line 77
    :try_start_0
    iget-object v0, p0, Lbqo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lyt;->n(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 79
    iget-object v0, p0, Lbqo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lyt;->Q(Ljava/lang/String;)J

    move-result-wide v3

    .line 80
    const-wide/16 v5, 0x0

    .line 81
    iget-object v0, p0, Lbqo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lyt;->A(Ljava/lang/String;)I

    move-result v0

    .line 82
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 83
    const-wide v5, 0x7fffffffffffffffL

    .line 87
    :cond_1
    new-instance v0, Lbqt;

    iget-object v1, p0, Lbqo;->a:Ljava/lang/String;

    iget-object v2, p0, Lbqo;->b:Lyj;

    invoke-virtual {v2}, Lyj;->c()Lbdk;

    move-result-object v2

    iget-object v8, p0, Lbqo;->d:Ljava/lang/String;

    move-object v7, p2

    invoke-direct/range {v0 .. v8}, Lbqt;-><init>(Ljava/lang/String;Lbdk;JJLjava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lbqo;->c:Lbnl;

    .line 89
    invoke-virtual {v0, p1, v1}, Lbqt;->a(Lyt;Lbnl;)V

    .line 90
    invoke-virtual {p1}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 92
    invoke-virtual {p1}, Lyt;->c()V

    .line 94
    return-object v9

    .line 92
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Lyt;->c()V

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 7

    .prologue
    .line 44
    new-instance v1, Lyt;

    iget-object v0, p0, Lbqo;->b:Lyj;

    invoke-direct {v1, v0}, Lyt;-><init>(Lyj;)V

    .line 45
    iget-object v0, p0, Lbqo;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbqo;->a:Ljava/lang/String;

    invoke-static {v0}, Lyt;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 46
    invoke-static {}, Lyt;->h()Ljava/lang/String;

    move-result-object v0

    .line 47
    invoke-direct {p0, v1, v0}, Lbqo;->a(Lyt;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 48
    iget-object v3, p0, Lbqo;->c:Lbnl;

    new-instance v4, Lbex;

    iget-object v5, p0, Lbqo;->a:Ljava/lang/String;

    iget-object v6, p0, Lbqo;->d:Ljava/lang/String;

    invoke-direct {v4, v0, v5, v6, v2}, Lbex;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lbnl;->a(Lbea;)V

    .line 51
    :cond_0
    iget-object v0, p0, Lbqo;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 52
    new-instance v0, Lbqp;

    iget-object v2, p0, Lbqo;->a:Ljava/lang/String;

    iget-object v3, p0, Lbqo;->e:Ljava/lang/Integer;

    .line 53
    const/4 v4, 0x0

    invoke-static {v3, v4}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v3

    invoke-direct {v0, v2, v3}, Lbqp;-><init>(Ljava/lang/String;I)V

    iget-object v2, p0, Lbqo;->c:Lbnl;

    .line 54
    invoke-virtual {v0, v1, v2}, Lbqp;->a(Lyt;Lbnl;)V

    .line 56
    :cond_1
    iget-boolean v0, p0, Lbqo;->h:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lbqo;->f:Z

    if-eqz v0, :cond_6

    .line 57
    :cond_2
    invoke-virtual {v1}, Lyt;->a()V

    :try_start_0
    iget-object v0, p0, Lbqo;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lyt;->ag(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-boolean v3, p0, Lbqo;->f:Z

    if-eqz v3, :cond_4

    iget-object v3, p0, Lbqo;->g:Ljava/lang/String;

    invoke-virtual {v1, v0, v3}, Lyt;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-boolean v3, p0, Lbqo;->h:Z

    if-eqz v3, :cond_3

    iget-object v3, p0, Lbqo;->i:Ljava/lang/String;

    invoke-virtual {v1, v0, v3}, Lyt;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lyt;->c()V

    throw v0

    :cond_5
    :try_start_1
    invoke-virtual {v1}, Lyt;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v1}, Lyt;->c()V

    .line 59
    :cond_6
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 39
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lbqo;->e:Ljava/lang/Integer;

    .line 40
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lbqo;->d:Ljava/lang/String;

    .line 36
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 193
    iput-object p1, p0, Lbqo;->g:Ljava/lang/String;

    .line 194
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbqo;->f:Z

    .line 195
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 198
    iput-object p1, p0, Lbqo;->i:Ljava/lang/String;

    .line 199
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbqo;->h:Z

    .line 200
    return-void
.end method
