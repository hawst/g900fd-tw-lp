.class public Lbgm;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lddi;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x3

    const/4 v0, 0x0

    .line 1347
    iget-object v1, p1, Lddi;->b:Ldcw;

    invoke-direct {p0, v1}, Lbfz;-><init>(Ldcw;)V

    .line 1343
    iput-object v2, p0, Lbgm;->g:Ljava/lang/String;

    .line 1344
    iput-object v2, p0, Lbgm;->h:Ljava/lang/String;

    .line 1349
    iget-object v1, p1, Lddi;->c:Leoe;

    iget-object v1, v1, Leoe;->c:[Lens;

    .line 1350
    if-eqz v1, :cond_0

    array-length v2, v1

    if-lez v2, :cond_0

    .line 1351
    aget-object v1, v1, v0

    .line 1352
    if-eqz v1, :cond_0

    iget-object v2, v1, Lens;->e:Lenr;

    iget-object v2, v2, Lenr;->b:Ljava/lang/Integer;

    invoke-static {v2, v0}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v2

    if-ne v2, v4, :cond_0

    .line 1354
    iget-object v2, v1, Lens;->c:Lemz;

    .line 1358
    if-eqz v2, :cond_0

    iget-object v3, v2, Lemz;->b:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 1359
    iget-object v2, v2, Lemz;->b:Ljava/lang/String;

    iput-object v2, p0, Lbgm;->h:Ljava/lang/String;

    .line 1360
    iget-object v1, v1, Lens;->e:Lenr;

    iget-object v1, v1, Lenr;->e:Lekn;

    .line 1361
    iget-object v2, v1, Lekn;->b:Ljava/lang/Integer;

    invoke-static {v2, v0}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v2

    if-ne v2, v4, :cond_0

    .line 1363
    iget-object v1, v1, Lekn;->d:Leko;

    .line 1364
    if-eqz v1, :cond_0

    .line 1365
    iget-object v1, v1, Leko;->c:[Lekp;

    .line 1366
    if-eqz v1, :cond_0

    array-length v2, v1

    if-lez v2, :cond_0

    .line 1367
    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 1368
    if-eqz v3, :cond_1

    iget-object v4, v3, Lekp;->c:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 1369
    iget-object v0, v3, Lekp;->c:Ljava/lang/String;

    iput-object v0, p0, Lbgm;->g:Ljava/lang/String;

    .line 1379
    :cond_0
    return-void

    .line 1367
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 1383
    new-instance v0, Lddi;

    invoke-direct {v0}, Lddi;-><init>()V

    .line 1384
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Lddi;

    .line 1385
    iget-object v1, v0, Lddi;->b:Ldcw;

    invoke-static {v1}, Lbgm;->a(Ldcw;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1386
    new-instance v1, Lbgk;

    iget-object v0, v0, Lddi;->b:Ldcw;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldcw;)V

    move-object v0, v1

    .line 1388
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbgm;

    invoke-direct {v1, v0}, Lbgm;-><init>(Lddi;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Lyt;Lbnl;)V
    .locals 4

    .prologue
    .line 1395
    invoke-super {p0, p1, p2}, Lbfz;->a(Lyt;Lbnl;)V

    .line 1396
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_0

    .line 1397
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetAudioDataResponse.processResponse: retrieved audio with id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbgm;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and has stream url of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbgm;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1400
    :cond_0
    iget-object v0, p0, Lbgm;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lbgm;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1401
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x14

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    .line 1402
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 1403
    iget-object v2, p0, Lbgm;->h:Ljava/lang/String;

    iget-object v3, p0, Lbgm;->g:Ljava/lang/String;

    invoke-virtual {p1, v2, v3, v0, v1}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;J)V

    .line 1404
    iget-object v0, p0, Lbgm;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lyt;->ad(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lyp;->d(Lyt;Ljava/lang/String;)V

    .line 1406
    :cond_1
    return-void
.end method
