.class public final Ldsp;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldsp;


# instance fields
.field public b:Ldvm;

.field public c:[I

.field public d:[I

.field public e:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17270
    const/4 v0, 0x0

    new-array v0, v0, [Ldsp;

    sput-object v0, Ldsp;->a:[Ldsp;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17271
    invoke-direct {p0}, Lepn;-><init>()V

    .line 17296
    const/4 v0, 0x0

    iput-object v0, p0, Ldsp;->b:Ldvm;

    .line 17299
    sget-object v0, Lept;->e:[I

    iput-object v0, p0, Ldsp;->c:[I

    .line 17302
    sget-object v0, Lept;->e:[I

    iput-object v0, p0, Ldsp;->d:[I

    .line 17271
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 17332
    iget-object v0, p0, Ldsp;->b:Ldvm;

    if-eqz v0, :cond_5

    .line 17333
    const/4 v0, 0x1

    iget-object v2, p0, Ldsp;->b:Ldvm;

    .line 17334
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 17336
    :goto_0
    iget-object v2, p0, Ldsp;->c:[I

    if-eqz v2, :cond_1

    iget-object v2, p0, Ldsp;->c:[I

    array-length v2, v2

    if-lez v2, :cond_1

    .line 17338
    iget-object v4, p0, Ldsp;->c:[I

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_0

    aget v6, v4, v2

    .line 17340
    invoke-static {v6}, Lepl;->f(I)I

    move-result v6

    add-int/2addr v3, v6

    .line 17338
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 17342
    :cond_0
    add-int/2addr v0, v3

    .line 17343
    iget-object v2, p0, Ldsp;->c:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 17345
    :cond_1
    iget-object v2, p0, Ldsp;->d:[I

    if-eqz v2, :cond_3

    iget-object v2, p0, Ldsp;->d:[I

    array-length v2, v2

    if-lez v2, :cond_3

    .line 17347
    iget-object v3, p0, Ldsp;->d:[I

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v1, v4, :cond_2

    aget v5, v3, v1

    .line 17349
    invoke-static {v5}, Lepl;->f(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 17347
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 17351
    :cond_2
    add-int/2addr v0, v2

    .line 17352
    iget-object v1, p0, Ldsp;->d:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 17354
    :cond_3
    iget-object v1, p0, Ldsp;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 17355
    const/4 v1, 0x4

    iget-object v2, p0, Ldsp;->e:Ljava/lang/Boolean;

    .line 17356
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 17358
    :cond_4
    iget-object v1, p0, Ldsp;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 17359
    iput v0, p0, Ldsp;->cachedSize:I

    .line 17360
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 17267
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldsp;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldsp;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldsp;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldsp;->b:Ldvm;

    if-nez v0, :cond_2

    new-instance v0, Ldvm;

    invoke-direct {v0}, Ldvm;-><init>()V

    iput-object v0, p0, Ldsp;->b:Ldvm;

    :cond_2
    iget-object v0, p0, Ldsp;->b:Ldvm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x10

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Ldsp;->c:[I

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [I

    iget-object v2, p0, Ldsp;->c:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Ldsp;->c:[I

    :goto_1
    iget-object v1, p0, Ldsp;->c:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Ldsp;->c:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    aput v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v1, p0, Ldsp;->c:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Ldsp;->d:[I

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [I

    iget-object v2, p0, Ldsp;->d:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Ldsp;->d:[I

    :goto_2
    iget-object v1, p0, Ldsp;->d:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    iget-object v1, p0, Ldsp;->d:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    aput v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v1, p0, Ldsp;->d:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    aput v2, v1, v0

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldsp;->e:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 17309
    iget-object v1, p0, Ldsp;->b:Ldvm;

    if-eqz v1, :cond_0

    .line 17310
    const/4 v1, 0x1

    iget-object v2, p0, Ldsp;->b:Ldvm;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 17312
    :cond_0
    iget-object v1, p0, Ldsp;->c:[I

    if-eqz v1, :cond_1

    iget-object v1, p0, Ldsp;->c:[I

    array-length v1, v1

    if-lez v1, :cond_1

    .line 17313
    iget-object v2, p0, Ldsp;->c:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget v4, v2, v1

    .line 17314
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Lepl;->a(II)V

    .line 17313
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 17317
    :cond_1
    iget-object v1, p0, Ldsp;->d:[I

    if-eqz v1, :cond_2

    iget-object v1, p0, Ldsp;->d:[I

    array-length v1, v1

    if-lez v1, :cond_2

    .line 17318
    iget-object v1, p0, Ldsp;->d:[I

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_2

    aget v3, v1, v0

    .line 17319
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lepl;->a(II)V

    .line 17318
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 17322
    :cond_2
    iget-object v0, p0, Ldsp;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 17323
    const/4 v0, 0x4

    iget-object v1, p0, Ldsp;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 17325
    :cond_3
    iget-object v0, p0, Ldsp;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 17327
    return-void
.end method
