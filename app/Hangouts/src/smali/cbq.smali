.class public abstract Lcbq;
.super Lbzz;
.source "PG"

# interfaces
.implements Laac;


# static fields
.field private static T:Z

.field public static f:I

.field public static final h:Ljava/lang/String;

.field public static final i:Ljava/lang/String;

.field private static final j:Z

.field private static final k:I

.field private static final l:I

.field private static final m:I

.field private static n:Ljava/lang/Boolean;


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field private C:Ljava/lang/String;

.field private D:I

.field private E:Ljava/lang/String;

.field private F:Ljava/lang/String;

.field private G:J

.field private H:J

.field private I:Ljava/lang/String;

.field private J:I

.field private K:Ljava/lang/String;

.field private L:Ljava/lang/String;

.field private M:Ljava/lang/String;

.field private N:I

.field private O:Z

.field private P:Ljava/lang/String;

.field private Q:Z

.field private R:I

.field private S:Ljava/lang/Object;

.field private final U:Laac;

.field private final V:Ljava/lang/StringBuilder;

.field private final W:Ljava/lang/StringBuilder;

.field private final Z:Landroid/text/SpannableStringBuilder;

.field public a:Ljava/lang/String;

.field public b:I

.field public c:I

.field public d:I

.field protected e:Lair;

.field public g:I

.field private o:Lzx;

.field private p:Lbzn;

.field private q:Lzx;

.field private r:Lbzn;

.field private s:I

.field private t:Ljava/lang/CharSequence;

.field private u:Ljava/lang/CharSequence;

.field private v:Z

.field private w:I

.field private x:Ljava/lang/String;

.field private y:I

.field private z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 67
    sget-object v0, Lbys;->s:Lcyp;

    sput-boolean v2, Lcbq;->j:Z

    .line 91
    const/4 v0, 0x0

    sput-object v0, Lcbq;->n:Ljava/lang/Boolean;

    .line 142
    sput v2, Lcbq;->f:I

    .line 150
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->d()Lcom/google/android/apps/hangouts/phone/EsApplication;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 152
    sget v1, Lf;->dw:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcbq;->k:I

    .line 154
    sget v1, Lf;->dC:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcbq;->l:I

    .line 156
    sget v1, Lf;->cP:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcbq;->m:I

    .line 159
    sget v1, Lh;->hi:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcbq;->h:Ljava/lang/String;

    .line 160
    sget v1, Lh;->km:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcbq;->i:Ljava/lang/String;

    .line 163
    sput-boolean v2, Lcbq;->T:Z

    .line 164
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcbq;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 168
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 171
    invoke-direct {p0, p1, p2}, Lbzz;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 134
    const/4 v0, -0x1

    iput v0, p0, Lcbq;->c:I

    .line 137
    iput v1, p0, Lcbq;->R:I

    .line 626
    new-instance v0, Lcbt;

    invoke-direct {v0, p0}, Lcbt;-><init>(Lcbq;)V

    iput-object v0, p0, Lcbq;->U:Laac;

    .line 946
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcbq;->V:Ljava/lang/StringBuilder;

    .line 1022
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcbq;->W:Ljava/lang/StringBuilder;

    .line 1023
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v0, p0, Lcbq;->Z:Landroid/text/SpannableStringBuilder;

    .line 172
    iput-boolean v1, p0, Lcbq;->v:Z

    .line 175
    sget v0, Lcbq;->f:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcbq;->f:I

    iput v0, p0, Lcbq;->g:I

    .line 176
    return-void
.end method

.method static synthetic a(Lcbq;Lbzn;)Lbzn;
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcbq;->r:Lbzn;

    return-object p1
.end method

.method static synthetic a(Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    .prologue
    .line 65
    sput-object p0, Lcbq;->n:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic a(Lcbq;)Lzx;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcbq;->q:Lzx;

    return-object v0
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 331
    invoke-static {p1}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->b(Landroid/database/Cursor;)J

    move-result-wide v0

    .line 332
    long-to-int v0, v0

    .line 334
    const/16 v1, 0x15

    .line 335
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 334
    invoke-static {v1, v0}, Lcom/google/android/apps/hangouts/content/EsProvider;->c(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcbq;->L:Ljava/lang/String;

    .line 337
    const/4 v1, 0x7

    .line 338
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 337
    invoke-static {v1, v0}, Lcom/google/android/apps/hangouts/content/EsProvider;->c(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcbq;->M:Ljava/lang/String;

    .line 341
    iget-object v0, p0, Lcbq;->M:Ljava/lang/String;

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    iput v0, p0, Lcbq;->N:I

    .line 342
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 610
    invoke-virtual {p0, v4}, Lcbq;->a(I)V

    .line 611
    iput-object p1, p0, Lcbq;->x:Ljava/lang/String;

    .line 612
    iput-object p2, p0, Lcbq;->t:Ljava/lang/CharSequence;

    .line 613
    iput-object p3, p0, Lcbq;->u:Ljava/lang/CharSequence;

    .line 614
    invoke-direct {p0}, Lcbq;->w()V

    iget-object v0, p0, Lcbq;->x:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, ""

    :goto_0
    iget-object v1, p0, Lcbq;->t:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {}, Lccc;->a()Lccc;

    move-result-object v1

    invoke-virtual {p0}, Lcbq;->f()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lccc;->a(Ljava/lang/CharSequence;Landroid/widget/TextView;)Landroid/text/SpannableString;

    move-result-object v1

    iget-object v2, p0, Lcbq;->Z:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->clear()V

    if-nez v1, :cond_5

    iget-object v1, p0, Lcbq;->Z:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :goto_2
    iget-object v0, p0, Lcbq;->u:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcbq;->Z:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcbq;->Z:Landroid/text/SpannableStringBuilder;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    :cond_0
    new-instance v0, Landroid/text/SpannableString;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<i>"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcbq;->u:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</i>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    const v2, -0x404041

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-interface {v0}, Landroid/text/Spannable;->length()I

    move-result v2

    const/16 v3, 0x21

    invoke-interface {v0, v1, v4, v2, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    iget-object v1, p0, Lcbq;->Z:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_1
    iget-object v0, p0, Lcbq;->Z:Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0, v0}, Lcbq;->a(Ljava/lang/CharSequence;)V

    .line 615
    return-void

    .line 614
    :cond_2
    iget v0, p0, Lcbq;->y:I

    invoke-static {v0}, Lf;->b(I)Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p0, Lcbq;->J:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcbq;->x:Ljava/lang/String;

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcbq;->x:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    goto/16 :goto_0

    :cond_4
    iget-object v1, p0, Lcbq;->W:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    iget-object v1, p0, Lcbq;->W:Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcbq;->t:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcbq;->W:Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcbq;->W:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcbq;->W:Ljava/lang/StringBuilder;

    goto/16 :goto_1

    :cond_5
    iget-object v0, p0, Lcbq;->Z:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_2
.end method

.method static synthetic b(Lcbq;)Lzx;
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcbq;->q:Lzx;

    return-object v0
.end method

.method static synthetic c(Lcbq;)Lbzn;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcbq;->r:Lbzn;

    return-object v0
.end method

.method private r()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 687
    iget-object v0, p0, Lcbq;->q:Lzx;

    if-eqz v0, :cond_0

    .line 688
    iget-object v0, p0, Lcbq;->q:Lzx;

    invoke-virtual {v0}, Lzx;->b()V

    .line 689
    iput-object v1, p0, Lcbq;->q:Lzx;

    .line 691
    :cond_0
    invoke-virtual {p0, v1}, Lcbq;->a(Landroid/graphics/Bitmap;)V

    .line 692
    iget-object v0, p0, Lcbq;->r:Lbzn;

    if-eqz v0, :cond_1

    .line 693
    iget-object v0, p0, Lcbq;->r:Lbzn;

    invoke-virtual {v0}, Lbzn;->b()V

    .line 694
    iput-object v1, p0, Lcbq;->r:Lbzn;

    .line 696
    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcbq;->d(I)V

    .line 697
    return-void
.end method

.method private s()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    const/4 v0, 0x0

    .line 701
    invoke-virtual {p0, v1}, Lcbq;->a(I)V

    .line 702
    invoke-direct {p0}, Lcbq;->w()V

    iput-object v0, p0, Lcbq;->x:Ljava/lang/String;

    iput-object v0, p0, Lcbq;->t:Ljava/lang/CharSequence;

    iput-object v0, p0, Lcbq;->u:Ljava/lang/CharSequence;

    invoke-virtual {p0, v1}, Lcbq;->a(I)V

    .line 703
    invoke-direct {p0}, Lcbq;->r()V

    .line 704
    invoke-virtual {p0, v1}, Lcbq;->m(I)V

    .line 705
    invoke-virtual {p0, v0}, Lcbq;->c(Ljava/lang/CharSequence;)V

    .line 706
    return-void
.end method

.method private t()V
    .locals 9

    .prologue
    const/4 v6, 0x6

    const/4 v4, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 714
    iget-object v0, p0, Lcbq;->e:Lair;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcbq;->e:Lair;

    iget-object v3, p0, Lcbq;->a:Ljava/lang/String;

    .line 715
    invoke-interface {v0, v3}, Lair;->b_(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 716
    :goto_0
    invoke-virtual {p0}, Lcbq;->isSelected()Z

    move-result v7

    .line 717
    if-eqz v0, :cond_5

    sget v3, Lcom/google/android/apps/hangouts/R$drawable;->e:I

    .line 719
    :goto_1
    invoke-virtual {p0, v3}, Lcbq;->setBackgroundResource(I)V

    .line 723
    if-nez v0, :cond_0

    if-eqz v7, :cond_6

    :cond_0
    move v3, v4

    move v5, v2

    .line 732
    :goto_2
    invoke-virtual {p0, v3, v5}, Lcbq;->a(II)V

    .line 734
    iget-boolean v3, p0, Lcbq;->v:Z

    if-eqz v3, :cond_8

    sget v3, Lcbq;->l:I

    :goto_3
    invoke-virtual {p0, v3}, Lcbq;->e(I)V

    .line 737
    iget-boolean v3, p0, Lcbq;->v:Z

    if-eqz v3, :cond_9

    const/16 v3, 0xff

    :goto_4
    invoke-virtual {p0, v3}, Lcbq;->f(I)V

    .line 740
    iget v3, p0, Lcbq;->d:I

    if-ne v3, v1, :cond_a

    move v3, v1

    .line 742
    :goto_5
    iget v5, p0, Lcbq;->c:I

    const/4 v8, 0x2

    if-eq v5, v8, :cond_1

    iget v5, p0, Lcbq;->c:I

    const/4 v8, 0x5

    if-ne v5, v8, :cond_d

    .line 743
    :cond_1
    iget v3, p0, Lcbq;->c:I

    .line 744
    if-eqz v7, :cond_b

    sget v3, Lcom/google/android/apps/hangouts/R$drawable;->aJ:I

    .line 768
    :goto_6
    invoke-virtual {p0, v3}, Lcbq;->b(I)V

    .line 769
    if-nez v0, :cond_2

    if-eqz v7, :cond_1a

    .line 770
    :cond_2
    invoke-virtual {p0, v4}, Lcbq;->h(I)V

    .line 772
    invoke-virtual {p0}, Lcbq;->o()Z

    move-result v0

    if-eqz v0, :cond_19

    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->ba:I

    .line 771
    :goto_7
    invoke-virtual {p0, v0}, Lcbq;->n(I)V

    .line 779
    :goto_8
    iget-boolean v0, p0, Lcbq;->v:Z

    if-eqz v0, :cond_1c

    move v0, v6

    .line 780
    :goto_9
    iget-object v3, p0, Lcbq;->u:Ljava/lang/CharSequence;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 781
    add-int/lit8 v0, v0, 0x1

    .line 785
    :cond_3
    if-ne v0, v1, :cond_1d

    :goto_a
    invoke-virtual {p0, v1}, Lcbq;->a(Z)V

    .line 786
    invoke-virtual {p0, v0}, Lcbq;->c(I)V

    .line 787
    return-void

    :cond_4
    move v0, v2

    .line 715
    goto :goto_0

    .line 717
    :cond_5
    sget v3, Lcom/google/android/apps/hangouts/R$drawable;->c:I

    goto :goto_1

    .line 725
    :cond_6
    iget-boolean v3, p0, Lcbq;->v:Z

    if-eqz v3, :cond_7

    .line 726
    const v3, -0xcccccd

    move v5, v1

    .line 727
    goto :goto_2

    .line 729
    :cond_7
    const/high16 v3, -0x66000000

    move v5, v2

    goto :goto_2

    .line 734
    :cond_8
    sget v3, Lcbq;->k:I

    goto :goto_3

    .line 737
    :cond_9
    const/16 v3, 0x7f

    goto :goto_4

    :cond_a
    move v3, v2

    .line 740
    goto :goto_5

    .line 744
    :cond_b
    iget-boolean v3, p0, Lcbq;->v:Z

    if-eqz v3, :cond_c

    sget v3, Lcom/google/android/apps/hangouts/R$drawable;->aH:I

    goto :goto_6

    :cond_c
    sget v3, Lcom/google/android/apps/hangouts/R$drawable;->aI:I

    goto :goto_6

    .line 747
    :cond_d
    iget v5, p0, Lcbq;->c:I

    if-ne v5, v1, :cond_e

    if-eqz v3, :cond_f

    :cond_e
    iget v5, p0, Lcbq;->c:I

    const/4 v8, 0x3

    if-ne v5, v8, :cond_13

    .line 749
    :cond_f
    if-nez v0, :cond_10

    if-eqz v7, :cond_11

    .line 751
    :cond_10
    sget v3, Lcom/google/android/apps/hangouts/R$drawable;->aR:I

    goto :goto_6

    .line 752
    :cond_11
    iget-boolean v3, p0, Lcbq;->v:Z

    if-eqz v3, :cond_12

    .line 753
    sget v3, Lcom/google/android/apps/hangouts/R$drawable;->aP:I

    goto :goto_6

    .line 755
    :cond_12
    sget v3, Lcom/google/android/apps/hangouts/R$drawable;->aQ:I

    goto :goto_6

    .line 757
    :cond_13
    iget v5, p0, Lcbq;->c:I

    if-ne v5, v1, :cond_14

    if-nez v3, :cond_15

    :cond_14
    iget v3, p0, Lcbq;->c:I

    if-ne v3, v6, :cond_1e

    .line 759
    :cond_15
    if-nez v0, :cond_16

    if-eqz v7, :cond_17

    .line 761
    :cond_16
    sget v3, Lcom/google/android/apps/hangouts/R$drawable;->aZ:I

    goto :goto_6

    .line 762
    :cond_17
    iget-boolean v3, p0, Lcbq;->v:Z

    if-eqz v3, :cond_18

    .line 763
    sget v3, Lcom/google/android/apps/hangouts/R$drawable;->aT:I

    goto/16 :goto_6

    .line 765
    :cond_18
    sget v3, Lcom/google/android/apps/hangouts/R$drawable;->aU:I

    goto/16 :goto_6

    .line 772
    :cond_19
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->bC:I

    goto :goto_7

    .line 774
    :cond_1a
    const/high16 v0, 0x26000000

    invoke-virtual {p0, v0}, Lcbq;->h(I)V

    .line 776
    invoke-virtual {p0}, Lcbq;->o()Z

    move-result v0

    if-eqz v0, :cond_1b

    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->aV:I

    .line 775
    :goto_b
    invoke-virtual {p0, v0}, Lcbq;->n(I)V

    goto/16 :goto_8

    .line 776
    :cond_1b
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->bB:I

    goto :goto_b

    :cond_1c
    move v0, v1

    .line 779
    goto/16 :goto_9

    :cond_1d
    move v1, v2

    .line 785
    goto :goto_a

    :cond_1e
    move v3, v2

    goto/16 :goto_6
.end method

.method private u()Z
    .locals 1

    .prologue
    .line 847
    iget v0, p0, Lcbq;->R:I

    invoke-static {v0}, Lf;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 848
    invoke-static {}, Lbtf;->k()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private v()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 929
    iget-object v0, p0, Lcbq;->o:Lzx;

    if-eqz v0, :cond_0

    .line 930
    iget-object v0, p0, Lcbq;->o:Lzx;

    invoke-virtual {v0}, Lzx;->b()V

    .line 931
    iput-object v1, p0, Lcbq;->o:Lzx;

    .line 933
    :cond_0
    iget-object v0, p0, Lcbq;->p:Lbzn;

    if-eqz v0, :cond_1

    .line 934
    iget-object v0, p0, Lcbq;->p:Lbzn;

    invoke-virtual {v0}, Lbzn;->b()V

    .line 935
    iput-object v1, p0, Lcbq;->p:Lbzn;

    .line 937
    :cond_1
    return-void
.end method

.method private w()V
    .locals 2

    .prologue
    .line 1013
    invoke-virtual {p0}, Lcbq;->e()Ljava/lang/CharSequence;

    move-result-object v0

    .line 1014
    if-eqz v0, :cond_1

    instance-of v1, v0, Landroid/text/SpannableString;

    if-nez v1, :cond_0

    instance-of v1, v0, Landroid/text/SpannedString;

    if-eqz v1, :cond_1

    .line 1016
    :cond_0
    invoke-static {}, Lccc;->a()Lccc;

    move-result-object v1

    check-cast v0, Landroid/text/Spanned;

    invoke-virtual {v1, v0}, Lccc;->a(Landroid/text/Spanned;)V

    .line 1018
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcbq;->a(Ljava/lang/CharSequence;)V

    .line 1019
    return-void
.end method


# virtual methods
.method public abstract a(I)V
.end method

.method public abstract a(II)V
.end method

.method public a(ILandroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 598
    iget v0, p0, Lcbq;->c:I

    if-eq v0, p1, :cond_0

    .line 599
    iput p1, p0, Lcbq;->c:I

    .line 601
    invoke-virtual {p0}, Lcbq;->l()V

    .line 602
    invoke-virtual {p0}, Lcbq;->p()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcbq;->g(I)V

    .line 604
    :cond_0
    return-void

    .line 602
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public a(Lair;)V
    .locals 0

    .prologue
    .line 204
    iput-object p1, p0, Lcbq;->e:Lair;

    .line 205
    return-void
.end method

.method public a(Landroid/database/Cursor;Lyj;ZLjava/lang/Object;)V
    .locals 11

    .prologue
    const/16 v10, 0xa

    const/16 v4, 0x8

    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 436
    invoke-direct {p0}, Lcbq;->s()V

    .line 438
    iput-object p4, p0, Lcbq;->S:Ljava/lang/Object;

    .line 439
    sget v0, Lg;->be:I

    invoke-virtual {p0, v0}, Lcbq;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 441
    invoke-static {p1}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a(Landroid/database/Cursor;)J

    move-result-wide v6

    long-to-int v8, v6

    const/16 v1, 0x20

    shr-long/2addr v6, v1

    long-to-int v1, v6

    iput v1, p0, Lcbq;->s:I

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v8}, Lcom/google/android/apps/hangouts/content/EsProvider;->c(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcbq;->a:Ljava/lang/String;

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v8}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcbq;->w:I

    iget v1, p0, Lcbq;->w:I

    const/4 v6, 0x3

    if-ne v1, v6, :cond_0

    invoke-virtual {p0}, Lcbq;->m()Z

    move-result v1

    if-nez v1, :cond_0

    const/16 v1, 0x9

    iput v1, p0, Lcbq;->w:I

    :cond_0
    const/16 v1, 0x1e

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v8}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcbq;->D:I

    const/16 v1, 0x9

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v8}, Lcom/google/android/apps/hangouts/content/EsProvider;->c(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcbq;->x:Ljava/lang/String;

    const/16 v1, 0x24

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v8}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcbq;->y:I

    const/16 v1, 0xb

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v8}, Lcom/google/android/apps/hangouts/content/EsProvider;->c(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcbq;->B:Ljava/lang/String;

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v8}, Lcom/google/android/apps/hangouts/content/EsProvider;->c(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcbq;->C:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v8}, Lcom/google/android/apps/hangouts/content/EsProvider;->c(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcbq;->A:Ljava/lang/String;

    const/16 v1, 0x25

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v8}, Lcom/google/android/apps/hangouts/content/EsProvider;->c(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcbq;->E:Ljava/lang/String;

    const/16 v1, 0xd

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v8}, Lcom/google/android/apps/hangouts/content/EsProvider;->c(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcbq;->z:Ljava/lang/String;

    const/16 v1, 0x1a

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v8}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_a

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcbq;->O:Z

    const/16 v1, 0x29

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v8}, Lcom/google/android/apps/hangouts/content/EsProvider;->c(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcbq;->F:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v8}, Lcom/google/android/apps/hangouts/content/EsProvider;->b(Ljava/lang/String;I)J

    move-result-wide v6

    iput-wide v6, p0, Lcbq;->G:J

    const/16 v1, 0x27

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v8}, Lcom/google/android/apps/hangouts/content/EsProvider;->b(Ljava/lang/String;I)J

    move-result-wide v6

    iput-wide v6, p0, Lcbq;->H:J

    const/16 v1, 0x28

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v8}, Lcom/google/android/apps/hangouts/content/EsProvider;->c(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcbq;->I:Ljava/lang/String;

    const/16 v1, 0x2b

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v8}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcbq;->J:I

    const/16 v1, 0x2e

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v8}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Ljava/lang/String;I)I

    move-result v1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    div-int/lit16 v1, v1, 0x3e8

    div-int/lit8 v7, v1, 0x3c

    rem-int/lit8 v1, v1, 0x3c

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-ge v1, v10, :cond_1

    const-string v7, "0"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcbq;->K:Ljava/lang/String;

    .line 442
    invoke-direct {p0, p1}, Lcbq;->a(Landroid/database/Cursor;)V

    .line 444
    const/4 v1, 0x3

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p0, Lcbq;->b:I

    .line 459
    iget-object v1, p0, Lcbq;->B:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcbq;->B:Ljava/lang/String;

    .line 461
    invoke-virtual {p2}, Lyj;->c()Lbdk;

    move-result-object v6

    iget-object v6, v6, Lbdk;->b:Ljava/lang/String;

    .line 459
    invoke-static {v1, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_b

    :cond_2
    move v1, v2

    :goto_1
    iput-boolean v1, p0, Lcbq;->Q:Z

    .line 463
    const/16 v1, 0x17

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p0, Lcbq;->d:I

    .line 465
    if-eqz p3, :cond_c

    const/4 v1, 0x4

    :goto_2
    invoke-virtual {p0, v1, p1}, Lcbq;->a(ILandroid/database/Cursor;)V

    .line 467
    sget-boolean v1, Lcbq;->j:Z

    if-eqz v1, :cond_3

    .line 468
    const-string v1, "ConversationListItem"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "bindConversationItem "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcbq;->a:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    :cond_3
    sget-boolean v1, Lcbq;->T:Z

    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    .line 472
    iget v1, p0, Lcbq;->s:I

    if-le v1, v2, :cond_12

    const-string v1, "(%d)"

    new-array v6, v2, [Ljava/lang/Object;

    iget v7, p0, Lcbq;->s:I

    .line 473
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-static {v1, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 472
    :goto_3
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 477
    :cond_4
    const/4 v0, 0x6

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 478
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_13

    .line 479
    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/ConversationNameView;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcbq;->b(Ljava/lang/String;)V

    .line 491
    :goto_4
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide v0

    .line 492
    const/4 v6, 0x5

    .line 493
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 494
    const-wide/16 v8, 0x0

    cmp-long v8, v6, v8

    if-lez v8, :cond_5

    cmp-long v0, v6, v0

    if-gez v0, :cond_5

    .line 495
    iget-object v0, p0, Lcbq;->a:Ljava/lang/String;

    invoke-static {p2, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ljava/lang/String;)V

    .line 498
    :cond_5
    invoke-virtual {p0}, Lcbq;->p()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 499
    const/4 v0, 0x4

    .line 500
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    const-wide/16 v6, 0x3e8

    div-long/2addr v0, v6

    .line 499
    invoke-static {v0, v1, v2}, Lf;->a(JZ)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcbq;->b(Ljava/lang/CharSequence;)V

    .line 501
    const/16 v0, 0x26

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v2, :cond_15

    move v1, v2

    .line 503
    :goto_5
    if-eqz v1, :cond_16

    move v0, v3

    :goto_6
    invoke-virtual {p0, v0}, Lcbq;->j(I)V

    .line 504
    const/4 v0, 0x2

    .line 505
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v10, :cond_17

    if-nez v1, :cond_17

    move v0, v3

    .line 504
    :goto_7
    invoke-virtual {p0, v0}, Lcbq;->i(I)V

    .line 509
    const/16 v0, 0x1c

    .line 510
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    const-wide/16 v6, 0x0

    cmp-long v0, v0, v6

    if-lez v0, :cond_18

    move v0, v3

    .line 509
    :goto_8
    invoke-virtual {p0, v0}, Lcbq;->k(I)V

    .line 512
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_19

    move v0, v3

    :goto_9
    invoke-virtual {p0, v0}, Lcbq;->l(I)V

    .line 517
    :cond_6
    iget v0, p0, Lcbq;->w:I

    if-eq v0, v4, :cond_7

    iget v0, p0, Lcbq;->w:I

    const/16 v1, 0xb

    if-ne v0, v1, :cond_1b

    .line 520
    :cond_7
    const/16 v0, 0x27

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_31

    .line 521
    const/16 v0, 0x27

    .line 522
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 524
    :goto_a
    const/16 v1, 0x1d

    .line 525
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 527
    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v0

    cmp-long v0, v0, v6

    if-lez v0, :cond_1a

    move v0, v2

    :goto_b
    iput-boolean v0, p0, Lcbq;->v:Z

    .line 532
    :goto_c
    const/16 v0, 0x24

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iget v1, p0, Lcbq;->R:I

    if-eq v1, v0, :cond_8

    iput v0, p0, Lcbq;->R:I

    .line 534
    :cond_8
    invoke-direct {p0, p1}, Lcbq;->a(Landroid/database/Cursor;)V

    .line 536
    iget-object v0, p0, Lcbq;->L:Ljava/lang/String;

    .line 537
    invoke-virtual {p2}, Lyj;->F()Ljava/lang/String;

    .line 538
    iget v1, p0, Lcbq;->N:I

    if-lez v1, :cond_29

    .line 539
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 540
    if-eqz v0, :cond_1d

    .line 541
    new-instance v4, Ljava/util/StringTokenizer;

    const-string v6, "|"

    invoke-direct {v4, v0, v6}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 542
    :cond_9
    :goto_d
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 543
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    .line 544
    if-eqz v0, :cond_9

    .line 545
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_d

    :cond_a
    move v1, v3

    .line 441
    goto/16 :goto_0

    :cond_b
    move v1, v3

    .line 459
    goto/16 :goto_1

    .line 465
    :cond_c
    const/16 v1, 0x17

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-eqz v1, :cond_d

    move v1, v2

    goto/16 :goto_2

    :cond_d
    iget v1, p0, Lcbq;->w:I

    if-ne v1, v2, :cond_e

    const/4 v1, 0x2

    goto/16 :goto_2

    :cond_e
    iget v1, p0, Lcbq;->w:I

    if-ne v1, v4, :cond_f

    const/4 v1, 0x3

    goto/16 :goto_2

    :cond_f
    iget v1, p0, Lcbq;->w:I

    if-ne v1, v10, :cond_10

    const/4 v1, 0x5

    goto/16 :goto_2

    :cond_10
    iget v1, p0, Lcbq;->w:I

    const/16 v6, 0xb

    if-ne v1, v6, :cond_11

    const/4 v1, 0x6

    goto/16 :goto_2

    :cond_11
    move v1, v3

    goto/16 :goto_2

    .line 473
    :cond_12
    const-string v1, ""

    goto/16 :goto_3

    .line 481
    :cond_13
    iget-object v0, p0, Lcbq;->M:Ljava/lang/String;

    .line 482
    if-eqz v0, :cond_14

    .line 483
    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/ConversationNameView;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcbq;->b(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 485
    :cond_14
    const-string v0, ""

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/ConversationNameView;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcbq;->b(Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_15
    move v1, v3

    .line 501
    goto/16 :goto_5

    :cond_16
    move v0, v4

    .line 503
    goto/16 :goto_6

    :cond_17
    move v0, v4

    .line 505
    goto/16 :goto_7

    :cond_18
    move v0, v4

    .line 510
    goto/16 :goto_8

    :cond_19
    move v0, v4

    .line 512
    goto/16 :goto_9

    :cond_1a
    move v0, v3

    .line 527
    goto/16 :goto_b

    .line 530
    :cond_1b
    iget-boolean v0, p0, Lcbq;->Q:Z

    if-nez v0, :cond_1c

    iget-boolean v0, p0, Lcbq;->O:Z

    if-eqz v0, :cond_1c

    move v0, v2

    :goto_e
    iput-boolean v0, p0, Lcbq;->v:Z

    goto/16 :goto_c

    :cond_1c
    move v0, v3

    goto :goto_e

    .line 549
    :cond_1d
    iget v0, p0, Lcbq;->N:I

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    sub-int/2addr v0, v4

    invoke-virtual {p0, v1, v0, p2}, Lcbq;->a(Ljava/util/List;ILyj;)V

    .line 556
    :goto_f
    sget-boolean v0, Lcbq;->j:Z

    if-eqz v0, :cond_1e

    const-string v0, "ConversationListItem"

    const-string v1, "bindSnippet"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1e
    iget v0, p0, Lcbq;->c:I

    if-ne v0, v2, :cond_2a

    invoke-virtual {p0}, Lcbq;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->ia:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v5, v5}, Lcbq;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 557
    :cond_1f
    :goto_10
    invoke-direct {p0}, Lcbq;->t()V

    .line 558
    iget-object v0, p0, Lcbq;->V:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    invoke-virtual {p0}, Lcbq;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0}, Lcbq;->b()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_20

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-lez v4, :cond_20

    iget-object v4, p0, Lcbq;->V:Ljava/lang/StringBuilder;

    sget v5, Lh;->jp:I

    new-array v6, v2, [Ljava/lang/Object;

    aput-object v1, v6, v3

    invoke-virtual {v0, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lf;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_20
    iget v1, p0, Lcbq;->R:I

    invoke-static {v1}, Lf;->c(I)Z

    move-result v1

    if-eqz v1, :cond_21

    iget-object v1, p0, Lcbq;->V:Ljava/lang/StringBuilder;

    sget v4, Lh;->kl:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lf;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_21
    invoke-virtual {p0}, Lcbq;->i()I

    move-result v1

    if-nez v1, :cond_22

    iget-object v1, p0, Lcbq;->V:Ljava/lang/StringBuilder;

    sget v4, Lh;->jn:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lf;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_22
    invoke-virtual {p0}, Lcbq;->j()I

    move-result v1

    if-nez v1, :cond_23

    iget-object v1, p0, Lcbq;->V:Ljava/lang/StringBuilder;

    sget v4, Lh;->jk:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lf;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_23
    invoke-virtual {p0}, Lcbq;->k()I

    move-result v1

    if-nez v1, :cond_24

    iget-object v1, p0, Lcbq;->V:Ljava/lang/StringBuilder;

    sget v4, Lh;->jq:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lf;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_24
    iget-boolean v1, p0, Lcbq;->v:Z

    if-eqz v1, :cond_25

    iget-object v1, p0, Lcbq;->V:Ljava/lang/StringBuilder;

    sget v4, Lh;->jl:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lf;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_25
    invoke-virtual {p0}, Lcbq;->g()I

    move-result v1

    if-nez v1, :cond_26

    invoke-virtual {p0}, Lcbq;->h()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_26

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-lez v4, :cond_26

    iget-object v4, p0, Lcbq;->V:Ljava/lang/StringBuilder;

    sget v5, Lh;->jo:I

    new-array v6, v2, [Ljava/lang/Object;

    aput-object v1, v6, v3

    invoke-virtual {v0, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lf;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_26
    invoke-virtual {p0}, Lcbq;->c()I

    move-result v1

    if-nez v1, :cond_27

    invoke-virtual {p0}, Lcbq;->e()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_27

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-lez v4, :cond_27

    iget-object v4, p0, Lcbq;->V:Ljava/lang/StringBuilder;

    sget v5, Lh;->jm:I

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v1, v2, v3

    invoke-virtual {v0, v5, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lf;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_27
    invoke-virtual {p0}, Lcbq;->d()I

    move-result v1

    if-nez v1, :cond_28

    iget-object v1, p0, Lcbq;->V:Ljava/lang/StringBuilder;

    sget v2, Lh;->fE:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lf;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_28
    iget-object v0, p0, Lcbq;->V:Ljava/lang/StringBuilder;

    invoke-virtual {p0, v0}, Lcbq;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 559
    return-void

    .line 552
    :cond_29
    const-string v0, "Babel"

    const-string v1, "No participants found for conversation."

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 553
    invoke-virtual {p0, v5, v3, p2}, Lcbq;->a(Ljava/util/List;ILyj;)V

    goto/16 :goto_f

    .line 556
    :cond_2a
    iget v0, p0, Lcbq;->c:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1f

    iget v0, p0, Lcbq;->w:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_10

    :pswitch_1
    invoke-virtual {p0}, Lcbq;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->gN:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v5, v5}, Lcbq;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto/16 :goto_10

    :pswitch_2
    iget-object v0, p0, Lcbq;->A:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcbq;->d(I)V

    invoke-static {v0}, Lf;->n(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcbq;->P:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2b

    iget-object v1, p0, Lcbq;->q:Lzx;

    if-nez v1, :cond_1f

    iget-object v1, p0, Lcbq;->r:Lbzn;

    if-nez v1, :cond_1f

    :cond_2b
    invoke-direct {p0}, Lcbq;->r()V

    iput-object v0, p0, Lcbq;->P:Ljava/lang/String;

    new-instance v1, Lzx;

    new-instance v4, Lbyq;

    invoke-direct {v4, v0, p2}, Lbyq;-><init>(Ljava/lang/String;Lyj;)V

    sget v0, Lcbq;->m:I

    invoke-virtual {v4, v0}, Lbyq;->a(I)Lbyq;

    move-result-object v0

    invoke-virtual {v0, v2}, Lbyq;->d(Z)Lbyq;

    move-result-object v0

    iget-object v4, p0, Lcbq;->U:Laac;

    invoke-direct {v1, v0, v4, v2, v5}, Lzx;-><init>(Lbyq;Laac;ZLjava/lang/Object;)V

    iput-object v1, p0, Lcbq;->q:Lzx;

    invoke-static {}, Lbsn;->c()Lbsn;

    move-result-object v0

    iget-object v1, p0, Lcbq;->q:Lzx;

    invoke-virtual {v0, v1}, Lbsn;->a(Lbrv;)Z

    move-result v0

    sget-boolean v1, Lcbq;->j:Z

    if-eqz v1, :cond_1f

    const-string v1, "ConversationListItem"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "setImageSnippet - image was cached:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_10

    :pswitch_3
    iget-object v1, p0, Lcbq;->x:Ljava/lang/String;

    iget-boolean v0, p0, Lcbq;->Q:Z

    if-eqz v0, :cond_2d

    invoke-virtual {p0}, Lcbq;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v4, Lh;->jU:I

    new-array v6, v2, [Ljava/lang/Object;

    sget-object v7, Lcbq;->h:Ljava/lang/String;

    aput-object v7, v6, v3

    invoke-virtual {v0, v4, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_11
    iget v4, p0, Lcbq;->w:I

    const/16 v6, 0x9

    if-ne v4, v6, :cond_2c

    iget-object v4, p0, Lcbq;->A:Ljava/lang/String;

    const-string v6, "://"

    invoke-static {v4, v6}, Lf;->c(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    if-lez v4, :cond_2c

    invoke-virtual {p0}, Lcbq;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lf;->hC:I

    new-array v7, v2, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v3

    invoke-virtual {v5, v6, v4, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    :cond_2c
    invoke-direct {p0, v1, v0, v5}, Lcbq;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto/16 :goto_10

    :cond_2d
    iget v0, p0, Lcbq;->b:I

    const/4 v4, 0x2

    if-ne v0, v4, :cond_30

    iget-object v0, p0, Lcbq;->z:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2e

    iget-object v0, p0, Lcbq;->E:Ljava/lang/String;

    :cond_2e
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2f

    iget-object v0, p0, Lcbq;->M:Ljava/lang/String;

    :cond_2f
    invoke-virtual {p0}, Lcbq;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v6, Lh;->jU:I

    new-array v7, v2, [Ljava/lang/Object;

    aput-object v0, v7, v3

    invoke-virtual {v4, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_11

    :pswitch_4
    iget-object v0, p0, Lcbq;->C:Ljava/lang/String;

    iget-object v1, p0, Lcbq;->B:Ljava/lang/String;

    iget-object v4, p0, Lcbq;->F:Ljava/lang/String;

    invoke-static {p2, v0, v1, v4, v3}, Lf;->a(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v5, v5}, Lcbq;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto/16 :goto_10

    :pswitch_5
    iget v0, p0, Lcbq;->D:I

    iget-object v1, p0, Lcbq;->C:Ljava/lang/String;

    iget-object v4, p0, Lcbq;->B:Ljava/lang/String;

    iget-object v6, p0, Lcbq;->I:Ljava/lang/String;

    invoke-static {p2, v0, v1, v4, v6}, Lf;->a(Lyj;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v5, v5}, Lcbq;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto/16 :goto_10

    :pswitch_6
    invoke-virtual {p0}, Lcbq;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->iT:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v5, v5}, Lcbq;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto/16 :goto_10

    :pswitch_7
    invoke-virtual {p0}, Lcbq;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->gO:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v5, v5}, Lcbq;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto/16 :goto_10

    :pswitch_8
    invoke-virtual {p0}, Lcbq;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->iU:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v5, v5}, Lcbq;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto/16 :goto_10

    :pswitch_9
    invoke-virtual {p0}, Lcbq;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->js:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v5, v5}, Lcbq;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v3}, Lcbq;->m(I)V

    iget-object v0, p0, Lcbq;->K:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcbq;->c(Ljava/lang/CharSequence;)V

    goto/16 :goto_10

    :cond_30
    move-object v0, v5

    goto/16 :goto_11

    :cond_31
    move-object v0, v5

    goto/16 :goto_a

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_3
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public abstract a(Landroid/graphics/Bitmap;)V
.end method

.method public abstract a(Landroid/graphics/drawable/Drawable;)V
.end method

.method public a(Lbzn;Lbyd;ZLzx;Z)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 811
    invoke-static {p2}, Lcwz;->a(Ljava/lang/Object;)V

    .line 812
    sget-boolean v0, Lcbq;->j:Z

    if-eqz v0, :cond_0

    .line 813
    const-string v3, "Babel_medialoader"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "CLIV("

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcbq;->g:I

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ") setImageBitmap "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-nez p1, :cond_3

    move-object v0, v1

    .line 814
    :goto_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " gifImage="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-nez p2, :cond_4

    move-object v0, v1

    .line 815
    :goto_1
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " success="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " loadedFromCache="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 813
    invoke-static {v3, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 819
    :cond_0
    iget-object v0, p0, Lcbq;->o:Lzx;

    if-eq v0, p4, :cond_5

    .line 821
    if-eqz p1, :cond_1

    .line 822
    invoke-virtual {p1}, Lbzn;->b()V

    .line 824
    :cond_1
    sget-boolean v0, Lcbq;->j:Z

    if-eqz v0, :cond_2

    .line 825
    const-string v0, "ConversationListItem"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setImage leaving early: mAvatarLoadedToken: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcbq;->S:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 844
    :cond_2
    :goto_2
    return-void

    .line 814
    :cond_3
    invoke-virtual {p1}, Lbzn;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 815
    :cond_4
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 832
    :cond_5
    iput-object v1, p0, Lcbq;->o:Lzx;

    .line 834
    if-eqz p3, :cond_2

    .line 840
    iget-object v0, p0, Lcbq;->p:Lbzn;

    invoke-static {v0}, Lcwz;->a(Ljava/lang/Object;)V

    .line 841
    iput-object p1, p0, Lcbq;->p:Lbzn;

    .line 842
    if-nez p5, :cond_6

    const/4 v0, 0x1

    :goto_3
    invoke-virtual {p1}, Lbzn;->e()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 843
    invoke-virtual {p4}, Lzx;->k()Ljava/lang/Object;

    move-result-object v3

    .line 842
    invoke-virtual {p0, v0, v1, v2, v3}, Lcbq;->a(ZLandroid/graphics/Bitmap;ZLjava/lang/Object;)V

    goto :goto_2

    :cond_6
    move v0, v2

    goto :goto_3
.end method

.method public abstract a(Ljava/lang/CharSequence;)V
.end method

.method public a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 422
    iput-object p1, p0, Lcbq;->S:Ljava/lang/Object;

    .line 423
    return-void
.end method

.method public a(Ljava/util/List;ILyj;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I",
            "Lyj;",
            ")V"
        }
    .end annotation

    .prologue
    .line 858
    iget-object v0, p0, Lcbq;->o:Lzx;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcbq;->o:Lzx;

    invoke-virtual {v0}, Lzx;->c()Ljava/lang/String;

    move-result-object v7

    .line 861
    :goto_0
    invoke-direct {p0}, Lcbq;->u()Z

    move-result v8

    .line 864
    invoke-static {}, Lyn;->b()I

    move-result v3

    iget-object v4, p0, Lcbq;->a:Ljava/lang/String;

    iget-object v6, p0, Lcbq;->S:Ljava/lang/Object;

    move-object v0, p1

    move v1, p2

    move-object v2, p3

    move-object v5, p0

    move v9, v8

    .line 863
    invoke-static/range {v0 .. v9}, Lxy;->a(Ljava/util/List;ILyj;ILjava/lang/String;Laac;Ljava/lang/Object;Ljava/lang/String;ZZ)Lzx;

    move-result-object v4

    .line 876
    if-eqz p1, :cond_0

    .line 877
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_8

    :cond_0
    if-nez p2, :cond_8

    const/4 v2, 0x1

    .line 878
    :goto_1
    if-nez v4, :cond_1

    if-eqz v2, :cond_9

    :cond_1
    const/4 v0, 0x1

    move v3, v0

    .line 879
    :goto_2
    if-eqz v4, :cond_a

    const/4 v0, 0x1

    .line 881
    :goto_3
    sget-boolean v1, Lcbq;->j:Z

    if-eqz v1, :cond_2

    .line 882
    const-string v5, "Babel_medialoader"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, "CLIV("

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcbq;->g:I

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, ") setAvatarUrls convId="

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v6, p0, Lcbq;->a:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, " newRequest="

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-nez v4, :cond_b

    const-string v1, "null"

    .line 884
    :goto_4
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, " oldBitmap="

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v1, p0, Lcbq;->p:Lbzn;

    if-nez v1, :cond_c

    const-string v1, "null"

    .line 885
    :goto_5
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, " clearPrev="

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, " fetch="

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, " setDefault="

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, " avatarUrls="

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-nez p1, :cond_d

    const-string v1, " null"

    .line 888
    :goto_6
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 882
    invoke-static {v5, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 891
    :cond_2
    if-eqz v3, :cond_3

    .line 892
    invoke-direct {p0}, Lcbq;->v()V

    .line 895
    :cond_3
    if-eqz v0, :cond_12

    .line 896
    invoke-virtual {p0}, Lcbq;->q()Z

    move-result v0

    invoke-virtual {v4, v0}, Lzx;->b(Z)V

    .line 897
    iput-object v4, p0, Lcbq;->o:Lzx;

    .line 898
    invoke-static {}, Lbsn;->c()Lbsn;

    move-result-object v0

    iget-object v1, p0, Lcbq;->o:Lzx;

    invoke-virtual {v0, v1}, Lbsn;->a(Lbrv;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 900
    sget-boolean v0, Lcbq;->j:Z

    if-eqz v0, :cond_4

    .line 901
    const-string v0, "Babel_medialoader"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CLIV("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcbq;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") loaded setAvatarUrl came from cache."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 904
    :cond_4
    const/4 v0, 0x0

    iput-object v0, p0, Lcbq;->o:Lzx;

    .line 905
    const/4 v0, 0x0

    .line 912
    :goto_7
    if-eqz v0, :cond_5

    .line 913
    const/4 v1, 0x0

    .line 914
    invoke-direct {p0}, Lcbq;->u()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-static {}, Lyn;->e()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 915
    :goto_8
    const/4 v2, 0x1

    iget-object v3, p0, Lcbq;->S:Ljava/lang/Object;

    .line 913
    invoke-virtual {p0, v1, v0, v2, v3}, Lcbq;->a(ZLandroid/graphics/Bitmap;ZLjava/lang/Object;)V

    .line 919
    sget-boolean v0, Lcbq;->j:Z

    if-eqz v0, :cond_5

    .line 920
    const-string v1, "Babel_medialoader"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "CLIV("

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcbq;->g:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") setAvatarUrls setting default avatar: oldkeynull="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez v7, :cond_10

    const-string v0, "true"

    :goto_9
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " avatarUrls="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez p1, :cond_11

    const-string v0, "null"

    .line 923
    :goto_a
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 920
    invoke-static {v1, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 926
    :cond_5
    return-void

    .line 858
    :cond_6
    iget-object v0, p0, Lcbq;->p:Lbzn;

    if-nez v0, :cond_7

    const/4 v7, 0x0

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lcbq;->p:Lbzn;

    .line 859
    invoke-virtual {v0}, Lbzn;->h()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    .line 877
    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 878
    :cond_9
    const/4 v0, 0x0

    move v3, v0

    goto/16 :goto_2

    .line 879
    :cond_a
    const/4 v0, 0x0

    goto/16 :goto_3

    .line 884
    :cond_b
    invoke-virtual {v4}, Lzx;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_4

    :cond_c
    iget-object v1, p0, Lcbq;->p:Lbzn;

    .line 885
    invoke-virtual {v1}, Lbzn;->h()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_5

    .line 888
    :cond_d
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_6

    .line 908
    :cond_e
    const/4 v0, 0x1

    goto :goto_7

    .line 915
    :cond_f
    invoke-static {}, Lyn;->c()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_8

    .line 920
    :cond_10
    const-string v0, "false"

    goto :goto_9

    .line 923
    :cond_11
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_a

    :cond_12
    move v0, v2

    goto/16 :goto_7
.end method

.method public abstract a(Z)V
.end method

.method public abstract a(ZLandroid/graphics/Bitmap;ZLjava/lang/Object;)V
.end method

.method public abstract b()Ljava/lang/CharSequence;
.end method

.method public abstract b(I)V
.end method

.method public abstract b(Ljava/lang/CharSequence;)V
.end method

.method public abstract b(Ljava/lang/String;)V
.end method

.method public abstract c()I
.end method

.method public abstract c(I)V
.end method

.method public abstract c(Ljava/lang/CharSequence;)V
.end method

.method public abstract d()I
.end method

.method public abstract d(I)V
.end method

.method public abstract e()Ljava/lang/CharSequence;
.end method

.method public abstract e(I)V
.end method

.method public abstract f()Landroid/widget/TextView;
.end method

.method public abstract f(I)V
.end method

.method public abstract g()I
.end method

.method public abstract g(I)V
.end method

.method public abstract h()Ljava/lang/CharSequence;
.end method

.method public abstract h(I)V
.end method

.method public abstract i()I
.end method

.method public abstract i(I)V
.end method

.method public abstract j()I
.end method

.method public abstract j(I)V
.end method

.method public abstract k()I
.end method

.method public abstract k(I)V
.end method

.method public abstract l()V
.end method

.method public abstract l(I)V
.end method

.method public abstract m(I)V
.end method

.method public abstract m()Z
.end method

.method public abstract n(I)V
.end method

.method public n()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 565
    iget v2, p0, Lcbq;->c:I

    if-ne v2, v0, :cond_1

    iget v2, p0, Lcbq;->d:I

    if-eq v2, v0, :cond_1

    move v2, v0

    .line 567
    :goto_0
    if-nez v2, :cond_0

    iget v2, p0, Lcbq;->c:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    :cond_0
    sget-object v2, Lcbq;->n:Ljava/lang/Boolean;

    .line 568
    invoke-static {v2, v1}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 569
    invoke-static {}, Lbkb;->j()Lyj;

    move-result-object v2

    invoke-virtual {v2}, Lyj;->aj()Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_1
    return v0

    :cond_1
    move v2, v1

    .line 565
    goto :goto_0

    :cond_2
    move v0, v1

    .line 569
    goto :goto_1
.end method

.method public o()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 576
    iget v2, p0, Lcbq;->c:I

    if-ne v2, v0, :cond_1

    iget v2, p0, Lcbq;->d:I

    if-ne v2, v0, :cond_1

    move v2, v0

    .line 578
    :goto_0
    if-nez v2, :cond_0

    iget v2, p0, Lcbq;->c:I

    const/4 v3, 0x5

    if-ne v2, v3, :cond_2

    :cond_0
    sget-object v2, Lcbq;->n:Ljava/lang/Boolean;

    .line 579
    invoke-static {v2, v1}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 580
    invoke-static {}, Lbkb;->j()Lyj;

    move-result-object v2

    invoke-virtual {v2}, Lyj;->aj()Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_1
    return v0

    :cond_1
    move v2, v1

    .line 576
    goto :goto_0

    :cond_2
    move v0, v1

    .line 580
    goto :goto_1
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 1073
    invoke-direct {p0}, Lcbq;->s()V

    .line 1074
    invoke-direct {p0}, Lcbq;->v()V

    .line 1075
    invoke-super {p0}, Lbzz;->onDetachedFromWindow()V

    .line 1076
    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 183
    sget-object v0, Lcbq;->n:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 184
    new-instance v0, Lcbr;

    invoke-direct {v0, p0}, Lcbr;-><init>(Lcbq;)V

    new-instance v1, Lcbs;

    invoke-direct {v1, p0}, Lcbs;-><init>(Lcbq;)V

    invoke-static {v0, v1}, Lape;->a(Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    .line 201
    :cond_0
    return-void
.end method

.method public p()Z
    .locals 2

    .prologue
    .line 588
    iget v0, p0, Lcbq;->c:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcbq;->c:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcbq;->c:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()Z
    .locals 1

    .prologue
    .line 1079
    const/4 v0, 0x0

    return v0
.end method

.method public setSelected(Z)V
    .locals 1

    .prologue
    .line 791
    invoke-super {p0}, Lbzz;->isSelected()Z

    move-result v0

    .line 792
    invoke-super {p0, p1}, Lbzz;->setSelected(Z)V

    .line 793
    if-eq p1, v0, :cond_0

    .line 794
    invoke-direct {p0}, Lcbq;->t()V

    .line 796
    :cond_0
    return-void
.end method
