.class public abstract Late;
.super Latc;
.source "PG"


# instance fields
.field protected A:I

.field protected final B:Latg;

.field protected final C:Landroid/widget/ImageButton;

.field protected final D:Landroid/widget/ImageView;

.field private final E:Landroid/graphics/Rect;

.field private F:I

.field private G:I

.field private final H:Landroid/view/ViewGroup;

.field private final I:Landroid/view/View;

.field protected a:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

.field protected final d:Lapk;

.field protected final e:Latd;

.field protected final f:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

.field protected final g:Lasx;

.field protected final h:Laui;

.field protected i:Z

.field protected final j:Ljava/lang/Object;

.field protected k:Z

.field protected l:F

.field protected m:Z

.field protected n:Z

.field protected final o:Landroid/graphics/Rect;

.field protected p:I

.field protected q:I

.field protected r:I

.field protected s:F

.field protected t:F

.field protected u:F

.field protected v:Z

.field protected w:Landroid/graphics/Bitmap;

.field protected x:Landroid/graphics/Bitmap;

.field protected y:I

.field protected z:Z


# direct methods
.method protected constructor <init>(Latd;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;Lasx;Ljava/lang/Object;IILandroid/view/ViewGroup;)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    .line 329
    invoke-direct {p0}, Latc;-><init>()V

    .line 74
    invoke-static {}, Lapk;->b()Lapk;

    move-result-object v1

    iput-object v1, p0, Late;->d:Lapk;

    .line 87
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Late;->E:Landroid/graphics/Rect;

    .line 88
    iput v2, p0, Late;->l:F

    .line 89
    iput-boolean v0, p0, Late;->m:Z

    .line 92
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Late;->o:Landroid/graphics/Rect;

    .line 95
    iput v0, p0, Late;->p:I

    .line 98
    iput v0, p0, Late;->q:I

    .line 101
    iput v0, p0, Late;->r:I

    .line 108
    iput v2, p0, Late;->u:F

    .line 114
    iput v0, p0, Late;->y:I

    .line 118
    iput v0, p0, Late;->A:I

    .line 330
    if-nez p2, :cond_0

    if-nez p2, :cond_1

    instance-of v1, p0, Latu;

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 331
    iput-object p1, p0, Late;->e:Latd;

    .line 332
    iput-object p2, p0, Late;->a:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 333
    iput-object p3, p0, Late;->f:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    .line 334
    iput-object p4, p0, Late;->g:Lasx;

    .line 335
    iput-object p5, p0, Late;->j:Ljava/lang/Object;

    .line 336
    iput-object p8, p0, Late;->H:Landroid/view/ViewGroup;

    .line 338
    invoke-virtual {p8}, Landroid/view/ViewGroup;->getRootView()Landroid/view/View;

    move-result-object v0

    sget v1, Lg;->cr:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Late;->I:Landroid/view/View;

    .line 339
    invoke-virtual {p8}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0}, Late;->a(Landroid/content/Context;)Latg;

    move-result-object v0

    iput-object v0, p0, Late;->B:Latg;

    .line 340
    iget-object v0, p0, Late;->B:Latg;

    iget-object v0, v0, Latg;->b:Landroid/widget/ImageButton;

    iput-object v0, p0, Late;->C:Landroid/widget/ImageButton;

    .line 341
    iget-object v0, p0, Late;->B:Latg;

    iget-object v0, v0, Latg;->d:Landroid/widget/ImageView;

    iput-object v0, p0, Late;->D:Landroid/widget/ImageView;

    .line 342
    iget-object v0, p0, Late;->B:Latg;

    invoke-virtual {p8, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 344
    new-instance v0, Laui;

    invoke-virtual {p0}, Late;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Laui;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Late;->h:Laui;

    .line 345
    if-eqz p6, :cond_2

    if-eqz p7, :cond_2

    .line 346
    iget-object v0, p0, Late;->h:Laui;

    invoke-virtual {v0, p6, p7}, Laui;->a(II)V

    .line 348
    :cond_2
    return-void
.end method

.method private A()V
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/high16 v4, 0x40000000    # 2.0f

    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 588
    iget-object v0, p0, Late;->E:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Late;->o:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 589
    :cond_0
    iput-boolean v3, p0, Late;->n:Z

    .line 675
    :goto_0
    return-void

    .line 593
    :cond_1
    iget-object v0, p0, Late;->o:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Late;->E:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    if-lt v0, v2, :cond_2

    iget-object v0, p0, Late;->o:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Late;->E:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    if-lt v0, v2, :cond_3

    .line 595
    :cond_2
    iput-boolean v3, p0, Late;->n:Z

    goto :goto_0

    .line 604
    :cond_3
    iget-object v0, p0, Late;->o:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    iget-object v2, p0, Late;->o:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v0, v2

    .line 611
    invoke-virtual {p0}, Late;->t()F

    move-result v6

    .line 612
    invoke-virtual {p0}, Late;->u()F

    move-result v7

    .line 617
    mul-float v2, v6, v4

    sub-float v8, v10, v2

    .line 618
    mul-float v2, v7, v4

    sub-float v2, v10, v2

    .line 621
    iget v3, p0, Late;->u:F

    cmpl-float v3, v0, v3

    if-lez v3, :cond_7

    .line 626
    iget v3, p0, Late;->u:F

    div-float v0, v3, v0

    sub-float v0, v10, v0

    div-float/2addr v0, v4

    .line 630
    mul-float/2addr v0, v2

    move v2, v0

    move v3, v1

    move v4, v1

    .line 644
    :goto_1
    iget-object v5, p0, Late;->o:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    int-to-float v5, v5

    iget v9, p0, Late;->u:F

    mul-float/2addr v5, v9

    const/high16 v9, 0x3f000000    # 0.5f

    add-float/2addr v5, v9

    float-to-int v5, v5

    .line 646
    cmpl-float v9, v2, v1

    if-nez v9, :cond_4

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_8

    .line 650
    :cond_4
    int-to-float v1, v5

    sub-float v5, v10, v2

    sub-float/2addr v5, v0

    div-float/2addr v1, v5

    float-to-int v1, v1

    .line 655
    :goto_2
    int-to-float v1, v1

    div-float/2addr v1, v8

    .line 656
    iget-object v5, p0, Late;->o:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    iget-object v8, p0, Late;->E:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->left:I

    if-ge v5, v8, :cond_5

    .line 661
    iget-object v5, p0, Late;->E:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    iget-object v8, p0, Late;->o:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->left:I

    sub-int/2addr v5, v8

    int-to-float v5, v5

    div-float/2addr v5, v1

    add-float/2addr v4, v5

    .line 662
    iget-object v5, p0, Late;->o:Landroid/graphics/Rect;

    iget-object v8, p0, Late;->E:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->left:I

    iput v8, v5, Landroid/graphics/Rect;->left:I

    .line 664
    :cond_5
    iget-object v5, p0, Late;->o:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    iget-object v8, p0, Late;->E:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->right:I

    if-le v5, v8, :cond_6

    .line 667
    iget-object v5, p0, Late;->o:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    iget-object v8, p0, Late;->E:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->right:I

    sub-int/2addr v5, v8

    int-to-float v5, v5

    div-float v1, v5, v1

    add-float/2addr v3, v1

    .line 668
    iget-object v1, p0, Late;->o:Landroid/graphics/Rect;

    iget-object v5, p0, Late;->E:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    iput v5, v1, Landroid/graphics/Rect;->right:I

    .line 671
    :cond_6
    iget-object v1, p0, Late;->h:Laui;

    add-float/2addr v4, v6

    add-float/2addr v2, v7

    add-float/2addr v3, v6

    add-float/2addr v0, v7

    invoke-virtual {v1, v4, v2, v3, v0}, Laui;->a(FFFF)V

    .line 674
    const/4 v0, 0x1

    iput-boolean v0, p0, Late;->n:Z

    goto/16 :goto_0

    .line 636
    :cond_7
    iget v2, p0, Late;->u:F

    div-float/2addr v0, v2

    sub-float v0, v10, v0

    div-float/2addr v0, v4

    .line 640
    mul-float/2addr v0, v8

    move v2, v1

    move v3, v0

    move v4, v0

    move v0, v1

    goto :goto_1

    :cond_8
    move v1, v5

    goto :goto_2
.end method

.method static synthetic a(Late;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Late;->z()V

    return-void
.end method

.method private z()V
    .locals 9

    .prologue
    const/4 v4, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 561
    iget-object v0, p0, Late;->B:Latg;

    invoke-virtual {v0}, Latg;->b()V

    .line 567
    new-array v0, v4, [I

    .line 568
    iget-object v1, p0, Late;->I:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 570
    iget-object v1, p0, Late;->H:Landroid/view/ViewGroup;

    iget-object v2, p0, Late;->E:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 571
    iget-object v1, p0, Late;->E:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->left:I

    aget v3, v0, v7

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 572
    iget-object v1, p0, Late;->E:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->top:I

    aget v3, v0, v8

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 574
    new-array v1, v4, [I

    .line 575
    iget-object v2, p0, Late;->C:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->getLocationOnScreen([I)V

    .line 576
    iget-object v2, p0, Late;->o:Landroid/graphics/Rect;

    aget v3, v1, v7

    aget v4, v1, v8

    aget v5, v1, v7

    iget-object v6, p0, Late;->C:Landroid/widget/ImageButton;

    .line 577
    invoke-virtual {v6}, Landroid/widget/ImageButton;->getWidth()I

    move-result v6

    add-int/2addr v5, v6

    aget v1, v1, v8

    iget-object v6, p0, Late;->C:Landroid/widget/ImageButton;

    .line 578
    invoke-virtual {v6}, Landroid/widget/ImageButton;->getHeight()I

    move-result v6

    add-int/2addr v1, v6

    .line 576
    invoke-virtual {v2, v3, v4, v5, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 579
    iget-object v1, p0, Late;->o:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->left:I

    aget v3, v0, v7

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 580
    iget-object v1, p0, Late;->o:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->top:I

    aget v3, v0, v8

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 581
    iget-object v1, p0, Late;->o:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->right:I

    aget v3, v0, v7

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 582
    iget-object v1, p0, Late;->o:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->bottom:I

    aget v0, v0, v8

    sub-int v0, v2, v0

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 584
    invoke-direct {p0}, Late;->A()V

    .line 585
    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;)Latg;
    .locals 1

    .prologue
    .line 351
    new-instance v0, Latg;

    invoke-direct {v0, p0, p1}, Latg;-><init>(Late;Landroid/content/Context;)V

    return-object v0
.end method

.method public a(F)V
    .locals 0

    .prologue
    .line 451
    iput p1, p0, Late;->l:F

    .line 452
    return-void
.end method

.method public a(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 696
    iget-object v0, p0, Late;->B:Latg;

    invoke-virtual {v0}, Latg;->b()V

    .line 697
    return-void
.end method

.method protected a(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 377
    invoke-static {p1}, Lcwz;->b(Ljava/lang/Object;)V

    .line 378
    iget-object v1, p0, Late;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 379
    :try_start_0
    iput-object p1, p0, Late;->w:Landroid/graphics/Bitmap;

    .line 380
    const/4 v0, 0x0

    iput-boolean v0, p0, Late;->z:Z

    .line 381
    iget-object v0, p0, Late;->w:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lbyr;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Late;->x:Landroid/graphics/Bitmap;

    .line 382
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 383
    iget-object v0, p0, Late;->g:Lasx;

    invoke-virtual {v0, p0}, Lasx;->b(Late;)V

    .line 384
    return-void

    .line 382
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 456
    iget-boolean v0, p0, Late;->m:Z

    if-eq v0, p1, :cond_0

    .line 457
    iput-boolean p1, p0, Late;->m:Z

    .line 458
    iget-object v1, p0, Late;->D:Landroid/widget/ImageView;

    iget-boolean v0, p0, Late;->m:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 461
    :cond_0
    return-void

    .line 458
    :cond_1
    const/4 v0, 0x4

    goto :goto_0
.end method

.method protected a(III)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 486
    iget v2, p0, Late;->r:I

    if-ne v2, p1, :cond_0

    iget v2, p0, Late;->F:I

    if-ne v2, p2, :cond_0

    iget v2, p0, Late;->G:I

    if-eq v2, p3, :cond_2

    .line 488
    :cond_0
    iput p1, p0, Late;->r:I

    .line 489
    iget-object v2, p0, Late;->h:Laui;

    iget v3, p0, Late;->r:I

    invoke-virtual {p0}, Late;->o()Z

    move-result v4

    invoke-virtual {v2, v3, v4}, Laui;->a(IZ)V

    .line 492
    iput p2, p0, Late;->F:I

    .line 493
    iput p3, p0, Late;->G:I

    .line 494
    int-to-float v2, p2

    int-to-float v3, p3

    div-float/2addr v2, v3

    iput v2, p0, Late;->u:F

    .line 495
    iget v2, p0, Late;->u:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1

    move v0, v1

    :cond_1
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 498
    iget-object v0, p0, Late;->g:Lasx;

    invoke-virtual {v0, p0}, Lasx;->a(Late;)V

    .line 501
    iget-object v2, p0, Late;->j:Ljava/lang/Object;

    monitor-enter v2

    .line 502
    :try_start_0
    invoke-direct {p0}, Late;->A()V

    .line 503
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v1

    .line 506
    :cond_2
    return v0

    .line 503
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 690
    iget-object v0, p0, Late;->H:Landroid/view/ViewGroup;

    iget-object v1, p0, Late;->B:Latg;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 691
    return-void
.end method

.method b(Z)V
    .locals 2

    .prologue
    .line 539
    invoke-static {}, Lcwz;->a()V

    .line 540
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iget-boolean v1, p0, Late;->k:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 541
    iput-boolean p1, p0, Late;->k:Z

    .line 542
    return-void
.end method

.method protected c(Z)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 708
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 709
    if-eqz p1, :cond_0

    .line 710
    iget-object v1, p0, Late;->C:Landroid/widget/ImageButton;

    sget v2, Lh;->iF:I

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Late;->B:Latg;

    .line 712
    invoke-virtual {v4}, Latg;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    .line 711
    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 718
    :goto_0
    return-void

    .line 714
    :cond_0
    iget-object v1, p0, Late;->C:Landroid/widget/ImageButton;

    sget v2, Lh;->nF:I

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Late;->B:Latg;

    .line 716
    invoke-virtual {v4}, Latg;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    .line 715
    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 355
    iget v1, p0, Late;->p:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Z
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 359
    iget v0, p0, Late;->p:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Late;->q:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 372
    const/4 v0, 0x1

    return v0
.end method

.method public f()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 387
    iget-object v0, p0, Late;->w:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 388
    iget-object v0, p0, Late;->w:Landroid/graphics/Bitmap;

    .line 392
    :goto_0
    return-object v0

    .line 389
    :cond_0
    iget-object v0, p0, Late;->a:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    instance-of v0, v0, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;

    if-eqz v0, :cond_1

    .line 390
    invoke-static {}, Lyn;->k()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 392
    :cond_1
    invoke-static {}, Lyn;->i()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public g()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 397
    iget-object v0, p0, Late;->x:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 398
    iget-object v0, p0, Late;->x:Landroid/graphics/Bitmap;

    .line 402
    :goto_0
    return-object v0

    .line 399
    :cond_0
    iget-object v0, p0, Late;->a:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    instance-of v0, v0, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;

    if-eqz v0, :cond_1

    .line 400
    invoke-static {}, Lyn;->j()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 402
    :cond_1
    invoke-static {}, Lyn;->h()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 680
    iget-object v0, p0, Late;->B:Latg;

    invoke-virtual {v0}, Latg;->b()V

    .line 681
    return-void
.end method

.method public k()V
    .locals 1

    .prologue
    .line 704
    iget-object v0, p0, Late;->B:Latg;

    invoke-virtual {v0}, Latg;->requestLayout()V

    .line 705
    return-void
.end method

.method public l()Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;
    .locals 1

    .prologue
    .line 413
    iget-object v0, p0, Late;->a:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    return-object v0
.end method

.method m()I
    .locals 2

    .prologue
    .line 418
    iget-boolean v0, p0, Late;->i:Z

    invoke-static {v0}, Lcwz;->a(Z)V

    .line 419
    iget v0, p0, Late;->r:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 420
    iget v0, p0, Late;->r:I

    return v0
.end method

.method n()Z
    .locals 2

    .prologue
    .line 425
    iget v0, p0, Late;->q:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method abstract o()Z
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 1

    .prologue
    .line 445
    invoke-super {p0, p1, p2, p3}, Latc;->onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V

    .line 446
    iget-object v0, p0, Late;->h:Laui;

    invoke-virtual {v0, p2, p3}, Laui;->a(II)V

    .line 447
    return-void
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 2

    .prologue
    .line 437
    iget-object v1, p0, Late;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 438
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Late;->i:Z

    .line 439
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public p()I
    .locals 1

    .prologue
    .line 431
    iget v0, p0, Late;->A:I

    return v0
.end method

.method protected q()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 466
    iget-object v0, p0, Late;->h:Laui;

    invoke-virtual {v0}, Laui;->a()V

    .line 467
    iput v1, p0, Late;->y:I

    .line 468
    iput-boolean v1, p0, Late;->z:Z

    .line 469
    iput v1, p0, Late;->r:I

    .line 470
    iput v1, p0, Late;->q:I

    .line 471
    const/4 v0, 0x1

    iput-boolean v0, p0, Late;->i:Z

    .line 472
    return-void
.end method

.method public abstract r()V
.end method

.method s()F
    .locals 1

    .prologue
    .line 510
    iget v0, p0, Late;->u:F

    return v0
.end method

.method t()F
    .locals 2

    .prologue
    .line 514
    iget v0, p0, Late;->q:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget v0, p0, Late;->s:F

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 364
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mEndpoint="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Late;->a:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method u()F
    .locals 2

    .prologue
    .line 518
    iget v0, p0, Late;->q:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget v0, p0, Late;->t:F

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method v()Z
    .locals 1

    .prologue
    .line 522
    iget-boolean v0, p0, Late;->v:Z

    return v0
.end method

.method public w()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 527
    iget-object v0, p0, Late;->d:Lapk;

    invoke-virtual {v0}, Lapk;->c()Lapx;

    move-result-object v0

    .line 528
    if-eqz v0, :cond_0

    .line 529
    invoke-virtual {v0}, Lapx;->F()Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    move-result-object v0

    .line 530
    iget-object v3, p0, Late;->B:Latg;

    iget-boolean v4, p0, Late;->k:Z

    invoke-virtual {v3, v4}, Latg;->a(Z)V

    .line 531
    iget-object v3, p0, Late;->B:Latg;

    if-eqz v0, :cond_1

    iget-object v4, p0, Late;->a:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->hasSameMucJid(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Latg;->b(Z)V

    .line 532
    iget-object v0, p0, Late;->B:Latg;

    iget-object v3, p0, Late;->a:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    if-eqz v3, :cond_2

    iget-object v3, p0, Late;->a:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    invoke-virtual {v3}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isMediaBlocked()Z

    move-result v3

    if-eqz v3, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Latg;->c(Z)V

    .line 533
    iget-object v0, p0, Late;->B:Latg;

    invoke-static {v0}, Latg;->b(Latg;)V

    .line 535
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 531
    goto :goto_0

    :cond_2
    move v1, v2

    .line 532
    goto :goto_1
.end method

.method public x()Z
    .locals 1

    .prologue
    .line 545
    iget-boolean v0, p0, Late;->k:Z

    return v0
.end method

.method public y()V
    .locals 2

    .prologue
    .line 550
    iget-object v1, p0, Late;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 551
    :try_start_0
    invoke-direct {p0}, Late;->z()V

    .line 552
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
