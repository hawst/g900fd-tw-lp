.class public final Lcdu;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:I

.field private b:Z

.field private c:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 273
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 269
    iput v0, p0, Lcdu;->a:I

    .line 270
    iput-boolean v0, p0, Lcdu;->b:Z

    .line 274
    iput-object p1, p0, Lcdu;->c:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    .line 275
    iput p2, p0, Lcdu;->a:I

    .line 276
    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 4

    .prologue
    .line 285
    if-eqz p1, :cond_0

    .line 286
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcdu;->b:Z

    .line 288
    :cond_0
    iget v0, p0, Lcdu;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcdu;->a:I

    if-nez v0, :cond_1

    .line 289
    iget-boolean v0, p0, Lcdu;->b:Z

    iget-object v1, p0, Lcdu;->c:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->a(Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;)V

    iget-object v1, p0, Lcdu;->c:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->c(Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcdu;->c:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->b(Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;)I

    move-result v0

    int-to-long v0, v0

    :goto_0
    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 291
    :cond_1
    return-void

    .line 289
    :cond_2
    const-wide/16 v0, 0x0

    goto :goto_0
.end method
