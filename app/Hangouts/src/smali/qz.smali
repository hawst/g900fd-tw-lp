.class public final Lqz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Lcom/android/ex/photo/views/PhotoView;

.field private b:F

.field private c:F

.field private d:J

.field private e:Z

.field private f:Z


# direct methods
.method public constructor <init>(Lcom/android/ex/photo/views/PhotoView;)V
    .locals 2

    .prologue
    .line 1163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1164
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lqz;->d:J

    .line 1165
    iput-object p1, p0, Lqz;->a:Lcom/android/ex/photo/views/PhotoView;

    .line 1166
    return-void
.end method

.method public static synthetic a(Lqz;)Z
    .locals 1

    .prologue
    .line 1149
    iget-boolean v0, p0, Lqz;->e:Z

    return v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 1188
    const/4 v0, 0x0

    iput-boolean v0, p0, Lqz;->e:Z

    .line 1189
    const/4 v0, 0x1

    iput-boolean v0, p0, Lqz;->f:Z

    .line 1190
    return-void
.end method

.method public a(FF)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1172
    iget-boolean v2, p0, Lqz;->e:Z

    if-eqz v2, :cond_0

    .line 1181
    :goto_0
    return v0

    .line 1175
    :cond_0
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lqz;->d:J

    .line 1176
    iput p1, p0, Lqz;->b:F

    .line 1177
    iput p2, p0, Lqz;->c:F

    .line 1178
    iput-boolean v0, p0, Lqz;->f:Z

    .line 1179
    iput-boolean v1, p0, Lqz;->e:Z

    .line 1180
    iget-object v0, p0, Lqz;->a:Lcom/android/ex/photo/views/PhotoView;

    invoke-virtual {v0, p0}, Lcom/android/ex/photo/views/PhotoView;->post(Ljava/lang/Runnable;)Z

    move v0, v1

    .line 1181
    goto :goto_0
.end method

.method public run()V
    .locals 9

    .prologue
    const/high16 v8, 0x447a0000    # 1000.0f

    const/4 v1, 0x0

    .line 1195
    iget-boolean v0, p0, Lqz;->f:Z

    if-eqz v0, :cond_1

    .line 1240
    :cond_0
    :goto_0
    return-void

    .line 1200
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1201
    iget-wide v4, p0, Lqz;->d:J

    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-eqz v0, :cond_7

    iget-wide v4, p0, Lqz;->d:J

    sub-long v4, v2, v4

    long-to-float v0, v4

    div-float/2addr v0, v8

    .line 1202
    :goto_1
    iget-object v4, p0, Lqz;->a:Lcom/android/ex/photo/views/PhotoView;

    iget v5, p0, Lqz;->b:F

    mul-float/2addr v5, v0

    iget v6, p0, Lqz;->c:F

    mul-float/2addr v6, v0

    invoke-static {v4, v5, v6}, Lcom/android/ex/photo/views/PhotoView;->a(Lcom/android/ex/photo/views/PhotoView;FF)Z

    move-result v4

    .line 1203
    iput-wide v2, p0, Lqz;->d:J

    .line 1205
    mul-float/2addr v0, v8

    .line 1206
    iget v2, p0, Lqz;->b:F

    cmpl-float v2, v2, v1

    if-lez v2, :cond_8

    .line 1207
    iget v2, p0, Lqz;->b:F

    sub-float/2addr v2, v0

    iput v2, p0, Lqz;->b:F

    .line 1208
    iget v2, p0, Lqz;->b:F

    cmpg-float v2, v2, v1

    if-gez v2, :cond_2

    .line 1209
    iput v1, p0, Lqz;->b:F

    .line 1217
    :cond_2
    :goto_2
    iget v2, p0, Lqz;->c:F

    cmpl-float v2, v2, v1

    if-lez v2, :cond_9

    .line 1218
    iget v2, p0, Lqz;->c:F

    sub-float v0, v2, v0

    iput v0, p0, Lqz;->c:F

    .line 1219
    iget v0, p0, Lqz;->c:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_3

    .line 1220
    iput v1, p0, Lqz;->c:F

    .line 1230
    :cond_3
    :goto_3
    iget v0, p0, Lqz;->b:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_4

    iget v0, p0, Lqz;->c:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_5

    :cond_4
    if-nez v4, :cond_6

    .line 1231
    :cond_5
    invoke-virtual {p0}, Lqz;->a()V

    .line 1232
    iget-object v0, p0, Lqz;->a:Lcom/android/ex/photo/views/PhotoView;

    invoke-static {v0}, Lcom/android/ex/photo/views/PhotoView;->a(Lcom/android/ex/photo/views/PhotoView;)V

    .line 1236
    :cond_6
    iget-boolean v0, p0, Lqz;->f:Z

    if-nez v0, :cond_0

    .line 1239
    iget-object v0, p0, Lqz;->a:Lcom/android/ex/photo/views/PhotoView;

    invoke-virtual {v0, p0}, Lcom/android/ex/photo/views/PhotoView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_7
    move v0, v1

    .line 1201
    goto :goto_1

    .line 1212
    :cond_8
    iget v2, p0, Lqz;->b:F

    add-float/2addr v2, v0

    iput v2, p0, Lqz;->b:F

    .line 1213
    iget v2, p0, Lqz;->b:F

    cmpl-float v2, v2, v1

    if-lez v2, :cond_2

    .line 1214
    iput v1, p0, Lqz;->b:F

    goto :goto_2

    .line 1223
    :cond_9
    iget v2, p0, Lqz;->c:F

    add-float/2addr v0, v2

    iput v0, p0, Lqz;->c:F

    .line 1224
    iget v0, p0, Lqz;->c:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    .line 1225
    iput v1, p0, Lqz;->c:F

    goto :goto_3
.end method
