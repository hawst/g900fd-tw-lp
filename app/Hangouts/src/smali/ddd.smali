.class public final Lddd;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lddd;


# instance fields
.field public b:Ldcv;

.field public c:Ldgq;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 240
    const/4 v0, 0x0

    new-array v0, v0, [Lddd;

    sput-object v0, Lddd;->a:[Lddd;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 241
    invoke-direct {p0}, Lepn;-><init>()V

    .line 244
    iput-object v0, p0, Lddd;->b:Ldcv;

    .line 247
    iput-object v0, p0, Lddd;->c:Ldgq;

    .line 241
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 264
    const/4 v0, 0x0

    .line 265
    iget-object v1, p0, Lddd;->b:Ldcv;

    if-eqz v1, :cond_0

    .line 266
    const/4 v0, 0x1

    iget-object v1, p0, Lddd;->b:Ldcv;

    .line 267
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 269
    :cond_0
    iget-object v1, p0, Lddd;->c:Ldgq;

    if-eqz v1, :cond_1

    .line 270
    const/4 v1, 0x2

    iget-object v2, p0, Lddd;->c:Ldgq;

    .line 271
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 273
    :cond_1
    iget-object v1, p0, Lddd;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 274
    iput v0, p0, Lddd;->cachedSize:I

    .line 275
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 237
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lddd;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lddd;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lddd;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lddd;->b:Ldcv;

    if-nez v0, :cond_2

    new-instance v0, Ldcv;

    invoke-direct {v0}, Ldcv;-><init>()V

    iput-object v0, p0, Lddd;->b:Ldcv;

    :cond_2
    iget-object v0, p0, Lddd;->b:Ldcv;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lddd;->c:Ldgq;

    if-nez v0, :cond_3

    new-instance v0, Ldgq;

    invoke-direct {v0}, Ldgq;-><init>()V

    iput-object v0, p0, Lddd;->c:Ldgq;

    :cond_3
    iget-object v0, p0, Lddd;->c:Ldgq;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 252
    iget-object v0, p0, Lddd;->b:Ldcv;

    if-eqz v0, :cond_0

    .line 253
    const/4 v0, 0x1

    iget-object v1, p0, Lddd;->b:Ldcv;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 255
    :cond_0
    iget-object v0, p0, Lddd;->c:Ldgq;

    if-eqz v0, :cond_1

    .line 256
    const/4 v0, 0x2

    iget-object v1, p0, Lddd;->c:Ldgq;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 258
    :cond_1
    iget-object v0, p0, Lddd;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 260
    return-void
.end method
