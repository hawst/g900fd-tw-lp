.class public final Laui;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:Z


# instance fields
.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private final l:Ljava/nio/FloatBuffer;

.field private final m:Ljava/nio/FloatBuffer;

.field private final n:Landroid/graphics/Rect;

.field private final o:Lcom/google/android/libraries/hangouts/video/FloatRect;

.field private final p:[F

.field private q:Z

.field private r:F

.field private s:F

.field private t:F

.field private u:F

.field private v:Z

.field private final w:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lbys;->e:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Laui;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput v0, p0, Laui;->e:I

    .line 67
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Laui;->n:Landroid/graphics/Rect;

    .line 69
    new-instance v0, Lcom/google/android/libraries/hangouts/video/FloatRect;

    invoke-direct {v0}, Lcom/google/android/libraries/hangouts/video/FloatRect;-><init>()V

    iput-object v0, p0, Laui;->o:Lcom/google/android/libraries/hangouts/video/FloatRect;

    .line 71
    const/16 v0, 0x8

    new-array v0, v0, [F

    iput-object v0, p0, Laui;->p:[F

    .line 119
    iput-object p1, p0, Laui;->w:Ljava/lang/String;

    .line 121
    const/16 v0, 0x20

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 122
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Laui;->l:Ljava/nio/FloatBuffer;

    .line 123
    iget-object v0, p0, Laui;->p:[F

    array-length v0, v0

    shl-int/lit8 v0, v0, 0x2

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 124
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Laui;->m:Ljava/nio/FloatBuffer;

    .line 125
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 128
    const-string v0, "attribute vec4 vPosition;\nattribute vec2 a_texCoord;\nvarying vec2 v_texCoord;\nvoid main() {\n  gl_Position = vPosition;\n  v_texCoord = a_texCoord;\n}\n"

    const-string v1, "uniform sampler2D s_texture;\nprecision mediump float;\nvarying vec2 v_texCoord;\nuniform float i_alpha;\nvoid main() {\n  gl_FragColor = texture2D(s_texture, v_texCoord);\n  gl_FragColor.a = i_alpha;\n}\n"

    invoke-static {v0, v1}, Lf;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Laui;->b:I

    .line 129
    const-string v0, "attribute vec4 vPosition;\nattribute vec2 a_texCoord;\nvarying vec2 v_texCoord;\nvoid main() {\n  gl_Position = vPosition;\n  v_texCoord = a_texCoord;\n}\n"

    const-string v1, "#extension GL_OES_EGL_image_external : require\nuniform samplerExternalOES s_texture;\nprecision mediump float;\nvarying vec2 v_texCoord;\nuniform float i_alpha;\nvoid main() {\n  gl_FragColor = texture2D(s_texture, v_texCoord);\n  gl_FragColor.a = i_alpha;\n}\n"

    invoke-static {v0, v1}, Lf;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Laui;->c:I

    .line 130
    const/4 v0, -0x1

    iput v0, p0, Laui;->d:I

    .line 132
    const/4 v0, 0x0

    iput v0, p0, Laui;->e:I

    .line 133
    const/4 v0, 0x1

    iput-boolean v0, p0, Laui;->q:Z

    .line 134
    return-void
.end method

.method public a(FF)V
    .locals 0

    .prologue
    .line 161
    invoke-virtual {p0, p1, p2, p1, p2}, Laui;->a(FFFF)V

    .line 162
    return-void
.end method

.method public a(FFFF)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 166
    cmpl-float v0, p1, v3

    if-ltz v0, :cond_3

    cmpl-float v0, p2, v3

    if-ltz v0, :cond_3

    cmpl-float v0, p3, v3

    if-ltz v0, :cond_3

    cmpl-float v0, p4, v3

    if-ltz v0, :cond_3

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 167
    cmpg-float v0, p1, v4

    if-gtz v0, :cond_0

    cmpg-float v0, p2, v4

    if-gtz v0, :cond_0

    cmpg-float v0, p3, v4

    if-gtz v0, :cond_0

    cmpg-float v0, p4, v4

    if-gtz v0, :cond_0

    move v2, v1

    :cond_0
    invoke-static {v2}, Lcwz;->a(Z)V

    .line 168
    iget v0, p0, Laui;->r:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_1

    iget v0, p0, Laui;->t:F

    cmpl-float v0, v0, p2

    if-nez v0, :cond_1

    iget v0, p0, Laui;->s:F

    cmpl-float v0, v0, p3

    if-nez v0, :cond_1

    iget v0, p0, Laui;->u:F

    cmpl-float v0, v0, p4

    if-eqz v0, :cond_2

    .line 170
    :cond_1
    iput p1, p0, Laui;->r:F

    .line 171
    iput p2, p0, Laui;->t:F

    .line 172
    iput p3, p0, Laui;->s:F

    .line 173
    iput p4, p0, Laui;->u:F

    .line 174
    iput-boolean v1, p0, Laui;->q:Z

    .line 176
    :cond_2
    return-void

    :cond_3
    move v0, v2

    .line 166
    goto :goto_0
.end method

.method public a(II)V
    .locals 0

    .prologue
    .line 179
    iput p1, p0, Laui;->f:I

    .line 180
    iput p2, p0, Laui;->g:I

    .line 181
    return-void
.end method

.method public a(IZ)V
    .locals 3

    .prologue
    .line 195
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Laui;->w:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " setInputTextureName "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", external texture? "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    iput p1, p0, Laui;->e:I

    .line 198
    iget v0, p0, Laui;->b:I

    if-eqz p2, :cond_0

    iget v0, p0, Laui;->c:I

    :cond_0
    iget v1, p0, Laui;->d:I

    if-eq v0, v1, :cond_1

    const-string v1, "a_texCoord"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v1

    iput v1, p0, Laui;->i:I

    const-string v1, "vPosition"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v1

    iput v1, p0, Laui;->j:I

    const-string v1, "i_alpha"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v1

    iput v1, p0, Laui;->k:I

    const-string v1, "s_texture"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v1

    iput v1, p0, Laui;->h:I

    const-string v1, "get..Location"

    invoke-static {v1}, Lf;->s(Ljava/lang/String;)V

    iput v0, p0, Laui;->d:I

    .line 199
    :cond_1
    return-void
.end method

.method public a(Landroid/graphics/Rect;F)V
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/high16 v11, 0x40000000    # 2.0f

    const/4 v1, 0x2

    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 236
    iget v6, p0, Laui;->d:I

    .line 237
    iget v0, p0, Laui;->c:I

    if-ne v6, v0, :cond_1

    move v0, v2

    .line 239
    :goto_0
    iget v4, p0, Laui;->e:I

    if-eqz v4, :cond_0

    if-eqz v6, :cond_0

    iget v4, p0, Laui;->f:I

    if-eqz v4, :cond_0

    iget v4, p0, Laui;->g:I

    if-nez v4, :cond_2

    .line 241
    :cond_0
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Laui;->w:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " dest="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/graphics/Rect;->toShortString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " alpha="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " input tex="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Laui;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " w="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Laui;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " h="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Laui;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "TextureToScreenRenderer not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v0, v3

    .line 237
    goto :goto_0

    .line 247
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    const/4 v4, 0x0

    cmpl-float v4, p2, v4

    if-nez v4, :cond_4

    .line 326
    :cond_3
    :goto_1
    return-void

    .line 250
    :cond_4
    iget-boolean v4, p0, Laui;->q:Z

    if-eqz v4, :cond_5

    .line 251
    iget-boolean v4, p0, Laui;->v:Z

    if-eqz v4, :cond_8

    iget v4, p0, Laui;->r:F

    sub-float v5, v10, v4

    iget v4, p0, Laui;->s:F

    :goto_2
    const/16 v7, 0x8

    new-array v7, v7, [F

    aput v5, v7, v3

    iget v8, p0, Laui;->u:F

    sub-float v8, v10, v8

    aput v8, v7, v2

    aput v4, v7, v1

    const/4 v8, 0x3

    iget v9, p0, Laui;->u:F

    sub-float v9, v10, v9

    aput v9, v7, v8

    const/4 v8, 0x4

    aput v4, v7, v8

    const/4 v4, 0x5

    iget v8, p0, Laui;->t:F

    aput v8, v7, v4

    const/4 v4, 0x6

    aput v5, v7, v4

    const/4 v4, 0x7

    iget v5, p0, Laui;->t:F

    aput v5, v7, v4

    iget-object v4, p0, Laui;->l:Ljava/nio/FloatBuffer;

    invoke-virtual {v4, v7}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 252
    iput-boolean v3, p0, Laui;->q:Z

    .line 254
    :cond_5
    iget-object v4, p0, Laui;->n:Landroid/graphics/Rect;

    invoke-virtual {p1, v4}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 255
    iget-object v4, p0, Laui;->n:Landroid/graphics/Rect;

    invoke-virtual {v4, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 257
    iget-object v4, p0, Laui;->o:Lcom/google/android/libraries/hangouts/video/FloatRect;

    const/high16 v5, -0x40800000    # -1.0f

    iget-object v7, p0, Laui;->n:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    int-to-float v7, v7

    iget v8, p0, Laui;->f:I

    int-to-float v8, v8

    div-float/2addr v7, v8

    mul-float/2addr v7, v11

    add-float/2addr v5, v7

    iput v5, v4, Lcom/google/android/libraries/hangouts/video/FloatRect;->left:F

    .line 259
    iget-object v4, p0, Laui;->o:Lcom/google/android/libraries/hangouts/video/FloatRect;

    iget-object v5, p0, Laui;->n:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    int-to-float v5, v5

    iget v7, p0, Laui;->g:I

    int-to-float v7, v7

    div-float/2addr v5, v7

    mul-float/2addr v5, v11

    sub-float v5, v10, v5

    iput v5, v4, Lcom/google/android/libraries/hangouts/video/FloatRect;->top:F

    .line 261
    iget-object v4, p0, Laui;->o:Lcom/google/android/libraries/hangouts/video/FloatRect;

    const/high16 v5, -0x40800000    # -1.0f

    iget-object v7, p0, Laui;->n:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    int-to-float v7, v7

    iget v8, p0, Laui;->f:I

    int-to-float v8, v8

    div-float/2addr v7, v8

    mul-float/2addr v7, v11

    add-float/2addr v5, v7

    iput v5, v4, Lcom/google/android/libraries/hangouts/video/FloatRect;->right:F

    .line 263
    iget-object v4, p0, Laui;->o:Lcom/google/android/libraries/hangouts/video/FloatRect;

    iget-object v5, p0, Laui;->n:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    int-to-float v5, v5

    iget v7, p0, Laui;->g:I

    int-to-float v7, v7

    div-float/2addr v5, v7

    mul-float/2addr v5, v11

    sub-float v5, v10, v5

    iput v5, v4, Lcom/google/android/libraries/hangouts/video/FloatRect;->bottom:F

    .line 265
    iget-object v4, p0, Laui;->o:Lcom/google/android/libraries/hangouts/video/FloatRect;

    iget-object v5, p0, Laui;->p:[F

    iget v7, v4, Lcom/google/android/libraries/hangouts/video/FloatRect;->left:F

    aput v7, v5, v3

    iget v7, v4, Lcom/google/android/libraries/hangouts/video/FloatRect;->bottom:F

    aput v7, v5, v2

    iget v7, v4, Lcom/google/android/libraries/hangouts/video/FloatRect;->right:F

    aput v7, v5, v1

    const/4 v7, 0x3

    iget v8, v4, Lcom/google/android/libraries/hangouts/video/FloatRect;->bottom:F

    aput v8, v5, v7

    const/4 v7, 0x4

    iget v8, v4, Lcom/google/android/libraries/hangouts/video/FloatRect;->right:F

    aput v8, v5, v7

    const/4 v7, 0x5

    iget v8, v4, Lcom/google/android/libraries/hangouts/video/FloatRect;->top:F

    aput v8, v5, v7

    const/4 v7, 0x6

    iget v8, v4, Lcom/google/android/libraries/hangouts/video/FloatRect;->left:F

    aput v8, v5, v7

    const/4 v7, 0x7

    iget v4, v4, Lcom/google/android/libraries/hangouts/video/FloatRect;->top:F

    aput v4, v5, v7

    .line 267
    sget-boolean v4, Laui;->a:Z

    if-eqz v4, :cond_6

    .line 268
    const-string v4, "Babel"

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "%s Pixels=%d,%d:%d,%d View=%d:%d Vertices=%.2f,%.2f:%.2f,%.2f)"

    const/16 v8, 0xb

    new-array v8, v8, [Ljava/lang/Object;

    iget-object v9, p0, Laui;->w:Ljava/lang/String;

    aput-object v9, v8, v3

    iget v9, p1, Landroid/graphics/Rect;->left:I

    .line 270
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v2

    iget v2, p1, Landroid/graphics/Rect;->top:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v8, v1

    const/4 v2, 0x3

    iget v9, p1, Landroid/graphics/Rect;->right:I

    .line 271
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v2

    const/4 v2, 0x4

    iget v9, p1, Landroid/graphics/Rect;->bottom:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v2

    const/4 v2, 0x5

    iget v9, p0, Laui;->f:I

    .line 272
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v2

    const/4 v2, 0x6

    iget v9, p0, Laui;->g:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v2

    const/4 v2, 0x7

    iget-object v9, p0, Laui;->o:Lcom/google/android/libraries/hangouts/video/FloatRect;

    iget v9, v9, Lcom/google/android/libraries/hangouts/video/FloatRect;->left:F

    .line 273
    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    aput-object v9, v8, v2

    const/16 v2, 0x8

    iget-object v9, p0, Laui;->o:Lcom/google/android/libraries/hangouts/video/FloatRect;

    iget v9, v9, Lcom/google/android/libraries/hangouts/video/FloatRect;->top:F

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    aput-object v9, v8, v2

    const/16 v2, 0x9

    iget-object v9, p0, Laui;->o:Lcom/google/android/libraries/hangouts/video/FloatRect;

    iget v9, v9, Lcom/google/android/libraries/hangouts/video/FloatRect;->right:F

    .line 274
    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    aput-object v9, v8, v2

    const/16 v2, 0xa

    iget-object v9, p0, Laui;->o:Lcom/google/android/libraries/hangouts/video/FloatRect;

    iget v9, v9, Lcom/google/android/libraries/hangouts/video/FloatRect;->bottom:F

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    aput-object v9, v8, v2

    .line 268
    invoke-static {v5, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    :cond_6
    iget-object v2, p0, Laui;->m:Ljava/nio/FloatBuffer;

    iget-object v4, p0, Laui;->p:[F

    invoke-virtual {v2, v4}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 279
    :cond_7
    invoke-static {v6}, Landroid/opengl/GLES20;->glUseProgram(I)V

    .line 280
    iget v2, p0, Laui;->f:I

    iget v4, p0, Laui;->g:I

    invoke-static {v3, v3, v2, v4}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 281
    const-string v2, "glViewport"

    invoke-static {v2}, Lf;->s(Ljava/lang/String;)V

    .line 283
    float-to-double v4, p2

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    cmpl-double v2, v4, v6

    if-nez v2, :cond_9

    .line 284
    const/16 v2, 0xbe2

    invoke-static {v2}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 291
    :goto_3
    iget v2, p0, Laui;->k:I

    invoke-static {v2, p2}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 294
    const v2, 0x84c0

    invoke-static {v2}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 295
    const/16 v2, 0xde1

    .line 296
    if-eqz v0, :cond_a

    .line 297
    const v0, 0x8d65

    .line 299
    :goto_4
    iget v2, p0, Laui;->e:I

    invoke-static {v0, v2}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 300
    iget v2, p0, Laui;->h:I

    invoke-static {}, Lf;->A()I

    move-result v4

    invoke-static {v2, v4}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 301
    const/16 v2, 0x2801

    const/16 v4, 0x2601

    invoke-static {v0, v2, v4}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 303
    const/16 v2, 0x2800

    const/16 v4, 0x2601

    invoke-static {v0, v2, v4}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 307
    const/16 v2, 0x2802

    const v4, 0x812f

    invoke-static {v0, v2, v4}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 309
    const/16 v2, 0x2803

    const v4, 0x812f

    invoke-static {v0, v2, v4}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 313
    iget v0, p0, Laui;->i:I

    const/16 v2, 0x1406

    iget-object v5, p0, Laui;->l:Ljava/nio/FloatBuffer;

    move v4, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 315
    iget v0, p0, Laui;->i:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 318
    iget v0, p0, Laui;->j:I

    const/16 v2, 0x1406

    iget-object v5, p0, Laui;->m:Ljava/nio/FloatBuffer;

    move v4, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 320
    iget v0, p0, Laui;->j:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 321
    const-string v0, "setup"

    invoke-static {v0}, Lf;->s(Ljava/lang/String;)V

    .line 324
    const/4 v0, 0x6

    const/4 v1, 0x4

    invoke-static {v0, v3, v1}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    .line 325
    const-string v0, "glDrawArrays"

    invoke-static {v0}, Lf;->s(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 251
    :cond_8
    iget v5, p0, Laui;->r:F

    iget v4, p0, Laui;->s:F

    sub-float v4, v10, v4

    goto/16 :goto_2

    .line 286
    :cond_9
    const/16 v2, 0xbe2

    invoke-static {v2}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 287
    const/16 v2, 0x302

    const/16 v4, 0x303

    invoke-static {v2, v4}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    goto/16 :goto_3

    :cond_a
    move v0, v2

    goto :goto_4
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 203
    iput-boolean p1, p0, Laui;->v:Z

    .line 204
    const/4 v0, 0x1

    iput-boolean v0, p0, Laui;->q:Z

    .line 205
    return-void
.end method
