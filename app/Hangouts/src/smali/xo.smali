.class public final Lxo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:I

.field private d:Lxq;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    iput-object p1, p0, Lxo;->a:Ljava/lang/String;

    .line 98
    iput-object p2, p0, Lxo;->b:Ljava/lang/String;

    .line 99
    return-void
.end method

.method public constructor <init>(Lxp;)V
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    iget-object v0, p1, Lxp;->a:Ljava/lang/String;

    iput-object v0, p0, Lxo;->a:Ljava/lang/String;

    .line 91
    iget-object v0, p1, Lxp;->b:Ljava/lang/String;

    iput-object v0, p0, Lxo;->b:Ljava/lang/String;

    .line 92
    iget v0, p1, Lxp;->c:I

    iput v0, p0, Lxo;->c:I

    .line 93
    iget-object v0, p1, Lxp;->d:Lxq;

    iput-object v0, p0, Lxo;->d:Lxq;

    .line 94
    return-void
.end method

.method public static newBuilder()Lxp;
    .locals 2

    .prologue
    .line 102
    new-instance v0, Lxp;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lxp;-><init>(B)V

    return-object v0
.end method

.method public static newBuilder(Lxo;)Lxp;
    .locals 1

    .prologue
    .line 106
    invoke-static {}, Lxo;->newBuilder()Lxp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lxp;->a(Lxo;)Lxp;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lxo;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lxo;->a:Ljava/lang/String;

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lxo;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 14
    new-instance v0, Lxp;

    invoke-direct {v0}, Lxp;-><init>()V

    iget-object v1, p0, Lxo;->d:Lxq;

    iput-object v1, v0, Lxp;->d:Lxq;

    iget-object v1, p0, Lxo;->a:Ljava/lang/String;

    iput-object v1, v0, Lxp;->a:Ljava/lang/String;

    iget-object v1, p0, Lxo;->b:Ljava/lang/String;

    iput-object v1, v0, Lxp;->b:Ljava/lang/String;

    iget v1, p0, Lxo;->c:I

    iput v1, v0, Lxp;->c:I

    invoke-virtual {v0}, Lxp;->a()Lxo;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lxo;->b:Ljava/lang/String;

    return-object v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 150
    iget v0, p0, Lxo;->c:I

    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lxo;->d:Lxq;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Lxq;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lxo;->d:Lxq;

    return-object v0
.end method
