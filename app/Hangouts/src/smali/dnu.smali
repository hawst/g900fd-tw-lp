.class public final Ldnu;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldnu;


# instance fields
.field public b:Ljava/lang/Float;

.field public c:Ljava/lang/Float;

.field public d:Ljava/lang/Float;

.field public e:Ljava/lang/Float;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 725
    const/4 v0, 0x0

    new-array v0, v0, [Ldnu;

    sput-object v0, Ldnu;->a:[Ldnu;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 726
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 757
    const/4 v0, 0x0

    .line 758
    iget-object v1, p0, Ldnu;->b:Ljava/lang/Float;

    if-eqz v1, :cond_0

    .line 759
    const/4 v0, 0x1

    iget-object v1, p0, Ldnu;->b:Ljava/lang/Float;

    .line 760
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    invoke-static {v0}, Lepl;->g(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 762
    :cond_0
    iget-object v1, p0, Ldnu;->c:Ljava/lang/Float;

    if-eqz v1, :cond_1

    .line 763
    const/4 v1, 0x2

    iget-object v2, p0, Ldnu;->c:Ljava/lang/Float;

    .line 764
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 766
    :cond_1
    iget-object v1, p0, Ldnu;->d:Ljava/lang/Float;

    if-eqz v1, :cond_2

    .line 767
    const/4 v1, 0x3

    iget-object v2, p0, Ldnu;->d:Ljava/lang/Float;

    .line 768
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 770
    :cond_2
    iget-object v1, p0, Ldnu;->e:Ljava/lang/Float;

    if-eqz v1, :cond_3

    .line 771
    const/4 v1, 0x4

    iget-object v2, p0, Ldnu;->e:Ljava/lang/Float;

    .line 772
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 774
    :cond_3
    iget-object v1, p0, Ldnu;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 775
    iput v0, p0, Ldnu;->cachedSize:I

    .line 776
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 722
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldnu;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldnu;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldnu;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Ldnu;->b:Ljava/lang/Float;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Ldnu;->c:Ljava/lang/Float;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Ldnu;->d:Ljava/lang/Float;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Ldnu;->e:Ljava/lang/Float;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
        0x1d -> :sswitch_3
        0x25 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 739
    iget-object v0, p0, Ldnu;->b:Ljava/lang/Float;

    if-eqz v0, :cond_0

    .line 740
    const/4 v0, 0x1

    iget-object v1, p0, Ldnu;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IF)V

    .line 742
    :cond_0
    iget-object v0, p0, Ldnu;->c:Ljava/lang/Float;

    if-eqz v0, :cond_1

    .line 743
    const/4 v0, 0x2

    iget-object v1, p0, Ldnu;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IF)V

    .line 745
    :cond_1
    iget-object v0, p0, Ldnu;->d:Ljava/lang/Float;

    if-eqz v0, :cond_2

    .line 746
    const/4 v0, 0x3

    iget-object v1, p0, Ldnu;->d:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IF)V

    .line 748
    :cond_2
    iget-object v0, p0, Ldnu;->e:Ljava/lang/Float;

    if-eqz v0, :cond_3

    .line 749
    const/4 v0, 0x4

    iget-object v1, p0, Ldnu;->e:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IF)V

    .line 751
    :cond_3
    iget-object v0, p0, Ldnu;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 753
    return-void
.end method
