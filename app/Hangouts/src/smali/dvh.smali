.class public final Ldvh;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldvh;


# instance fields
.field public b:Ldvm;

.field public c:Ldrx;

.field public d:Ljava/lang/String;

.field public e:[B

.field public f:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6437
    const/4 v0, 0x0

    new-array v0, v0, [Ldvh;

    sput-object v0, Ldvh;->a:[Ldvh;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 6438
    invoke-direct {p0}, Lepn;-><init>()V

    .line 6441
    iput-object v0, p0, Ldvh;->b:Ldvm;

    .line 6444
    iput-object v0, p0, Ldvh;->c:Ldrx;

    .line 6438
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 6476
    const/4 v0, 0x0

    .line 6477
    iget-object v1, p0, Ldvh;->b:Ldvm;

    if-eqz v1, :cond_0

    .line 6478
    const/4 v0, 0x1

    iget-object v1, p0, Ldvh;->b:Ldvm;

    .line 6479
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6481
    :cond_0
    iget-object v1, p0, Ldvh;->e:[B

    if-eqz v1, :cond_1

    .line 6482
    const/4 v1, 0x2

    iget-object v2, p0, Ldvh;->e:[B

    .line 6483
    invoke-static {v1, v2}, Lepl;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 6485
    :cond_1
    iget-object v1, p0, Ldvh;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 6486
    const/4 v1, 0x3

    iget-object v2, p0, Ldvh;->d:Ljava/lang/String;

    .line 6487
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6489
    :cond_2
    iget-object v1, p0, Ldvh;->f:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 6490
    const/4 v1, 0x4

    iget-object v2, p0, Ldvh;->f:Ljava/lang/Long;

    .line 6491
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 6493
    :cond_3
    iget-object v1, p0, Ldvh;->c:Ldrx;

    if-eqz v1, :cond_4

    .line 6494
    const/4 v1, 0x5

    iget-object v2, p0, Ldvh;->c:Ldrx;

    .line 6495
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6497
    :cond_4
    iget-object v1, p0, Ldvh;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6498
    iput v0, p0, Ldvh;->cachedSize:I

    .line 6499
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 6434
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldvh;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldvh;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldvh;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldvh;->b:Ldvm;

    if-nez v0, :cond_2

    new-instance v0, Ldvm;

    invoke-direct {v0}, Ldvm;-><init>()V

    iput-object v0, p0, Ldvh;->b:Ldvm;

    :cond_2
    iget-object v0, p0, Ldvh;->b:Ldvm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v0

    iput-object v0, p0, Ldvh;->e:[B

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldvh;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldvh;->f:Ljava/lang/Long;

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ldvh;->c:Ldrx;

    if-nez v0, :cond_3

    new-instance v0, Ldrx;

    invoke-direct {v0}, Ldrx;-><init>()V

    iput-object v0, p0, Ldvh;->c:Ldrx;

    :cond_3
    iget-object v0, p0, Ldvh;->c:Ldrx;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 6455
    iget-object v0, p0, Ldvh;->b:Ldvm;

    if-eqz v0, :cond_0

    .line 6456
    const/4 v0, 0x1

    iget-object v1, p0, Ldvh;->b:Ldvm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 6458
    :cond_0
    iget-object v0, p0, Ldvh;->e:[B

    if-eqz v0, :cond_1

    .line 6459
    const/4 v0, 0x2

    iget-object v1, p0, Ldvh;->e:[B

    invoke-virtual {p1, v0, v1}, Lepl;->a(I[B)V

    .line 6461
    :cond_1
    iget-object v0, p0, Ldvh;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 6462
    const/4 v0, 0x3

    iget-object v1, p0, Ldvh;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 6464
    :cond_2
    iget-object v0, p0, Ldvh;->f:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 6465
    const/4 v0, 0x4

    iget-object v1, p0, Ldvh;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 6467
    :cond_3
    iget-object v0, p0, Ldvh;->c:Ldrx;

    if-eqz v0, :cond_4

    .line 6468
    const/4 v0, 0x5

    iget-object v1, p0, Ldvh;->c:Ldrx;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 6470
    :cond_4
    iget-object v0, p0, Ldvh;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 6472
    return-void
.end method
