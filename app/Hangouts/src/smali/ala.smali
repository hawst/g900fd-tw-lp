.class public final Lala;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcvr;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)V
    .locals 0

    .prologue
    .line 657
    iput-object p1, p0, Lala;->a:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcft;Lcwf;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 661
    :try_start_0
    iget-object v1, p0, Lala;->a:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, v1, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->mInviterCircleIds:Ljava/util/HashSet;

    .line 662
    invoke-virtual {p2}, Lcwf;->a()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 665
    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Lcwf;->b(I)Lcwe;

    move-result-object v1

    .line 666
    invoke-interface {v1}, Lcwe;->e()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lala;->a:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    invoke-static {v3}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->a(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)Lbdk;

    move-result-object v3

    iget-object v3, v3, Lbdk;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 667
    invoke-interface {v1}, Lcwe;->i()[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 668
    iget-object v4, p0, Lala;->a:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    iget-object v4, v4, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->mInviterCircleIds:Ljava/util/HashSet;

    invoke-virtual {v4, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 667
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 672
    :cond_0
    iget-object v0, p0, Lala;->a:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->j(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 674
    invoke-virtual {p2}, Lcwf;->d()V

    .line 675
    return-void

    .line 674
    :catchall_0
    move-exception v0

    invoke-virtual {p2}, Lcwf;->d()V

    throw v0
.end method
