.class public Le;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljk;


# static fields
.field private static final a:La;


# instance fields
.field private final b:Landroid/app/Activity;

.field private final c:Li;

.field private final d:Landroid/support/v4/widget/DrawerLayout;

.field private e:Z

.field private f:Landroid/graphics/drawable/Drawable;

.field private g:Landroid/graphics/drawable/Drawable;

.field private h:Lk;

.field private final i:I

.field private final j:I

.field private final k:I

.field private l:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 169
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 170
    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    .line 171
    new-instance v0, Lh;

    invoke-direct {v0, v2}, Lh;-><init>(B)V

    sput-object v0, Le;->a:La;

    .line 177
    :goto_0
    return-void

    .line 172
    :cond_0
    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 173
    new-instance v0, Lg;

    invoke-direct {v0, v2}, Lg;-><init>(B)V

    sput-object v0, Le;->a:La;

    goto :goto_0

    .line 175
    :cond_1
    new-instance v0, Lf;

    invoke-direct {v0, v2}, Lf;-><init>(B)V

    sput-object v0, Le;->a:La;

    goto :goto_0
.end method

.method public constructor <init>(Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;III)V
    .locals 3

    .prologue
    .line 220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 188
    const/4 v0, 0x1

    iput-boolean v0, p0, Le;->e:Z

    .line 221
    iput-object p1, p0, Le;->b:Landroid/app/Activity;

    .line 224
    instance-of v0, p1, Lj;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 225
    check-cast v0, Lj;

    invoke-interface {v0}, Lj;->w_()Li;

    move-result-object v0

    iput-object v0, p0, Le;->c:Li;

    .line 230
    :goto_0
    iput-object p2, p0, Le;->d:Landroid/support/v4/widget/DrawerLayout;

    .line 231
    iput p3, p0, Le;->i:I

    .line 232
    iput p4, p0, Le;->j:I

    .line 233
    iput p5, p0, Le;->k:I

    .line 235
    invoke-virtual {p0}, Le;->c()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Le;->f:Landroid/graphics/drawable/Drawable;

    .line 236
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Le;->g:Landroid/graphics/drawable/Drawable;

    .line 237
    new-instance v0, Lk;

    iget-object v1, p0, Le;->g:Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lk;-><init>(Le;Landroid/graphics/drawable/Drawable;B)V

    iput-object v0, p0, Le;->h:Lk;

    .line 238
    iget-object v0, p0, Le;->h:Lk;

    invoke-virtual {v0}, Lk;->b()V

    .line 239
    return-void

    .line 227
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Le;->c:Li;

    goto :goto_0
.end method

.method static synthetic a(Le;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Le;->b:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 251
    iget-object v0, p0, Le;->d:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 252
    iget-object v0, p0, Le;->h:Lk;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lk;->a(F)V

    .line 257
    :goto_0
    iget-boolean v0, p0, Le;->e:Z

    if-eqz v0, :cond_0

    .line 258
    iget-object v1, p0, Le;->h:Lk;

    iget-object v0, p0, Le;->d:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Le;->k:I

    :goto_1
    invoke-virtual {p0, v1, v0}, Le;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 261
    :cond_0
    return-void

    .line 254
    :cond_1
    iget-object v0, p0, Le;->h:Lk;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lk;->a(F)V

    goto :goto_0

    .line 258
    :cond_2
    iget v0, p0, Le;->j:I

    goto :goto_1
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 386
    return-void
.end method

.method a(Landroid/graphics/drawable/Drawable;I)V
    .locals 3

    .prologue
    .line 396
    iget-object v0, p0, Le;->c:Li;

    if-eqz v0, :cond_0

    .line 397
    iget-object v0, p0, Le;->c:Li;

    invoke-interface {v0, p1, p2}, Li;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 402
    :goto_0
    return-void

    .line 400
    :cond_0
    sget-object v0, Le;->a:La;

    iget-object v1, p0, Le;->l:Ljava/lang/Object;

    iget-object v2, p0, Le;->b:Landroid/app/Activity;

    invoke-interface {v0, v1, v2, p1, p2}, La;->a(Ljava/lang/Object;Landroid/app/Activity;Landroid/graphics/drawable/Drawable;I)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Le;->l:Ljava/lang/Object;

    goto :goto_0
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 356
    iget-object v0, p0, Le;->h:Lk;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lk;->a(F)V

    .line 357
    iget-boolean v0, p0, Le;->e:Z

    if-eqz v0, :cond_0

    .line 358
    iget v0, p0, Le;->k:I

    invoke-virtual {p0, v0}, Le;->b(I)V

    .line 360
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;F)V
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    const/high16 v2, 0x3f000000    # 0.5f

    .line 338
    iget-object v0, p0, Le;->h:Lk;

    invoke-virtual {v0}, Lk;->a()F

    move-result v0

    .line 339
    cmpl-float v1, p2, v2

    if-lez v1, :cond_0

    .line 340
    const/4 v1, 0x0

    sub-float v2, p2, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    mul-float/2addr v1, v3

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 344
    :goto_0
    iget-object v1, p0, Le;->h:Lk;

    invoke-virtual {v1, v0}, Lk;->a(F)V

    .line 345
    return-void

    .line 342
    :cond_0
    mul-float v1, p2, v3

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto :goto_0
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 317
    if-eqz p1, :cond_1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Le;->e:Z

    if-eqz v0, :cond_1

    .line 318
    iget-object v0, p0, Le;->d:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, Le;->d:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->d()V

    .line 323
    :goto_0
    const/4 v0, 0x1

    .line 325
    :goto_1
    return v0

    .line 321
    :cond_0
    iget-object v0, p0, Le;->d:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->c()V

    goto :goto_0

    .line 325
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public b()V
    .locals 2

    .prologue
    .line 302
    invoke-virtual {p0}, Le;->c()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Le;->f:Landroid/graphics/drawable/Drawable;

    .line 303
    iget-object v0, p0, Le;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Le;->i:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Le;->g:Landroid/graphics/drawable/Drawable;

    .line 304
    invoke-virtual {p0}, Le;->a()V

    .line 305
    return-void
.end method

.method b(I)V
    .locals 3

    .prologue
    .line 405
    iget-object v0, p0, Le;->c:Li;

    if-eqz v0, :cond_0

    .line 406
    iget-object v0, p0, Le;->c:Li;

    invoke-interface {v0, p1}, Li;->a(I)V

    .line 411
    :goto_0
    return-void

    .line 409
    :cond_0
    sget-object v0, Le;->a:La;

    iget-object v1, p0, Le;->l:Ljava/lang/Object;

    iget-object v2, p0, Le;->b:Landroid/app/Activity;

    invoke-interface {v0, v1, v2, p1}, La;->a(Ljava/lang/Object;Landroid/app/Activity;I)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Le;->l:Ljava/lang/Object;

    goto :goto_0
.end method

.method public b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 371
    iget-object v0, p0, Le;->h:Lk;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lk;->a(F)V

    .line 372
    iget-boolean v0, p0, Le;->e:Z

    if-eqz v0, :cond_0

    .line 373
    iget v0, p0, Le;->j:I

    invoke-virtual {p0, v0}, Le;->b(I)V

    .line 375
    :cond_0
    return-void
.end method

.method c()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 389
    iget-object v0, p0, Le;->c:Li;

    if-eqz v0, :cond_0

    .line 390
    iget-object v0, p0, Le;->c:Li;

    invoke-interface {v0}, Li;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 392
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Le;->a:La;

    iget-object v1, p0, Le;->b:Landroid/app/Activity;

    invoke-interface {v0, v1}, La;->a(Landroid/app/Activity;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method
