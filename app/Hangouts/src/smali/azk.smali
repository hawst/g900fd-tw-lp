.class public final Lazk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/phone/DebugActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/phone/DebugActivity;)V
    .locals 0

    .prologue
    .line 374
    iput-object p1, p0, Lazk;->a:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3

    .prologue
    .line 381
    check-cast p2, Lazp;

    .line 382
    iget-object v0, p0, Lazk;->a:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    invoke-virtual {p2}, Lazp;->a()Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/DebugActivity;->a(Lcom/google/android/apps/hangouts/phone/DebugActivity;Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;)Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;

    .line 384
    iget-object v0, p0, Lazk;->a:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/DebugActivity;->d(Lcom/google/android/apps/hangouts/phone/DebugActivity;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lazk;->a:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/phone/DebugActivity;->f(Lcom/google/android/apps/hangouts/phone/DebugActivity;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 386
    iget-object v0, p0, Lazk;->a:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    iget-object v1, p0, Lazk;->a:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/phone/DebugActivity;->g(Lcom/google/android/apps/hangouts/phone/DebugActivity;)Landroid/widget/ArrayAdapter;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/DebugActivity;->a(Lcom/google/android/apps/hangouts/phone/DebugActivity;Landroid/widget/ArrayAdapter;)Landroid/widget/ArrayAdapter;

    .line 387
    iget-object v0, p0, Lazk;->a:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/DebugActivity;->i(Lcom/google/android/apps/hangouts/phone/DebugActivity;)Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;

    move-result-object v0

    iget-object v1, p0, Lazk;->a:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/phone/DebugActivity;->h(Lcom/google/android/apps/hangouts/phone/DebugActivity;)Landroid/widget/ArrayAdapter;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;->a(Landroid/widget/ArrayAdapter;Ljava/lang/String;)V

    .line 388
    iget-object v0, p0, Lazk;->a:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/DebugActivity;->d(Lcom/google/android/apps/hangouts/phone/DebugActivity;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lazk;->a:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/phone/DebugActivity;->h(Lcom/google/android/apps/hangouts/phone/DebugActivity;)Landroid/widget/ArrayAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 389
    iget-object v0, p0, Lazk;->a:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/DebugActivity;->d(Lcom/google/android/apps/hangouts/phone/DebugActivity;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lazk;->a:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/phone/DebugActivity;->h(Lcom/google/android/apps/hangouts/phone/DebugActivity;)Landroid/widget/ArrayAdapter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 390
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 395
    iget-object v0, p0, Lazk;->a:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/DebugActivity;->i(Lcom/google/android/apps/hangouts/phone/DebugActivity;)Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;->a(Landroid/widget/ArrayAdapter;Ljava/lang/String;)V

    .line 396
    iget-object v0, p0, Lazk;->a:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/DebugActivity;->a(Lcom/google/android/apps/hangouts/phone/DebugActivity;Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;)Lcom/google/android/apps/hangouts/phone/DebugActivity$RecordingService;

    .line 397
    return-void
.end method
