.class public Lbhv;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ldvs;)V
    .locals 3

    .prologue
    .line 2270
    iget-object v0, p1, Ldvs;->b:Ldvn;

    const-wide/16 v1, -0x1

    invoke-direct {p0, v0, v1, v2}, Lbfz;-><init>(Ldvn;J)V

    .line 2272
    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2275
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbhv;->g:Ljava/util/List;

    .line 2284
    :cond_0
    :goto_0
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_1

    .line 2285
    const-string v0, "Babel_protos"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SearchEntitiesResponse from:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2287
    :cond_1
    return-void

    .line 2277
    :cond_2
    iget-object v0, p1, Ldvs;->c:[Ldro;

    invoke-static {v0}, Lbdh;->a([Ldro;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbhv;->g:Ljava/util/List;

    .line 2279
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_0

    .line 2280
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SearchEntitiesResponse. Number of entities:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbhv;->g:Ljava/util/List;

    .line 2281
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2280
    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 2291
    new-instance v0, Ldvs;

    invoke-direct {v0}, Ldvs;-><init>()V

    .line 2292
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldvs;

    .line 2293
    iget-object v1, v0, Ldvs;->b:Ldvn;

    invoke-static {v1}, Lbhv;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2294
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldvs;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 2296
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbhv;

    invoke-direct {v1, v0}, Lbhv;-><init>(Ldvs;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public f()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2301
    iget-object v0, p0, Lbhv;->g:Ljava/util/List;

    return-object v0
.end method
