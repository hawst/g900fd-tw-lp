.class public final Lahv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Laac;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V
    .locals 0

    .prologue
    .line 6238
    iput-object p1, p0, Lahv;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;B)V
    .locals 0

    .prologue
    .line 6238
    invoke-direct {p0, p1}, Lahv;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    return-void
.end method


# virtual methods
.method public a(Lbzn;Lbyd;ZLzx;Z)V
    .locals 9

    .prologue
    const/16 v8, 0xe

    const/16 v7, 0xb

    const/16 v6, 0x9

    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 6243
    if-eqz p1, :cond_0

    .line 6244
    const-string v0, "Babel"

    const-string v1, "Unexpected refCountedBitmap returned on a STICKER_REQUEST"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 6247
    invoke-virtual {p1}, Lbzn;->b()V

    .line 6251
    :cond_0
    iget-object v0, p0, Lahv;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->as(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lzx;

    move-result-object v0

    if-eq p4, v0, :cond_2

    .line 6314
    :cond_1
    :goto_0
    return-void

    .line 6256
    :cond_2
    iget-object v0, p0, Lahv;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->at(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lzx;

    .line 6259
    if-eqz p3, :cond_1

    .line 6264
    if-eqz p2, :cond_1

    .line 6268
    const/4 v0, 0x1

    .line 6270
    invoke-virtual {p4}, Lzx;->k()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 6272
    invoke-virtual {p4}, Lzx;->k()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 6271
    invoke-static {v0, v4}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    move v1, v0

    .line 6277
    :goto_1
    iget-object v0, p0, Lahv;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 6278
    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->au(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 6279
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v2, v3, :cond_3

    .line 6280
    iget-object v2, p0, Lahv;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Landroid/widget/RelativeLayout$LayoutParams;)V

    .line 6289
    :goto_2
    packed-switch v1, :pswitch_data_0

    .line 6302
    invoke-virtual {v0, v7, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 6305
    :goto_3
    iget-object v1, p0, Lahv;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->au(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 6307
    iget-object v0, p0, Lahv;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->au(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 6285
    :cond_3
    invoke-virtual {v0, v6, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 6286
    invoke-virtual {v0, v7, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 6287
    invoke-virtual {v0, v8, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_2

    .line 6291
    :pswitch_0
    invoke-virtual {v0, v6, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_3

    .line 6295
    :pswitch_1
    invoke-virtual {v0, v8, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_3

    :cond_4
    move v1, v0

    goto :goto_1

    .line 6289
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
