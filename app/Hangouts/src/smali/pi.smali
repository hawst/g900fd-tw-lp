.class public abstract Lpi;
.super Landroid/widget/BaseAdapter;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lpj;",
            ">;"
        }
    .end annotation
.end field

.field private c:I

.field private d:Z

.field private e:Z

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lpi;-><init>(Landroid/content/Context;B)V

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;B)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 70
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 61
    const/4 v0, 0x0

    iput v0, p0, Lpi;->c:I

    .line 62
    iput-boolean v1, p0, Lpi;->d:Z

    .line 63
    iput-boolean v1, p0, Lpi;->e:Z

    .line 71
    iput-object p1, p0, Lpi;->a:Landroid/content/Context;

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lpi;->b:Ljava/util/ArrayList;

    .line 73
    return-void
.end method


# virtual methods
.method public a(II)I
    .locals 1

    .prologue
    .line 298
    const/4 v0, 0x1

    return v0
.end method

.method public a()Landroid/content/Context;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lpi;->a:Landroid/content/Context;

    return-object v0
.end method

.method public a(ILandroid/database/Cursor;ILandroid/view/View;)Landroid/view/View;
    .locals 1

    .prologue
    .line 388
    if-eqz p4, :cond_0

    .line 393
    :goto_0
    invoke-virtual {p0, p4, p1, p2, p3}, Lpi;->a(Landroid/view/View;ILandroid/database/Cursor;I)V

    .line 394
    return-object p4

    .line 391
    :cond_0
    iget-object v0, p0, Lpi;->a:Landroid/content/Context;

    invoke-virtual {p0, v0, p1, p3}, Lpi;->a(Landroid/content/Context;II)Landroid/view/View;

    move-result-object p4

    goto :goto_0
.end method

.method protected a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 361
    if-eqz p2, :cond_0

    .line 364
    :goto_0
    return-object p2

    .line 361
    :cond_0
    iget-object v0, p0, Lpi;->a:Landroid/content/Context;

    .line 363
    invoke-virtual {p0, v0, p1, p3}, Lpi;->a(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_0
.end method

.method public abstract a(Landroid/content/Context;II)Landroid/view/View;
.end method

.method public a(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 373
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(I)Lpj;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lpi;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpj;

    return-object v0
.end method

.method public a(ILandroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 208
    iget-object v0, p0, Lpi;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpj;

    iget-object v0, v0, Lpj;->c:Landroid/database/Cursor;

    .line 209
    if-eq v0, p2, :cond_2

    .line 210
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 211
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 213
    :cond_0
    iget-object v0, p0, Lpi;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpj;

    iput-object p2, v0, Lpj;->c:Landroid/database/Cursor;

    .line 214
    if-eqz p2, :cond_1

    .line 215
    iget-object v0, p0, Lpi;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpj;

    const-string v1, "_id"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lpj;->d:I

    .line 217
    :cond_1
    invoke-virtual {p0}, Lpi;->c()V

    .line 218
    invoke-virtual {p0}, Lpi;->notifyDataSetChanged()V

    .line 220
    :cond_2
    return-void
.end method

.method public abstract a(Landroid/view/View;ILandroid/database/Cursor;I)V
.end method

.method public a(Lpj;)V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lpi;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 90
    invoke-virtual {p0}, Lpi;->c()V

    .line 91
    invoke-virtual {p0}, Lpi;->notifyDataSetChanged()V

    .line 92
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 85
    new-instance v0, Lpj;

    invoke-direct {v0, p1}, Lpj;-><init>(Z)V

    invoke-virtual {p0, v0}, Lpi;->a(Lpj;)V

    .line 86
    return-void
.end method

.method public areAllItemsEnabled()Z
    .locals 2

    .prologue
    .line 473
    iget-object v0, p0, Lpi;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpj;

    .line 474
    iget-boolean v0, v0, Lpj;->b:Z

    if-eqz v0, :cond_0

    .line 475
    const/4 v0, 0x0

    .line 478
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 117
    iget-object v0, p0, Lpi;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpj;

    .line 118
    const/4 v2, 0x0

    iput-object v2, v0, Lpj;->c:Landroid/database/Cursor;

    goto :goto_0

    .line 120
    :cond_0
    invoke-virtual {p0}, Lpi;->c()V

    .line 121
    invoke-virtual {p0}, Lpi;->notifyDataSetChanged()V

    .line 122
    return-void
.end method

.method public b(I)Z
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lpi;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpj;

    iget-boolean v0, v0, Lpj;->b:Z

    return v0
.end method

.method public c(I)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lpi;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpj;

    iget-object v0, v0, Lpj;->c:Landroid/database/Cursor;

    return-object v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 154
    const/4 v0, 0x0

    iput-boolean v0, p0, Lpi;->d:Z

    .line 155
    return-void
.end method

.method public d()I
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lpi;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public d(I)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 234
    invoke-virtual {p0}, Lpi;->e()V

    .line 236
    iget-object v0, p0, Lpi;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v1, v3, :cond_1

    .line 237
    iget-object v0, p0, Lpi;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpj;

    iget v0, v0, Lpj;->e:I

    add-int/2addr v0, v2

    .line 238
    if-lt p1, v2, :cond_0

    if-ge p1, v0, :cond_0

    move v0, v1

    .line 243
    :goto_1
    return v0

    .line 236
    :cond_0
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_0

    .line 243
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method protected e()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 162
    iget-boolean v0, p0, Lpi;->d:Z

    if-eqz v0, :cond_0

    .line 180
    :goto_0
    return-void

    .line 166
    :cond_0
    iput v2, p0, Lpi;->c:I

    .line 167
    iget-object v0, p0, Lpi;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpj;

    .line 168
    iget-object v1, v0, Lpj;->c:Landroid/database/Cursor;

    .line 169
    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 170
    :goto_2
    iget-boolean v4, v0, Lpj;->b:Z

    if-eqz v4, :cond_2

    .line 171
    if-nez v1, :cond_1

    iget-boolean v4, v0, Lpj;->a:Z

    if-eqz v4, :cond_2

    .line 172
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 175
    :cond_2
    iput v1, v0, Lpj;->e:I

    .line 176
    iget v0, p0, Lpi;->c:I

    add-int/2addr v0, v1

    iput v0, p0, Lpi;->c:I

    goto :goto_1

    :cond_3
    move v1, v2

    .line 169
    goto :goto_2

    .line 179
    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lpi;->d:Z

    goto :goto_0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 290
    const/4 v0, 0x1

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 193
    invoke-virtual {p0}, Lpi;->e()V

    .line 194
    iget v0, p0, Lpi;->c:I

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 414
    invoke-virtual {p0}, Lpi;->e()V

    .line 415
    const/4 v0, 0x0

    .line 416
    iget-object v1, p0, Lpi;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpj;

    .line 417
    iget v2, v0, Lpj;->e:I

    add-int/2addr v2, v1

    .line 418
    if-lt p1, v1, :cond_2

    if-ge p1, v2, :cond_2

    .line 419
    sub-int v1, p1, v1

    .line 420
    iget-boolean v2, v0, Lpj;->b:Z

    if-eqz v2, :cond_0

    .line 421
    add-int/lit8 v1, v1, -0x1

    .line 423
    :cond_0
    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    move-object v0, v3

    .line 433
    :goto_1
    return-object v0

    .line 426
    :cond_1
    iget-object v0, v0, Lpj;->c:Landroid/database/Cursor;

    .line 427
    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    goto :goto_1

    :cond_2
    move v1, v2

    .line 431
    goto :goto_0

    :cond_3
    move-object v0, v3

    .line 433
    goto :goto_1
.end method

.method public getItemId(I)J
    .locals 7

    .prologue
    const/4 v6, -0x1

    const-wide/16 v3, 0x0

    .line 440
    invoke-virtual {p0}, Lpi;->e()V

    .line 441
    const/4 v0, 0x0

    .line 442
    iget-object v1, p0, Lpi;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpj;

    .line 443
    iget v2, v0, Lpj;->e:I

    add-int/2addr v2, v1

    .line 444
    if-lt p1, v1, :cond_5

    if-ge p1, v2, :cond_5

    .line 445
    sub-int v1, p1, v1

    .line 446
    iget-boolean v2, v0, Lpj;->b:Z

    if-eqz v2, :cond_0

    .line 447
    add-int/lit8 v1, v1, -0x1

    .line 449
    :cond_0
    if-ne v1, v6, :cond_1

    move-wide v0, v3

    .line 465
    :goto_1
    return-wide v0

    .line 452
    :cond_1
    iget v2, v0, Lpj;->d:I

    if-ne v2, v6, :cond_2

    move-wide v0, v3

    .line 453
    goto :goto_1

    .line 456
    :cond_2
    iget-object v2, v0, Lpj;->c:Landroid/database/Cursor;

    .line 457
    if-eqz v2, :cond_3

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v5

    if-nez v5, :cond_3

    invoke-interface {v2, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-nez v1, :cond_4

    :cond_3
    move-wide v0, v3

    .line 458
    goto :goto_1

    .line 460
    :cond_4
    iget v0, v0, Lpj;->d:I

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    goto :goto_1

    :cond_5
    move v1, v2

    .line 463
    goto :goto_0

    :cond_6
    move-wide v0, v3

    .line 465
    goto :goto_1
.end method

.method public getItemViewType(I)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, -0x1

    .line 303
    invoke-virtual {p0}, Lpi;->e()V

    .line 305
    iget-object v2, p0, Lpi;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v0

    move v2, v0

    :goto_0
    if-ge v3, v5, :cond_2

    .line 306
    iget-object v0, p0, Lpi;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpj;

    iget v0, v0, Lpj;->e:I

    add-int v4, v2, v0

    .line 307
    if-lt p1, v2, :cond_1

    if-ge p1, v4, :cond_1

    .line 308
    sub-int v2, p1, v2

    .line 309
    iget-object v0, p0, Lpi;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpj;

    iget-boolean v0, v0, Lpj;->b:Z

    if-eqz v0, :cond_3

    .line 310
    add-int/lit8 v0, v2, -0x1

    .line 312
    :goto_1
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 315
    :goto_2
    return v0

    :cond_0
    invoke-virtual {p0, v3, v0}, Lpi;->a(II)I

    move-result v0

    goto :goto_2

    .line 305
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v2, v4

    goto :goto_0

    .line 321
    :cond_2
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0, p1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V

    throw v0

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 325
    invoke-virtual {p0}, Lpi;->e()V

    .line 327
    iget-object v1, p0, Lpi;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v4, :cond_4

    .line 328
    iget-object v0, p0, Lpi;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpj;

    iget v0, v0, Lpj;->e:I

    add-int v3, v1, v0

    .line 329
    if-lt p1, v1, :cond_3

    if-ge p1, v3, :cond_3

    .line 330
    sub-int v1, p1, v1

    .line 331
    iget-object v0, p0, Lpi;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpj;

    iget-boolean v0, v0, Lpj;->b:Z

    if-eqz v0, :cond_0

    .line 332
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    .line 335
    :cond_0
    const/4 v0, -0x1

    if-ne v1, v0, :cond_1

    .line 336
    iget-object v0, p0, Lpi;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpj;

    iget-object v0, v0, Lpj;->c:Landroid/database/Cursor;

    invoke-virtual {p0, v2, p2, p3}, Lpi;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 344
    :goto_1
    if-nez v0, :cond_5

    .line 345
    new-instance v0, Ljava/lang/NullPointerException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "View should not be null, partition: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " position: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 338
    :cond_1
    iget-object v0, p0, Lpi;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpj;

    iget-object v0, v0, Lpj;->c:Landroid/database/Cursor;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 339
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Couldn\'t move cursor to position "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 342
    :cond_2
    iget-object v0, p0, Lpi;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpj;

    iget-object v0, v0, Lpj;->c:Landroid/database/Cursor;

    invoke-virtual {p0, v2, v0, v1, p2}, Lpi;->a(ILandroid/database/Cursor;ILandroid/view/View;)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    .line 327
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v1, v3

    goto/16 :goto_0

    .line 353
    :cond_4
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0, p1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V

    throw v0

    .line 348
    :cond_5
    return-object v0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 281
    invoke-virtual {p0}, Lpi;->f()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public isEnabled(I)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 486
    invoke-virtual {p0}, Lpi;->e()V

    .line 488
    iget-object v0, p0, Lpi;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v2, v1

    move v3, v1

    :goto_0
    if-ge v2, v5, :cond_2

    .line 489
    iget-object v0, p0, Lpi;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpj;

    iget v0, v0, Lpj;->e:I

    add-int v4, v3, v0

    .line 490
    if-lt p1, v3, :cond_1

    if-ge p1, v4, :cond_1

    .line 491
    sub-int v3, p1, v3

    .line 492
    iget-object v0, p0, Lpi;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpj;

    iget-boolean v0, v0, Lpj;->b:Z

    if-eqz v0, :cond_0

    if-nez v3, :cond_0

    move v0, v1

    .line 501
    :goto_1
    return v0

    .line 495
    :cond_0
    const/4 v0, 0x1

    goto :goto_1

    .line 488
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v3, v4

    goto :goto_0

    :cond_2
    move v0, v1

    .line 501
    goto :goto_1
.end method

.method public notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 525
    iget-boolean v0, p0, Lpi;->e:Z

    if-eqz v0, :cond_0

    .line 526
    const/4 v0, 0x0

    iput-boolean v0, p0, Lpi;->f:Z

    .line 527
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 531
    :goto_0
    return-void

    .line 529
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lpi;->f:Z

    goto :goto_0
.end method
