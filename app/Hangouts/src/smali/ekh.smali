.class public final Lekh;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lekh;


# instance fields
.field public b:Lekg;

.field public c:Leki;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 427
    const/4 v0, 0x0

    new-array v0, v0, [Lekh;

    sput-object v0, Lekh;->a:[Lekh;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 428
    invoke-direct {p0}, Lepn;-><init>()V

    .line 431
    iput-object v0, p0, Lekh;->b:Lekg;

    .line 434
    iput-object v0, p0, Lekh;->c:Leki;

    .line 428
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 451
    const/4 v0, 0x0

    .line 452
    iget-object v1, p0, Lekh;->b:Lekg;

    if-eqz v1, :cond_0

    .line 453
    const/4 v0, 0x1

    iget-object v1, p0, Lekh;->b:Lekg;

    .line 454
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 456
    :cond_0
    iget-object v1, p0, Lekh;->c:Leki;

    if-eqz v1, :cond_1

    .line 457
    const/4 v1, 0x2

    iget-object v2, p0, Lekh;->c:Leki;

    .line 458
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 460
    :cond_1
    iget-object v1, p0, Lekh;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 461
    iput v0, p0, Lekh;->cachedSize:I

    .line 462
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 424
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lekh;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lekh;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lekh;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lekh;->b:Lekg;

    if-nez v0, :cond_2

    new-instance v0, Lekg;

    invoke-direct {v0}, Lekg;-><init>()V

    iput-object v0, p0, Lekh;->b:Lekg;

    :cond_2
    iget-object v0, p0, Lekh;->b:Lekg;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lekh;->c:Leki;

    if-nez v0, :cond_3

    new-instance v0, Leki;

    invoke-direct {v0}, Leki;-><init>()V

    iput-object v0, p0, Lekh;->c:Leki;

    :cond_3
    iget-object v0, p0, Lekh;->c:Leki;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 439
    iget-object v0, p0, Lekh;->b:Lekg;

    if-eqz v0, :cond_0

    .line 440
    const/4 v0, 0x1

    iget-object v1, p0, Lekh;->b:Lekg;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 442
    :cond_0
    iget-object v0, p0, Lekh;->c:Leki;

    if-eqz v0, :cond_1

    .line 443
    const/4 v0, 0x2

    iget-object v1, p0, Lekh;->c:Leki;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 445
    :cond_1
    iget-object v0, p0, Lekh;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 447
    return-void
.end method
