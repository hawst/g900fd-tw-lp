.class public final Lcvo;
.super Ljava/lang/Object;

# interfaces
.implements Ladw;
.implements Lcvp;


# instance fields
.field public final a:Lcvi;

.field public final b:Lyj;

.field public final c:Lbsb;

.field public d:Z

.field public e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcvi;Lyj;Lbsb;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcvo;->d:Z

    .line 47
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcvo;->e:Ljava/util/Map;

    .line 58
    iput-object p1, p0, Lcvo;->a:Lcvi;

    .line 59
    iput-object p2, p0, Lcvo;->b:Lyj;

    .line 60
    iput-object p3, p0, Lcvo;->c:Lbsb;

    .line 61
    return-void
.end method

.method private a(Lcvz;)V
    .locals 4

    .prologue
    .line 141
    if-eqz p1, :cond_0

    .line 142
    iget-object v0, p0, Lcvo;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 143
    invoke-virtual {p1}, Lcvz;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcvy;

    .line 144
    iget-object v2, p0, Lcvo;->e:Ljava/util/Map;

    invoke-interface {v0}, Lcvy;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, Lcvy;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 147
    :cond_0
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 90
    iget-object v0, p0, Lcvo;->a:Lcvi;

    invoke-virtual {v0}, Lcvi;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 91
    const-string v0, "Babel"

    const-string v1, "People client not connected. Skip loading circles"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    :goto_0
    return-void

    .line 95
    :cond_0
    iget-object v0, p0, Lcvo;->a:Lcvi;

    iget-object v1, p0, Lcvo;->b:Lyj;

    invoke-virtual {v1}, Lyj;->ak()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcvo;->b:Lyj;

    .line 96
    invoke-virtual {v2}, Lyj;->al()Ljava/lang/String;

    move-result-object v2

    .line 95
    invoke-virtual {v0, p0, v1, v2}, Lcvi;->a(Lcvo;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcvo;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public a()V
    .locals 4

    .prologue
    .line 67
    invoke-direct {p0}, Lcvo;->c()V

    .line 69
    iget-object v0, p0, Lcvo;->a:Lcvi;

    invoke-virtual {v0}, Lcvi;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcvo;->d:Z

    if-nez v0, :cond_0

    .line 70
    iget-object v0, p0, Lcvo;->a:Lcvi;

    iget-object v1, p0, Lcvo;->b:Lyj;

    invoke-virtual {v1}, Lyj;->ak()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcvo;->b:Lyj;

    .line 71
    invoke-virtual {v2}, Lyj;->al()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    .line 70
    invoke-virtual {v0, p0, v1, v2, v3}, Lcvi;->a(Lcvp;Ljava/lang/String;Ljava/lang/String;I)Z

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcvo;->d:Z

    .line 74
    :cond_0
    return-void
.end method

.method public a(Lcft;Lcvz;)V
    .locals 3

    .prologue
    .line 126
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Circle loaded: status="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " circles="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    :cond_0
    invoke-direct {p0, p2}, Lcvo;->a(Lcvz;)V

    .line 130
    invoke-virtual {p1}, Lcft;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 131
    iget-object v0, p0, Lcvo;->c:Lbsb;

    if-eqz v0, :cond_2

    .line 132
    iget-object v0, p0, Lcvo;->c:Lbsb;

    invoke-interface {v0, p2}, Lbsb;->a(Lcvz;)V

    .line 137
    :cond_1
    :goto_0
    return-void

    .line 134
    :cond_2
    invoke-virtual {p2}, Lchj;->b()V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 108
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Data changed. Account: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " gaiaId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " scopes: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    :cond_0
    iget-object v0, p0, Lcvo;->b:Lyj;

    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 114
    invoke-direct {p0}, Lcvo;->c()V

    .line 119
    :cond_1
    :goto_0
    return-void

    .line 115
    :cond_2
    iget-object v0, p0, Lcvo;->b:Lyj;

    invoke-virtual {v0}, Lyj;->ak()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcvo;->b:Lyj;

    .line 116
    invoke-virtual {v0}, Lyj;->al()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 117
    invoke-direct {p0}, Lcvo;->c()V

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcvo;->a:Lcvi;

    invoke-virtual {v0}, Lcvi;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcvo;->d:Z

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcvo;->a:Lcvi;

    invoke-virtual {v0, p0}, Lcvi;->a(Lcvp;)V

    .line 83
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcvo;->d:Z

    .line 85
    :cond_0
    return-void
.end method
