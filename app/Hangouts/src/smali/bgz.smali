.class public final Lbgz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:I

.field public d:Ljava/util/Calendar;

.field public e:Ljava/util/Calendar;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4894
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ldei;)Ljava/util/Calendar;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 4908
    if-nez p0, :cond_0

    .line 4909
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 4915
    :goto_0
    return-object v0

    .line 4911
    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 4912
    iget-object v1, p0, Ldei;->d:Ljava/lang/Integer;

    invoke-static {v1, v4}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v1

    iget-object v2, p0, Ldei;->c:Ljava/lang/Integer;

    .line 4913
    invoke-static {v2, v4}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v2

    iget-object v3, p0, Ldei;->b:Ljava/lang/Integer;

    .line 4914
    invoke-static {v3, v4}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v3

    .line 4912
    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Calendar;->set(III)V

    goto :goto_0
.end method
