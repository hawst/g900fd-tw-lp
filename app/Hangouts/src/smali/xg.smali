.class public final Lxg;
.super Lxj;
.source "PG"


# instance fields
.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 102
    invoke-direct {p0}, Lxj;-><init>()V

    .line 103
    return-void
.end method

.method private constructor <init>(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 114
    invoke-direct {p0}, Lxj;-><init>()V

    .line 115
    iput-object p1, p0, Lxg;->e:Ljava/lang/Object;

    .line 116
    iget-object v0, p0, Lxg;->c:[Lxh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lxg;->c:[Lxh;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Lxh;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p2}, Lxh;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lxg;->d:Les;

    invoke-virtual {v2, v1}, Les;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lxg;->d:Les;

    invoke-virtual {v1, p2, v0}, Les;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iput-object p2, p0, Lxg;->f:Ljava/lang/String;

    iput-boolean v3, p0, Lxg;->b:Z

    .line 117
    return-void
.end method

.method public static varargs a(Ljava/lang/Object;Ljava/lang/String;[F)Lxg;
    .locals 1

    .prologue
    .line 154
    new-instance v0, Lxg;

    invoke-direct {v0, p0, p1}, Lxg;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    .line 155
    invoke-virtual {v0, p2}, Lxg;->a([F)V

    .line 156
    return-object v0
.end method

.method public static varargs a(Ljava/lang/Object;Ljava/lang/String;[I)Lxg;
    .locals 1

    .prologue
    .line 134
    new-instance v0, Lxg;

    invoke-direct {v0, p0, p1}, Lxg;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    .line 135
    invoke-virtual {v0, p2}, Lxg;->a([I)V

    .line 136
    return-object v0
.end method

.method public static varargs a(Ljava/lang/Object;[Lxh;)Lxg;
    .locals 1

    .prologue
    .line 238
    new-instance v0, Lxg;

    invoke-direct {v0}, Lxg;-><init>()V

    .line 239
    iput-object p0, v0, Lxg;->e:Ljava/lang/Object;

    .line 240
    invoke-virtual {v0, p1}, Lxg;->a([Lxh;)V

    .line 241
    return-object v0
.end method


# virtual methods
.method a(F)V
    .locals 4

    .prologue
    .line 390
    invoke-super {p0, p1}, Lxj;->a(F)V

    .line 391
    iget-object v0, p0, Lxg;->c:[Lxh;

    array-length v1, v0

    .line 392
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 393
    iget-object v2, p0, Lxg;->c:[Lxh;

    aget-object v2, v2, v0

    iget-object v3, p0, Lxg;->e:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Lxh;->b(Ljava/lang/Object;)V

    .line 392
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 395
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 353
    iput-object p1, p0, Lxg;->e:Ljava/lang/Object;

    .line 355
    const/4 v0, 0x0

    iput-boolean v0, p0, Lxg;->b:Z

    .line 356
    return-void
.end method

.method public varargs a([F)V
    .locals 3

    .prologue
    .line 257
    iget-object v0, p0, Lxg;->c:[Lxh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lxg;->c:[Lxh;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 260
    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Lxh;

    const/4 v1, 0x0

    iget-object v2, p0, Lxg;->f:Ljava/lang/String;

    invoke-static {v2, p1}, Lxh;->a(Ljava/lang/String;[F)Lxh;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lxg;->a([Lxh;)V

    .line 264
    :goto_0
    return-void

    .line 262
    :cond_1
    invoke-super {p0, p1}, Lxj;->a([F)V

    goto :goto_0
.end method

.method public varargs a([I)V
    .locals 3

    .prologue
    .line 246
    iget-object v0, p0, Lxg;->c:[Lxh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lxg;->c:[Lxh;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 249
    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Lxh;

    const/4 v1, 0x0

    iget-object v2, p0, Lxg;->f:Ljava/lang/String;

    invoke-static {v2, p1}, Lxh;->a(Ljava/lang/String;[I)Lxh;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lxg;->a([Lxh;)V

    .line 253
    :goto_0
    return-void

    .line 251
    :cond_1
    invoke-super {p0, p1}, Lxj;->a([I)V

    goto :goto_0
.end method

.method public b(J)Lxg;
    .locals 0

    .prologue
    .line 332
    invoke-super {p0, p1, p2}, Lxj;->c(J)Lxj;

    .line 333
    return-object p0
.end method

.method public synthetic c(J)Lxj;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0, p1, p2}, Lxg;->b(J)Lxg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lxg;->g()Lxg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e()Lwz;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lxg;->g()Lxg;

    move-result-object v0

    return-object v0
.end method

.method f()V
    .locals 4

    .prologue
    .line 311
    iget-boolean v0, p0, Lxg;->b:Z

    if-nez v0, :cond_1

    .line 314
    iget-object v0, p0, Lxg;->c:[Lxh;

    array-length v1, v0

    .line 315
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 316
    iget-object v2, p0, Lxg;->c:[Lxh;

    aget-object v2, v2, v0

    iget-object v3, p0, Lxg;->e:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Lxh;->a(Ljava/lang/Object;)V

    .line 315
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 318
    :cond_0
    invoke-super {p0}, Lxj;->f()V

    .line 320
    :cond_1
    return-void
.end method

.method public g()Lxg;
    .locals 1

    .prologue
    .line 399
    invoke-super {p0}, Lxj;->h()Lxj;

    move-result-object v0

    check-cast v0, Lxg;

    .line 400
    return-object v0
.end method

.method public synthetic h()Lxj;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lxg;->g()Lxg;

    move-result-object v0

    return-object v0
.end method
