.class public final Lbtc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcvw;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable",
            "<",
            "Lcwb;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable",
            "<",
            "Lcwg;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/lang/String;

.field private final f:Z

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcvw;)V
    .locals 4

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    invoke-interface {p1}, Lcvw;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbtc;->a:Ljava/lang/String;

    .line 87
    invoke-interface {p1}, Lcvw;->b()Ljava/lang/Iterable;

    move-result-object v0

    iput-object v0, p0, Lbtc;->b:Ljava/lang/Iterable;

    .line 88
    invoke-interface {p1}, Lcvw;->c()Ljava/lang/Iterable;

    move-result-object v0

    iput-object v0, p0, Lbtc;->c:Ljava/lang/Iterable;

    .line 89
    invoke-interface {p1}, Lcvw;->d()Ljava/lang/Iterable;

    move-result-object v0

    iput-object v0, p0, Lbtc;->d:Ljava/lang/Iterable;

    .line 90
    invoke-interface {p1}, Lcvw;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbtc;->e:Ljava/lang/String;

    .line 91
    invoke-interface {p1}, Lcvw;->f()Z

    move-result v0

    iput-boolean v0, p0, Lbtc;->f:Z

    .line 92
    invoke-interface {p1}, Lcvw;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbtc;->g:Ljava/lang/String;

    .line 93
    const/4 v0, 0x0

    .line 95
    :try_start_0
    invoke-interface {p1}, Lcvw;->h()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 99
    :goto_0
    iput-object v0, p0, Lbtc;->h:Ljava/lang/String;

    .line 100
    invoke-interface {p1}, Lcvw;->i()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbtc;->i:[Ljava/lang/String;

    .line 101
    return-void

    .line 96
    :catch_0
    move-exception v1

    .line 97
    const-string v2, "Babel"

    const-string v3, "Caught NPE in getAvatarUrl"

    invoke-static {v2, v3, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static a(Ljava/security/MessageDigest;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 132
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 133
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/security/MessageDigest;->update([B)V

    .line 135
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lbtc;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(Ljava/security/MessageDigest;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 104
    invoke-virtual {p0}, Lbtc;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lbtc;->a(Ljava/security/MessageDigest;Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lbtc;->b:Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 107
    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v3

    invoke-static {v3, v4}, Lbtb;->a(J)[B

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/security/MessageDigest;->update([B)V

    goto :goto_0

    .line 110
    :cond_0
    invoke-virtual {p0}, Lbtc;->c()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcwb;

    .line 111
    invoke-interface {v0}, Lcwb;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Lbtc;->a(Ljava/security/MessageDigest;Ljava/lang/String;)V

    .line 112
    invoke-interface {v0}, Lcwb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lbtc;->a(Ljava/security/MessageDigest;Ljava/lang/String;)V

    goto :goto_1

    .line 115
    :cond_1
    invoke-virtual {p0}, Lbtc;->d()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcwg;

    .line 116
    invoke-interface {v0}, Lcwg;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Lbtc;->a(Ljava/security/MessageDigest;Ljava/lang/String;)V

    .line 117
    invoke-interface {v0}, Lcwg;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lbtc;->a(Ljava/security/MessageDigest;Ljava/lang/String;)V

    goto :goto_2

    .line 120
    :cond_2
    iget-object v0, p0, Lbtc;->e:Ljava/lang/String;

    invoke-static {p1, v0}, Lbtc;->a(Ljava/security/MessageDigest;Ljava/lang/String;)V

    .line 122
    iget-boolean v0, p0, Lbtc;->f:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_3
    invoke-virtual {p1, v0}, Ljava/security/MessageDigest;->update(B)V

    .line 123
    iget-object v0, p0, Lbtc;->g:Ljava/lang/String;

    invoke-static {p1, v0}, Lbtc;->a(Ljava/security/MessageDigest;Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lbtc;->h:Ljava/lang/String;

    invoke-static {p1, v0}, Lbtc;->a(Ljava/security/MessageDigest;Ljava/lang/String;)V

    .line 126
    iget-object v0, p0, Lbtc;->i:[Ljava/lang/String;

    array-length v2, v0

    :goto_4
    if-ge v1, v2, :cond_4

    aget-object v3, v0, v1

    .line 127
    invoke-static {p1, v3}, Lbtc;->a(Ljava/security/MessageDigest;Ljava/lang/String;)V

    .line 126
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_3
    move v0, v1

    .line 122
    goto :goto_3

    .line 129
    :cond_4
    return-void
.end method

.method public b()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 144
    iget-object v0, p0, Lbtc;->b:Ljava/lang/Iterable;

    return-object v0
.end method

.method public c()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Lcwb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 149
    iget-object v0, p0, Lbtc;->c:Ljava/lang/Iterable;

    return-object v0
.end method

.method public d()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Lcwg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 154
    iget-object v0, p0, Lbtc;->d:Ljava/lang/Iterable;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lbtc;->e:Ljava/lang/String;

    return-object v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 164
    iget-boolean v0, p0, Lbtc;->f:Z

    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lbtc;->g:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lbtc;->h:Ljava/lang/String;

    return-object v0
.end method

.method public i()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lbtc;->i:[Ljava/lang/String;

    return-object v0
.end method
