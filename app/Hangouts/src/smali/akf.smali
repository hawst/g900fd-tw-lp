.class public final Lakf;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static c:I

.field private static final d:Ljava/lang/Object;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private e:Laki;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 450
    const/16 v0, 0x10

    sput v0, Lakf;->c:I

    .line 451
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lakf;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 493
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 494
    iput-object p1, p0, Lakf;->a:Landroid/content/Context;

    .line 495
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lakf;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 496
    new-instance v0, Lakh;

    invoke-direct {v0, p0, v1}, Lakh;-><init>(Lakf;B)V

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lakh;->executeOnThreadPool([Ljava/lang/Object;)Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;

    .line 497
    return-void
.end method

.method static synthetic a(Lakf;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 446
    iget-object v0, p0, Lakf;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b()I
    .locals 1

    .prologue
    .line 446
    sget v0, Lakf;->c:I

    return v0
.end method

.method static synthetic b(Lakf;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    .prologue
    .line 446
    iget-object v0, p0, Lakf;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method static synthetic c(Lakf;)Laki;
    .locals 1

    .prologue
    .line 446
    iget-object v0, p0, Lakf;->e:Laki;

    return-object v0
.end method

.method static synthetic c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 446
    sget-object v0, Lakf;->d:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 551
    iget-object v0, p0, Lakf;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method public a(I)V
    .locals 3

    .prologue
    .line 541
    :cond_0
    iget-object v0, p0, Lakf;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 543
    iget-object v0, p0, Lakf;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    const/4 v1, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(ILjava/lang/Object;)V

    .line 544
    iget-object v0, p0, Lakf;->e:Laki;

    if-eqz v0, :cond_1

    .line 545
    iget-object v0, p0, Lakf;->e:Laki;

    invoke-interface {v0}, Laki;->j_()V

    .line 547
    :cond_1
    new-instance v0, Lakg;

    invoke-direct {v0, p0}, Lakg;-><init>(Lakf;)V

    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;->executeOnThreadPool(Ljava/lang/Runnable;)V

    .line 548
    return-void
.end method

.method public a(Laki;)V
    .locals 0

    .prologue
    .line 532
    iput-object p1, p0, Lakf;->e:Laki;

    .line 533
    return-void
.end method
