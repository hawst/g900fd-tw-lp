.class public final Ladn;
.super Ls;
.source "PG"


# instance fields
.field private Y:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Laeh;",
            ">;"
        }
    .end annotation
.end field

.field private Z:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Laeh;",
            ">;"
        }
    .end annotation
.end field

.field private aa:Ladq;

.field private ab:Ljava/lang/String;

.field private ac:Ljava/lang/String;

.field private ad:Landroid/view/View;

.field private ae:Landroid/view/ViewGroup;

.field private af:Lads;

.field private ag:Landroid/support/v4/view/ViewPager;

.field private final ah:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ls;-><init>()V

    .line 143
    new-instance v0, Lado;

    invoke-direct {v0, p0}, Lado;-><init>(Ladn;)V

    iput-object v0, p0, Ladn;->ah:Landroid/view/View$OnClickListener;

    .line 266
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)Ladn;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Laeh;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Laeh;",
            ">;)",
            "Ladn;"
        }
    .end annotation

    .prologue
    .line 90
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_0

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 92
    new-instance v0, Ladn;

    invoke-direct {v0}, Ladn;-><init>()V

    .line 95
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 96
    const-string v2, "display_name"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 97
    const-string v2, "avatar_url"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 98
    const-string v2, "data_options"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 99
    const-string v2, "pstn_options"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 100
    invoke-virtual {v0, v1}, Ladn;->setArguments(Landroid/os/Bundle;)V

    .line 101
    return-object v0

    .line 90
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Ladn;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Ladn;->ae:Landroid/view/ViewGroup;

    return-object v0
.end method

.method private a(ILandroid/view/ViewGroup;)V
    .locals 4

    .prologue
    .line 167
    invoke-virtual {p0}, Ladn;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 168
    sget v1, Lf;->eu:I

    iget-object v2, p0, Ladn;->ae:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 170
    if-nez p1, :cond_0

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->bG:I

    .line 171
    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 172
    iget-object v1, p0, Ladn;->ah:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 173
    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 174
    return-void

    .line 170
    :cond_0
    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->aS:I

    goto :goto_0
.end method

.method static synthetic b(Ladn;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Ladn;->ag:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic c(Ladn;)Z
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ladn;->q()Z

    move-result v0

    return v0
.end method

.method static synthetic d(Ladn;)Z
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ladn;->r()Z

    move-result v0

    return v0
.end method

.method static synthetic e(Ladn;)Ljava/util/List;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Ladn;->Z:Ljava/util/List;

    return-object v0
.end method

.method static synthetic f(Ladn;)Ljava/util/List;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Ladn;->Y:Ljava/util/List;

    return-object v0
.end method

.method static synthetic g(Ladn;)Ladq;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Ladn;->aa:Ladq;

    return-object v0
.end method

.method static synthetic h(Ladn;)Landroid/view/View;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Ladn;->ad:Landroid/view/View;

    return-object v0
.end method

.method private q()Z
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Ladn;->Z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private r()Z
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Ladn;->Y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public f()Landroid/app/Dialog;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 110
    invoke-virtual {p0}, Ladn;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "data_options"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Ladn;->Z:Ljava/util/List;

    invoke-virtual {p0}, Ladn;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "pstn_options"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Ladn;->Y:Ljava/util/List;

    invoke-virtual {p0}, Ladn;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "display_name"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ladn;->ac:Ljava/lang/String;

    invoke-virtual {p0}, Ladn;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "avatar_url"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ladn;->ab:Ljava/lang/String;

    .line 111
    invoke-virtual {p0}, Ladn;->getTargetFragment()Lt;

    move-result-object v0

    check-cast v0, Ladq;

    iput-object v0, p0, Ladn;->aa:Ladq;

    .line 113
    invoke-virtual {p0}, Ladn;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 114
    sget v3, Lf;->et:I

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 116
    sget v0, Lg;->E:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/AvatarView;

    .line 117
    iget-object v4, p0, Ladn;->ab:Ljava/lang/String;

    invoke-static {}, Lbkb;->j()Lyj;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lcom/google/android/apps/hangouts/views/AvatarView;->a(Ljava/lang/String;Lyj;)V

    .line 118
    sget v0, Lg;->aD:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 119
    iget-object v4, p0, Ladn;->ac:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    sget v0, Lg;->gA:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ladn;->ad:Landroid/view/View;

    .line 122
    sget v0, Lg;->hL:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Ladn;->ae:Landroid/view/ViewGroup;

    .line 123
    invoke-direct {p0}, Ladn;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Ladn;->ae:Landroid/view/ViewGroup;

    invoke-direct {p0, v2, v0}, Ladn;->a(ILandroid/view/ViewGroup;)V

    .line 126
    :cond_0
    invoke-direct {p0}, Ladn;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 127
    iget-object v0, p0, Ladn;->ae:Landroid/view/ViewGroup;

    invoke-direct {p0, v1, v0}, Ladn;->a(ILandroid/view/ViewGroup;)V

    .line 129
    :cond_1
    iget-object v0, p0, Ladn;->ae:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-le v0, v1, :cond_2

    .line 130
    :goto_0
    sget v0, Lg;->hl:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 131
    if-eqz v1, :cond_3

    move v1, v2

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 133
    sget v0, Lg;->aG:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Ladn;->ag:Landroid/support/v4/view/ViewPager;

    .line 134
    new-instance v0, Lads;

    invoke-direct {v0, p0}, Lads;-><init>(Ladn;)V

    iput-object v0, p0, Ladn;->af:Lads;

    .line 135
    iget-object v0, p0, Ladn;->ag:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Ladn;->af:Lads;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->a(Lgq;)V

    .line 136
    iget-object v0, p0, Ladn;->ag:Landroid/support/v4/view/ViewPager;

    new-instance v1, Ladr;

    invoke-direct {v1, p0, v2}, Ladr;-><init>(Ladn;B)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->a(Lhz;)V

    .line 138
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Ladn;->getActivity()Ly;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 139
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 140
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    :cond_2
    move v1, v2

    .line 129
    goto :goto_0

    .line 131
    :cond_3
    const/16 v1, 0x8

    goto :goto_1
.end method
