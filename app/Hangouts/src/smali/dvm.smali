.class public final Ldvm;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldvm;


# instance fields
.field public b:Ldpv;

.field public c:Ldps;

.field public d:Ldpt;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/Boolean;

.field public g:Ljava/lang/Integer;

.field public h:Lesb;

.field public i:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 712
    const/4 v0, 0x0

    new-array v0, v0, [Ldvm;

    sput-object v0, Ldvm;->a:[Ldvm;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 713
    invoke-direct {p0}, Lepn;-><init>()V

    .line 716
    iput-object v0, p0, Ldvm;->b:Ldpv;

    .line 719
    iput-object v0, p0, Ldvm;->c:Ldps;

    .line 722
    iput-object v0, p0, Ldvm;->d:Ldpt;

    .line 731
    iput-object v0, p0, Ldvm;->h:Lesb;

    .line 713
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 768
    const/4 v0, 0x0

    .line 769
    iget-object v1, p0, Ldvm;->b:Ldpv;

    if-eqz v1, :cond_0

    .line 770
    const/4 v0, 0x1

    iget-object v1, p0, Ldvm;->b:Ldpv;

    .line 771
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 773
    :cond_0
    iget-object v1, p0, Ldvm;->c:Ldps;

    if-eqz v1, :cond_1

    .line 774
    const/4 v1, 0x2

    iget-object v2, p0, Ldvm;->c:Ldps;

    .line 775
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 777
    :cond_1
    iget-object v1, p0, Ldvm;->d:Ldpt;

    if-eqz v1, :cond_2

    .line 778
    const/4 v1, 0x3

    iget-object v2, p0, Ldvm;->d:Ldpt;

    .line 779
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 781
    :cond_2
    iget-object v1, p0, Ldvm;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 782
    const/4 v1, 0x4

    iget-object v2, p0, Ldvm;->e:Ljava/lang/String;

    .line 783
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 785
    :cond_3
    iget-object v1, p0, Ldvm;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 786
    const/4 v1, 0x5

    iget-object v2, p0, Ldvm;->f:Ljava/lang/Boolean;

    .line 787
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 789
    :cond_4
    iget-object v1, p0, Ldvm;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 790
    const/4 v1, 0x6

    iget-object v2, p0, Ldvm;->g:Ljava/lang/Integer;

    .line 791
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 793
    :cond_5
    iget-object v1, p0, Ldvm;->h:Lesb;

    if-eqz v1, :cond_6

    .line 794
    const/4 v1, 0x7

    iget-object v2, p0, Ldvm;->h:Lesb;

    .line 795
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 797
    :cond_6
    iget-object v1, p0, Ldvm;->i:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 798
    const/16 v1, 0x8

    iget-object v2, p0, Ldvm;->i:Ljava/lang/String;

    .line 799
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 801
    :cond_7
    iget-object v1, p0, Ldvm;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 802
    iput v0, p0, Ldvm;->cachedSize:I

    .line 803
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 709
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldvm;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldvm;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldvm;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldvm;->b:Ldpv;

    if-nez v0, :cond_2

    new-instance v0, Ldpv;

    invoke-direct {v0}, Ldpv;-><init>()V

    iput-object v0, p0, Ldvm;->b:Ldpv;

    :cond_2
    iget-object v0, p0, Ldvm;->b:Ldpv;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldvm;->c:Ldps;

    if-nez v0, :cond_3

    new-instance v0, Ldps;

    invoke-direct {v0}, Ldps;-><init>()V

    iput-object v0, p0, Ldvm;->c:Ldps;

    :cond_3
    iget-object v0, p0, Ldvm;->c:Ldps;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldvm;->d:Ldpt;

    if-nez v0, :cond_4

    new-instance v0, Ldpt;

    invoke-direct {v0}, Ldpt;-><init>()V

    iput-object v0, p0, Ldvm;->d:Ldpt;

    :cond_4
    iget-object v0, p0, Ldvm;->d:Ldpt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldvm;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldvm;->f:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->l()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldvm;->g:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Ldvm;->h:Lesb;

    if-nez v0, :cond_5

    new-instance v0, Lesb;

    invoke-direct {v0}, Lesb;-><init>()V

    iput-object v0, p0, Ldvm;->h:Lesb;

    :cond_5
    iget-object v0, p0, Ldvm;->h:Lesb;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldvm;->i:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 738
    iget-object v0, p0, Ldvm;->b:Ldpv;

    if-eqz v0, :cond_0

    .line 739
    const/4 v0, 0x1

    iget-object v1, p0, Ldvm;->b:Ldpv;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 741
    :cond_0
    iget-object v0, p0, Ldvm;->c:Ldps;

    if-eqz v0, :cond_1

    .line 742
    const/4 v0, 0x2

    iget-object v1, p0, Ldvm;->c:Ldps;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 744
    :cond_1
    iget-object v0, p0, Ldvm;->d:Ldpt;

    if-eqz v0, :cond_2

    .line 745
    const/4 v0, 0x3

    iget-object v1, p0, Ldvm;->d:Ldpt;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 747
    :cond_2
    iget-object v0, p0, Ldvm;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 748
    const/4 v0, 0x4

    iget-object v1, p0, Ldvm;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 750
    :cond_3
    iget-object v0, p0, Ldvm;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 751
    const/4 v0, 0x5

    iget-object v1, p0, Ldvm;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 753
    :cond_4
    iget-object v0, p0, Ldvm;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 754
    const/4 v0, 0x6

    iget-object v1, p0, Ldvm;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->c(II)V

    .line 756
    :cond_5
    iget-object v0, p0, Ldvm;->h:Lesb;

    if-eqz v0, :cond_6

    .line 757
    const/4 v0, 0x7

    iget-object v1, p0, Ldvm;->h:Lesb;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 759
    :cond_6
    iget-object v0, p0, Ldvm;->i:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 760
    const/16 v0, 0x8

    iget-object v1, p0, Ldvm;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 762
    :cond_7
    iget-object v0, p0, Ldvm;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 764
    return-void
.end method
