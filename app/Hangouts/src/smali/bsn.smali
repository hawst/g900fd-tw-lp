.class public final Lbsn;
.super Lbrr;
.source "PG"


# static fields
.field private static final c:Z

.field private static d:Lcxa;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcxa",
            "<",
            "Lbsw;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Ljava/lang/Object;

.field private static volatile f:Lbsn;

.field private static final g:Ljava/lang/Object;


# instance fields
.field final b:Ljava/lang/Object;

.field private final h:Ljava/util/concurrent/ExecutorService;

.field private final i:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lbsv;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lbsv;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lbys;->l:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lbsn;->c:Z

    .line 212
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lbsn;->e:Ljava/lang/Object;

    .line 215
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lbsn;->g:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lbrr;-><init>()V

    .line 221
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lbsn;->h:Ljava/util/concurrent/ExecutorService;

    .line 254
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lbsn;->i:Ljava/util/HashSet;

    .line 255
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lbsn;->j:Ljava/util/LinkedList;

    .line 256
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbsn;->b:Ljava/lang/Object;

    .line 670
    return-void
.end method

.method private static a(JJ)I
    .locals 4

    .prologue
    const-wide/16 v1, 0x0

    .line 793
    cmp-long v0, p0, v1

    if-lez v0, :cond_0

    cmp-long v0, p2, v1

    if-lez v0, :cond_0

    sub-long v0, p0, p2

    const-wide/32 v2, 0xf4240

    div-long/2addr v0, v2

    long-to-int v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method static synthetic a(Lbsm;Lbsv;)V
    .locals 5

    .prologue
    .line 49
    invoke-static {}, Lcwz;->b()V

    sget-object v0, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    invoke-static {v0}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {p0}, Lbsm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbsn;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v0, p1, Lbsv;->d:Ljava/lang/String;

    invoke-static {v0}, Lbsn;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {p1}, Lbsn;->a(Lbsv;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {v1, v0}, Lbyr;->a(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {p1}, Lbsn;->a(Lbsv;)V

    const-string v0, "Babel_medialoader"

    const-string v1, "could not create save name"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    move-result v2

    if-eqz v2, :cond_a

    :cond_3
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v1, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0}, Lbsm;->a()[B

    move-result-object v0

    const/4 v2, 0x0

    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {v1, v0}, Ljava/io/OutputStream;->write([B)V

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    sget-boolean v0, Lbsn;->c:Z

    if-eqz v0, :cond_4

    sget-object v0, Lbsn;->f:Lbsn;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Successful save "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lbsv;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lbsn;->f(Ljava/lang/String;)V

    :cond_4
    invoke-static {}, Lbsn;->c()Lbsn;

    move-result-object v0

    invoke-direct {v0, p1}, Lbsn;->d(Lbsv;)V

    sget-object v0, Lbsn;->f:Lbsn;

    invoke-virtual {p1}, Lbsv;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lbsn;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_6

    sget-boolean v0, Lbsn;->c:Z

    if-eqz v0, :cond_5

    const-string v0, "Babel_medialoader"

    const-string v2, "null requests"

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_5
    :try_start_2
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Babel_medialoader"

    const-string v2, "exception closing file while trying to save"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    :cond_6
    :try_start_3
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbrv;

    instance-of v3, v0, Lbsv;

    invoke-static {v3}, Lcwz;->a(Z)V

    check-cast v0, Lbsv;

    iget-boolean v3, v0, Lbsv;->a:Z

    if-eqz v3, :cond_7

    sget-object v3, Lbsn;->f:Lbsn;

    new-instance v4, Lbsq;

    invoke-direct {v4, v0}, Lbsq;-><init>(Lbsv;)V

    invoke-virtual {v3, v4}, Lbsn;->a(Ljava/lang/Runnable;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catch_1
    move-exception v0

    :goto_2
    :try_start_4
    invoke-static {p1}, Lbsn;->a(Lbsv;)V

    const-string v2, "Babel_medialoader"

    const-string v3, "exception writing bytes to file while trying to save"

    invoke-static {v2, v3, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v1, :cond_0

    :try_start_5
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_0

    :catch_2
    move-exception v0

    const-string v1, "Babel_medialoader"

    const-string v2, "exception closing file while trying to save"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    :cond_7
    :try_start_6
    invoke-virtual {v0}, Lbsv;->h()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v1, :cond_8

    :try_start_7
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    :cond_8
    :goto_4
    throw v0

    :cond_9
    :try_start_8
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    goto/16 :goto_0

    :catch_3
    move-exception v0

    const-string v1, "Babel_medialoader"

    const-string v2, "exception closing file while trying to save"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    :catch_4
    move-exception v1

    const-string v2, "Babel_medialoader"

    const-string v3, "exception closing file while trying to save"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    :cond_a
    const-string v0, "Babel_medialoader"

    const-string v1, "could not create save directory."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_3

    :catch_5
    move-exception v0

    move-object v1, v2

    goto :goto_2
.end method

.method static synthetic a(Lbsn;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lbsn;->f(Ljava/lang/String;)V

    return-void
.end method

.method public static a(Lbsv;)V
    .locals 3

    .prologue
    .line 629
    invoke-static {}, Lcwz;->b()V

    .line 630
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Media failed to load:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbsv;->m()Lbyu;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 631
    const-string v0, " Failed"

    invoke-static {p0, v0}, Lbsn;->a(Lbsv;Ljava/lang/String;)V

    .line 632
    invoke-static {}, Lbsn;->c()Lbsn;

    move-result-object v0

    invoke-direct {v0, p0}, Lbsn;->d(Lbsv;)V

    .line 633
    sget-object v0, Lbsn;->f:Lbsn;

    invoke-virtual {p0}, Lbsv;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsn;->c(Ljava/lang/String;)Ljava/util/List;

    .line 634
    iget-boolean v0, p0, Lbsv;->a:Z

    if-eqz v0, :cond_0

    .line 635
    sget-object v0, Lbsn;->f:Lbsn;

    new-instance v1, Lbss;

    invoke-direct {v1, p0}, Lbss;-><init>(Lbsv;)V

    invoke-virtual {v0, v1}, Lbsn;->a(Ljava/lang/Runnable;)V

    .line 644
    :goto_0
    return-void

    .line 642
    :cond_0
    invoke-virtual {p0}, Lbsv;->g()V

    goto :goto_0
.end method

.method public static a(Lbsv;Lbsm;)V
    .locals 5

    .prologue
    .line 328
    invoke-virtual {p1}, Lbsm;->a()[B

    move-result-object v1

    .line 329
    sget-boolean v0, Lbsn;->c:Z

    if-eqz v0, :cond_0

    .line 330
    sget-object v2, Lbsn;->f:Lbsn;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "mediaDownloaded "

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p1, :cond_3

    const-string v0, " null "

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 332
    invoke-virtual {p0}, Lbsv;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 330
    invoke-direct {v2, v0}, Lbsn;->f(Ljava/lang/String;)V

    .line 334
    :cond_0
    iget-object v0, p0, Lbsv;->e:Lbsw;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbsv;->e:Lbsw;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    iput-wide v2, v0, Lbsw;->e:J

    .line 335
    :cond_1
    invoke-virtual {p1}, Lbsm;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 336
    const-string v0, " Volley Cache Hit"

    invoke-static {p0, v0}, Lbsn;->a(Lbsv;Ljava/lang/String;)V

    .line 338
    :cond_2
    if-eqz v1, :cond_5

    array-length v0, v1

    if-lez v0, :cond_5

    .line 339
    invoke-virtual {p0}, Lbsv;->m()Lbyu;

    move-result-object v0

    invoke-virtual {v0}, Lbyu;->n()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 340
    sget-object v0, Lbsn;->f:Lbsn;

    iget-object v0, v0, Lbsn;->h:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lbso;

    invoke-direct {v1, p0, p1}, Lbso;-><init>(Lbsv;Lbsm;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 384
    :goto_1
    return-void

    .line 330
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, " size="

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v4, v1

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 357
    :cond_4
    sget-object v0, Lbsn;->f:Lbsn;

    iget-object v0, v0, Lbsn;->h:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lbsp;

    invoke-direct {v2, p0, v1}, Lbsp;-><init>(Lbsv;[B)V

    invoke-interface {v0, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_1

    .line 382
    :cond_5
    invoke-static {p0}, Lbsn;->b(Lbsv;)V

    goto :goto_1
.end method

.method public static a(Lbsv;Lbsu;)V
    .locals 5

    .prologue
    .line 532
    invoke-static {}, Lcwz;->b()V

    .line 534
    sget-boolean v0, Lbsn;->c:Z

    if-eqz v0, :cond_0

    .line 535
    sget-object v0, Lbsn;->f:Lbsn;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mediaDecoded "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbsv;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbsn;->f(Ljava/lang/String;)V

    .line 540
    :cond_0
    invoke-static {}, Lbsn;->c()Lbsn;

    move-result-object v0

    invoke-direct {v0, p0}, Lbsn;->d(Lbsv;)V

    .line 542
    sget-object v0, Lbsn;->f:Lbsn;

    invoke-virtual {p0}, Lbsv;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsn;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 543
    if-nez v0, :cond_3

    .line 544
    sget-boolean v0, Lbsn;->c:Z

    if-eqz v0, :cond_1

    .line 545
    const-string v0, "Babel_medialoader"

    const-string v1, "null requests"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 547
    :cond_1
    if-eqz p1, :cond_2

    .line 548
    invoke-interface {p1}, Lbsu;->b()V

    .line 573
    :cond_2
    :goto_0
    return-void

    .line 552
    :cond_3
    if-eqz p1, :cond_2

    .line 553
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbrv;

    .line 554
    instance-of v2, v0, Lbsv;

    invoke-static {v2}, Lcwz;->a(Z)V

    .line 555
    invoke-interface {p1}, Lbsu;->a()V

    .line 556
    check-cast v0, Lbsv;

    .line 557
    iget-object v2, v0, Lbsv;->e:Lbsw;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lbsv;->e:Lbsw;

    if-eqz v2, :cond_5

    if-eq v0, p0, :cond_4

    iget-object v2, v0, Lbsv;->e:Lbsw;

    iget-object v3, p0, Lbsv;->e:Lbsw;

    iget-wide v3, v3, Lbsw;->c:J

    iput-wide v3, v2, Lbsw;->c:J

    iget-object v2, v0, Lbsv;->e:Lbsw;

    iget-object v3, p0, Lbsv;->e:Lbsw;

    iget-wide v3, v3, Lbsw;->d:J

    iput-wide v3, v2, Lbsw;->d:J

    iget-object v2, v0, Lbsv;->e:Lbsw;

    iget-object v3, p0, Lbsv;->e:Lbsw;

    iget-wide v3, v3, Lbsw;->e:J

    iput-wide v3, v2, Lbsw;->e:J

    const-string v2, " Secondary request"

    invoke-static {v0, v2}, Lbsn;->a(Lbsv;Ljava/lang/String;)V

    :cond_4
    iget-object v2, v0, Lbsv;->e:Lbsw;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v3

    iput-wide v3, v2, Lbsw;->f:J

    .line 558
    :cond_5
    iget-boolean v2, v0, Lbsv;->a:Z

    if-eqz v2, :cond_6

    .line 559
    sget-object v2, Lbsn;->f:Lbsn;

    new-instance v3, Lbsr;

    invoke-direct {v3, v0, p1}, Lbsr;-><init>(Lbsv;Lbsu;)V

    invoke-virtual {v2, v3}, Lbsn;->a(Ljava/lang/Runnable;)V

    goto :goto_1

    .line 567
    :cond_6
    invoke-virtual {v0, p1}, Lbsv;->a(Lbsu;)V

    .line 568
    invoke-interface {p1}, Lbsu;->b()V

    goto :goto_1

    .line 571
    :cond_7
    invoke-interface {p1}, Lbsu;->b()V

    goto :goto_0
.end method

.method private static a(Lbsv;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 749
    iget-object v0, p0, Lbsv;->e:Lbsw;

    if-eqz v0, :cond_0

    .line 750
    iget-object v0, p0, Lbsv;->e:Lbsw;

    iget-object v0, v0, Lbsw;->h:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 751
    iget-object v0, p0, Lbsv;->e:Lbsw;

    iput-object p1, v0, Lbsw;->h:Ljava/lang/String;

    .line 756
    :cond_0
    :goto_0
    return-void

    .line 753
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lbsv;->e:Lbsw;

    iget-object v2, v1, Lbsw;->h:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lbsw;->h:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(Ljava/io/PrintWriter;)V
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 798
    sget-object v2, Lbsn;->e:Ljava/lang/Object;

    monitor-enter v2

    .line 799
    :try_start_0
    sget-object v1, Lbsn;->d:Lcxa;

    if-nez v1, :cond_0

    .line 800
    monitor-exit v2

    .line 815
    :goto_0
    return-void

    .line 802
    :cond_0
    const-string v1, "loadTime              w-e   a-e   dec-a del-dec del-e key extras"

    invoke-virtual {p0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 803
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v1, "MM-dd HH:mm:ss.SSS"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v3, v1, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    move v1, v0

    .line 804
    :goto_1
    sget-object v0, Lbsn;->d:Lcxa;

    invoke-virtual {v0}, Lcxa;->a()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 805
    sget-object v0, Lbsn;->d:Lcxa;

    invoke-virtual {v0, v1}, Lcxa;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsw;

    .line 806
    const-string v4, "%s %5d %5d %5d %5d %5d %s %s"

    const/16 v5, 0x8

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-wide v7, v0, Lbsw;->a:J

    .line 807
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-wide v7, v0, Lbsw;->c:J

    iget-wide v9, v0, Lbsw;->b:J

    .line 808
    invoke-static {v7, v8, v9, v10}, Lbsn;->a(JJ)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    iget-wide v7, v0, Lbsw;->d:J

    iget-wide v9, v0, Lbsw;->b:J

    .line 809
    invoke-static {v7, v8, v9, v10}, Lbsn;->a(JJ)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x3

    iget-wide v7, v0, Lbsw;->e:J

    iget-wide v9, v0, Lbsw;->d:J

    .line 810
    invoke-static {v7, v8, v9, v10}, Lbsn;->a(JJ)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x4

    iget-wide v7, v0, Lbsw;->f:J

    iget-wide v9, v0, Lbsw;->e:J

    .line 811
    invoke-static {v7, v8, v9, v10}, Lbsn;->a(JJ)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x5

    iget-wide v7, v0, Lbsw;->f:J

    iget-wide v9, v0, Lbsw;->b:J

    .line 812
    invoke-static {v7, v8, v9, v10}, Lbsn;->a(JJ)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x6

    iget-object v7, v0, Lbsw;->g:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x7

    iget-object v7, v0, Lbsw;->h:Ljava/lang/String;

    if-eqz v7, :cond_1

    iget-object v0, v0, Lbsw;->h:Ljava/lang/String;

    :goto_2
    aput-object v0, v5, v6

    .line 806
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 804
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_1

    .line 812
    :cond_1
    const-string v0, ""

    goto :goto_2

    .line 815
    :cond_2
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 388
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    if-eqz p0, :cond_0

    const-string v2, "*"

    invoke-virtual {p0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public static b(Lbsv;)V
    .locals 2

    .prologue
    .line 652
    sget-object v0, Lbsn;->f:Lbsn;

    iget-object v0, v0, Lbsn;->h:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lbst;

    invoke-direct {v1, p0}, Lbst;-><init>(Lbsv;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 658
    return-void
.end method

.method public static c()Lbsn;
    .locals 3

    .prologue
    .line 227
    sget-object v0, Lbsn;->f:Lbsn;

    if-nez v0, :cond_2

    .line 228
    sget-object v1, Lbsn;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 229
    :try_start_0
    sget-object v0, Lbsn;->f:Lbsn;

    if-nez v0, :cond_1

    .line 230
    sget-boolean v0, Lbsn;->c:Z

    if-eqz v0, :cond_0

    .line 231
    const-string v0, "Babel_medialoader"

    const-string v2, "Initialize media loader"

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    :cond_0
    new-instance v0, Lbsn;

    invoke-direct {v0}, Lbsn;-><init>()V

    sput-object v0, Lbsn;->f:Lbsn;

    .line 235
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 237
    :cond_2
    sget-object v0, Lbsn;->f:Lbsn;

    return-object v0

    .line 235
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static c(Lbsv;)V
    .locals 3

    .prologue
    .line 312
    sget-boolean v0, Lbsn;->c:Z

    if-eqz v0, :cond_0

    .line 313
    sget-object v0, Lbsn;->f:Lbsn;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "startGetBytes on "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbsv;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbsn;->f(Ljava/lang/String;)V

    .line 315
    :cond_0
    iget-object v0, p0, Lbsv;->e:Lbsw;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbsv;->e:Lbsw;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v1

    iput-wide v1, v0, Lbsw;->d:J

    .line 316
    :cond_1
    invoke-virtual {p0}, Lbsv;->c_()Lbsm;

    move-result-object v0

    .line 317
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lbsm;->a()[B

    move-result-object v1

    if-eqz v1, :cond_2

    .line 318
    invoke-static {p0, v0}, Lbsn;->a(Lbsv;Lbsm;)V

    .line 321
    :cond_2
    return-void
.end method

.method private d(Lbsv;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 468
    iget-object v0, p1, Lbsv;->e:Lbsw;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lbsv;->e:Lbsw;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    iput-wide v2, v0, Lbsw;->f:J

    .line 470
    :cond_0
    iget-object v2, p0, Lbsn;->b:Ljava/lang/Object;

    monitor-enter v2

    .line 471
    const/4 v0, 0x0

    .line 473
    :try_start_0
    invoke-virtual {p1}, Lbsv;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 475
    iget-object v0, p0, Lbsn;->i:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 476
    if-nez v0, :cond_1

    .line 477
    const-string v3, "Babel_medialoader"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lbsn;->f:Lbsn;

    invoke-virtual {v5}, Lbsn;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " removeFromActive for a request that isn\'t in the active."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    :cond_1
    sget-boolean v3, Lbsn;->c:Z

    if-eqz v3, :cond_2

    .line 481
    sget-object v3, Lbsn;->f:Lbsn;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "removeFromActive: reference request ="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 482
    invoke-virtual {p1}, Lbsv;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 481
    invoke-direct {v3, v4}, Lbsn;->f(Ljava/lang/String;)V

    .line 486
    :cond_2
    if-eqz v0, :cond_3

    iget-object v3, p0, Lbsn;->i:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->size()I

    move-result v3

    const/16 v4, 0x8

    if-lt v3, v4, :cond_3

    .line 487
    const-string v3, "Babel_medialoader"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Number of active requests ("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lbsn;->i:Ljava/util/HashSet;

    invoke-virtual {v5}, Ljava/util/HashSet;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") greater than max active (8)"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 493
    :cond_3
    if-eqz v0, :cond_8

    iget-object v0, p0, Lbsn;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_8

    move-object v0, v1

    .line 494
    :goto_0
    if-nez v0, :cond_9

    iget-object v3, p0, Lbsn;->j:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    if-lez v3, :cond_9

    .line 495
    iget-object v0, p0, Lbsn;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsv;

    .line 496
    sget-boolean v3, Lbsn;->c:Z

    if-eqz v3, :cond_4

    .line 497
    sget-object v3, Lbsn;->f:Lbsn;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "transferrequest selected  on "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 498
    invoke-virtual {v0}, Lbsv;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 497
    invoke-direct {v3, v4}, Lbsn;->f(Ljava/lang/String;)V

    .line 503
    :cond_4
    invoke-virtual {v0}, Lbsv;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lbsn;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 504
    sget-boolean v3, Lbsn;->c:Z

    if-eqz v3, :cond_5

    .line 505
    sget-object v3, Lbsn;->f:Lbsn;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "mediaDecoded waiting to active request ="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 506
    invoke-virtual {v0}, Lbsv;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 505
    invoke-direct {v3, v4}, Lbsn;->f(Ljava/lang/String;)V

    .line 508
    :cond_5
    iget-object v3, p0, Lbsn;->i:Ljava/util/HashSet;

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 518
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 510
    :cond_6
    :try_start_1
    sget-boolean v3, Lbsn;->c:Z

    if-eqz v3, :cond_7

    .line 511
    sget-object v3, Lbsn;->f:Lbsn;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "mediaDecoded waiting to active skipped: request ="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 512
    invoke-virtual {v0}, Lbsv;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 511
    invoke-direct {v3, v0}, Lbsn;->f(Ljava/lang/String;)V

    :cond_7
    move-object v0, v1

    .line 514
    goto :goto_0

    :cond_8
    move-object v0, v1

    .line 518
    :cond_9
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 519
    if-eqz v0, :cond_a

    .line 520
    invoke-static {v0}, Lbsn;->c(Lbsv;)V

    .line 522
    :cond_a
    return-void
.end method

.method static synthetic d()Z
    .locals 1

    .prologue
    .line 49
    sget-boolean v0, Lbsn;->c:Z

    return v0
.end method

.method static synthetic e()Lbsn;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lbsn;->f:Lbsn;

    return-object v0
.end method

.method private f(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 667
    const-string v0, "Babel_medialoader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lbsn;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 668
    return-void
.end method


# virtual methods
.method protected a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 243
    const-string v0, "MediaLoader"

    return-object v0
.end method

.method protected a(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 263
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_8

    .line 264
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 268
    invoke-virtual {p0, v0}, Lbsn;->d(Ljava/lang/String;)Lbrv;

    move-result-object v1

    .line 269
    if-nez v1, :cond_1

    .line 270
    sget-boolean v1, Lbsn;->c:Z

    if-eqz v1, :cond_0

    .line 271
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "No MediaRequests for key: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lbsn;->f(Ljava/lang/String;)V

    goto :goto_0

    .line 276
    :cond_1
    instance-of v0, v1, Lbsv;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 277
    check-cast v0, Lbsv;

    .line 278
    iget-object v1, v0, Lbsv;->e:Lbsw;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lbsv;->e:Lbsw;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v3

    iput-wide v3, v1, Lbsw;->c:J

    .line 279
    :cond_2
    iget-object v1, p0, Lbsn;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 280
    :try_start_0
    invoke-virtual {v0}, Lbsv;->a()Z

    move-result v3

    if-nez v3, :cond_4

    .line 281
    sget-boolean v3, Lbsn;->c:Z

    if-eqz v3, :cond_3

    .line 282
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "execute skip: reference request ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 283
    invoke-virtual {v0}, Lbsv;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 282
    invoke-direct {p0, v3}, Lbsn;->f(Ljava/lang/String;)V

    .line 302
    :cond_3
    :goto_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 303
    if-eqz v0, :cond_0

    .line 304
    invoke-static {v0}, Lbsn;->c(Lbsv;)V

    goto :goto_0

    .line 285
    :cond_4
    :try_start_1
    iget-object v3, p0, Lbsn;->i:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->size()I

    move-result v3

    const/16 v4, 0x8

    if-ge v3, v4, :cond_6

    .line 288
    sget-boolean v3, Lbsn;->c:Z

    if-eqz v3, :cond_5

    .line 289
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "execute add to active: reference request ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 290
    invoke-virtual {v0}, Lbsv;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 289
    invoke-direct {p0, v3}, Lbsn;->f(Ljava/lang/String;)V

    .line 292
    :cond_5
    iget-object v3, p0, Lbsn;->i:Ljava/util/HashSet;

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 302
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 295
    :cond_6
    :try_start_2
    sget-boolean v3, Lbsn;->c:Z

    if-eqz v3, :cond_7

    .line 296
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "execute add to waiting: reference request ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 297
    invoke-virtual {v0}, Lbsv;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 296
    invoke-direct {p0, v3}, Lbsn;->f(Ljava/lang/String;)V

    .line 299
    :cond_7
    iget-object v3, p0, Lbsn;->j:Ljava/util/LinkedList;

    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 300
    const/4 v0, 0x0

    goto :goto_1

    .line 309
    :cond_8
    return-void
.end method

.method public a(Lbrv;)Z
    .locals 1

    .prologue
    .line 581
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lbsn;->a(Lbrv;Z)Z

    move-result v0

    return v0
.end method

.method public a(Lbrv;Z)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 592
    if-eqz p1, :cond_0

    instance-of v0, p1, Lbsv;

    if-nez v0, :cond_1

    .line 593
    :cond_0
    const-string v0, "Babel_medialoader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MediaLoader.load: invalid request: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 620
    :goto_0
    return v0

    :cond_1
    move-object v0, p1

    .line 596
    check-cast v0, Lbsv;

    .line 597
    sget-boolean v2, Lbsn;->c:Z

    if-eqz v2, :cond_2

    .line 598
    sget-object v2, Lbsn;->f:Lbsn;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "load req="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lbsv;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " loadOnlyCached="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lbsn;->f(Ljava/lang/String;)V

    .line 600
    :cond_2
    invoke-static {}, Lbys;->b()Z

    move-result v2

    if-eqz v2, :cond_5

    new-instance v2, Lbsw;

    invoke-direct {v2, v1}, Lbsw;-><init>(B)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, v2, Lbsw;->a:J

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v3

    iput-wide v3, v2, Lbsw;->b:J

    invoke-virtual {v0}, Lbsv;->c()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lbsw;->g:Ljava/lang/String;

    sget-object v3, Lbsn;->e:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    sget-object v4, Lbsn;->d:Lcxa;

    if-nez v4, :cond_3

    new-instance v4, Lcxa;

    const/16 v5, 0xc8

    invoke-direct {v4, v5}, Lcxa;-><init>(I)V

    sput-object v4, Lbsn;->d:Lcxa;

    :cond_3
    sget-object v4, Lbsn;->d:Lcxa;

    invoke-virtual {v4, v2}, Lcxa;->a(Ljava/lang/Object;)V

    iput-object v2, v0, Lbsv;->e:Lbsw;

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 602
    :cond_4
    :goto_1
    invoke-virtual {v0}, Lbsv;->f()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {v0}, Lbsv;->e()Z

    move-result v2

    if-eqz v2, :cond_7

    const/4 v2, 0x1

    .line 606
    :goto_2
    if-nez v2, :cond_a

    if-nez p2, :cond_a

    .line 607
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    if-eq v2, v3, :cond_8

    .line 610
    invoke-super {p0, p1}, Lbrr;->c(Lbrv;)V

    move v0, v1

    .line 611
    goto :goto_0

    .line 600
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_5
    sget-object v2, Lbsn;->d:Lcxa;

    if-eqz v2, :cond_4

    sget-object v2, Lbsn;->e:Ljava/lang/Object;

    monitor-enter v2

    :try_start_1
    sget-object v3, Lbsn;->d:Lcxa;

    if-eqz v3, :cond_6

    const/4 v3, 0x0

    sput-object v3, Lbsn;->d:Lcxa;

    :cond_6
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_7
    move v2, v1

    .line 602
    goto :goto_2

    .line 613
    :cond_8
    sget-boolean v1, Lbsn;->c:Z

    if-eqz v1, :cond_9

    .line 614
    sget-object v1, Lbsn;->f:Lbsn;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "no cache found, load req="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lbsv;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lbsn;->f(Ljava/lang/String;)V

    .line 616
    :cond_9
    invoke-super {p0, p1}, Lbrr;->a(Lbrv;)Z

    move-result v0

    goto/16 :goto_0

    .line 619
    :cond_a
    iget-object v1, v0, Lbsv;->e:Lbsw;

    if-eqz v1, :cond_b

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v3

    iget-object v1, v0, Lbsv;->e:Lbsw;

    iput-wide v3, v1, Lbsw;->c:J

    iget-object v1, v0, Lbsv;->e:Lbsw;

    iput-wide v3, v1, Lbsw;->d:J

    iget-object v1, v0, Lbsv;->e:Lbsw;

    iput-wide v3, v1, Lbsw;->e:J

    iget-object v1, v0, Lbsv;->e:Lbsw;

    iput-wide v3, v1, Lbsw;->f:J

    if-eqz v2, :cond_c

    const-string v1, " Bitmap Cache Hit"

    :goto_3
    iget-object v3, v0, Lbsv;->e:Lbsw;

    iget-object v3, v3, Lbsw;->h:Ljava/lang/String;

    if-nez v3, :cond_d

    iget-object v3, v0, Lbsv;->e:Lbsw;

    iput-object v1, v3, Lbsw;->h:Ljava/lang/String;

    :goto_4
    sget-boolean v1, Lbsn;->c:Z

    if-eqz v1, :cond_b

    sget-object v1, Lbsn;->f:Lbsn;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "request history="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lbsv;->e:Lbsw;

    invoke-virtual {v0}, Lbsw;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lbsn;->f(Ljava/lang/String;)V

    :cond_b
    move v0, v2

    .line 620
    goto/16 :goto_0

    .line 619
    :cond_c
    const-string v1, " Bitmap Cache Miss"

    goto :goto_3

    :cond_d
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v0, Lbsv;->e:Lbsw;

    iget-object v5, v4, Lbsw;->h:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v4, Lbsw;->h:Ljava/lang/String;

    goto :goto_4
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 662
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MediaLoader: active/waiting="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lbsn;->i:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbsn;->j:Ljava/util/LinkedList;

    .line 663
    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
