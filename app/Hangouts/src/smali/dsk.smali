.class public final Ldsk;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldsk;


# instance fields
.field public b:Ldvm;

.field public c:Ldqk;

.field public d:Ljava/lang/Boolean;

.field public e:Ljava/lang/Boolean;

.field public f:Ldrv;

.field public g:Ljava/lang/Integer;

.field public h:Ljava/lang/Integer;

.field public i:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10564
    const/4 v0, 0x0

    new-array v0, v0, [Ldsk;

    sput-object v0, Ldsk;->a:[Ldsk;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10565
    invoke-direct {p0}, Lepn;-><init>()V

    .line 10568
    iput-object v0, p0, Ldsk;->b:Ldvm;

    .line 10571
    iput-object v0, p0, Ldsk;->c:Ldqk;

    .line 10578
    iput-object v0, p0, Ldsk;->f:Ldrv;

    .line 10583
    iput-object v0, p0, Ldsk;->h:Ljava/lang/Integer;

    .line 10565
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 10620
    const/4 v0, 0x0

    .line 10621
    iget-object v1, p0, Ldsk;->b:Ldvm;

    if-eqz v1, :cond_0

    .line 10622
    const/4 v0, 0x1

    iget-object v1, p0, Ldsk;->b:Ldvm;

    .line 10623
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 10625
    :cond_0
    iget-object v1, p0, Ldsk;->c:Ldqk;

    if-eqz v1, :cond_1

    .line 10626
    const/4 v1, 0x2

    iget-object v2, p0, Ldsk;->c:Ldqk;

    .line 10627
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10629
    :cond_1
    iget-object v1, p0, Ldsk;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 10630
    const/4 v1, 0x3

    iget-object v2, p0, Ldsk;->d:Ljava/lang/Boolean;

    .line 10631
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 10633
    :cond_2
    iget-object v1, p0, Ldsk;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 10634
    const/4 v1, 0x4

    iget-object v2, p0, Ldsk;->e:Ljava/lang/Boolean;

    .line 10635
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 10637
    :cond_3
    iget-object v1, p0, Ldsk;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 10638
    const/4 v1, 0x6

    iget-object v2, p0, Ldsk;->g:Ljava/lang/Integer;

    .line 10639
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 10641
    :cond_4
    iget-object v1, p0, Ldsk;->f:Ldrv;

    if-eqz v1, :cond_5

    .line 10642
    const/4 v1, 0x7

    iget-object v2, p0, Ldsk;->f:Ldrv;

    .line 10643
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10645
    :cond_5
    iget-object v1, p0, Ldsk;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 10646
    const/16 v1, 0x8

    iget-object v2, p0, Ldsk;->i:Ljava/lang/Boolean;

    .line 10647
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 10649
    :cond_6
    iget-object v1, p0, Ldsk;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 10650
    const/16 v1, 0x9

    iget-object v2, p0, Ldsk;->h:Ljava/lang/Integer;

    .line 10651
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 10653
    :cond_7
    iget-object v1, p0, Ldsk;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10654
    iput v0, p0, Ldsk;->cachedSize:I

    .line 10655
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 10561
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldsk;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldsk;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldsk;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldsk;->b:Ldvm;

    if-nez v0, :cond_2

    new-instance v0, Ldvm;

    invoke-direct {v0}, Ldvm;-><init>()V

    iput-object v0, p0, Ldsk;->b:Ldvm;

    :cond_2
    iget-object v0, p0, Ldsk;->b:Ldvm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldsk;->c:Ldqk;

    if-nez v0, :cond_3

    new-instance v0, Ldqk;

    invoke-direct {v0}, Ldqk;-><init>()V

    iput-object v0, p0, Ldsk;->c:Ldqk;

    :cond_3
    iget-object v0, p0, Ldsk;->c:Ldqk;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldsk;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldsk;->e:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldsk;->g:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Ldsk;->f:Ldrv;

    if-nez v0, :cond_4

    new-instance v0, Ldrv;

    invoke-direct {v0}, Ldrv;-><init>()V

    iput-object v0, p0, Ldsk;->f:Ldrv;

    :cond_4
    iget-object v0, p0, Ldsk;->f:Ldrv;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldsk;->i:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_5

    const/4 v1, 0x1

    if-ne v0, v1, :cond_6

    :cond_5
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldsk;->h:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_6
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldsk;->h:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x30 -> :sswitch_5
        0x3a -> :sswitch_6
        0x40 -> :sswitch_7
        0x48 -> :sswitch_8
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 10590
    iget-object v0, p0, Ldsk;->b:Ldvm;

    if-eqz v0, :cond_0

    .line 10591
    const/4 v0, 0x1

    iget-object v1, p0, Ldsk;->b:Ldvm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 10593
    :cond_0
    iget-object v0, p0, Ldsk;->c:Ldqk;

    if-eqz v0, :cond_1

    .line 10594
    const/4 v0, 0x2

    iget-object v1, p0, Ldsk;->c:Ldqk;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 10596
    :cond_1
    iget-object v0, p0, Ldsk;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 10597
    const/4 v0, 0x3

    iget-object v1, p0, Ldsk;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 10599
    :cond_2
    iget-object v0, p0, Ldsk;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 10600
    const/4 v0, 0x4

    iget-object v1, p0, Ldsk;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 10602
    :cond_3
    iget-object v0, p0, Ldsk;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 10603
    const/4 v0, 0x6

    iget-object v1, p0, Ldsk;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 10605
    :cond_4
    iget-object v0, p0, Ldsk;->f:Ldrv;

    if-eqz v0, :cond_5

    .line 10606
    const/4 v0, 0x7

    iget-object v1, p0, Ldsk;->f:Ldrv;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 10608
    :cond_5
    iget-object v0, p0, Ldsk;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 10609
    const/16 v0, 0x8

    iget-object v1, p0, Ldsk;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 10611
    :cond_6
    iget-object v0, p0, Ldsk;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 10612
    const/16 v0, 0x9

    iget-object v1, p0, Ldsk;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 10614
    :cond_7
    iget-object v0, p0, Ldsk;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 10616
    return-void
.end method
