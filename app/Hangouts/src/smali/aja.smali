.class public final Laja;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:I

.field public d:Z

.field public e:Z

.field public f:Z

.field public final g:I

.field public h:Landroid/view/View;

.field final synthetic i:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;Ljava/lang/String;Ljava/lang/String;IZZI)V
    .locals 1

    .prologue
    .line 386
    iput-object p1, p0, Laja;->i:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 387
    iput-object p2, p0, Laja;->a:Ljava/lang/String;

    .line 388
    iput-object p3, p0, Laja;->b:Ljava/lang/String;

    .line 389
    iput p4, p0, Laja;->c:I

    .line 390
    iput-boolean p5, p0, Laja;->d:Z

    .line 391
    iput-boolean p6, p0, Laja;->e:Z

    .line 392
    iput p7, p0, Laja;->g:I

    .line 393
    const/4 v0, 0x0

    iput-object v0, p0, Laja;->h:Landroid/view/View;

    .line 394
    const/4 v0, 0x1

    iput-boolean v0, p0, Laja;->f:Z

    .line 395
    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    .line 403
    iget-object v0, p0, Laja;->h:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 404
    iget-object v0, p0, Laja;->h:Landroid/view/View;

    iget-boolean v1, p0, Laja;->f:Z

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 406
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_2

    .line 407
    iget-object v0, p0, Laja;->i:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    iget-object v1, p0, Laja;->h:Landroid/view/View;

    iget-boolean v0, p0, Laja;->f:Z

    if-eqz v0, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    invoke-static {v1, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->a(Landroid/view/View;F)V

    .line 427
    :cond_0
    :goto_1
    return-void

    .line 407
    :cond_1
    const v0, 0x3ecccccd    # 0.4f

    goto :goto_0

    .line 410
    :cond_2
    iget-boolean v0, p0, Laja;->f:Z

    if-eqz v0, :cond_5

    const/16 v0, 0xff

    move v3, v0

    .line 413
    :goto_2
    iget-object v0, p0, Laja;->h:Landroid/view/View;

    sget v1, Lg;->hz:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 414
    iget-object v1, p0, Laja;->h:Landroid/view/View;

    sget v2, Lg;->hi:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 415
    iget-object v2, p0, Laja;->h:Landroid/view/View;

    sget v4, Lg;->du:I

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 416
    if-eqz v0, :cond_3

    .line 417
    iget-object v4, p0, Laja;->i:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    invoke-static {v0, v3}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->a(Landroid/widget/TextView;I)V

    .line 419
    :cond_3
    if-eqz v1, :cond_4

    .line 420
    iget-object v0, p0, Laja;->i:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    invoke-static {v1, v3}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->a(Landroid/widget/TextView;I)V

    .line 422
    :cond_4
    if-eqz v2, :cond_0

    .line 423
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setAlpha(I)V

    goto :goto_1

    .line 410
    :cond_5
    const/16 v0, 0x66

    move v3, v0

    goto :goto_2
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 398
    iput-boolean p1, p0, Laja;->f:Z

    .line 399
    invoke-virtual {p0}, Laja;->a()V

    .line 400
    return-void
.end method
