.class public Lbhq;
.super Lbgj;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method private constructor <init>(Ldvi;)V
    .locals 7

    .prologue
    .line 1438
    iget-object v1, p1, Ldvi;->b:Ldvn;

    iget-object v0, p1, Ldvi;->c:Ldrr;

    iget-object v0, v0, Ldrr;->d:Ljava/lang/Long;

    .line 1439
    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v2

    iget-object v0, p1, Ldvi;->c:Ldrr;

    iget-object v0, v0, Ldrr;->o:Ljava/lang/Long;

    .line 1440
    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v4

    iget-object v0, p1, Ldvi;->c:Ldrr;

    iget-object v6, v0, Ldrr;->e:Ljava/lang/String;

    move-object v0, p0

    .line 1438
    invoke-direct/range {v0 .. v6}, Lbgj;-><init>(Ldvn;JJLjava/lang/String;)V

    .line 1442
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_0

    .line 1443
    const-string v0, "Babel_protos"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RenameConversationResponse from:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1445
    :cond_0
    return-void
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 1449
    new-instance v0, Ldvi;

    invoke-direct {v0}, Ldvi;-><init>()V

    .line 1450
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldvi;

    .line 1451
    iget-object v1, v0, Ldvi;->b:Ldvn;

    invoke-static {v1}, Lbhq;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1452
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldvi;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 1454
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbhq;

    invoke-direct {v1, v0}, Lbhq;-><init>(Ldvi;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Lyt;Lbnl;)V
    .locals 0

    .prologue
    .line 1464
    invoke-super {p0, p1, p2}, Lbgj;->a(Lyt;Lbnl;)V

    .line 1470
    return-void
.end method
