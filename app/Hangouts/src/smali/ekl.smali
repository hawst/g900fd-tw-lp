.class public final Lekl;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lekl;


# instance fields
.field public b:Ljava/lang/Boolean;

.field public c:Lekm;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    new-array v0, v0, [Lekl;

    sput-object v0, Lekl;->a:[Lekl;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 87
    invoke-direct {p0}, Lepn;-><init>()V

    .line 152
    const/4 v0, 0x0

    iput-object v0, p0, Lekl;->c:Lekm;

    .line 87
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 169
    const/4 v0, 0x0

    .line 170
    iget-object v1, p0, Lekl;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 171
    const/4 v0, 0x1

    iget-object v1, p0, Lekl;->b:Ljava/lang/Boolean;

    .line 172
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Lepl;->g(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 174
    :cond_0
    iget-object v1, p0, Lekl;->c:Lekm;

    if-eqz v1, :cond_1

    .line 175
    const/4 v1, 0x2

    iget-object v2, p0, Lekl;->c:Lekm;

    .line 176
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 178
    :cond_1
    iget-object v1, p0, Lekl;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 179
    iput v0, p0, Lekl;->cachedSize:I

    .line 180
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 83
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lekl;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lekl;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lekl;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lekl;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lekl;->c:Lekm;

    if-nez v0, :cond_2

    new-instance v0, Lekm;

    invoke-direct {v0}, Lekm;-><init>()V

    iput-object v0, p0, Lekl;->c:Lekm;

    :cond_2
    iget-object v0, p0, Lekl;->c:Lekm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lekl;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 158
    const/4 v0, 0x1

    iget-object v1, p0, Lekl;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 160
    :cond_0
    iget-object v0, p0, Lekl;->c:Lekm;

    if-eqz v0, :cond_1

    .line 161
    const/4 v0, 0x2

    iget-object v1, p0, Lekl;->c:Lekm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 163
    :cond_1
    iget-object v0, p0, Lekl;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 165
    return-void
.end method
