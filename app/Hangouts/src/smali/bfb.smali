.class public Lbfb;
.super Lbei;
.source "PG"


# static fields
.field private static final E:Ljava/util/regex/Pattern;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field final A:I

.field public final B:J

.field private C:J

.field private transient D:Ljava/io/InputStream;

.field private F:J

.field private G:I

.field private H:J

.field private I:J

.field private J:Z

.field private K:Z

.field private L:Z

.field final a:Ljava/lang/String;

.field public final b:J

.field public final j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbiz;",
            ">;"
        }
    .end annotation
.end field

.field public final k:Ljava/lang/String;

.field final l:Ljava/lang/String;

.field final m:I

.field final n:Ljava/lang/String;

.field public final o:Ljava/lang/String;

.field public final p:Ljava/lang/String;

.field public final q:D

.field public final r:D

.field public final s:Ljava/lang/String;

.field public final t:Ljava/lang/String;

.field public final u:I

.field public final v:I

.field public final w:Z

.field final x:Z

.field final y:I

.field final z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1791
    const-string v0, "\\*\\*\\*([\\w]{1})(\\d*)(p?)\\*\\*\\*"

    .line 1792
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lbfb;->E:Ljava/util/regex/Pattern;

    .line 1791
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/util/List;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IILjava/lang/String;Ljava/lang/String;DDLjava/lang/String;Ljava/lang/String;ZILjava/lang/String;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "Ljava/util/List",
            "<",
            "Lbiz;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "II",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "DD",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "ZI",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 1411
    invoke-direct {p0, p2, p1}, Lbei;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1384
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lbfb;->C:J

    .line 1851
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lbfb;->F:J

    .line 1852
    const/4 v2, 0x0

    iput v2, p0, Lbfb;->G:I

    .line 1853
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lbfb;->H:J

    .line 1854
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lbfb;->I:J

    .line 1855
    const/4 v2, 0x0

    iput-boolean v2, p0, Lbfb;->J:Z

    .line 1856
    const/4 v2, 0x0

    iput-boolean v2, p0, Lbfb;->K:Z

    .line 1857
    const/4 v2, 0x0

    iput-boolean v2, p0, Lbfb;->L:Z

    .line 1413
    iget-object v2, p0, Lbfb;->i:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 1414
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "null clientGeneratedId"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1417
    :cond_0
    iput-object p3, p0, Lbfb;->a:Ljava/lang/String;

    .line 1418
    iput-wide p4, p0, Lbfb;->b:J

    .line 1419
    iput-object p6, p0, Lbfb;->j:Ljava/util/List;

    .line 1420
    iput-object p7, p0, Lbfb;->k:Ljava/lang/String;

    .line 1421
    iput-object p8, p0, Lbfb;->l:Ljava/lang/String;

    .line 1422
    iput p9, p0, Lbfb;->m:I

    .line 1423
    iput-object p10, p0, Lbfb;->n:Ljava/lang/String;

    .line 1424
    iput p11, p0, Lbfb;->v:I

    .line 1425
    move/from16 v0, p12

    iput v0, p0, Lbfb;->u:I

    .line 1426
    move-object/from16 v0, p13

    iput-object v0, p0, Lbfb;->o:Ljava/lang/String;

    .line 1427
    move-object/from16 v0, p14

    iput-object v0, p0, Lbfb;->p:Ljava/lang/String;

    .line 1428
    move-wide/from16 v0, p15

    iput-wide v0, p0, Lbfb;->q:D

    .line 1429
    move-wide/from16 v0, p17

    iput-wide v0, p0, Lbfb;->r:D

    .line 1430
    move-object/from16 v0, p19

    iput-object v0, p0, Lbfb;->s:Ljava/lang/String;

    .line 1431
    move-object/from16 v0, p20

    iput-object v0, p0, Lbfb;->t:Ljava/lang/String;

    .line 1432
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lbfb;->B:J

    .line 1433
    move/from16 v0, p21

    iput-boolean v0, p0, Lbfb;->w:Z

    .line 1434
    const/4 v2, 0x0

    iput-boolean v2, p0, Lbfb;->x:Z

    .line 1435
    move/from16 v0, p22

    iput v0, p0, Lbfb;->y:I

    .line 1436
    move-object/from16 v0, p23

    iput-object v0, p0, Lbfb;->z:Ljava/lang/String;

    .line 1437
    move/from16 v0, p24

    iput v0, p0, Lbfb;->A:I

    .line 1438
    invoke-direct {p0}, Lbfb;->k()V

    .line 1439
    return-void
.end method

.method private d(JI)Lbph;
    .locals 7

    .prologue
    .line 1716
    iget-wide v0, p0, Lbfb;->H:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 1717
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lbfb;->H:J

    add-long/2addr v2, p1

    cmp-long v0, v0, v2

    if-gez v0, :cond_4

    .line 1718
    new-instance v0, Lbph;

    const/16 v1, 0x66

    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lbph;-><init>(ILjava/lang/Exception;JZLjava/lang/String;)V

    .line 1759
    :goto_0
    return-object v0

    .line 1725
    :cond_0
    iget v0, p0, Lbfb;->G:I

    if-lez v0, :cond_1

    .line 1726
    iget v0, p0, Lbfb;->G:I

    if-le v0, p3, :cond_4

    .line 1727
    new-instance v0, Lbph;

    const/16 v1, 0x66

    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lbph;-><init>(ILjava/lang/Exception;JZLjava/lang/String;)V

    goto :goto_0

    .line 1734
    :cond_1
    iget-boolean v0, p0, Lbfb;->J:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x5

    if-ge p3, v0, :cond_2

    .line 1735
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 1736
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/hangouts/phone/ShowToastForTesting;

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1737
    const-string v0, "toast_text"

    const-string v1, "authentication"

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1738
    new-instance v0, Lbph;

    const/16 v1, 0x64

    new-instance v2, Lbxl;

    invoke-direct {v2, v3}, Lbxl;-><init>(Landroid/content/Intent;)V

    const-wide/16 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lbph;-><init>(ILjava/lang/Exception;JZLjava/lang/String;)V

    goto :goto_0

    .line 1744
    :cond_2
    iget-boolean v0, p0, Lbfb;->L:Z

    if-eqz v0, :cond_3

    .line 1745
    new-instance v0, Lbph;

    const/16 v1, 0x6d

    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lbph;-><init>(ILjava/lang/Exception;JZLjava/lang/String;)V

    goto :goto_0

    .line 1751
    :cond_3
    iget-wide v0, p0, Lbfb;->I:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_4

    if-nez p3, :cond_4

    .line 1752
    new-instance v0, Lbph;

    const/16 v1, 0x69

    const/4 v2, 0x0

    iget-wide v3, p0, Lbfb;->I:J

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lbph;-><init>(ILjava/lang/Exception;JZLjava/lang/String;)V

    goto :goto_0

    .line 1759
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()V
    .locals 5

    .prologue
    .line 1639
    iget-object v0, p0, Lbfb;->n:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1640
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1641
    const-string v1, "image/gif"

    iget-object v2, p0, Lbfb;->l:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lbfb;->l:Ljava/lang/String;

    .line 1642
    invoke-static {v1}, Lf;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1644
    :cond_0
    :try_start_0
    iget-object v1, p0, Lbfb;->n:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1645
    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lbfb;->D:Ljava/io/InputStream;

    .line 1646
    new-instance v0, Ljava/io/File;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    iput-wide v0, p0, Lbfb;->C:J
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1686
    :cond_1
    :goto_0
    return-void

    .line 1647
    :catch_0
    move-exception v0

    .line 1648
    const-string v1, "Babel_RequestWriter"

    const-string v2, "FileNotFoundException trying to detect how large the  attachment is"

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1653
    :cond_2
    const-string v1, "babel_thumbnail_photo_upload_size"

    const/16 v2, 0x280

    .line 1654
    invoke-static {v0, v1, v2}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 1657
    invoke-static {}, Lbyr;->a()Lbxs;

    move-result-object v2

    .line 1658
    const/4 v0, 0x0

    .line 1660
    :try_start_1
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v3

    .line 1661
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v4, p0, Lbfb;->n:Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v3

    .line 1662
    invoke-static {v3}, Lbyr;->a(Ljava/io/InputStream;)[B

    move-result-object v3

    .line 1663
    if-eqz v3, :cond_3

    array-length v4, v3

    if-lez v4, :cond_3

    .line 1666
    iget v4, p0, Lbfb;->m:I

    invoke-virtual {v2, v3, v1, v1, v4}, Lbxs;->a([BIII)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1668
    if-eqz v1, :cond_3

    .line 1669
    const/16 v3, 0x46

    invoke-static {v1, v3}, Lbyr;->a(Landroid/graphics/Bitmap;I)[B

    move-result-object v3

    .line 1670
    invoke-virtual {v2, v1}, Lbxs;->a(Landroid/graphics/Bitmap;)V

    .line 1671
    if-eqz v3, :cond_3

    array-length v1, v3

    if-lez v1, :cond_3

    .line 1672
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    iput-object v1, p0, Lbfb;->D:Ljava/io/InputStream;

    .line 1673
    array-length v1, v3

    int-to-long v1, v1

    iput-wide v1, p0, Lbfb;->C:J
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1674
    const/4 v0, 0x1

    .line 1681
    :cond_3
    :goto_1
    if-nez v0, :cond_1

    .line 1682
    const-string v0, "Babel_RequestWriter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failing to send image for uri: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbfb;->n:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1678
    :catch_1
    move-exception v1

    .line 1679
    const-string v2, "Babel_RequestWriter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not open file corresponding to uri "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lbfb;->n:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private j()Z
    .locals 2

    .prologue
    .line 1698
    iget-object v0, p0, Lbfb;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbfb;->n:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v0, "hangouts/location"

    iget-object v1, p0, Lbfb;->l:Ljava/lang/String;

    .line 1699
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 1781
    iget-object v0, p0, Lbfb;->j:Ljava/util/List;

    if-nez v0, :cond_1

    .line 1789
    :cond_0
    return-void

    .line 1784
    :cond_1
    iget-object v0, p0, Lbfb;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbiz;

    .line 1785
    iget v3, v0, Lbiz;->a:I

    if-nez v3, :cond_2

    .line 1786
    iget-object v0, v0, Lbiz;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    sget-object v3, Lbfb;->E:Ljava/util/regex/Pattern;

    invoke-virtual {v3, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Ljava/util/regex/Matcher;->find(I)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v3, v8}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    const/4 v0, 0x2

    invoke-virtual {v3, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    const/4 v5, 0x3

    invoke-virtual {v3, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    :try_start_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    move v0, v1

    :goto_1
    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :goto_2
    iget-boolean v0, p0, Lbfb;->K:Z

    const-string v4, "p"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    or-int/2addr v0, v4

    iput-boolean v0, p0, Lbfb;->K:Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_3
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->end()I

    move-result v0

    goto :goto_0

    :cond_3
    :try_start_1
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    :sswitch_0
    const-string v4, "Babel_RequestWriter"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "failure injection counter: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    iput v0, p0, Lbfb;->G:I

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_3

    :sswitch_1
    const-string v4, "Babel_RequestWriter"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "failure injection time: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v6, v0

    iput-wide v6, p0, Lbfb;->H:J

    goto :goto_2

    :sswitch_2
    const-string v4, "Babel_RequestWriter"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "failure injection max latency: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v6, v0

    iput-wide v6, p0, Lbfb;->F:J

    goto :goto_2

    :sswitch_3
    const-string v0, "Babel_RequestWriter"

    const-string v4, "failure inject auth error"

    invoke-static {v0, v4}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbfb;->J:Z

    goto :goto_2

    :sswitch_4
    const-string v0, "Babel_RequestWriter"

    const-string v4, "failure inject fatal (retry limit)"

    invoke-static {v0, v4}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbfb;->L:Z

    goto :goto_2

    :sswitch_5
    const-string v4, "Babel_RequestWriter"

    const-string v6, "failure inject server backoff"

    invoke-static {v4, v6}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v6, v0

    iput-wide v6, p0, Lbfb;->I:J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x41 -> :sswitch_3
        0x43 -> :sswitch_0
        0x46 -> :sswitch_4
        0x4d -> :sswitch_2
        0x53 -> :sswitch_5
        0x54 -> :sswitch_1
        0x61 -> :sswitch_3
        0x63 -> :sswitch_0
        0x66 -> :sswitch_4
        0x6d -> :sswitch_2
        0x73 -> :sswitch_5
        0x74 -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 1477
    const-string v0, "Babel_RequestWriter"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1478
    const-string v0, "Babel_RequestWriter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "sendMessage build protobuf clientGeneratedId="

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lbfb;->i:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1482
    :cond_0
    new-instance v2, Ldrx;

    invoke-direct {v2}, Ldrx;-><init>()V

    .line 1484
    iget-object v0, p0, Lbfb;->i:Ljava/lang/String;

    .line 1485
    invoke-static {v0}, Lyt;->c(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v2, Ldrx;->c:Ljava/lang/Long;

    .line 1486
    const-string v0, "babel_contingency_mode_enabled"

    invoke-static {v0, v3}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 1489
    if-nez v0, :cond_1

    iget-boolean v0, p0, Lbfb;->x:Z

    if-nez v0, :cond_1

    .line 1490
    iget-object v0, p0, Lbfb;->c:Ljava/lang/String;

    invoke-static {v0}, Lbea;->d(Ljava/lang/String;)Ldqf;

    move-result-object v0

    iput-object v0, v2, Ldrx;->b:Ldqf;

    .line 1492
    :cond_1
    iget-boolean v0, p0, Lbfb;->w:Z

    if-eqz v0, :cond_a

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v2, Ldrx;->d:Ljava/lang/Integer;

    .line 1495
    new-instance v0, Ldqw;

    invoke-direct {v0}, Ldqw;-><init>()V

    .line 1496
    iget v4, p0, Lbfb;->y:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, v0, Ldqw;->b:Ljava/lang/Integer;

    .line 1497
    iget-object v4, p0, Lbfb;->z:Ljava/lang/String;

    if-eqz v4, :cond_2

    .line 1498
    new-instance v4, Leir;

    invoke-direct {v4}, Leir;-><init>()V

    iput-object v4, v0, Ldqw;->c:Leir;

    .line 1499
    iget-object v4, v0, Ldqw;->c:Leir;

    iget-object v5, p0, Lbfb;->z:Ljava/lang/String;

    iput-object v5, v4, Leir;->b:Ljava/lang/String;

    .line 1501
    :cond_2
    iput-object v0, v2, Ldrx;->e:Ldqw;

    .line 1503
    iget v0, p0, Lbfb;->A:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v2, Ldrx;->f:Ljava/lang/Integer;

    .line 1505
    new-instance v4, Ldvu;

    invoke-direct {v4}, Ldvu;-><init>()V

    .line 1507
    iput-object v2, v4, Ldvu;->c:Ldrx;

    .line 1509
    new-instance v5, Ldtx;

    invoke-direct {v5}, Ldtx;-><init>()V

    .line 1510
    iget-object v0, p0, Lbfb;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    .line 1511
    if-lez v6, :cond_c

    .line 1512
    new-array v7, v6, [Lesg;

    move v2, v3

    .line 1513
    :goto_1
    if-ge v2, v6, :cond_b

    .line 1514
    iget-object v0, p0, Lbfb;->j:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbiz;

    .line 1515
    new-instance v8, Lesg;

    invoke-direct {v8}, Lesg;-><init>()V

    .line 1516
    iget-object v9, v0, Lbiz;->b:Ljava/lang/String;

    if-eqz v9, :cond_3

    .line 1517
    iget-object v9, v0, Lbiz;->b:Ljava/lang/String;

    iput-object v9, v8, Lesg;->c:Ljava/lang/String;

    .line 1519
    :cond_3
    iget-object v9, v0, Lbiz;->d:Ljava/lang/String;

    if-eqz v9, :cond_4

    .line 1520
    new-instance v9, Lesf;

    invoke-direct {v9}, Lesf;-><init>()V

    iput-object v9, v8, Lesg;->e:Lesf;

    .line 1521
    iget-object v9, v8, Lesg;->e:Lesf;

    iget-object v10, v0, Lbiz;->d:Ljava/lang/String;

    iput-object v10, v9, Lesf;->b:Ljava/lang/String;

    .line 1522
    iget-object v9, v0, Lbiz;->b:Ljava/lang/String;

    iput-object v9, v8, Lesg;->c:Ljava/lang/String;

    .line 1524
    :cond_4
    iget v9, v0, Lbiz;->c:I

    if-eqz v9, :cond_9

    .line 1525
    new-instance v9, Lesd;

    invoke-direct {v9}, Lesd;-><init>()V

    .line 1526
    iget v10, v0, Lbiz;->c:I

    and-int/lit8 v10, v10, 0x1

    if-eqz v10, :cond_5

    .line 1527
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    iput-object v10, v9, Lesd;->b:Ljava/lang/Boolean;

    .line 1529
    :cond_5
    iget v10, v0, Lbiz;->c:I

    and-int/lit8 v10, v10, 0x2

    if-eqz v10, :cond_6

    .line 1530
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    iput-object v10, v9, Lesd;->c:Ljava/lang/Boolean;

    .line 1532
    :cond_6
    iget v10, v0, Lbiz;->c:I

    and-int/lit8 v10, v10, 0x4

    if-eqz v10, :cond_7

    .line 1533
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    iput-object v10, v9, Lesd;->d:Ljava/lang/Boolean;

    .line 1535
    :cond_7
    iget v10, v0, Lbiz;->c:I

    and-int/lit8 v10, v10, 0x8

    if-eqz v10, :cond_8

    .line 1536
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    iput-object v10, v9, Lesd;->e:Ljava/lang/Boolean;

    .line 1538
    :cond_8
    iput-object v9, v8, Lesg;->d:Lesd;

    .line 1540
    :cond_9
    iget v0, v0, Lbiz;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v8, Lesg;->b:Ljava/lang/Integer;

    .line 1541
    aput-object v8, v7, v2

    .line 1513
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1492
    :cond_a
    const/4 v0, 0x2

    goto/16 :goto_0

    .line 1543
    :cond_b
    iput-object v7, v5, Ldtx;->b:[Lesg;

    .line 1545
    :cond_c
    iget-object v0, p0, Lbfb;->k:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 1546
    new-instance v0, Ldry;

    invoke-direct {v0}, Ldry;-><init>()V

    iput-object v0, v4, Ldvu;->h:Ldry;

    .line 1547
    iget-object v0, v4, Ldvu;->h:Ldry;

    new-instance v1, Ldsa;

    invoke-direct {v1}, Ldsa;-><init>()V

    iput-object v1, v0, Ldry;->c:Ldsa;

    .line 1548
    iget-object v0, v4, Ldvu;->h:Ldry;

    iget-object v0, v0, Ldry;->c:Ldsa;

    iget-object v1, p0, Lbfb;->k:Ljava/lang/String;

    iput-object v1, v0, Ldsa;->c:Ljava/lang/String;

    .line 1551
    :cond_d
    const-string v0, "hangouts/location"

    iget-object v1, p0, Lbfb;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1552
    sget-boolean v0, Lbea;->d:Z

    if-eqz v0, :cond_e

    .line 1553
    const-string v0, "Babel_RequestWriter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Sending location: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbfb;->o:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "url:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbfb;->n:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1556
    :cond_e
    new-instance v0, Leqf;

    invoke-direct {v0}, Leqf;-><init>()V

    .line 1557
    iget-wide v1, p0, Lbfb;->q:D

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    iput-object v1, v0, Leqf;->o:Ljava/lang/Double;

    .line 1558
    iget-wide v1, p0, Lbfb;->r:D

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    iput-object v1, v0, Leqf;->p:Ljava/lang/Double;

    .line 1560
    new-instance v1, Lepu;

    invoke-direct {v1}, Lepu;-><init>()V

    .line 1561
    sget-object v2, Leqf;->b:Lepo;

    invoke-virtual {v1, v2, v0}, Lepu;->setExtension(Lepo;Ljava/lang/Object;)V

    .line 1563
    new-instance v0, Leqn;

    invoke-direct {v0}, Leqn;-><init>()V

    .line 1564
    iget-object v2, p0, Lbfb;->n:Ljava/lang/String;

    iput-object v2, v0, Leqn;->d:Ljava/lang/String;

    .line 1566
    new-instance v2, Lepu;

    invoke-direct {v2}, Lepu;-><init>()V

    .line 1567
    sget-object v6, Leqn;->b:Lepo;

    invoke-virtual {v2, v6, v0}, Lepu;->setExtension(Lepo;Ljava/lang/Object;)V

    .line 1571
    new-instance v0, Leqs;

    invoke-direct {v0}, Leqs;-><init>()V

    .line 1572
    iget-object v6, p0, Lbfb;->o:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_f

    .line 1573
    iget-object v6, p0, Lbfb;->o:Ljava/lang/String;

    iput-object v6, v0, Leqs;->f:Ljava/lang/String;

    .line 1575
    :cond_f
    iput-object v2, v0, Leqs;->x:Lepu;

    .line 1576
    iput-object v1, v0, Leqs;->p:Lepu;

    .line 1578
    new-instance v1, Ldtq;

    invoke-direct {v1}, Ldtq;-><init>()V

    .line 1579
    iput-object v0, v1, Ldtq;->b:Leqs;

    .line 1581
    iput-object v1, v4, Ldvu;->i:Ldtq;

    .line 1584
    :cond_10
    iput-object v5, v4, Ldvu;->g:Ldtx;

    .line 1586
    iget-object v0, p0, Lbfb;->a:Ljava/lang/String;

    if-eqz v0, :cond_11

    .line 1587
    new-instance v0, Ldth;

    invoke-direct {v0}, Ldth;-><init>()V

    iput-object v0, v4, Ldvu;->f:Ldth;

    .line 1588
    iget-object v0, v4, Ldvu;->f:Ldth;

    iget-object v1, p0, Lbfb;->a:Ljava/lang/String;

    iput-object v1, v0, Ldth;->b:Ljava/lang/String;

    .line 1591
    :cond_11
    new-instance v0, Ldpt;

    invoke-direct {v0}, Ldpt;-><init>()V

    .line 1593
    iget-wide v1, p0, Lbfb;->b:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Ldpt;->b:Ljava/lang/Long;

    .line 1594
    invoke-virtual {p0, v0, v3, p1, p2}, Lbfb;->a(Ldpt;ZLjava/lang/String;I)Ldvm;

    move-result-object v0

    iput-object v0, v4, Ldvu;->b:Ldvm;

    .line 1600
    return-object v4
.end method

.method protected a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1704
    invoke-direct {p0}, Lbfb;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "https://www.googleapis.com/chat/v1android/"

    .line 1706
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lbfb;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1704
    :cond_0
    const-string v0, "https://www.googleapis.com/upload/chat/v1android/"

    goto :goto_0
.end method

.method public a(Lyj;Lbph;)V
    .locals 22

    .prologue
    .line 1886
    invoke-super/range {p0 .. p2}, Lbei;->a(Lyj;Lbph;)V

    .line 1887
    if-eqz p2, :cond_0

    invoke-virtual/range {p2 .. p2}, Lbph;->e()I

    move-result v1

    if-eqz v1, :cond_0

    .line 1888
    const/16 v1, 0x5f3

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lbfb;->a(Lyj;I)V

    .line 1889
    new-instance v1, Lyt;

    move-object/from16 v0, p1

    invoke-direct {v1, v0}, Lyt;-><init>(Lyj;)V

    .line 1890
    move-object/from16 v0, p0

    iget-object v2, v0, Lbfb;->i:Ljava/lang/String;

    .line 1891
    invoke-static {v2}, Lyt;->d(Ljava/lang/String;)J

    move-result-wide v6

    .line 1892
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long v11, v2, v4

    .line 1894
    const-wide/16 v2, -0x1

    const-wide/16 v4, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x5

    const/16 v10, 0x65

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual/range {v1 .. v14}, Lyt;->a(JJJLjava/lang/String;IIJLyu;I)J

    .line 1899
    const/4 v13, 0x5

    .line 1903
    invoke-virtual/range {p2 .. p2}, Lbph;->e()I

    move-result v14

    const/4 v15, 0x0

    const-wide/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbfb;->c:Ljava/lang/String;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v10, p1

    move-wide/from16 v18, v6

    .line 1899
    invoke-static/range {v10 .. v21}, Lf;->a(Lyj;JIILjava/lang/String;JJLjava/lang/String;Lyu;)V

    .line 1910
    :cond_0
    return-void
.end method

.method public b(JI)Lbph;
    .locals 1

    .prologue
    .line 1765
    iget-boolean v0, p0, Lbfb;->K:Z

    if-nez v0, :cond_0

    .line 1766
    invoke-direct {p0, p1, p2, p3}, Lbfb;->d(JI)Lbph;

    move-result-object v0

    .line 1768
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;I)Lcom/google/api/client/http/HttpContent;
    .locals 2

    .prologue
    .line 1711
    new-instance v0, Lbcy;

    invoke-virtual {p0, p1, p2}, Lbfb;->a(Ljava/lang/String;I)Lepr;

    move-result-object v1

    invoke-direct {v0, v1}, Lbcy;-><init>(Lepr;)V

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1605
    const-string v0, "conversations/sendchatmessage"

    return-object v0
.end method

.method public c(JI)Lbph;
    .locals 1

    .prologue
    .line 1774
    iget-boolean v0, p0, Lbfb;->K:Z

    if-eqz v0, :cond_0

    .line 1775
    invoke-direct {p0, p1, p2, p3}, Lbfb;->d(JI)Lbph;

    move-result-object v0

    .line 1777
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1694
    invoke-virtual {p0}, Lbfb;->v()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const-string v0, "photo_queue"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "event_queue"

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1881
    iget-object v0, p0, Lbfb;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public d()J
    .locals 4

    .prologue
    .line 1861
    iget-wide v0, p0, Lbfb;->F:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1862
    invoke-super {p0}, Lbei;->d()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lbfb;->F:J

    goto :goto_0
.end method

.method public h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1461
    iget-object v0, p0, Lbfb;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1462
    iget-object v0, p0, Lbfb;->j:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbiz;

    iget-object v0, v0, Lbiz;->b:Ljava/lang/String;

    .line 1463
    invoke-static {v0}, Lf;->m(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1468
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public r()Z
    .locals 1

    .prologue
    .line 1870
    const/4 v0, 0x1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1875
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lbei;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbfb;->j:Ljava/util/List;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lf;->a(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public u()Lcom/google/api/client/http/AbstractInputStreamContent;
    .locals 3

    .prologue
    .line 1624
    invoke-direct {p0}, Lbfb;->j()Z

    move-result v0

    invoke-static {v0}, Lcwz;->b(Z)V

    .line 1625
    iget-object v0, p0, Lbfb;->D:Ljava/io/InputStream;

    if-nez v0, :cond_0

    .line 1626
    invoke-direct {p0}, Lbfb;->i()V

    .line 1628
    :cond_0
    iget-object v0, p0, Lbfb;->D:Ljava/io/InputStream;

    if-nez v0, :cond_1

    .line 1629
    const-string v0, "Babel_RequestWriter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "We could not create an inputStream for the uri: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbfb;->n:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1630
    const/4 v0, 0x0

    .line 1635
    :goto_0
    return-object v0

    .line 1632
    :cond_1
    new-instance v0, Lcom/google/api/client/http/InputStreamContent;

    iget-object v1, p0, Lbfb;->l:Ljava/lang/String;

    iget-object v2, p0, Lbfb;->D:Ljava/io/InputStream;

    invoke-direct {v0, v1, v2}, Lcom/google/api/client/http/InputStreamContent;-><init>(Ljava/lang/String;Ljava/io/InputStream;)V

    .line 1634
    iget-wide v1, p0, Lbfb;->C:J

    invoke-virtual {v0, v1, v2}, Lcom/google/api/client/http/InputStreamContent;->setLength(J)Lcom/google/api/client/http/InputStreamContent;

    goto :goto_0
.end method

.method public v()I
    .locals 5

    .prologue
    .line 1610
    invoke-direct {p0}, Lbfb;->j()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1611
    iget-wide v0, p0, Lbfb;->C:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 1612
    invoke-direct {p0}, Lbfb;->i()V

    .line 1614
    :cond_0
    iget-wide v0, p0, Lbfb;->C:J

    const-string v2, "babel_direct_vs_resumable_upload_threshold"

    const-wide/32 v3, 0x400000

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;J)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    const/4 v0, 0x1

    .line 1619
    :goto_0
    return v0

    .line 1614
    :cond_1
    const/4 v0, 0x2

    goto :goto_0

    .line 1619
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
