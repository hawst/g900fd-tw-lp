.class public final Laej;
.super Ls;
.source "PG"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private Y:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Laee;",
            ">;"
        }
    .end annotation
.end field

.field private Z:Ljava/lang/String;

.field private aa:Laen;

.field private ab:Laei;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ls;-><init>()V

    .line 61
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/util/ArrayList;)Laej;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Laee;",
            ">;)",
            "Laej;"
        }
    .end annotation

    .prologue
    .line 48
    new-instance v0, Laej;

    invoke-direct {v0}, Laej;-><init>()V

    .line 49
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 50
    const-string v2, "name"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    const-string v2, "contacts"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 52
    invoke-virtual {v0, v1}, Laej;->setArguments(Landroid/os/Bundle;)V

    .line 53
    return-object v0
.end method

.method private a(Laee;)V
    .locals 1

    .prologue
    .line 154
    invoke-virtual {p0}, Laej;->b()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 155
    invoke-virtual {p0}, Laej;->b()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 158
    :cond_0
    iget-object v0, p0, Laej;->aa:Laen;

    if-eqz v0, :cond_1

    .line 159
    iget-object v0, p0, Laej;->aa:Laen;

    invoke-virtual {v0, p1}, Laen;->b(Laee;)V

    .line 161
    :cond_1
    return-void
.end method

.method static synthetic a(Laej;Laee;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1}, Laej;->a(Laee;)V

    return-void
.end method

.method private b(Z)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 167
    iget-object v0, p0, Laej;->Z:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    sget v0, Lh;->aU:I

    invoke-virtual {p0, v0}, Laej;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 174
    :goto_0
    return-object v0

    .line 171
    :cond_0
    if-eqz p1, :cond_1

    .line 172
    sget v0, Lh;->aR:I

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Laej;->Z:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Laej;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 174
    :cond_1
    sget v0, Lh;->aQ:I

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Laej;->Z:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Laej;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public f()Landroid/app/Dialog;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/high16 v8, 0x1040000

    const/4 v5, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 68
    invoke-virtual {p0}, Laej;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laej;->Z:Ljava/lang/String;

    .line 69
    invoke-virtual {p0}, Laej;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "contacts"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Laej;->Y:Ljava/util/ArrayList;

    .line 74
    invoke-virtual {p0}, Laej;->getTargetFragment()Lt;

    move-result-object v0

    check-cast v0, Laeo;

    .line 75
    if-eqz v0, :cond_0

    .line 76
    invoke-interface {v0}, Laeo;->c()Laen;

    move-result-object v0

    iput-object v0, p0, Laej;->aa:Laen;

    .line 79
    :cond_0
    iget-object v0, p0, Laej;->Y:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v7, :cond_1

    .line 80
    iget-object v2, p0, Laej;->Y:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Laej;->getActivity()Ly;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Laej;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v4, Lf;->eA:I

    invoke-virtual {v0, v4, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    sget v0, Lg;->aC:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    new-instance v4, Laei;

    invoke-virtual {p0}, Laej;->getActivity()Ly;

    move-result-object v5

    invoke-direct {v4, v5, v2}, Laei;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v4, p0, Laej;->ab:Laei;

    iget-object v2, p0, Laej;->ab:Laei;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-direct {p0, v7}, Laej;->b(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0, v8}, Laej;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Laem;

    invoke-direct {v2, p0}, Laem;-><init>(Laej;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Laej;->d()V

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 82
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Laej;->Y:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laee;

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Laej;->getActivity()Ly;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v6}, Laej;->b(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v4, p0, Laej;->Z:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    instance-of v4, v0, Laef;

    if-eqz v4, :cond_2

    move-object v1, v0

    check-cast v1, Laef;

    iget-object v1, v1, Laef;->a:Ljava/lang/String;

    sget v4, Lh;->aS:I

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v1, v5, v6

    aput-object v1, v5, v7

    invoke-virtual {p0, v4, v5}, Laej;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v3, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v3, Lh;->aP:I

    invoke-virtual {p0, v3}, Laej;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lael;

    invoke-direct {v4, p0, v0}, Lael;-><init>(Laej;Laee;)V

    invoke-virtual {v1, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0, v8}, Laej;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Laek;

    invoke-direct {v3, p0}, Laek;-><init>(Laej;)V

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :cond_2
    instance-of v4, v0, Laeh;

    if-eqz v4, :cond_5

    move-object v1, v0

    check-cast v1, Laeh;

    iget-object v1, v1, Laeh;->a:Ljava/lang/String;

    sget v4, Lh;->aT:I

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v1, v5, v6

    aput-object v1, v5, v7

    invoke-virtual {p0, v4, v5}, Laej;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_3
    instance-of v4, v0, Laef;

    if-eqz v4, :cond_4

    sget v1, Lh;->aM:I

    new-array v4, v5, [Ljava/lang/Object;

    iget-object v5, p0, Laej;->Z:Ljava/lang/String;

    aput-object v5, v4, v6

    iget-object v5, p0, Laej;->Z:Ljava/lang/String;

    aput-object v5, v4, v7

    invoke-virtual {p0, v1, v4}, Laej;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_4
    instance-of v4, v0, Laeh;

    if-eqz v4, :cond_5

    sget v1, Lh;->aN:I

    new-array v4, v5, [Ljava/lang/Object;

    iget-object v5, p0, Laej;->Z:Ljava/lang/String;

    aput-object v5, v4, v6

    iget-object v5, p0, Laej;->Z:Ljava/lang/String;

    aput-object v5, v4, v7

    invoke-virtual {p0, v1, v4}, Laej;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_5
    const-string v4, "Babel"

    const-string v5, "ContactInviteDialogFragment: couldn\'t get valid invite message"

    invoke-static {v4, v5}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 150
    iget-object v0, p0, Laej;->ab:Laei;

    invoke-virtual {v0, p3}, Laei;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laee;

    invoke-direct {p0, v0}, Laej;->a(Laee;)V

    .line 151
    return-void
.end method
