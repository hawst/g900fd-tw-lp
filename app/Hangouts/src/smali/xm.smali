.class public final Lxm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lxs;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lxo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lxn;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iget-object v0, p1, Lxn;->a:Ljava/util/List;

    iput-object v0, p0, Lxm;->a:Ljava/util/List;

    .line 22
    iget-object v0, p1, Lxn;->b:Ljava/util/List;

    iput-object v0, p0, Lxm;->b:Ljava/util/List;

    .line 23
    return-void
.end method

.method synthetic constructor <init>(Lxn;B)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lxm;-><init>(Lxn;)V

    return-void
.end method

.method public static newBuilder()Lxn;
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lxn;

    invoke-direct {v0}, Lxn;-><init>()V

    return-object v0
.end method

.method public static newBuilder(Lxm;)Lxn;
    .locals 1

    .prologue
    .line 30
    invoke-static {}, Lxm;->newBuilder()Lxn;

    move-result-object v0

    invoke-virtual {v0, p0}, Lxn;->a(Lxm;)Lxn;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lxs;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lxm;->a:Ljava/util/List;

    return-object v0
.end method

.method public a(I)Lxs;
    .locals 1

    .prologue
    .line 46
    if-ltz p1, :cond_0

    iget-object v0, p0, Lxm;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 47
    :cond_0
    const/4 v0, 0x0

    .line 49
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lxm;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxs;

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lxm;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public b(I)Lxo;
    .locals 1

    .prologue
    .line 61
    if-ltz p1, :cond_0

    iget-object v0, p0, Lxm;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 62
    :cond_0
    const/4 v0, 0x0

    .line 64
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lxm;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxo;

    goto :goto_0
.end method

.method public c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lxo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lxm;->b:Ljava/util/List;

    return-object v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lxm;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
