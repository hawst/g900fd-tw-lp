.class final Llb;
.super Lkh;
.source "PG"

# interfaces
.implements Landroid/app/ActionBar$TabListener;


# instance fields
.field final a:Landroid/app/ActionBar$Tab;

.field final synthetic b:Lkz;

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/CharSequence;

.field private e:Lki;


# direct methods
.method public constructor <init>(Lkz;Landroid/app/ActionBar$Tab;)V
    .locals 0

    .prologue
    .line 455
    iput-object p1, p0, Llb;->b:Lkz;

    invoke-direct {p0}, Lkh;-><init>()V

    .line 456
    iput-object p2, p0, Llb;->a:Landroid/app/ActionBar$Tab;

    .line 457
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 461
    iget-object v0, p0, Llb;->a:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0}, Landroid/app/ActionBar$Tab;->getPosition()I

    move-result v0

    return v0
.end method

.method public a(I)Lkh;
    .locals 1

    .prologue
    .line 540
    iget-object v0, p0, Llb;->b:Lkz;

    iget-object v0, v0, Lkz;->a:Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Llb;->d:Ljava/lang/CharSequence;

    .line 541
    return-object p0
.end method

.method public a(Landroid/view/View;)Lkh;
    .locals 1

    .prologue
    .line 500
    iget-object v0, p0, Llb;->a:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar$Tab;->setCustomView(Landroid/view/View;)Landroid/app/ActionBar$Tab;

    .line 501
    return-object p0
.end method

.method public a(Ljava/lang/Object;)Lkh;
    .locals 0

    .prologue
    .line 517
    iput-object p1, p0, Llb;->c:Ljava/lang/Object;

    .line 518
    return-object p0
.end method

.method public a(Lki;)Lkh;
    .locals 2

    .prologue
    .line 528
    iput-object p1, p0, Llb;->e:Lki;

    .line 529
    iget-object v1, p0, Llb;->a:Landroid/app/ActionBar$Tab;

    if-eqz p1, :cond_0

    move-object v0, p0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    .line 530
    return-object p0

    .line 529
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 466
    iget-object v0, p0, Llb;->a:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0}, Landroid/app/ActionBar$Tab;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 471
    iget-object v0, p0, Llb;->a:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0}, Landroid/app/ActionBar$Tab;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public d()Landroid/view/View;
    .locals 1

    .prologue
    .line 512
    iget-object v0, p0, Llb;->a:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0}, Landroid/app/ActionBar$Tab;->getCustomView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 523
    iget-object v0, p0, Llb;->c:Ljava/lang/Object;

    return-object v0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 535
    iget-object v0, p0, Llb;->a:Landroid/app/ActionBar$Tab;

    invoke-virtual {v0}, Landroid/app/ActionBar$Tab;->select()V

    .line 536
    return-void
.end method

.method public g()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 552
    iget-object v0, p0, Llb;->d:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public onTabReselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 2

    .prologue
    .line 571
    iget-object v0, p0, Llb;->e:Lki;

    if-eqz p2, :cond_0

    iget-object v1, p0, Llb;->b:Lkz;

    invoke-virtual {v1}, Lkz;->i()Lao;

    :cond_0
    invoke-interface {v0}, Lki;->b()V

    .line 572
    iget-object v0, p0, Llb;->b:Lkz;

    invoke-virtual {v0}, Lkz;->j()V

    .line 573
    return-void
.end method

.method public onTabSelected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 2

    .prologue
    .line 558
    iget-object v0, p0, Llb;->e:Lki;

    if-eqz p2, :cond_0

    iget-object v1, p0, Llb;->b:Lkz;

    invoke-virtual {v1}, Lkz;->i()Lao;

    :cond_0
    invoke-interface {v0, p0}, Lki;->a(Lkh;)V

    .line 559
    iget-object v0, p0, Llb;->b:Lkz;

    invoke-virtual {v0}, Lkz;->j()V

    .line 560
    return-void
.end method

.method public onTabUnselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 2

    .prologue
    .line 565
    iget-object v0, p0, Llb;->e:Lki;

    if-eqz p2, :cond_0

    iget-object v1, p0, Llb;->b:Lkz;

    invoke-virtual {v1}, Lkz;->i()Lao;

    :cond_0
    invoke-interface {v0}, Lki;->a()V

    .line 566
    return-void
.end method
