.class public final Ldzj;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldzj;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ldoc;

.field public g:Ldol;

.field public h:Ldon;

.field public i:Ldoq;

.field public j:Ldos;

.field public k:Ldok;

.field public l:[Ljava/lang/String;

.field public m:Ljava/lang/Boolean;

.field public n:Ljava/lang/Integer;

.field public o:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Ldzj;

    sput-object v0, Ldzj;->a:[Ldzj;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 31
    iput-object v1, p0, Ldzj;->f:Ldoc;

    .line 34
    iput-object v1, p0, Ldzj;->g:Ldol;

    .line 37
    iput-object v1, p0, Ldzj;->h:Ldon;

    .line 40
    iput-object v1, p0, Ldzj;->i:Ldoq;

    .line 43
    iput-object v1, p0, Ldzj;->j:Ldos;

    .line 46
    iput-object v1, p0, Ldzj;->k:Ldok;

    .line 49
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Ldzj;->l:[Ljava/lang/String;

    .line 54
    iput-object v1, p0, Ldzj;->n:Ljava/lang/Integer;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 112
    iget-object v0, p0, Ldzj;->b:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 113
    const/4 v0, 0x1

    iget-object v2, p0, Ldzj;->b:Ljava/lang/String;

    .line 114
    invoke-static {v0, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 116
    :goto_0
    iget-object v2, p0, Ldzj;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 117
    const/4 v2, 0x2

    iget-object v3, p0, Ldzj;->c:Ljava/lang/String;

    .line 118
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 120
    :cond_0
    iget-object v2, p0, Ldzj;->e:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 121
    const/4 v2, 0x3

    iget-object v3, p0, Ldzj;->e:Ljava/lang/String;

    .line 122
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 124
    :cond_1
    iget-object v2, p0, Ldzj;->f:Ldoc;

    if-eqz v2, :cond_2

    .line 125
    const/4 v2, 0x4

    iget-object v3, p0, Ldzj;->f:Ldoc;

    .line 126
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 128
    :cond_2
    iget-object v2, p0, Ldzj;->g:Ldol;

    if-eqz v2, :cond_3

    .line 129
    const/4 v2, 0x5

    iget-object v3, p0, Ldzj;->g:Ldol;

    .line 130
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 132
    :cond_3
    iget-object v2, p0, Ldzj;->h:Ldon;

    if-eqz v2, :cond_4

    .line 133
    const/4 v2, 0x6

    iget-object v3, p0, Ldzj;->h:Ldon;

    .line 134
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 136
    :cond_4
    iget-object v2, p0, Ldzj;->i:Ldoq;

    if-eqz v2, :cond_5

    .line 137
    const/4 v2, 0x7

    iget-object v3, p0, Ldzj;->i:Ldoq;

    .line 138
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 140
    :cond_5
    iget-object v2, p0, Ldzj;->j:Ldos;

    if-eqz v2, :cond_6

    .line 141
    const/16 v2, 0x8

    iget-object v3, p0, Ldzj;->j:Ldos;

    .line 142
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 144
    :cond_6
    iget-object v2, p0, Ldzj;->n:Ljava/lang/Integer;

    if-eqz v2, :cond_7

    .line 145
    const/16 v2, 0x9

    iget-object v3, p0, Ldzj;->n:Ljava/lang/Integer;

    .line 146
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 148
    :cond_7
    iget-object v2, p0, Ldzj;->k:Ldok;

    if-eqz v2, :cond_8

    .line 149
    const/16 v2, 0xa

    iget-object v3, p0, Ldzj;->k:Ldok;

    .line 150
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 152
    :cond_8
    iget-object v2, p0, Ldzj;->l:[Ljava/lang/String;

    if-eqz v2, :cond_a

    iget-object v2, p0, Ldzj;->l:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_a

    .line 154
    iget-object v3, p0, Ldzj;->l:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_9

    aget-object v5, v3, v1

    .line 156
    invoke-static {v5}, Lepl;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 154
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 158
    :cond_9
    add-int/2addr v0, v2

    .line 159
    iget-object v1, p0, Ldzj;->l:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 161
    :cond_a
    iget-object v1, p0, Ldzj;->m:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    .line 162
    const/16 v1, 0xc

    iget-object v2, p0, Ldzj;->m:Ljava/lang/Boolean;

    .line 163
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 165
    :cond_b
    iget-object v1, p0, Ldzj;->o:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 166
    const/16 v1, 0xd

    iget-object v2, p0, Ldzj;->o:Ljava/lang/String;

    .line 167
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 169
    :cond_c
    iget-object v1, p0, Ldzj;->d:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 170
    const/16 v1, 0xe

    iget-object v2, p0, Ldzj;->d:Ljava/lang/String;

    .line 171
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 173
    :cond_d
    iget-object v1, p0, Ldzj;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 174
    iput v0, p0, Ldzj;->cachedSize:I

    .line 175
    return v0

    :cond_e
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 5

    .prologue
    const/16 v4, 0x25

    const/4 v3, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldzj;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldzj;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldzj;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldzj;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldzj;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldzj;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldzj;->f:Ldoc;

    if-nez v0, :cond_2

    new-instance v0, Ldoc;

    invoke-direct {v0}, Ldoc;-><init>()V

    iput-object v0, p0, Ldzj;->f:Ldoc;

    :cond_2
    iget-object v0, p0, Ldzj;->f:Ldoc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ldzj;->g:Ldol;

    if-nez v0, :cond_3

    new-instance v0, Ldol;

    invoke-direct {v0}, Ldol;-><init>()V

    iput-object v0, p0, Ldzj;->g:Ldol;

    :cond_3
    iget-object v0, p0, Ldzj;->g:Ldol;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Ldzj;->h:Ldon;

    if-nez v0, :cond_4

    new-instance v0, Ldon;

    invoke-direct {v0}, Ldon;-><init>()V

    iput-object v0, p0, Ldzj;->h:Ldon;

    :cond_4
    iget-object v0, p0, Ldzj;->h:Ldon;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Ldzj;->i:Ldoq;

    if-nez v0, :cond_5

    new-instance v0, Ldoq;

    invoke-direct {v0}, Ldoq;-><init>()V

    iput-object v0, p0, Ldzj;->i:Ldoq;

    :cond_5
    iget-object v0, p0, Ldzj;->i:Ldoq;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Ldzj;->j:Ldos;

    if-nez v0, :cond_6

    new-instance v0, Ldos;

    invoke-direct {v0}, Ldos;-><init>()V

    iput-object v0, p0, Ldzj;->j:Ldos;

    :cond_6
    iget-object v0, p0, Ldzj;->j:Ldos;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v4, :cond_7

    const/16 v1, 0x2a

    if-eq v0, v1, :cond_7

    const/16 v1, 0x2b

    if-eq v0, v1, :cond_7

    const/16 v1, 0x37

    if-eq v0, v1, :cond_7

    const/16 v1, 0x3a

    if-eq v0, v1, :cond_7

    const/16 v1, 0x3b

    if-eq v0, v1, :cond_7

    const/16 v1, 0x3c

    if-ne v0, v1, :cond_8

    :cond_7
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldzj;->n:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_8
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldzj;->n:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Ldzj;->k:Ldok;

    if-nez v0, :cond_9

    new-instance v0, Ldok;

    invoke-direct {v0}, Ldok;-><init>()V

    iput-object v0, p0, Ldzj;->k:Ldok;

    :cond_9
    iget-object v0, p0, Ldzj;->k:Ldok;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_b
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Ldzj;->l:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    iget-object v2, p0, Ldzj;->l:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Ldzj;->l:[Ljava/lang/String;

    :goto_1
    iget-object v1, p0, Ldzj;->l:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_a

    iget-object v1, p0, Ldzj;->l:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_a
    iget-object v1, p0, Ldzj;->l:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldzj;->m:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldzj;->o:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldzj;->d:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 61
    iget-object v0, p0, Ldzj;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 62
    const/4 v0, 0x1

    iget-object v1, p0, Ldzj;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 64
    :cond_0
    iget-object v0, p0, Ldzj;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 65
    const/4 v0, 0x2

    iget-object v1, p0, Ldzj;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 67
    :cond_1
    iget-object v0, p0, Ldzj;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 68
    const/4 v0, 0x3

    iget-object v1, p0, Ldzj;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 70
    :cond_2
    iget-object v0, p0, Ldzj;->f:Ldoc;

    if-eqz v0, :cond_3

    .line 71
    const/4 v0, 0x4

    iget-object v1, p0, Ldzj;->f:Ldoc;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 73
    :cond_3
    iget-object v0, p0, Ldzj;->g:Ldol;

    if-eqz v0, :cond_4

    .line 74
    const/4 v0, 0x5

    iget-object v1, p0, Ldzj;->g:Ldol;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 76
    :cond_4
    iget-object v0, p0, Ldzj;->h:Ldon;

    if-eqz v0, :cond_5

    .line 77
    const/4 v0, 0x6

    iget-object v1, p0, Ldzj;->h:Ldon;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 79
    :cond_5
    iget-object v0, p0, Ldzj;->i:Ldoq;

    if-eqz v0, :cond_6

    .line 80
    const/4 v0, 0x7

    iget-object v1, p0, Ldzj;->i:Ldoq;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 82
    :cond_6
    iget-object v0, p0, Ldzj;->j:Ldos;

    if-eqz v0, :cond_7

    .line 83
    const/16 v0, 0x8

    iget-object v1, p0, Ldzj;->j:Ldos;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 85
    :cond_7
    iget-object v0, p0, Ldzj;->n:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 86
    const/16 v0, 0x9

    iget-object v1, p0, Ldzj;->n:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 88
    :cond_8
    iget-object v0, p0, Ldzj;->k:Ldok;

    if-eqz v0, :cond_9

    .line 89
    const/16 v0, 0xa

    iget-object v1, p0, Ldzj;->k:Ldok;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 91
    :cond_9
    iget-object v0, p0, Ldzj;->l:[Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 92
    iget-object v1, p0, Ldzj;->l:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_a

    aget-object v3, v1, v0

    .line 93
    const/16 v4, 0xb

    invoke-virtual {p1, v4, v3}, Lepl;->a(ILjava/lang/String;)V

    .line 92
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 96
    :cond_a
    iget-object v0, p0, Ldzj;->m:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    .line 97
    const/16 v0, 0xc

    iget-object v1, p0, Ldzj;->m:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 99
    :cond_b
    iget-object v0, p0, Ldzj;->o:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 100
    const/16 v0, 0xd

    iget-object v1, p0, Ldzj;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 102
    :cond_c
    iget-object v0, p0, Ldzj;->d:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 103
    const/16 v0, 0xe

    iget-object v1, p0, Ldzj;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 105
    :cond_d
    iget-object v0, p0, Ldzj;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 107
    return-void
.end method
