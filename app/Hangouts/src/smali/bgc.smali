.class public Lbgc;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 3574
    invoke-direct {p0}, Lbfz;-><init>()V

    .line 3575
    return-void
.end method

.method private constructor <init>(Ldtz;)V
    .locals 3

    .prologue
    .line 3563
    iget-object v0, p1, Ldtz;->b:Ldvn;

    const-wide/16 v1, 0x0

    invoke-direct {p0, v0, v1, v2}, Lbfz;-><init>(Ldvn;J)V

    .line 3564
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_0

    .line 3565
    const-string v0, "Babel_protos"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ArchiveConversationResponse from:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3568
    :cond_0
    return-void
.end method

.method public static f()Lbgc;
    .locals 1

    .prologue
    .line 3584
    new-instance v0, Lbgc;

    invoke-direct {v0}, Lbgc;-><init>()V

    return-object v0
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 3589
    new-instance v0, Ldtz;

    invoke-direct {v0}, Ldtz;-><init>()V

    .line 3590
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldtz;

    .line 3591
    iget-object v1, v0, Ldtz;->b:Ldvn;

    invoke-static {v1}, Lbgc;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3592
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldtz;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 3594
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbgc;

    invoke-direct {v1, v0}, Lbgc;-><init>(Ldtz;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Lyt;Lbnl;)V
    .locals 0

    .prologue
    .line 3601
    invoke-super {p0, p1, p2}, Lbfz;->a(Lyt;Lbnl;)V

    .line 3605
    return-void
.end method
