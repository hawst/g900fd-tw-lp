.class public Lbfj;
.super Lbed;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:Z

.field public final b:I

.field public final i:Ljava/lang/String;

.field public final j:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;ZI)V
    .locals 1

    .prologue
    .line 2260
    invoke-direct {p0, p1}, Lbed;-><init>(Ljava/lang/String;)V

    .line 2261
    iput-boolean p2, p0, Lbfj;->a:Z

    .line 2262
    const/4 v0, 0x0

    iput-object v0, p0, Lbfj;->i:Ljava/lang/String;

    .line 2263
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbfj;->j:Z

    .line 2264
    if-lez p3, :cond_0

    :goto_0
    iput p3, p0, Lbfj;->b:I

    .line 2265
    return-void

    .line 2264
    :cond_0
    const/16 p3, 0x12c

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 2269
    sget-boolean v0, Lbea;->d:Z

    if-eqz v0, :cond_0

    .line 2270
    const-string v0, "Babel_RequestWriter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setFocus build protobuf "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lbfj;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " isFocused: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lbfj;->a:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2274
    :cond_0
    new-instance v2, Ldwg;

    invoke-direct {v2}, Ldwg;-><init>()V

    .line 2275
    invoke-virtual {p0, p1, p2}, Lbfj;->c(Ljava/lang/String;I)Ldvm;

    move-result-object v0

    iput-object v0, v2, Ldwg;->b:Ldvm;

    .line 2276
    iget-object v0, p0, Lbfj;->c:Ljava/lang/String;

    invoke-static {v0}, Lbea;->d(Ljava/lang/String;)Ldqf;

    move-result-object v0

    iput-object v0, v2, Ldwg;->c:Ldqf;

    .line 2277
    iget-boolean v0, p0, Lbfj;->a:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v2, Ldwg;->d:Ljava/lang/Integer;

    .line 2279
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v2, Ldwg;->f:Ljava/lang/Boolean;

    .line 2280
    iget v0, p0, Lbfj;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v2, Ldwg;->e:Ljava/lang/Integer;

    .line 2282
    return-object v2

    .line 2277
    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public a(Lbea;)Z
    .locals 2

    .prologue
    .line 2303
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2304
    const/4 v0, 0x1

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2287
    const-string v0, "conversations/setfocus"

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2298
    const-string v0, "background_queue"

    return-object v0
.end method

.method public d()J
    .locals 3

    .prologue
    .line 2293
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x1e

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    return-wide v0
.end method
