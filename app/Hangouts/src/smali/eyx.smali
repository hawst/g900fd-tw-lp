.class public final Leyx;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leyx;


# instance fields
.field public b:Leyy;

.field public c:Leyz;

.field public d:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 289
    const/4 v0, 0x0

    new-array v0, v0, [Leyx;

    sput-object v0, Leyx;->a:[Leyx;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 290
    invoke-direct {p0}, Lepn;-><init>()V

    .line 309
    iput-object v0, p0, Leyx;->b:Leyy;

    .line 312
    iput-object v0, p0, Leyx;->c:Leyz;

    .line 315
    iput-object v0, p0, Leyx;->d:Ljava/lang/Integer;

    .line 290
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 335
    const/4 v0, 0x0

    .line 336
    iget-object v1, p0, Leyx;->b:Leyy;

    if-eqz v1, :cond_0

    .line 337
    const/4 v0, 0x1

    iget-object v1, p0, Leyx;->b:Leyy;

    .line 338
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 340
    :cond_0
    iget-object v1, p0, Leyx;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 341
    const/4 v1, 0x2

    iget-object v2, p0, Leyx;->d:Ljava/lang/Integer;

    .line 342
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 344
    :cond_1
    iget-object v1, p0, Leyx;->c:Leyz;

    if-eqz v1, :cond_2

    .line 345
    const/4 v1, 0x3

    iget-object v2, p0, Leyx;->c:Leyz;

    .line 346
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 348
    :cond_2
    iget-object v1, p0, Leyx;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 349
    iput v0, p0, Leyx;->cachedSize:I

    .line 350
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 286
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Leyx;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Leyx;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Leyx;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Leyx;->b:Leyy;

    if-nez v0, :cond_2

    new-instance v0, Leyy;

    invoke-direct {v0}, Leyy;-><init>()V

    iput-object v0, p0, Leyx;->b:Leyy;

    :cond_2
    iget-object v0, p0, Leyx;->b:Leyy;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-eq v0, v1, :cond_3

    const/4 v1, 0x5

    if-eq v0, v1, :cond_3

    const/4 v1, 0x6

    if-eq v0, v1, :cond_3

    const/16 v1, 0x9

    if-eq v0, v1, :cond_3

    const/16 v1, 0xa

    if-eq v0, v1, :cond_3

    const/16 v1, 0xb

    if-eq v0, v1, :cond_3

    const/16 v1, 0xc

    if-eq v0, v1, :cond_3

    const/16 v1, 0xd

    if-eq v0, v1, :cond_3

    const/16 v1, 0x2d

    if-ne v0, v1, :cond_4

    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leyx;->d:Ljava/lang/Integer;

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leyx;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Leyx;->c:Leyz;

    if-nez v0, :cond_5

    new-instance v0, Leyz;

    invoke-direct {v0}, Leyz;-><init>()V

    iput-object v0, p0, Leyx;->c:Leyz;

    :cond_5
    iget-object v0, p0, Leyx;->c:Leyz;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 320
    iget-object v0, p0, Leyx;->b:Leyy;

    if-eqz v0, :cond_0

    .line 321
    const/4 v0, 0x1

    iget-object v1, p0, Leyx;->b:Leyy;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 323
    :cond_0
    iget-object v0, p0, Leyx;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 324
    const/4 v0, 0x2

    iget-object v1, p0, Leyx;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 326
    :cond_1
    iget-object v0, p0, Leyx;->c:Leyz;

    if-eqz v0, :cond_2

    .line 327
    const/4 v0, 0x3

    iget-object v1, p0, Leyx;->c:Leyz;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 329
    :cond_2
    iget-object v0, p0, Leyx;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 331
    return-void
.end method
