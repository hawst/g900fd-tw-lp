.class public final Lbcw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:Lbcx;

.field public final b:I

.field private final c:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ldtg;)V
    .locals 2

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Lbcx;

    iget-object v1, p1, Ldtg;->b:Ldth;

    invoke-direct {v0, v1}, Lbcx;-><init>(Ldth;)V

    iput-object v0, p0, Lbcw;->a:Lbcx;

    .line 20
    iget-object v0, p1, Ldtg;->c:Ljava/lang/String;

    iput-object v0, p0, Lbcw;->c:Ljava/lang/String;

    .line 21
    iget-object v0, p1, Ldtg;->d:Ljava/lang/Integer;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    iput v0, p0, Lbcw;->b:I

    .line 22
    return-void
.end method

.method public static a([Ldtg;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ldtg;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbcw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 26
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 27
    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p0, v0

    .line 28
    new-instance v4, Lbcw;

    invoke-direct {v4, v3}, Lbcw;-><init>(Ldtg;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 27
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 30
    :cond_0
    return-object v1
.end method
