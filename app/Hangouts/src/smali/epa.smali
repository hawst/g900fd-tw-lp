.class public final Lepa;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lepa;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Float;

.field public d:Ljava/lang/Float;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/Integer;

.field public h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4394
    const/4 v0, 0x0

    new-array v0, v0, [Lepa;

    sput-object v0, Lepa;->a:[Lepa;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4395
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 4441
    const/4 v0, 0x0

    .line 4442
    iget-object v1, p0, Lepa;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 4443
    const/4 v0, 0x1

    iget-object v1, p0, Lepa;->b:Ljava/lang/String;

    .line 4444
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4446
    :cond_0
    iget-object v1, p0, Lepa;->c:Ljava/lang/Float;

    if-eqz v1, :cond_1

    .line 4447
    const/4 v1, 0x2

    iget-object v2, p0, Lepa;->c:Ljava/lang/Float;

    .line 4448
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 4450
    :cond_1
    iget-object v1, p0, Lepa;->d:Ljava/lang/Float;

    if-eqz v1, :cond_2

    .line 4451
    const/4 v1, 0x3

    iget-object v2, p0, Lepa;->d:Ljava/lang/Float;

    .line 4452
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 4454
    :cond_2
    iget-object v1, p0, Lepa;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 4455
    const/4 v1, 0x4

    iget-object v2, p0, Lepa;->e:Ljava/lang/String;

    .line 4456
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4458
    :cond_3
    iget-object v1, p0, Lepa;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 4459
    const/4 v1, 0x5

    iget-object v2, p0, Lepa;->f:Ljava/lang/Integer;

    .line 4460
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4462
    :cond_4
    iget-object v1, p0, Lepa;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 4463
    const/4 v1, 0x6

    iget-object v2, p0, Lepa;->g:Ljava/lang/Integer;

    .line 4464
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4466
    :cond_5
    iget-object v1, p0, Lepa;->h:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 4467
    const/4 v1, 0x7

    iget-object v2, p0, Lepa;->h:Ljava/lang/String;

    .line 4468
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4470
    :cond_6
    iget-object v1, p0, Lepa;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4471
    iput v0, p0, Lepa;->cachedSize:I

    .line 4472
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 4391
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lepa;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lepa;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lepa;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepa;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lepa;->c:Ljava/lang/Float;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lepa;->d:Ljava/lang/Float;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepa;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lepa;->f:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lepa;->g:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepa;->h:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x15 -> :sswitch_2
        0x1d -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 4414
    iget-object v0, p0, Lepa;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 4415
    const/4 v0, 0x1

    iget-object v1, p0, Lepa;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 4417
    :cond_0
    iget-object v0, p0, Lepa;->c:Ljava/lang/Float;

    if-eqz v0, :cond_1

    .line 4418
    const/4 v0, 0x2

    iget-object v1, p0, Lepa;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IF)V

    .line 4420
    :cond_1
    iget-object v0, p0, Lepa;->d:Ljava/lang/Float;

    if-eqz v0, :cond_2

    .line 4421
    const/4 v0, 0x3

    iget-object v1, p0, Lepa;->d:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IF)V

    .line 4423
    :cond_2
    iget-object v0, p0, Lepa;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 4424
    const/4 v0, 0x4

    iget-object v1, p0, Lepa;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 4426
    :cond_3
    iget-object v0, p0, Lepa;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 4427
    const/4 v0, 0x5

    iget-object v1, p0, Lepa;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 4429
    :cond_4
    iget-object v0, p0, Lepa;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 4430
    const/4 v0, 0x6

    iget-object v1, p0, Lepa;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 4432
    :cond_5
    iget-object v0, p0, Lepa;->h:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 4433
    const/4 v0, 0x7

    iget-object v1, p0, Lepa;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 4435
    :cond_6
    iget-object v0, p0, Lepa;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 4437
    return-void
.end method
