.class public Lbex;
.super Lbei;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2140
    invoke-direct {p0, p2, p1}, Lbei;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2141
    iput-object p3, p0, Lbex;->a:Ljava/lang/String;

    .line 2142
    iput-object p4, p0, Lbex;->b:Ljava/lang/String;

    .line 2143
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 3

    .prologue
    .line 2147
    sget-boolean v0, Lbea;->d:Z

    if-eqz v0, :cond_0

    .line 2148
    const-string v0, "Babel_RequestWriter"

    const-string v1, "setConversationName build protobuf"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2150
    :cond_0
    new-instance v0, Ldrx;

    invoke-direct {v0}, Ldrx;-><init>()V

    .line 2152
    iget-object v1, p0, Lbex;->i:Ljava/lang/String;

    .line 2153
    invoke-static {v1}, Lyt;->c(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Ldrx;->c:Ljava/lang/Long;

    .line 2154
    iget-object v1, p0, Lbex;->c:Ljava/lang/String;

    invoke-static {v1}, Lbea;->d(Ljava/lang/String;)Ldqf;

    move-result-object v1

    iput-object v1, v0, Ldrx;->b:Ldqf;

    .line 2155
    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Ldrx;->f:Ljava/lang/Integer;

    .line 2157
    new-instance v1, Ldvh;

    invoke-direct {v1}, Ldvh;-><init>()V

    .line 2160
    invoke-virtual {p0, p1, p2}, Lbex;->c(Ljava/lang/String;I)Ldvm;

    move-result-object v2

    iput-object v2, v1, Ldvh;->b:Ldvm;

    .line 2161
    iget-object v2, p0, Lbex;->a:Ljava/lang/String;

    iput-object v2, v1, Ldvh;->d:Ljava/lang/String;

    .line 2162
    iput-object v0, v1, Ldvh;->c:Ldrx;

    .line 2164
    return-object v1
.end method

.method public a(Lyj;Lbph;)V
    .locals 3

    .prologue
    .line 2174
    invoke-super {p0, p1, p2}, Lbei;->a(Lyj;Lbph;)V

    .line 2175
    invoke-virtual {p1}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lbex;->c:Ljava/lang/String;

    iget-object v2, p0, Lbex;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2177
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2169
    const-string v0, "conversations/renameconversation"

    return-object v0
.end method
