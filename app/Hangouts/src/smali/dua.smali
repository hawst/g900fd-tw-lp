.class public final Ldua;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldua;


# instance fields
.field public b:Ldvm;

.field public c:Ldrx;

.field public d:Ljava/lang/Integer;

.field public e:[B

.field public f:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10927
    const/4 v0, 0x0

    new-array v0, v0, [Ldua;

    sput-object v0, Ldua;->a:[Ldua;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10928
    invoke-direct {p0}, Lepn;-><init>()V

    .line 10931
    iput-object v0, p0, Ldua;->b:Ldvm;

    .line 10934
    iput-object v0, p0, Ldua;->c:Ldrx;

    .line 10937
    iput-object v0, p0, Ldua;->d:Ljava/lang/Integer;

    .line 10928
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 10967
    const/4 v0, 0x0

    .line 10968
    iget-object v1, p0, Ldua;->b:Ldvm;

    if-eqz v1, :cond_0

    .line 10969
    const/4 v0, 0x1

    iget-object v1, p0, Ldua;->b:Ldvm;

    .line 10970
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 10972
    :cond_0
    iget-object v1, p0, Ldua;->e:[B

    if-eqz v1, :cond_1

    .line 10973
    const/4 v1, 0x2

    iget-object v2, p0, Ldua;->e:[B

    .line 10974
    invoke-static {v1, v2}, Lepl;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 10976
    :cond_1
    iget-object v1, p0, Ldua;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 10977
    const/4 v1, 0x3

    iget-object v2, p0, Ldua;->d:Ljava/lang/Integer;

    .line 10978
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 10980
    :cond_2
    iget-object v1, p0, Ldua;->f:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 10981
    const/4 v1, 0x4

    iget-object v2, p0, Ldua;->f:Ljava/lang/Long;

    .line 10982
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 10984
    :cond_3
    iget-object v1, p0, Ldua;->c:Ldrx;

    if-eqz v1, :cond_4

    .line 10985
    const/4 v1, 0x5

    iget-object v2, p0, Ldua;->c:Ldrx;

    .line 10986
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10988
    :cond_4
    iget-object v1, p0, Ldua;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10989
    iput v0, p0, Ldua;->cachedSize:I

    .line 10990
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 10924
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldua;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldua;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldua;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldua;->b:Ldvm;

    if-nez v0, :cond_2

    new-instance v0, Ldvm;

    invoke-direct {v0}, Ldvm;-><init>()V

    iput-object v0, p0, Ldua;->b:Ldvm;

    :cond_2
    iget-object v0, p0, Ldua;->b:Ldvm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v0

    iput-object v0, p0, Ldua;->e:[B

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_3

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldua;->d:Ljava/lang/Integer;

    goto :goto_0

    :cond_4
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldua;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldua;->f:Ljava/lang/Long;

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ldua;->c:Ldrx;

    if-nez v0, :cond_5

    new-instance v0, Ldrx;

    invoke-direct {v0}, Ldrx;-><init>()V

    iput-object v0, p0, Ldua;->c:Ldrx;

    :cond_5
    iget-object v0, p0, Ldua;->c:Ldrx;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 10946
    iget-object v0, p0, Ldua;->b:Ldvm;

    if-eqz v0, :cond_0

    .line 10947
    const/4 v0, 0x1

    iget-object v1, p0, Ldua;->b:Ldvm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 10949
    :cond_0
    iget-object v0, p0, Ldua;->e:[B

    if-eqz v0, :cond_1

    .line 10950
    const/4 v0, 0x2

    iget-object v1, p0, Ldua;->e:[B

    invoke-virtual {p1, v0, v1}, Lepl;->a(I[B)V

    .line 10952
    :cond_1
    iget-object v0, p0, Ldua;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 10953
    const/4 v0, 0x3

    iget-object v1, p0, Ldua;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 10955
    :cond_2
    iget-object v0, p0, Ldua;->f:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 10956
    const/4 v0, 0x4

    iget-object v1, p0, Ldua;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 10958
    :cond_3
    iget-object v0, p0, Ldua;->c:Ldrx;

    if-eqz v0, :cond_4

    .line 10959
    const/4 v0, 0x5

    iget-object v1, p0, Ldua;->c:Ldrx;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 10961
    :cond_4
    iget-object v0, p0, Ldua;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 10963
    return-void
.end method
