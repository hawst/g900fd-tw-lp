.class final Lakd;
.super Lakb;
.source "PG"


# instance fields
.field final synthetic a:Lakc;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final c:I


# direct methods
.method public constructor <init>(Lakc;Landroid/content/res/Resources;Ljava/util/List;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 350
    iput-object p1, p0, Lakd;->a:Lakc;

    .line 351
    invoke-static {p1}, Lakc;->a(Lakc;)Lbyh;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lakb;-><init>(Landroid/content/res/Resources;Lbyh;)V

    .line 352
    iput-object p3, p0, Lakd;->b:Ljava/util/List;

    .line 353
    iput p4, p0, Lakd;->c:I

    .line 354
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 3

    .prologue
    .line 358
    iget v0, p0, Lakd;->c:I

    iget-object v1, p0, Lakd;->a:Lakc;

    invoke-static {v1}, Lakc;->c(Lakc;)I

    move-result v1

    mul-int/2addr v0, v1

    .line 359
    iget-object v1, p0, Lakd;->a:Lakc;

    invoke-static {v1}, Lakc;->c(Lakc;)I

    move-result v1

    iget-object v2, p0, Lakd;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    sub-int v0, v2, v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 360
    return v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 365
    iget v0, p0, Lakd;->c:I

    iget-object v1, p0, Lakd;->a:Lakc;

    invoke-static {v1}, Lakc;->c(Lakc;)I

    move-result v1

    mul-int/2addr v0, v1

    .line 366
    iget-object v1, p0, Lakd;->b:Ljava/util/List;

    add-int/2addr v0, p1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method
