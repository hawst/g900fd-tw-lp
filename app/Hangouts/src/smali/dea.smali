.class public final Ldea;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldea;


# instance fields
.field public b:Ljava/lang/Boolean;

.field public c:Ljava/lang/Boolean;

.field public d:Ljava/lang/Boolean;

.field public e:Ljava/lang/Boolean;

.field public f:Ljava/lang/Boolean;

.field public g:Ljava/lang/Boolean;

.field public h:Ljava/lang/Boolean;

.field public i:Ljava/lang/Boolean;

.field public j:Ljava/lang/Boolean;

.field public k:Ljava/lang/Integer;

.field public l:Ljava/lang/Integer;

.field public m:Ljava/lang/Boolean;

.field public n:Ljava/lang/Boolean;

.field public o:Ljava/lang/Boolean;

.field public p:Lddy;

.field public q:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 203
    const/4 v0, 0x0

    new-array v0, v0, [Ldea;

    sput-object v0, Ldea;->a:[Ldea;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 204
    invoke-direct {p0}, Lepn;-><init>()V

    .line 225
    iput-object v0, p0, Ldea;->k:Ljava/lang/Integer;

    .line 228
    iput-object v0, p0, Ldea;->l:Ljava/lang/Integer;

    .line 237
    iput-object v0, p0, Ldea;->p:Lddy;

    .line 204
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 298
    const/4 v0, 0x0

    .line 299
    iget-object v1, p0, Ldea;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 300
    const/4 v0, 0x1

    iget-object v1, p0, Ldea;->b:Ljava/lang/Boolean;

    .line 301
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Lepl;->g(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 303
    :cond_0
    iget-object v1, p0, Ldea;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 304
    const/4 v1, 0x2

    iget-object v2, p0, Ldea;->c:Ljava/lang/Boolean;

    .line 305
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 307
    :cond_1
    iget-object v1, p0, Ldea;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 308
    const/4 v1, 0x3

    iget-object v2, p0, Ldea;->d:Ljava/lang/Boolean;

    .line 309
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 311
    :cond_2
    iget-object v1, p0, Ldea;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 312
    const/4 v1, 0x4

    iget-object v2, p0, Ldea;->e:Ljava/lang/Boolean;

    .line 313
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 315
    :cond_3
    iget-object v1, p0, Ldea;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 316
    const/4 v1, 0x5

    iget-object v2, p0, Ldea;->f:Ljava/lang/Boolean;

    .line 317
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 319
    :cond_4
    iget-object v1, p0, Ldea;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 320
    const/4 v1, 0x6

    iget-object v2, p0, Ldea;->g:Ljava/lang/Boolean;

    .line 321
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 323
    :cond_5
    iget-object v1, p0, Ldea;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 324
    const/4 v1, 0x7

    iget-object v2, p0, Ldea;->h:Ljava/lang/Boolean;

    .line 325
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 327
    :cond_6
    iget-object v1, p0, Ldea;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 328
    const/16 v1, 0x8

    iget-object v2, p0, Ldea;->i:Ljava/lang/Boolean;

    .line 329
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 331
    :cond_7
    iget-object v1, p0, Ldea;->j:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 332
    const/16 v1, 0x9

    iget-object v2, p0, Ldea;->j:Ljava/lang/Boolean;

    .line 333
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 335
    :cond_8
    iget-object v1, p0, Ldea;->k:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 336
    const/16 v1, 0xa

    iget-object v2, p0, Ldea;->k:Ljava/lang/Integer;

    .line 337
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 339
    :cond_9
    iget-object v1, p0, Ldea;->l:Ljava/lang/Integer;

    if-eqz v1, :cond_a

    .line 340
    const/16 v1, 0xb

    iget-object v2, p0, Ldea;->l:Ljava/lang/Integer;

    .line 341
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 343
    :cond_a
    iget-object v1, p0, Ldea;->m:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    .line 344
    const/16 v1, 0xc

    iget-object v2, p0, Ldea;->m:Ljava/lang/Boolean;

    .line 345
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 347
    :cond_b
    iget-object v1, p0, Ldea;->n:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    .line 348
    const/16 v1, 0xd

    iget-object v2, p0, Ldea;->n:Ljava/lang/Boolean;

    .line 349
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 351
    :cond_c
    iget-object v1, p0, Ldea;->o:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    .line 352
    const/16 v1, 0xe

    iget-object v2, p0, Ldea;->o:Ljava/lang/Boolean;

    .line 353
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 355
    :cond_d
    iget-object v1, p0, Ldea;->p:Lddy;

    if-eqz v1, :cond_e

    .line 356
    const/16 v1, 0xf

    iget-object v2, p0, Ldea;->p:Lddy;

    .line 357
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 359
    :cond_e
    iget-object v1, p0, Ldea;->q:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    .line 360
    const/16 v1, 0x10

    iget-object v2, p0, Ldea;->q:Ljava/lang/Boolean;

    .line 361
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 363
    :cond_f
    iget-object v1, p0, Ldea;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 364
    iput v0, p0, Ldea;->cachedSize:I

    .line 365
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 200
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldea;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldea;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldea;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldea;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldea;->c:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldea;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldea;->e:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldea;->f:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldea;->g:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldea;->h:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldea;->i:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldea;->j:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    if-eq v0, v2, :cond_2

    if-ne v0, v3, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldea;->k:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldea;->k:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_4

    if-ne v0, v3, :cond_5

    :cond_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldea;->l:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_5
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldea;->l:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldea;->m:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldea;->n:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldea;->o:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_f
    iget-object v0, p0, Ldea;->p:Lddy;

    if-nez v0, :cond_6

    new-instance v0, Lddy;

    invoke-direct {v0}, Lddy;-><init>()V

    iput-object v0, p0, Ldea;->p:Lddy;

    :cond_6
    iget-object v0, p0, Ldea;->p:Lddy;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldea;->q:Ljava/lang/Boolean;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x7a -> :sswitch_f
        0x80 -> :sswitch_10
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 244
    iget-object v0, p0, Ldea;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 245
    const/4 v0, 0x1

    iget-object v1, p0, Ldea;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 247
    :cond_0
    iget-object v0, p0, Ldea;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 248
    const/4 v0, 0x2

    iget-object v1, p0, Ldea;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 250
    :cond_1
    iget-object v0, p0, Ldea;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 251
    const/4 v0, 0x3

    iget-object v1, p0, Ldea;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 253
    :cond_2
    iget-object v0, p0, Ldea;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 254
    const/4 v0, 0x4

    iget-object v1, p0, Ldea;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 256
    :cond_3
    iget-object v0, p0, Ldea;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 257
    const/4 v0, 0x5

    iget-object v1, p0, Ldea;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 259
    :cond_4
    iget-object v0, p0, Ldea;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 260
    const/4 v0, 0x6

    iget-object v1, p0, Ldea;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 262
    :cond_5
    iget-object v0, p0, Ldea;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 263
    const/4 v0, 0x7

    iget-object v1, p0, Ldea;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 265
    :cond_6
    iget-object v0, p0, Ldea;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 266
    const/16 v0, 0x8

    iget-object v1, p0, Ldea;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 268
    :cond_7
    iget-object v0, p0, Ldea;->j:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 269
    const/16 v0, 0x9

    iget-object v1, p0, Ldea;->j:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 271
    :cond_8
    iget-object v0, p0, Ldea;->k:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 272
    const/16 v0, 0xa

    iget-object v1, p0, Ldea;->k:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 274
    :cond_9
    iget-object v0, p0, Ldea;->l:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    .line 275
    const/16 v0, 0xb

    iget-object v1, p0, Ldea;->l:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 277
    :cond_a
    iget-object v0, p0, Ldea;->m:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    .line 278
    const/16 v0, 0xc

    iget-object v1, p0, Ldea;->m:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 280
    :cond_b
    iget-object v0, p0, Ldea;->n:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    .line 281
    const/16 v0, 0xd

    iget-object v1, p0, Ldea;->n:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 283
    :cond_c
    iget-object v0, p0, Ldea;->o:Ljava/lang/Boolean;

    if-eqz v0, :cond_d

    .line 284
    const/16 v0, 0xe

    iget-object v1, p0, Ldea;->o:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 286
    :cond_d
    iget-object v0, p0, Ldea;->p:Lddy;

    if-eqz v0, :cond_e

    .line 287
    const/16 v0, 0xf

    iget-object v1, p0, Ldea;->p:Lddy;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 289
    :cond_e
    iget-object v0, p0, Ldea;->q:Ljava/lang/Boolean;

    if-eqz v0, :cond_f

    .line 290
    const/16 v0, 0x10

    iget-object v1, p0, Ldea;->q:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 292
    :cond_f
    iget-object v0, p0, Ldea;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 294
    return-void
.end method
