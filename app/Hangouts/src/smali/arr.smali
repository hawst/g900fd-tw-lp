.class public final Larr;
.super Lt;
.source "PG"

# interfaces
.implements Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field private a:Landroid/nfc/NfcAdapter;

.field private b:Landroid/os/Handler;

.field private c:Lcom/google/android/libraries/hangouts/video/HangoutRequest;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lt;-><init>()V

    return-void
.end method

.method static synthetic a(Larr;)V
    .locals 2

    .prologue
    .line 30
    invoke-static {}, Lapk;->b()Lapk;

    move-result-object v0

    invoke-virtual {v0}, Lapk;->c()Lapx;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "HangoutState is null. Will not send NdefMessage"

    invoke-static {v0}, Lf;->j(Ljava/lang/String;)V

    :cond_0
    :goto_0
    iget-object v1, p0, Larr;->b:Landroid/os/Handler;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Larr;->b:Landroid/os/Handler;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :cond_1
    invoke-virtual {v0}, Lapx;->o()Lcom/google/android/libraries/hangouts/video/CallState;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/CallState;->getSelf()Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lapx;->c()Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;->getEndpointMucJid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->cloneForCallTransferSend(Ljava/lang/String;)Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v0

    iput-object v0, p0, Larr;->c:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public createNdefMessage(Landroid/nfc/NfcEvent;)Landroid/nfc/NdefMessage;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 48
    const-string v1, "createNdefMessage"

    invoke-static {v1}, Lf;->j(Ljava/lang/String;)V

    .line 49
    iget-object v1, p0, Larr;->b:Landroid/os/Handler;

    monitor-enter v1

    .line 51
    :try_start_0
    iget-object v2, p0, Larr;->b:Landroid/os/Handler;

    new-instance v3, Lars;

    invoke-direct {v3, p0}, Lars;-><init>(Larr;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    :try_start_1
    iget-object v2, p0, Larr;->b:Landroid/os/Handler;

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 64
    monitor-exit v1

    .line 66
    iget-object v1, p0, Larr;->c:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    if-nez v1, :cond_0

    .line 67
    const-string v1, "Could not fetch HangoutRequest. Will not send NdefMessage."

    invoke-static {v1}, Lf;->j(Ljava/lang/String;)V

    .line 81
    :goto_0
    return-object v0

    .line 61
    :catch_0
    move-exception v2

    :try_start_2
    const-string v2, "InterruptedException while creating NdefMessage."

    invoke-static {v2}, Lf;->j(Ljava/lang/String;)V

    .line 62
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 64
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 73
    :cond_0
    :try_start_3
    iget-object v1, p0, Larr;->c:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v3, Ljava/io/ObjectOutputStream;

    invoke-direct {v3, v2}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v3, v1}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    move-result-object v0

    .line 79
    const-string v1, "com.google.android.apps.hangouts"

    const-string v2, "hangoutrequest"

    invoke-static {v1, v2, v0}, Landroid/nfc/NdefRecord;->createExternal(Ljava/lang/String;Ljava/lang/String;[B)Landroid/nfc/NdefRecord;

    move-result-object v1

    .line 81
    new-instance v0, Landroid/nfc/NdefMessage;

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/nfc/NdefRecord;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    invoke-direct {v0, v2}, Landroid/nfc/NdefMessage;-><init>([Landroid/nfc/NdefRecord;)V

    goto :goto_0

    .line 74
    :catch_1
    move-exception v1

    .line 75
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot serialize hangout request: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lf;->j(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 37
    invoke-super {p0, p1}, Lt;->onCreate(Landroid/os/Bundle;)V

    .line 38
    const-string v0, "onCreate NfcHangoutFragment"

    const-string v1, "Babel"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "Babel"

    invoke-static {v1, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Larr;->b:Landroid/os/Handler;

    .line 40
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 108
    invoke-super {p0}, Lt;->onResume()V

    .line 109
    invoke-virtual {p0}, Larr;->getActivity()Ly;

    move-result-object v0

    invoke-static {v0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    iput-object v0, p0, Larr;->a:Landroid/nfc/NfcAdapter;

    .line 110
    iget-object v0, p0, Larr;->a:Landroid/nfc/NfcAdapter;

    if-nez v0, :cond_0

    .line 115
    :goto_0
    return-void

    .line 114
    :cond_0
    iget-object v0, p0, Larr;->a:Landroid/nfc/NfcAdapter;

    invoke-virtual {p0}, Larr;->getActivity()Ly;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Landroid/app/Activity;

    invoke-virtual {v0, p0, v1, v2}, Landroid/nfc/NfcAdapter;->setNdefPushMessageCallback(Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    goto :goto_0
.end method
