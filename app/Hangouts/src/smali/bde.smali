.class public Lbde;
.super Lbcz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Z

.field private final h:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 1

    .prologue
    .line 185
    invoke-direct {p0}, Lbcz;-><init>()V

    .line 186
    iput-object p1, p0, Lbde;->a:Ljava/lang/String;

    .line 187
    iget-object v0, p0, Lbde;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    invoke-static {v0}, Lcwz;->b(Z)V

    .line 188
    iput-object p2, p0, Lbde;->b:Ljava/lang/String;

    .line 189
    iput-boolean p3, p0, Lbde;->c:Z

    .line 190
    iput-boolean p4, p0, Lbde;->h:Z

    .line 191
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 195
    new-instance v0, Ldnc;

    invoke-direct {v0}, Ldnc;-><init>()V

    .line 196
    iget-object v1, p0, Lbde;->a:Ljava/lang/String;

    iput-object v1, v0, Ldnc;->d:Ljava/lang/String;

    .line 198
    new-instance v1, Ldnp;

    invoke-direct {v1}, Ldnp;-><init>()V

    .line 199
    iput-object v0, v1, Ldnp;->b:Ldnc;

    .line 200
    iget-object v0, p0, Lbde;->b:Ljava/lang/String;

    iput-object v0, v1, Ldnp;->c:Ljava/lang/String;

    .line 202
    new-instance v0, Ldnq;

    invoke-direct {v0}, Ldnq;-><init>()V

    .line 203
    new-array v2, v4, [Ldnp;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    iput-object v2, v0, Ldnq;->b:[Ldnp;

    .line 204
    iget-boolean v1, p0, Lbde;->c:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Ldnq;->c:Ljava/lang/Boolean;

    .line 206
    new-instance v1, Ldnx;

    invoke-direct {v1}, Ldnx;-><init>()V

    .line 207
    iput-object v0, v1, Ldnx;->b:Ldnq;

    .line 208
    iget-boolean v0, p0, Lbde;->c:Z

    if-nez v0, :cond_0

    .line 209
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v1, Ldnx;->e:Ljava/lang/Boolean;

    .line 213
    :cond_0
    new-instance v0, Ldcx;

    invoke-direct {v0}, Ldcx;-><init>()V

    .line 215
    iput-object v1, v0, Ldcx;->c:Ldnx;

    .line 216
    return-object v0
.end method

.method public a(JI)Z
    .locals 1

    .prologue
    .line 226
    iget-boolean v0, p0, Lbde;->h:Z

    if-eqz v0, :cond_0

    .line 227
    invoke-super {p0, p1, p2, p3}, Lbcz;->a(JI)Z

    move-result v0

    .line 229
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 221
    const-string v0, "blockuser"

    return-object v0
.end method
