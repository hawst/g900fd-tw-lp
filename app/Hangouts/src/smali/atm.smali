.class public final Latm;
.super Lapo;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Latm;->a:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    invoke-direct {p0}, Lapo;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;B)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1}, Latm;-><init>(Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;)V

    return-void
.end method


# virtual methods
.method public f()V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Latm;->a:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->c()V

    .line 67
    iget-object v0, p0, Latm;->a:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->a(Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;)V

    .line 68
    return-void
.end method

.method public onEndpointEvent(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/EndpointEvent;)V
    .locals 2

    .prologue
    .line 56
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isSelfEndpoint()Z

    move-result v0

    if-nez v0, :cond_1

    instance-of v0, p2, Lcom/google/android/libraries/hangouts/video/endpoint/ConnectionStatusChangedEvent;

    if-nez v0, :cond_0

    instance-of v0, p2, Lcom/google/android/libraries/hangouts/video/endpoint/MediaSourceEvent;

    if-eqz v0, :cond_1

    .line 57
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 58
    iget-object v0, p0, Latm;->a:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->a(Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Z)V

    .line 60
    :cond_1
    instance-of v0, p2, Lcom/google/android/libraries/hangouts/video/endpoint/EnterEvent;

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isSelfEndpoint()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 61
    iget-object v0, p0, Latm;->a:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->a(Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Z)V

    .line 63
    :cond_2
    return-void
.end method
