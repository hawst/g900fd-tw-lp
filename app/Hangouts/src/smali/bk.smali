.class public final Lbk;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Landroid/content/Context;

.field b:Ljava/lang/CharSequence;

.field c:Ljava/lang/CharSequence;

.field d:Landroid/app/PendingIntent;

.field e:Landroid/app/PendingIntent;

.field f:Landroid/widget/RemoteViews;

.field g:Landroid/graphics/Bitmap;

.field h:Ljava/lang/CharSequence;

.field i:I

.field j:I

.field k:Z

.field l:Lbu;

.field m:Ljava/lang/CharSequence;

.field n:I

.field o:I

.field p:Z

.field q:Ljava/lang/String;

.field r:Z

.field s:Ljava/lang/String;

.field t:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lbg;",
            ">;"
        }
    .end annotation
.end field

.field u:Z

.field v:Landroid/os/Bundle;

.field w:Landroid/app/Notification;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 676
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 659
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbk;->t:Ljava/util/ArrayList;

    .line 660
    iput-boolean v3, p0, Lbk;->u:Z

    .line 663
    new-instance v0, Landroid/app/Notification;

    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    iput-object v0, p0, Lbk;->w:Landroid/app/Notification;

    .line 677
    iput-object p1, p0, Lbk;->a:Landroid/content/Context;

    .line 680
    iget-object v0, p0, Lbk;->w:Landroid/app/Notification;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, v0, Landroid/app/Notification;->when:J

    .line 681
    iget-object v0, p0, Lbk;->w:Landroid/app/Notification;

    const/4 v1, -0x1

    iput v1, v0, Landroid/app/Notification;->audioStreamType:I

    .line 682
    iput v3, p0, Lbk;->j:I

    .line 683
    return-void
.end method

.method private a(IZ)V
    .locals 3

    .prologue
    .line 990
    if-eqz p2, :cond_0

    .line 991
    iget-object v0, p0, Lbk;->w:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/2addr v1, p1

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 995
    :goto_0
    return-void

    .line 993
    :cond_0
    iget-object v0, p0, Lbk;->w:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    xor-int/lit8 v2, p1, -0x1

    and-int/2addr v1, v2

    iput v1, v0, Landroid/app/Notification;->flags:I

    goto :goto_0
.end method


# virtual methods
.method public a()Lbk;
    .locals 1

    .prologue
    .line 706
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbk;->k:Z

    .line 707
    return-object p0
.end method

.method public a(I)Lbk;
    .locals 1

    .prologue
    .line 718
    iget-object v0, p0, Lbk;->w:Landroid/app/Notification;

    iput p1, v0, Landroid/app/Notification;->icon:I

    .line 719
    return-object p0
.end method

.method public a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Lbk;
    .locals 2

    .prologue
    .line 1133
    iget-object v0, p0, Lbk;->t:Ljava/util/ArrayList;

    new-instance v1, Lbg;

    invoke-direct {v1, p1, p2, p3}, Lbg;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1134
    return-object p0
.end method

.method public a(J)Lbk;
    .locals 1

    .prologue
    .line 690
    iget-object v0, p0, Lbk;->w:Landroid/app/Notification;

    iput-wide p1, v0, Landroid/app/Notification;->when:J

    .line 691
    return-object p0
.end method

.method public a(Landroid/app/PendingIntent;)Lbk;
    .locals 0

    .prologue
    .line 814
    iput-object p1, p0, Lbk;->d:Landroid/app/PendingIntent;

    .line 815
    return-object p0
.end method

.method public a(Landroid/graphics/Bitmap;)Lbk;
    .locals 0

    .prologue
    .line 873
    iput-object p1, p0, Lbk;->g:Landroid/graphics/Bitmap;

    .line 874
    return-object p0
.end method

.method public a(Landroid/net/Uri;)Lbk;
    .locals 2

    .prologue
    .line 881
    iget-object v0, p0, Lbk;->w:Landroid/app/Notification;

    iput-object p1, v0, Landroid/app/Notification;->sound:Landroid/net/Uri;

    .line 882
    iget-object v0, p0, Lbk;->w:Landroid/app/Notification;

    const/4 v1, -0x1

    iput v1, v0, Landroid/app/Notification;->audioStreamType:I

    .line 883
    return-object p0
.end method

.method public a(Landroid/os/Bundle;)Lbk;
    .locals 1

    .prologue
    .line 1072
    iget-object v0, p0, Lbk;->v:Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 1074
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0, p1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    iput-object v0, p0, Lbk;->v:Landroid/os/Bundle;

    .line 1079
    :goto_0
    return-object p0

    .line 1076
    :cond_0
    iget-object v0, p0, Lbk;->v:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public a(Lbl;)Lbk;
    .locals 0

    .prologue
    .line 1179
    invoke-interface {p1, p0}, Lbl;->a(Lbk;)Lbk;

    .line 1180
    return-object p0
.end method

.method public a(Lbu;)Lbk;
    .locals 1

    .prologue
    .line 1165
    iget-object v0, p0, Lbk;->l:Lbu;

    if-eq v0, p1, :cond_0

    .line 1166
    iput-object p1, p0, Lbk;->l:Lbu;

    .line 1167
    iget-object v0, p0, Lbk;->l:Lbu;

    if-eqz v0, :cond_0

    .line 1168
    iget-object v0, p0, Lbk;->l:Lbu;

    invoke-virtual {v0, p0}, Lbu;->a(Lbk;)V

    .line 1171
    :cond_0
    return-object p0
.end method

.method public a(Ljava/lang/CharSequence;)Lbk;
    .locals 0

    .prologue
    .line 742
    iput-object p1, p0, Lbk;->b:Ljava/lang/CharSequence;

    .line 743
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lbk;
    .locals 0

    .prologue
    .line 1031
    iput-object p1, p0, Lbk;->q:Ljava/lang/String;

    .line 1032
    return-object p0
.end method

.method public a(Z)Lbk;
    .locals 1

    .prologue
    .line 956
    const/16 v0, 0x10

    invoke-direct {p0, v0, p1}, Lbk;->a(IZ)V

    .line 957
    return-object p0
.end method

.method public b()Lbk;
    .locals 2

    .prologue
    .line 936
    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lbk;->a(IZ)V

    .line 937
    return-object p0
.end method

.method public b(I)Lbk;
    .locals 2

    .prologue
    .line 982
    iget-object v0, p0, Lbk;->w:Landroid/app/Notification;

    iput p1, v0, Landroid/app/Notification;->defaults:I

    .line 983
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_0

    .line 984
    iget-object v0, p0, Lbk;->w:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 986
    :cond_0
    return-object p0
.end method

.method public b(Landroid/app/PendingIntent;)Lbk;
    .locals 1

    .prologue
    .line 826
    iget-object v0, p0, Lbk;->w:Landroid/app/Notification;

    iput-object p1, v0, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    .line 827
    return-object p0
.end method

.method public b(Ljava/lang/CharSequence;)Lbk;
    .locals 0

    .prologue
    .line 750
    iput-object p1, p0, Lbk;->c:Ljava/lang/CharSequence;

    .line 751
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lbk;
    .locals 0

    .prologue
    .line 1060
    iput-object p1, p0, Lbk;->s:Ljava/lang/String;

    .line 1061
    return-object p0
.end method

.method public c()Lbk;
    .locals 1

    .prologue
    .line 1043
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbk;->r:Z

    .line 1044
    return-object p0
.end method

.method public c(I)Lbk;
    .locals 0

    .prologue
    .line 1015
    iput p1, p0, Lbk;->j:I

    .line 1016
    return-object p0
.end method

.method public c(Landroid/app/PendingIntent;)Lbk;
    .locals 2

    .prologue
    .line 844
    iput-object p1, p0, Lbk;->e:Landroid/app/PendingIntent;

    .line 845
    const/16 v0, 0x80

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lbk;->a(IZ)V

    .line 846
    return-object p0
.end method

.method public c(Ljava/lang/CharSequence;)Lbk;
    .locals 1

    .prologue
    .line 854
    iget-object v0, p0, Lbk;->w:Landroid/app/Notification;

    iput-object p1, v0, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    .line 855
    return-object p0
.end method

.method public d()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 1110
    iget-object v0, p0, Lbk;->v:Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 1111
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lbk;->v:Landroid/os/Bundle;

    .line 1113
    :cond_0
    iget-object v0, p0, Lbk;->v:Landroid/os/Bundle;

    return-object v0
.end method

.method public e()Landroid/app/Notification;
    .locals 1

    .prologue
    .line 1196
    invoke-static {}, Lbf;->a()Lbn;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbn;->a(Lbk;)Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method
