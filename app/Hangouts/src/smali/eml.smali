.class public final Leml;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leml;


# instance fields
.field public b:Ljava/lang/String;

.field public c:[Leoi;

.field public d:Ljava/lang/Long;

.field public e:Ljava/lang/Long;

.field public f:Lemr;

.field public g:[Lemm;

.field public h:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 361
    const/4 v0, 0x0

    new-array v0, v0, [Leml;

    sput-object v0, Leml;->a:[Leml;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 362
    invoke-direct {p0}, Lepn;-><init>()V

    .line 475
    sget-object v0, Leoi;->a:[Leoi;

    iput-object v0, p0, Leml;->c:[Leoi;

    .line 482
    iput-object v1, p0, Leml;->f:Lemr;

    .line 485
    sget-object v0, Lemm;->a:[Lemm;

    iput-object v0, p0, Leml;->g:[Lemm;

    .line 488
    iput-object v1, p0, Leml;->h:Ljava/lang/Integer;

    .line 362
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 529
    iget-object v0, p0, Leml;->b:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 530
    const/4 v0, 0x1

    iget-object v2, p0, Leml;->b:Ljava/lang/String;

    .line 531
    invoke-static {v0, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 533
    :goto_0
    iget-object v2, p0, Leml;->d:Ljava/lang/Long;

    if-eqz v2, :cond_0

    .line 534
    const/4 v2, 0x2

    iget-object v3, p0, Leml;->d:Ljava/lang/Long;

    .line 535
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 537
    :cond_0
    iget-object v2, p0, Leml;->e:Ljava/lang/Long;

    if-eqz v2, :cond_1

    .line 538
    const/4 v2, 0x3

    iget-object v3, p0, Leml;->e:Ljava/lang/Long;

    .line 539
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 541
    :cond_1
    iget-object v2, p0, Leml;->g:[Lemm;

    if-eqz v2, :cond_3

    .line 542
    iget-object v3, p0, Leml;->g:[Lemm;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    .line 543
    if-eqz v5, :cond_2

    .line 544
    const/4 v6, 0x5

    .line 545
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 542
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 549
    :cond_3
    iget-object v2, p0, Leml;->h:Ljava/lang/Integer;

    if-eqz v2, :cond_4

    .line 550
    const/4 v2, 0x6

    iget-object v3, p0, Leml;->h:Ljava/lang/Integer;

    .line 551
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 553
    :cond_4
    iget-object v2, p0, Leml;->c:[Leoi;

    if-eqz v2, :cond_6

    .line 554
    iget-object v2, p0, Leml;->c:[Leoi;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 555
    if-eqz v4, :cond_5

    .line 556
    const/4 v5, 0x7

    .line 557
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 554
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 561
    :cond_6
    iget-object v1, p0, Leml;->f:Lemr;

    if-eqz v1, :cond_7

    .line 562
    const/16 v1, 0x9

    iget-object v2, p0, Leml;->f:Lemr;

    .line 563
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 565
    :cond_7
    iget-object v1, p0, Leml;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 566
    iput v0, p0, Leml;->cachedSize:I

    .line 567
    return v0

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 358
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Leml;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Leml;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Leml;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leml;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Leml;->d:Ljava/lang/Long;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Leml;->e:Ljava/lang/Long;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leml;->g:[Lemm;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lemm;

    iget-object v3, p0, Leml;->g:[Lemm;

    if-eqz v3, :cond_2

    iget-object v3, p0, Leml;->g:[Lemm;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Leml;->g:[Lemm;

    :goto_2
    iget-object v2, p0, Leml;->g:[Lemm;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Leml;->g:[Lemm;

    new-instance v3, Lemm;

    invoke-direct {v3}, Lemm;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leml;->g:[Lemm;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Leml;->g:[Lemm;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Leml;->g:[Lemm;

    new-instance v3, Lemm;

    invoke-direct {v3}, Lemm;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leml;->g:[Lemm;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_5

    const/4 v2, 0x1

    if-ne v0, v2, :cond_6

    :cond_5
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leml;->h:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_6
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leml;->h:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_6
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leml;->c:[Leoi;

    if-nez v0, :cond_8

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Leoi;

    iget-object v3, p0, Leml;->c:[Leoi;

    if-eqz v3, :cond_7

    iget-object v3, p0, Leml;->c:[Leoi;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    iput-object v2, p0, Leml;->c:[Leoi;

    :goto_4
    iget-object v2, p0, Leml;->c:[Leoi;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    iget-object v2, p0, Leml;->c:[Leoi;

    new-instance v3, Leoi;

    invoke-direct {v3}, Leoi;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leml;->c:[Leoi;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_8
    iget-object v0, p0, Leml;->c:[Leoi;

    array-length v0, v0

    goto :goto_3

    :cond_9
    iget-object v2, p0, Leml;->c:[Leoi;

    new-instance v3, Leoi;

    invoke-direct {v3}, Leoi;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leml;->c:[Leoi;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Leml;->f:Lemr;

    if-nez v0, :cond_a

    new-instance v0, Lemr;

    invoke-direct {v0}, Lemr;-><init>()V

    iput-object v0, p0, Leml;->f:Lemr;

    :cond_a
    iget-object v0, p0, Leml;->f:Lemr;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x2a -> :sswitch_4
        0x30 -> :sswitch_5
        0x3a -> :sswitch_6
        0x4a -> :sswitch_7
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 493
    iget-object v1, p0, Leml;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 494
    const/4 v1, 0x1

    iget-object v2, p0, Leml;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 496
    :cond_0
    iget-object v1, p0, Leml;->d:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 497
    const/4 v1, 0x2

    iget-object v2, p0, Leml;->d:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Lepl;->b(IJ)V

    .line 499
    :cond_1
    iget-object v1, p0, Leml;->e:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 500
    const/4 v1, 0x3

    iget-object v2, p0, Leml;->e:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Lepl;->b(IJ)V

    .line 502
    :cond_2
    iget-object v1, p0, Leml;->g:[Lemm;

    if-eqz v1, :cond_4

    .line 503
    iget-object v2, p0, Leml;->g:[Lemm;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 504
    if-eqz v4, :cond_3

    .line 505
    const/4 v5, 0x5

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 503
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 509
    :cond_4
    iget-object v1, p0, Leml;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 510
    const/4 v1, 0x6

    iget-object v2, p0, Leml;->h:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 512
    :cond_5
    iget-object v1, p0, Leml;->c:[Leoi;

    if-eqz v1, :cond_7

    .line 513
    iget-object v1, p0, Leml;->c:[Leoi;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_7

    aget-object v3, v1, v0

    .line 514
    if-eqz v3, :cond_6

    .line 515
    const/4 v4, 0x7

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 513
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 519
    :cond_7
    iget-object v0, p0, Leml;->f:Lemr;

    if-eqz v0, :cond_8

    .line 520
    const/16 v0, 0x9

    iget-object v1, p0, Leml;->f:Lemr;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 522
    :cond_8
    iget-object v0, p0, Leml;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 524
    return-void
.end method
