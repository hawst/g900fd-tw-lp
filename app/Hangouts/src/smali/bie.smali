.class public Lbie;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method private constructor <init>(Ldwh;)V
    .locals 3

    .prologue
    .line 1507
    iget-object v0, p1, Ldwh;->b:Ldvn;

    iget-object v1, p1, Ldwh;->c:Ljava/lang/Long;

    .line 1508
    invoke-static {v1}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v1

    .line 1507
    invoke-direct {p0, v0, v1, v2}, Lbfz;-><init>(Ldvn;J)V

    .line 1509
    return-void
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 1513
    new-instance v0, Ldwh;

    invoke-direct {v0}, Ldwh;-><init>()V

    .line 1514
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldwh;

    .line 1515
    iget-object v1, v0, Ldwh;->b:Ldvn;

    invoke-static {v1}, Lbie;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1516
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldwh;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 1518
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbie;

    invoke-direct {v1, v0}, Lbie;-><init>(Ldwh;)V

    move-object v0, v1

    goto :goto_0
.end method
