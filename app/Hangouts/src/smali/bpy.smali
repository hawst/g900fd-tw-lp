.class public final Lbpy;
.super Lbnj;
.source "PG"


# instance fields
.field private final a:Z

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lbnj;-><init>(Lyj;)V

    .line 29
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    sget v1, Lh;->ak:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 30
    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lbpy;->a:Z

    .line 31
    iput-object p3, p0, Lbpy;->d:Ljava/lang/String;

    .line 32
    iput-object p4, p0, Lbpy;->e:Ljava/lang/String;

    .line 33
    iput-object p5, p0, Lbpy;->f:Ljava/lang/String;

    .line 34
    return-void
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    .line 38
    const/4 v0, 0x0

    .line 39
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 40
    sget v2, Lh;->aj:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lbpy;->f:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 42
    const/16 v0, 0x32

    .line 50
    :cond_0
    :goto_0
    iget-object v1, p0, Lbpy;->c:Lbnl;

    new-instance v2, Lbdg;

    iget-boolean v3, p0, Lbpy;->a:Z

    iget-object v4, p0, Lbpy;->d:Ljava/lang/String;

    iget-object v5, p0, Lbpy;->e:Ljava/lang/String;

    invoke-direct {v2, v3, v4, v5, v0}, Lbdg;-><init>(ZLjava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v1, v2}, Lbnl;->a(Lbea;)V

    .line 52
    return-void

    .line 43
    :cond_1
    sget v2, Lh;->ai:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lbpy;->f:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 45
    const/16 v0, 0x14

    goto :goto_0

    .line 46
    :cond_2
    sget v2, Lh;->ah:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbpy;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 48
    const/16 v0, 0xa

    goto :goto_0
.end method
