.class public final Lqx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Lcom/android/ex/photo/views/PhotoView;

.field private b:F

.field private c:F

.field private d:Z

.field private e:F

.field private f:F

.field private g:F

.field private h:J

.field private i:Z

.field private j:Z


# direct methods
.method public constructor <init>(Lcom/android/ex/photo/views/PhotoView;)V
    .locals 0

    .prologue
    .line 1087
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1088
    iput-object p1, p0, Lqx;->a:Lcom/android/ex/photo/views/PhotoView;

    .line 1089
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 1118
    const/4 v0, 0x0

    iput-boolean v0, p0, Lqx;->i:Z

    .line 1119
    const/4 v0, 0x1

    iput-boolean v0, p0, Lqx;->j:Z

    .line 1120
    return-void
.end method

.method public a(FFFF)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1095
    iget-boolean v0, p0, Lqx;->i:Z

    if-eqz v0, :cond_0

    .line 1111
    :goto_0
    return v2

    .line 1099
    :cond_0
    iput p3, p0, Lqx;->b:F

    .line 1100
    iput p4, p0, Lqx;->c:F

    .line 1103
    iput p2, p0, Lqx;->e:F

    .line 1104
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, p0, Lqx;->h:J

    .line 1105
    iput p1, p0, Lqx;->f:F

    .line 1106
    iget v0, p0, Lqx;->e:F

    iget v3, p0, Lqx;->f:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lqx;->d:Z

    .line 1107
    iget v0, p0, Lqx;->e:F

    iget v3, p0, Lqx;->f:F

    sub-float/2addr v0, v3

    const/high16 v3, 0x43960000    # 300.0f

    div-float/2addr v0, v3

    iput v0, p0, Lqx;->g:F

    .line 1108
    iput-boolean v1, p0, Lqx;->i:Z

    .line 1109
    iput-boolean v2, p0, Lqx;->j:Z

    .line 1110
    iget-object v0, p0, Lqx;->a:Lcom/android/ex/photo/views/PhotoView;

    invoke-virtual {v0, p0}, Lcom/android/ex/photo/views/PhotoView;->post(Ljava/lang/Runnable;)Z

    move v2, v1

    .line 1111
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1106
    goto :goto_1
.end method

.method public run()V
    .locals 4

    .prologue
    .line 1124
    iget-boolean v0, p0, Lqx;->j:Z

    if-eqz v0, :cond_1

    .line 1143
    :cond_0
    :goto_0
    return-void

    .line 1129
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1130
    iget-wide v2, p0, Lqx;->h:J

    sub-long/2addr v0, v2

    .line 1131
    iget v2, p0, Lqx;->f:F

    iget v3, p0, Lqx;->g:F

    long-to-float v0, v0

    mul-float/2addr v0, v3

    add-float/2addr v0, v2

    .line 1132
    iget-object v1, p0, Lqx;->a:Lcom/android/ex/photo/views/PhotoView;

    iget v2, p0, Lqx;->b:F

    iget v3, p0, Lqx;->c:F

    invoke-static {v1, v0, v2, v3}, Lcom/android/ex/photo/views/PhotoView;->a(Lcom/android/ex/photo/views/PhotoView;FFF)V

    .line 1135
    iget v1, p0, Lqx;->e:F

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lqx;->d:Z

    iget v2, p0, Lqx;->e:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    if-ne v1, v0, :cond_3

    .line 1136
    :cond_2
    iget-object v0, p0, Lqx;->a:Lcom/android/ex/photo/views/PhotoView;

    iget v1, p0, Lqx;->e:F

    iget v2, p0, Lqx;->b:F

    iget v3, p0, Lqx;->c:F

    invoke-static {v0, v1, v2, v3}, Lcom/android/ex/photo/views/PhotoView;->a(Lcom/android/ex/photo/views/PhotoView;FFF)V

    .line 1137
    invoke-virtual {p0}, Lqx;->a()V

    .line 1140
    :cond_3
    iget-boolean v0, p0, Lqx;->j:Z

    if-nez v0, :cond_0

    .line 1141
    iget-object v0, p0, Lqx;->a:Lcom/android/ex/photo/views/PhotoView;

    invoke-virtual {v0, p0}, Lcom/android/ex/photo/views/PhotoView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 1135
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method
