.class public Lbur;
.super Lbuc;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field private b:Lbus;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lbuc;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lbur;->b:Lbus;

    invoke-virtual {v0, p1, p2}, Lbus;->a(II)V

    .line 68
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 33
    invoke-super {p0, p1}, Lbuc;->onCreate(Landroid/os/Bundle;)V

    .line 35
    new-instance v0, Lbus;

    invoke-direct {v0, p0}, Lbus;-><init>(Lbtz;)V

    iput-object v0, p0, Lbur;->b:Lbus;

    .line 36
    iget-object v0, p0, Lbur;->b:Lbus;

    invoke-virtual {p0}, Lbur;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "account_name"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbus;->a(Ljava/lang/String;)V

    .line 39
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbur;->setHasOptionsMenu(Z)V

    .line 40
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 56
    invoke-super {p0}, Lbuc;->onDestroy()V

    .line 57
    iget-object v0, p0, Lbur;->b:Lbus;

    invoke-virtual {v0}, Lbus;->b()V

    .line 58
    return-void
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 62
    invoke-super {p0}, Lbuc;->onDestroyView()V

    .line 63
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 47
    invoke-super {p0}, Lbuc;->onResume()V

    .line 48
    iget-object v0, p0, Lbur;->b:Lbus;

    invoke-virtual {v0}, Lbus;->a()V

    .line 49
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 75
    invoke-super {p0}, Lbuc;->onStart()V

    .line 76
    iget-object v0, p0, Lbur;->b:Lbus;

    invoke-virtual {v0}, Lbus;->c()Lbor;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbor;)V

    .line 77
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 84
    invoke-super {p0}, Lbuc;->onStop()V

    .line 85
    iget-object v0, p0, Lbur;->b:Lbus;

    invoke-virtual {v0}, Lbus;->c()Lbor;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lbor;)V

    .line 86
    return-void
.end method
