.class public final Lbvl;
.super Lbvh;
.source "PG"


# static fields
.field public static final a:[Ljava/lang/String;

.field public static final b:[Ljava/lang/String;

.field public static final c:I

.field public static final d:I

.field public static final e:I

.field public static final f:I

.field public static final g:I

.field public static final h:I

.field public static final i:I

.field public static final j:I

.field public static final k:I

.field public static final l:I

.field private static w:I


# instance fields
.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:J

.field public p:J

.field public q:J

.field public r:I

.field public s:J

.field public t:I

.field public u:Z

.field public v:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 76
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "type"

    aput-object v1, v0, v4

    const-string v1, "address"

    aput-object v1, v0, v5

    const-string v1, "body"

    aput-object v1, v0, v6

    const-string v1, "date"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "thread_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "status"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "read"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "seen"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "date_sent"

    aput-object v2, v0, v1

    sput-object v0, Lbvl;->a:[Ljava/lang/String;

    .line 93
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "type"

    aput-object v1, v0, v4

    const-string v1, "address"

    aput-object v1, v0, v5

    const-string v1, "body"

    aput-object v1, v0, v6

    const-string v1, "date"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "thread_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "status"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "read"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "seen"

    aput-object v2, v0, v1

    sput-object v0, Lbvl;->b:[Ljava/lang/String;

    .line 105
    sput v3, Lbvl;->w:I

    sput v4, Lbvl;->w:I

    sput v3, Lbvl;->c:I

    .line 106
    sget v0, Lbvl;->w:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lbvl;->w:I

    sput v0, Lbvl;->d:I

    .line 107
    sget v0, Lbvl;->w:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lbvl;->w:I

    sput v0, Lbvl;->e:I

    .line 108
    sget v0, Lbvl;->w:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lbvl;->w:I

    sput v0, Lbvl;->f:I

    .line 109
    sget v0, Lbvl;->w:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lbvl;->w:I

    sput v0, Lbvl;->g:I

    .line 110
    sget v0, Lbvl;->w:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lbvl;->w:I

    sput v0, Lbvl;->h:I

    .line 111
    sget v0, Lbvl;->w:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lbvl;->w:I

    sput v0, Lbvl;->i:I

    .line 112
    sget v0, Lbvl;->w:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lbvl;->w:I

    sput v0, Lbvl;->j:I

    .line 113
    sget v0, Lbvl;->w:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lbvl;->w:I

    sput v0, Lbvl;->k:I

    .line 114
    sget v0, Lbvl;->w:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lbvl;->w:I

    sput v0, Lbvl;->l:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Lbvh;-><init>()V

    return-void
.end method

.method public static a(Landroid/database/Cursor;)Lbvl;
    .locals 7

    .prologue
    const-wide/16 v5, 0x3e8

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 150
    new-instance v4, Lbvl;

    invoke-direct {v4}, Lbvl;-><init>()V

    .line 151
    sget v0, Lbvl;->c:I

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v4, Lbvl;->o:J

    sget v0, Lbvl;->e:I

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lbvl;->m:Ljava/lang/String;

    sget v0, Lbvl;->f:I

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lbvl;->n:Ljava/lang/String;

    sget v0, Lbvl;->g:I

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    mul-long/2addr v0, v5

    iput-wide v0, v4, Lbvl;->p:J

    invoke-static {}, Lbvx;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lbvl;->l:I

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    mul-long/2addr v0, v5

    :goto_0
    iput-wide v0, v4, Lbvl;->q:J

    sget v0, Lbvl;->d:I

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v4, Lbvl;->r:I

    sget v0, Lbvl;->h:I

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v4, Lbvl;->s:J

    sget v0, Lbvl;->i:I

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v4, Lbvl;->t:I

    sget v0, Lbvl;->j:I

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    :goto_1
    iput-boolean v0, v4, Lbvl;->u:Z

    sget v0, Lbvl;->k:I

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_2

    :goto_2
    iput-boolean v2, v4, Lbvl;->v:Z

    .line 152
    return-object v4

    .line 151
    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0

    :cond_1
    move v0, v3

    goto :goto_1

    :cond_2
    move v2, v3

    goto :goto_2
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 162
    const/4 v0, 0x0

    return v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 157
    iget-wide v0, p0, Lbvl;->o:J

    return-wide v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 167
    iget-wide v0, p0, Lbvl;->p:J

    return-wide v0
.end method
