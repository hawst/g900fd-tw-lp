.class final Lchc;
.super Ldg;

# interfaces
.implements Lcfw;
.implements Lcgn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldg",
        "<",
        "Lcft;",
        ">;",
        "Lcfw;",
        "Lcgn;"
    }
.end annotation


# instance fields
.field public final a:Lcgl;

.field private b:Z

.field private c:Lcft;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcgl;)V
    .locals 0

    invoke-direct {p0, p1}, Ldg;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lchc;->a:Lcgl;

    return-void
.end method

.method private a(Lcft;)V
    .locals 1

    iput-object p1, p0, Lchc;->c:Lcft;

    invoke-virtual {p0}, Lchc;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lchc;->n()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lchc;->b(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    return-void
.end method

.method public b()V
    .locals 1

    iget-boolean v0, p0, Lchc;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lchc;->b:Z

    invoke-virtual {p0}, Lchc;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lchc;->n()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lchc;->a:Lcgl;

    invoke-interface {v0}, Lcgl;->a()V

    :cond_0
    return-void
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lchc;->b:Z

    sget-object v0, Lcft;->a:Lcft;

    invoke-direct {p0, v0}, Lchc;->a(Lcft;)V

    return-void
.end method

.method protected g()V
    .locals 1

    invoke-super {p0}, Ldg;->g()V

    iget-object v0, p0, Lchc;->a:Lcgl;

    invoke-interface {v0, p0}, Lcgl;->a(Lcgn;)V

    iget-object v0, p0, Lchc;->a:Lcgl;

    invoke-interface {v0, p0}, Lcgl;->a(Lcfw;)V

    iget-object v0, p0, Lchc;->c:Lcft;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lchc;->c:Lcft;

    invoke-virtual {p0, v0}, Lchc;->b(Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lchc;->a:Lcgl;

    invoke-interface {v0}, Lcgl;->c()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lchc;->a:Lcgl;

    invoke-interface {v0}, Lcgl;->d()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lchc;->b:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lchc;->a:Lcgl;

    invoke-interface {v0}, Lcgl;->a()V

    :cond_1
    return-void
.end method

.method protected h()V
    .locals 1

    iget-object v0, p0, Lchc;->a:Lcgl;

    invoke-interface {v0}, Lcgl;->b()V

    return-void
.end method

.method protected i()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lchc;->c:Lcft;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lchc;->b:Z

    iget-object v0, p0, Lchc;->a:Lcgl;

    invoke-interface {v0, p0}, Lcgl;->b(Lcgn;)V

    iget-object v0, p0, Lchc;->a:Lcgl;

    invoke-interface {v0, p0}, Lcgl;->b(Lcfw;)V

    iget-object v0, p0, Lchc;->a:Lcgl;

    invoke-interface {v0}, Lcgl;->b()V

    return-void
.end method

.method public onConnectionFailed(Lcft;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lchc;->b:Z

    invoke-direct {p0, p1}, Lchc;->a(Lcft;)V

    return-void
.end method
