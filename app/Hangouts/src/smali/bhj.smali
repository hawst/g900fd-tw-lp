.class public Lbhj;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbhk;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lddg;)V
    .locals 6

    .prologue
    .line 4320
    iget-object v0, p1, Lddg;->b:Ldcw;

    invoke-direct {p0, v0}, Lbfz;-><init>(Ldcw;)V

    .line 4321
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbhj;->g:Ljava/util/List;

    .line 4322
    iget-object v0, p1, Lddg;->c:Ldoa;

    iget-object v1, v0, Ldoa;->b:[Ldng;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 4323
    iget-object v4, v3, Ldng;->d:Ldnd;

    .line 4324
    new-instance v5, Lbhk;

    invoke-direct {v5}, Lbhk;-><init>()V

    .line 4325
    iget-object v3, v3, Ldng;->b:Ldnc;

    iget-object v3, v3, Ldnc;->d:Ljava/lang/String;

    iput-object v3, v5, Lbhk;->a:Ljava/lang/String;

    .line 4326
    iget-object v3, v4, Ldnd;->b:Ljava/lang/String;

    iput-object v3, v5, Lbhk;->b:Ljava/lang/String;

    .line 4327
    iget-object v3, v4, Ldnd;->f:Ljava/lang/String;

    iput-object v3, v5, Lbhk;->c:Ljava/lang/String;

    .line 4328
    iget-object v3, p0, Lbhj;->g:Ljava/util/List;

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4322
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4330
    :cond_0
    return-void
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 4334
    new-instance v0, Lddg;

    invoke-direct {v0}, Lddg;-><init>()V

    .line 4335
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Lddg;

    .line 4336
    iget-object v1, v0, Lddg;->b:Ldcw;

    invoke-static {v1}, Lbhj;->a(Ldcw;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4337
    new-instance v1, Lbgk;

    iget-object v0, v0, Lddg;->b:Ldcw;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldcw;)V

    move-object v0, v1

    .line 4339
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbhj;

    invoke-direct {v1, v0}, Lbhj;-><init>(Lddg;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Lyt;Lbnl;)V
    .locals 5

    .prologue
    .line 4346
    invoke-super {p0, p1, p2}, Lbfz;->a(Lyt;Lbnl;)V

    .line 4348
    invoke-virtual {p1}, Lyt;->a()V

    .line 4350
    :try_start_0
    invoke-virtual {p1}, Lyt;->o()V

    .line 4351
    iget-object v0, p0, Lbhj;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhk;

    .line 4352
    iget-object v2, v0, Lbhk;->a:Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, v0, Lbhk;->b:Ljava/lang/String;

    iget-object v0, v0, Lbhk;->c:Ljava/lang/String;

    invoke-virtual {p1, v2, v3, v4, v0}, Lyt;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 4356
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Lyt;->c()V

    throw v0

    .line 4354
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lyt;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4356
    invoke-virtual {p1}, Lyt;->c()V

    .line 4357
    return-void
.end method
