.class public final Lyt;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final A:Ljava/lang/String;

.field private static final B:[Ljava/lang/String;

.field private static final C:[Ljava/lang/String;

.field private static final D:[Ljava/lang/String;

.field public static final a:Z

.field static final c:[Ljava/lang/String;

.field private static final f:[Ljava/lang/String;

.field private static final g:[Ljava/lang/String;

.field private static final h:[Ljava/lang/String;

.field private static final i:Ljava/util/Random;

.field private static final j:[Ljava/lang/String;

.field private static final k:Ljava/lang/String;

.field private static l:Ljava/lang/String;

.field private static m:Ljava/lang/String;

.field private static n:Ljava/lang/String;

.field private static final o:[Ljava/lang/String;

.field private static final p:[Ljava/lang/String;

.field private static final q:[Ljava/lang/String;

.field private static final r:[Ljava/lang/String;

.field private static final s:[Ljava/lang/String;

.field private static final t:[Ljava/lang/String;

.field private static final u:[Ljava/lang/String;

.field private static final v:[Ljava/lang/String;

.field private static final w:[Ljava/lang/String;

.field private static final x:Ljava/lang/String;

.field private static final y:[Ljava/lang/String;

.field private static final z:[Ljava/lang/String;


# instance fields
.field public final b:Lzr;

.field private final d:Lyj;

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 84
    sget-object v0, Lbys;->c:Lcyp;

    sput-boolean v5, Lyt;->a:Z

    .line 102
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "gaia_id"

    aput-object v1, v0, v5

    const-string v1, "chat_id"

    aput-object v1, v0, v6

    const-string v1, "name"

    aput-object v1, v0, v7

    const-string v1, "first_name"

    aput-object v1, v0, v8

    const-string v1, "profile_photo_url"

    aput-object v1, v0, v9

    const/4 v1, 0x5

    const-string v2, "packed_circle_ids"

    aput-object v2, v0, v1

    sput-object v0, Lyt;->f:[Ljava/lang/String;

    .line 118
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v5

    const-string v1, "gaia_id"

    aput-object v1, v0, v6

    const-string v1, "chat_id"

    aput-object v1, v0, v7

    const-string v1, "phone_id"

    aput-object v1, v0, v8

    const-string v1, "circle_id"

    aput-object v1, v0, v9

    const/4 v1, 0x5

    const-string v2, "full_name"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "first_name"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "fallback_name"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "profile_photo_url"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "batch_gebi_tag"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "blocked"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "participant_type"

    aput-object v2, v0, v1

    sput-object v0, Lyt;->g:[Ljava/lang/String;

    .line 145
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "type"

    aput-object v1, v0, v5

    const-string v1, "transport_type"

    aput-object v1, v0, v6

    sput-object v0, Lyt;->h:[Ljava/lang/String;

    .line 151
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lyt;->i:Ljava/util/Random;

    .line 175
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v5

    sput-object v0, Lyt;->j:[Ljava/lang/String;

    .line 210
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "((%s=%d) AND (%s=%d OR %s=%d) AND (%s=%d))"

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "transport_type"

    aput-object v3, v2, v5

    .line 214
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    const-string v3, "sms_type"

    aput-object v3, v2, v7

    .line 216
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v8

    const-string v3, "sms_type"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    .line 218
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "status"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    .line 220
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 210
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lyt;->k:Ljava/lang/String;

    .line 1037
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "conversation_id"

    aput-object v1, v0, v5

    sput-object v0, Lyt;->o:[Ljava/lang/String;

    .line 2023
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "notification_level"

    aput-object v1, v0, v5

    const-string v1, "view"

    aput-object v1, v0, v6

    sput-object v0, Lyt;->c:[Ljava/lang/String;

    .line 2189
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "latest_message_timestamp"

    aput-object v1, v0, v5

    const-string v1, "latest_message_expiration_timestamp"

    aput-object v1, v0, v6

    const-string v1, "snippet_type"

    aput-object v1, v0, v7

    const-string v1, "snippet_text"

    aput-object v1, v0, v8

    const-string v1, "snippet_author_chat_id"

    aput-object v1, v0, v9

    const/4 v1, 0x5

    const-string v2, "snippet_image_url"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "chat_watermark"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "hangout_watermark"

    aput-object v2, v0, v1

    sput-object v0, Lyt;->p:[Ljava/lang/String;

    .line 2269
    new-array v0, v9, [Ljava/lang/String;

    const-string v1, "gaia_id"

    aput-object v1, v0, v5

    const-string v1, "chat_id"

    aput-object v1, v0, v6

    const-string v1, "circle_id"

    aput-object v1, v0, v7

    const-string v1, "phone_id"

    aput-object v1, v0, v8

    sput-object v0, Lyt;->q:[Ljava/lang/String;

    .line 3221
    const/16 v0, 0x22

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "message_id"

    aput-object v1, v0, v5

    const-string v1, "conversation_id"

    aput-object v1, v0, v6

    const-string v1, "author_chat_id"

    aput-object v1, v0, v7

    const-string v1, "text"

    aput-object v1, v0, v8

    const-string v1, "status"

    aput-object v1, v0, v9

    const/4 v1, 0x5

    const-string v2, "type"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "timestamp"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "expiration_timestamp"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "local_url"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "remote_url"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "image_id"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "album_id"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "stream_id"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "attachment_content_type"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "attachment_name"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "latitude"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "longitude"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "attachment_target_url"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "transport_type"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "transport_phone"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "sms_type"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "width_pixels"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "height_pixels"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "mms_subject"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "image_rotation"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "author_gaia_id"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "new_conversation_name"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "participant_keys"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "forwarded_mms_url"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "forwarded_mms_count"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "attachment_description"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "attachment_target_url_description"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "attachment_target_url_name"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "call_media_type"

    aput-object v2, v0, v1

    sput-object v0, Lyt;->r:[Ljava/lang/String;

    .line 3396
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "local_url"

    aput-object v1, v0, v5

    sput-object v0, Lyt;->s:[Ljava/lang/String;

    .line 4009
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "otr_toggle"

    aput-object v1, v0, v5

    sput-object v0, Lyt;->t:[Ljava/lang/String;

    .line 4185
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "conversation_id"

    aput-object v1, v0, v5

    const-string v1, "self_watermark"

    aput-object v1, v0, v6

    sput-object v0, Lyt;->u:[Ljava/lang/String;

    .line 4662
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "local_url"

    aput-object v1, v0, v5

    sput-object v0, Lyt;->v:[Ljava/lang/String;

    .line 4663
    new-array v0, v7, [Ljava/lang/String;

    sput-object v0, Lyt;->w:[Ljava/lang/String;

    .line 6479
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s=? AND %s=?"

    new-array v2, v7, [Ljava/lang/Object;

    const-string v3, "conversation_id"

    aput-object v3, v2, v5

    const-string v3, "participant_type"

    aput-object v3, v2, v6

    .line 6480
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lyt;->x:Ljava/lang/String;

    .line 7360
    new-array v0, v9, [Ljava/lang/String;

    const-string v1, "first_peak_scroll_time"

    aput-object v1, v0, v5

    const-string v1, "first_peak_scroll_to_message_timestamp"

    aput-object v1, v0, v6

    const-string v1, "second_peak_scroll_time"

    aput-object v1, v0, v7

    const-string v1, "second_peak_scroll_to_message_timestamp"

    aput-object v1, v0, v8

    sput-object v0, Lyt;->y:[Ljava/lang/String;

    .line 7484
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "sort_timestamp"

    aput-object v1, v0, v5

    sput-object v0, Lyt;->z:[Ljava/lang/String;

    .line 7562
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s in (select %s from %s where %s)"

    new-array v2, v9, [Ljava/lang/Object;

    const-string v3, "conversation_id"

    aput-object v3, v2, v5

    const-string v3, "conversation_id"

    aput-object v3, v2, v6

    const-string v3, "conversations"

    aput-object v3, v2, v7

    const-string v3, "sort_timestamp<? AND status=? AND transport_type!=3"

    aput-object v3, v2, v8

    .line 7563
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lyt;->A:Ljava/lang/String;

    .line 7635
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "timestamp"

    aput-object v1, v0, v5

    sput-object v0, Lyt;->B:[Ljava/lang/String;

    .line 7850
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "conversation_type"

    aput-object v1, v0, v5

    sput-object v0, Lyt;->C:[Ljava/lang/String;

    .line 8450
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "conversation_id"

    aput-object v1, v0, v5

    sput-object v0, Lyt;->D:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lyj;)V
    .locals 1

    .prologue
    .line 302
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 303
    iput-object p1, p0, Lyt;->d:Lyj;

    .line 304
    iget-object v0, p0, Lyt;->d:Lyj;

    invoke-static {v0}, Lzo;->a(Lyj;)Lzo;

    move-result-object v0

    .line 305
    invoke-virtual {v0}, Lzo;->d()Lzr;

    move-result-object v0

    iput-object v0, p0, Lyt;->b:Lzr;

    .line 306
    return-void
.end method

.method public constructor <init>(Lzr;Lyj;)V
    .locals 0

    .prologue
    .line 297
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 298
    iput-object p2, p0, Lyt;->d:Lyj;

    .line 299
    iput-object p1, p0, Lyt;->b:Lzr;

    .line 300
    return-void
.end method

.method public static A()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 8634
    const-string v0, "Babel"

    const-string v1, "refreshContactsDerivedData"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8635
    invoke-static {}, Lbkb;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 8636
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v2

    .line 8637
    if-eqz v2, :cond_1

    .line 8638
    new-instance v0, Lyt;

    invoke-direct {v0, v2}, Lyt;-><init>(Lyj;)V

    .line 8642
    invoke-static {}, Lbsc;->d()V

    .line 8644
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 8646
    invoke-static {v0, v12}, Lyt;->b(Lyt;Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v4, v4, 0x0

    .line 8647
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 8648
    invoke-static {v0, v12}, Lyt;->a(Lyt;Ljava/lang/String;)I

    move-result v7

    add-int/2addr v4, v7

    .line 8649
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    .line 8650
    const-string v9, "Babel"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "refreshContactsDerivedData timing "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-long v2, v5, v2

    invoke-virtual {v10, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " & "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sub-long v5, v7, v5

    invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v9, v2}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 8653
    if-lez v4, :cond_0

    .line 8654
    invoke-static {v0}, Lyp;->d(Lyt;)V

    goto :goto_0

    .line 8657
    :cond_1
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Can not find account in BabelAccountManager. smsAccountName: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 8658
    invoke-static {v0}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 8657
    invoke-static {v2, v0}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 8661
    :cond_2
    return-void
.end method

.method private static declared-synchronized B()Ljava/lang/String;
    .locals 3

    .prologue
    .line 642
    const-class v1, Lyt;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lyt;->l:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 643
    const-string v0, " WHERE conversation_id=? "

    const/4 v2, 0x1

    .line 644
    invoke-static {v0, v2}, Lyt;->b(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lyt;->l:Ljava/lang/String;

    .line 646
    :cond_0
    sget-object v0, Lyt;->l:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 642
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized C()Ljava/lang/String;
    .locals 3

    .prologue
    .line 653
    const-class v1, Lyt;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lyt;->m:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 654
    const-string v0, " WHERE conversation_id=? "

    const/4 v2, 0x0

    .line 655
    invoke-static {v0, v2}, Lyt;->b(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lyt;->m:Ljava/lang/String;

    .line 657
    :cond_0
    sget-object v0, Lyt;->m:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 653
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized D()Ljava/lang/String;
    .locals 3

    .prologue
    .line 664
    const-class v1, Lyt;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lyt;->n:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 665
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 666
    invoke-static {v0, v2}, Lyt;->b(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lyt;->n:Ljava/lang/String;

    .line 668
    :cond_0
    sget-object v0, Lyt;->n:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 664
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private E()Lzl;
    .locals 3

    .prologue
    .line 7281
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 7282
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getConversationScrollInfo: account="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lyt;->d:Lyj;

    .line 7283
    invoke-virtual {v2}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 7282
    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7286
    :cond_0
    new-instance v0, Lzl;

    invoke-direct {v0}, Lzl;-><init>()V

    .line 7287
    const-string v1, "first_peak_scroll_time"

    invoke-virtual {p0, v1}, Lyt;->O(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, v0, Lzl;->a:J

    .line 7289
    const-string v1, "first_peak_scroll_to_conversation_timestamp"

    invoke-virtual {p0, v1}, Lyt;->O(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, v0, Lzl;->b:J

    .line 7291
    const-string v1, "second_peak_scroll_time"

    invoke-virtual {p0, v1}, Lyt;->O(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, v0, Lzl;->c:J

    .line 7293
    const-string v1, "second_peak_scroll_to_conversation_timestamp"

    invoke-virtual {p0, v1}, Lyt;->O(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, v0, Lzl;->d:J

    .line 7296
    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 744
    sget-boolean v2, Lyt;->a:Z

    if-eqz v2, :cond_2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    move v6, v0

    .line 745
    :goto_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    invoke-static {v0}, Lcwz;->b(Z)V

    .line 746
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 749
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversations"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p3, v2, v3

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 752
    if-eqz v1, :cond_3

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 753
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result p4

    .line 754
    if-eqz v6, :cond_0

    .line 755
    const-string v0, "Babel_db"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", conversationId "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", query "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " returns "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 761
    :cond_0
    if-eqz v1, :cond_1

    .line 762
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 770
    :cond_1
    :goto_1
    return p4

    :cond_2
    move v6, v1

    .line 744
    goto :goto_0

    .line 761
    :cond_3
    if-eqz v1, :cond_4

    .line 762
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 766
    :cond_4
    if-eqz v6, :cond_1

    .line 767
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", conversationId "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", query "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " has no result"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 761
    :catchall_0
    move-exception v0

    move-object v1, v7

    :goto_2
    if-eqz v1, :cond_5

    .line 762
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0

    .line 761
    :catchall_1
    move-exception v0

    goto :goto_2
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 10

    .prologue
    .line 5995
    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object/from16 v5, p6

    invoke-direct/range {v0 .. v6}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v8

    .line 5998
    if-ltz v8, :cond_2

    .line 5999
    const/4 v7, 0x0

    const/4 v6, 0x0

    :try_start_0
    const-string v3, "participant_row_id=? AND conversation_id=?"

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x1

    aput-object p1, v4, v0

    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversation_participants"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v9, "_id"

    aput-object v9, v2, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_0
    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    if-nez v0, :cond_2

    const/4 v6, 0x0

    const/4 v7, 0x0

    :try_start_2
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversation_participants"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "MAX(sequence)"

    aput-object v4, v2, v3

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v1

    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result v0

    :goto_1
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "conversation_id"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "participant_row_id"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "sequence"

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "active"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v2, "conversation_participants"

    invoke-virtual {v0, v2, v1}, Lzr;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 6002
    :cond_2
    return v8

    .line 5999
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_2
    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    :catchall_1
    move-exception v0

    move-object v1, v6

    :goto_3
    if-eqz v1, :cond_4

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    :catchall_2
    move-exception v0

    goto :goto_3

    :catchall_3
    move-exception v0

    goto :goto_2

    :cond_5
    move v0, v7

    goto :goto_1

    :cond_6
    move v0, v7

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/4 v6, -0x1

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 6346
    invoke-direct {p0, p1, p2}, Lyt;->m(Ljava/lang/String;Ljava/lang/String;)V

    .line 6354
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6355
    invoke-static {p2, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 6356
    :cond_0
    const-string v3, "gaia_id=?"

    .line 6357
    new-array v4, v8, [Ljava/lang/String;

    aput-object p1, v4, v1

    .line 6373
    :goto_0
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "participants_view"

    sget-object v2, Lyt;->g:[Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 6377
    if-eqz v1, :cond_1

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6378
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 6379
    invoke-direct {p0, v1, p5}, Lyt;->a(Landroid/database/Cursor;Ljava/lang/String;)Lbdh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_1
    move v0, v6

    .line 6382
    if-eqz v1, :cond_2

    .line 6383
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 6387
    :cond_2
    if-eqz p6, :cond_4

    if-gez v0, :cond_4

    .line 6389
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 6391
    const-string v1, "batch_gebi_tag"

    const-string v2, "-1"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6393
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 6394
    const-string v1, "circle_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6395
    const-string v1, "participant_type"

    const/4 v2, 0x2

    .line 6396
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 6395
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 6413
    :goto_1
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 6414
    const-string v1, "fallback_name"

    invoke-virtual {v0, v1, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6416
    :cond_3
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "participants"

    invoke-virtual {v1, v2, v0}, Lzr;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    long-to-int v0, v0

    .line 6418
    if-gez v0, :cond_4

    .line 6419
    const-string v1, "Babel_db"

    const-string v2, "EsConversationsHelper.insertParticipantShell: insert failed"

    invoke-static {v1, v2}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 6423
    :cond_4
    :goto_2
    return v0

    .line 6358
    :cond_5
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 6359
    const-string v3, "chat_id=?"

    .line 6360
    new-array v4, v8, [Ljava/lang/String;

    aput-object p2, v4, v1

    goto :goto_0

    .line 6361
    :cond_6
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 6362
    const-string v3, "circle_id=?"

    .line 6363
    new-array v4, v8, [Ljava/lang/String;

    aput-object p4, v4, v1

    goto :goto_0

    .line 6364
    :cond_7
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 6365
    const-string v3, "phone_id=?"

    .line 6366
    new-array v4, v8, [Ljava/lang/String;

    aput-object p3, v4, v1

    goto/16 :goto_0

    :cond_8
    move v0, v6

    .line 6368
    goto :goto_2

    .line 6382
    :catchall_0
    move-exception v0

    move-object v1, v7

    :goto_3
    if-eqz v1, :cond_9

    .line 6383
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_9
    throw v0

    .line 6398
    :cond_a
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 6399
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6401
    :cond_b
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 6402
    const-string v1, "chat_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6404
    :cond_c
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 6405
    const-string v1, "phone_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6406
    const-string v1, "participant_type"

    const/4 v2, 0x3

    .line 6407
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 6406
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    .line 6409
    :cond_d
    const-string v1, "participant_type"

    .line 6410
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 6409
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_1

    .line 6382
    :catchall_1
    move-exception v0

    goto :goto_3
.end method

.method public static a(Lyt;Ljava/lang/String;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 8833
    invoke-direct {p0, p1}, Lyt;->am(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 8834
    if-eqz v3, :cond_3

    move v2, v1

    .line 8836
    :goto_0
    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 8838
    invoke-direct {p0, v3}, Lyt;->a(Landroid/database/Cursor;)Lyv;

    move-result-object v0

    .line 8837
    invoke-virtual {p0, v0}, Lyt;->a(Lyv;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, v0, Lyv;->t:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, v0, Lyv;->t:Ljava/lang/String;

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    goto :goto_0

    :cond_0
    iget-object v5, v0, Lyv;->t:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, v0, Lyv;->t:Ljava/lang/String;

    const-string v6, "SYNTH:"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    move v0, v1

    goto :goto_1

    :cond_1
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    const-string v6, "merge_key"

    invoke-virtual {v5, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "conversation_id"

    iget-object v7, v0, Lyv;->s:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lyt;->e()Lzr;

    move-result-object v6

    const-string v7, "merge_keys"

    invoke-virtual {v6, v7, v5}, Lzr;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    invoke-virtual {p0}, Lyt;->f()Lyj;

    iget-object v0, v0, Lyv;->s:Ljava/lang/String;

    invoke-static {v0, v4}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    goto :goto_1

    .line 8841
    :cond_2
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    move v1, v2

    .line 8845
    :cond_3
    if-lez v1, :cond_4

    .line 8846
    invoke-direct {p0, p1}, Lyt;->ar(Ljava/lang/String;)V

    .line 8862
    :cond_4
    return v1

    .line 8841
    :catchall_0
    move-exception v0

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static a(Lyt;Ljava/lang/String;Lbsc;)I
    .locals 12

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v11, 0x0

    .line 8963
    invoke-virtual {p0}, Lyt;->e()Lzr;

    move-result-object v0

    .line 8964
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 8966
    const-string v1, "participants"

    sget-object v2, Lzk;->a:[Ljava/lang/String;

    const-string v3, "participant_type=3"

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    move-object v2, v1

    .line 8985
    :goto_0
    if-eqz v2, :cond_a

    .line 8989
    :try_start_0
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 8990
    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    move v1, v11

    .line 8991
    :cond_0
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 8992
    const/4 v6, 0x0

    invoke-interface {v2, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 8994
    const/4 v8, 0x1

    invoke-interface {v2, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 8996
    const/4 v9, 0x2

    invoke-interface {v2, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 8998
    const/4 v10, 0x3

    invoke-interface {v2, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 9001
    invoke-direct {p0, p2, v8, v9, v10}, Lyt;->a(Lbsc;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lzj;

    move-result-object v8

    .line 9003
    iget-object v11, v8, Lzj;->a:Ljava/lang/String;

    invoke-static {v11, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1

    iget-object v9, v8, Lzj;->b:Ljava/lang/String;

    .line 9004
    invoke-static {v9, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 9005
    :cond_1
    invoke-virtual {v3}, Landroid/content/ContentValues;->clear()V

    .line 9011
    const-string v9, "full_name"

    iget-object v10, v8, Lzj;->a:Ljava/lang/String;

    invoke-virtual {v3, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 9012
    const-string v9, "profile_photo_url"

    iget-object v8, v8, Lzj;->b:Ljava/lang/String;

    invoke-virtual {v3, v9, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 9013
    const/4 v8, 0x0

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v5, v8

    .line 9014
    const-string v8, "participants"

    const-string v9, "_id=?"

    invoke-virtual {v0, v8, v3, v9, v5}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v8

    add-int/2addr v1, v8

    .line 9019
    if-nez v4, :cond_2

    .line 9020
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 9026
    :cond_2
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 9049
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    .line 8975
    :cond_3
    const-string v6, "conversation_participants_view"

    sget-object v7, Lzk;->a:[Ljava/lang/String;

    const-string v8, "participant_type=3 AND conversation_id=?"

    new-array v9, v2, [Ljava/lang/String;

    aput-object p1, v9, v11

    move-object v5, v0

    move-object v10, v4

    invoke-virtual/range {v5 .. v10}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    move-object v2, v1

    goto :goto_0

    .line 9030
    :cond_4
    if-eqz v4, :cond_9

    .line 9031
    :try_start_1
    const-string v3, ","

    invoke-static {v3, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    .line 9032
    const-string v4, "SELECT DISTINCT conversations.conversation_id  FROM conversations JOIN conversation_participants  WHERE conversation_participants.conversation_id=conversations.conversation_id  AND conversation_participants.participant_row_id IN (?); "

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    invoke-virtual {v0, v4, v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 9034
    if-eqz v3, :cond_6

    :try_start_2
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 9035
    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    add-int/2addr v1, v0

    .line 9037
    :cond_5
    const/4 v0, 0x0

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 9038
    invoke-static {p0, v0}, Lyp;->b(Lyt;Ljava/lang/String;)V

    .line 9040
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v0

    if-nez v0, :cond_5

    :cond_6
    move v0, v1

    .line 9043
    if-eqz v3, :cond_7

    .line 9044
    :try_start_3
    invoke-interface {v3}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 9049
    :cond_7
    :goto_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 9052
    :goto_3
    return v0

    .line 9043
    :catchall_1
    move-exception v0

    if-eqz v3, :cond_8

    .line 9044
    :try_start_4
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_9
    move v0, v1

    goto :goto_2

    :cond_a
    move v0, v11

    goto :goto_3
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)J
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 776
    sget-boolean v2, Lyt;->a:Z

    if-eqz v2, :cond_2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    move v6, v0

    .line 777
    :goto_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    invoke-static {v0}, Lcwz;->b(Z)V

    .line 778
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 781
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversations"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p3, v2, v3

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 784
    if-eqz v1, :cond_3

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 785
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide p4

    .line 786
    if-eqz v6, :cond_0

    .line 787
    const-string v0, "Babel_db"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", conversationId "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", query "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " returns "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 793
    :cond_0
    if-eqz v1, :cond_1

    .line 794
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 802
    :cond_1
    :goto_1
    return-wide p4

    :cond_2
    move v6, v1

    .line 776
    goto :goto_0

    .line 793
    :cond_3
    if-eqz v1, :cond_4

    .line 794
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 798
    :cond_4
    if-eqz v6, :cond_1

    .line 799
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", conversationId "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", query "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " has no result"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 793
    :catchall_0
    move-exception v0

    move-object v1, v7

    :goto_2
    if-eqz v1, :cond_5

    .line 794
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0

    .line 793
    :catchall_1
    move-exception v0

    goto :goto_2
.end method

.method private static a(Lzl;)J
    .locals 12

    .prologue
    const-wide/16 v10, 0x2

    .line 7252
    if-nez p0, :cond_1

    .line 7253
    const-wide/16 v0, 0x0

    .line 7273
    :cond_0
    :goto_0
    return-wide v0

    .line 7256
    :cond_1
    invoke-static {}, Lbrc;->c()Lbrc;

    move-result-object v0

    .line 7257
    invoke-virtual {v0}, Lbrc;->d()J

    move-result-wide v2

    .line 7258
    const-wide v0, 0x7fffffffffffffffL

    .line 7259
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 7262
    iget-wide v6, p0, Lzl;->a:J

    mul-long v8, v10, v2

    add-long/2addr v6, v8

    cmp-long v6, v4, v6

    if-gez v6, :cond_2

    .line 7263
    iget-wide v0, p0, Lzl;->b:J

    .line 7267
    :cond_2
    iget-wide v6, p0, Lzl;->c:J

    mul-long/2addr v2, v10

    add-long/2addr v2, v6

    cmp-long v2, v4, v2

    if-gez v2, :cond_0

    .line 7268
    iget-wide v2, p0, Lzl;->d:J

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 7269
    iget-wide v0, p0, Lzl;->d:J

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Lbsc;)Laea;
    .locals 11

    .prologue
    const-wide/16 v6, -0x1

    const/4 v2, 0x0

    .line 5809
    invoke-virtual {p1, p0}, Lbsc;->c(Ljava/lang/String;)Lcvw;

    move-result-object v0

    .line 5810
    if-eqz v0, :cond_0

    .line 5811
    invoke-static {v0}, Laea;->a(Lcvw;)Laea;

    move-result-object v0

    .line 5871
    :goto_0
    return-object v0

    .line 5814
    :cond_0
    invoke-virtual {p1, p0}, Lbsc;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 5816
    if-eqz v1, :cond_6

    .line 5820
    const/4 v0, 0x0

    .line 5821
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move v5, v0

    move-wide v3, v6

    move-object v8, v2

    move-object v1, v2

    :cond_1
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcvw;

    .line 5822
    invoke-interface {v0}, Lcvw;->a()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 5823
    if-nez v1, :cond_3

    .line 5835
    invoke-interface {v0}, Lcvw;->a()Ljava/lang/String;

    move-result-object v9

    .line 5842
    :goto_2
    if-nez v8, :cond_2

    invoke-interface {v0}, Lcvw;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 5843
    invoke-interface {v0}, Lcvw;->h()Ljava/lang/String;

    move-result-object v8

    .line 5847
    :cond_2
    cmp-long v1, v3, v6

    if-nez v1, :cond_9

    .line 5848
    invoke-interface {v0}, Lcvw;->b()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 5850
    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v0

    :goto_3
    move-wide v3, v0

    move-object v1, v9

    .line 5851
    goto :goto_1

    .line 5836
    :cond_3
    invoke-interface {v0}, Lcvw;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_8

    .line 5837
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ", "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Lcvw;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 5838
    const/4 v5, 0x1

    goto :goto_2

    .line 5857
    :cond_4
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    cmp-long v0, v3, v6

    if-eqz v0, :cond_7

    .line 5858
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lbrz;->a(Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v8

    move-object v3, v8

    .line 5861
    :goto_4
    if-eqz v5, :cond_5

    move-object v3, v2

    .line 5865
    :cond_5
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 5866
    new-instance v0, Laea;

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-direct/range {v0 .. v6}, Laea;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Iterable;Ljava/lang/Iterable;Ljava/lang/Iterable;)V

    goto/16 :goto_0

    :cond_6
    move-object v0, v2

    .line 5871
    goto/16 :goto_0

    :cond_7
    move-object v3, v8

    goto :goto_4

    :cond_8
    move-object v9, v1

    goto/16 :goto_2

    :cond_9
    move-wide v0, v3

    goto :goto_3
.end method

.method private a(Landroid/database/Cursor;Ljava/lang/String;)Lbdh;
    .locals 12

    .prologue
    .line 6127
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 6128
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 6129
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 6130
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 6131
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 6132
    const/4 v0, 0x5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 6133
    const/4 v0, 0x6

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 6134
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6135
    const/4 v0, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 6137
    :goto_0
    const/16 v0, 0x8

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 6138
    const/16 v0, 0x9

    .line 6139
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 6140
    const/4 v10, 0x0

    .line 6141
    const/16 v0, 0xa

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 6142
    sget-object v10, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 6144
    :cond_0
    const/16 v0, 0xb

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 6146
    invoke-static/range {v0 .. v10}, Lbdh;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)Lbdh;

    move-result-object v0

    .line 6149
    iget-object v1, p0, Lyt;->d:Lyj;

    invoke-static {v1, v0, v11}, Laal;->a(Lyj;Lbdh;Ljava/lang/String;)V

    .line 6151
    return-object v0

    :cond_1
    move-object v7, p2

    goto :goto_0
.end method

.method public static a(J)Ljava/lang/String;
    .locals 2

    .prologue
    .line 378
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "client_generated:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/database/Cursor;)Lyv;
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 555
    new-instance v3, Lyv;

    invoke-direct {v3}, Lyv;-><init>()V

    .line 556
    const/16 v0, 0x14

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lyv;->s:Ljava/lang/String;

    .line 557
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, v3, Lyv;->a:Z

    .line 558
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v3, Lyv;->b:I

    .line 559
    const/16 v0, 0x9

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v3, Lyv;->k:I

    .line 560
    const/16 v0, 0x10

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v3, Lyv;->q:J

    .line 561
    const/16 v0, 0x11

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lyv;->t:Ljava/lang/String;

    .line 562
    const/16 v0, 0x12

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v3, Lyv;->u:J

    .line 564
    const/16 v0, 0x13

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v3, Lyv;->r:I

    .line 565
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 566
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 567
    const/4 v0, 0x0

    iput-object v0, v3, Lyv;->d:Ljava/lang/String;

    .line 571
    :goto_1
    const/16 v0, 0xd

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lyv;->o:Ljava/lang/String;

    .line 573
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    iput-boolean v0, v3, Lyv;->e:Z

    .line 575
    invoke-interface {p1, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 576
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    iput-object v0, v3, Lyv;->f:[B

    .line 579
    :cond_0
    const/4 v0, 0x5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v3, Lyv;->g:J

    .line 581
    const/4 v0, 0x6

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v3, Lyv;->i:I

    .line 583
    new-instance v0, Lbdk;

    const/4 v4, 0x7

    .line 584
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x8

    .line 585
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v4, v5}, Lbdk;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, v3, Lyv;->j:Lbdk;

    .line 586
    const/16 v0, 0xa

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v3, Lyv;->n:J

    .line 588
    const/16 v0, 0xe

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_4

    :goto_3
    iput-boolean v1, v3, Lyv;->p:Z

    .line 590
    const/16 v0, 0xf

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v3, Lyv;->c:I

    .line 592
    const/16 v0, 0xb

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v3, Lyv;->l:I

    .line 593
    const/16 v0, 0xc

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v3, Lyv;->m:I

    .line 595
    invoke-direct {p0, p1}, Lyt;->b(Landroid/database/Cursor;)Ljava/util/List;

    move-result-object v0

    iput-object v0, v3, Lyv;->h:Ljava/util/List;

    .line 597
    return-object v3

    :cond_1
    move v0, v2

    .line 557
    goto/16 :goto_0

    .line 569
    :cond_2
    iput-object v0, v3, Lyv;->d:Ljava/lang/String;

    goto :goto_1

    :cond_3
    move v0, v2

    .line 573
    goto :goto_2

    :cond_4
    move v1, v2

    .line 588
    goto :goto_3
.end method

.method private a(Lza;Lza;)Lza;
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1361
    iget-object v2, p1, Lza;->a:Ljava/lang/String;

    const-string v3, "client_generated:"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-boolean v2, p1, Lza;->d:Z

    if-eqz v2, :cond_3

    :cond_0
    move-object v7, p2

    move-object p2, p1

    move-object p1, v7

    .line 1369
    :goto_0
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_1

    .line 1370
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "moveMessagesFromClientGeneratedId: moving messages from "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p2, Lza;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lza;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lyt;->ap(Ljava/lang/String;)V

    .line 1374
    :cond_1
    invoke-virtual {p0}, Lyt;->a()V

    .line 1381
    :try_start_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1382
    const-string v1, "conversation_id"

    iget-object v2, p1, Lza;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1383
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "messages"

    const-string v3, "conversation_id=? AND status IN (?, ?, ?)"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p2, Lza;->a:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const/4 v6, 0x1

    .line 1387
    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const/4 v6, 0x3

    .line 1388
    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const/4 v6, 0x4

    .line 1389
    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 1383
    invoke-virtual {v1, v2, v0, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 1393
    sget-boolean v1, Lyt;->a:Z

    if-eqz v1, :cond_2

    .line 1394
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "moveMessagesFromClientGeneratedId updated "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " rows"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lyt;->ap(Ljava/lang/String;)V

    .line 1397
    :cond_2
    iget-object v0, p2, Lza;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lyt;->C(Ljava/lang/String;)V

    .line 1398
    invoke-virtual {p0}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1400
    invoke-virtual {p0}, Lyt;->c()V

    .line 1403
    iget-object v0, p0, Lyt;->d:Lyj;

    iget-object v1, p2, Lza;->a:Ljava/lang/String;

    iget-object v2, p1, Lza;->a:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Lyj;Ljava/lang/String;Ljava/lang/String;)V

    .line 1405
    invoke-static {p0}, Lyp;->d(Lyt;)V

    .line 1406
    return-object p1

    .line 1365
    :cond_3
    iget-object v2, p2, Lza;->a:Ljava/lang/String;

    const-string v3, "client_generated:"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    iget-boolean v2, p2, Lza;->d:Z

    if-eqz v2, :cond_5

    :cond_4
    move v0, v1

    :cond_5
    invoke-static {v0}, Lcwz;->a(Z)V

    goto/16 :goto_0

    .line 1400
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lyt;->c()V

    throw v0
.end method

.method private a(Lbsc;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lzj;
    .locals 3

    .prologue
    .line 5774
    invoke-static {p2}, Lbzd;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 5775
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 5779
    :goto_0
    new-instance v0, Lzj;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lzj;-><init>(Lyt;B)V

    .line 5780
    iput-object p3, v0, Lzj;->a:Ljava/lang/String;

    .line 5781
    iput-object p4, v0, Lzj;->b:Ljava/lang/String;

    .line 5783
    invoke-static {p2, p1}, Lyt;->a(Ljava/lang/String;Lbsc;)Laea;

    move-result-object v1

    .line 5784
    if-eqz v1, :cond_1

    .line 5785
    invoke-virtual {v1}, Laea;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 5786
    invoke-virtual {v1}, Laea;->e()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lzj;->a:Ljava/lang/String;

    .line 5788
    :cond_0
    invoke-virtual {v1}, Laea;->f()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 5789
    invoke-virtual {v1}, Laea;->f()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lzj;->b:Ljava/lang/String;

    .line 5793
    :cond_1
    return-object v0

    :cond_2
    move-object p2, v0

    goto :goto_0
.end method

.method private static a(Lzl;JJ)Lzl;
    .locals 5

    .prologue
    .line 7217
    invoke-static {}, Lbrc;->c()Lbrc;

    move-result-object v0

    .line 7218
    invoke-virtual {v0}, Lbrc;->d()J

    move-result-wide v1

    .line 7219
    new-instance v0, Lzl;

    invoke-direct {v0}, Lzl;-><init>()V

    .line 7222
    iget-wide v3, p0, Lzl;->c:J

    add-long/2addr v1, v3

    cmp-long v1, p1, v1

    if-gez v1, :cond_1

    .line 7223
    iget-wide v1, p0, Lzl;->d:J

    cmp-long v1, p3, v1

    if-lez v1, :cond_0

    .line 7245
    :goto_0
    return-object p0

    .line 7230
    :cond_0
    iget-wide v1, p0, Lzl;->a:J

    iput-wide v1, v0, Lzl;->a:J

    .line 7231
    iget-wide v1, p0, Lzl;->b:J

    iput-wide v1, v0, Lzl;->b:J

    .line 7233
    iput-wide p1, v0, Lzl;->c:J

    .line 7234
    iput-wide p3, v0, Lzl;->d:J

    :goto_1
    move-object p0, v0

    .line 7245
    goto :goto_0

    .line 7238
    :cond_1
    iget-wide v1, p0, Lzl;->c:J

    iput-wide v1, v0, Lzl;->a:J

    .line 7239
    iget-wide v1, p0, Lzl;->d:J

    iput-wide v1, v0, Lzl;->b:J

    .line 7241
    iput-wide p1, v0, Lzl;->c:J

    .line 7242
    iput-wide p3, v0, Lzl;->d:J

    goto :goto_1
.end method

.method private a(Ljava/lang/String;JZ)V
    .locals 6

    .prologue
    .line 7868
    invoke-virtual {p0, p1}, Lyt;->Y(Ljava/lang/String;)J

    move-result-wide v0

    .line 7870
    if-eqz p4, :cond_0

    .line 7871
    or-long/2addr v0, p2

    .line 7876
    :goto_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 7877
    const-string v3, "is_pending_leave"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 7878
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversations"

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 7883
    return-void

    .line 7873
    :cond_0
    const-wide/16 v2, -0x1

    xor-long/2addr v2, p2

    and-long/2addr v0, v2

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)V
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 2009
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2012
    const-string v1, "conversation_id"

    invoke-virtual {p3, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2015
    :cond_0
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "conversations"

    const-string v3, "conversation_id=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v0

    invoke-virtual {v1, v2, p3, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2018
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    sget-object v2, Lyt;->c:[Ljava/lang/String;

    array-length v3, v2

    :goto_0
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    invoke-virtual {p3, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {p3, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Landroid/content/ContentValues;->size()I

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0, p2}, Lyt;->ai(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_5

    iget-object v0, p0, Lyt;->b:Lzr;

    invoke-virtual {v0}, Lzr;->a()V

    :try_start_0
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v4, p0, Lyt;->b:Lzr;

    const-string v5, "conversations"

    const-string v6, "conversation_id=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v0, v7, v8

    invoke-virtual {v4, v5, v1, v6, v7}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lyt;->b:Lzr;

    invoke-virtual {v1}, Lzr;->c()V

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    iget-object v2, p0, Lyt;->d:Lyj;

    invoke-static {v2}, Lyp;->b(Lyj;)V

    goto :goto_2

    :cond_3
    :try_start_1
    iget-object v0, p0, Lyt;->b:Lzr;

    invoke-virtual {v0}, Lzr;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lyt;->b:Lzr;

    invoke-virtual {v0}, Lzr;->c()V

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    iget-object v1, p0, Lyt;->d:Lyj;

    invoke-static {v1}, Lyp;->b(Lyj;)V

    goto :goto_3

    :cond_4
    throw v0

    .line 2019
    :cond_5
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 6196
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 6197
    invoke-virtual {v0, p2, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6198
    invoke-virtual {v0, p3, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6199
    iget-object v1, p0, Lyt;->b:Lzr;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=? OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p4, v3, v4

    const/4 v4, 0x1

    aput-object p5, v3, v4

    invoke-virtual {v1, p1, v0, v2, v3}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 6201
    return-void
.end method

.method private static a(Lyj;Z)V
    .locals 6

    .prologue
    .line 8668
    new-instance v1, Lyt;

    invoke-direct {v1, p0}, Lyt;-><init>(Lyj;)V

    .line 8671
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "update merge_keys set merge_key=\"%s\"||conversation_id;"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/Object;

    const/4 v5, 0x0

    if-eqz p1, :cond_0

    const-string v0, ""

    :goto_0
    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 8674
    invoke-virtual {v1}, Lyt;->e()Lzr;

    move-result-object v1

    invoke-virtual {v1, v0}, Lzr;->a(Ljava/lang/String;)V

    .line 8678
    return-void

    .line 8671
    :cond_0
    const-string v0, "SYNTH:"

    goto :goto_0
.end method

.method public static a(I)Z
    .locals 1

    .prologue
    .line 408
    const/4 v0, 0x3

    if-lt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 357
    const-string v0, "client_generated:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/String;JJILbdk;Ljava/lang/String;Ljava/lang/String;JILjava/lang/String;Ljava/lang/String;II)Z
    .locals 8

    .prologue
    .line 1688
    invoke-virtual {p0, p1}, Lyt;->m(Ljava/lang/String;)Lzc;

    move-result-object v2

    .line 1690
    iget-wide v3, v2, Lzc;->a:J

    cmp-long v1, v3, p2

    if-gtz v1, :cond_5

    .line 1694
    if-eqz p8, :cond_0

    invoke-virtual/range {p8 .. p8}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v3, 0x1f4

    if-le v1, v3, :cond_0

    .line 1695
    const/4 v1, 0x0

    const/16 v3, 0x1f4

    move-object/from16 v0, p8

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p8

    .line 1698
    :cond_0
    sget-boolean v1, Lyt;->a:Z

    if-eqz v1, :cond_1

    .line 1699
    const-string v1, "Babel_db"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "updateLatestEvent with conversationId="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " timestamp="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " expirationTimestamp"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " type="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " authorId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " text="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p8

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " imageUrl="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p9

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " smsType="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p15

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " voicemailDuration="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1710
    :cond_1
    if-eqz p7, :cond_3

    iget-object v1, p7, Lbdk;->b:Ljava/lang/String;

    .line 1711
    :goto_0
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 1713
    const-string v4, "latest_message_timestamp"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1715
    const/4 v4, 0x1

    if-eq p6, v4, :cond_2

    const/16 v4, 0x8

    if-eq p6, v4, :cond_2

    const/16 v4, 0xa

    if-eq p6, v4, :cond_2

    const/16 v4, 0xb

    if-eq p6, v4, :cond_2

    iget-wide v4, v2, Lzc;->g:J

    cmp-long v4, p2, v4

    if-lez v4, :cond_2

    .line 1720
    const-string v4, "has_chat_notifications"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1723
    :cond_2
    const-wide/16 v4, 0x0

    cmp-long v4, p4, v4

    if-lez v4, :cond_4

    .line 1724
    const-string v4, "latest_message_expiration_timestamp"

    .line 1725
    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    .line 1724
    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1729
    :goto_1
    const-string v4, "snippet_type"

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1730
    const-string v4, "snippet_message_row_id"

    invoke-static/range {p10 .. p11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1731
    const-string v4, "snippet_status"

    invoke-static/range {p12 .. p12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1732
    const-string v4, "snippet_sms_type"

    invoke-static/range {p15 .. p15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1733
    packed-switch p6, :pswitch_data_0

    .line 1813
    :goto_2
    :pswitch_0
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "conversations"

    const-string v4, "conversation_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    .line 1815
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    .line 1813
    invoke-virtual {v1, v2, v3, v4, v5}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1817
    invoke-static {p0}, Lyp;->d(Lyt;)V

    .line 1818
    const/4 v1, 0x1

    .line 1827
    :goto_3
    return v1

    .line 1710
    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 1727
    :cond_4
    const-string v4, "latest_message_expiration_timestamp"

    invoke-virtual {v3, v4}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_1

    .line 1735
    :pswitch_1
    const-string v2, "snippet_author_chat_id"

    invoke-virtual {v3, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1736
    const-string v1, "snippet_image_url"

    move-object/from16 v0, p9

    invoke-virtual {v3, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1737
    const-string v1, "snippet_text"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1738
    const-string v1, "snippet_new_conversation_name"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1739
    const-string v1, "snippet_participant_keys"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1740
    const-string v1, "snippet_voicemail_duration"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_2

    .line 1743
    :pswitch_2
    const-string v2, "snippet_author_chat_id"

    invoke-virtual {v3, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1744
    const-string v1, "snippet_text"

    move-object/from16 v0, p8

    invoke-virtual {v3, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1745
    const-string v1, "snippet_image_url"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1746
    const-string v1, "snippet_new_conversation_name"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1747
    const-string v1, "snippet_participant_keys"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1748
    const-string v1, "snippet_voicemail_duration"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_2

    .line 1751
    :pswitch_3
    const-string v2, "snippet_author_chat_id"

    invoke-virtual {v3, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1752
    const-string v1, "snippet_text"

    move-object/from16 v0, p8

    invoke-virtual {v3, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1753
    const-string v1, "snippet_image_url"

    move-object/from16 v0, p9

    invoke-virtual {v3, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1754
    const-string v1, "snippet_new_conversation_name"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1755
    const-string v1, "snippet_participant_keys"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1756
    const-string v1, "snippet_voicemail_duration"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1759
    :pswitch_4
    const-string v2, "snippet_author_chat_id"

    invoke-virtual {v3, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1760
    const-string v1, "snippet_text"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1761
    const-string v1, "snippet_image_url"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1762
    const-string v1, "snippet_new_conversation_name"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1763
    const-string v1, "snippet_participant_keys"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1764
    const-string v1, "snippet_voicemail_duration"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1767
    :pswitch_5
    const-string v1, "snippet_author_chat_id"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1768
    const-string v1, "snippet_text"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1769
    const-string v1, "snippet_image_url"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1770
    const-string v1, "snippet_new_conversation_name"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1771
    const-string v1, "snippet_participant_keys"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1772
    const-string v1, "snippet_voicemail_duration"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1775
    :pswitch_6
    const-string v2, "snippet_author_chat_id"

    invoke-virtual {v3, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1776
    const-string v1, "snippet_text"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1777
    const-string v1, "snippet_image_url"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1778
    const-string v1, "snippet_new_conversation_name"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1779
    const-string v1, "snippet_participant_keys"

    move-object/from16 v0, p14

    invoke-virtual {v3, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1780
    const-string v1, "snippet_voicemail_duration"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1783
    :pswitch_7
    const-string v2, "snippet_author_chat_id"

    invoke-virtual {v3, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1784
    const-string v1, "snippet_new_conversation_name"

    move-object/from16 v0, p13

    invoke-virtual {v3, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1785
    const-string v1, "snippet_text"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1786
    const-string v1, "snippet_image_url"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1787
    const-string v1, "snippet_participant_keys"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1788
    const-string v1, "snippet_voicemail_duration"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1795
    :pswitch_8
    const-string v1, "previous_latest_timestamp"

    iget-wide v4, v2, Lzc;->a:J

    .line 1796
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 1795
    invoke-virtual {v3, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1797
    const-string v1, "snippet_image_url"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1798
    const-string v1, "snippet_author_chat_id"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1799
    const-string v1, "snippet_new_conversation_name"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1800
    const-string v1, "snippet_participant_keys"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1801
    const-string v1, "snippet_voicemail_duration"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1804
    :pswitch_9
    const-string v2, "snippet_author_chat_id"

    invoke-virtual {v3, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1805
    const-string v1, "snippet_text"

    move-object/from16 v0, p8

    invoke-virtual {v3, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1806
    const-string v1, "snippet_image_url"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1807
    const-string v1, "snippet_new_conversation_name"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1808
    const-string v1, "snippet_participant_keys"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1809
    const-string v1, "snippet_voicemail_duration"

    invoke-static/range {p16 .. p16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_2

    .line 1820
    :cond_5
    sget-boolean v1, Lyt;->a:Z

    if-eqz v1, :cond_6

    .line 1821
    const-string v1, "Babel_db"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "updateLatestMessage skipped. currentLatestMessageTimestamp="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, v2, Lzc;->a:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " passed in timestamp="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1827
    :cond_6
    const/4 v1, 0x0

    goto/16 :goto_3

    .line 1733
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_2
        :pswitch_1
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_5
        :pswitch_8
        :pswitch_3
        :pswitch_0
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public static aj(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 8775
    const-string v0, "SYNTH:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static ak(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 8779
    const-string v0, "CONV:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private al(Ljava/lang/String;)Z
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 539
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversations"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "conversation_id"

    aput-object v4, v2, v3

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 543
    if-eqz v1, :cond_1

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-eqz v0, :cond_1

    .line 544
    if-eqz v1, :cond_0

    .line 548
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    move v0, v6

    .line 551
    :goto_0
    return v0

    .line 547
    :cond_1
    if-eqz v1, :cond_2

    .line 548
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    move v0, v7

    .line 551
    goto :goto_0

    .line 547
    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_1
    if-eqz v1, :cond_3

    .line 548
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 547
    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method private am(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 686
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 687
    iget-object v0, p0, Lyt;->b:Lzr;

    .line 688
    invoke-static {}, Lyt;->B()Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/String;

    aput-object p1, v2, v3

    .line 687
    invoke-virtual {v0, v1, v2}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 695
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_2

    .line 696
    :cond_0
    if-eqz v0, :cond_1

    .line 697
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 699
    :cond_1
    iget-object v0, p0, Lyt;->b:Lzr;

    invoke-static {}, Lyt;->C()Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/String;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 704
    :cond_2
    :goto_0
    return-object v0

    :cond_3
    iget-object v0, p0, Lyt;->b:Lzr;

    .line 705
    invoke-static {}, Lyt;->D()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 704
    invoke-virtual {v0, v1, v2}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0
.end method

.method private an(Ljava/lang/String;)Lbdh;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 6841
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 6842
    const-string v0, "Babel_db"

    const-string v1, "getSuggestedEntityByGaiaId"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 6847
    :cond_0
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "suggested_contacts"

    sget-object v2, Lyt;->f:[Ljava/lang/String;

    const-string v3, "gaia_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 6852
    if-eqz v6, :cond_1

    .line 6853
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6854
    new-instance v0, Lbdk;

    const/4 v1, 0x0

    .line 6856
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    .line 6857
    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lbdk;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x2

    .line 6858
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    .line 6859
    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x4

    .line 6861
    invoke-interface {v6, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x5

    .line 6862
    invoke-interface {v6, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 6854
    invoke-static/range {v0 .. v5}, Lbdh;->a(Lbdk;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lbdh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v7

    .line 6867
    :cond_1
    if-eqz v6, :cond_2

    .line 6868
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 6871
    :cond_2
    return-object v7

    .line 6867
    :catchall_0
    move-exception v0

    move-object v1, v7

    :goto_0
    if-eqz v1, :cond_3

    .line 6868
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 6867
    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_0
.end method

.method private ao(Ljava/lang/String;)Lzl;
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 7376
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 7377
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getMessageScrollInfo: conversationId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " account="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lyt;->d:Lyj;

    .line 7378
    invoke-virtual {v2}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 7377
    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7383
    :cond_0
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversations"

    sget-object v2, Lyt;->y:[Ljava/lang/String;

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 7386
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v5

    const/4 v5, 0x0

    .line 7383
    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 7388
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 7389
    new-instance v0, Lzl;

    invoke-direct {v0}, Lzl;-><init>()V

    .line 7390
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v0, Lzl;->a:J

    .line 7392
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v0, Lzl;->b:J

    .line 7394
    const/4 v2, 0x2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v0, Lzl;->c:J

    .line 7396
    const/4 v2, 0x3

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v0, Lzl;->d:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 7401
    if-eqz v1, :cond_1

    .line 7402
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 7408
    :cond_1
    :goto_0
    return-object v0

    .line 7401
    :cond_2
    if-eqz v1, :cond_3

    .line 7402
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 7406
    :cond_3
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Not able to get message scroll info for conversationId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v6

    .line 7408
    goto :goto_0

    .line 7401
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v6, :cond_4

    .line 7402
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 7401
    :catchall_1
    move-exception v0

    move-object v6, v1

    goto :goto_1
.end method

.method private static ap(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 8152
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[EsConversationsHelper] "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8153
    return-void
.end method

.method private static aq(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 8783
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SYNTH:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private ar(Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 8874
    iget-object v0, p0, Lyt;->b:Lzr;

    invoke-virtual {v0}, Lzr;->a()V

    .line 8876
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 8877
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversations JOIN merge_keys ON (conversations.conversation_id=merge_keys.conversation_id)"

    sget-object v2, Lze;->a:[Ljava/lang/String;

    const-string v3, "conversation_type=1"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    move-object v1, v0

    .line 8890
    :goto_0
    if-eqz v1, :cond_6

    move-object v0, v6

    .line 8891
    :cond_0
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 8892
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 8893
    const/4 v3, 0x1

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 8895
    const/4 v4, 0x2

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 8897
    if-nez v3, :cond_1

    if-eqz v4, :cond_0

    .line 8898
    :cond_1
    if-nez v0, :cond_5

    .line 8899
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 8907
    :goto_2
    if-eqz v3, :cond_2

    .line 8908
    const-string v5, "chat_ringtone_uri"

    invoke-virtual {v0, v5, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 8910
    :cond_2
    if-eqz v4, :cond_3

    .line 8911
    const-string v3, "hangout_ringtone_uri"

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 8913
    :cond_3
    invoke-virtual {v0}, Landroid/content/ContentValues;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 8914
    iget-object v3, p0, Lyt;->b:Lzr;

    const-string v4, "conversations"

    const-string v5, "conversation_id IN (SELECT conversation_id FROM merge_keys WHERE merge_key=?)"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    invoke-virtual {v3, v4, v0, v5, v6}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 8923
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lyt;->b:Lzr;

    invoke-virtual {v1}, Lzr;->c()V

    throw v0

    .line 8883
    :cond_4
    :try_start_1
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversations JOIN merge_keys ON (conversations.conversation_id=merge_keys.conversation_id)"

    sget-object v2, Lze;->a:[Ljava/lang/String;

    const-string v3, "conversation_type=1 AND conversations.conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 8901
    :cond_5
    invoke-virtual {v0}, Landroid/content/ContentValues;->clear()V

    goto :goto_2

    .line 8921
    :cond_6
    iget-object v0, p0, Lyt;->b:Lzr;

    invoke-virtual {v0}, Lzr;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 8923
    iget-object v0, p0, Lyt;->b:Lzr;

    invoke-virtual {v0}, Lzr;->c()V

    .line 8924
    return-void
.end method

.method public static b(Lyt;Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 8937
    invoke-static {}, Lbsc;->a()Lbsc;

    move-result-object v0

    .line 8941
    :try_start_0
    invoke-static {p0, p1, v0}, Lyt;->a(Lyt;Ljava/lang/String;Lbsc;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 8944
    invoke-virtual {v0}, Lbsc;->b()V

    .line 8947
    return v1

    .line 8944
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Lbsc;->b()V

    throw v1
.end method

.method private static b(Lbjc;)Landroid/content/ContentValues;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1916
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1917
    const-string v1, "status"

    iget v2, p0, Lbjc;->l:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1918
    const-string v1, "notification_level"

    iget v2, p0, Lbjc;->k:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1919
    const-string v1, "conversation_type"

    iget v2, p0, Lbjc;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1923
    invoke-virtual {p0}, Lbjc;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1924
    const-string v1, "sort_timestamp"

    iget-wide v2, p0, Lbjc;->t:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1927
    :cond_0
    iget-object v1, p0, Lbjc;->f:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1928
    const-string v1, "name"

    iget-object v2, p0, Lbjc;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1933
    :goto_0
    const-string v1, "metadata_present"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1934
    const-string v1, "is_draft"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1935
    const-string v1, "conversation_hash"

    iget-object v2, p0, Lbjc;->u:[B

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1937
    const-string v1, "otr_status"

    iget v2, p0, Lbjc;->q:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1938
    const-string v1, "otr_toggle"

    iget v2, p0, Lbjc;->r:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1939
    const-string v1, "is_temporary"

    iget-boolean v2, p0, Lbjc;->x:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1941
    const-string v1, "inviter_affinity"

    iget v2, p0, Lbjc;->v:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1943
    iget-object v1, p0, Lbjc;->m:[I

    if-eqz v1, :cond_3

    iget-object v1, p0, Lbjc;->m:[I

    array-length v1, v1

    if-lez v1, :cond_3

    .line 1946
    const-string v1, "view"

    iget-object v2, p0, Lbjc;->m:[I

    aget v2, v2, v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1951
    :goto_1
    iget-object v1, p0, Lbjc;->n:Lbdk;

    .line 1952
    if-eqz v1, :cond_1

    .line 1953
    const-string v2, "inviter_gaia_id"

    iget-object v3, v1, Lbdk;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1954
    const-string v2, "inviter_chat_id"

    iget-object v1, v1, Lbdk;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1957
    :cond_1
    return-object v0

    .line 1930
    :cond_2
    const-string v1, "name"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_0

    .line 1948
    :cond_3
    const-string v1, "view"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private b(Ljava/lang/String;Lbdk;)Landroid/database/Cursor;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 4968
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4986
    :cond_0
    :goto_0
    return-object v5

    .line 4971
    :cond_1
    iget-object v0, p2, Lbdk;->a:Ljava/lang/String;

    iget-object v1, p2, Lbdk;->b:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lyt;->m(Ljava/lang/String;Ljava/lang/String;)V

    .line 4974
    if-eqz p2, :cond_3

    .line 4975
    iget-object v0, p2, Lbdk;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 4976
    const-string v1, "gaia_id"

    .line 4977
    iget-object v0, p2, Lbdk;->a:Ljava/lang/String;

    move-object v6, v0

    move-object v3, v1

    .line 4983
    :goto_1
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4986
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "participants_view"

    new-array v2, v8, [Ljava/lang/String;

    aput-object p1, v2, v7

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v8, [Ljava/lang/String;

    aput-object v6, v4, v7

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    goto :goto_0

    .line 4978
    :cond_2
    iget-object v0, p2, Lbdk;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 4979
    const-string v1, "chat_id"

    .line 4980
    iget-object v0, p2, Lbdk;->b:Ljava/lang/String;

    move-object v6, v0

    move-object v3, v1

    goto :goto_1

    :cond_3
    move-object v6, v5

    move-object v3, v5

    goto :goto_1
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 808
    sget-boolean v2, Lyt;->a:Z

    if-eqz v2, :cond_2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    move v6, v0

    .line 809
    :goto_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    invoke-static {v0}, Lcwz;->b(Z)V

    .line 810
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 813
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversations"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p3, v2, v3

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 816
    if-eqz v1, :cond_3

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 817
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 818
    if-eqz v6, :cond_0

    .line 819
    const-string v2, "Babel_db"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", conversationId "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", query "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " returns "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 825
    :cond_0
    if-eqz v1, :cond_1

    .line 826
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 834
    :cond_1
    :goto_1
    return-object v0

    :cond_2
    move v6, v1

    .line 808
    goto :goto_0

    .line 825
    :cond_3
    if-eqz v1, :cond_4

    .line 826
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 830
    :cond_4
    if-eqz v6, :cond_5

    .line 831
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", conversationId "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", query "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " returns null (no match)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    move-object v0, v7

    .line 834
    goto :goto_1

    .line 825
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v7, :cond_6

    .line 826
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0

    .line 825
    :catchall_1
    move-exception v0

    move-object v7, v1

    goto :goto_2
.end method

.method private static b(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 3

    .prologue
    .line 607
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 608
    const-string v0, "SELECT "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 610
    const/4 v0, 0x0

    :goto_0
    sget-object v2, Lyw;->a:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 611
    if-nez p1, :cond_0

    const/16 v2, 0x15

    if-eq v0, v2, :cond_2

    .line 612
    :cond_0
    if-lez v0, :cond_1

    .line 615
    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 617
    :cond_1
    sget-object v2, Lyw;->a:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 610
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 619
    :cond_2
    const-string v0, " FROM "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 620
    const-string v0, "conversations"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 621
    const/16 v0, 0x20

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 622
    if-eqz p1, :cond_3

    .line 623
    const-string v0, " join conversation_participants_view using (conversation_id) "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 625
    :cond_3
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 626
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 628
    :cond_4
    const-string v0, " GROUP BY conversation_id "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 629
    const-string v0, ";"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 630
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(Landroid/database/Cursor;)Ljava/util/List;
    .locals 25
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 888
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 889
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getColumnCount()I

    move-result v1

    const/16 v2, 0x15

    if-ne v1, v2, :cond_0

    move-object v1, v13

    .line 936
    :goto_0
    return-object v1

    .line 892
    :cond_0
    const/16 v1, 0x15

    .line 893
    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 892
    invoke-static {v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v14

    .line 894
    const/16 v1, 0x16

    .line 895
    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 894
    invoke-static {v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v15

    .line 896
    const/16 v1, 0x17

    .line 897
    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 896
    invoke-static {v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v16

    .line 898
    const/16 v1, 0x18

    .line 899
    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 898
    invoke-static {v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v17

    .line 900
    const/16 v1, 0x1a

    .line 901
    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 900
    invoke-static {v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v18

    .line 902
    const/16 v1, 0x19

    .line 903
    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 902
    invoke-static {v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v19

    .line 904
    const/16 v1, 0x1b

    .line 905
    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 904
    invoke-static {v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v20

    .line 906
    const/16 v1, 0x1c

    .line 907
    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 906
    invoke-static {v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v21

    .line 908
    const/16 v1, 0x1d

    .line 909
    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 908
    invoke-static {v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v22

    .line 910
    const/16 v1, 0x1e

    .line 911
    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 910
    invoke-static {v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v23

    .line 912
    const/16 v1, 0x1f

    .line 913
    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 912
    invoke-static {v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v24

    .line 915
    move-object/from16 v0, p0

    iget-object v1, v0, Lyt;->d:Lyj;

    invoke-virtual {v1}, Lyj;->c()Lbdk;

    .line 916
    const/4 v1, 0x0

    move v12, v1

    :goto_1
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v1

    if-ge v12, v1, :cond_2

    .line 917
    move-object/from16 v0, v21

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 922
    :try_start_0
    move-object/from16 v0, v20

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 927
    invoke-interface {v14, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v15, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 928
    move-object/from16 v0, v18

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    move-object/from16 v0, v22

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    move-object/from16 v0, v23

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 929
    move-object/from16 v0, v24

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    .line 926
    invoke-static/range {v1 .. v11}, Lbdh;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)Lbdh;

    move-result-object v1

    .line 930
    invoke-virtual {v13, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 916
    :cond_1
    :goto_2
    add-int/lit8 v1, v12, 0x1

    move v12, v1

    goto :goto_1

    :cond_2
    move-object v1, v13

    .line 936
    goto/16 :goto_0

    .line 924
    :catch_0
    move-exception v1

    goto :goto_2
.end method

.method private b(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2124
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2125
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    .line 2126
    iget-object v3, v0, Lbdh;->d:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2128
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "participant.circleId not empty. Value = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v0, Lbdh;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcwz;->a(Ljava/lang/String;)V

    .line 2130
    :cond_1
    iget-object v3, p0, Lyt;->d:Lyj;

    invoke-virtual {v3}, Lyj;->c()Lbdk;

    move-result-object v3

    iget-object v4, v0, Lbdh;->b:Lbdk;

    invoke-virtual {v3, v4}, Lbdk;->a(Lbdk;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2131
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2134
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v2}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2135
    return-object v1
.end method

.method private b(Ljava/lang/String;Lbdk;Z)V
    .locals 12

    .prologue
    .line 5596
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 5597
    const-string v0, "conversation_id"

    invoke-virtual {v9, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5600
    const/4 v7, 0x0

    .line 5606
    const/4 v8, 0x0

    .line 5607
    :try_start_0
    iget-object v1, p2, Lbdk;->a:Ljava/lang/String;

    iget-object v2, p2, Lbdk;->b:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    int-to-long v10, v0

    .line 5614
    const-wide/16 v0, -0x1

    cmp-long v0, v10, v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 5616
    :goto_0
    if-nez v0, :cond_2

    .line 5687
    :cond_0
    :goto_1
    return-void

    .line 5614
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 5620
    :cond_2
    iget-object v0, p2, Lbdk;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 5623
    const-string v3, "gaia_id=? AND conversation_id=?"

    .line 5625
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v1, p2, Lbdk;->a:Ljava/lang/String;

    aput-object v1, v4, v0

    const/4 v0, 0x1

    aput-object p1, v4, v0

    .line 5638
    :goto_2
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversation_participants_view"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_id"

    aput-object v6, v2, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 5641
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 5642
    const/4 v0, 0x1

    move v8, v0

    .line 5644
    :cond_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 5645
    const/4 v6, 0x0

    .line 5647
    const/4 v7, 0x0

    .line 5648
    if-nez v8, :cond_b

    .line 5651
    :try_start_2
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversation_participants"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "MAX(sequence)"

    aput-object v4, v2, v3

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v1

    .line 5657
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 5658
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 5661
    :goto_3
    const-string v2, "sequence"

    add-int/lit8 v0, v0, 0x1

    .line 5662
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 5661
    invoke-virtual {v9, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 5664
    if-eqz v1, :cond_4

    .line 5665
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 5669
    :cond_4
    const-string v0, "participant_row_id"

    .line 5670
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 5669
    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 5673
    if-nez v8, :cond_9

    .line 5674
    const-string v1, "active"

    if-eqz p3, :cond_8

    const/4 v0, 0x1

    .line 5675
    :goto_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 5674
    invoke-virtual {v9, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5676
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversation_participants"

    invoke-virtual {v0, v1, v9}, Lzr;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    goto/16 :goto_1

    .line 5626
    :cond_5
    :try_start_4
    iget-object v0, p2, Lbdk;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 5627
    const-string v3, "chat_id=? AND conversation_id=?"

    .line 5629
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v1, p2, Lbdk;->b:Ljava/lang/String;

    aput-object v1, v4, v0

    const/4 v0, 0x1

    aput-object p1, v4, v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_2

    .line 5664
    :catchall_0
    move-exception v0

    move-object v1, v7

    :goto_5
    if-eqz v1, :cond_6

    .line 5665
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0

    .line 5630
    :cond_7
    const/4 v0, 0x0

    :try_start_5
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5631
    const-string v3, "phone_id=? AND conversation_id=?"

    .line 5633
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const/4 v1, 0x0

    aput-object v1, v4, v0

    const/4 v0, 0x1

    aput-object p1, v4, v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_2

    .line 5674
    :cond_8
    const/4 v0, 0x0

    goto :goto_4

    .line 5679
    :cond_9
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversation_participants"

    const-string v2, "participant_row_id=? AND conversation_id=?"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    .line 5682
    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    .line 5679
    invoke-virtual {v0, v1, v9, v2, v3}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_1

    .line 5664
    :catchall_1
    move-exception v0

    goto :goto_5

    :catchall_2
    move-exception v0

    move-object v1, v6

    goto :goto_5

    :cond_a
    move v0, v7

    goto/16 :goto_3

    :cond_b
    move v0, v7

    move-object v1, v6

    goto/16 :goto_3
.end method

.method private b(Lbdk;Ljava/lang/String;)Z
    .locals 10

    .prologue
    const/4 v8, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 5434
    :try_start_0
    iget-object v0, p1, Lbdk;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 5435
    iget-object v0, p1, Lbdk;->a:Ljava/lang/String;

    iget-object v1, p1, Lbdk;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 5440
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "participants_view"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "batch_gebi_tag"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "chat_id"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "gaia_id"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "phone_id"

    aput-object v4, v2, v3

    const-string v3, "gaia_id=? OR chat_id=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v9, p1, Lbdk;->a:Ljava/lang/String;

    aput-object v9, v4, v5

    const/4 v5, 0x1

    iget-object v9, p1, Lbdk;->b:Ljava/lang/String;

    aput-object v9, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 5469
    :goto_0
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 5471
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 5472
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_6

    move v2, v6

    .line 5473
    :goto_1
    const/4 v0, 0x2

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    move v3, v6

    .line 5476
    :goto_2
    if-eqz v1, :cond_0

    .line 5477
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 5482
    :cond_0
    if-nez v2, :cond_9

    .line 5484
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 5485
    const-string v2, "batch_gebi_tag"

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5487
    iget-object v2, p1, Lbdk;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5488
    const-string v0, "chat_id"

    iget-object v2, p1, Lbdk;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5490
    :cond_1
    iget-object v0, p1, Lbdk;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 5491
    const-string v0, "gaia_id"

    iget-object v2, p1, Lbdk;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5495
    :cond_2
    if-nez v3, :cond_8

    .line 5496
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v2, "participants"

    invoke-virtual {v0, v2, v1}, Lzr;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    if-gez v0, :cond_3

    .line 5497
    const-string v0, "Babel_db"

    const-string v1, "EsConversationsHelper.insertParticipantShell: insert failed"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    :goto_3
    move v0, v6

    .line 5512
    :goto_4
    return v0

    .line 5452
    :cond_4
    :try_start_2
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "participants_view"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "batch_gebi_tag"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "chat_id"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "gaia_id"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "phone_id"

    aput-object v4, v2, v3

    const-string v3, "gaia_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v9, p1, Lbdk;->a:Ljava/lang/String;

    aput-object v9, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    goto/16 :goto_0

    .line 5460
    :cond_5
    iget-object v0, p1, Lbdk;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 5462
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "participants_view"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "batch_gebi_tag"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "chat_id"

    aput-object v4, v2, v3

    const-string v3, "chat_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v9, p1, Lbdk;->b:Ljava/lang/String;

    aput-object v9, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v1

    goto/16 :goto_0

    :cond_6
    move v2, v7

    .line 5472
    goto/16 :goto_1

    .line 5476
    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_5
    if-eqz v1, :cond_7

    .line 5477
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v0

    .line 5501
    :cond_8
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v2, "participants"

    const-string v3, "_id=?"

    new-array v4, v6, [Ljava/lang/String;

    aput-object v8, v4, v7

    invoke-virtual {v0, v2, v1, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-gtz v0, :cond_3

    .line 5503
    const-string v0, "Babel_db"

    const-string v1, "EsConversationsHelper.insertParticipantShell:update failed on rowid lookup"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_9
    move v0, v7

    .line 5512
    goto/16 :goto_4

    .line 5476
    :catchall_1
    move-exception v0

    goto :goto_5

    :cond_a
    move-object v0, v8

    move v2, v7

    move v3, v7

    goto/16 :goto_2

    :cond_b
    move-object v1, v8

    goto/16 :goto_0
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 362
    const-string v0, "client_generated:sms:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static c(Ljava/lang/String;)Ljava/lang/Long;
    .locals 3

    .prologue
    .line 382
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 383
    const-string v0, "client_generated:"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 384
    const-string v1, "sms:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 385
    const-string v1, "sms:"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 388
    :cond_0
    :try_start_0
    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 393
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    .line 392
    :cond_1
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Malformed client-generated-id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Lbdk;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 5018
    sget-boolean v1, Lyt;->a:Z

    if-eqz v1, :cond_0

    .line 5019
    const-string v1, "Babel_db"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "queryParticipantFirstName, participantId="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5023
    :cond_0
    :try_start_0
    const-string v1, "first_name"

    invoke-direct {p0, v1, p1}, Lyt;->b(Ljava/lang/String;Lbdk;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 5025
    if-eqz v1, :cond_2

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 5026
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 5029
    if-eqz v1, :cond_1

    .line 5030
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 5033
    :cond_1
    :goto_0
    return-object v0

    .line 5029
    :cond_2
    if-eqz v1, :cond_1

    .line 5030
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 5029
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_1
    if-eqz v1, :cond_3

    .line 5030
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 5029
    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method private static c(Landroid/database/Cursor;)Lza;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1331
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1334
    invoke-interface {p0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1336
    const/4 v0, 0x0

    .line 1341
    :goto_0
    const/4 v4, 0x2

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 1342
    const/4 v5, 0x3

    .line 1343
    invoke-interface {p0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    if-ne v5, v1, :cond_1

    .line 1344
    :goto_1
    new-instance v2, Lza;

    invoke-direct {v2, v3, v0, v4, v1}, Lza;-><init>(Ljava/lang/String;[BIZ)V

    return-object v2

    .line 1338
    :cond_0
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    goto :goto_0

    :cond_1
    move v1, v2

    .line 1343
    goto :goto_1
.end method

.method private d(Lbdk;)I
    .locals 3

    .prologue
    .line 5041
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 5042
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "queryParticipantIdBlocked, participantId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5044
    :cond_0
    const/4 v1, 0x0

    .line 5046
    :try_start_0
    const-string v0, "blocked"

    invoke-direct {p0, v0, p1}, Lyt;->b(Ljava/lang/String;Lbdk;)Landroid/database/Cursor;

    move-result-object v1

    .line 5048
    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5049
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 5052
    if-eqz v1, :cond_1

    .line 5053
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 5056
    :cond_1
    :goto_0
    return v0

    .line 5052
    :cond_2
    if-eqz v1, :cond_3

    .line 5053
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 5056
    :cond_3
    const/4 v0, -0x1

    goto :goto_0

    .line 5052
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_4

    .line 5053
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method public static final d(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 403
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    invoke-static {p0}, Lyt;->c(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v0

    goto :goto_0
.end method

.method private d(Landroid/database/Cursor;)Lzg;
    .locals 42

    .prologue
    .line 3321
    const/4 v1, 0x5

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 3322
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 3323
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 3324
    packed-switch v1, :pswitch_data_0

    .line 3333
    move-object/from16 v0, p0

    iget-object v2, v0, Lyt;->d:Lyj;

    const/4 v4, 0x0

    const/4 v5, 0x2

    .line 3338
    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x1a

    .line 3339
    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0x1b

    .line 3340
    move-object/from16 v0, p1

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/16 v8, 0x21

    .line 3341
    move-object/from16 v0, p1

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    const/4 v9, 0x1

    .line 3333
    invoke-static/range {v1 .. v9}, Lf;->a(ILyj;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v9

    .line 3346
    :pswitch_0
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 3347
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 3349
    :try_start_0
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 3350
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    .line 3352
    :cond_0
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 3353
    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3360
    :cond_1
    :goto_0
    new-instance v4, Lzg;

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v2, 0x1

    .line 3361
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v2, 0x2

    .line 3362
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v2, 0x3

    .line 3363
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v2, 0x6

    .line 3367
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    const/4 v2, 0x7

    .line 3368
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    const/16 v2, 0x8

    .line 3369
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    const/16 v2, 0x9

    .line 3370
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    const/16 v2, 0xa

    .line 3371
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    const/16 v2, 0xb

    .line 3372
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    const/16 v2, 0xc

    .line 3373
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    const/16 v2, 0xd

    .line 3374
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    const/16 v2, 0xe

    .line 3375
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    const/16 v2, 0xf

    .line 3376
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v23

    const/16 v2, 0x10

    .line 3377
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v25

    const/16 v2, 0x11

    .line 3378
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    const/16 v2, 0x12

    .line 3379
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v28

    const/16 v2, 0x13

    .line 3380
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v29

    const/16 v2, 0x14

    .line 3381
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v30

    const/16 v2, 0x15

    .line 3382
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v31

    const/16 v2, 0x16

    .line 3383
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v32

    const/16 v2, 0x17

    .line 3384
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    const/16 v2, 0x18

    .line 3385
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v34

    const/16 v2, 0x1a

    .line 3386
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v35

    const/16 v2, 0x1b

    .line 3387
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v36

    const/16 v2, 0x1c

    .line 3388
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v37

    const/16 v2, 0x1d

    .line 3389
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v38

    const/16 v2, 0x20

    .line 3390
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v39

    const/16 v2, 0x1f

    .line 3391
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v40

    const/16 v2, 0x1e

    .line 3392
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v41

    move v10, v3

    move v11, v1

    invoke-direct/range {v4 .. v41}, Lzg;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;DDLjava/lang/String;ILjava/lang/String;IIILjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v4

    .line 3355
    :catch_0
    move-exception v2

    .line 3356
    const-string v6, "Babel"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, " Read message info with malformed uri: remote="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, " local="

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4, v2}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 3324
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private d(I)V
    .locals 3

    .prologue
    .line 6896
    invoke-static {p1}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 6897
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/hangouts/content/EsProvider;->buildRealtimechatMetadataUri(Lyj;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 6898
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v2

    invoke-static {v2}, Lbsx;->a(Lyj;)Lbsx;

    move-result-object v2

    invoke-virtual {v2, v0}, Lbsx;->e(Ljava/lang/String;)V

    .line 6900
    invoke-static {}, Lzo;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 6901
    return-void
.end method

.method private f(Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 7775
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 7776
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setConversationInviteDisposition, conversationId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", disposition="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7779
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 7780
    const-string v1, "disposition"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 7781
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "conversations"

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 7783
    invoke-static {p0}, Lyp;->d(Lyt;)V

    .line 7784
    return-void
.end method

.method public static g()Landroid/content/Context;
    .locals 1

    .prologue
    .line 352
    invoke-static {}, Lzo;->a()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public static h()Ljava/lang/String;
    .locals 3

    .prologue
    .line 367
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "client_generated:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lyt;->i:Ljava/util/Random;

    invoke-virtual {v1}, Ljava/util/Random;->nextLong()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Math;->abs(J)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static i()Ljava/lang/String;
    .locals 3

    .prologue
    .line 372
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "client_generated:sms:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lyt;->i:Ljava/util/Random;

    invoke-virtual {v1}, Ljava/util/Random;->nextLong()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Math;->abs(J)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private l(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lzg;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 3297
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 3300
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "messages_view"

    sget-object v2, Lyt;->r:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=? "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 3307
    :goto_0
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3308
    invoke-direct {p0, v1}, Lyt;->d(Landroid/database/Cursor;)Lzg;

    move-result-object v0

    .line 3309
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 3312
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_0

    .line 3313
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    .line 3312
    :cond_1
    if-eqz v1, :cond_2

    .line 3313
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 3317
    :cond_2
    return-object v7

    .line 3312
    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_1
.end method

.method private m(Ljava/lang/String;Ljava/lang/String;)V
    .locals 13

    .prologue
    const/4 v7, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 6214
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6215
    invoke-static {p1, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6341
    :cond_0
    :goto_0
    return-void

    .line 6221
    :cond_1
    new-array v4, v10, [Ljava/lang/String;

    aput-object p1, v4, v9

    .line 6224
    new-array v6, v10, [Ljava/lang/String;

    aput-object p2, v6, v9

    .line 6228
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "participants"

    sget-object v2, Lyt;->g:[Ljava/lang/String;

    const-string v3, "gaia_id=?"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v8

    .line 6230
    :try_start_1
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "participants"

    sget-object v2, Lyt;->g:[Ljava/lang/String;

    const-string v3, "chat_id=?"

    const/4 v5, 0x0

    move-object v4, v6

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v6

    .line 6234
    :try_start_2
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v11

    .line 6235
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v12

    .line 6239
    if-nez v11, :cond_2

    if-eqz v12, :cond_3

    :cond_2
    if-eqz v11, :cond_5

    if-eqz v12, :cond_5

    const/4 v0, 0x0

    .line 6241
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    .line 6242
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 6240
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-eqz v0, :cond_5

    :cond_3
    move v0, v10

    .line 6243
    :goto_1
    if-eqz v0, :cond_6

    .line 6334
    if-eqz v8, :cond_4

    .line 6335
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 6337
    :cond_4
    if-eqz v6, :cond_0

    .line 6338
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_5
    move v0, v9

    .line 6240
    goto :goto_1

    .line 6247
    :cond_6
    if-nez v11, :cond_7

    if-eqz v12, :cond_b

    :cond_7
    move v0, v10

    :goto_2
    :try_start_3
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 6250
    const-string v1, "conversations"

    const-string v2, "snippet_author_gaia_id"

    const-string v3, "snippet_author_chat_id"

    move-object v0, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 6253
    const-string v1, "conversations"

    const-string v2, "inviter_gaia_id"

    const-string v3, "inviter_chat_id"

    move-object v0, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 6257
    const-string v1, "dismissed_contacts"

    const-string v2, "gaia_id"

    const-string v3, "chat_id"

    move-object v0, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 6260
    const-string v1, "messages"

    const-string v2, "author_gaia_id"

    const-string v3, "author_chat_id"

    move-object v0, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 6265
    const-string v1, "suggested_contacts"

    const-string v2, "gaia_id"

    const-string v3, "chat_id"

    move-object v0, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 6287
    if-eqz v11, :cond_c

    if-nez v12, :cond_c

    move v2, v10

    .line 6290
    :goto_3
    if-eqz v11, :cond_8

    if-eqz v12, :cond_8

    const/4 v0, 0x0

    .line 6291
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 6292
    :cond_8
    if-eqz v2, :cond_d

    const/4 v0, 0x0

    .line 6293
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 6296
    :goto_4
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 6299
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 6300
    const-string v0, "participant_row_id"

    invoke-virtual {v3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6301
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v4, "conversation_participants"

    const-string v5, "participant_row_id=?"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object v7, v9, v10

    invoke-virtual {v0, v4, v3, v5, v9}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v9

    .line 6305
    invoke-virtual {v3}, Landroid/content/ContentValues;->clear()V

    .line 6311
    const-string v0, "gaia_id"

    invoke-virtual {v3, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 6312
    const-string v0, "chat_id"

    invoke-virtual {v3, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 6313
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v4, "participants"

    const-string v5, "_id=?"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    aput-object v7, v10, v11

    invoke-virtual {v0, v4, v3, v5, v10}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 6315
    invoke-virtual {v3}, Landroid/content/ContentValues;->clear()V

    .line 6319
    :goto_5
    if-eqz v2, :cond_e

    .line 6320
    const-string v2, "chat_id"

    invoke-virtual {v3, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6321
    const-string v2, "participant_type"

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 6322
    const-string v2, "phone_id"

    invoke-virtual {v3, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6326
    :goto_6
    iget-object v2, p0, Lyt;->b:Lzr;

    const-string v4, "participants"

    const-string v5, "_id=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object v1, v7, v10

    invoke-virtual {v2, v4, v3, v5, v7}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 6328
    sget-boolean v2, Lyt;->a:Z

    if-eqz v2, :cond_9

    .line 6329
    const-string v2, "Babel_db"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "offnetwork gaia fixed "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " conversation rows, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " participant(s) were deleted, "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " participant(s) were updated."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 6334
    :cond_9
    if-eqz v8, :cond_a

    .line 6335
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 6337
    :cond_a
    if-eqz v6, :cond_0

    .line 6338
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    :cond_b
    move v0, v9

    .line 6247
    goto/16 :goto_2

    :cond_c
    move v2, v9

    .line 6287
    goto/16 :goto_3

    .line 6293
    :cond_d
    const/4 v0, 0x0

    .line 6294
    :try_start_4
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_4

    .line 6324
    :cond_e
    const-string v2, "gaia_id"

    invoke-virtual {v3, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_6

    .line 6334
    :catchall_0
    move-exception v0

    move-object v1, v6

    move-object v7, v8

    :goto_7
    if-eqz v7, :cond_f

    .line 6335
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 6337
    :cond_f
    if-eqz v1, :cond_10

    .line 6338
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_10
    throw v0

    .line 6334
    :catchall_1
    move-exception v0

    move-object v1, v7

    goto :goto_7

    :catchall_2
    move-exception v0

    move-object v1, v7

    move-object v7, v8

    goto :goto_7

    :cond_11
    move v0, v9

    goto/16 :goto_5
.end method

.method private q(Ljava/lang/String;J)J
    .locals 2

    .prologue
    .line 6921
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v0

    invoke-static {v0}, Lbsx;->a(Lyj;)Lbsx;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lbsx;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static x()V
    .locals 4

    .prologue
    .line 8559
    invoke-static {}, Lbkb;->v()Ljava/util/List;

    move-result-object v0

    .line 8560
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyj;

    .line 8561
    invoke-virtual {v0}, Lyj;->v()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 8562
    new-instance v2, Lyt;

    invoke-direct {v2, v0}, Lyt;-><init>(Lyj;)V

    .line 8570
    const/4 v3, 0x1

    invoke-static {v0, v3}, Lyt;->a(Lyj;Z)V

    .line 8571
    const/4 v3, 0x0

    invoke-static {v2, v3}, Lyt;->a(Lyt;Ljava/lang/String;)I

    .line 8573
    invoke-static {v0}, Lyp;->a(Lyj;)V

    goto :goto_0

    .line 8575
    :cond_1
    return-void
.end method

.method public static y()V
    .locals 3

    .prologue
    .line 8579
    invoke-static {}, Lbkb;->v()Ljava/util/List;

    move-result-object v0

    .line 8580
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyj;

    .line 8581
    invoke-virtual {v0}, Lyj;->v()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 8582
    const/4 v2, 0x0

    invoke-static {v0, v2}, Lyt;->a(Lyj;Z)V

    .line 8585
    invoke-static {v0}, Lyp;->a(Lyj;)V

    goto :goto_0

    .line 8587
    :cond_1
    return-void
.end method

.method public static z()Ljava/lang/String;
    .locals 2

    .prologue
    .line 8624
    const-string v0, "|"

    .line 8625
    invoke-static {v0}, Lebe;->a(Ljava/lang/String;)Lebe;

    move-result-object v0

    invoke-static {}, Lbkb;->C()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lebe;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 8626
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lbci;->a()Lbci;

    move-result-object v1

    iget-object v1, v1, Lbci;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public A(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 4037
    const-string v0, "getConversationOtrStatus"

    const-string v1, "otr_status"

    const/4 v2, 0x1

    invoke-direct {p0, v0, p1, v1, v2}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public B(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 4281
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 4282
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "removeConversationFromDatabase:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4285
    :cond_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversations"

    const-string v2, "conversation_id=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 4289
    if-lez v0, :cond_1

    .line 4290
    invoke-static {p0}, Lyp;->d(Lyt;)V

    .line 4292
    :cond_1
    return-void
.end method

.method public C(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 4295
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 4296
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "deleteConversation:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4299
    :cond_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversations"

    const-string v2, "conversation_id=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4303
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Ljava/lang/String;Lyj;)V

    .line 4304
    return-void
.end method

.method public D(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 4356
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 4357
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "deleteMessageFromConversation, conversationId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4359
    :cond_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "messages"

    const-string v2, "conversation_id=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4362
    return-void
.end method

.method public E(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 4752
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 4753
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setMessageStatus: conversationId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " fromStatus=-1; toStatus=3"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4756
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 4757
    const-string v1, "status"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4761
    const-string v1, "conversation_id=?"

    .line 4769
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    .line 4771
    iget-object v3, p0, Lyt;->b:Lzr;

    const-string v4, "messages"

    invoke-virtual {v3, v4, v0, v1, v2}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4780
    invoke-static {p0, p1}, Lyp;->d(Lyt;Ljava/lang/String;)V

    .line 4781
    return-void
.end method

.method public F(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 5098
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 5099
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ensureLocalParticipantExists, conversationId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5102
    :cond_0
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v4

    .line 5104
    invoke-virtual {v4}, Lyj;->c()Lbdk;

    move-result-object v0

    invoke-virtual {v4}, Lyj;->e()Ljava/lang/String;

    move-result-object v1

    .line 5105
    invoke-virtual {v4}, Lyj;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4}, Lyj;->F()Ljava/lang/String;

    move-result-object v4

    move-object v5, v3

    .line 5103
    invoke-static/range {v0 .. v5}, Lbdh;->a(Lbdk;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lbdh;

    move-result-object v0

    .line 5106
    invoke-virtual {p0, p1, v0}, Lyt;->a(Ljava/lang/String;Lbdh;)V

    .line 5107
    return-void
.end method

.method public G(Ljava/lang/String;)Z
    .locals 10

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 5153
    const/4 v0, 0x0

    :try_start_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 5154
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "blocked_people"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const-string v3, "gaia_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v9, 0x0

    aput-object v9, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 5164
    :goto_0
    if-eqz v1, :cond_3

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-eqz v0, :cond_3

    move v0, v7

    .line 5168
    :goto_1
    if-eqz v1, :cond_0

    .line 5169
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 5172
    :cond_0
    return v0

    .line 5158
    :cond_1
    :try_start_2
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 5159
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "blocked_people"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const-string v3, "chat_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v1

    goto :goto_0

    .line 5168
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_2
    if-eqz v1, :cond_2

    .line 5169
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 5168
    :catchall_1
    move-exception v0

    goto :goto_2

    :cond_3
    move v0, v8

    goto :goto_1

    :cond_4
    move-object v1, v6

    goto :goto_0
.end method

.method public H(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 5251
    const-string v0, "Babel_db"

    const-string v1, "deleteDismissedContact"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5253
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "dismissed_contacts"

    const-string v2, "gaia_id=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 5257
    invoke-static {}, Lzo;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->j:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 5258
    return-void
.end method

.method public I(Ljava/lang/String;)Lbdh;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 6427
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 6428
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cacheParticipantRow for: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 6434
    :cond_0
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "participants_view"

    sget-object v2, Lyt;->g:[Ljava/lang/String;

    const-string v3, "_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 6438
    if-eqz v1, :cond_1

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6439
    const/4 v0, 0x0

    invoke-direct {p0, v1, v0}, Lyt;->a(Landroid/database/Cursor;Ljava/lang/String;)Lbdh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v6

    .line 6442
    :cond_1
    if-eqz v1, :cond_2

    .line 6443
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 6447
    :cond_2
    return-object v6

    .line 6442
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_0
    if-eqz v1, :cond_3

    .line 6443
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 6442
    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public J(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 6486
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 6487
    const-string v1, "active"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 6488
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "conversation_participants"

    sget-object v3, Lyt;->x:Ljava/lang/String;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v5

    const/4 v5, 0x1

    const-string v6, "2"

    .line 6491
    aput-object v6, v4, v5

    .line 6488
    invoke-virtual {v1, v2, v0, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 6494
    return-void
.end method

.method public K(Ljava/lang/String;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    .line 6519
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 6520
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "queryParticipantIdFirstNames ConversationId: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 6522
    :cond_0
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 6523
    const/4 v6, 0x0

    .line 6525
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversation_participants_view"

    const/16 v2, 0x9

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "gaia_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "chat_id"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "circle_id"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "first_name"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "full_name"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "fallback_name"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "profile_photo_url"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "participant_type"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "phone_id"

    aput-object v4, v2, v3

    const-string v3, "conversation_id=? AND active=1"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 6539
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v5

    const-string v5, "sequence ASC"

    .line 6525
    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v6

    .line 6542
    :cond_1
    :goto_0
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 6543
    const/4 v0, 0x3

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 6544
    const/4 v0, 0x6

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 6545
    const/4 v0, 0x7

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 6546
    const/4 v1, 0x4

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 6547
    if-ne v0, v9, :cond_3

    .line 6548
    new-instance v0, Lbdk;

    const/4 v3, 0x0

    .line 6549
    invoke-interface {v6, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x1

    invoke-interface {v6, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v3, v5}, Lbdk;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 6550
    const/4 v3, 0x5

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 6551
    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lbdh;->a(Lbdk;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lbdh;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 6562
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_1
    if-eqz v1, :cond_2

    .line 6563
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 6553
    :cond_3
    if-ne v0, v10, :cond_4

    .line 6554
    const/4 v0, 0x2

    :try_start_2
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 6555
    invoke-static {v0, v1}, Lbdh;->a(Ljava/lang/String;Ljava/lang/String;)Lbdh;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 6556
    :cond_4
    if-ne v0, v11, :cond_1

    .line 6557
    const/16 v0, 0x8

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 6558
    invoke-static {v0, v1, v4}, Lbdh;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lbdh;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 6562
    :cond_5
    if-eqz v6, :cond_6

    .line 6563
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 6566
    :cond_6
    return-object v7

    .line 6562
    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_1
.end method

.method public L(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbcx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 6619
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 6620
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "queryInvitees: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 6622
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 6623
    new-instance v0, Landroid/text/TextUtils$SimpleStringSplitter;

    const/16 v2, 0x7c

    invoke-direct {v0, v2}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    .line 6624
    invoke-virtual {v0, p1}, Landroid/text/TextUtils$SimpleStringSplitter;->setString(Ljava/lang/String;)V

    .line 6625
    invoke-virtual {v0}, Landroid/text/TextUtils$SimpleStringSplitter;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 6626
    invoke-virtual {p0, v0}, Lyt;->I(Ljava/lang/String;)Lbdh;

    move-result-object v0

    .line 6627
    invoke-static {v0}, Lbcx;->a(Lbdh;)Lbcx;

    move-result-object v0

    .line 6628
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 6630
    :cond_1
    return-object v1
.end method

.method public M(Ljava/lang/String;)Lbza;
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 6665
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 6666
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getActiveConversationParticipants ConversationId: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 6668
    :cond_0
    new-instance v7, Lbza;

    invoke-direct {v7}, Lbza;-><init>()V

    .line 6671
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversation_participants_view"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "gaia_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "chat_id"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "phone_id"

    aput-object v4, v2, v3

    const-string v3, "conversation_id=? AND active=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    const-string v8, "1"

    aput-object v8, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 6680
    :goto_0
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 6681
    new-instance v0, Lbdk;

    const/4 v2, 0x0

    .line 6682
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lbdk;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 6681
    invoke-virtual {v7, v0}, Lbza;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 6685
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_1

    .line 6686
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 6685
    :cond_2
    if-eqz v1, :cond_3

    .line 6686
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 6689
    :cond_3
    return-object v7

    .line 6685
    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_1
.end method

.method public N(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 6751
    const-string v0, "Babel_db"

    const-string v1, "deleteSuggestedEntity"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6752
    invoke-direct {p0, p1}, Lyt;->an(Ljava/lang/String;)Lbdh;

    move-result-object v0

    .line 6754
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "suggested_contacts"

    const-string v3, "gaia_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 6757
    if-eqz v0, :cond_0

    .line 6758
    iget-object v1, v0, Lbdh;->e:Ljava/lang/String;

    iget-object v0, v0, Lbdh;->h:Ljava/lang/String;

    invoke-virtual {p0, p1, v1, v0}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 6761
    :cond_0
    invoke-static {}, Lzo;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->g:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 6762
    return-void
.end method

.method public O(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 6917
    const-wide/16 v0, 0x0

    invoke-direct {p0, p1, v0, v1}, Lyt;->q(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public P(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 6925
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v0

    invoke-static {v0}, Lbsx;->a(Lyj;)Lbsx;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbsx;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public Q(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 6993
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lyt;->b(Ljava/lang/String;I)J

    move-result-wide v0

    return-wide v0
.end method

.method public R(Ljava/lang/String;)Lyy;
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 7062
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 7063
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getTimestamps, conversationId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7069
    :cond_0
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversations"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "sort_timestamp"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "self_watermark"

    aput-object v4, v2, v3

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 7074
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v5

    const/4 v5, 0x0

    .line 7069
    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 7077
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 7078
    new-instance v6, Lyy;

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const/4 v0, 0x1

    .line 7079
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-direct {v6, v2, v3, v4, v5}, Lyy;-><init>(JJ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 7082
    :cond_1
    if-eqz v1, :cond_2

    .line 7083
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 7086
    :cond_2
    return-object v6

    .line 7082
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_0
    if-eqz v1, :cond_3

    .line 7083
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 7082
    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public S(Ljava/lang/String;)J
    .locals 6

    .prologue
    .line 7093
    const-string v1, "getSortTimestamp"

    const-string v3, "sort_timestamp"

    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public T(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 7416
    invoke-direct {p0, p1}, Lyt;->ao(Ljava/lang/String;)Lzl;

    move-result-object v0

    invoke-static {v0}, Lyt;->a(Lzl;)J

    move-result-wide v0

    return-wide v0
.end method

.method public U(Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 7709
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 7710
    const-string v1, "notified_for_failure"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 7711
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7712
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "messages"

    const-string v3, "notified_for_failure=?"

    new-array v4, v6, [Ljava/lang/String;

    const-string v5, "0"

    aput-object v5, v4, v7

    invoke-virtual {v1, v2, v0, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 7728
    :goto_0
    return-void

    .line 7718
    :cond_0
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "messages"

    const-string v3, "notified_for_failure=? AND conversation_id=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const-string v5, "0"

    aput-object v5, v4, v7

    aput-object p1, v4, v6

    invoke-virtual {v1, v2, v0, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method public V(Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 7741
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 7743
    iget-object v1, p0, Lyt;->b:Lzr;

    invoke-virtual {v1}, Lzr;->a()V

    .line 7745
    :try_start_0
    const-string v1, "alert_status"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 7746
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "messages"

    const-string v3, "conversation_id=? AND alert_status=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    const-string v6, "1"

    .line 7752
    aput-object v6, v4, v5

    .line 7746
    invoke-virtual {v1, v2, v0, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 7755
    invoke-virtual {v0}, Landroid/content/ContentValues;->clear()V

    .line 7756
    const-string v1, "alert_status"

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 7757
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "messages"

    const-string v3, "conversation_id=? AND alert_status=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    const-string v6, "3"

    .line 7763
    aput-object v6, v4, v5

    .line 7757
    invoke-virtual {v1, v2, v0, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 7765
    iget-object v0, p0, Lyt;->b:Lzr;

    invoke-virtual {v0}, Lzr;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7767
    iget-object v0, p0, Lyt;->b:Lzr;

    invoke-virtual {v0}, Lzr;->c()V

    .line 7768
    return-void

    .line 7767
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lyt;->b:Lzr;

    invoke-virtual {v1}, Lzr;->c()V

    throw v0
.end method

.method public W(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 7796
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 7797
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "confirmConversationInvite, conversationId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7799
    :cond_0
    invoke-virtual {p0}, Lyt;->a()V

    .line 7801
    :try_start_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 7802
    const-string v1, "status"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 7803
    const-string v1, "disposition"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 7804
    const-string v1, "notification_level"

    const/16 v2, 0x1e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 7805
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "conversations"

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 7807
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v0

    invoke-static {v0}, Lyp;->a(Lyj;)V

    .line 7808
    invoke-virtual {p0}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7810
    invoke-virtual {p0}, Lyt;->c()V

    .line 7811
    return-void

    .line 7810
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lyt;->c()V

    throw v0
.end method

.method public X(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 7818
    invoke-virtual {p0}, Lyt;->e()Lzr;

    move-result-object v0

    const-string v1, "conversations"

    sget-object v2, Lyt;->C:[Ljava/lang/String;

    const-string v3, "conversation_id=?"

    new-array v4, v7, [Ljava/lang/String;

    aput-object p1, v4, v6

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 7828
    if-eqz v1, :cond_2

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 7829
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 7830
    if-ne v0, v7, :cond_2

    .line 7832
    invoke-virtual {p0, p1}, Lyt;->K(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 7833
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v2

    invoke-virtual {v2}, Lyj;->c()Lbdk;

    move-result-object v2

    .line 7834
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    .line 7835
    iget-object v0, v0, Lbdh;->b:Lbdk;

    .line 7836
    invoke-virtual {v2, v0}, Lbdk;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 7837
    iget-object v5, v0, Lbdk;->a:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7843
    if-eqz v1, :cond_1

    .line 7844
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 7847
    :cond_1
    :goto_0
    return-object v5

    .line 7843
    :cond_2
    if-eqz v1, :cond_1

    .line 7844
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 7843
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_3

    .line 7844
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public Y(Ljava/lang/String;)J
    .locals 6

    .prologue
    .line 7855
    const-string v1, "getConversationPendingStatus"

    const-string v3, "is_pending_leave"

    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public Z(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 7979
    const-string v0, "getSmsServiceCenter"

    const-string v1, "sms_service_center"

    invoke-direct {p0, v0, p1, v1}, Lyt;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(JJJLjava/lang/String;IIJLyu;I)J
    .locals 3

    .prologue
    .line 3639
    const-string v0, "babel_transport_events"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "babel_enable_clearcut_transport_events"

    const/4 v1, 0x0

    .line 3642
    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3645
    :cond_0
    const-wide/16 v0, 0x0

    .line 3676
    :goto_0
    return-wide v0

    .line 3648
    :cond_1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 3650
    const-wide/16 v1, 0x0

    cmp-long v1, p1, v1

    if-ltz v1, :cond_2

    .line 3651
    const-string v1, "message_row_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 3653
    :cond_2
    const-string v1, "request_trace_id"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 3654
    const-string v1, "client_generated_id"

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 3655
    const-string v1, "event_id"

    invoke-virtual {v0, v1, p7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3656
    const-string v1, "local_timestamp"

    invoke-static {p10, p11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 3657
    const-string v1, "event_type"

    invoke-static {p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3658
    const-string v1, "chat_action"

    invoke-static {p9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3659
    const-string v1, "event_reason"

    invoke-static/range {p13 .. p13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3661
    if-eqz p12, :cond_3

    .line 3662
    const-string v1, "notified"

    iget v2, p12, Lyu;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3663
    const-string v1, "was_newest"

    iget-boolean v2, p12, Lyu;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 3664
    const-string v1, "past_watermark"

    iget-boolean v2, p12, Lyu;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 3665
    const-string v1, "dnd"

    iget-boolean v2, p12, Lyu;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 3666
    const-string v1, "in_focused_conversation"

    iget-boolean v2, p12, Lyu;->e:Z

    .line 3667
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 3666
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 3668
    const-string v1, "active_client_state"

    iget v2, p12, Lyu;->f:I

    .line 3669
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 3668
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3670
    const-string v1, "notification_level"

    iget v2, p12, Lyu;->g:I

    .line 3671
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 3670
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3674
    :cond_3
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "transport_events"

    invoke-virtual {v1, v2, v0}, Lzr;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    goto/16 :goto_0
.end method

.method public a(Landroid/content/ContentValues;)J
    .locals 3

    .prologue
    .line 8330
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 8331
    const-string v0, "Babel_db"

    const-string v1, "insertMmsNotification"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8333
    :cond_0
    iget-object v0, p0, Lyt;->b:Lzr;

    invoke-virtual {v0}, Lzr;->a()V

    .line 8335
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "mms_notification_inds"

    invoke-virtual {v0, v1, p1}, Lzr;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 8339
    iget-object v2, p0, Lyt;->b:Lzr;

    invoke-virtual {v2}, Lzr;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8342
    iget-object v2, p0, Lyt;->b:Lzr;

    invoke-virtual {v2}, Lzr;->c()V

    return-wide v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lyt;->b:Lzr;

    invoke-virtual {v1}, Lzr;->c()V

    throw v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;ILbdk;IJJILjava/lang/String;Ljava/lang/String;)J
    .locals 14

    .prologue
    .line 4709
    const/4 v13, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move/from16 v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    move-wide/from16 v6, p6

    move-wide/from16 v8, p8

    move/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    invoke-virtual/range {v0 .. v13}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;ILbdk;IJJILjava/lang/String;Ljava/lang/String;I)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;ILbdk;IJJILjava/lang/String;Ljava/lang/String;I)J
    .locals 3

    .prologue
    .line 4718
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 4719
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "insertSystemMessage: conversationId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; ts "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6, p7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4722
    :cond_0
    invoke-virtual {p0, p1, p2}, Lyt;->h(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    .line 4723
    if-nez v0, :cond_4

    .line 4724
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 4725
    const-string v1, "message_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4727
    const-string v1, "conversation_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4728
    const-string v1, "author_chat_id"

    iget-object v2, p4, Lbdk;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4729
    const-string v1, "author_gaia_id"

    iget-object v2, p4, Lbdk;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4730
    const-string v1, "status"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4731
    const-string v1, "type"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4732
    const-string v1, "timestamp"

    invoke-static {p6, p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 4733
    const-wide/16 v1, 0x0

    cmp-long v1, p8, v1

    if-lez v1, :cond_1

    .line 4734
    const-string v1, "expiration_timestamp"

    invoke-static {p8, p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 4737
    :cond_1
    const-string v1, "notification_level"

    invoke-static {p10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4738
    if-eqz p11, :cond_2

    .line 4739
    const-string v1, "new_conversation_name"

    invoke-virtual {v0, v1, p11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4741
    :cond_2
    if-eqz p12, :cond_3

    .line 4742
    const-string v1, "participant_keys"

    invoke-virtual {v0, v1, p12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4744
    :cond_3
    const-string v1, "call_media_type"

    invoke-static/range {p13 .. p13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4745
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "messages"

    invoke-virtual {v1, v2, v0}, Lzr;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 4747
    :cond_4
    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Lyz;)J
    .locals 4

    .prologue
    .line 3563
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 3564
    const-string v1, "conversation_id"

    iget-object v2, p1, Lyz;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3565
    const-string v1, "event_id"

    iget-object v2, p1, Lyz;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3566
    const-string v1, "suggestion_id"

    iget-object v2, p1, Lyz;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3567
    const-string v1, "timestamp"

    iget-wide v2, p1, Lyz;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 3568
    const-string v1, "expiration_time_usec"

    iget-wide v2, p1, Lyz;->e:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 3569
    const-string v1, "type"

    iget v2, p1, Lyz;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3570
    const-string v1, "gem_asset_url"

    iget-object v2, p1, Lyz;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3571
    const-string v1, "gem_horizontal_alignment"

    iget v2, p1, Lyz;->h:I

    .line 3572
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 3571
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3574
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "event_suggestions"

    invoke-virtual {v1, v2, v0}, Lzr;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 3576
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 3577
    iget-object v2, p1, Lyz;->a:Ljava/lang/String;

    invoke-static {p0, v2}, Lyp;->e(Lyt;Ljava/lang/String;)V

    .line 3580
    :cond_0
    return-wide v0
.end method

.method public a(Lzf;)J
    .locals 11

    .prologue
    const-wide/16 v8, -0x1

    const/4 v10, 0x0

    const/4 v7, 0x1

    .line 3778
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 3779
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "createMessage, conversationId "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lzf;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", senderId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lzf;->f:Lbdk;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", messageClientGeneratedId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lzf;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3783
    :cond_0
    iget-object v0, p1, Lzf;->c:Ljava/lang/String;

    iget-object v1, p1, Lzf;->a:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lyt;->h(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    .line 3784
    if-eqz v0, :cond_3

    .line 3785
    sget-boolean v1, Lyt;->a:Z

    if-eqz v1, :cond_1

    .line 3786
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "createMessage called for a message already in the database. message id: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lzf;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " conversationId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lzf;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " new notification level "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lzf;->v:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3794
    :cond_1
    iget v1, p1, Lzf;->v:I

    const/16 v2, 0xa

    if-le v1, v2, :cond_2

    .line 3795
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 3796
    const-string v2, "notification_level"

    iget v3, p1, Lzf;->v:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3797
    iget-object v2, p0, Lyt;->b:Lzr;

    const-string v3, "messages"

    const-string v4, "_id=?"

    new-array v5, v7, [Ljava/lang/String;

    .line 3799
    invoke-virtual {v0}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v10

    .line 3797
    invoke-virtual {v2, v3, v1, v4, v5}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3801
    :cond_2
    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v0

    .line 3944
    :goto_0
    return-wide v0

    .line 3802
    :cond_3
    iget-object v1, p1, Lzf;->b:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 3803
    iget-object v0, p1, Lzf;->c:Ljava/lang/String;

    iget-object v1, p1, Lzf;->b:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lyt;->h(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    .line 3804
    if-eqz v6, :cond_6

    .line 3811
    sget-boolean v0, Lyt;->a:Z

    if-nez v0, :cond_4

    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 3812
    :cond_4
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "createMessage found clientGeneratedId "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lzf;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", messageId "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lzf;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " -- fixing up the message"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3818
    :cond_5
    iget-object v0, p1, Lzf;->c:Ljava/lang/String;

    iget-object v1, p1, Lzf;->b:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lyt;->g(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    .line 3820
    if-eqz v0, :cond_6

    .line 3821
    iget-object v1, p1, Lzf;->c:Ljava/lang/String;

    iget-wide v2, p1, Lzf;->i:J

    .line 3823
    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v4

    move-object v0, p0

    .line 3821
    invoke-virtual/range {v0 .. v5}, Lyt;->a(Ljava/lang/String;JJ)V

    :cond_6
    move-object v0, v6

    .line 3828
    :cond_7
    iget-object v1, p1, Lzf;->c:Ljava/lang/String;

    .line 3831
    invoke-direct {p0, v1}, Lyt;->al(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 3832
    const-string v2, "client_generated:"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 3833
    invoke-static {v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3834
    if-eqz v1, :cond_8

    invoke-direct {p0, v1}, Lyt;->al(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 3835
    :cond_8
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Messaging: skip message because conversation doesn\'t exist:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lzf;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " messageId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lzf;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    move-wide v0, v8

    .line 3837
    goto/16 :goto_0

    .line 3839
    :cond_9
    sget-boolean v2, Lyt;->a:Z

    if-eqz v2, :cond_a

    .line 3840
    const-string v2, "Babel_db"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Messaging: conversationId changed from "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lzf;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3851
    :cond_a
    sget-boolean v2, Lyt;->a:Z

    if-eqz v2, :cond_b

    iget-object v2, p1, Lzf;->b:Ljava/lang/String;

    if-eqz v2, :cond_b

    .line 3852
    const-string v2, "Babel_db"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Messaging: saving message:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lzf;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3855
    :cond_b
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 3856
    const-string v3, "message_id"

    iget-object v4, p1, Lzf;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3857
    const-string v3, "conversation_id"

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3858
    iget-object v1, p1, Lzf;->f:Lbdk;

    if-eqz v1, :cond_c

    .line 3859
    const-string v1, "author_chat_id"

    iget-object v3, p1, Lzf;->f:Lbdk;

    iget-object v3, v3, Lbdk;->b:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3860
    const-string v1, "author_gaia_id"

    iget-object v3, p1, Lzf;->f:Lbdk;

    iget-object v3, v3, Lbdk;->a:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3862
    :cond_c
    const-string v1, "text"

    iget-object v3, p1, Lzf;->g:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3863
    const-string v1, "status"

    iget v3, p1, Lzf;->h:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3864
    const-string v1, "notification_level"

    iget v3, p1, Lzf;->v:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3865
    const-string v3, "type"

    iget-boolean v1, p1, Lzf;->u:Z

    if-eqz v1, :cond_14

    move v1, v7

    .line 3866
    :goto_1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 3865
    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3870
    const-string v1, "transport_type"

    iget v3, p1, Lzf;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3871
    const-string v1, "transport_phone"

    iget-object v3, p1, Lzf;->e:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3872
    const-string v1, "timestamp"

    iget-wide v3, p1, Lzf;->i:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 3875
    const-string v1, "width_pixels"

    iget v3, p1, Lzf;->y:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3876
    const-string v1, "height_pixels"

    iget v3, p1, Lzf;->z:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3877
    const-string v1, "image_id"

    iget-object v3, p1, Lzf;->j:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3878
    const-string v1, "album_id"

    iget-object v3, p1, Lzf;->k:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3879
    const-string v1, "image_rotation"

    iget v3, p1, Lzf;->l:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3882
    iget-object v1, p1, Lzf;->m:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 3883
    iget-object v1, p1, Lzf;->m:Ljava/lang/String;

    const-string v3, "content://"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 3884
    const-string v1, "local_url"

    iget-object v3, p1, Lzf;->m:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3892
    :cond_d
    :goto_2
    :try_start_0
    iget-object v1, p1, Lzf;->m:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 3893
    iget-object v1, p1, Lzf;->m:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3900
    :cond_e
    :goto_3
    const-string v1, "attachment_name"

    iget-object v3, p1, Lzf;->n:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3901
    const-string v1, "attachment_description"

    iget-object v3, p1, Lzf;->o:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3902
    const-string v1, "attachment_target_url"

    iget-object v3, p1, Lzf;->r:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3903
    const-string v1, "attachment_target_url_name"

    iget-object v3, p1, Lzf;->s:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3904
    const-string v1, "attachment_target_url_description"

    iget-object v3, p1, Lzf;->t:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3906
    const-string v1, "attachment_content_type"

    iget-object v3, p1, Lzf;->A:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3909
    const-string v1, "latitude"

    iget-wide v3, p1, Lzf;->p:D

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 3910
    const-string v1, "longitude"

    iget-wide v3, p1, Lzf;->q:D

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 3912
    iget-wide v3, p1, Lzf;->w:J

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-lez v1, :cond_f

    .line 3913
    const-string v1, "expiration_timestamp"

    iget-wide v3, p1, Lzf;->w:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 3915
    :cond_f
    const-string v1, "off_the_record"

    iget-boolean v3, p1, Lzf;->x:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 3916
    iget-object v1, p1, Lzf;->B:Ljava/lang/String;

    if-eqz v1, :cond_10

    .line 3917
    const-string v1, "external_ids"

    iget-object v3, p1, Lzf;->B:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3919
    :cond_10
    const-string v1, "sms_timestamp_sent"

    iget-wide v3, p1, Lzf;->C:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 3920
    const-string v1, "sms_priority"

    iget v3, p1, Lzf;->D:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3921
    const-string v1, "sms_message_size"

    iget-wide v3, p1, Lzf;->E:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 3922
    const-string v1, "mms_subject"

    iget-object v3, p1, Lzf;->F:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3923
    iget-object v1, p1, Lzf;->G:Ljava/lang/String;

    if-eqz v1, :cond_11

    .line 3924
    const-string v1, "sms_raw_sender"

    iget-object v3, p1, Lzf;->G:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3926
    :cond_11
    iget-object v1, p1, Lzf;->H:Ljava/lang/String;

    if-eqz v1, :cond_12

    .line 3927
    const-string v1, "sms_raw_recipients"

    iget-object v3, p1, Lzf;->H:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3929
    :cond_12
    const-string v1, "persisted"

    iget-boolean v3, p1, Lzf;->I:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 3930
    const-string v1, "sms_message_status"

    iget v3, p1, Lzf;->J:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3931
    const-string v1, "sms_type"

    iget v3, p1, Lzf;->K:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3932
    const-string v1, "forwarded_mms_url"

    iget-object v3, p1, Lzf;->L:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3933
    const-string v1, "forwarded_mms_count"

    iget v3, p1, Lzf;->M:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3934
    const-string v1, "sending_error"

    iget v3, p1, Lzf;->N:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3935
    const-string v1, "voicemail_length"

    iget v3, p1, Lzf;->O:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3937
    if-eqz v0, :cond_16

    .line 3939
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v3, "messages"

    const-string v4, "_id=?"

    new-array v5, v7, [Ljava/lang/String;

    .line 3941
    invoke-virtual {v0}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v10

    .line 3939
    invoke-virtual {v1, v3, v2, v4, v5}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3942
    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v0

    goto/16 :goto_0

    .line 3845
    :cond_13
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Messaging: skip message because conversation doesn\'t exist:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lzf;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " messageId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lzf;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    move-wide v0, v8

    .line 3847
    goto/16 :goto_0

    .line 3865
    :cond_14
    const/4 v1, 0x2

    goto/16 :goto_1

    .line 3886
    :cond_15
    const-string v1, "remote_url"

    iget-object v3, p1, Lzf;->m:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 3895
    :catch_0
    move-exception v1

    .line 3896
    const-string v3, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, " Writing message info with malformed url="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p1, Lzf;->m:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_3

    .line 3944
    :cond_16
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "messages"

    invoke-virtual {v0, v1, v2}, Lzr;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    goto/16 :goto_0
.end method

.method public a(Lzh;)J
    .locals 3

    .prologue
    .line 3551
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 3552
    const-string v1, "conversation_id"

    iget-object v2, p1, Lzh;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3553
    const-string v1, "message_id"

    iget-object v2, p1, Lzh;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3554
    const-string v1, "url"

    iget-object v2, p1, Lzh;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3555
    const-string v1, "content_type"

    iget-object v2, p1, Lzh;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3556
    const-string v1, "width"

    iget v2, p1, Lzh;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3557
    const-string v1, "height"

    iget v2, p1, Lzh;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3559
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "multipart_attachments"

    invoke-virtual {v1, v2, v0}, Lzr;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6

    .prologue
    .line 7160
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "messages"

    const-string v3, "conversation_id = ? AND status = ?"

    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v4, v2

    const/4 v2, 0x1

    const-string v5, "1"

    .line 7165
    aput-object v5, v4, v2

    const-string v5, "timestamp ASC"

    move-object v2, p2

    .line 7160
    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public a(Lbcn;)Lbdh;
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 6155
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 6156
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getParticipantEntity for: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 6162
    :cond_0
    :try_start_0
    iget-object v0, p1, Lbcn;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 6163
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "participants_view"

    sget-object v2, Lyt;->g:[Ljava/lang/String;

    const-string v3, "gaia_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v7, p1, Lbcn;->a:Ljava/lang/String;

    aput-object v7, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 6177
    :goto_0
    if-eqz v1, :cond_1

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6178
    const/4 v0, 0x0

    invoke-direct {p0, v1, v0}, Lyt;->a(Landroid/database/Cursor;Ljava/lang/String;)Lbdh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v6

    .line 6181
    :cond_1
    if-eqz v1, :cond_2

    .line 6182
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 6186
    :cond_2
    return-object v6

    .line 6169
    :cond_3
    :try_start_2
    iget-object v0, p1, Lbcn;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 6170
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "participants_view"

    sget-object v2, Lyt;->g:[Ljava/lang/String;

    const-string v3, "chat_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v7, p1, Lbcn;->b:Ljava/lang/String;

    aput-object v7, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v1

    goto :goto_0

    .line 6181
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_1
    if-eqz v1, :cond_4

    .line 6182
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 6181
    :catchall_1
    move-exception v0

    goto :goto_1

    :cond_5
    move-object v1, v6

    goto :goto_0
.end method

.method public a(IIJLjava/lang/String;I)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1077
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 1078
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "insertConversation: conversationType="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", temporaryTimestamp="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1081
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1082
    const-string v1, "conversation_type"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1083
    const-string v1, "is_pending_leave"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1084
    const-string v1, "conversation_id"

    invoke-virtual {v0, v1, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1085
    const-string v1, "status"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1086
    const-string v1, "view"

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1087
    const-string v1, "is_draft"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1088
    const-string v1, "has_oldest_message"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1089
    const-string v1, "call_media_type"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1090
    const-string v1, "notification_level"

    const/16 v2, 0x1e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1091
    const-string v1, "disposition"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1092
    const-string v1, "transport_type"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1094
    const-string v1, "otr_status"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1099
    const-string v1, "sort_timestamp"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1103
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "conversations"

    invoke-virtual {v1, v2, v0}, Lzr;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 1104
    return-object p5
.end method

.method public a(Lbdk;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 4996
    sget-boolean v1, Lyt;->a:Z

    if-eqz v1, :cond_0

    .line 4997
    const-string v1, "Babel_db"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "queryParticipantFullName, participantId="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5001
    :cond_0
    :try_start_0
    const-string v1, "full_name"

    invoke-direct {p0, v1, p1}, Lyt;->b(Ljava/lang/String;Lbdk;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 5003
    if-eqz v1, :cond_2

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 5004
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 5007
    if-eqz v1, :cond_1

    .line 5008
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 5011
    :cond_1
    :goto_0
    return-object v0

    .line 5007
    :cond_2
    if-eqz v1, :cond_1

    .line 5008
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 5007
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_1
    if-eqz v1, :cond_3

    .line 5008
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 5007
    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public a(Lbdk;Ljava/lang/String;)Ljava/lang/String;
    .locals 9

    .prologue
    const-wide/16 v4, -0x1

    const/4 v1, 0x0

    .line 5709
    invoke-static {}, Lbsc;->a()Lbsc;

    move-result-object v7

    .line 5714
    :try_start_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5715
    invoke-virtual {p0, p2, v7}, Lyt;->b(Ljava/lang/String;Lbsc;)Laea;

    move-result-object v0

    .line 5717
    if-eqz v0, :cond_6

    .line 5719
    iget-object v2, p0, Lyt;->d:Lyj;

    invoke-virtual {v2}, Lyj;->u()Z

    move-result v2

    if-nez v2, :cond_5

    .line 5720
    invoke-virtual {v0}, Laea;->c()Ljava/lang/String;

    move-result-object v6

    .line 5723
    :goto_0
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 5725
    invoke-virtual {v0}, Laea;->d()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 5726
    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v2

    .line 5727
    :goto_1
    invoke-static {p2}, Lbzd;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 5737
    :goto_2
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 5738
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "g:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 5745
    :goto_3
    invoke-virtual {v7}, Lbsc;->b()V

    .line 5748
    return-object v0

    .line 5734
    :cond_0
    :try_start_1
    iget-object v0, p1, Lbdk;->a:Ljava/lang/String;

    move-wide v2, v4

    move-object v6, v0

    move-object v0, v1

    goto :goto_2

    .line 5739
    :cond_1
    cmp-long v4, v2, v4

    if-eqz v4, :cond_2

    .line 5740
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "c:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 5741
    :cond_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 5742
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "p:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_3

    .line 5745
    :catchall_0
    move-exception v0

    invoke-virtual {v7}, Lbsc;->b()V

    throw v0

    :cond_3
    move-object v0, v1

    goto :goto_3

    :cond_4
    move-wide v2, v4

    goto :goto_1

    :cond_5
    move-object v6, v1

    goto :goto_0

    :cond_6
    move-wide v2, v4

    move-object v6, v1

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lbdk;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 6113
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 6114
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdk;

    .line 6115
    iget-object v2, v0, Lbdk;->a:Ljava/lang/String;

    iget-object v3, v0, Lbdk;->b:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    .line 6117
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 6118
    const/16 v2, 0x7c

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 6120
    :cond_0
    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 6122
    :cond_1
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/List;)Ljava/lang/String;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v10, -0x1

    const/4 v6, 0x0

    .line 2863
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v7, v0, 0x1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    new-array v9, v7, [Ljava/lang/String;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v5, v0

    move v1, v6

    :goto_0
    if-lt v5, v10, :cond_3

    if-ne v5, v10, :cond_1

    iget-object v0, p0, Lyt;->d:Lyj;

    invoke-virtual {v0}, Lyj;->c()Lbdk;

    move-result-object v0

    move-object v3, v2

    move-object v4, v0

    :goto_1
    if-lez v1, :cond_0

    const-string v0, " OR "

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    if-eqz v4, :cond_2

    iget-object v0, v4, Lbdk;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "chat_id=?"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v1, 0x1

    iget-object v3, v4, Lbdk;->b:Ljava/lang/String;

    aput-object v3, v9, v1

    :goto_2
    add-int/lit8 v1, v5, -0x1

    move v5, v1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    iget-object v3, v0, Lbdh;->b:Lbdk;

    invoke-virtual {v0}, Lbdh;->c()Ljava/lang/String;

    move-result-object v0

    move-object v4, v3

    move-object v3, v0

    goto :goto_1

    :cond_2
    if-eqz v4, :cond_a

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    const-string v0, "phone_id=?"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v1, 0x1

    aput-object v3, v9, v1

    goto :goto_2

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SELECT conversation_id FROM conversation_participants_view GROUP BY conversation_id HAVING count(*) = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "SELECT DISTINCT conversation_id FROM conversation_participants_view WHERE conversation_id IN ("

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") AND ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") GROUP BY conversation_id HAVING count(*) = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lyt;->b:Lzr;

    invoke-virtual {v1, v0, v9}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    sget-boolean v3, Lyt;->a:Z

    if-eqz v3, :cond_4

    const-string v3, "Babel_db"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "queryMultiple "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "; selection "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v4, v9, v6

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " ==> "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2865
    :cond_4
    if-eqz v1, :cond_7

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2866
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2870
    sget-boolean v2, Lyt;->a:Z

    if-eqz v2, :cond_5

    .line 2871
    const-string v2, "Babel_db"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "queryGroupPhoneConversation found conversation "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2877
    :cond_5
    if-eqz v1, :cond_6

    .line 2878
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2881
    :cond_6
    :goto_3
    return-object v0

    .line 2877
    :cond_7
    if-eqz v1, :cond_8

    .line 2878
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_8
    move-object v0, v2

    .line 2881
    goto :goto_3

    .line 2877
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_9

    .line 2878
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_9
    throw v0

    :cond_a
    move v0, v1

    goto/16 :goto_2
.end method

.method public a(Lyv;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 5937
    iget-object v0, p1, Lyv;->s:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CONV:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 5939
    invoke-static {}, Lbtf;->k()Z

    move-result v0

    if-nez v0, :cond_1

    .line 5978
    :cond_0
    :goto_0
    return-object v2

    .line 5942
    :cond_1
    iget v0, p1, Lyv;->b:I

    if-ne v0, v7, :cond_0

    .line 5945
    iget v0, p1, Lyv;->k:I

    if-eq v0, v7, :cond_0

    .line 5948
    iget-object v0, p1, Lyv;->t:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lyv;->t:Ljava/lang/String;

    const-string v1, "SYNTH:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5949
    iget-object v2, p1, Lyv;->t:Ljava/lang/String;

    goto :goto_0

    .line 5952
    :cond_2
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v0

    invoke-virtual {v0}, Lyj;->c()Lbdk;

    move-result-object v4

    .line 5953
    const/4 v0, 0x0

    .line 5955
    iget-object v1, p1, Lyv;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v1, v2

    move v3, v0

    :cond_3
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    .line 5956
    iget-object v6, v0, Lbdh;->b:Lbdk;

    invoke-virtual {v4, v6}, Lbdk;->a(Lbdk;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 5957
    add-int/lit8 v3, v3, 0x1

    .line 5960
    iget-object v6, v0, Lbdh;->b:Lbdk;

    invoke-direct {p0, v6}, Lyt;->d(Lbdk;)I

    move-result v6

    if-eq v6, v7, :cond_0

    .line 5963
    iget-object v6, v0, Lbdh;->b:Lbdk;

    iget-object v0, v0, Lbdh;->c:Ljava/lang/String;

    invoke-virtual {p0, v6, v0}, Lyt;->a(Lbdk;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 5964
    if-eqz v0, :cond_7

    :goto_2
    move-object v1, v0

    .line 5967
    goto :goto_1

    .line 5968
    :cond_4
    if-le v3, v7, :cond_6

    .line 5969
    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "computeMergeKeyForConversation with > 1 participants: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 5971
    iget-object v0, p1, Lyv;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    .line 5972
    const-string v4, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "gaia: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 5973
    invoke-virtual {v0}, Lbdh;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; phone: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v0, v0, Lbdh;->c:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5972
    invoke-static {v4, v0}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 5976
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "participant count (not including self) is "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    :cond_6
    move-object v2, v1

    .line 5978
    goto/16 :goto_0

    :cond_7
    move-object v0, v1

    goto :goto_2
.end method

.method public a(II)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v4, 0x1

    .line 7496
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 7497
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getConversationTimestamps: account="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lyt;->d:Lyj;

    .line 7498
    invoke-virtual {v2}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 7497
    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7503
    :cond_0
    :try_start_0
    const-string v3, "transport_type!=3"

    .line 7505
    const/4 v0, -0x1

    if-eq p2, v0, :cond_5

    .line 7506
    if-ne p2, v4, :cond_2

    .line 7508
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND status=? AND inviter_affinity=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 7510
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    .line 7511
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x1

    const-string v1, "1"

    .line 7512
    aput-object v1, v4, v0

    .line 7519
    :goto_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversations"

    sget-object v2, Lyt;->z:[Ljava/lang/String;

    const-string v5, "sort_timestamp DESC"

    .line 7526
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    .line 7519
    invoke-virtual/range {v0 .. v6}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 7528
    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 7529
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 7530
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 7535
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_1

    .line 7536
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 7515
    :cond_2
    :try_start_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND status=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 7516
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 7535
    :catchall_1
    move-exception v0

    move-object v1, v7

    goto :goto_2

    :cond_3
    if-eqz v1, :cond_4

    .line 7536
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    return-object v0

    :cond_5
    move-object v4, v7

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lbdk;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lbdk;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 949
    invoke-static {p2}, Lcwz;->b(Ljava/lang/Object;)V

    .line 950
    iget-object v0, p2, Lbdk;->a:Ljava/lang/String;

    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 952
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversation_participants_view"

    sget-object v2, Lbdj;->a:[Ljava/lang/String;

    const-string v3, "conversation_id=? AND gaia_id!=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    iget-object v7, p2, Lbdk;->a:Ljava/lang/String;

    aput-object v7, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 961
    :try_start_1
    invoke-static {v1}, Lbdh;->a(Landroid/database/Cursor;)Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 963
    if-eqz v1, :cond_0

    .line 964
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 967
    :cond_0
    return-object v0

    .line 963
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_0
    if-eqz v1, :cond_1

    .line 964
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 963
    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lzd;
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 1259
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 1260
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "getExistingMergedConversationId "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lyt;->ap(Ljava/lang/String;)V

    .line 1265
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1266
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1267
    invoke-interface {v4, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1269
    :cond_1
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1270
    invoke-interface {v4, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1272
    :cond_2
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1273
    invoke-interface {v4, p4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1275
    :cond_3
    invoke-interface {v4, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1277
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1288
    const-string v3, "conversation_id=?"

    .line 1292
    :goto_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversations"

    sget-object v2, Lzb;->a:[Ljava/lang/String;

    .line 1294
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    new-array v7, v7, [Ljava/lang/String;

    invoke-interface {v4, v7}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    .line 1292
    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 1297
    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1299
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_4

    .line 1300
    const-string v0, "getExistingMergedConversationId didn\'t find any matches"

    invoke-static {v0}, Lyt;->ap(Ljava/lang/String;)V

    .line 1302
    :cond_4
    new-instance v0, Lzd;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lzd;-><init>(Lza;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1326
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :goto_1
    return-object v0

    .line 1279
    :pswitch_0
    const-string v3, "conversation_id IN (?, ?, ?, ?)"

    goto :goto_0

    .line 1282
    :pswitch_1
    const-string v3, "conversation_id IN (?, ?, ?)"

    goto :goto_0

    .line 1285
    :pswitch_2
    const-string v3, "conversation_id IN (?, ?)"

    goto :goto_0

    .line 1304
    :cond_5
    :try_start_1
    invoke-static {v3}, Lyt;->c(Landroid/database/Cursor;)Lza;

    move-result-object v0

    .line 1305
    sget-boolean v1, Lyt;->a:Z

    if-eqz v1, :cond_6

    .line 1306
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getExistingMergedConversationId found match "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v0, Lza;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lyt;->ap(Ljava/lang/String;)V

    :cond_6
    move v1, v6

    move-object v2, v0

    .line 1311
    :goto_2
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1312
    invoke-static {v3}, Lyt;->c(Landroid/database/Cursor;)Lza;

    move-result-object v0

    .line 1313
    sget-boolean v1, Lyt;->a:Z

    if-eqz v1, :cond_7

    .line 1314
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "getExistingMergedConversationId found another conversation "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v0, Lza;->a:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lyt;->ap(Ljava/lang/String;)V

    .line 1320
    :cond_7
    invoke-direct {p0, v2, v0}, Lyt;->a(Lza;Lza;)Lza;

    move-result-object v1

    .line 1322
    const/4 v0, 0x1

    move-object v2, v1

    move v1, v0

    goto :goto_2

    .line 1324
    :cond_8
    new-instance v0, Lzd;

    invoke-direct {v0, v2, v1}, Lzd;-><init>(Lza;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1326
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v0

    .line 1277
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lbdk;ZI)Lzi;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2964
    sget-boolean v1, Lyt;->a:Z

    if-eqz v1, :cond_0

    .line 2965
    const-string v1, "Babel_db"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "queryOneToOneConversation, participantId "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2967
    :cond_0
    if-eqz p1, :cond_1

    .line 2968
    iget-object v1, p1, Lbdk;->a:Ljava/lang/String;

    iget-object v2, p1, Lbdk;->b:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lyt;->m(Ljava/lang/String;Ljava/lang/String;)V

    .line 2974
    :cond_1
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "select conversations.conversation_id, sort_timestamp, view, transport_type, (select merge_key FROM merge_keys WHERE merge_keys.conversation_id=conversations.conversation_id) as merge_key  FROM conversations,conversation_participants_view"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2975
    const-string v2, " WHERE conversation_participants_view.conversation_id=conversations.conversation_id"

    .line 2994
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 3003
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    .line 3004
    const-string v3, " AND conversation_type=?"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3007
    const/4 v3, 0x0

    const-string v4, "1"

    .line 3008
    aput-object v4, v2, v3

    .line 3010
    const-string v3, " AND transport_type=?"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3011
    const/4 v3, 0x1

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 3013
    const-string v3, " AND "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3014
    if-eqz p1, :cond_7

    iget-object v3, p1, Lbdk;->a:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 3015
    const-string v3, "gaia_id=?"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3016
    const/4 v3, 0x2

    iget-object v4, p1, Lbdk;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 3027
    :goto_0
    if-eqz p2, :cond_2

    .line 3028
    const-string v3, " AND is_temporary = 1"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3029
    :cond_2
    iget-object v3, p0, Lyt;->b:Lzr;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1, v2}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 3034
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 3049
    new-instance v0, Lzi;

    invoke-direct {v0, p0}, Lzi;-><init>(Lyt;)V

    .line 3051
    :cond_3
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 3054
    iget-object v3, v0, Lzi;->a:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, v0, Lzi;->a:Ljava/lang/String;

    .line 3055
    const-string v4, "client_generated:"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 3056
    const-string v3, "client_generated:"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 3057
    :cond_4
    iput-object v2, v0, Lzi;->a:Ljava/lang/String;

    .line 3058
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v0, Lzi;->c:J

    .line 3059
    const/4 v2, 0x2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lzi;->d:I

    .line 3060
    const/4 v2, 0x3

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lzi;->e:I

    .line 3061
    const/4 v2, 0x4

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lzi;->b:Ljava/lang/String;

    .line 3063
    :cond_5
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v2

    if-nez v2, :cond_3

    .line 3067
    if-eqz v1, :cond_6

    .line 3068
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 3071
    :cond_6
    :goto_1
    return-object v0

    .line 3017
    :cond_7
    if-eqz p1, :cond_9

    :try_start_2
    iget-object v3, p1, Lbdk;->b:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 3018
    const-string v3, "chat_id=?"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3019
    const/4 v3, 0x2

    iget-object v4, p1, Lbdk;->b:Ljava/lang/String;

    aput-object v4, v2, v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 3067
    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_2
    if-eqz v1, :cond_8

    .line 3068
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v0

    .line 3022
    :cond_9
    :try_start_3
    const-string v1, "Babel_db"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "queryOneToOneConversation: participantId is empty:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 3067
    :cond_a
    if-eqz v1, :cond_6

    .line 3068
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 3067
    :catchall_1
    move-exception v0

    goto :goto_2
.end method

.method public a()V
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lyt;->b:Lzr;

    invoke-virtual {v0}, Lzr;->a()V

    .line 310
    iget v0, p0, Lyt;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lyt;->e:I

    .line 311
    return-void
.end method

.method public a(IIJLjava/lang/String;)V
    .locals 6

    .prologue
    .line 2819
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 2820
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateConversationOtrStatus, conversationId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", otrStatus="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", otrToggle="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", eventTimestamp="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2826
    :cond_0
    const-string v1, "getLastOtrModificationTime"

    const-string v3, "last_otr_modification_time"

    const-wide/16 v4, -0x1

    move-object v0, p0

    move-object v2, p5

    invoke-direct/range {v0 .. v5}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)J

    move-result-wide v0

    .line 2827
    cmp-long v0, p3, v0

    if-ltz v0, :cond_1

    .line 2828
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2829
    const-string v1, "otr_status"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2830
    const-string v1, "otr_toggle"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2831
    const-string v1, "last_otr_modification_time"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2833
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "conversations"

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p5, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 2836
    if-lez v0, :cond_2

    .line 2837
    invoke-static {p0}, Lyp;->d(Lyt;)V

    .line 2842
    :cond_1
    :goto_0
    return-void

    .line 2839
    :cond_2
    const-string v0, "Babel_db"

    const-string v1, "updateConversationOtrState: failed to update database"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(IJLjava/lang/String;)V
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 2803
    invoke-virtual {p0}, Lyt;->a()V

    .line 2805
    :try_start_0
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateConversationCallMediaType, conversationId "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", callMediaType="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", hangoutEventTimestamp="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v0, "getCallMediaType"

    const-string v1, "call_media_type"

    const/4 v2, 0x0

    invoke-direct {p0, v0, p4, v1, v2}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v8

    const-string v1, "getLastHangoutEventTime"

    const-string v3, "last_hangout_event_time"

    const-wide/16 v4, -0x1

    move-object v0, p0

    move-object v2, p4

    invoke-direct/range {v0 .. v5}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)J

    move-result-wide v2

    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    if-nez v8, :cond_5

    move p1, v6

    :cond_1
    :goto_0
    if-eqz v8, :cond_6

    move v1, v6

    :goto_1
    if-eqz p1, :cond_7

    move v0, v6

    :goto_2
    cmp-long v2, p2, v2

    if-ltz v2, :cond_8

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "call_media_type"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {p0, p4}, Lyt;->m(Ljava/lang/String;)Lzc;

    move-result-object v3

    iget-wide v3, v3, Lzc;->h:J

    cmp-long v3, p2, v3

    if-lez v3, :cond_2

    const-string v3, "has_video_notifications"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_2
    if-eq v1, v0, :cond_3

    const-string v0, "last_hangout_event_time"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_3
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversations"

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p4, v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move v0, v6

    .line 2807
    :goto_3
    invoke-virtual {p0}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2809
    invoke-virtual {p0}, Lyt;->c()V

    .line 2812
    if-eqz v0, :cond_4

    .line 2813
    iget-object v0, p0, Lyt;->d:Lyj;

    invoke-static {v0}, Lyp;->b(Lyj;)V

    .line 2815
    :cond_4
    return-void

    :cond_5
    move p1, v8

    .line 2805
    goto :goto_0

    :cond_6
    move v1, v7

    goto :goto_1

    :cond_7
    move v0, v7

    goto :goto_2

    :cond_8
    move v0, v7

    goto :goto_3

    .line 2809
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lyt;->c()V

    throw v0
.end method

.method public a(JII)V
    .locals 4

    .prologue
    .line 6884
    invoke-static {p3}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 6885
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/hangouts/content/EsProvider;->buildRealtimechatMetadataUri(Lyj;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 6886
    const-wide/16 v2, -0x2

    cmp-long v2, p1, v2

    if-nez v2, :cond_0

    if-lez p4, :cond_0

    .line 6888
    const-wide/16 p1, -0x3

    .line 6890
    :cond_0
    invoke-virtual {p0, v0, p1, p2}, Lyt;->h(Ljava/lang/String;J)V

    .line 6892
    invoke-static {}, Lzo;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 6893
    return-void
.end method

.method public a(JIJ)V
    .locals 5

    .prologue
    .line 4830
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 4831
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setMessageStatusTimestamp: messageRowId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", status="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", ts="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4834
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 4835
    const-string v1, "status"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4836
    const-wide/16 v1, 0x0

    cmp-long v1, p4, v1

    if-ltz v1, :cond_1

    .line 4837
    const-string v1, "timestamp"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 4842
    :cond_1
    const/4 v1, 0x1

    if-ne p3, v1, :cond_2

    .line 4843
    const-string v1, "notified_for_failure"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4845
    :cond_2
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "messages"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 4846
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 4845
    invoke-virtual {v1, v2, v0, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4847
    return-void
.end method

.method public a(JJ)V
    .locals 6

    .prologue
    .line 7315
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 7316
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateConversationScrollTime: scrollTime="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " scrollToConverationTimestamp="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7321
    :cond_0
    invoke-virtual {p0}, Lyt;->a()V

    .line 7323
    :try_start_0
    invoke-direct {p0}, Lyt;->E()Lzl;

    move-result-object v0

    .line 7324
    invoke-static {v0, p1, p2, p3, p4}, Lyt;->a(Lzl;JJ)Lzl;

    move-result-object v1

    .line 7327
    if-eq v1, v0, :cond_4

    .line 7328
    iget-wide v2, v1, Lzl;->a:J

    iget-wide v4, v0, Lzl;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 7329
    const-string v2, "first_peak_scroll_time"

    iget-wide v3, v1, Lzl;->a:J

    invoke-virtual {p0, v2, v3, v4}, Lyt;->h(Ljava/lang/String;J)V

    .line 7333
    :cond_1
    iget-wide v2, v1, Lzl;->b:J

    iget-wide v4, v0, Lzl;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 7335
    const-string v2, "first_peak_scroll_to_conversation_timestamp"

    iget-wide v3, v1, Lzl;->b:J

    invoke-virtual {p0, v2, v3, v4}, Lyt;->h(Ljava/lang/String;J)V

    .line 7340
    :cond_2
    iget-wide v2, v1, Lzl;->c:J

    iget-wide v4, v0, Lzl;->c:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    .line 7342
    const-string v2, "second_peak_scroll_time"

    iget-wide v3, v1, Lzl;->c:J

    invoke-virtual {p0, v2, v3, v4}, Lyt;->h(Ljava/lang/String;J)V

    .line 7346
    :cond_3
    iget-wide v2, v1, Lzl;->d:J

    iget-wide v4, v0, Lzl;->d:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_4

    .line 7348
    const-string v0, "second_peak_scroll_to_conversation_timestamp"

    iget-wide v1, v1, Lzl;->d:J

    invoke-virtual {p0, v0, v1, v2}, Lyt;->h(Ljava/lang/String;J)V

    .line 7354
    :cond_4
    invoke-virtual {p0}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7356
    invoke-virtual {p0}, Lyt;->c()V

    .line 7357
    return-void

    .line 7356
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lyt;->c()V

    throw v0
.end method

.method public a(Lbcj;)V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x1

    .line 5275
    const-string v0, "Babel_db"

    invoke-static {v0, v7}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5276
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateParticipantData  for participantId: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lbcj;->a:Lbdk;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 5279
    :cond_0
    iget-object v0, p1, Lbcj;->a:Lbdk;

    iget-object v1, v0, Lbdk;->a:Ljava/lang/String;

    iget-object v0, p1, Lbcj;->a:Lbdk;

    iget-object v2, v0, Lbdk;->b:Ljava/lang/String;

    iget-object v3, p1, Lbcj;->b:Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p1, Lbcj;->c:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    .line 5281
    if-ltz v0, :cond_2

    .line 5282
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 5283
    const-string v2, "fallback_name"

    iget-object v3, p1, Lbcj;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5284
    iget-object v2, p1, Lbcj;->b:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 5285
    const-string v2, "participant_type"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5286
    const-string v2, "phone_id"

    iget-object v3, p1, Lbcj;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5289
    :cond_1
    iget-object v2, p0, Lyt;->b:Lzr;

    const-string v3, "participants"

    const-string v4, "_id=?"

    new-array v5, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    .line 5291
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    .line 5289
    invoke-virtual {v2, v3, v1, v4, v5}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 5296
    :goto_0
    return-void

    .line 5293
    :cond_2
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateParticipantData did not find db row for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lbcj;->a:Lbdk;

    .line 5294
    invoke-virtual {v2}, Lbdk;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and fallbackName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lbcj;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 5293
    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lbhw;)V
    .locals 12

    .prologue
    const/4 v9, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 3406
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 3407
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "finalizeMessageFromResponse, conversationId "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 3408
    invoke-virtual {p1}, Lbhw;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 3407
    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3410
    :cond_0
    invoke-virtual {p1}, Lbhw;->f()Ljava/lang/String;

    move-result-object v10

    .line 3411
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 3412
    const-string v0, "message_id"

    invoke-virtual {p1}, Lbhw;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3413
    const-string v0, "status"

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3414
    const-string v0, "type"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3415
    const-string v0, "timestamp"

    iget-wide v1, p1, Lbhw;->d:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 3416
    iget-object v0, p1, Lbhw;->c:Lbht;

    iget v0, v0, Lbht;->b:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_a

    move v0, v6

    .line 3419
    :goto_0
    if-eqz v0, :cond_1

    .line 3420
    const-string v1, "off_the_record"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v11, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 3422
    :cond_1
    const-string v1, "persisted"

    if-nez v0, :cond_b

    move v0, v6

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v11, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 3423
    invoke-virtual {p1}, Lbhw;->i()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_c

    .line 3424
    const-string v0, "expiration_timestamp"

    .line 3425
    invoke-virtual {p1}, Lbhw;->i()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 3424
    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 3433
    :goto_2
    invoke-virtual {p1}, Lbhw;->j()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_2

    .line 3434
    const-string v0, "image_id"

    invoke-virtual {p1}, Lbhw;->j()[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v7

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3436
    :cond_2
    invoke-virtual {p1}, Lbhw;->k()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_3

    .line 3437
    const-string v0, "album_id"

    invoke-virtual {p1}, Lbhw;->k()[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v7

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3439
    :cond_3
    invoke-virtual {p1}, Lbhw;->m()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_4

    .line 3440
    const-string v0, "stream_id"

    invoke-virtual {p1}, Lbhw;->m()[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v7

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3444
    :cond_4
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    .line 3445
    invoke-virtual {p1}, Lbhw;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    aput-object v10, v4, v6

    .line 3447
    invoke-virtual {p1}, Lbhw;->j()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_f

    .line 3448
    invoke-virtual {p1}, Lbhw;->l()[Ljava/lang/String;

    move-result-object v0

    aget-object v8, v0, v7

    .line 3449
    const-string v0, "remote_url"

    invoke-virtual {v11, v0, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3453
    :try_start_0
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 3454
    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3462
    :cond_5
    :goto_3
    invoke-virtual {p1}, Lbhw;->n()Z

    move-result v0

    if-nez v0, :cond_e

    .line 3472
    :try_start_1
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "messages"

    sget-object v2, Lyt;->s:[Ljava/lang/String;

    const-string v3, "message_id=? AND conversation_id=?"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 3477
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 3478
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 3479
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 3480
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 3481
    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/hangouts/content/EsProvider;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 3484
    invoke-static {}, Lzo;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3489
    :cond_6
    if-eqz v9, :cond_e

    .line 3490
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    move-object v0, v8

    .line 3496
    :goto_4
    sget-boolean v1, Lyt;->a:Z

    if-eqz v1, :cond_7

    .line 3497
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "finalizeMessageFromResponse: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 3498
    invoke-virtual {p1}, Lbhw;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 3497
    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3501
    :cond_7
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "messages"

    const-string v3, "message_id=? AND conversation_id=?"

    invoke-virtual {v1, v2, v11, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 3504
    if-nez v1, :cond_8

    .line 3507
    invoke-virtual {p1}, Lbhw;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v7

    .line 3508
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "messages"

    const-string v3, "message_id=? AND conversation_id=?"

    invoke-virtual {v1, v2, v11, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 3511
    if-nez v1, :cond_8

    .line 3512
    const-string v1, "Babel_db"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to update the given message with client id: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 3513
    invoke-virtual {p1}, Lbhw;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " and message id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 3514
    invoke-virtual {p1}, Lbhw;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 3512
    invoke-static {v1, v2}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 3518
    :cond_8
    if-eqz v0, :cond_9

    .line 3519
    invoke-virtual {v11}, Landroid/content/ContentValues;->clear()V

    .line 3520
    const-string v1, "snippet_image_url"

    invoke-virtual {v11, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3521
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversations"

    const-string v2, "conversation_id=?"

    new-array v3, v6, [Ljava/lang/String;

    aput-object v10, v3, v7

    invoke-virtual {v0, v1, v11, v2, v3}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 3526
    if-eq v0, v6, :cond_9

    .line 3527
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "couldn\'t update remote url for conversation; got count "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " for conversation "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 3531
    :cond_9
    return-void

    :cond_a
    move v0, v7

    .line 3416
    goto/16 :goto_0

    :cond_b
    move v0, v7

    .line 3422
    goto/16 :goto_1

    .line 3431
    :cond_c
    const-string v0, "expiration_timestamp"

    invoke-virtual {v11, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 3456
    :catch_0
    move-exception v0

    .line 3457
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " Writing message info with malformed url: remote="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_3

    .line 3489
    :catchall_0
    move-exception v0

    if-eqz v9, :cond_d

    .line 3490
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_d
    throw v0

    :cond_e
    move-object v0, v8

    goto/16 :goto_4

    :cond_f
    move-object v0, v9

    goto/16 :goto_4
.end method

.method public a(Lbjc;)V
    .locals 4

    .prologue
    .line 2149
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 2150
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "insertNewConversation -- conversationId: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lbjc;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", name: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lbjc;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2154
    :cond_0
    invoke-static {p1}, Lyt;->b(Lbjc;)Landroid/content/ContentValues;

    move-result-object v0

    .line 2155
    invoke-virtual {p1}, Lbjc;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2159
    const-string v1, "sort_timestamp"

    iget-wide v2, p1, Lbjc;->s:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2163
    :cond_1
    const-string v1, "is_pending_leave"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2164
    const-string v1, "conversation_id"

    iget-object v2, p1, Lbjc;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2165
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "conversations"

    invoke-virtual {v1, v2, v0}, Lzr;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 2166
    return-void
.end method

.method public a(Lbsc;Lbdh;)V
    .locals 3

    .prologue
    .line 5763
    iget-object v0, p2, Lbdh;->c:Ljava/lang/String;

    iget-object v1, p2, Lbdh;->e:Ljava/lang/String;

    iget-object v2, p2, Lbdh;->h:Ljava/lang/String;

    invoke-direct {p0, p1, v0, v1, v2}, Lyt;->a(Lbsc;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lzj;

    move-result-object v0

    .line 5765
    iget-object v1, v0, Lzj;->a:Ljava/lang/String;

    iget-object v0, v0, Lzj;->b:Ljava/lang/String;

    invoke-virtual {p2, v1, v0}, Lbdh;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5766
    return-void
.end method

.method public a(Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 2445
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 2446
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setConversationNotificationLevel, conversationId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "notificationLevel="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2449
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2450
    const-string v1, "notification_level"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2451
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "conversations"

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2453
    invoke-static {p0}, Lyp;->d(Lyt;)V

    .line 2454
    return-void
.end method

.method public a(Ljava/lang/String;ILbjc;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1972
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 1973
    const-string v3, "Babel_db"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "updateExistingConversation -- currentId: "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", conversationId: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p3, Lbjc;->c:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", is_new_id: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v0, p3, Lbjc;->c:Ljava/lang/String;

    .line 1975
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", name: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p3, Lbjc;->f:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1973
    invoke-static {v3, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1978
    :cond_0
    invoke-static {p3}, Lyt;->b(Lbjc;)Landroid/content/ContentValues;

    move-result-object v0

    .line 1979
    if-ne p2, v5, :cond_1

    iget v3, p3, Lbjc;->l:I

    if-ne v3, v1, :cond_1

    .line 1984
    const-string v3, "status"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1986
    :cond_1
    iget-object v3, p3, Lbjc;->c:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1989
    const-string v3, "conversation_id"

    iget-object v4, p3, Lbjc;->c:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1993
    invoke-virtual {p0, p1, p1}, Lyt;->h(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    .line 1994
    if-eqz v3, :cond_2

    .line 1995
    invoke-static {v3}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v3

    invoke-virtual {p0, v3, v4}, Lyt;->f(J)V

    .line 2002
    :cond_2
    iget v3, p3, Lbjc;->l:I

    if-eq v3, v1, :cond_3

    .line 2003
    const-string v1, "disposition"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2005
    :cond_3
    iget-object v1, p3, Lbjc;->c:Ljava/lang/String;

    invoke-direct {p0, p1, v1, v0}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 2006
    return-void

    :cond_4
    move v0, v2

    .line 1975
    goto :goto_0
.end method

.method public a(Ljava/lang/String;J)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1108
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 1109
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "insertPlaceholderConversation: conversationId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " seenTimestamp="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1112
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1113
    const-string v1, "conversation_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1114
    const-string v1, "is_pending_leave"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1115
    const-string v1, "metadata_present"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1116
    const-string v1, "chat_watermark"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1117
    const-string v1, "hangout_watermark"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1119
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "conversations"

    invoke-virtual {v1, v2, v0}, Lzr;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 1120
    return-void
.end method

.method public a(Ljava/lang/String;JJ)V
    .locals 10

    .prologue
    .line 4043
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 4044
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setTimestampsForPendingSentMessages, conversationId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", ts="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", orgTs="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4047
    :cond_0
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 4048
    const/4 v6, 0x0

    .line 4060
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "messages"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const-string v3, "conversation_id=? AND (status=2) AND timestamp>? AND timestamp<?"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    .line 4068
    invoke-static {p4, p5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v5

    const/4 v5, 0x2

    .line 4069
    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v5

    const-string v5, "timestamp ASC"

    .line 4060
    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 4072
    :try_start_1
    invoke-virtual {v7}, Landroid/content/ContentValues;->clear()V

    .line 4073
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4074
    const-wide/16 v2, 0x1

    add-long/2addr p2, v2

    .line 4075
    const-string v0, "timestamp"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v7, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 4076
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v2, "messages"

    const-string v3, "_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 4078
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 4076
    invoke-virtual {v0, v2, v7, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 4081
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_1

    .line 4082
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 4081
    :cond_2
    if-eqz v1, :cond_3

    .line 4082
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 4093
    :cond_3
    return-void

    .line 4081
    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_1
.end method

.method public a(Ljava/lang/String;JJJ)V
    .locals 7

    .prologue
    .line 1548
    invoke-virtual {p0, p1}, Lyt;->l(Ljava/lang/String;)J

    move-result-wide v0

    .line 1550
    cmp-long v2, v0, p2

    if-nez v2, :cond_3

    .line 1551
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 1552
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateLatestMessageTimestamp. Local timestamp "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Server timestamp "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1556
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1558
    const-string v1, "latest_message_timestamp"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1559
    const-wide/16 v1, 0x0

    cmp-long v1, p6, v1

    if-lez v1, :cond_2

    .line 1560
    const-string v1, "latest_message_expiration_timestamp"

    .line 1561
    invoke-static {p6, p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 1560
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1566
    :goto_0
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "conversations"

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 1568
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 1566
    invoke-virtual {v1, v2, v0, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1576
    :cond_1
    :goto_1
    return-void

    .line 1563
    :cond_2
    const-string v1, "latest_message_expiration_timestamp"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_0

    .line 1570
    :cond_3
    sget-boolean v2, Lyt;->a:Z

    if-eqz v2, :cond_1

    .line 1571
    const-string v2, "Babel_db"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "updateLatestMessageTimestamp skipped. Local timestamp for message "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ". Latest message timestamp "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(Ljava/lang/String;JLbdk;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 8128
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 8129
    const-string v1, "conversation_type"

    .line 8130
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 8129
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 8131
    const-string v1, "is_pending_leave"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 8132
    const-string v1, "conversation_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 8133
    const-string v1, "has_persistent_events"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 8134
    const-string v1, "status"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 8135
    const-string v1, "view"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 8136
    const-string v1, "is_draft"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 8137
    const-string v1, "has_oldest_message"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 8138
    const-string v1, "call_media_type"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 8139
    const-string v1, "notification_level"

    const/16 v2, 0x1e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 8140
    const-string v1, "disposition"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 8141
    const-string v1, "otr_status"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 8142
    const-string v1, "otr_toggle"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 8143
    const-string v1, "is_temporary"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 8144
    const-string v1, "name"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 8145
    const-string v1, "inviter_gaia_id"

    iget-object v2, p4, Lbdk;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 8146
    const-string v1, "inviter_chat_id"

    iget-object v2, p4, Lbdk;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 8147
    const-string v1, "sort_timestamp"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 8148
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "conversations"

    invoke-virtual {v1, v2, v0}, Lzr;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 8149
    return-void
.end method

.method public a(Ljava/lang/String;Lbcm;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lbcm;",
            "Ljava/util/List",
            "<",
            "Lbcm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 8091
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 8092
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateDeliveryMediums, conversationId "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", default delivery medium="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", deliveryMediumList="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 8095
    invoke-static {p3}, Lbcm;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 8092
    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8098
    :cond_0
    if-nez p2, :cond_1

    .line 8122
    :goto_0
    return-void

    .line 8104
    :cond_1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 8105
    iget-object v1, p0, Lyt;->b:Lzr;

    invoke-virtual {v1}, Lzr;->a()V

    .line 8107
    :try_start_0
    const-string v1, "transport_type"

    iget v2, p2, Lbcm;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 8108
    const-string v1, "default_transport_phone"

    iget-object v2, p2, Lbcm;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 8111
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "conversations"

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 8119
    iget-object v0, p0, Lyt;->b:Lzr;

    invoke-virtual {v0}, Lzr;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8121
    iget-object v0, p0, Lyt;->b:Lzr;

    invoke-virtual {v0}, Lzr;->c()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lyt;->b:Lzr;

    invoke-virtual {v1}, Lzr;->c()V

    throw v0
.end method

.method public a(Ljava/lang/String;Lbdh;)V
    .locals 2

    .prologue
    .line 5521
    const/4 v0, 0x0

    invoke-virtual {p0, p2, v0}, Lyt;->a(Lbdh;Z)Z

    .line 5522
    iget-object v0, p2, Lbdh;->b:Lbdk;

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lyt;->b(Ljava/lang/String;Lbdk;Z)V

    .line 5524
    return-void
.end method

.method public a(Ljava/lang/String;Lbdk;Z)V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 5547
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 5548
    const-string v4, "active"

    if-eqz p3, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5550
    if-eqz p2, :cond_2

    iget-object v0, p2, Lbdk;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 5551
    const-string v0, "(SELECT _id FROM conversation_participants_view WHERE gaia_id=? AND conversation_id=?)"

    .line 5557
    iget-object v4, p0, Lyt;->b:Lzr;

    const-string v5, "conversation_participants"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "participant_row_id="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, " AND conversation_id=?"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v6, v9, [Ljava/lang/String;

    iget-object v7, p2, Lbdk;->a:Ljava/lang/String;

    aput-object v7, v6, v2

    aput-object p1, v6, v1

    aput-object p1, v6, v8

    invoke-virtual {v4, v5, v3, v0, v6}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 5579
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 5548
    goto :goto_0

    .line 5564
    :cond_2
    if-eqz p2, :cond_0

    iget-object v0, p2, Lbdk;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5565
    const-string v0, "(SELECT _id FROM conversation_participants_view WHERE chat_id=? AND conversation_id=?)"

    .line 5571
    iget-object v4, p0, Lyt;->b:Lzr;

    const-string v5, "conversation_participants"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "participant_row_id="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, " AND conversation_id=?"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v6, v9, [Ljava/lang/String;

    iget-object v7, p2, Lbdk;->b:Ljava/lang/String;

    aput-object v7, v6, v2

    aput-object p1, v6, v1

    aput-object p1, v6, v8

    invoke-virtual {v4, v5, v3, v0, v6}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Lbnl;)V
    .locals 9

    .prologue
    .line 2069
    invoke-virtual {p0, p1}, Lyt;->e(Ljava/lang/String;)Lyv;

    move-result-object v0

    .line 2070
    if-nez v0, :cond_0

    .line 2121
    :goto_0
    return-void

    .line 2079
    :cond_0
    invoke-static {}, Lyt;->h()Ljava/lang/String;

    move-result-object v1

    .line 2081
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2082
    const-string v3, "disposition"

    const/4 v4, 0x3

    .line 2083
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 2082
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2085
    invoke-virtual {p0}, Lyt;->a()V

    .line 2090
    :try_start_0
    invoke-direct {p0, p1, v1, v2}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 2095
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2096
    const-string v3, "status"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2097
    iget-object v3, p0, Lyt;->b:Lzr;

    const-string v4, "messages"

    const-string v5, "conversation_id=? AND status=?"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    const/4 v7, 0x1

    const/4 v8, 0x2

    .line 2101
    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    .line 2097
    invoke-virtual {v3, v4, v2, v5, v6}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2104
    invoke-virtual {p0}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2106
    invoke-virtual {p0}, Lyt;->c()V

    .line 2109
    invoke-static {p0}, Lyp;->d(Lyt;)V

    .line 2110
    iget-object v2, p0, Lyt;->d:Lyj;

    invoke-static {v2, p1, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Lyj;Ljava/lang/String;Ljava/lang/String;)V

    .line 2116
    new-instance v2, Lbee;

    iget v3, v0, Lyv;->b:I

    iget-object v4, v0, Lyv;->d:Ljava/lang/String;

    iget-object v0, v0, Lyv;->h:Ljava/util/List;

    .line 2120
    invoke-direct {p0, v0}, Lyt;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v2, v1, v3, v4, v0}, Lbee;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/util/List;)V

    .line 2116
    invoke-virtual {p2, v2}, Lbnl;->a(Lbea;)V

    goto :goto_0

    .line 2106
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lyt;->c()V

    throw v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2846
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 2847
    const-string v0, "Babel_db"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "updateConversationHasPersistentEvents, conversationId "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", hasPersistentEvents="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2852
    :cond_0
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 2853
    const-string v4, "has_persistent_events"

    if-nez p2, :cond_1

    const/4 v0, -0x1

    .line 2854
    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 2853
    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2857
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v4, "conversations"

    const-string v5, "conversation_id=?"

    new-array v1, v1, [Ljava/lang/String;

    aput-object p1, v1, v2

    invoke-virtual {v0, v4, v3, v5, v1}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2860
    return-void

    .line 2855
    :cond_1
    invoke-static {p2, v2}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Long;)V
    .locals 6

    .prologue
    .line 4366
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 4367
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "deleteMessageFromConversationBefore, conversationId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", ts="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4370
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "conversation_id"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 4371
    const-string v1, "= ? AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4372
    const-string v1, "timestamp"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4373
    const-string v1, "< ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4375
    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4377
    const-string v1, "persisted"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4378
    const-string v1, " = 1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4380
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "messages"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    .line 4382
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 4380
    invoke-virtual {v1, v2, v0, v3}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4384
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x1

    const/4 v6, 0x3

    const/4 v5, 0x0

    .line 4785
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setMessageStatusSent: conversationId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " messageId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 4787
    if-ne p3, v6, :cond_4

    .line 4788
    const-string v1, "Babel_db"

    invoke-static {v1, v0}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 4792
    :cond_0
    :goto_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 4793
    const-string v1, "status"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4794
    if-ne p3, v6, :cond_5

    .line 4795
    const-string v1, "sending_error"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4800
    const-string v1, "notified_for_failure"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4801
    iget-object v1, p0, Lyt;->d:Lyj;

    const-wide/16 v2, 0x0

    invoke-static {v1, v2, v3, v8, v5}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;JIZ)V

    .line 4806
    :goto_1
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "messages"

    const-string v3, "conversation_id=? AND message_id=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v5

    aput-object p2, v4, v7

    invoke-virtual {v1, v2, v0, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 4811
    if-eq v0, v7, :cond_1

    .line 4812
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "!!!!! failed to set message status to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; updateCount == "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 4816
    :cond_1
    invoke-static {p0, p1}, Lyp;->d(Lyt;Ljava/lang/String;)V

    .line 4821
    if-eq p3, v6, :cond_2

    if-ne p3, v8, :cond_3

    .line 4823
    :cond_2
    invoke-static {p0}, Lyp;->d(Lyt;)V

    .line 4824
    invoke-static {p0, p1}, Lyp;->d(Lyt;Ljava/lang/String;)V

    .line 4826
    :cond_3
    return-void

    .line 4789
    :cond_4
    sget-boolean v1, Lyt;->a:Z

    if-eqz v1, :cond_0

    .line 4790
    const-string v1, "Babel_db"

    invoke-static {v1, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4804
    :cond_5
    const-string v1, "sending_error"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 6

    .prologue
    .line 8266
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 8267
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateStreamUrlForPhotoId, photoId = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", streamUrl ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8271
    :cond_0
    iget-object v0, p0, Lyt;->b:Lzr;

    invoke-virtual {v0}, Lzr;->a()V

    .line 8273
    :try_start_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 8274
    const-string v1, "stream_url"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 8276
    const-string v1, "stream_expiration"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 8277
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "messages"

    const-string v3, "image_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 8283
    iget-object v0, p0, Lyt;->b:Lzr;

    invoke-virtual {v0}, Lzr;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8285
    iget-object v0, p0, Lyt;->b:Lzr;

    invoke-virtual {v0}, Lzr;->c()V

    .line 8286
    return-void

    .line 8285
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lyt;->b:Lzr;

    invoke-virtual {v1}, Lzr;->c()V

    throw v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;J)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 3535
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 3536
    const-string v1, "status"

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3537
    const-string v1, "type"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3539
    if-eqz p3, :cond_0

    .line 3540
    const-string v1, "external_ids"

    new-array v2, v4, [Ljava/lang/String;

    .line 3541
    invoke-virtual {p3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v2}, Lf;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3540
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3543
    :cond_0
    const-wide/16 v1, 0x0

    cmp-long v1, p4, v1

    if-lez v1, :cond_1

    .line 3544
    const-string v1, "sms_message_size"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 3546
    :cond_1
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    aput-object p1, v1, v4

    .line 3547
    iget-object v2, p0, Lyt;->b:Lzr;

    const-string v3, "messages"

    const-string v4, "message_id=? AND conversation_id=?"

    invoke-virtual {v2, v3, v0, v4, v1}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3548
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 5264
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 5265
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5266
    const-string v1, "chat_id"

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5267
    const-string v1, "name"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5268
    const-string v1, "profile_photo_url"

    invoke-virtual {v0, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5270
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "dismissed_contacts"

    invoke-virtual {v1, v2, v0}, Lzr;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 5271
    invoke-static {}, Lzo;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->j:Landroid/net/Uri;

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 5272
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1015
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 1016
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setDraft conversationId: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " draft: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " subject: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " attachmentUrl: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " photoRotation: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " picasaId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " contentType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1021
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1022
    const-string v1, "draft"

    if-eqz p2, :cond_1

    :goto_0
    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1023
    const-string v1, "draft_subject"

    if-eqz p3, :cond_2

    :goto_1
    invoke-virtual {v0, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1024
    const-string v1, "draft_attachment_url"

    if-eqz p4, :cond_3

    :goto_2
    invoke-virtual {v0, v1, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1025
    const-string v1, "draft_photo_rotation"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1026
    const-string v1, "draft_picasa_id"

    if-eqz p6, :cond_4

    :goto_3
    invoke-virtual {v0, v1, p6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1027
    const-string v1, "draft_content_type"

    if-eqz p7, :cond_5

    :goto_4
    invoke-virtual {v0, v1, p7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1028
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "conversations"

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1031
    iget-object v0, p0, Lyt;->d:Lyj;

    invoke-static {v0}, Lyp;->a(Lyj;)V

    .line 1032
    return-void

    .line 1022
    :cond_1
    const-string p2, ""

    goto :goto_0

    .line 1023
    :cond_2
    const-string p3, ""

    goto :goto_1

    .line 1024
    :cond_3
    const-string p4, ""

    goto :goto_2

    .line 1026
    :cond_4
    const-string p6, ""

    goto :goto_3

    .line 1027
    :cond_5
    const-string p7, ""

    goto :goto_4
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 5180
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5181
    const-string v1, "gaia_id=?"

    .line 5182
    new-array v0, v2, [Ljava/lang/String;

    aput-object p1, v0, v3

    .line 5191
    :goto_0
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 5192
    const-string v5, "blocked"

    if-eqz p3, :cond_2

    :goto_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v4, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5193
    iget-object v2, p0, Lyt;->b:Lzr;

    const-string v3, "participants"

    invoke-virtual {v2, v3, v4, v1, v0}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 5194
    invoke-static {}, Lzo;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->i:Landroid/net/Uri;

    invoke-virtual {v0, v1, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 5195
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Lyj;)Landroid/net/Uri;

    move-result-object v0

    .line 5196
    invoke-static {}, Lzo;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, v0, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 5197
    :goto_2
    return-void

    .line 5183
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 5184
    const-string v1, "chat_id=?"

    .line 5185
    new-array v0, v2, [Ljava/lang/String;

    aput-object p2, v0, v3

    goto :goto_0

    .line 5187
    :cond_1
    const-string v0, "Babel_db"

    const-string v1, "setUserBlocked without a valid gaiaId or chatId"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    move v2, v3

    .line 5192
    goto :goto_1
.end method

.method public a(Ljava/lang/String;Lxo;)V
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 5115
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 5116
    const-string v0, "Babel_db"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "insertCircleParticipant ConversationId: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", circle id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 5117
    invoke-virtual {p2}, Lxo;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 5116
    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5120
    :cond_0
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 5121
    const-string v0, "conversation_id"

    invoke-virtual {v7, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5123
    const-string v0, "participant_type"

    const/4 v2, 0x2

    .line 5124
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 5123
    invoke-virtual {v7, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5126
    const-string v0, "active"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v7, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5129
    invoke-virtual {p2}, Lxo;->b()Ljava/lang/String;

    move-result-object v4

    .line 5130
    invoke-virtual {p2}, Lxo;->d()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    .line 5129
    invoke-direct/range {v0 .. v6}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    .line 5132
    const-string v1, "participant_row_id"

    .line 5133
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 5132
    invoke-virtual {v7, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5136
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversation_participants"

    invoke-virtual {v0, v1, v7}, Lzr;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 5138
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 8067
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 8068
    const-string v0, "Babel_db"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "updateConversationIsTemporary, conversationId "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", isTemporary="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8073
    :cond_0
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 8074
    const-string v4, "is_temporary"

    if-eqz p2, :cond_1

    move v0, v1

    .line 8075
    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 8074
    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 8076
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v4, "conversations"

    const-string v5, "conversation_id=?"

    new-array v1, v1, [Ljava/lang/String;

    aput-object p1, v1, v2

    invoke-virtual {v0, v4, v3, v5, v1}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 8082
    return-void

    :cond_1
    move v0, v2

    .line 8074
    goto :goto_0
.end method

.method public a(Ljava/lang/String;[BJ)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1143
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 1144
    const-string v0, "Babel_db"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "setContinuationToken: conversationId="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " token "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1145
    invoke-static {p2}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " eventTimestamp "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1144
    invoke-static {v0, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1147
    :cond_0
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 1148
    if-nez p2, :cond_1

    .line 1149
    const-string v0, "continuation_token"

    invoke-virtual {v3, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1153
    :goto_0
    const-string v0, "continuation_event_timestamp"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1154
    const-string v4, "has_oldest_message"

    if-nez p2, :cond_2

    const-wide/16 v5, 0x0

    cmp-long v0, p3, v5

    if-nez v0, :cond_2

    move v0, v1

    .line 1155
    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1154
    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1156
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v4, "conversations"

    const-string v5, "conversation_id=?"

    new-array v1, v1, [Ljava/lang/String;

    aput-object p1, v1, v2

    invoke-virtual {v0, v4, v3, v5, v1}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1161
    invoke-static {p0}, Lyp;->e(Lyt;)V

    .line 1162
    return-void

    .line 1151
    :cond_1
    const-string v0, "continuation_token"

    invoke-virtual {v3, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 1154
    goto :goto_1
.end method

.method public a(Ljava/util/HashSet;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Lbdk;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 6698
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 6700
    invoke-virtual {p1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdk;

    .line 6701
    iget-object v1, v0, Lbdk;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 6704
    const-string v3, "gaia_id=?"

    .line 6705
    new-array v4, v10, [Ljava/lang/String;

    iget-object v0, v0, Lbdk;->a:Ljava/lang/String;

    aput-object v0, v4, v9

    .line 6715
    :goto_1
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversation_participants_view"

    sget-object v2, Lyt;->o:[Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 6717
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 6719
    :cond_1
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 6720
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_1

    .line 6723
    :cond_2
    if-eqz v1, :cond_0

    .line 6724
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 6706
    :cond_3
    iget-object v1, v0, Lbdk;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 6707
    const-string v3, "chat_id=?"

    .line 6708
    new-array v4, v10, [Ljava/lang/String;

    iget-object v0, v0, Lbdk;->b:Ljava/lang/String;

    aput-object v0, v4, v9

    goto :goto_1

    .line 6723
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_2
    if-eqz v1, :cond_4

    .line 6724
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 6729
    :cond_5
    invoke-virtual {v7}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 6732
    invoke-static {p0, v0}, Lyp;->b(Lyt;Ljava/lang/String;)V

    .line 6734
    iget-object v0, p0, Lyt;->d:Lyj;

    invoke-static {v0}, Lyp;->b(Lyj;)V

    goto :goto_3

    .line 6736
    :cond_6
    return-void

    .line 6723
    :catchall_1
    move-exception v0

    goto :goto_2
.end method

.method public a(Ljava/util/List;I)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;I)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 6773
    const-string v1, "Babel_db"

    const-string v2, "updateSuggestedEntitiesList"

    invoke-static {v1, v2}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6776
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "suggested_contacts"

    const-string v3, "suggestion_type=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    .line 6778
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    .line 6776
    invoke-virtual {v1, v2, v3, v4}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 6780
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 6782
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    .line 6784
    iget-object v4, v0, Lbdh;->f:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, v0, Lbdh;->e:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 6785
    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    .line 6789
    const-string v4, "chat_id"

    iget-object v5, v0, Lbdh;->b:Lbdk;

    iget-object v5, v5, Lbdk;->b:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6790
    const-string v4, "gaia_id"

    iget-object v5, v0, Lbdh;->b:Lbdk;

    iget-object v5, v5, Lbdk;->a:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6792
    const-string v4, "name"

    iget-object v5, v0, Lbdh;->e:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6793
    const-string v4, "first_name"

    iget-object v5, v0, Lbdh;->f:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6794
    const-string v4, "profile_photo_url"

    iget-object v5, v0, Lbdh;->h:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6795
    const-string v4, "packed_circle_ids"

    iget-object v0, v0, Lbdh;->p:Ljava/lang/String;

    invoke-virtual {v2, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6796
    const-string v0, "sequence"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 6797
    const-string v0, "suggestion_type"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 6798
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v4, "suggested_contacts"

    invoke-virtual {v0, v4, v2}, Lzr;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 6799
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 6800
    goto :goto_0

    .line 6801
    :cond_1
    invoke-static {}, Lzo;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->g:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 6802
    return-void
.end method

.method public a(Ljava/util/List;Ljava/lang/String;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbdk;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 6072
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 6073
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 6075
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversation_participants_view"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "gaia_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "chat_id"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "active"

    aput-object v4, v2, v3

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 6084
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 6086
    :cond_0
    new-instance v2, Lbdk;

    const/4 v0, 0x0

    .line 6087
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x1

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lbdk;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 6088
    const/4 v0, 0x2

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v7, :cond_4

    move v0, v7

    .line 6089
    :goto_0
    if-eqz v0, :cond_5

    invoke-interface {p1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 6090
    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6094
    :cond_1
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 6097
    :cond_2
    if-eqz v1, :cond_3

    .line 6098
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 6102
    :cond_3
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdk;

    .line 6103
    invoke-virtual {p0, p2, v0, v8}, Lyt;->a(Ljava/lang/String;Lbdk;Z)V

    goto :goto_2

    :cond_4
    move v0, v8

    .line 6088
    goto :goto_0

    .line 6091
    :cond_5
    if-nez v0, :cond_1

    :try_start_2
    invoke-interface {p1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6092
    invoke-interface {v10, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 6097
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v1, :cond_6

    .line 6098
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0

    .line 6106
    :cond_7
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdk;

    .line 6107
    invoke-virtual {p0, p2, v0, v7}, Lyt;->a(Ljava/lang/String;Lbdk;Z)V

    goto :goto_4

    .line 6109
    :cond_8
    return-void

    .line 6097
    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_3
.end method

.method public a(Lyj;Ljava/lang/String;Ljava/lang/String;JJI)V
    .locals 16

    .prologue
    .line 9058
    new-instance v15, Lbnl;

    invoke-direct {v15}, Lbnl;-><init>()V

    .line 9059
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 9060
    invoke-static/range {p2 .. p2}, Lbdk;->b(Ljava/lang/String;)Lbdk;

    move-result-object v6

    .line 9061
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lyt;->K(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbdh;

    .line 9062
    iget-object v5, v2, Lbdh;->b:Lbdk;

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 9063
    iget-object v5, v2, Lbdh;->b:Lbdk;

    iget-object v5, v5, Lbdk;->a:Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 9064
    iget-object v6, v2, Lbdh;->b:Lbdk;

    goto :goto_0

    .line 9068
    :cond_1
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lyt;->A(Ljava/lang/String;)I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_2

    sget-object v2, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x1

    .line 9070
    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide v4

    add-long v10, p6, v4

    .line 9072
    :goto_1
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1, v3}, Lyt;->a(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v14

    .line 9074
    invoke-static/range {p4 .. p5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x4

    const/4 v7, 0x7

    const/4 v13, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p3

    move-wide/from16 v8, p6

    move/from16 v12, p8

    .line 9073
    invoke-virtual/range {v2 .. v14}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;ILbdk;IJJILjava/lang/String;Ljava/lang/String;)J

    .line 9080
    const/16 v2, 0x14

    move/from16 v0, p8

    if-ne v0, v2, :cond_3

    .line 9081
    move-wide/from16 v0, p6

    invoke-virtual {v15, v0, v1}, Lbnl;->a(J)V

    .line 9087
    :goto_2
    invoke-virtual {v15}, Lbnl;->e()I

    move-result v2

    .line 9086
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lbne;->a(Lyj;I)V

    .line 9089
    return-void

    .line 9070
    :cond_2
    const-wide/16 v10, 0x0

    goto :goto_1

    .line 9083
    :cond_3
    invoke-virtual {v15}, Lbnl;->d()V

    goto :goto_2
.end method

.method public a([Ljava/lang/String;)V
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 8684
    invoke-virtual {p0}, Lyt;->e()Lzr;

    move-result-object v1

    .line 8686
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 8688
    invoke-virtual {v1}, Lzr;->a()V

    .line 8690
    :try_start_0
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 8691
    array-length v4, p1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, p1, v0

    .line 8692
    invoke-virtual {v3}, Landroid/content/ContentValues;->clear()V

    .line 8694
    invoke-static {v5}, Lyt;->aq(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 8695
    const-string v7, "merge_key"

    invoke-virtual {v3, v7, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 8697
    const-string v7, "merge_keys"

    const-string v8, "conversation_id=?"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object v5, v9, v10

    invoke-virtual {v1, v7, v3, v8, v9}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 8702
    new-instance v7, Landroid/util/Pair;

    invoke-direct {v7, v5, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 8691
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 8704
    :cond_0
    invoke-virtual {v1}, Lzr;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8706
    invoke-virtual {v1}, Lzr;->c()V

    .line 8708
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/util/Pair;

    .line 8710
    invoke-virtual {p0}, Lyt;->f()Lyj;

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    .line 8709
    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 8706
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lzr;->c()V

    throw v0

    .line 8714
    :cond_1
    iget-object v0, p0, Lyt;->d:Lyj;

    invoke-static {v0}, Lyp;->a(Lyj;)V

    .line 8715
    return-void
.end method

.method public a(JI)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 7574
    sget-boolean v2, Lyt;->a:Z

    if-eqz v2, :cond_0

    .line 7575
    const-string v2, "Babel_db"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "deleteOldConversations: account="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lyt;->d:Lyj;

    invoke-virtual {v4}, Lyj;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " cutOffTimestamp="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7579
    :cond_0
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    .line 7580
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    .line 7581
    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 7584
    iget-object v3, p0, Lyt;->b:Lzr;

    const-string v4, "messages"

    sget-object v5, Lyt;->A:Ljava/lang/String;

    invoke-virtual {v3, v4, v5, v2}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    .line 7586
    if-lez v3, :cond_1

    .line 7587
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 7588
    const-string v5, "continuation_event_timestamp"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 7589
    const-string v5, "continuation_token"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 7590
    const-string v5, "has_oldest_message"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 7591
    iget-object v5, p0, Lyt;->b:Lzr;

    const-string v6, "conversations"

    const-string v7, "sort_timestamp<? AND status=? AND transport_type!=3"

    invoke-virtual {v5, v6, v4, v7, v2}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 7596
    invoke-static {p0}, Lyp;->d(Lyt;)V

    .line 7599
    :cond_1
    if-lez v3, :cond_2

    sget-boolean v2, Lyt;->a:Z

    if-eqz v2, :cond_2

    .line 7600
    const-string v2, "Babel_db"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "deleteOldConversations:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " cutOffTimestamp:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " conversationStatus:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7605
    :cond_2
    sget-boolean v2, Lyt;->a:Z

    if-eqz v2, :cond_3

    .line 7606
    const-string v2, "Babel_db"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Deleted "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " old conversations."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7609
    :cond_3
    if-lez v3, :cond_4

    :goto_0
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public a(Lbdh;Z)Z
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x3

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 5309
    iget-object v0, p1, Lbdh;->b:Lbdk;

    if-nez v0, :cond_1

    .line 5310
    const-string v0, "Babel_db"

    const-string v1, "null participantId in insertOrUpdateParticipant"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 5416
    :cond_0
    :goto_0
    return v6

    .line 5314
    :cond_1
    invoke-static {}, Lbsc;->a()Lbsc;

    move-result-object v0

    .line 5316
    :try_start_0
    invoke-virtual {p0, v0, p1}, Lyt;->a(Lbsc;Lbdh;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5318
    invoke-virtual {v0}, Lbsc;->b()V

    .line 5321
    iget-object v0, p1, Lbdh;->b:Lbdk;

    iget-object v1, v0, Lbdk;->a:Ljava/lang/String;

    .line 5322
    iget-object v0, p1, Lbdh;->b:Lbdk;

    iget-object v2, v0, Lbdk;->b:Ljava/lang/String;

    .line 5323
    invoke-virtual {p1}, Lbdh;->c()Ljava/lang/String;

    move-result-object v3

    .line 5326
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p1, Lbdh;->a:I

    if-eq v0, v5, :cond_2

    .line 5328
    const-string v0, "Babel_db"

    const-string v1, "no gaiaId/chatId/phoneNumber in insertOrUpdateParticipant"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 5318
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Lbsc;->b()V

    throw v1

    .line 5331
    :cond_2
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 5332
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 5333
    const-string v0, "gaia_id"

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5335
    :cond_3
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 5336
    const-string v0, "chat_id"

    invoke-virtual {v8, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5346
    :cond_4
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 5347
    const-string v0, "phone_id"

    invoke-virtual {v8, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5349
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget v5, p1, Lbdh;->a:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v0, v5}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 5351
    :cond_5
    const-string v0, "participant_type"

    iget v5, p1, Lbdh;->a:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v8, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5353
    iget v0, p1, Lbdh;->a:I

    if-nez v0, :cond_6

    .line 5355
    const-string v0, "babel_enable_selective_participant_entity_checking"

    invoke-static {v0, v7}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 5358
    const-string v0, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v9, "Encountered participant with participantType PARTICIPANT_TYPE_UNKNOWN, ParticipantEntity: "

    invoke-direct {v5, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 5360
    invoke-virtual {p1}, Lbdh;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 5358
    invoke-static {v0, v5}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 5362
    const-string v0, "PARTICIPANT_TYPE_UNKNOWN found"

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    .line 5366
    :cond_6
    iget-object v0, p1, Lbdh;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 5367
    const-string v0, "first_name"

    iget-object v5, p1, Lbdh;->f:Ljava/lang/String;

    invoke-virtual {v8, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5370
    :cond_7
    iget-object v0, p1, Lbdh;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 5371
    const-string v0, "full_name"

    iget-object v5, p1, Lbdh;->e:Ljava/lang/String;

    invoke-virtual {v8, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5374
    :cond_8
    iget-object v0, p1, Lbdh;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 5375
    const-string v0, "fallback_name"

    iget-object v5, p1, Lbdh;->g:Ljava/lang/String;

    invoke-virtual {v8, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5378
    :cond_9
    iget-object v0, p1, Lbdh;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 5379
    const-string v0, "profile_photo_url"

    iget-object v5, p1, Lbdh;->h:Ljava/lang/String;

    invoke-virtual {v8, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5385
    :cond_a
    iget-object v0, p1, Lbdh;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    .line 5386
    const-string v5, "blocked"

    iget-object v0, p1, Lbdh;->i:Ljava/lang/Boolean;

    .line 5387
    invoke-static {v0, v6}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    if-eqz v0, :cond_e

    move v0, v7

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 5386
    invoke-virtual {v8, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5390
    :cond_b
    if-eqz p2, :cond_c

    .line 5391
    const-string v0, "batch_gebi_tag"

    invoke-virtual {v8, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    :cond_c
    move-object v0, p0

    move-object v5, v4

    .line 5395
    invoke-direct/range {v0 .. v6}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    .line 5397
    if-gez v0, :cond_f

    .line 5398
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "participants"

    invoke-virtual {v0, v1, v8}, Lzr;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    long-to-int v0, v0

    .line 5399
    if-gez v0, :cond_d

    .line 5400
    const-string v1, "Babel_db"

    const-string v2, "EsConversationsHelper.insertOrUpdateParticipant: insert failed"

    invoke-static {v1, v2}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 5412
    :cond_d
    :goto_2
    if-ltz v0, :cond_0

    .line 5413
    iget-object v1, p0, Lyt;->d:Lyj;

    invoke-static {v1}, Laal;->a(Lyj;)Laal;

    move-result-object v1

    .line 5414
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 5413
    invoke-virtual {v1, v0, p1, p2}, Laal;->a(Ljava/lang/String;Lbdh;Z)Z

    move-result v6

    goto/16 :goto_0

    :cond_e
    move v0, v6

    .line 5387
    goto :goto_1

    .line 5405
    :cond_f
    const-string v1, "phone_id"

    invoke-virtual {v8, v1}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 5406
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "participants"

    const-string v3, "_id=?"

    new-array v4, v7, [Ljava/lang/String;

    .line 5407
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    .line 5406
    invoke-virtual {v1, v2, v8, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_d

    .line 5408
    const-string v1, "Babel_db"

    const-string v2, "EsConversationsHelper.insertOrUpdateParticipant: update failed"

    invoke-static {v1, v2}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public a(Lbdk;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 5537
    invoke-direct {p0, p1, p2}, Lyt;->b(Lbdk;Ljava/lang/String;)Z

    move-result v0

    .line 5538
    const/4 v1, 0x0

    invoke-direct {p0, p3, p1, v1}, Lyt;->b(Ljava/lang/String;Lbdk;Z)V

    .line 5539
    return v0
.end method

.method public a(Ljava/lang/String;JJILbdk;JILjava/lang/String;Ljava/lang/String;)Z
    .locals 17

    .prologue
    .line 1679
    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v15, -0x1

    const/16 v16, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide/from16 v2, p2

    move-wide/from16 v4, p4

    move/from16 v6, p6

    move-object/from16 v7, p7

    move-wide/from16 v10, p8

    move/from16 v12, p10

    move-object/from16 v13, p11

    move-object/from16 v14, p12

    invoke-direct/range {v0 .. v16}, Lyt;->a(Ljava/lang/String;JJILbdk;Ljava/lang/String;Ljava/lang/String;JILjava/lang/String;Ljava/lang/String;II)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;JJILjava/lang/String;Lbdk;Ljava/lang/String;Ljava/lang/String;JII)Z
    .locals 18

    .prologue
    .line 1657
    const/4 v1, 0x6

    move/from16 v0, p6

    if-ne v0, v1, :cond_5

    .line 1658
    invoke-static/range {p7 .. p7}, Lf;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "hangouts/location"

    .line 1659
    move-object/from16 v0, p7

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1660
    :cond_0
    const/4 v7, 0x3

    .line 1670
    :goto_0
    const/4 v13, -0x1

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-wide/from16 v3, p2

    move-wide/from16 v5, p4

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-wide/from16 v11, p11

    move/from16 v16, p13

    move/from16 v17, p14

    invoke-direct/range {v1 .. v17}, Lyt;->a(Ljava/lang/String;JJILbdk;Ljava/lang/String;Ljava/lang/String;JILjava/lang/String;Ljava/lang/String;II)Z

    move-result v1

    return v1

    .line 1661
    :cond_1
    invoke-static/range {p7 .. p7}, Lf;->d(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "application/vnd.wap.multipart.mixed"

    .line 1662
    move-object/from16 v0, p7

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1663
    :cond_2
    const/16 v7, 0x9

    goto :goto_0

    .line 1664
    :cond_3
    const-string v1, "hangouts/gv_voicemail"

    move-object/from16 v0, p7

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1665
    const/16 v7, 0xc

    goto :goto_0

    .line 1667
    :cond_4
    const/4 v7, 0x2

    goto :goto_0

    :cond_5
    move/from16 v7, p6

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 19

    .prologue
    .line 1605
    const/4 v7, 0x0

    .line 1607
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lyt;->b:Lzr;

    const-string v2, "messages"

    sget-object v3, Lzm;->a:[Ljava/lang/String;

    const-string v4, "conversation_id=? AND message_id=?"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x1

    aput-object p2, v5, v6

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v18

    .line 1611
    :try_start_1
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1612
    const/4 v1, 0x3

    move-object/from16 v0, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 1613
    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    .line 1614
    const/4 v1, 0x1

    .line 1615
    move-object/from16 v0, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    const/4 v1, 0x2

    .line 1616
    move-object/from16 v0, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    const/4 v7, 0x5

    const/4 v1, 0x4

    .line 1618
    move-object/from16 v0, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lbdk;->c(Ljava/lang/String;)Lbdk;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v1, 0x0

    .line 1622
    move-object/from16 v0, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    const/4 v1, 0x6

    .line 1623
    move-object/from16 v0, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    const/4 v1, 0x7

    .line 1624
    move-object/from16 v0, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    const/16 v16, -0x1

    const/16 v17, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    .line 1614
    invoke-direct/range {v1 .. v17}, Lyt;->a(Ljava/lang/String;JJILbdk;Ljava/lang/String;Ljava/lang/String;JILjava/lang/String;Ljava/lang/String;II)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    .line 1632
    if-eqz v18, :cond_0

    .line 1633
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 1636
    :cond_0
    :goto_0
    return v1

    .line 1632
    :cond_1
    if-eqz v18, :cond_2

    .line 1633
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 1636
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 1632
    :catchall_0
    move-exception v1

    move-object v2, v7

    :goto_1
    if-eqz v2, :cond_3

    .line 1633
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v1

    .line 1632
    :catchall_1
    move-exception v1

    move-object/from16 v2, v18

    goto :goto_1
.end method

.method public aa(Ljava/lang/String;)J
    .locals 6

    .prologue
    .line 8002
    const-string v1, "getSmsThreadId"

    const-string v3, "sms_thread_id"

    const-wide/16 v4, -0x1

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public ab(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 8085
    const-string v2, "getConversationIsTemporary"

    const-string v3, "is_temporary"

    invoke-direct {p0, v2, p1, v3, v1}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public ac(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 8219
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 8220
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getStreamUrlForPhotoId, photoId ="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8225
    :cond_0
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "messages"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "stream_url"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "stream_expiration"

    aput-object v4, v2, v3

    const-string v3, "image_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 8237
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    move-object v0, v6

    .line 8238
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 8239
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 8240
    const/4 v4, 0x1

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 8241
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    cmp-long v6, v2, v4

    if-gez v6, :cond_1

    .line 8244
    invoke-virtual {p0, p1, v0, v4, v5}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 8249
    if-eqz v1, :cond_2

    .line 8250
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 8253
    :cond_2
    :goto_0
    return-object v0

    .line 8249
    :cond_3
    if-eqz v1, :cond_2

    .line 8250
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 8249
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_1
    if-eqz v1, :cond_4

    .line 8250
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 8249
    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public ad(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 8295
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 8296
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getConversationIdForPhotoId, photoId ="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8301
    :cond_0
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "messages"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "conversation_id"

    aput-object v4, v2, v3

    const-string v3, "image_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 8312
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 8313
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v6

    .line 8316
    :cond_1
    if-eqz v1, :cond_2

    .line 8317
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 8320
    :cond_2
    return-object v6

    .line 8316
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_0
    if-eqz v1, :cond_3

    .line 8317
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 8316
    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public ae(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 8386
    const-string v0, "Babel_db"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 8387
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "acceptConversationLocally conversationId: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 8390
    :cond_0
    invoke-virtual {p0}, Lyt;->a()V

    .line 8392
    :try_start_0
    invoke-virtual {p0, p1}, Lyt;->r(Ljava/lang/String;)V

    .line 8394
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lyt;->f(Ljava/lang/String;I)V

    .line 8396
    const/16 v0, 0x1e

    invoke-virtual {p0, p1, v0}, Lyt;->a(Ljava/lang/String;I)V

    .line 8401
    invoke-virtual {p0}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8403
    invoke-virtual {p0}, Lyt;->c()V

    .line 8404
    return-void

    .line 8403
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lyt;->c()V

    throw v0
.end method

.method public af(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 8428
    const-string v0, "Babel_db"

    const-string v1, "removeConversationTransaction"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 8429
    invoke-virtual {p0}, Lyt;->a()V

    .line 8431
    :try_start_0
    invoke-virtual {p0, p1}, Lyt;->C(Ljava/lang/String;)V

    .line 8432
    invoke-virtual {p0}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8434
    invoke-virtual {p0}, Lyt;->c()V

    .line 8435
    return-void

    .line 8434
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lyt;->c()V

    throw v0
.end method

.method public ag(Ljava/lang/String;)Ljava/util/Set;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 8493
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "merge_keys"

    sget-object v2, Lyx;->a:[Ljava/lang/String;

    const-string v3, "merge_key IN (SELECT merge_key FROM merge_keys WHERE conversation_id=?)"

    new-array v4, v7, [Ljava/lang/String;

    aput-object p1, v4, v6

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 8502
    :try_start_0
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 8503
    if-eqz v1, :cond_1

    .line 8504
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 8505
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 8506
    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 8519
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_0

    .line 8520
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    .line 8514
    :cond_1
    :try_start_1
    invoke-interface {v2, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 8515
    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v0

    if-eqz v0, :cond_2

    .line 8516
    invoke-static {p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_2
    move v0, v7

    .line 8514
    :goto_1
    invoke-static {v0}, Lcwz;->a(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 8519
    if-eqz v1, :cond_3

    .line 8520
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    return-object v2

    :cond_4
    move v0, v6

    .line 8516
    goto :goto_1
.end method

.method public ah(Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lyv;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 8526
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "merge_keys"

    sget-object v2, Lyx;->a:[Ljava/lang/String;

    const-string v3, "merge_key IN (SELECT merge_key FROM merge_keys WHERE conversation_id=?)"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 8535
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 8536
    if-eqz v1, :cond_1

    .line 8537
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 8538
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 8539
    invoke-virtual {p0, v2}, Lyt;->e(Ljava/lang/String;)Lyv;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 8545
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_0

    .line 8546
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    .line 8545
    :cond_1
    if-eqz v1, :cond_2

    .line 8546
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    return-object v0
.end method

.method public ai(Ljava/lang/String;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 8552
    invoke-virtual {p0, p1}, Lyt;->ag(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    .line 8553
    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 8554
    return-object v0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)I
    .locals 6

    .prologue
    .line 2139
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2140
    const-string v1, "conversation_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2141
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "messages"

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public b(I)J
    .locals 9

    .prologue
    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    .line 1483
    const-string v0, "Babel_db"

    const-string v1, "queryLoadConversationsBefore"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1492
    :try_start_0
    const-string v3, "conversation_id NOT LIKE \'client_generated:%\' AND sort_timestamp> 0 AND transport_type!=3"

    .line 1496
    packed-switch p1, :pswitch_data_0

    .line 1509
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversations"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "MIN(sort_timestamp)"

    aput-object v5, v2, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1514
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1515
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v0

    .line 1516
    const-wide/16 v3, 0x0

    cmp-long v3, v0, v3

    if-nez v3, :cond_0

    move-wide v0, v6

    .line 1521
    :cond_0
    if-eqz v2, :cond_1

    .line 1522
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1525
    :cond_1
    :goto_1
    return-wide v0

    .line 1500
    :pswitch_1
    :try_start_2
    const-string v3, "conversation_id NOT LIKE \'client_generated:%\' AND sort_timestamp> 0 AND transport_type!=3 AND view=2"

    goto :goto_0

    .line 1503
    :pswitch_2
    const-string v3, "conversation_id NOT LIKE \'client_generated:%\' AND sort_timestamp> 0 AND transport_type!=3 AND view=1 AND status=1 AND inviter_affinity=1"

    goto :goto_0

    .line 1506
    :pswitch_3
    const-string v3, "conversation_id NOT LIKE \'client_generated:%\' AND sort_timestamp> 0 AND transport_type!=3 AND view=1 AND status=1 AND inviter_affinity=2"
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1521
    :cond_2
    if-eqz v2, :cond_3

    .line 1522
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_3
    move-wide v0, v6

    .line 1525
    goto :goto_1

    .line 1521
    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_2
    if-eqz v1, :cond_4

    .line 1522
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 1521
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_2

    .line 1496
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public b(Ljava/lang/String;I)J
    .locals 6

    .prologue
    .line 7001
    invoke-virtual {p0, p1}, Lyt;->S(Ljava/lang/String;)J

    move-result-wide v2

    .line 7004
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v4, 0x3e8

    mul-long/2addr v0, v4

    .line 7005
    cmp-long v4, v0, v2

    if-gtz v4, :cond_0

    .line 7010
    int-to-long v0, p2

    rem-long v0, v2, v0

    .line 7011
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-nez v4, :cond_1

    int-to-long v0, p2

    .line 7012
    :goto_0
    add-long/2addr v0, v2

    .line 7014
    :cond_0
    return-wide v0

    .line 7011
    :cond_1
    int-to-long v4, p2

    sub-long v0, v4, v0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;Lbsc;)Laea;
    .locals 9

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5886
    invoke-virtual {p2, p1}, Lbsc;->c(Ljava/lang/String;)Lcvw;

    move-result-object v0

    .line 5887
    if-eqz v0, :cond_1

    .line 5888
    invoke-static {v0}, Laea;->a(Lcvw;)Laea;

    move-result-object v6

    .line 5933
    :cond_0
    :goto_0
    return-object v6

    .line 5894
    :cond_1
    invoke-virtual {p2, p1}, Lbsc;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 5899
    if-eqz v0, :cond_7

    .line 5900
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v1, v2

    move v4, v2

    move-object v5, v6

    :cond_2
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcvw;

    .line 5901
    invoke-interface {v0}, Lcvw;->e()Ljava/lang/String;

    move-result-object v7

    .line 5902
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {v7, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 5903
    invoke-static {v7}, Lbdk;->b(Ljava/lang/String;)Lbdk;

    move-result-object v0

    .line 5910
    invoke-virtual {p0, v0, v2, v3}, Lyt;->a(Lbdk;ZI)Lzi;

    move-result-object v0

    if-eqz v0, :cond_3

    move v0, v3

    .line 5912
    :goto_2
    if-nez v5, :cond_4

    move v4, v0

    move-object v5, v7

    .line 5915
    goto :goto_1

    :cond_3
    move v0, v2

    .line 5910
    goto :goto_2

    .line 5918
    :cond_4
    if-nez v4, :cond_5

    if-eqz v0, :cond_5

    move v1, v3

    move v4, v0

    move-object v5, v7

    .line 5921
    goto :goto_1

    .line 5922
    :cond_5
    if-eqz v4, :cond_6

    if-nez v0, :cond_0

    :cond_6
    move v1, v3

    .line 5927
    goto :goto_1

    :cond_7
    move v1, v2

    move v4, v2

    move-object v5, v6

    .line 5930
    :cond_8
    if-eqz v5, :cond_0

    if-nez v4, :cond_9

    if-nez v1, :cond_0

    .line 5931
    :cond_9
    invoke-virtual {p2, v5}, Lbsc;->a(Ljava/lang/String;)Laea;

    move-result-object v6

    goto :goto_0
.end method

.method public b(Lbdk;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 5063
    invoke-virtual {p0, p1}, Lyt;->a(Lbdk;)Ljava/lang/String;

    move-result-object v0

    .line 5064
    if-nez v0, :cond_0

    .line 5065
    invoke-direct {p0, p1}, Lyt;->c(Lbdk;)Ljava/lang/String;

    move-result-object v0

    .line 5067
    :cond_0
    return-object v0
.end method

.method public b(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lbcx;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 6635
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 6636
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbcx;

    .line 6637
    iget-object v2, v0, Lbcx;->a:Ljava/lang/String;

    iget-object v3, v0, Lbcx;->b:Ljava/lang/String;

    iget-object v4, v0, Lbcx;->d:Ljava/lang/String;

    iget-object v5, v0, Lbcx;->c:Ljava/lang/String;

    iget-object v6, v0, Lbcx;->e:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 6640
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 6641
    const/16 v1, 0x7c

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 6643
    :cond_0
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 6645
    :cond_1
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(J)Lzg;
    .locals 3

    .prologue
    .line 3182
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 3183
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getMessageInfo, messageRowId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3186
    :cond_0
    const-string v0, "_id"

    .line 3187
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 3186
    invoke-direct {p0, v0, v1}, Lyt;->l(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 3189
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 3190
    const/4 v0, 0x0

    .line 3193
    :goto_0
    return-object v0

    :cond_1
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lzg;

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lyt;->b:Lzr;

    invoke-virtual {v0}, Lzr;->b()V

    .line 315
    return-void
.end method

.method public b(JIJ)V
    .locals 5

    .prologue
    .line 8043
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 8044
    iget-object v1, p0, Lyt;->b:Lzr;

    invoke-virtual {v1}, Lzr;->a()V

    .line 8046
    :try_start_0
    const-string v1, "sms_message_status"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 8047
    const-string v1, "sms_timestamp_sent"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 8048
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "messages"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 8051
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 8048
    invoke-virtual {v1, v2, v0, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 8053
    iget-object v0, p0, Lyt;->b:Lzr;

    invoke-virtual {v0}, Lzr;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8055
    iget-object v0, p0, Lyt;->b:Lzr;

    invoke-virtual {v0}, Lzr;->c()V

    .line 8056
    return-void

    .line 8055
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lyt;->b:Lzr;

    invoke-virtual {v1}, Lzr;->c()V

    throw v0
.end method

.method public b(JJ)V
    .locals 5

    .prologue
    .line 7961
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 7962
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setMmsNotificationStatus: messageRowId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", status=2, ts="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7965
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 7966
    const-string v1, "status"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 7967
    const-wide/16 v1, 0x0

    cmp-long v1, p3, v1

    if-lez v1, :cond_1

    .line 7969
    const-string v1, "sms_timestamp_sent"

    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p3

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 7971
    :cond_1
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "messages"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 7972
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 7971
    invoke-virtual {v1, v2, v0, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 7973
    return-void
.end method

.method public b(Ljava/lang/String;J)V
    .locals 6

    .prologue
    .line 1446
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 1447
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setConversationSequenceNumber: conversationId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", sequenceNumber="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1450
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1451
    const-string v1, "sequence_number"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1452
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "conversations"

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1455
    return-void
.end method

.method public b(Ljava/lang/String;JJ)V
    .locals 7

    .prologue
    .line 7429
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 7430
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateMessageScrollTime: conversationId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " scrollTime="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " scrollToMessageTimestamp="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7436
    :cond_0
    invoke-virtual {p0}, Lyt;->a()V

    .line 7438
    :try_start_0
    invoke-direct {p0, p1}, Lyt;->ao(Ljava/lang/String;)Lzl;

    move-result-object v0

    .line 7439
    if-nez v0, :cond_1

    .line 7440
    invoke-virtual {p0}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7480
    invoke-virtual {p0}, Lyt;->c()V

    .line 7481
    :goto_0
    return-void

    .line 7444
    :cond_1
    :try_start_1
    invoke-static {v0, p2, p3, p4, p5}, Lyt;->a(Lzl;JJ)Lzl;

    move-result-object v1

    .line 7447
    if-eq v1, v0, :cond_6

    .line 7448
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 7449
    iget-wide v3, v1, Lzl;->a:J

    iget-wide v5, v0, Lzl;->a:J

    cmp-long v3, v3, v5

    if-eqz v3, :cond_2

    .line 7450
    const-string v3, "first_peak_scroll_time"

    iget-wide v4, v1, Lzl;->a:J

    .line 7451
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 7450
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 7454
    :cond_2
    iget-wide v3, v1, Lzl;->b:J

    iget-wide v5, v0, Lzl;->b:J

    cmp-long v3, v3, v5

    if-eqz v3, :cond_3

    .line 7456
    const-string v3, "first_peak_scroll_to_message_timestamp"

    iget-wide v4, v1, Lzl;->b:J

    .line 7457
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 7456
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 7460
    :cond_3
    iget-wide v3, v1, Lzl;->c:J

    iget-wide v5, v0, Lzl;->c:J

    cmp-long v3, v3, v5

    if-eqz v3, :cond_4

    .line 7462
    const-string v3, "second_peak_scroll_time"

    iget-wide v4, v1, Lzl;->c:J

    .line 7463
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 7462
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 7466
    :cond_4
    iget-wide v3, v1, Lzl;->d:J

    iget-wide v5, v0, Lzl;->d:J

    cmp-long v0, v3, v5

    if-eqz v0, :cond_5

    .line 7468
    const-string v0, "second_peak_scroll_to_message_timestamp"

    iget-wide v3, v1, Lzl;->d:J

    .line 7469
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 7468
    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 7472
    :cond_5
    invoke-virtual {v2}, Landroid/content/ContentValues;->size()I

    move-result v0

    if-lez v0, :cond_6

    .line 7473
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversations"

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 7478
    :cond_6
    invoke-virtual {p0}, Lyt;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 7480
    invoke-virtual {p0}, Lyt;->c()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lyt;->c()V

    throw v0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2243
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 2244
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateConversationParticipantInfo, conversationId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", generatedName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2247
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2248
    const-string v1, "generated_name"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2249
    const-string v1, "packed_avatar_urls"

    invoke-virtual {v0, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2250
    const-string v1, "self_avatar_url"

    invoke-virtual {v0, v1, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2252
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "conversations"

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2255
    return-void
.end method

.method public b([Ljava/lang/String;)V
    .locals 11

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 8724
    array-length v3, p1

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v4, p1, v2

    .line 8725
    const-string v5, "client_generated:sms:"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 8726
    add-int/lit8 v0, v0, 0x1

    .line 8724
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 8729
    :cond_1
    if-le v0, v6, :cond_2

    .line 8730
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "cannot call mergeConversation with more than 1 gaia conversations"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 8734
    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 8735
    invoke-virtual {p0}, Lyt;->e()Lzr;

    move-result-object v3

    .line 8737
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lyp;->b:Ljava/util/Random;

    .line 8739
    invoke-virtual {v4}, Ljava/util/Random;->nextLong()J

    move-result-wide v4

    .line 8738
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v4, Lyp;->b:Ljava/util/Random;

    .line 8741
    invoke-virtual {v4}, Ljava/util/Random;->nextLong()J

    move-result-wide v4

    .line 8740
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 8737
    invoke-static {v0}, Lyt;->aq(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 8744
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 8745
    const-string v0, "merge_key"

    invoke-virtual {v5, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 8747
    invoke-virtual {v3}, Lzr;->a()V

    .line 8749
    :try_start_0
    array-length v6, p1

    move v0, v1

    :goto_1
    if-ge v0, v6, :cond_3

    aget-object v1, p1, v0

    .line 8750
    const-string v7, "merge_keys"

    const-string v8, "conversation_id=?"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object v1, v9, v10

    invoke-virtual {v3, v7, v5, v8, v9}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 8756
    new-instance v7, Landroid/util/Pair;

    invoke-direct {v7, v1, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 8749
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 8758
    :cond_3
    invoke-virtual {v3}, Lzr;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8760
    invoke-virtual {v3}, Lzr;->c()V

    .line 8762
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/util/Pair;

    .line 8764
    invoke-virtual {p0}, Lyt;->f()Lyj;

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    .line 8763
    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 8760
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lzr;->c()V

    throw v0

    .line 8768
    :cond_4
    iget-object v0, p0, Lyt;->d:Lyj;

    invoke-static {v0}, Lyp;->a(Lyj;)V

    .line 8769
    return-void
.end method

.method public c(I)J
    .locals 4

    .prologue
    .line 6875
    invoke-static {p1}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(I)Ljava/lang/String;

    move-result-object v0

    const-wide/16 v1, -0x1

    invoke-direct {p0, v0, v1, v2}, Lyt;->q(Ljava/lang/String;J)J

    move-result-wide v0

    .line 6877
    const-wide/16 v2, -0x3

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 6878
    const-wide/16 v0, -0x2

    .line 6880
    :cond_0
    return-wide v0
.end method

.method public c(J)Landroid/util/Pair;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 3984
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 3985
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getConversationIdStatus, messageRowId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3991
    :cond_0
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "messages"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "conversation_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "status"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 3994
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 3991
    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 3997
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3998
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 3999
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    .line 4002
    :goto_0
    if-eqz v1, :cond_1

    .line 4003
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 4006
    :cond_1
    new-instance v1, Landroid/util/Pair;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v1, v6, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1

    .line 4002
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_1
    if-eqz v1, :cond_2

    .line 4003
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 4002
    :catchall_1
    move-exception v0

    goto :goto_1

    :cond_3
    move v0, v7

    goto :goto_0
.end method

.method public c(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lbcw;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 6650
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 6651
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbcw;

    .line 6652
    iget-object v1, v0, Lbcw;->a:Lbcx;

    iget-object v2, v1, Lbcx;->a:Ljava/lang/String;

    iget-object v1, v0, Lbcw;->a:Lbcx;

    iget-object v3, v1, Lbcx;->b:Ljava/lang/String;

    iget-object v1, v0, Lbcw;->a:Lbcx;

    iget-object v4, v1, Lbcx;->d:Ljava/lang/String;

    iget-object v1, v0, Lbcw;->a:Lbcx;

    iget-object v5, v1, Lbcx;->c:Ljava/lang/String;

    iget-object v0, v0, Lbcw;->a:Lbcx;

    iget-object v6, v0, Lbcx;->e:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 6656
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 6657
    const/16 v1, 0x7c

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 6659
    :cond_0
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 6661
    :cond_1
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()V
    .locals 3

    .prologue
    .line 320
    iget-object v0, p0, Lyt;->b:Lzr;

    if-eqz v0, :cond_0

    .line 321
    iget-object v0, p0, Lyt;->b:Lzr;

    invoke-virtual {v0}, Lzr;->c()V

    .line 326
    :goto_0
    iget v0, p0, Lyt;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lyt;->e:I

    .line 327
    return-void

    .line 323
    :cond_0
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "endTransaction called on a database not fully setup. Account: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lyt;->d:Lyj;

    .line 324
    invoke-virtual {v2}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 323
    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public c(Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 7031
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 7032
    const-string v1, "view"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 7033
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "conversations"

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 7038
    if-eqz v0, :cond_0

    .line 7039
    invoke-static {p0}, Lyp;->d(Lyt;)V

    .line 7041
    :cond_0
    return-void
.end method

.method public c(Ljava/lang/String;J)V
    .locals 21

    .prologue
    .line 2511
    const/4 v8, 0x0

    .line 2512
    const-wide/16 v17, 0x0

    .line 2513
    const-wide/16 v15, 0x0

    .line 2514
    const-wide/16 v13, 0x0

    .line 2515
    const/4 v12, 0x0

    .line 2516
    const/4 v11, 0x0

    .line 2517
    const-wide/16 v9, 0x0

    .line 2519
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lyt;->b:Lzr;

    const-string v3, "conversations"

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "self_watermark"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "chat_watermark"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "hangout_watermark"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "has_chat_notifications"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "has_video_notifications"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    const-string v6, "latest_message_timestamp"

    aput-object v6, v4, v5

    const-string v5, "conversation_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 2531
    :try_start_1
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_11

    .line 2532
    const/4 v2, 0x0

    invoke-interface {v3, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2533
    const/4 v2, 0x0

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v17

    .line 2535
    :cond_0
    const/4 v2, 0x1

    invoke-interface {v3, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2536
    const/4 v2, 0x1

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v15

    .line 2538
    :cond_1
    const/4 v2, 0x2

    invoke-interface {v3, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_10

    .line 2539
    const/4 v2, 0x2

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 2541
    :goto_0
    const/4 v2, 0x3

    invoke-interface {v3, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_f

    .line 2542
    const/4 v2, 0x3

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 2544
    :goto_1
    const/4 v2, 0x4

    invoke-interface {v3, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_e

    .line 2545
    const/4 v2, 0x4

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 2547
    :goto_2
    const/4 v4, 0x5

    invoke-interface {v3, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_d

    .line 2548
    const/4 v4, 0x5

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v4

    int-to-long v4, v4

    move-wide v10, v4

    move v12, v6

    move-wide v4, v7

    move-wide v6, v15

    move-wide/from16 v8, v17

    .line 2552
    :goto_3
    if-eqz v3, :cond_2

    .line 2553
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 2557
    :cond_2
    sget-boolean v3, Lyt;->a:Z

    if-eqz v3, :cond_3

    .line 2558
    const-string v3, "Babel_db"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "setSelfWatermarkTimestamp, conversationId: "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", watermarkTimestamp: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-wide/from16 v0, p2

    invoke-virtual {v13, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", currentSelfWatermark: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", currentChatWatermark: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", currentHangoutWatermark: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", hasChatNotifications: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", hasVideoNotifications: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v12, ", latestMessageTimestamp: "

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2571
    :cond_3
    const-wide v2, 0x3fffffffffffffffL    # 1.9999999999999998

    cmp-long v2, v8, v2

    if-nez v2, :cond_4

    .line 2572
    const-wide/16 v2, 0x0

    move-wide v8, v2

    .line 2574
    :cond_4
    const-wide v2, 0x3fffffffffffffffL    # 1.9999999999999998

    cmp-long v2, v6, v2

    if-nez v2, :cond_5

    .line 2575
    const-wide/16 v2, 0x0

    move-wide v6, v2

    .line 2577
    :cond_5
    const-wide v2, 0x3fffffffffffffffL    # 1.9999999999999998

    cmp-long v2, v4, v2

    if-nez v2, :cond_c

    .line 2578
    const-wide/16 v2, 0x0

    move-wide v3, v2

    .line 2581
    :goto_4
    cmp-long v2, p2, v8

    if-gtz v2, :cond_6

    cmp-long v2, p2, v6

    if-gtz v2, :cond_6

    cmp-long v2, p2, v3

    if-lez v2, :cond_a

    .line 2584
    :cond_6
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 2585
    cmp-long v2, p2, v8

    if-lez v2, :cond_7

    .line 2586
    const-string v2, "self_watermark"

    invoke-static/range {p2 .. p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v5, v2, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2589
    :cond_7
    const/4 v2, 0x0

    .line 2590
    cmp-long v6, p2, v6

    if-lez v6, :cond_8

    .line 2591
    const-string v6, "chat_watermark"

    .line 2592
    invoke-static/range {p2 .. p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 2591
    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2593
    cmp-long v6, v10, p2

    if-lez v6, :cond_8

    .line 2594
    const/4 v2, 0x1

    .line 2597
    :cond_8
    const-string v6, "has_chat_notifications"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v5, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2599
    const/4 v2, 0x0

    .line 2600
    cmp-long v3, p2, v3

    if-lez v3, :cond_9

    .line 2601
    const-string v3, "hangout_watermark"

    .line 2602
    invoke-static/range {p2 .. p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 2601
    invoke-virtual {v5, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2603
    cmp-long v3, v10, p2

    if-lez v3, :cond_9

    .line 2604
    const/4 v2, 0x1

    .line 2607
    :cond_9
    const-string v3, "has_video_notifications"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v5, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2608
    move-object/from16 v0, p0

    iget-object v2, v0, Lyt;->b:Lzr;

    const-string v3, "conversations"

    const-string v4, "conversation_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    invoke-virtual {v2, v3, v5, v4, v6}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2612
    :cond_a
    return-void

    .line 2552
    :catchall_0
    move-exception v2

    move-object v3, v8

    :goto_5
    if-eqz v3, :cond_b

    .line 2553
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_b
    throw v2

    .line 2552
    :catchall_1
    move-exception v2

    goto :goto_5

    :cond_c
    move-wide v3, v4

    goto :goto_4

    :cond_d
    move v12, v6

    move-wide v4, v7

    move-wide v6, v15

    move-wide/from16 v19, v9

    move-wide/from16 v10, v19

    move-wide/from16 v8, v17

    goto/16 :goto_3

    :cond_e
    move v2, v11

    goto/16 :goto_2

    :cond_f
    move v6, v12

    goto/16 :goto_1

    :cond_10
    move-wide v7, v13

    goto/16 :goto_0

    :cond_11
    move v2, v11

    move-wide v4, v13

    move-wide v6, v15

    move-wide/from16 v19, v9

    move-wide/from16 v10, v19

    move-wide/from16 v8, v17

    goto/16 :goto_3
.end method

.method public c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2388
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 2389
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateConversationName, conversationId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", name="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2392
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2393
    const-string v1, "name"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2395
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "conversations"

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2398
    return-void
.end method

.method public c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 5223
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 5224
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5225
    const-string v1, "chat_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5226
    const-string v1, "name"

    invoke-virtual {v0, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5227
    const-string v1, "profile_photo_url"

    invoke-virtual {v0, v1, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5229
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "blocked_people"

    invoke-virtual {v1, v2, v0}, Lzr;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 5230
    invoke-static {}, Lzo;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->i:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 5231
    return-void
.end method

.method public d(Ljava/lang/String;J)I
    .locals 6

    .prologue
    .line 2706
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2707
    const-string v1, "wearable_watermark"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2708
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "conversations"

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public d(J)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 4147
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 4148
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "queryConversationsSince, lastSuccessfulSyncTime="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4151
    :cond_0
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 4153
    const/4 v6, 0x0

    .line 4161
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "messages_view"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "conversation_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "message_id"

    aput-object v4, v2, v3

    const-string v3, "timestamp >? AND conversation_id NOT LIKE ?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 4166
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v5

    const/4 v5, 0x1

    const-string v8, "client_generated:%"

    aput-object v8, v4, v5

    const-string v5, "conversation_id"

    .line 4161
    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 4172
    :goto_0
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4173
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 4174
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 4175
    new-instance v3, Landroid/util/Pair;

    invoke-direct {v3, v0, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 4178
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_1

    .line 4179
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 4178
    :cond_2
    if-eqz v1, :cond_3

    .line 4179
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 4182
    :cond_3
    return-object v7

    .line 4178
    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_1
.end method

.method public d(Ljava/lang/String;I)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 7645
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 7646
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getMessageTimestamps: account="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lyt;->d:Lyj;

    invoke-virtual {v2}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " conversationId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7650
    :cond_0
    const/4 v7, 0x0

    .line 7652
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "messages"

    sget-object v2, Lyt;->B:[Ljava/lang/String;

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const-string v5, "timestamp DESC"

    .line 7657
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    .line 7652
    invoke-virtual/range {v0 .. v6}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 7659
    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 7660
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 7661
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 7666
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_1

    .line 7667
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 7666
    :cond_2
    if-eqz v1, :cond_3

    .line 7667
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    return-object v0

    .line 7666
    :catchall_1
    move-exception v0

    move-object v1, v7

    goto :goto_1
.end method

.method public d()V
    .locals 3

    .prologue
    .line 330
    iget v0, p0, Lyt;->e:I

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 331
    iget-object v0, p0, Lyt;->b:Lzr;

    invoke-virtual {v0}, Lzr;->d()V

    .line 340
    :goto_0
    return-void

    .line 338
    :cond_0
    const-string v0, "Babel_db"

    const-string v1, "cannot yield from within a nested transaction"

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-static {v0, v1, v2}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2401
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2402
    const-string v1, "chat_ringtone_uri"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2404
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "conversations"

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2407
    return-void
.end method

.method public e(Ljava/lang/String;)Lyv;
    .locals 3

    .prologue
    .line 724
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 725
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getConversationInfo "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 727
    :cond_0
    const/4 v0, 0x0

    .line 728
    invoke-direct {p0, p1}, Lyt;->am(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 730
    if-eqz v1, :cond_1

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 731
    invoke-direct {p0, v1}, Lyt;->a(Landroid/database/Cursor;)Lyv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 734
    :cond_1
    if-eqz v1, :cond_2

    .line 735
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 738
    :cond_2
    return-object v0

    .line 734
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_3

    .line 735
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public e()Lzr;
    .locals 1

    .prologue
    .line 344
    iget-object v0, p0, Lyt;->b:Lzr;

    return-object v0
.end method

.method public e(J)V
    .locals 11

    .prologue
    const/4 v6, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 4238
    const-string v0, "Babel_db"

    const-string v1, "Rewinding"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4239
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "messages"

    const-string v2, "timestamp>? AND persisted = 1"

    new-array v3, v10, [Ljava/lang/String;

    .line 4241
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v9

    .line 4239
    invoke-virtual {v0, v1, v2, v3}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4243
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 4246
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversations"

    sget-object v2, Lyt;->u:[Ljava/lang/String;

    const-string v3, "self_watermark >? AND conversation_id NOT LIKE \'client_generated:%\'"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 4251
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v5

    const/4 v5, 0x0

    .line 4246
    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 4253
    :goto_0
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4254
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 4255
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 4258
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_0

    .line 4259
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    .line 4258
    :cond_1
    if-eqz v1, :cond_2

    .line 4259
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 4262
    :cond_2
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 4263
    const-string v0, "self_watermark"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 4264
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 4265
    iget-object v3, p0, Lyt;->b:Lzr;

    const-string v4, "conversations"

    const-string v5, "conversation_id=?"

    new-array v6, v10, [Ljava/lang/String;

    aput-object v0, v6, v9

    invoke-virtual {v3, v4, v1, v5, v6}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4268
    invoke-static {p0}, Lyp;->e(Lyt;)V

    goto :goto_2

    .line 4273
    :cond_3
    const-string v0, "last_successful_sync_time"

    invoke-virtual {p0, v0, p1, p2}, Lyt;->h(Ljava/lang/String;J)V

    .line 4276
    invoke-static {p0}, Lyp;->d(Lyt;)V

    .line 4277
    invoke-static {p0}, Lyp;->c(Lyt;)V

    .line 4278
    return-void

    .line 4258
    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_1
.end method

.method public e(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 8372
    invoke-virtual {p0}, Lyt;->a()V

    .line 8374
    :try_start_0
    invoke-direct {p0, p1, p2}, Lyt;->f(Ljava/lang/String;I)V

    .line 8375
    invoke-virtual {p0}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8377
    invoke-virtual {p0}, Lyt;->c()V

    .line 8378
    return-void

    .line 8377
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lyt;->c()V

    throw v0
.end method

.method public e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2410
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2411
    const-string v1, "hangout_ringtone_uri"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2413
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "conversations"

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2416
    return-void
.end method

.method public e(Ljava/lang/String;J)Z
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 4323
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 4324
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "deleteConversation:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " upperBoundTimestamp:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4328
    :cond_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "messages"

    const-string v2, "conversation_id=? AND timestamp<=?"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    aput-object p1, v3, v7

    .line 4330
    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    .line 4328
    invoke-virtual {v0, v1, v2, v3}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4335
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "messages"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 4338
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_2

    move v0, v6

    .line 4340
    :goto_0
    if-eqz v1, :cond_1

    .line 4341
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 4344
    :cond_1
    if-eqz v0, :cond_4

    .line 4345
    invoke-virtual {p0, p1}, Lyt;->C(Ljava/lang/String;)V

    .line 4352
    :goto_1
    return v6

    :cond_2
    move v0, v7

    .line 4338
    goto :goto_0

    .line 4340
    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_2
    if-eqz v1, :cond_3

    .line 4341
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 4350
    :cond_4
    invoke-virtual {p0, p1, v8, p2, p3}, Lyt;->a(Ljava/lang/String;[BJ)V

    move v6, v7

    .line 4352
    goto :goto_1

    .line 4340
    :catchall_1
    move-exception v0

    goto :goto_2
.end method

.method public f(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 841
    const-string v0, "getConversationTransportType"

    const-string v1, "transport_type"

    const/4 v2, 0x0

    invoke-direct {p0, v0, p1, v1, v2}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public f(Ljava/lang/String;J)I
    .locals 6

    .prologue
    .line 4472
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 4473
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "deletePlayedEventSuggestions for account: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lyt;->d:Lyj;

    .line 4474
    invoke-virtual {v2}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 4473
    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4477
    :cond_0
    invoke-virtual {p0}, Lyt;->a()V

    .line 4480
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "event_suggestions"

    const-string v2, "conversation_id=? AND timestamp<=?"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    .line 4482
    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 4480
    invoke-virtual {v0, v1, v2, v3}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 4483
    invoke-virtual {p0}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4485
    invoke-virtual {p0}, Lyt;->c()V

    .line 4490
    return v0

    .line 4485
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lyt;->c()V

    throw v0
.end method

.method public f()Lyj;
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lyt;->d:Lyj;

    return-object v0
.end method

.method public f(Ljava/lang/String;Ljava/lang/String;)Lzg;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 3200
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "messages_view"

    sget-object v2, Lyt;->r:[Ljava/lang/String;

    const-string v3, "conversation_id=? AND message_id=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    aput-object p2, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 3209
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3210
    invoke-direct {p0, v1}, Lyt;->d(Landroid/database/Cursor;)Lzg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v6

    .line 3213
    :cond_0
    if-eqz v1, :cond_1

    .line 3214
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 3218
    :cond_1
    return-object v6

    .line 3213
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_0
    if-eqz v1, :cond_2

    .line 3214
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 3213
    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public f(J)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 4390
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 4391
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "deleteMessage, rowId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4394
    :cond_0
    invoke-virtual {p0, p1, p2}, Lyt;->g(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lf;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 4395
    if-eqz v0, :cond_1

    const-string v1, "content://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4397
    invoke-static {}, Lzo;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0, v4, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4399
    :cond_1
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "messages"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "_id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 4400
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 4399
    invoke-virtual {v0, v1, v2, v4}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4402
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "latest_message_expiration_timestamp"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v1, "snippet_type"

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "snippet_author_chat_id"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v1, "snippet_image_url"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v1, "snippet_text"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v1, "snippet_message_row_id"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v1, "snippet_status"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v1, "previous_latest_timestamp"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v1, "snippet_new_conversation_name"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v1, "snippet_participant_keys"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v1, "snippet_voicemail_duration"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "conversations"

    const-string v3, "snippet_message_row_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4403
    return-void
.end method

.method public g(Ljava/lang/String;)J
    .locals 6

    .prologue
    .line 1129
    const-string v1, "getContinuationEventTimestamp"

    const-string v3, "continuation_event_timestamp"

    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public g(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Long;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 4122
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 4123
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getMessageTimeStamp, conversationId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4128
    :cond_0
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "messages"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "timestamp"

    aput-object v4, v2, v3

    const-string v3, "conversation_id=? AND message_id=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    aput-object p2, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 4135
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4136
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v6

    .line 4139
    :cond_1
    if-eqz v1, :cond_2

    .line 4140
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 4143
    :cond_2
    return-object v6

    .line 4139
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_0
    if-eqz v1, :cond_3

    .line 4140
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 4139
    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public g(J)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 4409
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 4410
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getMessageExternalId, messageRowId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4414
    :cond_0
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "messages"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "external_ids"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 4416
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 4414
    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 4419
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4420
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 4423
    if-eqz v1, :cond_1

    .line 4424
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 4427
    :cond_1
    :goto_0
    return-object v0

    .line 4423
    :cond_2
    if-eqz v1, :cond_3

    .line 4424
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    move-object v0, v6

    .line 4427
    goto :goto_0

    .line 4423
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v6, :cond_4

    .line 4424
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 4423
    :catchall_1
    move-exception v0

    move-object v6, v1

    goto :goto_1
.end method

.method public g(Ljava/lang/String;J)V
    .locals 7

    .prologue
    .line 4853
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 4854
    const-string v1, "status"

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4855
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "messages"

    const-string v3, "conversation_id=? AND _id=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    .line 4858
    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 4855
    invoke-virtual {v1, v2, v0, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4860
    return-void
.end method

.method public h(J)I
    .locals 6

    .prologue
    .line 4442
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 4443
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "deleteExpiredOTRMessages for account: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lyt;->d:Lyj;

    .line 4444
    invoke-virtual {v2}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 4443
    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4446
    :cond_0
    invoke-virtual {p0}, Lyt;->a()V

    .line 4449
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "messages"

    const-string v2, "expiration_timestamp<? AND transport_type!=3"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    .line 4451
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 4449
    invoke-virtual {v0, v1, v2, v3}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 4452
    invoke-virtual {p0}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4454
    invoke-virtual {p0}, Lyt;->c()V

    .line 4457
    return v0

    .line 4454
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lyt;->c()V

    throw v0
.end method

.method public h(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Long;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 4637
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 4638
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "queryMessageRowId, conversationId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", messageId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4643
    :cond_0
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "messages"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const-string v3, "conversation_id=? AND message_id=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    aput-object p2, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 4650
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 4651
    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_2

    .line 4652
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 4655
    if-eqz v1, :cond_1

    .line 4656
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 4659
    :cond_1
    :goto_0
    return-object v0

    .line 4655
    :cond_2
    if-eqz v1, :cond_3

    .line 4656
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    move-object v0, v6

    .line 4659
    goto :goto_0

    .line 4655
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v6, :cond_4

    .line 4656
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 4655
    :catchall_1
    move-exception v0

    move-object v6, v1

    goto :goto_1
.end method

.method public h(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1170
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 1171
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "clearContinuationToken: conversationId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1173
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1174
    const-string v1, "continuation_token"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1175
    const-string v1, "continuation_event_timestamp"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1176
    const-string v1, "has_oldest_message"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1177
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "conversations"

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1182
    invoke-static {p0}, Lyp;->e(Lyt;)V

    .line 1183
    return-void
.end method

.method public h(Ljava/lang/String;J)V
    .locals 1

    .prologue
    .line 6929
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v0

    invoke-static {v0}, Lbsx;->a(Lyj;)Lbsx;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lbsx;->b(Ljava/lang/String;J)V

    .line 6930
    return-void
.end method

.method public i(J)I
    .locals 6

    .prologue
    .line 4532
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 4533
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "deleteExpiredEventSuggestions for account: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lyt;->d:Lyj;

    .line 4534
    invoke-virtual {v2}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 4533
    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4537
    :cond_0
    invoke-virtual {p0}, Lyt;->a()V

    .line 4540
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "event_suggestions"

    const-string v2, "expiration_time_usec<=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    .line 4542
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 4540
    invoke-virtual {v0, v1, v2, v3}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 4544
    invoke-virtual {p0}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4546
    invoke-virtual {p0}, Lyt;->c()V

    .line 4549
    if-lez v0, :cond_1

    .line 4550
    sget-boolean v1, Lyt;->a:Z

    if-eqz v1, :cond_1

    .line 4551
    const-string v1, "Babel_db"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "deleteExpiredEventSuggestions:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " cutoffTimestamp:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4558
    :cond_1
    return v0

    .line 4546
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lyt;->c()V

    throw v0
.end method

.method public i(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1410
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 1411
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "clearConversation: conversationId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1413
    :cond_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversations"

    const-string v2, "conversation_id=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 1417
    if-lez v0, :cond_1

    .line 1418
    invoke-static {p0}, Lyp;->d(Lyt;)V

    .line 1420
    :cond_1
    return-void
.end method

.method public i(Ljava/lang/String;J)V
    .locals 3

    .prologue
    .line 7101
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 7102
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateSortTimestamp, conversationId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", timestamp="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7105
    :cond_0
    invoke-virtual {p0}, Lyt;->a()V

    .line 7108
    :try_start_0
    invoke-virtual {p0, p1, p2, p3}, Lyt;->j(Ljava/lang/String;J)I

    move-result v0

    .line 7109
    invoke-virtual {p0}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7111
    invoke-virtual {p0}, Lyt;->c()V

    .line 7114
    if-lez v0, :cond_1

    .line 7115
    invoke-static {p0}, Lyp;->d(Lyt;)V

    .line 7117
    :cond_1
    return-void

    .line 7111
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lyt;->c()V

    throw v0
.end method

.method public i(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5205
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5206
    const-string v1, "gaia_id=?"

    .line 5207
    new-array v0, v3, [Ljava/lang/String;

    aput-object p1, v0, v2

    .line 5216
    :goto_0
    iget-object v2, p0, Lyt;->b:Lzr;

    const-string v3, "blocked_people"

    invoke-virtual {v2, v3, v1, v0}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 5217
    invoke-static {}, Lzo;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->i:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 5218
    :goto_1
    return-void

    .line 5208
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 5209
    const-string v1, "chat_id=?"

    .line 5210
    new-array v0, v3, [Ljava/lang/String;

    aput-object p2, v0, v2

    goto :goto_0

    .line 5212
    :cond_1
    const-string v0, "Babel_db"

    const-string v1, "removeBlockedPerson without a valid gaiaId or chatId"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public j(J)I
    .locals 6

    .prologue
    .line 4570
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 4571
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "deleteEmptyConversations for account: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lyt;->d:Lyj;

    .line 4572
    invoke-virtual {v2}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 4571
    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4575
    :cond_0
    invoke-virtual {p0}, Lyt;->a()V

    .line 4578
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversations"

    const-string v2, "(has_oldest_message =1 OR has_persistent_events =0) AND sort_timestamp <? AND status != 1 AND NOT EXISTS (SELECT 1 FROM messages WHERE messages.conversation_id=conversations.conversation_id)"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    .line 4580
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 4578
    invoke-virtual {v0, v1, v2, v3}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 4582
    invoke-virtual {p0}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4584
    invoke-virtual {p0}, Lyt;->c()V

    .line 4587
    if-lez v0, :cond_2

    .line 4588
    sget-boolean v1, Lyt;->a:Z

    if-eqz v1, :cond_1

    .line 4589
    const-string v1, "Babel_db"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "deleteEmptyConversations:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " cutoffTimestamp:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4592
    :cond_1
    invoke-static {p0}, Lyp;->d(Lyt;)V

    .line 4594
    :cond_2
    return v0

    .line 4584
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lyt;->c()V

    throw v0
.end method

.method public j(Ljava/lang/String;J)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 7124
    invoke-virtual {p0, p1}, Lyt;->S(Ljava/lang/String;)J

    move-result-wide v1

    .line 7125
    cmp-long v1, p2, v1

    if-gez v1, :cond_1

    .line 7126
    sget-boolean v1, Lyt;->a:Z

    if-eqz v1, :cond_0

    .line 7127
    const-string v1, "Babel_db"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Skip updateSortTimestamp because new timestamp is smaller than current timestamp, conversationId="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", timestamp="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7140
    :cond_0
    :goto_0
    return v0

    .line 7132
    :cond_1
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 7133
    const-string v2, "sort_timestamp"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 7134
    iget-object v2, p0, Lyt;->b:Lzr;

    const-string v3, "conversations"

    const-string v4, "conversation_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    aput-object p1, v5, v0

    invoke-virtual {v2, v3, v1, v4, v5}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public j(Ljava/lang/String;)J
    .locals 6

    .prologue
    const-wide/16 v4, 0x1

    .line 1440
    const-string v1, "getConversationSequenceNumber"

    const-string v3, "sequence_number"

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)J

    move-result-wide v0

    .line 1442
    cmp-long v2, v0, v4

    if-gez v2, :cond_0

    :goto_0
    return-wide v4

    :cond_0
    move-wide v4, v0

    goto :goto_0
.end method

.method public j()Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 1042
    const-string v0, "Babel_db"

    const-string v1, "getConversationIds"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1044
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1047
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversations"

    sget-object v2, Lyt;->o:[Ljava/lang/String;

    const-string v3, "transport_type!=3"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 1050
    :goto_0
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1051
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1054
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_0

    .line 1055
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    .line 1054
    :cond_1
    if-eqz v1, :cond_2

    .line 1055
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1058
    :cond_2
    return-object v7

    .line 1054
    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_1
.end method

.method public j(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 6933
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v0

    invoke-static {v0}, Lbsx;->a(Lyj;)Lbsx;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lbsx;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 6934
    return-void
.end method

.method public k(J)I
    .locals 6

    .prologue
    .line 4605
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 4606
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "deleteExpiredInvitations for account:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lyt;->d:Lyj;

    .line 4607
    invoke-virtual {v2}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 4606
    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4610
    :cond_0
    invoke-virtual {p0}, Lyt;->a()V

    .line 4613
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversations"

    const-string v2, "sort_timestamp <? AND status = 1 AND otr_status = 1"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    .line 4615
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 4613
    invoke-virtual {v0, v1, v2, v3}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 4617
    invoke-virtual {p0}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4619
    invoke-virtual {p0}, Lyt;->c()V

    .line 4622
    if-lez v0, :cond_2

    .line 4623
    sget-boolean v1, Lyt;->a:Z

    if-eqz v1, :cond_1

    .line 4624
    const-string v1, "Babel_db"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "deleteExpiredInvitations:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " cutoffTimestamp:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4627
    :cond_1
    invoke-static {p0}, Lyp;->d(Lyt;)V

    .line 4629
    :cond_2
    return v0

    .line 4619
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lyt;->c()V

    throw v0
.end method

.method public k()V
    .locals 4

    .prologue
    .line 1427
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversations"

    const-string v2, "is_pending_leave<0"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 1430
    sget-boolean v1, Lyt;->a:Z

    if-eqz v1, :cond_0

    .line 1431
    const-string v1, "Babel_db"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "RemoveLeftConversations "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1434
    :cond_0
    if-lez v0, :cond_1

    .line 1435
    invoke-static {p0}, Lyp;->d(Lyt;)V

    .line 1437
    :cond_1
    return-void
.end method

.method public k(Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 1867
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 1868
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "expireLatestMessage. conversationId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1871
    :cond_0
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide v0

    .line 1873
    invoke-virtual {p0}, Lyt;->a()V

    .line 1875
    :try_start_0
    invoke-virtual {p0, p1}, Lyt;->m(Ljava/lang/String;)Lzc;

    move-result-object v2

    .line 1880
    iget-wide v3, v2, Lzc;->b:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-lez v3, :cond_1

    iget-wide v2, v2, Lzc;->b:J

    cmp-long v0, v2, v0

    if-lez v0, :cond_2

    .line 1882
    :cond_1
    invoke-virtual {p0}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1908
    invoke-virtual {p0}, Lyt;->c()V

    .line 1909
    :goto_0
    return-void

    .line 1886
    :cond_2
    :try_start_1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1891
    const-string v1, "latest_message_expiration_timestamp"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1892
    const-string v1, "snippet_type"

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1893
    const-string v1, "snippet_author_chat_id"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1894
    const-string v1, "snippet_image_url"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1895
    const-string v1, "snippet_text"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1896
    const-string v1, "snippet_message_row_id"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1897
    const-string v1, "snippet_status"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1898
    const-string v1, "previous_latest_timestamp"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1899
    const-string v1, "snippet_new_conversation_name"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1900
    const-string v1, "snippet_participant_keys"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1901
    const-string v1, "snippet_voicemail_duration"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1903
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "conversations"

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1906
    invoke-virtual {p0}, Lyt;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1908
    invoke-virtual {p0}, Lyt;->c()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lyt;->c()V

    throw v0
.end method

.method public k(Ljava/lang/String;J)V
    .locals 6

    .prologue
    .line 7150
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 7151
    const-string v1, "sort_timestamp"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 7152
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "conversations"

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 7157
    return-void
.end method

.method public k(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 7989
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 7990
    const-string v1, "sms_service_center"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 7991
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "conversations"

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 7996
    return-void
.end method

.method public l(J)I
    .locals 7

    .prologue
    const/4 v4, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 8185
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "(%s=%d) AND (%s<=?)"

    new-array v2, v4, [Ljava/lang/Object;

    const-string v3, "transport_type"

    aput-object v3, v2, v5

    .line 8189
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    const/4 v3, 0x2

    const-string v4, "timestamp"

    aput-object v4, v2, v3

    .line 8185
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 8191
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "messages"

    new-array v3, v6, [Ljava/lang/String;

    .line 8192
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    .line 8191
    invoke-virtual {v1, v2, v0, v3}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public l(Ljava/lang/String;J)I
    .locals 6

    .prologue
    .line 7684
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 7685
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "deleteOldMessages: account="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lyt;->d:Lyj;

    invoke-virtual {v2}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " conversationId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " cutOffTimestamp="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7691
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2, p3}, Lyt;->a(Ljava/lang/String;[BJ)V

    .line 7693
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "messages"

    const-string v2, "conversation_id=? AND timestamp<? AND transport_type!=3"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    .line 7695
    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 7693
    invoke-virtual {v0, v1, v2, v3}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 7696
    invoke-static {p0, p1}, Lyp;->d(Lyt;Ljava/lang/String;)V

    .line 7698
    return v0
.end method

.method public l(Ljava/lang/String;)J
    .locals 6

    .prologue
    .line 2174
    const-string v1, "getLatestMessageTimestamp"

    const-string v3, "latest_message_timestamp"

    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public l()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbcn;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 2289
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 2292
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "participants_view"

    sget-object v2, Lyt;->q:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 2293
    :goto_0
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2294
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2295
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2296
    const/4 v3, 0x2

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2298
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2299
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2300
    const-string v0, "Babel_db"

    const-string v2, "RefreshParticipantsOperation: found a participant with no valid id"

    invoke-static {v0, v2}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v6

    .line 2311
    :goto_1
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lbcn;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2312
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2322
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_0

    .line 2323
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    .line 2302
    :cond_1
    :try_start_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 2303
    invoke-static {v0}, Lbcn;->a(Ljava/lang/String;)Lbcn;

    move-result-object v0

    goto :goto_1

    .line 2304
    :cond_2
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 2305
    new-instance v3, Lbdk;

    invoke-direct {v3, v0, v2}, Lbdk;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2306
    invoke-virtual {v3}, Lbdk;->b()Lbcn;

    move-result-object v0

    goto :goto_1

    .line 2308
    :cond_3
    const-string v0, "Babel_db"

    const-string v2, "RefreshParticipantsOperation: skip circle"

    invoke-static {v0, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2318
    :cond_4
    const-string v0, "Babel_db"

    const-string v2, "RefreshParticipantsOperation: participant has no gaia id"

    invoke-static {v0, v2}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2322
    :cond_5
    if-eqz v1, :cond_6

    .line 2323
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2326
    :cond_6
    return-object v7

    .line 2322
    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_2
.end method

.method public m(J)I
    .locals 6

    .prologue
    .line 8352
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 8353
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "deleteMmsNotification "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 8355
    :cond_0
    invoke-virtual {p0}, Lyt;->a()V

    .line 8357
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "mms_notification_inds"

    const-string v2, "_id=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    .line 8360
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 8357
    invoke-virtual {v0, v1, v2, v3}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 8361
    invoke-virtual {p0}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8364
    invoke-virtual {p0}, Lyt;->c()V

    return v0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lyt;->c()V

    throw v0
.end method

.method public m()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 4208
    const-string v0, "Babel_db"

    const-string v1, "getUnreadConversations"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4210
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 4214
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversations"

    sget-object v2, Lyt;->u:[Ljava/lang/String;

    const-string v3, "self_watermark < sort_timestamp AND conversation_id NOT LIKE \'client_generated:%\'"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 4218
    :goto_0
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4219
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 4220
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 4221
    new-instance v3, Landroid/util/Pair;

    invoke-direct {v3, v0, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 4224
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_0

    .line 4225
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    .line 4224
    :cond_1
    if-eqz v1, :cond_2

    .line 4225
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 4228
    :cond_2
    return-object v7

    .line 4224
    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_1
.end method

.method public m(Ljava/lang/String;)Lzc;
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 2209
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 2210
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getLatestMessageInfo, conversationId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213
    :cond_0
    new-instance v7, Lzc;

    invoke-direct {v7}, Lzc;-><init>()V

    .line 2215
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversations"

    sget-object v2, Lyt;->p:[Ljava/lang/String;

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 2218
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v5

    const/4 v5, 0x0

    .line 2215
    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 2220
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2221
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v7, Lzc;->a:J

    .line 2222
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v7, Lzc;->b:J

    .line 2224
    const/4 v0, 0x2

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lzc;->c:I

    .line 2225
    const/4 v0, 0x3

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lzc;->e:Ljava/lang/String;

    .line 2226
    const/4 v0, 0x4

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lzc;->d:Ljava/lang/String;

    .line 2227
    const/4 v0, 0x5

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lzc;->f:Ljava/lang/String;

    .line 2228
    const/4 v0, 0x6

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v7, Lzc;->g:J

    .line 2229
    const/4 v0, 0x7

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v7, Lzc;->h:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2232
    :cond_1
    if-eqz v1, :cond_2

    .line 2233
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2236
    :cond_2
    return-object v7

    .line 2232
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_0
    if-eqz v1, :cond_3

    .line 2233
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 2232
    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public m(Ljava/lang/String;J)V
    .locals 1

    .prologue
    .line 7860
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lyt;->a(Ljava/lang/String;JZ)V

    .line 7861
    return-void
.end method

.method public n(J)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x0

    .line 8459
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "messages"

    sget-object v2, Lyt;->D:[Ljava/lang/String;

    const-string v3, "_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    .line 8463
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v7

    .line 8459
    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 8467
    if-eqz v1, :cond_0

    .line 8469
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 8470
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 8473
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 8476
    :cond_0
    :goto_0
    return-object v5

    .line 8473
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public n(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2360
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 2361
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getConversationName, conversationId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2366
    :cond_0
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversations"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "name"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "generated_name"

    aput-object v4, v2, v3

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 2373
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2374
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2375
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2376
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v6

    .line 2380
    :cond_1
    if-eqz v1, :cond_2

    .line 2381
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2384
    :cond_2
    return-object v6

    .line 2380
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_0
    if-eqz v1, :cond_3

    .line 2381
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 2380
    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public n()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 4945
    const-string v0, "Babel_db"

    const-string v1, "clearMessagesConversationsParticipants"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4947
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "messages"

    invoke-virtual {v0, v1, v2, v2}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4948
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversation_participants"

    invoke-virtual {v0, v1, v2, v2}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4951
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversations"

    invoke-virtual {v0, v1, v2, v2}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4953
    invoke-static {p0}, Lyp;->d(Lyt;)V

    .line 4954
    invoke-static {p0}, Lyp;->c(Lyt;)V

    .line 4955
    invoke-static {p0}, Lyp;->b(Lyt;)V

    .line 4956
    return-void
.end method

.method public n(Ljava/lang/String;J)V
    .locals 1

    .prologue
    .line 7864
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lyt;->a(Ljava/lang/String;JZ)V

    .line 7865
    return-void
.end method

.method public o()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 5141
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "blocked_people"

    invoke-virtual {v0, v1, v2, v2}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 5142
    invoke-static {}, Lzo;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->i:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 5143
    return-void
.end method

.method public o(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2419
    const-string v2, "getConversationHasMetadata"

    const-string v3, "metadata_present"

    invoke-direct {p0, v2, p1, v3, v1}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public o(Ljava/lang/String;J)Z
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 7888
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversations"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "is_pending_leave"

    aput-object v4, v2, v3

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 7897
    if-eqz v1, :cond_2

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 7898
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v2

    and-long/2addr v2, p2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_1

    move v0, v6

    .line 7901
    :goto_0
    if-eqz v1, :cond_0

    .line 7902
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 7905
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v0, v7

    .line 7898
    goto :goto_0

    .line 7901
    :cond_2
    if-eqz v1, :cond_3

    .line 7902
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    move v0, v7

    .line 7905
    goto :goto_1

    .line 7901
    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_2
    if-eqz v1, :cond_4

    .line 7902
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 7901
    :catchall_1
    move-exception v0

    goto :goto_2
.end method

.method public p(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 2464
    const-string v0, "getConversationType"

    const-string v1, "conversation_type"

    const/4 v2, 0x0

    invoke-direct {p0, v0, p1, v1, v2}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public p()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 5236
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "dismissed_contacts"

    invoke-virtual {v0, v1, v2, v2}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 5237
    invoke-static {}, Lzo;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->j:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 5238
    return-void
.end method

.method public p(Ljava/lang/String;J)V
    .locals 6

    .prologue
    .line 8023
    invoke-virtual {p0, p1}, Lyt;->aa(Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-lez v0, :cond_0

    .line 8024
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 8025
    const-string v1, "sms_thread_id"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 8026
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "conversations"

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 8032
    :cond_0
    return-void
.end method

.method public q()V
    .locals 5

    .prologue
    .line 6502
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 6503
    const-string v1, "status"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 6504
    invoke-virtual {p0}, Lyt;->a()V

    .line 6506
    :try_start_0
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "messages"

    const-string v3, "status=2"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 6509
    invoke-virtual {p0}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6511
    invoke-virtual {p0}, Lyt;->c()V

    .line 6513
    sget-boolean v1, Lyt;->a:Z

    if-nez v1, :cond_0

    sget-boolean v1, Lyt;->a:Z

    if-eqz v1, :cond_1

    .line 6514
    :cond_0
    const-string v1, "Babel_db"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "failAnySendingMessages patched "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " rows"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 6516
    :cond_1
    return-void

    .line 6511
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lyt;->c()V

    throw v0
.end method

.method public q(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2481
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 2482
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setConversationHidden, conversationId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2484
    :cond_0
    sget-wide v0, Lyp;->h:J

    invoke-virtual {p0, p1, v0, v1}, Lyt;->n(Ljava/lang/String;J)V

    .line 2487
    return-void
.end method

.method public r()V
    .locals 2

    .prologue
    .line 6907
    const/4 v0, 0x1

    .line 6908
    :goto_0
    const/4 v1, 0x4

    if-gt v0, v1, :cond_0

    .line 6910
    invoke-direct {p0, v0}, Lyt;->d(I)V

    .line 6909
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6912
    :cond_0
    return-void
.end method

.method public r(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2492
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 2493
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setConversationPendingAccept, conversationId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", status=2"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2496
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2497
    const-string v1, "status"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2498
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "conversations"

    const-string v3, "conversation_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2501
    return-void
.end method

.method public s(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 2505
    const-string v0, "getConversationStatus"

    const-string v1, "status"

    const/4 v2, 0x0

    invoke-direct {p0, v0, p1, v1, v2}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public s()J
    .locals 2

    .prologue
    .line 7303
    invoke-direct {p0}, Lyt;->E()Lzl;

    move-result-object v0

    invoke-static {v0}, Lyt;->a(Lzl;)J

    move-result-wide v0

    return-wide v0
.end method

.method public t(Ljava/lang/String;)J
    .locals 6

    .prologue
    .line 2615
    const-string v1, "getSelfWatermarkTimestamp"

    const-string v3, "self_watermark"

    const-wide/16 v4, -0x1

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public t()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 7546
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    .line 7547
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "deleteAllConversations: account="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lyt;->d:Lyj;

    invoke-virtual {v2}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 7552
    :cond_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversations"

    invoke-virtual {v0, v1, v3, v3}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 7553
    invoke-static {p0}, Lyp;->d(Lyt;)V

    .line 7554
    return-void
.end method

.method public u()I
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x3

    .line 8164
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "(%s=%d) AND (%s=%d) AND %s"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "transport_type"

    aput-object v4, v2, v3

    .line 8168
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    const/4 v3, 0x2

    const-string v4, "sms_type"

    aput-object v4, v2, v3

    .line 8170
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x4

    const-string v4, "attachment_content_type"

    .line 8171
    invoke-static {v4}, Lbvx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 8164
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 8172
    const-string v1, "Babel_db"

    invoke-static {v1, v5}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8173
    const-string v1, "Babel_db"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "deleteSmsMediaMessags: selection = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 8175
    :cond_0
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "messages"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public u(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 2658
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2662
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "UPDATE conversations SET chat_watermark=sort_timestamp, has_chat_notifications=0 WHERE sort_timestamp>chat_watermark"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 2669
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "UPDATE conversations SET chat_watermark=sort_timestamp, has_chat_notifications=0 WHERE sort_timestamp>chat_watermark AND conversation_id=?"

    invoke-virtual {v0, v1, p1}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public v(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 2680
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2684
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "UPDATE conversations SET hangout_watermark=sort_timestamp, has_video_notifications=0 WHERE sort_timestamp>hangout_watermark"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 2691
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "UPDATE conversations SET hangout_watermark=sort_timestamp, has_video_notifications=0 WHERE sort_timestamp>hangout_watermark AND conversation_id=?"

    invoke-virtual {v0, v1, p1}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public v()V
    .locals 4

    .prologue
    .line 8411
    const-string v0, "Babel_db"

    const-string v1, "removeSmsConversationsTransaction"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 8415
    invoke-virtual {p0}, Lyt;->a()V

    .line 8417
    :try_start_0
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "conversations"

    const-string v2, "transport_type=3"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 8418
    invoke-virtual {p0}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8420
    invoke-virtual {p0}, Lyt;->c()V

    .line 8421
    return-void

    .line 8420
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lyt;->c()V

    throw v0
.end method

.method public w(Ljava/lang/String;)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2715
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2716
    const-string v0, "wearable_watermark"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2718
    const-string v1, "wearable_watermark>0"

    .line 2719
    const/4 v0, 0x0

    .line 2720
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2721
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND conversation_id=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2722
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    aput-object p1, v0, v4

    .line 2724
    :cond_0
    iget-object v3, p0, Lyt;->b:Lzr;

    const-string v4, "conversations"

    invoke-virtual {v3, v4, v2, v1, v0}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public w()V
    .locals 4

    .prologue
    .line 8439
    invoke-virtual {p0}, Lyt;->a()V

    .line 8441
    :try_start_0
    sget-boolean v0, Lyt;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "deleteAllLowAffinityInvites: account="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lyt;->d:Lyj;

    invoke-virtual {v2}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "view=1 AND status=1 AND inviter_affinity=2"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "conversations"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_1

    sget-boolean v1, Lyt;->a:Z

    if-eqz v1, :cond_1

    const-string v1, "Babel_db"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "deleteAllLowAffinityInvites:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-static {p0}, Lyp;->d(Lyt;)V

    .line 8442
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lyt;->d(I)V

    .line 8444
    invoke-virtual {p0}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8446
    invoke-virtual {p0}, Lyt;->c()V

    .line 8447
    return-void

    .line 8446
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lyt;->c()V

    throw v0
.end method

.method public x(Ljava/lang/String;)Ljava/util/List;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbfx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3687
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 3690
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 3691
    const-string v2, "upload_key"

    move-object/from16 v0, p1

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3692
    move-object/from16 v0, p0

    iget-object v2, v0, Lyt;->b:Lzr;

    const-string v3, "transport_events"

    const-string v4, "upload_key ISNULL"

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3695
    const/4 v7, 0x0

    .line 3697
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lyt;->b:Lzr;

    const-string v2, "transport_events"

    sget-object v3, Lzn;->a:[Ljava/lang/String;

    const-string v4, "upload_key =? "

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v18

    .line 3701
    :goto_0
    :try_start_1
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 3702
    const/4 v1, 0x5

    move-object/from16 v0, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 3703
    const/4 v1, 0x0

    .line 3704
    move-object/from16 v0, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 3705
    const/4 v1, 0x1

    .line 3706
    move-object/from16 v0, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 3707
    const/4 v1, 0x4

    .line 3708
    move-object/from16 v0, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 3709
    const/4 v1, 0x2

    move-object/from16 v0, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 3710
    const/4 v1, 0x3

    move-object/from16 v0, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 3711
    const/16 v1, 0xd

    move-object/from16 v0, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 3713
    const/16 v1, 0xc

    .line 3714
    move-object/from16 v0, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 3715
    if-lez v16, :cond_5

    .line 3717
    const/4 v1, 0x6

    move-object/from16 v0, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 3718
    invoke-static {v1}, Lyt;->a(I)Z

    move-result v11

    .line 3719
    const/4 v1, 0x7

    .line 3720
    move-object/from16 v0, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-eqz v1, :cond_1

    const/4 v12, 0x1

    .line 3721
    :goto_1
    const/16 v1, 0x8

    .line 3722
    move-object/from16 v0, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-eqz v1, :cond_2

    const/4 v13, 0x1

    .line 3723
    :goto_2
    const/16 v1, 0x9

    move-object/from16 v0, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-eqz v1, :cond_3

    const/4 v14, 0x1

    .line 3724
    :goto_3
    const/16 v1, 0xa

    move-object/from16 v0, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-eqz v1, :cond_4

    const/4 v15, 0x1

    .line 3726
    :goto_4
    const/16 v1, 0xb

    .line 3727
    move-object/from16 v0, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    .line 3729
    new-instance v1, Lbfx;

    invoke-direct/range {v1 .. v17}, Lbfx;-><init>(JIILjava/lang/String;JJZZZZZII)V

    .line 3733
    move-object/from16 v0, v19

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 3742
    :catchall_0
    move-exception v1

    move-object/from16 v2, v18

    :goto_5
    if-eqz v2, :cond_0

    .line 3743
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v1

    .line 3720
    :cond_1
    const/4 v12, 0x0

    goto :goto_1

    .line 3722
    :cond_2
    const/4 v13, 0x0

    goto :goto_2

    .line 3723
    :cond_3
    const/4 v14, 0x0

    goto :goto_3

    .line 3724
    :cond_4
    const/4 v15, 0x0

    goto :goto_4

    .line 3736
    :cond_5
    :try_start_2
    new-instance v1, Lbfx;

    move/from16 v11, v17

    invoke-direct/range {v1 .. v11}, Lbfx;-><init>(JIILjava/lang/String;JJI)V

    .line 3738
    move-object/from16 v0, v19

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 3742
    :cond_6
    if-eqz v18, :cond_7

    .line 3743
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 3747
    :cond_7
    return-object v19

    .line 3742
    :catchall_1
    move-exception v1

    move-object v2, v7

    goto :goto_5
.end method

.method public y(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 3757
    iget-object v0, p0, Lyt;->b:Lzr;

    const-string v1, "transport_events"

    const-string v2, "upload_key=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lzr;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3760
    return-void
.end method

.method public z(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 3768
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 3769
    const-string v1, "upload_key"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 3770
    iget-object v1, p0, Lyt;->b:Lzr;

    const-string v2, "transport_events"

    const-string v3, "upload_key=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3773
    return-void
.end method
