.class public final Lbxq;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:[Ljava/lang/reflect/Field;

.field private b:Ljava/lang/Object;

.field private c:Lbxr;


# direct methods
.method public constructor <init>([Ljava/lang/reflect/Field;Ljava/lang/Object;Lbxr;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p2, p0, Lbxq;->b:Ljava/lang/Object;

    .line 33
    iput-object p3, p0, Lbxq;->c:Lbxr;

    .line 34
    invoke-virtual {p1}, [Ljava/lang/reflect/Field;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/reflect/Field;

    iput-object v0, p0, Lbxq;->a:[Ljava/lang/reflect/Field;

    .line 35
    return-void
.end method

.method private b()Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 49
    const/4 v0, 0x1

    .line 50
    iget-object v3, p0, Lbxq;->a:[Ljava/lang/reflect/Field;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 52
    :try_start_0
    iget-object v6, p0, Lbxq;->b:Ljava/lang/Object;

    invoke-virtual {v5, v6}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v5

    if-nez v5, :cond_0

    move v0, v1

    .line 50
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 55
    :catch_0
    move-exception v0

    .line 56
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "can\'t check field"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 57
    :catch_1
    move-exception v0

    .line 58
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "can\'t check field"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 61
    :cond_1
    return v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lbxq;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    iget-object v0, p0, Lbxq;->c:Lbxr;

    invoke-interface {v0}, Lbxr;->a()V

    .line 43
    :goto_0
    return-void

    .line 41
    :cond_0
    iget-object v0, p0, Lbxq;->c:Lbxr;

    goto :goto_0
.end method
