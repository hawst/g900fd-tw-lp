.class public final Ldvl;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldvl;


# instance fields
.field public b:Ldvn;

.field public c:Ldqa;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16738
    const/4 v0, 0x0

    new-array v0, v0, [Ldvl;

    sput-object v0, Ldvl;->a:[Ldvl;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 16739
    invoke-direct {p0}, Lepn;-><init>()V

    .line 16742
    iput-object v0, p0, Ldvl;->b:Ldvn;

    .line 16745
    iput-object v0, p0, Ldvl;->c:Ldqa;

    .line 16739
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 16762
    const/4 v0, 0x0

    .line 16763
    iget-object v1, p0, Ldvl;->b:Ldvn;

    if-eqz v1, :cond_0

    .line 16764
    const/4 v0, 0x1

    iget-object v1, p0, Ldvl;->b:Ldvn;

    .line 16765
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 16767
    :cond_0
    iget-object v1, p0, Ldvl;->c:Ldqa;

    if-eqz v1, :cond_1

    .line 16768
    const/4 v1, 0x2

    iget-object v2, p0, Ldvl;->c:Ldqa;

    .line 16769
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16771
    :cond_1
    iget-object v1, p0, Ldvl;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16772
    iput v0, p0, Ldvl;->cachedSize:I

    .line 16773
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 16735
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldvl;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldvl;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldvl;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldvl;->b:Ldvn;

    if-nez v0, :cond_2

    new-instance v0, Ldvn;

    invoke-direct {v0}, Ldvn;-><init>()V

    iput-object v0, p0, Ldvl;->b:Ldvn;

    :cond_2
    iget-object v0, p0, Ldvl;->b:Ldvn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldvl;->c:Ldqa;

    if-nez v0, :cond_3

    new-instance v0, Ldqa;

    invoke-direct {v0}, Ldqa;-><init>()V

    iput-object v0, p0, Ldvl;->c:Ldqa;

    :cond_3
    iget-object v0, p0, Ldvl;->c:Ldqa;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 16750
    iget-object v0, p0, Ldvl;->b:Ldvn;

    if-eqz v0, :cond_0

    .line 16751
    const/4 v0, 0x1

    iget-object v1, p0, Ldvl;->b:Ldvn;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 16753
    :cond_0
    iget-object v0, p0, Ldvl;->c:Ldqa;

    if-eqz v0, :cond_1

    .line 16754
    const/4 v0, 0x2

    iget-object v1, p0, Ldvl;->c:Ldqa;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 16756
    :cond_1
    iget-object v0, p0, Ldvl;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 16758
    return-void
.end method
