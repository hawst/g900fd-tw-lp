.class public final Leti;
.super Lcnx;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcnx",
        "<",
        "Leti;",
        ">;"
    }
.end annotation


# instance fields
.field public c:J

.field public d:Ljava/lang/String;

.field public e:I

.field public f:I

.field public g:Z

.field public h:[Letj;

.field public i:Leth;

.field public j:[B

.field public k:[B

.field public l:[B

.field public m:Letg;

.field public n:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Lcnx;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Leti;->c:J

    const-string v0, ""

    iput-object v0, p0, Leti;->d:Ljava/lang/String;

    iput v2, p0, Leti;->e:I

    iput v2, p0, Leti;->f:I

    iput-boolean v2, p0, Leti;->g:Z

    invoke-static {}, Letj;->e()[Letj;

    move-result-object v0

    iput-object v0, p0, Leti;->h:[Letj;

    iput-object v3, p0, Leti;->i:Leth;

    sget-object v0, Lcod;->h:[B

    iput-object v0, p0, Leti;->j:[B

    sget-object v0, Lcod;->h:[B

    iput-object v0, p0, Leti;->k:[B

    sget-object v0, Lcod;->h:[B

    iput-object v0, p0, Leti;->l:[B

    iput-object v3, p0, Leti;->m:Letg;

    const-string v0, ""

    iput-object v0, p0, Leti;->n:Ljava/lang/String;

    iput-object v3, p0, Leti;->a:Lcny;

    const/4 v0, -0x1

    iput v0, p0, Leti;->b:I

    return-void
.end method


# virtual methods
.method protected a()I
    .locals 13

    const/4 v3, 0x6

    const/4 v2, 0x3

    const/4 v1, 0x2

    const/4 v0, 0x1

    const-wide/16 v10, 0x0

    invoke-super {p0}, Lcnx;->a()I

    move-result v4

    iget-wide v5, p0, Leti;->c:J

    cmp-long v5, v5, v10

    if-eqz v5, :cond_16

    iget-wide v5, p0, Leti;->c:J

    invoke-static {v0}, Lcnw;->b(I)I

    move-result v7

    const-wide/16 v8, -0x80

    and-long/2addr v8, v5

    cmp-long v8, v8, v10

    if-nez v8, :cond_2

    :goto_0
    add-int/2addr v0, v7

    add-int/2addr v0, v4

    :goto_1
    iget-object v4, p0, Leti;->d:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Leti;->d:Ljava/lang/String;

    invoke-static {v1, v4}, Lcnw;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Leti;->h:[Letj;

    if-eqz v1, :cond_c

    iget-object v1, p0, Leti;->h:[Letj;

    array-length v1, v1

    if-lez v1, :cond_c

    const/4 v1, 0x0

    move v12, v1

    move v1, v0

    move v0, v12

    :goto_2
    iget-object v4, p0, Leti;->h:[Letj;

    array-length v4, v4

    if-ge v0, v4, :cond_b

    iget-object v4, p0, Leti;->h:[Letj;

    aget-object v4, v4, v0

    if-eqz v4, :cond_1

    invoke-static {v2, v4}, Lcnw;->c(ILcob;)I

    move-result v4

    add-int/2addr v1, v4

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    const-wide/16 v8, -0x4000

    and-long/2addr v8, v5

    cmp-long v0, v8, v10

    if-nez v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    const-wide/32 v8, -0x200000

    and-long/2addr v8, v5

    cmp-long v0, v8, v10

    if-nez v0, :cond_4

    move v0, v2

    goto :goto_0

    :cond_4
    const-wide/32 v8, -0x10000000

    and-long/2addr v8, v5

    cmp-long v0, v8, v10

    if-nez v0, :cond_5

    const/4 v0, 0x4

    goto :goto_0

    :cond_5
    const-wide v8, -0x800000000L

    and-long/2addr v8, v5

    cmp-long v0, v8, v10

    if-nez v0, :cond_6

    const/4 v0, 0x5

    goto :goto_0

    :cond_6
    const-wide v8, -0x40000000000L

    and-long/2addr v8, v5

    cmp-long v0, v8, v10

    if-nez v0, :cond_7

    move v0, v3

    goto :goto_0

    :cond_7
    const-wide/high16 v8, -0x2000000000000L

    and-long/2addr v8, v5

    cmp-long v0, v8, v10

    if-nez v0, :cond_8

    const/4 v0, 0x7

    goto :goto_0

    :cond_8
    const-wide/high16 v8, -0x100000000000000L

    and-long/2addr v8, v5

    cmp-long v0, v8, v10

    if-nez v0, :cond_9

    const/16 v0, 0x8

    goto :goto_0

    :cond_9
    const-wide/high16 v8, -0x8000000000000000L

    and-long/2addr v5, v8

    cmp-long v0, v5, v10

    if-nez v0, :cond_a

    const/16 v0, 0x9

    goto/16 :goto_0

    :cond_a
    const/16 v0, 0xa

    goto/16 :goto_0

    :cond_b
    move v0, v1

    :cond_c
    iget-object v1, p0, Leti;->j:[B

    sget-object v2, Lcod;->h:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_d

    iget-object v1, p0, Leti;->j:[B

    invoke-static {v3, v1}, Lcnw;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    iget-object v1, p0, Leti;->m:Letg;

    if-eqz v1, :cond_e

    const/4 v1, 0x7

    iget-object v2, p0, Leti;->m:Letg;

    invoke-static {v1, v2}, Lcnw;->c(ILcob;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_e
    iget-object v1, p0, Leti;->k:[B

    sget-object v2, Lcod;->h:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_f

    const/16 v1, 0x8

    iget-object v2, p0, Leti;->k:[B

    invoke-static {v1, v2}, Lcnw;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    :cond_f
    iget-object v1, p0, Leti;->i:Leth;

    if-eqz v1, :cond_10

    const/16 v1, 0x9

    iget-object v2, p0, Leti;->i:Leth;

    invoke-static {v1, v2}, Lcnw;->c(ILcob;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_10
    iget-boolean v1, p0, Leti;->g:Z

    if-eqz v1, :cond_11

    iget-boolean v1, p0, Leti;->g:Z

    const/16 v1, 0xa

    invoke-static {v1}, Lcnw;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_11
    iget v1, p0, Leti;->e:I

    if-eqz v1, :cond_12

    const/16 v1, 0xb

    iget v2, p0, Leti;->e:I

    invoke-static {v1, v2}, Lcnw;->b(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_12
    iget v1, p0, Leti;->f:I

    if-eqz v1, :cond_13

    const/16 v1, 0xc

    iget v2, p0, Leti;->f:I

    invoke-static {v1, v2}, Lcnw;->b(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_13
    iget-object v1, p0, Leti;->l:[B

    sget-object v2, Lcod;->h:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_14

    const/16 v1, 0xd

    iget-object v2, p0, Leti;->l:[B

    invoke-static {v1, v2}, Lcnw;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    :cond_14
    iget-object v1, p0, Leti;->n:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_15

    const/16 v1, 0xe

    iget-object v2, p0, Leti;->n:Ljava/lang/String;

    invoke-static {v1, v2}, Lcnw;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_15
    return v0

    :cond_16
    move v0, v4

    goto/16 :goto_1
.end method

.method public a(Lcnw;)V
    .locals 4

    iget-wide v0, p0, Leti;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Leti;->c:J

    invoke-virtual {p1, v0, v1}, Lcnw;->a(J)V

    :cond_0
    iget-object v0, p0, Leti;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Leti;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcnw;->a(ILjava/lang/String;)V

    :cond_1
    iget-object v0, p0, Leti;->h:[Letj;

    if-eqz v0, :cond_3

    iget-object v0, p0, Leti;->h:[Letj;

    array-length v0, v0

    if-lez v0, :cond_3

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Leti;->h:[Letj;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Leti;->h:[Letj;

    aget-object v1, v1, v0

    if-eqz v1, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Lcnw;->a(ILcob;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Leti;->j:[B

    sget-object v1, Lcod;->h:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x6

    iget-object v1, p0, Leti;->j:[B

    invoke-virtual {p1, v0, v1}, Lcnw;->a(I[B)V

    :cond_4
    iget-object v0, p0, Leti;->m:Letg;

    if-eqz v0, :cond_5

    const/4 v0, 0x7

    iget-object v1, p0, Leti;->m:Letg;

    invoke-virtual {p1, v0, v1}, Lcnw;->a(ILcob;)V

    :cond_5
    iget-object v0, p0, Leti;->k:[B

    sget-object v1, Lcod;->h:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_6

    const/16 v0, 0x8

    iget-object v1, p0, Leti;->k:[B

    invoke-virtual {p1, v0, v1}, Lcnw;->a(I[B)V

    :cond_6
    iget-object v0, p0, Leti;->i:Leth;

    if-eqz v0, :cond_7

    const/16 v0, 0x9

    iget-object v1, p0, Leti;->i:Leth;

    invoke-virtual {p1, v0, v1}, Lcnw;->a(ILcob;)V

    :cond_7
    iget-boolean v0, p0, Leti;->g:Z

    if-eqz v0, :cond_8

    iget-boolean v0, p0, Leti;->g:Z

    invoke-virtual {p1, v0}, Lcnw;->a(Z)V

    :cond_8
    iget v0, p0, Leti;->e:I

    if-eqz v0, :cond_9

    const/16 v0, 0xb

    iget v1, p0, Leti;->e:I

    invoke-virtual {p1, v0, v1}, Lcnw;->a(II)V

    :cond_9
    iget v0, p0, Leti;->f:I

    if-eqz v0, :cond_a

    const/16 v0, 0xc

    iget v1, p0, Leti;->f:I

    invoke-virtual {p1, v0, v1}, Lcnw;->a(II)V

    :cond_a
    iget-object v0, p0, Leti;->l:[B

    sget-object v1, Lcod;->h:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_b

    const/16 v0, 0xd

    iget-object v1, p0, Leti;->l:[B

    invoke-virtual {p1, v0, v1}, Lcnw;->a(I[B)V

    :cond_b
    iget-object v0, p0, Leti;->n:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    const/16 v0, 0xe

    iget-object v1, p0, Leti;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcnw;->a(ILjava/lang/String;)V

    :cond_c
    invoke-super {p0, p1}, Lcnx;->a(Lcnw;)V

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x0

    if-ne p1, p0, :cond_1

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v1, p1, Leti;

    if-eqz v1, :cond_0

    check-cast p1, Leti;

    iget-wide v1, p0, Leti;->c:J

    iget-wide v3, p1, Leti;->c:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    iget-object v1, p0, Leti;->d:Ljava/lang/String;

    if-nez v1, :cond_6

    iget-object v1, p1, Leti;->d:Ljava/lang/String;

    if-nez v1, :cond_0

    :cond_2
    iget v1, p0, Leti;->e:I

    iget v2, p1, Leti;->e:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Leti;->f:I

    iget v2, p1, Leti;->f:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Leti;->g:Z

    iget-boolean v2, p1, Leti;->g:Z

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Leti;->h:[Letj;

    iget-object v2, p1, Leti;->h:[Letj;

    invoke-static {v1, v2}, Lcoa;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Leti;->i:Leth;

    if-nez v1, :cond_7

    iget-object v1, p1, Leti;->i:Leth;

    if-nez v1, :cond_0

    :cond_3
    iget-object v1, p0, Leti;->j:[B

    iget-object v2, p1, Leti;->j:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Leti;->k:[B

    iget-object v2, p1, Leti;->k:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Leti;->l:[B

    iget-object v2, p1, Leti;->l:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Leti;->m:Letg;

    if-nez v1, :cond_8

    iget-object v1, p1, Leti;->m:Letg;

    if-nez v1, :cond_0

    :cond_4
    iget-object v1, p0, Leti;->n:Ljava/lang/String;

    if-nez v1, :cond_9

    iget-object v1, p1, Leti;->n:Ljava/lang/String;

    if-nez v1, :cond_0

    :cond_5
    invoke-virtual {p0, p1}, Leti;->a(Lcnx;)Z

    move-result v0

    goto :goto_0

    :cond_6
    iget-object v1, p0, Leti;->d:Ljava/lang/String;

    iget-object v2, p1, Leti;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    :cond_7
    iget-object v1, p0, Leti;->i:Leth;

    iget-object v2, p1, Leti;->i:Leth;

    invoke-virtual {v1, v2}, Leth;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto/16 :goto_0

    :cond_8
    iget-object v1, p0, Leti;->m:Letg;

    iget-object v2, p1, Leti;->m:Letg;

    invoke-virtual {v1, v2}, Letg;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto/16 :goto_0

    :cond_9
    iget-object v1, p0, Leti;->n:Ljava/lang/String;

    iget-object v2, p1, Leti;->n:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 6

    const/4 v1, 0x0

    iget-wide v2, p0, Leti;->c:J

    iget-wide v4, p0, Leti;->c:J

    const/16 v0, 0x20

    ushr-long/2addr v4, v0

    xor-long/2addr v2, v4

    long-to-int v0, v2

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Leti;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Leti;->e:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Leti;->f:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Leti;->g:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x4cf

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Leti;->h:[Letj;

    invoke-static {v2}, Lcoa;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Leti;->i:Leth;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Leti;->j:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Leti;->k:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Leti;->l:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Leti;->m:Letg;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Leti;->n:Ljava/lang/String;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Leti;->b()I

    move-result v1

    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Leti;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    const/16 v0, 0x4d5

    goto :goto_1

    :cond_2
    iget-object v0, p0, Leti;->i:Leth;

    invoke-virtual {v0}, Leth;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_3
    iget-object v0, p0, Leti;->m:Letg;

    invoke-virtual {v0}, Letg;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_4
    iget-object v1, p0, Leti;->n:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4
.end method
