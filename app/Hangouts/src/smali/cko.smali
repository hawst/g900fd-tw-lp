.class public Lcko;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lckk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lckk",
            "<",
            "Lcka;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/google/android/gms/internal/nt;

.field private final d:Ljava/util/Locale;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcko;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcko;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lckk;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lckk",
            "<",
            "Lcka;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcko;->b:Lckk;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, Lcko;->d:Ljava/util/Locale;

    new-instance v0, Lcom/google/android/gms/internal/nt;

    iget-object v1, p0, Lcko;->d:Ljava/util/Locale;

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/internal/nt;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcko;->c:Lcom/google/android/gms/internal/nt;

    return-void
.end method
