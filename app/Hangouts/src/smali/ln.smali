.class public final Lln;
.super Llv;
.source "PG"

# interfaces
.implements Lfs;


# instance fields
.field a:Llr;

.field b:Llo;

.field c:Llp;

.field final d:Lls;

.field e:I

.field private l:Landroid/view/View;

.field private m:Z

.field private n:Z

.field private o:I

.field private p:I

.field private q:I

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:I

.field private final w:Landroid/util/SparseBooleanArray;

.field private x:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 76
    sget v0, Lf;->af:I

    sget v1, Lf;->ae:I

    invoke-direct {p0, p1, v0, v1}, Llv;-><init>(Landroid/content/Context;II)V

    .line 63
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, Lln;->w:Landroid/util/SparseBooleanArray;

    .line 72
    new-instance v0, Lls;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lls;-><init>(Lln;B)V

    iput-object v0, p0, Lln;->d:Lls;

    .line 77
    return-void
.end method


# virtual methods
.method public a(Lme;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 158
    invoke-virtual {p1}, Lme;->getActionView()Landroid/view/View;

    move-result-object v0

    .line 159
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lme;->m()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 160
    :cond_0
    instance-of v0, p2, Landroid/support/v7/internal/view/menu/ActionMenuItemView;

    if-nez v0, :cond_1

    .line 161
    const/4 p2, 0x0

    .line 163
    :cond_1
    invoke-super {p0, p1, p2, p3}, Llv;->a(Lme;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 165
    :cond_2
    invoke-virtual {p1}, Lme;->isActionViewExpanded()Z

    move-result v1

    if-eqz v1, :cond_4

    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 167
    check-cast p3, Landroid/support/v7/internal/view/menu/ActionMenuView;

    .line 168
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 169
    invoke-virtual {p3, v1}, Landroid/support/v7/internal/view/menu/ActionMenuView;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 170
    invoke-virtual {p3, v1}, Landroid/support/v7/internal/view/menu/ActionMenuView;->a(Landroid/view/ViewGroup$LayoutParams;)Llu;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 172
    :cond_3
    return-object v0

    .line 165
    :cond_4
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/ViewGroup;)Lmr;
    .locals 2

    .prologue
    .line 151
    invoke-super {p0, p1}, Llv;->a(Landroid/view/ViewGroup;)Lmr;

    move-result-object v1

    move-object v0, v1

    .line 152
    check-cast v0, Landroid/support/v7/internal/view/menu/ActionMenuView;

    invoke-virtual {v0, p0}, Landroid/support/v7/internal/view/menu/ActionMenuView;->a(Lln;)V

    .line 153
    return-object v1
.end method

.method public a()V
    .locals 2

    .prologue
    .line 120
    iget-boolean v0, p0, Lln;->r:Z

    if-nez v0, :cond_0

    .line 121
    iget-object v0, p0, Lln;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lf;->W:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lln;->q:I

    .line 124
    :cond_0
    iget-object v0, p0, Lln;->h:Lma;

    if-eqz v0, :cond_1

    .line 125
    iget-object v0, p0, Lln;->h:Lma;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lma;->b(Z)V

    .line 127
    :cond_1
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 130
    iput p1, p0, Lln;->o:I

    .line 131
    iput-boolean v0, p0, Lln;->s:Z

    .line 132
    iput-boolean v0, p0, Lln;->t:Z

    .line 133
    return-void
.end method

.method public a(Landroid/content/Context;Lma;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 81
    invoke-super {p0, p1, p2}, Llv;->a(Landroid/content/Context;Lma;)V

    .line 83
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 85
    invoke-static {p1}, Llf;->a(Landroid/content/Context;)Llf;

    move-result-object v3

    .line 86
    iget-boolean v0, p0, Lln;->n:Z

    if-nez v0, :cond_0

    .line 87
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-lt v0, v4, :cond_4

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lln;->m:Z

    .line 90
    :cond_0
    iget-boolean v0, p0, Lln;->t:Z

    if-nez v0, :cond_1

    .line 91
    invoke-virtual {v3}, Llf;->b()I

    move-result v0

    iput v0, p0, Lln;->o:I

    .line 95
    :cond_1
    iget-boolean v0, p0, Lln;->r:Z

    if-nez v0, :cond_2

    .line 96
    invoke-virtual {v3}, Llf;->a()I

    move-result v0

    iput v0, p0, Lln;->q:I

    .line 99
    :cond_2
    iget v0, p0, Lln;->o:I

    .line 100
    iget-boolean v3, p0, Lln;->m:Z

    if-eqz v3, :cond_5

    .line 101
    iget-object v3, p0, Lln;->l:Landroid/view/View;

    if-nez v3, :cond_3

    .line 102
    new-instance v3, Llq;

    iget-object v4, p0, Lln;->f:Landroid/content/Context;

    invoke-direct {v3, p0, v4}, Llq;-><init>(Lln;Landroid/content/Context;)V

    iput-object v3, p0, Lln;->l:Landroid/view/View;

    .line 103
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 104
    iget-object v3, p0, Lln;->l:Landroid/view/View;

    invoke-virtual {v3, v1, v1}, Landroid/view/View;->measure(II)V

    .line 106
    :cond_3
    iget-object v1, p0, Lln;->l:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr v0, v1

    .line 111
    :goto_1
    iput v0, p0, Lln;->p:I

    .line 113
    const/high16 v0, 0x42600000    # 56.0f

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lln;->v:I

    .line 116
    iput-object v5, p0, Lln;->x:Landroid/view/View;

    .line 117
    return-void

    :cond_4
    move v0, v1

    .line 87
    goto :goto_0

    .line 108
    :cond_5
    iput-object v5, p0, Lln;->l:Landroid/view/View;

    goto :goto_1
.end method

.method public a(Lma;Z)V
    .locals 0

    .prologue
    .line 506
    invoke-virtual {p0}, Lln;->e()Z

    .line 507
    invoke-super {p0, p1, p2}, Llv;->a(Lma;Z)V

    .line 508
    return-void
.end method

.method public a(Lme;Lms;)V
    .locals 1

    .prologue
    .line 177
    invoke-interface {p2, p1}, Lms;->a(Lme;)V

    .line 179
    iget-object v0, p0, Lln;->k:Lmr;

    check-cast v0, Landroid/support/v7/internal/view/menu/ActionMenuView;

    .line 180
    check-cast p2, Landroid/support/v7/internal/view/menu/ActionMenuItemView;

    .line 181
    invoke-virtual {p2, v0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->a(Lmc;)V

    .line 182
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 531
    if-eqz p1, :cond_0

    .line 533
    const/4 v0, 0x0

    invoke-super {p0, v0}, Llv;->a(Lmu;)Z

    .line 537
    :goto_0
    return-void

    .line 535
    :cond_0
    iget-object v0, p0, Lln;->h:Lma;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lma;->a(Z)V

    goto :goto_0
.end method

.method public a(Landroid/view/ViewGroup;I)Z
    .locals 2

    .prologue
    .line 242
    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lln;->l:Landroid/view/View;

    if-ne v0, v1, :cond_0

    .line 243
    const/4 v0, 0x0

    .line 245
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Llv;->a(Landroid/view/ViewGroup;I)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Lme;)Z
    .locals 1

    .prologue
    .line 186
    invoke-virtual {p1}, Lme;->h()Z

    move-result v0

    return v0
.end method

.method public a(Lmu;)Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 249
    invoke-virtual {p1}, Lmu;->hasVisibleItems()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v3

    .line 269
    :goto_0
    return v0

    :cond_0
    move-object v0, p1

    .line 254
    :goto_1
    invoke-virtual {v0}, Lmu;->u()Landroid/view/Menu;

    move-result-object v1

    iget-object v2, p0, Lln;->h:Lma;

    if-eq v1, v2, :cond_1

    .line 255
    invoke-virtual {v0}, Lmu;->u()Landroid/view/Menu;

    move-result-object v0

    check-cast v0, Lmu;

    goto :goto_1

    .line 257
    :cond_1
    invoke-virtual {v0}, Lmu;->getItem()Landroid/view/MenuItem;

    move-result-object v5

    iget-object v0, p0, Lln;->k:Lmr;

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v6

    move v4, v3

    :goto_2
    if-ge v4, v6, :cond_3

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    instance-of v1, v2, Lms;

    if-eqz v1, :cond_2

    move-object v1, v2

    check-cast v1, Lms;

    invoke-interface {v1}, Lms;->a()Lme;

    move-result-object v1

    if-ne v1, v5, :cond_2

    .line 258
    :goto_3
    if-nez v2, :cond_5

    .line 259
    iget-object v0, p0, Lln;->l:Landroid/view/View;

    if-nez v0, :cond_4

    move v0, v3

    .line 260
    goto :goto_0

    .line 257
    :cond_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    goto :goto_3

    .line 262
    :cond_4
    iget-object v0, p0, Lln;->l:Landroid/view/View;

    .line 265
    :cond_5
    invoke-virtual {p1}, Lmu;->getItem()Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    iput v0, p0, Lln;->e:I

    .line 266
    new-instance v0, Llo;

    invoke-direct {v0, p0, p1}, Llo;-><init>(Lln;Lmu;)V

    iput-object v0, p0, Lln;->b:Llo;

    .line 267
    iget-object v0, p0, Lln;->b:Llo;

    invoke-virtual {v0}, Llo;->a()V

    .line 268
    invoke-super {p0, p1}, Llv;->a(Lmu;)Z

    .line 269
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 141
    const v0, 0x7fffffff

    iput v0, p0, Lln;->q:I

    .line 142
    const/4 v0, 0x1

    iput-boolean v0, p0, Lln;->r:Z

    .line 143
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 146
    iput-boolean p1, p0, Lln;->u:Z

    .line 147
    return-void
.end method

.method public c(Z)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 191
    invoke-super {p0, p1}, Llv;->c(Z)V

    .line 193
    iget-object v0, p0, Lln;->k:Lmr;

    if-nez v0, :cond_0

    .line 238
    :goto_0
    return-void

    .line 197
    :cond_0
    iget-object v0, p0, Lln;->h:Lma;

    if-eqz v0, :cond_2

    .line 198
    iget-object v0, p0, Lln;->h:Lma;

    invoke-virtual {v0}, Lma;->m()Ljava/util/ArrayList;

    move-result-object v4

    .line 199
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v2

    .line 200
    :goto_1
    if-ge v3, v5, :cond_2

    .line 201
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lme;

    invoke-virtual {v0}, Lme;->l()Lfr;

    move-result-object v0

    .line 202
    if-eqz v0, :cond_1

    .line 203
    invoke-virtual {v0, p0}, Lfr;->a(Lfs;)V

    .line 200
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 208
    :cond_2
    iget-object v0, p0, Lln;->h:Lma;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lln;->h:Lma;

    invoke-virtual {v0}, Lma;->n()Ljava/util/ArrayList;

    move-result-object v0

    .line 212
    :goto_2
    iget-boolean v3, p0, Lln;->m:Z

    if-eqz v3, :cond_3

    if-eqz v0, :cond_3

    .line 213
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 214
    if-ne v3, v1, :cond_9

    .line 215
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lme;

    invoke-virtual {v0}, Lme;->isActionViewExpanded()Z

    move-result v0

    if-nez v0, :cond_8

    move v0, v1

    :goto_3
    move v2, v0

    .line 221
    :cond_3
    :goto_4
    if-eqz v2, :cond_b

    .line 222
    iget-object v0, p0, Lln;->l:Landroid/view/View;

    if-nez v0, :cond_4

    .line 223
    new-instance v0, Llq;

    iget-object v1, p0, Lln;->f:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Llq;-><init>(Lln;Landroid/content/Context;)V

    iput-object v0, p0, Lln;->l:Landroid/view/View;

    .line 225
    :cond_4
    iget-object v0, p0, Lln;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 226
    iget-object v1, p0, Lln;->k:Lmr;

    if-eq v0, v1, :cond_6

    .line 227
    if-eqz v0, :cond_5

    .line 228
    iget-object v1, p0, Lln;->l:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 230
    :cond_5
    iget-object v0, p0, Lln;->k:Lmr;

    check-cast v0, Landroid/support/v7/internal/view/menu/ActionMenuView;

    .line 231
    iget-object v1, p0, Lln;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/ActionMenuView;->b()Llu;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/internal/view/menu/ActionMenuView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 237
    :cond_6
    :goto_5
    iget-object v0, p0, Lln;->k:Lmr;

    check-cast v0, Landroid/support/v7/internal/view/menu/ActionMenuView;

    iget-boolean v1, p0, Lln;->m:Z

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/ActionMenuView;->a(Z)V

    goto/16 :goto_0

    .line 208
    :cond_7
    const/4 v0, 0x0

    goto :goto_2

    :cond_8
    move v0, v2

    .line 215
    goto :goto_3

    .line 217
    :cond_9
    if-lez v3, :cond_a

    :goto_6
    move v2, v1

    goto :goto_4

    :cond_a
    move v1, v2

    goto :goto_6

    .line 233
    :cond_b
    iget-object v0, p0, Lln;->l:Landroid/view/View;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lln;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Lln;->k:Lmr;

    if-ne v0, v1, :cond_6

    .line 234
    iget-object v0, p0, Lln;->k:Lmr;

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lln;->l:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_5
.end method

.method public c()Z
    .locals 4

    .prologue
    .line 295
    iget-boolean v0, p0, Lln;->m:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lln;->g()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lln;->h:Lma;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lln;->k:Lmr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lln;->c:Llp;

    if-nez v0, :cond_0

    .line 297
    new-instance v0, Llr;

    iget-object v1, p0, Lln;->g:Landroid/content/Context;

    iget-object v2, p0, Lln;->h:Lma;

    iget-object v3, p0, Lln;->l:Landroid/view/View;

    invoke-direct {v0, p0, v1, v2, v3}, Llr;-><init>(Lln;Landroid/content/Context;Lma;Landroid/view/View;)V

    .line 298
    new-instance v1, Llp;

    invoke-direct {v1, p0, v0}, Llp;-><init>(Lln;Llr;)V

    iput-object v1, p0, Lln;->c:Llp;

    .line 300
    iget-object v0, p0, Lln;->k:Lmr;

    check-cast v0, Landroid/view/View;

    iget-object v1, p0, Lln;->c:Llp;

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 304
    const/4 v0, 0x0

    invoke-super {p0, v0}, Llv;->a(Lmu;)Z

    .line 306
    const/4 v0, 0x1

    .line 308
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 317
    iget-object v0, p0, Lln;->c:Llp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lln;->k:Lmr;

    if-eqz v0, :cond_0

    .line 318
    iget-object v0, p0, Lln;->k:Lmr;

    check-cast v0, Landroid/view/View;

    iget-object v2, p0, Lln;->c:Llp;

    invoke-virtual {v0, v2}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 319
    const/4 v0, 0x0

    iput-object v0, p0, Lln;->c:Llp;

    move v0, v1

    .line 328
    :goto_0
    return v0

    .line 323
    :cond_0
    iget-object v0, p0, Lln;->a:Llr;

    .line 324
    if-eqz v0, :cond_1

    .line 325
    invoke-virtual {v0}, Lmn;->c()V

    move v0, v1

    .line 326
    goto :goto_0

    .line 328
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 337
    invoke-virtual {p0}, Lln;->d()Z

    move-result v0

    .line 338
    invoke-virtual {p0}, Lln;->f()Z

    move-result v1

    or-int/2addr v0, v1

    .line 339
    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lln;->b:Llo;

    if-eqz v0, :cond_0

    .line 349
    iget-object v0, p0, Lln;->b:Llo;

    invoke-virtual {v0}, Llo;->b()V

    .line 350
    const/4 v0, 0x1

    .line 352
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lln;->a:Llr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lln;->a:Llr;

    invoke-virtual {v0}, Llr;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 366
    iget-boolean v0, p0, Lln;->m:Z

    return v0
.end method

.method public i()Z
    .locals 21

    .prologue
    .line 370
    move-object/from16 v0, p0

    iget-object v2, v0, Lln;->h:Lma;

    invoke-virtual {v2}, Lma;->k()Ljava/util/ArrayList;

    move-result-object v13

    .line 371
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v14

    .line 372
    move-object/from16 v0, p0

    iget v7, v0, Lln;->q:I

    .line 373
    move-object/from16 v0, p0

    iget v9, v0, Lln;->p:I

    .line 374
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    .line 375
    move-object/from16 v0, p0

    iget-object v2, v0, Lln;->k:Lmr;

    check-cast v2, Landroid/view/ViewGroup;

    .line 377
    const/4 v6, 0x0

    .line 378
    const/4 v5, 0x0

    .line 379
    const/4 v8, 0x0

    .line 380
    const/4 v4, 0x0

    .line 381
    const/4 v3, 0x0

    move v10, v3

    :goto_0
    if-ge v10, v14, :cond_2

    .line 382
    invoke-virtual {v13, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lme;

    .line 383
    invoke-virtual {v3}, Lme;->j()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 384
    add-int/lit8 v6, v6, 0x1

    .line 390
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lln;->u:Z

    if-eqz v11, :cond_1e

    invoke-virtual {v3}, Lme;->isActionViewExpanded()Z

    move-result v3

    if-eqz v3, :cond_1e

    .line 393
    const/4 v3, 0x0

    .line 381
    :goto_2
    add-int/lit8 v7, v10, 0x1

    move v10, v7

    move v7, v3

    goto :goto_0

    .line 385
    :cond_0
    invoke-virtual {v3}, Lme;->i()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 386
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 388
    :cond_1
    const/4 v4, 0x1

    goto :goto_1

    .line 398
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lln;->m:Z

    if-eqz v3, :cond_4

    if-nez v4, :cond_3

    add-int v3, v6, v5

    if-le v3, v7, :cond_4

    .line 400
    :cond_3
    add-int/lit8 v7, v7, -0x1

    .line 402
    :cond_4
    sub-int v10, v7, v6

    .line 404
    move-object/from16 v0, p0

    iget-object v0, v0, Lln;->w:Landroid/util/SparseBooleanArray;

    move-object/from16 v16, v0

    .line 405
    invoke-virtual/range {v16 .. v16}, Landroid/util/SparseBooleanArray;->clear()V

    .line 407
    const/4 v4, 0x0

    .line 408
    const/4 v3, 0x0

    .line 409
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lln;->s:Z

    if-eqz v5, :cond_1d

    .line 410
    move-object/from16 v0, p0

    iget v3, v0, Lln;->v:I

    div-int v3, v9, v3

    .line 411
    move-object/from16 v0, p0

    iget v4, v0, Lln;->v:I

    rem-int v4, v9, v4

    .line 412
    move-object/from16 v0, p0

    iget v5, v0, Lln;->v:I

    div-int/2addr v4, v3

    add-int/2addr v4, v5

    move v5, v4

    .line 416
    :goto_3
    const/4 v4, 0x0

    move v12, v4

    move v7, v8

    move v4, v3

    :goto_4
    if-ge v12, v14, :cond_16

    .line 417
    invoke-virtual {v13, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lme;

    .line 419
    invoke-virtual {v3}, Lme;->j()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 420
    move-object/from16 v0, p0

    iget-object v6, v0, Lln;->x:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v6, v2}, Lln;->a(Lme;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 421
    move-object/from16 v0, p0

    iget-object v8, v0, Lln;->x:Landroid/view/View;

    if-nez v8, :cond_5

    .line 422
    move-object/from16 v0, p0

    iput-object v6, v0, Lln;->x:Landroid/view/View;

    .line 424
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v8, v0, Lln;->s:Z

    if-eqz v8, :cond_7

    .line 425
    const/4 v8, 0x0

    invoke-static {v6, v5, v4, v15, v8}, Landroid/support/v7/internal/view/menu/ActionMenuView;->a(Landroid/view/View;IIII)I

    move-result v8

    sub-int/2addr v4, v8

    .line 430
    :goto_5
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    .line 431
    sub-int v8, v9, v6

    .line 432
    if-nez v7, :cond_1c

    .line 435
    :goto_6
    invoke-virtual {v3}, Lme;->getGroupId()I

    move-result v7

    .line 436
    if-eqz v7, :cond_6

    .line 437
    const/4 v9, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v7, v9}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 439
    :cond_6
    const/4 v7, 0x1

    invoke-virtual {v3, v7}, Lme;->d(Z)V

    move v3, v8

    move v7, v10

    .line 416
    :goto_7
    add-int/lit8 v8, v12, 0x1

    move v12, v8

    move v9, v3

    move v10, v7

    move v7, v6

    goto :goto_4

    .line 428
    :cond_7
    invoke-virtual {v6, v15, v15}, Landroid/view/View;->measure(II)V

    goto :goto_5

    .line 440
    :cond_8
    invoke-virtual {v3}, Lme;->i()Z

    move-result v6

    if-eqz v6, :cond_1b

    .line 443
    invoke-virtual {v3}, Lme;->getGroupId()I

    move-result v17

    .line 444
    invoke-virtual/range {v16 .. v17}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v18

    .line 445
    if-gtz v10, :cond_9

    if-eqz v18, :cond_e

    :cond_9
    if-lez v9, :cond_e

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lln;->s:Z

    if-eqz v6, :cond_a

    if-lez v4, :cond_e

    :cond_a
    const/4 v6, 0x1

    .line 448
    :goto_8
    if-eqz v6, :cond_1a

    .line 449
    move-object/from16 v0, p0

    iget-object v8, v0, Lln;->x:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v8, v2}, Lln;->a(Lme;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v11

    .line 450
    move-object/from16 v0, p0

    iget-object v8, v0, Lln;->x:Landroid/view/View;

    if-nez v8, :cond_b

    .line 451
    move-object/from16 v0, p0

    iput-object v11, v0, Lln;->x:Landroid/view/View;

    .line 453
    :cond_b
    move-object/from16 v0, p0

    iget-boolean v8, v0, Lln;->s:Z

    if-eqz v8, :cond_f

    .line 454
    const/4 v8, 0x0

    invoke-static {v11, v5, v4, v15, v8}, Landroid/support/v7/internal/view/menu/ActionMenuView;->a(Landroid/view/View;IIII)I

    move-result v19

    .line 456
    sub-int v8, v4, v19

    .line 457
    if-nez v19, :cond_19

    .line 458
    const/4 v4, 0x0

    :goto_9
    move v6, v8

    .line 463
    :goto_a
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    .line 464
    sub-int/2addr v9, v8

    .line 465
    if-nez v7, :cond_c

    move v7, v8

    .line 469
    :cond_c
    move-object/from16 v0, p0

    iget-boolean v8, v0, Lln;->s:Z

    if-eqz v8, :cond_11

    .line 470
    if-ltz v9, :cond_10

    const/4 v8, 0x1

    :goto_b
    and-int/2addr v4, v8

    move v11, v4

    move v8, v7

    move v7, v6

    .line 477
    :goto_c
    if-eqz v11, :cond_13

    if-eqz v17, :cond_13

    .line 478
    const/4 v4, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v0, v1, v4}, Landroid/util/SparseBooleanArray;->put(IZ)V

    move v4, v10

    .line 494
    :goto_d
    if-eqz v11, :cond_d

    .line 495
    add-int/lit8 v4, v4, -0x1

    .line 498
    :cond_d
    invoke-virtual {v3, v11}, Lme;->d(Z)V

    move v6, v8

    move v3, v9

    move/from16 v20, v7

    move v7, v4

    move/from16 v4, v20

    goto :goto_7

    .line 445
    :cond_e
    const/4 v6, 0x0

    goto :goto_8

    .line 461
    :cond_f
    invoke-virtual {v11, v15, v15}, Landroid/view/View;->measure(II)V

    move/from16 v20, v6

    move v6, v4

    move/from16 v4, v20

    goto :goto_a

    .line 470
    :cond_10
    const/4 v8, 0x0

    goto :goto_b

    .line 473
    :cond_11
    add-int v8, v9, v7

    if-lez v8, :cond_12

    const/4 v8, 0x1

    :goto_e
    and-int/2addr v4, v8

    move v11, v4

    move v8, v7

    move v7, v6

    goto :goto_c

    :cond_12
    const/4 v8, 0x0

    goto :goto_e

    .line 479
    :cond_13
    if-eqz v18, :cond_18

    .line 481
    const/4 v4, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v0, v1, v4}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 482
    const/4 v4, 0x0

    move v6, v10

    move v10, v4

    :goto_f
    if-ge v10, v12, :cond_17

    .line 483
    invoke-virtual {v13, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lme;

    .line 484
    invoke-virtual {v4}, Lme;->getGroupId()I

    move-result v18

    move/from16 v0, v18

    move/from16 v1, v17

    if-ne v0, v1, :cond_15

    .line 486
    invoke-virtual {v4}, Lme;->h()Z

    move-result v18

    if-eqz v18, :cond_14

    .line 487
    add-int/lit8 v6, v6, 0x1

    .line 489
    :cond_14
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v4, v0}, Lme;->d(Z)V

    .line 482
    :cond_15
    add-int/lit8 v4, v10, 0x1

    move v10, v4

    goto :goto_f

    .line 501
    :cond_16
    const/4 v2, 0x1

    return v2

    :cond_17
    move v4, v6

    goto :goto_d

    :cond_18
    move v4, v10

    goto :goto_d

    :cond_19
    move v4, v6

    goto/16 :goto_9

    :cond_1a
    move v11, v6

    move v8, v7

    move v7, v4

    goto :goto_c

    :cond_1b
    move v6, v7

    move v3, v9

    move v7, v10

    goto/16 :goto_7

    :cond_1c
    move v6, v7

    goto/16 :goto_6

    :cond_1d
    move v5, v4

    goto/16 :goto_3

    :cond_1e
    move v3, v7

    goto/16 :goto_2
.end method
