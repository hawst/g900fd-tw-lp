.class public final Lcws;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lcns;

.field private b:Lcom/google/android/gms/internal/qr;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcws;-><init>(Landroid/content/Context;Ljava/lang/String;B)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcws;-><init>(Landroid/content/Context;Ljava/lang/String;B)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;B)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcws;-><init>(Landroid/content/Context;Ljava/lang/String;C)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;C)V
    .locals 4

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    iget v0, v2, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v2, Lcom/google/android/gms/internal/qr;

    const/16 v3, 0x32

    invoke-direct {v2, v1, v0, v3, p2}, Lcom/google/android/gms/internal/qr;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    iput-object v2, p0, Lcws;->b:Lcom/google/android/gms/internal/qr;

    new-instance v0, Lcns;

    new-instance v1, Lcnq;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcnq;-><init>(La;)V

    invoke-direct {v0, p1, v1}, Lcns;-><init>(Landroid/content/Context;Lcnq;)V

    iput-object v0, p0, Lcws;->a:Lcns;

    return-void

    :catch_0
    move-exception v2

    const-string v2, "PlayLogger"

    const-string v3, "This can\'t happen."

    invoke-static {v2, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcws;->a:Lcns;

    invoke-virtual {v0}, Lcns;->j()V

    return-void
.end method

.method public varargs a(JLjava/lang/String;[B[Ljava/lang/String;)V
    .locals 8

    iget-object v6, p0, Lcws;->a:Lcns;

    iget-object v7, p0, Lcws;->b:Lcom/google/android/gms/internal/qr;

    new-instance v0, Lcom/google/android/gms/internal/qn;

    move-wide v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/internal/qn;-><init>(JLjava/lang/String;[B[Ljava/lang/String;)V

    invoke-virtual {v6, v7, v0}, Lcns;->a(Lcom/google/android/gms/internal/qr;Lcom/google/android/gms/internal/qn;)V

    return-void
.end method

.method public varargs a(Ljava/lang/String;[B[Ljava/lang/String;)V
    .locals 6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcws;->a(JLjava/lang/String;[B[Ljava/lang/String;)V

    return-void
.end method
