.class public final Leil;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leil;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:[I

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Leim;

.field public i:Leig;

.field public j:Lfab;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Leil;

    sput-object v0, Leil;->a:[Leil;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 57
    sget-object v0, Lept;->e:[I

    iput-object v0, p0, Leil;->d:[I

    .line 60
    iput-object v1, p0, Leil;->e:Ljava/lang/Integer;

    .line 67
    iput-object v1, p0, Leil;->h:Leim;

    .line 70
    iput-object v1, p0, Leil;->i:Leig;

    .line 73
    iput-object v1, p0, Leil;->j:Lfab;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 111
    const/4 v0, 0x1

    iget-object v2, p0, Leil;->b:Ljava/lang/String;

    .line 113
    invoke-static {v0, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 114
    iget-object v2, p0, Leil;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 115
    const/4 v2, 0x2

    iget-object v3, p0, Leil;->c:Ljava/lang/String;

    .line 116
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 118
    :cond_0
    iget-object v2, p0, Leil;->d:[I

    if-eqz v2, :cond_2

    iget-object v2, p0, Leil;->d:[I

    array-length v2, v2

    if-lez v2, :cond_2

    .line 120
    iget-object v3, p0, Leil;->d:[I

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v1, v4, :cond_1

    aget v5, v3, v1

    .line 122
    invoke-static {v5}, Lepl;->f(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 120
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 124
    :cond_1
    add-int/2addr v0, v2

    .line 125
    iget-object v1, p0, Leil;->d:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 127
    :cond_2
    iget-object v1, p0, Leil;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 128
    const/4 v1, 0x4

    iget-object v2, p0, Leil;->e:Ljava/lang/Integer;

    .line 129
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 131
    :cond_3
    iget-object v1, p0, Leil;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 132
    const/4 v1, 0x5

    iget-object v2, p0, Leil;->f:Ljava/lang/String;

    .line 133
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 135
    :cond_4
    iget-object v1, p0, Leil;->g:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 136
    const/4 v1, 0x6

    iget-object v2, p0, Leil;->g:Ljava/lang/String;

    .line 137
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 139
    :cond_5
    iget-object v1, p0, Leil;->j:Lfab;

    if-eqz v1, :cond_6

    .line 140
    const/16 v1, 0xf

    iget-object v2, p0, Leil;->j:Lfab;

    .line 141
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 143
    :cond_6
    iget-object v1, p0, Leil;->h:Leim;

    if-eqz v1, :cond_7

    .line 144
    const/16 v1, 0x64

    iget-object v2, p0, Leil;->h:Leim;

    .line 145
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 147
    :cond_7
    iget-object v1, p0, Leil;->i:Leig;

    if-eqz v1, :cond_8

    .line 148
    const/16 v1, 0x1f4

    iget-object v2, p0, Leil;->i:Leig;

    .line 149
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 151
    :cond_8
    iget-object v1, p0, Leil;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 152
    iput v0, p0, Leil;->cachedSize:I

    .line 153
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Leil;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Leil;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Leil;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leil;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leil;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Leil;->d:[I

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [I

    iget-object v2, p0, Leil;->d:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Leil;->d:[I

    :goto_1
    iget-object v1, p0, Leil;->d:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Leil;->d:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    aput v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Leil;->d:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-eq v0, v1, :cond_3

    const/4 v1, 0x5

    if-eq v0, v1, :cond_3

    const/4 v1, 0x6

    if-eq v0, v1, :cond_3

    const/4 v1, 0x7

    if-eq v0, v1, :cond_3

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leil;->e:Ljava/lang/Integer;

    goto :goto_0

    :cond_4
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leil;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leil;->f:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leil;->g:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Leil;->j:Lfab;

    if-nez v0, :cond_5

    new-instance v0, Lfab;

    invoke-direct {v0}, Lfab;-><init>()V

    iput-object v0, p0, Leil;->j:Lfab;

    :cond_5
    iget-object v0, p0, Leil;->j:Lfab;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Leil;->h:Leim;

    if-nez v0, :cond_6

    new-instance v0, Leim;

    invoke-direct {v0}, Leim;-><init>()V

    iput-object v0, p0, Leil;->h:Leim;

    :cond_6
    iget-object v0, p0, Leil;->h:Leim;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Leil;->i:Leig;

    if-nez v0, :cond_7

    new-instance v0, Leig;

    invoke-direct {v0}, Leig;-><init>()V

    iput-object v0, p0, Leil;->i:Leig;

    :cond_7
    iget-object v0, p0, Leil;->i:Leig;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x7a -> :sswitch_7
        0x322 -> :sswitch_8
        0xfa2 -> :sswitch_9
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 78
    const/4 v0, 0x1

    iget-object v1, p0, Leil;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 79
    iget-object v0, p0, Leil;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 80
    const/4 v0, 0x2

    iget-object v1, p0, Leil;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 82
    :cond_0
    iget-object v0, p0, Leil;->d:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Leil;->d:[I

    array-length v0, v0

    if-lez v0, :cond_1

    .line 83
    iget-object v1, p0, Leil;->d:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget v3, v1, v0

    .line 84
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lepl;->a(II)V

    .line 83
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 87
    :cond_1
    iget-object v0, p0, Leil;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 88
    const/4 v0, 0x4

    iget-object v1, p0, Leil;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 90
    :cond_2
    iget-object v0, p0, Leil;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 91
    const/4 v0, 0x5

    iget-object v1, p0, Leil;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 93
    :cond_3
    iget-object v0, p0, Leil;->g:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 94
    const/4 v0, 0x6

    iget-object v1, p0, Leil;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 96
    :cond_4
    iget-object v0, p0, Leil;->j:Lfab;

    if-eqz v0, :cond_5

    .line 97
    const/16 v0, 0xf

    iget-object v1, p0, Leil;->j:Lfab;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 99
    :cond_5
    iget-object v0, p0, Leil;->h:Leim;

    if-eqz v0, :cond_6

    .line 100
    const/16 v0, 0x64

    iget-object v1, p0, Leil;->h:Leim;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 102
    :cond_6
    iget-object v0, p0, Leil;->i:Leig;

    if-eqz v0, :cond_7

    .line 103
    const/16 v0, 0x1f4

    iget-object v1, p0, Leil;->i:Leig;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 105
    :cond_7
    iget-object v0, p0, Leil;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 107
    return-void
.end method
