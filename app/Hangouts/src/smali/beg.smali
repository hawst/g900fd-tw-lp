.class public Lbeg;
.super Lbep;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2915
    invoke-direct {p0}, Lbep;-><init>()V

    .line 2916
    iput-object p1, p0, Lbeg;->a:Ljava/lang/String;

    .line 2917
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2921
    sget-boolean v0, Lbea;->d:Z

    if-eqz v0, :cond_0

    .line 2922
    const-string v0, "Babel_RequestWriter"

    const-string v1, "DismissSuggestedContactsRequest()"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2925
    :cond_0
    new-instance v0, Ldrd;

    invoke-direct {v0}, Ldrd;-><init>()V

    .line 2928
    const/4 v1, 0x0

    .line 2929
    invoke-virtual {p0, v1, v3, p1, p2}, Lbeg;->a(Ldpt;ZLjava/lang/String;I)Ldvm;

    move-result-object v1

    iput-object v1, v0, Ldrd;->b:Ldvm;

    .line 2931
    iget-object v1, p0, Lbeg;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2932
    new-instance v1, Ldui;

    invoke-direct {v1}, Ldui;-><init>()V

    .line 2933
    iget-object v2, p0, Lbeg;->a:Ljava/lang/String;

    iput-object v2, v1, Ldui;->c:Ljava/lang/String;

    .line 2935
    new-array v2, v3, [Ldui;

    iput-object v2, v0, Ldrd;->c:[Ldui;

    .line 2936
    iget-object v2, v0, Ldrd;->c:[Ldui;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    .line 2939
    :cond_1
    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2944
    const-string v0, "contacts/dismisssuggestedcontacts"

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2949
    const-string v0, "ui_queue"

    return-object v0
.end method
