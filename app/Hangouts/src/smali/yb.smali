.class public Lyb;
.super Lbrv;
.source "PG"


# instance fields
.field protected final a:Lbcn;

.field protected final b:Lbri;


# direct methods
.method public constructor <init>(Lbcn;Lbri;)V
    .locals 2

    .prologue
    .line 14
    invoke-direct {p0}, Lbrv;-><init>()V

    .line 15
    if-nez p1, :cond_0

    .line 16
    const-string v0, "Babel"

    const-string v1, "lookupSpec should not be empty"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    :cond_0
    if-nez p2, :cond_1

    .line 20
    const-string v0, "Babel"

    const-string v1, "consumer should not be null"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    :cond_1
    iput-object p1, p0, Lyb;->a:Lbcn;

    .line 24
    iput-object p2, p0, Lyb;->b:Lbri;

    .line 25
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lbri;)V
    .locals 1

    .prologue
    .line 28
    invoke-static {p1}, Lbcn;->a(Ljava/lang/String;)Lbcn;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lyb;-><init>(Lbcn;Lbri;)V

    .line 29
    return-void
.end method


# virtual methods
.method public a()Lbri;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lyb;->b:Lbri;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lyb;->a:Lbcn;

    invoke-virtual {v0}, Lbcn;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Lbcn;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lyb;->a:Lbcn;

    return-object v0
.end method
