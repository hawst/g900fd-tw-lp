.class public final Ldys;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldys;


# instance fields
.field public b:Ldvn;

.field public c:Ldyq;

.field public d:Ldzg;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1548
    const/4 v0, 0x0

    new-array v0, v0, [Ldys;

    sput-object v0, Ldys;->a:[Ldys;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1549
    invoke-direct {p0}, Lepn;-><init>()V

    .line 1552
    iput-object v0, p0, Ldys;->b:Ldvn;

    .line 1555
    iput-object v0, p0, Ldys;->c:Ldyq;

    .line 1558
    iput-object v0, p0, Ldys;->d:Ldzg;

    .line 1549
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1578
    const/4 v0, 0x0

    .line 1579
    iget-object v1, p0, Ldys;->b:Ldvn;

    if-eqz v1, :cond_0

    .line 1580
    const/4 v0, 0x1

    iget-object v1, p0, Ldys;->b:Ldvn;

    .line 1581
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1583
    :cond_0
    iget-object v1, p0, Ldys;->c:Ldyq;

    if-eqz v1, :cond_1

    .line 1584
    const/4 v1, 0x2

    iget-object v2, p0, Ldys;->c:Ldyq;

    .line 1585
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1587
    :cond_1
    iget-object v1, p0, Ldys;->d:Ldzg;

    if-eqz v1, :cond_2

    .line 1588
    const/4 v1, 0x3

    iget-object v2, p0, Ldys;->d:Ldzg;

    .line 1589
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1591
    :cond_2
    iget-object v1, p0, Ldys;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1592
    iput v0, p0, Ldys;->cachedSize:I

    .line 1593
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 1545
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldys;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldys;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldys;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldys;->b:Ldvn;

    if-nez v0, :cond_2

    new-instance v0, Ldvn;

    invoke-direct {v0}, Ldvn;-><init>()V

    iput-object v0, p0, Ldys;->b:Ldvn;

    :cond_2
    iget-object v0, p0, Ldys;->b:Ldvn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldys;->c:Ldyq;

    if-nez v0, :cond_3

    new-instance v0, Ldyq;

    invoke-direct {v0}, Ldyq;-><init>()V

    iput-object v0, p0, Ldys;->c:Ldyq;

    :cond_3
    iget-object v0, p0, Ldys;->c:Ldyq;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldys;->d:Ldzg;

    if-nez v0, :cond_4

    new-instance v0, Ldzg;

    invoke-direct {v0}, Ldzg;-><init>()V

    iput-object v0, p0, Ldys;->d:Ldzg;

    :cond_4
    iget-object v0, p0, Ldys;->d:Ldzg;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 1563
    iget-object v0, p0, Ldys;->b:Ldvn;

    if-eqz v0, :cond_0

    .line 1564
    const/4 v0, 0x1

    iget-object v1, p0, Ldys;->b:Ldvn;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1566
    :cond_0
    iget-object v0, p0, Ldys;->c:Ldyq;

    if-eqz v0, :cond_1

    .line 1567
    const/4 v0, 0x2

    iget-object v1, p0, Ldys;->c:Ldyq;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1569
    :cond_1
    iget-object v0, p0, Ldys;->d:Ldzg;

    if-eqz v0, :cond_2

    .line 1570
    const/4 v0, 0x3

    iget-object v1, p0, Ldys;->d:Ldzg;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1572
    :cond_2
    iget-object v0, p0, Ldys;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1574
    return-void
.end method
