.class public final Ldlg;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldlg;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4991
    const/4 v0, 0x0

    new-array v0, v0, [Ldlg;

    sput-object v0, Ldlg;->a:[Ldlg;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4992
    invoke-direct {p0}, Lepn;-><init>()V

    .line 5001
    const/4 v0, 0x0

    iput-object v0, p0, Ldlg;->b:Ljava/lang/Integer;

    .line 4992
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 5020
    const/4 v0, 0x0

    .line 5021
    iget-object v1, p0, Ldlg;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 5022
    const/4 v0, 0x1

    iget-object v1, p0, Ldlg;->b:Ljava/lang/Integer;

    .line 5023
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5025
    :cond_0
    iget-object v1, p0, Ldlg;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 5026
    const/4 v1, 0x2

    iget-object v2, p0, Ldlg;->c:Ljava/lang/String;

    .line 5027
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5029
    :cond_1
    iget-object v1, p0, Ldlg;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5030
    iput v0, p0, Ldlg;->cachedSize:I

    .line 5031
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 4988
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldlg;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldlg;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldlg;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldlg;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldlg;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldlg;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 5008
    iget-object v0, p0, Ldlg;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 5009
    const/4 v0, 0x1

    iget-object v1, p0, Ldlg;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 5011
    :cond_0
    iget-object v0, p0, Ldlg;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 5012
    const/4 v0, 0x2

    iget-object v1, p0, Ldlg;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 5014
    :cond_1
    iget-object v0, p0, Ldlg;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 5016
    return-void
.end method
