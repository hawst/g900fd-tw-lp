.class public final Lekc;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lekc;


# instance fields
.field public b:Ljava/lang/Double;

.field public c:Ljava/lang/Double;

.field public d:Ljava/lang/Double;

.field public e:[Lekb;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 251
    const/4 v0, 0x0

    new-array v0, v0, [Lekc;

    sput-object v0, Lekc;->a:[Lekc;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 252
    invoke-direct {p0}, Lepn;-><init>()V

    .line 261
    sget-object v0, Lekb;->a:[Lekb;

    iput-object v0, p0, Lekc;->e:[Lekb;

    .line 252
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 289
    iget-object v0, p0, Lekc;->b:Ljava/lang/Double;

    if-eqz v0, :cond_4

    .line 290
    const/4 v0, 0x1

    iget-object v2, p0, Lekc;->b:Ljava/lang/Double;

    .line 291
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v0}, Lepl;->g(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x0

    .line 293
    :goto_0
    iget-object v2, p0, Lekc;->c:Ljava/lang/Double;

    if-eqz v2, :cond_0

    .line 294
    const/4 v2, 0x2

    iget-object v3, p0, Lekc;->c:Ljava/lang/Double;

    .line 295
    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    add-int/2addr v0, v2

    .line 297
    :cond_0
    iget-object v2, p0, Lekc;->d:Ljava/lang/Double;

    if-eqz v2, :cond_1

    .line 298
    const/4 v2, 0x3

    iget-object v3, p0, Lekc;->d:Ljava/lang/Double;

    .line 299
    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    add-int/2addr v0, v2

    .line 301
    :cond_1
    iget-object v2, p0, Lekc;->e:[Lekb;

    if-eqz v2, :cond_3

    .line 302
    iget-object v2, p0, Lekc;->e:[Lekb;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 303
    if-eqz v4, :cond_2

    .line 304
    const/4 v5, 0x4

    .line 305
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 302
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 309
    :cond_3
    iget-object v1, p0, Lekc;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 310
    iput v0, p0, Lekc;->cachedSize:I

    .line 311
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 248
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lekc;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lekc;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lekc;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->b()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lekc;->b:Ljava/lang/Double;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->b()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lekc;->c:Ljava/lang/Double;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->b()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lekc;->d:Ljava/lang/Double;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lekc;->e:[Lekb;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lekb;

    iget-object v3, p0, Lekc;->e:[Lekb;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lekc;->e:[Lekb;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lekc;->e:[Lekb;

    :goto_2
    iget-object v2, p0, Lekc;->e:[Lekb;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lekc;->e:[Lekb;

    new-instance v3, Lekb;

    invoke-direct {v3}, Lekb;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lekc;->e:[Lekb;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lekc;->e:[Lekb;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lekc;->e:[Lekb;

    new-instance v3, Lekb;

    invoke-direct {v3}, Lekb;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lekc;->e:[Lekb;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x11 -> :sswitch_2
        0x19 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 266
    iget-object v0, p0, Lekc;->b:Ljava/lang/Double;

    if-eqz v0, :cond_0

    .line 267
    const/4 v0, 0x1

    iget-object v1, p0, Lekc;->b:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(ID)V

    .line 269
    :cond_0
    iget-object v0, p0, Lekc;->c:Ljava/lang/Double;

    if-eqz v0, :cond_1

    .line 270
    const/4 v0, 0x2

    iget-object v1, p0, Lekc;->c:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(ID)V

    .line 272
    :cond_1
    iget-object v0, p0, Lekc;->d:Ljava/lang/Double;

    if-eqz v0, :cond_2

    .line 273
    const/4 v0, 0x3

    iget-object v1, p0, Lekc;->d:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(ID)V

    .line 275
    :cond_2
    iget-object v0, p0, Lekc;->e:[Lekb;

    if-eqz v0, :cond_4

    .line 276
    iget-object v1, p0, Lekc;->e:[Lekb;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 277
    if-eqz v3, :cond_3

    .line 278
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 276
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 282
    :cond_4
    iget-object v0, p0, Lekc;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 284
    return-void
.end method
