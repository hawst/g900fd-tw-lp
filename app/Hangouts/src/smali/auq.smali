.class public final Lauq;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field public static a:F

.field private static b:Landroid/view/animation/DecelerateInterpolator;

.field private static c:I

.field private static d:I

.field private static e:I

.field private static f:I

.field private static g:I

.field private static h:F

.field private static i:F


# instance fields
.field private j:F

.field private final k:Lauv;

.field private final l:I

.field private final m:Landroid/view/VelocityTracker;

.field private n:F

.field private o:Z

.field private p:Z

.field private q:Lauw;

.field private r:Landroid/view/View;

.field private s:Z

.field private t:F

.field private u:F

.field private v:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    sput-object v0, Lauq;->b:Landroid/view/animation/DecelerateInterpolator;

    .line 38
    const/4 v0, -0x1

    sput v0, Lauq;->c:I

    .line 46
    const/4 v0, 0x0

    sput v0, Lauq;->a:F

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Landroid/view/VelocityTracker;Lauv;FF)V
    .locals 2

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p3, p0, Lauq;->k:Lauv;

    .line 70
    const/4 v0, 0x0

    iput v0, p0, Lauq;->l:I

    .line 71
    iput-object p2, p0, Lauq;->m:Landroid/view/VelocityTracker;

    .line 72
    iput p4, p0, Lauq;->t:F

    .line 73
    iput p5, p0, Lauq;->j:F

    .line 74
    sget v0, Lauq;->c:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 75
    sget v0, Lf;->iu:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lauq;->c:I

    .line 76
    sget v0, Lf;->iq:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lauq;->d:I

    .line 77
    sget v0, Lf;->is:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lauq;->e:I

    .line 78
    sget v0, Lf;->ir:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lauq;->f:I

    .line 79
    sget v0, Lf;->it:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lauq;->g:I

    .line 80
    sget v0, Lf;->il:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lauq;->h:F

    .line 81
    sget v0, Lf;->im:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lauq;->i:F

    .line 83
    :cond_0
    return-void
.end method

.method static synthetic a(Lauq;Landroid/view/View;)F
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lauq;->c(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method private a(Landroid/view/View;F)Landroid/animation/ObjectAnimator;
    .locals 3

    .prologue
    .line 98
    iget v0, p0, Lauq;->l:I

    if-nez v0, :cond_0

    const-string v0, "translationX"

    :goto_0
    const/4 v1, 0x1

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput p2, v1, v2

    invoke-static {p1, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "translationY"

    goto :goto_0
.end method

.method static synthetic a(Lauq;)Lauv;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lauq;->k:Lauv;

    return-object v0
.end method

.method public static a(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 141
    new-instance v0, Landroid/graphics/RectF;

    .line 143
    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result v4

    int-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 141
    invoke-static {p0, v0}, Lauq;->a(Landroid/view/View;Landroid/graphics/RectF;)V

    .line 144
    return-void
.end method

.method private static a(Landroid/view/View;Landroid/graphics/RectF;)V
    .locals 6

    .prologue
    .line 153
    :goto_0
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 154
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 155
    invoke-virtual {v0}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 156
    iget v1, p1, Landroid/graphics/RectF;->left:F

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->floor(D)D

    move-result-wide v1

    double-to-int v1, v1

    iget v2, p1, Landroid/graphics/RectF;->top:F

    float-to-double v2, v2

    .line 157
    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v2, v2

    iget v3, p1, Landroid/graphics/RectF;->right:F

    float-to-double v3, v3

    .line 158
    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v3

    double-to-int v3, v3

    iget v4, p1, Landroid/graphics/RectF;->bottom:F

    float-to-double v4, v4

    .line 159
    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v4, v4

    .line 156
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->invalidate(IIII)V

    move-object p0, v0

    goto :goto_0

    .line 167
    :cond_0
    return-void
.end method

.method private b(Landroid/view/View;)F
    .locals 1

    .prologue
    .line 122
    iget v0, p0, Lauq;->l:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    int-to-float v0, v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    int-to-float v0, v0

    goto :goto_0
.end method

.method private c(Landroid/view/View;)F
    .locals 5

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 126
    invoke-direct {p0, p1}, Lauq;->b(Landroid/view/View;)F

    move-result v1

    .line 127
    const v2, 0x3f333333    # 0.7f

    mul-float/2addr v2, v1

    .line 129
    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result v3

    .line 130
    sget v4, Lauq;->a:F

    mul-float/2addr v4, v1

    cmpl-float v4, v3, v4

    if-ltz v4, :cond_1

    .line 131
    sget v4, Lauq;->a:F

    mul-float/2addr v1, v4

    sub-float v1, v3, v1

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    .line 135
    :cond_0
    :goto_0
    const/high16 v1, 0x3f000000    # 0.5f

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0

    .line 132
    :cond_1
    sget v4, Lauq;->a:F

    sub-float v4, v0, v4

    mul-float/2addr v4, v1

    cmpg-float v4, v3, v4

    if-gez v4, :cond_0

    .line 133
    sget v4, Lauq;->a:F

    mul-float/2addr v1, v4

    add-float/2addr v1, v3

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(F)V
    .locals 0

    .prologue
    .line 86
    iput p1, p0, Lauq;->t:F

    .line 87
    return-void
.end method

.method public a(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 170
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 171
    packed-switch v2, :pswitch_data_0

    .line 227
    :goto_0
    iget-boolean v0, p0, Lauq;->o:Z

    :cond_0
    :goto_1
    return v0

    .line 173
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iput v2, p0, Lauq;->u:F

    .line 174
    iput-boolean v0, p0, Lauq;->o:Z

    .line 175
    iput-boolean v0, p0, Lauq;->p:Z

    .line 176
    iget-object v0, p0, Lauq;->k:Lauv;

    invoke-interface {v0, p1}, Lauv;->a(Landroid/view/MotionEvent;)Landroid/view/View;

    move-result-object v0

    .line 177
    instance-of v2, v0, Lauw;

    if-eqz v2, :cond_2

    check-cast v0, Lauw;

    :goto_2
    iput-object v0, p0, Lauq;->q:Lauw;

    .line 178
    iget-object v0, p0, Lauq;->m:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 179
    iget-object v0, p0, Lauq;->q:Lauw;

    if-eqz v0, :cond_1

    .line 180
    iget-object v0, p0, Lauq;->q:Lauw;

    invoke-interface {v0}, Lauw;->a()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lauq;->r:Landroid/view/View;

    .line 181
    iget-object v0, p0, Lauq;->k:Lauv;

    iget-object v1, p0, Lauq;->q:Lauw;

    invoke-interface {v0, v1}, Lauv;->a(Lauw;)Z

    move-result v0

    iput-boolean v0, p0, Lauq;->s:Z

    .line 182
    iget-object v0, p0, Lauq;->m:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 183
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lauq;->n:F

    .line 184
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lauq;->v:F

    .line 186
    :cond_1
    iget-object v0, p0, Lauq;->k:Lauv;

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 177
    goto :goto_2

    .line 189
    :pswitch_1
    iget-object v1, p0, Lauq;->q:Lauw;

    if-eqz v1, :cond_5

    .line 191
    iget v1, p0, Lauq;->u:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_4

    iget-boolean v1, p0, Lauq;->o:Z

    if-nez v1, :cond_4

    .line 192
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 193
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 194
    iget v3, p0, Lauq;->v:F

    sub-float/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 195
    iget v3, p0, Lauq;->n:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 196
    iget-boolean v3, p0, Lauq;->p:Z

    if-nez v3, :cond_3

    const/high16 v3, 0x40000000    # 2.0f

    cmpl-float v3, v1, v3

    if-lez v3, :cond_4

    const v3, 0x3f99999a    # 1.2f

    mul-float/2addr v2, v3

    cmpl-float v2, v1, v2

    if-lez v2, :cond_4

    .line 198
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iput v2, p0, Lauq;->u:F

    .line 199
    iget-object v2, p0, Lauq;->k:Lauv;

    .line 200
    sget v2, Lauq;->i:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    .line 201
    iput-boolean v4, p0, Lauq;->p:Z

    goto/16 :goto_1

    .line 206
    :cond_4
    iget-object v0, p0, Lauq;->m:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 207
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 208
    iget v1, p0, Lauq;->n:F

    sub-float/2addr v0, v1

    .line 209
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lauq;->j:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_5

    .line 210
    iget-object v0, p0, Lauq;->k:Lauv;

    iget-object v1, p0, Lauq;->q:Lauw;

    invoke-interface {v0, v1}, Lauv;->b(Lauw;)V

    .line 211
    iput-boolean v4, p0, Lauq;->o:Z

    .line 212
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v1, p0, Lauq;->r:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTranslationX()F

    move-result v1

    sub-float/2addr v0, v1

    iput v0, p0, Lauq;->n:F

    .line 213
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lauq;->v:F

    .line 216
    :cond_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lauq;->u:F

    goto/16 :goto_0

    .line 220
    :pswitch_2
    iput-boolean v0, p0, Lauq;->o:Z

    .line 221
    iput-object v1, p0, Lauq;->q:Lauw;

    .line 222
    iput-object v1, p0, Lauq;->r:Landroid/view/View;

    .line 223
    const/high16 v1, -0x40800000    # -1.0f

    iput v1, p0, Lauq;->u:F

    .line 224
    iput-boolean v0, p0, Lauq;->p:Z

    goto/16 :goto_0

    .line 171
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public b(F)V
    .locals 0

    .prologue
    .line 90
    iput p1, p0, Lauq;->j:F

    .line 91
    return-void
.end method

.method public b(Landroid/view/MotionEvent;)Z
    .locals 12

    .prologue
    .line 324
    iget-boolean v0, p0, Lauq;->o:Z

    if-nez v0, :cond_0

    .line 325
    const/4 v0, 0x0

    .line 396
    :goto_0
    return v0

    .line 327
    :cond_0
    iget-object v0, p0, Lauq;->m:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 328
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 329
    packed-switch v0, :pswitch_data_0

    .line 396
    :cond_1
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 332
    :pswitch_0
    iget-object v0, p0, Lauq;->q:Lauw;

    if-eqz v0, :cond_1

    .line 333
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget v1, p0, Lauq;->n:F

    sub-float v1, v0, v1

    .line 334
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    sget v2, Lauq;->h:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_2

    .line 336
    const/4 v0, 0x1

    goto :goto_0

    .line 340
    :cond_2
    iget-object v0, p0, Lauq;->k:Lauv;

    iget-object v2, p0, Lauq;->q:Lauw;

    invoke-interface {v0, v2}, Lauv;->a(Lauw;)Z

    move-result v0

    if-nez v0, :cond_16

    .line 341
    iget-object v0, p0, Lauq;->r:Landroid/view/View;

    invoke-direct {p0, v0}, Lauq;->b(Landroid/view/View;)F

    move-result v2

    .line 342
    const v0, 0x3e19999a    # 0.15f

    mul-float/2addr v0, v2

    .line 343
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpl-float v3, v3, v2

    if-ltz v3, :cond_5

    .line 344
    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_4

    .line 350
    :goto_2
    iget-object v1, p0, Lauq;->r:Landroid/view/View;

    iget v2, p0, Lauq;->l:I

    if-nez v2, :cond_6

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 351
    :goto_3
    iget-boolean v0, p0, Lauq;->s:Z

    if-eqz v0, :cond_3

    .line 352
    iget-object v0, p0, Lauq;->r:Landroid/view/View;

    iget-object v1, p0, Lauq;->r:Landroid/view/View;

    invoke-direct {p0, v1}, Lauq;->c(Landroid/view/View;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 354
    :cond_3
    iget-object v0, p0, Lauq;->q:Lauw;

    invoke-interface {v0}, Lauw;->a()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lauq;->a(Landroid/view/View;)V

    goto :goto_1

    .line 344
    :cond_4
    neg-float v0, v0

    goto :goto_2

    .line 346
    :cond_5
    div-float/2addr v1, v2

    float-to-double v1, v1

    const-wide v3, 0x3ff921fb54442d18L    # 1.5707963267948966

    mul-double/2addr v1, v3

    .line 347
    invoke-static {v1, v2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v1

    double-to-float v1, v1

    mul-float/2addr v0, v1

    goto :goto_2

    .line 350
    :cond_6
    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_3

    .line 359
    :pswitch_1
    iget-object v0, p0, Lauq;->q:Lauw;

    if-eqz v0, :cond_1

    .line 360
    sget v0, Lauq;->f:I

    int-to-float v0, v0

    iget v1, p0, Lauq;->t:F

    mul-float/2addr v0, v1

    .line 361
    iget-object v1, p0, Lauq;->m:Landroid/view/VelocityTracker;

    const/16 v2, 0x3e8

    invoke-virtual {v1, v2, v0}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 362
    sget v0, Lauq;->c:I

    int-to-float v0, v0

    iget v1, p0, Lauq;->t:F

    mul-float v3, v0, v1

    .line 363
    iget-object v0, p0, Lauq;->m:Landroid/view/VelocityTracker;

    iget v1, p0, Lauq;->l:I

    if-nez v1, :cond_b

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    .line 364
    :goto_4
    iget-object v1, p0, Lauq;->m:Landroid/view/VelocityTracker;

    iget v2, p0, Lauq;->l:I

    if-nez v2, :cond_c

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v1

    .line 369
    :goto_5
    iget-object v2, p0, Lauq;->r:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTranslationX()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v4

    .line 370
    iget-object v2, p0, Lauq;->r:Landroid/view/View;

    invoke-direct {p0, v2}, Lauq;->b(Landroid/view/View;)F

    move-result v5

    .line 372
    float-to-double v6, v4

    const-wide v8, 0x3fd999999999999aL    # 0.4

    float-to-double v10, v5

    mul-double/2addr v8, v10

    cmpl-double v2, v6, v8

    if-lez v2, :cond_d

    const/4 v2, 0x1

    .line 375
    :goto_6
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v6

    cmpl-float v3, v6, v3

    if-lez v3, :cond_10

    .line 376
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v3

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v3, v1

    if-lez v1, :cond_10

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-lez v1, :cond_e

    const/4 v1, 0x1

    :goto_7
    iget-object v3, p0, Lauq;->r:Landroid/view/View;

    .line 377
    invoke-virtual {v3}, Landroid/view/View;->getTranslationX()F

    move-result v3

    const/4 v6, 0x0

    cmpl-float v3, v3, v6

    if-lez v3, :cond_f

    const/4 v3, 0x1

    :goto_8
    if-ne v1, v3, :cond_10

    float-to-double v3, v4

    const-wide v6, 0x3fa999999999999aL    # 0.05

    float-to-double v8, v5

    mul-double v5, v6, v8

    cmpl-double v1, v3, v5

    if-lez v1, :cond_10

    const/4 v1, 0x1

    .line 385
    :goto_9
    iget-object v3, p0, Lauq;->k:Lauv;

    iget-object v4, p0, Lauq;->q:Lauw;

    invoke-interface {v3, v4}, Lauv;->a(Lauw;)Z

    move-result v3

    if-eqz v3, :cond_11

    if-nez v1, :cond_7

    if-eqz v2, :cond_11

    :cond_7
    const/4 v2, 0x1

    .line 388
    :goto_a
    if-eqz v2, :cond_15

    .line 389
    iget-object v2, p0, Lauq;->q:Lauw;

    if-eqz v1, :cond_12

    :goto_b
    iget-object v1, p0, Lauq;->q:Lauw;

    invoke-interface {v1}, Lauw;->a()Landroid/view/View;

    move-result-object v3

    iget-object v1, p0, Lauq;->k:Lauv;

    invoke-interface {v1, v2}, Lauv;->a(Lauw;)Z

    move-result v4

    const/4 v1, 0x0

    cmpg-float v1, v0, v1

    if-ltz v1, :cond_9

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-nez v1, :cond_8

    invoke-virtual {v3}, Landroid/view/View;->getTranslationX()F

    move-result v1

    const/4 v5, 0x0

    cmpg-float v1, v1, v5

    if-ltz v1, :cond_9

    :cond_8
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-nez v1, :cond_13

    invoke-virtual {v3}, Landroid/view/View;->getTranslationX()F

    move-result v1

    const/4 v5, 0x0

    cmpl-float v1, v1, v5

    if-nez v1, :cond_13

    iget v1, p0, Lauq;->l:I

    const/4 v5, 0x1

    if-ne v1, v5, :cond_13

    :cond_9
    invoke-direct {p0, v3}, Lauq;->b(Landroid/view/View;)F

    move-result v1

    neg-float v1, v1

    :goto_c
    sget v5, Lauq;->e:I

    const/4 v6, 0x0

    cmpl-float v6, v0, v6

    if-eqz v6, :cond_14

    invoke-virtual {v3}, Landroid/view/View;->getTranslationX()F

    move-result v6

    sub-float v6, v1, v6

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    const/high16 v7, 0x447a0000    # 1000.0f

    mul-float/2addr v6, v7

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    div-float v0, v6, v0

    float-to-int v0, v0

    invoke-static {v5, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    :goto_d
    if-eqz v3, :cond_a

    invoke-virtual {v3}, Landroid/view/View;->isHardwareAccelerated()Z

    move-result v5

    if-eqz v5, :cond_a

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    invoke-virtual {v3}, Landroid/view/View;->buildLayer()V

    :cond_a
    invoke-direct {p0, v3, v1}, Lauq;->a(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    sget-object v5, Lauq;->b:Landroid/view/animation/DecelerateInterpolator;

    invoke-virtual {v1, v5}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    int-to-long v5, v0

    invoke-virtual {v1, v5, v6}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-instance v0, Laur;

    invoke-direct {v0, p0, v3, v2}, Laur;-><init>(Lauq;Landroid/view/View;Lauw;)V

    invoke-virtual {v1, v0}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    new-instance v0, Laus;

    invoke-direct {v0, p0, v4, v3}, Laus;-><init>(Lauq;ZLandroid/view/View;)V

    invoke-virtual {v1, v0}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    goto/16 :goto_1

    .line 363
    :cond_b
    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v0

    goto/16 :goto_4

    .line 364
    :cond_c
    invoke-virtual {v1}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v1

    goto/16 :goto_5

    .line 372
    :cond_d
    const/4 v2, 0x0

    goto/16 :goto_6

    .line 376
    :cond_e
    const/4 v1, 0x0

    goto/16 :goto_7

    .line 377
    :cond_f
    const/4 v3, 0x0

    goto/16 :goto_8

    :cond_10
    const/4 v1, 0x0

    goto/16 :goto_9

    .line 385
    :cond_11
    const/4 v2, 0x0

    goto/16 :goto_a

    .line 389
    :cond_12
    const/4 v0, 0x0

    goto/16 :goto_b

    :cond_13
    invoke-direct {p0, v3}, Lauq;->b(Landroid/view/View;)F

    move-result v1

    goto :goto_c

    :cond_14
    sget v0, Lauq;->d:I

    goto :goto_d

    .line 391
    :cond_15
    iget-object v0, p0, Lauq;->q:Lauw;

    invoke-interface {v0}, Lauw;->a()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lauq;->k:Lauv;

    invoke-interface {v2, v0}, Lauv;->a(Lauw;)Z

    move-result v2

    const/4 v3, 0x0

    invoke-direct {p0, v1, v3}, Lauq;->a(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    sget v4, Lauq;->g:I

    int-to-long v4, v4

    invoke-virtual {v3, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-instance v4, Laut;

    invoke-direct {v4, p0, v2, v1}, Laut;-><init>(Lauq;ZLandroid/view/View;)V

    invoke-virtual {v3, v4}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    new-instance v2, Lauu;

    invoke-direct {v2, p0, v1, v0}, Lauu;-><init>(Lauq;Landroid/view/View;Lauw;)V

    invoke-virtual {v3, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v3}, Landroid/animation/ObjectAnimator;->start()V

    goto/16 :goto_1

    :cond_16
    move v0, v1

    goto/16 :goto_2

    .line 329
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
