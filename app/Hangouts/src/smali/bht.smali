.class public final Lbht;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:Ljava/lang/String;

.field public final d:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 332
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 333
    const-string v0, ""

    iput-object v0, p0, Lbht;->a:Ljava/lang/String;

    .line 334
    const/4 v0, 0x1

    iput v0, p0, Lbht;->b:I

    .line 335
    const-string v0, ""

    iput-object v0, p0, Lbht;->c:Ljava/lang/String;

    .line 336
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lbht;->d:J

    .line 337
    return-void
.end method

.method public constructor <init>(Ldcw;)V
    .locals 2

    .prologue
    .line 339
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 341
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lbht;->d:J

    .line 342
    iget-object v0, p1, Ldcw;->b:Letl;

    .line 346
    const-string v0, ""

    iput-object v0, p0, Lbht;->a:Ljava/lang/String;

    .line 352
    const/4 v0, 0x1

    iput v0, p0, Lbht;->b:I

    .line 353
    const-string v0, ""

    iput-object v0, p0, Lbht;->c:Ljava/lang/String;

    .line 355
    return-void
.end method

.method public constructor <init>(Ldvn;)V
    .locals 2

    .prologue
    .line 357
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 358
    iget-object v0, p1, Ldvn;->c:Ljava/lang/String;

    iput-object v0, p0, Lbht;->a:Ljava/lang/String;

    .line 359
    iget-object v0, p1, Ldvn;->b:Ljava/lang/Integer;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    iput v0, p0, Lbht;->b:I

    .line 360
    iget-object v0, p1, Ldvn;->d:Ljava/lang/String;

    iput-object v0, p0, Lbht;->c:Ljava/lang/String;

    .line 361
    iget-object v0, p1, Ldvn;->e:Ljava/lang/Long;

    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v0

    iput-wide v0, p0, Lbht;->d:J

    .line 362
    return-void
.end method
