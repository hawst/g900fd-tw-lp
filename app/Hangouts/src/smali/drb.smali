.class public final Ldrb;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldrb;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Double;

.field public d:Ljava/lang/Double;

.field public e:Ljava/lang/Long;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13199
    const/4 v0, 0x0

    new-array v0, v0, [Ldrb;

    sput-object v0, Ldrb;->a:[Ldrb;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13200
    invoke-direct {p0}, Lepn;-><init>()V

    .line 13208
    const/4 v0, 0x0

    iput-object v0, p0, Ldrb;->b:Ljava/lang/Integer;

    .line 13200
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 13247
    const/4 v0, 0x0

    .line 13248
    iget-object v1, p0, Ldrb;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 13249
    const/4 v0, 0x1

    iget-object v1, p0, Ldrb;->b:Ljava/lang/Integer;

    .line 13250
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 13252
    :cond_0
    iget-object v1, p0, Ldrb;->c:Ljava/lang/Double;

    if-eqz v1, :cond_1

    .line 13253
    const/4 v1, 0x2

    iget-object v2, p0, Ldrb;->c:Ljava/lang/Double;

    .line 13254
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 13256
    :cond_1
    iget-object v1, p0, Ldrb;->d:Ljava/lang/Double;

    if-eqz v1, :cond_2

    .line 13257
    const/4 v1, 0x3

    iget-object v2, p0, Ldrb;->d:Ljava/lang/Double;

    .line 13258
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 13260
    :cond_2
    iget-object v1, p0, Ldrb;->e:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 13261
    const/4 v1, 0x4

    iget-object v2, p0, Ldrb;->e:Ljava/lang/Long;

    .line 13262
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 13264
    :cond_3
    iget-object v1, p0, Ldrb;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 13265
    const/4 v1, 0x5

    iget-object v2, p0, Ldrb;->f:Ljava/lang/Integer;

    .line 13266
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 13268
    :cond_4
    iget-object v1, p0, Ldrb;->g:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 13269
    const/4 v1, 0x6

    iget-object v2, p0, Ldrb;->g:Ljava/lang/String;

    .line 13270
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13272
    :cond_5
    iget-object v1, p0, Ldrb;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13273
    iput v0, p0, Ldrb;->cachedSize:I

    .line 13274
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 13196
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldrb;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldrb;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldrb;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldrb;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldrb;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->b()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Ldrb;->c:Ljava/lang/Double;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->b()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Ldrb;->d:Ljava/lang/Double;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldrb;->e:Ljava/lang/Long;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldrb;->f:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldrb;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x11 -> :sswitch_2
        0x19 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 13223
    iget-object v0, p0, Ldrb;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 13224
    const/4 v0, 0x1

    iget-object v1, p0, Ldrb;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 13226
    :cond_0
    iget-object v0, p0, Ldrb;->c:Ljava/lang/Double;

    if-eqz v0, :cond_1

    .line 13227
    const/4 v0, 0x2

    iget-object v1, p0, Ldrb;->c:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(ID)V

    .line 13229
    :cond_1
    iget-object v0, p0, Ldrb;->d:Ljava/lang/Double;

    if-eqz v0, :cond_2

    .line 13230
    const/4 v0, 0x3

    iget-object v1, p0, Ldrb;->d:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(ID)V

    .line 13232
    :cond_2
    iget-object v0, p0, Ldrb;->e:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 13233
    const/4 v0, 0x4

    iget-object v1, p0, Ldrb;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 13235
    :cond_3
    iget-object v0, p0, Ldrb;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 13236
    const/4 v0, 0x5

    iget-object v1, p0, Ldrb;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 13238
    :cond_4
    iget-object v0, p0, Ldrb;->g:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 13239
    const/4 v0, 0x6

    iget-object v1, p0, Ldrb;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 13241
    :cond_5
    iget-object v0, p0, Ldrb;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 13243
    return-void
.end method
