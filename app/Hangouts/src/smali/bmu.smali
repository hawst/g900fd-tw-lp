.class public abstract Lbmu;
.super Lbnj;
.source "PG"


# instance fields
.field private final a:J

.field private final d:J

.field private e:I


# direct methods
.method public constructor <init>(Lyj;JJ)V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lbnj;-><init>(Lyj;)V

    .line 61
    iput-wide p2, p0, Lbmu;->a:J

    .line 62
    iput-wide p4, p0, Lbmu;->d:J

    .line 63
    const/4 v0, 0x0

    iput v0, p0, Lbmu;->e:I

    .line 64
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 2

    .prologue
    .line 70
    monitor-enter p0

    .line 71
    :try_start_0
    iput p1, p0, Lbmu;->e:I

    .line 72
    iget v0, p0, Lbmu;->e:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 73
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lbmu;->a(J)V

    .line 75
    const/4 v0, 0x0

    iput v0, p0, Lbmu;->e:I

    .line 77
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected abstract a(J)V
.end method

.method public d()I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lbmu;->e:I

    return v0
.end method

.method public e()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 91
    monitor-enter p0

    .line 92
    :try_start_0
    iget v1, p0, Lbmu;->e:I

    if-lez v1, :cond_0

    .line 93
    monitor-exit p0

    .line 95
    :goto_0
    return v0

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p0}, Lbmu;->i()J

    move-result-wide v3

    sub-long/2addr v1, v3

    iget-wide v3, p0, Lbmu;->a:J

    cmp-long v1, v1, v3

    if-lez v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 96
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public f()Z
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 103
    monitor-enter p0

    .line 104
    :try_start_0
    iget v1, p0, Lbmu;->e:I

    const/4 v2, 0x2

    if-lt v1, v2, :cond_0

    .line 105
    monitor-exit p0

    .line 112
    :goto_0
    return v0

    .line 108
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 109
    invoke-virtual {p0}, Lbmu;->i()J

    move-result-wide v3

    .line 112
    sub-long v5, v1, v3

    iget-wide v7, p0, Lbmu;->d:J

    cmp-long v5, v5, v7

    if-gtz v5, :cond_1

    sub-long v1, v3, v1

    const-wide/32 v3, 0x5265c00

    cmp-long v1, v1, v3

    if-lez v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :cond_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 114
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 121
    monitor-enter p0

    .line 122
    :try_start_0
    iget v0, p0, Lbmu;->e:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 123
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public h()V
    .locals 2

    .prologue
    .line 130
    monitor-enter p0

    .line 131
    const-wide/16 v0, -0x1

    :try_start_0
    invoke-virtual {p0, v0, v1}, Lbmu;->a(J)V

    .line 132
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected abstract i()J
.end method
