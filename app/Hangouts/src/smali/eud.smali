.class public final Leud;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leud;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 322
    const/4 v0, 0x0

    new-array v0, v0, [Leud;

    sput-object v0, Leud;->a:[Leud;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 323
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 359
    const/4 v0, 0x0

    .line 360
    iget-object v1, p0, Leud;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 361
    const/4 v0, 0x1

    iget-object v1, p0, Leud;->b:Ljava/lang/String;

    .line 362
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 364
    :cond_0
    iget-object v1, p0, Leud;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 365
    const/4 v1, 0x2

    iget-object v2, p0, Leud;->c:Ljava/lang/String;

    .line 366
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 368
    :cond_1
    iget-object v1, p0, Leud;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 369
    const/4 v1, 0x3

    iget-object v2, p0, Leud;->d:Ljava/lang/String;

    .line 370
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 372
    :cond_2
    iget-object v1, p0, Leud;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 373
    const/4 v1, 0x4

    iget-object v2, p0, Leud;->e:Ljava/lang/String;

    .line 374
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 376
    :cond_3
    iget-object v1, p0, Leud;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 377
    const/4 v1, 0x5

    iget-object v2, p0, Leud;->f:Ljava/lang/Boolean;

    .line 378
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 380
    :cond_4
    iget-object v1, p0, Leud;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 381
    iput v0, p0, Leud;->cachedSize:I

    .line 382
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 319
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Leud;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Leud;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Leud;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leud;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leud;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leud;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leud;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Leud;->f:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 338
    iget-object v0, p0, Leud;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 339
    const/4 v0, 0x1

    iget-object v1, p0, Leud;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 341
    :cond_0
    iget-object v0, p0, Leud;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 342
    const/4 v0, 0x2

    iget-object v1, p0, Leud;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 344
    :cond_1
    iget-object v0, p0, Leud;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 345
    const/4 v0, 0x3

    iget-object v1, p0, Leud;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 347
    :cond_2
    iget-object v0, p0, Leud;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 348
    const/4 v0, 0x4

    iget-object v1, p0, Leud;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 350
    :cond_3
    iget-object v0, p0, Leud;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 351
    const/4 v0, 0x5

    iget-object v1, p0, Leud;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 353
    :cond_4
    iget-object v0, p0, Leud;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 355
    return-void
.end method
