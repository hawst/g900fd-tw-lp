.class public final Laix;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbsb;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;)V
    .locals 0

    .prologue
    .line 231
    iput-object p1, p0, Laix;->a:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcvz;)V
    .locals 5

    .prologue
    .line 235
    :try_start_0
    iget-object v0, p0, Laix;->a:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    .line 236
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->cV:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 237
    new-instance v2, Les;

    invoke-direct {v2}, Les;-><init>()V

    .line 238
    invoke-virtual {p1}, Lcvz;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 239
    invoke-virtual {p1, v0}, Lcvz;->b(I)Lcvy;

    move-result-object v3

    .line 240
    invoke-interface {v3}, Lcvy;->a()Ljava/lang/String;

    move-result-object v4

    .line 241
    invoke-interface {v3}, Lcvy;->b()Ljava/lang/String;

    move-result-object v3

    .line 242
    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    invoke-static {v3, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 244
    iget-object v3, p0, Laix;->a:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    iput-object v4, v3, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->a:Ljava/lang/String;

    .line 238
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 247
    :cond_1
    iget-object v0, p0, Laix;->a:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    iput-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->mMyCircles:Ljava/util/Map;

    .line 248
    iget-object v0, p0, Laix;->a:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->b(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;)Lbxq;

    move-result-object v0

    invoke-virtual {v0}, Lbxq;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 250
    invoke-virtual {p1}, Lcvz;->d()V

    .line 251
    return-void

    .line 250
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Lcvz;->d()V

    throw v0
.end method
