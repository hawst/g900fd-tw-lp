.class public final Lcaw;
.super Lcax;
.source "PG"


# static fields
.field private static final a:Z


# instance fields
.field private final b:Landroid/widget/TextView;

.field private final c:Lcom/google/android/apps/hangouts/views/AvatarView;

.field private final d:Lcom/google/android/apps/hangouts/views/ContactDetailItemView;

.field private final e:Landroid/widget/TextView;

.field private final f:Lyj;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:I

.field private m:I

.field private n:I

.field private o:Laea;

.field private p:Ladm;

.field private q:Ljava/lang/String;

.field private final r:Landroid/text/SpannableStringBuilder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lbys;->s:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lcaw;->a:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lyj;)V
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcaw;-><init>(Landroid/content/Context;Lyj;B)V

    .line 73
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lyj;B)V
    .locals 2

    .prologue
    .line 76
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcax;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 69
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v0, p0, Lcaw;->r:Landroid/text/SpannableStringBuilder;

    .line 77
    iput-object p2, p0, Lcaw;->f:Lyj;

    .line 79
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lf;->eq:I

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 80
    sget v0, Lg;->eH:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcaw;->b:Landroid/widget/TextView;

    .line 81
    sget v0, Lg;->E:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/AvatarView;

    iput-object v0, p0, Lcaw;->c:Lcom/google/android/apps/hangouts/views/AvatarView;

    .line 83
    sget v0, Lg;->aB:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/ContactDetailItemView;

    iput-object v0, p0, Lcaw;->d:Lcom/google/android/apps/hangouts/views/ContactDetailItemView;

    .line 84
    sget v0, Lg;->hy:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcaw;->e:Landroid/widget/TextView;

    .line 85
    return-void
.end method

.method private a(I)Z
    .locals 1

    .prologue
    .line 257
    iget v0, p0, Lcaw;->n:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()Z
    .locals 2

    .prologue
    .line 150
    iget v0, p0, Lcaw;->m:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/16 v0, 0x10

    .line 151
    invoke-direct {p0, v0}, Lcaw;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Laea;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcaw;->o:Laea;

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 245
    if-nez p1, :cond_0

    .line 246
    const/4 v0, 0x0

    iput-object v0, p0, Lcaw;->q:Ljava/lang/String;

    .line 250
    :goto_0
    return-void

    .line 248
    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcaw;->q:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILaea;Ladm;I)V
    .locals 5

    .prologue
    .line 119
    iput-object p1, p0, Lcaw;->g:Ljava/lang/String;

    .line 120
    iput-object p2, p0, Lcaw;->h:Ljava/lang/String;

    .line 121
    iput-object p3, p0, Lcaw;->i:Ljava/lang/String;

    .line 122
    iput-object p4, p0, Lcaw;->j:Ljava/lang/String;

    .line 123
    iput-object p7, p0, Lcaw;->o:Laea;

    .line 124
    iput p5, p0, Lcaw;->l:I

    .line 125
    iput p6, p0, Lcaw;->m:I

    .line 126
    const/4 v0, 0x0

    iput-object v0, p0, Lcaw;->k:Ljava/lang/String;

    .line 127
    iput-object p8, p0, Lcaw;->p:Ladm;

    .line 128
    iput p9, p0, Lcaw;->n:I

    .line 130
    sget-boolean v0, Lcaw;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Redrawing call contact item: mName="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcaw;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcaw;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcaw;->b:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcaw;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lcaw;->h:Ljava/lang/String;

    iget-object v2, p0, Lcaw;->r:Landroid/text/SpannableStringBuilder;

    iget-object v3, p0, Lcaw;->q:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v2, v3}, Lcaw;->a(Landroid/widget/TextView;Ljava/lang/String;Landroid/text/SpannableStringBuilder;Ljava/lang/String;)V

    :goto_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcaw;->a(I)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcaw;->c:Lcom/google/android/apps/hangouts/views/AvatarView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/AvatarView;->setVisibility(I)V

    iget-object v0, p0, Lcaw;->c:Lcom/google/android/apps/hangouts/views/AvatarView;

    iget-object v1, p0, Lcaw;->j:Ljava/lang/String;

    iget-object v2, p0, Lcaw;->f:Lyj;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/hangouts/views/AvatarView;->a(Ljava/lang/String;Lyj;)V

    iget-object v1, p0, Lcaw;->c:Lcom/google/android/apps/hangouts/views/AvatarView;

    iget v0, p0, Lcaw;->l:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_4

    const/4 v0, 0x3

    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/android/apps/hangouts/views/AvatarView;->a(I)V

    :goto_2
    const/4 v0, 0x0

    iget v1, p0, Lcaw;->l:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_6

    iget-object v1, p0, Lcaw;->k:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    new-instance v0, Laec;

    iget-object v1, p0, Lcaw;->k:Ljava/lang/String;

    invoke-direct {v0, v1}, Laec;-><init>(Ljava/lang/String;)V

    :cond_1
    :goto_3
    if-eqz v0, :cond_2

    iget-object v1, p0, Lcaw;->d:Lcom/google/android/apps/hangouts/views/ContactDetailItemView;

    iget-object v2, p0, Lcaw;->q:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/hangouts/views/ContactDetailItemView;->a(Laee;Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcaw;->p:Ladm;

    if-eqz v0, :cond_9

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcaw;->a(I)Z

    move-result v0

    if-eqz v0, :cond_9

    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->aW:I

    iget-object v1, p0, Lcaw;->p:Ladm;

    invoke-virtual {v1}, Ladm;->d()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    const-string v1, "Babel"

    const-string v2, "unsupported call type!"

    invoke-static {v1, v2}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    :goto_4
    iget-object v1, p0, Lcaw;->e:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcaw;->e:Landroid/widget/TextView;

    iget-object v2, p0, Lcaw;->p:Ladm;

    invoke-virtual {v2}, Ladm;->c()J

    move-result-wide v2

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lf;->a(JZ)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcaw;->e:Landroid/widget/TextView;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    :goto_5
    invoke-virtual {p0}, Lcaw;->invalidate()V

    .line 131
    return-void

    .line 130
    :cond_3
    iget-object v0, p0, Lcaw;->b:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_4
    const/4 v0, 0x2

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcaw;->c:Lcom/google/android/apps/hangouts/views/AvatarView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/AvatarView;->setVisibility(I)V

    iget-object v0, p0, Lcaw;->c:Lcom/google/android/apps/hangouts/views/AvatarView;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/hangouts/views/AvatarView;->a(Ljava/lang/String;Lyj;)V

    goto :goto_2

    :cond_6
    iget-object v1, p0, Lcaw;->o:Laea;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcaw;->o:Laea;

    invoke-virtual {v1}, Laea;->h()Laeh;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-direct {p0}, Lcaw;->h()Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v1, v1, Laeh;->a:Ljava/lang/String;

    invoke-static {v1}, Lbzd;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lbzd;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    new-instance v2, Ljava/util/Locale;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Laec;

    invoke-virtual {v2}, Ljava/util/Locale;->getDisplayCountry()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Laec;-><init>(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_7
    move-object v0, v1

    goto/16 :goto_3

    :cond_8
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Call contact item "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcaw;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " doesn\'t have phone number!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    :pswitch_0
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->aW:I

    goto/16 :goto_4

    :pswitch_1
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->aY:I

    goto/16 :goto_4

    :pswitch_2
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->aX:I

    goto/16 :goto_4

    :cond_9
    iget-object v0, p0, Lcaw;->e:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcaw;->e:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcaw;->e:Landroid/widget/TextView;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto/16 :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcaw;->g:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcaw;->p:Ladm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcaw;->p:Ladm;

    invoke-virtual {v0}, Ladm;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    invoke-direct {p0}, Lcaw;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcaw;->h:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcaw;->i:Ljava/lang/String;

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcaw;->j:Ljava/lang/String;

    return-object v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 142
    iget v0, p0, Lcaw;->l:I

    return v0
.end method
