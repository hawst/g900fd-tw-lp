.class public final Lbdh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/io/Serializable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lbdh;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public a:I

.field public b:Lbdk;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/Boolean;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Z

.field public n:Z

.field public o:Z

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field public r:Z

.field public s:Z

.field public t:I

.field public u:Ljava/lang/String;

.field public v:Z

.field private w:Ljava/lang/String;

.field private x:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lbcn;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 647
    new-instance v0, Lbdi;

    invoke-direct {v0}, Lbdi;-><init>()V

    sput-object v0, Lbdh;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-boolean v0, p0, Lbdh;->m:Z

    .line 64
    iput-boolean v0, p0, Lbdh;->n:Z

    .line 68
    iput-boolean v0, p0, Lbdh;->o:Z

    .line 82
    iput-boolean v0, p0, Lbdh;->r:Z

    .line 85
    iput-boolean v0, p0, Lbdh;->s:Z

    .line 87
    iput v0, p0, Lbdh;->t:I

    .line 151
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lbdh;-><init>()V

    return-void
.end method

.method public constructor <init>(Ldro;Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x3

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 330
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-boolean v3, p0, Lbdh;->m:Z

    .line 64
    iput-boolean v3, p0, Lbdh;->n:Z

    .line 68
    iput-boolean v3, p0, Lbdh;->o:Z

    .line 82
    iput-boolean v3, p0, Lbdh;->r:Z

    .line 85
    iput-boolean v3, p0, Lbdh;->s:Z

    .line 87
    iput v3, p0, Lbdh;->t:I

    .line 331
    iget-object v0, p1, Ldro;->b:Ljava/lang/Integer;

    .line 332
    invoke-static {v0, v3}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    .line 333
    packed-switch v0, :pswitch_data_0

    .line 363
    iput v3, p0, Lbdh;->a:I

    .line 364
    new-instance v0, Lbdk;

    iget-object v4, p1, Ldro;->c:Ldui;

    invoke-direct {v0, v4, v1}, Lbdk;-><init>(Ldui;Ljava/lang/String;)V

    iput-object v0, p0, Lbdh;->b:Lbdk;

    .line 368
    :goto_0
    iget-object v0, p1, Ldro;->d:Ldrq;

    if-eqz v0, :cond_3

    .line 369
    iget-object v4, p1, Ldro;->d:Ldrq;

    .line 370
    iget-object v0, v4, Ldrq;->c:Ljava/lang/String;

    iput-object v0, p0, Lbdh;->e:Ljava/lang/String;

    .line 371
    iget-object v0, v4, Ldrq;->d:Ljava/lang/String;

    iput-object v0, p0, Lbdh;->f:Ljava/lang/String;

    .line 372
    iget-object v0, p0, Lbdh;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 373
    iget-object v0, p0, Lbdh;->e:Ljava/lang/String;

    if-nez v0, :cond_a

    move-object v0, v1

    :cond_0
    :goto_1
    iput-object v0, p0, Lbdh;->f:Ljava/lang/String;

    .line 375
    :cond_1
    iget-object v0, v4, Ldrq;->e:Ljava/lang/String;

    iput-object v0, p0, Lbdh;->h:Ljava/lang/String;

    .line 376
    iget-object v0, v4, Ldrq;->b:Ljava/lang/Integer;

    invoke-static {v0, v3}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    if-ne v0, v8, :cond_c

    move v0, v2

    :goto_2
    iput-boolean v0, p0, Lbdh;->n:Z

    .line 378
    iget-object v0, v4, Ldrq;->b:Ljava/lang/Integer;

    invoke-static {v0, v3}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    if-eq v0, v2, :cond_2

    iget-boolean v0, p0, Lbdh;->n:Z

    if-eqz v0, :cond_d

    :cond_2
    move v0, v2

    :goto_3
    iput-boolean v0, p0, Lbdh;->m:Z

    .line 380
    iput-object v1, p0, Lbdh;->p:Ljava/lang/String;

    .line 382
    iget-object v0, v4, Ldrq;->h:Ljava/lang/String;

    iput-object v0, p0, Lbdh;->j:Ljava/lang/String;

    .line 383
    iget-object v0, v4, Ldrq;->i:Ljava/lang/String;

    iput-object v0, p0, Lbdh;->k:Ljava/lang/String;

    .line 384
    iget-object v0, v4, Ldrq;->j:Ljava/lang/String;

    iput-object v0, p0, Lbdh;->l:Ljava/lang/String;

    .line 385
    iget-object v0, v4, Ldrq;->k:Ljava/lang/Boolean;

    invoke-static {v0, v3}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    iput-boolean v0, p0, Lbdh;->v:Z

    .line 388
    :cond_3
    iget-object v0, p1, Ldro;->e:Ldri;

    if-eqz v0, :cond_6

    .line 389
    iget-object v0, p1, Ldro;->e:Ldri;

    .line 390
    iget-object v4, v0, Ldri;->g:Ljava/lang/Boolean;

    invoke-static {v4, v3}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v4

    iput-boolean v4, p0, Lbdh;->o:Z

    .line 392
    iget-object v4, v0, Ldri;->h:Ljava/lang/Integer;

    invoke-static {v4, v3}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v4

    .line 393
    if-eq v4, v8, :cond_4

    if-ne v4, v7, :cond_5

    :cond_4
    move v3, v2

    :cond_5
    iput-boolean v3, p0, Lbdh;->s:Z

    .line 395
    iget-object v0, v0, Ldri;->f:Ljava/lang/String;

    iput-object v0, p0, Lbdh;->u:Ljava/lang/String;

    .line 398
    :cond_6
    iget-object v0, p1, Ldro;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    iget-object v1, p1, Ldro;->h:Ljava/lang/Boolean;

    :cond_7
    iput-object v1, p0, Lbdh;->i:Ljava/lang/Boolean;

    .line 399
    return-void

    .line 335
    :pswitch_0
    iput v2, p0, Lbdh;->a:I

    .line 336
    new-instance v0, Lbdk;

    iget-object v4, p1, Ldro;->c:Ldui;

    invoke-direct {v0, v4, v1}, Lbdk;-><init>(Ldui;Ljava/lang/String;)V

    iput-object v0, p0, Lbdh;->b:Lbdk;

    goto/16 :goto_0

    .line 341
    :pswitch_1
    iget-object v0, p1, Ldro;->d:Ldrq;

    if-eqz v0, :cond_e

    iget-object v0, p1, Ldro;->d:Ldrq;

    iget-object v0, v0, Ldrq;->g:[Ljava/lang/String;

    if-eqz v0, :cond_e

    iget-object v0, p1, Ldro;->d:Ldrq;

    iget-object v0, v0, Ldrq;->g:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_e

    iget-object v0, p1, Ldro;->d:Ldrq;

    iget-object v0, v0, Ldrq;->g:[Ljava/lang/String;

    aget-object v0, v0, v3

    .line 343
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 344
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v4, p1, Ldro;->d:Ldrq;

    iget-object v4, v4, Ldrq;->g:[Ljava/lang/String;

    array-length v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v0, v4}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 345
    iget-object v0, p1, Ldro;->d:Ldrq;

    iget-object v0, v0, Ldrq;->g:[Ljava/lang/String;

    aget-object v0, v0, v3

    .line 347
    :goto_4
    iput v7, p0, Lbdh;->a:I

    .line 348
    iget-object v4, p1, Ldro;->c:Ldui;

    if-eqz v4, :cond_8

    .line 349
    new-instance v4, Lbdk;

    iget-object v5, p1, Ldro;->c:Ldui;

    iget-object v5, v5, Ldui;->c:Ljava/lang/String;

    iget-object v6, p1, Ldro;->c:Ldui;

    iget-object v6, v6, Ldui;->b:Ljava/lang/String;

    invoke-direct {v4, v5, v6, v0}, Lbdk;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v4, p0, Lbdh;->b:Lbdk;

    .line 354
    :goto_5
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_9

    :goto_6
    iput-object v0, p0, Lbdh;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 352
    :cond_8
    invoke-static {v0}, Lbdk;->d(Ljava/lang/String;)Lbdk;

    move-result-object v4

    iput-object v4, p0, Lbdh;->b:Lbdk;

    goto :goto_5

    :cond_9
    move-object v0, p2

    .line 354
    goto :goto_6

    .line 358
    :pswitch_2
    iput v7, p0, Lbdh;->a:I

    .line 359
    new-instance v0, Lbdk;

    iget-object v4, p1, Ldro;->c:Ldui;

    invoke-direct {v0, v4, v1}, Lbdk;-><init>(Ldui;Ljava/lang/String;)V

    iput-object v0, p0, Lbdh;->b:Lbdk;

    goto/16 :goto_0

    .line 373
    :cond_a
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_b

    move-object v0, v1

    goto/16 :goto_1

    :cond_b
    const/16 v5, 0x20

    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    if-ltz v5, :cond_0

    invoke-virtual {v0, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_c
    move v0, v3

    .line 376
    goto/16 :goto_2

    :cond_d
    move v0, v3

    .line 378
    goto/16 :goto_3

    :cond_e
    move-object v0, p2

    goto :goto_4

    .line 333
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public static a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)Lbdh;
    .locals 2

    .prologue
    .line 277
    new-instance v0, Lbdh;

    invoke-direct {v0}, Lbdh;-><init>()V

    .line 278
    new-instance v1, Lbdk;

    invoke-direct {v1, p1, p2}, Lbdk;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, v0, Lbdh;->b:Lbdk;

    .line 279
    iput p0, v0, Lbdh;->a:I

    .line 280
    iput-object p3, v0, Lbdh;->d:Ljava/lang/String;

    .line 281
    iput-object p4, v0, Lbdh;->c:Ljava/lang/String;

    .line 282
    iput-object p5, v0, Lbdh;->e:Ljava/lang/String;

    .line 283
    iput-object p6, v0, Lbdh;->f:Ljava/lang/String;

    .line 284
    iput-object p7, v0, Lbdh;->g:Ljava/lang/String;

    .line 285
    iput-object p8, v0, Lbdh;->h:Ljava/lang/String;

    .line 287
    iput-object p9, v0, Lbdh;->w:Ljava/lang/String;

    .line 288
    iput-object p10, v0, Lbdh;->i:Ljava/lang/Boolean;

    .line 289
    const/4 v1, 0x0

    iput-object v1, v0, Lbdh;->p:Ljava/lang/String;

    .line 290
    invoke-static {v0}, Lbdh;->a(Lbdh;)V

    .line 291
    return-object v0
.end method

.method public static a(Lbdk;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lbdh;
    .locals 2

    .prologue
    .line 224
    new-instance v0, Lbdh;

    invoke-direct {v0}, Lbdh;-><init>()V

    .line 225
    iput-object p0, v0, Lbdh;->b:Lbdk;

    .line 226
    iput-object p1, v0, Lbdh;->e:Ljava/lang/String;

    .line 227
    iput-object p2, v0, Lbdh;->f:Ljava/lang/String;

    .line 228
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    invoke-virtual {v0, p1}, Lbdh;->b(Ljava/lang/String;)V

    .line 229
    iput-object p4, v0, Lbdh;->h:Ljava/lang/String;

    .line 230
    iput-object p5, v0, Lbdh;->p:Ljava/lang/String;

    .line 231
    const/4 v1, 0x1

    iput v1, v0, Lbdh;->a:I

    .line 232
    return-object v0

    :cond_0
    move-object p1, p3

    .line 228
    goto :goto_0
.end method

.method public static a(Lbdk;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)Lbdh;
    .locals 2

    .prologue
    .line 255
    new-instance v1, Lbdh;

    invoke-direct {v1}, Lbdh;-><init>()V

    .line 256
    iput-object p0, v1, Lbdh;->b:Lbdk;

    .line 257
    iput-object p1, v1, Lbdh;->c:Ljava/lang/String;

    .line 258
    iput-object p2, v1, Lbdh;->e:Ljava/lang/String;

    .line 259
    iput-object p3, v1, Lbdh;->f:Ljava/lang/String;

    .line 260
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 261
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object p2, p1

    .line 263
    :cond_0
    :goto_0
    invoke-virtual {v1, p2}, Lbdh;->b(Ljava/lang/String;)V

    .line 264
    iput-object p5, v1, Lbdh;->h:Ljava/lang/String;

    .line 265
    iput-object p6, v1, Lbdh;->p:Ljava/lang/String;

    .line 266
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput v0, v1, Lbdh;->a:I

    .line 268
    iput-object p7, v1, Lbdh;->i:Ljava/lang/Boolean;

    .line 269
    invoke-static {v1}, Lbdh;->a(Lbdh;)V

    .line 270
    return-object v1

    .line 266
    :cond_1
    const/4 v0, 0x3

    goto :goto_1

    :cond_2
    move-object p2, p4

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lbdh;
    .locals 2

    .prologue
    .line 170
    new-instance v0, Lbdh;

    invoke-direct {v0}, Lbdh;-><init>()V

    .line 171
    iput-object p0, v0, Lbdh;->d:Ljava/lang/String;

    .line 172
    iput-object p1, v0, Lbdh;->e:Ljava/lang/String;

    .line 173
    invoke-virtual {v0, p1}, Lbdh;->b(Ljava/lang/String;)V

    .line 174
    const/4 v1, 0x2

    iput v1, v0, Lbdh;->a:I

    .line 175
    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lbdh;
    .locals 3

    .prologue
    .line 180
    new-instance v0, Lbdh;

    invoke-direct {v0}, Lbdh;-><init>()V

    .line 184
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 185
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 186
    invoke-static {p0}, Lbzd;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 192
    :cond_0
    :goto_0
    iput-object p1, v0, Lbdh;->e:Ljava/lang/String;

    .line 193
    invoke-static {p0}, Lbdk;->d(Ljava/lang/String;)Lbdk;

    move-result-object v1

    iput-object v1, v0, Lbdh;->b:Lbdk;

    .line 194
    iput-object p0, v0, Lbdh;->c:Ljava/lang/String;

    .line 195
    const/4 v1, 0x3

    iput v1, v0, Lbdh;->a:I

    .line 196
    iput-object p2, v0, Lbdh;->h:Ljava/lang/String;

    .line 197
    invoke-virtual {v0, p1}, Lbdh;->b(Ljava/lang/String;)V

    .line 198
    return-object v0

    .line 188
    :cond_1
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lh;->nA:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)Lbdh;
    .locals 8

    .prologue
    .line 238
    if-eqz p2, :cond_1

    .line 239
    if-eqz p4, :cond_0

    :goto_0
    invoke-static {p2, p4}, Lbdh;->a(Ljava/lang/String;Ljava/lang/String;)Lbdh;

    move-result-object v0

    .line 245
    :goto_1
    return-object v0

    :cond_0
    move-object p4, p6

    .line 239
    goto :goto_0

    .line 241
    :cond_1
    if-nez p0, :cond_2

    if-eqz p1, :cond_3

    .line 242
    :cond_2
    new-instance v0, Lbdk;

    invoke-direct {v0, p0, p1, p3}, Lbdk;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, p3

    move-object v2, p4

    move-object v3, p5

    move-object v4, p6

    move-object v5, p7

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    invoke-static/range {v0 .. v7}, Lbdh;->a(Lbdk;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)Lbdh;

    move-result-object v0

    goto :goto_1

    .line 245
    :cond_3
    new-instance v0, Lbdh;

    invoke-direct {v0}, Lbdh;-><init>()V

    iput-object p6, v0, Lbdh;->e:Ljava/lang/String;

    const/4 v1, 0x0

    iput v1, v0, Lbdh;->a:I

    invoke-virtual {v0, p6}, Lbdh;->b(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;I)Lbdh;
    .locals 2

    .prologue
    .line 209
    new-instance v0, Lbdh;

    invoke-direct {v0}, Lbdh;-><init>()V

    .line 210
    iput-object p4, v0, Lbdh;->e:Ljava/lang/String;

    .line 211
    new-instance v1, Lbdk;

    invoke-direct {v1, p0}, Lbdk;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lbdh;->b:Lbdk;

    .line 212
    const/4 v1, 0x3

    iput v1, v0, Lbdh;->a:I

    .line 213
    iput-object p1, v0, Lbdh;->q:Ljava/lang/String;

    .line 214
    iput-object p0, v0, Lbdh;->c:Ljava/lang/String;

    .line 215
    iput-boolean p2, v0, Lbdh;->r:Z

    .line 216
    iput-object p3, v0, Lbdh;->h:Ljava/lang/String;

    .line 217
    iput p5, v0, Lbdh;->t:I

    .line 218
    invoke-virtual {v0, p0}, Lbdh;->b(Ljava/lang/String;)V

    .line 219
    return-object v0
.end method

.method public static a(Landroid/database/Cursor;)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    .line 317
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 318
    if-eqz p0, :cond_3

    .line 319
    :goto_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 320
    if-nez p0, :cond_0

    move-object v0, v11

    :goto_1
    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const/16 v0, 0x8

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/4 v1, 0x1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0xa

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x4

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x5

    invoke-interface {p0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    const/4 v7, 0x6

    invoke-interface {p0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    :goto_2
    const/4 v8, 0x7

    invoke-interface {p0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/16 v9, 0xb

    invoke-interface {p0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/16 v10, 0x9

    invoke-interface {p0, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    if-eqz v10, :cond_2

    sget-object v10, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_3
    invoke-static/range {v0 .. v10}, Lbdh;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)Lbdh;

    move-result-object v0

    goto :goto_1

    :cond_1
    move-object v7, v11

    goto :goto_2

    :cond_2
    move-object v10, v11

    goto :goto_3

    .line 323
    :cond_3
    return-object v12
.end method

.method public static a([Ldro;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ldro;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 404
    if-nez p0, :cond_0

    .line 419
    :goto_0
    return-object v0

    .line 408
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 409
    array-length v4, p0

    move v2, v3

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v5, p0, v2

    .line 411
    iget-object v6, v5, Ldro;->g:Ljava/lang/Boolean;

    invoke-static {v6, v3}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 413
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 410
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 416
    :cond_1
    new-instance v6, Lbdh;

    invoke-direct {v6, v5, v0}, Lbdh;-><init>(Ldro;Ljava/lang/String;)V

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 418
    :cond_2
    invoke-static {v1}, Lbdh;->a(Ljava/util/List;)V

    move-object v0, v1

    .line 419
    goto :goto_0
.end method

.method public static a([Ldsu;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ldsu;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 446
    if-nez p0, :cond_0

    .line 455
    :goto_0
    return-object v0

    .line 450
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 451
    array-length v3, p0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_1

    aget-object v4, p0, v2

    .line 452
    new-instance v5, Lbdh;

    iget-object v4, v4, Ldsu;->b:Ldro;

    invoke-direct {v5, v4, v0}, Lbdh;-><init>(Ldro;Ljava/lang/String;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 451
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 454
    :cond_1
    invoke-static {v1}, Lbdh;->a(Ljava/util/List;)V

    move-object v0, v1

    .line 455
    goto :goto_0
.end method

.method private static a(Lbdh;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 603
    if-nez p0, :cond_1

    .line 617
    :cond_0
    :goto_0
    return-void

    .line 606
    :cond_1
    const-string v0, "babel_enable_selective_participant_entity_checking"

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 609
    iget v0, p0, Lbdh;->a:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lbdh;->c:Ljava/lang/String;

    .line 610
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 612
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Encountered participant with participantType PARTICIPANT_TYPE_PERSON, but contained a phoneNumber like: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 614
    invoke-virtual {p0}, Lbdh;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 612
    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 621
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    .line 622
    invoke-static {v0}, Lbdh;->a(Lbdh;)V

    goto :goto_0

    .line 624
    :cond_0
    return-void
.end method

.method public static a([Ldro;Lbcn;)[Lbdh;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 424
    if-eqz p0, :cond_0

    array-length v0, p0

    if-nez v0, :cond_1

    .line 441
    :cond_0
    :goto_0
    return-object v1

    .line 428
    :cond_1
    array-length v0, p0

    new-array v2, v0, [Lbdh;

    .line 429
    if-eqz p1, :cond_2

    iget-object v0, p1, Lbcn;->d:Ljava/lang/String;

    .line 431
    :goto_1
    array-length v6, p0

    move v3, v4

    move v5, v4

    :goto_2
    if-ge v3, v6, :cond_4

    aget-object v7, p0, v3

    .line 432
    iget-object v8, v7, Ldro;->g:Ljava/lang/Boolean;

    invoke-static {v8, v4}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 434
    aput-object v1, v2, v5

    .line 438
    :goto_3
    add-int/lit8 v5, v5, 0x1

    .line 431
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_2
    move-object v0, v1

    .line 429
    goto :goto_1

    .line 436
    :cond_3
    new-instance v8, Lbdh;

    invoke-direct {v8, v7, v0}, Lbdh;-><init>(Ldro;Ljava/lang/String;)V

    aput-object v8, v2, v5

    goto :goto_3

    .line 440
    :cond_4
    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lbdh;->a(Ljava/util/List;)V

    move-object v1, v2

    .line 441
    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lbdh;->b:Lbdk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbdh;->b:Lbdk;

    iget-object v0, v0, Lbdk;->a:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Z)Ljava/lang/String;
    .locals 2

    .prologue
    .line 498
    if-eqz p1, :cond_0

    iget-object v0, p0, Lbdh;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 499
    iget-object v0, p0, Lbdh;->f:Ljava/lang/String;

    .line 505
    :goto_0
    return-object v0

    .line 500
    :cond_0
    iget-object v0, p0, Lbdh;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lbdh;->e:Ljava/lang/String;

    iget-object v1, p0, Lbdh;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 501
    iget-object v0, p0, Lbdh;->e:Ljava/lang/String;

    goto :goto_0

    .line 502
    :cond_1
    invoke-virtual {p0}, Lbdh;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 503
    iget-object v0, p0, Lbdh;->c:Ljava/lang/String;

    invoke-static {v0}, Lbzd;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 505
    :cond_2
    iget-object v0, p0, Lbdh;->g:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Lbcn;)V
    .locals 1

    .prologue
    .line 536
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbdh;->x:Ljava/util/ArrayList;

    .line 537
    iget-object v0, p0, Lbdh;->x:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 538
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 510
    iput-object p1, p0, Lbdh;->w:Ljava/lang/String;

    .line 511
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lbdh;->b:Lbdk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbdh;->b:Lbdk;

    iget-object v0, v0, Lbdk;->b:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 518
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 519
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    .line 520
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->nD:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbdh;->g:Ljava/lang/String;

    .line 524
    :goto_0
    return-void

    .line 522
    :cond_0
    iput-object p1, p0, Lbdh;->g:Ljava/lang/String;

    goto :goto_0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 527
    if-eqz p1, :cond_0

    .line 528
    iput-object p1, p0, Lbdh;->e:Ljava/lang/String;

    .line 530
    :cond_0
    if-eqz p2, :cond_1

    .line 531
    iput-object p2, p0, Lbdh;->h:Ljava/lang/String;

    .line 533
    :cond_1
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 459
    iget-object v0, p0, Lbdh;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 463
    iget-object v0, p0, Lbdh;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 628
    const/4 v0, 0x0

    return v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 467
    iget-object v0, p0, Lbdh;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    iget-object v1, p0, Lbdh;->c:Ljava/lang/String;

    .line 468
    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 675
    instance-of v1, p1, Lbdh;

    if-nez v1, :cond_1

    .line 683
    :cond_0
    :goto_0
    return v0

    .line 679
    :cond_1
    check-cast p1, Lbdh;

    .line 680
    iget-object v1, p0, Lbdh;->b:Lbdk;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lbdh;->b:Lbdk;

    if-eqz v1, :cond_0

    .line 683
    iget-object v0, p0, Lbdh;->b:Lbdk;

    iget-object v1, p1, Lbdh;->b:Lbdk;

    invoke-virtual {v0, v1}, Lbdk;->a(Lbdk;)Z

    move-result v0

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 472
    iget-object v0, p0, Lbdh;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbdh;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isEmergencyNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 514
    iget-object v0, p0, Lbdh;->w:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ldth;
    .locals 7

    .prologue
    .line 545
    new-instance v2, Ldth;

    invoke-direct {v2}, Ldth;-><init>()V

    .line 546
    invoke-virtual {p0}, Lbdh;->a()Ljava/lang/String;

    move-result-object v0

    .line 547
    if-eqz v0, :cond_4

    .line 548
    const-string v1, "g:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 549
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 551
    :cond_0
    iput-object v0, v2, Ldth;->b:Ljava/lang/String;

    .line 559
    :cond_1
    :goto_0
    iget-object v0, p0, Lbdh;->g:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 560
    iget-object v0, p0, Lbdh;->g:Ljava/lang/String;

    iput-object v0, v2, Ldth;->f:Ljava/lang/String;

    .line 563
    :cond_2
    iget-object v0, p0, Lbdh;->x:Ljava/util/ArrayList;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lbdh;->x:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_8

    .line 564
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 566
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lbdh;->x:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 567
    iget-object v0, p0, Lbdh;->x:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbcn;

    .line 568
    iget-object v4, v0, Lbcn;->d:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 569
    new-instance v4, Ldti;

    invoke-direct {v4}, Ldti;-><init>()V

    .line 571
    new-instance v5, Ldtl;

    invoke-direct {v5}, Ldtl;-><init>()V

    iput-object v5, v4, Ldti;->b:Ldtl;

    .line 573
    iget-object v5, v4, Ldti;->b:Ldtl;

    new-instance v6, Leir;

    invoke-direct {v6}, Leir;-><init>()V

    iput-object v6, v5, Ldtl;->b:Leir;

    .line 574
    iget-object v5, v4, Ldti;->b:Ldtl;

    iget-object v5, v5, Ldtl;->b:Leir;

    iget-object v0, v0, Lbcn;->d:Ljava/lang/String;

    iput-object v0, v5, Leir;->b:Ljava/lang/String;

    .line 575
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 566
    :cond_3
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 552
    :cond_4
    iget-object v0, p0, Lbdh;->d:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 553
    iget-object v0, p0, Lbdh;->d:Ljava/lang/String;

    iput-object v0, v2, Ldth;->c:Ljava/lang/String;

    goto :goto_0

    .line 554
    :cond_5
    iget-object v0, p0, Lbdh;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 555
    new-instance v0, Leir;

    invoke-direct {v0}, Leir;-><init>()V

    iput-object v0, v2, Ldth;->e:Leir;

    .line 556
    iget-object v0, v2, Ldth;->e:Leir;

    iget-object v1, p0, Lbdh;->c:Ljava/lang/String;

    invoke-static {v1}, Lbzd;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Leir;->b:Ljava/lang/String;

    goto :goto_0

    .line 576
    :cond_6
    iget-object v4, v0, Lbcn;->c:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 577
    new-instance v4, Ldti;

    invoke-direct {v4}, Ldti;-><init>()V

    .line 579
    new-instance v5, Ldtj;

    invoke-direct {v5}, Ldtj;-><init>()V

    iput-object v5, v4, Ldti;->c:Ldtj;

    .line 581
    iget-object v5, v4, Ldti;->c:Ldtj;

    iget-object v0, v0, Lbcn;->c:Ljava/lang/String;

    iput-object v0, v5, Ldtj;->b:Ljava/lang/String;

    .line 582
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 586
    :cond_7
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ldti;

    iput-object v0, v2, Ldth;->g:[Ldti;

    .line 587
    iget-object v0, v2, Ldth;->g:[Ldti;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 590
    :cond_8
    return-object v2
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 689
    iget-object v0, p0, Lbdh;->b:Lbdk;

    if-nez v0, :cond_0

    .line 690
    const/4 v0, 0x0

    .line 692
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lbdh;->b:Lbdk;

    invoke-virtual {v0}, Lbdk;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 698
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ParticipantEntity {id:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lbdh;->b:Lbdk;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", type:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lbdh;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " phoneNumber:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbdh;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " displayName:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbdh;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " firstName:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbdh;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " fallbackName:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbdh;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " avatar:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbdh;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " blocked:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbdh;->i:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 633
    iget v0, p0, Lbdh;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 634
    iget-object v0, p0, Lbdh;->b:Lbdk;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 635
    iget-object v0, p0, Lbdh;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 636
    iget-object v0, p0, Lbdh;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 637
    iget-object v0, p0, Lbdh;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 638
    iget-object v0, p0, Lbdh;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 639
    iget-object v0, p0, Lbdh;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 640
    iget-object v0, p0, Lbdh;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 641
    iget-object v0, p0, Lbdh;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 642
    iget-object v0, p0, Lbdh;->q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 643
    iget-boolean v0, p0, Lbdh;->r:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 644
    iget v0, p0, Lbdh;->t:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 645
    return-void

    .line 643
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
