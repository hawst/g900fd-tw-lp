.class public abstract Lalw;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<H:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field protected a:Landroid/view/ViewGroup;

.field protected b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TH;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;ILjava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "ITH;)V"
        }
    .end annotation

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lalw;->a:Landroid/view/ViewGroup;

    .line 15
    iput-object p3, p0, Lalw;->b:Ljava/lang/Object;

    .line 16
    invoke-virtual {p0}, Lalw;->c()V

    .line 17
    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 2

    .prologue
    .line 24
    if-eqz p1, :cond_0

    .line 25
    invoke-virtual {p0}, Lalw;->b()V

    .line 27
    :cond_0
    if-eqz p1, :cond_1

    const/4 v0, 0x0

    .line 28
    :goto_0
    iget-object v1, p0, Lalw;->a:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 29
    return-void

    .line 27
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method protected abstract a()Z
.end method

.method protected abstract b()V
.end method

.method protected abstract c()V
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0}, Lalw;->a()Z

    move-result v0

    return v0
.end method
