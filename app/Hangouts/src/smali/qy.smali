.class public final Lqy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Lcom/android/ex/photo/views/PhotoView;

.field private b:F

.field private c:F

.field private d:J

.field private e:Z

.field private f:Z


# direct methods
.method public constructor <init>(Lcom/android/ex/photo/views/PhotoView;)V
    .locals 2

    .prologue
    .line 1259
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1260
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lqy;->d:J

    .line 1261
    iput-object p1, p0, Lqy;->a:Lcom/android/ex/photo/views/PhotoView;

    .line 1262
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 1284
    const/4 v0, 0x0

    iput-boolean v0, p0, Lqy;->e:Z

    .line 1285
    const/4 v0, 0x1

    iput-boolean v0, p0, Lqy;->f:Z

    .line 1286
    return-void
.end method

.method public a(FF)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1268
    iget-boolean v2, p0, Lqy;->e:Z

    if-eqz v2, :cond_0

    .line 1277
    :goto_0
    return v0

    .line 1271
    :cond_0
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lqy;->d:J

    .line 1272
    iput p1, p0, Lqy;->b:F

    .line 1273
    iput p2, p0, Lqy;->c:F

    .line 1274
    iput-boolean v0, p0, Lqy;->f:Z

    .line 1275
    iput-boolean v1, p0, Lqy;->e:Z

    .line 1276
    iget-object v0, p0, Lqy;->a:Lcom/android/ex/photo/views/PhotoView;

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, p0, v2, v3}, Lcom/android/ex/photo/views/PhotoView;->postDelayed(Ljava/lang/Runnable;J)Z

    move v0, v1

    .line 1277
    goto :goto_0
.end method

.method public run()V
    .locals 11

    .prologue
    const-wide/16 v8, -0x1

    const/high16 v7, 0x41200000    # 10.0f

    const/high16 v6, 0x42c80000    # 100.0f

    const/4 v1, 0x0

    .line 1291
    iget-boolean v0, p0, Lqy;->f:Z

    if-eqz v0, :cond_1

    .line 1332
    :cond_0
    :goto_0
    return-void

    .line 1296
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1297
    iget-wide v4, p0, Lqy;->d:J

    cmp-long v0, v4, v8

    if-eqz v0, :cond_5

    iget-wide v4, p0, Lqy;->d:J

    sub-long v4, v2, v4

    long-to-float v0, v4

    .line 1299
    :goto_1
    iget-wide v4, p0, Lqy;->d:J

    cmp-long v4, v4, v8

    if-nez v4, :cond_2

    .line 1300
    iput-wide v2, p0, Lqy;->d:J

    .line 1305
    :cond_2
    cmpl-float v2, v0, v6

    if-ltz v2, :cond_6

    .line 1306
    iget v0, p0, Lqy;->b:F

    .line 1315
    :cond_3
    iget v2, p0, Lqy;->c:F

    move v10, v2

    move v2, v0

    move v0, v10

    .line 1319
    :goto_2
    iget-object v3, p0, Lqy;->a:Lcom/android/ex/photo/views/PhotoView;

    invoke-static {v3, v2, v0}, Lcom/android/ex/photo/views/PhotoView;->a(Lcom/android/ex/photo/views/PhotoView;FF)Z

    .line 1320
    iget v3, p0, Lqy;->b:F

    sub-float v2, v3, v2

    iput v2, p0, Lqy;->b:F

    .line 1321
    iget v2, p0, Lqy;->c:F

    sub-float v0, v2, v0

    iput v0, p0, Lqy;->c:F

    .line 1323
    iget v0, p0, Lqy;->b:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_4

    iget v0, p0, Lqy;->c:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_4

    .line 1324
    invoke-virtual {p0}, Lqy;->a()V

    .line 1328
    :cond_4
    iget-boolean v0, p0, Lqy;->f:Z

    if-nez v0, :cond_0

    .line 1331
    iget-object v0, p0, Lqy;->a:Lcom/android/ex/photo/views/PhotoView;

    invoke-virtual {v0, p0}, Lcom/android/ex/photo/views/PhotoView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_5
    move v0, v1

    .line 1297
    goto :goto_1

    .line 1309
    :cond_6
    iget v2, p0, Lqy;->b:F

    sub-float v3, v6, v0

    div-float/2addr v2, v3

    mul-float/2addr v2, v7

    .line 1310
    iget v3, p0, Lqy;->c:F

    sub-float v0, v6, v0

    div-float v0, v3, v0

    mul-float v3, v0, v7

    .line 1311
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v4, p0, Lqy;->b:F

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpl-float v0, v0, v4

    if-gtz v0, :cond_7

    invoke-static {v2}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1312
    :cond_7
    iget v0, p0, Lqy;->b:F

    .line 1314
    :goto_3
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v4, p0, Lqy;->c:F

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpl-float v2, v2, v4

    if-gtz v2, :cond_3

    invoke-static {v3}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-nez v2, :cond_3

    move v2, v0

    move v0, v3

    goto :goto_2

    :cond_8
    move v0, v2

    goto :goto_3
.end method
