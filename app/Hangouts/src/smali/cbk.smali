.class public final Lcbk;
.super Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/hangouts/video/SafeAsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

.field private final b:I

.field private final c:I

.field private final d:Z

.field private final e:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/views/ComposeMessageView;IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 804
    iput-object p1, p0, Lcbk;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;-><init>()V

    .line 805
    iput p2, p0, Lcbk;->b:I

    .line 806
    iput p3, p0, Lcbk;->c:I

    .line 807
    iget v1, p0, Lcbk;->c:I

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcbk;->d:Z

    .line 808
    iput-object p4, p0, Lcbk;->e:Landroid/content/Intent;

    .line 809
    return-void

    .line 807
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected synthetic doInBackgroundTimed([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 798
    iget v0, p0, Lcbk;->c:I

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->c(I)Ljava/lang/String;

    move-result-object v1

    iget-boolean v0, p0, Lcbk;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcbk;->e:Landroid/content/Intent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcbk;->e:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v2, "content://"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget v1, p0, Lcbk;->b:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    iget-boolean v1, p0, Lcbk;->d:Z

    invoke-static {v0, v1}, Lbyr;->a(Ljava/io/File;Z)V

    :cond_2
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected synthetic onCancelled(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 798
    check-cast p1, Ljava/lang/String;

    if-eqz p1, :cond_0

    :try_start_0
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Babel"

    const-string v2, "Caught IllegalArgumentException trying to delete photo"

    invoke-static {v1, v2, v0}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 798
    check-cast p1, Ljava/lang/String;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcbk;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    iget v1, p0, Lcbk;->c:I

    invoke-static {v0, p1, v1}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a(Lcom/google/android/apps/hangouts/views/ComposeMessageView;Ljava/lang/String;I)V

    :goto_0
    iget-object v0, p0, Lcbk;->a:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->n(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    iget-boolean v0, p0, Lcbk;->d:Z

    if-eqz v0, :cond_1

    sget v0, Lh;->Z:I

    :goto_1
    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_1
    sget v0, Lh;->aa:I

    goto :goto_1
.end method
