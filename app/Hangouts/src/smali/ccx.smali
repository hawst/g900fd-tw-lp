.class public final Lccx;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/Long;

.field public d:Z

.field public final e:Z

.field public f:J

.field public g:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0

    .prologue
    .line 557
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 558
    iput-boolean p1, p0, Lccx;->e:Z

    .line 559
    invoke-virtual {p0}, Lccx;->a()V

    .line 560
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 618
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lccx;->a:Ljava/util/Queue;

    .line 619
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lccx;->b:Ljava/util/Set;

    .line 620
    const/4 v0, 0x0

    iput-object v0, p0, Lccx;->c:Ljava/lang/Long;

    .line 621
    const/4 v0, 0x1

    iput-boolean v0, p0, Lccx;->d:Z

    .line 622
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lccx;->f:J

    .line 623
    const/4 v0, 0x0

    iput-boolean v0, p0, Lccx;->g:Z

    .line 624
    return-void
.end method

.method public a(J)V
    .locals 2

    .prologue
    .line 631
    iget-wide v0, p0, Lccx;->f:J

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lccx;->f:J

    .line 632
    return-void
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 9

    .prologue
    .line 567
    sget-boolean v0, Laad;->j:Z

    if-eqz v0, :cond_0

    .line 568
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "[MessageListState] setNewCursor "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p1, :cond_6

    const-string v0, "null"

    .line 569
    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 568
    invoke-static {v1, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 572
    :cond_0
    monitor-enter p0

    .line 573
    :try_start_0
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 575
    if-eqz p1, :cond_4

    invoke-interface {p1}, Landroid/database/Cursor;->moveToLast()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 576
    const-string v1, "_id"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    .line 577
    const-string v2, "timestamp"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 578
    iget-boolean v3, p0, Lccx;->d:Z

    if-nez v3, :cond_2

    iget-boolean v3, p0, Lccx;->e:Z

    if-nez v3, :cond_2

    iget-boolean v3, p0, Lccx;->g:Z

    if-nez v3, :cond_2

    .line 585
    :cond_1
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    .line 586
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    .line 587
    iget-object v7, p0, Lccx;->b:Ljava/util/Set;

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    iget-wide v7, p0, Lccx;->f:J

    cmp-long v3, v3, v7

    if-lez v3, :cond_2

    .line 589
    const/4 v3, 0x0

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 593
    invoke-interface {p1}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v3

    if-nez v3, :cond_1

    .line 596
    :cond_2
    invoke-interface {p1}, Landroid/database/Cursor;->moveToLast()Z

    .line 599
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lccx;->a(J)V

    .line 600
    iget-boolean v2, p0, Lccx;->d:Z

    if-nez v2, :cond_3

    iget-boolean v2, p0, Lccx;->e:Z

    if-nez v2, :cond_3

    iget-boolean v2, p0, Lccx;->g:Z

    if-eqz v2, :cond_4

    .line 601
    :cond_3
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, p0, Lccx;->c:Ljava/lang/Long;

    .line 605
    :cond_4
    iget-object v1, p0, Lccx;->a:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->addAll(Ljava/util/Collection;)Z

    .line 609
    iget-object v1, p0, Lccx;->b:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 610
    sget-boolean v0, Laad;->j:Z

    if-eqz v0, :cond_5

    .line 611
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[MessageListState] newIds "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lccx;->a:Ljava/util/Queue;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    :cond_5
    const/4 v0, 0x0

    iput-boolean v0, p0, Lccx;->d:Z

    .line 614
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    .line 569
    :cond_6
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0

    .line 614
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;)V
    .locals 5

    .prologue
    .line 666
    invoke-virtual {p0}, Lccx;->c()Ljava/lang/Long;

    move-result-object v0

    .line 667
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->d()J

    move-result-wide v1

    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v3

    cmp-long v0, v1, v3

    if-nez v0, :cond_0

    .line 668
    iget-object v0, p0, Lccx;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iput-object v0, p0, Lccx;->c:Ljava/lang/Long;

    .line 674
    :goto_0
    return-void

    .line 671
    :cond_0
    const-string v0, "Babel"

    const-string v1, "[MessageCursorAdapter] onRevealFinished called when the new message queue was empty."

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 677
    iput-boolean p1, p0, Lccx;->g:Z

    .line 678
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 635
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lccx;->a:Ljava/util/Queue;

    .line 636
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lccx;->b:Ljava/util/Set;

    .line 637
    const/4 v0, 0x1

    iput-boolean v0, p0, Lccx;->d:Z

    .line 638
    const/4 v0, 0x0

    iput-boolean v0, p0, Lccx;->g:Z

    .line 639
    return-void
.end method

.method public b(J)Z
    .locals 2

    .prologue
    .line 656
    iget-object v0, p0, Lccx;->a:Ljava/util/Queue;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Queue;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public c()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 652
    iget-object v0, p0, Lccx;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    return-object v0
.end method

.method public c(J)Z
    .locals 2

    .prologue
    .line 660
    iget-object v0, p0, Lccx;->c:Ljava/lang/Long;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lccx;->c:Ljava/lang/Long;

    .line 661
    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v0

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
