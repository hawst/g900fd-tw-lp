.class public final Lbzd;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Z

.field private static final b:Landroid/telephony/TelephonyManager;

.field private static final c:Lbzf;

.field private static final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static e:I

.field private static f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 32
    sget-object v0, Lbys;->r:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lbzd;->a:Z

    .line 35
    new-instance v0, Lbzf;

    invoke-direct {v0}, Lbzf;-><init>()V

    sput-object v0, Lbzd;->c:Lbzf;

    .line 43
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "phone"

    .line 44
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    sput-object v0, Lbzd;->b:Landroid/telephony/TelephonyManager;

    .line 46
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 47
    const/16 v1, 0x34

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    const/16 v1, 0x36

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "9"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lbzd;->d:Ljava/util/Map;

    .line 50
    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 158
    const/4 v1, 0x0

    :try_start_0
    invoke-static {p0, v1}, Lbzd;->c(Ljava/lang/String;Ljava/lang/String;)Lbzc;

    move-result-object v1

    .line 159
    invoke-virtual {v1}, Lbzc;->d()Ljava/lang/String;
    :try_end_0
    .catch Lcxo; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 164
    :cond_0
    :goto_0
    return-object v0

    .line 161
    :catch_0
    move-exception v1

    sget-boolean v1, Lbzd;->a:Z

    if-eqz v1, :cond_0

    .line 162
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getCountry: Not able to parse e164 number "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;I)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 310
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x6

    if-ge v0, v1, :cond_1

    .line 357
    :cond_0
    :goto_0
    return-object p0

    .line 316
    :cond_1
    sget-object v0, Lbzd;->f:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 317
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 318
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbzd;->f:Ljava/lang/String;

    .line 320
    sget-object v0, Lbzd;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v0

    .line 323
    :try_start_0
    sget-object v1, Lbzd;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lbzd;->c(Ljava/lang/String;Ljava/lang/String;)Lbzc;

    move-result-object v0

    .line 324
    invoke-virtual {v0}, Lbzc;->a()Lcye;

    move-result-object v0

    invoke-virtual {v0}, Lcye;->a()I

    move-result v0

    sput v0, Lbzd;->e:I
    :try_end_0
    .catch Lcxo; {:try_start_0 .. :try_end_0} :catch_0

    .line 330
    :cond_2
    :goto_1
    invoke-static {p0}, Lbzd;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 331
    if-eqz v1, :cond_0

    .line 337
    :try_start_1
    sget-object v0, Lbzd;->f:Ljava/lang/String;

    invoke-static {v1, v0}, Lbzd;->c(Ljava/lang/String;Ljava/lang/String;)Lbzc;

    move-result-object v2

    .line 339
    sget-object v0, Lbze;->a:[I

    add-int/lit8 v3, p1, -0x1

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 351
    sget v0, Lcxt;->b:I

    .line 355
    :goto_2
    invoke-virtual {v2, v0}, Lbzc;->a(I)Ljava/lang/String;
    :try_end_1
    .catch Lcxo; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object p0

    goto :goto_0

    .line 326
    :catch_0
    move-exception v0

    sput v4, Lbzd;->e:I

    goto :goto_1

    .line 341
    :pswitch_0
    :try_start_2
    sget v0, Lbzd;->e:I

    if-eq v0, v4, :cond_3

    .line 342
    invoke-virtual {v2}, Lbzc;->a()Lcye;

    move-result-object v0

    invoke-virtual {v0}, Lcye;->a()I

    move-result v0

    sget v3, Lbzd;->e:I

    if-ne v0, v3, :cond_3

    sget v0, Lcxt;->c:I

    goto :goto_2

    :cond_3
    sget v0, Lcxt;->b:I

    goto :goto_2

    .line 347
    :pswitch_1
    sget v0, Lcxt;->c:I
    :try_end_2
    .catch Lcxo; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 357
    :catch_1
    move-exception v0

    move-object p0, v1

    goto :goto_0

    .line 339
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 188
    :try_start_0
    invoke-static {p0, p1}, Lbzd;->c(Ljava/lang/String;Ljava/lang/String;)Lbzc;

    move-result-object v1

    .line 189
    invoke-virtual {v1}, Lbzc;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 190
    sget v2, Lcxt;->a:I

    invoke-virtual {v1, v2}, Lbzc;->a(I)Ljava/lang/String;
    :try_end_0
    .catch Lcxo; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 198
    :cond_0
    :goto_0
    return-object v0

    .line 195
    :catch_0
    move-exception v1

    sget-boolean v1, Lbzd;->a:Z

    if-eqz v1, :cond_0

    .line 196
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getValidE164Number: Not able to parse phone number "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a()V
    .locals 2

    .prologue
    .line 53
    invoke-static {}, Lcxq;->b()Lcxq;

    move-result-object v0

    sput-object v0, Lbzc;->a:Lcxq;

    .line 54
    sget-object v0, Lcxe;->a:Lcxe;

    sget-object v1, Lbzd;->c:Lbzf;

    invoke-virtual {v0, v1}, Lcxe;->a(Lcxf;)V

    .line 55
    return-void
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 175
    invoke-static {}, Lbzd;->h()Ljava/lang/String;

    move-result-object v0

    .line 176
    invoke-static {p0, v0}, Lbzd;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b()Z
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lbzd;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 238
    invoke-static {}, Lbzd;->h()Ljava/lang/String;

    move-result-object v1

    .line 240
    :try_start_0
    invoke-static {p0, v1}, Lbzd;->c(Ljava/lang/String;Ljava/lang/String;)Lbzc;

    move-result-object v2

    .line 241
    invoke-static {p1, v1}, Lbzd;->c(Ljava/lang/String;Ljava/lang/String;)Lbzc;

    move-result-object v1

    .line 242
    invoke-virtual {v2, v1}, Lbzc;->a(Lbzc;)I

    move-result v1

    .line 243
    sget v2, Lcxs;->e:I

    if-eq v1, v2, :cond_0

    sget v2, Lcxs;->d:I

    if-eq v1, v2, :cond_0

    sget v2, Lcxs;->c:I
    :try_end_0
    .catch Lcxo; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v1, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 246
    :cond_1
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private static c(Ljava/lang/String;Ljava/lang/String;)Lbzc;
    .locals 3

    .prologue
    .line 383
    if-nez p0, :cond_0

    .line 384
    new-instance v0, Lcxo;

    sget-object v1, Lcxp;->b:Lcxp;

    const-string v2, "Number may not be null"

    invoke-direct {v0, v1, v2}, Lcxo;-><init>(Lcxp;Ljava/lang/String;)V

    throw v0

    .line 387
    :cond_0
    sget-object v0, Lbzd;->c:Lbzf;

    invoke-virtual {v0, p0}, Lbzf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzc;

    .line 388
    if-nez v0, :cond_1

    .line 389
    new-instance v0, Lbzc;

    invoke-direct {v0, p0, p1}, Lbzc;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    sget-object v1, Lbzd;->c:Lbzf;

    invoke-virtual {v1, p0, v0}, Lbzf;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 393
    :cond_1
    return-object v0
.end method

.method public static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 203
    invoke-static {p0}, Lbzd;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 204
    if-eqz v0, :cond_1

    move-object p0, v0

    .line 213
    :cond_0
    :goto_0
    return-object p0

    .line 208
    :cond_1
    invoke-static {p0}, Lbzd;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 209
    if-eqz v0, :cond_0

    move-object p0, v0

    .line 210
    goto :goto_0
.end method

.method public static c()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 69
    sget-object v1, Lbzd;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v1

    .line 70
    sget-boolean v2, Lbzd;->a:Z

    if-eqz v2, :cond_0

    .line 71
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "telephonyManager.getSimState() returns:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    :cond_0
    if-eq v1, v0, :cond_1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 220
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 221
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    move v0, v1

    .line 222
    :goto_0
    if-ge v0, v3, :cond_1

    .line 223
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 224
    invoke-static {v4}, Ljava/lang/Character;->isDigit(C)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 225
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 222
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 229
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x6

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static d()Z
    .locals 2

    .prologue
    .line 82
    invoke-static {}, Lbzd;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 362
    sget v0, Lbzg;->a:I

    invoke-static {p0, v0}, Lbzd;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static e()Z
    .locals 1

    .prologue
    .line 91
    invoke-static {}, Lbzd;->b()Z

    move-result v0

    return v0
.end method

.method public static f(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 370
    const/4 v0, 0x0

    .line 372
    if-eqz p0, :cond_0

    .line 373
    invoke-static {p0}, Lbzd;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 374
    if-eqz v1, :cond_0

    .line 375
    new-instance v0, Ljava/util/Locale;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    invoke-virtual {v0}, Ljava/util/Locale;->getDisplayCountry()Ljava/lang/String;

    move-result-object v0

    .line 379
    :cond_0
    return-object v0
.end method

.method public static f()Z
    .locals 1

    .prologue
    .line 99
    invoke-static {}, Lbzd;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lbzd;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 106
    sget-object v0, Lbzd;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v0

    .line 108
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 109
    const/4 v0, 0x0

    .line 113
    :goto_0
    return-object v0

    .line 112
    :cond_0
    invoke-static {}, Lbzd;->k()Ljava/lang/String;

    move-result-object v1

    .line 113
    invoke-static {v0, v1}, Lbzd;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static g(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x2

    .line 402
    invoke-static {}, Lbzd;->h()Ljava/lang/String;

    move-result-object v1

    .line 404
    :try_start_0
    invoke-static {p0, v1}, Lbzd;->c(Ljava/lang/String;Ljava/lang/String;)Lbzc;

    move-result-object v2

    .line 407
    invoke-virtual {v2}, Lbzc;->b()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 408
    sget v1, Lcxt;->a:I

    invoke-virtual {v2, v1}, Lbzc;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 426
    :cond_0
    :goto_0
    return-object v0

    .line 413
    :cond_1
    invoke-virtual {v2}, Lbzc;->c()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 416
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v4, v5, :cond_2

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v5, 0x12

    if-gt v4, v5, :cond_0

    .line 420
    :cond_2
    invoke-virtual {v2, v1}, Lbzc;->a(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 421
    const-string v2, "+%s%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    const/4 v1, 0x1

    aput-object v3, v4, v1

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Lcxo; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 423
    :catch_0
    move-exception v1

    sget-boolean v1, Lbzd;->a:Z

    if-eqz v1, :cond_0

    .line 424
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Not able to parse phone number "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 117
    invoke-static {}, Lbzd;->k()Ljava/lang/String;

    move-result-object v0

    .line 118
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 119
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    .line 122
    :cond_0
    return-object v0
.end method

.method public static i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    sget-object v0, Lbzd;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static j()Ljava/lang/String;
    .locals 2

    .prologue
    .line 143
    sget-object v0, Lbzd;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimOperatorName()Ljava/lang/String;

    move-result-object v0

    .line 144
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 145
    sget-object v0, Lbzd;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v0

    .line 147
    :cond_0
    return-object v0
.end method

.method private static k()Ljava/lang/String;
    .locals 2

    .prologue
    .line 126
    sget-object v0, Lbzd;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v0

    .line 127
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 128
    const/4 v0, 0x0

    .line 131
    :goto_0
    return-object v0

    :cond_0
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
