.class public final Lerg;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lerg;


# instance fields
.field public b:Leqz;

.field public c:[Leqq;

.field public d:Leqq;

.field public e:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1697
    const/4 v0, 0x0

    new-array v0, v0, [Lerg;

    sput-object v0, Lerg;->a:[Lerg;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1698
    invoke-direct {p0}, Lepn;-><init>()V

    .line 1709
    iput-object v1, p0, Lerg;->b:Leqz;

    .line 1712
    sget-object v0, Leqq;->a:[Leqq;

    iput-object v0, p0, Lerg;->c:[Leqq;

    .line 1715
    iput-object v1, p0, Lerg;->d:Leqq;

    .line 1718
    iput-object v1, p0, Lerg;->e:Ljava/lang/Integer;

    .line 1698
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1746
    iget-object v0, p0, Lerg;->b:Leqz;

    if-eqz v0, :cond_4

    .line 1747
    const/4 v0, 0x1

    iget-object v2, p0, Lerg;->b:Leqz;

    .line 1748
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1750
    :goto_0
    iget-object v2, p0, Lerg;->c:[Leqq;

    if-eqz v2, :cond_1

    .line 1751
    iget-object v2, p0, Lerg;->c:[Leqq;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1752
    if-eqz v4, :cond_0

    .line 1753
    const/4 v5, 0x2

    .line 1754
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1751
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1758
    :cond_1
    iget-object v1, p0, Lerg;->d:Leqq;

    if-eqz v1, :cond_2

    .line 1759
    const/4 v1, 0x3

    iget-object v2, p0, Lerg;->d:Leqq;

    .line 1760
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1762
    :cond_2
    iget-object v1, p0, Lerg;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 1763
    const/4 v1, 0x4

    iget-object v2, p0, Lerg;->e:Ljava/lang/Integer;

    .line 1764
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1766
    :cond_3
    iget-object v1, p0, Lerg;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1767
    iput v0, p0, Lerg;->cachedSize:I

    .line 1768
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1694
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lerg;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lerg;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lerg;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lerg;->b:Leqz;

    if-nez v0, :cond_2

    new-instance v0, Leqz;

    invoke-direct {v0}, Leqz;-><init>()V

    iput-object v0, p0, Lerg;->b:Leqz;

    :cond_2
    iget-object v0, p0, Lerg;->b:Leqz;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lerg;->c:[Leqq;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Leqq;

    iget-object v3, p0, Lerg;->c:[Leqq;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lerg;->c:[Leqq;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Lerg;->c:[Leqq;

    :goto_2
    iget-object v2, p0, Lerg;->c:[Leqq;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Lerg;->c:[Leqq;

    new-instance v3, Leqq;

    invoke-direct {v3}, Leqq;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lerg;->c:[Leqq;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lerg;->c:[Leqq;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lerg;->c:[Leqq;

    new-instance v3, Leqq;

    invoke-direct {v3}, Leqq;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lerg;->c:[Leqq;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lerg;->d:Leqq;

    if-nez v0, :cond_6

    new-instance v0, Leqq;

    invoke-direct {v0}, Leqq;-><init>()V

    iput-object v0, p0, Lerg;->d:Leqq;

    :cond_6
    iget-object v0, p0, Lerg;->d:Leqq;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v4, :cond_7

    const/4 v2, 0x2

    if-eq v0, v2, :cond_7

    const/4 v2, 0x3

    if-eq v0, v2, :cond_7

    const/4 v2, 0x4

    if-eq v0, v2, :cond_7

    const/4 v2, 0x5

    if-ne v0, v2, :cond_8

    :cond_7
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lerg;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_8
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lerg;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 1723
    iget-object v0, p0, Lerg;->b:Leqz;

    if-eqz v0, :cond_0

    .line 1724
    const/4 v0, 0x1

    iget-object v1, p0, Lerg;->b:Leqz;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1726
    :cond_0
    iget-object v0, p0, Lerg;->c:[Leqq;

    if-eqz v0, :cond_2

    .line 1727
    iget-object v1, p0, Lerg;->c:[Leqq;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 1728
    if-eqz v3, :cond_1

    .line 1729
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 1727
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1733
    :cond_2
    iget-object v0, p0, Lerg;->d:Leqq;

    if-eqz v0, :cond_3

    .line 1734
    const/4 v0, 0x3

    iget-object v1, p0, Lerg;->d:Leqq;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1736
    :cond_3
    iget-object v0, p0, Lerg;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 1737
    const/4 v0, 0x4

    iget-object v1, p0, Lerg;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1739
    :cond_4
    iget-object v0, p0, Lerg;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1741
    return-void
.end method
