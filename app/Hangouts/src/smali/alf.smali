.class public final Lalf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/fragments/MoodPickerFragment;

.field private b:Lale;

.field private c:Lyj;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/MoodPickerFragment;Lale;Lyj;)V
    .locals 0

    .prologue
    .line 216
    iput-object p1, p0, Lalf;->a:Lcom/google/android/apps/hangouts/fragments/MoodPickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 217
    iput-object p2, p0, Lalf;->b:Lale;

    .line 218
    iput-object p3, p0, Lalf;->c:Lyj;

    .line 219
    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 224
    iget-object v0, p0, Lalf;->b:Lale;

    invoke-static {v0}, Lale;->a(Lale;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    .line 225
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 226
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    .line 227
    iget-object v2, p0, Lalf;->c:Lyj;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->d(Lyj;Ljava/lang/String;)V

    .line 228
    iget-object v1, p0, Lalf;->a:Lcom/google/android/apps/hangouts/fragments/MoodPickerFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/MoodPickerFragment;->a(Lcom/google/android/apps/hangouts/fragments/MoodPickerFragment;)Lalg;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 229
    iget-object v1, p0, Lalf;->a:Lcom/google/android/apps/hangouts/fragments/MoodPickerFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/MoodPickerFragment;->a(Lcom/google/android/apps/hangouts/fragments/MoodPickerFragment;)Lalg;

    move-result-object v1

    invoke-interface {v1, v0}, Lalg;->a(I)V

    .line 231
    :cond_0
    return-void
.end method
