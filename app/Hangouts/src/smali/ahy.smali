.class public final Lahy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Laac;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V
    .locals 0

    .prologue
    .line 6368
    iput-object p1, p0, Lahy;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;B)V
    .locals 0

    .prologue
    .line 6368
    invoke-direct {p0, p1}, Lahy;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    return-void
.end method


# virtual methods
.method public a(Lbzn;Lbyd;ZLzx;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 6373
    invoke-static {p1}, Lcwz;->a(Ljava/lang/Object;)V

    .line 6374
    invoke-static {p2}, Lcwz;->a(Ljava/lang/Object;)V

    .line 6376
    invoke-virtual {p4}, Lzx;->k()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-static {v0, v3}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v1

    .line 6377
    iget-object v0, p0, Lahy;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->az(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 6378
    invoke-interface {v0, p4}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 6379
    iget-object v2, p0, Lahy;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v2

    .line 6380
    if-eqz p3, :cond_1

    .line 6381
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 6382
    iget-object v0, p0, Lahy;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->az(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->remove(I)V

    .line 6383
    if-eqz v2, :cond_0

    .line 6384
    sget v0, Lh;->nn:I

    invoke-static {v2, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 6385
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 6398
    :cond_0
    :goto_0
    return-void

    .line 6390
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsv;

    .line 6391
    invoke-virtual {v0}, Lbsv;->b()V

    goto :goto_1

    .line 6393
    :cond_2
    if-eqz v2, :cond_0

    .line 6394
    sget v0, Lh;->nm:I

    invoke-static {v2, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 6395
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
