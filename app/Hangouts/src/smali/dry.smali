.class public final Ldry;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldry;


# instance fields
.field public b:Ldrz;

.field public c:Ldsa;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2325
    const/4 v0, 0x0

    new-array v0, v0, [Ldry;

    sput-object v0, Ldry;->a:[Ldry;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2326
    invoke-direct {p0}, Lepn;-><init>()V

    .line 2488
    iput-object v0, p0, Ldry;->b:Ldrz;

    .line 2491
    iput-object v0, p0, Ldry;->c:Ldsa;

    .line 2326
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 2508
    const/4 v0, 0x0

    .line 2509
    iget-object v1, p0, Ldry;->c:Ldsa;

    if-eqz v1, :cond_0

    .line 2510
    const/4 v0, 0x1

    iget-object v1, p0, Ldry;->c:Ldsa;

    .line 2511
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2513
    :cond_0
    iget-object v1, p0, Ldry;->b:Ldrz;

    if-eqz v1, :cond_1

    .line 2514
    const/4 v1, 0x2

    iget-object v2, p0, Ldry;->b:Ldrz;

    .line 2515
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2517
    :cond_1
    iget-object v1, p0, Ldry;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2518
    iput v0, p0, Ldry;->cachedSize:I

    .line 2519
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 2322
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldry;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldry;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldry;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldry;->c:Ldsa;

    if-nez v0, :cond_2

    new-instance v0, Ldsa;

    invoke-direct {v0}, Ldsa;-><init>()V

    iput-object v0, p0, Ldry;->c:Ldsa;

    :cond_2
    iget-object v0, p0, Ldry;->c:Ldsa;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldry;->b:Ldrz;

    if-nez v0, :cond_3

    new-instance v0, Ldrz;

    invoke-direct {v0}, Ldrz;-><init>()V

    iput-object v0, p0, Ldry;->b:Ldrz;

    :cond_3
    iget-object v0, p0, Ldry;->b:Ldrz;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 2496
    iget-object v0, p0, Ldry;->c:Ldsa;

    if-eqz v0, :cond_0

    .line 2497
    const/4 v0, 0x1

    iget-object v1, p0, Ldry;->c:Ldsa;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 2499
    :cond_0
    iget-object v0, p0, Ldry;->b:Ldrz;

    if-eqz v0, :cond_1

    .line 2500
    const/4 v0, 0x2

    iget-object v1, p0, Ldry;->b:Ldrz;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 2502
    :cond_1
    iget-object v0, p0, Ldry;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 2504
    return-void
.end method
