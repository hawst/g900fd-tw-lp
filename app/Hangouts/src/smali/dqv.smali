.class public final Ldqv;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldqv;


# instance fields
.field public b:Ldvn;

.field public c:Ldqs;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21800
    const/4 v0, 0x0

    new-array v0, v0, [Ldqv;

    sput-object v0, Ldqv;->a:[Ldqv;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 21801
    invoke-direct {p0}, Lepn;-><init>()V

    .line 21804
    iput-object v0, p0, Ldqv;->b:Ldvn;

    .line 21807
    iput-object v0, p0, Ldqv;->c:Ldqs;

    .line 21801
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 21824
    const/4 v0, 0x0

    .line 21825
    iget-object v1, p0, Ldqv;->b:Ldvn;

    if-eqz v1, :cond_0

    .line 21826
    const/4 v0, 0x1

    iget-object v1, p0, Ldqv;->b:Ldvn;

    .line 21827
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 21829
    :cond_0
    iget-object v1, p0, Ldqv;->c:Ldqs;

    if-eqz v1, :cond_1

    .line 21830
    const/4 v1, 0x2

    iget-object v2, p0, Ldqv;->c:Ldqs;

    .line 21831
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21833
    :cond_1
    iget-object v1, p0, Ldqv;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21834
    iput v0, p0, Ldqv;->cachedSize:I

    .line 21835
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 21797
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldqv;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldqv;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldqv;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldqv;->b:Ldvn;

    if-nez v0, :cond_2

    new-instance v0, Ldvn;

    invoke-direct {v0}, Ldvn;-><init>()V

    iput-object v0, p0, Ldqv;->b:Ldvn;

    :cond_2
    iget-object v0, p0, Ldqv;->b:Ldvn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldqv;->c:Ldqs;

    if-nez v0, :cond_3

    new-instance v0, Ldqs;

    invoke-direct {v0}, Ldqs;-><init>()V

    iput-object v0, p0, Ldqv;->c:Ldqs;

    :cond_3
    iget-object v0, p0, Ldqv;->c:Ldqs;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 21812
    iget-object v0, p0, Ldqv;->b:Ldvn;

    if-eqz v0, :cond_0

    .line 21813
    const/4 v0, 0x1

    iget-object v1, p0, Ldqv;->b:Ldvn;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 21815
    :cond_0
    iget-object v0, p0, Ldqv;->c:Ldqs;

    if-eqz v0, :cond_1

    .line 21816
    const/4 v0, 0x2

    iget-object v1, p0, Ldqv;->c:Ldqs;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 21818
    :cond_1
    iget-object v0, p0, Ldqv;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 21820
    return-void
.end method
