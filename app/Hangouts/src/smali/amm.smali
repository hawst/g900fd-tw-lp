.class public final Lamm;
.super Ls;
.source "PG"


# static fields
.field private static final Y:Landroid/text/style/StyleSpan;

.field private static final Z:Landroid/text/style/UnderlineSpan;

.field private static final aa:Landroid/text/style/ForegroundColorSpan;


# instance fields
.field private ab:I

.field private ac:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private ad:Ljava/lang/String;

.field private ae:Ljava/lang/String;

.field private af:Lbrp;

.field private ag:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbrp;",
            ">;"
        }
    .end annotation
.end field

.field private ah:Landroid/widget/EditText;

.field private ai:Lwi;

.field private aj:Landroid/app/Dialog;

.field private final ak:Lbme;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 75
    new-instance v0, Landroid/text/style/StyleSpan;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    sput-object v0, Lamm;->Y:Landroid/text/style/StyleSpan;

    .line 76
    new-instance v0, Landroid/text/style/UnderlineSpan;

    invoke-direct {v0}, Landroid/text/style/UnderlineSpan;-><init>()V

    sput-object v0, Lamm;->Z:Landroid/text/style/UnderlineSpan;

    .line 77
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    const v1, -0xffff01

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    sput-object v0, Lamm;->aa:Landroid/text/style/ForegroundColorSpan;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 230
    invoke-direct {p0}, Ls;-><init>()V

    .line 103
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lbme;

    invoke-static {v0, v1}, Lcyj;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbme;

    iput-object v0, p0, Lamm;->ak:Lbme;

    .line 231
    return-void
.end method

.method public static a(ILjava/lang/String;Ljava/util/ArrayList;)Lamm;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lamm;"
        }
    .end annotation

    .prologue
    .line 173
    new-instance v0, Lamm;

    invoke-direct {v0}, Lamm;-><init>()V

    .line 174
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 175
    const-string v2, "dialog_id"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 176
    const-string v2, "start_mode"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 177
    const-string v2, "phone_number"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    const-string v2, "account_names"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 179
    invoke-virtual {v0, v1}, Lamm;->setArguments(Landroid/os/Bundle;)V

    .line 180
    return-object v0
.end method

.method static synthetic a(Lamm;Lbrp;)Lbrp;
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lamm;->af:Lbrp;

    return-object p1
.end method

.method static synthetic a(Lamm;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lamm;->ac:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 140
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 141
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 143
    invoke-static {}, Lbmz;->a()Lbmz;

    invoke-static {p0}, Lbmz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 145
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 146
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    .line 159
    :goto_0
    return-object v0

    .line 151
    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Lbkb;->f(Z)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 152
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v3

    .line 154
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v3, p0}, Lyj;->c(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 155
    :cond_2
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 159
    goto :goto_0
.end method

.method static synthetic a(Lamm;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lamm;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lamm;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lamm;->ae:Ljava/lang/String;

    return-object p1
.end method

.method public static b(Z)Ljava/lang/String;
    .locals 2

    .prologue
    .line 115
    if-eqz p0, :cond_1

    .line 116
    invoke-static {}, Lbmz;->a()Lbmz;

    move-result-object v0

    invoke-virtual {v0}, Lbmz;->j()Ljava/lang/String;

    move-result-object v0

    .line 117
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 133
    :cond_0
    :goto_0
    return-object v0

    .line 123
    :cond_1
    invoke-static {}, Lbzd;->g()Ljava/lang/String;

    move-result-object v0

    .line 124
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 129
    invoke-static {}, Lbmz;->a()Lbmz;

    move-result-object v0

    invoke-virtual {v0}, Lbmz;->k()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic b(Lamm;)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Lamm;->t()V

    return-void
.end method

.method private b(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 636
    invoke-static {}, Lbmz;->a()Lbmz;

    iget-object v2, p0, Lamm;->ad:Ljava/lang/String;

    .line 637
    invoke-static {v2}, Lbmz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 640
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 642
    iget-object v3, p0, Lamm;->ad:Ljava/lang/String;

    sget v4, Lbzg;->c:I

    .line 643
    invoke-static {v3, v4}, Lbzd;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    .line 646
    invoke-static {p1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 647
    sget v2, Lh;->oe:I

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v0

    invoke-virtual {p0, v2, v1}, Lamm;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 649
    invoke-direct {p0, v1}, Lamm;->c(Ljava/lang/String;)V

    .line 668
    :goto_0
    return v0

    .line 654
    :cond_0
    invoke-static {v2}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v2

    .line 656
    if-eqz v2, :cond_1

    invoke-static {v2}, Lbkb;->f(Lyj;)I

    move-result v2

    const/16 v4, 0x66

    if-eq v2, v4, :cond_2

    .line 658
    :cond_1
    sget v2, Lh;->of:I

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v0

    invoke-virtual {p0, v2, v1}, Lamm;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 660
    invoke-direct {p0, v1}, Lamm;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 665
    :cond_3
    invoke-static {}, Lbmz;->a()Lbmz;

    move-result-object v2

    iget-object v3, p0, Lamm;->ad:Ljava/lang/String;

    .line 666
    invoke-direct {p0}, Lamm;->r()Z

    move-result v4

    .line 665
    invoke-virtual {v2, v3, p1, v4, v0}, Lbmz;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Z

    move v0, v1

    .line 668
    goto :goto_0
.end method

.method static synthetic c(Lamm;)V
    .locals 8

    .prologue
    .line 67
    invoke-virtual {p0}, Lamm;->getActivity()Ly;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lamm;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->e()Lae;

    move-result-object v0

    invoke-virtual {v0}, Lae;->a()Lao;

    move-result-object v0

    iget v1, p0, Lamm;->ab:I

    iget-object v2, p0, Lamm;->ac:Ljava/util/ArrayList;

    iget-object v3, p0, Lamm;->ad:Ljava/lang/String;

    new-instance v4, Lamm;

    invoke-direct {v4}, Lamm;-><init>()V

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    const-string v6, "dialog_id"

    const/4 v7, 0x2

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v6, "start_mode"

    invoke-virtual {v5, v6, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "phone_number"

    invoke-virtual {v5, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "account_names"

    invoke-virtual {v5, v1, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {v4, v5}, Lamm;->setArguments(Landroid/os/Bundle;)V

    const-string v1, "phone editor"

    invoke-virtual {v4, v0, v1}, Ls;->a(Lao;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 672
    invoke-virtual {p0}, Lamm;->getActivity()Ly;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 673
    const/16 v1, 0x31

    invoke-virtual {v0, v1, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    .line 674
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 675
    return-void
.end method

.method static synthetic d(Lamm;)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Lamm;->s()V

    return-void
.end method

.method static synthetic e(Lamm;)Lbrp;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lamm;->af:Lbrp;

    return-object v0
.end method

.method static synthetic f(Lamm;)Ljava/util/List;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lamm;->ag:Ljava/util/List;

    return-object v0
.end method

.method static synthetic g(Lamm;)Lwi;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lamm;->ai:Lwi;

    return-object v0
.end method

.method static synthetic h(Lamm;)V
    .locals 3

    .prologue
    .line 67
    invoke-virtual {p0}, Lamm;->getActivity()Ly;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lamm;->getActivity()Ly;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Ly;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lamm;->ah:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    :cond_0
    return-void
.end method

.method static synthetic i(Lamm;)V
    .locals 5

    .prologue
    .line 67
    iget-object v0, p0, Lamm;->ah:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lamm;->af:Lbrp;

    iget-object v1, v1, Lbrp;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lbzd;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lamm;->ad:Ljava/lang/String;

    iget-object v0, p0, Lamm;->ad:Ljava/lang/String;

    if-nez v0, :cond_1

    sget v0, Lh;->og:I

    invoke-virtual {p0, v0}, Lamm;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lamm;->c(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x0

    iget-object v0, p0, Lamm;->ac:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v2, 0x1

    if-le v0, v2, :cond_5

    invoke-static {}, Lbmz;->a()Lbmz;

    iget-object v0, p0, Lamm;->ad:Ljava/lang/String;

    invoke-static {v0}, Lbmz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lamm;->ac:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lamm;->t()V

    :cond_3
    :goto_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-direct {p0, v0}, Lamm;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_4
    invoke-virtual {p0}, Lamm;->a()V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lamm;->ac:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_2

    :cond_6
    move-object v0, v1

    goto :goto_1
.end method

.method static synthetic j(Lamm;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lamm;->ae:Ljava/lang/String;

    return-object v0
.end method

.method private q()Landroid/app/Dialog;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 512
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lamm;->getActivity()Ly;

    move-result-object v0

    invoke-direct {v4, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 514
    invoke-virtual {p0}, Lamm;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 515
    sget v1, Lf;->gG:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 518
    sget v0, Lg;->l:I

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    .line 522
    iget-object v1, p0, Lamm;->ac:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lamm;->ae:Ljava/lang/String;

    move v2, v3

    .line 524
    :goto_0
    iget-object v1, p0, Lamm;->ac:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v2, v1, :cond_1

    .line 525
    new-instance v6, Landroid/widget/RadioButton;

    invoke-virtual {p0}, Lamm;->getActivity()Ly;

    move-result-object v1

    invoke-direct {v6, v1}, Landroid/widget/RadioButton;-><init>(Landroid/content/Context;)V

    .line 526
    iget-object v1, p0, Lamm;->ac:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v6, v1}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 527
    invoke-virtual {v6, v2}, Landroid/widget/RadioButton;->setId(I)V

    .line 528
    invoke-virtual {v0, v6}, Landroid/widget/RadioGroup;->addView(Landroid/view/View;)V

    .line 530
    if-nez v2, :cond_0

    .line 531
    invoke-virtual {v6, v7}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 524
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 535
    :cond_1
    new-instance v1, Lamw;

    invoke-direct {v1, p0}, Lamw;-><init>(Lamm;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 546
    iget-object v0, p0, Lamm;->ad:Ljava/lang/String;

    .line 547
    iget-object v1, p0, Lamm;->ad:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 548
    iget-object v0, p0, Lamm;->ad:Ljava/lang/String;

    sget v1, Lbzg;->c:I

    .line 549
    invoke-static {v0, v1}, Lbzd;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 551
    :cond_2
    sget v1, Lh;->nN:I

    new-array v2, v7, [Ljava/lang/Object;

    aput-object v0, v2, v3

    .line 552
    invoke-virtual {p0, v1, v2}, Lamm;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 551
    invoke-virtual {v4, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 553
    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    .line 554
    invoke-virtual {p0, v1}, Lamm;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lamx;

    invoke-direct {v2, p0}, Lamx;-><init>(Lamm;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 567
    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 568
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 569
    return-object v0
.end method

.method private r()Z
    .locals 2

    .prologue
    .line 626
    iget v0, p0, Lamm;->ab:I

    const/16 v1, 0x66

    if-eq v0, v1, :cond_0

    iget v0, p0, Lamm;->ab:I

    const/16 v1, 0x68

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private s()V
    .locals 2

    .prologue
    .line 680
    invoke-direct {p0}, Lamm;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 681
    invoke-static {}, Lbmz;->a()Lbmz;

    move-result-object v0

    invoke-virtual {v0}, Lbmz;->e()V

    .line 683
    :cond_0
    iget-object v0, p0, Lamm;->ak:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v1, 0x63d

    .line 684
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 683
    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    .line 685
    return-void
.end method

.method private t()V
    .locals 8

    .prologue
    .line 703
    invoke-virtual {p0}, Lamm;->getActivity()Ly;

    move-result-object v0

    if-nez v0, :cond_0

    .line 711
    :goto_0
    return-void

    .line 707
    :cond_0
    invoke-virtual {p0}, Lamm;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->e()Lae;

    move-result-object v0

    invoke-virtual {v0}, Lae;->a()Lao;

    move-result-object v0

    .line 708
    iget v1, p0, Lamm;->ab:I

    iget-object v2, p0, Lamm;->ac:Ljava/util/ArrayList;

    iget-object v3, p0, Lamm;->ad:Ljava/lang/String;

    new-instance v4, Lamm;

    invoke-direct {v4}, Lamm;-><init>()V

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    const-string v6, "dialog_id"

    const/4 v7, 0x3

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v6, "start_mode"

    invoke-virtual {v5, v6, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "phone_number"

    invoke-virtual {v5, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "account_names"

    invoke-virtual {v5, v1, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {v4, v5}, Lamm;->setArguments(Landroid/os/Bundle;)V

    .line 710
    const-string v1, "account chooser"

    invoke-virtual {v4, v0, v1}, Ls;->a(Lao;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private u()Ljava/lang/String;
    .locals 2

    .prologue
    .line 717
    iget v0, p0, Lamm;->ab:I

    packed-switch v0, :pswitch_data_0

    .line 730
    const-string v0, "Babel"

    const-string v1, "Invalid start mode for PhoneVerificationDialogFragment"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 734
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 721
    :pswitch_0
    sget v0, Lh;->nW:I

    invoke-virtual {p0, v0}, Lamm;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 724
    :pswitch_1
    sget v0, Lh;->nO:I

    invoke-virtual {p0, v0}, Lamm;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 727
    :pswitch_2
    sget v0, Lh;->oi:I

    invoke-virtual {p0, v0}, Lamm;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 717
    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private v()Z
    .locals 2

    .prologue
    .line 825
    iget-object v0, p0, Lamm;->ac:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 826
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    .line 828
    invoke-virtual {v0}, Lyj;->H()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 829
    const/4 v0, 0x1

    .line 833
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public f()Landroid/app/Dialog;
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 243
    invoke-virtual {p0}, Lamm;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "start_mode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lamm;->ab:I

    .line 244
    invoke-virtual {p0}, Lamm;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "phone_number"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lamm;->ad:Ljava/lang/String;

    .line 245
    invoke-virtual {p0}, Lamm;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account_names"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lamm;->ac:Ljava/util/ArrayList;

    .line 246
    iput-object v2, p0, Lamm;->aj:Landroid/app/Dialog;

    .line 248
    iget-object v0, p0, Lamm;->ac:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lamm;->ac:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 249
    :cond_0
    const-string v0, "Babel"

    const-string v1, "Account names should not be empty"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    :goto_0
    return-object v2

    .line 253
    :cond_1
    invoke-virtual {p0}, Lamm;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "dialog_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 272
    :goto_1
    iget-object v0, p0, Lamm;->aj:Landroid/app/Dialog;

    if-eqz v0, :cond_e

    .line 273
    iget-object v2, p0, Lamm;->aj:Landroid/app/Dialog;

    goto :goto_0

    .line 255
    :pswitch_0
    new-instance v7, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lamm;->getActivity()Ly;

    move-result-object v0

    invoke-direct {v7, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lamm;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lf;->gl:I

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    sget v0, Lg;->dH:I

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lamm;->ad:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_f

    iget-object v1, p0, Lamm;->ad:Ljava/lang/String;

    sget v5, Lbzg;->c:I

    invoke-static {v1, v5}, Lbzd;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    :goto_2
    iget v5, p0, Lamm;->ab:I

    packed-switch v5, :pswitch_data_1

    const-string v1, "Babel"

    const-string v5, "Invalid start mode for PhoneVerificationDialogFragment"

    invoke-static {v1, v5}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    :goto_3
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    sget v0, Lg;->bm:I

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lamm;->ac:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lyj;->l()Z

    move-result v1

    if-nez v1, :cond_7

    :goto_4
    if-eqz v4, :cond_9

    invoke-virtual {p0}, Lamm;->getActivity()Ly;

    move-result-object v1

    invoke-virtual {p0}, Lamm;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const-string v5, "androidverify"

    sget v6, Lh;->nS:I

    invoke-static {v0, v1, v4, v5, v6}, Lf;->a(Landroid/widget/TextView;Landroid/app/Activity;Landroid/content/res/Resources;Ljava/lang/String;I)V

    :goto_5
    sget v0, Lg;->cs:I

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-direct {p0}, Lamm;->v()Z

    move-result v1

    if-eqz v1, :cond_a

    sget v1, Lh;->nT:I

    invoke-virtual {p0, v1}, Lamm;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_6
    iget-object v0, p0, Lamm;->ad:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    iget v0, p0, Lamm;->ab:I

    const/16 v1, 0x66

    if-eq v0, v1, :cond_b

    invoke-direct {p0}, Lamm;->u()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lh;->ax:I

    invoke-virtual {p0, v1}, Lamm;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Lamn;

    invoke-direct {v4, p0}, Lamn;-><init>(Lamm;)V

    invoke-virtual {v0, v1, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    :goto_7
    sget v0, Lh;->bx:I

    invoke-virtual {p0, v0}, Lamm;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lamq;

    invoke-direct {v1, p0}, Lamq;-><init>(Lamm;)V

    invoke-virtual {v7, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    iput-object v0, p0, Lamm;->aj:Landroid/app/Dialog;

    goto/16 :goto_1

    :pswitch_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    sget v1, Lh;->nV:I

    invoke-virtual {p0, v1}, Lamm;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v5, v1

    move v1, v3

    :goto_8
    new-instance v6, Landroid/text/SpannableStringBuilder;

    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    invoke-direct {v6, v5}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    if-eqz v1, :cond_2

    new-instance v1, Landroid/text/SpannableStringBuilder;

    sget v5, Lh;->nX:I

    invoke-virtual {p0, v5}, Lamm;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v5}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    new-instance v5, Lamo;

    invoke-direct {v5, p0}, Lamo;-><init>(Lamm;)V

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v9

    invoke-virtual {v1, v5, v3, v9, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    sget v5, Lf;->cy:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v9

    invoke-virtual {v1, v5, v3, v9, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    sget-object v5, Lamm;->Z:Landroid/text/style/UnderlineSpan;

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v9

    invoke-virtual {v1, v5, v3, v9, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    const-string v5, " "

    invoke-virtual {v6, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v6, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_2
    move-object v1, v6

    goto/16 :goto_3

    :cond_3
    sget v5, Lh;->nU:I

    new-array v6, v4, [Ljava/lang/Object;

    aput-object v1, v6, v3

    invoke-virtual {p0, v5, v6}, Lamm;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v5, v1

    move v1, v4

    goto :goto_8

    :pswitch_2
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    sget v1, Lh;->nQ:I

    invoke-virtual {p0, v1}, Lamm;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v5, v1

    move v1, v3

    goto :goto_8

    :cond_4
    sget v5, Lh;->nP:I

    new-array v6, v4, [Ljava/lang/Object;

    aput-object v1, v6, v3

    invoke-virtual {p0, v5, v6}, Lamm;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v5, v1

    move v1, v4

    goto :goto_8

    :pswitch_3
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    sget v1, Lh;->oa:I

    invoke-virtual {p0, v1}, Lamm;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v5, v1

    move v1, v3

    goto :goto_8

    :cond_5
    sget v5, Lh;->nZ:I

    new-array v6, v4, [Ljava/lang/Object;

    aput-object v1, v6, v3

    invoke-virtual {p0, v5, v6}, Lamm;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v5, v1

    move v1, v3

    goto/16 :goto_8

    :pswitch_4
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_6

    sget v1, Lh;->oj:I

    invoke-virtual {p0, v1}, Lamm;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v5, v1

    move v1, v3

    goto/16 :goto_8

    :cond_6
    sget v5, Lh;->oh:I

    new-array v6, v4, [Ljava/lang/Object;

    aput-object v1, v6, v3

    invoke-virtual {p0, v5, v6}, Lamm;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v5, v1

    move v1, v4

    goto/16 :goto_8

    :cond_7
    move v4, v3

    goto/16 :goto_4

    :cond_8
    move v4, v3

    goto/16 :goto_4

    :cond_9
    invoke-virtual {p0}, Lamm;->getActivity()Ly;

    move-result-object v1

    invoke-virtual {p0}, Lamm;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const-string v5, "androidverify"

    sget v6, Lh;->nR:I

    invoke-static {v0, v1, v4, v5, v6}, Lf;->a(Landroid/widget/TextView;Landroid/app/Activity;Landroid/content/res/Resources;Ljava/lang/String;I)V

    goto/16 :goto_5

    :cond_a
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_6

    :cond_b
    invoke-direct {p0}, Lamm;->u()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    invoke-virtual {p0, v1}, Lamm;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Lamp;

    invoke-direct {v4, p0}, Lamp;-><init>(Lamm;)V

    invoke-virtual {v0, v1, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_7

    .line 259
    :pswitch_5
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lamm;->getActivity()Ly;

    move-result-object v0

    invoke-direct {v4, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v0, p0, Lamm;->ad:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, p0, Lamm;->ad:Ljava/lang/String;

    invoke-static {v0}, Lbzd;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lamm;->ad:Ljava/lang/String;

    sget v5, Lbzg;->b:I

    invoke-static {v0, v5}, Lbzd;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    move-object v10, v0

    move-object v0, v1

    move-object v1, v10

    :goto_9
    invoke-static {v0}, Lbro;->a(Ljava/lang/String;)V

    invoke-static {}, Lbro;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lamm;->ag:Ljava/util/List;

    iget-object v0, p0, Lamm;->ag:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbrp;

    iput-object v0, p0, Lamm;->af:Lbrp;

    invoke-virtual {p0}, Lamm;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v5, Lf;->gj:I

    invoke-virtual {v0, v5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    sget v0, Lg;->fn:I

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lamm;->ah:Landroid/widget/EditText;

    iget-object v0, p0, Lamm;->ah:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Lwi;

    iget-object v1, p0, Lamm;->af:Lbrp;

    iget-object v1, v1, Lbrp;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Lwi;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lamm;->ai:Lwi;

    iget-object v0, p0, Lamm;->ah:Landroid/widget/EditText;

    iget-object v1, p0, Lamm;->ai:Lwi;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    sget v0, Lg;->aZ:I

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    sget v1, Lg;->aY:I

    invoke-virtual {v5, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    new-instance v6, Lamy;

    invoke-virtual {p0}, Lamm;->getActivity()Ly;

    move-result-object v7

    iget-object v8, p0, Lamm;->ag:Ljava/util/List;

    invoke-direct {v6, p0, v7, v8}, Lamy;-><init>(Lamm;Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v1, v6}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    new-instance v6, Lamr;

    invoke-direct {v6, p0, v0}, Lamr;-><init>(Lamm;Landroid/widget/EditText;)V

    invoke-virtual {v1, v6}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    sget v0, Lh;->nY:I

    invoke-virtual {p0, v0}, Lamm;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lh;->ax:I

    invoke-virtual {p0, v1}, Lamm;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v5, Lamt;

    invoke-direct {v5, p0}, Lamt;-><init>(Lamm;)V

    invoke-virtual {v0, v1, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    invoke-virtual {p0, v1}, Lamm;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v5, Lams;

    invoke-direct {v5, p0}, Lams;-><init>(Lamm;)V

    invoke-virtual {v0, v1, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    new-instance v1, Lamu;

    invoke-direct {v1, p0, v0}, Lamu;-><init>(Lamm;Landroid/app/AlertDialog;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    iput-object v0, p0, Lamm;->aj:Landroid/app/Dialog;

    goto/16 :goto_1

    :cond_c
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    move-object v1, v2

    goto/16 :goto_9

    .line 264
    :pswitch_6
    iget-object v0, p0, Lamm;->ad:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 265
    const-string v0, "Babel"

    const-string v1, "Phone number should not be empty"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    :cond_d
    invoke-direct {p0}, Lamm;->q()Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lamm;->aj:Landroid/app/Dialog;

    goto/16 :goto_1

    .line 275
    :cond_e
    const-string v0, "Babel"

    const-string v1, "Invalid dialog id"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_f
    move-object v1, v2

    goto/16 :goto_2

    .line 253
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 255
    :pswitch_data_1
    .packed-switch 0x64
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_2
    .end packed-switch
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 868
    iget-object v0, p0, Lamm;->aj:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 875
    invoke-virtual {p0}, Lamm;->e()V

    .line 878
    :cond_0
    invoke-super {p0, p1}, Ls;->onActivityCreated(Landroid/os/Bundle;)V

    .line 879
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 618
    invoke-direct {p0}, Lamm;->s()V

    .line 619
    return-void
.end method
