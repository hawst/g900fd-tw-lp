.class public final Leug;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leug;


# instance fields
.field public b:Levy;

.field public c:Levz;

.field public d:Levt;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Leug;

    sput-object v0, Leug;->a:[Leug;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 13
    iput-object v0, p0, Leug;->b:Levy;

    .line 16
    iput-object v0, p0, Leug;->c:Levz;

    .line 19
    iput-object v0, p0, Leug;->d:Levt;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 39
    const/4 v0, 0x0

    .line 40
    iget-object v1, p0, Leug;->b:Levy;

    if-eqz v1, :cond_0

    .line 41
    const/4 v0, 0x1

    iget-object v1, p0, Leug;->b:Levy;

    .line 42
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 44
    :cond_0
    iget-object v1, p0, Leug;->c:Levz;

    if-eqz v1, :cond_1

    .line 45
    const/4 v1, 0x2

    iget-object v2, p0, Leug;->c:Levz;

    .line 46
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 48
    :cond_1
    iget-object v1, p0, Leug;->d:Levt;

    if-eqz v1, :cond_2

    .line 49
    const/4 v1, 0x3

    iget-object v2, p0, Leug;->d:Levt;

    .line 50
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 52
    :cond_2
    iget-object v1, p0, Leug;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 53
    iput v0, p0, Leug;->cachedSize:I

    .line 54
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Leug;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Leug;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Leug;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Leug;->b:Levy;

    if-nez v0, :cond_2

    new-instance v0, Levy;

    invoke-direct {v0}, Levy;-><init>()V

    iput-object v0, p0, Leug;->b:Levy;

    :cond_2
    iget-object v0, p0, Leug;->b:Levy;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Leug;->c:Levz;

    if-nez v0, :cond_3

    new-instance v0, Levz;

    invoke-direct {v0}, Levz;-><init>()V

    iput-object v0, p0, Leug;->c:Levz;

    :cond_3
    iget-object v0, p0, Leug;->c:Levz;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Leug;->d:Levt;

    if-nez v0, :cond_4

    new-instance v0, Levt;

    invoke-direct {v0}, Levt;-><init>()V

    iput-object v0, p0, Leug;->d:Levt;

    :cond_4
    iget-object v0, p0, Leug;->d:Levt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Leug;->b:Levy;

    if-eqz v0, :cond_0

    .line 25
    const/4 v0, 0x1

    iget-object v1, p0, Leug;->b:Levy;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 27
    :cond_0
    iget-object v0, p0, Leug;->c:Levz;

    if-eqz v0, :cond_1

    .line 28
    const/4 v0, 0x2

    iget-object v1, p0, Leug;->c:Levz;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 30
    :cond_1
    iget-object v0, p0, Leug;->d:Levt;

    if-eqz v0, :cond_2

    .line 31
    const/4 v0, 0x3

    iget-object v1, p0, Leug;->d:Levt;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 33
    :cond_2
    iget-object v0, p0, Leug;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 35
    return-void
.end method
