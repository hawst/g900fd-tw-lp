.class public final Lbfx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final a:J

.field private final b:J

.field private final c:I

.field private final d:I

.field private final e:J

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Z

.field private final i:Z

.field private final j:Z

.field private final k:Z

.field private final l:Z

.field private final m:Z

.field private final n:I

.field private final o:I


# direct methods
.method public constructor <init>(JIILjava/lang/String;JJI)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3146
    iput-wide p1, p0, Lbfx;->a:J

    .line 3147
    iput p3, p0, Lbfx;->c:I

    .line 3148
    iput p4, p0, Lbfx;->d:I

    .line 3149
    iput-object p5, p0, Lbfx;->f:Ljava/lang/String;

    .line 3150
    iput-wide p6, p0, Lbfx;->b:J

    .line 3151
    iput-wide p8, p0, Lbfx;->e:J

    .line 3152
    const/4 v0, 0x0

    iput-object v0, p0, Lbfx;->g:Ljava/lang/String;

    .line 3153
    iput p10, p0, Lbfx;->o:I

    .line 3154
    iput-boolean v1, p0, Lbfx;->h:Z

    .line 3155
    iput-boolean v1, p0, Lbfx;->i:Z

    .line 3156
    iput-boolean v1, p0, Lbfx;->j:Z

    .line 3157
    iput-boolean v1, p0, Lbfx;->k:Z

    .line 3158
    iput-boolean v1, p0, Lbfx;->l:Z

    .line 3159
    iput-boolean v1, p0, Lbfx;->m:Z

    .line 3160
    iput v1, p0, Lbfx;->n:I

    .line 3161
    return-void
.end method

.method public constructor <init>(JIILjava/lang/String;JJZZZZZII)V
    .locals 2

    .prologue
    .line 3125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3126
    iput-wide p1, p0, Lbfx;->a:J

    .line 3127
    iput p3, p0, Lbfx;->c:I

    .line 3128
    iput p4, p0, Lbfx;->d:I

    .line 3129
    iput-object p5, p0, Lbfx;->f:Ljava/lang/String;

    .line 3130
    iput-wide p6, p0, Lbfx;->b:J

    .line 3131
    iput-wide p8, p0, Lbfx;->e:J

    .line 3132
    const/4 v1, 0x0

    iput-object v1, p0, Lbfx;->g:Ljava/lang/String;

    .line 3133
    const/4 v1, 0x1

    iput-boolean v1, p0, Lbfx;->h:Z

    .line 3134
    iput-boolean p10, p0, Lbfx;->i:Z

    .line 3135
    iput-boolean p11, p0, Lbfx;->j:Z

    .line 3136
    iput-boolean p12, p0, Lbfx;->k:Z

    .line 3137
    iput-boolean p13, p0, Lbfx;->l:Z

    .line 3138
    move/from16 v0, p14

    iput-boolean v0, p0, Lbfx;->m:Z

    .line 3139
    move/from16 v0, p15

    iput v0, p0, Lbfx;->n:I

    .line 3140
    move/from16 v0, p16

    iput v0, p0, Lbfx;->o:I

    .line 3141
    return-void
.end method


# virtual methods
.method public a()Ldpj;
    .locals 5

    .prologue
    const-wide/16 v3, 0x0

    .line 3164
    new-instance v0, Ldpj;

    invoke-direct {v0}, Ldpj;-><init>()V

    .line 3165
    iget-wide v1, p0, Lbfx;->e:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Ldpj;->l:Ljava/lang/Long;

    .line 3166
    iget v1, p0, Lbfx;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Ldpj;->b:Ljava/lang/Integer;

    .line 3167
    iget v1, p0, Lbfx;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Ldpj;->o:Ljava/lang/Integer;

    .line 3168
    iget v1, p0, Lbfx;->o:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Ldpj;->q:Ljava/lang/Integer;

    .line 3169
    iget-wide v1, p0, Lbfx;->b:J

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    .line 3171
    iget-wide v1, p0, Lbfx;->b:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Ldpj;->n:Ljava/lang/Long;

    .line 3173
    :cond_0
    iget-wide v1, p0, Lbfx;->a:J

    cmp-long v1, v1, v3

    if-eqz v1, :cond_1

    .line 3175
    iget-wide v1, p0, Lbfx;->a:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Ldpj;->c:Ljava/lang/Long;

    .line 3177
    :cond_1
    iget-object v1, p0, Lbfx;->f:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 3178
    iget-object v1, p0, Lbfx;->f:Ljava/lang/String;

    iput-object v1, v0, Ldpj;->m:Ljava/lang/String;

    .line 3180
    :cond_2
    iget-object v1, p0, Lbfx;->g:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 3181
    iget-object v1, p0, Lbfx;->g:Ljava/lang/String;

    iput-object v1, v0, Ldpj;->d:Ljava/lang/String;

    .line 3183
    :cond_3
    iget-boolean v1, p0, Lbfx;->h:Z

    if-eqz v1, :cond_4

    .line 3184
    iget-boolean v1, p0, Lbfx;->i:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Ldpj;->e:Ljava/lang/Boolean;

    .line 3185
    iget-boolean v1, p0, Lbfx;->j:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Ldpj;->f:Ljava/lang/Boolean;

    .line 3186
    iget-boolean v1, p0, Lbfx;->k:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Ldpj;->g:Ljava/lang/Boolean;

    .line 3187
    iget-boolean v1, p0, Lbfx;->l:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Ldpj;->h:Ljava/lang/Boolean;

    .line 3188
    iget-boolean v1, p0, Lbfx;->m:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Ldpj;->i:Ljava/lang/Boolean;

    .line 3189
    iget v1, p0, Lbfx;->n:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Ldpj;->k:Ljava/lang/Integer;

    .line 3191
    :cond_4
    return-object v0
.end method
