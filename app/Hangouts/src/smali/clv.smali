.class final Lclv;
.super Lckv;


# instance fields
.field final synthetic a:Lclj;

.field private final b:Lcgk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcgk",
            "<",
            "Lcvb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lclj;Lcgk;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcgk",
            "<",
            "Lcvb;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lclv;->a:Lclj;

    invoke-direct {p0}, Lckv;-><init>()V

    iput-object p2, p0, Lclv;->b:Lcgk;

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;)V
    .locals 5

    const-string v0, "PeopleService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Avatar callback: status="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " resolution="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " pfd="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-static {p1, p2}, Lclj;->a(ILandroid/os/Bundle;)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    iget-object v1, p0, Lclv;->a:Lclj;

    new-instance v2, Lclm;

    iget-object v3, p0, Lclv;->a:Lclj;

    iget-object v4, p0, Lclv;->b:Lcgk;

    invoke-direct {v2, v3, v4, v0, p3}, Lclm;-><init>(Lclj;Lcgk;Lcom/google/android/gms/common/api/Status;Landroid/os/ParcelFileDescriptor;)V

    invoke-virtual {v1, v2}, Lclj;->a(Lcht;)V

    return-void
.end method
