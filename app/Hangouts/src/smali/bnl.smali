.class public final Lbnl;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Z


# instance fields
.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbea;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Z

.field private final d:I

.field private e:I

.field private f:Z

.field private g:Lbnm;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lbys;->k:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lbnl;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lbnl;->b:Ljava/util/List;

    .line 54
    iput v1, p0, Lbnl;->e:I

    .line 55
    iput-boolean v1, p0, Lbnl;->c:Z

    .line 56
    iput v1, p0, Lbnl;->d:I

    .line 57
    return-void
.end method

.method private a(JI)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x3e8

    const/4 v1, 0x1

    .line 113
    const/4 v0, 0x4

    if-eq p3, v0, :cond_0

    const/4 v0, 0x3

    if-ne p3, v0, :cond_5

    :cond_0
    move v0, v1

    :goto_0
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 115
    sget-boolean v0, Lbnl;->a:Z

    if-eqz v0, :cond_1

    .line 116
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "setNotificationTrigger eventTime: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", currentTimeMillis: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 117
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", triggerLevel: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", mShouldTriggerNotification: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lbnl;->e:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", mIsFromLivePush: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Lbnl;->f:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 116
    invoke-static {v0}, Lbnl;->a(Ljava/lang/String;)V

    .line 122
    :cond_1
    iget v0, p0, Lbnl;->e:I

    if-ge v0, p3, :cond_4

    .line 123
    iget v0, p0, Lbnl;->e:I

    if-gt v0, v1, :cond_3

    iget-boolean v0, p0, Lbnl;->f:Z

    if-eqz v0, :cond_3

    const-wide/16 v2, -0x1

    cmp-long v0, p1, v2

    if-eqz v0, :cond_3

    .line 129
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 130
    div-long v4, p1, v8

    sub-long v4, v2, v4

    const-string v0, "babel_pushislate"

    const-wide/32 v6, 0xea60

    invoke-static {v0, v6, v7}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;J)J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-lez v0, :cond_3

    .line 134
    sget-boolean v0, Lbnl;->a:Z

    if-eqz v0, :cond_2

    .line 135
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "push is late by "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    div-long v4, p1, v8

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ms, so setting trigger to deferred"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbnl;->a(Ljava/lang/String;)V

    :cond_2
    move p3, v1

    .line 141
    :cond_3
    iput p3, p0, Lbnl;->e:I

    .line 143
    :cond_4
    return-void

    .line 113
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private static a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 262
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[RealTimeChatOperationState] "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbnl;->f:Z

    .line 65
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 152
    sget-boolean v0, Lbnl;->a:Z

    if-eqz v0, :cond_0

    .line 153
    const-string v0, "setShouldTriggerLoudNotification"

    invoke-static {v0}, Lbnl;->a(Ljava/lang/String;)V

    .line 155
    :cond_0
    const/4 v0, 0x4

    invoke-direct {p0, p1, p2, v0}, Lbnl;->a(JI)V

    .line 156
    return-void
.end method

.method public a(Lbea;)V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lbnl;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    return-void
.end method

.method public a(Lyj;)V
    .locals 1

    .prologue
    .line 60
    new-instance v0, Lbnm;

    invoke-direct {v0, p1}, Lbnm;-><init>(Lyj;)V

    iput-object v0, p0, Lbnl;->g:Lbnm;

    .line 61
    return-void
.end method

.method public a(Lyt;Ljava/lang/String;Ljava/util/List;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lyt;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lbdk;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Lbnl;->g:Lbnm;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lbnl;->g:Lbnm;

    invoke-virtual {v0, p1, p2, p3}, Lbnm;->a(Lyt;Ljava/lang/String;Ljava/util/List;)Z

    move-result v0

    .line 81
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbea;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lbnl;->b:Ljava/util/List;

    return-object v0
.end method

.method public b(J)V
    .locals 1

    .prologue
    .line 165
    sget-boolean v0, Lbnl;->a:Z

    if-eqz v0, :cond_0

    .line 166
    const-string v0, "setShouldTriggerSoftNotifications"

    invoke-static {v0}, Lbnl;->a(Ljava/lang/String;)V

    .line 168
    :cond_0
    const/4 v0, 0x3

    invoke-direct {p0, p1, p2, v0}, Lbnl;->a(JI)V

    .line 169
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lbnl;->g:Lbnm;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lbnl;->g:Lbnm;

    invoke-virtual {v0}, Lbnm;->a()V

    .line 88
    :cond_0
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 176
    iget v0, p0, Lbnl;->e:I

    if-ge v0, v1, :cond_1

    .line 177
    sget-boolean v0, Lbnl;->a:Z

    if-eqz v0, :cond_0

    .line 178
    const-string v0, "setShouldTriggerSilentNotifications"

    invoke-static {v0}, Lbnl;->a(Ljava/lang/String;)V

    .line 180
    :cond_0
    iput v1, p0, Lbnl;->e:I

    .line 182
    :cond_1
    return-void
.end method

.method public e()I
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lbnl;->e:I

    return v0
.end method
