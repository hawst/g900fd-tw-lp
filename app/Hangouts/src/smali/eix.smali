.class public final Leix;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leix;


# instance fields
.field public b:Ljava/lang/Float;

.field public c:Leiu;

.field public d:Leiu;

.field public e:Leiu;

.field public f:Leiy;

.field public g:Leiy;

.field public h:Leiy;

.field public i:Leiw;

.field public j:Leiw;

.field public k:Leiw;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 285
    const/4 v0, 0x0

    new-array v0, v0, [Leix;

    sput-object v0, Leix;->a:[Leix;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 286
    invoke-direct {p0}, Lepn;-><init>()V

    .line 291
    iput-object v0, p0, Leix;->c:Leiu;

    .line 294
    iput-object v0, p0, Leix;->d:Leiu;

    .line 297
    iput-object v0, p0, Leix;->e:Leiu;

    .line 300
    iput-object v0, p0, Leix;->f:Leiy;

    .line 303
    iput-object v0, p0, Leix;->g:Leiy;

    .line 306
    iput-object v0, p0, Leix;->h:Leiy;

    .line 309
    iput-object v0, p0, Leix;->i:Leiw;

    .line 312
    iput-object v0, p0, Leix;->j:Leiw;

    .line 315
    iput-object v0, p0, Leix;->k:Leiw;

    .line 286
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 356
    const/4 v0, 0x0

    .line 357
    iget-object v1, p0, Leix;->b:Ljava/lang/Float;

    if-eqz v1, :cond_0

    .line 358
    const/4 v0, 0x1

    iget-object v1, p0, Leix;->b:Ljava/lang/Float;

    .line 359
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    invoke-static {v0}, Lepl;->g(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 361
    :cond_0
    iget-object v1, p0, Leix;->c:Leiu;

    if-eqz v1, :cond_1

    .line 362
    const/4 v1, 0x2

    iget-object v2, p0, Leix;->c:Leiu;

    .line 363
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 365
    :cond_1
    iget-object v1, p0, Leix;->d:Leiu;

    if-eqz v1, :cond_2

    .line 366
    const/4 v1, 0x3

    iget-object v2, p0, Leix;->d:Leiu;

    .line 367
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 369
    :cond_2
    iget-object v1, p0, Leix;->e:Leiu;

    if-eqz v1, :cond_3

    .line 370
    const/4 v1, 0x4

    iget-object v2, p0, Leix;->e:Leiu;

    .line 371
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 373
    :cond_3
    iget-object v1, p0, Leix;->f:Leiy;

    if-eqz v1, :cond_4

    .line 374
    const/4 v1, 0x5

    iget-object v2, p0, Leix;->f:Leiy;

    .line 375
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 377
    :cond_4
    iget-object v1, p0, Leix;->g:Leiy;

    if-eqz v1, :cond_5

    .line 378
    const/4 v1, 0x6

    iget-object v2, p0, Leix;->g:Leiy;

    .line 379
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 381
    :cond_5
    iget-object v1, p0, Leix;->h:Leiy;

    if-eqz v1, :cond_6

    .line 382
    const/4 v1, 0x7

    iget-object v2, p0, Leix;->h:Leiy;

    .line 383
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 385
    :cond_6
    iget-object v1, p0, Leix;->i:Leiw;

    if-eqz v1, :cond_7

    .line 386
    const/16 v1, 0x8

    iget-object v2, p0, Leix;->i:Leiw;

    .line 387
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 389
    :cond_7
    iget-object v1, p0, Leix;->j:Leiw;

    if-eqz v1, :cond_8

    .line 390
    const/16 v1, 0x9

    iget-object v2, p0, Leix;->j:Leiw;

    .line 391
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 393
    :cond_8
    iget-object v1, p0, Leix;->k:Leiw;

    if-eqz v1, :cond_9

    .line 394
    const/16 v1, 0xa

    iget-object v2, p0, Leix;->k:Leiw;

    .line 395
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 397
    :cond_9
    iget-object v1, p0, Leix;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 398
    iput v0, p0, Leix;->cachedSize:I

    .line 399
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 282
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Leix;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Leix;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Leix;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Leix;->b:Ljava/lang/Float;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Leix;->c:Leiu;

    if-nez v0, :cond_2

    new-instance v0, Leiu;

    invoke-direct {v0}, Leiu;-><init>()V

    iput-object v0, p0, Leix;->c:Leiu;

    :cond_2
    iget-object v0, p0, Leix;->c:Leiu;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Leix;->d:Leiu;

    if-nez v0, :cond_3

    new-instance v0, Leiu;

    invoke-direct {v0}, Leiu;-><init>()V

    iput-object v0, p0, Leix;->d:Leiu;

    :cond_3
    iget-object v0, p0, Leix;->d:Leiu;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Leix;->e:Leiu;

    if-nez v0, :cond_4

    new-instance v0, Leiu;

    invoke-direct {v0}, Leiu;-><init>()V

    iput-object v0, p0, Leix;->e:Leiu;

    :cond_4
    iget-object v0, p0, Leix;->e:Leiu;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Leix;->f:Leiy;

    if-nez v0, :cond_5

    new-instance v0, Leiy;

    invoke-direct {v0}, Leiy;-><init>()V

    iput-object v0, p0, Leix;->f:Leiy;

    :cond_5
    iget-object v0, p0, Leix;->f:Leiy;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Leix;->g:Leiy;

    if-nez v0, :cond_6

    new-instance v0, Leiy;

    invoke-direct {v0}, Leiy;-><init>()V

    iput-object v0, p0, Leix;->g:Leiy;

    :cond_6
    iget-object v0, p0, Leix;->g:Leiy;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Leix;->h:Leiy;

    if-nez v0, :cond_7

    new-instance v0, Leiy;

    invoke-direct {v0}, Leiy;-><init>()V

    iput-object v0, p0, Leix;->h:Leiy;

    :cond_7
    iget-object v0, p0, Leix;->h:Leiy;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Leix;->i:Leiw;

    if-nez v0, :cond_8

    new-instance v0, Leiw;

    invoke-direct {v0}, Leiw;-><init>()V

    iput-object v0, p0, Leix;->i:Leiw;

    :cond_8
    iget-object v0, p0, Leix;->i:Leiw;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Leix;->j:Leiw;

    if-nez v0, :cond_9

    new-instance v0, Leiw;

    invoke-direct {v0}, Leiw;-><init>()V

    iput-object v0, p0, Leix;->j:Leiw;

    :cond_9
    iget-object v0, p0, Leix;->j:Leiw;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Leix;->k:Leiw;

    if-nez v0, :cond_a

    new-instance v0, Leiw;

    invoke-direct {v0}, Leiw;-><init>()V

    iput-object v0, p0, Leix;->k:Leiw;

    :cond_a
    iget-object v0, p0, Leix;->k:Leiw;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 320
    iget-object v0, p0, Leix;->b:Ljava/lang/Float;

    if-eqz v0, :cond_0

    .line 321
    const/4 v0, 0x1

    iget-object v1, p0, Leix;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IF)V

    .line 323
    :cond_0
    iget-object v0, p0, Leix;->c:Leiu;

    if-eqz v0, :cond_1

    .line 324
    const/4 v0, 0x2

    iget-object v1, p0, Leix;->c:Leiu;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 326
    :cond_1
    iget-object v0, p0, Leix;->d:Leiu;

    if-eqz v0, :cond_2

    .line 327
    const/4 v0, 0x3

    iget-object v1, p0, Leix;->d:Leiu;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 329
    :cond_2
    iget-object v0, p0, Leix;->e:Leiu;

    if-eqz v0, :cond_3

    .line 330
    const/4 v0, 0x4

    iget-object v1, p0, Leix;->e:Leiu;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 332
    :cond_3
    iget-object v0, p0, Leix;->f:Leiy;

    if-eqz v0, :cond_4

    .line 333
    const/4 v0, 0x5

    iget-object v1, p0, Leix;->f:Leiy;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 335
    :cond_4
    iget-object v0, p0, Leix;->g:Leiy;

    if-eqz v0, :cond_5

    .line 336
    const/4 v0, 0x6

    iget-object v1, p0, Leix;->g:Leiy;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 338
    :cond_5
    iget-object v0, p0, Leix;->h:Leiy;

    if-eqz v0, :cond_6

    .line 339
    const/4 v0, 0x7

    iget-object v1, p0, Leix;->h:Leiy;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 341
    :cond_6
    iget-object v0, p0, Leix;->i:Leiw;

    if-eqz v0, :cond_7

    .line 342
    const/16 v0, 0x8

    iget-object v1, p0, Leix;->i:Leiw;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 344
    :cond_7
    iget-object v0, p0, Leix;->j:Leiw;

    if-eqz v0, :cond_8

    .line 345
    const/16 v0, 0x9

    iget-object v1, p0, Leix;->j:Leiw;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 347
    :cond_8
    iget-object v0, p0, Leix;->k:Leiw;

    if-eqz v0, :cond_9

    .line 348
    const/16 v0, 0xa

    iget-object v1, p0, Leix;->k:Leiw;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 350
    :cond_9
    iget-object v0, p0, Leix;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 352
    return-void
.end method
