.class public final Ldyx;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldyx;


# instance fields
.field public b:Ldvn;

.field public c:[Ldyt;

.field public d:Ldzg;

.field public e:[Ldzf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2307
    const/4 v0, 0x0

    new-array v0, v0, [Ldyx;

    sput-object v0, Ldyx;->a:[Ldyx;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2308
    invoke-direct {p0}, Lepn;-><init>()V

    .line 2311
    iput-object v1, p0, Ldyx;->b:Ldvn;

    .line 2314
    sget-object v0, Ldyt;->a:[Ldyt;

    iput-object v0, p0, Ldyx;->c:[Ldyt;

    .line 2317
    iput-object v1, p0, Ldyx;->d:Ldzg;

    .line 2320
    sget-object v0, Ldzf;->a:[Ldzf;

    iput-object v0, p0, Ldyx;->e:[Ldzf;

    .line 2308
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2352
    iget-object v0, p0, Ldyx;->b:Ldvn;

    if-eqz v0, :cond_5

    .line 2353
    const/4 v0, 0x1

    iget-object v2, p0, Ldyx;->b:Ldvn;

    .line 2354
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2356
    :goto_0
    iget-object v2, p0, Ldyx;->c:[Ldyt;

    if-eqz v2, :cond_1

    .line 2357
    iget-object v3, p0, Ldyx;->c:[Ldyt;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 2358
    if-eqz v5, :cond_0

    .line 2359
    const/4 v6, 0x2

    .line 2360
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 2357
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2364
    :cond_1
    iget-object v2, p0, Ldyx;->d:Ldzg;

    if-eqz v2, :cond_2

    .line 2365
    const/4 v2, 0x3

    iget-object v3, p0, Ldyx;->d:Ldzg;

    .line 2366
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2368
    :cond_2
    iget-object v2, p0, Ldyx;->e:[Ldzf;

    if-eqz v2, :cond_4

    .line 2369
    iget-object v2, p0, Ldyx;->e:[Ldzf;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 2370
    if-eqz v4, :cond_3

    .line 2371
    const/4 v5, 0x4

    .line 2372
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2369
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2376
    :cond_4
    iget-object v1, p0, Ldyx;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2377
    iput v0, p0, Ldyx;->cachedSize:I

    .line 2378
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2304
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldyx;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldyx;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldyx;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldyx;->b:Ldvn;

    if-nez v0, :cond_2

    new-instance v0, Ldvn;

    invoke-direct {v0}, Ldvn;-><init>()V

    iput-object v0, p0, Ldyx;->b:Ldvn;

    :cond_2
    iget-object v0, p0, Ldyx;->b:Ldvn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldyx;->c:[Ldyt;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldyt;

    iget-object v3, p0, Ldyx;->c:[Ldyt;

    if-eqz v3, :cond_3

    iget-object v3, p0, Ldyx;->c:[Ldyt;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Ldyx;->c:[Ldyt;

    :goto_2
    iget-object v2, p0, Ldyx;->c:[Ldyt;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Ldyx;->c:[Ldyt;

    new-instance v3, Ldyt;

    invoke-direct {v3}, Ldyt;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldyx;->c:[Ldyt;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Ldyx;->c:[Ldyt;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Ldyx;->c:[Ldyt;

    new-instance v3, Ldyt;

    invoke-direct {v3}, Ldyt;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldyx;->c:[Ldyt;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldyx;->d:Ldzg;

    if-nez v0, :cond_6

    new-instance v0, Ldzg;

    invoke-direct {v0}, Ldzg;-><init>()V

    iput-object v0, p0, Ldyx;->d:Ldzg;

    :cond_6
    iget-object v0, p0, Ldyx;->d:Ldzg;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldyx;->e:[Ldzf;

    if-nez v0, :cond_8

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ldzf;

    iget-object v3, p0, Ldyx;->e:[Ldzf;

    if-eqz v3, :cond_7

    iget-object v3, p0, Ldyx;->e:[Ldzf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    iput-object v2, p0, Ldyx;->e:[Ldzf;

    :goto_4
    iget-object v2, p0, Ldyx;->e:[Ldzf;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    iget-object v2, p0, Ldyx;->e:[Ldzf;

    new-instance v3, Ldzf;

    invoke-direct {v3}, Ldzf;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldyx;->e:[Ldzf;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_8
    iget-object v0, p0, Ldyx;->e:[Ldzf;

    array-length v0, v0

    goto :goto_3

    :cond_9
    iget-object v2, p0, Ldyx;->e:[Ldzf;

    new-instance v3, Ldzf;

    invoke-direct {v3}, Ldzf;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldyx;->e:[Ldzf;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2325
    iget-object v1, p0, Ldyx;->b:Ldvn;

    if-eqz v1, :cond_0

    .line 2326
    const/4 v1, 0x1

    iget-object v2, p0, Ldyx;->b:Ldvn;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 2328
    :cond_0
    iget-object v1, p0, Ldyx;->c:[Ldyt;

    if-eqz v1, :cond_2

    .line 2329
    iget-object v2, p0, Ldyx;->c:[Ldyt;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 2330
    if-eqz v4, :cond_1

    .line 2331
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 2329
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2335
    :cond_2
    iget-object v1, p0, Ldyx;->d:Ldzg;

    if-eqz v1, :cond_3

    .line 2336
    const/4 v1, 0x3

    iget-object v2, p0, Ldyx;->d:Ldzg;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 2338
    :cond_3
    iget-object v1, p0, Ldyx;->e:[Ldzf;

    if-eqz v1, :cond_5

    .line 2339
    iget-object v1, p0, Ldyx;->e:[Ldzf;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 2340
    if-eqz v3, :cond_4

    .line 2341
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 2339
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2345
    :cond_5
    iget-object v0, p0, Ldyx;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 2347
    return-void
.end method
