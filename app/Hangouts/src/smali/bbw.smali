.class public final Lbbw;
.super Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/hangouts/video/SafeAsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/ArrayList",
        "<",
        "Lbdh;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Ljava/lang/String;

.field private final c:Lyj;

.field private final d:I

.field private final e:Z

.field private final f:Z

.field private final g:I

.field private final h:Z

.field private final i:J


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lyj;Ljava/lang/String;ZIZIZ)V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;-><init>()V

    .line 37
    iput-object p1, p0, Lbbw;->a:Landroid/app/Activity;

    .line 38
    iput-object p2, p0, Lbbw;->c:Lyj;

    .line 39
    iput-object p3, p0, Lbbw;->b:Ljava/lang/String;

    .line 40
    iput-boolean p4, p0, Lbbw;->e:Z

    .line 41
    iput p5, p0, Lbbw;->d:I

    .line 42
    iput-boolean p6, p0, Lbbw;->f:Z

    .line 43
    iput p7, p0, Lbbw;->g:I

    .line 44
    iput-boolean p8, p0, Lbbw;->h:Z

    .line 45
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lbbw;->i:J

    .line 46
    return-void
.end method


# virtual methods
.method protected synthetic doInBackgroundTimed([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 23
    new-instance v0, Lyt;

    iget-object v1, p0, Lbbw;->c:Lyj;

    invoke-direct {v0, v1}, Lyt;-><init>(Lyj;)V

    iget-object v1, p0, Lbbw;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lyt;->K(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 13

    .prologue
    const/4 v10, 0x2

    const/4 v6, 0x0

    const/4 v2, 0x1

    .line 23
    check-cast p1, Ljava/util/ArrayList;

    iget v0, p0, Lbbw;->d:I

    if-ne v0, v2, :cond_3

    move v3, v10

    :goto_0
    new-instance v0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    iget-object v1, p0, Lbbw;->c:Lyj;

    invoke-virtual {v1}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    const-string v4, "conversation"

    iget-object v5, p0, Lbbw;->b:Ljava/lang/String;

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lbbw;->c:Lyj;

    invoke-virtual {v1}, Lyj;->c()Lbdk;

    move-result-object v4

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbdh;

    iget-object v7, v1, Lbdh;->b:Lbdk;

    invoke-virtual {v7, v4}, Lbdk;->a(Lbdk;)Z

    move-result v7

    if-eqz v7, :cond_0

    :goto_1
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-boolean v1, p0, Lbbw;->e:Z

    if-eqz v1, :cond_5

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_4

    iget-boolean v7, p0, Lbbw;->f:Z

    iget v8, p0, Lbbw;->g:I

    iget-wide v9, p0, Lbbw;->i:J

    move-object v4, v0

    move-object v5, p1

    invoke-static/range {v4 .. v10}, Lbbl;->a(Lcom/google/android/libraries/hangouts/video/HangoutRequest;Ljava/util/ArrayList;Ljava/util/ArrayList;ZIJ)Landroid/content/Intent;

    move-result-object v6

    :goto_2
    if-eqz v6, :cond_1

    iget-object v0, p0, Lbbw;->a:Landroid/app/Activity;

    invoke-virtual {v0, v6}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, Lbbw;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_1
    iget-boolean v0, p0, Lbbw;->h:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbbw;->a:Landroid/app/Activity;

    sget v1, Lf;->be:I

    sget v2, Lf;->bf:I

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->overridePendingTransition(II)V

    :cond_2
    return-void

    :cond_3
    move v3, v2

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lbbw;->a:Landroid/app/Activity;

    iget-object v1, p0, Lbbw;->a:Landroid/app/Activity;

    sget v3, Lh;->cv:I

    invoke-virtual {v1, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_2

    :cond_5
    if-ne v3, v2, :cond_6

    move v7, v2

    :goto_3
    iget-boolean v8, p0, Lbbw;->f:Z

    iget v9, p0, Lbbw;->g:I

    iget-wide v11, p0, Lbbw;->i:J

    move-object v4, v0

    move-object v5, p1

    invoke-static/range {v4 .. v12}, Lbbl;->a(Lcom/google/android/libraries/hangouts/video/HangoutRequest;Ljava/util/ArrayList;Lbdh;ZZIIJ)Landroid/content/Intent;

    move-result-object v6

    goto :goto_2

    :cond_6
    const/4 v7, 0x0

    goto :goto_3

    :cond_7
    move-object v1, v6

    goto :goto_1
.end method
