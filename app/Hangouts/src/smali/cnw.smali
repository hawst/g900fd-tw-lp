.class public final Lcnw;
.super Ljava/lang/Object;


# instance fields
.field private final a:[B

.field private final b:I

.field private c:I


# direct methods
.method private constructor <init>([BI)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcnw;->a:[B

    const/4 v0, 0x0

    iput v0, p0, Lcnw;->c:I

    iput p2, p0, Lcnw;->b:I

    return-void
.end method

.method public static a(I)I
    .locals 1

    if-ltz p0, :cond_0

    invoke-static {p0}, Lcnw;->d(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0xa

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)I
    .locals 2

    :try_start_0
    const-string v0, "UTF-8"

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    array-length v1, v0

    invoke-static {v1}, Lcnw;->d(I)I

    move-result v1

    array-length v0, v0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/2addr v0, v1

    return v0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "UTF-8 not supported."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a([BI)Lcnw;
    .locals 1

    new-instance v0, Lcnw;

    invoke-direct {v0, p0, p1}, Lcnw;-><init>([BI)V

    return-object v0
.end method

.method public static b(I)I
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcod;->a(II)I

    move-result v0

    invoke-static {v0}, Lcnw;->d(I)I

    move-result v0

    return v0
.end method

.method public static b(II)I
    .locals 2

    invoke-static {p0}, Lcnw;->b(I)I

    move-result v0

    invoke-static {p1}, Lcnw;->a(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static b(ILcob;)I
    .locals 2

    invoke-static {p0}, Lcnw;->b(I)I

    move-result v0

    shl-int/lit8 v0, v0, 0x1

    invoke-virtual {p1}, Lcob;->d()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static b(ILjava/lang/String;)I
    .locals 2

    invoke-static {p0}, Lcnw;->b(I)I

    move-result v0

    invoke-static {p1}, Lcnw;->a(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static b(I[B)I
    .locals 3

    invoke-static {p0}, Lcnw;->b(I)I

    move-result v0

    array-length v1, p1

    invoke-static {v1}, Lcnw;->d(I)I

    move-result v1

    array-length v2, p1

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method private b(J)V
    .locals 4

    :goto_0
    const-wide/16 v0, -0x80

    and-long/2addr v0, p1

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    long-to-int v0, p1

    invoke-direct {p0, v0}, Lcnw;->e(I)V

    return-void

    :cond_0
    long-to-int v0, p1

    and-int/lit8 v0, v0, 0x7f

    or-int/lit16 v0, v0, 0x80

    invoke-direct {p0, v0}, Lcnw;->e(I)V

    const/4 v0, 0x7

    ushr-long/2addr p1, v0

    goto :goto_0
.end method

.method public static c(ILcob;)I
    .locals 3

    invoke-static {p0}, Lcnw;->b(I)I

    move-result v0

    invoke-virtual {p1}, Lcob;->d()I

    move-result v1

    invoke-static {v1}, Lcnw;->d(I)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method public static d(I)I
    .locals 1

    and-int/lit8 v0, p0, -0x80

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    and-int/lit16 v0, p0, -0x4000

    if-nez v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const/high16 v0, -0x200000

    and-int/2addr v0, p0

    if-nez v0, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    :cond_2
    const/high16 v0, -0x10000000

    and-int/2addr v0, p0

    if-nez v0, :cond_3

    const/4 v0, 0x4

    goto :goto_0

    :cond_3
    const/4 v0, 0x5

    goto :goto_0
.end method

.method private e(I)V
    .locals 4

    int-to-byte v0, p1

    iget v1, p0, Lcnw;->c:I

    iget v2, p0, Lcnw;->b:I

    if-ne v1, v2, :cond_0

    new-instance v0, Lavh;

    iget v1, p0, Lcnw;->c:I

    iget v2, p0, Lcnw;->b:I

    invoke-direct {v0, v1, v2}, Lavh;-><init>(II)V

    throw v0

    :cond_0
    iget-object v1, p0, Lcnw;->a:[B

    iget v2, p0, Lcnw;->c:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcnw;->c:I

    aput-byte v0, v1, v2

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    iget v0, p0, Lcnw;->b:I

    iget v1, p0, Lcnw;->c:I

    sub-int/2addr v0, v1

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Did not write as much data as expected."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public a(II)V
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcnw;->c(II)V

    if-ltz p2, :cond_0

    invoke-virtual {p0, p2}, Lcnw;->c(I)V

    :goto_0
    return-void

    :cond_0
    int-to-long v0, p2

    invoke-direct {p0, v0, v1}, Lcnw;->b(J)V

    goto :goto_0
.end method

.method public a(ILcob;)V
    .locals 1

    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Lcnw;->c(II)V

    invoke-virtual {p0, p2}, Lcnw;->b(Lcob;)V

    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 2

    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Lcnw;->c(II)V

    const-string v0, "UTF-8"

    invoke-virtual {p2, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    array-length v1, v0

    invoke-virtual {p0, v1}, Lcnw;->c(I)V

    invoke-virtual {p0, v0}, Lcnw;->a([B)V

    return-void
.end method

.method public a(I[B)V
    .locals 1

    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Lcnw;->c(II)V

    array-length v0, p2

    invoke-virtual {p0, v0}, Lcnw;->c(I)V

    invoke-virtual {p0, p2}, Lcnw;->a([B)V

    return-void
.end method

.method public a(J)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcnw;->c(II)V

    invoke-direct {p0, p1, p2}, Lcnw;->b(J)V

    return-void
.end method

.method public a(Lcob;)V
    .locals 0

    invoke-virtual {p1, p0}, Lcob;->a(Lcnw;)V

    return-void
.end method

.method public a(Z)V
    .locals 2

    const/4 v0, 0x0

    const/16 v1, 0xa

    invoke-virtual {p0, v1, v0}, Lcnw;->c(II)V

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-direct {p0, v0}, Lcnw;->e(I)V

    return-void
.end method

.method public a([B)V
    .locals 4

    array-length v0, p1

    iget v1, p0, Lcnw;->b:I

    iget v2, p0, Lcnw;->c:I

    sub-int/2addr v1, v2

    if-lt v1, v0, :cond_0

    const/4 v1, 0x0

    iget-object v2, p0, Lcnw;->a:[B

    iget v3, p0, Lcnw;->c:I

    invoke-static {p1, v1, v2, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v1, p0, Lcnw;->c:I

    add-int/2addr v0, v1

    iput v0, p0, Lcnw;->c:I

    return-void

    :cond_0
    new-instance v0, Lavh;

    iget v1, p0, Lcnw;->c:I

    iget v2, p0, Lcnw;->b:I

    invoke-direct {v0, v1, v2}, Lavh;-><init>(II)V

    throw v0
.end method

.method public b(Lcob;)V
    .locals 1

    invoke-virtual {p1}, Lcob;->c()I

    move-result v0

    invoke-virtual {p0, v0}, Lcnw;->c(I)V

    invoke-virtual {p1, p0}, Lcob;->a(Lcnw;)V

    return-void
.end method

.method public c(I)V
    .locals 1

    :goto_0
    and-int/lit8 v0, p1, -0x80

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcnw;->e(I)V

    return-void

    :cond_0
    and-int/lit8 v0, p1, 0x7f

    or-int/lit16 v0, v0, 0x80

    invoke-direct {p0, v0}, Lcnw;->e(I)V

    ushr-int/lit8 p1, p1, 0x7

    goto :goto_0
.end method

.method public c(II)V
    .locals 1

    invoke-static {p1, p2}, Lcod;->a(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcnw;->c(I)V

    return-void
.end method
