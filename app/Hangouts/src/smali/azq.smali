.class public final Lazq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/widget/ExpandableListAdapter;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/phone/DebugBitmapsActivity;

.field private b:Lbxt;

.field private c:Lbxy;

.field private d:Lazr;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/hangouts/phone/DebugBitmapsActivity;)V
    .locals 2

    .prologue
    .line 165
    iput-object p1, p0, Lazq;->a:Lcom/google/android/apps/hangouts/phone/DebugBitmapsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166
    new-instance v0, Lbxt;

    invoke-virtual {p1}, Lcom/google/android/apps/hangouts/phone/DebugBitmapsActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-direct {v0, v1}, Lbxt;-><init>(Landroid/view/LayoutInflater;)V

    iput-object v0, p0, Lazq;->b:Lbxt;

    .line 168
    :try_start_0
    new-instance v0, Lbxy;

    invoke-virtual {p1}, Lcom/google/android/apps/hangouts/phone/DebugBitmapsActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-direct {v0, v1}, Lbxy;-><init>(Landroid/view/LayoutInflater;)V

    iput-object v0, p0, Lazq;->c:Lbxy;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 172
    :goto_0
    new-instance v0, Lazr;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lazr;-><init>(Lcom/google/android/apps/hangouts/phone/DebugBitmapsActivity;B)V

    iput-object v0, p0, Lazq;->d:Lazr;

    .line 173
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/hangouts/phone/DebugBitmapsActivity;B)V
    .locals 0

    .prologue
    .line 160
    invoke-direct {p0, p1}, Lazq;-><init>(Lcom/google/android/apps/hangouts/phone/DebugBitmapsActivity;)V

    return-void
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 272
    const/4 v0, 0x1

    return v0
.end method

.method public getChild(II)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 212
    packed-switch p1, :pswitch_data_0

    .line 217
    :cond_0
    :goto_0
    return-object v0

    .line 213
    :pswitch_0
    iget-object v0, p0, Lazq;->b:Lbxt;

    invoke-virtual {v0, p2}, Lbxt;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 214
    :pswitch_1
    iget-object v1, p0, Lazq;->c:Lbxy;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lazq;->c:Lbxy;

    invoke-virtual {v0, p2}, Lbxy;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 215
    :pswitch_2
    iget-object v0, p0, Lazq;->d:Lazr;

    invoke-virtual {v0, p2}, Lazr;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 212
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getChildId(II)J
    .locals 2

    .prologue
    .line 227
    int-to-long v0, p2

    return-wide v0
.end method

.method public getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 256
    packed-switch p1, :pswitch_data_0

    .line 262
    :cond_0
    :goto_0
    return-object v0

    .line 257
    :pswitch_0
    iget-object v0, p0, Lazq;->b:Lbxt;

    invoke-virtual {v0, p2, p4, p5}, Lbxt;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 258
    :pswitch_1
    iget-object v1, p0, Lazq;->c:Lbxy;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lazq;->c:Lbxy;

    .line 259
    invoke-virtual {v0, p2, p4, p5}, Lbxy;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 260
    :pswitch_2
    iget-object v0, p0, Lazq;->d:Lazr;

    invoke-virtual {v0, p2, p4, p5}, Lazr;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 256
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getChildrenCount(I)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 192
    packed-switch p1, :pswitch_data_0

    .line 197
    :cond_0
    :goto_0
    return v0

    .line 193
    :pswitch_0
    iget-object v0, p0, Lazq;->b:Lbxt;

    invoke-virtual {v0}, Lbxt;->getCount()I

    move-result v0

    goto :goto_0

    .line 194
    :pswitch_1
    iget-object v1, p0, Lazq;->c:Lbxy;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lazq;->c:Lbxy;

    invoke-virtual {v0}, Lbxy;->getCount()I

    move-result v0

    goto :goto_0

    .line 195
    :pswitch_2
    iget-object v0, p0, Lazq;->d:Lazr;

    invoke-virtual {v0}, Lazr;->getCount()I

    move-result v0

    goto :goto_0

    .line 192
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getCombinedChildId(JJ)J
    .locals 2

    .prologue
    .line 292
    const-wide/16 v0, 0x3e8

    mul-long/2addr v0, p1

    add-long/2addr v0, p3

    return-wide v0
.end method

.method public getCombinedGroupId(J)J
    .locals 0

    .prologue
    .line 297
    return-wide p1
.end method

.method public getGroup(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 202
    packed-switch p1, :pswitch_data_0

    .line 207
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 203
    :pswitch_0
    iget-object v0, p0, Lazq;->b:Lbxt;

    goto :goto_0

    .line 204
    :pswitch_1
    iget-object v0, p0, Lazq;->c:Lbxy;

    goto :goto_0

    .line 205
    :pswitch_2
    iget-object v0, p0, Lazq;->d:Lazr;

    goto :goto_0

    .line 202
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getGroupCount()I
    .locals 1

    .prologue
    .line 187
    const/4 v0, 0x3

    return v0
.end method

.method public getGroupId(I)J
    .locals 2

    .prologue
    .line 222
    int-to-long v0, p1

    return-wide v0
.end method

.method public getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 237
    check-cast p3, Landroid/widget/TextView;

    .line 238
    if-nez p3, :cond_0

    .line 239
    new-instance p3, Landroid/widget/TextView;

    iget-object v0, p0, Lazq;->a:Lcom/google/android/apps/hangouts/phone/DebugBitmapsActivity;

    invoke-direct {p3, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 240
    const/4 v0, 0x2

    const/high16 v1, 0x42100000    # 36.0f

    invoke-virtual {p3, v0, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 242
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 250
    :goto_0
    return-object p3

    .line 243
    :pswitch_0
    const-string v0, "   Cached Bitmaps"

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 245
    :pswitch_1
    const-string v0, "   Pooled Bitmaps"

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 247
    :pswitch_2
    const-string v0, "   Default Bitmaps"

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 242
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 232
    const/4 v0, 0x0

    return v0
.end method

.method public isChildSelectable(II)Z
    .locals 1

    .prologue
    .line 267
    const/4 v0, 0x1

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 277
    const/4 v0, 0x0

    return v0
.end method

.method public onGroupCollapsed(I)V
    .locals 0

    .prologue
    .line 288
    return-void
.end method

.method public onGroupExpanded(I)V
    .locals 0

    .prologue
    .line 283
    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 0

    .prologue
    .line 178
    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 0

    .prologue
    .line 183
    return-void
.end method
