.class public Lbud;
.super Lbuc;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field private b:Lbue;

.field private c:Lbme;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lbuc;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 31
    invoke-super {p0, p1}, Lbuc;->onCreate(Landroid/os/Bundle;)V

    .line 33
    new-instance v0, Lbue;

    invoke-direct {v0, p0}, Lbue;-><init>(Lbtz;)V

    iput-object v0, p0, Lbud;->b:Lbue;

    .line 34
    iget-object v0, p0, Lbud;->b:Lbue;

    invoke-virtual {p0}, Lbud;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "account_name"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbue;->a(Ljava/lang/String;)V

    .line 35
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lbme;

    invoke-static {v0, v1}, Lcyj;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbme;

    iput-object v0, p0, Lbud;->c:Lbme;

    .line 36
    iget-object v0, p0, Lbud;->c:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v1, 0x639

    .line 37
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 36
    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    .line 38
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 54
    invoke-super {p0}, Lbuc;->onDestroy()V

    .line 55
    iget-object v0, p0, Lbud;->b:Lbue;

    invoke-virtual {v0}, Lbue;->b()V

    .line 56
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 60
    invoke-super {p0}, Lbuc;->onDestroyView()V

    .line 61
    iget-object v0, p0, Lbud;->b:Lbue;

    invoke-virtual {v0}, Lbue;->c()V

    .line 62
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 45
    invoke-super {p0}, Lbuc;->onResume()V

    .line 46
    iget-object v0, p0, Lbud;->b:Lbue;

    invoke-virtual {v0}, Lbue;->a()V

    .line 47
    return-void
.end method
