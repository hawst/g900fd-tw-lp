.class public final Ldiu;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldiu;


# instance fields
.field public b:Ldcm;

.field public c:Ljava/lang/String;

.field public d:Ldcp;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Ldiu;

    sput-object v0, Ldiu;->a:[Ldiu;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 13
    iput-object v0, p0, Ldiu;->b:Ldcm;

    .line 18
    iput-object v0, p0, Ldiu;->d:Ldcp;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 38
    const/4 v0, 0x0

    .line 39
    iget-object v1, p0, Ldiu;->b:Ldcm;

    if-eqz v1, :cond_0

    .line 40
    const/4 v0, 0x1

    iget-object v1, p0, Ldiu;->b:Ldcm;

    .line 41
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 43
    :cond_0
    iget-object v1, p0, Ldiu;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 44
    const/4 v1, 0x2

    iget-object v2, p0, Ldiu;->c:Ljava/lang/String;

    .line 45
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 47
    :cond_1
    iget-object v1, p0, Ldiu;->d:Ldcp;

    if-eqz v1, :cond_2

    .line 48
    const/4 v1, 0x3

    iget-object v2, p0, Ldiu;->d:Ldcp;

    .line 49
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51
    :cond_2
    iget-object v1, p0, Ldiu;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 52
    iput v0, p0, Ldiu;->cachedSize:I

    .line 53
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldiu;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldiu;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldiu;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldiu;->b:Ldcm;

    if-nez v0, :cond_2

    new-instance v0, Ldcm;

    invoke-direct {v0}, Ldcm;-><init>()V

    iput-object v0, p0, Ldiu;->b:Ldcm;

    :cond_2
    iget-object v0, p0, Ldiu;->b:Ldcm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldiu;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldiu;->d:Ldcp;

    if-nez v0, :cond_3

    new-instance v0, Ldcp;

    invoke-direct {v0}, Ldcp;-><init>()V

    iput-object v0, p0, Ldiu;->d:Ldcp;

    :cond_3
    iget-object v0, p0, Ldiu;->d:Ldcp;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 23
    iget-object v0, p0, Ldiu;->b:Ldcm;

    if-eqz v0, :cond_0

    .line 24
    const/4 v0, 0x1

    iget-object v1, p0, Ldiu;->b:Ldcm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 26
    :cond_0
    iget-object v0, p0, Ldiu;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 27
    const/4 v0, 0x2

    iget-object v1, p0, Ldiu;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 29
    :cond_1
    iget-object v0, p0, Ldiu;->d:Ldcp;

    if-eqz v0, :cond_2

    .line 30
    const/4 v0, 0x3

    iget-object v1, p0, Ldiu;->d:Ldcp;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 32
    :cond_2
    iget-object v0, p0, Ldiu;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 34
    return-void
.end method
