.class public final Ldes;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldes;


# instance fields
.field public b:Ldfi;

.field public c:Ldfi;

.field public d:[Lder;

.field public e:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2474
    const/4 v0, 0x0

    new-array v0, v0, [Ldes;

    sput-object v0, Ldes;->a:[Ldes;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2475
    invoke-direct {p0}, Lepn;-><init>()V

    .line 2478
    iput-object v0, p0, Ldes;->b:Ldfi;

    .line 2481
    iput-object v0, p0, Ldes;->c:Ldfi;

    .line 2484
    sget-object v0, Lder;->a:[Lder;

    iput-object v0, p0, Ldes;->d:[Lder;

    .line 2475
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2514
    iget-object v0, p0, Ldes;->b:Ldfi;

    if-eqz v0, :cond_4

    .line 2515
    const/4 v0, 0x1

    iget-object v2, p0, Ldes;->b:Ldfi;

    .line 2516
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2518
    :goto_0
    iget-object v2, p0, Ldes;->d:[Lder;

    if-eqz v2, :cond_1

    .line 2519
    iget-object v2, p0, Ldes;->d:[Lder;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 2520
    if-eqz v4, :cond_0

    .line 2521
    const/4 v5, 0x2

    .line 2522
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2519
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2526
    :cond_1
    iget-object v1, p0, Ldes;->c:Ldfi;

    if-eqz v1, :cond_2

    .line 2527
    const/4 v1, 0x3

    iget-object v2, p0, Ldes;->c:Ldfi;

    .line 2528
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2530
    :cond_2
    iget-object v1, p0, Ldes;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 2531
    const/4 v1, 0x4

    iget-object v2, p0, Ldes;->e:Ljava/lang/Boolean;

    .line 2532
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2534
    :cond_3
    iget-object v1, p0, Ldes;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2535
    iput v0, p0, Ldes;->cachedSize:I

    .line 2536
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2471
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldes;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldes;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldes;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldes;->b:Ldfi;

    if-nez v0, :cond_2

    new-instance v0, Ldfi;

    invoke-direct {v0}, Ldfi;-><init>()V

    iput-object v0, p0, Ldes;->b:Ldfi;

    :cond_2
    iget-object v0, p0, Ldes;->b:Ldfi;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldes;->d:[Lder;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lder;

    iget-object v3, p0, Ldes;->d:[Lder;

    if-eqz v3, :cond_3

    iget-object v3, p0, Ldes;->d:[Lder;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Ldes;->d:[Lder;

    :goto_2
    iget-object v2, p0, Ldes;->d:[Lder;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Ldes;->d:[Lder;

    new-instance v3, Lder;

    invoke-direct {v3}, Lder;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldes;->d:[Lder;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Ldes;->d:[Lder;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Ldes;->d:[Lder;

    new-instance v3, Lder;

    invoke-direct {v3}, Lder;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldes;->d:[Lder;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldes;->c:Ldfi;

    if-nez v0, :cond_6

    new-instance v0, Ldfi;

    invoke-direct {v0}, Ldfi;-><init>()V

    iput-object v0, p0, Ldes;->c:Ldfi;

    :cond_6
    iget-object v0, p0, Ldes;->c:Ldfi;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldes;->e:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 2491
    iget-object v0, p0, Ldes;->b:Ldfi;

    if-eqz v0, :cond_0

    .line 2492
    const/4 v0, 0x1

    iget-object v1, p0, Ldes;->b:Ldfi;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 2494
    :cond_0
    iget-object v0, p0, Ldes;->d:[Lder;

    if-eqz v0, :cond_2

    .line 2495
    iget-object v1, p0, Ldes;->d:[Lder;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 2496
    if-eqz v3, :cond_1

    .line 2497
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 2495
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2501
    :cond_2
    iget-object v0, p0, Ldes;->c:Ldfi;

    if-eqz v0, :cond_3

    .line 2502
    const/4 v0, 0x3

    iget-object v1, p0, Ldes;->c:Ldfi;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 2504
    :cond_3
    iget-object v0, p0, Ldes;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 2505
    const/4 v0, 0x4

    iget-object v1, p0, Ldes;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 2507
    :cond_4
    iget-object v0, p0, Ldes;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 2509
    return-void
.end method
