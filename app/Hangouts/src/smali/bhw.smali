.class public Lbhw;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private final g:Ljava/lang/String;

.field private final h:J

.field private final i:[Ljava/lang/String;

.field private final j:[Ljava/lang/String;

.field private final k:[Ljava/lang/String;

.field private final l:[Ljava/lang/String;

.field private final m:[B

.field private n:Z

.field private final o:J


# direct methods
.method private constructor <init>(Ldvv;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 708
    iget-object v0, p1, Ldvv;->b:Ldvn;

    iget-object v1, p1, Ldvv;->e:Ldrr;

    iget-object v1, v1, Ldrr;->d:Ljava/lang/Long;

    .line 709
    invoke-static {v1}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v3

    .line 708
    invoke-direct {p0, v0, v3, v4}, Lbfz;-><init>(Ldvn;J)V

    .line 710
    iget-object v0, p1, Ldvv;->e:Ldrr;

    iget-object v0, v0, Ldrr;->e:Ljava/lang/String;

    iput-object v0, p0, Lbhw;->g:Ljava/lang/String;

    .line 711
    iget-object v0, p1, Ldvv;->e:Ldrr;

    iget-object v0, v0, Ldrr;->o:Ljava/lang/Long;

    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v0

    iput-wide v0, p0, Lbhw;->h:J

    .line 713
    iget-object v0, p1, Ldvv;->d:[Ldtv;

    array-length v4, v0

    .line 714
    new-array v0, v4, [Ljava/lang/String;

    iput-object v0, p0, Lbhw;->j:[Ljava/lang/String;

    .line 715
    new-array v0, v4, [Ljava/lang/String;

    iput-object v0, p0, Lbhw;->k:[Ljava/lang/String;

    .line 716
    new-array v0, v4, [Ljava/lang/String;

    iput-object v0, p0, Lbhw;->l:[Ljava/lang/String;

    .line 717
    new-array v0, v4, [Ljava/lang/String;

    iput-object v0, p0, Lbhw;->i:[Ljava/lang/String;

    .line 718
    iget-object v0, p1, Ldvv;->b:Ldvn;

    iget-object v0, v0, Ldvn;->e:Ljava/lang/Long;

    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v0

    iput-wide v0, p0, Lbhw;->o:J

    .line 721
    iget-object v0, p1, Ldvv;->e:Ldrr;

    iget-object v0, v0, Ldrr;->f:Ldrs;

    if-eqz v0, :cond_1

    iget-object v0, p1, Ldvv;->e:Ldrr;

    iget-object v0, v0, Ldrr;->f:Ldrs;

    iget-object v0, v0, Ldrs;->e:Ldxe;

    if-eqz v0, :cond_1

    .line 723
    iget-object v0, p1, Ldvv;->e:Ldrr;

    iget-object v0, v0, Ldrr;->f:Ldrs;

    iget-object v0, v0, Ldrs;->e:Ldxe;

    invoke-static {v0}, Lepr;->toByteArray(Lepr;)[B

    move-result-object v0

    iput-object v0, p0, Lbhw;->m:[B

    :goto_0
    move v3, v2

    .line 728
    :goto_1
    if-ge v3, v4, :cond_3

    .line 729
    iget-object v0, p1, Ldvv;->d:[Ldtv;

    aget-object v0, v0, v3

    iget-object v0, v0, Ldtv;->b:Lesc;

    .line 730
    iget-object v0, v0, Lesc;->b:Lepu;

    .line 731
    sget-object v1, Lerj;->b:Lepo;

    invoke-virtual {v0, v1}, Lepu;->getExtension(Lepo;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lerj;

    .line 732
    iget-object v1, p0, Lbhw;->j:[Ljava/lang/String;

    iget-object v5, v0, Lerj;->f:Ljava/lang/String;

    aput-object v5, v1, v3

    .line 733
    iget-object v1, p0, Lbhw;->k:[Ljava/lang/String;

    iget-object v5, v0, Lerj;->e:Ljava/lang/String;

    aput-object v5, v1, v3

    .line 734
    iget-object v1, p0, Lbhw;->l:[Ljava/lang/String;

    iget-object v5, v0, Lerj;->i:Ljava/lang/String;

    aput-object v5, v1, v3

    .line 737
    iget-object v1, v0, Lerj;->g:[Ljava/lang/String;

    array-length v5, v1

    move v1, v2

    .line 738
    :goto_2
    if-ge v1, v5, :cond_0

    .line 739
    iget-object v6, v0, Lerj;->g:[Ljava/lang/String;

    aget-object v6, v6, v1

    .line 740
    const-string v7, "BABEL_UNIQUE_ID"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 741
    iget-object v0, p0, Lbhw;->i:[Ljava/lang/String;

    aput-object v6, v0, v3

    .line 728
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 726
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lbhw;->m:[B

    goto :goto_0

    .line 738
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 746
    :cond_3
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_4

    .line 747
    const-string v0, "Babel_protos"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SendChatMessageResponse from:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 749
    :cond_4
    return-void
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 753
    new-instance v0, Ldvv;

    invoke-direct {v0}, Ldvv;-><init>()V

    .line 754
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldvv;

    .line 755
    iget-object v1, v0, Ldvv;->b:Ldvn;

    invoke-static {v1}, Lbhw;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 756
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldvv;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 758
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbhw;

    invoke-direct {v1, v0}, Lbhw;-><init>(Ldvv;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Lbea;)V
    .locals 2

    .prologue
    .line 992
    invoke-super {p0, p1}, Lbfz;->a(Lbea;)V

    .line 993
    check-cast p1, Lbfb;

    .line 994
    iget-object v0, p1, Lbfb;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lbfb;->l:Ljava/lang/String;

    .line 995
    invoke-static {v0}, Lf;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "image/gif"

    iget-object v1, p1, Lbfb;->l:Ljava/lang/String;

    .line 996
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lbhw;->n:Z

    .line 997
    return-void

    .line 996
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lyt;Lbnl;)V
    .locals 23

    .prologue
    .line 768
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long v16, v2, v4

    .line 769
    invoke-super/range {p0 .. p2}, Lbfz;->a(Lyt;Lbnl;)V

    .line 770
    invoke-virtual/range {p0 .. p0}, Lbhw;->f()Ljava/lang/String;

    move-result-object v3

    .line 771
    move-object/from16 v0, p0

    iget-object v2, v0, Lbhw;->b:Lbea;

    check-cast v2, Lbfb;

    iget-object v4, v2, Lbfb;->i:Ljava/lang/String;

    .line 774
    invoke-static {v4}, Lyt;->d(Ljava/lang/String;)J

    move-result-wide v12

    .line 776
    sget-boolean v2, Lbfz;->a:Z

    if-eqz v2, :cond_1

    .line 777
    const-string v2, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "processSendChatMessageResponse Conversation id: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", messageId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lbhw;->g:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", messageTimestamp: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lbhw;->d:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", messageClientGeneratedId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mediaId count: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lbhw;->j:[Ljava/lang/String;

    array-length v6, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 782
    move-object/from16 v0, p0

    iget-object v5, v0, Lbhw;->j:[Ljava/lang/String;

    array-length v6, v5

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v6, :cond_0

    aget-object v7, v5, v2

    .line 783
    const-string v8, "Babel"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "  photoId "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 782
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 785
    :cond_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lbhw;->k:[Ljava/lang/String;

    array-length v6, v5

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v6, :cond_1

    aget-object v7, v5, v2

    .line 786
    const-string v8, "Babel"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "  albumId "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 785
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 790
    :cond_1
    sget-boolean v2, Lbfz;->a:Z

    if-eqz v2, :cond_2

    .line 791
    move-object/from16 v0, p0

    iget-object v2, v0, Lbhw;->b:Lbea;

    check-cast v2, Lbfb;

    .line 792
    invoke-virtual {v2}, Lbfb;->h()Ljava/lang/String;

    move-result-object v2

    .line 793
    if-eqz v2, :cond_2

    .line 794
    const-string v5, "Babel_Stress"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Stress message sent successful update message state:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 799
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lyt;->a()V

    .line 800
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    const-wide/16 v7, 0x3e8

    mul-long v18, v5, v7

    .line 802
    :try_start_0
    sget-boolean v2, Lbfz;->a:Z

    if-eqz v2, :cond_3

    .line 803
    const-string v2, "Babel"

    const-string v5, "modifying database"

    invoke-static {v2, v5}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 809
    :cond_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Lyt;->g(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    .line 810
    if-eqz v2, :cond_8

    move-object v10, v2

    .line 814
    :goto_2
    if-nez v10, :cond_9

    .line 815
    const-string v2, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Received SendChatMessageResponse for nonexistant  clientGeneratedId  = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " /  eventId = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 817
    invoke-virtual/range {p0 .. p0}, Lbhw;->h()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 815
    invoke-static {v2, v4}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 839
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lbhw;->m:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_4

    .line 841
    :try_start_1
    new-instance v2, Ldxe;

    invoke-direct {v2}, Ldxe;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lbhw;->m:[B

    .line 842
    invoke-static {v2, v4}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v2

    check-cast v2, Ldxe;

    iget-object v2, v2, Ldxe;->b:[Ldxc;

    .line 843
    move-object/from16 v0, p0

    iget-object v4, v0, Lbhw;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v5, v0, Lbhw;->d:J

    move-object/from16 v7, p1

    invoke-static/range {v2 .. v7}, Lbqs;->a([Ldxc;Ljava/lang/String;Ljava/lang/String;JLyt;)V
    :try_end_1
    .catch Lepq; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 858
    :cond_4
    :goto_4
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long v21, v4, v6

    .line 859
    invoke-virtual/range {p1 .. p1}, Lyt;->f()Lyj;

    move-result-object v4

    const/16 v7, 0xa

    const/16 v8, 0xcc

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    const/4 v15, 0x0

    move-wide/from16 v5, v16

    move-object v14, v3

    invoke-static/range {v4 .. v15}, Lf;->a(Lyj;JIILjava/lang/String;JJLjava/lang/String;Lyu;)V

    const/16 v7, 0xa

    const/16 v8, 0x67

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    const/4 v15, 0x0

    move-wide/from16 v5, v18

    move-object v14, v3

    invoke-static/range {v4 .. v15}, Lf;->a(Lyj;JIILjava/lang/String;JJLjava/lang/String;Lyu;)V

    const/16 v7, 0xd

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    const/4 v15, 0x0

    move-wide/from16 v5, v21

    move-object v14, v3

    invoke-static/range {v4 .. v15}, Lf;->a(Lyj;JIILjava/lang/String;JJLjava/lang/String;Lyu;)V

    .line 862
    const-string v2, "babel_transport_events"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 865
    const-wide/16 v8, -0x1

    move-object/from16 v0, p0

    iget-wide v10, v0, Lbhw;->o:J

    move-object/from16 v0, p0

    iget-object v14, v0, Lbhw;->g:Ljava/lang/String;

    const/4 v15, 0x2

    const/16 v16, 0x65

    .line 868
    invoke-virtual/range {p0 .. p0}, Lbhw;->c()J

    move-result-wide v17

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v7, p1

    .line 866
    invoke-virtual/range {v7 .. v20}, Lyt;->a(JJJLjava/lang/String;IIJLyu;I)J

    .line 870
    const-wide/16 v8, -0x1

    move-object/from16 v0, p0

    iget-wide v10, v0, Lbhw;->o:J

    move-object/from16 v0, p0

    iget-object v14, v0, Lbhw;->g:Ljava/lang/String;

    const/16 v15, 0xc

    const/16 v16, 0x65

    .line 872
    invoke-virtual/range {p0 .. p0}, Lbhw;->d()J

    move-result-wide v17

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v7, p1

    .line 870
    invoke-virtual/range {v7 .. v20}, Lyt;->a(JJJLjava/lang/String;IIJLyu;I)J

    .line 874
    const-wide/16 v8, -0x1

    move-object/from16 v0, p0

    iget-wide v10, v0, Lbhw;->o:J

    move-object/from16 v0, p0

    iget-object v14, v0, Lbhw;->g:Ljava/lang/String;

    const/16 v15, 0xd

    const/16 v16, 0x65

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v7, p1

    move-wide/from16 v17, v21

    invoke-virtual/range {v7 .. v20}, Lyt;->a(JJJLjava/lang/String;IIJLyu;I)J

    .line 881
    :cond_5
    invoke-virtual/range {p1 .. p1}, Lyt;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 883
    invoke-virtual/range {p1 .. p1}, Lyt;->c()V

    .line 884
    sget-boolean v2, Lbfz;->a:Z

    if-eqz v2, :cond_e

    .line 885
    invoke-virtual/range {p1 .. p1}, Lyt;->e()Lzr;

    move-result-object v2

    const-string v3, "messages"

    const/4 v4, 0x0

    const-string v5, "message_id=? OR message_id=?"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    .line 891
    invoke-virtual/range {p0 .. p0}, Lbhw;->g()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    .line 892
    invoke-virtual/range {p0 .. p0}, Lbhw;->h()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x0

    .line 885
    invoke-virtual/range {v2 .. v7}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 898
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "SendChatMessage.processResponse after endTransaction  cursor count is "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 899
    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 898
    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 900
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 902
    :cond_6
    invoke-interface {v4}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v5

    array-length v6, v5

    const/4 v2, 0x0

    move v3, v2

    :goto_5
    if-ge v3, v6, :cond_c

    aget-object v7, v5, v3

    .line 903
    invoke-interface {v4, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v4, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 904
    const-string v8, "text"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 905
    invoke-static {v2}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 907
    :cond_7
    const-string v8, "Babel"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, " ==> "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v8, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 902
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_5

    .line 812
    :cond_8
    :try_start_3
    invoke-virtual/range {p0 .. p0}, Lbhw;->h()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, Lyt;->g(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    move-object v10, v2

    goto/16 :goto_2

    .line 821
    :cond_9
    invoke-static {v10}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lbhw;->d:J

    .line 822
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-wide v8, v0, Lbhw;->h:J

    .line 823
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v8

    move-object/from16 v2, p1

    .line 819
    invoke-virtual/range {v2 .. v9}, Lyt;->a(Ljava/lang/String;JJJ)V

    .line 825
    move-object/from16 v0, p0

    iget-wide v4, v0, Lbhw;->d:J

    .line 827
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v4

    .line 828
    invoke-static {v10}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v6

    move-object/from16 v2, p1

    .line 825
    invoke-virtual/range {v2 .. v7}, Lyt;->a(Ljava/lang/String;JJ)V

    .line 830
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lyt;->a(Lbhw;)V

    .line 833
    move-object/from16 v0, p0

    iget-wide v4, v0, Lbhw;->d:J

    .line 835
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v4

    .line 833
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5}, Lyt;->i(Ljava/lang/String;J)V

    .line 836
    move-object/from16 v0, p1

    invoke-static {v0, v3}, Lyp;->d(Lyt;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_3

    .line 883
    :catchall_0
    move-exception v2

    move-object v8, v2

    invoke-virtual/range {p1 .. p1}, Lyt;->c()V

    .line 884
    sget-boolean v2, Lbfz;->a:Z

    if-eqz v2, :cond_11

    .line 885
    invoke-virtual/range {p1 .. p1}, Lyt;->e()Lzr;

    move-result-object v2

    const-string v3, "messages"

    const/4 v4, 0x0

    const-string v5, "message_id=? OR message_id=?"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    .line 891
    invoke-virtual/range {p0 .. p0}, Lbhw;->g()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v7

    const/4 v7, 0x1

    .line 892
    invoke-virtual/range {p0 .. p0}, Lbhw;->h()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v7

    const/4 v7, 0x0

    .line 885
    invoke-virtual/range {v2 .. v7}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 898
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "SendChatMessage.processResponse after endTransaction  cursor count is "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 899
    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 898
    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 900
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 902
    :cond_a
    invoke-interface {v4}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v5

    array-length v6, v5

    const/4 v2, 0x0

    move v3, v2

    :goto_6
    if-ge v3, v6, :cond_f

    aget-object v7, v5, v3

    .line 903
    invoke-interface {v4, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v4, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 904
    const-string v9, "text"

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_b

    .line 905
    invoke-static {v2}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 907
    :cond_b
    const-string v9, "Babel"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, " ==> "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v9, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 902
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_6

    .line 846
    :catch_0
    move-exception v2

    :try_start_4
    const-string v2, "Babel"

    const-string v4, "Invalid ClientSuggestions protobuf parsed from ClientSendChatMessageResponse. This happening likely means a corrupt response proto has been recieved."

    invoke-static {v2, v4}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_4

    .line 909
    :cond_c
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_6

    .line 911
    :cond_d
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 915
    :cond_e
    invoke-virtual/range {p1 .. p1}, Lyt;->f()Lyj;

    move-result-object v2

    const-wide/16 v3, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    invoke-static {v2, v3, v4, v5, v6}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;JIZ)V

    .line 917
    return-void

    .line 909
    :cond_f
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_a

    .line 911
    :cond_10
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 912
    :cond_11
    throw v8
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 955
    iget-object v0, p0, Lbhw;->b:Lbea;

    check-cast v0, Lbfb;

    iget-object v0, v0, Lbfb;->c:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 959
    iget-object v0, p0, Lbhw;->b:Lbea;

    check-cast v0, Lbfb;

    iget-object v0, v0, Lbfb;->i:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 963
    iget-object v0, p0, Lbhw;->g:Ljava/lang/String;

    return-object v0
.end method

.method public i()J
    .locals 2

    .prologue
    .line 967
    iget-wide v0, p0, Lbhw;->h:J

    return-wide v0
.end method

.method public j()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 971
    iget-object v0, p0, Lbhw;->j:[Ljava/lang/String;

    return-object v0
.end method

.method public k()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 975
    iget-object v0, p0, Lbhw;->k:[Ljava/lang/String;

    return-object v0
.end method

.method public l()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 979
    iget-object v0, p0, Lbhw;->l:[Ljava/lang/String;

    return-object v0
.end method

.method public m()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 983
    iget-object v0, p0, Lbhw;->i:[Ljava/lang/String;

    return-object v0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 987
    iget-boolean v0, p0, Lbhw;->n:Z

    return v0
.end method
