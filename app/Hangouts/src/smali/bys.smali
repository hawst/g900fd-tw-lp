.class public final Lbys;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Lcyp;

.field public static final b:Lcyp;

.field public static final c:Lcyp;

.field public static final d:Lcyp;

.field public static final e:Lcyp;

.field public static final f:Lcyp;

.field public static final g:Lcyp;

.field public static final h:Lcyp;

.field public static final i:Lcyp;

.field public static final j:Lcyp;

.field public static final k:Lcyp;

.field public static final l:Lcyp;

.field public static final m:Lcyp;

.field public static final n:Lcyp;

.field public static final o:Lcyp;

.field public static final p:Lcyp;

.field public static final q:Lcyp;

.field public static final r:Lcyp;

.field public static final s:Lcyp;

.field public static final t:Lcyp;

.field public static final u:Lcyo;

.field public static v:Lcxh;

.field private static volatile w:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 31
    const-string v0, "audioplayer"

    invoke-static {v0}, Lbys;->d(Ljava/lang/String;)Lcyp;

    move-result-object v0

    sput-object v0, Lbys;->a:Lcyp;

    .line 32
    const-string v0, "clearcut"

    invoke-static {v0}, Lbys;->d(Ljava/lang/String;)Lcyp;

    move-result-object v0

    sput-object v0, Lbys;->b:Lcyp;

    .line 33
    const-string v0, "content"

    invoke-static {v0}, Lbys;->d(Ljava/lang/String;)Lcyp;

    move-result-object v0

    sput-object v0, Lbys;->c:Lcyp;

    .line 34
    const-string v0, "fragment"

    invoke-static {v0}, Lbys;->d(Ljava/lang/String;)Lcyp;

    move-result-object v0

    sput-object v0, Lbys;->d:Lcyp;

    .line 35
    const-string v0, "hangout"

    invoke-static {v0}, Lbys;->d(Ljava/lang/String;)Lcyp;

    move-result-object v0

    sput-object v0, Lbys;->e:Lcyp;

    .line 36
    const-string v0, "network"

    invoke-static {v0}, Lbys;->d(Ljava/lang/String;)Lcyp;

    move-result-object v0

    sput-object v0, Lbys;->f:Lcyp;

    .line 37
    const-string v0, "phone"

    invoke-static {v0}, Lbys;->d(Ljava/lang/String;)Lcyp;

    move-result-object v0

    sput-object v0, Lbys;->g:Lcyp;

    .line 38
    const-string v0, "protocol"

    invoke-static {v0}, Lbys;->d(Ljava/lang/String;)Lcyp;

    move-result-object v0

    sput-object v0, Lbys;->h:Lcyp;

    .line 39
    const-string v0, "pstn_meta"

    invoke-static {v0}, Lbys;->d(Ljava/lang/String;)Lcyp;

    move-result-object v0

    sput-object v0, Lbys;->i:Lcyp;

    .line 40
    const-string v0, "quark"

    invoke-static {v0}, Lbys;->d(Ljava/lang/String;)Lcyp;

    move-result-object v0

    sput-object v0, Lbys;->j:Lcyp;

    .line 41
    const-string v0, "realtimechat"

    invoke-static {v0}, Lbys;->d(Ljava/lang/String;)Lcyp;

    move-result-object v0

    sput-object v0, Lbys;->k:Lcyp;

    .line 42
    const-string v0, "service"

    invoke-static {v0}, Lbys;->d(Ljava/lang/String;)Lcyp;

    move-result-object v0

    sput-object v0, Lbys;->l:Lcyp;

    .line 43
    const-string v0, "setting"

    invoke-static {v0}, Lbys;->d(Ljava/lang/String;)Lcyp;

    move-result-object v0

    sput-object v0, Lbys;->m:Lcyp;

    .line 44
    const-string v0, "sms"

    invoke-static {v0}, Lbys;->d(Ljava/lang/String;)Lcyp;

    move-result-object v0

    sput-object v0, Lbys;->n:Lcyp;

    .line 45
    const-string v0, "talk"

    invoke-static {v0}, Lbys;->d(Ljava/lang/String;)Lcyp;

    move-result-object v0

    sput-object v0, Lbys;->o:Lcyp;

    .line 46
    const-string v0, "telephony"

    invoke-static {v0}, Lbys;->d(Ljava/lang/String;)Lcyp;

    move-result-object v0

    sput-object v0, Lbys;->p:Lcyp;

    .line 47
    const-string v0, "uploader"

    invoke-static {v0}, Lbys;->d(Ljava/lang/String;)Lcyp;

    move-result-object v0

    sput-object v0, Lbys;->q:Lcyp;

    .line 48
    const-string v0, "util"

    invoke-static {v0}, Lbys;->d(Ljava/lang/String;)Lcyp;

    move-result-object v0

    sput-object v0, Lbys;->r:Lcyp;

    .line 49
    const-string v0, "view"

    invoke-static {v0}, Lbys;->d(Ljava/lang/String;)Lcyp;

    move-result-object v0

    sput-object v0, Lbys;->s:Lcyp;

    .line 50
    const-string v0, "widget"

    invoke-static {v0}, Lbys;->d(Ljava/lang/String;)Lcyp;

    move-result-object v0

    sput-object v0, Lbys;->t:Lcyp;

    .line 51
    new-instance v0, Lcyo;

    const-string v1, "pii"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcyo;-><init>(Ljava/lang/String;B)V

    sput-object v0, Lbys;->u:Lcyo;

    return-void
.end method

.method public static a()V
    .locals 3

    .prologue
    .line 82
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "babel_log_dump"

    const/4 v2, 0x0

    .line 81
    invoke-static {v0, v1, v2}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    .line 85
    sput-boolean v0, Lbys;->w:Z

    if-eqz v0, :cond_1

    sget-object v0, Lbys;->v:Lcxh;

    if-nez v0, :cond_1

    .line 87
    new-instance v0, Lcxh;

    invoke-direct {v0}, Lcxh;-><init>()V

    sput-object v0, Lbys;->v:Lcxh;

    .line 92
    :cond_0
    :goto_0
    sget-object v0, Lbys;->v:Lcxh;

    invoke-static {v0}, Lcxc;->a(Lcxh;)I

    .line 93
    return-void

    .line 88
    :cond_1
    sget-boolean v0, Lbys;->w:Z

    if-nez v0, :cond_0

    sget-object v0, Lbys;->v:Lcxh;

    if-eqz v0, :cond_0

    .line 90
    const/4 v0, 0x0

    sput-object v0, Lbys;->v:Lcxh;

    goto :goto_0
.end method

.method public static a(Ljava/io/PrintWriter;)V
    .locals 1

    .prologue
    .line 333
    sget-object v0, Lbys;->v:Lcxh;

    .line 334
    if-eqz v0, :cond_0

    .line 335
    invoke-virtual {v0, p0}, Lcxh;->a(Ljava/io/PrintWriter;)V

    .line 337
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 107
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-static {v0, v1, p0, p1}, Lbys;->a(ZILjava/lang/String;Ljava/lang/String;)V

    .line 108
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 120
    const/4 v0, 0x1

    const/4 v1, 0x2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 121
    invoke-static {p2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 120
    invoke-static {v0, v1, p0, v2}, Lbys;->a(ZILjava/lang/String;Ljava/lang/String;)V

    .line 122
    return-void
.end method

.method private static a(ZILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 299
    if-nez p0, :cond_0

    const/4 v0, 0x4

    if-ge p1, v0, :cond_0

    invoke-static {p2, p1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 300
    :cond_0
    invoke-static {p1, p2, p3}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I

    .line 303
    :cond_1
    sget-object v0, Lbys;->v:Lcxh;

    .line 304
    if-eqz v0, :cond_2

    const/4 v1, 0x3

    if-lt p1, v1, :cond_2

    .line 305
    invoke-virtual {v0, p1, p2, p3}, Lcxh;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 307
    :cond_2
    return-void
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 340
    invoke-static {p0}, Lcxc;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static a(Ljava/lang/String;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 319
    const/4 v1, 0x3

    if-ne p1, v1, :cond_2

    .line 320
    sget-object v1, Lbys;->v:Lcxh;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 325
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 326
    invoke-static {p0, p1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    .line 329
    :cond_1
    return v0

    .line 321
    :cond_2
    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 350
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lbys;->u:Lcyo;

    .line 353
    if-nez p0, :cond_1

    const/4 v0, 0x0

    .line 354
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Redacted-"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    :cond_0
    return-object p0

    .line 353
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 146
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-static {v0, v1, p0, p1}, Lbys;->a(ZILjava/lang/String;Ljava/lang/String;)V

    .line 147
    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 181
    const/4 v0, 0x0

    const/4 v1, 0x3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 182
    invoke-static {p2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 181
    invoke-static {v0, v1, p0, v2}, Lbys;->a(ZILjava/lang/String;Ljava/lang/String;)V

    .line 183
    return-void
.end method

.method public static b()Z
    .locals 1

    .prologue
    .line 96
    sget-boolean v0, Lbys;->w:Z

    return v0
.end method

.method public static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 359
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Babel."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 157
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-static {v0, v1, p0, p1}, Lbys;->a(ZILjava/lang/String;Ljava/lang/String;)V

    .line 158
    return-void
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    const/4 v2, 0x5

    const/4 v1, 0x1

    .line 229
    invoke-static {v1, v2, p0, p1}, Lbys;->a(ZILjava/lang/String;Ljava/lang/String;)V

    .line 230
    invoke-static {p2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, p0, v0}, Lbys;->a(ZILjava/lang/String;Ljava/lang/String;)V

    .line 231
    return-void
.end method

.method private static d(Ljava/lang/String;)Lcyp;
    .locals 3

    .prologue
    .line 74
    new-instance v0, Lcyp;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "debug.chat."

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcyp;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 169
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-static {v0, v1, p0, p1}, Lbys;->a(ZILjava/lang/String;Ljava/lang/String;)V

    .line 170
    return-void
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    const/4 v2, 0x6

    const/4 v1, 0x1

    .line 253
    invoke-static {v1, v2, p0, p1}, Lbys;->a(ZILjava/lang/String;Ljava/lang/String;)V

    .line 254
    invoke-static {p2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, p0, v0}, Lbys;->a(ZILjava/lang/String;Ljava/lang/String;)V

    .line 255
    return-void
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 193
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-static {v0, v1, p0, p1}, Lbys;->a(ZILjava/lang/String;Ljava/lang/String;)V

    .line 194
    return-void
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 284
    const/4 v0, 0x1

    const/4 v1, 0x7

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "wtf\n"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 285
    invoke-static {p2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 284
    invoke-static {v0, v1, p0, v2}, Lbys;->a(ZILjava/lang/String;Ljava/lang/String;)V

    .line 286
    invoke-static {p0, p1, p2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 287
    return-void
.end method

.method public static f(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 217
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-static {v0, v1, p0, p1}, Lbys;->a(ZILjava/lang/String;Ljava/lang/String;)V

    .line 218
    return-void
.end method

.method public static g(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 241
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-static {v0, v1, p0, p1}, Lbys;->a(ZILjava/lang/String;Ljava/lang/String;)V

    .line 242
    return-void
.end method

.method public static h(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 268
    const/4 v0, 0x1

    const/4 v1, 0x7

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "wtf\n"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, p0, v2}, Lbys;->a(ZILjava/lang/String;Ljava/lang/String;)V

    .line 269
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    invoke-static {p0, p1, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 270
    return-void
.end method
