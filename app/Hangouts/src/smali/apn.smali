.class final Lapn;
.super Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;
.source "PG"


# instance fields
.field final synthetic a:Lapk;


# direct methods
.method private constructor <init>(Lapk;)V
    .locals 0

    .prologue
    .line 411
    iput-object p1, p0, Lapn;->a:Lapk;

    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lapk;B)V
    .locals 0

    .prologue
    .line 411
    invoke-direct {p0, p1}, Lapn;-><init>(Lapk;)V

    return-void
.end method


# virtual methods
.method public getListeners()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/hangouts/video/CallStateListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 418
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 419
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapo;

    .line 420
    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 422
    :cond_0
    return-object v1
.end method

.method public onAuthUserActionRequired(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 449
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    if-eqz v0, :cond_0

    .line 450
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    invoke-virtual {v0}, Lapx;->c()Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->sendCallCompleteIntent()V

    .line 451
    iget-object v0, p0, Lapn;->a:Lapk;

    invoke-virtual {v0}, Lapk;->j()V

    .line 452
    iget-object v0, p0, Lapn;->a:Lapk;

    const/4 v1, 0x0

    iput-object v1, v0, Lapk;->c:Lapx;

    .line 455
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;->onAuthUserActionRequired(Landroid/content/Intent;)V

    .line 456
    return-void
.end method

.method public onBoundToService()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 428
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getCurrentCall()Lcom/google/android/libraries/hangouts/video/CallState;

    move-result-object v0

    .line 429
    if-nez v0, :cond_1

    .line 430
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getRecentPreviousCall()Lcom/google/android/libraries/hangouts/video/CallState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lapn;->onCallEnded(Lcom/google/android/libraries/hangouts/video/CallState;)V

    .line 445
    :cond_0
    return-void

    .line 433
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->isMediaConnected()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 434
    invoke-virtual {p0, v0}, Lapn;->onMediaStarted(Lcom/google/android/libraries/hangouts/video/CallState;)V

    .line 436
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->getSelf()Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 437
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->getSelf()Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    move-result-object v1

    new-instance v2, Lcom/google/android/libraries/hangouts/video/endpoint/EnterEvent;

    invoke-direct {v2, v3}, Lcom/google/android/libraries/hangouts/video/endpoint/EnterEvent;-><init>(Z)V

    invoke-virtual {p0, v1, v2}, Lapn;->onEndpointEvent(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/EndpointEvent;)V

    .line 439
    :cond_3
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->getPresenter()Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 440
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->getPresenter()Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->getPresentAgent()Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lapn;->onPresenterUpdate(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V

    .line 442
    :cond_4
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->getRemoteEndpoints()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 443
    new-instance v2, Lcom/google/android/libraries/hangouts/video/endpoint/EnterEvent;

    invoke-direct {v2, v3}, Lcom/google/android/libraries/hangouts/video/endpoint/EnterEvent;-><init>(Z)V

    invoke-virtual {p0, v0, v2}, Lapn;->onEndpointEvent(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/EndpointEvent;)V

    goto :goto_0
.end method

.method public onBroadcastProducerChange(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 696
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    if-eqz v0, :cond_0

    .line 697
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    invoke-static {}, Lapx;->G()V

    .line 699
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;->onBroadcastProducerChange(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    return-void
.end method

.method public onBroadcastSessionStateChange(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 705
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    if-eqz v0, :cond_0

    .line 706
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    invoke-virtual {v0, p2, p3}, Lapx;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 708
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;->onBroadcastSessionStateChange(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 709
    return-void
.end method

.method public onBroadcastStreamStateChange(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 679
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    if-eqz v0, :cond_0

    .line 680
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    invoke-virtual {v0, p2}, Lapx;->f(Ljava/lang/String;)V

    .line 682
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;->onBroadcastStreamStateChange(Ljava/lang/String;Ljava/lang/String;)V

    .line 683
    return-void
.end method

.method public onBroadcastStreamViewerCountChange(I)V
    .locals 1

    .prologue
    .line 687
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    if-eqz v0, :cond_0

    .line 688
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    invoke-virtual {v0, p1}, Lapx;->a(I)V

    .line 690
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;->onBroadcastStreamViewerCountChange(I)V

    .line 691
    return-void
.end method

.method public onCallEnded(Lcom/google/android/libraries/hangouts/video/CallState;)V
    .locals 7

    .prologue
    .line 511
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    if-nez v0, :cond_0

    .line 512
    invoke-super {p0, p1}, Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;->onCallEnded(Lcom/google/android/libraries/hangouts/video/CallState;)V

    .line 560
    :goto_0
    return-void

    .line 516
    :cond_0
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    .line 517
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/CallState;->getEndCause()I

    move-result v0

    .line 518
    iget-object v2, p0, Lapn;->a:Lapk;

    iget-object v2, v2, Lapk;->c:Lapx;

    invoke-virtual {v2}, Lapx;->c()Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3}, Lcom/google/android/libraries/hangouts/video/ExitHistory;->recordExit(Landroid/content/Context;Lcom/google/android/libraries/hangouts/video/HangoutRequest;IZ)V

    .line 519
    iget-object v2, p0, Lapn;->a:Lapk;

    .line 520
    iget-object v2, v2, Lapk;->c:Lapx;

    invoke-virtual {v2}, Lapx;->c()Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getAccountName()Ljava/lang/String;

    move-result-object v2

    .line 519
    invoke-static {v2}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v6

    .line 521
    const/16 v2, 0x13

    if-eq v0, v2, :cond_3

    const/16 v2, 0x19

    if-eq v0, v2, :cond_3

    .line 528
    invoke-static {}, Lbci;->a()Lbci;

    move-result-object v0

    iget-object v2, v0, Lbci;->a:Ljava/lang/String;

    iget-object v0, p0, Lapn;->a:Lapk;

    .line 529
    iget-object v0, v0, Lapk;->c:Lapx;

    invoke-virtual {v0}, Lapx;->d()I

    move-result v3

    iget-object v0, p0, Lapn;->a:Lapk;

    .line 530
    iget-object v0, v0, Lapk;->c:Lapx;

    invoke-virtual {v0}, Lapx;->e()I

    move-result v4

    .line 531
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getLocalState()Lcom/google/android/libraries/hangouts/video/LocalState;

    move-result-object v5

    move-object v0, p1

    .line 526
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/libraries/hangouts/video/CallState;->getLogData(Landroid/content/Context;Ljava/lang/String;IILcom/google/android/libraries/hangouts/video/LocalState;)Ldzj;

    move-result-object v0

    .line 532
    if-eqz v0, :cond_1

    .line 533
    iget-object v1, p0, Lapn;->a:Lapk;

    iget-object v1, v1, Lapk;->c:Lapx;

    invoke-virtual {v1}, Lapx;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ljava/lang/String;Ldzj;)I

    .line 538
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/CallState;->wasMediaInitiated()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 539
    invoke-static {v6}, Lf;->c(Lyj;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 542
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "babel_hangout_upload_end_causes"

    const-string v2, "6,10,29,47"

    .line 541
    invoke-static {v0, v1, v2}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    .line 545
    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 546
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/CallState;->getMappedEndCause()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 547
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 549
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lapx;->g(Ljava/lang/String;)V

    .line 553
    :cond_2
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    invoke-virtual {v0}, Lapx;->Q()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 554
    iget-object v0, p0, Lapn;->a:Lapk;

    .line 555
    iget-object v0, v0, Lapk;->c:Lapx;

    invoke-virtual {v0}, Lapx;->R()Ljava/lang/String;

    move-result-object v0

    .line 554
    invoke-static {v6, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->q(Lyj;Ljava/lang/String;)I

    .line 558
    :cond_3
    invoke-super {p0, p1}, Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;->onCallEnded(Lcom/google/android/libraries/hangouts/video/CallState;)V

    .line 559
    iget-object v0, p0, Lapn;->a:Lapk;

    invoke-virtual {v0, p1}, Lapk;->a(Lcom/google/android/libraries/hangouts/video/CallState;)V

    goto/16 :goto_0
.end method

.method public onCommonNotificationReceived(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 659
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    if-eqz v0, :cond_0

    .line 660
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    invoke-virtual {v0, p1, p2, p4}, Lapx;->a(Ljava/lang/String;ILjava/lang/String;)V

    .line 664
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;->onCommonNotificationReceived(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 666
    return-void
.end method

.method public onCommonNotificationRetracted(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 670
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    if-eqz v0, :cond_0

    .line 671
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    invoke-virtual {v0, p1}, Lapx;->e(Ljava/lang/String;)V

    .line 674
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;->onCommonNotificationRetracted(Ljava/lang/String;)V

    .line 675
    return-void
.end method

.method public onConversationIdChanged(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 648
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    if-eqz v0, :cond_0

    .line 649
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    invoke-virtual {v0, p1}, Lapx;->a(Ljava/lang/String;)V

    .line 650
    iget-object v0, p0, Lapn;->a:Lapk;

    invoke-virtual {v0}, Lapk;->j()V

    .line 653
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;->onConversationIdChanged(Ljava/lang/String;)V

    .line 654
    return-void
.end method

.method public onEndpointEvent(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/EndpointEvent;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 564
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    if-nez v0, :cond_1

    .line 565
    invoke-super {p0, p1, p2}, Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;->onEndpointEvent(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/EndpointEvent;)V

    .line 614
    :cond_0
    return-void

    .line 571
    :cond_1
    instance-of v0, p2, Lcom/google/android/libraries/hangouts/video/endpoint/EnterEvent;

    if-eqz v0, :cond_3

    move-object v0, p2

    .line 572
    check-cast v0, Lcom/google/android/libraries/hangouts/video/endpoint/EnterEvent;

    .line 573
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isSelfEndpoint()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 574
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v3, v0, Lapk;->c:Lapx;

    move-object v0, p1

    check-cast v0, Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    invoke-virtual {v3, v0}, Lapx;->a(Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;)V

    move v0, v2

    .line 602
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;->onEndpointEvent(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/EndpointEvent;)V

    .line 604
    iget-object v3, p0, Lapn;->a:Lapk;

    iget-object v3, v3, Lapk;->c:Lapx;

    invoke-virtual {v3}, Lapx;->D()I

    move-result v3

    .line 605
    if-eqz v0, :cond_a

    if-ne v3, v1, :cond_a

    .line 606
    iget-object v0, p0, Lapn;->a:Lapk;

    invoke-virtual {v0}, Lapk;->i()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapo;

    .line 607
    invoke-virtual {v0}, Lapo;->b()V

    goto :goto_1

    .line 575
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isConnecting()Z

    move-result v3

    if-nez v3, :cond_b

    .line 577
    iget-object v3, p0, Lapn;->a:Lapk;

    iget-object v3, v3, Lapk;->c:Lapx;

    .line 578
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/endpoint/EnterEvent;->mayBePreExistingEndpoint()Z

    move-result v0

    .line 577
    invoke-virtual {v3, p1, v0}, Lapx;->a(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Z)V

    move v0, v1

    goto :goto_0

    .line 580
    :cond_3
    instance-of v0, p2, Lcom/google/android/libraries/hangouts/video/endpoint/MediaSourceEvent;

    if-eqz v0, :cond_6

    move-object v0, p2

    .line 581
    check-cast v0, Lcom/google/android/libraries/hangouts/video/endpoint/MediaSourceEvent;

    .line 582
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isSelfEndpoint()Z

    move-result v3

    if-nez v3, :cond_5

    iget v3, v0, Lcom/google/android/libraries/hangouts/video/endpoint/MediaSourceEvent;->type:I

    const/4 v4, 0x2

    if-eq v3, v4, :cond_4

    iget v3, v0, Lcom/google/android/libraries/hangouts/video/endpoint/MediaSourceEvent;->type:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_5

    .line 585
    :cond_4
    iget-object v3, p0, Lapn;->a:Lapk;

    iget-object v3, v3, Lapk;->c:Lapx;

    invoke-virtual {v3, p1, v0}, Lapx;->a(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/MediaSourceEvent;)V

    :cond_5
    move v0, v2

    .line 587
    goto :goto_0

    :cond_6
    instance-of v0, p2, Lcom/google/android/libraries/hangouts/video/endpoint/ConnectionStatusChangedEvent;

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isSelfEndpoint()Z

    move-result v0

    if-nez v0, :cond_8

    .line 588
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 590
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    invoke-virtual {v0, p1, v1}, Lapx;->a(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Z)V

    move v0, v1

    goto :goto_0

    .line 591
    :cond_7
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isConnectingWithMedia()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 594
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    invoke-virtual {v0, p1}, Lapx;->b(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V

    move v0, v2

    goto/16 :goto_0

    .line 596
    :cond_8
    instance-of v0, p2, Lcom/google/android/libraries/hangouts/video/endpoint/ExitEvent;

    if-eqz v0, :cond_b

    move-object v0, p2

    .line 597
    check-cast v0, Lcom/google/android/libraries/hangouts/video/endpoint/ExitEvent;

    .line 598
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isConnecting()Z

    move-result v3

    if-nez v3, :cond_9

    move v3, v1

    .line 599
    :goto_2
    iget-object v4, p0, Lapn;->a:Lapk;

    iget-object v4, v4, Lapk;->c:Lapx;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/endpoint/ExitEvent;->getExitEventErrorCode()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, p1, v0}, Lapx;->a(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Ljava/lang/Integer;)V

    move v0, v2

    move v2, v3

    goto/16 :goto_0

    :cond_9
    move v3, v2

    .line 598
    goto :goto_2

    .line 609
    :cond_a
    if-eqz v2, :cond_0

    if-nez v3, :cond_0

    .line 610
    iget-object v0, p0, Lapn;->a:Lapk;

    invoke-virtual {v0}, Lapk;->i()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapo;

    .line 611
    invoke-virtual {v0}, Lapo;->e()V

    goto :goto_3

    :cond_b
    move v0, v2

    goto/16 :goto_0
.end method

.method public onHangoutIdResolved(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;ZZ)V
    .locals 1

    .prologue
    .line 463
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    if-eqz v0, :cond_2

    .line 464
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    invoke-virtual {v0}, Lapx;->p()V

    .line 465
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    invoke-virtual {v0}, Lapx;->c()Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getHangoutId()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 466
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    invoke-virtual {v0, p1, p2}, Lapx;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    :cond_0
    if-eqz p3, :cond_1

    .line 469
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    invoke-virtual {v0, p3}, Lapx;->a(Ljava/lang/String;)V

    .line 471
    :cond_1
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    invoke-virtual {v0, p4}, Lapx;->a(Z)V

    .line 472
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    invoke-virtual {v0, p5}, Lapx;->b(Z)V

    .line 473
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    invoke-virtual {v0, p6}, Lapx;->c(Ljava/lang/String;)V

    .line 474
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    invoke-virtual {v0, p7}, Lapx;->c(Z)V

    .line 475
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    invoke-virtual {v0, p8}, Lapx;->d(Z)V

    .line 477
    :cond_2
    invoke-super/range {p0 .. p8}, Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;->onHangoutIdResolved(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;ZZ)V

    .line 479
    return-void
.end method

.method public onLoudestSpeakerUpdate(Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 624
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    if-eqz v0, :cond_0

    .line 625
    if-nez p2, :cond_1

    .line 626
    const-string v0, "Babel"

    const-string v1, "No one is talking at the moment"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 627
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lapx;->c(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V

    .line 643
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;->onLoudestSpeakerUpdate(Ljava/lang/String;I)V

    .line 644
    return-void

    .line 629
    :cond_1
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Loudest speaker is now "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 633
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getCurrentCall()Lcom/google/android/libraries/hangouts/video/CallState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->getRemoteEndpoints()Ljava/util/Collection;

    move-result-object v0

    .line 634
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 635
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getAudioSsrcs()Ljava/util/List;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 636
    iget-object v1, p0, Lapn;->a:Lapk;

    iget-object v1, v1, Lapk;->c:Lapx;

    invoke-virtual {v1, v0}, Lapx;->c(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V

    goto :goto_0
.end method

.method public onMediaStarted(Lcom/google/android/libraries/hangouts/video/CallState;)V
    .locals 3

    .prologue
    .line 483
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Got state "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/CallState;->getMediaState()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 484
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/CallState;->getRemoteFullJid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 483
    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    if-eqz v0, :cond_0

    .line 487
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    invoke-virtual {v0}, Lapx;->u()V

    .line 492
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    invoke-virtual {v0}, Lapx;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 493
    iget-object v0, p0, Lapn;->a:Lapk;

    invoke-static {v0}, Lapk;->a(Lapk;)V

    .line 506
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;->onMediaStarted(Lcom/google/android/libraries/hangouts/video/CallState;)V

    .line 507
    return-void

    .line 494
    :cond_1
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    invoke-virtual {v0}, Lapx;->C()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lapn;->a:Lapk;

    .line 495
    iget-object v0, v0, Lapk;->c:Lapx;

    invoke-virtual {v0}, Lapx;->D()I

    move-result v0

    if-nez v0, :cond_0

    .line 502
    iget-object v0, p0, Lapn;->a:Lapk;

    iget-object v0, v0, Lapk;->c:Lapx;

    const/16 v1, 0x3f4

    invoke-virtual {v0, v1}, Lapx;->c(I)V

    goto :goto_0
.end method
