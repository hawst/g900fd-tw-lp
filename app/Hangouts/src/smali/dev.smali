.class public final Ldev;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldev;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ldep;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2042
    const/4 v0, 0x0

    new-array v0, v0, [Ldev;

    sput-object v0, Ldev;->a:[Ldev;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2043
    invoke-direct {p0}, Lepn;-><init>()V

    .line 2050
    const/4 v0, 0x0

    iput-object v0, p0, Ldev;->d:Ldep;

    .line 2043
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 2080
    const/4 v0, 0x0

    .line 2081
    iget-object v1, p0, Ldev;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2082
    const/4 v0, 0x1

    iget-object v1, p0, Ldev;->b:Ljava/lang/String;

    .line 2083
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2085
    :cond_0
    iget-object v1, p0, Ldev;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 2086
    const/4 v1, 0x2

    iget-object v2, p0, Ldev;->c:Ljava/lang/String;

    .line 2087
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2089
    :cond_1
    iget-object v1, p0, Ldev;->d:Ldep;

    if-eqz v1, :cond_2

    .line 2090
    const/4 v1, 0x3

    iget-object v2, p0, Ldev;->d:Ldep;

    .line 2091
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2093
    :cond_2
    iget-object v1, p0, Ldev;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 2094
    const/4 v1, 0x4

    iget-object v2, p0, Ldev;->e:Ljava/lang/String;

    .line 2095
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2097
    :cond_3
    iget-object v1, p0, Ldev;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 2098
    const/4 v1, 0x5

    iget-object v2, p0, Ldev;->f:Ljava/lang/String;

    .line 2099
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2101
    :cond_4
    iget-object v1, p0, Ldev;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2102
    iput v0, p0, Ldev;->cachedSize:I

    .line 2103
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 2039
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldev;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldev;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldev;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldev;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldev;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldev;->d:Ldep;

    if-nez v0, :cond_2

    new-instance v0, Ldep;

    invoke-direct {v0}, Ldep;-><init>()V

    iput-object v0, p0, Ldev;->d:Ldep;

    :cond_2
    iget-object v0, p0, Ldev;->d:Ldep;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldev;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldev;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 2059
    iget-object v0, p0, Ldev;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2060
    const/4 v0, 0x1

    iget-object v1, p0, Ldev;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 2062
    :cond_0
    iget-object v0, p0, Ldev;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2063
    const/4 v0, 0x2

    iget-object v1, p0, Ldev;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 2065
    :cond_1
    iget-object v0, p0, Ldev;->d:Ldep;

    if-eqz v0, :cond_2

    .line 2066
    const/4 v0, 0x3

    iget-object v1, p0, Ldev;->d:Ldep;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 2068
    :cond_2
    iget-object v0, p0, Ldev;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 2069
    const/4 v0, 0x4

    iget-object v1, p0, Ldev;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 2071
    :cond_3
    iget-object v0, p0, Ldev;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 2072
    const/4 v0, 0x5

    iget-object v1, p0, Ldev;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 2074
    :cond_4
    iget-object v0, p0, Ldev;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 2076
    return-void
.end method
