.class public final Lbvx;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static a:Ljava/lang/String;

.field public static final b:Landroid/net/Uri;

.field private static final c:Z

.field private static d:[Ljava/lang/String;

.field private static final e:Landroid/net/Uri;

.field private static final f:Landroid/net/Uri;

.field private static final g:Landroid/net/Uri;

.field private static final h:Landroid/net/Uri;

.field private static final i:Landroid/net/Uri;

.field private static final j:Landroid/net/Uri;

.field private static final k:Landroid/net/Uri;

.field private static final l:Landroid/net/Uri;

.field private static final m:[Ljava/lang/String;

.field private static final n:Landroid/net/Uri;

.field private static final o:[Ljava/lang/String;

.field private static p:Ljava/lang/Boolean;

.field private static q:Lbdh;

.field private static final r:[Ljava/lang/String;

.field private static s:Ljava/lang/Boolean;

.field private static volatile t:Ljava/lang/Boolean;

.field private static volatile u:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/16 v2, 0x13

    .line 90
    sget-object v0, Lbys;->n:Lcyp;

    sput-boolean v4, Lbvx;->c:Z

    .line 200
    new-instance v0, Ljava/lang/String;

    const/16 v1, 0x7c

    .line 201
    invoke-static {v1}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    sput-object v0, Lbvx;->a:Ljava/lang/String;

    .line 206
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_0

    .line 207
    sget-object v0, Landroid/provider/Telephony$Mms;->CONTENT_URI:Landroid/net/Uri;

    :goto_0
    sput-object v0, Lbvx;->e:Landroid/net/Uri;

    .line 209
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_1

    .line 210
    sget-object v0, Landroid/provider/Telephony$Mms$Inbox;->CONTENT_URI:Landroid/net/Uri;

    :goto_1
    sput-object v0, Lbvx;->f:Landroid/net/Uri;

    .line 212
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_2

    .line 213
    sget-object v0, Landroid/provider/Telephony$Mms$Sent;->CONTENT_URI:Landroid/net/Uri;

    :goto_2
    sput-object v0, Lbvx;->g:Landroid/net/Uri;

    .line 215
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_3

    .line 216
    sget-object v0, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    :goto_3
    sput-object v0, Lbvx;->h:Landroid/net/Uri;

    .line 218
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_4

    .line 219
    sget-object v0, Landroid/provider/Telephony$Sms$Sent;->CONTENT_URI:Landroid/net/Uri;

    :goto_4
    sput-object v0, Lbvx;->i:Landroid/net/Uri;

    .line 221
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_5

    .line 222
    sget-object v0, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    :goto_5
    sput-object v0, Lbvx;->j:Landroid/net/Uri;

    .line 224
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_6

    .line 225
    sget-object v0, Landroid/provider/Telephony$Threads;->CONTENT_URI:Landroid/net/Uri;

    .line 837
    :goto_6
    sput-object v0, Lbvx;->k:Landroid/net/Uri;

    .line 838
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "simple"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lbvx;->l:Landroid/net/Uri;

    .line 840
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "recipient_ids"

    aput-object v1, v0, v5

    sput-object v0, Lbvx;->m:[Ljava/lang/String;

    .line 873
    const-string v0, "content://mms-sms/canonical-address"

    .line 874
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lbvx;->n:Landroid/net/Uri;

    .line 1746
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "date_sent"

    aput-object v1, v0, v4

    sput-object v0, Lbvx;->o:[Ljava/lang/String;

    .line 1747
    sput-object v3, Lbvx;->p:Ljava/lang/Boolean;

    .line 1788
    sput-object v3, Lbvx;->q:Lbdh;

    .line 1803
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "mmsc"

    aput-object v1, v0, v4

    sput-object v0, Lbvx;->r:[Ljava/lang/String;

    .line 1805
    sput-object v3, Lbvx;->s:Ljava/lang/Boolean;

    .line 1871
    sput-object v3, Lbvx;->t:Ljava/lang/Boolean;

    .line 1872
    sput-object v3, Lbvx;->u:Ljava/lang/Boolean;

    .line 1927
    const-string v0, "content://mms/part"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lbvx;->b:Landroid/net/Uri;

    return-void

    .line 207
    :cond_0
    const-string v0, "content://mms"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_0

    .line 210
    :cond_1
    const-string v0, "content://mms/inbox"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_1

    .line 213
    :cond_2
    const-string v0, "content://mms/sent"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_2

    .line 216
    :cond_3
    const-string v0, "content://sms"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_3

    .line 219
    :cond_4
    const-string v0, "content://sms/sent"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_4

    .line 222
    :cond_5
    const-string v0, "content://telephony/carriers"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_5

    .line 225
    :cond_6
    const-string v0, "content://mms-sms/conversations"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_6
.end method

.method private static a(Landroid/content/Context;Landroid/net/Uri;)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 577
    const/4 v2, 0x0

    .line 579
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 581
    if-nez v2, :cond_1

    .line 588
    :goto_0
    if-eqz v2, :cond_0

    .line 590
    :try_start_1
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 596
    :cond_0
    :goto_1
    return v0

    .line 581
    :cond_1
    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->available()I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    goto :goto_0

    .line 591
    :catch_0
    move-exception v1

    .line 592
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getDataLength couldn\'t close: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 582
    :catch_1
    move-exception v1

    .line 583
    :try_start_3
    const-string v3, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "getDataLength couldn\'t stream: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 588
    if-eqz v2, :cond_0

    .line 590
    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 591
    :catch_2
    move-exception v1

    .line 592
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getDataLength couldn\'t close: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 585
    :catch_3
    move-exception v1

    .line 586
    :try_start_5
    const-string v3, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "getDataLength couldn\'t open: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 588
    if-eqz v2, :cond_0

    .line 590
    :try_start_6
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_1

    .line 591
    :catch_4
    move-exception v1

    .line 592
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getDataLength couldn\'t close: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 588
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_2

    .line 590
    :try_start_7
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 593
    :cond_2
    :goto_2
    throw v0

    .line 591
    :catch_5
    move-exception v1

    .line 592
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getDataLength couldn\'t close: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method public static a(Landroid/content/Context;Ljava/util/List;)J
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)J"
        }
    .end annotation

    .prologue
    const-wide/16 v0, -0x1

    .line 921
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 930
    :cond_0
    :goto_0
    return-wide v0

    .line 925
    :cond_1
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2, p1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 927
    :try_start_0
    invoke-static {p0, v2}, Lsj;->a(Landroid/content/Context;Ljava/util/Set;)J
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    goto :goto_0

    .line 928
    :catch_0
    move-exception v2

    .line 929
    const-string v3, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "MmsUtils: getting thread id failed: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Landroid/net/Uri;)J
    .locals 6

    .prologue
    .line 813
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    .line 814
    const/4 v1, 0x0

    .line 816
    :try_start_0
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "r"

    invoke-virtual {v0, p0, v2}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 817
    if-eqz v2, :cond_1

    .line 818
    :try_start_1
    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->getStatSize()J
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v0

    .line 823
    if-eqz v2, :cond_0

    .line 825
    :try_start_2
    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 831
    :cond_0
    :goto_0
    return-wide v0

    .line 826
    :catch_0
    move-exception v2

    .line 827
    const-string v3, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "MmsUtils.getMediaFileSize: failed to close "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 823
    :cond_1
    if-eqz v2, :cond_2

    .line 825
    :try_start_3
    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 831
    :cond_2
    :goto_1
    const-wide/16 v0, 0x0

    goto :goto_0

    .line 826
    :catch_1
    move-exception v0

    .line 827
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MmsUtils.getMediaFileSize: failed to close "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 820
    :catch_2
    move-exception v0

    .line 821
    :goto_2
    :try_start_4
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "MmsUtils.getMediaFileSize: cound not find media file: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 823
    if-eqz v1, :cond_2

    .line 825
    :try_start_5
    invoke-virtual {v1}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_1

    .line 826
    :catch_3
    move-exception v0

    .line 827
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MmsUtils.getMediaFileSize: failed to close "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 823
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v1, :cond_3

    .line 825
    :try_start_6
    invoke-virtual {v1}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 828
    :cond_3
    :goto_4
    throw v0

    .line 826
    :catch_4
    move-exception v1

    .line 827
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "MmsUtils.getMediaFileSize: failed to close "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 823
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_3

    .line 820
    :catch_5
    move-exception v0

    move-object v1, v2

    goto :goto_2
.end method

.method public static a(Landroid/content/Context;[Landroid/telephony/SmsMessage;I)Landroid/content/ContentValues;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1050
    aget-object v2, p1, v1

    .line 1051
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 1053
    const-string v4, "address"

    invoke-virtual {v2}, Landroid/telephony/SmsMessage;->getDisplayOriginatingAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1054
    const-string v4, "body"

    invoke-static {p1}, Lbvx;->a([Landroid/telephony/SmsMessage;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1055
    invoke-static {}, Lbvx;->e()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1057
    const-string v4, "date_sent"

    invoke-virtual {v2}, Landroid/telephony/SmsMessage;->getTimestampMillis()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1059
    :cond_0
    const-string v4, "protocol"

    invoke-virtual {v2}, Landroid/telephony/SmsMessage;->getProtocolIdentifier()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1060
    const-string v4, "seen"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1061
    invoke-virtual {v2}, Landroid/telephony/SmsMessage;->getPseudoSubject()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_1

    .line 1062
    const-string v4, "subject"

    invoke-virtual {v2}, Landroid/telephony/SmsMessage;->getPseudoSubject()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1064
    :cond_1
    const-string v4, "reply_path_present"

    invoke-virtual {v2}, Landroid/telephony/SmsMessage;->isReplyPathPresent()Z

    move-result v5

    if-eqz v5, :cond_2

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1065
    const-string v0, "service_center"

    invoke-virtual {v2}, Landroid/telephony/SmsMessage;->getServiceCenterAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1067
    const-string v0, "error_code"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1070
    invoke-virtual {v2}, Landroid/telephony/SmsMessage;->getDisplayOriginatingAddress()Ljava/lang/String;

    move-result-object v0

    .line 1071
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1072
    invoke-static {v0}, Lsi;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1081
    :goto_1
    const-string v1, "thread_id"

    invoke-static {p0, v0}, Lsj;->a(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1083
    return-object v3

    :cond_2
    move v0, v1

    .line 1064
    goto :goto_0

    .line 1074
    :cond_3
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->nB:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1075
    const-string v1, "address"

    invoke-virtual {v3, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static a()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 282
    sget-object v0, Lbvx;->j:Landroid/net/Uri;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JJ)Landroid/net/Uri;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 974
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lbvx;->i:Landroid/net/Uri;

    .line 976
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 974
    new-instance v4, Landroid/content/ContentValues;

    const/4 v5, 0x7

    invoke-direct {v4, v5}, Landroid/content/ContentValues;-><init>(I)V

    const-string v5, "address"

    invoke-virtual {v4, v5, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v3, :cond_0

    const-string v5, "date"

    invoke-virtual {v4, v5, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_0
    const-string v3, "read"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "subject"

    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "body"

    invoke-virtual {v4, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v5, -0x1

    cmp-long v3, p5, v5

    if-eqz v3, :cond_1

    const-string v3, "thread_id"

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_1
    invoke-virtual {v1, v2, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 985
    :goto_0
    return-object v0

    .line 980
    :catch_0
    move-exception v1

    .line 981
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "MmsUtils: persist sms message failure "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 982
    :catch_1
    move-exception v1

    .line 983
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "MmsUtils: persist sms message failure "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lsa;)Landroid/net/Uri;
    .locals 8

    .prologue
    .line 1017
    invoke-static {p0}, Lrv;->a(Landroid/content/Context;)Lrv;

    move-result-object v0

    .line 1018
    const/4 v1, 0x0

    .line 1020
    :try_start_0
    sget-object v2, Lbvx;->f:Landroid/net/Uri;

    .line 1024
    invoke-static {}, Lbvx;->b()Z

    move-result v3

    .line 1020
    invoke-virtual {v0, p1, v2, v3}, Lrv;->a(Lri;Landroid/net/Uri;Z)Landroid/net/Uri;
    :try_end_0
    .catch Lrc; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 1027
    :try_start_1
    new-instance v1, Landroid/content/ContentValues;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/content/ContentValues;-><init>(I)V

    .line 1028
    const-string v2, "date"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    div-long/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1029
    const-string v2, "seen"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1030
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v2, v0, v1}, Lf;->a(Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;)I
    :try_end_1
    .catch Lrc; {:try_start_1 .. :try_end_1} :catch_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_2

    .line 1038
    :goto_0
    return-object v0

    .line 1031
    :catch_0
    move-exception v0

    move-object v7, v0

    move-object v0, v1

    move-object v1, v7

    .line 1032
    :goto_1
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "MmsUtils: persist mms received message failure "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1034
    :catch_1
    move-exception v0

    move-object v7, v0

    move-object v0, v1

    move-object v1, v7

    .line 1035
    :goto_2
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "MmsUtils: update mms received message failure "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1034
    :catch_2
    move-exception v1

    goto :goto_2

    .line 1031
    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Lsc;Lsb;Lrw;)Landroid/net/Uri;
    .locals 6

    .prologue
    .line 991
    invoke-static {p0}, Lrv;->a(Landroid/content/Context;)Lrv;

    move-result-object v0

    .line 992
    const/4 v1, 0x0

    .line 995
    :try_start_0
    sget-object v2, Lbvx;->g:Landroid/net/Uri;

    .line 999
    invoke-static {}, Lbvx;->b()Z

    move-result v3

    .line 995
    invoke-virtual {v0, p1, v2, v3, p3}, Lrv;->a(Lri;Landroid/net/Uri;ZLrw;)Landroid/net/Uri;
    :try_end_0
    .catch Lrc; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 1003
    :try_start_1
    new-instance v1, Landroid/content/ContentValues;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/content/ContentValues;-><init>(I)V

    .line 1004
    const-string v2, "resp_st"

    invoke-virtual {p2}, Lsb;->f()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1005
    const-string v2, "m_id"

    invoke-virtual {p2}, Lsb;->e()[B

    move-result-object v3

    invoke-static {v3}, Lrv;->a([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1006
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v2, v0, v1}, Lf;->a(Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;)I
    :try_end_1
    .catch Lrc; {:try_start_1 .. :try_end_1} :catch_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_2

    .line 1012
    :goto_0
    return-object v0

    .line 1007
    :catch_0
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    .line 1008
    :goto_1
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "MmsUtils: persist mms sent message failure "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1009
    :catch_1
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    .line 1010
    :goto_2
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "MmsUtils: update mms sent message failure "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1009
    :catch_2
    move-exception v1

    goto :goto_2

    .line 1007
    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method public static a(Landroid/content/Intent;)Landroid/telephony/SmsMessage;
    .locals 1

    .prologue
    .line 1418
    const-string v0, "pdu"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    .line 1419
    invoke-static {v0}, Landroid/telephony/SmsMessage;->createFromPdu([B)Landroid/telephony/SmsMessage;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Lbvz;
    .locals 4

    .prologue
    .line 334
    invoke-static {p0}, Lrv;->a(Landroid/content/Context;)Lrv;

    move-result-object v0

    .line 336
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrv;->a(Landroid/net/Uri;)Lri;

    move-result-object v0

    .line 338
    instance-of v1, v0, Lrj;

    if-nez v1, :cond_0

    .line 339
    new-instance v0, Lrc;

    const-string v1, "Invalid PDU type to load"

    invoke-direct {v0, v1}, Lrc;-><init>(Ljava/lang/String;)V

    throw v0

    .line 341
    :cond_0
    check-cast v0, Lrj;

    .line 343
    new-instance v1, Lbvz;

    invoke-direct {v1}, Lbvz;-><init>()V

    .line 344
    invoke-virtual {v0}, Lrj;->e()Lrm;

    move-result-object v2

    iput-object v2, v1, Lbvz;->b:Lrm;

    .line 345
    invoke-virtual {v0}, Lrj;->h()J

    move-result-wide v2

    long-to-int v0, v2

    iput v0, v1, Lbvz;->a:I

    .line 347
    return-object v1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)Lbvz;
    .locals 14

    .prologue
    .line 296
    new-instance v8, Lrm;

    invoke-direct {v8}, Lrm;-><init>()V

    .line 298
    const/4 v6, 0x0

    .line 299
    const/4 v3, 0x0

    .line 301
    if-eqz p2, :cond_2

    const/4 v2, 0x1

    .line 302
    :goto_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1e

    .line 303
    if-nez v2, :cond_3

    const/4 v3, 0x1

    :goto_1
    new-instance v5, Lru;

    invoke-direct {v5}, Lru;-><init>()V

    const/16 v4, 0x6a

    invoke-virtual {v5, v4}, Lru;->a(I)V

    const-string v4, "text/plain"

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v5, v4}, Lru;->e([B)V

    const-string v4, "text_0.txt"

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v5, v4}, Lru;->c([B)V

    const-string v4, "text_0.txt"

    const-string v6, "."

    invoke-virtual {v4, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    const/4 v6, -0x1

    if-ne v4, v6, :cond_4

    const-string v4, "text_0.txt"

    :goto_2
    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v5, v4}, Lru;->b([B)V

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v5, v4}, Lru;->a([B)V

    invoke-virtual {v8, v5}, Lrm;->a(Lru;)Z

    if-eqz v3, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    const-string v4, "<smil><head><layout><root-layout/><region height=\"100%%\" id=\"Text\" left=\"0%%\" top=\"0%%\" width=\"100%%\"/></layout></head><body><par dur=\"8000ms\"><text src=\"%s\" region=\"Text\"/></par></body></smil>"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v9, "text_0.txt"

    aput-object v9, v6, v7

    invoke-static {v3, v4, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v8, v3}, Lbvx;->a(Lrm;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v5}, Lru;->a()[B

    move-result-object v3

    array-length v3, v3

    add-int/lit8 v6, v3, 0x0

    .line 304
    const-string v3, "text_0.txt"

    move-object v7, v3

    .line 306
    :goto_3
    if-eqz v2, :cond_1c

    .line 307
    invoke-static/range {p3 .. p3}, Lf;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 308
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string p3, "image/jpeg"

    :cond_1
    invoke-static/range {p3 .. p3}, Lf;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unsupported image contentType: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-static {v2, v3, v4}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v3, 0x0

    :goto_4
    add-int v2, v6, v3

    .line 325
    :goto_5
    new-instance v3, Lbvz;

    invoke-direct {v3}, Lbvz;-><init>()V

    .line 326
    iput-object v8, v3, Lbvz;->b:Lrm;

    .line 327
    iput v2, v3, Lbvz;->a:I

    .line 329
    return-object v3

    .line 301
    :cond_2
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 303
    :cond_3
    const/4 v3, 0x0

    goto/16 :goto_1

    :cond_4
    const-string v6, "text_0.txt"

    const/4 v7, 0x0

    invoke-virtual {v6, v7, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    .line 308
    :cond_5
    invoke-static/range {p2 .. p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-static {p0, v5}, Lbvx;->a(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v4

    if-gtz v4, :cond_6

    const-string v2, "Babel"

    const-string v3, "Can\'t get image"

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-static {v2, v3, v4}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v3, 0x0

    goto :goto_4

    :cond_6
    invoke-static {}, Lbvm;->a()Lsm;

    move-result-object v2

    invoke-virtual {v2}, Lsm;->e()I

    move-result v2

    add-int/lit16 v9, v2, -0x400

    invoke-static {}, Lbvm;->a()Lsm;

    move-result-object v2

    invoke-virtual {v2}, Lsm;->m()I

    move-result v2

    invoke-static {}, Lbvm;->a()Lsm;

    move-result-object v3

    invoke-virtual {v3}, Lsm;->l()I

    move-result v3

    move/from16 v0, p5

    move/from16 v1, p4

    if-le v0, v1, :cond_1d

    :goto_6
    sget-boolean v10, Lbvx;->c:Z

    if-eqz v10, :cond_7

    const-string v10, "Babel"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "addPicturePart size: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " width: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move/from16 v0, p4

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " widthLimit: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " height: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move/from16 v0, p5

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " heightLimit: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    if-gt v4, v9, :cond_a

    move/from16 v0, p4

    if-gt v0, v3, :cond_a

    move/from16 v0, p5

    if-gt v0, v2, :cond_a

    if-nez p6, :cond_a

    sget-boolean v2, Lbvx;->c:Z

    if-eqz v2, :cond_8

    const-string v2, "Babel"

    const-string v3, "addPicturePart - already sized"

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    new-instance v2, Lru;

    invoke-direct {v2}, Lru;-><init>()V

    invoke-virtual {v2, v5}, Lru;->a(Landroid/net/Uri;)V

    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v2, v3}, Lru;->e([B)V

    move v3, v4

    :goto_7
    const-string v5, "image.jpg"

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v2, v4}, Lru;->c([B)V

    const-string v4, "."

    invoke-virtual {v5, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    const/4 v9, -0x1

    if-ne v4, v9, :cond_e

    move-object v4, v5

    :goto_8
    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v2, v4}, Lru;->b([B)V

    invoke-virtual {v8, v2}, Lrm;->a(Lru;)Z

    sget-boolean v2, Lbvx;->c:Z

    if-eqz v2, :cond_9

    const-string v2, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v9, "addPicturePart size: "

    invoke-direct {v4, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_f

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    const-string v4, "<text src=\"%s\" region=\"Text\"/>"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v7, v9, v10

    invoke-static {v2, v4, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_9
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    const-string v7, "<smil><head><layout><root-layout width=\"%dpx\" height=\"%dpx\" /><region id=\"Image\" left=\"0\" top=\"0\" width=\"%dpx\" height=\"%dpx\" fit=\"meet\" /></layout></head><body><par dur=\"5000ms\"><img src=\"%s\" region=\"Image\" />%s</par></body></smil>"

    const/4 v9, 0x6

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x3

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x4

    aput-object v5, v9, v10

    const/4 v5, 0x5

    aput-object v2, v9, v5

    invoke-static {v4, v7, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v8, v2}, Lbvx;->a(Lrm;Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_a
    new-instance v4, Lru;

    invoke-direct {v4}, Lru;-><init>()V

    move/from16 v0, p6

    invoke-static {v0, v3, v2, v9, v5}, Lbvx;->a(IIIILandroid/net/Uri;)[B

    move-result-object v2

    if-nez v2, :cond_c

    sget-boolean v2, Lbvx;->c:Z

    if-eqz v2, :cond_b

    const-string v2, "Babel"

    const-string v3, "Resize image failed."

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_b
    const/4 v2, 0x0

    :goto_a
    if-nez v2, :cond_d

    const-string v2, "Babel"

    const-string v3, "Can\'t resize image: not enough memory?"

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-static {v2, v3, v4}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v3, 0x0

    goto/16 :goto_4

    :cond_c
    invoke-virtual {v4, v2}, Lru;->a([B)V

    const-string v2, "image/jpeg"

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v4, v2}, Lru;->e([B)V

    move-object v2, v4

    goto :goto_a

    :cond_d
    invoke-virtual {v2}, Lru;->a()[B

    move-result-object v3

    array-length v3, v3

    goto/16 :goto_7

    :cond_e
    const/4 v9, 0x0

    invoke-virtual {v5, v9, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_8

    :cond_f
    const-string v2, ""

    goto :goto_9

    .line 310
    :cond_10
    invoke-static/range {p3 .. p3}, Lf;->d(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 311
    invoke-static/range {p2 .. p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {p0, v2}, Lbvx;->a(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v4

    if-gtz v4, :cond_11

    const-string v2, "Babel"

    const-string v3, "Can\'t get vcard"

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-static {v2, v3, v4}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v2, 0x0

    :goto_b
    add-int/2addr v2, v6

    goto/16 :goto_5

    :cond_11
    new-instance v5, Lru;

    invoke-direct {v5}, Lru;-><init>()V

    invoke-virtual {v5, v2}, Lru;->a(Landroid/net/Uri;)V

    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v5, v2}, Lru;->e([B)V

    const-string v3, "contact.vcf"

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v5, v2}, Lru;->c([B)V

    const-string v2, "."

    invoke-virtual {v3, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    const/4 v9, -0x1

    if-ne v2, v9, :cond_13

    move-object v2, v3

    :goto_c
    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v5, v2}, Lru;->b([B)V

    invoke-virtual {v8, v5}, Lrm;->a(Lru;)Z

    sget-boolean v2, Lbvx;->c:Z

    if-eqz v2, :cond_12

    const-string v2, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v9, "addVCardPart size: "

    invoke-direct {v5, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_12
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_14

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    const-string v5, "<text src=\"%s\" region=\"Text\"/>"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v7, v9, v10

    invoke-static {v2, v5, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_d
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    const-string v7, "<smil><head><layout><root-layout width=\"%dpx\" height=\"%dpx\" /></layout></head><body><par dur=\"5000ms\"><ref src=\"%s\" />%s</par></body></smil>"

    const/4 v9, 0x6

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const/16 v11, 0x12c

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    const/16 v11, 0x12c

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    const/16 v11, 0x12c

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x3

    const/16 v11, 0x12c

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x4

    aput-object v3, v9, v10

    const/4 v3, 0x5

    aput-object v2, v9, v3

    invoke-static {v5, v7, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v8, v2}, Lbvx;->a(Lrm;Ljava/lang/String;)V

    move v2, v4

    goto/16 :goto_b

    :cond_13
    const/4 v9, 0x0

    invoke-virtual {v3, v9, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_c

    :cond_14
    const-string v2, ""

    goto :goto_d

    .line 313
    :cond_15
    invoke-static/range {p3 .. p3}, Lf;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 314
    if-nez p4, :cond_16

    .line 315
    const/16 p4, 0x140

    .line 317
    :cond_16
    if-nez p5, :cond_17

    .line 318
    const/16 p5, 0xa0

    .line 320
    :cond_17
    sget-boolean v2, Lbvx;->c:Z

    if-eqz v2, :cond_18

    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "addVideoPart attachmentUrl: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_18
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_19

    const-string p3, "video/3gpp2"

    :cond_19
    new-instance v4, Lru;

    invoke-direct {v4}, Lru;-><init>()V

    invoke-static/range {p2 .. p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5}, Lru;->a(Landroid/net/Uri;)V

    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v4, v2}, Lru;->e([B)V

    const-string v3, "video.3gp"

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v4, v2}, Lru;->c([B)V

    const-string v2, "."

    invoke-virtual {v3, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    const/4 v9, -0x1

    if-ne v2, v9, :cond_1a

    move-object v2, v3

    :goto_e
    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v4, v2}, Lru;->b([B)V

    invoke-virtual {v8, v4}, Lrm;->a(Lru;)Z

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1b

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    const-string v4, "<text src=\"%s\" region=\"Text\"/>"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v7, v9, v10

    invoke-static {v2, v4, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_f
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    const-string v7, "<smil><head><layout><root-layout width=\"%d\" height=\"%d\" /><region id=\"Image\" left=\"0\" top=\"0\" width=\"%dpx\" height=\"%dpx\" fit=\"meet\" /></layout></head><body><par dur=\"8000ms\"><video src=\"%s\" region=\"Image\" />%s</par></body></smil>"

    const/4 v9, 0x6

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x3

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x4

    aput-object v3, v9, v10

    const/4 v3, 0x5

    aput-object v2, v9, v3

    invoke-static {v4, v7, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v8, v2}, Lbvx;->a(Lrm;Ljava/lang/String;)V

    invoke-static {v5}, Lbvx;->a(Landroid/net/Uri;)J

    move-result-wide v2

    long-to-int v2, v2

    add-int/2addr v2, v6

    goto/16 :goto_5

    :cond_1a
    const/4 v9, 0x0

    invoke-virtual {v3, v9, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_e

    :cond_1b
    const-string v2, ""

    goto :goto_f

    :cond_1c
    move v2, v6

    goto/16 :goto_5

    :cond_1d
    move v13, v3

    move v3, v2

    move v2, v13

    goto/16 :goto_6

    :cond_1e
    move-object v7, v3

    goto/16 :goto_3
.end method

.method public static a(Ljava/lang/String;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 6

    .prologue
    .line 1136
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    .line 1137
    invoke-static {v1, p0}, Lbvx;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1138
    if-nez p1, :cond_1

    .line 1139
    const-string v0, ""

    .line 1141
    :goto_0
    if-eqz v2, :cond_0

    sget v3, Lh;->fI:I

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    const/4 v2, 0x1

    aput-object v0, v4, v2

    invoke-virtual {v1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0

    :cond_1
    move-object v0, p1

    goto :goto_0
.end method

.method public static a(Landroid/telephony/SmsMessage;J)Ljava/lang/Long;
    .locals 4

    .prologue
    .line 1115
    new-instance v0, Ljava/util/GregorianCalendar;

    const/16 v1, 0x7db

    const/16 v2, 0x8

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Ljava/util/GregorianCalendar;-><init>(III)V

    .line 1116
    new-instance v1, Ljava/util/GregorianCalendar;

    invoke-direct {v1}, Ljava/util/GregorianCalendar;-><init>()V

    .line 1117
    invoke-virtual {v1, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1118
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1122
    invoke-virtual {p0}, Landroid/telephony/SmsMessage;->getTimestampMillis()J

    move-result-wide p1

    .line 1124
    :cond_0
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1455
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "((%s LIKE \'%s\') OR (%s LIKE \'%s\') OR (%s LIKE \'%s\') OR (%s=\'%s\'))"

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    const-string v4, "image/%"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object p0, v2, v3

    const/4 v3, 0x3

    const-string v4, "video/%"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    aput-object p0, v2, v3

    const/4 v3, 0x5

    const-string v4, "audio/%"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    aput-object p0, v2, v3

    const/4 v3, 0x7

    const-string v4, "application/ogg"

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/List;J)Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;J)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1998
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    .line 2001
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v6, :cond_0

    .line 2002
    invoke-interface {p0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2023
    :goto_0
    return-object v0

    .line 2008
    :cond_0
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "content://mms/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/addr"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2009
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "address"

    aput-object v3, v2, v5

    const-string v3, "charset"

    aput-object v3, v2, v6

    const-string v3, "type=137"

    move-object v5, v4

    .line 2006
    invoke-static/range {v0 .. v5}, Lf;->a(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2014
    if-eqz v1, :cond_2

    .line 2016
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2017
    invoke-static {v1}, Lbvi;->a(Landroid/database/Cursor;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2020
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v0, v4

    .line 2023
    goto :goto_0

    .line 2020
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static a(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1396
    if-eqz p0, :cond_5

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_5

    .line 1397
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1398
    if-eqz p1, :cond_0

    .line 1399
    invoke-static {p1}, Lbzd;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 1401
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1402
    if-eqz p1, :cond_2

    .line 1403
    invoke-static {v0}, Lbzd;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1404
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_3

    .line 1405
    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1407
    :cond_3
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1410
    :cond_4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_5

    .line 1411
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1414
    :goto_1
    return-object v0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Lyt;Lbdk;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1210
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1211
    const/4 v0, 0x0

    .line 1213
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ";"

    invoke-virtual {p2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lbvx;->a(Lyt;Lbdk;Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lyt;Lbdk;Ljava/util/List;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lyt;",
            "Lbdk;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1226
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 1241
    :goto_0
    return-object v0

    .line 1230
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1231
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1234
    invoke-static {v0, v1, v1}, Lbdh;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lbdh;

    move-result-object v0

    .line 1235
    if-eqz p1, :cond_2

    iget-object v4, v0, Lbdh;->b:Lbdk;

    invoke-virtual {p1, v4}, Lbdk;->a(Lbdk;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1236
    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v4, v0, Lbdh;->i:Ljava/lang/Boolean;

    .line 1238
    :cond_2
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1241
    :cond_3
    if-eqz p1, :cond_4

    const/4 v0, 0x1

    :goto_2
    invoke-static {p0, v0, v2}, Lbvx;->a(Lyt;ZLjava/util/List;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static a(Lyt;ZLjava/util/List;)Ljava/lang/String;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lyt;",
            "Z",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x1

    .line 1254
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1261
    :cond_0
    :goto_0
    return-object v3

    .line 1258
    :cond_1
    new-instance v9, Lbnl;

    invoke-direct {v9}, Lbnl;-><init>()V

    .line 1259
    const-wide/16 v0, -0x1

    invoke-virtual {v9, v0, v1}, Lbnl;->a(J)V

    .line 1268
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v5, :cond_2

    const/4 v6, 0x2

    :goto_1
    const/4 v7, 0x3

    const/4 v8, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v4, v3

    .line 1261
    invoke-static/range {v0 .. v9}, Lyp;->a(Lyt;ZLjava/util/List;Ljava/util/List;Ljava/util/List;ZIIZLbnl;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_2
    move v6, v5

    .line 1268
    goto :goto_1
.end method

.method public static a([BLjava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1720
    if-nez p0, :cond_0

    .line 1721
    const/4 v0, 0x0

    .line 1727
    :goto_0
    return-object v0

    .line 1724
    :cond_0
    :try_start_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p0, p1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1725
    :catch_0
    move-exception v0

    .line 1726
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MmsUtils.bytesToString: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1727
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p0}, Ljava/lang/String;-><init>([B)V

    goto :goto_0
.end method

.method private static a([Landroid/telephony/SmsMessage;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1093
    array-length v1, p0

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 1095
    aget-object v0, p0, v0

    invoke-virtual {v0}, Landroid/telephony/SmsMessage;->getDisplayMessageBody()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbvx;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1106
    :goto_0
    return-object v0

    .line 1098
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1099
    array-length v2, p0

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v3, p0, v0

    .line 1102
    :try_start_0
    invoke-virtual {v3}, Landroid/telephony/SmsMessage;->getDisplayMessageBody()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1099
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1106
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbvx;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v3

    goto :goto_2
.end method

.method public static a(J)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 848
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-gtz v0, :cond_1

    .line 870
    :cond_0
    :goto_0
    return-object v5

    .line 851
    :cond_1
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v6

    .line 852
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 853
    sget-object v1, Lbvx;->l:Landroid/net/Uri;

    sget-object v2, Lbvx;->m:[Ljava/lang/String;

    const-string v3, "_id=?"

    new-array v4, v4, [Ljava/lang/String;

    const/4 v7, 0x0

    .line 855
    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v7

    .line 853
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 856
    if-eqz v1, :cond_0

    .line 858
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 861
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 862
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 863
    invoke-static {v6, v0}, Lbvx;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 867
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static a(JJ)V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1610
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1611
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1612
    const-string v2, "read"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1613
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%s=%d AND %s<=%d AND %s=0"

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "thread_id"

    aput-object v5, v4, v7

    .line 1617
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v6

    const-string v5, "date"

    aput-object v5, v4, v8

    .line 1619
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v9

    const-string v5, "read"

    aput-object v5, v4, v10

    .line 1613
    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1621
    sget-object v3, Lbvx;->h:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v1, v2, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1626
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%s=%d AND %s<=%d AND %s=0"

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "thread_id"

    aput-object v5, v4, v7

    .line 1630
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v6

    const-string v5, "date"

    aput-object v5, v4, v8

    const-wide/16 v5, 0x3e8

    div-long v5, p2, v5

    .line 1632
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v9

    const-string v5, "read"

    aput-object v5, v4, v10

    .line 1626
    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1634
    sget-object v3, Lbvx;->e:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v1, v2, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1639
    return-void
.end method

.method public static a(Landroid/net/Uri;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1645
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1646
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1647
    const-string v3, "read"

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1648
    invoke-virtual {v1, p0, v2, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1649
    return-void

    .line 1647
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 1878
    sput-object p0, Lbvx;->t:Ljava/lang/Boolean;

    .line 1879
    return-void
.end method

.method public static a(Ljava/lang/String;IJ)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1427
    if-nez p0, :cond_1

    .line 1441
    :cond_0
    :goto_0
    return-void

    .line 1430
    :cond_1
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1431
    if-eqz v0, :cond_0

    .line 1434
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1435
    const-string v2, "status"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1436
    invoke-static {}, Lbvx;->e()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1437
    const-string v2, "date_sent"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1439
    :cond_2
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 1440
    invoke-virtual {v2, v0, v1, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;[B)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1281
    new-instance v1, Lrt;

    invoke-direct {v1, p1}, Lrt;-><init>([B)V

    .line 1282
    const/4 v0, 0x0

    .line 1284
    :try_start_0
    invoke-virtual {v1}, Lrt;->a()Lri;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1288
    :goto_0
    if-nez v0, :cond_0

    .line 1289
    const-string v0, "Babel"

    const-string v1, "Invalid WAP push data"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 1328
    :goto_1
    return-void

    .line 1285
    :catch_0
    move-exception v1

    .line 1286
    const-string v2, "Babel"

    const-string v3, "Invalid MMS WAP push"

    invoke-static {v2, v3, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1292
    :cond_0
    invoke-virtual {v0}, Lri;->b()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    goto :goto_1

    .line 1294
    :sswitch_0
    const-string v1, "Babel"

    const-string v2, "Received MMS notification"

    invoke-static {v1, v2}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1296
    check-cast v0, Lrk;

    .line 1297
    invoke-static {}, Lbvm;->a()Lsm;

    move-result-object v1

    invoke-virtual {v1}, Lsm;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1298
    invoke-virtual {v0}, Lrk;->e()[B

    move-result-object v1

    .line 1299
    const/16 v2, 0x3d

    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    aget-byte v3, v1, v3

    if-ne v2, v3, :cond_1

    .line 1300
    invoke-virtual {v0}, Lrk;->h()[B

    move-result-object v2

    .line 1301
    array-length v3, v1

    array-length v4, v2

    add-int/2addr v3, v4

    new-array v3, v3, [B

    .line 1303
    array-length v4, v1

    invoke-static {v1, v5, v3, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1306
    array-length v1, v1

    array-length v4, v2

    invoke-static {v2, v5, v3, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1309
    invoke-virtual {v0, v3}, Lrk;->a([B)V

    .line 1315
    :cond_1
    invoke-static {p0, v0}, Lyp;->a(Ljava/lang/String;Lrk;)V

    goto :goto_1

    .line 1320
    :sswitch_1
    const-string v0, "Babel"

    const-string v1, "Received delivery report"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1324
    :sswitch_2
    const-string v0, "Babel"

    const-string v1, "Received read report"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1292
    nop

    :sswitch_data_0
    .sparse-switch
        0x82 -> :sswitch_0
        0x86 -> :sswitch_1
        0x88 -> :sswitch_2
    .end sparse-switch
.end method

.method private static a(Lrm;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 568
    new-instance v0, Lru;

    invoke-direct {v0}, Lru;-><init>()V

    .line 569
    const-string v1, "smil"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lru;->b([B)V

    .line 570
    const-string v1, "smil.xml"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lru;->c([B)V

    .line 571
    const-string v1, "application/smil"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lru;->e([B)V

    .line 572
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lru;->a([B)V

    .line 573
    invoke-virtual {p0, v0}, Lrm;->b(Lru;)V

    .line 574
    return-void
.end method

.method public static a(Z)V
    .locals 9

    .prologue
    .line 1843
    if-nez p0, :cond_0

    .line 1845
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-eq v0, v1, :cond_0

    .line 1849
    invoke-static {}, Lxw;->a()Lxw;

    move-result-object v0

    invoke-virtual {v0}, Lxw;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1852
    :try_start_0
    const-string v1, "apn"

    sget-object v2, Lxw;->a:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1861
    if-eqz v0, :cond_0

    .line 1862
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1867
    :cond_0
    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lbvx;->s:Ljava/lang/Boolean;

    .line 1868
    return-void

    .line 1859
    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {}, Lxw;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1862
    :catchall_0
    move-exception v0

    throw v0
.end method

.method public static a(I)Z
    .locals 1

    .prologue
    .line 607
    invoke-static {p0}, Lf;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lbvx;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(II)Z
    .locals 4

    .prologue
    .line 1918
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    .line 1919
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1920
    const-string v2, "smsmms"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1922
    invoke-virtual {v1, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1923
    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 1924
    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static a(IIIILandroid/net/Uri;)[B
    .locals 16

    .prologue
    .line 691
    const/4 v2, 0x0

    .line 693
    :try_start_0
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 694
    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    .line 695
    invoke-static {v1}, Lbyr;->a(Ljava/io/InputStream;)[B
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    move-object v9, v1

    .line 701
    :goto_0
    if-nez v9, :cond_2

    .line 702
    sget-boolean v1, Lbvx;->c:Z

    if-eqz v1, :cond_0

    .line 703
    const-string v1, "Babel_medialoader"

    const-string v2, "getResizedImageData: Could not get image bytes"

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 706
    :cond_0
    const/4 v1, 0x0

    .line 806
    :cond_1
    :goto_1
    return-object v1

    .line 696
    :catch_0
    move-exception v1

    .line 697
    const-string v3, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Could not open file corresponding to uri "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v9, v2

    .line 700
    goto :goto_0

    .line 698
    :catch_1
    move-exception v1

    .line 699
    const-string v3, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "NPE trying to open the uri "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v9, v2

    goto :goto_0

    .line 711
    :cond_2
    invoke-static {}, Lbyr;->a()Lbxs;

    move-result-object v10

    .line 712
    const/4 v1, 0x0

    move v6, v1

    move/from16 v7, p2

    move/from16 v8, p1

    :goto_2
    const/4 v1, 0x4

    if-ge v6, v1, :cond_1c

    .line 713
    const/4 v3, 0x0

    .line 714
    const/4 v1, 0x0

    .line 716
    :try_start_1
    sget-boolean v2, Lbvx;->c:Z

    if-eqz v2, :cond_3

    .line 717
    const-string v2, "Babel_medialoader"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "getResizedImageData: decode limit w="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " h="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 721
    :cond_3
    move/from16 v0, p0

    invoke-virtual {v10, v9, v8, v7, v0}, Lbxs;->b([BIII)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 723
    if-nez v3, :cond_6

    .line 724
    sget-boolean v2, Lbvx;->c:Z

    if-eqz v2, :cond_4

    .line 725
    const-string v2, "Babel_medialoader"

    const-string v4, "getResizedImageData: got empty decoded bitmap"

    invoke-static {v2, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 728
    :cond_4
    if-eqz v3, :cond_5

    .line 795
    invoke-virtual {v10, v3}, Lbxs;->a(Landroid/graphics/Bitmap;)V

    .line 797
    :cond_5
    const/4 v1, 0x0

    goto :goto_1

    .line 730
    :cond_6
    :try_start_2
    sget-boolean v2, Lbvx;->c:Z

    if-eqz v2, :cond_7

    .line 731
    const-string v2, "Babel_medialoader"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "getResizedImageData: decoded w,h="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 732
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 731
    invoke-static {v2, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 736
    :cond_7
    :try_start_3
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    .line 737
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    .line 738
    move/from16 v0, p1

    if-gt v11, v0, :cond_8

    move/from16 v0, p2

    if-le v12, v0, :cond_1e

    .line 739
    :cond_8
    if-nez p1, :cond_c

    const-wide/high16 v1, 0x3ff0000000000000L    # 1.0

    move-wide v4, v1

    :goto_3
    if-nez p2, :cond_d

    const-wide/high16 v1, 0x3ff0000000000000L    # 1.0

    :goto_4
    invoke-static {v4, v5, v1, v2}, Ljava/lang/Math;->max(DD)D

    move-result-wide v1

    .line 742
    int-to-double v4, v11

    div-double/2addr v4, v1

    double-to-int v4, v4

    .line 743
    int-to-double v11, v12

    div-double v1, v11, v1

    double-to-int v1, v1

    .line 744
    const/4 v2, 0x0

    invoke-static {v3, v4, v1, v2}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v2

    .line 747
    :goto_5
    if-nez v2, :cond_e

    .line 748
    :try_start_4
    sget-boolean v1, Lbvx;->c:Z

    if-eqz v1, :cond_9

    .line 749
    const-string v1, "Babel_medialoader"

    const-string v4, "getResizedImageData: got empty scaled bitmap"

    invoke-static {v1, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 752
    :cond_9
    if-eqz v3, :cond_a

    .line 795
    invoke-virtual {v10, v3}, Lbxs;->a(Landroid/graphics/Bitmap;)V

    .line 797
    :cond_a
    if-eqz v2, :cond_b

    if-eq v2, v3, :cond_b

    .line 798
    invoke-virtual {v10, v2}, Lbxs;->a(Landroid/graphics/Bitmap;)V

    :cond_b
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 739
    :cond_c
    int-to-double v1, v11

    move/from16 v0, p1

    int-to-double v4, v0

    div-double/2addr v1, v4

    move-wide v4, v1

    goto :goto_3

    :cond_d
    int-to-double v1, v12

    move/from16 v0, p2

    int-to-double v13, v0

    div-double/2addr v1, v13

    goto :goto_4

    .line 754
    :cond_e
    :try_start_5
    sget-boolean v1, Lbvx;->c:Z

    if-eqz v1, :cond_f

    .line 755
    const-string v1, "Babel_medialoader"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "getResizedImageData: scaled w,h="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 756
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 755
    invoke-static {v1, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 759
    :cond_f
    const/16 v1, 0x5f

    invoke-static {v2, v1}, Lbyr;->a(Landroid/graphics/Bitmap;I)[B

    move-result-object v1

    .line 762
    if-eqz v1, :cond_10

    array-length v4, v1

    move/from16 v0, p3

    if-le v4, v0, :cond_14

    .line 764
    :cond_10
    mul-int/lit8 v4, p3, 0x5f

    array-length v1, v1

    div-int v1, v4, v1

    .line 765
    const/16 v4, 0x32

    if-ge v1, v4, :cond_1d

    .line 766
    const/16 v1, 0x32

    move v4, v1

    .line 768
    :goto_6
    sget-boolean v1, Lbvx;->c:Z

    if-eqz v1, :cond_11

    .line 769
    const-string v1, "Babel_medialoader"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v11, "getResizedImageData: compress(2) w/ quality="

    invoke-direct {v5, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 772
    :cond_11
    invoke-static {v2, v4}, Lbyr;->a(Landroid/graphics/Bitmap;I)[B

    move-result-object v1

    .line 773
    const/16 v5, 0x32

    if-le v4, v5, :cond_14

    if-eqz v1, :cond_12

    array-length v4, v1

    move/from16 v0, p3

    if-le v4, v0, :cond_14

    .line 776
    :cond_12
    sget-boolean v1, Lbvx;->c:Z

    if-eqz v1, :cond_13

    .line 778
    const-string v1, "Babel_medialoader"

    const-string v4, "getResizedImageData: compress(2) w/ quality=50"

    invoke-static {v1, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 781
    :cond_13
    const/16 v1, 0x32

    invoke-static {v2, v1}, Lbyr;->a(Landroid/graphics/Bitmap;I)[B

    move-result-object v1

    .line 785
    :cond_14
    if-eqz v1, :cond_16

    array-length v4, v1
    :try_end_5
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move/from16 v0, p3

    if-gt v4, v0, :cond_16

    .line 794
    if-eqz v3, :cond_15

    .line 795
    invoke-virtual {v10, v3}, Lbxs;->a(Landroid/graphics/Bitmap;)V

    .line 797
    :cond_15
    if-eqz v2, :cond_1

    if-eq v2, v3, :cond_1

    .line 798
    invoke-virtual {v10, v2}, Lbxs;->a(Landroid/graphics/Bitmap;)V

    goto/16 :goto_1

    .line 794
    :cond_16
    if-eqz v3, :cond_17

    .line 795
    invoke-virtual {v10, v3}, Lbxs;->a(Landroid/graphics/Bitmap;)V

    .line 797
    :cond_17
    if-eqz v2, :cond_18

    if-eq v2, v3, :cond_18

    .line 798
    invoke-virtual {v10, v2}, Lbxs;->a(Landroid/graphics/Bitmap;)V

    .line 803
    :cond_18
    :goto_7
    int-to-double v1, v8

    const-wide v3, 0x3fe3333340000000L    # 0.6000000238418579

    mul-double/2addr v1, v3

    double-to-int v3, v1

    .line 804
    int-to-double v1, v7

    const-wide v4, 0x3fe3333340000000L    # 0.6000000238418579

    mul-double/2addr v1, v4

    double-to-int v2, v1

    .line 712
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    move v7, v2

    move v8, v3

    goto/16 :goto_2

    .line 789
    :catch_2
    move-exception v2

    :goto_8
    :try_start_6
    const-string v2, "Babel"

    const-string v4, "getResizedImageData - image too big (OutOfMemoryError), will try  with smaller scale factor"

    invoke-static {v2, v4}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 794
    if-eqz v3, :cond_19

    .line 795
    invoke-virtual {v10, v3}, Lbxs;->a(Landroid/graphics/Bitmap;)V

    .line 797
    :cond_19
    if-eqz v1, :cond_18

    if-eq v1, v3, :cond_18

    .line 798
    invoke-virtual {v10, v1}, Lbxs;->a(Landroid/graphics/Bitmap;)V

    goto :goto_7

    .line 794
    :catchall_0
    move-exception v2

    move-object v15, v2

    move-object v2, v1

    move-object v1, v15

    :goto_9
    if-eqz v3, :cond_1a

    .line 795
    invoke-virtual {v10, v3}, Lbxs;->a(Landroid/graphics/Bitmap;)V

    .line 797
    :cond_1a
    if-eqz v2, :cond_1b

    if-eq v2, v3, :cond_1b

    .line 798
    invoke-virtual {v10, v2}, Lbxs;->a(Landroid/graphics/Bitmap;)V

    :cond_1b
    throw v1

    .line 806
    :cond_1c
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 794
    :catchall_1
    move-exception v1

    move-object v2, v3

    goto :goto_9

    :catchall_2
    move-exception v1

    goto :goto_9

    :catchall_3
    move-exception v2

    move-object v15, v2

    move-object v2, v1

    move-object v1, v15

    goto :goto_9

    .line 789
    :catch_3
    move-exception v1

    move-object v1, v3

    goto :goto_8

    :catch_4
    move-exception v1

    move-object v1, v2

    goto :goto_8

    :cond_1d
    move v4, v1

    goto/16 :goto_6

    :cond_1e
    move-object v2, v3

    goto/16 :goto_5
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)[B
    .locals 4

    .prologue
    .line 1735
    if-nez p0, :cond_0

    .line 1736
    const/4 v0, 0x0

    .line 1742
    :goto_0
    return-object v0

    .line 1739
    :cond_0
    :try_start_0
    invoke-virtual {p0, p1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 1740
    :catch_0
    move-exception v0

    .line 1741
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MmsUtils.stringToBytes: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1742
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    goto :goto_0
.end method

.method public static b(J)J
    .locals 6

    .prologue
    const-wide/16 v4, 0x3e8

    .line 1370
    add-long v0, p0, v4

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    div-long/2addr v0, v4

    return-wide v0
.end method

.method public static b(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1544
    if-gtz p0, :cond_0

    .line 1545
    const/4 v0, 0x0

    .line 1553
    :goto_0
    return-object v0

    .line 1547
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1548
    const-string v0, "(?"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1549
    const/4 v0, 0x0

    :goto_1
    add-int/lit8 v2, p0, -0x1

    if-ge v0, v2, :cond_1

    .line 1550
    const-string v2, ",?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1549
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1552
    :cond_1
    const-string v0, ")"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1553
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1153
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    move-object p1, v0

    .line 1165
    :cond_0
    :goto_0
    return-object p1

    .line 1156
    :cond_1
    sget-object v1, Lbvx;->d:[Ljava/lang/String;

    if-nez v1, :cond_2

    .line 1158
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lf;->bu:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lbvx;->d:[Ljava/lang/String;

    .line 1160
    :cond_2
    sget-object v2, Lbvx;->d:[Ljava/lang/String;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 1161
    invoke-virtual {p1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    move-object p1, v0

    .line 1162
    goto :goto_0

    .line 1160
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static b(Landroid/net/Uri;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, -0x1

    .line 1170
    invoke-virtual {p0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    .line 1171
    const/16 v2, 0x3f

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 1172
    if-eq v2, v6, :cond_0

    .line 1173
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1175
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_2

    aget-char v1, v3, v0

    const/16 v5, 0xa

    invoke-static {v1, v5}, Ljava/lang/Character;->digit(CI)I

    move-result v5

    if-eq v5, v6, :cond_1

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x2c

    const/16 v2, 0x3b

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    .line 1176
    return-object v0
.end method

.method public static b(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lbvy;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1678
    invoke-static {p0}, Lf;->f(Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v0

    .line 1680
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1681
    const/4 v1, 0x0

    .line 1682
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v3

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1683
    packed-switch v2, :pswitch_data_0

    move-object v0, v1

    move v1, v2

    .line 1711
    :goto_1
    add-int/lit8 v2, v1, 0x1

    move-object v1, v0

    .line 1712
    goto :goto_0

    .line 1685
    :pswitch_0
    new-instance v1, Lbvy;

    invoke-direct {v1}, Lbvy;-><init>()V

    .line 1686
    iput-object v0, v1, Lbvy;->a:Ljava/lang/String;

    move-object v0, v1

    move v1, v2

    .line 1687
    goto :goto_1

    .line 1690
    :pswitch_1
    iput-object v0, v1, Lbvy;->b:Ljava/lang/String;

    move-object v0, v1

    move v1, v2

    .line 1691
    goto :goto_1

    .line 1695
    :pswitch_2
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v1, Lbvy;->c:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    move v1, v2

    .line 1698
    goto :goto_1

    .line 1697
    :catch_0
    move-exception v0

    iput v3, v1, Lbvy;->c:I

    move-object v0, v1

    move v1, v2

    .line 1699
    goto :goto_1

    .line 1703
    :pswitch_3
    :try_start_1
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v1, Lbvy;->d:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1707
    :goto_2
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1708
    const/4 v0, -0x1

    move-object v6, v1

    move v1, v0

    move-object v0, v6

    goto :goto_1

    .line 1705
    :catch_1
    move-exception v0

    iput v3, v1, Lbvy;->d:I

    goto :goto_2

    .line 1713
    :cond_0
    return-object v4

    .line 1683
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static b(Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 1885
    sput-object p0, Lbvx;->u:Ljava/lang/Boolean;

    .line 1886
    return-void
.end method

.method public static b()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 624
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    .line 625
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 626
    const-string v3, "smsmms"

    invoke-virtual {v1, v3, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 628
    sget v3, Lh;->ds:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 629
    sget v4, Lf;->bN:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    .line 630
    invoke-interface {v1, v3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 631
    invoke-static {}, Lbvm;->a()Lsm;

    move-result-object v2

    invoke-virtual {v2}, Lsm;->u()Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 633
    invoke-static {}, Lbzd;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static c(J)I
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v5, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1582
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1585
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s AND (%s<=%d)"

    new-array v3, v5, [Ljava/lang/Object;

    .line 1588
    invoke-static {}, Lbwf;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    const-string v4, "date"

    aput-object v4, v3, v7

    .line 1590
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v8

    .line 1585
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1591
    sget-object v2, Lbvx;->h:Landroid/net/Uri;

    invoke-virtual {v0, v2, v1, v9}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x0

    .line 1593
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%s AND (%s<=%d)"

    new-array v4, v5, [Ljava/lang/Object;

    .line 1596
    invoke-static {}, Lbwf;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    const-string v5, "date"

    aput-object v5, v4, v7

    const-wide/16 v5, 0x3e8

    div-long v5, p0, v5

    .line 1598
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v8

    .line 1593
    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1599
    sget-object v3, Lbvx;->e:Landroid/net/Uri;

    invoke-virtual {v0, v3, v2, v9}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    add-int/2addr v0, v1

    .line 1600
    return v0
.end method

.method public static c(Landroid/net/Uri;)J
    .locals 2

    .prologue
    .line 1381
    if-eqz p0, :cond_0

    .line 1382
    :try_start_0
    invoke-static {p0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v0

    .line 1387
    :goto_0
    return-wide v0

    .line 1386
    :catch_0
    move-exception v0

    .line 1387
    :cond_0
    :goto_1
    const-wide/16 v0, -0x1

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1088
    if-nez p0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    const/16 v0, 0xc

    const/16 v1, 0xa

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static c(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 877
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 878
    const-string v1, " "

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 879
    array-length v10, v9

    move v7, v0

    :goto_0
    if-ge v7, v10, :cond_3

    aget-object v0, v9, v7

    .line 883
    :try_start_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v11

    .line 884
    const-wide/16 v0, 0x0

    cmp-long v0, v11, v0

    if-gez v0, :cond_1

    .line 885
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MmsUtils.getAddresses: invalid id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 879
    :cond_0
    :goto_1
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 888
    :catch_0
    move-exception v0

    .line 889
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MmsUtils.getAddresses: invalid id. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 897
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lbvx;->n:Landroid/net/Uri;

    .line 898
    invoke-static {v1, v11, v12}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 897
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    move-object v1, v0

    .line 903
    :goto_2
    if-eqz v1, :cond_0

    .line 905
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 906
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 907
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 908
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 912
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 900
    :catch_1
    move-exception v0

    .line 901
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MmsUtils.getAddresses: query failed for id "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v6

    goto :goto_2

    .line 912
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 916
    :cond_3
    return-object v8
.end method

.method public static c()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1344
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    .line 1345
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 1346
    const-string v4, "smsmms"

    invoke-virtual {v0, v4, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1348
    sget v4, Lh;->cx:I

    .line 1349
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1348
    invoke-interface {v0, v4, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 1350
    if-eqz v4, :cond_1

    .line 1351
    sget v4, Lh;->cw:I

    .line 1352
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1351
    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 1353
    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v3, "phone"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 1357
    :goto_0
    return v0

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method public static d()I
    .locals 16

    .prologue
    const/4 v15, 0x3

    const/4 v14, 0x2

    const/4 v4, 0x0

    const/4 v13, 0x1

    const/4 v6, 0x0

    .line 1486
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s AND (%s IN (SELECT %s FROM part WHERE %s))"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    .line 1489
    invoke-static {}, Lbwf;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    const-string v3, "_id"

    aput-object v3, v2, v13

    const-string v3, "mid"

    aput-object v3, v2, v14

    const-string v3, "ct"

    .line 1492
    invoke-static {v3}, Lbvx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v15

    .line 1486
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1493
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1494
    sget-object v1, Lbvx;->e:Landroid/net/Uri;

    new-array v2, v13, [Ljava/lang/String;

    const-string v5, "_id"

    aput-object v5, v2, v6

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 1500
    if-eqz v3, :cond_5

    .line 1501
    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    new-array v7, v1, [J

    move v1, v6

    .line 1504
    :goto_0
    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1505
    add-int/lit8 v2, v1, 0x1

    const/4 v5, 0x0

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    aput-wide v8, v7, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v1, v2

    goto :goto_0

    .line 1508
    :cond_0
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 1510
    array-length v8, v7

    .line 1511
    if-lez v8, :cond_5

    move v5, v6

    move v2, v6

    .line 1514
    :goto_1
    if-ge v5, v8, :cond_4

    .line 1515
    add-int/lit16 v1, v5, 0x80

    invoke-static {v1, v8}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1516
    sub-int v9, v1, v5

    .line 1517
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%s IN %s"

    new-array v10, v14, [Ljava/lang/Object;

    const-string v11, "_id"

    aput-object v11, v10, v6

    .line 1521
    invoke-static {v9}, Lbvx;->b(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v13

    .line 1517
    invoke-static {v1, v3, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 1523
    if-gtz v9, :cond_3

    move-object v1, v4

    .line 1524
    :cond_1
    sget-object v3, Lbvx;->e:Landroid/net/Uri;

    invoke-virtual {v0, v3, v10, v1}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    .line 1528
    const-string v9, "Babel"

    invoke-static {v9, v15}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1529
    const-string v9, "Babel"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "deleteMediaMessages: deleting IDs = "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, ","

    .line 1530
    invoke-static {v11, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v10, ", deleted = "

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1529
    invoke-static {v9, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1533
    :cond_2
    add-int/2addr v2, v3

    .line 1514
    add-int/lit16 v1, v5, 0x80

    move v5, v1

    goto :goto_1

    .line 1508
    :catchall_0
    move-exception v0

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v0

    .line 1523
    :cond_3
    new-array v1, v9, [Ljava/lang/String;

    move v3, v6

    :goto_2
    if-ge v3, v9, :cond_1

    add-int v11, v5, v3

    aget-wide v11, v7, v11

    invoke-static {v11, v12}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_4
    move v0, v2

    .line 1537
    :goto_3
    return v0

    :cond_5
    move v0, v6

    goto :goto_3
.end method

.method public static d(Landroid/net/Uri;)V
    .locals 4

    .prologue
    .line 1657
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1658
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1659
    const-string v2, "seen"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1660
    const-string v2, "seen != 1"

    const/4 v3, 0x0

    invoke-virtual {v0, p0, v1, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1661
    return-void
.end method

.method public static e(Landroid/net/Uri;)Lbvj;
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 1936
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    .line 1937
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1942
    :try_start_0
    sget-object v2, Lbvj;->a:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p0

    invoke-static/range {v0 .. v5}, Lf;->a(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1946
    if-eqz v2, :cond_7

    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1947
    invoke-static {v2}, Lbvj;->a(Landroid/database/Cursor;)Lbvj;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v7

    .line 1952
    :goto_0
    if-eqz v2, :cond_0

    .line 1953
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1956
    :cond_0
    :goto_1
    if-nez v7, :cond_2

    move-object v0, v6

    .line 1987
    :goto_2
    return-object v0

    .line 1949
    :catch_0
    move-exception v1

    move-object v2, v6

    .line 1950
    :goto_3
    :try_start_2
    const-string v3, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "MmsLoader: query pdu failure: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1952
    if-eqz v2, :cond_6

    .line 1953
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    move-object v7, v6

    goto :goto_1

    .line 1952
    :catchall_0
    move-exception v0

    move-object v2, v6

    :goto_4
    if-eqz v2, :cond_1

    .line 1953
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 1961
    :cond_2
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s != \'%s\' AND %s = ?"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "ct"

    aput-object v4, v3, v9

    const-string v4, "application/smil"

    aput-object v4, v3, v8

    const/4 v4, 0x2

    const-string v5, "mid"

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1969
    :try_start_3
    sget-object v1, Lbvx;->b:Landroid/net/Uri;

    sget-object v2, Lbvk;->a:[Ljava/lang/String;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-wide v8, v7, Lbvj;->m:J

    .line 1973
    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v5

    const/4 v5, 0x0

    .line 1969
    invoke-static/range {v0 .. v5}, Lf;->a(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1975
    if-eqz v6, :cond_4

    .line 1976
    :goto_5
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1977
    const/4 v0, 0x1

    invoke-static {v6, v0}, Lbvk;->a(Landroid/database/Cursor;Z)Lbvk;

    move-result-object v0

    invoke-virtual {v7, v0}, Lbvj;->a(Lbvk;)V
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_5

    .line 1980
    :catch_1
    move-exception v0

    .line 1981
    :try_start_4
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MmsLoader: query parts failure: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1983
    if-eqz v6, :cond_3

    .line 1984
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    :goto_6
    move-object v0, v7

    .line 1987
    goto :goto_2

    .line 1983
    :cond_4
    if-eqz v6, :cond_3

    .line 1984
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_6

    .line 1983
    :catchall_1
    move-exception v0

    if-eqz v6, :cond_5

    .line 1984
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0

    .line 1952
    :catchall_2
    move-exception v0

    goto :goto_4

    .line 1949
    :catch_2
    move-exception v1

    goto/16 :goto_3

    :cond_6
    move-object v7, v6

    goto/16 :goto_1

    :cond_7
    move-object v7, v6

    goto/16 :goto_0
.end method

.method public static e()Z
    .locals 2

    .prologue
    .line 1756
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 1757
    invoke-static {}, Lbvx;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f()Lbdh;
    .locals 2

    .prologue
    .line 1794
    sget-object v0, Lbvx;->q:Lbdh;

    if-nez v0, :cond_0

    .line 1795
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1796
    sget v1, Lh;->hh:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1797
    const/4 v1, 0x0

    invoke-static {v0, v0, v1}, Lbdh;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lbdh;

    move-result-object v0

    sput-object v0, Lbvx;->q:Lbdh;

    .line 1800
    :cond_0
    sget-object v0, Lbvx;->q:Lbdh;

    return-object v0
.end method

.method public static g()Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 1815
    sget-object v0, Lbvx;->s:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 1818
    :try_start_0
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    .line 1819
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1820
    sget-object v1, Lbvx;->j:Landroid/net/Uri;

    sget-object v2, Lbvx;->r:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lf;->a(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1828
    const/4 v0, 0x1

    :try_start_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lbvx;->s:Ljava/lang/Boolean;
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1833
    if-eqz v1, :cond_0

    .line 1834
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1838
    :cond_0
    :goto_0
    sget-object v0, Lbvx;->s:Ljava/lang/Boolean;

    invoke-static {v0, v7}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    return v0

    .line 1829
    :catch_0
    move-exception v0

    move-object v1, v6

    .line 1830
    :goto_1
    :try_start_2
    const-string v2, "Babel"

    const-string v3, "Can\'t access system APN, using internal table"

    invoke-static {v2, v3, v0}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1831
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lbvx;->s:Ljava/lang/Boolean;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1833
    if-eqz v1, :cond_0

    .line 1834
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1833
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_2
    if-eqz v1, :cond_1

    .line 1834
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 1833
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 1829
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public static h()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1894
    sget-object v1, Lbvx;->t:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 1895
    sget v1, Lh;->cm:I

    sget v2, Lf;->bK:I

    invoke-static {v1, v2}, Lbvx;->a(II)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lbvx;->t:Ljava/lang/Boolean;

    .line 1898
    :cond_0
    invoke-static {}, Lf;->w()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lbvx;->t:Ljava/lang/Boolean;

    invoke-static {v1, v0}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public static i()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1907
    sget-object v1, Lbvx;->u:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 1908
    sget v1, Lh;->cl:I

    sget v2, Lf;->bJ:I

    invoke-static {v1, v2}, Lbvx;->a(II)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lbvx;->u:Ljava/lang/Boolean;

    .line 1911
    :cond_0
    invoke-static {}, Lf;->w()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lbvx;->u:Ljava/lang/Boolean;

    invoke-static {v1, v0}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method private static j()Z
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 1762
    sget-object v0, Lbvx;->p:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 1765
    :try_start_0
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    .line 1766
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1767
    sget-object v1, Lbvx;->h:Landroid/net/Uri;

    sget-object v2, Lbvx;->o:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "date_sent ASC LIMIT 1"

    invoke-static/range {v0 .. v5}, Lf;->a(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1775
    const/4 v0, 0x1

    :try_start_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lbvx;->p:Ljava/lang/Boolean;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1780
    if-eqz v1, :cond_0

    .line 1781
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1785
    :cond_0
    :goto_0
    sget-object v0, Lbvx;->p:Ljava/lang/Boolean;

    invoke-static {v0, v7}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    return v0

    .line 1776
    :catch_0
    move-exception v0

    move-object v1, v6

    .line 1777
    :goto_1
    :try_start_2
    const-string v2, "Babel"

    const-string v3, "date_sent in sms table does not exist"

    invoke-static {v2, v3, v0}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1778
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lbvx;->p:Ljava/lang/Boolean;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1780
    if-eqz v1, :cond_0

    .line 1781
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1780
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_2
    if-eqz v1, :cond_1

    .line 1781
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 1780
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 1776
    :catch_1
    move-exception v0

    goto :goto_1
.end method
