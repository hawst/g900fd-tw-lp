.class public final Laxh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Laod;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)V
    .locals 0

    .prologue
    .line 2403
    iput-object p1, p0, Laxh;->a:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 2474
    return-void
.end method

.method public a(I)V
    .locals 3

    .prologue
    .line 2458
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "callback from UberEditAudienceFragment; onConversationCreateFailed: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2462
    iget-object v0, p0, Laxh;->a:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->r()V

    .line 2463
    return-void
.end method

.method public a(Ljava/lang/String;Lyj;IZLxm;)V
    .locals 10

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 2411
    iget-object v1, p0, Laxh;->a:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->f(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)Lyj;

    move-result-object v1

    invoke-virtual {v1, p2}, Lyj;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2412
    iget-object v1, p0, Laxh;->a:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

    invoke-virtual {v1, p2, v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->a(Lyj;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2416
    iget-object v1, p0, Laxh;->a:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->n(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)Lbme;

    move-result-object v1

    invoke-interface {v1}, Lbme;->a()Laux;

    move-result-object v1

    const/16 v3, 0x618

    .line 2417
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 2416
    invoke-virtual {v1, v3}, Laux;->a(Ljava/lang/Integer;)V

    .line 2420
    :cond_0
    if-eqz p4, :cond_1

    .line 2422
    new-instance v0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-virtual {p2}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    const-string v3, "conversation"

    move-object v4, p1

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2427
    invoke-static {p5}, Lf;->a(Lxm;)Ljava/util/ArrayList;

    move-result-object v4

    .line 2432
    invoke-static {p5}, Lf;->b(Lxm;)Ljava/util/ArrayList;

    move-result-object v5

    .line 2436
    const/16 v7, 0x3d

    .line 2438
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    move-object v3, v0

    move v6, v2

    .line 2436
    invoke-static/range {v3 .. v9}, Lbbl;->a(Lcom/google/android/libraries/hangouts/video/HangoutRequest;Ljava/util/ArrayList;Ljava/util/ArrayList;ZIJ)Landroid/content/Intent;

    move-result-object v0

    .line 2439
    iget-object v1, p0, Laxh;->a:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 2440
    iget-object v0, p0, Laxh;->a:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->finish()V

    .line 2441
    iget-object v0, p0, Laxh;->a:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

    sget v1, Lf;->be:I

    sget v2, Lf;->bf:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->overridePendingTransition(II)V

    .line 2454
    :goto_0
    return-void

    .line 2443
    :cond_1
    new-instance v1, Lahc;

    invoke-direct {v1, p1, p2, p3}, Lahc;-><init>(Ljava/lang/String;Lyj;I)V

    .line 2448
    if-ne p3, v2, :cond_2

    :goto_1
    iput-boolean v2, v1, Lahc;->h:Z

    .line 2450
    iput-boolean v0, v1, Lahc;->b:Z

    .line 2451
    iput-boolean v0, v1, Lahc;->l:Z

    .line 2452
    iget-object v0, p0, Laxh;->a:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->a(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;Lahc;)V

    goto :goto_0

    :cond_2
    move v2, v0

    .line 2448
    goto :goto_1
.end method

.method public b()V
    .locals 2

    .prologue
    .line 2467
    const-string v0, "Babel"

    const-string v1, "callback from UberEditAudienceFragment; onFinish"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2469
    iget-object v0, p0, Laxh;->a:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->r()V

    .line 2470
    return-void
.end method

.method public c()Landroid/os/Bundle;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2478
    iget-object v0, p0, Laxh;->a:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

    .line 2479
    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->f(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)Lyj;

    move-result-object v0

    const/4 v4, 0x1

    .line 2481
    invoke-virtual {p0}, Laxh;->d()I

    move-result v5

    move-object v2, v1

    move-object v3, v1

    .line 2478
    invoke-static/range {v0 .. v5}, Lbbl;->a(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;II)Landroid/content/Intent;

    move-result-object v0

    .line 2482
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 2487
    iget-object v0, p0, Laxh;->a:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->f(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)Lyj;

    move-result-object v0

    invoke-static {v0}, Lbkb;->b(Lyj;)I

    move-result v0

    return v0
.end method

.method public k()Lyj;
    .locals 1

    .prologue
    .line 2492
    iget-object v0, p0, Laxh;->a:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->f(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)Lyj;

    move-result-object v0

    return-object v0
.end method

.method public z_()Z
    .locals 1

    .prologue
    .line 2497
    const/4 v0, 0x0

    return v0
.end method
