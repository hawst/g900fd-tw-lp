.class public Loc;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private A:Z

.field a:Landroid/widget/PopupWindow;

.field b:Lof;

.field c:I

.field final d:Lok;

.field e:Landroid/os/Handler;

.field private f:Landroid/content/Context;

.field private g:Landroid/widget/ListAdapter;

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Landroid/view/View;

.field private p:I

.field private q:Landroid/database/DataSetObserver;

.field private r:Landroid/view/View;

.field private s:Landroid/graphics/drawable/Drawable;

.field private t:Landroid/widget/AdapterView$OnItemClickListener;

.field private u:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private final v:Loj;

.field private final w:Loi;

.field private final x:Log;

.field private y:Ljava/lang/Runnable;

.field private z:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 167
    const/4 v0, 0x0

    sget v1, Lf;->o:I

    invoke-direct {p0, p1, v0, v1}, Loc;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 168
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v0, -0x2

    const/4 v1, 0x0

    .line 189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput v0, p0, Loc;->h:I

    .line 72
    iput v0, p0, Loc;->i:I

    .line 77
    iput-boolean v1, p0, Loc;->m:Z

    .line 78
    iput-boolean v1, p0, Loc;->n:Z

    .line 79
    const v0, 0x7fffffff

    iput v0, p0, Loc;->c:I

    .line 82
    iput v1, p0, Loc;->p:I

    .line 93
    new-instance v0, Lok;

    invoke-direct {v0, p0, v1}, Lok;-><init>(Loc;B)V

    iput-object v0, p0, Loc;->d:Lok;

    .line 94
    new-instance v0, Loj;

    invoke-direct {v0, p0, v1}, Loj;-><init>(Loc;B)V

    iput-object v0, p0, Loc;->v:Loj;

    .line 95
    new-instance v0, Loi;

    invoke-direct {v0, p0, v1}, Loi;-><init>(Loc;B)V

    iput-object v0, p0, Loc;->w:Loi;

    .line 96
    new-instance v0, Log;

    invoke-direct {v0, p0, v1}, Log;-><init>(Loc;B)V

    iput-object v0, p0, Loc;->x:Log;

    .line 99
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Loc;->e:Landroid/os/Handler;

    .line 101
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Loc;->z:Landroid/graphics/Rect;

    .line 190
    iput-object p1, p0, Loc;->f:Landroid/content/Context;

    .line 191
    new-instance v0, Landroid/widget/PopupWindow;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1, p3}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Loc;->a:Landroid/widget/PopupWindow;

    .line 192
    iget-object v0, p0, Loc;->a:Landroid/widget/PopupWindow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    .line 194
    iget-object v0, p0, Loc;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 196
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;IZ)I
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1089
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 1090
    invoke-virtual {p1, v1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 1092
    const/4 v0, 0x2

    new-array v2, v0, [I

    .line 1094
    invoke-virtual {p1, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1096
    iget v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 1097
    if-eqz p3, :cond_0

    .line 1098
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1099
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 1101
    :cond_0
    aget v3, v2, v5

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    sub-int/2addr v0, v3

    sub-int/2addr v0, p2

    .line 1102
    aget v2, v2, v5

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int v1, v2, v1

    add-int/2addr v1, p2

    .line 1105
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1106
    iget-object v1, p0, Loc;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1107
    iget-object v1, p0, Loc;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v2, p0, Loc;->z:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 1108
    iget-object v1, p0, Loc;->z:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Loc;->z:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    .line 1111
    :cond_1
    return v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 229
    const/4 v0, 0x0

    iput v0, p0, Loc;->p:I

    .line 230
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 393
    iput p1, p0, Loc;->j:I

    .line 394
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Loc;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 341
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 377
    iput-object p1, p0, Loc;->r:Landroid/view/View;

    .line 378
    return-void
.end method

.method public a(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 0

    .prologue
    .line 472
    iput-object p1, p0, Loc;->t:Landroid/widget/AdapterView$OnItemClickListener;

    .line 473
    return-void
.end method

.method public a(Landroid/widget/ListAdapter;)V
    .locals 2

    .prologue
    .line 205
    iget-object v0, p0, Loc;->q:Landroid/database/DataSetObserver;

    if-nez v0, :cond_3

    .line 206
    new-instance v0, Loh;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Loh;-><init>(Loc;B)V

    iput-object v0, p0, Loc;->q:Landroid/database/DataSetObserver;

    .line 210
    :cond_0
    :goto_0
    iput-object p1, p0, Loc;->g:Landroid/widget/ListAdapter;

    .line 211
    iget-object v0, p0, Loc;->g:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_1

    .line 212
    iget-object v0, p0, Loc;->q:Landroid/database/DataSetObserver;

    invoke-interface {p1, v0}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 215
    :cond_1
    iget-object v0, p0, Loc;->b:Lof;

    if-eqz v0, :cond_2

    .line 216
    iget-object v0, p0, Loc;->b:Lof;

    iget-object v1, p0, Loc;->g:Landroid/widget/ListAdapter;

    invoke-virtual {v0, v1}, Lof;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 218
    :cond_2
    return-void

    .line 207
    :cond_3
    iget-object v0, p0, Loc;->g:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Loc;->g:Landroid/widget/ListAdapter;

    iget-object v1, p0, Loc;->q:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    goto :goto_0
.end method

.method public a(Landroid/widget/PopupWindow$OnDismissListener;)V
    .locals 1

    .prologue
    .line 613
    iget-object v0, p0, Loc;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 614
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 250
    iput-boolean v1, p0, Loc;->A:Z

    .line 251
    iget-object v0, p0, Loc;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 252
    return-void
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 412
    iput p1, p0, Loc;->k:I

    .line 413
    const/4 v0, 0x1

    iput-boolean v0, p0, Loc;->l:Z

    .line 414
    return-void
.end method

.method public c()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Loc;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public c(I)V
    .locals 0

    .prologue
    .line 430
    iput p1, p0, Loc;->i:I

    .line 431
    return-void
.end method

.method public d()Landroid/view/View;
    .locals 1

    .prologue
    .line 367
    iget-object v0, p0, Loc;->r:Landroid/view/View;

    return-object v0
.end method

.method public d(I)V
    .locals 2

    .prologue
    .line 440
    iget-object v0, p0, Loc;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 441
    if-eqz v0, :cond_0

    .line 442
    iget-object v1, p0, Loc;->z:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 443
    iget-object v0, p0, Loc;->z:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Loc;->z:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    add-int/2addr v0, p1

    iput v0, p0, Loc;->i:I

    .line 447
    :goto_0
    return-void

    .line 445
    :cond_0
    invoke-virtual {p0, p1}, Loc;->c(I)V

    goto :goto_0
.end method

.method public e()V
    .locals 11

    .prologue
    const/high16 v10, -0x80000000

    const/4 v9, -0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v5, -0x1

    .line 514
    iget-object v0, p0, Loc;->b:Lof;

    if-nez v0, :cond_6

    iget-object v4, p0, Loc;->f:Landroid/content/Context;

    new-instance v0, Lod;

    invoke-direct {v0, p0}, Lod;-><init>(Loc;)V

    iput-object v0, p0, Loc;->y:Ljava/lang/Runnable;

    new-instance v3, Lof;

    iget-boolean v0, p0, Loc;->A:Z

    if-nez v0, :cond_5

    move v0, v1

    :goto_0
    invoke-direct {v3, v4, v0}, Lof;-><init>(Landroid/content/Context;Z)V

    iput-object v3, p0, Loc;->b:Lof;

    iget-object v0, p0, Loc;->s:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Loc;->b:Lof;

    iget-object v3, p0, Loc;->s:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v3}, Lof;->setSelector(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    iget-object v0, p0, Loc;->b:Lof;

    iget-object v3, p0, Loc;->g:Landroid/widget/ListAdapter;

    invoke-virtual {v0, v3}, Lof;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Loc;->b:Lof;

    iget-object v3, p0, Loc;->t:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v3}, Lof;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Loc;->b:Lof;

    invoke-virtual {v0, v1}, Lof;->setFocusable(Z)V

    iget-object v0, p0, Loc;->b:Lof;

    invoke-virtual {v0, v1}, Lof;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Loc;->b:Lof;

    new-instance v3, Loe;

    invoke-direct {v3, p0}, Loe;-><init>(Loc;)V

    invoke-virtual {v0, v3}, Lof;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Loc;->b:Lof;

    iget-object v3, p0, Loc;->w:Loi;

    invoke-virtual {v0, v3}, Lof;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    iget-object v0, p0, Loc;->u:Landroid/widget/AdapterView$OnItemSelectedListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Loc;->b:Lof;

    iget-object v3, p0, Loc;->u:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v0, v3}, Lof;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    :cond_1
    iget-object v0, p0, Loc;->b:Lof;

    iget-object v6, p0, Loc;->o:Landroid/view/View;

    if-eqz v6, :cond_1d

    new-instance v3, Landroid/widget/LinearLayout;

    invoke-direct {v3, v4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-direct {v4, v5, v2, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    iget v7, p0, Loc;->p:I

    packed-switch v7, :pswitch_data_0

    const-string v0, "ListPopupWindow"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "Invalid hint position "

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, p0, Loc;->p:I

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    iget v0, p0, Loc;->i:I

    invoke-static {v0, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v6, v0, v2}, Landroid/view/View;->measure(II)V

    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    iget v6, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    add-int/2addr v4, v6

    iget v0, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    add-int/2addr v0, v4

    :goto_2
    iget-object v4, p0, Loc;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v4, v3}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    :goto_3
    iget-object v3, p0, Loc;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v3}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    if-eqz v3, :cond_7

    iget-object v4, p0, Loc;->z:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    iget-object v3, p0, Loc;->z:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Loc;->z:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v3, v4

    iget-boolean v4, p0, Loc;->l:Z

    if-nez v4, :cond_2

    iget-object v4, p0, Loc;->z:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    neg-int v4, v4

    iput v4, p0, Loc;->k:I

    :cond_2
    :goto_4
    iget-object v4, p0, Loc;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v4}, Landroid/widget/PopupWindow;->getInputMethodMode()I

    move-result v4

    const/4 v6, 0x2

    if-ne v4, v6, :cond_8

    move v4, v1

    :goto_5
    invoke-virtual {p0}, Loc;->d()Landroid/view/View;

    move-result-object v6

    iget v7, p0, Loc;->k:I

    invoke-virtual {p0, v6, v7, v4}, Loc;->a(Landroid/view/View;IZ)I

    move-result v6

    iget-boolean v4, p0, Loc;->m:Z

    if-nez v4, :cond_3

    iget v4, p0, Loc;->h:I

    if-ne v4, v5, :cond_9

    :cond_3
    add-int v0, v6, v3

    .line 519
    :goto_6
    invoke-virtual {p0}, Loc;->j()Z

    move-result v6

    .line 521
    iget-object v3, p0, Loc;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v3}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_14

    .line 522
    iget v3, p0, Loc;->i:I

    if-ne v3, v5, :cond_b

    move v4, v5

    .line 532
    :goto_7
    iget v3, p0, Loc;->h:I

    if-ne v3, v5, :cond_11

    .line 535
    if-eqz v6, :cond_d

    move v3, v0

    .line 536
    :goto_8
    if-eqz v6, :cond_f

    .line 537
    iget-object v0, p0, Loc;->a:Landroid/widget/PopupWindow;

    iget v6, p0, Loc;->i:I

    if-ne v6, v5, :cond_e

    :goto_9
    invoke-virtual {v0, v5, v2}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    move v5, v3

    .line 552
    :goto_a
    iget-object v0, p0, Loc;->a:Landroid/widget/PopupWindow;

    iget-boolean v3, p0, Loc;->n:Z

    if-nez v3, :cond_13

    iget-boolean v3, p0, Loc;->m:Z

    if-nez v3, :cond_13

    :goto_b
    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 554
    iget-object v0, p0, Loc;->a:Landroid/widget/PopupWindow;

    invoke-virtual {p0}, Loc;->d()Landroid/view/View;

    move-result-object v1

    iget v2, p0, Loc;->j:I

    iget v3, p0, Loc;->k:I

    invoke-virtual/range {v0 .. v5}, Landroid/widget/PopupWindow;->update(Landroid/view/View;IIII)V

    .line 594
    :cond_4
    :goto_c
    return-void

    :cond_5
    move v0, v2

    .line 514
    goto/16 :goto_0

    :pswitch_0
    invoke-virtual {v3, v0, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_1

    :pswitch_1
    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v3, v0, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_1

    :cond_6
    iget-object v0, p0, Loc;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    iget-object v3, p0, Loc;->o:Landroid/view/View;

    if-eqz v3, :cond_1c

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    iget v4, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    add-int/2addr v3, v4

    iget v0, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    add-int/2addr v0, v3

    goto/16 :goto_3

    :cond_7
    iget-object v3, p0, Loc;->z:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->setEmpty()V

    move v3, v2

    goto/16 :goto_4

    :cond_8
    move v4, v2

    goto/16 :goto_5

    :cond_9
    iget v4, p0, Loc;->i:I

    packed-switch v4, :pswitch_data_1

    iget v4, p0, Loc;->i:I

    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v4, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    :goto_d
    iget-object v7, p0, Loc;->b:Lof;

    sub-int/2addr v6, v0

    invoke-virtual {v7, v4, v6}, Lof;->a(II)I

    move-result v4

    if-lez v4, :cond_a

    add-int/2addr v0, v3

    :cond_a
    add-int/2addr v0, v4

    goto/16 :goto_6

    :pswitch_2
    iget-object v4, p0, Loc;->f:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v7, p0, Loc;->z:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    iget-object v8, p0, Loc;->z:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->right:I

    add-int/2addr v7, v8

    sub-int/2addr v4, v7

    invoke-static {v4, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    goto :goto_d

    :pswitch_3
    iget-object v4, p0, Loc;->f:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v7, p0, Loc;->z:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    iget-object v8, p0, Loc;->z:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->right:I

    add-int/2addr v7, v8

    sub-int/2addr v4, v7

    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v4, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    goto :goto_d

    .line 526
    :cond_b
    iget v3, p0, Loc;->i:I

    if-ne v3, v9, :cond_c

    .line 527
    invoke-virtual {p0}, Loc;->d()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v4

    goto/16 :goto_7

    .line 529
    :cond_c
    iget v4, p0, Loc;->i:I

    goto/16 :goto_7

    :cond_d
    move v3, v5

    .line 535
    goto/16 :goto_8

    :cond_e
    move v5, v2

    .line 537
    goto/16 :goto_9

    .line 541
    :cond_f
    iget-object v6, p0, Loc;->a:Landroid/widget/PopupWindow;

    iget v0, p0, Loc;->i:I

    if-ne v0, v5, :cond_10

    move v0, v5

    :goto_e
    invoke-virtual {v6, v0, v5}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    move v5, v3

    goto/16 :goto_a

    :cond_10
    move v0, v2

    goto :goto_e

    .line 546
    :cond_11
    iget v3, p0, Loc;->h:I

    if-ne v3, v9, :cond_12

    move v5, v0

    .line 547
    goto/16 :goto_a

    .line 549
    :cond_12
    iget v5, p0, Loc;->h:I

    goto/16 :goto_a

    :cond_13
    move v1, v2

    .line 552
    goto/16 :goto_b

    .line 557
    :cond_14
    iget v3, p0, Loc;->i:I

    if-ne v3, v5, :cond_17

    move v3, v5

    .line 567
    :goto_f
    iget v4, p0, Loc;->h:I

    if-ne v4, v5, :cond_19

    move v0, v5

    .line 577
    :goto_10
    iget-object v4, p0, Loc;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v4, v3, v0}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    .line 581
    iget-object v0, p0, Loc;->a:Landroid/widget/PopupWindow;

    iget-boolean v3, p0, Loc;->n:Z

    if-nez v3, :cond_1b

    iget-boolean v3, p0, Loc;->m:Z

    if-nez v3, :cond_1b

    :goto_11
    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 582
    iget-object v0, p0, Loc;->a:Landroid/widget/PopupWindow;

    iget-object v1, p0, Loc;->v:Loj;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setTouchInterceptor(Landroid/view/View$OnTouchListener;)V

    .line 583
    iget-object v0, p0, Loc;->a:Landroid/widget/PopupWindow;

    invoke-virtual {p0}, Loc;->d()Landroid/view/View;

    move-result-object v1

    iget v2, p0, Loc;->j:I

    iget v3, p0, Loc;->k:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    .line 585
    iget-object v0, p0, Loc;->b:Lof;

    invoke-virtual {v0, v5}, Lof;->setSelection(I)V

    .line 587
    iget-boolean v0, p0, Loc;->A:Z

    if-eqz v0, :cond_15

    iget-object v0, p0, Loc;->b:Lof;

    invoke-virtual {v0}, Lof;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 588
    :cond_15
    invoke-virtual {p0}, Loc;->h()V

    .line 590
    :cond_16
    iget-boolean v0, p0, Loc;->A:Z

    if-nez v0, :cond_4

    .line 591
    iget-object v0, p0, Loc;->e:Landroid/os/Handler;

    iget-object v1, p0, Loc;->x:Log;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_c

    .line 560
    :cond_17
    iget v3, p0, Loc;->i:I

    if-ne v3, v9, :cond_18

    .line 561
    iget-object v3, p0, Loc;->a:Landroid/widget/PopupWindow;

    invoke-virtual {p0}, Loc;->d()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/PopupWindow;->setWidth(I)V

    move v3, v2

    goto :goto_f

    .line 563
    :cond_18
    iget-object v3, p0, Loc;->a:Landroid/widget/PopupWindow;

    iget v4, p0, Loc;->i:I

    invoke-virtual {v3, v4}, Landroid/widget/PopupWindow;->setWidth(I)V

    move v3, v2

    goto :goto_f

    .line 570
    :cond_19
    iget v4, p0, Loc;->h:I

    if-ne v4, v9, :cond_1a

    .line 571
    iget-object v4, p0, Loc;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v4, v0}, Landroid/widget/PopupWindow;->setHeight(I)V

    move v0, v2

    goto :goto_10

    .line 573
    :cond_1a
    iget-object v0, p0, Loc;->a:Landroid/widget/PopupWindow;

    iget v4, p0, Loc;->h:I

    invoke-virtual {v0, v4}, Landroid/widget/PopupWindow;->setHeight(I)V

    move v0, v2

    goto :goto_10

    :cond_1b
    move v1, v2

    .line 581
    goto :goto_11

    :cond_1c
    move v0, v2

    goto/16 :goto_3

    :cond_1d
    move-object v3, v0

    move v0, v2

    goto/16 :goto_2

    .line 514
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public e(I)V
    .locals 2

    .prologue
    .line 656
    iget-object v0, p0, Loc;->b:Lof;

    .line 657
    invoke-virtual {p0}, Loc;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 658
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lof;->a(Lof;Z)Z

    .line 659
    invoke-virtual {v0, p1}, Lof;->setSelection(I)V

    .line 660
    invoke-virtual {v0}, Lof;->getChoiceMode()I

    move-result v1

    if-eqz v1, :cond_0

    .line 661
    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lof;->setItemChecked(IZ)V

    .line 664
    :cond_0
    return-void
.end method

.method public f()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 600
    iget-object v0, p0, Loc;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 601
    iget-object v0, p0, Loc;->o:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Loc;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Loc;->o:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 602
    :cond_0
    iget-object v0, p0, Loc;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v2}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 603
    iput-object v2, p0, Loc;->b:Lof;

    .line 604
    iget-object v0, p0, Loc;->e:Landroid/os/Handler;

    iget-object v1, p0, Loc;->d:Lok;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 605
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 638
    iget-object v0, p0, Loc;->a:Landroid/widget/PopupWindow;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    .line 639
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    .line 670
    iget-object v0, p0, Loc;->b:Lof;

    .line 671
    if-eqz v0, :cond_0

    .line 673
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lof;->a(Lof;Z)Z

    .line 675
    invoke-virtual {v0}, Lof;->requestLayout()V

    .line 677
    :cond_0
    return-void
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 683
    iget-object v0, p0, Loc;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    return v0
.end method

.method public j()Z
    .locals 2

    .prologue
    .line 691
    iget-object v0, p0, Loc;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getInputMethodMode()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 765
    iget-object v0, p0, Loc;->b:Lof;

    return-object v0
.end method
