.class public final Lbku;
.super Lbqr;
.source "PG"


# instance fields
.field final a:Ljava/lang/String;

.field final b:J

.field final c:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lbje;)V
    .locals 2

    .prologue
    .line 136
    invoke-direct {p0}, Lbqr;-><init>()V

    .line 137
    iget-object v0, p1, Lbje;->b:Ljava/lang/String;

    iput-object v0, p0, Lbku;->a:Ljava/lang/String;

    .line 138
    iget-wide v0, p1, Lbje;->c:J

    iput-wide v0, p0, Lbku;->b:J

    .line 139
    iget-object v0, p1, Lbje;->d:[Ljava/lang/String;

    iput-object v0, p0, Lbku;->c:[Ljava/lang/String;

    .line 140
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;J[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 150
    invoke-direct {p0}, Lbqr;-><init>()V

    .line 151
    iput-object p1, p0, Lbku;->a:Ljava/lang/String;

    .line 152
    iput-wide p2, p0, Lbku;->b:J

    .line 153
    iput-object p4, p0, Lbku;->c:[Ljava/lang/String;

    .line 154
    return-void
.end method


# virtual methods
.method public a(Lbnl;)V
    .locals 5

    .prologue
    .line 161
    new-instance v0, Lbef;

    iget-object v1, p0, Lbku;->a:Ljava/lang/String;

    iget-wide v2, p0, Lbku;->b:J

    iget-object v4, p0, Lbku;->c:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lbef;-><init>(Ljava/lang/String;J[Ljava/lang/String;)V

    .line 163
    invoke-virtual {p1, v0}, Lbnl;->a(Lbea;)V

    .line 164
    return-void
.end method

.method public a(Lyt;)V
    .locals 5

    .prologue
    .line 170
    iget-object v0, p0, Lbku;->c:[Ljava/lang/String;

    if-nez v0, :cond_1

    .line 172
    iget-object v0, p0, Lbku;->a:Ljava/lang/String;

    iget-wide v1, p0, Lbku;->b:J

    invoke-virtual {p1, v0, v1, v2}, Lyt;->e(Ljava/lang/String;J)Z

    .line 173
    invoke-static {p1}, Lyp;->d(Lyt;)V

    .line 184
    :cond_0
    iget-object v0, p0, Lbku;->a:Ljava/lang/String;

    invoke-static {p1, v0}, Lyp;->d(Lyt;Ljava/lang/String;)V

    .line 185
    return-void

    .line 176
    :cond_1
    iget-object v1, p0, Lbku;->c:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 177
    iget-object v4, p0, Lbku;->a:Ljava/lang/String;

    invoke-virtual {p1, v4, v3}, Lyt;->h(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    .line 178
    if-eqz v3, :cond_2

    .line 180
    invoke-static {v3}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v3

    .line 179
    invoke-static {p1, v3, v4}, Lyp;->a(Lyt;J)V

    .line 176
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
