.class public Lbgs;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbzo",
            "<",
            "Lbcn;",
            "[",
            "Lbdh;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ldsn;)V
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 2344
    iget-object v0, p1, Ldsn;->b:Ldvn;

    const-wide/16 v4, -0x1

    invoke-direct {p0, v0, v4, v5}, Lbfz;-><init>(Ldvn;J)V

    .line 2346
    iget-object v6, p1, Ldsn;->d:[Ldso;

    if-nez v6, :cond_2

    :goto_0
    iput-object v1, p0, Lbgs;->g:Ljava/util/List;

    .line 2347
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_a

    .line 2348
    iget-object v0, p0, Lbgs;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 2349
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "GetEntityByIdResponse: Number of specs="

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2351
    iget-object v0, p0, Lbgs;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzo;

    .line 2352
    if-eqz v0, :cond_1

    iget-object v1, v0, Lbzo;->b:Ljava/io/Serializable;

    if-nez v1, :cond_7

    .line 2353
    :cond_1
    const-string v0, "Babel"

    const-string v1, "- GetEntityByIdResponse: Invalid Entry."

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2346
    :cond_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    array-length v7, v6

    move v5, v3

    :goto_2
    if-ge v5, v7, :cond_6

    aget-object v0, v6, v5

    iget-object v2, v0, Ldso;->b:Ldrp;

    invoke-static {v2}, Lbcn;->a(Ldrp;)Lbcn;

    move-result-object v8

    iget-object v0, v0, Ldso;->c:[Ldro;

    invoke-static {v0, v8}, Lbdh;->a([Ldro;Lbcn;)[Lbdh;

    move-result-object v9

    if-eqz v9, :cond_5

    iget-object v0, v8, Lbcn;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, v8, Lbcn;->c:Ljava/lang/String;

    :goto_3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    move v2, v3

    :goto_4
    array-length v10, v9

    if-ge v2, v10, :cond_5

    aget-object v10, v9, v2

    if-eqz v10, :cond_3

    aget-object v10, v9, v2

    invoke-virtual {v10, v0}, Lbdh;->b(Ljava/lang/String;)V

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_4
    iget-object v0, v8, Lbcn;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    iget-object v0, v8, Lbcn;->d:Ljava/lang/String;

    goto :goto_3

    :cond_5
    new-instance v0, Lbzo;

    invoke-direct {v0, v8, v9}, Lbzo;-><init>(Ljava/io/Serializable;Ljava/io/Serializable;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_2

    :cond_6
    move-object v1, v4

    goto/16 :goto_0

    .line 2356
    :cond_7
    iget-object v1, v0, Lbzo;->b:Ljava/io/Serializable;

    if-nez v1, :cond_8

    move v1, v3

    .line 2357
    :goto_5
    const-string v4, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "- GetEntityByIdResponse: Number of entities="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2358
    iget-object v0, v0, Lbzo;->b:Ljava/io/Serializable;

    check-cast v0, [Lbdh;

    array-length v4, v0

    move v1, v3

    :goto_6
    if-ge v1, v4, :cond_0

    aget-object v5, v0, v1

    .line 2359
    if-nez v5, :cond_9

    .line 2360
    const-string v5, "Babel"

    const-string v6, "-- entity: null"

    invoke-static {v5, v6}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2358
    :goto_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 2356
    :cond_8
    iget-object v1, v0, Lbzo;->b:Ljava/io/Serializable;

    check-cast v1, [Lbdh;

    array-length v1, v1

    goto :goto_5

    .line 2362
    :cond_9
    const-string v6, "Babel"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "-- entity: displayName "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, v5, Lbdh;->e:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ",firstName "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v5, Lbdh;->f:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ",avatarUrl "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v5, Lbdh;->h:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ",participantId "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v5, Lbdh;->b:Lbdk;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ",circleId "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v5, v5, Lbdh;->d:Ljava/lang/String;

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7

    .line 2371
    :cond_a
    return-void

    :cond_b
    move-object v0, v1

    goto/16 :goto_3
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 2395
    new-instance v0, Ldsn;

    invoke-direct {v0}, Ldsn;-><init>()V

    .line 2396
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldsn;

    .line 2397
    iget-object v1, v0, Ldsn;->b:Ldvn;

    invoke-static {v1}, Lbgs;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2398
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldsn;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 2400
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbgs;

    invoke-direct {v1, v0}, Lbgs;-><init>(Ldsn;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Lyt;Lbnl;)V
    .locals 3

    .prologue
    .line 2376
    invoke-super {p0, p1, p2}, Lbfz;->a(Lyt;Lbnl;)V

    .line 2378
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2379
    iget-object v0, p0, Lbgs;->b:Lbea;

    if-eqz v0, :cond_2

    .line 2380
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GEBI Response for: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbgs;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " tag."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2388
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lbgs;->h()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lbgs;->g:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 2389
    invoke-virtual {p1}, Lyt;->f()Lyj;

    move-result-object v0

    invoke-static {v0, p0}, Lbrf;->a(Lyj;Lbgs;)V

    .line 2391
    :cond_1
    return-void

    .line 2382
    :cond_2
    const-string v0, "Babel"

    const-string v1, "GEBI Response with null request!"

    invoke-static {v0, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public f()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbzo",
            "<",
            "Lbcn;",
            "[",
            "Lbdh;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2405
    iget-object v0, p0, Lbgs;->g:Ljava/util/List;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2409
    iget-object v0, p0, Lbgs;->b:Lbea;

    check-cast v0, Lbel;

    iget-object v0, v0, Lbel;->b:Ljava/lang/String;

    return-object v0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 2413
    iget-object v0, p0, Lbgs;->b:Lbea;

    check-cast v0, Lbel;

    iget-boolean v0, v0, Lbel;->h:Z

    return v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 2417
    iget-object v0, p0, Lbgs;->b:Lbea;

    check-cast v0, Lbel;

    iget-boolean v0, v0, Lbel;->c:Z

    return v0
.end method
