.class public final Laad;
.super Lbai;
.source "PG"


# static fields
.field public static final j:Z

.field public static k:J


# instance fields
.field private final l:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

.field private final m:Lccx;

.field private final n:Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;

.field private o:Laaf;

.field private p:I

.field private q:Z

.field private r:Z

.field private s:J

.field private final t:Lcdh;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 50
    sget-object v0, Lbys;->c:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Laad;->j:Z

    .line 66
    const-wide/16 v0, -0x1

    sput-wide v0, Laad;->k:J

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Landroid/widget/AbsListView;Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;IZ)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 140
    invoke-virtual {p1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v0

    invoke-direct {p0, v0}, Lbai;-><init>(Landroid/content/Context;)V

    .line 71
    iput-boolean v4, p0, Laad;->r:Z

    .line 74
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Laad;->s:J

    .line 84
    new-instance v0, Laag;

    invoke-direct {v0, v4}, Laag;-><init>(B)V

    iput-object v0, p0, Laad;->t:Lcdh;

    .line 142
    sget-wide v0, Laad;->k:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 144
    invoke-virtual {p1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "babel_message_block_max_time_diff_ms"

    const-wide/32 v2, 0x2bf20

    .line 143
    invoke-static {v0, v1, v2, v3}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    sput-wide v0, Laad;->k:J

    .line 149
    :cond_0
    iput-object p1, p0, Laad;->l:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 151
    iput-boolean v4, p0, Laad;->q:Z

    .line 153
    iput p4, p0, Laad;->p:I

    .line 155
    new-instance v0, Lccx;

    invoke-direct {v0, p5}, Lccx;-><init>(Z)V

    iput-object v0, p0, Laad;->m:Lccx;

    .line 156
    iput-object p3, p0, Laad;->n:Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;

    .line 157
    iget-object v0, p0, Laad;->n:Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;

    iget-object v1, p0, Laad;->m:Lccx;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->a(Lccx;)V

    .line 159
    new-instance v0, Laae;

    invoke-direct {v0, p0}, Laae;-><init>(Laad;)V

    invoke-virtual {p2, v0}, Landroid/widget/AbsListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    .line 170
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 173
    iput p1, p0, Laad;->p:I

    .line 174
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 719
    iget-object v0, p0, Laad;->m:Lccx;

    invoke-virtual {v0, p1, p2}, Lccx;->a(J)V

    .line 720
    return-void
.end method

.method public a(Laaf;)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Laad;->o:Laaf;

    .line 182
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 706
    iget-object v0, p0, Laad;->m:Lccx;

    invoke-virtual {v0, p1}, Lccx;->a(Z)V

    .line 707
    return-void
.end method

.method public b(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 485
    iget-object v0, p0, Laad;->m:Lccx;

    invoke-virtual {v0, p1}, Lccx;->a(Landroid/database/Cursor;)V

    .line 486
    invoke-super {p0, p1}, Lbai;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 715
    iput-boolean p1, p0, Laad;->q:Z

    .line 716
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 469
    invoke-virtual {p0}, Laad;->d_()V

    .line 470
    return-void
.end method

.method public d(Landroid/database/Cursor;)Z
    .locals 1

    .prologue
    .line 516
    invoke-virtual {p0, p1}, Laad;->a(Landroid/database/Cursor;)V

    .line 517
    iget-object v0, p0, Laad;->m:Lccx;

    invoke-virtual {v0}, Lccx;->c()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d_()V
    .locals 6

    .prologue
    const-wide v4, 0x7fffffffffffffffL

    .line 493
    iget-object v0, p0, Laad;->l:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a()Lyj;

    move-result-object v0

    .line 494
    iget-object v1, p0, Laad;->l:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->N()Ljava/lang/String;

    move-result-object v1

    .line 496
    iget-wide v2, p0, Laad;->s:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    .line 497
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 498
    const-string v0, "Babel"

    const-string v1, "Update message scroll time"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    iget-object v0, p0, Laad;->l:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a()Lyj;

    move-result-object v0

    iget-object v1, p0, Laad;->l:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 501
    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->N()Ljava/lang/String;

    move-result-object v1

    iget-wide v2, p0, Laad;->s:J

    .line 500
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lyj;Ljava/lang/String;J)I

    .line 503
    iput-wide v4, p0, Laad;->s:J

    .line 507
    :goto_0
    return-void

    .line 505
    :cond_0
    const-string v0, "Babel"

    const-string v1, "Skip updating message scroll time"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 521
    iget-object v0, p0, Laad;->m:Lccx;

    invoke-virtual {v0}, Lccx;->a()V

    .line 522
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 525
    iget-object v0, p0, Laad;->m:Lccx;

    invoke-virtual {v0}, Lccx;->b()V

    .line 526
    return-void
.end method

.method public getItemViewType(I)I
    .locals 2

    .prologue
    .line 693
    iget-object v0, p0, Laad;->c:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 694
    iget-object v0, p0, Laad;->c:Landroid/database/Cursor;

    const/16 v1, 0x8

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 22

    .prologue
    .line 189
    move-object/from16 v0, p0

    iget-boolean v2, v0, Laad;->a:Z

    if-nez v2, :cond_0

    .line 190
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "this should only be called when the cursor is valid"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 192
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Laad;->c:Landroid/database/Cursor;

    move/from16 v0, p1

    invoke-interface {v2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 193
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "couldn\'t move cursor to position "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 198
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Laad;->c:Landroid/database/Cursor;

    const/16 v3, 0xa

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 199
    move-object/from16 v0, p0

    iget-object v2, v0, Laad;->c:Landroid/database/Cursor;

    const/16 v4, 0x9

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 201
    :try_start_0
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 202
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    .line 204
    :cond_2
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 205
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 213
    :cond_3
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Laad;->c:Landroid/database/Cursor;

    const/4 v3, 0x6

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-wide v4, v0, Laad;->s:J

    cmp-long v4, v4, v2

    if-lez v4, :cond_4

    move-object/from16 v0, p0

    iput-wide v2, v0, Laad;->s:J

    .line 215
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Laad;->d:Landroid/content/Context;

    const-string v3, "layout_inflater"

    .line 216
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 217
    move-object/from16 v0, p0

    iget-object v3, v0, Laad;->c:Landroid/database/Cursor;

    const/16 v4, 0x8

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 219
    if-eqz p2, :cond_c

    .line 221
    check-cast p2, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;

    .line 230
    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Laad;->l:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    if-eqz v3, :cond_d

    move-object/from16 v0, p0

    iget-object v3, v0, Laad;->l:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->O()Z

    move-result v3

    if-eqz v3, :cond_d

    .line 231
    const/4 v3, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->b(Z)V

    .line 236
    :goto_2
    invoke-virtual/range {p0 .. p1}, Laad;->getItemId(I)J

    move-result-wide v3

    .line 237
    move-object/from16 v0, p0

    iget-object v5, v0, Laad;->m:Lccx;

    invoke-virtual {v5}, Lccx;->c()Ljava/lang/Long;

    move-result-object v5

    .line 238
    move-object/from16 v0, p0

    iget-object v6, v0, Laad;->m:Lccx;

    invoke-virtual {v6, v3, v4}, Lccx;->c(J)Z

    move-result v6

    .line 240
    if-eqz v5, :cond_e

    .line 242
    invoke-static {v5}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v7

    cmp-long v7, v3, v7

    if-nez v7, :cond_e

    .line 243
    const/4 v3, 0x1

    move v8, v3

    .line 263
    :goto_3
    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->a(I)V

    .line 265
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->b()Lcdg;

    move-result-object v3

    .line 267
    const/4 v4, 0x2

    if-eq v9, v4, :cond_5

    const/4 v4, 0x1

    if-ne v9, v4, :cond_15

    .line 269
    :cond_5
    if-eqz v3, :cond_12

    .line 271
    invoke-interface {v3}, Lcdg;->b()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/hangouts/views/MessageListItemView;

    .line 282
    :cond_6
    :goto_4
    const/4 v3, 0x1

    if-ne v9, v3, :cond_7

    .line 283
    move-object/from16 v0, p0

    iget-object v3, v0, Laad;->l:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a()Lyj;

    move-result-object v3

    invoke-virtual {v3}, Lyj;->F()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->a(Ljava/lang/String;)V

    .line 286
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Laad;->c:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget v4, v0, Laad;->p:I

    move-object/from16 v0, p0

    iget-object v5, v0, Laad;->l:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v5}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a()Lyj;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Laad;->l:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    move-object/from16 v0, p0

    iget-boolean v7, v0, Laad;->q:Z

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->a(Landroid/database/Cursor;ILyj;Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Z)V

    move-object/from16 v0, p0

    iget-object v3, v0, Laad;->c:Landroid/database/Cursor;

    const/4 v4, 0x7

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    int-to-long v4, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Laad;->c:Landroid/database/Cursor;

    const/4 v6, 0x0

    invoke-interface {v3, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v3, v0, Laad;->l:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v3, v6, v7}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(J)Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->setSelected(Z)V

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Laad;->c:Landroid/database/Cursor;

    const/4 v7, 0x6

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v10, v0, Laad;->c:Landroid/database/Cursor;

    const/4 v11, 0x4

    invoke-interface {v10, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Laad;->c:Landroid/database/Cursor;

    const/4 v12, 0x3

    invoke-interface {v11, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Laad;->c:Landroid/database/Cursor;

    const/16 v13, 0x22

    invoke-interface {v12, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    move-object/from16 v0, p0

    iget-object v13, v0, Laad;->c:Landroid/database/Cursor;

    invoke-interface {v13}, Landroid/database/Cursor;->isLast()Z

    move-result v13

    if-nez v13, :cond_9

    move-object/from16 v0, p0

    iget-object v13, v0, Laad;->c:Landroid/database/Cursor;

    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v13

    if-eqz v13, :cond_9

    move-object/from16 v0, p0

    iget-object v13, v0, Laad;->c:Landroid/database/Cursor;

    const/4 v14, 0x6

    invoke-interface {v13, v14}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v13

    move-object/from16 v0, p0

    iget-object v15, v0, Laad;->c:Landroid/database/Cursor;

    const/16 v16, 0x4

    invoke-interface/range {v15 .. v16}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Laad;->c:Landroid/database/Cursor;

    move-object/from16 v16, v0

    const/16 v17, 0x3

    invoke-interface/range {v16 .. v17}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Laad;->c:Landroid/database/Cursor;

    move-object/from16 v17, v0

    const/16 v18, 0x8

    invoke-interface/range {v17 .. v18}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    move-object/from16 v0, p0

    iget-object v0, v0, Laad;->c:Landroid/database/Cursor;

    move-object/from16 v18, v0

    const/16 v19, 0x7

    invoke-interface/range {v18 .. v19}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Laad;->c:Landroid/database/Cursor;

    move-object/from16 v20, v0

    const/16 v21, 0x22

    invoke-interface/range {v20 .. v21}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    sub-long v6, v13, v6

    sget-wide v13, Laad;->k:J

    cmp-long v6, v6, v13

    if-gez v6, :cond_8

    move/from16 v0, v20

    if-ne v0, v12, :cond_8

    move-object/from16 v0, v16

    invoke-static {v11, v10, v0, v15}, Lbdk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_8

    const-wide/16 v6, 0x4

    cmp-long v4, v4, v6

    if-nez v4, :cond_8

    const-wide/16 v4, 0x3

    cmp-long v4, v18, v4

    if-eqz v4, :cond_8

    const-wide/16 v4, 0x1

    cmp-long v4, v18, v4

    if-eqz v4, :cond_8

    move/from16 v0, v17

    if-ne v0, v9, :cond_8

    const/4 v3, 0x1

    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Laad;->c:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->moveToPrevious()Z

    :cond_9
    packed-switch v8, :pswitch_data_0

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->b(Z)V

    const/4 v3, 0x0

    :cond_a
    :goto_5
    invoke-virtual {v2, v3}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->a(Z)V

    .line 287
    invoke-virtual {v2}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->requestLayout()V

    .line 324
    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, Laad;->c:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v3, v0, Laad;->l:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    move-object/from16 v0, p0

    iget v4, v0, Laad;->p:I

    move-object/from16 v0, p0

    iget-object v5, v0, Laad;->l:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 325
    invoke-virtual {v5}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->N()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Laas;->a(Ljava/lang/String;)Laat;

    move-result-object v5

    .line 324
    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->a(Landroid/database/Cursor;Laah;ILaat;)V

    .line 326
    move-object/from16 v0, p0

    iget-boolean v2, v0, Laad;->r:Z

    if-eqz v2, :cond_b

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_b

    .line 327
    const/4 v2, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 329
    :cond_b
    return-object p2

    .line 207
    :catch_0
    move-exception v2

    .line 208
    const-string v5, "Babel"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, " Read message info with malformed url: remote="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " local="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3, v2}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 223
    :cond_c
    sget v3, Lf;->fV:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;

    .line 225
    move-object/from16 v0, p0

    iget-object v4, v0, Laad;->n:Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->a(Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;)V

    .line 226
    move-object/from16 v0, p0

    iget-object v4, v0, Laad;->t:Lcdh;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->a(Lcdh;)V

    move-object/from16 p2, v3

    goto/16 :goto_1

    .line 233
    :cond_d
    const/4 v3, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->b(Z)V

    goto/16 :goto_2

    .line 244
    :cond_e
    move-object/from16 v0, p0

    iget-object v7, v0, Laad;->m:Lccx;

    invoke-virtual {v7, v3, v4}, Lccx;->b(J)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 245
    const/4 v3, 0x0

    move v8, v3

    goto/16 :goto_3

    .line 246
    :cond_f
    if-eqz v6, :cond_11

    .line 247
    if-eqz v5, :cond_10

    .line 253
    const/4 v3, 0x3

    move v8, v3

    goto/16 :goto_3

    .line 258
    :cond_10
    const/4 v3, 0x2

    move v8, v3

    goto/16 :goto_3

    .line 261
    :cond_11
    const/4 v3, 0x4

    move v8, v3

    goto/16 :goto_3

    .line 273
    :cond_12
    invoke-virtual/range {p0 .. p1}, Laad;->getItemViewType(I)I

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_13

    const/4 v4, 0x2

    if-ne v3, v4, :cond_14

    :cond_13
    sget v3, Lf;->fU:I

    .line 274
    :goto_7
    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/hangouts/views/MessageListItemView;

    .line 275
    move-object/from16 v0, p0

    iget-object v3, v0, Laad;->l:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->a(Lccu;)V

    .line 276
    move-object/from16 v0, p0

    iget-object v3, v0, Laad;->l:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    .line 277
    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->a(Lcdg;)V

    .line 278
    move-object/from16 v0, p0

    iget-object v3, v0, Laad;->o:Laaf;

    if-eqz v3, :cond_6

    .line 279
    move-object/from16 v0, p0

    iget-object v3, v0, Laad;->o:Laaf;

    invoke-interface {v3, v2}, Laaf;->a(Lcom/google/android/apps/hangouts/views/MessageListItemView;)V

    goto/16 :goto_4

    .line 273
    :cond_14
    const/4 v3, -0x1

    goto :goto_7

    .line 286
    :pswitch_0
    invoke-virtual {v2, v3}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->b(Z)V

    goto/16 :goto_5

    :pswitch_1
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->b(Z)V

    if-eqz v3, :cond_a

    invoke-virtual {v2}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->t()V

    goto/16 :goto_5

    .line 288
    :cond_15
    const/4 v4, 0x7

    if-eq v9, v4, :cond_16

    const/16 v4, 0x8

    if-ne v9, v4, :cond_18

    .line 290
    :cond_16
    if-eqz v3, :cond_17

    .line 293
    invoke-interface {v3}, Lcdg;->b()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;

    move-object v11, v2

    .line 299
    :goto_8
    move-object/from16 v0, p0

    iget-object v2, v0, Laad;->c:Landroid/database/Cursor;

    const/4 v3, 0x6

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long v12, v2, v4

    move-object/from16 v0, p0

    iget-object v2, v0, Laad;->c:Landroid/database/Cursor;

    const/16 v3, 0x8

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Laad;->c:Landroid/database/Cursor;

    const/16 v4, 0x21

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v3, v0, Laad;->c:Landroid/database/Cursor;

    const/16 v4, 0x24

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    move-object/from16 v0, p0

    iget-object v3, v0, Laad;->l:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a()Lyj;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Laad;->c:Landroid/database/Cursor;

    const/4 v5, 0x7

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Laad;->c:Landroid/database/Cursor;

    const/4 v6, 0x4

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Laad;->c:Landroid/database/Cursor;

    const/4 v7, 0x3

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Laad;->c:Landroid/database/Cursor;

    const/16 v10, 0x20

    invoke-interface {v7, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lf;->a(ILyj;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v2, v0, Laad;->l:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a()Lyj;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v8, v3}, Lf;->a(Lyj;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v7

    const/4 v2, 0x0

    invoke-static {v12, v13, v2}, Lf;->a(JZ)Ljava/lang/CharSequence;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v2, v0, Laad;->l:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a()Lyj;

    move-result-object v6

    move-object/from16 v0, p0

    iget v8, v0, Laad;->p:I

    move-object v3, v11

    invoke-virtual/range {v3 .. v9}, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lyj;Ljava/util/List;II)V

    invoke-virtual {v11, v12, v13}, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->a(J)V

    goto/16 :goto_6

    .line 295
    :cond_17
    sget v3, Lf;->fp:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;

    .line 297
    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->a(Lcdg;)V

    move-object v11, v2

    goto/16 :goto_8

    .line 300
    :cond_18
    const/16 v4, 0x9

    if-eq v9, v4, :cond_19

    const/16 v4, 0xa

    if-ne v9, v4, :cond_1c

    .line 302
    :cond_19
    if-eqz v3, :cond_1a

    .line 305
    invoke-interface {v3}, Lcdg;->b()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/hangouts/views/OtrModificationMessageListItemView;

    .line 311
    :goto_9
    move-object/from16 v0, p0

    iget-object v3, v0, Laad;->c:Landroid/database/Cursor;

    const/4 v4, 0x6

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    div-long v4, v3, v5

    move-object/from16 v0, p0

    iget-object v3, v0, Laad;->c:Landroid/database/Cursor;

    const/16 v6, 0x8

    invoke-interface {v3, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/16 v6, 0x9

    if-ne v3, v6, :cond_1b

    const/4 v3, 0x1

    :goto_a
    new-instance v6, Lbdk;

    move-object/from16 v0, p0

    iget-object v7, v0, Laad;->c:Landroid/database/Cursor;

    const/4 v8, 0x4

    invoke-interface {v7, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Laad;->c:Landroid/database/Cursor;

    const/4 v9, 0x3

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lbdk;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v7, v0, Laad;->l:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v7, v6}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Lbdk;)Z

    move-result v7

    move-object/from16 v0, p0

    iget-object v8, v0, Laad;->c:Landroid/database/Cursor;

    const/4 v9, 0x7

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, Laad;->l:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v9, v6}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->e(Lbdk;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v3, v8, v6, v7}, Lcom/google/android/apps/hangouts/views/OtrModificationMessageListItemView;->a(IILjava/lang/String;Z)V

    invoke-virtual {v2, v4, v5}, Lcom/google/android/apps/hangouts/views/OtrModificationMessageListItemView;->a(J)V

    invoke-virtual {v2}, Lcom/google/android/apps/hangouts/views/OtrModificationMessageListItemView;->c()V

    goto/16 :goto_6

    .line 307
    :cond_1a
    sget v3, Lf;->ge:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/hangouts/views/OtrModificationMessageListItemView;

    .line 309
    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->a(Lcdg;)V

    goto :goto_9

    .line 311
    :cond_1b
    const/4 v3, 0x2

    goto :goto_a

    .line 313
    :cond_1c
    if-eqz v3, :cond_1e

    .line 315
    invoke-interface {v3}, Lcdg;->b()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/hangouts/views/SystemMessageListItemView;

    move-object v11, v2

    .line 321
    :goto_b
    move-object/from16 v0, p0

    iget-object v2, v0, Laad;->c:Landroid/database/Cursor;

    const/4 v3, 0x6

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long v12, v2, v4

    move-object/from16 v0, p0

    iget-object v2, v0, Laad;->c:Landroid/database/Cursor;

    const/16 v3, 0x8

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Laad;->c:Landroid/database/Cursor;

    const/4 v4, 0x7

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/4 v4, 0x5

    if-eq v2, v4, :cond_1d

    const/16 v4, 0xf

    if-eq v2, v4, :cond_1d

    const/16 v4, 0xd

    if-eq v2, v4, :cond_1d

    const/4 v4, 0x3

    if-ne v3, v4, :cond_1f

    :cond_1d
    const/4 v3, 0x1

    :goto_c
    invoke-virtual {v11, v3}, Lcom/google/android/apps/hangouts/views/SystemMessageListItemView;->a(Z)V

    move-object/from16 v0, p0

    iget-object v3, v0, Laad;->l:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a()Lyj;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Laad;->c:Landroid/database/Cursor;

    const/4 v5, 0x7

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Laad;->c:Landroid/database/Cursor;

    const/4 v6, 0x4

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Laad;->c:Landroid/database/Cursor;

    const/4 v7, 0x3

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Laad;->c:Landroid/database/Cursor;

    const/16 v8, 0x20

    invoke-interface {v7, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Laad;->c:Landroid/database/Cursor;

    const/16 v9, 0x21

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Laad;->c:Landroid/database/Cursor;

    const/16 v10, 0x24

    invoke-interface {v9, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lf;->a(ILyj;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11, v2}, Lcom/google/android/apps/hangouts/views/SystemMessageListItemView;->a(Ljava/lang/CharSequence;)V

    const/4 v2, 0x0

    invoke-static {v12, v13, v2}, Lf;->a(JZ)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v11, v2}, Lcom/google/android/apps/hangouts/views/SystemMessageListItemView;->b(Ljava/lang/CharSequence;)V

    invoke-virtual {v11, v12, v13}, Lcom/google/android/apps/hangouts/views/SystemMessageListItemView;->a(J)V

    goto/16 :goto_6

    .line 317
    :cond_1e
    sget v3, Lf;->gx:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/hangouts/views/SystemMessageListItemView;

    .line 319
    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->a(Lcdg;)V

    move-object v11, v2

    goto/16 :goto_b

    .line 321
    :cond_1f
    const/4 v3, 0x0

    goto :goto_c

    .line 286
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 699
    const/16 v0, 0x10

    return v0
.end method
