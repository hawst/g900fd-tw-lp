.class public final Ldja;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldja;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Long;

.field public e:[Ldjb;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Ldja;

    sput-object v0, Ldja;->a:[Ldja;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 92
    sget-object v0, Ldjb;->a:[Ldjb;

    iput-object v0, p0, Ldja;->e:[Ldjb;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 120
    iget-object v0, p0, Ldja;->b:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 121
    const/4 v0, 0x1

    iget-object v2, p0, Ldja;->b:Ljava/lang/String;

    .line 122
    invoke-static {v0, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 124
    :goto_0
    iget-object v2, p0, Ldja;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 125
    const/4 v2, 0x2

    iget-object v3, p0, Ldja;->c:Ljava/lang/String;

    .line 126
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 128
    :cond_0
    iget-object v2, p0, Ldja;->d:Ljava/lang/Long;

    if-eqz v2, :cond_1

    .line 129
    const/4 v2, 0x3

    iget-object v3, p0, Ldja;->d:Ljava/lang/Long;

    .line 130
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 132
    :cond_1
    iget-object v2, p0, Ldja;->e:[Ldjb;

    if-eqz v2, :cond_3

    .line 133
    iget-object v2, p0, Ldja;->e:[Ldjb;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 134
    if-eqz v4, :cond_2

    .line 135
    const/4 v5, 0x4

    .line 136
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 133
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 140
    :cond_3
    iget-object v1, p0, Ldja;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 141
    iput v0, p0, Ldja;->cachedSize:I

    .line 142
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldja;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldja;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldja;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldja;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldja;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldja;->d:Ljava/lang/Long;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldja;->e:[Ldjb;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldjb;

    iget-object v3, p0, Ldja;->e:[Ldjb;

    if-eqz v3, :cond_2

    iget-object v3, p0, Ldja;->e:[Ldjb;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Ldja;->e:[Ldjb;

    :goto_2
    iget-object v2, p0, Ldja;->e:[Ldjb;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Ldja;->e:[Ldjb;

    new-instance v3, Ldjb;

    invoke-direct {v3}, Ldjb;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldja;->e:[Ldjb;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ldja;->e:[Ldjb;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Ldja;->e:[Ldjb;

    new-instance v3, Ldjb;

    invoke-direct {v3}, Ldjb;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldja;->e:[Ldjb;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 97
    iget-object v0, p0, Ldja;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 98
    const/4 v0, 0x1

    iget-object v1, p0, Ldja;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 100
    :cond_0
    iget-object v0, p0, Ldja;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 101
    const/4 v0, 0x2

    iget-object v1, p0, Ldja;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 103
    :cond_1
    iget-object v0, p0, Ldja;->d:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 104
    const/4 v0, 0x3

    iget-object v1, p0, Ldja;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 106
    :cond_2
    iget-object v0, p0, Ldja;->e:[Ldjb;

    if-eqz v0, :cond_4

    .line 107
    iget-object v1, p0, Ldja;->e:[Ldjb;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 108
    if-eqz v3, :cond_3

    .line 109
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 107
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 113
    :cond_4
    iget-object v0, p0, Ldja;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 115
    return-void
.end method
