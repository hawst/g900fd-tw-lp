.class public final Ladt;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Z

.field private final b:Landroid/content/Context;

.field private c:Z

.field private d:Landroid/telephony/PhoneStateListener;

.field private e:Landroid/telephony/TelephonyManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Ladt;->b:Landroid/content/Context;

    .line 34
    return-void
.end method


# virtual methods
.method public a(Ladv;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 42
    iget-boolean v0, p0, Ladt;->c:Z

    invoke-static {v0}, Lcwz;->b(Z)V

    .line 43
    iput-boolean v2, p0, Ladt;->c:Z

    .line 45
    iget-object v0, p0, Ladt;->b:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Ladt;->e:Landroid/telephony/TelephonyManager;

    .line 46
    iget-object v0, p0, Ladt;->e:Landroid/telephony/TelephonyManager;

    if-nez v0, :cond_0

    .line 48
    const-string v0, "Babel"

    const-string v1, "couldn\'t retrieve TelephonyManager!"

    invoke-static {v0, v1}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    invoke-interface {p1}, Ladv;->a()V

    .line 65
    :goto_0
    return-void

    .line 55
    :cond_0
    new-instance v0, Ladu;

    invoke-direct {v0, p0, p1}, Ladu;-><init>(Ladt;Ladv;)V

    iput-object v0, p0, Ladt;->d:Landroid/telephony/PhoneStateListener;

    .line 64
    iget-object v0, p0, Ladt;->e:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Ladt;->d:Landroid/telephony/PhoneStateListener;

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 72
    iget-boolean v0, p0, Ladt;->c:Z

    invoke-static {v0}, Lcwz;->a(Z)V

    .line 74
    iget-boolean v0, p0, Ladt;->a:Z

    return v0
.end method

.method public b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 81
    iput-boolean v2, p0, Ladt;->c:Z

    .line 83
    iget-object v0, p0, Ladt;->d:Landroid/telephony/PhoneStateListener;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Ladt;->e:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Ladt;->d:Landroid/telephony/PhoneStateListener;

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 85
    const/4 v0, 0x0

    iput-object v0, p0, Ladt;->d:Landroid/telephony/PhoneStateListener;

    .line 87
    :cond_0
    return-void
.end method
