.class public final Lelz;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lelz;


# instance fields
.field public b:Lely;

.field public c:Lelh;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 840
    const/4 v0, 0x0

    new-array v0, v0, [Lelz;

    sput-object v0, Lelz;->a:[Lelz;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 841
    invoke-direct {p0}, Lepn;-><init>()V

    .line 844
    iput-object v0, p0, Lelz;->b:Lely;

    .line 847
    iput-object v0, p0, Lelz;->c:Lelh;

    .line 841
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 864
    const/4 v0, 0x0

    .line 865
    iget-object v1, p0, Lelz;->b:Lely;

    if-eqz v1, :cond_0

    .line 866
    const/4 v0, 0x1

    iget-object v1, p0, Lelz;->b:Lely;

    .line 867
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 869
    :cond_0
    iget-object v1, p0, Lelz;->c:Lelh;

    if-eqz v1, :cond_1

    .line 870
    const/4 v1, 0x2

    iget-object v2, p0, Lelz;->c:Lelh;

    .line 871
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 873
    :cond_1
    iget-object v1, p0, Lelz;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 874
    iput v0, p0, Lelz;->cachedSize:I

    .line 875
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 837
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lelz;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lelz;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lelz;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lelz;->b:Lely;

    if-nez v0, :cond_2

    new-instance v0, Lely;

    invoke-direct {v0}, Lely;-><init>()V

    iput-object v0, p0, Lelz;->b:Lely;

    :cond_2
    iget-object v0, p0, Lelz;->b:Lely;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lelz;->c:Lelh;

    if-nez v0, :cond_3

    new-instance v0, Lelh;

    invoke-direct {v0}, Lelh;-><init>()V

    iput-object v0, p0, Lelz;->c:Lelh;

    :cond_3
    iget-object v0, p0, Lelz;->c:Lelh;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 852
    iget-object v0, p0, Lelz;->b:Lely;

    if-eqz v0, :cond_0

    .line 853
    const/4 v0, 0x1

    iget-object v1, p0, Lelz;->b:Lely;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 855
    :cond_0
    iget-object v0, p0, Lelz;->c:Lelh;

    if-eqz v0, :cond_1

    .line 856
    const/4 v0, 0x2

    iget-object v1, p0, Lelz;->c:Lelh;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 858
    :cond_1
    iget-object v0, p0, Lelz;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 860
    return-void
.end method
