.class public final Ldgc;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldgc;


# instance fields
.field public b:Ldfi;

.field public c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1398
    const/4 v0, 0x0

    new-array v0, v0, [Ldgc;

    sput-object v0, Ldgc;->a:[Ldgc;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1399
    invoke-direct {p0}, Lepn;-><init>()V

    .line 1402
    const/4 v0, 0x0

    iput-object v0, p0, Ldgc;->b:Ldfi;

    .line 1399
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1421
    const/4 v0, 0x0

    .line 1422
    iget-object v1, p0, Ldgc;->b:Ldfi;

    if-eqz v1, :cond_0

    .line 1423
    const/4 v0, 0x1

    iget-object v1, p0, Ldgc;->b:Ldfi;

    .line 1424
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1426
    :cond_0
    iget-object v1, p0, Ldgc;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1427
    const/4 v1, 0x2

    iget-object v2, p0, Ldgc;->c:Ljava/lang/String;

    .line 1428
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1430
    :cond_1
    iget-object v1, p0, Ldgc;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1431
    iput v0, p0, Ldgc;->cachedSize:I

    .line 1432
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 1395
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldgc;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldgc;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldgc;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldgc;->b:Ldfi;

    if-nez v0, :cond_2

    new-instance v0, Ldfi;

    invoke-direct {v0}, Ldfi;-><init>()V

    iput-object v0, p0, Ldgc;->b:Ldfi;

    :cond_2
    iget-object v0, p0, Ldgc;->b:Ldfi;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldgc;->c:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 1409
    iget-object v0, p0, Ldgc;->b:Ldfi;

    if-eqz v0, :cond_0

    .line 1410
    const/4 v0, 0x1

    iget-object v1, p0, Ldgc;->b:Ldfi;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1412
    :cond_0
    iget-object v0, p0, Ldgc;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1413
    const/4 v0, 0x2

    iget-object v1, p0, Ldgc;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1415
    :cond_1
    iget-object v0, p0, Ldgc;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1417
    return-void
.end method
