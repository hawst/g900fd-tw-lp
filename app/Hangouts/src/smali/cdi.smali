.class public final Lcdi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lwl;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView$WatermarkGalleryStateTransition;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView$WatermarkGalleryStateTransition;)V
    .locals 0

    .prologue
    .line 605
    iput-object p1, p0, Lcdi;->a:Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView$WatermarkGalleryStateTransition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 625
    iget-object v0, p0, Lcdi;->a:Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView$WatermarkGalleryStateTransition;

    iget-object v0, v0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView$WatermarkGalleryStateTransition;->a:Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;

    iget-object v1, p0, Lcdi;->a:Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView$WatermarkGalleryStateTransition;

    iget-object v1, v1, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView$WatermarkGalleryStateTransition;->a:Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;

    .line 626
    invoke-static {v1}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->a(Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;)I

    move-result v1

    .line 625
    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->a(Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;I)I

    .line 627
    iget-object v0, p0, Lcdi;->a:Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView$WatermarkGalleryStateTransition;

    iget-object v0, v0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView$WatermarkGalleryStateTransition;->a:Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->b(Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;I)I

    .line 628
    iget-object v0, p0, Lcdi;->a:Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView$WatermarkGalleryStateTransition;

    iget-object v0, v0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView$WatermarkGalleryStateTransition;->a:Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->c(Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;)Lcdh;

    move-result-object v0

    iget-object v1, p0, Lcdi;->a:Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView$WatermarkGalleryStateTransition;

    iget-object v1, v1, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView$WatermarkGalleryStateTransition;->a:Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;

    .line 629
    invoke-static {v1}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->b(Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcdi;->a:Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView$WatermarkGalleryStateTransition;

    invoke-static {v2}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView$WatermarkGalleryStateTransition;->a(Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView$WatermarkGalleryStateTransition;)I

    move-result v2

    .line 628
    invoke-virtual {v0, v1, v2}, Lcdh;->a(Ljava/lang/String;I)V

    .line 630
    iget-object v0, p0, Lcdi;->a:Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView$WatermarkGalleryStateTransition;

    iget-object v0, v0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView$WatermarkGalleryStateTransition;->a:Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->d(Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;)Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->a()Lccw;

    move-result-object v0

    .line 631
    if-eqz v0, :cond_0

    .line 632
    invoke-interface {v0}, Lccw;->b()V

    .line 634
    :cond_0
    return-void
.end method


# virtual methods
.method public onAnimationCancel(Lwk;)V
    .locals 0

    .prologue
    .line 616
    invoke-direct {p0}, Lcdi;->a()V

    .line 617
    return-void
.end method

.method public onAnimationEnd(Lwk;)V
    .locals 0

    .prologue
    .line 621
    invoke-direct {p0}, Lcdi;->a()V

    .line 622
    return-void
.end method

.method public onAnimationRepeat(Lwk;)V
    .locals 0

    .prologue
    .line 612
    return-void
.end method

.method public onAnimationStart(Lwk;)V
    .locals 0

    .prologue
    .line 608
    return-void
.end method
