.class public final Levv;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Levv;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Levt;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Levt;

.field public l:[Levx;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Levv;

    sput-object v0, Levv;->a:[Levv;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 23
    iput-object v0, p0, Levv;->g:Levt;

    .line 32
    iput-object v0, p0, Levv;->k:Levt;

    .line 35
    sget-object v0, Levx;->a:[Levx;

    iput-object v0, p0, Levv;->l:[Levx;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 84
    iget-object v0, p0, Levv;->c:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 85
    const/4 v0, 0x1

    iget-object v2, p0, Levv;->c:Ljava/lang/String;

    .line 86
    invoke-static {v0, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 88
    :goto_0
    iget-object v2, p0, Levv;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 89
    const/4 v2, 0x2

    iget-object v3, p0, Levv;->d:Ljava/lang/String;

    .line 90
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 92
    :cond_0
    iget-object v2, p0, Levv;->g:Levt;

    if-eqz v2, :cond_1

    .line 93
    const/4 v2, 0x4

    iget-object v3, p0, Levv;->g:Levt;

    .line 94
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 96
    :cond_1
    iget-object v2, p0, Levv;->h:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 97
    const/4 v2, 0x5

    iget-object v3, p0, Levv;->h:Ljava/lang/String;

    .line 98
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 100
    :cond_2
    iget-object v2, p0, Levv;->j:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 101
    const/4 v2, 0x6

    iget-object v3, p0, Levv;->j:Ljava/lang/String;

    .line 102
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 104
    :cond_3
    iget-object v2, p0, Levv;->k:Levt;

    if-eqz v2, :cond_4

    .line 105
    const/4 v2, 0x7

    iget-object v3, p0, Levv;->k:Levt;

    .line 106
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 108
    :cond_4
    iget-object v2, p0, Levv;->i:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 109
    const/16 v2, 0x8

    iget-object v3, p0, Levv;->i:Ljava/lang/String;

    .line 110
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 112
    :cond_5
    iget-object v2, p0, Levv;->l:[Levx;

    if-eqz v2, :cond_7

    .line 113
    iget-object v2, p0, Levv;->l:[Levx;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    .line 114
    if-eqz v4, :cond_6

    .line 115
    const/16 v5, 0x9

    .line 116
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 113
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 120
    :cond_7
    iget-object v1, p0, Levv;->b:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 121
    const/16 v1, 0xa

    iget-object v2, p0, Levv;->b:Ljava/lang/String;

    .line 122
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 124
    :cond_8
    iget-object v1, p0, Levv;->e:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 125
    const/16 v1, 0xb

    iget-object v2, p0, Levv;->e:Ljava/lang/String;

    .line 126
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 128
    :cond_9
    iget-object v1, p0, Levv;->f:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 129
    const/16 v1, 0xc

    iget-object v2, p0, Levv;->f:Ljava/lang/String;

    .line 130
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 132
    :cond_a
    iget-object v1, p0, Levv;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 133
    iput v0, p0, Levv;->cachedSize:I

    .line 134
    return v0

    :cond_b
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Levv;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Levv;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Levv;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Levv;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Levv;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Levv;->g:Levt;

    if-nez v0, :cond_2

    new-instance v0, Levt;

    invoke-direct {v0}, Levt;-><init>()V

    iput-object v0, p0, Levv;->g:Levt;

    :cond_2
    iget-object v0, p0, Levv;->g:Levt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Levv;->h:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Levv;->j:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Levv;->k:Levt;

    if-nez v0, :cond_3

    new-instance v0, Levt;

    invoke-direct {v0}, Levt;-><init>()V

    iput-object v0, p0, Levv;->k:Levt;

    :cond_3
    iget-object v0, p0, Levv;->k:Levt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Levv;->i:Ljava/lang/String;

    goto :goto_0

    :sswitch_8
    const/16 v0, 0x4a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Levv;->l:[Levx;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Levx;

    iget-object v3, p0, Levv;->l:[Levx;

    if-eqz v3, :cond_4

    iget-object v3, p0, Levv;->l:[Levx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iput-object v2, p0, Levv;->l:[Levx;

    :goto_2
    iget-object v2, p0, Levv;->l:[Levx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Levv;->l:[Levx;

    new-instance v3, Levx;

    invoke-direct {v3}, Levx;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Levv;->l:[Levx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Levv;->l:[Levx;

    array-length v0, v0

    goto :goto_1

    :cond_6
    iget-object v2, p0, Levv;->l:[Levx;

    new-instance v3, Levx;

    invoke-direct {v3}, Levx;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Levv;->l:[Levx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Levv;->b:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Levv;->e:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Levv;->f:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 40
    iget-object v0, p0, Levv;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 41
    const/4 v0, 0x1

    iget-object v1, p0, Levv;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 43
    :cond_0
    iget-object v0, p0, Levv;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 44
    const/4 v0, 0x2

    iget-object v1, p0, Levv;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 46
    :cond_1
    iget-object v0, p0, Levv;->g:Levt;

    if-eqz v0, :cond_2

    .line 47
    const/4 v0, 0x4

    iget-object v1, p0, Levv;->g:Levt;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 49
    :cond_2
    iget-object v0, p0, Levv;->h:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 50
    const/4 v0, 0x5

    iget-object v1, p0, Levv;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 52
    :cond_3
    iget-object v0, p0, Levv;->j:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 53
    const/4 v0, 0x6

    iget-object v1, p0, Levv;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 55
    :cond_4
    iget-object v0, p0, Levv;->k:Levt;

    if-eqz v0, :cond_5

    .line 56
    const/4 v0, 0x7

    iget-object v1, p0, Levv;->k:Levt;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 58
    :cond_5
    iget-object v0, p0, Levv;->i:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 59
    const/16 v0, 0x8

    iget-object v1, p0, Levv;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 61
    :cond_6
    iget-object v0, p0, Levv;->l:[Levx;

    if-eqz v0, :cond_8

    .line 62
    iget-object v1, p0, Levv;->l:[Levx;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_8

    aget-object v3, v1, v0

    .line 63
    if-eqz v3, :cond_7

    .line 64
    const/16 v4, 0x9

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 62
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 68
    :cond_8
    iget-object v0, p0, Levv;->b:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 69
    const/16 v0, 0xa

    iget-object v1, p0, Levv;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 71
    :cond_9
    iget-object v0, p0, Levv;->e:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 72
    const/16 v0, 0xb

    iget-object v1, p0, Levv;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 74
    :cond_a
    iget-object v0, p0, Levv;->f:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 75
    const/16 v0, 0xc

    iget-object v1, p0, Levv;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 77
    :cond_b
    iget-object v0, p0, Levv;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 79
    return-void
.end method
