.class public final Ldub;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldub;


# instance fields
.field public b:Ldvn;

.field public c:Ldrr;

.field public d:Ldqa;

.field public e:Ljava/lang/Boolean;

.field public f:Ljava/lang/Long;

.field public g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11054
    const/4 v0, 0x0

    new-array v0, v0, [Ldub;

    sput-object v0, Ldub;->a:[Ldub;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 11055
    invoke-direct {p0}, Lepn;-><init>()V

    .line 11058
    iput-object v0, p0, Ldub;->b:Ldvn;

    .line 11061
    iput-object v0, p0, Ldub;->c:Ldrr;

    .line 11064
    iput-object v0, p0, Ldub;->d:Ldqa;

    .line 11055
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 11099
    const/4 v0, 0x0

    .line 11100
    iget-object v1, p0, Ldub;->b:Ldvn;

    if-eqz v1, :cond_0

    .line 11101
    const/4 v0, 0x1

    iget-object v1, p0, Ldub;->b:Ldvn;

    .line 11102
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 11104
    :cond_0
    iget-object v1, p0, Ldub;->f:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 11105
    const/4 v1, 0x2

    iget-object v2, p0, Ldub;->f:Ljava/lang/Long;

    .line 11106
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 11108
    :cond_1
    iget-object v1, p0, Ldub;->g:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 11109
    const/4 v1, 0x3

    iget-object v2, p0, Ldub;->g:Ljava/lang/String;

    .line 11110
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11112
    :cond_2
    iget-object v1, p0, Ldub;->c:Ldrr;

    if-eqz v1, :cond_3

    .line 11113
    const/4 v1, 0x4

    iget-object v2, p0, Ldub;->c:Ldrr;

    .line 11114
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11116
    :cond_3
    iget-object v1, p0, Ldub;->d:Ldqa;

    if-eqz v1, :cond_4

    .line 11117
    const/4 v1, 0x5

    iget-object v2, p0, Ldub;->d:Ldqa;

    .line 11118
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11120
    :cond_4
    iget-object v1, p0, Ldub;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 11121
    const/4 v1, 0x6

    iget-object v2, p0, Ldub;->e:Ljava/lang/Boolean;

    .line 11122
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 11124
    :cond_5
    iget-object v1, p0, Ldub;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11125
    iput v0, p0, Ldub;->cachedSize:I

    .line 11126
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 11051
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldub;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldub;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldub;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldub;->b:Ldvn;

    if-nez v0, :cond_2

    new-instance v0, Ldvn;

    invoke-direct {v0}, Ldvn;-><init>()V

    iput-object v0, p0, Ldub;->b:Ldvn;

    :cond_2
    iget-object v0, p0, Ldub;->b:Ldvn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldub;->f:Ljava/lang/Long;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldub;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldub;->c:Ldrr;

    if-nez v0, :cond_3

    new-instance v0, Ldrr;

    invoke-direct {v0}, Ldrr;-><init>()V

    iput-object v0, p0, Ldub;->c:Ldrr;

    :cond_3
    iget-object v0, p0, Ldub;->c:Ldrr;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ldub;->d:Ldqa;

    if-nez v0, :cond_4

    new-instance v0, Ldqa;

    invoke-direct {v0}, Ldqa;-><init>()V

    iput-object v0, p0, Ldub;->d:Ldqa;

    :cond_4
    iget-object v0, p0, Ldub;->d:Ldqa;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldub;->e:Ljava/lang/Boolean;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 11075
    iget-object v0, p0, Ldub;->b:Ldvn;

    if-eqz v0, :cond_0

    .line 11076
    const/4 v0, 0x1

    iget-object v1, p0, Ldub;->b:Ldvn;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 11078
    :cond_0
    iget-object v0, p0, Ldub;->f:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 11079
    const/4 v0, 0x2

    iget-object v1, p0, Ldub;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 11081
    :cond_1
    iget-object v0, p0, Ldub;->g:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 11082
    const/4 v0, 0x3

    iget-object v1, p0, Ldub;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 11084
    :cond_2
    iget-object v0, p0, Ldub;->c:Ldrr;

    if-eqz v0, :cond_3

    .line 11085
    const/4 v0, 0x4

    iget-object v1, p0, Ldub;->c:Ldrr;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 11087
    :cond_3
    iget-object v0, p0, Ldub;->d:Ldqa;

    if-eqz v0, :cond_4

    .line 11088
    const/4 v0, 0x5

    iget-object v1, p0, Ldub;->d:Ldqa;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 11090
    :cond_4
    iget-object v0, p0, Ldub;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 11091
    const/4 v0, 0x6

    iget-object v1, p0, Ldub;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 11093
    :cond_5
    iget-object v0, p0, Ldub;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 11095
    return-void
.end method
