.class public final enum Lxq;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lxq;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lxq;

.field public static final enum b:Lxq;

.field public static final enum c:Lxq;

.field public static final enum d:Lxq;

.field public static final enum e:Lxq;

.field public static final enum f:Lxq;

.field public static final enum g:Lxq;

.field public static final enum h:Lxq;

.field public static final enum i:Lxq;

.field public static final enum j:Lxq;

.field public static final enum k:Lxq;

.field private static final synthetic m:[Lxq;


# instance fields
.field private final l:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 18
    new-instance v0, Lxq;

    const-string v1, "PERSONAL"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lxq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lxq;->a:Lxq;

    .line 19
    new-instance v0, Lxq;

    const-string v1, "PRIVATE_SHARED"

    invoke-direct {v0, v1, v4, v5}, Lxq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lxq;->b:Lxq;

    .line 20
    new-instance v0, Lxq;

    const-string v1, "PUBLIC_SHARED"

    invoke-direct {v0, v1, v5, v6}, Lxq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lxq;->c:Lxq;

    .line 21
    new-instance v0, Lxq;

    const-string v1, "FOLLOWING"

    invoke-direct {v0, v1, v6, v7}, Lxq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lxq;->d:Lxq;

    .line 22
    new-instance v0, Lxq;

    const-string v1, "MY_CIRCLES"

    invoke-direct {v0, v1, v7, v8}, Lxq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lxq;->e:Lxq;

    .line 23
    new-instance v0, Lxq;

    const-string v1, "VISIBLE_CIRCLE_MEMBERS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lxq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lxq;->f:Lxq;

    .line 24
    new-instance v0, Lxq;

    const-string v1, "EXTENDED"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lxq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lxq;->g:Lxq;

    .line 25
    new-instance v0, Lxq;

    const-string v1, "DOMAIN"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lxq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lxq;->h:Lxq;

    .line 26
    new-instance v0, Lxq;

    const-string v1, "PUBLIC"

    const/16 v2, 0x8

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lxq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lxq;->i:Lxq;

    .line 27
    new-instance v0, Lxq;

    const-string v1, "BLOCKED"

    const/16 v2, 0x9

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lxq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lxq;->j:Lxq;

    .line 28
    new-instance v0, Lxq;

    const-string v1, "ALL_CIRCLES"

    const/16 v2, 0xa

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lxq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lxq;->k:Lxq;

    .line 17
    const/16 v0, 0xb

    new-array v0, v0, [Lxq;

    const/4 v1, 0x0

    sget-object v2, Lxq;->a:Lxq;

    aput-object v2, v0, v1

    sget-object v1, Lxq;->b:Lxq;

    aput-object v1, v0, v4

    sget-object v1, Lxq;->c:Lxq;

    aput-object v1, v0, v5

    sget-object v1, Lxq;->d:Lxq;

    aput-object v1, v0, v6

    sget-object v1, Lxq;->e:Lxq;

    aput-object v1, v0, v7

    sget-object v1, Lxq;->f:Lxq;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lxq;->g:Lxq;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lxq;->h:Lxq;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lxq;->i:Lxq;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lxq;->j:Lxq;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lxq;->k:Lxq;

    aput-object v2, v0, v1

    sput-object v0, Lxq;->m:[Lxq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 45
    iput p3, p0, Lxq;->l:I

    .line 46
    return-void
.end method

.method public static a(I)Lxq;
    .locals 1

    .prologue
    .line 53
    packed-switch p0, :pswitch_data_0

    .line 77
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 55
    :pswitch_0
    sget-object v0, Lxq;->a:Lxq;

    goto :goto_0

    .line 57
    :pswitch_1
    sget-object v0, Lxq;->b:Lxq;

    goto :goto_0

    .line 59
    :pswitch_2
    sget-object v0, Lxq;->c:Lxq;

    goto :goto_0

    .line 61
    :pswitch_3
    sget-object v0, Lxq;->d:Lxq;

    goto :goto_0

    .line 63
    :pswitch_4
    sget-object v0, Lxq;->e:Lxq;

    goto :goto_0

    .line 65
    :pswitch_5
    sget-object v0, Lxq;->f:Lxq;

    goto :goto_0

    .line 67
    :pswitch_6
    sget-object v0, Lxq;->g:Lxq;

    goto :goto_0

    .line 69
    :pswitch_7
    sget-object v0, Lxq;->h:Lxq;

    goto :goto_0

    .line 71
    :pswitch_8
    sget-object v0, Lxq;->i:Lxq;

    goto :goto_0

    .line 73
    :pswitch_9
    sget-object v0, Lxq;->j:Lxq;

    goto :goto_0

    .line 75
    :pswitch_a
    sget-object v0, Lxq;->k:Lxq;

    goto :goto_0

    .line 53
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lxq;
    .locals 1

    .prologue
    .line 17
    const-class v0, Lxq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lxq;

    return-object v0
.end method

.method public static values()[Lxq;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lxq;->m:[Lxq;

    invoke-virtual {v0}, [Lxq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lxq;

    return-object v0
.end method
