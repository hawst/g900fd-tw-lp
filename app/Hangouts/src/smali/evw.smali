.class public final Levw;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Levw;


# instance fields
.field public b:Lexn;

.field public c:[Levv;

.field public d:[Levt;

.field public e:Levt;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 320
    const/4 v0, 0x0

    new-array v0, v0, [Levw;

    sput-object v0, Levw;->a:[Levw;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 321
    invoke-direct {p0}, Lepn;-><init>()V

    .line 324
    iput-object v1, p0, Levw;->b:Lexn;

    .line 327
    sget-object v0, Levv;->a:[Levv;

    iput-object v0, p0, Levw;->c:[Levv;

    .line 330
    sget-object v0, Levt;->a:[Levt;

    iput-object v0, p0, Levw;->d:[Levt;

    .line 333
    iput-object v1, p0, Levw;->e:Levt;

    .line 321
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 365
    iget-object v0, p0, Levw;->b:Lexn;

    if-eqz v0, :cond_5

    .line 366
    const/4 v0, 0x1

    iget-object v2, p0, Levw;->b:Lexn;

    .line 367
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 369
    :goto_0
    iget-object v2, p0, Levw;->c:[Levv;

    if-eqz v2, :cond_1

    .line 370
    iget-object v3, p0, Levw;->c:[Levv;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 371
    if-eqz v5, :cond_0

    .line 372
    const/4 v6, 0x2

    .line 373
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 370
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 377
    :cond_1
    iget-object v2, p0, Levw;->e:Levt;

    if-eqz v2, :cond_2

    .line 378
    const/4 v2, 0x3

    iget-object v3, p0, Levw;->e:Levt;

    .line 379
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 381
    :cond_2
    iget-object v2, p0, Levw;->d:[Levt;

    if-eqz v2, :cond_4

    .line 382
    iget-object v2, p0, Levw;->d:[Levt;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 383
    if-eqz v4, :cond_3

    .line 384
    const/4 v5, 0x4

    .line 385
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 382
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 389
    :cond_4
    iget-object v1, p0, Levw;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 390
    iput v0, p0, Levw;->cachedSize:I

    .line 391
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 317
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Levw;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Levw;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Levw;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Levw;->b:Lexn;

    if-nez v0, :cond_2

    new-instance v0, Lexn;

    invoke-direct {v0}, Lexn;-><init>()V

    iput-object v0, p0, Levw;->b:Lexn;

    :cond_2
    iget-object v0, p0, Levw;->b:Lexn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Levw;->c:[Levv;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Levv;

    iget-object v3, p0, Levw;->c:[Levv;

    if-eqz v3, :cond_3

    iget-object v3, p0, Levw;->c:[Levv;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Levw;->c:[Levv;

    :goto_2
    iget-object v2, p0, Levw;->c:[Levv;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Levw;->c:[Levv;

    new-instance v3, Levv;

    invoke-direct {v3}, Levv;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Levw;->c:[Levv;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Levw;->c:[Levv;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Levw;->c:[Levv;

    new-instance v3, Levv;

    invoke-direct {v3}, Levv;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Levw;->c:[Levv;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Levw;->e:Levt;

    if-nez v0, :cond_6

    new-instance v0, Levt;

    invoke-direct {v0}, Levt;-><init>()V

    iput-object v0, p0, Levw;->e:Levt;

    :cond_6
    iget-object v0, p0, Levw;->e:Levt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Levw;->d:[Levt;

    if-nez v0, :cond_8

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Levt;

    iget-object v3, p0, Levw;->d:[Levt;

    if-eqz v3, :cond_7

    iget-object v3, p0, Levw;->d:[Levt;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    iput-object v2, p0, Levw;->d:[Levt;

    :goto_4
    iget-object v2, p0, Levw;->d:[Levt;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    iget-object v2, p0, Levw;->d:[Levt;

    new-instance v3, Levt;

    invoke-direct {v3}, Levt;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Levw;->d:[Levt;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_8
    iget-object v0, p0, Levw;->d:[Levt;

    array-length v0, v0

    goto :goto_3

    :cond_9
    iget-object v2, p0, Levw;->d:[Levt;

    new-instance v3, Levt;

    invoke-direct {v3}, Levt;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Levw;->d:[Levt;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 338
    iget-object v1, p0, Levw;->b:Lexn;

    if-eqz v1, :cond_0

    .line 339
    const/4 v1, 0x1

    iget-object v2, p0, Levw;->b:Lexn;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 341
    :cond_0
    iget-object v1, p0, Levw;->c:[Levv;

    if-eqz v1, :cond_2

    .line 342
    iget-object v2, p0, Levw;->c:[Levv;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 343
    if-eqz v4, :cond_1

    .line 344
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 342
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 348
    :cond_2
    iget-object v1, p0, Levw;->e:Levt;

    if-eqz v1, :cond_3

    .line 349
    const/4 v1, 0x3

    iget-object v2, p0, Levw;->e:Levt;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 351
    :cond_3
    iget-object v1, p0, Levw;->d:[Levt;

    if-eqz v1, :cond_5

    .line 352
    iget-object v1, p0, Levw;->d:[Levt;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 353
    if-eqz v3, :cond_4

    .line 354
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 352
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 358
    :cond_5
    iget-object v0, p0, Levw;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 360
    return-void
.end method
