.class public final Lcoe;
.super Ljava/lang/Object;

# interfaces
.implements Lft;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final a:Lckg;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcfv;Lcfw;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lckg;

    const-string v1, "location"

    invoke-direct {v0, p1, p2, p3, v1}, Lckg;-><init>(Landroid/content/Context;Lcfv;Lcfw;Ljava/lang/String;)V

    iput-object v0, p0, Lcoe;->a:Lckg;

    return-void
.end method


# virtual methods
.method public a()Landroid/location/Location;
    .locals 1

    iget-object v0, p0, Lcoe;->a:Lckg;

    invoke-virtual {v0}, Lckg;->j()Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/gms/location/LocationRequest;Lcof;)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcoe;->a:Lckg;

    invoke-static {p1}, Lcom/google/android/gms/internal/nh;->a(Lcom/google/android/gms/location/LocationRequest;)Lcom/google/android/gms/internal/nh;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lckg;->a(Lcom/google/android/gms/internal/nh;Lcof;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcoe;->a:Lckg;

    invoke-virtual {v0}, Lckg;->a()V

    return-void
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lcoe;->a:Lckg;

    invoke-virtual {v0}, Lckg;->b()V

    return-void
.end method
