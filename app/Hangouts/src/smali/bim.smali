.class public Lbim;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final g:Z

.field private final h:J

.field private final i:J

.field private final j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<[B>;"
        }
    .end annotation
.end field

.field private final k:Z

.field private final l:[Lbdh;


# direct methods
.method private constructor <init>(Ldxi;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2529
    iget-object v1, p1, Ldxi;->b:Ldvn;

    const-wide/16 v2, -0x1

    invoke-direct {p0, v1, v2, v3}, Lbfz;-><init>(Ldvn;J)V

    .line 2502
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lbim;->j:Ljava/util/List;

    .line 2530
    iget-object v1, p1, Ldxi;->c:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 2531
    const/4 v1, 0x1

    iput-boolean v1, p0, Lbim;->g:Z

    .line 2532
    iget-object v1, p1, Ldxi;->c:Ljava/lang/Long;

    invoke-static {v1}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v1

    iput-wide v1, p0, Lbim;->h:J

    .line 2538
    :goto_0
    iget-object v1, p1, Ldxi;->e:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 2539
    iget-object v1, p1, Ldxi;->e:Ljava/lang/Long;

    invoke-static {v1}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v1

    iput-wide v1, p0, Lbim;->i:J

    .line 2544
    :goto_1
    iget-object v1, p1, Ldxi;->d:[Ldql;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 2545
    iget-object v4, p0, Lbim;->j:Ljava/util/List;

    invoke-static {v3}, Ldql;->toByteArray(Lepr;)[B

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2544
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2534
    :cond_0
    iput-boolean v0, p0, Lbim;->g:Z

    .line 2535
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lbim;->h:J

    goto :goto_0

    .line 2542
    :cond_1
    const-wide/16 v1, -0x2

    iput-wide v1, p0, Lbim;->i:J

    goto :goto_1

    .line 2547
    :cond_2
    iget-object v0, p1, Ldxi;->g:[Ldro;

    iget-object v1, p1, Ldxi;->d:[Ldql;

    invoke-static {v0, v1}, Lbfz;->a([Ldro;[Ldql;)[Lbdh;

    move-result-object v0

    iput-object v0, p0, Lbim;->l:[Lbdh;

    .line 2549
    invoke-virtual {p0}, Lbim;->f()Z

    move-result v0

    iput-boolean v0, p0, Lbim;->k:Z

    .line 2551
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_3

    .line 2552
    const-string v0, "Babel_protos"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SyncRecentConversationsResponse from:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2555
    :cond_3
    return-void
.end method

.method private d(J)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List",
            "<",
            "Lbck;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2507
    const/4 v0, 0x0

    .line 2508
    iget-object v1, p0, Lbim;->b:Lbea;

    if-eqz v1, :cond_3

    .line 2509
    iget-object v0, p0, Lbim;->b:Lbea;

    check-cast v0, Lbft;

    iget-boolean v0, v0, Lbft;->h:Z

    .line 2511
    sget-boolean v1, Lbfz;->a:Z

    if-eqz v1, :cond_0

    .line 2512
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SRCResp.buildConversationStateList: suppressNotif="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move v1, v0

    .line 2516
    :goto_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2517
    iget-object v0, p0, Lbim;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 2518
    invoke-static {v0, v1, p1, p2}, Lbck;->a([BZJ)Lbck;

    move-result-object v0

    .line 2520
    if-eqz v0, :cond_1

    .line 2521
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2524
    :cond_2
    return-object v2

    :cond_3
    move v1, v0

    goto :goto_0
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 2559
    new-instance v0, Ldxi;

    invoke-direct {v0}, Ldxi;-><init>()V

    .line 2560
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldxi;

    .line 2561
    iget-object v1, v0, Ldxi;->b:Ldvn;

    invoke-static {v1}, Lbim;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2562
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldxi;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 2564
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbim;

    invoke-direct {v1, v0}, Lbim;-><init>(Ldxi;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Lyt;Lbnl;)V
    .locals 25

    .prologue
    .line 2602
    invoke-super/range {p0 .. p2}, Lbfz;->a(Lyt;Lbnl;)V

    .line 2603
    const-string v1, "Babel"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2604
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "processSyncRecentConversationsResponse count: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-wide/16 v3, 0x0

    .line 2605
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lbim;->d(J)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " has_sync_timestamp: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lbim;->g:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " sync_timestamp: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-wide v3, v0, Lbim;->h:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " requestWasForScrollback: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lbim;->k:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2604
    invoke-static {v1, v2}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2611
    :cond_0
    invoke-static {}, Lbya;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2612
    new-instance v1, Lbyc;

    invoke-direct {v1}, Lbyc;-><init>()V

    const-string v2, "src_response"

    .line 2613
    invoke-virtual {v1, v2}, Lbyc;->a(Ljava/lang/String;)Lbyc;

    move-result-object v1

    .line 2614
    invoke-virtual/range {p1 .. p1}, Lyt;->f()Lyj;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbyc;->a(Lyj;)Lbyc;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "forScrollback="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lbim;->k:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2615
    invoke-virtual {v1, v2}, Lbyc;->b(Ljava/lang/String;)Lbyc;

    move-result-object v1

    .line 2616
    invoke-virtual {v1}, Lbyc;->b()V

    .line 2619
    :cond_1
    new-instance v18, Ljava/util/HashSet;

    invoke-direct/range {v18 .. v18}, Ljava/util/HashSet;-><init>()V

    .line 2620
    new-instance v19, Ljava/util/HashSet;

    invoke-direct/range {v19 .. v19}, Ljava/util/HashSet;-><init>()V

    .line 2621
    const/4 v9, 0x0

    .line 2623
    const-wide/16 v1, 0x0

    .line 2624
    sget-boolean v3, Lyp;->a:Z

    if-eqz v3, :cond_17

    .line 2625
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    move-wide v11, v1

    .line 2628
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lyt;->a()V

    .line 2629
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    mul-long v20, v1, v3

    .line 2631
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 2632
    move-object/from16 v0, p0

    iget-object v1, v0, Lbim;->l:[Lbdh;

    if-eqz v1, :cond_3

    .line 2634
    move-object/from16 v0, p0

    iget-object v4, v0, Lbim;->l:[Lbdh;

    array-length v5, v4

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v5, :cond_3

    aget-object v6, v4, v1

    .line 2636
    if-eqz v6, :cond_2

    .line 2637
    const/4 v7, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7}, Lyt;->a(Lbdh;Z)Z

    .line 2634
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2642
    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lbim;->c:Lbht;

    iget-wide v4, v1, Lbht;->d:J

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lbim;->d(J)Ljava/util/List;

    move-result-object v22

    .line 2644
    invoke-interface/range {v22 .. v22}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v23

    :goto_2
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lbck;

    move-object v10, v0

    .line 2645
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long/2addr v4, v2

    .line 2646
    invoke-static {}, Lzo;->b()J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-ltz v1, :cond_16

    .line 2647
    invoke-virtual/range {p1 .. p1}, Lyt;->d()V

    .line 2648
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    move-wide/from16 v16, v1

    .line 2650
    :goto_3
    iget-object v1, v10, Lbck;->a:Ljava/lang/String;

    .line 2651
    iget-object v0, v10, Lbck;->c:Ljava/util/List;

    move-object/from16 v24, v0

    .line 2652
    const-string v2, "Babel"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2653
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "processing conversation "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " events: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 2654
    invoke-interface/range {v24 .. v24}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2653
    invoke-static {v2, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2656
    :cond_4
    const/4 v14, 0x0

    .line 2657
    const/4 v13, 0x0

    .line 2659
    iget-object v1, v10, Lbck;->b:Lbjc;

    if-eqz v1, :cond_15

    .line 2660
    iget-object v2, v10, Lbck;->b:Lbjc;

    iget-wide v3, v10, Lbck;->h:J

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v1, p1

    move-object/from16 v6, p2

    invoke-static/range {v1 .. v8}, Lyp;->a(Lyt;Lbjc;JLjava/lang/String;Lbnl;Lys;Z)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 2664
    const/4 v1, 0x1

    move v15, v1

    .line 2669
    :goto_4
    iget-object v1, v10, Lbck;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_5

    .line 2670
    iget-object v1, v10, Lbck;->a:Ljava/lang/String;

    iget-object v2, v10, Lbck;->e:[B

    iget-wide v3, v10, Lbck;->f:J

    move-object/from16 v0, p1

    invoke-static {v0, v1, v2, v3, v4}, Lyp;->a(Lyt;Ljava/lang/String;[BJ)V

    .line 2677
    :cond_5
    invoke-interface/range {v24 .. v24}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v24

    move v1, v13

    move v4, v14

    :goto_5
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbjg;

    .line 2678
    instance-of v3, v2, Lbji;

    if-eqz v3, :cond_6

    .line 2679
    const/4 v3, 0x1

    move v13, v1

    move v14, v3

    .line 2691
    :goto_6
    invoke-virtual/range {p0 .. p0}, Lbim;->d()J

    move-result-wide v4

    invoke-virtual/range {p0 .. p0}, Lbim;->e()J

    move-result-wide v6

    move-object/from16 v1, p1

    move-object/from16 v3, p2

    move-wide/from16 v8, v20

    .line 2690
    invoke-static/range {v1 .. v9}, Lyp;->a(Lyt;Lbjg;Lbnl;JJJ)V

    move v1, v13

    move v4, v14

    .line 2692
    goto :goto_5

    .line 2680
    :cond_6
    instance-of v3, v2, Lbiv;

    if-eqz v3, :cond_14

    .line 2681
    const/4 v3, 0x1

    .line 2682
    invoke-virtual/range {p0 .. p0}, Lbim;->f()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2683
    move-object v0, v2

    check-cast v0, Lbiv;

    move-object v1, v0

    const/4 v5, 0x4

    invoke-virtual {v1, v5}, Lbiv;->a(I)V

    move v13, v3

    move v14, v4

    goto :goto_6

    .line 2686
    :cond_7
    move-object v0, v2

    check-cast v0, Lbiv;

    move-object v1, v0

    const/4 v5, 0x3

    invoke-virtual {v1, v5}, Lbiv;->a(I)V

    move v13, v3

    move v14, v4

    goto :goto_6

    .line 2694
    :cond_8
    if-eqz v1, :cond_9

    .line 2695
    iget-object v1, v10, Lbck;->a:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2697
    :cond_9
    if-eqz v4, :cond_a

    .line 2698
    iget-object v1, v10, Lbck;->a:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_a
    move-wide/from16 v2, v16

    move v9, v15

    .line 2700
    goto/16 :goto_2

    .line 2702
    :cond_b
    move-object/from16 v0, p0

    iget-object v1, v0, Lbim;->b:Lbea;

    check-cast v1, Lbft;

    .line 2703
    invoke-virtual {v1}, Lbft;->h()I

    move-result v1

    .line 2704
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lbim;->g:Z

    if-eqz v2, :cond_c

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lbim;->k:Z

    if-nez v2, :cond_c

    const/4 v2, 0x5

    if-ne v1, v2, :cond_c

    .line 2708
    const-string v2, "last_successful_sync_time"

    move-object/from16 v0, p0

    iget-wide v3, v0, Lbim;->h:J

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, Lyt;->h(Ljava/lang/String;J)V

    .line 2711
    :cond_c
    invoke-interface/range {v22 .. v22}, Ljava/util/List;->size()I

    move-result v2

    .line 2712
    move-object/from16 v0, p0

    iget-wide v3, v0, Lbim;->i:J

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v1, v2}, Lyt;->a(JII)V

    .line 2714
    const/4 v3, 0x5

    if-ne v1, v3, :cond_11

    .line 2717
    move-object/from16 v0, p0

    iget-wide v3, v0, Lbim;->i:J

    const/4 v1, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v1, v2}, Lyt;->a(JII)V

    .line 2726
    :cond_d
    :goto_7
    invoke-virtual/range {p1 .. p1}, Lyt;->k()V

    .line 2727
    invoke-virtual/range {p1 .. p1}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2729
    invoke-virtual/range {p1 .. p1}, Lyt;->c()V

    .line 2732
    move-object/from16 v0, p0

    iget-object v1, v0, Lbim;->b:Lbea;

    check-cast v1, Lbft;

    .line 2733
    invoke-virtual {v1}, Lbft;->j()Ljava/lang/String;

    move-result-object v1

    .line 2734
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_e

    .line 2737
    invoke-virtual/range {p1 .. p1}, Lyt;->f()Lyj;

    move-result-object v2

    invoke-static {v2}, Lbpf;->a(Lyj;)Lbpf;

    move-result-object v2

    .line 2738
    invoke-virtual {v2, v1}, Lbpf;->c(Ljava/lang/String;)Z

    .line 2741
    :cond_e
    sget-boolean v1, Lyp;->a:Z

    if-eqz v1, :cond_f

    .line 2742
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    .line 2743
    const-string v3, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "processSyncRecentConversations took "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-long/2addr v1, v11

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2747
    :cond_f
    if-eqz v9, :cond_10

    .line 2748
    invoke-static/range {p1 .. p1}, Lyp;->d(Lyt;)V

    .line 2750
    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lyt;->a(Lyt;Ljava/lang/String;)I

    .line 2752
    :cond_10
    invoke-virtual/range {v18 .. v18}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 2753
    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_12

    .line 2754
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2755
    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lyp;->d(Lyt;Ljava/lang/String;)V

    goto :goto_8

    .line 2719
    :cond_11
    const/4 v3, 0x6

    if-ne v1, v3, :cond_d

    .line 2722
    :try_start_1
    move-object/from16 v0, p0

    iget-wide v3, v0, Lbim;->i:J

    const/4 v1, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v1, v2}, Lyt;->a(JII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_7

    .line 2729
    :catchall_0
    move-exception v1

    invoke-virtual/range {p1 .. p1}, Lyt;->c()V

    throw v1

    .line 2758
    :cond_12
    invoke-virtual/range {v19 .. v19}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 2759
    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_13

    .line 2760
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2761
    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lyp;->c(Lyt;Ljava/lang/String;)V

    goto :goto_9

    .line 2763
    :cond_13
    return-void

    :cond_14
    move v13, v1

    move v14, v4

    goto/16 :goto_6

    :cond_15
    move v15, v9

    goto/16 :goto_4

    :cond_16
    move-wide/from16 v16, v2

    goto/16 :goto_3

    :cond_17
    move-wide v11, v1

    goto/16 :goto_0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 2766
    iget-object v0, p0, Lbim;->b:Lbea;

    if-eqz v0, :cond_0

    .line 2767
    iget-object v0, p0, Lbim;->b:Lbea;

    instance-of v0, v0, Lbft;

    invoke-static {v0}, Lcwz;->a(Z)V

    .line 2768
    iget-object v0, p0, Lbim;->b:Lbea;

    check-cast v0, Lbft;

    invoke-virtual {v0}, Lbft;->i()Z

    move-result v0

    .line 2770
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
