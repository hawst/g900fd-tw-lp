.class public final Lddi;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lddi;


# instance fields
.field public b:Ldcw;

.field public c:Leoe;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 807
    const/4 v0, 0x0

    new-array v0, v0, [Lddi;

    sput-object v0, Lddi;->a:[Lddi;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 808
    invoke-direct {p0}, Lepn;-><init>()V

    .line 811
    iput-object v0, p0, Lddi;->b:Ldcw;

    .line 814
    iput-object v0, p0, Lddi;->c:Leoe;

    .line 808
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 831
    const/4 v0, 0x0

    .line 832
    iget-object v1, p0, Lddi;->b:Ldcw;

    if-eqz v1, :cond_0

    .line 833
    const/4 v0, 0x1

    iget-object v1, p0, Lddi;->b:Ldcw;

    .line 834
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 836
    :cond_0
    iget-object v1, p0, Lddi;->c:Leoe;

    if-eqz v1, :cond_1

    .line 837
    const/4 v1, 0x2

    iget-object v2, p0, Lddi;->c:Leoe;

    .line 838
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 840
    :cond_1
    iget-object v1, p0, Lddi;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 841
    iput v0, p0, Lddi;->cachedSize:I

    .line 842
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 804
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lddi;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lddi;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lddi;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lddi;->b:Ldcw;

    if-nez v0, :cond_2

    new-instance v0, Ldcw;

    invoke-direct {v0}, Ldcw;-><init>()V

    iput-object v0, p0, Lddi;->b:Ldcw;

    :cond_2
    iget-object v0, p0, Lddi;->b:Ldcw;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lddi;->c:Leoe;

    if-nez v0, :cond_3

    new-instance v0, Leoe;

    invoke-direct {v0}, Leoe;-><init>()V

    iput-object v0, p0, Lddi;->c:Leoe;

    :cond_3
    iget-object v0, p0, Lddi;->c:Leoe;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 819
    iget-object v0, p0, Lddi;->b:Ldcw;

    if-eqz v0, :cond_0

    .line 820
    const/4 v0, 0x1

    iget-object v1, p0, Lddi;->b:Ldcw;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 822
    :cond_0
    iget-object v0, p0, Lddi;->c:Leoe;

    if-eqz v0, :cond_1

    .line 823
    const/4 v0, 0x2

    iget-object v1, p0, Lddi;->c:Leoe;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 825
    :cond_1
    iget-object v0, p0, Lddi;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 827
    return-void
.end method
