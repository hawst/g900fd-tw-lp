.class public abstract Lbal;
.super Lkj;
.source "PG"

# interfaces
.implements Landroid/view/View$OnCreateContextMenuListener;
.implements Landroid/widget/ExpandableListView$OnChildClickListener;
.implements Landroid/widget/ExpandableListView$OnGroupCollapseListener;
.implements Landroid/widget/ExpandableListView$OnGroupExpandListener;


# instance fields
.field o:Landroid/widget/ExpandableListAdapter;

.field p:Landroid/widget/ExpandableListView;

.field q:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 124
    invoke-direct {p0}, Lkj;-><init>()V

    .line 129
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbal;->q:Z

    return-void
.end method

.method private k()V
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lbal;->p:Landroid/widget/ExpandableListView;

    if-eqz v0, :cond_0

    .line 236
    :goto_0
    return-void

    .line 235
    :cond_0
    sget v0, Lf;->gD:I

    invoke-virtual {p0, v0}, Lbal;->setContentView(I)V

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/widget/ExpandableListAdapter;)V
    .locals 1

    .prologue
    .line 206
    monitor-enter p0

    .line 207
    :try_start_0
    invoke-direct {p0}, Lbal;->k()V

    .line 208
    iput-object p1, p0, Lbal;->o:Landroid/widget/ExpandableListAdapter;

    .line 209
    iget-object v0, p0, Lbal;->p:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, p1}, Landroid/widget/ExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 210
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public j()Landroid/widget/ExpandableListView;
    .locals 1

    .prologue
    .line 220
    invoke-direct {p0}, Lbal;->k()V

    .line 221
    iget-object v0, p0, Lbal;->p:Landroid/widget/ExpandableListView;

    return-object v0
.end method

.method public onChildClick(Landroid/widget/ExpandableListView;Landroid/view/View;IIJ)Z
    .locals 1

    .prologue
    .line 146
    const/4 v0, 0x0

    return v0
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 0

    .prologue
    .line 139
    return-void
.end method

.method public onGroupCollapse(I)V
    .locals 0

    .prologue
    .line 153
    return-void
.end method

.method public onGroupExpand(I)V
    .locals 0

    .prologue
    .line 159
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 169
    invoke-direct {p0}, Lbal;->k()V

    .line 170
    invoke-super {p0, p1}, Lkj;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 171
    return-void
.end method

.method public x_()V
    .locals 2

    .prologue
    .line 180
    invoke-super {p0}, Lkj;->x_()V

    .line 181
    sget v0, Lg;->bS:I

    invoke-virtual {p0, v0}, Lbal;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 182
    sget v0, Lg;->dZ:I

    invoke-virtual {p0, v0}, Lbal;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ExpandableListView;

    iput-object v0, p0, Lbal;->p:Landroid/widget/ExpandableListView;

    .line 183
    iget-object v0, p0, Lbal;->p:Landroid/widget/ExpandableListView;

    if-nez v0, :cond_0

    .line 184
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Your content must have a ExpandableListView whose id attribute is \'android.R.id.list\'"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 189
    :cond_0
    if-eqz v1, :cond_1

    .line 190
    iget-object v0, p0, Lbal;->p:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setEmptyView(Landroid/view/View;)V

    .line 192
    :cond_1
    iget-object v0, p0, Lbal;->p:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, p0}, Landroid/widget/ExpandableListView;->setOnChildClickListener(Landroid/widget/ExpandableListView$OnChildClickListener;)V

    .line 193
    iget-object v0, p0, Lbal;->p:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, p0}, Landroid/widget/ExpandableListView;->setOnGroupExpandListener(Landroid/widget/ExpandableListView$OnGroupExpandListener;)V

    .line 194
    iget-object v0, p0, Lbal;->p:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, p0}, Landroid/widget/ExpandableListView;->setOnGroupCollapseListener(Landroid/widget/ExpandableListView$OnGroupCollapseListener;)V

    .line 196
    iget-boolean v0, p0, Lbal;->q:Z

    if-eqz v0, :cond_2

    .line 197
    iget-object v0, p0, Lbal;->o:Landroid/widget/ExpandableListAdapter;

    invoke-virtual {p0, v0}, Lbal;->a(Landroid/widget/ExpandableListAdapter;)V

    .line 199
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbal;->q:Z

    .line 200
    return-void
.end method
