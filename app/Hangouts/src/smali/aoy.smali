.class public Laoy;
.super Lbor;
.source "PG"


# static fields
.field public static final a:Z


# instance fields
.field protected final b:Lyj;

.field protected final c:Ljava/lang/String;

.field protected final d:Ljava/lang/String;

.field protected e:Ljava/lang/String;

.field protected f:Z

.field protected g:Z

.field protected h:I

.field private final i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Laoz;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lbys;->e:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Laoy;->a:Z

    return-void
.end method

.method public constructor <init>(Lyj;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Lbor;-><init>()V

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Laoy;->i:Ljava/util/ArrayList;

    .line 52
    iput-object p1, p0, Laoy;->b:Lyj;

    .line 53
    iput-object p2, p0, Laoy;->c:Ljava/lang/String;

    .line 54
    iput-object p3, p0, Laoy;->d:Ljava/lang/String;

    .line 55
    const/4 v0, -0x1

    iput v0, p0, Laoy;->h:I

    .line 56
    invoke-static {p0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbor;)V

    .line 57
    return-void
.end method


# virtual methods
.method protected a(I)V
    .locals 3

    .prologue
    .line 104
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Broadcast removed: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Laoy;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    const/4 v0, 0x1

    iput-boolean v0, p0, Laoy;->g:Z

    .line 106
    invoke-virtual {p0}, Laoy;->j()V

    .line 107
    return-void
.end method

.method protected a(ILbga;)V
    .locals 3

    .prologue
    .line 87
    invoke-virtual {p2}, Lbga;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laoy;->e:Ljava/lang/String;

    .line 88
    invoke-virtual {p0}, Laoy;->h()V

    .line 89
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Broadcast created: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Laoy;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    return-void
.end method

.method protected a(ILbgn;)V
    .locals 3

    .prologue
    .line 94
    invoke-virtual {p2}, Lbgn;->f()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Laoy;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 95
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Broadcast retrieved: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Laoy;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    return-void
.end method

.method protected a(ILbhf;)V
    .locals 0

    .prologue
    .line 100
    return-void
.end method

.method public a(ILyj;Lbos;)V
    .locals 4

    .prologue
    .line 62
    invoke-virtual {p3}, Lbos;->c()Lbfz;

    move-result-object v0

    .line 64
    sget-boolean v1, Laoy;->a:Z

    if-eqz v1, :cond_0

    .line 65
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Received: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    :cond_0
    iget v1, p0, Laoy;->h:I

    if-ne p1, v1, :cond_1

    .line 69
    const/4 v1, -0x1

    iput v1, p0, Laoy;->h:I

    .line 71
    instance-of v1, v0, Lbga;

    if-eqz v1, :cond_2

    .line 72
    check-cast v0, Lbga;

    invoke-virtual {p0, p1, v0}, Laoy;->a(ILbga;)V

    .line 83
    :cond_1
    :goto_0
    return-void

    .line 73
    :cond_2
    instance-of v1, v0, Lbgn;

    if-eqz v1, :cond_3

    .line 74
    check-cast v0, Lbgn;

    invoke-virtual {p0, p1, v0}, Laoy;->a(ILbgn;)V

    goto :goto_0

    .line 75
    :cond_3
    instance-of v1, v0, Lbhf;

    if-eqz v1, :cond_4

    .line 76
    check-cast v0, Lbhf;

    invoke-virtual {p0, p1, v0}, Laoy;->a(ILbhf;)V

    goto :goto_0

    .line 77
    :cond_4
    instance-of v0, v0, Lbho;

    if-eqz v0, :cond_5

    .line 78
    invoke-virtual {p0, p1}, Laoy;->a(I)V

    goto :goto_0

    .line 80
    :cond_5
    const-string v0, "Request id matched but not to an expected type of response."

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Laoz;)V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Laoy;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 119
    iget-object v0, p0, Laoy;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 121
    :cond_0
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Laoy;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 111
    invoke-static {p0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lbor;)V

    .line 112
    return-void
.end method

.method public g()V
    .locals 3

    .prologue
    .line 132
    iget v0, p0, Laoy;->h:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 133
    iget-object v0, p0, Laoy;->e:Ljava/lang/String;

    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 134
    iget-boolean v0, p0, Laoy;->f:Z

    invoke-static {v0}, Lcwz;->a(Z)V

    .line 135
    iget-boolean v0, p0, Laoy;->g:Z

    invoke-static {v0}, Lcwz;->b(Z)V

    .line 136
    iget-object v0, p0, Laoy;->b:Lyj;

    iget-object v1, p0, Laoy;->e:Ljava/lang/String;

    iget-object v2, p0, Laoy;->c:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lyj;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Laoy;->h:I

    .line 137
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Stopping broadcast. Request ID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Laoy;->h:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    return-void
.end method

.method protected h()V
    .locals 3

    .prologue
    .line 141
    iget-object v0, p0, Laoy;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    .line 142
    iget-object v2, p0, Laoy;->e:Ljava/lang/String;

    invoke-interface {v0}, Laoz;->a()V

    goto :goto_0

    .line 144
    :cond_0
    return-void
.end method

.method protected i()V
    .locals 2

    .prologue
    .line 147
    iget-object v0, p0, Laoy;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    .line 148
    invoke-interface {v0, p0}, Laoz;->a(Laoy;)V

    goto :goto_0

    .line 150
    :cond_0
    return-void
.end method

.method protected j()V
    .locals 2

    .prologue
    .line 153
    iget-object v0, p0, Laoy;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    .line 154
    invoke-interface {v0, p0}, Laoz;->b(Laoy;)V

    goto :goto_0

    .line 156
    :cond_0
    return-void
.end method

.method protected k()V
    .locals 2

    .prologue
    .line 159
    iget-object v0, p0, Laoy;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    .line 160
    invoke-interface {v0, p0}, Laoz;->c(Laoy;)V

    goto :goto_0

    .line 162
    :cond_0
    return-void
.end method
