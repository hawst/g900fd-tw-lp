.class public final Lwn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field private a:Lwl;

.field private b:Lwk;


# direct methods
.method public constructor <init>(Lwl;Lwk;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lwn;->a:Lwl;

    .line 16
    iput-object p2, p0, Lwn;->b:Lwk;

    .line 17
    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lwn;->a:Lwl;

    iget-object v1, p0, Lwn;->b:Lwk;

    invoke-interface {v0, v1}, Lwl;->onAnimationCancel(Lwk;)V

    .line 32
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 26
    iget-object v0, p0, Lwn;->a:Lwl;

    iget-object v1, p0, Lwn;->b:Lwk;

    invoke-interface {v0, v1}, Lwl;->onAnimationEnd(Lwk;)V

    .line 27
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lwn;->a:Lwl;

    iget-object v1, p0, Lwn;->b:Lwk;

    invoke-interface {v0, v1}, Lwl;->onAnimationRepeat(Lwk;)V

    .line 37
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 21
    iget-object v0, p0, Lwn;->a:Lwl;

    iget-object v1, p0, Lwn;->b:Lwk;

    invoke-interface {v0, v1}, Lwl;->onAnimationStart(Lwk;)V

    .line 22
    return-void
.end method
