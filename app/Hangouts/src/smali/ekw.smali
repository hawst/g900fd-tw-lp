.class public final Lekw;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lekw;


# instance fields
.field public b:Ljava/lang/Boolean;

.field public c:Lekr;

.field public d:Lekv;

.field public e:Lekq;

.field public f:Lelm;

.field public g:Lelz;

.field public h:Lekh;

.field public i:Lelu;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lekw;

    sput-object v0, Lekw;->a:[Lekw;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 15
    iput-object v0, p0, Lekw;->c:Lekr;

    .line 18
    iput-object v0, p0, Lekw;->d:Lekv;

    .line 21
    iput-object v0, p0, Lekw;->e:Lekq;

    .line 24
    iput-object v0, p0, Lekw;->f:Lelm;

    .line 27
    iput-object v0, p0, Lekw;->g:Lelz;

    .line 30
    iput-object v0, p0, Lekw;->h:Lekh;

    .line 33
    iput-object v0, p0, Lekw;->i:Lelu;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 68
    const/4 v0, 0x0

    .line 69
    iget-object v1, p0, Lekw;->c:Lekr;

    if-eqz v1, :cond_0

    .line 70
    const/4 v0, 0x1

    iget-object v1, p0, Lekw;->c:Lekr;

    .line 71
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 73
    :cond_0
    iget-object v1, p0, Lekw;->d:Lekv;

    if-eqz v1, :cond_1

    .line 74
    const/4 v1, 0x2

    iget-object v2, p0, Lekw;->d:Lekv;

    .line 75
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 77
    :cond_1
    iget-object v1, p0, Lekw;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 78
    const/4 v1, 0x3

    iget-object v2, p0, Lekw;->b:Ljava/lang/Boolean;

    .line 79
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 81
    :cond_2
    iget-object v1, p0, Lekw;->e:Lekq;

    if-eqz v1, :cond_3

    .line 82
    const/4 v1, 0x4

    iget-object v2, p0, Lekw;->e:Lekq;

    .line 83
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 85
    :cond_3
    iget-object v1, p0, Lekw;->f:Lelm;

    if-eqz v1, :cond_4

    .line 86
    const/4 v1, 0x5

    iget-object v2, p0, Lekw;->f:Lelm;

    .line 87
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 89
    :cond_4
    iget-object v1, p0, Lekw;->g:Lelz;

    if-eqz v1, :cond_5

    .line 90
    const/4 v1, 0x6

    iget-object v2, p0, Lekw;->g:Lelz;

    .line 91
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 93
    :cond_5
    iget-object v1, p0, Lekw;->h:Lekh;

    if-eqz v1, :cond_6

    .line 94
    const/4 v1, 0x7

    iget-object v2, p0, Lekw;->h:Lekh;

    .line 95
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 97
    :cond_6
    iget-object v1, p0, Lekw;->i:Lelu;

    if-eqz v1, :cond_7

    .line 98
    const/16 v1, 0x8

    iget-object v2, p0, Lekw;->i:Lelu;

    .line 99
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 101
    :cond_7
    iget-object v1, p0, Lekw;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 102
    iput v0, p0, Lekw;->cachedSize:I

    .line 103
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lekw;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lekw;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lekw;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lekw;->c:Lekr;

    if-nez v0, :cond_2

    new-instance v0, Lekr;

    invoke-direct {v0}, Lekr;-><init>()V

    iput-object v0, p0, Lekw;->c:Lekr;

    :cond_2
    iget-object v0, p0, Lekw;->c:Lekr;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lekw;->d:Lekv;

    if-nez v0, :cond_3

    new-instance v0, Lekv;

    invoke-direct {v0}, Lekv;-><init>()V

    iput-object v0, p0, Lekw;->d:Lekv;

    :cond_3
    iget-object v0, p0, Lekw;->d:Lekv;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lekw;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lekw;->e:Lekq;

    if-nez v0, :cond_4

    new-instance v0, Lekq;

    invoke-direct {v0}, Lekq;-><init>()V

    iput-object v0, p0, Lekw;->e:Lekq;

    :cond_4
    iget-object v0, p0, Lekw;->e:Lekq;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lekw;->f:Lelm;

    if-nez v0, :cond_5

    new-instance v0, Lelm;

    invoke-direct {v0}, Lelm;-><init>()V

    iput-object v0, p0, Lekw;->f:Lelm;

    :cond_5
    iget-object v0, p0, Lekw;->f:Lelm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lekw;->g:Lelz;

    if-nez v0, :cond_6

    new-instance v0, Lelz;

    invoke-direct {v0}, Lelz;-><init>()V

    iput-object v0, p0, Lekw;->g:Lelz;

    :cond_6
    iget-object v0, p0, Lekw;->g:Lelz;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lekw;->h:Lekh;

    if-nez v0, :cond_7

    new-instance v0, Lekh;

    invoke-direct {v0}, Lekh;-><init>()V

    iput-object v0, p0, Lekw;->h:Lekh;

    :cond_7
    iget-object v0, p0, Lekw;->h:Lekh;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lekw;->i:Lelu;

    if-nez v0, :cond_8

    new-instance v0, Lelu;

    invoke-direct {v0}, Lelu;-><init>()V

    iput-object v0, p0, Lekw;->i:Lelu;

    :cond_8
    iget-object v0, p0, Lekw;->i:Lelu;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lekw;->c:Lekr;

    if-eqz v0, :cond_0

    .line 39
    const/4 v0, 0x1

    iget-object v1, p0, Lekw;->c:Lekr;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 41
    :cond_0
    iget-object v0, p0, Lekw;->d:Lekv;

    if-eqz v0, :cond_1

    .line 42
    const/4 v0, 0x2

    iget-object v1, p0, Lekw;->d:Lekv;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 44
    :cond_1
    iget-object v0, p0, Lekw;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 45
    const/4 v0, 0x3

    iget-object v1, p0, Lekw;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 47
    :cond_2
    iget-object v0, p0, Lekw;->e:Lekq;

    if-eqz v0, :cond_3

    .line 48
    const/4 v0, 0x4

    iget-object v1, p0, Lekw;->e:Lekq;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 50
    :cond_3
    iget-object v0, p0, Lekw;->f:Lelm;

    if-eqz v0, :cond_4

    .line 51
    const/4 v0, 0x5

    iget-object v1, p0, Lekw;->f:Lelm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 53
    :cond_4
    iget-object v0, p0, Lekw;->g:Lelz;

    if-eqz v0, :cond_5

    .line 54
    const/4 v0, 0x6

    iget-object v1, p0, Lekw;->g:Lelz;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 56
    :cond_5
    iget-object v0, p0, Lekw;->h:Lekh;

    if-eqz v0, :cond_6

    .line 57
    const/4 v0, 0x7

    iget-object v1, p0, Lekw;->h:Lekh;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 59
    :cond_6
    iget-object v0, p0, Lekw;->i:Lelu;

    if-eqz v0, :cond_7

    .line 60
    const/16 v0, 0x8

    iget-object v1, p0, Lekw;->i:Lelu;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 62
    :cond_7
    iget-object v0, p0, Lekw;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 64
    return-void
.end method
