.class public final Lcom;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/gms/location/places/PlaceType;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/lang/String;

.field private c:Z

.field private d:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/gms/location/places/UserDataType;",
            ">;"
        }
    .end annotation
.end field

.field private e:[Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom;->a:Ljava/util/Collection;

    iput-object v1, p0, Lcom;->b:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom;->c:Z

    iput-object v1, p0, Lcom;->d:Ljava/util/Collection;

    iput-object v1, p0, Lcom;->e:[Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Lcom;-><init>()V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/location/places/PlaceFilter;)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom;->a:Ljava/util/Collection;

    iput-object v0, p0, Lcom;->b:Ljava/lang/String;

    iput-boolean v1, p0, Lcom;->c:Z

    iput-object v0, p0, Lcom;->d:Ljava/util/Collection;

    iput-object v0, p0, Lcom;->e:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/PlaceFilter;->b()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom;->a:Ljava/util/Collection;

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/PlaceFilter;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom;->b:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/PlaceFilter;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom;->c:Z

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/PlaceFilter;->e()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom;->d:Ljava/util/Collection;

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/PlaceFilter;->a()Ljava/util/Set;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom;->e:[Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/gms/location/places/PlaceFilter;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom;-><init>(Lcom/google/android/gms/location/places/PlaceFilter;)V

    return-void
.end method
