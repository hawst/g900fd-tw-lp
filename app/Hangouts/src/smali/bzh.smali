.class public final Lbzh;
.super Lbor;
.source "PG"


# static fields
.field private static final a:Z

.field private static volatile b:J

.field private static volatile c:J

.field private static volatile d:J

.field private static volatile e:Z

.field private static m:Lbzh;

.field private static final n:Ljava/lang/Object;


# instance fields
.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lbzk;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lbzm;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/lang/Thread;

.field private volatile k:Z

.field private final l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lbzl;",
            ">;"
        }
    .end annotation
.end field

.field private final o:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 61
    sget-object v0, Lbys;->r:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lbzh;->a:Z

    .line 67
    const-wide/32 v0, 0x1d4c0

    sput-wide v0, Lbzh;->b:J

    .line 70
    const-wide/32 v0, 0x493e0

    sput-wide v0, Lbzh;->c:J

    .line 73
    const-wide/32 v0, 0x2932e00

    sput-wide v0, Lbzh;->d:J

    .line 77
    const/4 v0, 0x1

    sput-boolean v0, Lbzh;->e:Z

    .line 105
    const/4 v0, 0x0

    sput-object v0, Lbzh;->m:Lbzh;

    .line 106
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lbzh;->n:Ljava/lang/Object;

    .line 632
    invoke-static {}, Lbzh;->f()V

    .line 635
    new-instance v0, Lbzj;

    invoke-direct {v0}, Lbzj;-><init>()V

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/Runnable;)V

    .line 641
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 201
    invoke-direct {p0}, Lbor;-><init>()V

    .line 83
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbzh;->f:Ljava/util/Map;

    .line 88
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lbzh;->g:Landroid/util/SparseArray;

    .line 92
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lbzh;->h:Ljava/util/Set;

    .line 95
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbzh;->i:Ljava/util/ArrayList;

    .line 99
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbzh;->k:Z

    .line 102
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbzh;->l:Ljava/util/ArrayList;

    .line 241
    new-instance v0, Lbzi;

    invoke-direct {v0, p0}, Lbzi;-><init>(Lbzh;)V

    iput-object v0, p0, Lbzh;->o:Ljava/lang/Runnable;

    .line 202
    return-void
.end method

.method private a(Ljava/lang/String;Lbdn;)V
    .locals 3

    .prologue
    .line 600
    sget-boolean v0, Lbzh;->a:Z

    if-eqz v0, :cond_0

    .line 601
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Broadcasting presence. gaiaId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", presence="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 605
    :cond_0
    iget-object v0, p0, Lbzh;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzl;

    .line 606
    invoke-interface {v0, p1, p2}, Lbzl;->a(Ljava/lang/String;Lbdn;)V

    goto :goto_0

    .line 608
    :cond_1
    return-void
.end method

.method static synthetic a(Lbzh;)Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lbzh;->k:Z

    return v0
.end method

.method public static b()Lbzh;
    .locals 2

    .prologue
    .line 205
    sget-object v1, Lbzh;->n:Ljava/lang/Object;

    monitor-enter v1

    .line 206
    :try_start_0
    sget-object v0, Lbzh;->m:Lbzh;

    if-nez v0, :cond_0

    .line 207
    new-instance v0, Lbzh;

    invoke-direct {v0}, Lbzh;-><init>()V

    sput-object v0, Lbzh;->m:Lbzh;

    .line 209
    :cond_0
    sget-object v0, Lbzh;->m:Lbzh;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 210
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic b(Lbzh;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lbzh;->i:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic c(Lbzh;)Landroid/util/SparseArray;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lbzh;->g:Landroid/util/SparseArray;

    return-object v0
.end method

.method static synthetic c()V
    .locals 0

    .prologue
    .line 59
    invoke-static {}, Lbzh;->f()V

    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 232
    iget-object v0, p0, Lbzh;->j:Ljava/lang/Thread;

    if-nez v0, :cond_0

    .line 233
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lbzh;->o:Ljava/lang/Runnable;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lbzh;->j:Ljava/lang/Thread;

    .line 234
    iget-object v0, p0, Lbzh;->j:Ljava/lang/Thread;

    const-string v1, "presence_fetcher"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 235
    iget-object v0, p0, Lbzh;->j:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 237
    :cond_0
    return-void
.end method

.method static synthetic d(Lbzh;)Z
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbzh;->k:Z

    return v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 587
    sget-boolean v0, Lbzh;->a:Z

    if-eqz v0, :cond_0

    .line 588
    const-string v0, "Babel"

    const-string v1, "Update presence based on server results"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 591
    :cond_0
    iget-object v0, p0, Lbzh;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzl;

    .line 592
    invoke-interface {v0}, Lbzl;->f_()V

    goto :goto_0

    .line 594
    :cond_1
    return-void
.end method

.method private static f()V
    .locals 6

    .prologue
    const-wide/32 v4, 0xea60

    .line 612
    const-string v0, "babel_pc_availability_low_mark"

    const/16 v1, 0x78

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    sput-wide v0, Lbzh;->b:J

    .line 616
    const-string v0, "babel_pc_low_mark"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    int-to-long v0, v0

    mul-long/2addr v0, v4

    sput-wide v0, Lbzh;->c:J

    .line 620
    const-string v0, "babel_pc_high_mark"

    const/16 v1, 0x2d0

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    int-to-long v0, v0

    mul-long/2addr v0, v4

    sput-wide v0, Lbzh;->d:J

    .line 624
    const-string v0, "babel_richstatus"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lbzh;->e:Z

    .line 627
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)J
    .locals 4

    .prologue
    .line 466
    iget-object v0, p0, Lbzh;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzk;

    .line 467
    if-eqz v0, :cond_0

    .line 469
    invoke-virtual {v0}, Lbzk;->d()Ljava/lang/Long;

    move-result-object v0

    .line 468
    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v0

    .line 470
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 471
    sub-long v0, v2, v0

    .line 473
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lyj;)Lbdn;
    .locals 13

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 402
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, v3

    .line 460
    :cond_0
    :goto_0
    return-object v0

    .line 406
    :cond_1
    if-nez p2, :cond_2

    .line 407
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "EsAccount parameter is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 410
    :cond_2
    invoke-static {p2}, Lbtf;->a(Lyj;)Z

    move-result v4

    .line 412
    const/4 v1, 0x1

    .line 415
    iget-object v0, p0, Lbzh;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzk;

    .line 416
    if-eqz v0, :cond_8

    .line 418
    invoke-virtual {v0}, Lbzk;->b()Ljava/lang/Long;

    move-result-object v5

    .line 417
    invoke-static {v5}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v5

    .line 420
    invoke-virtual {v0}, Lbzk;->c()Ljava/lang/Long;

    move-result-object v7

    .line 419
    invoke-static {v7}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v7

    .line 421
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v9

    .line 424
    sget-wide v11, Lbzh;->c:J

    add-long/2addr v11, v5

    cmp-long v11, v9, v11

    if-gtz v11, :cond_4

    if-eqz v4, :cond_3

    sget-wide v11, Lbzh;->b:J

    add-long/2addr v7, v11

    cmp-long v4, v9, v7

    if-gtz v4, :cond_4

    :cond_3
    move v1, v2

    .line 430
    :cond_4
    sget-wide v7, Lbzh;->d:J

    add-long v4, v5, v7

    cmp-long v2, v9, v4

    if-gtz v2, :cond_7

    .line 431
    invoke-virtual {v0}, Lbzk;->a()Lbdn;

    move-result-object v3

    move-object v0, v3

    .line 438
    :goto_1
    if-eqz v1, :cond_6

    .line 439
    iget-object v1, p0, Lbzh;->i:Ljava/util/ArrayList;

    monitor-enter v1

    .line 440
    :try_start_0
    iget-object v2, p0, Lbzh;->h:Ljava/util/Set;

    invoke-interface {v2, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 442
    new-instance v2, Lbzm;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lbzm;-><init>(B)V

    .line 444
    iput-object p1, v2, Lbzm;->b:Ljava/lang/String;

    .line 445
    iput-object p2, v2, Lbzm;->a:Lyj;

    .line 446
    iget-object v3, p0, Lbzh;->h:Ljava/util/Set;

    invoke-interface {v3, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 447
    iget-object v3, p0, Lbzh;->i:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 448
    iget-object v2, p0, Lbzh;->i:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/lang/Object;->notify()V

    .line 450
    :cond_5
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 451
    invoke-direct {p0}, Lbzh;->d()V

    .line 454
    :cond_6
    if-eqz v0, :cond_0

    iget-boolean v1, v0, Lbdn;->a:Z

    if-nez v1, :cond_0

    .line 455
    sget-boolean v1, Lbzh;->a:Z

    if-eqz v1, :cond_0

    .line 456
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Returning unreachable presence for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 433
    :cond_7
    iget-object v0, p0, Lbzh;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_8
    move-object v0, v3

    goto :goto_1

    .line 450
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public a(ILyj;Lbos;)V
    .locals 8

    .prologue
    .line 522
    invoke-virtual {p3}, Lbos;->c()Lbfz;

    move-result-object v1

    .line 525
    monitor-enter p0

    .line 528
    :try_start_0
    iget-object v0, p0, Lbzh;->g:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 529
    if-nez v0, :cond_1

    .line 530
    monitor-exit p0

    .line 581
    :cond_0
    :goto_0
    return-void

    .line 532
    :cond_1
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 533
    iget-object v0, p0, Lbzh;->g:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 534
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 537
    const/4 v2, 0x0

    .line 539
    instance-of v0, v1, Lbhl;

    if-eqz v0, :cond_6

    move-object v0, v1

    .line 540
    check-cast v0, Lbhl;

    .line 541
    invoke-virtual {v0}, Lbhl;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbju;

    .line 542
    if-eqz v0, :cond_2

    .line 543
    iget-object v2, v0, Lbju;->a:Lbdk;

    iget-object v2, v2, Lbdk;->a:Ljava/lang/String;

    .line 546
    invoke-interface {v3, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 549
    iget-object v5, p0, Lbzh;->i:Ljava/util/ArrayList;

    monitor-enter v5

    .line 550
    :try_start_1
    iget-object v6, p0, Lbzh;->h:Ljava/util/Set;

    invoke-interface {v6, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 551
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 553
    iget-object v5, v0, Lbju;->b:Lbdn;

    if-eqz v5, :cond_5

    .line 555
    iget-object v5, v0, Lbju;->b:Lbdn;

    sget-boolean v1, Lbzh;->a:Z

    if-eqz v1, :cond_3

    const-string v1, "Babel"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Caching presence. gaiaId="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", presence="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v1, p0, Lbzh;->f:Ljava/util/Map;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbzk;

    if-nez v1, :cond_4

    new-instance v1, Lbzk;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-direct {v1, p0, v5, v6}, Lbzk;-><init>(Lbzh;Lbdn;Ljava/lang/Long;)V

    :goto_2
    iget-object v5, p0, Lbzh;->f:Ljava/util/Map;

    invoke-interface {v5, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 556
    iget-object v0, v0, Lbju;->b:Lbdn;

    invoke-direct {p0, v2, v0}, Lbzh;->a(Ljava/lang/String;Lbdn;)V

    .line 558
    const/4 v0, 0x1

    move v1, v0

    goto :goto_1

    .line 534
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 551
    :catchall_1
    move-exception v0

    monitor-exit v5

    throw v0

    .line 555
    :cond_4
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Lbzk;->a(Lbdn;Ljava/lang/Long;)V

    goto :goto_2

    .line 561
    :cond_5
    const-string v2, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Server skipped returning presence for"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lbju;->a:Lbdk;

    iget-object v0, v0, Lbdk;->a:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_6
    move v1, v2

    .line 570
    :cond_7
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 571
    const-string v3, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Server skipped returning presence for"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 572
    iget-object v3, p0, Lbzh;->i:Ljava/util/ArrayList;

    monitor-enter v3

    .line 573
    :try_start_2
    iget-object v4, p0, Lbzh;->h:Ljava/util/Set;

    invoke-interface {v4, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 574
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_3

    :catchall_2
    move-exception v0

    monitor-exit v3

    throw v0

    .line 575
    :cond_8
    if-eqz v1, :cond_0

    .line 579
    invoke-direct {p0}, Lbzh;->e()V

    goto/16 :goto_0
.end method

.method public a(Lbzl;)V
    .locals 2

    .prologue
    .line 214
    iget-object v0, p0, Lbzh;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 215
    iget-object v0, p0, Lbzh;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 216
    invoke-static {p0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbor;)V

    .line 218
    :cond_0
    return-void
.end method

.method public a(Ljava/util/List;Lyj;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lyj;",
            ")V"
        }
    .end annotation

    .prologue
    .line 305
    if-nez p2, :cond_0

    .line 306
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "EsAccount parameter is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 309
    :cond_0
    sget-boolean v0, Lbzh;->a:Z

    if-eqz v0, :cond_1

    .line 310
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "refresh presence for account: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    :cond_1
    invoke-static {p2}, Lbtf;->a(Lyj;)Z

    move-result v3

    .line 315
    iget-object v4, p0, Lbzh;->i:Ljava/util/ArrayList;

    monitor-enter v4

    .line 316
    const/4 v0, 0x0

    .line 317
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v0

    :cond_2
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 318
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 320
    if-eqz v3, :cond_3

    iget-object v1, p0, Lbzh;->f:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbzk;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lbzk;->c()Ljava/lang/Long;

    move-result-object v1

    invoke-static {v1}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v6

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    sget-wide v10, Lbzh;->b:J

    add-long/2addr v10, v6

    cmp-long v1, v8, v10

    if-gtz v1, :cond_4

    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_2

    .line 324
    :cond_3
    iget-object v1, p0, Lbzh;->f:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbzk;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lbzk;->b()Ljava/lang/Long;

    move-result-object v1

    invoke-static {v1}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v6

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    sget-wide v10, Lbzh;->c:J

    add-long/2addr v10, v6

    cmp-long v1, v8, v10

    if-gtz v1, :cond_6

    const/4 v1, 0x1

    :goto_2
    if-nez v1, :cond_2

    .line 325
    iget-object v1, p0, Lbzh;->h:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 329
    add-int/lit8 v1, v2, 0x1

    .line 334
    iget-object v2, p0, Lbzh;->h:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 335
    new-instance v2, Lbzm;

    const/4 v6, 0x0

    invoke-direct {v2, v6}, Lbzm;-><init>(B)V

    .line 336
    iput-object v0, v2, Lbzm;->b:Ljava/lang/String;

    .line 337
    iput-object p2, v2, Lbzm;->a:Lyj;

    .line 339
    iget-object v0, p0, Lbzh;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v2, v1

    .line 340
    goto :goto_0

    .line 320
    :cond_4
    sget-wide v10, Lbzh;->d:J

    add-long/2addr v6, v10

    cmp-long v1, v8, v6

    if-lez v1, :cond_5

    iget-object v1, p0, Lbzh;->f:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    const/4 v1, 0x0

    goto :goto_1

    .line 324
    :cond_6
    sget-wide v10, Lbzh;->d:J

    add-long/2addr v6, v10

    cmp-long v1, v8, v6

    if-lez v1, :cond_7

    iget-object v1, p0, Lbzh;->f:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_7
    const/4 v1, 0x0

    goto :goto_2

    .line 342
    :cond_8
    if-lez v2, :cond_9

    .line 343
    invoke-direct {p0}, Lbzh;->d()V

    .line 345
    iget-object v0, p0, Lbzh;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 347
    :cond_9
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0
.end method

.method public b(Lbzl;)V
    .locals 3

    .prologue
    .line 221
    iget-object v0, p0, Lbzh;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 222
    iget-object v0, p0, Lbzh;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 223
    invoke-static {p0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lbor;)V

    .line 224
    sget-object v1, Lbzh;->n:Ljava/lang/Object;

    monitor-enter v1

    .line 225
    :try_start_0
    iget-object v2, p0, Lbzh;->i:Ljava/util/ArrayList;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lbzh;->k:Z

    iget-object v0, p0, Lbzh;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    const/4 v0, 0x0

    iput-object v0, p0, Lbzh;->j:Ljava/lang/Thread;

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 226
    const/4 v0, 0x0

    :try_start_2
    sput-object v0, Lbzh;->m:Lbzh;

    .line 227
    monitor-exit v1

    .line 229
    :cond_0
    return-void

    .line 225
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 227
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public b(Ljava/lang/String;Lyj;)V
    .locals 3

    .prologue
    .line 479
    sget-boolean v0, Lbzh;->e:Z

    if-nez v0, :cond_1

    .line 480
    sget-boolean v0, Lbzh;->a:Z

    if-eqz v0, :cond_0

    .line 481
    const-string v0, "Babel"

    const-string v1, "Requesting rich presence when it\'s disabled."

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    :cond_0
    :goto_0
    return-void

    .line 488
    :cond_1
    const-string v0, "_sms_only_account"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 489
    const-string v0, "Requested rich presence for SMS account."

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 492
    :cond_2
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 493
    const-string v0, "Requested rich presence for non-Gaia account."

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 496
    :cond_3
    const/16 v0, 0x2b

    invoke-static {p1, v0}, Landroid/text/TextUtils;->indexOf(Ljava/lang/CharSequence;C)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 505
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 506
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 511
    const/4 v1, 0x1

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ljava/util/ArrayList;Z)I

    move-result v1

    .line 513
    monitor-enter p0

    .line 514
    :try_start_0
    iget-object v2, p0, Lbzh;->g:Landroid/util/SparseArray;

    invoke-virtual {v2, v1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 515
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
