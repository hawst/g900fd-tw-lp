.class public final Lazd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/phone/DebugActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/phone/DebugActivity;)V
    .locals 0

    .prologue
    .line 823
    iput-object p1, p0, Lazd;->a:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 826
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 828
    iget-object v1, p0, Lazd;->a:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/phone/DebugActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lf;->eZ:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    .line 829
    const v1, 0x102000a

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 831
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v10

    .line 832
    invoke-virtual {v10}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    .line 833
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Bundle;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    .line 832
    invoke-interface {v2, v3}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    .line 834
    array-length v3, v2

    new-array v6, v3, [Ljava/lang/Object;

    .line 836
    const/4 v4, 0x0

    .line 837
    array-length v11, v2

    const/4 v3, 0x0

    move v8, v3

    :goto_0
    if-ge v8, v11, :cond_4

    aget-object v5, v2, v8

    .line 838
    invoke-virtual {v10, v5}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 839
    const-string v7, "server_response"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 841
    invoke-virtual {v10, v5}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->c([B)Lbfz;

    move-result-object v3

    .line 842
    add-int/lit8 v5, v4, 0x1

    move v7, v5

    move-object v5, v6

    .line 855
    :goto_1
    aput-object v3, v5, v4

    .line 837
    add-int/lit8 v3, v8, 0x1

    move v8, v3

    move v4, v7

    goto :goto_0

    .line 843
    :cond_0
    const-string v7, "server_request"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 845
    invoke-virtual {v10, v5}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->b([B)Lbea;

    move-result-object v3

    .line 846
    add-int/lit8 v5, v4, 0x1

    move v7, v5

    move-object v5, v6

    .line 847
    goto :goto_1

    :cond_1
    const-string v7, "payload"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 848
    const-string v3, "account_name"

    .line 849
    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 850
    invoke-static {v3}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v3

    .line 851
    const-string v5, "payload"

    .line 852
    invoke-virtual {v10, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 851
    invoke-static {v5, v3}, Lbiq;->a(Ljava/lang/String;Lyj;)Ljava/util/List;

    move-result-object v3

    .line 853
    add-int/lit8 v5, v4, 0x1

    move v7, v5

    move-object v5, v6

    .line 854
    goto :goto_1

    .line 855
    :cond_2
    add-int/lit8 v5, v4, 0x1

    if-nez v3, :cond_3

    const-string v3, "NULL"

    move v7, v5

    move-object v5, v6

    goto :goto_1

    :cond_3
    move v7, v5

    move-object v5, v6

    goto :goto_1

    .line 859
    :cond_4
    iget-object v0, p0, Lazd;->a:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    invoke-static {v0, v2, v6}, Lcom/google/android/apps/hangouts/phone/DebugActivity;->a(Lcom/google/android/apps/hangouts/phone/DebugActivity;[Ljava/lang/String;[Ljava/lang/Object;)Landroid/widget/BaseAdapter;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 860
    iget-object v0, p0, Lazd;->a:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/DebugActivity;->j(Lcom/google/android/apps/hangouts/phone/DebugActivity;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 861
    iget-object v0, p0, Lazd;->a:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v9, v1, v2}, Lcom/google/android/apps/hangouts/phone/DebugActivity;->a(Lcom/google/android/apps/hangouts/phone/DebugActivity;Landroid/view/View;Landroid/database/Cursor;Ljava/lang/Runnable;)V

    .line 862
    return-void
.end method
