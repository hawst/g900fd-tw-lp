.class public final Leiv;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leiv;


# instance fields
.field public b:Ljava/lang/Float;

.field public c:Leix;

.field public d:Ljava/lang/Float;

.field public e:Ljava/lang/Float;

.field public f:[Leix;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 498
    const/4 v0, 0x0

    new-array v0, v0, [Leiv;

    sput-object v0, Leiv;->a:[Leiv;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 499
    invoke-direct {p0}, Lepn;-><init>()V

    .line 504
    const/4 v0, 0x0

    iput-object v0, p0, Leiv;->c:Leix;

    .line 511
    sget-object v0, Leix;->a:[Leix;

    iput-object v0, p0, Leiv;->f:[Leix;

    .line 499
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 542
    iget-object v0, p0, Leiv;->b:Ljava/lang/Float;

    if-eqz v0, :cond_5

    .line 543
    const/4 v0, 0x1

    iget-object v2, p0, Leiv;->b:Ljava/lang/Float;

    .line 544
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v0}, Lepl;->g(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 546
    :goto_0
    iget-object v2, p0, Leiv;->c:Leix;

    if-eqz v2, :cond_0

    .line 547
    const/4 v2, 0x2

    iget-object v3, p0, Leiv;->c:Leix;

    .line 548
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 550
    :cond_0
    iget-object v2, p0, Leiv;->d:Ljava/lang/Float;

    if-eqz v2, :cond_1

    .line 551
    const/4 v2, 0x3

    iget-object v3, p0, Leiv;->d:Ljava/lang/Float;

    .line 552
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 554
    :cond_1
    iget-object v2, p0, Leiv;->e:Ljava/lang/Float;

    if-eqz v2, :cond_2

    .line 555
    const/4 v2, 0x4

    iget-object v3, p0, Leiv;->e:Ljava/lang/Float;

    .line 556
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 558
    :cond_2
    iget-object v2, p0, Leiv;->f:[Leix;

    if-eqz v2, :cond_4

    .line 559
    iget-object v2, p0, Leiv;->f:[Leix;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 560
    if-eqz v4, :cond_3

    .line 561
    const/4 v5, 0x5

    .line 562
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 559
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 566
    :cond_4
    iget-object v1, p0, Leiv;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 567
    iput v0, p0, Leiv;->cachedSize:I

    .line 568
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 495
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Leiv;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Leiv;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Leiv;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Leiv;->b:Ljava/lang/Float;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Leiv;->c:Leix;

    if-nez v0, :cond_2

    new-instance v0, Leix;

    invoke-direct {v0}, Leix;-><init>()V

    iput-object v0, p0, Leiv;->c:Leix;

    :cond_2
    iget-object v0, p0, Leiv;->c:Leix;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Leiv;->d:Ljava/lang/Float;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Leiv;->e:Ljava/lang/Float;

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leiv;->f:[Leix;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Leix;

    iget-object v3, p0, Leiv;->f:[Leix;

    if-eqz v3, :cond_3

    iget-object v3, p0, Leiv;->f:[Leix;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Leiv;->f:[Leix;

    :goto_2
    iget-object v2, p0, Leiv;->f:[Leix;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Leiv;->f:[Leix;

    new-instance v3, Leix;

    invoke-direct {v3}, Leix;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leiv;->f:[Leix;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Leiv;->f:[Leix;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Leiv;->f:[Leix;

    new-instance v3, Leix;

    invoke-direct {v3}, Leix;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leiv;->f:[Leix;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x12 -> :sswitch_2
        0x1d -> :sswitch_3
        0x25 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 516
    iget-object v0, p0, Leiv;->b:Ljava/lang/Float;

    if-eqz v0, :cond_0

    .line 517
    const/4 v0, 0x1

    iget-object v1, p0, Leiv;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IF)V

    .line 519
    :cond_0
    iget-object v0, p0, Leiv;->c:Leix;

    if-eqz v0, :cond_1

    .line 520
    const/4 v0, 0x2

    iget-object v1, p0, Leiv;->c:Leix;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 522
    :cond_1
    iget-object v0, p0, Leiv;->d:Ljava/lang/Float;

    if-eqz v0, :cond_2

    .line 523
    const/4 v0, 0x3

    iget-object v1, p0, Leiv;->d:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IF)V

    .line 525
    :cond_2
    iget-object v0, p0, Leiv;->e:Ljava/lang/Float;

    if-eqz v0, :cond_3

    .line 526
    const/4 v0, 0x4

    iget-object v1, p0, Leiv;->e:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IF)V

    .line 528
    :cond_3
    iget-object v0, p0, Leiv;->f:[Leix;

    if-eqz v0, :cond_5

    .line 529
    iget-object v1, p0, Leiv;->f:[Leix;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 530
    if-eqz v3, :cond_4

    .line 531
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 529
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 535
    :cond_5
    iget-object v0, p0, Leiv;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 537
    return-void
.end method
