.class final Lou;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Loy;


# instance fields
.field final synthetic a:Lot;

.field private b:Landroid/app/AlertDialog;

.field private c:Landroid/widget/ListAdapter;

.field private d:Ljava/lang/CharSequence;


# direct methods
.method private constructor <init>(Lot;)V
    .locals 0

    .prologue
    .line 649
    iput-object p1, p0, Lou;->a:Lot;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lot;B)V
    .locals 0

    .prologue
    .line 649
    invoke-direct {p0, p1}, Lou;-><init>(Lot;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/widget/ListAdapter;)V
    .locals 0

    .prologue
    .line 664
    iput-object p1, p0, Lou;->c:Landroid/widget/ListAdapter;

    .line 665
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 668
    iput-object p1, p0, Lou;->d:Ljava/lang/CharSequence;

    .line 669
    return-void
.end method

.method public e()V
    .locals 3

    .prologue
    .line 676
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lou;->a:Lot;

    invoke-virtual {v1}, Lot;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 677
    iget-object v1, p0, Lou;->d:Ljava/lang/CharSequence;

    if-eqz v1, :cond_0

    .line 678
    iget-object v1, p0, Lou;->d:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 680
    :cond_0
    iget-object v1, p0, Lou;->c:Landroid/widget/ListAdapter;

    iget-object v2, p0, Lou;->a:Lot;

    invoke-virtual {v2}, Lot;->g()I

    move-result v2

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lou;->b:Landroid/app/AlertDialog;

    .line 682
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 655
    iget-object v0, p0, Lou;->b:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 656
    const/4 v0, 0x0

    iput-object v0, p0, Lou;->b:Landroid/app/AlertDialog;

    .line 657
    return-void
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 660
    iget-object v0, p0, Lou;->b:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lou;->b:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 685
    iget-object v0, p0, Lou;->a:Lot;

    invoke-virtual {v0, p2}, Lot;->a(I)V

    .line 686
    iget-object v0, p0, Lou;->a:Lot;

    iget-object v0, v0, Lot;->t:Lnw;

    if-eqz v0, :cond_0

    .line 687
    iget-object v0, p0, Lou;->a:Lot;

    const/4 v1, 0x0

    iget-object v2, p0, Lou;->c:Landroid/widget/ListAdapter;

    invoke-interface {v2, p2}, Landroid/widget/ListAdapter;->getItemId(I)J

    invoke-virtual {v0, v1, p2}, Lot;->a(Landroid/view/View;I)Z

    .line 689
    :cond_0
    invoke-virtual {p0}, Lou;->f()V

    .line 690
    return-void
.end method
