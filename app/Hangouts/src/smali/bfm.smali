.class public Lbfm;
.super Lbep;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 4276
    invoke-direct {p0}, Lbep;-><init>()V

    .line 4277
    iput-object p1, p0, Lbfm;->a:Ljava/lang/String;

    .line 4278
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 3

    .prologue
    .line 4282
    sget-boolean v0, Lbea;->d:Z

    if-eqz v0, :cond_0

    .line 4283
    const-string v0, "Babel_RequestWriter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SetMoodRequest build protobuf "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbfm;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4286
    :cond_0
    new-instance v0, Ldwk;

    invoke-direct {v0}, Ldwk;-><init>()V

    .line 4288
    new-instance v1, Ldud;

    invoke-direct {v1}, Ldud;-><init>()V

    .line 4289
    iget-object v2, p0, Lbfm;->a:Ljava/lang/String;

    iput-object v2, v1, Ldud;->b:Ljava/lang/String;

    .line 4291
    iput-object v1, v0, Ldwk;->f:Ldud;

    .line 4292
    invoke-virtual {p0, p1, p2}, Lbfm;->c(Ljava/lang/String;I)Ldvm;

    move-result-object v1

    iput-object v1, v0, Ldwk;->b:Ldvm;

    .line 4293
    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4298
    const-string v0, "presence/setpresence"

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4303
    const-string v0, "event_queue"

    return-object v0
.end method
