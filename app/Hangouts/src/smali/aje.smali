.class final Laje;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lbdk;

.field final synthetic b:Lajc;


# direct methods
.method constructor <init>(Lajc;Lbdk;)V
    .locals 0

    .prologue
    .line 897
    iput-object p1, p0, Laje;->b:Lajc;

    iput-object p2, p0, Laje;->a:Lbdk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 900
    iget-object v0, p0, Laje;->b:Lajc;

    iget-object v0, v0, Lajc;->j:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    iget-object v0, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->mCircleIdsMapping:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laje;->b:Lajc;

    iget-object v0, v0, Lajc;->j:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    iget-object v0, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->mMyCircles:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laje;->b:Lajc;

    iget-object v0, v0, Lajc;->j:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    .line 901
    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->e(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 929
    :cond_0
    :goto_0
    return-void

    .line 904
    :cond_1
    iget-object v0, p0, Laje;->a:Lbdk;

    iget-object v2, v0, Lbdk;->a:Ljava/lang/String;

    .line 905
    if-eqz v2, :cond_0

    .line 908
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 910
    iget-object v0, p0, Laje;->b:Lajc;

    iget-object v0, v0, Lajc;->j:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    iget-object v0, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->mCircleIdsMapping:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 911
    if-eqz v0, :cond_2

    .line 912
    iget-object v0, p0, Laje;->b:Lajc;

    iget-object v0, v0, Lajc;->j:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    iget-object v0, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->mCircleIdsMapping:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 913
    iget-object v1, p0, Laje;->b:Lajc;

    iget-object v1, v1, Lajc;->j:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    iget-object v1, v1, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->mMyCircles:Ljava/util/Map;

    .line 915
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 914
    invoke-static {v0, v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    .line 916
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 919
    :cond_2
    iget-object v0, p0, Laje;->b:Lajc;

    iget-object v0, v0, Lajc;->j:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    invoke-static {v0, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->a(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 921
    invoke-static {}, Lf;->z()Lchg;

    move-result-object v0

    iget-object v1, p0, Laje;->b:Lajc;

    iget-object v1, v1, Lajc;->j:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    .line 922
    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->c(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;)Lyj;

    move-result-object v1

    invoke-virtual {v1}, Lyj;->ak()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lchg;->c(Ljava/lang/String;)Lchg;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "g:"

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 923
    invoke-interface {v0, v1}, Lchg;->a(Ljava/lang/String;)Lchg;

    move-result-object v0

    .line 924
    invoke-interface {v0, v3}, Lchg;->a(Ljava/util/List;)Lchg;

    move-result-object v0

    iget-object v1, p0, Laje;->b:Lajc;

    iget-object v1, v1, Lajc;->j:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    .line 925
    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->c(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;)Lyj;

    move-result-object v1

    invoke-virtual {v1}, Lyj;->al()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lchg;->b(Ljava/lang/String;)Lchg;

    move-result-object v0

    .line 926
    invoke-interface {v0}, Lchg;->a()Landroid/content/Intent;

    move-result-object v0

    .line 927
    iget-object v1, p0, Laje;->b:Lajc;

    iget-object v1, v1, Lajc;->j:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    const/16 v2, 0x65

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 928
    iget-object v0, p0, Laje;->b:Lajc;

    iget-object v0, v0, Lajc;->j:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->f(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;)Z

    goto/16 :goto_0
.end method
