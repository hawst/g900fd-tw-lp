.class public final Lepg;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lepg;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Float;

.field public e:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 363
    const/4 v0, 0x0

    new-array v0, v0, [Lepg;

    sput-object v0, Lepg;->a:[Lepg;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 364
    invoke-direct {p0}, Lepn;-><init>()V

    .line 396
    const/4 v0, 0x0

    iput-object v0, p0, Lepg;->e:Ljava/lang/Integer;

    .line 364
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 419
    const/4 v0, 0x0

    .line 420
    iget-object v1, p0, Lepg;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 421
    const/4 v0, 0x1

    iget-object v1, p0, Lepg;->b:Ljava/lang/Integer;

    .line 422
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 424
    :cond_0
    iget-object v1, p0, Lepg;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 425
    const/4 v1, 0x2

    iget-object v2, p0, Lepg;->c:Ljava/lang/Integer;

    .line 426
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 428
    :cond_1
    iget-object v1, p0, Lepg;->d:Ljava/lang/Float;

    if-eqz v1, :cond_2

    .line 429
    const/4 v1, 0x3

    iget-object v2, p0, Lepg;->d:Ljava/lang/Float;

    .line 430
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 432
    :cond_2
    iget-object v1, p0, Lepg;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 433
    const/4 v1, 0x4

    iget-object v2, p0, Lepg;->e:Ljava/lang/Integer;

    .line 434
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 436
    :cond_3
    iget-object v1, p0, Lepg;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 437
    iput v0, p0, Lepg;->cachedSize:I

    .line 438
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 3

    .prologue
    const/16 v2, 0x2b

    .line 360
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lepg;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lepg;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lepg;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lepg;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lepg;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lepg;->d:Ljava/lang/Float;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_2

    const/16 v1, 0x2c

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_2

    const/16 v1, 0xdc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xdd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xde

    if-eq v0, v1, :cond_2

    const/16 v1, 0xdf

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe0

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe1

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe2

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe3

    if-eq v0, v1, :cond_2

    const/16 v1, 0x12c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x12e

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf0

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf1

    if-eq v0, v1, :cond_2

    const/16 v1, 0x138

    if-eq v0, v1, :cond_2

    const/16 v1, 0x13a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x13b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x13c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3a98

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lepg;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lepg;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1d -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 401
    iget-object v0, p0, Lepg;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 402
    const/4 v0, 0x1

    iget-object v1, p0, Lepg;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 404
    :cond_0
    iget-object v0, p0, Lepg;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 405
    const/4 v0, 0x2

    iget-object v1, p0, Lepg;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 407
    :cond_1
    iget-object v0, p0, Lepg;->d:Ljava/lang/Float;

    if-eqz v0, :cond_2

    .line 408
    const/4 v0, 0x3

    iget-object v1, p0, Lepg;->d:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IF)V

    .line 410
    :cond_2
    iget-object v0, p0, Lepg;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 411
    const/4 v0, 0x4

    iget-object v1, p0, Lepg;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 413
    :cond_3
    iget-object v0, p0, Lepg;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 415
    return-void
.end method
