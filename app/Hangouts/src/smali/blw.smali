.class abstract Lblw;
.super Lack;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Lbcq;",
        "S:",
        "Lbfz;",
        ">",
        "Lack",
        "<TR;TS;>;"
    }
.end annotation


# instance fields
.field protected final d:Ljava/lang/String;

.field private final e:Landroid/content/Context;

.field private final f:Lblx;

.field private final g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lblx;I)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lack;-><init>()V

    .line 65
    iput-object p1, p0, Lblw;->e:Landroid/content/Context;

    .line 66
    iput-object p2, p0, Lblw;->d:Ljava/lang/String;

    .line 67
    iput-object p3, p0, Lblw;->f:Lblx;

    .line 68
    iput p4, p0, Lblw;->g:I

    .line 69
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lblw;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lblw;->g:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lbos;)V
    .locals 1

    .prologue
    .line 73
    invoke-super {p0, p1}, Lack;->a(Lbos;)V

    .line 74
    iget-object v0, p0, Lblw;->f:Lblx;

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lblw;->f:Lblx;

    invoke-interface {v0}, Lblx;->a()V

    .line 77
    :cond_0
    return-void
.end method

.method protected a(Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 81
    invoke-super {p0, p1}, Lack;->a(Ljava/lang/Exception;)V

    .line 82
    iget-object v0, p0, Lblw;->f:Lblx;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lblw;->f:Lblx;

    invoke-interface {v0}, Lblx;->b()V

    .line 85
    :cond_0
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 94
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 98
    return-void
.end method
