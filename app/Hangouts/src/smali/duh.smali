.class public final Lduh;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lduh;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11191
    const/4 v0, 0x0

    new-array v0, v0, [Lduh;

    sput-object v0, Lduh;->a:[Lduh;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 11192
    invoke-direct {p0}, Lepn;-><init>()V

    .line 11195
    iput-object v0, p0, Lduh;->b:Ljava/lang/Integer;

    .line 11198
    iput-object v0, p0, Lduh;->c:Ljava/lang/Integer;

    .line 11201
    iput-object v0, p0, Lduh;->d:Ljava/lang/Integer;

    .line 11204
    iput-object v0, p0, Lduh;->e:Ljava/lang/Integer;

    .line 11192
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 11227
    const/4 v0, 0x0

    .line 11228
    iget-object v1, p0, Lduh;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 11229
    const/4 v0, 0x1

    iget-object v1, p0, Lduh;->b:Ljava/lang/Integer;

    .line 11230
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 11232
    :cond_0
    iget-object v1, p0, Lduh;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 11233
    const/4 v1, 0x2

    iget-object v2, p0, Lduh;->c:Ljava/lang/Integer;

    .line 11234
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11236
    :cond_1
    iget-object v1, p0, Lduh;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 11237
    const/4 v1, 0x3

    iget-object v2, p0, Lduh;->d:Ljava/lang/Integer;

    .line 11238
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11240
    :cond_2
    iget-object v1, p0, Lduh;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 11241
    const/4 v1, 0x4

    iget-object v2, p0, Lduh;->e:Ljava/lang/Integer;

    .line 11242
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11244
    :cond_3
    iget-object v1, p0, Lduh;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11245
    iput v0, p0, Lduh;->cachedSize:I

    .line 11246
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 11188
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lduh;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lduh;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lduh;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_2

    if-ne v0, v3, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lduh;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lduh;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_4

    if-ne v0, v3, :cond_5

    :cond_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lduh;->c:Ljava/lang/Integer;

    goto :goto_0

    :cond_5
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lduh;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_6

    if-ne v0, v3, :cond_7

    :cond_6
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lduh;->d:Ljava/lang/Integer;

    goto :goto_0

    :cond_7
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lduh;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_8

    if-ne v0, v3, :cond_9

    :cond_8
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lduh;->e:Ljava/lang/Integer;

    goto :goto_0

    :cond_9
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lduh;->e:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 11209
    iget-object v0, p0, Lduh;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 11210
    const/4 v0, 0x1

    iget-object v1, p0, Lduh;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 11212
    :cond_0
    iget-object v0, p0, Lduh;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 11213
    const/4 v0, 0x2

    iget-object v1, p0, Lduh;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 11215
    :cond_1
    iget-object v0, p0, Lduh;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 11216
    const/4 v0, 0x3

    iget-object v1, p0, Lduh;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 11218
    :cond_2
    iget-object v0, p0, Lduh;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 11219
    const/4 v0, 0x4

    iget-object v1, p0, Lduh;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 11221
    :cond_3
    iget-object v0, p0, Lduh;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 11223
    return-void
.end method
