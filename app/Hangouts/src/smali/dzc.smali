.class public final Ldzc;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldzc;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1351
    const/4 v0, 0x0

    new-array v0, v0, [Ldzc;

    sput-object v0, Ldzc;->a:[Ldzc;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1352
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1383
    const/4 v0, 0x0

    .line 1384
    iget-object v1, p0, Ldzc;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1385
    const/4 v0, 0x1

    iget-object v1, p0, Ldzc;->b:Ljava/lang/String;

    .line 1386
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1388
    :cond_0
    iget-object v1, p0, Ldzc;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1389
    const/4 v1, 0x2

    iget-object v2, p0, Ldzc;->c:Ljava/lang/String;

    .line 1390
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1392
    :cond_1
    iget-object v1, p0, Ldzc;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1393
    const/4 v1, 0x3

    iget-object v2, p0, Ldzc;->d:Ljava/lang/String;

    .line 1394
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1396
    :cond_2
    iget-object v1, p0, Ldzc;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1397
    const/4 v1, 0x4

    iget-object v2, p0, Ldzc;->e:Ljava/lang/String;

    .line 1398
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1400
    :cond_3
    iget-object v1, p0, Ldzc;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1401
    iput v0, p0, Ldzc;->cachedSize:I

    .line 1402
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 1348
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldzc;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldzc;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldzc;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldzc;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldzc;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldzc;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldzc;->e:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 1365
    iget-object v0, p0, Ldzc;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1366
    const/4 v0, 0x1

    iget-object v1, p0, Ldzc;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1368
    :cond_0
    iget-object v0, p0, Ldzc;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1369
    const/4 v0, 0x2

    iget-object v1, p0, Ldzc;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1371
    :cond_1
    iget-object v0, p0, Ldzc;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1372
    const/4 v0, 0x3

    iget-object v1, p0, Ldzc;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1374
    :cond_2
    iget-object v0, p0, Ldzc;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1375
    const/4 v0, 0x4

    iget-object v1, p0, Ldzc;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1377
    :cond_3
    iget-object v0, p0, Ldzc;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1379
    return-void
.end method
