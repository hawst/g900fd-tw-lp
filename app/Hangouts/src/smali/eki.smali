.class public final Leki;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leki;


# instance fields
.field public b:Lelh;

.field public c:Lelh;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 346
    const/4 v0, 0x0

    new-array v0, v0, [Leki;

    sput-object v0, Leki;->a:[Leki;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 347
    invoke-direct {p0}, Lepn;-><init>()V

    .line 350
    iput-object v0, p0, Leki;->b:Lelh;

    .line 353
    iput-object v0, p0, Leki;->c:Lelh;

    .line 347
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 370
    const/4 v0, 0x0

    .line 371
    iget-object v1, p0, Leki;->b:Lelh;

    if-eqz v1, :cond_0

    .line 372
    const/4 v0, 0x1

    iget-object v1, p0, Leki;->b:Lelh;

    .line 373
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 375
    :cond_0
    iget-object v1, p0, Leki;->c:Lelh;

    if-eqz v1, :cond_1

    .line 376
    const/4 v1, 0x2

    iget-object v2, p0, Leki;->c:Lelh;

    .line 377
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 379
    :cond_1
    iget-object v1, p0, Leki;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 380
    iput v0, p0, Leki;->cachedSize:I

    .line 381
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 343
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Leki;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Leki;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Leki;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Leki;->b:Lelh;

    if-nez v0, :cond_2

    new-instance v0, Lelh;

    invoke-direct {v0}, Lelh;-><init>()V

    iput-object v0, p0, Leki;->b:Lelh;

    :cond_2
    iget-object v0, p0, Leki;->b:Lelh;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Leki;->c:Lelh;

    if-nez v0, :cond_3

    new-instance v0, Lelh;

    invoke-direct {v0}, Lelh;-><init>()V

    iput-object v0, p0, Leki;->c:Lelh;

    :cond_3
    iget-object v0, p0, Leki;->c:Lelh;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 358
    iget-object v0, p0, Leki;->b:Lelh;

    if-eqz v0, :cond_0

    .line 359
    const/4 v0, 0x1

    iget-object v1, p0, Leki;->b:Lelh;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 361
    :cond_0
    iget-object v0, p0, Leki;->c:Lelh;

    if-eqz v0, :cond_1

    .line 362
    const/4 v0, 0x2

    iget-object v1, p0, Leki;->c:Lelh;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 364
    :cond_1
    iget-object v0, p0, Leki;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 366
    return-void
.end method
