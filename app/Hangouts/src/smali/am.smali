.class final Lam;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lam;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final a:Ljava/lang/String;

.field final b:I

.field final c:Z

.field final d:I

.field final e:I

.field final f:Ljava/lang/String;

.field final g:Z

.field final h:Z

.field final i:Landroid/os/Bundle;

.field j:Landroid/os/Bundle;

.field k:Lt;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 138
    new-instance v0, Lan;

    invoke-direct {v0}, Lan;-><init>()V

    sput-object v0, Lam;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lam;->a:Ljava/lang/String;

    .line 79
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lam;->b:I

    .line 80
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lam;->c:Z

    .line 81
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lam;->d:I

    .line 82
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lam;->e:I

    .line 83
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lam;->f:Ljava/lang/String;

    .line 84
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lam;->g:Z

    .line 85
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    :goto_2
    iput-boolean v1, p0, Lam;->h:Z

    .line 86
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lam;->i:Landroid/os/Bundle;

    .line 87
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lam;->j:Landroid/os/Bundle;

    .line 88
    return-void

    :cond_0
    move v0, v2

    .line 80
    goto :goto_0

    :cond_1
    move v0, v2

    .line 84
    goto :goto_1

    :cond_2
    move v1, v2

    .line 85
    goto :goto_2
.end method

.method public constructor <init>(Lt;)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lam;->a:Ljava/lang/String;

    .line 67
    iget v0, p1, Lt;->o:I

    iput v0, p0, Lam;->b:I

    .line 68
    iget-boolean v0, p1, Lt;->x:Z

    iput-boolean v0, p0, Lam;->c:Z

    .line 69
    iget v0, p1, Lt;->F:I

    iput v0, p0, Lam;->d:I

    .line 70
    iget v0, p1, Lt;->G:I

    iput v0, p0, Lam;->e:I

    .line 71
    iget-object v0, p1, Lt;->H:Ljava/lang/String;

    iput-object v0, p0, Lam;->f:Ljava/lang/String;

    .line 72
    iget-boolean v0, p1, Lt;->K:Z

    iput-boolean v0, p0, Lam;->g:Z

    .line 73
    iget-boolean v0, p1, Lt;->J:Z

    iput-boolean v0, p0, Lam;->h:Z

    .line 74
    iget-object v0, p1, Lt;->q:Landroid/os/Bundle;

    iput-object v0, p0, Lam;->i:Landroid/os/Bundle;

    .line 75
    return-void
.end method


# virtual methods
.method public a(Ly;Lt;)Lt;
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lam;->k:Lt;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lam;->k:Lt;

    .line 118
    :goto_0
    return-object v0

    .line 95
    :cond_0
    iget-object v0, p0, Lam;->i:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    .line 96
    iget-object v0, p0, Lam;->i:Landroid/os/Bundle;

    invoke-virtual {p1}, Ly;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 99
    :cond_1
    iget-object v0, p0, Lam;->a:Ljava/lang/String;

    iget-object v1, p0, Lam;->i:Landroid/os/Bundle;

    invoke-static {p1, v0, v1}, Lt;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Lt;

    move-result-object v0

    iput-object v0, p0, Lam;->k:Lt;

    .line 101
    iget-object v0, p0, Lam;->j:Landroid/os/Bundle;

    if-eqz v0, :cond_2

    .line 102
    iget-object v0, p0, Lam;->j:Landroid/os/Bundle;

    invoke-virtual {p1}, Ly;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 103
    iget-object v0, p0, Lam;->k:Lt;

    iget-object v1, p0, Lam;->j:Landroid/os/Bundle;

    iput-object v1, v0, Lt;->m:Landroid/os/Bundle;

    .line 105
    :cond_2
    iget-object v0, p0, Lam;->k:Lt;

    iget v1, p0, Lam;->b:I

    invoke-virtual {v0, v1, p2}, Lt;->a(ILt;)V

    .line 106
    iget-object v0, p0, Lam;->k:Lt;

    iget-boolean v1, p0, Lam;->c:Z

    iput-boolean v1, v0, Lt;->x:Z

    .line 107
    iget-object v0, p0, Lam;->k:Lt;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lt;->z:Z

    .line 108
    iget-object v0, p0, Lam;->k:Lt;

    iget v1, p0, Lam;->d:I

    iput v1, v0, Lt;->F:I

    .line 109
    iget-object v0, p0, Lam;->k:Lt;

    iget v1, p0, Lam;->e:I

    iput v1, v0, Lt;->G:I

    .line 110
    iget-object v0, p0, Lam;->k:Lt;

    iget-object v1, p0, Lam;->f:Ljava/lang/String;

    iput-object v1, v0, Lt;->H:Ljava/lang/String;

    .line 111
    iget-object v0, p0, Lam;->k:Lt;

    iget-boolean v1, p0, Lam;->g:Z

    iput-boolean v1, v0, Lt;->K:Z

    .line 112
    iget-object v0, p0, Lam;->k:Lt;

    iget-boolean v1, p0, Lam;->h:Z

    iput-boolean v1, v0, Lt;->J:Z

    .line 113
    iget-object v0, p0, Lam;->k:Lt;

    iget-object v1, p1, Ly;->b:Laf;

    iput-object v1, v0, Lt;->B:Laf;

    .line 115
    sget-boolean v0, Laf;->a:Z

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Instantiated fragment "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lam;->k:Lt;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 118
    :cond_3
    iget-object v0, p0, Lam;->k:Lt;

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 126
    iget-object v0, p0, Lam;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 127
    iget v0, p0, Lam;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 128
    iget-boolean v0, p0, Lam;->c:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 129
    iget v0, p0, Lam;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 130
    iget v0, p0, Lam;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 131
    iget-object v0, p0, Lam;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 132
    iget-boolean v0, p0, Lam;->g:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 133
    iget-boolean v0, p0, Lam;->h:Z

    if-eqz v0, :cond_2

    :goto_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 134
    iget-object v0, p0, Lam;->i:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 135
    iget-object v0, p0, Lam;->j:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 136
    return-void

    :cond_0
    move v0, v2

    .line 128
    goto :goto_0

    :cond_1
    move v0, v2

    .line 132
    goto :goto_1

    :cond_2
    move v1, v2

    .line 133
    goto :goto_2
.end method
