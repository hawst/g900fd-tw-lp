.class public final Ldin;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldin;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/String;

.field public e:Ldik;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/Double;

.field public i:Ldio;

.field public j:Ldil;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:[Ldij;

.field public o:Ljava/lang/Boolean;

.field public p:Ljava/lang/Boolean;

.field public q:Ljava/lang/String;

.field public r:Ljava/lang/String;

.field public s:Ljava/lang/String;

.field public t:Ljava/lang/Integer;

.field public u:Ljava/lang/Integer;

.field public v:[Ldim;

.field public w:Ljava/lang/Boolean;

.field public x:Ljava/lang/String;

.field public y:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 728
    const/4 v0, 0x0

    new-array v0, v0, [Ldin;

    sput-object v0, Ldin;->a:[Ldin;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 729
    invoke-direct {p0}, Lepn;-><init>()V

    .line 746
    iput-object v0, p0, Ldin;->c:Ljava/lang/Integer;

    .line 751
    iput-object v0, p0, Ldin;->e:Ldik;

    .line 760
    iput-object v0, p0, Ldin;->i:Ldio;

    .line 763
    iput-object v0, p0, Ldin;->j:Ldil;

    .line 772
    sget-object v0, Ldij;->a:[Ldij;

    iput-object v0, p0, Ldin;->n:[Ldij;

    .line 789
    sget-object v0, Ldim;->a:[Ldim;

    iput-object v0, p0, Ldin;->v:[Ldim;

    .line 729
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 887
    iget-object v0, p0, Ldin;->b:Ljava/lang/String;

    if-eqz v0, :cond_19

    .line 888
    const/4 v0, 0x1

    iget-object v2, p0, Ldin;->b:Ljava/lang/String;

    .line 889
    invoke-static {v0, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 891
    :goto_0
    iget-object v2, p0, Ldin;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 892
    const/4 v2, 0x2

    iget-object v3, p0, Ldin;->c:Ljava/lang/Integer;

    .line 893
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 895
    :cond_0
    iget-object v2, p0, Ldin;->d:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 896
    const/4 v2, 0x3

    iget-object v3, p0, Ldin;->d:Ljava/lang/String;

    .line 897
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 899
    :cond_1
    iget-object v2, p0, Ldin;->e:Ldik;

    if-eqz v2, :cond_2

    .line 900
    const/4 v2, 0x4

    iget-object v3, p0, Ldin;->e:Ldik;

    .line 901
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 903
    :cond_2
    iget-object v2, p0, Ldin;->f:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 904
    const/4 v2, 0x5

    iget-object v3, p0, Ldin;->f:Ljava/lang/String;

    .line 905
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 907
    :cond_3
    iget-object v2, p0, Ldin;->g:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 908
    const/4 v2, 0x6

    iget-object v3, p0, Ldin;->g:Ljava/lang/String;

    .line 909
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 911
    :cond_4
    iget-object v2, p0, Ldin;->h:Ljava/lang/Double;

    if-eqz v2, :cond_5

    .line 912
    const/4 v2, 0x7

    iget-object v3, p0, Ldin;->h:Ljava/lang/Double;

    .line 913
    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    add-int/2addr v0, v2

    .line 915
    :cond_5
    iget-object v2, p0, Ldin;->i:Ldio;

    if-eqz v2, :cond_6

    .line 916
    const/16 v2, 0x8

    iget-object v3, p0, Ldin;->i:Ldio;

    .line 917
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 919
    :cond_6
    iget-object v2, p0, Ldin;->j:Ldil;

    if-eqz v2, :cond_7

    .line 920
    const/16 v2, 0x9

    iget-object v3, p0, Ldin;->j:Ldil;

    .line 921
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 923
    :cond_7
    iget-object v2, p0, Ldin;->k:Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 924
    const/16 v2, 0xa

    iget-object v3, p0, Ldin;->k:Ljava/lang/String;

    .line 925
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 927
    :cond_8
    iget-object v2, p0, Ldin;->l:Ljava/lang/String;

    if-eqz v2, :cond_9

    .line 928
    const/16 v2, 0xb

    iget-object v3, p0, Ldin;->l:Ljava/lang/String;

    .line 929
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 931
    :cond_9
    iget-object v2, p0, Ldin;->m:Ljava/lang/String;

    if-eqz v2, :cond_a

    .line 932
    const/16 v2, 0xc

    iget-object v3, p0, Ldin;->m:Ljava/lang/String;

    .line 933
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 935
    :cond_a
    iget-object v2, p0, Ldin;->n:[Ldij;

    if-eqz v2, :cond_c

    .line 936
    iget-object v3, p0, Ldin;->n:[Ldij;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_c

    aget-object v5, v3, v2

    .line 937
    if-eqz v5, :cond_b

    .line 938
    const/16 v6, 0xd

    .line 939
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 936
    :cond_b
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 943
    :cond_c
    iget-object v2, p0, Ldin;->o:Ljava/lang/Boolean;

    if-eqz v2, :cond_d

    .line 944
    const/16 v2, 0xe

    iget-object v3, p0, Ldin;->o:Ljava/lang/Boolean;

    .line 945
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 947
    :cond_d
    iget-object v2, p0, Ldin;->q:Ljava/lang/String;

    if-eqz v2, :cond_e

    .line 948
    const/16 v2, 0xf

    iget-object v3, p0, Ldin;->q:Ljava/lang/String;

    .line 949
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 951
    :cond_e
    iget-object v2, p0, Ldin;->r:Ljava/lang/String;

    if-eqz v2, :cond_f

    .line 952
    const/16 v2, 0x10

    iget-object v3, p0, Ldin;->r:Ljava/lang/String;

    .line 953
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 955
    :cond_f
    iget-object v2, p0, Ldin;->t:Ljava/lang/Integer;

    if-eqz v2, :cond_10

    .line 956
    const/16 v2, 0x11

    iget-object v3, p0, Ldin;->t:Ljava/lang/Integer;

    .line 957
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 959
    :cond_10
    iget-object v2, p0, Ldin;->v:[Ldim;

    if-eqz v2, :cond_12

    .line 960
    iget-object v2, p0, Ldin;->v:[Ldim;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_12

    aget-object v4, v2, v1

    .line 961
    if-eqz v4, :cond_11

    .line 962
    const/16 v5, 0x12

    .line 963
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 960
    :cond_11
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 967
    :cond_12
    iget-object v1, p0, Ldin;->w:Ljava/lang/Boolean;

    if-eqz v1, :cond_13

    .line 968
    const/16 v1, 0x13

    iget-object v2, p0, Ldin;->w:Ljava/lang/Boolean;

    .line 969
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 971
    :cond_13
    iget-object v1, p0, Ldin;->u:Ljava/lang/Integer;

    if-eqz v1, :cond_14

    .line 972
    const/16 v1, 0x14

    iget-object v2, p0, Ldin;->u:Ljava/lang/Integer;

    .line 973
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 975
    :cond_14
    iget-object v1, p0, Ldin;->p:Ljava/lang/Boolean;

    if-eqz v1, :cond_15

    .line 976
    const/16 v1, 0x15

    iget-object v2, p0, Ldin;->p:Ljava/lang/Boolean;

    .line 977
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 979
    :cond_15
    iget-object v1, p0, Ldin;->s:Ljava/lang/String;

    if-eqz v1, :cond_16

    .line 980
    const/16 v1, 0x16

    iget-object v2, p0, Ldin;->s:Ljava/lang/String;

    .line 981
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 983
    :cond_16
    iget-object v1, p0, Ldin;->x:Ljava/lang/String;

    if-eqz v1, :cond_17

    .line 984
    const/16 v1, 0x17

    iget-object v2, p0, Ldin;->x:Ljava/lang/String;

    .line 985
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 987
    :cond_17
    iget-object v1, p0, Ldin;->y:Ljava/lang/Boolean;

    if-eqz v1, :cond_18

    .line 988
    const/16 v1, 0x18

    iget-object v2, p0, Ldin;->y:Ljava/lang/Boolean;

    .line 989
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 991
    :cond_18
    iget-object v1, p0, Ldin;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 992
    iput v0, p0, Ldin;->cachedSize:I

    .line 993
    return v0

    :cond_19
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 725
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldin;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldin;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldin;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldin;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v4, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_2

    const/4 v2, 0x5

    if-eq v0, v2, :cond_2

    const/4 v2, 0x6

    if-eq v0, v2, :cond_2

    const/4 v2, 0x7

    if-eq v0, v2, :cond_2

    const/16 v2, 0x8

    if-eq v0, v2, :cond_2

    const/16 v2, 0x9

    if-ne v0, v2, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldin;->c:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldin;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldin;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldin;->e:Ldik;

    if-nez v0, :cond_4

    new-instance v0, Ldik;

    invoke-direct {v0}, Ldik;-><init>()V

    iput-object v0, p0, Ldin;->e:Ldik;

    :cond_4
    iget-object v0, p0, Ldin;->e:Ldik;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldin;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldin;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->b()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Ldin;->h:Ljava/lang/Double;

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Ldin;->i:Ldio;

    if-nez v0, :cond_5

    new-instance v0, Ldio;

    invoke-direct {v0}, Ldio;-><init>()V

    iput-object v0, p0, Ldin;->i:Ldio;

    :cond_5
    iget-object v0, p0, Ldin;->i:Ldio;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Ldin;->j:Ldil;

    if-nez v0, :cond_6

    new-instance v0, Ldil;

    invoke-direct {v0}, Ldil;-><init>()V

    iput-object v0, p0, Ldin;->j:Ldil;

    :cond_6
    iget-object v0, p0, Ldin;->j:Ldil;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldin;->k:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldin;->l:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldin;->m:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_d
    const/16 v0, 0x6a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldin;->n:[Ldij;

    if-nez v0, :cond_8

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldij;

    iget-object v3, p0, Ldin;->n:[Ldij;

    if-eqz v3, :cond_7

    iget-object v3, p0, Ldin;->n:[Ldij;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    iput-object v2, p0, Ldin;->n:[Ldij;

    :goto_2
    iget-object v2, p0, Ldin;->n:[Ldij;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    iget-object v2, p0, Ldin;->n:[Ldij;

    new-instance v3, Ldij;

    invoke-direct {v3}, Ldij;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldin;->n:[Ldij;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_8
    iget-object v0, p0, Ldin;->n:[Ldij;

    array-length v0, v0

    goto :goto_1

    :cond_9
    iget-object v2, p0, Ldin;->n:[Ldij;

    new-instance v3, Ldij;

    invoke-direct {v3}, Ldij;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldin;->n:[Ldij;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldin;->o:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldin;->q:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldin;->r:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldin;->t:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_12
    const/16 v0, 0x92

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldin;->v:[Ldim;

    if-nez v0, :cond_b

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ldim;

    iget-object v3, p0, Ldin;->v:[Ldim;

    if-eqz v3, :cond_a

    iget-object v3, p0, Ldin;->v:[Ldim;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_a
    iput-object v2, p0, Ldin;->v:[Ldim;

    :goto_4
    iget-object v2, p0, Ldin;->v:[Ldim;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_c

    iget-object v2, p0, Ldin;->v:[Ldim;

    new-instance v3, Ldim;

    invoke-direct {v3}, Ldim;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldin;->v:[Ldim;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_b
    iget-object v0, p0, Ldin;->v:[Ldim;

    array-length v0, v0

    goto :goto_3

    :cond_c
    iget-object v2, p0, Ldin;->v:[Ldim;

    new-instance v3, Ldim;

    invoke-direct {v3}, Ldim;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldin;->v:[Ldim;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldin;->w:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_14
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldin;->u:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_15
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldin;->p:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_16
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldin;->s:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_17
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldin;->x:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_18
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldin;->y:Ljava/lang/Boolean;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x39 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x70 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x88 -> :sswitch_11
        0x92 -> :sswitch_12
        0x98 -> :sswitch_13
        0xa0 -> :sswitch_14
        0xa8 -> :sswitch_15
        0xb2 -> :sswitch_16
        0xba -> :sswitch_17
        0xc0 -> :sswitch_18
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 800
    iget-object v1, p0, Ldin;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 801
    const/4 v1, 0x1

    iget-object v2, p0, Ldin;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 803
    :cond_0
    iget-object v1, p0, Ldin;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 804
    const/4 v1, 0x2

    iget-object v2, p0, Ldin;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 806
    :cond_1
    iget-object v1, p0, Ldin;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 807
    const/4 v1, 0x3

    iget-object v2, p0, Ldin;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 809
    :cond_2
    iget-object v1, p0, Ldin;->e:Ldik;

    if-eqz v1, :cond_3

    .line 810
    const/4 v1, 0x4

    iget-object v2, p0, Ldin;->e:Ldik;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 812
    :cond_3
    iget-object v1, p0, Ldin;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 813
    const/4 v1, 0x5

    iget-object v2, p0, Ldin;->f:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 815
    :cond_4
    iget-object v1, p0, Ldin;->g:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 816
    const/4 v1, 0x6

    iget-object v2, p0, Ldin;->g:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 818
    :cond_5
    iget-object v1, p0, Ldin;->h:Ljava/lang/Double;

    if-eqz v1, :cond_6

    .line 819
    const/4 v1, 0x7

    iget-object v2, p0, Ldin;->h:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Lepl;->a(ID)V

    .line 821
    :cond_6
    iget-object v1, p0, Ldin;->i:Ldio;

    if-eqz v1, :cond_7

    .line 822
    const/16 v1, 0x8

    iget-object v2, p0, Ldin;->i:Ldio;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 824
    :cond_7
    iget-object v1, p0, Ldin;->j:Ldil;

    if-eqz v1, :cond_8

    .line 825
    const/16 v1, 0x9

    iget-object v2, p0, Ldin;->j:Ldil;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 827
    :cond_8
    iget-object v1, p0, Ldin;->k:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 828
    const/16 v1, 0xa

    iget-object v2, p0, Ldin;->k:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 830
    :cond_9
    iget-object v1, p0, Ldin;->l:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 831
    const/16 v1, 0xb

    iget-object v2, p0, Ldin;->l:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 833
    :cond_a
    iget-object v1, p0, Ldin;->m:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 834
    const/16 v1, 0xc

    iget-object v2, p0, Ldin;->m:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 836
    :cond_b
    iget-object v1, p0, Ldin;->n:[Ldij;

    if-eqz v1, :cond_d

    .line 837
    iget-object v2, p0, Ldin;->n:[Ldij;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_d

    aget-object v4, v2, v1

    .line 838
    if-eqz v4, :cond_c

    .line 839
    const/16 v5, 0xd

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 837
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 843
    :cond_d
    iget-object v1, p0, Ldin;->o:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    .line 844
    const/16 v1, 0xe

    iget-object v2, p0, Ldin;->o:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 846
    :cond_e
    iget-object v1, p0, Ldin;->q:Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 847
    const/16 v1, 0xf

    iget-object v2, p0, Ldin;->q:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 849
    :cond_f
    iget-object v1, p0, Ldin;->r:Ljava/lang/String;

    if-eqz v1, :cond_10

    .line 850
    const/16 v1, 0x10

    iget-object v2, p0, Ldin;->r:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 852
    :cond_10
    iget-object v1, p0, Ldin;->t:Ljava/lang/Integer;

    if-eqz v1, :cond_11

    .line 853
    const/16 v1, 0x11

    iget-object v2, p0, Ldin;->t:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 855
    :cond_11
    iget-object v1, p0, Ldin;->v:[Ldim;

    if-eqz v1, :cond_13

    .line 856
    iget-object v1, p0, Ldin;->v:[Ldim;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_13

    aget-object v3, v1, v0

    .line 857
    if-eqz v3, :cond_12

    .line 858
    const/16 v4, 0x12

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 856
    :cond_12
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 862
    :cond_13
    iget-object v0, p0, Ldin;->w:Ljava/lang/Boolean;

    if-eqz v0, :cond_14

    .line 863
    const/16 v0, 0x13

    iget-object v1, p0, Ldin;->w:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 865
    :cond_14
    iget-object v0, p0, Ldin;->u:Ljava/lang/Integer;

    if-eqz v0, :cond_15

    .line 866
    const/16 v0, 0x14

    iget-object v1, p0, Ldin;->u:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 868
    :cond_15
    iget-object v0, p0, Ldin;->p:Ljava/lang/Boolean;

    if-eqz v0, :cond_16

    .line 869
    const/16 v0, 0x15

    iget-object v1, p0, Ldin;->p:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 871
    :cond_16
    iget-object v0, p0, Ldin;->s:Ljava/lang/String;

    if-eqz v0, :cond_17

    .line 872
    const/16 v0, 0x16

    iget-object v1, p0, Ldin;->s:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 874
    :cond_17
    iget-object v0, p0, Ldin;->x:Ljava/lang/String;

    if-eqz v0, :cond_18

    .line 875
    const/16 v0, 0x17

    iget-object v1, p0, Ldin;->x:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 877
    :cond_18
    iget-object v0, p0, Ldin;->y:Ljava/lang/Boolean;

    if-eqz v0, :cond_19

    .line 878
    const/16 v0, 0x18

    iget-object v1, p0, Ldin;->y:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 880
    :cond_19
    iget-object v0, p0, Ldin;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 882
    return-void
.end method
