.class public abstract Lasl;
.super Lasw;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/hangout/ToastView;

.field private final b:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

.field private final d:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;


# direct methods
.method constructor <init>(Lcom/google/android/apps/hangouts/hangout/ToastView;ILcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V
    .locals 0

    .prologue
    .line 272
    iput-object p1, p0, Lasl;->a:Lcom/google/android/apps/hangouts/hangout/ToastView;

    .line 273
    invoke-direct {p0, p1, p2}, Lasw;-><init>(Lcom/google/android/apps/hangouts/hangout/ToastView;I)V

    .line 274
    iput-object p3, p0, Lasl;->b:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 275
    iput-object p4, p0, Lasl;->d:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 276
    return-void
.end method


# virtual methods
.method protected abstract a()[I
.end method

.method b()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 282
    iget-object v0, p0, Lasl;->b:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    .line 283
    iget-object v1, p0, Lasl;->d:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    .line 285
    iget-object v2, p0, Lasl;->d:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isSelfEndpoint()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 286
    iget-object v1, p0, Lasl;->a:Lcom/google/android/apps/hangouts/hangout/ToastView;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/hangout/ToastView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 287
    invoke-virtual {p0}, Lasl;->a()[I

    move-result-object v2

    aget v2, v2, v5

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v0, v3, v5

    .line 286
    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 295
    :goto_0
    return-object v0

    .line 288
    :cond_0
    iget-object v2, p0, Lasl;->b:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isSelfEndpoint()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 289
    iget-object v0, p0, Lasl;->a:Lcom/google/android/apps/hangouts/hangout/ToastView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/ToastView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 290
    invoke-virtual {p0}, Lasl;->a()[I

    move-result-object v2

    aget v2, v2, v6

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v1, v3, v5

    .line 289
    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 292
    :cond_1
    iget-object v2, p0, Lasl;->a:Lcom/google/android/apps/hangouts/hangout/ToastView;

    invoke-virtual {v2}, Lcom/google/android/apps/hangouts/hangout/ToastView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 293
    invoke-virtual {p0}, Lasl;->a()[I

    move-result-object v3

    aget v3, v3, v4

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v5

    aput-object v0, v4, v6

    .line 292
    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
