.class public final Legb;
.super Lefe;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lefe",
        "<TK;TV;>;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x3L


# direct methods
.method public constructor <init>(Legd;Legd;Leba;Leba;JJIILft;Ljava/util/concurrent/ConcurrentMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Legd;",
            "Legd;",
            "Leba",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Leba",
            "<",
            "Ljava/lang/Object;",
            ">;JJII",
            "Lft",
            "<-TK;-TV;>;",
            "Ljava/util/concurrent/ConcurrentMap",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 3879
    invoke-direct/range {p0 .. p12}, Lefe;-><init>(Legd;Legd;Leba;Leba;JJIILft;Ljava/util/concurrent/ConcurrentMap;)V

    .line 3881
    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 1

    .prologue
    .line 3889
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 3890
    invoke-virtual {p0, p1}, Legb;->a(Ljava/io/ObjectInputStream;)Lees;

    move-result-object v0

    .line 3891
    invoke-virtual {v0}, Lees;->j()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, Legb;->j:Ljava/util/concurrent/ConcurrentMap;

    .line 3892
    invoke-virtual {p0, p1}, Legb;->b(Ljava/io/ObjectInputStream;)V

    .line 3893
    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 3896
    iget-object v0, p0, Legb;->j:Ljava/util/concurrent/ConcurrentMap;

    return-object v0
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 0

    .prologue
    .line 3884
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 3885
    invoke-virtual {p0, p1}, Legb;->a(Ljava/io/ObjectOutputStream;)V

    .line 3886
    return-void
.end method
