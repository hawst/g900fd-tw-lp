.class public final Lavv;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 251
    iput-object p1, p0, Lavv;->b:Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;

    iput-object p2, p0, Lavv;->a:Ljava/lang/String;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 251
    iget-object v0, p0, Lavv;->b:Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;->a(Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "apn"

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;->b()Landroid/content/ContentValues;

    move-result-object v2

    const-string v3, "current =?"

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;->c()[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    const-string v0, "_id =?"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lavv;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    iget-object v2, p0, Lavv;->b:Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;

    invoke-static {v2}, Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;->a(Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v3, "apn"

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;->d()Landroid/content/ContentValues;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    const/4 v0, 0x0

    return-object v0
.end method
