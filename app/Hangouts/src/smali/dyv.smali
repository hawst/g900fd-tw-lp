.class public final Ldyv;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldyv;


# instance fields
.field public b:Ljava/lang/Boolean;

.field public c:Ljava/lang/Boolean;

.field public d:Ljava/lang/Boolean;

.field public e:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 814
    const/4 v0, 0x0

    new-array v0, v0, [Ldyv;

    sput-object v0, Ldyv;->a:[Ldyv;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 815
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 846
    const/4 v0, 0x0

    .line 847
    iget-object v1, p0, Ldyv;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 848
    const/4 v0, 0x1

    iget-object v1, p0, Ldyv;->b:Ljava/lang/Boolean;

    .line 849
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Lepl;->g(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 851
    :cond_0
    iget-object v1, p0, Ldyv;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 852
    const/4 v1, 0x2

    iget-object v2, p0, Ldyv;->c:Ljava/lang/Boolean;

    .line 853
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 855
    :cond_1
    iget-object v1, p0, Ldyv;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 856
    const/4 v1, 0x3

    iget-object v2, p0, Ldyv;->d:Ljava/lang/Boolean;

    .line 857
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 859
    :cond_2
    iget-object v1, p0, Ldyv;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 860
    const/4 v1, 0x4

    iget-object v2, p0, Ldyv;->e:Ljava/lang/Boolean;

    .line 861
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 863
    :cond_3
    iget-object v1, p0, Ldyv;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 864
    iput v0, p0, Ldyv;->cachedSize:I

    .line 865
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 811
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldyv;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldyv;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldyv;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldyv;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldyv;->c:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldyv;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldyv;->e:Ljava/lang/Boolean;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 828
    iget-object v0, p0, Ldyv;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 829
    const/4 v0, 0x1

    iget-object v1, p0, Ldyv;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 831
    :cond_0
    iget-object v0, p0, Ldyv;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 832
    const/4 v0, 0x2

    iget-object v1, p0, Ldyv;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 834
    :cond_1
    iget-object v0, p0, Ldyv;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 835
    const/4 v0, 0x3

    iget-object v1, p0, Ldyv;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 837
    :cond_2
    iget-object v0, p0, Ldyv;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 838
    const/4 v0, 0x4

    iget-object v1, p0, Ldyv;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 840
    :cond_3
    iget-object v0, p0, Ldyv;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 842
    return-void
.end method
