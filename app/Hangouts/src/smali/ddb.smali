.class public final Lddb;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lddb;


# instance fields
.field public b:Ldcv;

.field public c:Lddz;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 888
    const/4 v0, 0x0

    new-array v0, v0, [Lddb;

    sput-object v0, Lddb;->a:[Lddb;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 889
    invoke-direct {p0}, Lepn;-><init>()V

    .line 892
    iput-object v0, p0, Lddb;->b:Ldcv;

    .line 895
    iput-object v0, p0, Lddb;->c:Lddz;

    .line 889
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 912
    const/4 v0, 0x0

    .line 913
    iget-object v1, p0, Lddb;->b:Ldcv;

    if-eqz v1, :cond_0

    .line 914
    const/4 v0, 0x1

    iget-object v1, p0, Lddb;->b:Ldcv;

    .line 915
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 917
    :cond_0
    iget-object v1, p0, Lddb;->c:Lddz;

    if-eqz v1, :cond_1

    .line 918
    const/4 v1, 0x2

    iget-object v2, p0, Lddb;->c:Lddz;

    .line 919
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 921
    :cond_1
    iget-object v1, p0, Lddb;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 922
    iput v0, p0, Lddb;->cachedSize:I

    .line 923
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 885
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lddb;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lddb;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lddb;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lddb;->b:Ldcv;

    if-nez v0, :cond_2

    new-instance v0, Ldcv;

    invoke-direct {v0}, Ldcv;-><init>()V

    iput-object v0, p0, Lddb;->b:Ldcv;

    :cond_2
    iget-object v0, p0, Lddb;->b:Ldcv;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lddb;->c:Lddz;

    if-nez v0, :cond_3

    new-instance v0, Lddz;

    invoke-direct {v0}, Lddz;-><init>()V

    iput-object v0, p0, Lddb;->c:Lddz;

    :cond_3
    iget-object v0, p0, Lddb;->c:Lddz;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 900
    iget-object v0, p0, Lddb;->b:Ldcv;

    if-eqz v0, :cond_0

    .line 901
    const/4 v0, 0x1

    iget-object v1, p0, Lddb;->b:Ldcv;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 903
    :cond_0
    iget-object v0, p0, Lddb;->c:Lddz;

    if-eqz v0, :cond_1

    .line 904
    const/4 v0, 0x2

    iget-object v1, p0, Lddb;->c:Lddz;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 906
    :cond_1
    iget-object v0, p0, Lddb;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 908
    return-void
.end method
