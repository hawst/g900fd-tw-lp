.class public final Leqz;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leqz;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;

.field public d:[Leqy;

.field public e:Ljava/lang/Boolean;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1056
    const/4 v0, 0x0

    new-array v0, v0, [Leqz;

    sput-object v0, Leqz;->a:[Leqz;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1057
    invoke-direct {p0}, Lepn;-><init>()V

    .line 1060
    const/4 v0, 0x0

    iput-object v0, p0, Leqz;->b:Ljava/lang/Integer;

    .line 1065
    sget-object v0, Leqy;->a:[Leqy;

    iput-object v0, p0, Leqz;->d:[Leqy;

    .line 1057
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1110
    iget-object v0, p0, Leqz;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 1111
    const/4 v0, 0x1

    iget-object v2, p0, Leqz;->b:Ljava/lang/Integer;

    .line 1112
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1114
    :goto_0
    iget-object v2, p0, Leqz;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 1115
    const/4 v2, 0x2

    iget-object v3, p0, Leqz;->c:Ljava/lang/Integer;

    .line 1116
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1118
    :cond_0
    iget-object v2, p0, Leqz;->d:[Leqy;

    if-eqz v2, :cond_2

    .line 1119
    iget-object v2, p0, Leqz;->d:[Leqy;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 1120
    if-eqz v4, :cond_1

    .line 1121
    const/4 v5, 0x3

    .line 1122
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1119
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1126
    :cond_2
    iget-object v1, p0, Leqz;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 1127
    const/4 v1, 0x4

    iget-object v2, p0, Leqz;->e:Ljava/lang/Boolean;

    .line 1128
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1130
    :cond_3
    iget-object v1, p0, Leqz;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 1131
    const/4 v1, 0x5

    iget-object v2, p0, Leqz;->f:Ljava/lang/Integer;

    .line 1132
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1134
    :cond_4
    iget-object v1, p0, Leqz;->g:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 1135
    const/4 v1, 0x6

    iget-object v2, p0, Leqz;->g:Ljava/lang/String;

    .line 1136
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1138
    :cond_5
    iget-object v1, p0, Leqz;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 1139
    const/4 v1, 0x7

    iget-object v2, p0, Leqz;->h:Ljava/lang/Boolean;

    .line 1140
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1142
    :cond_6
    iget-object v1, p0, Leqz;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1143
    iput v0, p0, Leqz;->cachedSize:I

    .line 1144
    return v0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1053
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Leqz;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Leqz;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Leqz;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v4, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_2

    const/4 v2, 0x5

    if-eq v0, v2, :cond_2

    const/4 v2, 0x6

    if-eq v0, v2, :cond_2

    const/4 v2, 0x7

    if-ne v0, v2, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leqz;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leqz;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leqz;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leqz;->d:[Leqy;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Leqy;

    iget-object v3, p0, Leqz;->d:[Leqy;

    if-eqz v3, :cond_4

    iget-object v3, p0, Leqz;->d:[Leqy;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iput-object v2, p0, Leqz;->d:[Leqy;

    :goto_2
    iget-object v2, p0, Leqz;->d:[Leqy;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Leqz;->d:[Leqy;

    new-instance v3, Leqy;

    invoke-direct {v3}, Leqy;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leqz;->d:[Leqy;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Leqz;->d:[Leqy;

    array-length v0, v0

    goto :goto_1

    :cond_6
    iget-object v2, p0, Leqz;->d:[Leqy;

    new-instance v3, Leqy;

    invoke-direct {v3}, Leqy;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leqz;->d:[Leqy;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Leqz;->e:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leqz;->f:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leqz;->g:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Leqz;->h:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 1078
    iget-object v0, p0, Leqz;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1079
    const/4 v0, 0x1

    iget-object v1, p0, Leqz;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1081
    :cond_0
    iget-object v0, p0, Leqz;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1082
    const/4 v0, 0x2

    iget-object v1, p0, Leqz;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1084
    :cond_1
    iget-object v0, p0, Leqz;->d:[Leqy;

    if-eqz v0, :cond_3

    .line 1085
    iget-object v1, p0, Leqz;->d:[Leqy;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 1086
    if-eqz v3, :cond_2

    .line 1087
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 1085
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1091
    :cond_3
    iget-object v0, p0, Leqz;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 1092
    const/4 v0, 0x4

    iget-object v1, p0, Leqz;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 1094
    :cond_4
    iget-object v0, p0, Leqz;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 1095
    const/4 v0, 0x5

    iget-object v1, p0, Leqz;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1097
    :cond_5
    iget-object v0, p0, Leqz;->g:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1098
    const/4 v0, 0x6

    iget-object v1, p0, Leqz;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1100
    :cond_6
    iget-object v0, p0, Leqz;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 1101
    const/4 v0, 0x7

    iget-object v1, p0, Leqz;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 1103
    :cond_7
    iget-object v0, p0, Leqz;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1105
    return-void
.end method
