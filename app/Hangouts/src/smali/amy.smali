.class final Lamy;
.super Landroid/widget/ArrayAdapter;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lbrp;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lamm;


# direct methods
.method public constructor <init>(Lamm;Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lbrp;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 840
    iput-object p1, p0, Lamy;->a:Lamm;

    .line 841
    sget v0, Lf;->eS:I

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 842
    return-void
.end method


# virtual methods
.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 862
    invoke-virtual {p0, p1, p2, p3}, Lamy;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 847
    if-eqz p2, :cond_0

    instance-of v0, p2, Landroid/widget/TextView;

    if-nez v0, :cond_1

    .line 848
    :cond_0
    invoke-virtual {p0}, Lamy;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 849
    sget v1, Lf;->eS:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    move-object p2, v0

    .line 855
    :goto_0
    invoke-virtual {p0, p1}, Lamy;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbrp;

    .line 856
    iget-object v0, v0, Lbrp;->d:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 857
    return-object p2

    .line 852
    :cond_1
    check-cast p2, Landroid/widget/TextView;

    goto :goto_0
.end method
