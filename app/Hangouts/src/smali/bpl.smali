.class public final Lbpl;
.super Landroid/os/Handler;
.source "PG"


# instance fields
.field private a:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;)V
    .locals 0

    .prologue
    .line 586
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 587
    iput-object p1, p0, Lbpl;->a:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    .line 588
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 16

    .prologue
    .line 593
    :try_start_0
    move-object/from16 v0, p1

    iget v1, v0, Landroid/os/Message;->arg1:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 594
    move-object/from16 v0, p0

    iget-object v1, v0, Lbpl;->a:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    new-instance v2, Lbpk;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbpl;->a:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    invoke-direct {v2, v3}, Lbpk;-><init>(Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;)V

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a(Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;Lbpk;)Lbpk;

    .line 595
    move-object/from16 v0, p0

    iget-object v1, v0, Lbpl;->a:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a(Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;)Lbpk;

    move-result-object v1

    invoke-static {v1}, Lbpk;->a(Lbpk;)V

    .line 596
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 597
    const-string v1, "Babel_RequestWriter"

    move-object/from16 v0, p0

    iget-object v2, v0, Lbpl;->a:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    const-string v3, "MSG_LOAD"

    invoke-virtual {v2, v3}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 648
    :cond_0
    :goto_0
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 649
    const-string v1, "Babel_RequestWriter"

    move-object/from16 v0, p0

    iget-object v2, v0, Lbpl;->a:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    const-string v3, "Looper release"

    invoke-virtual {v2, v3}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 651
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lbpl;->a:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->c()V

    .line 652
    return-void

    .line 600
    :cond_2
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "intent"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    .line 601
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 602
    if-eqz v2, :cond_7

    const-string v3, "server_request"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 603
    const-string v3, "server_request"

    .line 604
    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v3

    .line 603
    invoke-static {v3}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->b([B)Lbea;

    move-result-object v14

    .line 608
    if-eqz v14, :cond_5

    .line 609
    invoke-virtual {v14}, Lbea;->q()V

    .line 610
    const-string v3, "account_name"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 611
    instance-of v2, v14, Lbfb;

    if-eqz v2, :cond_4

    .line 613
    move-object v0, v14

    check-cast v0, Lbfb;

    move-object v13, v0

    const-string v2, "timestamp"

    const-wide/16 v3, 0x0

    .line 614
    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 613
    invoke-static {v15}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v1

    iget-object v4, v13, Lbfb;->i:Ljava/lang/String;

    invoke-static {v4}, Lyt;->d(Ljava/lang/String;)J

    move-result-wide v9

    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_3

    const/16 v4, 0xa

    const/16 v5, 0xca

    const/4 v6, 0x0

    const-wide/16 v7, 0x0

    iget-object v11, v13, Lbfb;->c:Ljava/lang/String;

    const/4 v12, 0x0

    invoke-static/range {v1 .. v12}, Lf;->a(Lyj;JIILjava/lang/String;JJLjava/lang/String;Lyu;)V

    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    const/16 v4, 0xa

    const/16 v5, 0xcb

    const/4 v6, 0x0

    const-wide/16 v7, 0x0

    iget-object v11, v13, Lbfb;->c:Ljava/lang/String;

    const/4 v12, 0x0

    invoke-static/range {v1 .. v12}, Lf;->a(Lyj;JIILjava/lang/String;JJLjava/lang/String;Lyu;)V

    .line 616
    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lbpl;->a:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    invoke-virtual {v1, v14, v15}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a(Lbea;Ljava/lang/String;)V

    .line 641
    :cond_5
    :goto_1
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 642
    const-string v1, "Babel_RequestWriter"

    move-object/from16 v0, p0

    iget-object v2, v0, Lbpl;->a:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    const-string v3, "MSG_INTENT"

    invoke-virtual {v2, v3}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 648
    :catchall_0
    move-exception v1

    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->j()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 649
    const-string v2, "Babel_RequestWriter"

    move-object/from16 v0, p0

    iget-object v3, v0, Lbpl;->a:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    const-string v4, "Looper release"

    invoke-virtual {v3, v4}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 651
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lbpl;->a:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    invoke-virtual {v2}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->c()V

    throw v1

    .line 618
    :cond_7
    if-eqz v2, :cond_8

    :try_start_2
    const-string v3, "cancel_request"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 619
    move-object/from16 v0, p0

    iget-object v1, v0, Lbpl;->a:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    const-string v3, "cancel_class"

    .line 620
    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "cancel_request"

    .line 621
    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 619
    invoke-static {v1, v3, v2}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a(Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 622
    :cond_8
    if-eqz v2, :cond_9

    const-string v3, "sms_send_result"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 623
    const-string v1, "sms_request_id"

    const-wide/16 v3, -0x1

    .line 624
    invoke-virtual {v2, v1, v3, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    const-string v1, "sms_send_result"

    .line 625
    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 623
    invoke-static {v3, v4, v1}, Lbwd;->a(JI)V

    goto :goto_1

    .line 629
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lbpl;->a:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    invoke-static {v2}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->b(Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;)Z

    .line 630
    const-string v2, "backoff_period"

    const-wide/16 v3, 0x1f4

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    .line 632
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->j()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 633
    const-string v3, "Babel_RequestWriter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "RequestWriter.handleMessage. resumeAllQueues "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 639
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lbpl;->a:Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    invoke-static {v3, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a(Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1
.end method
