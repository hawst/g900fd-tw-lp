.class public Lbhh;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method private constructor <init>(Ldwe;)V
    .locals 3

    .prologue
    .line 1551
    iget-object v0, p1, Ldwe;->b:Ldvn;

    iget-object v1, p1, Ldwe;->c:Ljava/lang/Long;

    .line 1552
    invoke-static {v1}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v1

    .line 1551
    invoke-direct {p0, v0, v1, v2}, Lbfz;-><init>(Ldvn;J)V

    .line 1553
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_0

    .line 1554
    const-string v0, "Babel_protos"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "NotificationLevelResponse from:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1556
    :cond_0
    return-void
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 1560
    new-instance v0, Ldwe;

    invoke-direct {v0}, Ldwe;-><init>()V

    .line 1561
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldwe;

    .line 1562
    iget-object v1, v0, Ldwe;->b:Ldvn;

    invoke-static {v1}, Lbhh;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1563
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldwe;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 1565
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbhh;

    invoke-direct {v1, v0}, Lbhh;-><init>(Ldwe;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Lyt;Lbnl;)V
    .locals 3

    .prologue
    .line 1575
    invoke-super {p0, p1, p2}, Lbfz;->a(Lyt;Lbnl;)V

    .line 1576
    sget-boolean v0, Lyp;->a:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_1

    .line 1577
    :cond_0
    const-string v0, "Babel"

    const-string v1, "processNotificationLevelResponse"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1579
    :cond_1
    iget-object v0, p0, Lbhh;->c:Lbht;

    iget v0, v0, Lbht;->b:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    .line 1582
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to set conversation preference with code "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbhh;->c:Lbht;

    iget v2, v2, Lbht;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1587
    :cond_2
    return-void
.end method
