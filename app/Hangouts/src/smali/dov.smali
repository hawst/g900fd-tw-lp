.class public final Ldov;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldov;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/Boolean;

.field public h:Ljava/lang/Boolean;

.field public i:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 813
    const/4 v0, 0x0

    new-array v0, v0, [Ldov;

    sput-object v0, Ldov;->a:[Ldov;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 814
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 865
    const/4 v0, 0x0

    .line 866
    iget-object v1, p0, Ldov;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 867
    const/4 v0, 0x1

    iget-object v1, p0, Ldov;->b:Ljava/lang/Integer;

    .line 868
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 870
    :cond_0
    iget-object v1, p0, Ldov;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 871
    const/4 v1, 0x2

    iget-object v2, p0, Ldov;->c:Ljava/lang/String;

    .line 872
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 874
    :cond_1
    iget-object v1, p0, Ldov;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 875
    const/4 v1, 0x3

    iget-object v2, p0, Ldov;->d:Ljava/lang/Integer;

    .line 876
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 878
    :cond_2
    iget-object v1, p0, Ldov;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 879
    const/4 v1, 0x4

    iget-object v2, p0, Ldov;->e:Ljava/lang/String;

    .line 880
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 882
    :cond_3
    iget-object v1, p0, Ldov;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 883
    const/4 v1, 0x5

    iget-object v2, p0, Ldov;->f:Ljava/lang/Integer;

    .line 884
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 886
    :cond_4
    iget-object v1, p0, Ldov;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 887
    const/4 v1, 0x6

    iget-object v2, p0, Ldov;->g:Ljava/lang/Boolean;

    .line 888
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 890
    :cond_5
    iget-object v1, p0, Ldov;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 891
    const/4 v1, 0x7

    iget-object v2, p0, Ldov;->h:Ljava/lang/Boolean;

    .line 892
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 894
    :cond_6
    iget-object v1, p0, Ldov;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 895
    const/16 v1, 0x8

    iget-object v2, p0, Ldov;->i:Ljava/lang/Boolean;

    .line 896
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 898
    :cond_7
    iget-object v1, p0, Ldov;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 899
    iput v0, p0, Ldov;->cachedSize:I

    .line 900
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 810
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldov;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldov;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldov;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldov;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldov;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldov;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldov;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldov;->f:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldov;->g:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldov;->h:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldov;->i:Ljava/lang/Boolean;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 835
    iget-object v0, p0, Ldov;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 836
    const/4 v0, 0x1

    iget-object v1, p0, Ldov;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 838
    :cond_0
    iget-object v0, p0, Ldov;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 839
    const/4 v0, 0x2

    iget-object v1, p0, Ldov;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 841
    :cond_1
    iget-object v0, p0, Ldov;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 842
    const/4 v0, 0x3

    iget-object v1, p0, Ldov;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 844
    :cond_2
    iget-object v0, p0, Ldov;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 845
    const/4 v0, 0x4

    iget-object v1, p0, Ldov;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 847
    :cond_3
    iget-object v0, p0, Ldov;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 848
    const/4 v0, 0x5

    iget-object v1, p0, Ldov;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 850
    :cond_4
    iget-object v0, p0, Ldov;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 851
    const/4 v0, 0x6

    iget-object v1, p0, Ldov;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 853
    :cond_5
    iget-object v0, p0, Ldov;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 854
    const/4 v0, 0x7

    iget-object v1, p0, Ldov;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 856
    :cond_6
    iget-object v0, p0, Ldov;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 857
    const/16 v0, 0x8

    iget-object v1, p0, Ldov;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 859
    :cond_7
    iget-object v0, p0, Ldov;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 861
    return-void
.end method
