.class final Lbap;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Z

.field final b:Lbaw;


# direct methods
.method private constructor <init>(Lbaw;)V
    .locals 1

    .prologue
    .line 388
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lbap;-><init>(Lbaw;Z)V

    .line 389
    return-void
.end method

.method private constructor <init>(Lbaw;Z)V
    .locals 0

    .prologue
    .line 391
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 392
    iput-object p1, p0, Lbap;->b:Lbaw;

    .line 393
    iput-boolean p2, p0, Lbap;->a:Z

    .line 394
    return-void
.end method

.method public static a(Ljava/lang/String;)Lbap;
    .locals 2

    .prologue
    .line 397
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 398
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "separator may not be empty or null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 401
    :cond_0
    new-instance v0, Lbap;

    new-instance v1, Lbaq;

    invoke-direct {v1, p0}, Lbaq;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lbap;-><init>(Lbaw;)V

    return-object v0
.end method


# virtual methods
.method public a()Lbap;
    .locals 3

    .prologue
    .line 434
    new-instance v0, Lbap;

    iget-object v1, p0, Lbap;->b:Lbaw;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lbap;-><init>(Lbaw;Z)V

    return-object v0
.end method

.method public a(Ljava/lang/CharSequence;)Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            ")",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 438
    new-instance v0, Lbas;

    invoke-direct {v0, p0, p1}, Lbas;-><init>(Lbap;Ljava/lang/CharSequence;)V

    return-object v0
.end method
