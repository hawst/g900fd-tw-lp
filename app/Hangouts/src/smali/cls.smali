.class final Lcls;
.super Lckv;


# instance fields
.field final synthetic a:Lclj;

.field private final b:Lcgk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcgk",
            "<",
            "Lcuv;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lclj;Lcgk;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcgk",
            "<",
            "Lcuv;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcls;->a:Lclj;

    invoke-direct {p0}, Lckv;-><init>()V

    iput-object p2, p0, Lcls;->b:Lcgk;

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 6

    const-string v0, "PeopleService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Circles callback: status="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nresolution="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nholder="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-static {p1, p2}, Lclj;->a(ILandroid/os/Bundle;)Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    if-nez p3, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcls;->a:Lclj;

    new-instance v3, Lclp;

    iget-object v4, p0, Lcls;->a:Lclj;

    iget-object v5, p0, Lcls;->b:Lcgk;

    invoke-direct {v3, v4, v5, v1, v0}, Lclp;-><init>(Lclj;Lcgk;Lcom/google/android/gms/common/api/Status;Lcvz;)V

    invoke-virtual {v2, v3}, Lclj;->a(Lcht;)V

    return-void

    :cond_1
    new-instance v0, Lcvz;

    invoke-direct {v0, p3}, Lcvz;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    goto :goto_0
.end method
