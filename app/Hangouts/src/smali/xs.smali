.class public final Lxs;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x2L


# instance fields
.field private final a:Lbcx;

.field private b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lxt;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iget-object v0, p1, Lxt;->a:Lbcx;

    iput-object v0, p0, Lxs;->a:Lbcx;

    .line 29
    iget-object v0, p1, Lxt;->b:Ljava/lang/String;

    iput-object v0, p0, Lxs;->b:Ljava/lang/String;

    .line 30
    iget-object v0, p1, Lxt;->c:Ljava/lang/String;

    iput-object v0, p0, Lxs;->c:Ljava/lang/String;

    .line 31
    iget-object v0, p1, Lxt;->d:Ljava/lang/String;

    iput-object v0, p0, Lxs;->d:Ljava/lang/String;

    .line 32
    return-void
.end method

.method public static a(Lbcx;Ljava/lang/String;Ljava/lang/String;)Lxs;
    .locals 1

    .prologue
    .line 185
    invoke-static {}, Lxs;->newBuilder()Lxt;

    move-result-object v0

    .line 187
    invoke-virtual {v0, p0}, Lxt;->a(Lbcx;)V

    .line 188
    invoke-virtual {v0, p1}, Lxt;->a(Ljava/lang/String;)V

    .line 189
    invoke-virtual {v0, p2}, Lxt;->c(Ljava/lang/String;)V

    .line 191
    invoke-virtual {v0}, Lxt;->a()Lxs;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder()Lxt;
    .locals 1

    .prologue
    .line 92
    new-instance v0, Lxt;

    invoke-direct {v0}, Lxt;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lxs;->a:Lbcx;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Lbcx;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lxs;->a:Lbcx;

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lxs;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 19
    new-instance v0, Lxt;

    invoke-direct {v0}, Lxt;-><init>()V

    iget-object v1, p0, Lxs;->a:Lbcx;

    invoke-virtual {v0, v1}, Lxt;->a(Lbcx;)V

    iget-object v1, p0, Lxs;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lxt;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lxs;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lxt;->b(Ljava/lang/String;)V

    iget-object v1, p0, Lxs;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lxt;->c(Ljava/lang/String;)V

    invoke-virtual {v0}, Lxt;->a()Lxs;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lxs;->b:Ljava/lang/String;

    return-object v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lxs;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lxs;->c:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lxs;->d:Ljava/lang/String;

    return-object v0
.end method

.method public h()Lbdh;
    .locals 6

    .prologue
    .line 77
    iget-object v0, p0, Lxs;->a:Lbcx;

    .line 78
    invoke-virtual {v0}, Lbcx;->a()Lbdk;

    move-result-object v0

    iget-object v1, p0, Lxs;->b:Ljava/lang/String;

    iget-object v2, p0, Lxs;->b:Ljava/lang/String;

    iget-object v3, p0, Lxs;->a:Lbcx;

    iget-object v3, v3, Lbcx;->e:Ljava/lang/String;

    iget-object v4, p0, Lxs;->d:Ljava/lang/String;

    const/4 v5, 0x0

    .line 77
    invoke-static/range {v0 .. v5}, Lbdh;->a(Lbdk;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lbdh;

    move-result-object v0

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 2

    .prologue
    .line 87
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[Person] "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lxs;->b()Lbcx;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lxs;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 88
    invoke-virtual {p0}, Lxs;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lxs;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
