.class public final Lbyd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Lbsu;
.implements Ljava/lang/Runnable;


# static fields
.field private static L:Lbye;

.field private static M:Landroid/os/Handler;

.field private static final U:[B

.field private static final ab:Ljava/lang/Object;

.field private static final d:[B


# instance fields
.field private A:[I

.field private B:I

.field private C:Landroid/graphics/Bitmap;

.field private final D:Lbxs;

.field private E:J

.field private F:Z

.field private G:I

.field private H:I

.field private I:Z

.field private J:Z

.field private final K:Landroid/os/Handler;

.field private N:Z

.field private O:Z

.field private P:Z

.field private final Q:[B

.field private R:I

.field private S:I

.field private T:[I

.field private V:Z

.field private W:I

.field private X:I

.field private Y:Z

.field private Z:Lbyg;

.field public a:I

.field private aa:La;

.field public final b:[I

.field public c:I

.field private volatile e:Z

.field private volatile f:Z

.field private g:I

.field private h:Z

.field private i:I

.field private j:[I

.field private k:[I

.field private l:Z

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private final r:[B

.field private s:I

.field private t:Z

.field private u:I

.field private final v:[S

.field private final w:[B

.field private final x:[B

.field private y:[B

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    const-string v0, "NETSCAPE2.0"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lbyd;->d:[B

    .line 125
    const/16 v0, 0x300

    new-array v0, v0, [B

    sput-object v0, Lbyd;->U:[B

    .line 154
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lbyd;->ab:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>([BLbyg;Lbxs;)V
    .locals 7

    .prologue
    const/16 v3, 0x1000

    const/16 v4, 0x100

    const/4 v6, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    new-array v0, v4, [B

    iput-object v0, p0, Lbyd;->r:[B

    .line 88
    iput v6, p0, Lbyd;->s:I

    .line 93
    new-array v0, v3, [S

    iput-object v0, p0, Lbyd;->v:[S

    .line 94
    new-array v0, v3, [B

    iput-object v0, p0, Lbyd;->w:[B

    .line 95
    const/16 v0, 0x1001

    new-array v0, v0, [B

    iput-object v0, p0, Lbyd;->x:[B

    .line 111
    iput-boolean v2, p0, Lbyd;->J:Z

    .line 112
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v0, v3, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lbyd;->K:Landroid/os/Handler;

    .line 129
    new-array v0, v4, [I

    iput-object v0, p0, Lbyd;->b:[I

    .line 157
    iput-object p3, p0, Lbyd;->D:Lbxs;

    .line 160
    iput-boolean v2, p0, Lbyd;->Y:Z

    .line 163
    sget-object v3, Lbyd;->ab:Ljava/lang/Object;

    monitor-enter v3

    .line 164
    :try_start_0
    sget-object v0, Lbyd;->L:Lbye;

    if-nez v0, :cond_0

    .line 165
    new-instance v0, Lbye;

    invoke-direct {v0}, Lbye;-><init>()V

    .line 166
    sput-object v0, Lbyd;->L:Lbye;

    invoke-virtual {v0}, Lbye;->start()V

    .line 167
    new-instance v0, Landroid/os/Handler;

    sget-object v4, Lbyd;->L:Lbye;

    invoke-virtual {v4}, Lbye;->getLooper()Landroid/os/Looper;

    move-result-object v4

    sget-object v5, Lbyd;->L:Lbye;

    invoke-direct {v0, v4, v5}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    sput-object v0, Lbyd;->M:Landroid/os/Handler;

    .line 169
    :cond_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 171
    iput-object p2, p0, Lbyd;->Z:Lbyg;

    .line 172
    iput-object p1, p0, Lbyd;->Q:[B

    .line 173
    new-instance v3, Lbyf;

    invoke-direct {v3, p0, p1, v1}, Lbyf;-><init>(Lbyd;[BB)V

    .line 175
    :try_start_1
    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    move-result v0

    const/16 v4, 0x47

    if-ne v0, v4, :cond_2

    move v0, v2

    :goto_0
    if-eqz v0, :cond_3

    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    move-result v0

    const/16 v4, 0x49

    if-ne v0, v4, :cond_3

    move v0, v2

    :goto_1
    if-eqz v0, :cond_4

    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    move-result v0

    const/16 v4, 0x46

    if-ne v0, v4, :cond_4

    move v0, v2

    :goto_2
    if-nez v0, :cond_5

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbyd;->e:Z

    const-string v0, "Babel"

    const-string v4, "Not a valid Gif."

    invoke-static {v0, v4}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    :cond_1
    :goto_3
    invoke-virtual {v3}, Lbyf;->a()I

    move-result v0

    iput v0, p0, Lbyd;->a:I

    .line 177
    iget v0, p0, Lbyd;->a:I

    iput v0, p0, Lbyd;->X:I

    .line 178
    iget v0, p0, Lbyd;->R:I

    iput v0, p0, Lbyd;->q:I

    iput v0, p0, Lbyd;->o:I

    .line 179
    iget v0, p0, Lbyd;->S:I

    iput v0, p0, Lbyd;->p:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2

    move v0, v1

    .line 183
    :goto_4
    if-nez v1, :cond_7

    .line 185
    :try_start_2
    iget-object v4, p0, Lbyd;->D:Lbxs;

    iget v5, p0, Lbyd;->R:I

    iget v6, p0, Lbyd;->S:I

    invoke-virtual {v4, v5, v6}, Lbxs;->b(II)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lbyd;->C:Landroid/graphics/Bitmap;

    .line 186
    const/4 v4, 0x0

    iput-boolean v4, p0, Lbyd;->N:Z

    .line 187
    iget v4, p0, Lbyd;->R:I

    iget v5, p0, Lbyd;->S:I

    mul-int/2addr v4, v5

    .line 188
    new-array v5, v4, [I

    iput-object v5, p0, Lbyd;->T:[I

    .line 189
    new-array v4, v4, [B

    iput-object v4, p0, Lbyd;->y:[B

    .line 192
    sget-object v4, Lbyd;->M:Landroid/os/Handler;

    sget-object v5, Lbyd;->M:Landroid/os/Handler;

    const/16 v6, 0xa

    invoke-virtual {v5, v6, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    move v1, v2

    .line 206
    goto :goto_4

    .line 169
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_2
    move v0, v1

    .line 175
    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2

    :cond_5
    const-wide/16 v4, 0x3

    :try_start_3
    invoke-virtual {v3, v4, v5}, Ljava/io/InputStream;->skip(J)J

    invoke-static {v3}, Lbyd;->a(Ljava/io/InputStream;)I

    move-result v0

    iput v0, p0, Lbyd;->R:I

    invoke-static {v3}, Lbyd;->a(Ljava/io/InputStream;)I

    move-result v0

    iput v0, p0, Lbyd;->S:I

    iget v0, p0, Lbyd;->R:I

    if-lez v0, :cond_6

    iget v0, p0, Lbyd;->S:I

    if-gtz v0, :cond_8

    :cond_6
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbyd;->e:Z

    const-string v0, "Babel"

    const-string v4, "Not a valid Gif. Width or height is 0"

    invoke-static {v0, v4}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    :goto_5
    iget-boolean v0, p0, Lbyd;->V:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lbyd;->e:Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_2

    if-nez v0, :cond_1

    :try_start_4
    iget-object v0, p0, Lbyd;->b:[I

    iget v4, p0, Lbyd;->W:I

    invoke-static {v3, v0, v4}, Lbyd;->a(Ljava/io/InputStream;[II)Z

    iget-object v0, p0, Lbyd;->b:[I

    iget v4, p0, Lbyd;->c:I

    aget v0, v0, v4

    iput v0, p0, Lbyd;->g:I
    :try_end_4
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_3

    :catch_0
    move-exception v0

    const/4 v4, 0x1

    :try_start_5
    iput-boolean v4, p0, Lbyd;->e:Z

    const-string v4, "Babel"

    const-string v5, "Not a valid Gif."

    invoke-static {v4, v5, v0}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_3

    .line 208
    :catch_1
    move-exception v0

    .line 209
    iput-boolean v2, p0, Lbyd;->e:Z

    .line 210
    const-string v1, "Babel"

    const-string v2, "Could not read input stream from the gif."

    invoke-static {v1, v2, v0}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 217
    :cond_7
    :goto_6
    :try_start_6
    invoke-virtual {v3}, Lbyf;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 221
    :goto_7
    return-void

    .line 175
    :cond_8
    :try_start_7
    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    move-result v4

    and-int/lit16 v0, v4, 0x80

    if-eqz v0, :cond_9

    move v0, v2

    :goto_8
    iput-boolean v0, p0, Lbyd;->V:Z

    and-int/lit8 v0, v4, 0x7

    shl-int v0, v6, v0

    iput v0, p0, Lbyd;->W:I

    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    move-result v0

    iput v0, p0, Lbyd;->c:I

    const-wide/16 v4, 0x1

    invoke-virtual {v3, v4, v5}, Ljava/io/InputStream;->skip(J)J
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_2

    goto :goto_5

    .line 211
    :catch_2
    move-exception v0

    .line 212
    iput-boolean v2, p0, Lbyd;->e:Z

    .line 213
    const-string v1, "Babel"

    const-string v2, "Gif has invalid arguments"

    invoke-static {v1, v2, v0}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_6

    :cond_9
    move v0, v1

    .line 175
    goto :goto_8

    .line 196
    :catch_3
    move-exception v4

    :try_start_8
    const-string v4, "Babel"

    const-string v5, "Out of memory trying to create bitmap to use for Gif."

    invoke-static {v4, v5}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    add-int/lit8 v0, v0, 0x1

    .line 198
    if-gt v0, v2, :cond_a

    .line 199
    sget-object v4, Lcxe;->a:Lcxe;

    invoke-virtual {v4}, Lcxe;->a()V

    goto/16 :goto_4

    .line 202
    :cond_a
    const/4 v1, 0x1

    iput-boolean v1, p0, Lbyd;->e:Z

    .line 203
    const-string v1, "Babel"

    const-string v4, "Out of Memory. Failed to create bitmap to use for Gif. Aborting"

    invoke-static {v1, v4}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_8 .. :try_end_8} :catch_2

    move v1, v2

    .line 206
    goto/16 :goto_4

    .line 221
    :catch_4
    move-exception v0

    goto :goto_7
.end method

.method private static a(Ljava/io/InputStream;)I
    .locals 2

    .prologue
    .line 396
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v0

    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v1

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    return v0
.end method

.method private static a([B[III)I
    .locals 5

    .prologue
    .line 379
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_0

    .line 380
    add-int/lit8 v1, p3, 0x1

    aget-byte v2, p0, p3

    and-int/lit16 v2, v2, 0xff

    .line 381
    add-int/lit8 v3, v1, 0x1

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    .line 382
    add-int/lit8 p3, v3, 0x1

    aget-byte v3, p0, v3

    and-int/lit16 v3, v3, 0xff

    .line 383
    const/high16 v4, -0x1000000

    shl-int/lit8 v2, v2, 0x10

    or-int/2addr v2, v4

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v1, v2

    or-int/2addr v1, v3

    aput v1, p1, v0

    .line 379
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 385
    :cond_0
    return p3
.end method

.method static synthetic a(Lbyd;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lbyd;->C:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method private a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 439
    iget-object v0, p0, Lbyd;->K:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 440
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbyd;->F:Z

    .line 441
    return-void
.end method

.method private static a(Ljava/io/InputStream;[II)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 359
    sget-object v1, Lbyd;->U:[B

    monitor-enter v1

    .line 360
    mul-int/lit8 v2, p2, 0x3

    .line 361
    :try_start_0
    sget-object v3, Lbyd;->U:[B

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v3

    .line 362
    if-ge v3, v2, :cond_0

    .line 363
    monitor-exit v1

    .line 368
    :goto_0
    return v0

    .line 365
    :cond_0
    sget-object v0, Lbyd;->U:[B

    const/4 v2, 0x0

    invoke-static {v0, p1, p2, v2}, Lbyd;->a([B[III)I

    .line 367
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 368
    const/4 v0, 0x1

    goto :goto_0

    .line 367
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a([B)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 255
    array-length v2, p0

    const/4 v3, 0x3

    if-lt v2, v3, :cond_0

    aget-byte v2, p0, v1

    const/16 v3, 0x47

    if-ne v2, v3, :cond_0

    aget-byte v2, p0, v0

    const/16 v3, 0x49

    if-ne v2, v3, :cond_0

    const/4 v2, 0x2

    aget-byte v2, p0, v2

    const/16 v3, 0x46

    if-ne v2, v3, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method static synthetic b(Lbyd;)Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lbyd;->N:Z

    return v0
.end method

.method static synthetic c(Lbyd;)V
    .locals 22

    .prologue
    .line 39
    move-object/from16 v0, p0

    iget v2, v0, Lbyd;->s:I

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lbyd;->s:I

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lbyd;->t:Z

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lbyd;->P:Z

    const/16 v2, 0x64

    move-object/from16 v0, p0

    iput v2, v0, Lbyd;->H:I

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lbyd;->j:[I

    :cond_1
    :goto_1
    :sswitch_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lbyd;->Q:[B

    move-object/from16 v0, p0

    iget v3, v0, Lbyd;->X:I

    add-int/lit8 v4, v3, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lbyd;->X:I

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    sparse-switch v2, :sswitch_data_0

    goto :goto_1

    :sswitch_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lbyd;->Q:[B

    move-object/from16 v0, p0

    iget v3, v0, Lbyd;->X:I

    add-int/lit8 v4, v3, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lbyd;->X:I

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    sparse-switch v2, :sswitch_data_1

    invoke-direct/range {p0 .. p0}, Lbyd;->k()V

    goto :goto_1

    :pswitch_0
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lbyd;->z:Z

    goto :goto_0

    :pswitch_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lbyd;->z:Z

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lbyd;->A:[I

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lbyd;->T:[I

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lbyd;->A:[I

    array-length v6, v6

    invoke-static {v2, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    :pswitch_2
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lbyd;->z:Z

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lbyd;->t:Z

    if-nez v3, :cond_2

    move-object/from16 v0, p0

    iget v2, v0, Lbyd;->g:I

    :cond_2
    const/4 v3, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget v4, v0, Lbyd;->p:I

    if-ge v3, v4, :cond_0

    move-object/from16 v0, p0

    iget v4, v0, Lbyd;->n:I

    add-int/2addr v4, v3

    move-object/from16 v0, p0

    iget v5, v0, Lbyd;->R:I

    mul-int/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lbyd;->m:I

    add-int/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lbyd;->o:I

    add-int/2addr v5, v4

    :goto_3
    if-ge v4, v5, :cond_3

    move-object/from16 v0, p0

    iget-object v6, v0, Lbyd;->T:[I

    aput v2, v6, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :sswitch_2
    move-object/from16 v0, p0

    iget v2, v0, Lbyd;->X:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lbyd;->X:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lbyd;->Q:[B

    move-object/from16 v0, p0

    iget v3, v0, Lbyd;->X:I

    add-int/lit8 v4, v3, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lbyd;->X:I

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    and-int/lit8 v3, v2, 0x1c

    shr-int/lit8 v3, v3, 0x2

    move-object/from16 v0, p0

    iput v3, v0, Lbyd;->s:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :goto_4
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lbyd;->t:Z

    invoke-direct/range {p0 .. p0}, Lbyd;->j()I

    move-result v2

    mul-int/lit8 v2, v2, 0xa

    move-object/from16 v0, p0

    iput v2, v0, Lbyd;->H:I

    move-object/from16 v0, p0

    iget v2, v0, Lbyd;->H:I

    const/16 v3, 0xa

    if-gt v2, v3, :cond_4

    const/16 v2, 0x64

    move-object/from16 v0, p0

    iput v2, v0, Lbyd;->H:I

    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lbyd;->Q:[B

    move-object/from16 v0, p0

    iget v3, v0, Lbyd;->X:I

    add-int/lit8 v4, v3, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lbyd;->X:I

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    move-object/from16 v0, p0

    iput v2, v0, Lbyd;->u:I

    move-object/from16 v0, p0

    iget v2, v0, Lbyd;->X:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lbyd;->X:I

    goto/16 :goto_1

    :cond_5
    const/4 v2, 0x0

    goto :goto_4

    :sswitch_3
    invoke-direct/range {p0 .. p0}, Lbyd;->i()I

    const/4 v3, 0x1

    const/4 v2, 0x0

    :goto_5
    sget-object v4, Lbyd;->d:[B

    array-length v4, v4

    if-ge v2, v4, :cond_2a

    move-object/from16 v0, p0

    iget-object v4, v0, Lbyd;->r:[B

    aget-byte v4, v4, v2

    sget-object v5, Lbyd;->d:[B

    aget-byte v5, v5, v2

    if-eq v4, v5, :cond_7

    const/4 v2, 0x0

    :goto_6
    if-eqz v2, :cond_8

    :cond_6
    invoke-direct/range {p0 .. p0}, Lbyd;->i()I

    move-result v2

    if-lez v2, :cond_1

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lbyd;->e:Z

    if-eqz v2, :cond_6

    goto/16 :goto_1

    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_8
    invoke-direct/range {p0 .. p0}, Lbyd;->k()V

    goto/16 :goto_1

    :sswitch_4
    invoke-direct/range {p0 .. p0}, Lbyd;->k()V

    goto/16 :goto_1

    :sswitch_5
    invoke-direct/range {p0 .. p0}, Lbyd;->k()V

    goto/16 :goto_1

    :sswitch_6
    invoke-direct/range {p0 .. p0}, Lbyd;->j()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lbyd;->m:I

    invoke-direct/range {p0 .. p0}, Lbyd;->j()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lbyd;->n:I

    invoke-direct/range {p0 .. p0}, Lbyd;->j()I

    move-result v2

    invoke-direct/range {p0 .. p0}, Lbyd;->j()I

    move-result v3

    move-object/from16 v0, p0

    iget v4, v0, Lbyd;->R:I

    move-object/from16 v0, p0

    iget v5, v0, Lbyd;->m:I

    sub-int/2addr v4, v5

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lbyd;->o:I

    move-object/from16 v0, p0

    iget v4, v0, Lbyd;->S:I

    move-object/from16 v0, p0

    iget v5, v0, Lbyd;->n:I

    sub-int/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lbyd;->p:I

    move-object/from16 v0, p0

    iput v2, v0, Lbyd;->q:I

    mul-int/2addr v2, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lbyd;->y:[B

    array-length v3, v3

    if-le v2, v3, :cond_9

    new-array v2, v2, [B

    move-object/from16 v0, p0

    iput-object v2, v0, Lbyd;->y:[B

    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lbyd;->Q:[B

    move-object/from16 v0, p0

    iget v3, v0, Lbyd;->X:I

    add-int/lit8 v4, v3, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lbyd;->X:I

    aget-byte v2, v2, v3

    and-int/lit16 v3, v2, 0xff

    and-int/lit8 v2, v3, 0x40

    if-eqz v2, :cond_e

    const/4 v2, 0x1

    :goto_7
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lbyd;->l:Z

    and-int/lit16 v2, v3, 0x80

    if-eqz v2, :cond_f

    const/4 v2, 0x1

    :goto_8
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lbyd;->h:Z

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    and-int/lit8 v2, v3, 0x7

    add-int/lit8 v2, v2, 0x1

    int-to-double v2, v2

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    double-to-int v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lbyd;->i:I

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lbyd;->h:Z

    if-eqz v2, :cond_10

    move-object/from16 v0, p0

    iget-object v2, v0, Lbyd;->j:[I

    if-nez v2, :cond_a

    const/16 v2, 0x100

    new-array v2, v2, [I

    move-object/from16 v0, p0

    iput-object v2, v0, Lbyd;->j:[I

    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lbyd;->Q:[B

    move-object/from16 v0, p0

    iget-object v3, v0, Lbyd;->j:[I

    move-object/from16 v0, p0

    iget v4, v0, Lbyd;->i:I

    move-object/from16 v0, p0

    iget v5, v0, Lbyd;->X:I

    invoke-static {v2, v3, v4, v5}, Lbyd;->a([B[III)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lbyd;->X:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lbyd;->j:[I

    move-object/from16 v0, p0

    iput-object v2, v0, Lbyd;->k:[I

    :cond_b
    :goto_9
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lbyd;->t:Z

    if-eqz v3, :cond_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lbyd;->k:[I

    move-object/from16 v0, p0

    iget v3, v0, Lbyd;->u:I

    aget v2, v2, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lbyd;->k:[I

    move-object/from16 v0, p0

    iget v4, v0, Lbyd;->u:I

    const/4 v5, 0x0

    aput v5, v3, v4

    :cond_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lbyd;->k:[I

    if-nez v3, :cond_d

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lbyd;->e:Z

    const-string v3, "Babel"

    const-string v4, "Could not read color table from the gif. Abort."

    invoke-static {v3, v4}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    :cond_d
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lbyd;->e:Z

    if-nez v3, :cond_25

    move-object/from16 v0, p0

    iget v3, v0, Lbyd;->o:I

    move-object/from16 v0, p0

    iget v4, v0, Lbyd;->p:I

    mul-int v14, v3, v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lbyd;->Q:[B

    move-object/from16 v0, p0

    iget v4, v0, Lbyd;->X:I

    add-int/lit8 v5, v4, 0x1

    move-object/from16 v0, p0

    iput v5, v0, Lbyd;->X:I

    aget-byte v3, v3, v4

    and-int/lit16 v15, v3, 0xff

    const/4 v3, 0x1

    shl-int v16, v3, v15

    add-int/lit8 v17, v16, 0x1

    add-int/lit8 v11, v16, 0x2

    const/4 v10, -0x1

    add-int/lit8 v9, v15, 0x1

    const/4 v3, 0x1

    shl-int/2addr v3, v9

    add-int/lit8 v8, v3, -0x1

    const/4 v3, 0x0

    :goto_a
    move/from16 v0, v16

    if-ge v3, v0, :cond_11

    move-object/from16 v0, p0

    iget-object v4, v0, Lbyd;->v:[S

    const/4 v5, 0x0

    aput-short v5, v4, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lbyd;->w:[B

    int-to-byte v5, v3

    aput-byte v5, v4, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    :cond_e
    const/4 v2, 0x0

    goto/16 :goto_7

    :cond_f
    const/4 v2, 0x0

    goto/16 :goto_8

    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lbyd;->b:[I

    move-object/from16 v0, p0

    iput-object v2, v0, Lbyd;->k:[I

    move-object/from16 v0, p0

    iget v2, v0, Lbyd;->c:I

    move-object/from16 v0, p0

    iget v3, v0, Lbyd;->u:I

    if-ne v2, v3, :cond_b

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lbyd;->g:I

    goto/16 :goto_9

    :cond_11
    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    :cond_12
    if-ge v3, v14, :cond_21

    move-object/from16 v0, p0

    iget-object v12, v0, Lbyd;->Q:[B

    move-object/from16 v0, p0

    iget v13, v0, Lbyd;->X:I

    add-int/lit8 v18, v13, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lbyd;->X:I

    aget-byte v12, v12, v13

    and-int/lit16 v12, v12, 0xff

    if-eqz v12, :cond_21

    move-object/from16 v0, p0

    iget v13, v0, Lbyd;->X:I

    add-int v18, v13, v12

    :cond_13
    move-object/from16 v0, p0

    iget v12, v0, Lbyd;->X:I

    move/from16 v0, v18

    if-ge v12, v0, :cond_12

    move-object/from16 v0, p0

    iget-object v12, v0, Lbyd;->Q:[B

    move-object/from16 v0, p0

    iget v13, v0, Lbyd;->X:I

    add-int/lit8 v19, v13, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lbyd;->X:I

    aget-byte v12, v12, v13

    and-int/lit16 v12, v12, 0xff

    shl-int/2addr v12, v6

    add-int/2addr v7, v12

    add-int/lit8 v6, v6, 0x8

    :goto_b
    if-lt v6, v9, :cond_13

    and-int v12, v7, v8

    shr-int v13, v7, v9

    sub-int v7, v6, v9

    move/from16 v0, v16

    if-ne v12, v0, :cond_14

    add-int/lit8 v9, v15, 0x1

    const/4 v6, 0x1

    shl-int/2addr v6, v9

    add-int/lit8 v8, v6, -0x1

    add-int/lit8 v11, v16, 0x2

    const/4 v10, -0x1

    move v6, v7

    move v7, v13

    goto :goto_b

    :cond_14
    move/from16 v0, v17

    if-ne v12, v0, :cond_1a

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lbyd;->X:I

    :cond_15
    :goto_c
    invoke-direct/range {p0 .. p0}, Lbyd;->k()V

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lbyd;->e:Z

    if-nez v3, :cond_25

    move-object/from16 v0, p0

    iget v3, v0, Lbyd;->s:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_17

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lbyd;->z:Z

    if-nez v3, :cond_17

    move-object/from16 v0, p0

    iget-object v3, v0, Lbyd;->A:[I

    if-nez v3, :cond_16

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lbyd;->A:[I

    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lbyd;->T:[I

    array-length v3, v3

    new-array v3, v3, [I

    move-object/from16 v0, p0

    iput-object v3, v0, Lbyd;->A:[I
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    :cond_16
    :goto_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lbyd;->A:[I

    if-eqz v3, :cond_17

    move-object/from16 v0, p0

    iget-object v3, v0, Lbyd;->T:[I

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lbyd;->A:[I

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lbyd;->T:[I

    array-length v7, v7

    invoke-static {v3, v4, v5, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lbyd;->z:Z

    :cond_17
    const/4 v6, 0x1

    const/16 v5, 0x8

    const/4 v4, 0x0

    const/4 v3, 0x0

    :goto_e
    move-object/from16 v0, p0

    iget v7, v0, Lbyd;->p:I

    if-ge v3, v7, :cond_23

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lbyd;->l:Z

    if-eqz v7, :cond_26

    move-object/from16 v0, p0

    iget v7, v0, Lbyd;->p:I

    if-lt v4, v7, :cond_18

    add-int/lit8 v6, v6, 0x1

    packed-switch v6, :pswitch_data_1

    :cond_18
    :goto_f
    add-int v7, v4, v5

    move/from16 v21, v4

    move v4, v7

    move/from16 v7, v21

    :goto_10
    move-object/from16 v0, p0

    iget v8, v0, Lbyd;->n:I

    add-int/2addr v7, v8

    move-object/from16 v0, p0

    iget v8, v0, Lbyd;->S:I

    if-ge v7, v8, :cond_22

    move-object/from16 v0, p0

    iget v8, v0, Lbyd;->R:I

    mul-int/2addr v7, v8

    move-object/from16 v0, p0

    iget v8, v0, Lbyd;->m:I

    add-int/2addr v8, v7

    move-object/from16 v0, p0

    iget v7, v0, Lbyd;->o:I

    add-int v10, v8, v7

    move-object/from16 v0, p0

    iget v7, v0, Lbyd;->q:I

    mul-int/2addr v7, v3

    move v9, v8

    :goto_11
    if-ge v9, v10, :cond_22

    move-object/from16 v0, p0

    iget-object v11, v0, Lbyd;->y:[B

    add-int/lit8 v8, v7, 0x1

    aget-byte v7, v11, v7

    and-int/lit16 v7, v7, 0xff

    move-object/from16 v0, p0

    iget-object v11, v0, Lbyd;->k:[I

    aget v7, v11, v7

    if-eqz v7, :cond_19

    move-object/from16 v0, p0

    iget-object v11, v0, Lbyd;->T:[I

    aput v7, v11, v9

    :cond_19
    add-int/lit8 v7, v9, 0x1

    move v9, v7

    move v7, v8

    goto :goto_11

    :cond_1a
    const/4 v6, -0x1

    if-ne v10, v6, :cond_1b

    move-object/from16 v0, p0

    iget-object v6, v0, Lbyd;->y:[B

    add-int/lit8 v5, v3, 0x1

    move-object/from16 v0, p0

    iget-object v10, v0, Lbyd;->w:[B

    aget-byte v10, v10, v12

    aput-byte v10, v6, v3

    move v3, v5

    move v6, v7

    move v10, v12

    move v7, v13

    move v5, v12

    goto/16 :goto_b

    :cond_1b
    if-lt v12, v11, :cond_29

    move-object/from16 v0, p0

    iget-object v0, v0, Lbyd;->x:[B

    move-object/from16 v19, v0

    add-int/lit8 v6, v4, 0x1

    int-to-byte v5, v5

    aput-byte v5, v19, v4

    const/16 v4, 0x1001

    if-ne v6, v4, :cond_28

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lbyd;->e:Z

    const-string v3, "Babel"

    const-string v4, "Error while decoding Gif."

    invoke-static {v3, v4}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_c

    :cond_1c
    move v5, v6

    :goto_12
    move/from16 v0, v16

    if-lt v4, v0, :cond_1f

    const/16 v6, 0x1001

    if-ge v4, v6, :cond_1d

    move-object/from16 v0, p0

    iget-object v6, v0, Lbyd;->v:[S

    aget-short v6, v6, v4

    if-ne v4, v6, :cond_1e

    :cond_1d
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lbyd;->e:Z

    const-string v3, "Babel"

    const-string v4, "Error while decoding Gif."

    invoke-static {v3, v4}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_c

    :cond_1e
    move-object/from16 v0, p0

    iget-object v0, v0, Lbyd;->x:[B

    move-object/from16 v19, v0

    add-int/lit8 v6, v5, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lbyd;->w:[B

    move-object/from16 v20, v0

    aget-byte v20, v20, v4

    aput-byte v20, v19, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lbyd;->v:[S

    aget-short v4, v5, v4

    const/16 v5, 0x1001

    if-ne v6, v5, :cond_1c

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lbyd;->e:Z

    const-string v3, "Babel"

    const-string v4, "Error while decoding Gif."

    invoke-static {v3, v4}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_c

    :cond_1f
    move-object/from16 v0, p0

    iget-object v6, v0, Lbyd;->w:[B

    aget-byte v6, v6, v4

    move-object/from16 v0, p0

    iget-object v0, v0, Lbyd;->x:[B

    move-object/from16 v19, v0

    add-int/lit8 v4, v5, 0x1

    int-to-byte v0, v6

    move/from16 v20, v0

    aput-byte v20, v19, v5

    const/16 v5, 0x1000

    if-ge v11, v5, :cond_20

    move-object/from16 v0, p0

    iget-object v5, v0, Lbyd;->v:[S

    int-to-short v10, v10

    aput-short v10, v5, v11

    move-object/from16 v0, p0

    iget-object v5, v0, Lbyd;->w:[B

    int-to-byte v10, v6

    aput-byte v10, v5, v11

    add-int/lit8 v11, v11, 0x1

    and-int v5, v11, v8

    if-nez v5, :cond_20

    const/16 v5, 0x1000

    if-ge v11, v5, :cond_20

    add-int/lit8 v9, v9, 0x1

    add-int/2addr v8, v11

    :cond_20
    move v5, v4

    :goto_13
    move-object/from16 v0, p0

    iget-object v10, v0, Lbyd;->y:[B

    add-int/lit8 v4, v3, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lbyd;->x:[B

    move-object/from16 v19, v0

    add-int/lit8 v5, v5, -0x1

    aget-byte v19, v19, v5

    aput-byte v19, v10, v3

    if-gtz v5, :cond_27

    move v3, v4

    move v10, v12

    move v4, v5

    move v5, v6

    move v6, v7

    move v7, v13

    goto/16 :goto_b

    :cond_21
    :goto_14
    if-ge v3, v14, :cond_15

    move-object/from16 v0, p0

    iget-object v5, v0, Lbyd;->y:[B

    add-int/lit8 v4, v3, 0x1

    const/4 v6, 0x0

    aput-byte v6, v5, v3

    move v3, v4

    goto :goto_14

    :catch_0
    move-exception v3

    const-string v4, "Babel"

    const-string v5, "Gif backupFrame threw an OOME"

    invoke-static {v4, v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_d

    :pswitch_3
    const/4 v4, 0x4

    goto/16 :goto_f

    :pswitch_4
    const/4 v4, 0x2

    const/4 v5, 0x4

    goto/16 :goto_f

    :pswitch_5
    const/4 v4, 0x1

    const/4 v5, 0x2

    goto/16 :goto_f

    :cond_22
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_e

    :cond_23
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lbyd;->t:Z

    if-eqz v3, :cond_24

    move-object/from16 v0, p0

    iget-object v3, v0, Lbyd;->k:[I

    move-object/from16 v0, p0

    iget v4, v0, Lbyd;->u:I

    aput v2, v3, v4

    :cond_24
    move-object/from16 v0, p0

    iget v2, v0, Lbyd;->B:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lbyd;->B:I

    :cond_25
    :goto_15
    return-void

    :sswitch_7
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lbyd;->P:Z

    goto :goto_15

    :cond_26
    move v7, v3

    goto/16 :goto_10

    :cond_27
    move v3, v4

    goto :goto_13

    :cond_28
    move v4, v10

    move v5, v6

    goto/16 :goto_12

    :cond_29
    move v5, v4

    move v4, v12

    goto/16 :goto_12

    :cond_2a
    move v2, v3

    goto/16 :goto_6

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x21 -> :sswitch_1
        0x2c -> :sswitch_6
        0x3b -> :sswitch_7
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_5
        0xf9 -> :sswitch_2
        0xfe -> :sswitch_4
        0xff -> :sswitch_3
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method static synthetic d(Lbyd;)Z
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbyd;->P:Z

    return v0
.end method

.method static synthetic e(Lbyd;)Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lbyd;->P:Z

    return v0
.end method

.method static synthetic f(Lbyd;)I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lbyd;->B:I

    return v0
.end method

.method static synthetic g(Lbyd;)Z
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbyd;->e:Z

    return v0
.end method

.method static synthetic h(Lbyd;)Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lbyd;->Y:Z

    return v0
.end method

.method private i()I
    .locals 5

    .prologue
    .line 924
    iget-object v0, p0, Lbyd;->Q:[B

    iget v1, p0, Lbyd;->X:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lbyd;->X:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    .line 925
    if-lez v0, :cond_0

    .line 926
    iget-object v1, p0, Lbyd;->Q:[B

    iget v2, p0, Lbyd;->X:I

    iget-object v3, p0, Lbyd;->r:[B

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 927
    iget v1, p0, Lbyd;->X:I

    add-int/2addr v1, v0

    iput v1, p0, Lbyd;->X:I

    .line 929
    :cond_0
    return v0
.end method

.method static synthetic i(Lbyd;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 39
    iget v0, p0, Lbyd;->a:I

    iput v0, p0, Lbyd;->X:I

    iput-boolean v1, p0, Lbyd;->z:Z

    iput v1, p0, Lbyd;->B:I

    iput v1, p0, Lbyd;->s:I

    return-void
.end method

.method private j()I
    .locals 4

    .prologue
    .line 937
    iget-object v0, p0, Lbyd;->Q:[B

    iget v1, p0, Lbyd;->X:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lbyd;->X:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    .line 938
    iget-object v1, p0, Lbyd;->Q:[B

    iget v2, p0, Lbyd;->X:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lbyd;->X:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    .line 939
    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    return v0
.end method

.method static synthetic j(Lbyd;)Z
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbyd;->f:Z

    return v0
.end method

.method private k()V
    .locals 3

    .prologue
    .line 949
    :cond_0
    iget-object v0, p0, Lbyd;->Q:[B

    iget v1, p0, Lbyd;->X:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lbyd;->X:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    .line 950
    iget v1, p0, Lbyd;->X:I

    add-int/2addr v1, v0

    iput v1, p0, Lbyd;->X:I

    .line 951
    if-gtz v0, :cond_0

    .line 952
    return-void
.end method

.method static synthetic k(Lbyd;)Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lbyd;->e:Z

    return v0
.end method

.method static synthetic l(Lbyd;)Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lbyd;->f:Z

    return v0
.end method

.method static synthetic m(Lbyd;)I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lbyd;->H:I

    return v0
.end method

.method static synthetic n(Lbyd;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lbyd;->K:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 401
    return-void
.end method

.method public a(Lbyg;)V
    .locals 2

    .prologue
    .line 284
    iput-object p1, p0, Lbyd;->Z:Lbyg;

    .line 285
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lbyd;->O:Z

    if-eqz v0, :cond_0

    .line 286
    iget-object v0, p0, Lbyd;->Z:Lbyg;

    iget-object v1, p0, Lbyd;->C:Landroid/graphics/Bitmap;

    invoke-interface {v0, v1}, Lbyg;->a(Landroid/graphics/Bitmap;)V

    .line 288
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 473
    iget-boolean v0, p0, Lbyd;->J:Z

    if-ne v0, p1, :cond_0

    .line 483
    :goto_0
    return-void

    .line 477
    :cond_0
    iput-boolean p1, p0, Lbyd;->J:Z

    .line 478
    iget-boolean v0, p0, Lbyd;->J:Z

    if-eqz v0, :cond_1

    .line 479
    invoke-virtual {p0}, Lbyd;->f()V

    goto :goto_0

    .line 481
    :cond_1
    invoke-virtual {p0}, Lbyd;->g()V

    goto :goto_0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 404
    return-void
.end method

.method public c()I
    .locals 1

    .prologue
    .line 267
    iget v0, p0, Lbyd;->R:I

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 271
    iget v0, p0, Lbyd;->S:I

    return v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 389
    iget-boolean v0, p0, Lbyd;->e:Z

    return v0
.end method

.method public f()V
    .locals 2

    .prologue
    .line 418
    iget-boolean v0, p0, Lbyd;->F:Z

    if-nez v0, :cond_0

    .line 419
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbyd;->F:Z

    .line 420
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lbyd;->E:J

    .line 421
    invoke-virtual {p0}, Lbyd;->run()V

    .line 423
    :cond_0
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 426
    iget-boolean v0, p0, Lbyd;->F:Z

    if-eqz v0, :cond_0

    .line 427
    invoke-direct {p0, p0}, Lbyd;->a(Ljava/lang/Runnable;)V

    .line 429
    :cond_0
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    .line 733
    iget-object v0, p0, Lbyd;->C:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 734
    iget-object v0, p0, Lbyd;->D:Lbxs;

    iget-object v1, p0, Lbyd;->C:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lbxs;->a(Landroid/graphics/Bitmap;)V

    .line 735
    const/4 v0, 0x0

    iput-object v0, p0, Lbyd;->C:Landroid/graphics/Bitmap;

    .line 737
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbyd;->N:Z

    .line 738
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 956
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0xb

    if-ne v0, v1, :cond_3

    .line 957
    iget v0, p1, Landroid/os/Message;->arg1:I

    iput v0, p0, Lbyd;->G:I

    .line 958
    iget-object v0, p0, Lbyd;->C:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 959
    iget-object v0, p0, Lbyd;->C:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lbyd;->T:[I

    iget v3, p0, Lbyd;->R:I

    iget v6, p0, Lbyd;->R:I

    iget v7, p0, Lbyd;->S:I

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 961
    iput-boolean v8, p0, Lbyd;->O:Z

    .line 962
    iput-boolean v2, p0, Lbyd;->I:Z

    .line 964
    iget-object v0, p0, Lbyd;->Z:Lbyg;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lbyd;->e:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lbyd;->O:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lbyd;->N:Z

    if-nez v0, :cond_0

    iget v0, p0, Lbyd;->o:I

    if-lez v0, :cond_0

    iget v0, p0, Lbyd;->p:I

    if-lez v0, :cond_0

    move v2, v8

    :cond_0
    if-eqz v2, :cond_1

    .line 965
    iget-object v0, p0, Lbyd;->Z:Lbyg;

    iget-object v1, p0, Lbyd;->C:Landroid/graphics/Bitmap;

    invoke-interface {v0, v1}, Lbyg;->a(Landroid/graphics/Bitmap;)V

    .line 966
    iget-boolean v0, p0, Lbyd;->F:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lbyd;->I:Z

    if-nez v0, :cond_1

    iget-wide v0, p0, Lbyd;->E:J

    iget v2, p0, Lbyd;->G:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x5

    add-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lbyd;->E:J

    iget-wide v0, p0, Lbyd;->E:J

    iget-boolean v2, p0, Lbyd;->J:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lbyd;->K:Landroid/os/Handler;

    invoke-virtual {v2, p0, v0, v1}, Landroid/os/Handler;->postAtTime(Ljava/lang/Runnable;J)Z

    iput-boolean v8, p0, Lbyd;->I:Z

    .line 969
    :cond_1
    :goto_0
    iget-object v0, p0, Lbyd;->aa:La;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lbyd;->f:Z

    if-eqz v0, :cond_2

    .line 970
    iget-object v0, p0, Lbyd;->aa:La;

    iget-boolean v0, p0, Lbyd;->e:Z

    :cond_2
    move v2, v8

    .line 974
    :cond_3
    return v2

    .line 966
    :cond_4
    invoke-direct {p0, p0}, Lbyd;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public run()V
    .locals 3

    .prologue
    .line 448
    iget-boolean v0, p0, Lbyd;->N:Z

    if-eqz v0, :cond_1

    .line 456
    :cond_0
    :goto_0
    return-void

    .line 453
    :cond_1
    iget-boolean v0, p0, Lbyd;->f:Z

    if-nez v0, :cond_0

    .line 454
    sget-object v0, Lbyd;->M:Landroid/os/Handler;

    sget-object v1, Lbyd;->M:Landroid/os/Handler;

    const/16 v2, 0xa

    invoke-virtual {v1, v2, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
