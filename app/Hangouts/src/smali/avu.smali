.class public final Lavu;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Lavu;->b:Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;

    iput-object p2, p0, Lavu;->a:Ljava/lang/String;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 122
    const-string v3, "numeric =?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v1, p0, Lavu;->a:Ljava/lang/String;

    aput-object v1, v4, v0

    iget-object v0, p0, Lavu;->b:Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;->a(Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "apn"

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;->a()[Ljava/lang/String;

    move-result-object v2

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 122
    check-cast p1, Landroid/database/Cursor;

    if-eqz p1, :cond_5

    iget-object v0, p0, Lavu;->b:Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;

    const-string v1, "apn_list"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    invoke-virtual {v0}, Landroid/preference/PreferenceGroup;->removeAll()V

    iget-object v1, p0, Lavu;->b:Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;

    iget-object v4, p0, Lavu;->b:Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;

    invoke-static {v4}, Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;->a(Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    iget-object v5, p0, Lavu;->a:Ljava/lang/String;

    invoke-static {v4, v5}, Lbwq;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;->a(Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;Ljava/lang/String;)Ljava/lang/String;

    move v1, v2

    :cond_0
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x3

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "mms"

    invoke-static {v7, v8}, Lbwq;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    new-instance v7, Lavt;

    iget-object v8, p0, Lavu;->b:Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;

    invoke-direct {v7, v8, v3}, Lavt;-><init>(Landroid/content/Context;B)V

    invoke-virtual {v7, v6}, Lavt;->setKey(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Lavt;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v7, v5}, Lavt;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {v7, v3}, Lavt;->setPersistent(Z)V

    iget-object v4, p0, Lavu;->b:Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;

    invoke-virtual {v7, v4}, Lavt;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    invoke-virtual {v7, v2}, Lavt;->setSelectable(Z)V

    iget-object v4, p0, Lavu;->b:Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;

    invoke-static {v4}, Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;->b(Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lavu;->b:Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;

    invoke-static {v4}, Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;->b(Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    :cond_1
    if-eqz v1, :cond_3

    iget-object v4, p0, Lavu;->b:Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;

    invoke-static {v4}, Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;->b(Lcom/google/android/apps/hangouts/phone/ApnSettingsActivity;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_3

    :cond_2
    invoke-virtual {v7}, Lavt;->a()V

    move v1, v3

    :cond_3
    invoke-virtual {v0, v7}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0

    :cond_4
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    :cond_5
    return-void
.end method
