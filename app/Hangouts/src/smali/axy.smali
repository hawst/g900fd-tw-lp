.class public final Laxy;
.super Ldg;
.source "PG"

# interfaces
.implements Laac;
.implements Lqm;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldg",
        "<",
        "Lqn;",
        ">;",
        "Laac;",
        "Lqm;"
    }
.end annotation


# instance fields
.field private final a:Lyj;

.field private final b:I

.field private c:Lbyq;

.field private d:Lzx;

.field private final e:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILyj;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1}, Ldg;-><init>(Landroid/content/Context;)V

    .line 41
    iput-object p1, p0, Laxy;->e:Landroid/content/Context;

    .line 42
    iput-object p4, p0, Laxy;->a:Lyj;

    .line 43
    iput p3, p0, Laxy;->b:I

    .line 44
    iget v0, p0, Laxy;->b:I

    invoke-direct {p0, p2, v0}, Laxy;->a(Ljava/lang/String;I)V

    .line 45
    return-void
.end method

.method private a(Ljava/lang/String;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 53
    const/4 v0, -0x1

    .line 54
    packed-switch p2, :pswitch_data_0

    .line 72
    :goto_0
    new-instance v1, Lbyq;

    iget-object v2, p0, Laxy;->a:Lyj;

    invoke-direct {v1, p1, v2}, Lbyq;-><init>(Ljava/lang/String;Lyj;)V

    invoke-virtual {v1, v0, v0}, Lbyq;->a(II)Lbyq;

    move-result-object v0

    invoke-virtual {v0, v3}, Lbyq;->a(Z)Lbyq;

    move-result-object v0

    .line 73
    invoke-virtual {v0, v3}, Lbyq;->d(Z)Lbyq;

    move-result-object v0

    iput-object v0, p0, Laxy;->c:Lbyq;

    .line 74
    if-ne p2, v4, :cond_0

    .line 75
    iget-object v0, p0, Laxy;->c:Lbyq;

    invoke-virtual {v0, v4}, Lbyq;->b(Z)Lbyq;

    .line 77
    :cond_0
    new-instance v0, Lzx;

    iget-object v1, p0, Laxy;->c:Lbyq;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v4, v2}, Lzx;-><init>(Lbyq;Laac;ZLjava/lang/Object;)V

    iput-object v0, p0, Laxy;->d:Lzx;

    .line 79
    return-void

    .line 56
    :pswitch_0
    invoke-static {}, Lyn;->b()I

    move-result v0

    goto :goto_0

    .line 59
    :pswitch_1
    iget-object v0, p0, Laxy;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lf;->cP:I

    .line 60
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_0

    .line 69
    :pswitch_2
    sget v0, Lpr;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0, v3}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    goto :goto_0

    .line 54
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Lqn;)V
    .locals 1

    .prologue
    .line 88
    invoke-virtual {p0}, Laxy;->o()Z

    move-result v0

    if-nez v0, :cond_0

    .line 93
    invoke-virtual {p0}, Laxy;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    invoke-super {p0, p1}, Ldg;->b(Ljava/lang/Object;)V

    .line 96
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lbzn;Lbyd;ZLzx;Z)V
    .locals 4

    .prologue
    .line 102
    iget-object v0, p0, Laxy;->d:Lzx;

    if-ne p4, v0, :cond_3

    .line 104
    const/4 v0, 0x0

    iput-object v0, p0, Laxy;->d:Lzx;

    .line 105
    new-instance v0, Lqn;

    invoke-direct {v0}, Lqn;-><init>()V

    .line 106
    if-eqz p3, :cond_2

    .line 107
    const/4 v1, 0x0

    iput v1, v0, Lqn;->c:I

    .line 108
    if-eqz p2, :cond_1

    .line 109
    invoke-virtual {p2}, Lbyd;->e()Z

    move-result v1

    if-nez v1, :cond_2

    .line 110
    new-instance v1, Lccm;

    invoke-direct {v1, p2}, Lccm;-><init>(Lbyd;)V

    iput-object v1, v0, Lqn;->a:Landroid/graphics/drawable/Drawable;

    .line 128
    :goto_0
    invoke-direct {p0, v0}, Laxy;->a(Lqn;)V

    .line 136
    :cond_0
    :goto_1
    return-void

    .line 115
    :cond_1
    invoke-static {p1}, Lcwz;->b(Ljava/lang/Object;)V

    .line 116
    invoke-virtual {p1}, Lbzn;->d()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 117
    invoke-static {v1}, Lcwz;->b(Ljava/lang/Object;)V

    .line 118
    const/16 v2, 0xa0

    invoke-virtual {v1, v2}, Landroid/graphics/Bitmap;->setDensity(I)V

    .line 119
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v3, p0, Laxy;->e:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v2, v0, Lqn;->a:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 126
    :cond_2
    const/4 v1, 0x1

    iput v1, v0, Lqn;->c:I

    goto :goto_0

    .line 132
    :cond_3
    if-eqz p1, :cond_0

    .line 133
    invoke-virtual {p1}, Lbzn;->b()V

    goto :goto_1
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Laxy;->b:I

    invoke-direct {p0, p1, v0}, Laxy;->a(Ljava/lang/String;I)V

    .line 50
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 28
    check-cast p1, Lqn;

    invoke-direct {p0, p1}, Laxy;->a(Lqn;)V

    return-void
.end method

.method protected g()V
    .locals 2

    .prologue
    .line 83
    invoke-static {}, Lbsn;->c()Lbsn;

    move-result-object v0

    iget-object v1, p0, Laxy;->d:Lzx;

    invoke-virtual {v0, v1}, Lbsn;->a(Lbrv;)Z

    .line 84
    return-void
.end method
