.class public final Leid;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leid;


# instance fields
.field public b:[Ljava/lang/Integer;

.field public c:[Ljava/lang/Float;

.field public d:Ljava/lang/Boolean;

.field public e:Ljava/lang/Boolean;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Lfab;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Leid;

    sput-object v0, Leid;->a:[Leid;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 13
    sget-object v0, Lept;->m:[Ljava/lang/Integer;

    iput-object v0, p0, Leid;->b:[Ljava/lang/Integer;

    .line 16
    sget-object v0, Lept;->o:[Ljava/lang/Float;

    iput-object v0, p0, Leid;->c:[Ljava/lang/Float;

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Leid;->h:Lfab;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 64
    iget-object v1, p0, Leid;->b:[Ljava/lang/Integer;

    if-eqz v1, :cond_1

    iget-object v1, p0, Leid;->b:[Ljava/lang/Integer;

    array-length v1, v1

    if-lez v1, :cond_1

    .line 66
    iget-object v2, p0, Leid;->b:[Ljava/lang/Integer;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 68
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Lepl;->f(I)I

    move-result v4

    add-int/2addr v1, v4

    .line 66
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 71
    :cond_0
    iget-object v0, p0, Leid;->b:[Ljava/lang/Integer;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    .line 73
    :cond_1
    iget-object v1, p0, Leid;->c:[Ljava/lang/Float;

    if-eqz v1, :cond_2

    iget-object v1, p0, Leid;->c:[Ljava/lang/Float;

    array-length v1, v1

    if-lez v1, :cond_2

    .line 74
    iget-object v1, p0, Leid;->c:[Ljava/lang/Float;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    .line 75
    add-int/2addr v0, v1

    .line 76
    iget-object v1, p0, Leid;->c:[Ljava/lang/Float;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 78
    :cond_2
    iget-object v1, p0, Leid;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 79
    const/4 v1, 0x3

    iget-object v2, p0, Leid;->e:Ljava/lang/Boolean;

    .line 80
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 82
    :cond_3
    iget-object v1, p0, Leid;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 83
    const/4 v1, 0x4

    iget-object v2, p0, Leid;->f:Ljava/lang/String;

    .line 84
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 86
    :cond_4
    iget-object v1, p0, Leid;->g:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 87
    const/4 v1, 0x5

    iget-object v2, p0, Leid;->g:Ljava/lang/String;

    .line 88
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 90
    :cond_5
    iget-object v1, p0, Leid;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 91
    const/4 v1, 0x6

    iget-object v2, p0, Leid;->d:Ljava/lang/Boolean;

    .line 92
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 94
    :cond_6
    iget-object v1, p0, Leid;->h:Lfab;

    if-eqz v1, :cond_7

    .line 95
    const/16 v1, 0xf

    iget-object v2, p0, Leid;->h:Lfab;

    .line 96
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 98
    :cond_7
    iget-object v1, p0, Leid;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 99
    iput v0, p0, Leid;->cachedSize:I

    .line 100
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Leid;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Leid;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Leid;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Leid;->b:[Ljava/lang/Integer;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Integer;

    iget-object v2, p0, Leid;->b:[Ljava/lang/Integer;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Leid;->b:[Ljava/lang/Integer;

    :goto_1
    iget-object v1, p0, Leid;->b:[Ljava/lang/Integer;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Leid;->b:[Ljava/lang/Integer;

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Leid;->b:[Ljava/lang/Integer;

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x15

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Leid;->c:[Ljava/lang/Float;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Float;

    iget-object v2, p0, Leid;->c:[Ljava/lang/Float;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Leid;->c:[Ljava/lang/Float;

    :goto_2
    iget-object v1, p0, Leid;->c:[Ljava/lang/Float;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Leid;->c:[Ljava/lang/Float;

    invoke-virtual {p1}, Lepk;->c()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v1, p0, Leid;->c:[Ljava/lang/Float;

    invoke-virtual {p1}, Lepk;->c()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Leid;->e:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leid;->f:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leid;->g:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Leid;->d:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Leid;->h:Lfab;

    if-nez v0, :cond_4

    new-instance v0, Lfab;

    invoke-direct {v0}, Lfab;-><init>()V

    iput-object v0, p0, Leid;->h:Lfab;

    :cond_4
    iget-object v0, p0, Leid;->h:Lfab;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x15 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x7a -> :sswitch_7
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 32
    iget-object v1, p0, Leid;->b:[Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 33
    iget-object v2, p0, Leid;->b:[Ljava/lang/Integer;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 34
    const/4 v5, 0x1

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p1, v5, v4}, Lepl;->a(II)V

    .line 33
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 37
    :cond_0
    iget-object v1, p0, Leid;->c:[Ljava/lang/Float;

    if-eqz v1, :cond_1

    .line 38
    iget-object v1, p0, Leid;->c:[Ljava/lang/Float;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 39
    const/4 v4, 0x2

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    invoke-virtual {p1, v4, v3}, Lepl;->a(IF)V

    .line 38
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 42
    :cond_1
    iget-object v0, p0, Leid;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 43
    const/4 v0, 0x3

    iget-object v1, p0, Leid;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 45
    :cond_2
    iget-object v0, p0, Leid;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 46
    const/4 v0, 0x4

    iget-object v1, p0, Leid;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 48
    :cond_3
    iget-object v0, p0, Leid;->g:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 49
    const/4 v0, 0x5

    iget-object v1, p0, Leid;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 51
    :cond_4
    iget-object v0, p0, Leid;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 52
    const/4 v0, 0x6

    iget-object v1, p0, Leid;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 54
    :cond_5
    iget-object v0, p0, Leid;->h:Lfab;

    if-eqz v0, :cond_6

    .line 55
    const/16 v0, 0xf

    iget-object v1, p0, Leid;->h:Lfab;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 57
    :cond_6
    iget-object v0, p0, Leid;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 59
    return-void
.end method
