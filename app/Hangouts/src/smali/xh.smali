.class public final Lxh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field private static final f:Lxi;

.field private static final g:Lxi;

.field private static final h:Lxi;

.field private static i:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static j:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static k:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/reflect/Method;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final m:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/reflect/Method;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/reflect/Method;

.field private c:Ljava/lang/reflect/Method;

.field private d:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private e:Lxf;

.field private final n:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

.field private final o:[Ljava/lang/Object;

.field private p:Lxi;

.field private q:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 60
    new-instance v0, Lxd;

    invoke-direct {v0}, Lxd;-><init>()V

    sput-object v0, Lxh;->f:Lxi;

    .line 61
    new-instance v0, Lxc;

    invoke-direct {v0}, Lxc;-><init>()V

    sput-object v0, Lxh;->g:Lxi;

    .line 62
    new-instance v0, Lxb;

    invoke-direct {v0}, Lxb;-><init>()V

    sput-object v0, Lxh;->h:Lxi;

    .line 71
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Class;

    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    aput-object v1, v0, v3

    const-class v1, Ljava/lang/Float;

    aput-object v1, v0, v4

    sget-object v1, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    aput-object v1, v0, v5

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v1, v0, v6

    const-class v1, Ljava/lang/Double;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-class v2, Ljava/lang/Integer;

    aput-object v2, v0, v1

    sput-object v0, Lxh;->i:[Ljava/lang/Class;

    .line 73
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Class;

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v1, v0, v3

    const-class v1, Ljava/lang/Integer;

    aput-object v1, v0, v4

    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    aput-object v1, v0, v5

    sget-object v1, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    aput-object v1, v0, v6

    const-class v1, Ljava/lang/Float;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-class v2, Ljava/lang/Double;

    aput-object v2, v0, v1

    sput-object v0, Lxh;->j:[Ljava/lang/Class;

    .line 75
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Class;

    sget-object v1, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    aput-object v1, v0, v3

    const-class v1, Ljava/lang/Double;

    aput-object v1, v0, v4

    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    aput-object v1, v0, v5

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v1, v0, v6

    const-class v1, Ljava/lang/Float;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-class v2, Ljava/lang/Integer;

    aput-object v2, v0, v1

    sput-object v0, Lxh;->k:[Ljava/lang/Class;

    .line 81
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    sput-object v0, Lxh;->l:Ljava/util/Map;

    .line 83
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    sput-object v0, Lxh;->m:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object v0, p0, Lxh;->b:Ljava/lang/reflect/Method;

    .line 45
    iput-object v0, p0, Lxh;->c:Ljava/lang/reflect/Method;

    .line 56
    iput-object v0, p0, Lxh;->e:Lxf;

    .line 88
    new-instance v0, Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>()V

    iput-object v0, p0, Lxh;->n:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    .line 91
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lxh;->o:[Ljava/lang/Object;

    .line 114
    iput-object p1, p0, Lxh;->a:Ljava/lang/String;

    .line 115
    return-void
.end method

.method private a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/reflect/Method;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/reflect/Method;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 332
    iget-object v0, p0, Lxh;->a:Ljava/lang/String;

    invoke-virtual {v0, v3, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 333
    iget-object v2, p0, Lxh;->a:Ljava/lang/String;

    invoke-virtual {v2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 334
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 335
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 336
    if-nez p3, :cond_0

    .line 339
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p1, v4, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 376
    :goto_0
    return-object v0

    .line 340
    :catch_0
    move-exception v0

    .line 341
    const-string v2, "PropertyValuesHolder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Couldn\'t find no-arg method for property "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lxh;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 343
    goto :goto_0

    .line 345
    :cond_0
    new-array v5, v6, [Ljava/lang/Class;

    .line 347
    iget-object v0, p0, Lxh;->d:Ljava/lang/Class;

    const-class v2, Ljava/lang/Float;

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 348
    sget-object v0, Lxh;->i:[Ljava/lang/Class;

    .line 357
    :goto_1
    array-length v6, v0

    move v2, v3

    :goto_2
    if-ge v2, v6, :cond_4

    aget-object v7, v0, v2

    .line 358
    aput-object v7, v5, v3

    .line 360
    :try_start_1
    invoke-virtual {p1, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 362
    iput-object v7, p0, Lxh;->d:Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v1

    .line 363
    goto :goto_0

    .line 349
    :cond_1
    iget-object v0, p0, Lxh;->d:Ljava/lang/Class;

    const-class v2, Ljava/lang/Integer;

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 350
    sget-object v0, Lxh;->j:[Ljava/lang/Class;

    goto :goto_1

    .line 351
    :cond_2
    iget-object v0, p0, Lxh;->d:Ljava/lang/Class;

    const-class v2, Ljava/lang/Double;

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 352
    sget-object v0, Lxh;->k:[Ljava/lang/Class;

    goto :goto_1

    .line 354
    :cond_3
    new-array v0, v6, [Ljava/lang/Class;

    .line 355
    iget-object v2, p0, Lxh;->d:Ljava/lang/Class;

    aput-object v2, v0, v3

    goto :goto_1

    .line 357
    :catch_1
    move-exception v7

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 369
    :cond_4
    const-string v0, "PropertyValuesHolder"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Couldn\'t find setter/getter for property "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lxh;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with value type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lxh;->d:Ljava/lang/Class;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " methodName: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " targetClass: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    goto/16 :goto_0
.end method

.method private a(Ljava/lang/Class;Ljava/util/Map;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/reflect/Method;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/reflect/Method;",
            ">;>;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/reflect/Method;"
        }
    .end annotation

    .prologue
    .line 394
    const/4 v1, 0x0

    .line 400
    :try_start_0
    iget-object v0, p0, Lxh;->n:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->lock()V

    .line 401
    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 402
    if-eqz v0, :cond_0

    .line 403
    iget-object v1, p0, Lxh;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/reflect/Method;

    .line 405
    :cond_0
    if-nez v1, :cond_2

    .line 406
    invoke-direct {p0, p1, p3, p4}, Lxh;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 407
    if-nez v0, :cond_1

    .line 408
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    .line 409
    invoke-interface {p2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 411
    :cond_1
    iget-object v2, p0, Lxh;->a:Ljava/lang/String;

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    move-object v0, v1

    .line 414
    iget-object v1, p0, Lxh;->n:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V

    .line 416
    return-object v0

    .line 414
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lxh;->n:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V

    throw v0
.end method

.method public static varargs a(Ljava/lang/String;[F)Lxh;
    .locals 1

    .prologue
    .line 140
    new-instance v0, Lxh;

    invoke-direct {v0, p0}, Lxh;-><init>(Ljava/lang/String;)V

    .line 141
    invoke-virtual {v0, p1}, Lxh;->a([F)V

    .line 142
    return-object v0
.end method

.method public static varargs a(Ljava/lang/String;[I)Lxh;
    .locals 1

    .prologue
    .line 126
    new-instance v0, Lxh;

    invoke-direct {v0, p0}, Lxh;-><init>(Ljava/lang/String;)V

    .line 127
    invoke-virtual {v0, p1}, Lxh;->a([I)V

    .line 128
    return-object v0
.end method


# virtual methods
.method a(F)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 575
    iget-object v0, p0, Lxh;->e:Lxf;

    iget-object v1, p0, Lxh;->p:Lxi;

    invoke-virtual {v0, p1, v1}, Lxf;->a(FLxi;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lxh;->q:Ljava/lang/Object;

    .line 576
    iget-object v0, p0, Lxh;->q:Ljava/lang/Object;

    return-object v0
.end method

.method public a()Lxh;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 512
    iget-object v0, p0, Lxh;->e:Lxf;

    iget-object v3, v0, Lxf;->a:Ljava/util/ArrayList;

    .line 513
    iget-object v0, p0, Lxh;->e:Lxf;

    iget-object v0, v0, Lxf;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 514
    new-array v5, v4, [Lxe;

    move v2, v1

    .line 515
    :goto_0
    if-ge v2, v4, :cond_0

    .line 516
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxe;

    invoke-virtual {v0}, Lxe;->e()Lxe;

    move-result-object v0

    aput-object v0, v5, v2

    .line 515
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 518
    :cond_0
    iget-object v0, p0, Lxh;->a:Ljava/lang/String;

    new-instance v2, Lxh;

    invoke-direct {v2, v0}, Lxh;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x2

    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    new-array v3, v0, [Lxe;

    aget-object v0, v5, v1

    invoke-virtual {v0}, Lxe;->d()Ljava/lang/Class;

    move-result-object v0

    iput-object v0, v2, Lxh;->d:Ljava/lang/Class;

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_1

    aget-object v1, v5, v0

    aput-object v1, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    new-instance v0, Lxf;

    invoke-direct {v0, v3}, Lxf;-><init>([Lxe;)V

    iput-object v0, v2, Lxh;->e:Lxf;

    return-object v2
.end method

.method a(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 446
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 447
    iget-object v0, p0, Lxh;->b:Ljava/lang/reflect/Method;

    if-nez v0, :cond_0

    .line 448
    sget-object v0, Lxh;->l:Ljava/util/Map;

    const-string v2, "set"

    iget-object v3, p0, Lxh;->d:Ljava/lang/Class;

    invoke-direct {p0, v1, v0, v2, v3}, Lxh;->a(Ljava/lang/Class;Ljava/util/Map;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lxh;->b:Ljava/lang/reflect/Method;

    .line 450
    :cond_0
    iget-object v0, p0, Lxh;->e:Lxf;

    iget-object v0, v0, Lxf;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxe;

    .line 451
    invoke-virtual {v0}, Lxe;->a()Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_1

    .line 452
    iget-object v3, p0, Lxh;->c:Ljava/lang/reflect/Method;

    if-nez v3, :cond_2

    .line 453
    sget-object v3, Lxh;->m:Ljava/util/Map;

    const-string v4, "get"

    const/4 v5, 0x0

    invoke-direct {p0, v1, v3, v4, v5}, Lxh;->a(Ljava/lang/Class;Ljava/util/Map;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    iput-object v3, p0, Lxh;->c:Ljava/lang/reflect/Method;

    .line 456
    :cond_2
    :try_start_0
    iget-object v3, p0, Lxh;->c:Ljava/lang/reflect/Method;

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v3, p1, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Lxe;->a(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 457
    :catch_0
    move-exception v0

    .line 458
    const-string v3, "PropertyValuesHolder"

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 459
    :catch_1
    move-exception v0

    .line 460
    const-string v3, "PropertyValuesHolder"

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 464
    :cond_3
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 654
    iput-object p1, p0, Lxh;->a:Ljava/lang/String;

    .line 655
    return-void
.end method

.method public varargs a([F)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v6, 0x0

    .line 246
    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    iput-object v1, p0, Lxh;->d:Ljava/lang/Class;

    .line 247
    array-length v1, p1

    const/4 v2, 0x2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    new-array v2, v2, [Lxe;

    if-ne v1, v0, :cond_1

    new-instance v1, Lxe;

    invoke-direct {v1}, Lxe;-><init>()V

    aput-object v1, v2, v6

    new-instance v1, Lxe;

    const/high16 v3, 0x3f800000    # 1.0f

    aget v4, p1, v6

    invoke-direct {v1, v3, v4}, Lxe;-><init>(FF)V

    aput-object v1, v2, v0

    :cond_0
    new-instance v0, Lxf;

    invoke-direct {v0, v2}, Lxf;-><init>([Lxe;)V

    iput-object v0, p0, Lxh;->e:Lxf;

    .line 248
    return-void

    .line 247
    :cond_1
    new-instance v3, Lxe;

    const/4 v4, 0x0

    aget v5, p1, v6

    invoke-direct {v3, v4, v5}, Lxe;-><init>(FF)V

    aput-object v3, v2, v6

    :goto_0
    if-ge v0, v1, :cond_0

    new-instance v3, Lxe;

    int-to-float v4, v0

    add-int/lit8 v5, v1, -0x1

    int-to-float v5, v5

    div-float/2addr v4, v5

    aget v5, p1, v0

    invoke-direct {v3, v4, v5}, Lxe;-><init>(FF)V

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public varargs a([I)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v6, 0x0

    .line 229
    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    iput-object v1, p0, Lxh;->d:Ljava/lang/Class;

    .line 230
    array-length v1, p1

    const/4 v2, 0x2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    new-array v2, v2, [Lxe;

    if-ne v1, v0, :cond_1

    new-instance v1, Lxe;

    invoke-direct {v1}, Lxe;-><init>()V

    aput-object v1, v2, v6

    new-instance v1, Lxe;

    const/high16 v3, 0x3f800000    # 1.0f

    aget v4, p1, v6

    invoke-direct {v1, v3, v4}, Lxe;-><init>(FI)V

    aput-object v1, v2, v0

    :cond_0
    new-instance v0, Lxf;

    invoke-direct {v0, v2}, Lxf;-><init>([Lxe;)V

    iput-object v0, p0, Lxh;->e:Lxf;

    .line 231
    return-void

    .line 230
    :cond_1
    new-instance v3, Lxe;

    const/4 v4, 0x0

    aget v5, p1, v6

    invoke-direct {v3, v4, v5}, Lxe;-><init>(FI)V

    aput-object v3, v2, v6

    :goto_0
    if-ge v0, v1, :cond_0

    new-instance v3, Lxe;

    int-to-float v4, v0

    add-int/lit8 v5, v1, -0x1

    int-to-float v5, v5

    div-float/2addr v4, v5

    aget v5, p1, v0

    invoke-direct {v3, v4, v5}, Lxe;-><init>(FI)V

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method b()V
    .locals 2

    .prologue
    .line 547
    iget-object v0, p0, Lxh;->p:Lxi;

    if-nez v0, :cond_1

    .line 548
    iget-object v0, p0, Lxh;->d:Ljava/lang/Class;

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lxh;->d:Ljava/lang/Class;

    const-class v1, Ljava/lang/Integer;

    if-ne v0, v1, :cond_2

    :cond_0
    sget-object v0, Lxh;->f:Lxi;

    :goto_0
    iput-object v0, p0, Lxh;->p:Lxi;

    .line 552
    :cond_1
    return-void

    .line 548
    :cond_2
    iget-object v0, p0, Lxh;->d:Ljava/lang/Class;

    sget-object v1, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lxh;->d:Ljava/lang/Class;

    const-class v1, Ljava/lang/Double;

    if-ne v0, v1, :cond_4

    :cond_3
    sget-object v0, Lxh;->h:Lxi;

    goto :goto_0

    :cond_4
    sget-object v0, Lxh;->g:Lxi;

    goto :goto_0
.end method

.method b(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 530
    iget-object v0, p0, Lxh;->b:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    .line 532
    :try_start_0
    iget-object v0, p0, Lxh;->o:[Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lxh;->q:Ljava/lang/Object;

    aput-object v2, v0, v1

    .line 533
    iget-object v0, p0, Lxh;->b:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lxh;->o:[Ljava/lang/Object;

    invoke-virtual {v0, p1, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 540
    :cond_0
    :goto_0
    return-void

    .line 534
    :catch_0
    move-exception v0

    .line 535
    const-string v1, "PropertyValuesHolder"

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 536
    :catch_1
    move-exception v0

    .line 537
    const-string v1, "PropertyValuesHolder"

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 665
    iget-object v0, p0, Lxh;->a:Ljava/lang/String;

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Lxh;->a()Lxh;

    move-result-object v0

    return-object v0
.end method
