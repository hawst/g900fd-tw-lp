.class public Lczt;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lczu;

.field private final b:Lczv;


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 386
    new-instance v1, Ldbn;

    new-instance v2, Lczx;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v3, "\\$[0-9]+"

    const-string v4, "\\$"

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/16 v0, 0x24

    invoke-virtual {v3, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    const/4 v4, -0x1

    if-ne v0, v4, :cond_0

    const/16 v0, 0x2e

    invoke-virtual {v3, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v3, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Lczx;-><init>(Ljava/lang/String;B)V

    invoke-direct {v1, v2}, Ldbn;-><init>(Lczx;)V

    const-string v0, "header"

    iget-object v2, p0, Lczt;->a:Lczu;

    invoke-virtual {v1, v0, v2}, Ldbn;->a(Ljava/lang/String;Ljava/lang/Object;)Ldbn;

    move-result-object v0

    const-string v1, "payload"

    iget-object v2, p0, Lczt;->b:Lczv;

    invoke-virtual {v0, v1, v2}, Ldbn;->a(Ljava/lang/String;Ljava/lang/Object;)Ldbn;

    move-result-object v0

    invoke-virtual {v0}, Ldbn;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
