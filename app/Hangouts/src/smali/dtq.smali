.class public final Ldtq;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldtq;


# instance fields
.field public b:Leqs;

.field public c:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4370
    const/4 v0, 0x0

    new-array v0, v0, [Ldtq;

    sput-object v0, Ldtq;->a:[Ldtq;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4371
    invoke-direct {p0}, Lepn;-><init>()V

    .line 4374
    const/4 v0, 0x0

    iput-object v0, p0, Ldtq;->b:Leqs;

    .line 4371
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 4393
    const/4 v0, 0x0

    .line 4394
    iget-object v1, p0, Ldtq;->b:Leqs;

    if-eqz v1, :cond_0

    .line 4395
    const/4 v0, 0x1

    iget-object v1, p0, Ldtq;->b:Leqs;

    .line 4396
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4398
    :cond_0
    iget-object v1, p0, Ldtq;->c:[B

    if-eqz v1, :cond_1

    .line 4399
    const/4 v1, 0x2

    iget-object v2, p0, Ldtq;->c:[B

    .line 4400
    invoke-static {v1, v2}, Lepl;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 4402
    :cond_1
    iget-object v1, p0, Ldtq;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4403
    iput v0, p0, Ldtq;->cachedSize:I

    .line 4404
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 4367
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldtq;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldtq;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldtq;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldtq;->b:Leqs;

    if-nez v0, :cond_2

    new-instance v0, Leqs;

    invoke-direct {v0}, Leqs;-><init>()V

    iput-object v0, p0, Ldtq;->b:Leqs;

    :cond_2
    iget-object v0, p0, Ldtq;->b:Leqs;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v0

    iput-object v0, p0, Ldtq;->c:[B

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 4381
    iget-object v0, p0, Ldtq;->b:Leqs;

    if-eqz v0, :cond_0

    .line 4382
    const/4 v0, 0x1

    iget-object v1, p0, Ldtq;->b:Leqs;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 4384
    :cond_0
    iget-object v0, p0, Ldtq;->c:[B

    if-eqz v0, :cond_1

    .line 4385
    const/4 v0, 0x2

    iget-object v1, p0, Ldtq;->c:[B

    invoke-virtual {p1, v0, v1}, Lepl;->a(I[B)V

    .line 4387
    :cond_1
    iget-object v0, p0, Ldtq;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 4389
    return-void
.end method
