.class public final Ldsx;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldsx;


# instance fields
.field public b:Lehf;

.field public c:Ldsy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8474
    const/4 v0, 0x0

    new-array v0, v0, [Ldsx;

    sput-object v0, Ldsx;->a:[Ldsx;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8475
    invoke-direct {p0}, Lepn;-><init>()V

    .line 8478
    iput-object v0, p0, Ldsx;->b:Lehf;

    .line 8481
    iput-object v0, p0, Ldsx;->c:Ldsy;

    .line 8475
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 8498
    const/4 v0, 0x0

    .line 8499
    iget-object v1, p0, Ldsx;->b:Lehf;

    if-eqz v1, :cond_0

    .line 8500
    const/4 v0, 0x1

    iget-object v1, p0, Ldsx;->b:Lehf;

    .line 8501
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 8503
    :cond_0
    iget-object v1, p0, Ldsx;->c:Ldsy;

    if-eqz v1, :cond_1

    .line 8504
    const/4 v1, 0x2

    iget-object v2, p0, Ldsx;->c:Ldsy;

    .line 8505
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8507
    :cond_1
    iget-object v1, p0, Ldsx;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8508
    iput v0, p0, Ldsx;->cachedSize:I

    .line 8509
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 8471
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldsx;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldsx;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldsx;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldsx;->b:Lehf;

    if-nez v0, :cond_2

    new-instance v0, Lehf;

    invoke-direct {v0}, Lehf;-><init>()V

    iput-object v0, p0, Ldsx;->b:Lehf;

    :cond_2
    iget-object v0, p0, Ldsx;->b:Lehf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldsx;->c:Ldsy;

    if-nez v0, :cond_3

    new-instance v0, Ldsy;

    invoke-direct {v0}, Ldsy;-><init>()V

    iput-object v0, p0, Ldsx;->c:Ldsy;

    :cond_3
    iget-object v0, p0, Ldsx;->c:Ldsy;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 8486
    iget-object v0, p0, Ldsx;->b:Lehf;

    if-eqz v0, :cond_0

    .line 8487
    const/4 v0, 0x1

    iget-object v1, p0, Ldsx;->b:Lehf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 8489
    :cond_0
    iget-object v0, p0, Ldsx;->c:Ldsy;

    if-eqz v0, :cond_1

    .line 8490
    const/4 v0, 0x2

    iget-object v1, p0, Ldsx;->c:Ldsy;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 8492
    :cond_1
    iget-object v0, p0, Ldsx;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 8494
    return-void
.end method
