.class public abstract Lbly;
.super Lbmr;
.source "PG"


# static fields
.field static final a:Z


# instance fields
.field protected b:Ljava/lang/String;

.field protected c:Ljava/lang/CharSequence;

.field protected d:Landroid/content/Intent;

.field protected final e:J

.field protected f:I

.field protected g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lbys;->k:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lbly;->a:Z

    return-void
.end method

.method constructor <init>(Lyj;ILbxz;J)V
    .locals 2

    .prologue
    .line 60
    invoke-direct {p0, p1, p3}, Lbmr;-><init>(Lyj;Lbxz;)V

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lbly;->c:Ljava/lang/CharSequence;

    .line 62
    iput p2, p0, Lbly;->f:I

    .line 63
    const-wide/16 v0, 0x0

    cmp-long v0, p4, v0

    if-lez v0, :cond_0

    :goto_0
    iput-wide p4, p0, Lbly;->e:J

    .line 64
    return-void

    .line 63
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p4

    goto :goto_0
.end method

.method public static a(Lyj;)Ljava/util/ArrayList;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lyj;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lbmr;",
            ">;"
        }
    .end annotation

    .prologue
    .line 87
    invoke-virtual/range {p0 .. p0}, Lyj;->u()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 88
    const/4 v1, 0x0

    .line 214
    :goto_0
    return-object v1

    .line 90
    :cond_0
    const/4 v15, 0x0

    .line 91
    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual/range {p0 .. p0}, Lyj;->c()Lbdk;

    move-result-object v2

    iget-object v2, v2, Lbdk;->b:Ljava/lang/String;

    aput-object v2, v5, v1

    .line 92
    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->h:Landroid/net/Uri;

    move-object/from16 v0, p0

    invoke-static {v1, v0}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Landroid/net/Uri;Lyj;)Landroid/net/Uri;

    move-result-object v2

    .line 94
    invoke-static {}, Lzo;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v3, Lbmt;->a:[Ljava/lang/String;

    const-string v4, "conversation_id IN (SELECT conversation_id FROM conversations WHERE conversations.has_video_notifications=1) AND author_chat_id!=? AND conversation_notification_level!=10 AND conversation_pending_leave!=1 AND (type=7 OR type=8) AND conversation_status!=1 AND timestamp>hangout_watermark AND author_full_name IS NOT NULL AND timestamp > 1355097600000000"

    const-string v6, "merge_key, timestamp DESC"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 99
    if-eqz v16, :cond_1

    :try_start_0
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_4

    .line 100
    :cond_1
    sget-boolean v1, Lbly;->a:Z

    if-eqz v1, :cond_2

    .line 101
    const-string v1, "Babel"

    const-string v2, "no unseen hangout notifications"

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 103
    :cond_2
    if-eqz v16, :cond_3

    .line 199
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 200
    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 105
    :cond_4
    :try_start_1
    sget-boolean v1, Lbly;->a:Z

    if-eqz v1, :cond_5

    .line 106
    const-string v1, "Babel"

    const-string v2, "Some unseen hangout notifications"

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    :cond_5
    const/4 v2, 0x0

    .line 110
    const/4 v14, 0x0

    .line 111
    const/4 v4, 0x1

    .line 114
    const/4 v1, 0x0

    .line 115
    invoke-static {}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->a()Lcom/google/android/apps/hangouts/hangout/IncomingRing;

    move-result-object v3

    if-eqz v3, :cond_9

    .line 116
    invoke-static {}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->a()Lcom/google/android/apps/hangouts/hangout/IncomingRing;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->f()Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getConversationId()Ljava/lang/String;

    move-result-object v1

    :goto_1
    move-object v10, v1

    .line 126
    :goto_2
    const-wide/16 v12, 0x0

    .line 127
    const/4 v11, 0x0

    move v1, v4

    .line 129
    :goto_3
    const/16 v3, 0x8

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 130
    const/16 v3, 0x1c

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 131
    sget-boolean v3, Lbly;->a:Z

    if-eqz v3, :cond_6

    .line 132
    const-string v3, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "[HangoutNotifications] conversationId: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " newConvId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    :cond_6
    invoke-static {v4, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_17

    .line 136
    const/16 v1, 0xb

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 137
    const/16 v1, 0xe

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    const-wide/16 v6, 0x3e8

    div-long v7, v1, v6

    .line 138
    invoke-static {v4, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 139
    sget-boolean v1, Lbly;->a:Z

    if-eqz v1, :cond_16

    .line 140
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[HangoutNotifications]  newConvId: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ignored because of active ring or joined hangout."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v11

    move-wide v7, v12

    move-object v2, v14

    move-object v3, v15

    :goto_4
    move-object/from16 v17, v1

    move-object v1, v4

    move v4, v5

    move-object v5, v2

    move-object v2, v3

    move-object/from16 v3, v17

    move-wide/from16 v18, v7

    move-wide/from16 v6, v18

    .line 188
    :goto_5
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z

    move-result v8

    if-nez v8, :cond_18

    .line 190
    if-eqz v3, :cond_7

    invoke-virtual {v3}, Lbxz;->size()I

    move-result v1

    if-lez v1, :cond_7

    .line 191
    if-nez v2, :cond_12

    .line 192
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 194
    :goto_6
    new-instance v1, Lblz;

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v7}, Lblz;-><init>(Lyj;Lbxz;ILjava/util/ArrayList;J)V

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v2, v8

    .line 198
    :cond_7
    if-eqz v16, :cond_8

    .line 199
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 204
    :cond_8
    sget-boolean v1, Lbly;->a:Z

    if-eqz v1, :cond_11

    .line 205
    if-eqz v2, :cond_11

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_11

    .line 206
    const-string v1, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " hangout notification created."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_11

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbmr;

    .line 208
    check-cast v1, Lbly;

    .line 209
    const-string v4, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "title: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v1, Lbly;->b:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " content: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v1, v1, Lbly;->c:Ljava/lang/CharSequence;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7

    .line 118
    :cond_9
    :try_start_2
    invoke-static {}, Lape;->c()Ljava/lang/Boolean;

    move-result-object v3

    .line 119
    const/4 v5, 0x0

    invoke-static {v3, v5}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v3

    if-eqz v3, :cond_19

    .line 120
    invoke-static {}, Lapk;->b()Lapk;

    move-result-object v1

    invoke-virtual {v1}, Lapk;->c()Lapx;

    move-result-object v1

    .line 121
    if-nez v1, :cond_a

    const/4 v1, 0x0

    goto/16 :goto_1

    .line 122
    :cond_a
    invoke-virtual {v1}, Lapx;->c()Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getConversationId()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    .line 143
    :cond_b
    const/4 v1, 0x5

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v2, 0x7

    if-ne v1, v2, :cond_e

    .line 145
    invoke-virtual/range {p0 .. p0}, Lyj;->s()Z

    move-result v1

    if-nez v1, :cond_e

    .line 149
    invoke-static/range {v16 .. v16}, Lbly;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    .line 150
    const/4 v1, 0x0

    .line 151
    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 152
    if-nez v6, :cond_c

    .line 153
    const-string v6, "..."

    .line 154
    const-string v1, "Babel"

    const-string v2, "Should at least have a fallback name for the inviter"

    invoke-static {v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    :cond_c
    if-nez v15, :cond_d

    .line 157
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 159
    :cond_d
    new-instance v1, Lbma;

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v9}, Lbma;-><init>(Lyj;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JI)V

    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v1, v11

    move-wide v7, v12

    move-object v2, v14

    move-object v3, v15

    .line 162
    goto/16 :goto_4

    .line 163
    :cond_e
    const/16 v1, 0x16

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 165
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-static {v0, v1, v2}, Lf;->a(Lyj;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v2

    .line 167
    const/4 v1, 0x0

    .line 168
    if-eqz v2, :cond_f

    .line 170
    invoke-virtual/range {p0 .. p0}, Lyj;->c()Lbdk;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    .line 172
    :cond_f
    if-nez v1, :cond_16

    .line 173
    if-nez v14, :cond_15

    .line 174
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 176
    :goto_8
    invoke-static/range {v16 .. v16}, Lbly;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 177
    if-nez v11, :cond_14

    .line 178
    new-instance v1, Lbxz;

    invoke-direct {v1}, Lbxz;-><init>()V

    .line 180
    :goto_9
    invoke-virtual {v1, v4}, Lbxz;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 181
    cmp-long v3, v7, v12

    if-lez v3, :cond_13

    move-object v3, v15

    .line 182
    goto/16 :goto_4

    .line 198
    :catchall_0
    move-exception v1

    if-eqz v16, :cond_10

    .line 199
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 200
    :cond_10
    throw v1

    :cond_11
    move-object v1, v2

    .line 214
    goto/16 :goto_0

    :cond_12
    move-object v8, v2

    goto/16 :goto_6

    :cond_13
    move-wide v7, v12

    move-object v3, v15

    goto/16 :goto_4

    :cond_14
    move-object v1, v11

    goto :goto_9

    :cond_15
    move-object v2, v14

    goto :goto_8

    :cond_16
    move-object v1, v11

    move-wide v7, v12

    move-object v2, v14

    move-object v3, v15

    goto/16 :goto_4

    :cond_17
    move-object v3, v11

    move-wide v6, v12

    move v4, v1

    move-object v5, v14

    move-object v1, v2

    move-object v2, v15

    goto/16 :goto_5

    :cond_18
    move-object v11, v3

    move-wide v12, v6

    move-object v14, v5

    move-object v15, v2

    move-object v2, v1

    move v1, v4

    goto/16 :goto_3

    :cond_19
    move-object v10, v1

    goto/16 :goto_2
.end method


# virtual methods
.method public a()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 68
    iget-object v0, p0, Lbly;->m:Lyj;

    const/4 v1, 0x2

    iget-object v2, p0, Lbly;->n:Lbxz;

    invoke-static {v0, v1, v2}, Lbbl;->a(Lyj;ILbxz;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public b()I
    .locals 2

    .prologue
    .line 74
    const-string v0, "babel_notify_video_priority_level"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 221
    const/4 v0, 0x0

    return v0
.end method
