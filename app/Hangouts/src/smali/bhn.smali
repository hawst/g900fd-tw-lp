.class public Lbhn;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private g:I

.field private h:Ljava/lang/String;

.field private final i:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ldve;)V
    .locals 3

    .prologue
    .line 1598
    iget-object v0, p1, Ldve;->b:Ldvn;

    const-wide/16 v1, -0x1

    invoke-direct {p0, v0, v1, v2}, Lbfz;-><init>(Ldvn;J)V

    .line 1599
    iget-object v0, p1, Ldve;->c:Ljava/lang/Integer;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    iput v0, p0, Lbhn;->g:I

    .line 1600
    iget-object v0, p1, Ldve;->d:Ljava/lang/String;

    iput-object v0, p0, Lbhn;->h:Ljava/lang/String;

    .line 1601
    iget-object v0, p1, Ldve;->e:Ljava/lang/String;

    iput-object v0, p0, Lbhn;->i:Ljava/lang/String;

    .line 1602
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_0

    .line 1603
    const-string v0, "Babel_protos"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RegisterDeviceResponse from:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1605
    :cond_0
    return-void
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 1609
    new-instance v0, Ldve;

    invoke-direct {v0}, Ldve;-><init>()V

    .line 1610
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldve;

    .line 1611
    iget-object v1, v0, Ldve;->b:Ldvn;

    invoke-static {v1}, Lbhn;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1612
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldve;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 1614
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbhn;

    invoke-direct {v1, v0}, Lbhn;-><init>(Ldve;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Lbea;)V
    .locals 2

    .prologue
    .line 1620
    invoke-super {p0, p1}, Lbfz;->a(Lbea;)V

    .line 1622
    iget-object v0, p0, Lbhn;->b:Lbea;

    check-cast v0, Lbev;

    .line 1623
    iget-object v1, v0, Lbev;->t:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1624
    const/4 v1, 0x2

    iput v1, p0, Lbhn;->g:I

    .line 1625
    iget-object v0, v0, Lbev;->t:Ljava/lang/String;

    iput-object v0, p0, Lbhn;->h:Ljava/lang/String;

    .line 1627
    :cond_0
    return-void
.end method

.method public f()I
    .locals 1

    .prologue
    .line 1630
    iget-object v0, p0, Lbhn;->b:Lbea;

    check-cast v0, Lbev;

    iget v0, v0, Lbev;->a:I

    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1634
    iget-object v0, p0, Lbhn;->b:Lbea;

    check-cast v0, Lbev;

    iget-object v0, v0, Lbev;->l:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1638
    iget-object v0, p0, Lbhn;->i:Ljava/lang/String;

    return-object v0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 1642
    iget v0, p0, Lbhn;->g:I

    return v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1646
    iget-object v0, p0, Lbhn;->h:Ljava/lang/String;

    return-object v0
.end method
