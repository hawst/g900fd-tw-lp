.class public final Lbak;
.super Landroid/database/AbstractCursor;
.source "PG"


# instance fields
.field private final a:[Ljava/lang/String;

.field private b:[Ljava/lang/Object;

.field private c:I

.field private final d:I

.field private final e:Landroid/os/Bundle;


# direct methods
.method public constructor <init>([Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 76
    const/16 v0, 0x10

    invoke-direct {p0, p1, v0}, Lbak;-><init>([Ljava/lang/String;I)V

    .line 77
    return-void
.end method

.method public constructor <init>([Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Landroid/database/AbstractCursor;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput v0, p0, Lbak;->c:I

    .line 39
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lbak;->e:Landroid/os/Bundle;

    .line 59
    iput-object p1, p0, Lbak;->a:[Ljava/lang/String;

    .line 60
    array-length v0, p1

    iput v0, p0, Lbak;->d:I

    .line 62
    if-gtz p2, :cond_0

    .line 63
    const/4 p2, 0x1

    .line 66
    :cond_0
    iget v0, p0, Lbak;->d:I

    mul-int/2addr v0, p2

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lbak;->b:[Ljava/lang/Object;

    .line 67
    return-void
.end method

.method private a(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 83
    if-ltz p1, :cond_0

    iget v0, p0, Lbak;->d:I

    if-lt p1, v0, :cond_1

    .line 84
    :cond_0
    new-instance v0, Landroid/database/CursorIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Requested column: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", # of columns: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lbak;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/database/CursorIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 87
    :cond_1
    iget v0, p0, Lbak;->mPos:I

    if-gez v0, :cond_2

    .line 88
    new-instance v0, Landroid/database/CursorIndexOutOfBoundsException;

    const-string v1, "Before first row."

    invoke-direct {v0, v1}, Landroid/database/CursorIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 90
    :cond_2
    iget v0, p0, Lbak;->mPos:I

    iget v1, p0, Lbak;->c:I

    if-lt v0, v1, :cond_3

    .line 91
    new-instance v0, Landroid/database/CursorIndexOutOfBoundsException;

    const-string v1, "After last row."

    invoke-direct {v0, v1}, Landroid/database/CursorIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 93
    :cond_3
    iget-object v0, p0, Lbak;->b:[Ljava/lang/Object;

    iget v1, p0, Lbak;->mPos:I

    iget v2, p0, Lbak;->d:I

    mul-int/2addr v1, v2

    add-int/2addr v1, p1

    aget-object v0, v0, v1

    return-object v0
.end method

.method private b(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 183
    iget-object v0, p0, Lbak;->b:[Ljava/lang/Object;

    array-length v0, v0

    if-le p1, v0, :cond_0

    .line 184
    iget-object v1, p0, Lbak;->b:[Ljava/lang/Object;

    .line 185
    iget-object v0, p0, Lbak;->b:[Ljava/lang/Object;

    array-length v0, v0

    shl-int/lit8 v0, v0, 0x1

    .line 186
    if-ge v0, p1, :cond_1

    .line 189
    :goto_0
    new-array v0, p1, [Ljava/lang/Object;

    iput-object v0, p0, Lbak;->b:[Ljava/lang/Object;

    .line 190
    iget-object v0, p0, Lbak;->b:[Ljava/lang/Object;

    array-length v2, v1

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 192
    :cond_0
    return-void

    :cond_1
    move p1, v0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/Iterable;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 136
    iget v0, p0, Lbak;->c:I

    iget v1, p0, Lbak;->d:I

    mul-int/2addr v1, v0

    .line 137
    iget v0, p0, Lbak;->d:I

    add-int/2addr v0, v1

    .line 138
    invoke-direct {p0, v0}, Lbak;->b(I)V

    .line 140
    check-cast p1, Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    iget v0, p0, Lbak;->d:I

    if-eq v2, v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "columnNames.length = "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lbak;->d:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", columnValues.size() = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget v0, p0, Lbak;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbak;->c:I

    iget-object v3, p0, Lbak;->b:[Ljava/lang/Object;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    add-int v4, v1, v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v3, v4

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 142
    :cond_1
    return-void
.end method

.method public a([Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 117
    array-length v0, p1

    iget v1, p0, Lbak;->d:I

    if-eq v0, v1, :cond_0

    .line 118
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "columnNames.length = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lbak;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", columnValues.length = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 123
    :cond_0
    iget v0, p0, Lbak;->c:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lbak;->c:I

    iget v1, p0, Lbak;->d:I

    mul-int/2addr v0, v1

    .line 124
    iget v1, p0, Lbak;->d:I

    add-int/2addr v1, v0

    invoke-direct {p0, v1}, Lbak;->b(I)V

    .line 125
    const/4 v1, 0x0

    iget-object v2, p0, Lbak;->b:[Ljava/lang/Object;

    iget v3, p0, Lbak;->d:I

    invoke-static {p1, v1, v2, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 126
    return-void
.end method

.method public getBlob(I)[B
    .locals 1

    .prologue
    .line 308
    invoke-direct {p0, p1}, Lbak;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 309
    check-cast v0, [B

    return-object v0
.end method

.method public getColumnNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lbak;->a:[Ljava/lang/String;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 229
    iget v0, p0, Lbak;->c:I

    return v0
.end method

.method public getDouble(I)D
    .locals 2

    .prologue
    .line 296
    invoke-direct {p0, p1}, Lbak;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 297
    if-nez v0, :cond_0

    .line 298
    const-wide/16 v0, 0x0

    .line 303
    :goto_0
    return-wide v0

    .line 300
    :cond_0
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_1

    .line 301
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    goto :goto_0

    .line 303
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    goto :goto_0
.end method

.method public getExtras()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lbak;->e:Landroid/os/Bundle;

    return-object v0
.end method

.method public getFloat(I)F
    .locals 2

    .prologue
    .line 284
    invoke-direct {p0, p1}, Lbak;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 285
    if-nez v0, :cond_0

    .line 286
    const/4 v0, 0x0

    .line 291
    :goto_0
    return v0

    .line 288
    :cond_0
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_1

    .line 289
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    goto :goto_0

    .line 291
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    goto :goto_0
.end method

.method public getInt(I)I
    .locals 2

    .prologue
    .line 260
    invoke-direct {p0, p1}, Lbak;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 261
    if-nez v0, :cond_0

    .line 262
    const/4 v0, 0x0

    .line 267
    :goto_0
    return v0

    .line 264
    :cond_0
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_1

    .line 265
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    goto :goto_0

    .line 267
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public getLong(I)J
    .locals 2

    .prologue
    .line 272
    invoke-direct {p0, p1}, Lbak;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 273
    if-nez v0, :cond_0

    .line 274
    const-wide/16 v0, 0x0

    .line 279
    :goto_0
    return-wide v0

    .line 276
    :cond_0
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_1

    .line 277
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    goto :goto_0

    .line 279
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public getShort(I)S
    .locals 2

    .prologue
    .line 248
    invoke-direct {p0, p1}, Lbak;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 249
    if-nez v0, :cond_0

    .line 250
    const/4 v0, 0x0

    .line 255
    :goto_0
    return v0

    .line 252
    :cond_0
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_1

    .line 253
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->shortValue()S

    move-result v0

    goto :goto_0

    .line 255
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Short;->parseShort(Ljava/lang/String;)S

    move-result v0

    goto :goto_0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 239
    invoke-direct {p0, p1}, Lbak;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 240
    if-nez v0, :cond_0

    .line 241
    const/4 v0, 0x0

    .line 243
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getType(I)I
    .locals 2

    .prologue
    .line 314
    invoke-direct {p0, p1}, Lbak;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 315
    if-nez v0, :cond_0

    .line 316
    const/4 v0, 0x0

    .line 324
    :goto_0
    return v0

    .line 317
    :cond_0
    instance-of v1, v0, [B

    if-eqz v1, :cond_1

    .line 318
    const/4 v0, 0x4

    goto :goto_0

    .line 319
    :cond_1
    instance-of v1, v0, Ljava/lang/Float;

    if-nez v1, :cond_2

    instance-of v1, v0, Ljava/lang/Double;

    if-eqz v1, :cond_3

    .line 320
    :cond_2
    const/4 v0, 0x2

    goto :goto_0

    .line 321
    :cond_3
    instance-of v1, v0, Ljava/lang/Long;

    if-nez v1, :cond_4

    instance-of v0, v0, Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 322
    :cond_4
    const/4 v0, 0x1

    goto :goto_0

    .line 324
    :cond_5
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public isNull(I)Z
    .locals 1

    .prologue
    .line 330
    invoke-direct {p0, p1}, Lbak;->a(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
