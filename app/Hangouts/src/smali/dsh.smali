.class public final Ldsh;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldsh;


# instance fields
.field public b:Ldvm;

.field public c:Leir;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/Boolean;

.field public f:[Leir;

.field public g:Ldul;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22727
    const/4 v0, 0x0

    new-array v0, v0, [Ldsh;

    sput-object v0, Ldsh;->a:[Ldsh;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 22728
    invoke-direct {p0}, Lepn;-><init>()V

    .line 22731
    iput-object v1, p0, Ldsh;->b:Ldvm;

    .line 22734
    iput-object v1, p0, Ldsh;->c:Leir;

    .line 22741
    sget-object v0, Leir;->a:[Leir;

    iput-object v0, p0, Ldsh;->f:[Leir;

    .line 22744
    iput-object v1, p0, Ldsh;->g:Ldul;

    .line 22728
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 22778
    iget-object v0, p0, Ldsh;->b:Ldvm;

    if-eqz v0, :cond_6

    .line 22779
    const/4 v0, 0x1

    iget-object v2, p0, Ldsh;->b:Ldvm;

    .line 22780
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 22782
    :goto_0
    iget-object v2, p0, Ldsh;->c:Leir;

    if-eqz v2, :cond_0

    .line 22783
    const/4 v2, 0x2

    iget-object v3, p0, Ldsh;->c:Leir;

    .line 22784
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 22786
    :cond_0
    iget-object v2, p0, Ldsh;->d:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 22787
    const/4 v2, 0x3

    iget-object v3, p0, Ldsh;->d:Ljava/lang/String;

    .line 22788
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 22790
    :cond_1
    iget-object v2, p0, Ldsh;->e:Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    .line 22791
    const/4 v2, 0x4

    iget-object v3, p0, Ldsh;->e:Ljava/lang/Boolean;

    .line 22792
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 22794
    :cond_2
    iget-object v2, p0, Ldsh;->f:[Leir;

    if-eqz v2, :cond_4

    .line 22795
    iget-object v2, p0, Ldsh;->f:[Leir;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 22796
    if-eqz v4, :cond_3

    .line 22797
    const/4 v5, 0x5

    .line 22798
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 22795
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 22802
    :cond_4
    iget-object v1, p0, Ldsh;->g:Ldul;

    if-eqz v1, :cond_5

    .line 22803
    const/4 v1, 0x6

    iget-object v2, p0, Ldsh;->g:Ldul;

    .line 22804
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22806
    :cond_5
    iget-object v1, p0, Ldsh;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22807
    iput v0, p0, Ldsh;->cachedSize:I

    .line 22808
    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 22724
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldsh;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldsh;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldsh;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldsh;->b:Ldvm;

    if-nez v0, :cond_2

    new-instance v0, Ldvm;

    invoke-direct {v0}, Ldvm;-><init>()V

    iput-object v0, p0, Ldsh;->b:Ldvm;

    :cond_2
    iget-object v0, p0, Ldsh;->b:Ldvm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldsh;->c:Leir;

    if-nez v0, :cond_3

    new-instance v0, Leir;

    invoke-direct {v0}, Leir;-><init>()V

    iput-object v0, p0, Ldsh;->c:Leir;

    :cond_3
    iget-object v0, p0, Ldsh;->c:Leir;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldsh;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldsh;->e:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldsh;->f:[Leir;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Leir;

    iget-object v3, p0, Ldsh;->f:[Leir;

    if-eqz v3, :cond_4

    iget-object v3, p0, Ldsh;->f:[Leir;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iput-object v2, p0, Ldsh;->f:[Leir;

    :goto_2
    iget-object v2, p0, Ldsh;->f:[Leir;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Ldsh;->f:[Leir;

    new-instance v3, Leir;

    invoke-direct {v3}, Leir;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldsh;->f:[Leir;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Ldsh;->f:[Leir;

    array-length v0, v0

    goto :goto_1

    :cond_6
    iget-object v2, p0, Ldsh;->f:[Leir;

    new-instance v3, Leir;

    invoke-direct {v3}, Leir;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldsh;->f:[Leir;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Ldsh;->g:Ldul;

    if-nez v0, :cond_7

    new-instance v0, Ldul;

    invoke-direct {v0}, Ldul;-><init>()V

    iput-object v0, p0, Ldsh;->g:Ldul;

    :cond_7
    iget-object v0, p0, Ldsh;->g:Ldul;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 22749
    iget-object v0, p0, Ldsh;->b:Ldvm;

    if-eqz v0, :cond_0

    .line 22750
    const/4 v0, 0x1

    iget-object v1, p0, Ldsh;->b:Ldvm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 22752
    :cond_0
    iget-object v0, p0, Ldsh;->c:Leir;

    if-eqz v0, :cond_1

    .line 22753
    const/4 v0, 0x2

    iget-object v1, p0, Ldsh;->c:Leir;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 22755
    :cond_1
    iget-object v0, p0, Ldsh;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 22756
    const/4 v0, 0x3

    iget-object v1, p0, Ldsh;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 22758
    :cond_2
    iget-object v0, p0, Ldsh;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 22759
    const/4 v0, 0x4

    iget-object v1, p0, Ldsh;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 22761
    :cond_3
    iget-object v0, p0, Ldsh;->f:[Leir;

    if-eqz v0, :cond_5

    .line 22762
    iget-object v1, p0, Ldsh;->f:[Leir;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 22763
    if-eqz v3, :cond_4

    .line 22764
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 22762
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 22768
    :cond_5
    iget-object v0, p0, Ldsh;->g:Ldul;

    if-eqz v0, :cond_6

    .line 22769
    const/4 v0, 0x6

    iget-object v1, p0, Ldsh;->g:Ldul;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 22771
    :cond_6
    iget-object v0, p0, Ldsh;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 22773
    return-void
.end method
