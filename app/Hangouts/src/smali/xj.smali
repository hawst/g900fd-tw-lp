.class public Lxj;
.super Lwz;
.source "PG"


# static fields
.field private static g:Lxk;

.field private static final h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lxj;",
            ">;"
        }
    .end annotation
.end field

.field private static final i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lxj;",
            ">;"
        }
    .end annotation
.end field

.field private static final j:Lwt;

.field private static final p:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lxj;",
            ">;"
        }
    .end annotation
.end field

.field private static final q:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lxj;",
            ">;"
        }
    .end annotation
.end field

.field private static final r:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lxj;",
            ">;"
        }
    .end annotation
.end field

.field private static u:J


# instance fields
.field b:Z

.field c:[Lxh;

.field d:Les;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Les",
            "<",
            "Ljava/lang/String;",
            "Lxh;",
            ">;"
        }
    .end annotation
.end field

.field private e:J

.field private f:J

.field private k:Z

.field private l:I

.field private m:Z

.field private n:J

.field private o:I

.field private s:J

.field private t:J

.field private v:I

.field private w:I

.field private x:Lwt;

.field private y:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lxl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lxj;->h:Ljava/util/ArrayList;

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lxj;->i:Ljava/util/ArrayList;

    .line 81
    new-instance v0, Lwy;

    invoke-direct {v0}, Lwy;-><init>()V

    sput-object v0, Lxj;->j:Lwt;

    .line 119
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lxj;->p:Ljava/util/ArrayList;

    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lxj;->q:Ljava/util/ArrayList;

    .line 121
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lxj;->r:Ljava/util/ArrayList;

    .line 140
    const-wide/16 v0, 0xa

    sput-wide v0, Lxj;->u:J

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 200
    invoke-direct {p0}, Lwz;-><init>()V

    .line 68
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lxj;->f:J

    .line 88
    iput-boolean v2, p0, Lxj;->k:Z

    .line 94
    iput v2, p0, Lxj;->l:I

    .line 99
    iput-boolean v2, p0, Lxj;->m:Z

    .line 113
    iput v2, p0, Lxj;->o:I

    .line 127
    iput-boolean v2, p0, Lxj;->b:Z

    .line 134
    const-wide/16 v0, 0x12c

    iput-wide v0, p0, Lxj;->s:J

    .line 137
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lxj;->t:J

    .line 144
    iput v2, p0, Lxj;->v:I

    .line 151
    const/4 v0, 0x1

    iput v0, p0, Lxj;->w:I

    .line 158
    sget-object v0, Lxj;->j:Lwt;

    iput-object v0, p0, Lxj;->x:Lwt;

    .line 163
    const/4 v0, 0x0

    iput-object v0, p0, Lxj;->y:Ljava/util/ArrayList;

    .line 201
    return-void
.end method

.method static synthetic a(Lxj;)J
    .locals 2

    .prologue
    .line 25
    iget-wide v0, p0, Lxj;->t:J

    return-wide v0
.end method

.method static synthetic a(Lxj;J)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 25
    iget v1, p0, Lxj;->o:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    iget v1, p0, Lxj;->o:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-boolean v1, p0, Lxj;->m:Z

    if-nez v1, :cond_3

    iput-boolean v0, p0, Lxj;->m:Z

    iput-wide p1, p0, Lxj;->n:J

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    iget-wide v1, p0, Lxj;->n:J

    sub-long v1, p1, v1

    iget-wide v3, p0, Lxj;->t:J

    cmp-long v3, v1, v3

    if-lez v3, :cond_2

    iget-wide v3, p0, Lxj;->t:J

    sub-long/2addr v1, v3

    sub-long v1, p1, v1

    iput-wide v1, p0, Lxj;->e:J

    iput v0, p0, Lxj;->o:I

    goto :goto_0
.end method

.method static synthetic b(Lxj;)I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lxj;->o:I

    return v0
.end method

.method private b(J)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/high16 v7, 0x3f800000    # 1.0f

    .line 1053
    iget v0, p0, Lxj;->o:I

    if-nez v0, :cond_0

    .line 1054
    iput v2, p0, Lxj;->o:I

    .line 1055
    iget-wide v3, p0, Lxj;->f:J

    const-wide/16 v5, 0x0

    cmp-long v0, v3, v5

    if-gez v0, :cond_1

    .line 1056
    iput-wide p1, p0, Lxj;->e:J

    .line 1063
    :cond_0
    :goto_0
    iget v0, p0, Lxj;->o:I

    packed-switch v0, :pswitch_data_0

    .line 1108
    :goto_1
    return v1

    .line 1058
    :cond_1
    iget-wide v3, p0, Lxj;->f:J

    sub-long v3, p1, v3

    iput-wide v3, p0, Lxj;->e:J

    .line 1060
    const-wide/16 v3, -0x1

    iput-wide v3, p0, Lxj;->f:J

    goto :goto_0

    .line 1066
    :pswitch_0
    iget-wide v3, p0, Lxj;->e:J

    sub-long v3, p1, v3

    long-to-float v0, v3

    iget-wide v3, p0, Lxj;->s:J

    long-to-float v3, v3

    div-float v3, v0, v3

    .line 1067
    cmpl-float v0, v3, v7

    if-ltz v0, :cond_9

    .line 1068
    iget v0, p0, Lxj;->l:I

    iget v4, p0, Lxj;->v:I

    if-lt v0, v4, :cond_2

    iget v0, p0, Lxj;->v:I

    const/4 v4, -0x1

    if-ne v0, v4, :cond_7

    .line 1070
    :cond_2
    iget-object v0, p0, Lxj;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 1071
    iget-object v0, p0, Lxj;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxa;

    .line 1072
    invoke-virtual {v0}, Lxa;->d()V

    goto :goto_2

    .line 1075
    :cond_3
    iget v0, p0, Lxj;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lxj;->l:I

    .line 1076
    iget v0, p0, Lxj;->w:I

    const/4 v4, 0x2

    if-ne v0, v4, :cond_4

    .line 1077
    iget-boolean v0, p0, Lxj;->k:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lxj;->k:Z

    .line 1080
    :cond_4
    sub-float v0, v3, v7

    .line 1081
    iget-wide v2, p0, Lxj;->e:J

    iget-wide v4, p0, Lxj;->s:J

    add-long/2addr v2, v4

    iput-wide v2, p0, Lxj;->e:J

    .line 1087
    :goto_4
    iget-boolean v2, p0, Lxj;->k:Z

    if-eqz v2, :cond_5

    .line 1088
    sub-float v0, v7, v0

    .line 1090
    :cond_5
    invoke-virtual {p0, v0}, Lxj;->a(F)V

    goto :goto_1

    :cond_6
    move v0, v2

    .line 1077
    goto :goto_3

    .line 1084
    :cond_7
    invoke-static {v3, v7}, Ljava/lang/Math;->min(FF)F

    move-result v0

    move v1, v2

    goto :goto_4

    .line 1095
    :pswitch_1
    iget v0, p0, Lxj;->v:I

    if-lez v0, :cond_8

    iget v0, p0, Lxj;->v:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_8

    .line 1096
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lxj;->a(F)V

    .line 1104
    :goto_5
    :pswitch_2
    iput v1, p0, Lxj;->o:I

    move v1, v2

    goto :goto_1

    .line 1098
    :cond_8
    invoke-virtual {p0, v7}, Lxj;->a(F)V

    goto :goto_5

    :cond_9
    move v0, v3

    goto :goto_4

    .line 1063
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic b(Lxj;J)Z
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lxj;->b(J)Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lxj;)V
    .locals 4

    .prologue
    .line 25
    invoke-virtual {p0}, Lxj;->f()V

    sget-object v0, Lxj;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-wide v0, p0, Lxj;->t:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Lxj;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lxj;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxa;

    invoke-virtual {v0}, Lxa;->a()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic d(Lxj;)V
    .locals 2

    .prologue
    .line 25
    sget-object v0, Lxj;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    iput v0, p0, Lxj;->o:I

    iget-object v0, p0, Lxj;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lxj;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxa;

    invoke-virtual {v0}, Lxa;->b()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic j()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lxj;->h:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic k()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lxj;->q:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic l()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lxj;->i:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic m()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lxj;->r:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic n()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lxj;->p:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic o()J
    .locals 2

    .prologue
    .line 25
    sget-wide v0, Lxj;->u:J

    return-wide v0
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 913
    iput-boolean v1, p0, Lxj;->k:Z

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v2}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v2

    if-ne v3, v2, :cond_1

    :cond_0
    :goto_0
    iget-wide v2, p0, Lxj;->t:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    if-eqz v0, :cond_3

    iget-object v0, p0, Lxj;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lxj;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxa;

    invoke-virtual {v0}, Lxa;->a()V

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lxj;->i()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lxj;->d(J)V

    :cond_3
    iput v1, p0, Lxj;->l:I

    iput v1, p0, Lxj;->o:I

    iput-boolean v1, p0, Lxj;->m:Z

    sget-object v0, Lxj;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lxj;->g:Lxk;

    if-nez v0, :cond_4

    new-instance v0, Lxk;

    invoke-direct {v0, v1}, Lxk;-><init>(B)V

    sput-object v0, Lxj;->g:Lxk;

    :cond_4
    sget-object v0, Lxj;->g:Lxk;

    invoke-virtual {v0, v1}, Lxk;->sendEmptyMessage(I)Z

    .line 914
    return-void
.end method

.method a(F)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1124
    iget-object v1, p0, Lxj;->x:Lwt;

    invoke-interface {v1, p1}, Lwt;->getInterpolation(F)F

    move-result v2

    .line 1125
    iget-object v1, p0, Lxj;->c:[Lxh;

    array-length v3, v1

    move v1, v0

    .line 1126
    :goto_0
    if-ge v1, v3, :cond_0

    .line 1127
    iget-object v4, p0, Lxj;->c:[Lxh;

    aget-object v4, v4, v1

    invoke-virtual {v4, v2}, Lxh;->a(F)Ljava/lang/Object;

    .line 1126
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1129
    :cond_0
    iget-object v1, p0, Lxj;->y:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 1130
    iget-object v1, p0, Lxj;->y:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    .line 1131
    :goto_1
    if-ge v1, v2, :cond_1

    .line 1132
    iget-object v0, p0, Lxj;->y:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxl;

    invoke-virtual {v0}, Lxl;->a()V

    .line 1131
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1135
    :cond_1
    return-void
.end method

.method public a(J)V
    .locals 0

    .prologue
    .line 674
    iput-wide p1, p0, Lxj;->t:J

    .line 675
    return-void
.end method

.method public a(Lwt;)V
    .locals 0

    .prologue
    .line 834
    iput-object p1, p0, Lxj;->x:Lwt;

    .line 837
    return-void
.end method

.method public a(Lxl;)V
    .locals 1

    .prologue
    .line 791
    iget-object v0, p0, Lxj;->y:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 792
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lxj;->y:Ljava/util/ArrayList;

    .line 794
    :cond_0
    iget-object v0, p0, Lxj;->y:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 795
    return-void
.end method

.method public varargs a([F)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 345
    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_1

    .line 356
    :cond_0
    :goto_0
    return-void

    .line 348
    :cond_1
    iget-object v0, p0, Lxj;->c:[Lxh;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lxj;->c:[Lxh;

    array-length v0, v0

    if-nez v0, :cond_3

    .line 349
    :cond_2
    const/4 v0, 0x1

    new-array v0, v0, [Lxh;

    const-string v1, ""

    invoke-static {v1, p1}, Lxh;->a(Ljava/lang/String;[F)Lxh;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lxj;->a([Lxh;)V

    .line 355
    :goto_1
    iput-boolean v2, p0, Lxj;->b:Z

    goto :goto_0

    .line 351
    :cond_3
    iget-object v0, p0, Lxj;->c:[Lxh;

    aget-object v0, v0, v2

    .line 352
    invoke-virtual {v0, p1}, Lxh;->a([F)V

    goto :goto_1
.end method

.method public varargs a([I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 318
    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_1

    .line 329
    :cond_0
    :goto_0
    return-void

    .line 321
    :cond_1
    iget-object v0, p0, Lxj;->c:[Lxh;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lxj;->c:[Lxh;

    array-length v0, v0

    if-nez v0, :cond_3

    .line 322
    :cond_2
    const/4 v0, 0x1

    new-array v0, v0, [Lxh;

    const-string v1, ""

    invoke-static {v1, p1}, Lxh;->a(Ljava/lang/String;[I)Lxh;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lxj;->a([Lxh;)V

    .line 328
    :goto_1
    iput-boolean v2, p0, Lxj;->b:Z

    goto :goto_0

    .line 324
    :cond_3
    iget-object v0, p0, Lxj;->c:[Lxh;

    aget-object v0, v0, v2

    .line 325
    invoke-virtual {v0, p1}, Lxh;->a([I)V

    goto :goto_1
.end method

.method public varargs a([Lxh;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 452
    array-length v2, p1

    .line 453
    iput-object p1, p0, Lxj;->c:[Lxh;

    .line 454
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    iput-object v0, p0, Lxj;->d:Les;

    move v0, v1

    .line 455
    :goto_0
    if-ge v0, v2, :cond_0

    .line 456
    aget-object v3, p1, v0

    .line 457
    iget-object v4, p0, Lxj;->d:Les;

    invoke-virtual {v3}, Lxh;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Les;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 455
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 460
    :cond_0
    iput-boolean v1, p0, Lxj;->b:Z

    .line 461
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 918
    iget-object v0, p0, Lxj;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 920
    iget-object v0, p0, Lxj;->a:Ljava/util/ArrayList;

    .line 921
    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 922
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxa;

    .line 923
    invoke-virtual {v0}, Lxa;->c()V

    goto :goto_0

    .line 928
    :cond_0
    const/4 v0, 0x2

    iput v0, p0, Lxj;->o:I

    .line 929
    return-void
.end method

.method public b(Lxl;)V
    .locals 1

    .prologue
    .line 815
    iget-object v0, p0, Lxj;->y:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 822
    :cond_0
    :goto_0
    return-void

    .line 818
    :cond_1
    iget-object v0, p0, Lxj;->y:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 819
    iget-object v0, p0, Lxj;->y:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 820
    const/4 v0, 0x0

    iput-object v0, p0, Lxj;->y:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public c(J)Lxj;
    .locals 0

    .prologue
    .line 504
    iput-wide p1, p0, Lxj;->s:J

    .line 505
    return-object p0
.end method

.method public c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 933
    sget-object v0, Lxj;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lxj;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 935
    iput-boolean v1, p0, Lxj;->m:Z

    .line 936
    sget-object v0, Lxj;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 937
    sget-object v0, Lxj;->g:Lxk;

    if-nez v0, :cond_0

    .line 938
    new-instance v0, Lxk;

    invoke-direct {v0, v1}, Lxk;-><init>(B)V

    sput-object v0, Lxj;->g:Lxk;

    .line 940
    :cond_0
    sget-object v0, Lxj;->g:Lxk;

    invoke-virtual {v0, v1}, Lxk;->sendEmptyMessage(I)Z

    .line 944
    :cond_1
    const/4 v0, 0x3

    iput v0, p0, Lxj;->o:I

    .line 945
    return-void
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lxj;->h()Lxj;

    move-result-object v0

    return-object v0
.end method

.method public d(J)V
    .locals 4

    .prologue
    .line 529
    invoke-virtual {p0}, Lxj;->f()V

    .line 530
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    .line 531
    iget v2, p0, Lxj;->o:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    .line 532
    iput-wide p1, p0, Lxj;->f:J

    .line 533
    const/4 v2, 0x4

    iput v2, p0, Lxj;->o:I

    .line 535
    :cond_0
    sub-long v2, v0, p1

    iput-wide v2, p0, Lxj;->e:J

    .line 536
    invoke-direct {p0, v0, v1}, Lxj;->b(J)Z

    .line 537
    return-void
.end method

.method public d()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 950
    iget v1, p0, Lxj;->o:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lxj;->o:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    iget v1, p0, Lxj;->o:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic e()Lwz;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lxj;->h()Lxj;

    move-result-object v0

    return-object v0
.end method

.method f()V
    .locals 3

    .prologue
    .line 484
    iget-boolean v0, p0, Lxj;->b:Z

    if-nez v0, :cond_1

    .line 485
    iget-object v0, p0, Lxj;->c:[Lxh;

    array-length v1, v0

    .line 486
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 487
    iget-object v2, p0, Lxj;->c:[Lxh;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lxh;->b()V

    .line 486
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 489
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lxj;->b:Z

    .line 491
    :cond_1
    return-void
.end method

.method public h()Lxj;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1139
    invoke-super {p0}, Lwz;->e()Lwz;

    move-result-object v0

    check-cast v0, Lxj;

    .line 1140
    iget-object v2, p0, Lxj;->y:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 1141
    iget-object v3, p0, Lxj;->y:Ljava/util/ArrayList;

    .line 1142
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v0, Lxj;->y:Ljava/util/ArrayList;

    .line 1143
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v1

    .line 1144
    :goto_0
    if-ge v2, v4, :cond_0

    .line 1145
    iget-object v5, v0, Lxj;->y:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1144
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1148
    :cond_0
    const-wide/16 v2, -0x1

    iput-wide v2, v0, Lxj;->f:J

    .line 1149
    iput-boolean v1, v0, Lxj;->k:Z

    .line 1150
    iput v1, v0, Lxj;->l:I

    .line 1151
    iput-boolean v1, v0, Lxj;->b:Z

    .line 1152
    iput v1, v0, Lxj;->o:I

    .line 1153
    iput-boolean v1, v0, Lxj;->m:Z

    .line 1154
    iget-object v3, p0, Lxj;->c:[Lxh;

    .line 1155
    if-eqz v3, :cond_2

    .line 1156
    array-length v4, v3

    .line 1157
    new-array v2, v4, [Lxh;

    iput-object v2, v0, Lxj;->c:[Lxh;

    move v2, v1

    .line 1158
    :goto_1
    if-ge v2, v4, :cond_1

    .line 1159
    iget-object v5, v0, Lxj;->c:[Lxh;

    aget-object v6, v3, v2

    invoke-virtual {v6}, Lxh;->a()Lxh;

    move-result-object v6

    aput-object v6, v5, v2

    .line 1158
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1161
    :cond_1
    new-instance v2, Les;

    invoke-direct {v2}, Les;-><init>()V

    iput-object v2, v0, Lxj;->d:Les;

    .line 1162
    :goto_2
    if-ge v1, v4, :cond_2

    .line 1163
    iget-object v2, p0, Lxj;->c:[Lxh;

    aget-object v2, v2, v1

    .line 1164
    iget-object v3, v0, Lxj;->d:Les;

    invoke-virtual {v2}, Lxh;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5, v2}, Les;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1162
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1167
    :cond_2
    return-object v0
.end method

.method public i()J
    .locals 4

    .prologue
    .line 547
    iget-boolean v0, p0, Lxj;->b:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lxj;->o:I

    if-nez v0, :cond_1

    .line 548
    :cond_0
    const-wide/16 v0, 0x0

    .line 550
    :goto_0
    return-wide v0

    :cond_1
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lxj;->e:J

    sub-long/2addr v0, v2

    goto :goto_0
.end method
