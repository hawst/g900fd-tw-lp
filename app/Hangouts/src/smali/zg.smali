.class public final Lzg;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final A:I

.field public final B:Ljava/lang/String;

.field public final C:I

.field public final D:Ljava/lang/String;

.field public final E:Ljava/lang/String;

.field public final F:Ljava/lang/String;

.field public final G:I

.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:I

.field public final g:I

.field public final h:J

.field public final i:J

.field public final j:Ljava/lang/String;

.field public final k:Ljava/lang/String;

.field public final l:Ljava/lang/String;

.field public final m:Ljava/lang/String;

.field public final n:Ljava/lang/String;

.field public final o:Ljava/lang/String;

.field public final p:Ljava/lang/String;

.field public final q:Ljava/lang/String;

.field public final r:D

.field public final s:D

.field public final t:Ljava/lang/String;

.field public final u:Ljava/lang/String;

.field public final v:Ljava/lang/String;

.field public final w:I

.field public final x:Ljava/lang/String;

.field public final y:I

.field public final z:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;DDLjava/lang/String;ILjava/lang/String;IIILjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 3098
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3099
    iput-object p1, p0, Lzg;->a:Ljava/lang/String;

    .line 3100
    iput-object p2, p0, Lzg;->b:Ljava/lang/String;

    .line 3101
    iput-object p3, p0, Lzg;->c:Ljava/lang/String;

    .line 3102
    iput-object p4, p0, Lzg;->d:Ljava/lang/String;

    .line 3103
    iput-object p5, p0, Lzg;->e:Ljava/lang/String;

    .line 3104
    iput p6, p0, Lzg;->f:I

    .line 3105
    iput p7, p0, Lzg;->g:I

    .line 3106
    iput-wide p8, p0, Lzg;->h:J

    .line 3107
    iput-wide p10, p0, Lzg;->i:J

    .line 3108
    iput-object p12, p0, Lzg;->j:Ljava/lang/String;

    .line 3109
    iput-object p13, p0, Lzg;->k:Ljava/lang/String;

    .line 3110
    move-object/from16 v0, p14

    iput-object v0, p0, Lzg;->l:Ljava/lang/String;

    .line 3111
    move-object/from16 v0, p15

    iput-object v0, p0, Lzg;->m:Ljava/lang/String;

    .line 3112
    move-object/from16 v0, p16

    iput-object v0, p0, Lzg;->n:Ljava/lang/String;

    .line 3113
    move-object/from16 v0, p17

    iput-object v0, p0, Lzg;->o:Ljava/lang/String;

    .line 3114
    move-object/from16 v0, p18

    iput-object v0, p0, Lzg;->p:Ljava/lang/String;

    .line 3115
    move-object/from16 v0, p37

    iput-object v0, p0, Lzg;->q:Ljava/lang/String;

    .line 3116
    move-wide/from16 v0, p19

    iput-wide v0, p0, Lzg;->r:D

    .line 3117
    move-wide/from16 v0, p21

    iput-wide v0, p0, Lzg;->s:D

    .line 3118
    move-object/from16 v0, p23

    iput-object v0, p0, Lzg;->t:Ljava/lang/String;

    .line 3119
    move-object/from16 v0, p35

    iput-object v0, p0, Lzg;->u:Ljava/lang/String;

    .line 3120
    move-object/from16 v0, p36

    iput-object v0, p0, Lzg;->v:Ljava/lang/String;

    .line 3121
    move/from16 v0, p24

    iput v0, p0, Lzg;->w:I

    .line 3122
    move-object/from16 v0, p25

    iput-object v0, p0, Lzg;->x:Ljava/lang/String;

    .line 3123
    move/from16 v0, p26

    iput v0, p0, Lzg;->y:I

    .line 3124
    move/from16 v0, p27

    iput v0, p0, Lzg;->z:I

    .line 3125
    move/from16 v0, p28

    iput v0, p0, Lzg;->A:I

    .line 3126
    move-object/from16 v0, p29

    iput-object v0, p0, Lzg;->B:Ljava/lang/String;

    .line 3127
    move/from16 v0, p30

    iput v0, p0, Lzg;->C:I

    .line 3128
    move-object/from16 v0, p31

    iput-object v0, p0, Lzg;->D:Ljava/lang/String;

    .line 3129
    move-object/from16 v0, p32

    iput-object v0, p0, Lzg;->E:Ljava/lang/String;

    .line 3130
    move-object/from16 v0, p33

    iput-object v0, p0, Lzg;->F:Ljava/lang/String;

    .line 3131
    move/from16 v0, p34

    iput v0, p0, Lzg;->G:I

    .line 3132
    return-void
.end method
