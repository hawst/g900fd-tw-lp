.class final Lbov;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lbou;


# direct methods
.method constructor <init>(Lbou;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lbov;->a:Lbou;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 55
    iget-object v0, p0, Lbov;->a:Lbou;

    invoke-static {v0}, Lbou;->a(Lbou;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 56
    :try_start_0
    invoke-static {}, Lbou;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    const-string v0, "Babel"

    iget-object v2, p0, Lbov;->a:Lbou;

    const-string v3, "Shutdown Runnable"

    invoke-virtual {v2, v3}, Lbou;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    :cond_0
    iget-object v0, p0, Lbov;->a:Lbou;

    invoke-static {v0}, Lbou;->b(Lbou;)I

    move-result v0

    if-lez v0, :cond_1

    .line 62
    monitor-exit v1

    .line 72
    :goto_0
    return-void

    .line 64
    :cond_1
    iget-object v0, p0, Lbov;->a:Lbou;

    iget-object v2, p0, Lbov;->a:Lbou;

    invoke-static {v2}, Lbou;->c(Lbou;)I

    move-result v2

    invoke-virtual {v0, v2}, Lbou;->stopSelfResult(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 65
    invoke-static {}, Lbou;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 66
    const-string v0, "Babel"

    iget-object v2, p0, Lbov;->a:Lbou;

    const-string v3, "StopSelf Succeeded"

    invoke-virtual {v2, v3}, Lbou;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    :cond_2
    iget-object v0, p0, Lbov;->a:Lbou;

    invoke-virtual {v0}, Lbou;->d()V

    .line 72
    :cond_3
    :goto_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 69
    :cond_4
    :try_start_1
    invoke-static {}, Lbou;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 70
    const-string v0, "Babel"

    iget-object v2, p0, Lbov;->a:Lbou;

    const-string v3, "StopSelf Cancelled"

    invoke-virtual {v2, v3}, Lbou;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method
