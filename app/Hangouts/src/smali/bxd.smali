.class final Lbxd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lorg/apache/http/conn/ClientConnectionRequest;


# instance fields
.field final synthetic a:Lorg/apache/http/conn/ClientConnectionRequest;

.field final synthetic b:Lbxc;


# direct methods
.method constructor <init>(Lbxc;Lorg/apache/http/conn/ClientConnectionRequest;)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lbxd;->b:Lbxc;

    iput-object p2, p0, Lbxd;->a:Lorg/apache/http/conn/ClientConnectionRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abortRequest()V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lbxd;->a:Lorg/apache/http/conn/ClientConnectionRequest;

    invoke-interface {v0}, Lorg/apache/http/conn/ClientConnectionRequest;->abortRequest()V

    .line 140
    return-void
.end method

.method public getConnection(JLjava/util/concurrent/TimeUnit;)Lorg/apache/http/conn/ManagedClientConnection;
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lbxd;->a:Lorg/apache/http/conn/ClientConnectionRequest;

    invoke-interface {v0, p1, p2, p3}, Lorg/apache/http/conn/ClientConnectionRequest;->getConnection(JLjava/util/concurrent/TimeUnit;)Lorg/apache/http/conn/ManagedClientConnection;

    move-result-object v0

    .line 129
    invoke-interface {v0}, Lorg/apache/http/conn/ManagedClientConnection;->getMetrics()Lorg/apache/http/HttpConnectionMetrics;

    move-result-object v1

    .line 131
    if-eqz v1, :cond_0

    .line 132
    invoke-interface {v1}, Lorg/apache/http/HttpConnectionMetrics;->reset()V

    .line 134
    :cond_0
    return-object v0
.end method
