.class public final Ldor;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldor;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Boolean;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3804
    const/4 v0, 0x0

    new-array v0, v0, [Ldor;

    sput-object v0, Ldor;->a:[Ldor;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3805
    invoke-direct {p0}, Lepn;-><init>()V

    .line 3827
    const/4 v0, 0x0

    iput-object v0, p0, Ldor;->h:Ljava/lang/Integer;

    .line 3805
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 3859
    const/4 v0, 0x0

    .line 3860
    iget-object v1, p0, Ldor;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3861
    const/4 v0, 0x1

    iget-object v1, p0, Ldor;->b:Ljava/lang/String;

    .line 3862
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3864
    :cond_0
    iget-object v1, p0, Ldor;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 3865
    const/4 v1, 0x2

    iget-object v2, p0, Ldor;->c:Ljava/lang/String;

    .line 3866
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3868
    :cond_1
    iget-object v1, p0, Ldor;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 3869
    const/4 v1, 0x3

    iget-object v2, p0, Ldor;->d:Ljava/lang/Boolean;

    .line 3870
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3872
    :cond_2
    iget-object v1, p0, Ldor;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 3873
    const/4 v1, 0x4

    iget-object v2, p0, Ldor;->e:Ljava/lang/String;

    .line 3874
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3876
    :cond_3
    iget-object v1, p0, Ldor;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 3877
    const/4 v1, 0x5

    iget-object v2, p0, Ldor;->f:Ljava/lang/String;

    .line 3878
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3880
    :cond_4
    iget-object v1, p0, Ldor;->g:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 3881
    const/4 v1, 0x6

    iget-object v2, p0, Ldor;->g:Ljava/lang/String;

    .line 3882
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3884
    :cond_5
    iget-object v1, p0, Ldor;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 3885
    const/4 v1, 0x7

    iget-object v2, p0, Ldor;->h:Ljava/lang/Integer;

    .line 3886
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3888
    :cond_6
    iget-object v1, p0, Ldor;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3889
    iput v0, p0, Ldor;->cachedSize:I

    .line 3890
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 3801
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldor;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldor;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldor;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldor;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldor;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldor;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldor;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldor;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldor;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldor;->h:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldor;->h:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 3832
    iget-object v0, p0, Ldor;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 3833
    const/4 v0, 0x1

    iget-object v1, p0, Ldor;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 3835
    :cond_0
    iget-object v0, p0, Ldor;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 3836
    const/4 v0, 0x2

    iget-object v1, p0, Ldor;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 3838
    :cond_1
    iget-object v0, p0, Ldor;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 3839
    const/4 v0, 0x3

    iget-object v1, p0, Ldor;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 3841
    :cond_2
    iget-object v0, p0, Ldor;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 3842
    const/4 v0, 0x4

    iget-object v1, p0, Ldor;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 3844
    :cond_3
    iget-object v0, p0, Ldor;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 3845
    const/4 v0, 0x5

    iget-object v1, p0, Ldor;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 3847
    :cond_4
    iget-object v0, p0, Ldor;->g:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 3848
    const/4 v0, 0x6

    iget-object v1, p0, Ldor;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 3850
    :cond_5
    iget-object v0, p0, Ldor;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 3851
    const/4 v0, 0x7

    iget-object v1, p0, Ldor;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 3853
    :cond_6
    iget-object v0, p0, Ldor;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 3855
    return-void
.end method
