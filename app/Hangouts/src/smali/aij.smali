.class public final Laij;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lain;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic b:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

.field private final c:Lyj;

.field private d:J

.field private final e:Laim;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;Lyj;)V
    .locals 2

    .prologue
    .line 2512
    iput-object p1, p0, Laij;->b:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2457
    new-instance v0, Laim;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Laim;-><init>(Laij;B)V

    iput-object v0, p0, Laij;->e:Laim;

    .line 2513
    iput-object p2, p0, Laij;->c:Lyj;

    .line 2514
    return-void
.end method

.method private a(Ljava/lang/String;JZZ)V
    .locals 4

    .prologue
    .line 2634
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2635
    invoke-static {p1}, Lf;->f(Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2636
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2638
    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v2, v0, [J

    .line 2639
    const/4 v0, 0x0

    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_1

    .line 2642
    aput-wide p2, v2, v0

    .line 2639
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2645
    :cond_1
    iget-object v3, p0, Laij;->c:Lyj;

    .line 2646
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 2645
    invoke-static {v3, v0, v2, p4, p5}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;[Ljava/lang/String;[JZZ)I

    .line 2649
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 2529
    iget-object v0, p0, Laij;->e:Laim;

    invoke-virtual {v0}, Laim;->a()V

    .line 2530
    return-void
.end method

.method a(I)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 2573
    if-le p1, v2, :cond_1

    .line 2574
    iget-object v0, p0, Laij;->b:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->hl:I

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2581
    :goto_0
    iget-object v1, p0, Laij;->b:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->m(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)Lcom/google/android/apps/hangouts/listui/ActionableToastBar;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2582
    iget-object v1, p0, Laij;->b:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->m(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)Lcom/google/android/apps/hangouts/listui/ActionableToastBar;

    move-result-object v1

    new-instance v2, Laik;

    invoke-direct {v2, p0}, Laik;-><init>(Laij;)V

    sget v3, Lh;->r:I

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->a(Laup;Ljava/lang/CharSequence;I)V

    .line 2608
    :cond_0
    :goto_1
    return-void

    .line 2575
    :cond_1
    if-ne p1, v2, :cond_0

    .line 2576
    iget-object v0, p0, Laij;->b:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->bc:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2600
    :cond_2
    iget-object v0, p0, Laij;->b:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->n(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-static {v0}, Lcwz;->a(Ljava/lang/Object;)V

    .line 2601
    iget-object v0, p0, Laij;->b:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    new-instance v1, Lail;

    invoke-direct {v1, p0, p1}, Lail;-><init>(Laij;I)V

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_1
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2522
    iget-object v0, p0, Laij;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laij;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 2523
    const-string v0, "last_archived"

    iget-object v1, p0, Laij;->a:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2526
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2533
    iget-object v0, p0, Laij;->e:Laim;

    invoke-virtual {v0, p1}, Laim;->a(Ljava/lang/String;)V

    .line 2534
    return-void
.end method

.method public a(Ljava/lang/String;J)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 2547
    iget-object v0, p0, Laij;->e:Laim;

    new-instance v1, Lain;

    invoke-direct {v1, p1, p2, p3}, Lain;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v0, v1}, Laim;->a(Lain;)Z

    move-result v0

    .line 2550
    if-nez v0, :cond_0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move v5, v4

    .line 2551
    invoke-direct/range {v0 .. v5}, Laij;->a(Ljava/lang/String;JZZ)V

    .line 2554
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 2556
    iget-object v2, p0, Laij;->a:Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    .line 2557
    iget-wide v2, p0, Laij;->d:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x3e8

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 2558
    iget-object v2, p0, Laij;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 2564
    :cond_1
    :goto_0
    iput-wide v0, p0, Laij;->d:J

    .line 2566
    iget-object v0, p0, Laij;->a:Ljava/util/ArrayList;

    new-instance v1, Lain;

    invoke-direct {v1, p1, p2, p3}, Lain;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2568
    iget-object v0, p0, Laij;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {p0, v0}, Laij;->a(I)V

    .line 2569
    return-void

    .line 2561
    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Laij;->a:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lain;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2517
    iput-object p1, p0, Laij;->a:Ljava/util/ArrayList;

    .line 2518
    iget-object v0, p0, Laij;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {p0, v0}, Laij;->a(I)V

    .line 2519
    return-void
.end method

.method a(Ljava/util/List;ZZ)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lain;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    .line 2622
    if-nez p1, :cond_1

    .line 2630
    :cond_0
    return-void

    .line 2626
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lain;

    .line 2627
    iget-object v1, v0, Lain;->a:Ljava/lang/String;

    iget-wide v2, v0, Lain;->b:J

    move-object v0, p0

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Laij;->a(Ljava/lang/String;JZZ)V

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 2537
    iget-object v0, p0, Laij;->e:Laim;

    invoke-virtual {v0}, Laim;->b()V

    .line 2538
    return-void
.end method

.method public b(Ljava/lang/String;J)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move v5, v4

    .line 2617
    invoke-direct/range {v0 .. v5}, Laij;->a(Ljava/lang/String;JZZ)V

    .line 2618
    return-void
.end method
