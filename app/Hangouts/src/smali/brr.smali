.class public abstract Lbrr;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final b:Z


# instance fields
.field protected final a:Landroid/os/Handler;

.field private final c:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lbrv;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private volatile e:Ljava/lang/Thread;

.field private final f:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lbys;->l:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lbrr;->b:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lbrr;->c:Ljava/util/concurrent/ConcurrentHashMap;

    .line 84
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lbrr;->d:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 90
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lbrr;->a:Landroid/os/Handler;

    .line 101
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbrr;->f:Ljava/lang/Object;

    return-void
.end method

.method static synthetic a(Lbrr;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 23
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    sget-boolean v0, Lbrr;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "loadKeys looping: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbrr;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lbrr;->d:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/LinkedBlockingQueue;->drainTo(Ljava/util/Collection;)I

    move-result v0

    if-nez v0, :cond_3

    :try_start_0
    iget-object v0, p0, Lbrr;->d:Ljava/util/concurrent/LinkedBlockingQueue;

    const-wide/16 v3, 0x3c

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v3, v4, v5}, Ljava/util/concurrent/LinkedBlockingQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    if-nez v0, :cond_5

    iget-object v3, p0, Lbrr;->f:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    iget-object v0, p0, Lbrr;->d:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/LinkedBlockingQueue;->drainTo(Ljava/util/Collection;)I

    move-result v0

    if-nez v0, :cond_2

    sget-boolean v0, Lbrr;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Exiting loading thread: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbrr;->e:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Babel"

    invoke-virtual {p0}, Lbrr;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lbrr;->e:Ljava/lang/Thread;

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    :catch_0
    move-exception v0

    const-string v3, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Our background loader thread was interrupted?? "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_1

    :cond_2
    monitor-exit v3

    :goto_2
    iget-object v0, p0, Lbrr;->d:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/LinkedBlockingQueue;->drainTo(Ljava/util/Collection;)I

    :cond_3
    sget-boolean v0, Lbrr;->b:Z

    if-eqz v0, :cond_4

    const-string v0, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Loading "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " keys."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p0, v2}, Lbrr;->a(Ljava/util/ArrayList;)V

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_5
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method private c()V
    .locals 4

    .prologue
    .line 357
    iget-object v1, p0, Lbrr;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 358
    :try_start_0
    iget-object v0, p0, Lbrr;->e:Ljava/lang/Thread;

    if-nez v0, :cond_1

    .line 359
    new-instance v0, Ljava/lang/Thread;

    new-instance v2, Lbru;

    invoke-direct {v2, p0}, Lbru;-><init>(Lbrr;)V

    .line 364
    invoke-virtual {p0}, Lbrr;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v0, p0, Lbrr;->e:Ljava/lang/Thread;

    .line 366
    sget-boolean v0, Lbrr;->b:Z

    if-eqz v0, :cond_0

    .line 367
    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Starting loader thread: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lbrr;->e:Ljava/lang/Thread;

    invoke-virtual {v3}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    :cond_0
    iget-object v0, p0, Lbrr;->e:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 371
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private f(Lbrv;)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 274
    invoke-virtual {p1}, Lbrv;->c()Ljava/lang/String;

    move-result-object v3

    .line 276
    sget-boolean v0, Lbrr;->b:Z

    if-eqz v0, :cond_0

    .line 277
    const-string v0, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "addRequest 1: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbrr;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    :cond_0
    iget-object v0, p1, Lbrv;->c:Lbrr;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lbrv;->c:Lbrr;

    if-ne v0, p0, :cond_4

    :cond_1
    move v0, v2

    :goto_0
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 281
    iput-object p0, p1, Lbrv;->c:Lbrr;

    .line 284
    iget-object v4, p0, Lbrr;->f:Ljava/lang/Object;

    monitor-enter v4

    .line 285
    :try_start_0
    iget-object v0, p0, Lbrr;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 286
    if-nez v0, :cond_5

    .line 287
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 288
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 289
    iget-object v1, p0, Lbrr;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, v3, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 291
    sget-boolean v0, Lbrr;->b:Z

    if-eqz v0, :cond_2

    .line 292
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "addRequest 2: "

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbrr;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    :cond_2
    iget-object v0, p0, Lbrr;->d:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0, v3}, Ljava/util/concurrent/LinkedBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 298
    sget-boolean v0, Lbrr;->b:Z

    if-eqz v0, :cond_3

    .line 299
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "Added first request for: "

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    :cond_3
    :goto_1
    monitor-exit v4

    .line 308
    return v2

    :cond_4
    move v0, v1

    .line 280
    goto :goto_0

    .line 302
    :cond_5
    sget-boolean v2, Lbrr;->b:Z

    if-eqz v2, :cond_6

    .line 303
    const-string v2, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Added additional request for: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    :cond_6
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v2, v1

    goto :goto_1

    .line 307
    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0
.end method


# virtual methods
.method protected abstract a()Ljava/lang/String;
.end method

.method protected a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lbrr;->a:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 193
    return-void
.end method

.method protected abstract a(Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public a(Lbrv;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 113
    invoke-static {}, Lcwz;->a()V

    .line 114
    if-nez p1, :cond_1

    .line 123
    :cond_0
    :goto_0
    return v1

    .line 118
    :cond_1
    invoke-direct {p0, p1}, Lbrr;->f(Lbrv;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    invoke-direct {p0}, Lbrr;->c()V

    goto :goto_0
.end method

.method protected b()Ljava/lang/String;
    .locals 3

    .prologue
    .line 185
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 186
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Number of keys in mPendingKeys: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbrr;->d:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/LinkedBlockingQueue;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 187
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\nNumber of keys in mRequestsByKey: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbrr;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Lbrv;)V
    .locals 0

    .prologue
    .line 133
    invoke-static {}, Lcwz;->a()V

    .line 134
    invoke-virtual {p0, p1}, Lbrr;->e(Lbrv;)V

    .line 135
    return-void
.end method

.method protected b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lbrr;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected c(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbrv;",
            ">;"
        }
    .end annotation

    .prologue
    .line 200
    iget-object v0, p0, Lbrr;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public c(Lbrv;)V
    .locals 1

    .prologue
    .line 142
    new-instance v0, Lbrs;

    invoke-direct {v0, p0, p1}, Lbrs;-><init>(Lbrr;Lbrv;)V

    invoke-virtual {p0, v0}, Lbrr;->a(Ljava/lang/Runnable;)V

    .line 148
    return-void
.end method

.method protected d(Ljava/lang/String;)Lbrv;
    .locals 4

    .prologue
    .line 204
    const/4 v1, 0x0

    .line 205
    iget-object v2, p0, Lbrr;->f:Ljava/lang/Object;

    monitor-enter v2

    .line 206
    :try_start_0
    iget-object v0, p0, Lbrr;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 207
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 208
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbrv;

    .line 210
    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 211
    return-object v0

    .line 210
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public d(Lbrv;)V
    .locals 1

    .prologue
    .line 156
    new-instance v0, Lbrt;

    invoke-direct {v0, p0, p1}, Lbrt;-><init>(Lbrr;Lbrv;)V

    invoke-virtual {p0, v0}, Lbrr;->a(Ljava/lang/Runnable;)V

    .line 162
    return-void
.end method

.method protected e(Lbrv;)V
    .locals 5

    .prologue
    .line 313
    iget-object v0, p1, Lbrv;->c:Lbrr;

    if-nez v0, :cond_0

    .line 336
    :goto_0
    return-void

    .line 316
    :cond_0
    iget-object v0, p1, Lbrv;->c:Lbrr;

    invoke-static {v0, p0}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 318
    iget-object v1, p0, Lbrr;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 319
    :try_start_0
    invoke-virtual {p1}, Lbrv;->c()Ljava/lang/String;

    move-result-object v2

    .line 320
    iget-object v0, p0, Lbrr;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 321
    if-nez v0, :cond_1

    .line 322
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 336
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 324
    :cond_1
    const/4 v3, 0x0

    :try_start_1
    iput-object v3, p1, Lbrv;->c:Lbrr;

    .line 325
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 326
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 327
    iget-object v0, p0, Lbrr;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 330
    iget-object v0, p0, Lbrr;->d:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/LinkedBlockingQueue;->remove(Ljava/lang/Object;)Z

    .line 332
    sget-boolean v0, Lbrr;->b:Z

    if-eqz v0, :cond_2

    .line 333
    const-string v0, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Removed request for: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    :cond_2
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method protected e(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 346
    iget-object v1, p0, Lbrr;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 347
    :try_start_0
    iget-object v0, p0, Lbrr;->d:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/LinkedBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 348
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 349
    invoke-direct {p0}, Lbrr;->c()V

    .line 350
    return-void

    .line 348
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
