.class final Lbsh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field a:Z

.field final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbsi;",
            ">;"
        }
    .end annotation
.end field

.field final c:Lcvx;

.field final d:Lcwf;

.field final e:Lcvx;

.field final f:Lcwf;


# direct methods
.method public constructor <init>(Ljava/util/List;Lcvx;Lcwf;Lcvx;Lcwf;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbsi;",
            ">;",
            "Lcvx;",
            "Lcwf;",
            "Lcvx;",
            "Lcwf;",
            ")V"
        }
    .end annotation

    .prologue
    .line 710
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 711
    iput-object p1, p0, Lbsh;->b:Ljava/util/List;

    .line 712
    iput-object p2, p0, Lbsh;->c:Lcvx;

    .line 713
    iput-object p3, p0, Lbsh;->d:Lcwf;

    .line 714
    iput-object p4, p0, Lbsh;->e:Lcvx;

    .line 715
    iput-object p5, p0, Lbsh;->f:Lcwf;

    .line 716
    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 721
    invoke-static {}, Lcwz;->a()V

    .line 722
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 723
    iget-object v0, p0, Lbsh;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsi;

    .line 724
    iget-object v4, p0, Lbsh;->c:Lcvx;

    iget-object v5, p0, Lbsh;->d:Lcwf;

    invoke-interface {v0, v4, v5}, Lbsi;->a(Lcvx;Lcwf;)V

    goto :goto_0

    .line 726
    :cond_0
    invoke-static {}, Lbsc;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 727
    const-string v0, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GmsPeopleCache: onDataBufferReady "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 729
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v1, v4, v1

    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 727
    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 733
    :cond_1
    iget-object v0, p0, Lbsh;->e:Lcvx;

    if-eqz v0, :cond_2

    .line 734
    iget-object v0, p0, Lbsh;->e:Lcvx;

    invoke-virtual {v0}, Lcvx;->d()V

    .line 737
    :cond_2
    iget-object v0, p0, Lbsh;->f:Lcwf;

    if-eqz v0, :cond_3

    .line 738
    iget-object v0, p0, Lbsh;->f:Lcwf;

    invoke-virtual {v0}, Lcwf;->d()V

    .line 741
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbsh;->a:Z

    .line 742
    return-void
.end method
