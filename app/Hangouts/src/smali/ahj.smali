.class final Lahj;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lahi;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Les",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lahi;)V
    .locals 1

    .prologue
    .line 7656
    iput-object p1, p0, Lahj;->a:Lahi;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 7657
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lahj;->b:Ljava/util/List;

    .line 7659
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lahj;->c:Ljava/util/List;

    return-void
.end method

.method private varargs a()Ljava/lang/Void;
    .locals 4

    .prologue
    .line 7757
    new-instance v0, Lyt;

    iget-object v1, p0, Lahj;->a:Lahi;

    iget-object v1, v1, Lahi;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a()Lyj;

    move-result-object v1

    invoke-direct {v0, v1}, Lyt;-><init>(Lyj;)V

    .line 7759
    iget-object v1, p0, Lahj;->a:Lahi;

    iget-object v1, v1, Lahi;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->K()Lbdh;

    move-result-object v1

    .line 7760
    invoke-static {}, Lbsc;->a()Lbsc;

    move-result-object v2

    .line 7764
    :try_start_0
    invoke-virtual {v2}, Lbsc;->c()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 7765
    iget-object v3, v1, Lbdh;->b:Lbdk;

    iget-object v3, v3, Lbdk;->a:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 7766
    iget-object v1, v1, Lbdh;->b:Lbdk;

    iget-object v1, v1, Lbdk;->a:Ljava/lang/String;

    .line 7767
    invoke-virtual {v2, v1}, Lbsc;->a(Ljava/lang/String;)Laea;

    move-result-object v3

    invoke-direct {p0, v1, v3, v0}, Lahj;->a(Ljava/lang/String;Laea;Lyt;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7775
    :cond_0
    :goto_0
    invoke-virtual {v2}, Lbsc;->b()V

    .line 7777
    const/4 v0, 0x0

    return-object v0

    .line 7768
    :cond_1
    :try_start_1
    iget-object v3, v1, Lbdh;->c:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 7769
    iget-object v1, v1, Lbdh;->c:Ljava/lang/String;

    .line 7770
    invoke-virtual {v0, v1, v2}, Lyt;->b(Ljava/lang/String;Lbsc;)Laea;

    move-result-object v3

    invoke-direct {p0, v1, v3, v0}, Lahj;->a(Ljava/lang/String;Laea;Lyt;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 7775
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lbsc;->b()V

    throw v0
.end method

.method private a(Ljava/lang/String;Laea;Lyt;)V
    .locals 11

    .prologue
    .line 7674
    if-nez p2, :cond_0

    .line 7675
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    const-string v1, "text"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/ no contacts match"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Les;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lahj;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lahj;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 7715
    :goto_0
    return-void

    .line 7679
    :cond_0
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    .line 7680
    const-string v1, "text"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Laea;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Les;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7681
    iget-object v1, p0, Lahj;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 7683
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 7685
    invoke-virtual {p2}, Laea;->j()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laeh;

    .line 7686
    iget-object v4, v0, Laeh;->a:Ljava/lang/String;

    .line 7687
    const/4 v1, 0x0

    .line 7688
    invoke-virtual {p3, v1, v4}, Lyt;->a(Lbdk;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 7690
    invoke-static {v4}, Lbdk;->d(Ljava/lang/String;)Lbdk;

    move-result-object v1

    .line 7691
    const/4 v6, 0x0

    const/4 v7, 0x3

    .line 7692
    invoke-virtual {p3, v1, v6, v7}, Lyt;->a(Lbdk;ZI)Lzi;

    move-result-object v6

    .line 7695
    iget-object v1, p0, Lahj;->a:Lahi;

    iget-object v1, v1, Lahi;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 7696
    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->C(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "(INCL)"

    .line 7700
    :goto_2
    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "    %s\n    computed merge key: %s %s\n    conversation exists? %s"

    const/4 v9, 0x4

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v0, v0, Laeh;->d:Ljava/lang/String;

    aput-object v0, v9, v10

    const/4 v0, 0x1

    aput-object v5, v9, v0

    const/4 v0, 0x2

    aput-object v1, v9, v0

    const/4 v1, 0x3

    if-eqz v6, :cond_2

    const/4 v0, 0x1

    .line 7705
    :goto_3
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v9, v1

    .line 7700
    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v4, v0}, Lahj;->a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 7696
    :cond_1
    const-string v1, "(EXCL)"

    goto :goto_2

    .line 7700
    :cond_2
    const/4 v0, 0x0

    goto :goto_3

    .line 7709
    :cond_3
    invoke-virtual {p2}, Laea;->k()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laef;

    .line 7710
    const-string v3, "email"

    iget-object v0, v0, Laef;->a:Ljava/lang/String;

    invoke-static {v2, v3, v0}, Lahj;->a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 7713
    :cond_4
    const-string v0, "qualifiedId"

    invoke-virtual {p2}, Laea;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v0, v1}, Lahj;->a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    .line 7714
    iget-object v0, p0, Lahj;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method private static a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Les",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 7666
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    .line 7667
    const-string v1, "main"

    invoke-virtual {v0, v1, p1}, Les;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7668
    const-string v1, "sub"

    invoke-virtual {v0, v1, p2}, Les;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7669
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 7670
    return-void
.end method


# virtual methods
.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 7656
    invoke-direct {p0}, Lahj;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 12

    .prologue
    .line 7656
    iget-object v0, p0, Lahj;->a:Lahi;

    iget-object v0, v0, Lahi;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v1

    new-instance v10, Landroid/app/AlertDialog$Builder;

    invoke-direct {v10, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/widget/SimpleExpandableListAdapter;

    iget-object v2, p0, Lahj;->b:Ljava/util/List;

    sget v3, Lf;->eW:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "text"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    new-array v5, v5, [I

    const/4 v6, 0x0

    const v7, 0x1020014

    aput v7, v5, v6

    iget-object v6, p0, Lahj;->c:Ljava/util/List;

    sget v7, Lf;->eX:I

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v11, "main"

    aput-object v11, v8, v9

    const/4 v9, 0x1

    const-string v11, "sub"

    aput-object v11, v8, v9

    const/4 v9, 0x2

    new-array v9, v9, [I

    fill-array-data v9, :array_0

    invoke-direct/range {v0 .. v9}, Landroid/widget/SimpleExpandableListAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[ILjava/util/List;I[Ljava/lang/String;[I)V

    new-instance v2, Landroid/widget/ExpandableListView;

    invoke-direct {v2, v1}, Landroid/widget/ExpandableListView;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x1

    invoke-direct {v1, v3, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v1}, Landroid/widget/ExpandableListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v2, v0}, Landroid/widget/ExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    invoke-virtual {v10, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v10}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void

    nop

    :array_0
    .array-data 4
        0x1020014
        0x1020015
    .end array-data
.end method
