.class public abstract Lbmf;
.super Lbmr;
.source "PG"


# static fields
.field static final a:Z


# instance fields
.field protected b:Ljava/lang/String;

.field protected c:Ljava/lang/CharSequence;

.field protected d:Ljava/lang/String;

.field protected e:Ljava/lang/CharSequence;

.field protected f:Landroid/content/Intent;

.field protected g:Ljava/lang/String;

.field protected h:I

.field protected i:Z

.field final j:Lbmh;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 86
    sget-object v0, Lbys;->k:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lbmf;->a:Z

    return-void
.end method

.method constructor <init>(Lyj;Lbmh;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 326
    invoke-static {p2}, Lbmf;->a(Lbmh;)Lbxz;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lbmr;-><init>(Lyj;Lbxz;)V

    .line 94
    iput-object v1, p0, Lbmf;->b:Ljava/lang/String;

    .line 95
    iput-object v1, p0, Lbmf;->c:Ljava/lang/CharSequence;

    .line 96
    iput-object v1, p0, Lbmf;->d:Ljava/lang/String;

    .line 97
    iput-object v1, p0, Lbmf;->e:Ljava/lang/CharSequence;

    .line 98
    iput-object v1, p0, Lbmf;->f:Landroid/content/Intent;

    .line 99
    iput-object v1, p0, Lbmf;->g:Ljava/lang/String;

    .line 100
    iput v2, p0, Lbmf;->h:I

    .line 327
    iput-object p2, p0, Lbmf;->j:Lbmh;

    .line 328
    iput v2, p0, Lbmf;->s:I

    .line 329
    return-void
.end method

.method public static a(I)I
    .locals 1

    .prologue
    .line 1663
    packed-switch p0, :pswitch_data_0

    .line 1671
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 1665
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1667
    :pswitch_1
    const/4 v0, 0x3

    goto :goto_0

    .line 1663
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Lyj;Ljava/lang/String;IZ)Landroid/app/Notification;
    .locals 15

    .prologue
    .line 738
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v8

    .line 739
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->h:Landroid/net/Uri;

    invoke-static {v0, p0}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Landroid/net/Uri;Lyj;)Landroid/net/Uri;

    move-result-object v0

    .line 744
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "limit"

    const-string v2, "21"

    .line 746
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 748
    const/4 v6, 0x0

    .line 749
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 750
    const/4 v7, 0x0

    .line 752
    invoke-static/range {p0 .. p1}, Lbmf;->a(Lyj;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v10

    .line 754
    :try_start_0
    invoke-static {}, Lzo;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lbmt;->a:[Ljava/lang/String;

    const-string v3, "conversation_id IN (SELECT + conversation_id FROM merge_keys WHERE merge_key=( SELECT merge_key FROM merge_keys WHERE conversation_id=?)) AND type IN (2, 1)"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const-string v5, "timestamp DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 759
    if-eqz v1, :cond_0

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_2

    .line 760
    :cond_0
    if-eqz v1, :cond_1

    .line 798
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    const/4 v0, 0x0

    .line 860
    :goto_0
    return-object v0

    :cond_2
    move v0, v7

    .line 763
    :cond_3
    const/4 v2, 0x0

    :try_start_2
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 764
    const/4 v3, 0x4

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 765
    invoke-static {v1}, Lbmf;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v5

    .line 766
    const/16 v4, 0x11

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lbmf;->c(Ljava/lang/String;)I

    move-result v6

    .line 768
    const/16 v4, 0x12

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 769
    invoke-static {v4}, Lf;->b(I)Z

    move-result v7

    .line 770
    const/4 v4, 0x2

    const/16 v11, 0x18

    .line 771
    invoke-interface {v1, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    if-ne v4, v11, :cond_a

    const/4 v4, 0x1

    .line 772
    :goto_1
    const/16 v11, 0xe

    invoke-interface {v1, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    .line 773
    const/16 v13, 0x19

    invoke-interface {v1, v13}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v13

    .line 776
    if-eqz v7, :cond_4

    if-eqz v4, :cond_5

    .line 777
    :cond_4
    invoke-static {v3}, Lbmf;->d(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 780
    :cond_5
    if-eqz v2, :cond_8

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_6

    if-eqz v5, :cond_8

    .line 781
    :cond_6
    cmp-long v4, v11, v13

    if-gtz v4, :cond_7

    .line 785
    const/4 v0, 0x1

    .line 787
    :cond_7
    invoke-static {v10, v2}, Lbmf;->a(Ljava/util/HashMap;Ljava/lang/String;)Z

    move-result v4

    .line 788
    if-eqz v4, :cond_b

    .line 791
    :goto_2
    invoke-static {v2, v3, v5, v6}, Lbne;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 793
    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 795
    :cond_8
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v2

    if-nez v2, :cond_3

    .line 797
    if-eqz v1, :cond_9

    .line 798
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 804
    :cond_9
    if-nez v0, :cond_d

    .line 805
    const/4 v0, 0x0

    goto :goto_0

    .line 771
    :cond_a
    const/4 v4, 0x0

    goto :goto_1

    .line 788
    :cond_b
    const/4 v2, 0x1

    .line 790
    :try_start_3
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v2

    goto :goto_2

    .line 797
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_3
    if-eqz v1, :cond_c

    .line 798
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_c
    throw v0

    .line 808
    :cond_d
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 809
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 812
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v1

    const/16 v3, 0x15

    if-ne v1, v3, :cond_e

    .line 813
    sget v1, Lh;->cr:I

    invoke-virtual {v8, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    const-string v3, "\n\n"

    invoke-virtual {v1, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 814
    add-int/lit8 v0, v0, -0x1

    :cond_e
    move v1, v0

    .line 818
    :goto_4
    if-ltz v1, :cond_10

    .line 819
    invoke-interface {v9, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 820
    if-lez v1, :cond_f

    .line 821
    const-string v0, "\n\n"

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 818
    :cond_f
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_4

    .line 824
    :cond_10
    add-int/lit8 v3, p2, 0x1

    .line 826
    const/4 v0, 0x0

    .line 827
    const/4 v1, 0x2

    if-le v3, v1, :cond_14

    .line 828
    new-instance v1, Landroid/text/SpannableString;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    if-eqz p3, :cond_13

    sget v0, Lf;->hB:I

    :goto_5
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    .line 834
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    .line 829
    invoke-virtual {v4, v0, v3, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    move-object v0, v1

    .line 839
    :cond_11
    :goto_6
    if-eqz v0, :cond_12

    .line 844
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    .line 845
    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lf;->cL:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v1, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v3, 0x0

    .line 846
    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v4

    const/16 v5, 0x21

    .line 844
    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 848
    const-string v1, "\n\n"

    invoke-virtual {v2, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 851
    :cond_12
    new-instance v0, Lbk;

    invoke-direct {v0, v8}, Lbk;-><init>(Landroid/content/Context;)V

    .line 852
    new-instance v1, Lbj;

    invoke-direct {v1, v0}, Lbj;-><init>(Lbk;)V

    .line 853
    invoke-virtual {v1, v2}, Lbj;->b(Ljava/lang/CharSequence;)Lbj;

    move-result-object v1

    .line 854
    invoke-virtual {v0, v1}, Lbk;->a(Lbu;)Lbk;

    .line 856
    new-instance v1, Lbv;

    invoke-direct {v1}, Lbv;-><init>()V

    .line 857
    invoke-virtual {v1}, Lbv;->a()Lbv;

    .line 858
    invoke-virtual {v0, v1}, Lbk;->a(Lbl;)Lbk;

    .line 860
    invoke-virtual {v0}, Lbk;->e()Landroid/app/Notification;

    move-result-object v0

    goto/16 :goto_0

    .line 828
    :cond_13
    sget v0, Lf;->hA:I

    goto :goto_5

    .line 835
    :cond_14
    if-eqz p3, :cond_11

    .line 836
    new-instance v0, Landroid/text/SpannableString;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lh;->kf:I

    .line 837
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_6

    .line 797
    :catchall_1
    move-exception v0

    goto/16 :goto_3
.end method

.method public static a(Lyj;)Lbmr;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1204
    const/4 v0, 0x0

    .line 1205
    invoke-static {p0}, Lbmf;->e(Lyj;)Lbmh;

    move-result-object v2

    .line 1207
    if-eqz v2, :cond_0

    iget-object v1, v2, Lbmh;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_3

    .line 1208
    :cond_0
    sget-boolean v1, Lbmf;->a:Z

    if-eqz v1, :cond_1

    .line 1209
    const-string v1, "Babel"

    const-string v2, "No unseen notifications"

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1227
    :cond_1
    :goto_0
    sget-boolean v1, Lbmf;->a:Z

    if-eqz v1, :cond_2

    .line 1228
    if-eqz v0, :cond_2

    .line 1229
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "title: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lbmf;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " content: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lbmf;->e:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1232
    :cond_2
    return-object v0

    .line 1212
    :cond_3
    iget-object v0, v2, Lbmh;->b:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmj;

    .line 1213
    iget-object v0, v0, Lbmj;->h:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmo;

    .line 1215
    instance-of v0, v0, Lbmk;

    if-eqz v0, :cond_4

    .line 1216
    new-instance v0, Lbmi;

    invoke-direct {v0, p0, v2}, Lbmi;-><init>(Lyj;Lbmh;)V

    .line 1220
    :goto_1
    iget-object v1, v2, Lbmh;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v3, 0x1

    if-le v1, v3, :cond_1

    .line 1224
    new-instance v1, Lbmm;

    invoke-direct {v1, p0, v2, v0}, Lbmm;-><init>(Lyj;Lbmh;Lbmf;)V

    move-object v0, v1

    goto :goto_0

    .line 1218
    :cond_4
    new-instance v0, Lbmn;

    invoke-direct {v0, p0, v2}, Lbmn;-><init>(Lyj;Lbmh;)V

    goto :goto_1
.end method

.method private static a(Lbmh;)Lbxz;
    .locals 4

    .prologue
    .line 308
    const/4 v0, 0x0

    .line 309
    if-eqz p0, :cond_3

    iget-object v1, p0, Lbmh;->b:Ljava/util/List;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lbmh;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 310
    new-instance v1, Lbxz;

    invoke-direct {v1}, Lbxz;-><init>()V

    .line 311
    iget-object v0, p0, Lbmh;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmj;

    .line 312
    iget-boolean v3, v0, Lbmj;->j:Z

    if-eqz v3, :cond_1

    .line 313
    iget-object v0, v0, Lbmj;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmo;

    .line 314
    check-cast v0, Lbmk;

    .line 315
    iget-object v0, v0, Lbmk;->f:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lbxz;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 318
    :cond_1
    iget-object v0, v0, Lbmj;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lbxz;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 322
    :cond_3
    return-object v0
.end method

.method private static a(Lyj;Ljava/lang/String;)Ljava/util/HashMap;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lyj;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 878
    new-instance v0, Lyt;

    invoke-direct {v0, p0}, Lyt;-><init>(Lyj;)V

    .line 880
    invoke-virtual {v0, p1}, Lyt;->K(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 882
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 885
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    .line 886
    iget-object v4, v0, Lbdh;->f:Ljava/lang/String;

    .line 888
    invoke-virtual {p0}, Lyj;->c()Lbdk;

    move-result-object v5

    iget-object v0, v0, Lbdh;->b:Lbdk;

    invoke-virtual {v5, v0}, Lbdk;->a(Lbdk;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 889
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 892
    invoke-virtual {v2, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 895
    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-static {v0, v1}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    .line 896
    :goto_1
    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    move v0, v1

    .line 895
    goto :goto_1

    .line 898
    :cond_2
    return-object v2
.end method

.method private static a(Ljava/util/HashMap;Ljava/lang/String;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 865
    if-nez p0, :cond_0

    move v0, v1

    .line 872
    :goto_0
    return v0

    .line 868
    :cond_0
    if-nez p1, :cond_1

    move v0, v1

    .line 869
    goto :goto_0

    .line 871
    :cond_1
    invoke-virtual {p0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 872
    invoke-static {v0, v1}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    if-le v0, v2, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method private static b(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1281
    const/4 v0, 0x6

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1282
    if-nez v0, :cond_0

    .line 1283
    const/4 v0, 0x7

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1284
    const/16 v1, 0x11

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1285
    const-string v2, "hangouts/location"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1286
    invoke-static {v0}, Lf;->n(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1289
    :cond_0
    return-object v0
.end method

.method public static b(Lyj;)V
    .locals 24

    .prologue
    .line 1402
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "babel_max_pending_message_animation"

    const-wide/32 v4, 0x1d4c0

    .line 1401
    invoke-static {v2, v3, v4, v5}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v12

    .line 1408
    invoke-static/range {p0 .. p0}, Lzo;->a(Lyj;)Lzo;

    move-result-object v2

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    const-string v6, "alert_status"

    const-string v7, "1"

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "alert_status=0 AND (status=3 OR (status!=4 AND timestamp+"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-wide/16 v7, 0x3e8

    mul-long/2addr v7, v12

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "<"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")) "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lzo;->d()Lzr;

    move-result-object v2

    const-string v5, "messages"

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v3, v4, v6}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_0

    invoke-static/range {p0 .. p0}, Lyp;->a(Lyj;)V

    .line 1410
    :cond_0
    sget-object v2, Lcom/google/android/apps/hangouts/content/EsProvider;->h:Landroid/net/Uri;

    move-object/from16 v0, p0

    invoke-static {v2, v0}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Landroid/net/Uri;Lyj;)Landroid/net/Uri;

    move-result-object v3

    .line 1413
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v14

    .line 1415
    invoke-static {}, Lzo;->a()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v4, Lbmt;->a:[Ljava/lang/String;

    const-string v5, "notified_for_failure!=1 AND conversation_pending_leave!=1 AND status!=4"

    const/4 v6, 0x0

    const-string v7, "timestamp ASC"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 1423
    :try_start_0
    const-string v2, "notification"

    .line 1424
    invoke-virtual {v14, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    .line 1425
    if-eqz v15, :cond_7

    .line 1427
    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    .line 1428
    new-instance v16, Ljava/util/HashSet;

    invoke-direct/range {v16 .. v16}, Ljava/util/HashSet;-><init>()V

    .line 1432
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1433
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 1435
    const-wide v7, 0x7fffffffffffffffL

    .line 1436
    const/4 v3, -0x1

    .line 1437
    const-wide/16 v5, 0x0

    .line 1439
    const/4 v10, -0x1

    invoke-interface {v15, v10}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-wide v10, v7

    .line 1440
    :cond_1
    :goto_0
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1441
    const/16 v7, 0x10

    invoke-interface {v15, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 1442
    const/16 v8, 0x8

    invoke-interface {v15, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 1443
    packed-switch v7, :pswitch_data_0

    goto :goto_0

    .line 1446
    :pswitch_0
    const/16 v7, 0xe

    .line 1447
    invoke-interface {v15, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    const-wide/16 v19, 0x3e8

    div-long v7, v7, v19

    .line 1449
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v19

    add-long v21, v7, v12

    sub-long v19, v19, v21

    .line 1453
    const-wide/16 v21, 0x0

    cmp-long v21, v19, v21

    if-lez v21, :cond_2

    .line 1454
    invoke-interface {v15}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1455
    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1456
    invoke-interface {v15}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    .line 1457
    cmp-long v18, v7, v5

    if-lez v18, :cond_1

    move-wide v5, v7

    .line 1458
    goto :goto_0

    .line 1463
    :cond_2
    move-wide/from16 v0, v19

    neg-long v7, v0

    invoke-static {v10, v11, v7, v8}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v7

    move-wide v10, v7

    .line 1465
    goto :goto_0

    .line 1467
    :pswitch_1
    invoke-interface {v15}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1468
    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1469
    invoke-interface {v15}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    goto :goto_0

    .line 1474
    :cond_3
    sget-boolean v7, Lbmf;->a:Z

    if-eqz v7, :cond_4

    .line 1475
    const-string v7, "Babel"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v12, "query for failed message sends: "

    invoke-direct {v8, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1476
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v12

    invoke-virtual {v8, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v12, " "

    invoke-virtual {v8, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 1478
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v12

    invoke-virtual {v8, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1475
    invoke-static {v7, v8}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1480
    :cond_4
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-gtz v7, :cond_5

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_d

    .line 1482
    :cond_5
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v7

    add-int v12, v4, v7

    .line 1483
    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    .line 1484
    new-instance v17, Lbk;

    move-object/from16 v0, v17

    invoke-direct {v0, v14}, Lbk;-><init>(Landroid/content/Context;)V

    .line 1487
    const/4 v4, 0x2

    const/4 v7, 0x7

    const/4 v8, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v4, v7, v8}, Lbzb;->a(Lyj;IILjava/lang/String;)I

    move-result v18

    .line 1492
    const/4 v4, 0x0

    .line 1493
    const/4 v7, 0x0

    .line 1494
    const/4 v8, 0x1

    if-ne v12, v8, :cond_9

    .line 1495
    invoke-interface {v15, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1496
    const/16 v3, 0x8

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1498
    invoke-static {v3}, Lbxz;->a(Ljava/lang/String;)Lbxz;

    move-result-object v7

    .line 1499
    const/16 v8, 0xb

    .line 1501
    invoke-interface {v15, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 1499
    move-object/from16 v0, p0

    invoke-static {v0, v3, v8}, Lbbl;->a(Lyj;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v3

    .line 1502
    const/high16 v8, 0x4000000

    invoke-virtual {v3, v8}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1503
    const-string v8, "reset_failed_notifications"

    const/4 v9, 0x1

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1505
    const/high16 v8, 0x10000000

    move/from16 v0, v18

    invoke-static {v14, v0, v3, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lbk;->a(Landroid/app/PendingIntent;)Lbk;

    .line 1510
    const/4 v3, 0x4

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1512
    sget v3, Lh;->hL:I

    invoke-virtual {v13, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 1515
    const/16 v3, 0x12

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 1516
    const/16 v12, 0x18

    invoke-interface {v15, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 1519
    invoke-static {v3}, Lf;->b(I)Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x2

    if-ne v12, v3, :cond_e

    .line 1521
    :cond_6
    const/4 v3, 0x1

    :goto_1
    move-object v4, v9

    move/from16 v23, v3

    move-object v3, v7

    move/from16 v7, v23

    .line 1547
    :goto_2
    const/4 v9, 0x4

    move-object/from16 v0, p0

    invoke-static {v0, v9, v3}, Lbbl;->a(Lyj;ILbxz;)Landroid/content/Intent;

    move-result-object v3

    .line 1549
    add-int/lit8 v9, v18, 0x1

    const/high16 v12, 0x10000000

    invoke-static {v14, v9, v3, v12}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v9

    .line 1553
    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lbk;->a(Ljava/lang/CharSequence;)Lbk;

    move-result-object v12

    const-wide/16 v3, 0x0

    cmp-long v3, v5, v3

    if-lez v3, :cond_a

    move-wide v3, v5

    .line 1554
    :goto_3
    invoke-virtual {v12, v3, v4}, Lbk;->a(J)Lbk;

    move-result-object v3

    sget v4, Lcom/google/android/apps/hangouts/R$drawable;->cI:I

    .line 1555
    invoke-virtual {v3, v4}, Lbk;->a(I)Lbk;

    move-result-object v3

    .line 1556
    invoke-virtual {v3, v9}, Lbk;->b(Landroid/app/PendingIntent;)Lbk;

    .line 1557
    if-eqz v7, :cond_b

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_b

    .line 1558
    new-instance v3, Lbm;

    move-object/from16 v0, v17

    invoke-direct {v3, v0}, Lbm;-><init>(Lbk;)V

    .line 1560
    invoke-static {v8}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v3, v4}, Lbm;->b(Ljava/lang/CharSequence;)Lbm;

    .line 1561
    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lbk;->a(Lbu;)Lbk;

    .line 1566
    :goto_4
    const/4 v3, 0x7

    .line 1567
    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lbne;->c(Lyj;I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x7

    .line 1570
    invoke-virtual/range {v17 .. v17}, Lbk;->e()Landroid/app/Notification;

    move-result-object v5

    .line 1566
    invoke-virtual {v2, v3, v4, v5}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 1580
    :goto_5
    const-wide v2, 0x7fffffffffffffffL

    cmp-long v2, v10, v2

    if-eqz v2, :cond_7

    .line 1581
    const/4 v2, 0x4

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v10, v11, v2, v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;JIZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1587
    :cond_7
    if-eqz v15, :cond_8

    .line 1588
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 1591
    :cond_8
    return-void

    .line 1524
    :cond_9
    :try_start_1
    invoke-static/range {p0 .. p0}, Lbbl;->c(Lyj;)Landroid/content/Intent;

    move-result-object v3

    .line 1526
    const/high16 v8, 0x14000000

    invoke-virtual {v3, v8}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1528
    const-string v8, "reset_failed_notifications"

    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v3, v8, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1529
    const/high16 v8, 0x10000000

    move/from16 v0, v18

    invoke-static {v14, v0, v3, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lbk;->a(Landroid/app/PendingIntent;)Lbk;

    .line 1535
    sget v3, Lh;->hK:I

    invoke-virtual {v13, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1537
    sget v3, Lf;->hv:I

    .line 1539
    invoke-virtual/range {v16 .. v16}, Ljava/util/HashSet;->size()I

    move-result v19

    .line 1540
    invoke-virtual {v9}, Ljava/util/HashSet;->size()I

    move-result v20

    add-int v19, v19, v20

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    .line 1541
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v20, v21

    const/4 v12, 0x1

    .line 1542
    invoke-virtual/range {v16 .. v16}, Ljava/util/HashSet;->size()I

    move-result v16

    .line 1543
    invoke-virtual {v9}, Ljava/util/HashSet;->size()I

    move-result v9

    add-int v9, v9, v16

    .line 1542
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v20, v12

    .line 1537
    move/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v13, v3, v0, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v23, v7

    move v7, v4

    move-object v4, v8

    move-object v8, v3

    move-object/from16 v3, v23

    goto/16 :goto_2

    .line 1554
    :cond_a
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    goto/16 :goto_3

    .line 1563
    :cond_b
    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Lbk;->b(Ljava/lang/CharSequence;)Lbk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_4

    .line 1587
    :catchall_0
    move-exception v2

    if-eqz v15, :cond_c

    .line 1588
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    :cond_c
    throw v2

    .line 1573
    :cond_d
    const/4 v3, 0x7

    .line 1574
    :try_start_2
    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lbne;->c(Lyj;I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x7

    .line 1573
    invoke-virtual {v2, v3, v4}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_5

    :cond_e
    move v3, v4

    goto/16 :goto_1

    .line 1443
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static c(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 1293
    invoke-static {p0}, Lf;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1294
    const/4 v0, 0x1

    .line 1313
    :goto_0
    return v0

    .line 1297
    :cond_0
    invoke-static {p0}, Lf;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1298
    const/4 v0, 0x2

    goto :goto_0

    .line 1301
    :cond_1
    invoke-static {p0}, Lf;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1302
    const/4 v0, 0x3

    goto :goto_0

    .line 1305
    :cond_2
    const-string v0, "hangouts/location"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1306
    const/4 v0, 0x4

    goto :goto_0

    .line 1309
    :cond_3
    invoke-static {p0}, Lf;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1310
    const/4 v0, 0x6

    goto :goto_0

    .line 1313
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lyj;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/16 v7, 0xa

    const/4 v6, 0x1

    .line 1597
    sget-boolean v0, Lbmf;->a:Z

    if-eqz v0, :cond_0

    .line 1598
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[MessageNotifState.notifySignInFailed] account="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1599
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1598
    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1602
    :cond_0
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    .line 1604
    const-string v0, "notification"

    .line 1605
    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 1606
    new-instance v2, Lbk;

    invoke-direct {v2, v1}, Lbk;-><init>(Landroid/content/Context;)V

    .line 1609
    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {p0, v3, v7, v4}, Lbzb;->a(Lyj;IILjava/lang/String;)I

    move-result v3

    .line 1614
    invoke-static {p0}, Lbbl;->b(Lyj;)Landroid/content/Intent;

    move-result-object v4

    .line 1615
    const/high16 v5, 0x14000000

    invoke-virtual {v4, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1617
    const-string v5, "reset_signin_failed_notifications"

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1618
    const/high16 v5, 0x10000000

    invoke-static {v1, v3, v4, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {v2, v3}, Lbk;->a(Landroid/app/PendingIntent;)Lbk;

    .line 1624
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1625
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v3

    .line 1626
    sget v4, Lh;->hN:I

    new-array v5, v6, [Ljava/lang/Object;

    aput-object v3, v5, v8

    invoke-virtual {v1, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1628
    sget v5, Lh;->hO:I

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v3, v6, v8

    invoke-virtual {v1, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1631
    invoke-virtual {v2, v4}, Lbk;->a(Ljava/lang/CharSequence;)Lbk;

    move-result-object v3

    .line 1632
    invoke-virtual {v3, v1}, Lbk;->b(Ljava/lang/CharSequence;)Lbk;

    move-result-object v1

    .line 1633
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, Lbk;->a(J)Lbk;

    move-result-object v1

    sget v3, Lcom/google/android/apps/hangouts/R$drawable;->cI:I

    .line 1634
    invoke-virtual {v1, v3}, Lbk;->a(I)Lbk;

    .line 1637
    invoke-static {p0, v7}, Lbne;->c(Lyj;I)Ljava/lang/String;

    move-result-object v1

    .line 1640
    invoke-virtual {v2}, Lbk;->e()Landroid/app/Notification;

    move-result-object v2

    .line 1636
    invoke-virtual {v0, v1, v7, v2}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 1642
    return-void
.end method

.method private static d(Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1358
    invoke-static {p0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    .line 1359
    instance-of v0, v1, Landroid/text/Spannable;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 1360
    check-cast v0, Landroid/text/Spannable;

    invoke-interface {v0}, Landroid/text/Spannable;->length()I

    move-result v2

    const-class v4, Landroid/text/style/URLSpan;

    invoke-interface {v0, v3, v2, v4}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Landroid/text/style/URLSpan;

    array-length v4, v2

    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v5, v2, v3

    invoke-interface {v0, v5}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1362
    :cond_0
    return-object v1
.end method

.method public static d(Lyj;)V
    .locals 4

    .prologue
    const/16 v3, 0xa

    .line 1648
    sget-boolean v0, Lbmf;->a:Z

    if-eqz v0, :cond_0

    .line 1649
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "### [MessageNotifState.dismissSignInFailedNotification] account="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1650
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1649
    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1654
    :cond_0
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 1656
    invoke-static {p0, v3}, Lbne;->c(Lyj;I)Ljava/lang/String;

    move-result-object v1

    .line 1655
    invoke-virtual {v0, v1, v3}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 1660
    return-void
.end method

.method private static e(Lyj;)Lbmh;
    .locals 29

    .prologue
    .line 945
    const/4 v11, 0x0

    .line 946
    const/16 v18, 0x0

    .line 947
    const/4 v8, 0x0

    .line 948
    const/16 v17, 0x0

    .line 950
    :try_start_0
    const-string v7, "sort_timestamp DESC"

    invoke-virtual/range {p0 .. p0}, Lyj;->u()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_b

    const/16 v16, 0x0

    .line 951
    :goto_0
    if-eqz v16, :cond_2b

    :try_start_1
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2b

    .line 952
    sget-boolean v2, Lbmf;->a:Z

    if-eqz v2, :cond_0

    .line 953
    const-string v2, "Babel"

    const-string v3, "Some unseen invite notifications"

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 959
    :cond_0
    const/16 v2, 0x8

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 961
    const/4 v2, 0x7

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 963
    if-eqz v3, :cond_3

    if-eqz v4, :cond_3

    .line 964
    new-instance v5, Lbdk;

    const/4 v2, 0x5

    .line 965
    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x6

    .line 966
    move-object/from16 v0, v16

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v2, v6}, Lbdk;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 967
    new-instance v2, Lbmk;

    const/4 v6, 0x2

    .line 968
    move-object/from16 v0, v16

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    .line 970
    move-object/from16 v0, v16

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    const/4 v8, 0x0

    .line 971
    move-object/from16 v0, v16

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v2 .. v8}, Lbmk;-><init>(Ljava/lang/String;Ljava/lang/String;Lbdk;Ljava/lang/String;ILjava/lang/String;)V

    .line 973
    if-nez v11, :cond_2

    .line 974
    const/4 v3, 0x2

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 976
    const/4 v4, 0x3

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 978
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    move-object v7, v3

    .line 981
    :cond_1
    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v3, v3

    add-int/lit8 v11, v3, -0x1

    .line 982
    const/4 v3, 0x0

    .line 983
    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 984
    new-instance v3, Lbmj;

    const/4 v5, 0x1

    .line 986
    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    const/4 v5, 0x4

    .line 988
    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    const/4 v10, 0x1

    const/4 v12, 0x0

    const/4 v13, 0x2

    const-wide/16 v14, 0x0

    move-object v5, v4

    invoke-direct/range {v3 .. v15}, Lbmj;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JZILjava/lang/String;IJ)V

    move-object v11, v3

    .line 996
    :cond_2
    iget-object v3, v11, Lbmj;->h:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 997
    iget-object v2, v11, Lbmj;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x7

    if-eq v2, v3, :cond_4

    .line 998
    :cond_3
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1002
    :cond_4
    if-eqz v11, :cond_2b

    .line 1003
    iget-object v2, v11, Lbmj;->h:Ljava/util/List;

    .line 1004
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    iput v2, v11, Lbmj;->i:I

    .line 1005
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 1006
    invoke-interface {v10, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1007
    iget v2, v11, Lbmj;->i:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    add-int/lit8 v9, v2, 0x0

    .line 1011
    :goto_1
    if-eqz v16, :cond_5

    .line 1012
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 1016
    :cond_5
    const/4 v8, 0x0

    .line 1018
    :try_start_2
    const-string v7, "merge_key, timestamp DESC"

    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual/range {p0 .. p0}, Lyj;->c()Lbdk;

    move-result-object v3

    iget-object v3, v3, Lbdk;->b:Ljava/lang/String;

    aput-object v3, v6, v2

    sget-object v2, Lcom/google/android/apps/hangouts/content/EsProvider;->h:Landroid/net/Uri;

    move-object/from16 v0, p0

    invoke-static {v2, v0}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Landroid/net/Uri;Lyj;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {}, Lzo;->a()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v4, Lbmt;->a:[Ljava/lang/String;

    const-string v5, "conversation_id IN (SELECT conversation_id FROM conversations WHERE conversations.has_chat_notifications=1 OR conversations.wearable_watermark>0) AND (author_chat_id is NULL OR author_chat_id!=?) AND conversation_notification_level!=10 AND notification_level!=10 AND conversation_pending_leave!=1 AND type!=7 AND type!=8 AND conversation_status!=1 AND ((wearable_watermark>0 AND timestamp>wearable_watermark) OR (timestamp>chat_watermark)) AND timestamp > 1355097600000000"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v24

    .line 1019
    if-eqz v24, :cond_2a

    :try_start_3
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2a

    .line 1020
    sget-boolean v2, Lbmf;->a:Z

    if-eqz v2, :cond_6

    .line 1021
    const-string v2, "Babel"

    const-string v3, "Some unseen message notifications."

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1023
    :cond_6
    const/4 v7, 0x0

    .line 1024
    const/4 v4, 0x0

    .line 1025
    const/4 v3, 0x0

    .line 1026
    invoke-static {}, Lbne;->a()Z

    move-result v2

    if-eqz v2, :cond_d

    const-string v2, "babel_max_messages_in_conversation_notification_with_wearable"

    const/4 v5, 0x1

    invoke-static {v2, v5}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v2

    move/from16 v23, v2

    :goto_2
    move-object/from16 v25, v3

    move-object/from16 v26, v4

    move v2, v9

    move-object v8, v10

    move-object v3, v11

    .line 1030
    :goto_3
    const/4 v4, 0x1

    move-object/from16 v0, v24

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    .line 1032
    const/4 v4, 0x0

    move-object/from16 v0, v24

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    .line 1034
    const/4 v4, 0x5

    move-object/from16 v0, v24

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 1035
    const/16 v4, 0x12

    move-object/from16 v0, v24

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 1037
    const/16 v4, 0x13

    move-object/from16 v0, v24

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 1040
    const/4 v4, 0x4

    move-object/from16 v0, v24

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1041
    invoke-static {v9}, Lf;->b(I)Z

    move-result v10

    .line 1043
    const/16 v4, 0x14

    .line 1044
    move-object/from16 v0, v24

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 1045
    const/4 v4, 0x2

    .line 1046
    move-object/from16 v0, v24

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 1047
    const/16 v4, 0x15

    .line 1048
    move-object/from16 v0, v24

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 1049
    const/16 v4, 0x16

    .line 1050
    move-object/from16 v0, v24

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 1051
    const/16 v4, 0x1c

    .line 1052
    move-object/from16 v0, v24

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 1056
    const/4 v4, 0x2

    const/16 v11, 0x18

    .line 1057
    move-object/from16 v0, v24

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    if-ne v4, v11, :cond_e

    const/4 v4, 0x1

    .line 1058
    :goto_4
    if-eqz v10, :cond_f

    if-nez v4, :cond_f

    .line 1061
    :goto_5
    invoke-static/range {v24 .. v24}, Lbmf;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v14

    .line 1064
    if-eqz v27, :cond_7

    if-eqz v28, :cond_7

    if-eqz v6, :cond_7

    if-nez v14, :cond_8

    .line 1066
    :cond_7
    invoke-static {v9}, Lf;->c(I)Z

    move-result v4

    if-eqz v4, :cond_1c

    .line 1067
    :cond_8
    const/16 v4, 0x8

    move-object/from16 v0, v24

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 1069
    const/16 v4, 0x17

    move-object/from16 v0, v24

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1072
    if-eqz v7, :cond_9

    iget-object v4, v7, Lbmj;->b:Ljava/lang/String;

    .line 1073
    invoke-static {v12, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_26

    .line 1074
    :cond_9
    const-string v4, "Babel"

    const/4 v10, 0x3

    invoke-static {v4, v10}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1075
    const-string v10, "Babel"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v4, "[Notifications] conversationId: "

    invoke-direct {v13, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez v7, :cond_11

    const-string v4, "null"

    :goto_6
    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, " newConvId: "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v10, v4}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1079
    :cond_a
    const/16 v4, 0x19

    move-object/from16 v0, v24

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v15

    .line 1081
    const/16 v4, 0x1d

    move-object/from16 v0, v24

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v21

    .line 1083
    const-wide/16 v17, 0x0

    cmp-long v4, v21, v17

    if-lez v4, :cond_12

    .line 1086
    :goto_7
    new-instance v10, Lbmj;

    const/16 v4, 0xb

    .line 1087
    move-object/from16 v0, v24

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    const/16 v4, 0xe

    .line 1089
    move-object/from16 v0, v24

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v15

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v4, 0x1a

    .line 1090
    move-object/from16 v0, v24

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    const/16 v4, 0x1b

    .line 1091
    move-object/from16 v0, v24

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    invoke-direct/range {v10 .. v22}, Lbmj;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JZILjava/lang/String;IJ)V

    .line 1093
    if-nez v8, :cond_25

    .line 1094
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1098
    :goto_8
    if-eqz v3, :cond_24

    .line 1099
    iget-wide v12, v3, Lbmj;->g:J

    iget-wide v15, v10, Lbmj;->g:J

    cmp-long v4, v12, v15

    if-lez v4, :cond_24

    .line 1100
    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1101
    iget v3, v3, Lbmj;->i:I

    add-int/2addr v2, v3

    .line 1102
    const/4 v3, 0x0

    move-object v4, v3

    move v3, v2

    .line 1108
    :goto_9
    const/4 v2, 0x0

    move v8, v2

    .line 1109
    :goto_a
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v2

    if-ge v8, v2, :cond_13

    iget-wide v12, v10, Lbmj;->g:J

    .line 1111
    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbmj;

    iget-wide v15, v2, Lbmj;->g:J
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    cmp-long v2, v12, v15

    if-gez v2, :cond_13

    .line 1112
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    goto :goto_a

    .line 950
    :cond_b
    const/4 v2, 0x1

    :try_start_4
    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual/range {p0 .. p0}, Lyj;->c()Lbdk;

    move-result-object v3

    iget-object v3, v3, Lbdk;->b:Ljava/lang/String;

    aput-object v3, v6, v2

    sget-object v2, Lcom/google/android/apps/hangouts/content/EsProvider;->f:Landroid/net/Uri;

    move-object/from16 v0, p0

    invoke-static {v2, v0}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Landroid/net/Uri;Lyj;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {}, Lzo;->a()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v4, Lbms;->a:[Ljava/lang/String;

    const-string v5, "inviter_chat_id!=? AND notification_level!=10 AND is_pending_leave!=1 AND sort_timestamp>chat_watermark AND inviter_first_name IS NOT NULL"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v16

    goto/16 :goto_0

    .line 1011
    :catchall_0
    move-exception v2

    move-object v3, v8

    :goto_b
    if-eqz v3, :cond_c

    .line 1012
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_c
    throw v2

    .line 1026
    :cond_d
    :try_start_5
    const-string v2, "babel_max_messages_in_conversation_notification"

    const/4 v5, 0x7

    invoke-static {v2, v5}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v2

    move/from16 v23, v2

    goto/16 :goto_2

    .line 1057
    :cond_e
    const/4 v4, 0x0

    goto/16 :goto_4

    .line 1059
    :cond_f
    if-eqz v6, :cond_10

    packed-switch v5, :pswitch_data_0

    :pswitch_0
    const/4 v12, 0x4

    const/16 v18, 0x1

    move v10, v5

    move-object/from16 v11, p0

    invoke-static/range {v10 .. v18}, Lf;->a(ILyj;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v6

    :goto_c
    :pswitch_1
    if-eqz v6, :cond_10

    invoke-static {v6}, Lbmf;->d(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v6

    goto/16 :goto_5

    :pswitch_2
    const-string v4, "Babel"

    const-string v6, "Message with depracated membership change type"

    invoke-static {v4, v6}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v6, 0x0

    goto :goto_c

    :cond_10
    const/4 v6, 0x0

    goto/16 :goto_5

    .line 1075
    :cond_11
    iget-object v4, v7, Lbmj;->a:Ljava/lang/String;

    goto/16 :goto_6

    :cond_12
    move-wide/from16 v21, v15

    .line 1083
    goto/16 :goto_7

    .line 1114
    :cond_13
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v2

    if-ge v8, v2, :cond_17

    .line 1115
    invoke-interface {v7, v8, v10}, Ljava/util/List;->add(ILjava/lang/Object;)V

    move v12, v3

    move-object v13, v7

    move-object v15, v4

    .line 1121
    :goto_d
    iget v2, v10, Lbmj;->i:I

    move/from16 v0, v23

    if-ge v2, v0, :cond_15

    .line 1122
    invoke-static/range {v24 .. v24}, Lbmf;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v7

    .line 1123
    const/16 v2, 0x11

    move-object/from16 v0, v24

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbmf;->c(Ljava/lang/String;)I

    move-result v8

    .line 1126
    if-eqz v7, :cond_14

    sget-object v2, Lbvx;->a:Ljava/lang/String;

    .line 1127
    invoke-virtual {v7, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 1132
    invoke-static {v7}, Lbmf;->e(Ljava/lang/String;)Lbvy;

    move-result-object v2

    .line 1133
    if-eqz v2, :cond_18

    .line 1134
    iget-object v7, v2, Lbvy;->a:Ljava/lang/String;

    .line 1135
    iget-object v2, v2, Lbvy;->b:Ljava/lang/String;

    invoke-static {v2}, Lbmf;->c(Ljava/lang/String;)I

    move-result v8

    .line 1142
    :cond_14
    :goto_e
    iget-boolean v2, v10, Lbmj;->c:Z

    if-eqz v2, :cond_19

    const/4 v2, 0x3

    if-ne v9, v2, :cond_19

    .line 1144
    if-nez v28, :cond_23

    move-object/from16 v4, v27

    move-object/from16 v3, v27

    .line 1168
    :goto_f
    iget-object v11, v10, Lbmj;->h:Ljava/util/List;

    new-instance v2, Lbml;

    iget-boolean v14, v10, Lbmj;->c:Z

    invoke-direct/range {v2 .. v9}, Lbml;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/CharSequence;Ljava/lang/String;II)V

    invoke-interface {v11, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_15
    move-object/from16 v4, v25

    move-object/from16 v5, v26

    .line 1172
    add-int/lit8 v2, v12, 0x1

    .line 1173
    iget v3, v10, Lbmj;->i:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v10, Lbmj;->i:I

    move-object v7, v10

    move-object v3, v13

    move-object v6, v15

    .line 1182
    :goto_10
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move-result v8

    if-nez v8, :cond_27

    move-object v4, v3

    move v3, v2

    .line 1185
    :goto_11
    if-eqz v24, :cond_16

    .line 1186
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    .line 1189
    :cond_16
    if-nez v4, :cond_22

    .line 1190
    const/4 v2, 0x0

    .line 1192
    :goto_12
    return-object v2

    .line 1117
    :cond_17
    :try_start_6
    invoke-interface {v7, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v12, v3

    move-object v13, v7

    move-object v15, v4

    goto :goto_d

    .line 1137
    :cond_18
    const/4 v7, 0x0

    .line 1138
    const/4 v8, 0x0

    goto :goto_e

    .line 1152
    :cond_19
    move-object/from16 v0, v25

    invoke-static {v0, v11}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1a

    .line 1153
    move-object/from16 v0, p0

    invoke-static {v0, v11}, Lbmf;->a(Lyj;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v26

    move-object/from16 v25, v11

    .line 1157
    :cond_1a
    move-object/from16 v0, v26

    move-object/from16 v1, v28

    invoke-static {v0, v1}, Lbmf;->a(Ljava/util/HashMap;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_29

    move-object/from16 v4, v27

    .line 1161
    :goto_13
    if-nez v27, :cond_1b

    move-object/from16 v27, v14

    .line 1164
    :cond_1b
    if-nez v4, :cond_28

    move-object v4, v14

    move-object/from16 v3, v27

    .line 1165
    goto :goto_f

    .line 1175
    :cond_1c
    const-string v5, "Babel"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v4, "Skipping notification because"

    invoke-direct {v9, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez v27, :cond_1d

    const-string v4, " null authorFullName "

    :goto_14
    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    if-nez v28, :cond_1e

    const-string v4, " null authorFirstName "

    :goto_15
    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    if-nez v6, :cond_1f

    const-string v4, " null text "

    :goto_16
    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-nez v14, :cond_20

    const-string v4, " null groupConversationName "

    :goto_17
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v4, v25

    move-object/from16 v5, v26

    move-object v6, v3

    move-object v3, v8

    goto :goto_10

    :cond_1d
    const-string v4, ""

    goto :goto_14

    :cond_1e
    const-string v4, ""

    goto :goto_15

    :cond_1f
    const-string v4, ""

    goto :goto_16

    :cond_20
    const-string v4, ""
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    goto :goto_17

    .line 1185
    :catchall_1
    move-exception v2

    move-object v3, v8

    :goto_18
    if-eqz v3, :cond_21

    .line 1186
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_21
    throw v2

    .line 1192
    :cond_22
    new-instance v2, Lbmh;

    invoke-direct {v2, v3, v4}, Lbmh;-><init>(ILjava/util/List;)V

    goto/16 :goto_12

    .line 1185
    :catchall_2
    move-exception v2

    move-object/from16 v3, v24

    goto :goto_18

    .line 1011
    :catchall_3
    move-exception v2

    move-object/from16 v3, v16

    goto/16 :goto_b

    :cond_23
    move-object/from16 v4, v28

    move-object/from16 v3, v27

    goto/16 :goto_f

    :cond_24
    move-object v4, v3

    move v3, v2

    goto/16 :goto_9

    :cond_25
    move-object v7, v8

    goto/16 :goto_8

    :cond_26
    move-object v10, v7

    move v12, v2

    move-object v13, v8

    move-object v15, v3

    goto/16 :goto_d

    :cond_27
    move-object/from16 v25, v4

    move-object/from16 v26, v5

    move-object v8, v3

    move-object v3, v6

    goto/16 :goto_3

    :cond_28
    move-object/from16 v3, v27

    goto/16 :goto_f

    :cond_29
    move-object/from16 v4, v28

    goto/16 :goto_13

    :cond_2a
    move v3, v9

    move-object v4, v10

    goto/16 :goto_11

    :cond_2b
    move/from16 v9, v17

    move-object/from16 v10, v18

    goto/16 :goto_1

    .line 1059
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static e(Ljava/lang/String;)Lbvy;
    .locals 3

    .prologue
    .line 1683
    invoke-static {p0}, Lbvx;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1684
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvy;

    .line 1685
    iget-object v2, v0, Lbvy;->b:Ljava/lang/String;

    invoke-static {v2}, Lf;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1689
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private r()I
    .locals 2

    .prologue
    .line 339
    iget v0, p0, Lbmf;->t:I

    invoke-super {p0}, Lbmr;->h()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public a()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 352
    iget-object v0, p0, Lbmf;->m:Lyj;

    const/4 v1, 0x1

    iget-object v2, p0, Lbmf;->n:Lbxz;

    invoke-static {v0, v1, v2}, Lbbl;->a(Lyj;ILbxz;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public b()I
    .locals 2

    .prologue
    .line 120
    const-string v0, "babel_notify_chat_priority_level"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1253
    iget-object v1, p0, Lbmf;->j:Lbmh;

    iget-object v1, v1, Lbmh;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 1254
    iget-object v1, p0, Lbmf;->j:Lbmh;

    iget-object v1, v1, Lbmh;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmj;

    .line 1255
    invoke-virtual {v0}, Lbmj;->b()Z

    move-result v0

    .line 1257
    :cond_0
    return v0
.end method

.method protected d()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 1277
    iget-object v0, p0, Lbmf;->f:Landroid/content/Intent;

    return-object v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 115
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->cI:I

    return v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lbmf;->g:Ljava/lang/String;

    return-object v0
.end method

.method protected g()I
    .locals 1

    .prologue
    .line 110
    iget v0, p0, Lbmf;->h:I

    return v0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 335
    invoke-super {p0}, Lbmr;->h()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 343
    invoke-direct {p0}, Lbmf;->r()I

    move-result v0

    return v0
.end method

.method public j()I
    .locals 1

    .prologue
    .line 347
    invoke-direct {p0}, Lbmf;->r()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method protected k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1236
    iget-object v0, p0, Lbmf;->d:Ljava/lang/String;

    return-object v0
.end method

.method public l()I
    .locals 2

    .prologue
    .line 1244
    iget-object v0, p0, Lbmf;->j:Lbmh;

    iget-object v0, v0, Lbmh;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1245
    iget-object v0, p0, Lbmf;->j:Lbmh;

    iget-object v0, v0, Lbmh;->b:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmj;

    .line 1246
    invoke-virtual {v0}, Lbmj;->a()I

    move-result v0

    .line 1248
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public m()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1262
    iget-object v0, p0, Lbmf;->j:Lbmh;

    iget-object v0, v0, Lbmh;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1263
    iget-object v0, p0, Lbmf;->j:Lbmh;

    iget-object v0, v0, Lbmh;->b:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmj;

    iget-object v0, v0, Lbmj;->k:Ljava/lang/String;

    .line 1265
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected n()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 1269
    iget-object v0, p0, Lbmf;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbmf;->b:Ljava/lang/String;

    :goto_0
    iget-object v1, p0, Lbmf;->c:Ljava/lang/CharSequence;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lbmf;->c:Ljava/lang/CharSequence;

    :goto_1
    invoke-static {v0, v1}, Lbne;->a(Ljava/lang/String;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lbmf;->d:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-boolean v1, p0, Lbmf;->i:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lbmf;->e:Ljava/lang/CharSequence;

    goto :goto_1
.end method
