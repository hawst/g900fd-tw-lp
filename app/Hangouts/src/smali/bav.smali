.class abstract Lbav;
.super Lbat;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbat",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final d:Ljava/lang/CharSequence;

.field final e:Z

.field f:I


# direct methods
.method protected constructor <init>(Lbap;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 460
    invoke-direct {p0, v0}, Lbat;-><init>(B)V

    .line 458
    iput v0, p0, Lbav;->f:I

    .line 461
    iget-boolean v0, p1, Lbap;->a:Z

    iput-boolean v0, p0, Lbav;->e:Z

    .line 462
    iput-object p2, p0, Lbav;->d:Ljava/lang/CharSequence;

    .line 463
    return-void
.end method


# virtual methods
.method abstract a(I)I
.end method

.method protected synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 450
    invoke-virtual {p0}, Lbav;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method abstract b(I)I
.end method

.method protected c()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 467
    :cond_0
    iget v0, p0, Lbav;->f:I

    if-eq v0, v3, :cond_3

    .line 468
    iget v1, p0, Lbav;->f:I

    .line 471
    iget v0, p0, Lbav;->f:I

    invoke-virtual {p0, v0}, Lbav;->a(I)I

    move-result v0

    .line 472
    if-ne v0, v3, :cond_2

    .line 473
    iget-object v0, p0, Lbav;->d:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 474
    iput v3, p0, Lbav;->f:I

    .line 480
    :goto_0
    iget-boolean v2, p0, Lbav;->e:Z

    if-eqz v2, :cond_1

    if-eq v1, v0, :cond_0

    .line 481
    :cond_1
    iget-object v2, p0, Lbav;->d:Ljava/lang/CharSequence;

    invoke-interface {v2, v1, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 486
    :goto_1
    return-object v0

    .line 477
    :cond_2
    invoke-virtual {p0, v0}, Lbav;->b(I)I

    move-result v2

    iput v2, p0, Lbav;->f:I

    goto :goto_0

    .line 486
    :cond_3
    sget v0, Lbau;->c:I

    iput v0, p0, Lbat;->b:I

    const/4 v0, 0x0

    goto :goto_1
.end method
