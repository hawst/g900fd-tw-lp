.class public final Lacx;
.super Lacy;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field a:Landroid/view/View;

.field b:Landroid/view/View;

.field final synthetic c:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 317
    iput-object p1, p0, Lacx;->c:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;

    .line 318
    sget v0, Lf;->eo:I

    sget v1, Lg;->p:I

    invoke-direct {p0, p1, p2, v0, v1}, Lacy;-><init>(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;Landroid/view/ViewGroup;II)V

    .line 320
    iget-object v0, p0, Lacx;->d:Landroid/view/View;

    sget v1, Lg;->Y:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lacx;->a:Landroid/view/View;

    .line 321
    iget-object v0, p0, Lacx;->a:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 323
    iget-object v0, p0, Lacx;->d:Landroid/view/View;

    sget v1, Lg;->X:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lacx;->b:Landroid/view/View;

    .line 324
    iget-object v0, p0, Lacx;->b:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 325
    return-void
.end method


# virtual methods
.method a()Z
    .locals 1

    .prologue
    .line 346
    invoke-static {}, Lavy;->b()Z

    move-result v0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 329
    iget-object v0, p0, Lacx;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 330
    iget-object v0, p0, Lacx;->a:Landroid/view/View;

    if-ne p1, v0, :cond_2

    .line 331
    invoke-static {}, Lavy;->c()V

    .line 340
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lacx;->b()V

    .line 342
    :cond_1
    return-void

    .line 333
    :cond_2
    iget-object v0, p0, Lacx;->b:Landroid/view/View;

    if-ne p1, v0, :cond_1

    .line 334
    invoke-static {}, Lavy;->d()V

    .line 335
    iget-object v0, p0, Lacx;->c:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->f(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 337
    iget-object v0, p0, Lacx;->c:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbbl;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 338
    const-string v0, "Babel"

    const-string v1, "Cannot start Play store"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
