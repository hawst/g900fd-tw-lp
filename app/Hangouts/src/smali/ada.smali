.class public final Lada;
.super Lacy;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field a:Landroid/view/View;

.field b:Landroid/view/View;

.field final synthetic c:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 359
    iput-object p1, p0, Lada;->c:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;

    .line 360
    sget v0, Lf;->ep:I

    sget v1, Lg;->gl:I

    invoke-direct {p0, p1, p2, v0, v1}, Lacy;-><init>(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;Landroid/view/ViewGroup;II)V

    .line 362
    iget-object v0, p0, Lada;->d:Landroid/view/View;

    sget v1, Lg;->gI:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lada;->a:Landroid/view/View;

    .line 363
    iget-object v0, p0, Lada;->a:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 365
    iget-object v0, p0, Lada;->d:Landroid/view/View;

    sget v1, Lg;->e:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lada;->b:Landroid/view/View;

    .line 366
    iget-object v0, p0, Lada;->b:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 367
    return-void
.end method

.method private a(Z)V
    .locals 1

    .prologue
    .line 395
    iget-object v0, p0, Lada;->c:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->e(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;)Lyj;

    move-result-object v0

    invoke-static {v0, p1}, Lym;->a(Lyj;Z)V

    .line 396
    invoke-virtual {p0}, Lada;->b()V

    .line 397
    return-void
.end method


# virtual methods
.method a()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 387
    const-string v1, "babel_richstatus"

    invoke-static {v1, v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v1

    .line 390
    if-eqz v1, :cond_0

    iget-object v1, p0, Lada;->c:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->e(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;)Lyj;

    move-result-object v1

    invoke-virtual {v1}, Lyj;->u()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lada;->c:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;

    .line 391
    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->e(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;)Lyj;

    move-result-object v1

    invoke-virtual {v1}, Lyj;->D()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 371
    iget-object v0, p0, Lada;->a:Landroid/view/View;

    if-ne p1, v0, :cond_1

    .line 372
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lada;->a(Z)V

    .line 374
    iget-object v0, p0, Lada;->c:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;

    .line 375
    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->e(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;)Lyj;

    move-result-object v0

    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    .line 374
    invoke-static {v0}, Lbbl;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 376
    const-string v1, "settings_goto_rstatus"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 378
    const-string v1, "account_name"

    iget-object v2, p0, Lada;->c:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;

    invoke-static {v2}, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->e(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;)Lyj;

    move-result-object v2

    invoke-virtual {v2}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 379
    iget-object v1, p0, Lada;->c:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 383
    :cond_0
    :goto_0
    return-void

    .line 380
    :cond_1
    iget-object v0, p0, Lada;->b:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 381
    invoke-direct {p0, v2}, Lada;->a(Z)V

    goto :goto_0
.end method
