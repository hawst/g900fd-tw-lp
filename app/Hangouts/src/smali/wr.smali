.class public final Lwr;
.super Lww;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 10
    invoke-direct {p0}, Lww;-><init>()V

    .line 11
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    new-instance v0, Landroid/animation/ObjectAnimator;

    invoke-direct {v0}, Landroid/animation/ObjectAnimator;-><init>()V

    invoke-virtual {p0, v0}, Lwr;->a(Ljava/lang/Object;)V

    .line 12
    :goto_0
    return-void

    .line 11
    :cond_0
    new-instance v0, Lxg;

    invoke-direct {v0}, Lxg;-><init>()V

    invoke-virtual {p0, v0}, Lwr;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private constructor <init>(B)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lww;-><init>()V

    .line 22
    return-void
.end method

.method public static varargs a(Ljava/lang/Object;Ljava/lang/String;[F)Lwr;
    .locals 3

    .prologue
    .line 88
    new-instance v0, Lwr;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lwr;-><init>(B)V

    .line 89
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_0

    .line 90
    invoke-static {p0, p1, p2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v0, v1}, Lwr;->a(Ljava/lang/Object;)V

    .line 95
    :goto_0
    return-object v0

    .line 93
    :cond_0
    invoke-static {p0, p1, p2}, Lxg;->a(Ljava/lang/Object;Ljava/lang/String;[F)Lxg;

    move-result-object v1

    .line 92
    invoke-virtual {v0, v1}, Lwr;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static varargs a(Ljava/lang/Object;Ljava/lang/String;[I)Lwr;
    .locals 3

    .prologue
    .line 56
    new-instance v0, Lwr;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lwr;-><init>(B)V

    .line 57
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_0

    .line 58
    invoke-static {p0, p1, p2}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v0, v1}, Lwr;->a(Ljava/lang/Object;)V

    .line 63
    :goto_0
    return-object v0

    .line 61
    :cond_0
    invoke-static {p0, p1, p2}, Lxg;->a(Ljava/lang/Object;Ljava/lang/String;[I)Lxg;

    move-result-object v1

    .line 60
    invoke-virtual {v0, v1}, Lwr;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static varargs a(Ljava/lang/Object;[Lws;)Lwr;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 154
    new-instance v2, Lwr;

    invoke-direct {v2, v0}, Lwr;-><init>(B)V

    .line 155
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v1, v3, :cond_1

    .line 156
    array-length v1, p1

    new-array v3, v1, [Landroid/animation/PropertyValuesHolder;

    move v1, v0

    :goto_0
    array-length v0, p1

    if-ge v1, v0, :cond_0

    aget-object v0, p1, v1

    invoke-virtual {v0}, Lws;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/PropertyValuesHolder;

    aput-object v0, v3, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-static {p0, v3}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v2, v0}, Lwr;->a(Ljava/lang/Object;)V

    .line 169
    :goto_1
    return-object v2

    .line 158
    :cond_1
    array-length v1, p1

    new-array v3, v1, [Lxh;

    move v1, v0

    .line 161
    :goto_2
    array-length v0, p1

    if-ge v1, v0, :cond_2

    .line 162
    aget-object v0, p1, v1

    .line 164
    invoke-virtual {v0}, Lws;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxh;

    aput-object v0, v3, v1

    .line 161
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 167
    :cond_2
    invoke-static {p0, v3}, Lxg;->a(Ljava/lang/Object;[Lxh;)Lxg;

    move-result-object v0

    .line 166
    invoke-virtual {v2, v0}, Lwr;->a(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private j()Lwr;
    .locals 3

    .prologue
    .line 267
    new-instance v1, Lwr;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lwr;-><init>(B)V

    .line 268
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v0, v2, :cond_0

    .line 269
    invoke-virtual {p0}, Lwr;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->clone()Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v1, v0}, Lwr;->a(Ljava/lang/Object;)V

    .line 276
    :goto_0
    return-object v1

    .line 272
    :cond_0
    invoke-virtual {p0}, Lwr;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxg;

    .line 273
    invoke-virtual {v0}, Lxg;->g()Lxg;

    move-result-object v0

    .line 271
    invoke-virtual {v1, v0}, Lwr;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public varargs a([I)V
    .locals 2

    .prologue
    .line 187
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 188
    invoke-virtual {p0}, Lwr;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, p1}, Landroid/animation/ObjectAnimator;->setIntValues([I)V

    .line 193
    :goto_0
    return-void

    .line 190
    :cond_0
    invoke-virtual {p0}, Lwr;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxg;

    .line 191
    invoke-virtual {v0, p1}, Lxg;->a([I)V

    goto :goto_0
.end method

.method public synthetic b(J)Lwk;
    .locals 1

    .prologue
    .line 7
    invoke-virtual {p0, p1, p2}, Lwr;->c(J)Lwr;

    move-result-object v0

    return-object v0
.end method

.method public c(J)Lwr;
    .locals 0

    .prologue
    .line 242
    invoke-super {p0, p1, p2}, Lww;->d(J)Lww;

    .line 243
    return-object p0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Lwr;->j()Lwr;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d(J)Lww;
    .locals 1

    .prologue
    .line 7
    invoke-virtual {p0, p1, p2}, Lwr;->c(J)Lwr;

    move-result-object v0

    return-object v0
.end method

.method public synthetic g()Lwk;
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Lwr;->j()Lwr;

    move-result-object v0

    return-object v0
.end method
