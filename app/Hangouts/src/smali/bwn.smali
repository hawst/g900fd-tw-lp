.class final Lbwn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbwj;


# instance fields
.field private a:Landroid/database/Cursor;

.field private b:Landroid/database/Cursor;

.field private c:Lbvh;

.field private d:Lbvh;


# direct methods
.method public constructor <init>(JJ)V
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 312
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 313
    iput-object v0, p0, Lbwn;->a:Landroid/database/Cursor;

    .line 314
    iput-object v0, p0, Lbwn;->b:Landroid/database/Cursor;

    .line 316
    :try_start_0
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v7

    .line 323
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    .line 324
    invoke-static {}, Lbwf;->d()Landroid/net/Uri;

    move-result-object v9

    .line 326
    invoke-static {}, Lbvx;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lbvl;->a:[Ljava/lang/String;

    move-object v6, v0

    .line 329
    :goto_0
    invoke-static {}, Lbwf;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "date"

    move-wide v2, p1

    move-wide v4, p3

    .line 328
    invoke-static/range {v0 .. v5}, Lbwf;->a(Ljava/lang/String;Ljava/lang/String;JJ)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const-string v5, "date DESC"

    move-object v0, v8

    move-object v1, v9

    move-object v2, v6

    .line 321
    invoke-static/range {v0 .. v5}, Lf;->a(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lbwn;->a:Landroid/database/Cursor;

    .line 350
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 351
    invoke-static {}, Lbwf;->e()Landroid/net/Uri;

    move-result-object v7

    sget-object v8, Lbvj;->a:[Ljava/lang/String;

    .line 354
    invoke-static {}, Lbwf;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "date"

    const-wide/16 v2, 0x3e8

    div-long v2, p1, v2

    const-wide/16 v4, 0x3e7

    add-long/2addr v4, p3

    const-wide/16 v9, 0x3e8

    div-long/2addr v4, v9

    .line 353
    invoke-static/range {v0 .. v5}, Lbwf;->a(Ljava/lang/String;Ljava/lang/String;JJ)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const-string v5, "date DESC"

    move-object v0, v6

    move-object v1, v7

    move-object v2, v8

    .line 348
    invoke-static/range {v0 .. v5}, Lf;->a(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lbwn;->b:Landroid/database/Cursor;

    .line 364
    invoke-direct {p0}, Lbwn;->c()Lbvh;

    move-result-object v0

    iput-object v0, p0, Lbwn;->c:Lbvh;

    .line 365
    invoke-direct {p0}, Lbwn;->d()Lbvh;

    move-result-object v0

    iput-object v0, p0, Lbwn;->d:Lbvh;

    .line 372
    return-void

    .line 326
    :cond_0
    sget-object v0, Lbvl;->b:[Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v6, v0

    goto :goto_0

    .line 366
    :catch_0
    move-exception v0

    .line 367
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SmsSyncManager.queryMms: failed to query mms "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 371
    throw v0
.end method

.method private c()Lbvh;
    .locals 1

    .prologue
    .line 399
    iget-object v0, p0, Lbwn;->a:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbwn;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 400
    iget-object v0, p0, Lbwn;->a:Landroid/database/Cursor;

    invoke-static {v0}, Lbvl;->a(Landroid/database/Cursor;)Lbvl;

    move-result-object v0

    .line 402
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()Lbvh;
    .locals 1

    .prologue
    .line 406
    iget-object v0, p0, Lbwn;->b:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbwn;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 407
    iget-object v0, p0, Lbwn;->b:Landroid/database/Cursor;

    invoke-static {v0}, Lbvj;->a(Landroid/database/Cursor;)Lbvj;

    move-result-object v0

    .line 409
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Lbvh;
    .locals 4

    .prologue
    .line 377
    iget-object v0, p0, Lbwn;->c:Lbvh;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbwn;->d:Lbvh;

    if-eqz v0, :cond_1

    .line 379
    iget-object v0, p0, Lbwn;->c:Lbvh;

    invoke-virtual {v0}, Lbvh;->c()J

    move-result-wide v0

    iget-object v2, p0, Lbwn;->d:Lbvh;

    invoke-virtual {v2}, Lbvh;->c()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 380
    iget-object v0, p0, Lbwn;->c:Lbvh;

    .line 381
    invoke-direct {p0}, Lbwn;->c()Lbvh;

    move-result-object v1

    iput-object v1, p0, Lbwn;->c:Lbvh;

    .line 395
    :goto_0
    return-object v0

    .line 383
    :cond_0
    iget-object v0, p0, Lbwn;->d:Lbvh;

    .line 384
    invoke-direct {p0}, Lbwn;->d()Lbvh;

    move-result-object v1

    iput-object v1, p0, Lbwn;->d:Lbvh;

    goto :goto_0

    .line 387
    :cond_1
    iget-object v0, p0, Lbwn;->c:Lbvh;

    if-eqz v0, :cond_2

    .line 388
    iget-object v0, p0, Lbwn;->c:Lbvh;

    .line 389
    invoke-direct {p0}, Lbwn;->c()Lbvh;

    move-result-object v1

    iput-object v1, p0, Lbwn;->c:Lbvh;

    goto :goto_0

    .line 391
    :cond_2
    iget-object v0, p0, Lbwn;->d:Lbvh;

    .line 392
    invoke-direct {p0}, Lbwn;->d()Lbvh;

    move-result-object v1

    iput-object v1, p0, Lbwn;->d:Lbvh;

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 414
    iget-object v0, p0, Lbwn;->a:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 415
    iget-object v0, p0, Lbwn;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 416
    iput-object v1, p0, Lbwn;->a:Landroid/database/Cursor;

    .line 418
    :cond_0
    iget-object v0, p0, Lbwn;->b:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    .line 419
    iget-object v0, p0, Lbwn;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 420
    iput-object v1, p0, Lbwn;->b:Landroid/database/Cursor;

    .line 422
    :cond_1
    return-void
.end method
