.class public final Lafx;
.super Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/hangouts/video/SafeAsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field a:I

.field final synthetic b:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

.field private final c:Les;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Les",
            "<",
            "Ljava/lang/String;",
            "Lahf;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lyj;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V
    .locals 2

    .prologue
    .line 5119
    iput-object p1, p0, Lafx;->b:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;-><init>()V

    .line 5120
    const/4 v0, 0x0

    iput v0, p0, Lafx;->a:I

    .line 5124
    new-instance v0, Les;

    iget-object v1, p0, Lafx;->b:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 5125
    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Les;

    move-result-object v1

    invoke-direct {v0, v1}, Les;-><init>(Lfe;)V

    iput-object v0, p0, Lafx;->c:Les;

    .line 5126
    iget-object v0, p0, Lafx;->b:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lyj;

    move-result-object v0

    iput-object v0, p0, Lafx;->d:Lyj;

    .line 5127
    iget-object v0, p0, Lafx;->b:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->C(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lafx;->e:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected synthetic doInBackgroundTimed([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 5119
    new-instance v2, Lyt;

    iget-object v0, p0, Lafx;->d:Lyj;

    invoke-direct {v2, v0}, Lyt;-><init>(Lyj;)V

    iget-object v0, p0, Lafx;->c:Les;

    invoke-virtual {v0}, Les;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lyt;->e(Ljava/lang/String;)Lyv;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v2, v4}, Lyt;->a(Lyv;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "Babel"

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "possibly invalid merge detected: %s ==> %s (computed merge key %s)"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v9, v10

    const/4 v0, 0x2

    aput-object v5, v9, v0

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lyt;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, v4, Lyv;->c:I

    invoke-static {v0}, Lf;->d(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lafx;->e:Ljava/lang/String;

    invoke-static {v5, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lafx;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lafx;->a:I

    goto :goto_0

    :cond_1
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "counted "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lafx;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " non-GV, server-based, conversations"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 5119
    iget v0, p0, Lafx;->a:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "merged more than one hangouts conversation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method
