.class public final Ldhi;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldhi;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ldhl;

.field public d:Ldig;

.field public e:Ljava/lang/Integer;

.field public f:Ldhg;

.field public g:Ldhh;

.field public h:Ldhl;

.field public i:Ldig;

.field public j:[Ljava/lang/String;

.field public k:[Ldhl;

.field public l:[Ldig;

.field public m:Ljava/lang/Boolean;

.field public n:Ljava/lang/Boolean;

.field public o:Ljava/lang/Boolean;

.field public p:Ljava/lang/Integer;

.field public q:Ldhp;

.field public r:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3636
    const/4 v0, 0x0

    new-array v0, v0, [Ldhi;

    sput-object v0, Ldhi;->a:[Ldhi;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3637
    invoke-direct {p0}, Lepn;-><init>()V

    .line 3656
    iput-object v1, p0, Ldhi;->c:Ldhl;

    .line 3659
    iput-object v1, p0, Ldhi;->d:Ldig;

    .line 3662
    iput-object v1, p0, Ldhi;->e:Ljava/lang/Integer;

    .line 3665
    iput-object v1, p0, Ldhi;->f:Ldhg;

    .line 3668
    iput-object v1, p0, Ldhi;->g:Ldhh;

    .line 3671
    iput-object v1, p0, Ldhi;->h:Ldhl;

    .line 3674
    iput-object v1, p0, Ldhi;->i:Ldig;

    .line 3677
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Ldhi;->j:[Ljava/lang/String;

    .line 3680
    sget-object v0, Ldhl;->a:[Ldhl;

    iput-object v0, p0, Ldhi;->k:[Ldhl;

    .line 3683
    sget-object v0, Ldig;->a:[Ldig;

    iput-object v0, p0, Ldhi;->l:[Ldig;

    .line 3692
    iput-object v1, p0, Ldhi;->p:Ljava/lang/Integer;

    .line 3695
    iput-object v1, p0, Ldhi;->q:Ldhp;

    .line 3637
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 3767
    const/4 v0, 0x1

    iget-object v2, p0, Ldhi;->b:Ljava/lang/String;

    .line 3769
    invoke-static {v0, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3770
    iget-object v2, p0, Ldhi;->c:Ldhl;

    if-eqz v2, :cond_0

    .line 3771
    const/4 v2, 0x2

    iget-object v3, p0, Ldhi;->c:Ldhl;

    .line 3772
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3774
    :cond_0
    iget-object v2, p0, Ldhi;->e:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 3775
    const/4 v2, 0x3

    iget-object v3, p0, Ldhi;->e:Ljava/lang/Integer;

    .line 3776
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 3778
    :cond_1
    iget-object v2, p0, Ldhi;->f:Ldhg;

    if-eqz v2, :cond_2

    .line 3779
    const/4 v2, 0x4

    iget-object v3, p0, Ldhi;->f:Ldhg;

    .line 3780
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3782
    :cond_2
    iget-object v2, p0, Ldhi;->h:Ldhl;

    if-eqz v2, :cond_3

    .line 3783
    const/4 v2, 0x5

    iget-object v3, p0, Ldhi;->h:Ldhl;

    .line 3784
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3786
    :cond_3
    iget-object v2, p0, Ldhi;->j:[Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Ldhi;->j:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 3788
    iget-object v4, p0, Ldhi;->j:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_0
    if-ge v2, v5, :cond_4

    aget-object v6, v4, v2

    .line 3790
    invoke-static {v6}, Lepl;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 3788
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 3792
    :cond_4
    add-int/2addr v0, v3

    .line 3793
    iget-object v2, p0, Ldhi;->j:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 3795
    :cond_5
    iget-object v2, p0, Ldhi;->k:[Ldhl;

    if-eqz v2, :cond_7

    .line 3796
    iget-object v3, p0, Ldhi;->k:[Ldhl;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_7

    aget-object v5, v3, v2

    .line 3797
    if-eqz v5, :cond_6

    .line 3798
    const/4 v6, 0x7

    .line 3799
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 3796
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 3803
    :cond_7
    iget-object v2, p0, Ldhi;->m:Ljava/lang/Boolean;

    if-eqz v2, :cond_8

    .line 3804
    const/16 v2, 0x8

    iget-object v3, p0, Ldhi;->m:Ljava/lang/Boolean;

    .line 3805
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 3807
    :cond_8
    iget-object v2, p0, Ldhi;->n:Ljava/lang/Boolean;

    if-eqz v2, :cond_9

    .line 3808
    const/16 v2, 0x9

    iget-object v3, p0, Ldhi;->n:Ljava/lang/Boolean;

    .line 3809
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 3811
    :cond_9
    iget-object v2, p0, Ldhi;->p:Ljava/lang/Integer;

    if-eqz v2, :cond_a

    .line 3812
    const/16 v2, 0xa

    iget-object v3, p0, Ldhi;->p:Ljava/lang/Integer;

    .line 3813
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 3815
    :cond_a
    iget-object v2, p0, Ldhi;->q:Ldhp;

    if-eqz v2, :cond_b

    .line 3816
    const/16 v2, 0xb

    iget-object v3, p0, Ldhi;->q:Ldhp;

    .line 3817
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3819
    :cond_b
    iget-object v2, p0, Ldhi;->g:Ldhh;

    if-eqz v2, :cond_c

    .line 3820
    const/16 v2, 0xc

    iget-object v3, p0, Ldhi;->g:Ldhh;

    .line 3821
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3823
    :cond_c
    iget-object v2, p0, Ldhi;->o:Ljava/lang/Boolean;

    if-eqz v2, :cond_d

    .line 3824
    const/16 v2, 0xd

    iget-object v3, p0, Ldhi;->o:Ljava/lang/Boolean;

    .line 3825
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 3827
    :cond_d
    iget-object v2, p0, Ldhi;->r:Ljava/lang/String;

    if-eqz v2, :cond_e

    .line 3828
    const/16 v2, 0xe

    iget-object v3, p0, Ldhi;->r:Ljava/lang/String;

    .line 3829
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3831
    :cond_e
    iget-object v2, p0, Ldhi;->d:Ldig;

    if-eqz v2, :cond_f

    .line 3832
    const/16 v2, 0xf

    iget-object v3, p0, Ldhi;->d:Ldig;

    .line 3833
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3835
    :cond_f
    iget-object v2, p0, Ldhi;->i:Ldig;

    if-eqz v2, :cond_10

    .line 3836
    const/16 v2, 0x10

    iget-object v3, p0, Ldhi;->i:Ldig;

    .line 3837
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3839
    :cond_10
    iget-object v2, p0, Ldhi;->l:[Ldig;

    if-eqz v2, :cond_12

    .line 3840
    iget-object v2, p0, Ldhi;->l:[Ldig;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_12

    aget-object v4, v2, v1

    .line 3841
    if-eqz v4, :cond_11

    .line 3842
    const/16 v5, 0x11

    .line 3843
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 3840
    :cond_11
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3847
    :cond_12
    iget-object v1, p0, Ldhi;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3848
    iput v0, p0, Ldhi;->cachedSize:I

    .line 3849
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 3633
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldhi;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldhi;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldhi;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldhi;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldhi;->c:Ldhl;

    if-nez v0, :cond_2

    new-instance v0, Ldhl;

    invoke-direct {v0}, Ldhl;-><init>()V

    iput-object v0, p0, Ldhi;->c:Ldhl;

    :cond_2
    iget-object v0, p0, Ldhi;->c:Ldhl;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_3

    if-eq v0, v4, :cond_3

    if-eq v0, v5, :cond_3

    const/4 v2, 0x3

    if-eq v0, v2, :cond_3

    const/4 v2, 0x4

    if-eq v0, v2, :cond_3

    const/4 v2, 0x5

    if-ne v0, v2, :cond_4

    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldhi;->e:Ljava/lang/Integer;

    goto :goto_0

    :cond_4
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldhi;->e:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldhi;->f:Ldhg;

    if-nez v0, :cond_5

    new-instance v0, Ldhg;

    invoke-direct {v0}, Ldhg;-><init>()V

    iput-object v0, p0, Ldhi;->f:Ldhg;

    :cond_5
    iget-object v0, p0, Ldhi;->f:Ldhg;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ldhi;->h:Ldhl;

    if-nez v0, :cond_6

    new-instance v0, Ldhl;

    invoke-direct {v0}, Ldhl;-><init>()V

    iput-object v0, p0, Ldhi;->h:Ldhl;

    :cond_6
    iget-object v0, p0, Ldhi;->h:Ldhl;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldhi;->j:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Ldhi;->j:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Ldhi;->j:[Ljava/lang/String;

    :goto_1
    iget-object v2, p0, Ldhi;->j:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Ldhi;->j:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_7
    iget-object v2, p0, Ldhi;->j:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldhi;->k:[Ldhl;

    if-nez v0, :cond_9

    move v0, v1

    :goto_2
    add-int/2addr v2, v0

    new-array v2, v2, [Ldhl;

    iget-object v3, p0, Ldhi;->k:[Ldhl;

    if-eqz v3, :cond_8

    iget-object v3, p0, Ldhi;->k:[Ldhl;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    iput-object v2, p0, Ldhi;->k:[Ldhl;

    :goto_3
    iget-object v2, p0, Ldhi;->k:[Ldhl;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    iget-object v2, p0, Ldhi;->k:[Ldhl;

    new-instance v3, Ldhl;

    invoke-direct {v3}, Ldhl;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldhi;->k:[Ldhl;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_9
    iget-object v0, p0, Ldhi;->k:[Ldhl;

    array-length v0, v0

    goto :goto_2

    :cond_a
    iget-object v2, p0, Ldhi;->k:[Ldhl;

    new-instance v3, Ldhl;

    invoke-direct {v3}, Ldhl;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldhi;->k:[Ldhl;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldhi;->m:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldhi;->n:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v4, :cond_b

    if-ne v0, v5, :cond_c

    :cond_b
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldhi;->p:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_c
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldhi;->p:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Ldhi;->q:Ldhp;

    if-nez v0, :cond_d

    new-instance v0, Ldhp;

    invoke-direct {v0}, Ldhp;-><init>()V

    iput-object v0, p0, Ldhi;->q:Ldhp;

    :cond_d
    iget-object v0, p0, Ldhi;->q:Ldhp;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Ldhi;->g:Ldhh;

    if-nez v0, :cond_e

    new-instance v0, Ldhh;

    invoke-direct {v0}, Ldhh;-><init>()V

    iput-object v0, p0, Ldhi;->g:Ldhh;

    :cond_e
    iget-object v0, p0, Ldhi;->g:Ldhh;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldhi;->o:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldhi;->r:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_f
    iget-object v0, p0, Ldhi;->d:Ldig;

    if-nez v0, :cond_f

    new-instance v0, Ldig;

    invoke-direct {v0}, Ldig;-><init>()V

    iput-object v0, p0, Ldhi;->d:Ldig;

    :cond_f
    iget-object v0, p0, Ldhi;->d:Ldig;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_10
    iget-object v0, p0, Ldhi;->i:Ldig;

    if-nez v0, :cond_10

    new-instance v0, Ldig;

    invoke-direct {v0}, Ldig;-><init>()V

    iput-object v0, p0, Ldhi;->i:Ldig;

    :cond_10
    iget-object v0, p0, Ldhi;->i:Ldig;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_11
    const/16 v0, 0x8a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldhi;->l:[Ldig;

    if-nez v0, :cond_12

    move v0, v1

    :goto_4
    add-int/2addr v2, v0

    new-array v2, v2, [Ldig;

    iget-object v3, p0, Ldhi;->l:[Ldig;

    if-eqz v3, :cond_11

    iget-object v3, p0, Ldhi;->l:[Ldig;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_11
    iput-object v2, p0, Ldhi;->l:[Ldig;

    :goto_5
    iget-object v2, p0, Ldhi;->l:[Ldig;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_13

    iget-object v2, p0, Ldhi;->l:[Ldig;

    new-instance v3, Ldig;

    invoke-direct {v3}, Ldig;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldhi;->l:[Ldig;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_12
    iget-object v0, p0, Ldhi;->l:[Ldig;

    array-length v0, v0

    goto :goto_4

    :cond_13
    iget-object v2, p0, Ldhi;->l:[Ldig;

    new-instance v3, Ldig;

    invoke-direct {v3}, Ldig;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldhi;->l:[Ldig;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x68 -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 3702
    const/4 v1, 0x1

    iget-object v2, p0, Ldhi;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 3703
    iget-object v1, p0, Ldhi;->c:Ldhl;

    if-eqz v1, :cond_0

    .line 3704
    const/4 v1, 0x2

    iget-object v2, p0, Ldhi;->c:Ldhl;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 3706
    :cond_0
    iget-object v1, p0, Ldhi;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 3707
    const/4 v1, 0x3

    iget-object v2, p0, Ldhi;->e:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 3709
    :cond_1
    iget-object v1, p0, Ldhi;->f:Ldhg;

    if-eqz v1, :cond_2

    .line 3710
    const/4 v1, 0x4

    iget-object v2, p0, Ldhi;->f:Ldhg;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 3712
    :cond_2
    iget-object v1, p0, Ldhi;->h:Ldhl;

    if-eqz v1, :cond_3

    .line 3713
    const/4 v1, 0x5

    iget-object v2, p0, Ldhi;->h:Ldhl;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 3715
    :cond_3
    iget-object v1, p0, Ldhi;->j:[Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 3716
    iget-object v2, p0, Ldhi;->j:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 3717
    const/4 v5, 0x6

    invoke-virtual {p1, v5, v4}, Lepl;->a(ILjava/lang/String;)V

    .line 3716
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3720
    :cond_4
    iget-object v1, p0, Ldhi;->k:[Ldhl;

    if-eqz v1, :cond_6

    .line 3721
    iget-object v2, p0, Ldhi;->k:[Ldhl;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 3722
    if-eqz v4, :cond_5

    .line 3723
    const/4 v5, 0x7

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 3721
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3727
    :cond_6
    iget-object v1, p0, Ldhi;->m:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 3728
    const/16 v1, 0x8

    iget-object v2, p0, Ldhi;->m:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 3730
    :cond_7
    iget-object v1, p0, Ldhi;->n:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 3731
    const/16 v1, 0x9

    iget-object v2, p0, Ldhi;->n:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 3733
    :cond_8
    iget-object v1, p0, Ldhi;->p:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 3734
    const/16 v1, 0xa

    iget-object v2, p0, Ldhi;->p:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 3736
    :cond_9
    iget-object v1, p0, Ldhi;->q:Ldhp;

    if-eqz v1, :cond_a

    .line 3737
    const/16 v1, 0xb

    iget-object v2, p0, Ldhi;->q:Ldhp;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 3739
    :cond_a
    iget-object v1, p0, Ldhi;->g:Ldhh;

    if-eqz v1, :cond_b

    .line 3740
    const/16 v1, 0xc

    iget-object v2, p0, Ldhi;->g:Ldhh;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 3742
    :cond_b
    iget-object v1, p0, Ldhi;->o:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    .line 3743
    const/16 v1, 0xd

    iget-object v2, p0, Ldhi;->o:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 3745
    :cond_c
    iget-object v1, p0, Ldhi;->r:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 3746
    const/16 v1, 0xe

    iget-object v2, p0, Ldhi;->r:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 3748
    :cond_d
    iget-object v1, p0, Ldhi;->d:Ldig;

    if-eqz v1, :cond_e

    .line 3749
    const/16 v1, 0xf

    iget-object v2, p0, Ldhi;->d:Ldig;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 3751
    :cond_e
    iget-object v1, p0, Ldhi;->i:Ldig;

    if-eqz v1, :cond_f

    .line 3752
    const/16 v1, 0x10

    iget-object v2, p0, Ldhi;->i:Ldig;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 3754
    :cond_f
    iget-object v1, p0, Ldhi;->l:[Ldig;

    if-eqz v1, :cond_11

    .line 3755
    iget-object v1, p0, Ldhi;->l:[Ldig;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_11

    aget-object v3, v1, v0

    .line 3756
    if-eqz v3, :cond_10

    .line 3757
    const/16 v4, 0x11

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 3755
    :cond_10
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 3761
    :cond_11
    iget-object v0, p0, Ldhi;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 3763
    return-void
.end method
