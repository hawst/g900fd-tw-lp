.class public final Laxl;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:J

.field final synthetic b:Lyj;

.field final synthetic c:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

.field private d:I

.field private e:Ljava/lang/String;

.field private f:J


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;JLyj;)V
    .locals 0

    .prologue
    .line 592
    iput-object p1, p0, Laxl;->c:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

    iput-wide p2, p0, Laxl;->a:J

    iput-object p4, p0, Laxl;->b:Lyj;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method static synthetic a(Laxl;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 592
    iget-object v0, p0, Laxl;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 592
    new-instance v0, Lyt;

    iget-object v1, p0, Laxl;->b:Lyj;

    invoke-direct {v0, v1}, Lyt;-><init>(Lyj;)V

    const-string v1, "upgrade_value"

    invoke-virtual {v0, v1}, Lyt;->O(Ljava/lang/String;)J

    move-result-wide v1

    const-wide/16 v3, 0x2

    cmp-long v3, v1, v3

    if-eqz v3, :cond_0

    const-wide/16 v3, 0x1

    cmp-long v3, v1, v3

    if-nez v3, :cond_1

    :cond_0
    const-string v3, "upgrade_url"

    invoke-virtual {v0, v3}, Lyt;->P(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    long-to-int v1, v1

    iput v1, p0, Laxl;->d:I

    iput-object v3, p0, Laxl;->e:Ljava/lang/String;

    const-string v1, "upgrade_version"

    invoke-virtual {v0, v1}, Lyt;->O(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Laxl;->f:J

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 592
    iget-object v0, p0, Laxl;->c:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->h(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-wide v0, p0, Laxl;->a:J

    iget-wide v2, p0, Laxl;->f:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    iget v0, p0, Laxl;->d:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Laxl;->c:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->i(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)Z

    new-instance v0, Laxm;

    invoke-direct {v0, p0}, Laxm;-><init>(Laxl;)V

    new-instance v1, Laxn;

    invoke-direct {v1, p0}, Laxn;-><init>(Laxl;)V

    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Laxl;->c:Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v3, Lh;->cR:I

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    sget v3, Lh;->le:I

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lh;->hZ:I

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
