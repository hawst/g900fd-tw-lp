.class final Lbwp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Lyj;

.field private final b:Z

.field private c:Z

.field private d:Lbwf;


# direct methods
.method public constructor <init>(Lyj;Z)V
    .locals 1

    .prologue
    .line 485
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 486
    iput-object p1, p0, Lbwp;->a:Lyj;

    .line 487
    iput-boolean p2, p0, Lbwp;->b:Z

    .line 488
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbwp;->c:Z

    .line 489
    const/4 v0, 0x0

    iput-object v0, p0, Lbwp;->d:Lbwf;

    .line 490
    return-void
.end method

.method static synthetic a(Lbwp;)Z
    .locals 1

    .prologue
    .line 479
    iget-boolean v0, p0, Lbwp;->b:Z

    return v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 493
    monitor-enter p0

    .line 494
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lbwp;->c:Z

    .line 495
    iget-object v0, p0, Lbwp;->d:Lbwf;

    if-eqz v0, :cond_0

    .line 496
    iget-object v0, p0, Lbwp;->d:Lbwf;

    invoke-static {v0}, Lbwf;->a(Lbwf;)V

    .line 498
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .locals 8

    .prologue
    const-wide/16 v2, -0x1

    .line 504
    :try_start_0
    new-instance v1, Lyt;

    iget-object v0, p0, Lbwp;->a:Lyj;

    invoke-direct {v1, v0}, Lyt;-><init>(Lyj;)V

    .line 507
    iget-boolean v0, p0, Lbwp;->b:Z

    if-eqz v0, :cond_0

    .line 508
    iget-object v0, p0, Lbwp;->a:Lyj;

    invoke-static {v0}, Lbsx;->a(Lyj;)Lbsx;

    move-result-object v0

    const-string v4, "sms_no_full_sync_till_millis"

    const-wide/16 v5, -0x1

    invoke-virtual {v0, v4, v5, v6}, Lbsx;->a(Ljava/lang/String;J)J

    move-result-wide v4

    .line 510
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-lez v0, :cond_1

    .line 512
    invoke-static {}, Lbwf;->f()Z

    .line 513
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SmsSyncManager A#"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbwp;->a:Lyj;

    invoke-virtual {v2}, Lyj;->j()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": task can not be scheduled yet"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 570
    iget-object v0, p0, Lbwp;->a:Lyj;

    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0}, Lbwf;->a(Ljava/lang/String;Lbwp;)V

    .line 571
    :goto_0
    return-void

    .line 523
    :cond_0
    :try_start_1
    iget-object v0, p0, Lbwp;->a:Lyj;

    invoke-static {v0}, Lbsx;->a(Lyj;)Lbsx;

    move-result-object v0

    const-string v2, "sms_last_sync_time_millis"

    const-wide/16 v3, -0x1

    invoke-virtual {v0, v2, v3, v4}, Lbsx;->a(Ljava/lang/String;J)J

    move-result-wide v2

    .line 526
    :cond_1
    monitor-enter p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 527
    :try_start_2
    iget-boolean v0, p0, Lbwp;->c:Z

    if-eqz v0, :cond_2

    .line 528
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SmsSyncManager A#"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbwp;->a:Lyj;

    invoke-virtual {v2}, Lyj;->j()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": task aborted"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 530
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 570
    iget-object v0, p0, Lbwp;->a:Lyj;

    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0}, Lbwf;->a(Ljava/lang/String;Lbwp;)V

    goto :goto_0

    .line 532
    :cond_2
    :try_start_3
    new-instance v0, Lbwf;

    iget-boolean v4, p0, Lbwp;->b:Z

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lbwf;-><init>(Lyt;JZB)V

    iput-object v0, p0, Lbwp;->d:Lbwf;

    .line 534
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 535
    :try_start_4
    iget-object v0, p0, Lbwp;->d:Lbwf;

    invoke-static {v0}, Lbwf;->b(Lbwf;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v0

    .line 536
    if-gez v0, :cond_3

    .line 570
    iget-object v0, p0, Lbwp;->a:Lyj;

    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0}, Lbwf;->a(Ljava/lang/String;Lbwp;)V

    goto :goto_0

    .line 534
    :catchall_0
    move-exception v0

    :try_start_5
    monitor-exit p0

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 570
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lbwp;->a:Lyj;

    invoke-virtual {v1}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p0}, Lbwf;->a(Ljava/lang/String;Lbwp;)V

    throw v0

    .line 540
    :cond_3
    :try_start_6
    iget-boolean v2, p0, Lbwp;->b:Z

    if-eqz v2, :cond_4

    .line 543
    iget-object v2, p0, Lbwp;->a:Lyj;

    invoke-static {v2}, Lbsx;->a(Lyj;)Lbsx;

    move-result-object v2

    const-string v3, "sms_last_full_sync_time_millis"

    .line 545
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 543
    invoke-virtual {v2, v3, v4, v5}, Lbsx;->b(Ljava/lang/String;J)V

    .line 547
    :cond_4
    invoke-static {v1}, Lbwf;->a(Lyt;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 549
    iget-boolean v1, p0, Lbwp;->b:Z

    if-eqz v1, :cond_6

    if-nez v0, :cond_6

    .line 557
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SmsSyncManager A#"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbwp;->a:Lyj;

    invoke-virtual {v2}, Lyj;->j()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": even after full sync we are still not in sync"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 559
    iget-object v0, p0, Lbwp;->a:Lyj;

    invoke-static {v0}, Lbsx;->a(Lyj;)Lbsx;

    move-result-object v0

    const-string v1, "sms_no_full_sync_till_millis"

    .line 561
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {}, Lbwf;->g()J

    move-result-wide v4

    add-long/2addr v2, v4

    .line 559
    invoke-virtual {v0, v1, v2, v3}, Lbsx;->b(Ljava/lang/String;J)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 570
    :cond_5
    :goto_1
    iget-object v0, p0, Lbwp;->a:Lyj;

    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0}, Lbwf;->a(Ljava/lang/String;Lbwp;)V

    goto/16 :goto_0

    .line 566
    :cond_6
    :try_start_7
    iget-object v0, p0, Lbwp;->a:Lyj;

    const/4 v1, 0x1

    const-wide/16 v2, 0x2710

    invoke-static {v0, v1, v2, v3}, Lbwf;->a(Lyj;ZJ)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_1
.end method
