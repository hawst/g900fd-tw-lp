.class public Lbik;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final g:Z


# direct methods
.method private constructor <init>(Ldwu;)V
    .locals 3

    .prologue
    .line 3969
    iget-object v0, p1, Ldwu;->b:Ldvn;

    const-wide/16 v1, -0x1

    invoke-direct {p0, v0, v1, v2}, Lbfz;-><init>(Ldvn;J)V

    .line 3971
    iget-object v0, p1, Ldwu;->d:Ljava/lang/Boolean;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    iput-boolean v0, p0, Lbik;->g:Z

    .line 3974
    sget-boolean v0, Lbfz;->a:Z

    if-eqz v0, :cond_0

    .line 3975
    const-string v0, "Babel_protos"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "StartPhoneVerificationResponse from:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3978
    :cond_0
    return-void
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 3982
    new-instance v0, Ldwu;

    invoke-direct {v0}, Ldwu;-><init>()V

    .line 3983
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldwu;

    .line 3984
    iget-object v1, v0, Ldwu;->b:Ldvn;

    invoke-static {v1}, Lbik;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3985
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldwu;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 3987
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbik;

    invoke-direct {v1, v0}, Lbik;-><init>(Ldwu;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Lyt;Lbnl;)V
    .locals 3

    .prologue
    .line 3994
    invoke-super {p0, p1, p2}, Lbfz;->a(Lyt;Lbnl;)V

    .line 3996
    iget-boolean v0, p0, Lbik;->g:Z

    if-eqz v0, :cond_0

    .line 3997
    const-string v0, "Babel"

    const-string v1, "Rate limit exceeded for phone verification"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 4001
    invoke-static {}, Lbmz;->a()Lbmz;

    move-result-object v0

    const/16 v1, 0x69

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lbmz;->a(IZ)V

    .line 4008
    :goto_0
    return-void

    .line 4006
    :cond_0
    invoke-static {}, Lbmz;->a()Lbmz;

    move-result-object v0

    const/16 v1, 0x66

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lbmz;->a(IZ)V

    goto :goto_0
.end method
