.class public Lbft;
.super Lbep;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:I

.field public final b:J

.field public final c:[I

.field public final h:Z

.field private final i:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 6

    .prologue
    .line 3466
    const-wide/16 v1, -0x1

    const/4 v4, 0x0

    move-object v0, p0

    move v3, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lbft;-><init>(JIZLjava/lang/String;)V

    .line 3467
    return-void
.end method

.method public constructor <init>(JI)V
    .locals 6

    .prologue
    .line 3417
    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-wide v1, p1

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lbft;-><init>(JIZLjava/lang/String;)V

    .line 3418
    return-void
.end method

.method private constructor <init>(JIZLjava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x3

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 3423
    invoke-direct {p0}, Lbep;-><init>()V

    .line 3424
    iput-wide p1, p0, Lbft;->b:J

    .line 3429
    iput-object p5, p0, Lbft;->i:Ljava/lang/String;

    .line 3430
    iput p3, p0, Lbft;->a:I

    .line 3431
    iput-boolean p4, p0, Lbft;->h:Z

    .line 3432
    new-array v0, v4, [I

    iput-object v0, p0, Lbft;->c:[I

    .line 3433
    packed-switch p3, :pswitch_data_0

    .line 3459
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "illegal filter mode: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3435
    :pswitch_0
    iget-object v0, p0, Lbft;->c:[I

    aput v2, v0, v1

    .line 3436
    iget-object v0, p0, Lbft;->c:[I

    aput v3, v0, v2

    .line 3457
    :goto_0
    return-void

    .line 3439
    :pswitch_1
    iget-object v0, p0, Lbft;->c:[I

    aput v4, v0, v1

    .line 3440
    iget-object v0, p0, Lbft;->c:[I

    aput v3, v0, v2

    goto :goto_0

    .line 3443
    :pswitch_2
    iget-object v0, p0, Lbft;->c:[I

    aput v2, v0, v1

    .line 3444
    iget-object v0, p0, Lbft;->c:[I

    aput v5, v0, v2

    goto :goto_0

    .line 3447
    :pswitch_3
    iget-object v0, p0, Lbft;->c:[I

    aput v2, v0, v1

    .line 3448
    iget-object v0, p0, Lbft;->c:[I

    const/4 v1, 0x5

    aput v1, v0, v2

    goto :goto_0

    .line 3451
    :pswitch_4
    iget-object v0, p0, Lbft;->c:[I

    aput v2, v0, v1

    .line 3452
    iget-object v0, p0, Lbft;->c:[I

    aput v3, v0, v2

    goto :goto_0

    .line 3455
    :pswitch_5
    iget-object v0, p0, Lbft;->c:[I

    aput v2, v0, v1

    .line 3456
    iget-object v0, p0, Lbft;->c:[I

    aput v5, v0, v2

    goto :goto_0

    .line 3433
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 6

    .prologue
    .line 3475
    new-instance v1, Ldxh;

    invoke-direct {v1}, Ldxh;-><init>()V

    .line 3478
    iget-wide v2, p0, Lbft;->b:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-ltz v0, :cond_0

    .line 3479
    iget-wide v2, p0, Lbft;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v1, Ldxh;->c:Ljava/lang/Long;

    .line 3484
    :cond_0
    iget v0, p0, Lbft;->a:I

    const/4 v2, 0x6

    if-ne v0, v2, :cond_2

    const/16 v0, 0x64

    .line 3490
    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldxh;->d:Ljava/lang/Integer;

    .line 3491
    invoke-virtual {p0, p1, p2}, Lbft;->c(Ljava/lang/String;I)Ldvm;

    move-result-object v0

    iput-object v0, v1, Ldxh;->b:Ldvm;

    .line 3492
    const-string v0, "babel_smaxevperconv"

    const/16 v2, 0x14

    invoke-static {v0, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldxh;->e:Ljava/lang/Integer;

    .line 3495
    iget-object v0, p0, Lbft;->c:[I

    if-eqz v0, :cond_1

    .line 3496
    iget-object v0, p0, Lbft;->c:[I

    iput-object v0, v1, Ldxh;->f:[I

    .line 3499
    :cond_1
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldxh;->g:Ljava/lang/Integer;

    .line 3500
    return-object v1

    .line 3484
    :cond_2
    const-string v0, "babel_smaxconv"

    const/16 v2, 0x1e

    .line 3487
    invoke-static {v0, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public a(Lyj;Lbph;)V
    .locals 4

    .prologue
    .line 3546
    invoke-virtual {p1}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    .line 3547
    sget-boolean v1, Lbea;->d:Z

    if-eqz v1, :cond_0

    .line 3548
    const-string v1, "Babel_RequestWriter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SyncRecentConversations.onFailed "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3551
    :cond_0
    invoke-static {}, Lbya;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3552
    new-instance v0, Lbyc;

    invoke-direct {v0}, Lbyc;-><init>()V

    const-string v1, "src_expired"

    invoke-virtual {v0, v1}, Lbyc;->a(Ljava/lang/String;)Lbyc;

    move-result-object v0

    .line 3553
    invoke-virtual {v0, p1}, Lbyc;->a(Lyj;)Lbyc;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "filterMode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lbft;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 3554
    invoke-virtual {v0, v1}, Lbyc;->b(Ljava/lang/String;)Lbyc;

    move-result-object v0

    .line 3555
    invoke-virtual {v0}, Lbyc;->b()V

    .line 3557
    :cond_1
    iget-object v0, p0, Lbft;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 3558
    invoke-static {p1}, Lbpf;->a(Lyj;)Lbpf;

    move-result-object v0

    .line 3559
    iget-object v1, p0, Lbft;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lbpf;->d(Ljava/lang/String;)Z

    .line 3562
    :cond_2
    return-void
.end method

.method public a(Lbea;)Z
    .locals 4

    .prologue
    .line 3534
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3535
    check-cast p1, Lbft;

    .line 3536
    iget-wide v0, p0, Lbft;->b:J

    iget-wide v2, p1, Lbft;->b:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget v0, p0, Lbft;->a:I

    iget v1, p1, Lbft;->a:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lbft;->h:Z

    iget-boolean v1, p1, Lbft;->h:Z

    if-ne v0, v1, :cond_0

    .line 3539
    const/4 v0, 0x1

    .line 3541
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3505
    const-string v0, "conversations/syncrecentconversations"

    return-object v0
.end method

.method public d()J
    .locals 3

    .prologue
    .line 3518
    invoke-virtual {p0}, Lbft;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3520
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x3c

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    .line 3522
    :goto_0
    return-wide v0

    :cond_0
    const-string v0, "babel_src_timeout"

    sget-wide v1, Lbkj;->e:J

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;J)J

    move-result-wide v0

    goto :goto_0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 3470
    iget v0, p0, Lbft;->a:I

    return v0
.end method

.method public i()Z
    .locals 4

    .prologue
    .line 3509
    iget-wide v0, p0, Lbft;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3513
    iget-object v0, p0, Lbft;->i:Ljava/lang/String;

    return-object v0
.end method
