.class public final Leuu;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leuu;


# instance fields
.field public b:Leuz;

.field public c:Leus;

.field public d:Leuv;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 275
    const/4 v0, 0x0

    new-array v0, v0, [Leuu;

    sput-object v0, Leuu;->a:[Leuu;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 276
    invoke-direct {p0}, Lepn;-><init>()V

    .line 279
    iput-object v0, p0, Leuu;->b:Leuz;

    .line 282
    iput-object v0, p0, Leuu;->c:Leus;

    .line 285
    iput-object v0, p0, Leuu;->d:Leuv;

    .line 276
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 305
    const/4 v0, 0x0

    .line 306
    iget-object v1, p0, Leuu;->b:Leuz;

    if-eqz v1, :cond_0

    .line 307
    const/4 v0, 0x1

    iget-object v1, p0, Leuu;->b:Leuz;

    .line 308
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 310
    :cond_0
    iget-object v1, p0, Leuu;->c:Leus;

    if-eqz v1, :cond_1

    .line 311
    const/4 v1, 0x2

    iget-object v2, p0, Leuu;->c:Leus;

    .line 312
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 314
    :cond_1
    iget-object v1, p0, Leuu;->d:Leuv;

    if-eqz v1, :cond_2

    .line 315
    const/4 v1, 0x4

    iget-object v2, p0, Leuu;->d:Leuv;

    .line 316
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 318
    :cond_2
    iget-object v1, p0, Leuu;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 319
    iput v0, p0, Leuu;->cachedSize:I

    .line 320
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 272
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Leuu;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Leuu;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Leuu;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Leuu;->b:Leuz;

    if-nez v0, :cond_2

    new-instance v0, Leuz;

    invoke-direct {v0}, Leuz;-><init>()V

    iput-object v0, p0, Leuu;->b:Leuz;

    :cond_2
    iget-object v0, p0, Leuu;->b:Leuz;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Leuu;->c:Leus;

    if-nez v0, :cond_3

    new-instance v0, Leus;

    invoke-direct {v0}, Leus;-><init>()V

    iput-object v0, p0, Leuu;->c:Leus;

    :cond_3
    iget-object v0, p0, Leuu;->c:Leus;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Leuu;->d:Leuv;

    if-nez v0, :cond_4

    new-instance v0, Leuv;

    invoke-direct {v0}, Leuv;-><init>()V

    iput-object v0, p0, Leuu;->d:Leuv;

    :cond_4
    iget-object v0, p0, Leuu;->d:Leuv;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x22 -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 290
    iget-object v0, p0, Leuu;->b:Leuz;

    if-eqz v0, :cond_0

    .line 291
    const/4 v0, 0x1

    iget-object v1, p0, Leuu;->b:Leuz;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 293
    :cond_0
    iget-object v0, p0, Leuu;->c:Leus;

    if-eqz v0, :cond_1

    .line 294
    const/4 v0, 0x2

    iget-object v1, p0, Leuu;->c:Leus;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 296
    :cond_1
    iget-object v0, p0, Leuu;->d:Leuv;

    if-eqz v0, :cond_2

    .line 297
    const/4 v0, 0x4

    iget-object v1, p0, Leuu;->d:Leuv;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 299
    :cond_2
    iget-object v0, p0, Leuu;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 301
    return-void
.end method
