.class public final Ldjx;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldjx;


# instance fields
.field public b:Ldjy;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ldju;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 657
    const/4 v0, 0x0

    new-array v0, v0, [Ldjx;

    sput-object v0, Ldjx;->a:[Ldjx;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 658
    invoke-direct {p0}, Lepn;-><init>()V

    .line 661
    iput-object v0, p0, Ldjx;->b:Ldjy;

    .line 668
    iput-object v0, p0, Ldjx;->e:Ldju;

    .line 658
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 691
    const/4 v0, 0x0

    .line 692
    iget-object v1, p0, Ldjx;->b:Ldjy;

    if-eqz v1, :cond_0

    .line 693
    const/4 v0, 0x1

    iget-object v1, p0, Ldjx;->b:Ldjy;

    .line 694
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 696
    :cond_0
    iget-object v1, p0, Ldjx;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 697
    const/4 v1, 0x2

    iget-object v2, p0, Ldjx;->c:Ljava/lang/String;

    .line 698
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 700
    :cond_1
    iget-object v1, p0, Ldjx;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 701
    const/4 v1, 0x3

    iget-object v2, p0, Ldjx;->d:Ljava/lang/String;

    .line 702
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 704
    :cond_2
    iget-object v1, p0, Ldjx;->e:Ldju;

    if-eqz v1, :cond_3

    .line 705
    const/4 v1, 0x4

    iget-object v2, p0, Ldjx;->e:Ldju;

    .line 706
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 708
    :cond_3
    iget-object v1, p0, Ldjx;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 709
    iput v0, p0, Ldjx;->cachedSize:I

    .line 710
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 654
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldjx;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldjx;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldjx;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldjx;->b:Ldjy;

    if-nez v0, :cond_2

    new-instance v0, Ldjy;

    invoke-direct {v0}, Ldjy;-><init>()V

    iput-object v0, p0, Ldjx;->b:Ldjy;

    :cond_2
    iget-object v0, p0, Ldjx;->b:Ldjy;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldjx;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldjx;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldjx;->e:Ldju;

    if-nez v0, :cond_3

    new-instance v0, Ldju;

    invoke-direct {v0}, Ldju;-><init>()V

    iput-object v0, p0, Ldjx;->e:Ldju;

    :cond_3
    iget-object v0, p0, Ldjx;->e:Ldju;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 673
    iget-object v0, p0, Ldjx;->b:Ldjy;

    if-eqz v0, :cond_0

    .line 674
    const/4 v0, 0x1

    iget-object v1, p0, Ldjx;->b:Ldjy;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 676
    :cond_0
    iget-object v0, p0, Ldjx;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 677
    const/4 v0, 0x2

    iget-object v1, p0, Ldjx;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 679
    :cond_1
    iget-object v0, p0, Ldjx;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 680
    const/4 v0, 0x3

    iget-object v1, p0, Ldjx;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 682
    :cond_2
    iget-object v0, p0, Ldjx;->e:Ldju;

    if-eqz v0, :cond_3

    .line 683
    const/4 v0, 0x4

    iget-object v1, p0, Ldjx;->e:Ldju;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 685
    :cond_3
    iget-object v0, p0, Ldjx;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 687
    return-void
.end method
