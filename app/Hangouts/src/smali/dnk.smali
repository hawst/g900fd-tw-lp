.class public final Ldnk;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldnk;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3955
    const/4 v0, 0x0

    new-array v0, v0, [Ldnk;

    sput-object v0, Ldnk;->a:[Ldnk;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3956
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 3982
    const/4 v0, 0x0

    .line 3983
    iget-object v1, p0, Ldnk;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 3984
    const/4 v0, 0x1

    iget-object v1, p0, Ldnk;->b:Ljava/lang/Integer;

    .line 3985
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3987
    :cond_0
    iget-object v1, p0, Ldnk;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 3988
    const/4 v1, 0x2

    iget-object v2, p0, Ldnk;->c:Ljava/lang/String;

    .line 3989
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3991
    :cond_1
    iget-object v1, p0, Ldnk;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 3992
    const/4 v1, 0x3

    iget-object v2, p0, Ldnk;->d:Ljava/lang/String;

    .line 3993
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3995
    :cond_2
    iget-object v1, p0, Ldnk;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3996
    iput v0, p0, Ldnk;->cachedSize:I

    .line 3997
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 3952
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldnk;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldnk;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldnk;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldnk;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldnk;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldnk;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 3967
    iget-object v0, p0, Ldnk;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 3968
    const/4 v0, 0x1

    iget-object v1, p0, Ldnk;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 3970
    :cond_0
    iget-object v0, p0, Ldnk;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 3971
    const/4 v0, 0x2

    iget-object v1, p0, Ldnk;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 3973
    :cond_1
    iget-object v0, p0, Ldnk;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 3974
    const/4 v0, 0x3

    iget-object v1, p0, Ldnk;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 3976
    :cond_2
    iget-object v0, p0, Ldnk;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 3978
    return-void
.end method
