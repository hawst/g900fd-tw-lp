.class public final Ldkd;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldkd;


# instance fields
.field public b:[Ljava/lang/String;

.field public c:[Ldke;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 148
    const/4 v0, 0x0

    new-array v0, v0, [Ldkd;

    sput-object v0, Ldkd;->a:[Ldkd;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 149
    invoke-direct {p0}, Lepn;-><init>()V

    .line 152
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Ldkd;->b:[Ljava/lang/String;

    .line 155
    sget-object v0, Ldke;->a:[Ldke;

    iput-object v0, p0, Ldkd;->c:[Ldke;

    .line 149
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 179
    iget-object v0, p0, Ldkd;->b:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ldkd;->b:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 181
    iget-object v3, p0, Ldkd;->b:[Ljava/lang/String;

    array-length v4, v3

    move v0, v1

    move v2, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    .line 183
    invoke-static {v5}, Lepl;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 181
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 186
    :cond_0
    iget-object v0, p0, Ldkd;->b:[Ljava/lang/String;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v2

    .line 188
    :goto_1
    iget-object v2, p0, Ldkd;->c:[Ldke;

    if-eqz v2, :cond_2

    .line 189
    iget-object v2, p0, Ldkd;->c:[Ldke;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 190
    if-eqz v4, :cond_1

    .line 191
    const/4 v5, 0x2

    .line 192
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 189
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 196
    :cond_2
    iget-object v1, p0, Ldkd;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 197
    iput v0, p0, Ldkd;->cachedSize:I

    .line 198
    return v0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 145
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldkd;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldkd;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldkd;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldkd;->b:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Ldkd;->b:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Ldkd;->b:[Ljava/lang/String;

    :goto_1
    iget-object v2, p0, Ldkd;->b:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Ldkd;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v2, p0, Ldkd;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldkd;->c:[Ldke;

    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    add-int/2addr v2, v0

    new-array v2, v2, [Ldke;

    iget-object v3, p0, Ldkd;->c:[Ldke;

    if-eqz v3, :cond_3

    iget-object v3, p0, Ldkd;->c:[Ldke;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Ldkd;->c:[Ldke;

    :goto_3
    iget-object v2, p0, Ldkd;->c:[Ldke;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Ldkd;->c:[Ldke;

    new-instance v3, Ldke;

    invoke-direct {v3}, Ldke;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldkd;->c:[Ldke;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    iget-object v0, p0, Ldkd;->c:[Ldke;

    array-length v0, v0

    goto :goto_2

    :cond_5
    iget-object v2, p0, Ldkd;->c:[Ldke;

    new-instance v3, Ldke;

    invoke-direct {v3}, Ldke;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldkd;->c:[Ldke;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 160
    iget-object v1, p0, Ldkd;->b:[Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 161
    iget-object v2, p0, Ldkd;->b:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 162
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Lepl;->a(ILjava/lang/String;)V

    .line 161
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 165
    :cond_0
    iget-object v1, p0, Ldkd;->c:[Ldke;

    if-eqz v1, :cond_2

    .line 166
    iget-object v1, p0, Ldkd;->c:[Ldke;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 167
    if-eqz v3, :cond_1

    .line 168
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 166
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 172
    :cond_2
    iget-object v0, p0, Ldkd;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 174
    return-void
.end method
