.class public Lpn;
.super Lkj;
.source "PG"

# interfaces
.implements Lqa;


# instance fields
.field private o:Lpr;

.field private p:Lpk;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lkj;-><init>()V

    return-void
.end method


# virtual methods
.method public j()Lpr;
    .locals 1

    .prologue
    .line 44
    new-instance v0, Lpr;

    invoke-direct {v0, p0}, Lpr;-><init>(Lqa;)V

    return-object v0
.end method

.method public k()Lpr;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lpn;->o:Lpr;

    return-object v0
.end method

.method public l()Landroid/content/Context;
    .locals 0

    .prologue
    .line 118
    return-object p0
.end method

.method public m()Lpk;
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lpn;->p:Lpk;

    if-nez v0, :cond_0

    .line 124
    new-instance v0, Lpk;

    invoke-virtual {p0}, Lpn;->g()Lkd;

    move-result-object v1

    invoke-direct {v0, v1}, Lpk;-><init>(Lkd;)V

    iput-object v0, p0, Lpn;->p:Lpk;

    .line 126
    :cond_0
    iget-object v0, p0, Lpn;->p:Lpk;

    return-object v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 112
    invoke-super {p0, p1, p2, p3}, Lkj;->onActivityResult(IILandroid/content/Intent;)V

    .line 113
    iget-object v0, p0, Lpn;->o:Lpr;

    .line 114
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lpn;->o:Lpr;

    invoke-virtual {v0}, Lpr;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 85
    invoke-super {p0}, Lkj;->onBackPressed()V

    .line 87
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 38
    invoke-super {p0, p1}, Lkj;->onCreate(Landroid/os/Bundle;)V

    .line 39
    invoke-virtual {p0}, Lpn;->j()Lpr;

    move-result-object v0

    iput-object v0, p0, Lpn;->o:Lpr;

    .line 40
    iget-object v0, p0, Lpn;->o:Lpr;

    invoke-virtual {v0, p1}, Lpr;->a(Landroid/os/Bundle;)V

    .line 41
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lpn;->o:Lpr;

    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lpn;->o:Lpr;

    invoke-virtual {v0}, Lpr;->h()V

    .line 79
    invoke-super {p0}, Lkj;->onDestroy()V

    .line 80
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lpn;->o:Lpr;

    invoke-virtual {v0, p1}, Lpr;->a(Landroid/view/MenuItem;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lkj;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lpn;->o:Lpr;

    invoke-virtual {v0}, Lpr;->f()V

    .line 67
    invoke-super {p0}, Lkj;->onPause()V

    .line 68
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lpn;->o:Lpr;

    const/4 v0, 0x1

    return v0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 60
    invoke-super {p0}, Lkj;->onResume()V

    .line 61
    iget-object v0, p0, Lpn;->o:Lpr;

    invoke-virtual {v0}, Lpr;->e()V

    .line 62
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 91
    invoke-super {p0, p1}, Lkj;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 92
    iget-object v0, p0, Lpn;->o:Lpr;

    invoke-virtual {v0, p1}, Lpr;->b(Landroid/os/Bundle;)V

    .line 93
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 54
    invoke-super {p0}, Lkj;->onStart()V

    .line 55
    iget-object v0, p0, Lpn;->o:Lpr;

    invoke-virtual {v0}, Lpr;->d()V

    .line 56
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lpn;->o:Lpr;

    invoke-virtual {v0}, Lpr;->g()V

    .line 73
    invoke-super {p0}, Lkj;->onStop()V

    .line 74
    return-void
.end method
