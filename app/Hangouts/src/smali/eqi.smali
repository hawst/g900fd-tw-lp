.class public final Leqi;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leqi;

.field public static final b:Lepo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lepo",
            "<",
            "Leqi;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public c:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:[Leqq;

.field public g:Ljava/lang/Integer;

.field public h:Ljava/lang/Boolean;

.field public i:Ljava/lang/Integer;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/Boolean;

.field public m:Ljava/lang/Boolean;

.field public n:Ljava/lang/Boolean;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:[Leqk;

.field public r:Ljava/lang/String;

.field public s:Ljava/lang/Integer;

.field public t:[Lepu;

.field public u:Ljava/lang/Integer;

.field public v:Lepu;

.field public w:[Ljava/lang/String;

.field public x:[I

.field public y:[Lepu;

.field public z:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Leqi;

    sput-object v0, Leqi;->a:[Leqi;

    .line 13
    const v0, 0x1a89f67

    new-instance v1, Leqj;

    invoke-direct {v1}, Leqj;-><init>()V

    .line 14
    invoke-static {v0, v1}, Lepo;->a(ILepp;)Lepo;

    move-result-object v0

    sput-object v0, Leqi;->b:Lepo;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 82
    iput-object v1, p0, Leqi;->c:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;

    .line 89
    sget-object v0, Leqq;->a:[Leqq;

    iput-object v0, p0, Leqi;->f:[Leqq;

    .line 92
    iput-object v1, p0, Leqi;->g:Ljava/lang/Integer;

    .line 113
    sget-object v0, Leqk;->a:[Leqk;

    iput-object v0, p0, Leqi;->q:[Leqk;

    .line 120
    sget-object v0, Lepu;->a:[Lepu;

    iput-object v0, p0, Leqi;->t:[Lepu;

    .line 125
    iput-object v1, p0, Leqi;->v:Lepu;

    .line 128
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Leqi;->w:[Ljava/lang/String;

    .line 131
    sget-object v0, Lept;->e:[I

    iput-object v0, p0, Leqi;->x:[I

    .line 134
    sget-object v0, Lepu;->a:[Lepu;

    iput-object v0, p0, Leqi;->y:[Lepu;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 240
    iget-object v0, p0, Leqi;->c:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;

    if-eqz v0, :cond_1d

    .line 241
    const/4 v0, 0x1

    iget-object v2, p0, Leqi;->c:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;

    .line 242
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 244
    :goto_0
    iget-object v2, p0, Leqi;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 245
    const/4 v2, 0x2

    iget-object v3, p0, Leqi;->d:Ljava/lang/String;

    .line 246
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 248
    :cond_0
    iget-object v2, p0, Leqi;->e:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 249
    const/4 v2, 0x3

    iget-object v3, p0, Leqi;->e:Ljava/lang/String;

    .line 250
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 252
    :cond_1
    iget-object v2, p0, Leqi;->f:[Leqq;

    if-eqz v2, :cond_3

    .line 253
    iget-object v3, p0, Leqi;->f:[Leqq;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    .line 254
    if-eqz v5, :cond_2

    .line 255
    const/4 v6, 0x4

    .line 256
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 253
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 260
    :cond_3
    iget-object v2, p0, Leqi;->g:Ljava/lang/Integer;

    if-eqz v2, :cond_4

    .line 261
    const/4 v2, 0x5

    iget-object v3, p0, Leqi;->g:Ljava/lang/Integer;

    .line 262
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 264
    :cond_4
    iget-object v2, p0, Leqi;->h:Ljava/lang/Boolean;

    if-eqz v2, :cond_5

    .line 265
    const/4 v2, 0x6

    iget-object v3, p0, Leqi;->h:Ljava/lang/Boolean;

    .line 266
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 268
    :cond_5
    iget-object v2, p0, Leqi;->i:Ljava/lang/Integer;

    if-eqz v2, :cond_6

    .line 269
    const/4 v2, 0x7

    iget-object v3, p0, Leqi;->i:Ljava/lang/Integer;

    .line 270
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 272
    :cond_6
    iget-object v2, p0, Leqi;->j:Ljava/lang/String;

    if-eqz v2, :cond_7

    .line 273
    const/16 v2, 0x8

    iget-object v3, p0, Leqi;->j:Ljava/lang/String;

    .line 274
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 276
    :cond_7
    iget-object v2, p0, Leqi;->k:Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 277
    const/16 v2, 0x9

    iget-object v3, p0, Leqi;->k:Ljava/lang/String;

    .line 278
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 280
    :cond_8
    iget-object v2, p0, Leqi;->l:Ljava/lang/Boolean;

    if-eqz v2, :cond_9

    .line 281
    const/16 v2, 0xa

    iget-object v3, p0, Leqi;->l:Ljava/lang/Boolean;

    .line 282
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 284
    :cond_9
    iget-object v2, p0, Leqi;->m:Ljava/lang/Boolean;

    if-eqz v2, :cond_a

    .line 285
    const/16 v2, 0xb

    iget-object v3, p0, Leqi;->m:Ljava/lang/Boolean;

    .line 286
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 288
    :cond_a
    iget-object v2, p0, Leqi;->n:Ljava/lang/Boolean;

    if-eqz v2, :cond_b

    .line 289
    const/16 v2, 0xc

    iget-object v3, p0, Leqi;->n:Ljava/lang/Boolean;

    .line 290
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 292
    :cond_b
    iget-object v2, p0, Leqi;->o:Ljava/lang/String;

    if-eqz v2, :cond_c

    .line 293
    const/16 v2, 0xd

    iget-object v3, p0, Leqi;->o:Ljava/lang/String;

    .line 294
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 296
    :cond_c
    iget-object v2, p0, Leqi;->p:Ljava/lang/String;

    if-eqz v2, :cond_d

    .line 297
    const/16 v2, 0xe

    iget-object v3, p0, Leqi;->p:Ljava/lang/String;

    .line 298
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 300
    :cond_d
    iget-object v2, p0, Leqi;->q:[Leqk;

    if-eqz v2, :cond_f

    .line 301
    iget-object v3, p0, Leqi;->q:[Leqk;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_f

    aget-object v5, v3, v2

    .line 302
    if-eqz v5, :cond_e

    .line 303
    const/16 v6, 0xf

    .line 304
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 301
    :cond_e
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 308
    :cond_f
    iget-object v2, p0, Leqi;->r:Ljava/lang/String;

    if-eqz v2, :cond_10

    .line 309
    const/16 v2, 0x10

    iget-object v3, p0, Leqi;->r:Ljava/lang/String;

    .line 310
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 312
    :cond_10
    iget-object v2, p0, Leqi;->s:Ljava/lang/Integer;

    if-eqz v2, :cond_11

    .line 313
    const/16 v2, 0x11

    iget-object v3, p0, Leqi;->s:Ljava/lang/Integer;

    .line 314
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 316
    :cond_11
    iget-object v2, p0, Leqi;->t:[Lepu;

    if-eqz v2, :cond_13

    .line 317
    iget-object v3, p0, Leqi;->t:[Lepu;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_13

    aget-object v5, v3, v2

    .line 318
    if-eqz v5, :cond_12

    .line 319
    const/16 v6, 0x12

    .line 320
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 317
    :cond_12
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 324
    :cond_13
    iget-object v2, p0, Leqi;->u:Ljava/lang/Integer;

    if-eqz v2, :cond_14

    .line 325
    const/16 v2, 0x13

    iget-object v3, p0, Leqi;->u:Ljava/lang/Integer;

    .line 326
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 328
    :cond_14
    iget-object v2, p0, Leqi;->v:Lepu;

    if-eqz v2, :cond_15

    .line 329
    const/16 v2, 0x14

    iget-object v3, p0, Leqi;->v:Lepu;

    .line 330
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 332
    :cond_15
    iget-object v2, p0, Leqi;->w:[Ljava/lang/String;

    if-eqz v2, :cond_17

    iget-object v2, p0, Leqi;->w:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_17

    .line 334
    iget-object v4, p0, Leqi;->w:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_4
    if-ge v2, v5, :cond_16

    aget-object v6, v4, v2

    .line 336
    invoke-static {v6}, Lepl;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 334
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 338
    :cond_16
    add-int/2addr v0, v3

    .line 339
    iget-object v2, p0, Leqi;->w:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 341
    :cond_17
    iget-object v2, p0, Leqi;->x:[I

    if-eqz v2, :cond_19

    iget-object v2, p0, Leqi;->x:[I

    array-length v2, v2

    if-lez v2, :cond_19

    .line 343
    iget-object v4, p0, Leqi;->x:[I

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_5
    if-ge v2, v5, :cond_18

    aget v6, v4, v2

    .line 345
    invoke-static {v6}, Lepl;->f(I)I

    move-result v6

    add-int/2addr v3, v6

    .line 343
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 347
    :cond_18
    add-int/2addr v0, v3

    .line 348
    iget-object v2, p0, Leqi;->x:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 350
    :cond_19
    iget-object v2, p0, Leqi;->y:[Lepu;

    if-eqz v2, :cond_1b

    .line 351
    iget-object v2, p0, Leqi;->y:[Lepu;

    array-length v3, v2

    :goto_6
    if-ge v1, v3, :cond_1b

    aget-object v4, v2, v1

    .line 352
    if-eqz v4, :cond_1a

    .line 353
    const/16 v5, 0x17

    .line 354
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 351
    :cond_1a
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 358
    :cond_1b
    iget-object v1, p0, Leqi;->z:Ljava/lang/Long;

    if-eqz v1, :cond_1c

    .line 359
    const/16 v1, 0x18

    iget-object v2, p0, Leqi;->z:Ljava/lang/Long;

    .line 360
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 362
    :cond_1c
    iget-object v1, p0, Leqi;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 363
    iput v0, p0, Leqi;->cachedSize:I

    .line 364
    return v0

    :cond_1d
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Leqi;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Leqi;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Leqi;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Leqi;->c:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;

    invoke-direct {v0}, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;-><init>()V

    iput-object v0, p0, Leqi;->c:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;

    :cond_2
    iget-object v0, p0, Leqi;->c:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leqi;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leqi;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leqi;->f:[Leqq;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Leqq;

    iget-object v3, p0, Leqi;->f:[Leqq;

    if-eqz v3, :cond_3

    iget-object v3, p0, Leqi;->f:[Leqq;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Leqi;->f:[Leqq;

    :goto_2
    iget-object v2, p0, Leqi;->f:[Leqq;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Leqi;->f:[Leqq;

    new-instance v3, Leqq;

    invoke-direct {v3}, Leqq;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leqi;->f:[Leqq;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Leqi;->f:[Leqq;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Leqi;->f:[Leqq;

    new-instance v3, Leqq;

    invoke-direct {v3}, Leqq;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leqi;->f:[Leqq;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_6

    const/4 v2, 0x1

    if-ne v0, v2, :cond_7

    :cond_6
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leqi;->g:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_7
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leqi;->g:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Leqi;->h:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leqi;->i:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leqi;->j:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leqi;->k:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Leqi;->l:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Leqi;->m:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Leqi;->n:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leqi;->o:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leqi;->p:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_f
    const/16 v0, 0x7a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leqi;->q:[Leqk;

    if-nez v0, :cond_9

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Leqk;

    iget-object v3, p0, Leqi;->q:[Leqk;

    if-eqz v3, :cond_8

    iget-object v3, p0, Leqi;->q:[Leqk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    iput-object v2, p0, Leqi;->q:[Leqk;

    :goto_4
    iget-object v2, p0, Leqi;->q:[Leqk;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    iget-object v2, p0, Leqi;->q:[Leqk;

    new-instance v3, Leqk;

    invoke-direct {v3}, Leqk;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leqi;->q:[Leqk;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_9
    iget-object v0, p0, Leqi;->q:[Leqk;

    array-length v0, v0

    goto :goto_3

    :cond_a
    iget-object v2, p0, Leqi;->q:[Leqk;

    new-instance v3, Leqk;

    invoke-direct {v3}, Leqk;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leqi;->q:[Leqk;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leqi;->r:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leqi;->s:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_12
    const/16 v0, 0x92

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leqi;->t:[Lepu;

    if-nez v0, :cond_c

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lepu;

    iget-object v3, p0, Leqi;->t:[Lepu;

    if-eqz v3, :cond_b

    iget-object v3, p0, Leqi;->t:[Lepu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_b
    iput-object v2, p0, Leqi;->t:[Lepu;

    :goto_6
    iget-object v2, p0, Leqi;->t:[Lepu;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_d

    iget-object v2, p0, Leqi;->t:[Lepu;

    new-instance v3, Lepu;

    invoke-direct {v3}, Lepu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leqi;->t:[Lepu;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_c
    iget-object v0, p0, Leqi;->t:[Lepu;

    array-length v0, v0

    goto :goto_5

    :cond_d
    iget-object v2, p0, Leqi;->t:[Lepu;

    new-instance v3, Lepu;

    invoke-direct {v3}, Lepu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leqi;->t:[Lepu;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leqi;->u:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_14
    iget-object v0, p0, Leqi;->v:Lepu;

    if-nez v0, :cond_e

    new-instance v0, Lepu;

    invoke-direct {v0}, Lepu;-><init>()V

    iput-object v0, p0, Leqi;->v:Lepu;

    :cond_e
    iget-object v0, p0, Leqi;->v:Lepu;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_15
    const/16 v0, 0xaa

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leqi;->w:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Leqi;->w:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Leqi;->w:[Ljava/lang/String;

    :goto_7
    iget-object v2, p0, Leqi;->w:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_f

    iget-object v2, p0, Leqi;->w:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_f
    iget-object v2, p0, Leqi;->w:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    :sswitch_16
    const/16 v0, 0xb0

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leqi;->x:[I

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [I

    iget-object v3, p0, Leqi;->x:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Leqi;->x:[I

    :goto_8
    iget-object v2, p0, Leqi;->x:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_10

    iget-object v2, p0, Leqi;->x:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_10
    iget-object v2, p0, Leqi;->x:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v3

    aput v3, v2, v0

    goto/16 :goto_0

    :sswitch_17
    const/16 v0, 0xba

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leqi;->y:[Lepu;

    if-nez v0, :cond_12

    move v0, v1

    :goto_9
    add-int/2addr v2, v0

    new-array v2, v2, [Lepu;

    iget-object v3, p0, Leqi;->y:[Lepu;

    if-eqz v3, :cond_11

    iget-object v3, p0, Leqi;->y:[Lepu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_11
    iput-object v2, p0, Leqi;->y:[Lepu;

    :goto_a
    iget-object v2, p0, Leqi;->y:[Lepu;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_13

    iget-object v2, p0, Leqi;->y:[Lepu;

    new-instance v3, Lepu;

    invoke-direct {v3}, Lepu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leqi;->y:[Lepu;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_12
    iget-object v0, p0, Leqi;->y:[Lepu;

    array-length v0, v0

    goto :goto_9

    :cond_13
    iget-object v2, p0, Leqi;->y:[Lepu;

    new-instance v3, Lepu;

    invoke-direct {v3}, Lepu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leqi;->y:[Lepu;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_18
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Leqi;->z:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x88 -> :sswitch_11
        0x92 -> :sswitch_12
        0x98 -> :sswitch_13
        0xa2 -> :sswitch_14
        0xaa -> :sswitch_15
        0xb0 -> :sswitch_16
        0xba -> :sswitch_17
        0xc0 -> :sswitch_18
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 141
    iget-object v1, p0, Leqi;->c:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;

    if-eqz v1, :cond_0

    .line 142
    const/4 v1, 0x1

    iget-object v2, p0, Leqi;->c:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 144
    :cond_0
    iget-object v1, p0, Leqi;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 145
    const/4 v1, 0x2

    iget-object v2, p0, Leqi;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 147
    :cond_1
    iget-object v1, p0, Leqi;->e:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 148
    const/4 v1, 0x3

    iget-object v2, p0, Leqi;->e:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 150
    :cond_2
    iget-object v1, p0, Leqi;->f:[Leqq;

    if-eqz v1, :cond_4

    .line 151
    iget-object v2, p0, Leqi;->f:[Leqq;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 152
    if-eqz v4, :cond_3

    .line 153
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 151
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 157
    :cond_4
    iget-object v1, p0, Leqi;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 158
    const/4 v1, 0x5

    iget-object v2, p0, Leqi;->g:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 160
    :cond_5
    iget-object v1, p0, Leqi;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 161
    const/4 v1, 0x6

    iget-object v2, p0, Leqi;->h:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 163
    :cond_6
    iget-object v1, p0, Leqi;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 164
    const/4 v1, 0x7

    iget-object v2, p0, Leqi;->i:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 166
    :cond_7
    iget-object v1, p0, Leqi;->j:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 167
    const/16 v1, 0x8

    iget-object v2, p0, Leqi;->j:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 169
    :cond_8
    iget-object v1, p0, Leqi;->k:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 170
    const/16 v1, 0x9

    iget-object v2, p0, Leqi;->k:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 172
    :cond_9
    iget-object v1, p0, Leqi;->l:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    .line 173
    const/16 v1, 0xa

    iget-object v2, p0, Leqi;->l:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 175
    :cond_a
    iget-object v1, p0, Leqi;->m:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    .line 176
    const/16 v1, 0xb

    iget-object v2, p0, Leqi;->m:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 178
    :cond_b
    iget-object v1, p0, Leqi;->n:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    .line 179
    const/16 v1, 0xc

    iget-object v2, p0, Leqi;->n:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 181
    :cond_c
    iget-object v1, p0, Leqi;->o:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 182
    const/16 v1, 0xd

    iget-object v2, p0, Leqi;->o:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 184
    :cond_d
    iget-object v1, p0, Leqi;->p:Ljava/lang/String;

    if-eqz v1, :cond_e

    .line 185
    const/16 v1, 0xe

    iget-object v2, p0, Leqi;->p:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 187
    :cond_e
    iget-object v1, p0, Leqi;->q:[Leqk;

    if-eqz v1, :cond_10

    .line 188
    iget-object v2, p0, Leqi;->q:[Leqk;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_10

    aget-object v4, v2, v1

    .line 189
    if-eqz v4, :cond_f

    .line 190
    const/16 v5, 0xf

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 188
    :cond_f
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 194
    :cond_10
    iget-object v1, p0, Leqi;->r:Ljava/lang/String;

    if-eqz v1, :cond_11

    .line 195
    const/16 v1, 0x10

    iget-object v2, p0, Leqi;->r:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 197
    :cond_11
    iget-object v1, p0, Leqi;->s:Ljava/lang/Integer;

    if-eqz v1, :cond_12

    .line 198
    const/16 v1, 0x11

    iget-object v2, p0, Leqi;->s:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 200
    :cond_12
    iget-object v1, p0, Leqi;->t:[Lepu;

    if-eqz v1, :cond_14

    .line 201
    iget-object v2, p0, Leqi;->t:[Lepu;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_14

    aget-object v4, v2, v1

    .line 202
    if-eqz v4, :cond_13

    .line 203
    const/16 v5, 0x12

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 201
    :cond_13
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 207
    :cond_14
    iget-object v1, p0, Leqi;->u:Ljava/lang/Integer;

    if-eqz v1, :cond_15

    .line 208
    const/16 v1, 0x13

    iget-object v2, p0, Leqi;->u:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 210
    :cond_15
    iget-object v1, p0, Leqi;->v:Lepu;

    if-eqz v1, :cond_16

    .line 211
    const/16 v1, 0x14

    iget-object v2, p0, Leqi;->v:Lepu;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 213
    :cond_16
    iget-object v1, p0, Leqi;->w:[Ljava/lang/String;

    if-eqz v1, :cond_17

    .line 214
    iget-object v2, p0, Leqi;->w:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_3
    if-ge v1, v3, :cond_17

    aget-object v4, v2, v1

    .line 215
    const/16 v5, 0x15

    invoke-virtual {p1, v5, v4}, Lepl;->a(ILjava/lang/String;)V

    .line 214
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 218
    :cond_17
    iget-object v1, p0, Leqi;->x:[I

    if-eqz v1, :cond_18

    iget-object v1, p0, Leqi;->x:[I

    array-length v1, v1

    if-lez v1, :cond_18

    .line 219
    iget-object v2, p0, Leqi;->x:[I

    array-length v3, v2

    move v1, v0

    :goto_4
    if-ge v1, v3, :cond_18

    aget v4, v2, v1

    .line 220
    const/16 v5, 0x16

    invoke-virtual {p1, v5, v4}, Lepl;->a(II)V

    .line 219
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 223
    :cond_18
    iget-object v1, p0, Leqi;->y:[Lepu;

    if-eqz v1, :cond_1a

    .line 224
    iget-object v1, p0, Leqi;->y:[Lepu;

    array-length v2, v1

    :goto_5
    if-ge v0, v2, :cond_1a

    aget-object v3, v1, v0

    .line 225
    if-eqz v3, :cond_19

    .line 226
    const/16 v4, 0x17

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 224
    :cond_19
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 230
    :cond_1a
    iget-object v0, p0, Leqi;->z:Ljava/lang/Long;

    if-eqz v0, :cond_1b

    .line 231
    const/16 v0, 0x18

    iget-object v1, p0, Leqi;->z:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 233
    :cond_1b
    iget-object v0, p0, Leqi;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 235
    return-void
.end method
