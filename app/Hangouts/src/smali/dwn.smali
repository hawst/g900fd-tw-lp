.class public final Ldwn;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldwn;


# instance fields
.field public b:Ldvn;

.field public c:[Ldvo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15079
    const/4 v0, 0x0

    new-array v0, v0, [Ldwn;

    sput-object v0, Ldwn;->a:[Ldwn;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15080
    invoke-direct {p0}, Lepn;-><init>()V

    .line 15083
    const/4 v0, 0x0

    iput-object v0, p0, Ldwn;->b:Ldvn;

    .line 15086
    sget-object v0, Ldvo;->a:[Ldvo;

    iput-object v0, p0, Ldwn;->c:[Ldvo;

    .line 15080
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 15108
    iget-object v0, p0, Ldwn;->b:Ldvn;

    if-eqz v0, :cond_2

    .line 15109
    const/4 v0, 0x1

    iget-object v2, p0, Ldwn;->b:Ldvn;

    .line 15110
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 15112
    :goto_0
    iget-object v2, p0, Ldwn;->c:[Ldvo;

    if-eqz v2, :cond_1

    .line 15113
    iget-object v2, p0, Ldwn;->c:[Ldvo;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 15114
    if-eqz v4, :cond_0

    .line 15115
    const/4 v5, 0x2

    .line 15116
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 15113
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 15120
    :cond_1
    iget-object v1, p0, Ldwn;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15121
    iput v0, p0, Ldwn;->cachedSize:I

    .line 15122
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 15076
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldwn;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldwn;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldwn;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldwn;->b:Ldvn;

    if-nez v0, :cond_2

    new-instance v0, Ldvn;

    invoke-direct {v0}, Ldvn;-><init>()V

    iput-object v0, p0, Ldwn;->b:Ldvn;

    :cond_2
    iget-object v0, p0, Ldwn;->b:Ldvn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldwn;->c:[Ldvo;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldvo;

    iget-object v3, p0, Ldwn;->c:[Ldvo;

    if-eqz v3, :cond_3

    iget-object v3, p0, Ldwn;->c:[Ldvo;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Ldwn;->c:[Ldvo;

    :goto_2
    iget-object v2, p0, Ldwn;->c:[Ldvo;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Ldwn;->c:[Ldvo;

    new-instance v3, Ldvo;

    invoke-direct {v3}, Ldvo;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldwn;->c:[Ldvo;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Ldwn;->c:[Ldvo;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Ldwn;->c:[Ldvo;

    new-instance v3, Ldvo;

    invoke-direct {v3}, Ldvo;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldwn;->c:[Ldvo;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 15091
    iget-object v0, p0, Ldwn;->b:Ldvn;

    if-eqz v0, :cond_0

    .line 15092
    const/4 v0, 0x1

    iget-object v1, p0, Ldwn;->b:Ldvn;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 15094
    :cond_0
    iget-object v0, p0, Ldwn;->c:[Ldvo;

    if-eqz v0, :cond_2

    .line 15095
    iget-object v1, p0, Ldwn;->c:[Ldvo;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 15096
    if-eqz v3, :cond_1

    .line 15097
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 15095
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 15101
    :cond_2
    iget-object v0, p0, Ldwn;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 15103
    return-void
.end method
