.class public final Ldts;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldts;


# instance fields
.field public b:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4965
    const/4 v0, 0x0

    new-array v0, v0, [Ldts;

    sput-object v0, Ldts;->a:[Ldts;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4966
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 4982
    const/4 v0, 0x0

    .line 4983
    iget-object v1, p0, Ldts;->b:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 4984
    const/4 v0, 0x1

    iget-object v1, p0, Ldts;->b:Ljava/lang/Long;

    .line 4985
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lepl;->e(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4987
    :cond_0
    iget-object v1, p0, Ldts;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4988
    iput v0, p0, Ldts;->cachedSize:I

    .line 4989
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 4962
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldts;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldts;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldts;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldts;->b:Ljava/lang/Long;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 4973
    iget-object v0, p0, Ldts;->b:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 4974
    const/4 v0, 0x1

    iget-object v1, p0, Ldts;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 4976
    :cond_0
    iget-object v0, p0, Ldts;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 4978
    return-void
.end method
