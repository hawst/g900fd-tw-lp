.class public final Lanw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field final synthetic a:Z

.field final synthetic b:I

.field final synthetic c:Ljava/lang/Runnable;

.field final synthetic d:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;ZILjava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lanw;->d:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    iput-boolean p2, p0, Lanw;->a:Z

    iput p3, p0, Lanw;->b:I

    iput-object p4, p0, Lanw;->c:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 193
    iget-boolean v0, p0, Lanw;->a:Z

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lanw;->d:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    iget v1, p0, Lanw;->b:I

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->a(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;I)V

    .line 196
    :cond_0
    iget-object v0, p0, Lanw;->d:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->a(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;)Landroid/animation/ValueAnimator;

    .line 197
    iget-object v0, p0, Lanw;->c:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 198
    iget-object v0, p0, Lanw;->c:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 200
    :cond_1
    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 204
    invoke-direct {p0}, Lanw;->a()V

    .line 205
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 209
    invoke-direct {p0}, Lanw;->a()V

    .line 210
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 214
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 218
    iget-boolean v0, p0, Lanw;->a:Z

    if-nez v0, :cond_0

    .line 219
    iget-object v0, p0, Lanw;->d:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->a(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;I)V

    .line 221
    :cond_0
    return-void
.end method
