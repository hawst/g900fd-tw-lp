.class public final Ldws;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldws;


# instance fields
.field public b:Ldvm;

.field public c:Leir;

.field public d:Ljava/lang/Integer;

.field public e:Ldwt;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22434
    const/4 v0, 0x0

    new-array v0, v0, [Ldws;

    sput-object v0, Ldws;->a:[Ldws;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22435
    invoke-direct {p0}, Lepn;-><init>()V

    .line 22505
    iput-object v0, p0, Ldws;->b:Ldvm;

    .line 22508
    iput-object v0, p0, Ldws;->c:Leir;

    .line 22511
    iput-object v0, p0, Ldws;->d:Ljava/lang/Integer;

    .line 22514
    iput-object v0, p0, Ldws;->e:Ldwt;

    .line 22435
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 22537
    const/4 v0, 0x0

    .line 22538
    iget-object v1, p0, Ldws;->b:Ldvm;

    if-eqz v1, :cond_0

    .line 22539
    const/4 v0, 0x1

    iget-object v1, p0, Ldws;->b:Ldvm;

    .line 22540
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 22542
    :cond_0
    iget-object v1, p0, Ldws;->c:Leir;

    if-eqz v1, :cond_1

    .line 22543
    const/4 v1, 0x2

    iget-object v2, p0, Ldws;->c:Leir;

    .line 22544
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22546
    :cond_1
    iget-object v1, p0, Ldws;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 22547
    const/4 v1, 0x3

    iget-object v2, p0, Ldws;->d:Ljava/lang/Integer;

    .line 22548
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 22550
    :cond_2
    iget-object v1, p0, Ldws;->e:Ldwt;

    if-eqz v1, :cond_3

    .line 22551
    const/4 v1, 0x4

    iget-object v2, p0, Ldws;->e:Ldwt;

    .line 22552
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22554
    :cond_3
    iget-object v1, p0, Ldws;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22555
    iput v0, p0, Ldws;->cachedSize:I

    .line 22556
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 22431
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldws;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldws;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldws;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldws;->b:Ldvm;

    if-nez v0, :cond_2

    new-instance v0, Ldvm;

    invoke-direct {v0}, Ldvm;-><init>()V

    iput-object v0, p0, Ldws;->b:Ldvm;

    :cond_2
    iget-object v0, p0, Ldws;->b:Ldvm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldws;->c:Leir;

    if-nez v0, :cond_3

    new-instance v0, Leir;

    invoke-direct {v0}, Leir;-><init>()V

    iput-object v0, p0, Ldws;->c:Leir;

    :cond_3
    iget-object v0, p0, Ldws;->c:Leir;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    :cond_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldws;->d:Ljava/lang/Integer;

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldws;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldws;->e:Ldwt;

    if-nez v0, :cond_6

    new-instance v0, Ldwt;

    invoke-direct {v0}, Ldwt;-><init>()V

    iput-object v0, p0, Ldws;->e:Ldwt;

    :cond_6
    iget-object v0, p0, Ldws;->e:Ldwt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 22519
    iget-object v0, p0, Ldws;->b:Ldvm;

    if-eqz v0, :cond_0

    .line 22520
    const/4 v0, 0x1

    iget-object v1, p0, Ldws;->b:Ldvm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 22522
    :cond_0
    iget-object v0, p0, Ldws;->c:Leir;

    if-eqz v0, :cond_1

    .line 22523
    const/4 v0, 0x2

    iget-object v1, p0, Ldws;->c:Leir;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 22525
    :cond_1
    iget-object v0, p0, Ldws;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 22526
    const/4 v0, 0x3

    iget-object v1, p0, Ldws;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 22528
    :cond_2
    iget-object v0, p0, Ldws;->e:Ldwt;

    if-eqz v0, :cond_3

    .line 22529
    const/4 v0, 0x4

    iget-object v1, p0, Ldws;->e:Ldwt;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 22531
    :cond_3
    iget-object v0, p0, Ldws;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 22533
    return-void
.end method
