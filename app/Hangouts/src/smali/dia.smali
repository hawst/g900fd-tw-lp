.class public final Ldia;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldia;


# instance fields
.field public b:[I

.field public c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 174
    const/4 v0, 0x0

    new-array v0, v0, [Ldia;

    sput-object v0, Ldia;->a:[Ldia;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 175
    invoke-direct {p0}, Lepn;-><init>()V

    .line 194
    sget-object v0, Lept;->e:[I

    iput-object v0, p0, Ldia;->b:[I

    .line 175
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 216
    iget-object v1, p0, Ldia;->b:[I

    if-eqz v1, :cond_1

    iget-object v1, p0, Ldia;->b:[I

    array-length v1, v1

    if-lez v1, :cond_1

    .line 218
    iget-object v2, p0, Ldia;->b:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget v4, v2, v0

    .line 220
    invoke-static {v4}, Lepl;->f(I)I

    move-result v4

    add-int/2addr v1, v4

    .line 218
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 223
    :cond_0
    iget-object v0, p0, Ldia;->b:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    .line 225
    :cond_1
    iget-object v1, p0, Ldia;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 226
    const/4 v1, 0x2

    iget-object v2, p0, Ldia;->c:Ljava/lang/String;

    .line 227
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 229
    :cond_2
    iget-object v1, p0, Ldia;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 230
    iput v0, p0, Ldia;->cachedSize:I

    .line 231
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 171
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldia;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldia;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldia;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Ldia;->b:[I

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [I

    iget-object v2, p0, Ldia;->b:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Ldia;->b:[I

    :goto_1
    iget-object v1, p0, Ldia;->b:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Ldia;->b:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    aput v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Ldia;->b:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldia;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 201
    iget-object v0, p0, Ldia;->b:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldia;->b:[I

    array-length v0, v0

    if-lez v0, :cond_0

    .line 202
    iget-object v1, p0, Ldia;->b:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, v1, v0

    .line 203
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Lepl;->a(II)V

    .line 202
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 206
    :cond_0
    iget-object v0, p0, Ldia;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 207
    const/4 v0, 0x2

    iget-object v1, p0, Ldia;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 209
    :cond_1
    iget-object v0, p0, Ldia;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 211
    return-void
.end method
