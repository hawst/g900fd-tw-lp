.class public final Lduj;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lduj;


# instance fields
.field public b:Leir;

.field public c:Ljava/lang/Boolean;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Boolean;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17920
    const/4 v0, 0x0

    new-array v0, v0, [Lduj;

    sput-object v0, Lduj;->a:[Lduj;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 17921
    invoke-direct {p0}, Lepn;-><init>()V

    .line 17924
    iput-object v0, p0, Lduj;->b:Leir;

    .line 17929
    iput-object v0, p0, Lduj;->d:Ljava/lang/Integer;

    .line 17934
    iput-object v0, p0, Lduj;->f:Ljava/lang/Integer;

    .line 17921
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 17965
    const/4 v0, 0x0

    .line 17966
    iget-object v1, p0, Lduj;->b:Leir;

    if-eqz v1, :cond_0

    .line 17967
    const/4 v0, 0x1

    iget-object v1, p0, Lduj;->b:Leir;

    .line 17968
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 17970
    :cond_0
    iget-object v1, p0, Lduj;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 17971
    const/4 v1, 0x2

    iget-object v2, p0, Lduj;->c:Ljava/lang/Boolean;

    .line 17972
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 17974
    :cond_1
    iget-object v1, p0, Lduj;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 17975
    const/4 v1, 0x3

    iget-object v2, p0, Lduj;->d:Ljava/lang/Integer;

    .line 17976
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 17978
    :cond_2
    iget-object v1, p0, Lduj;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 17979
    const/4 v1, 0x4

    iget-object v2, p0, Lduj;->e:Ljava/lang/Boolean;

    .line 17980
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 17982
    :cond_3
    iget-object v1, p0, Lduj;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 17983
    const/4 v1, 0x5

    iget-object v2, p0, Lduj;->f:Ljava/lang/Integer;

    .line 17984
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 17986
    :cond_4
    iget-object v1, p0, Lduj;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 17987
    const/4 v1, 0x6

    iget-object v2, p0, Lduj;->g:Ljava/lang/Boolean;

    .line 17988
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 17990
    :cond_5
    iget-object v1, p0, Lduj;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 17991
    iput v0, p0, Lduj;->cachedSize:I

    .line 17992
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 17917
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lduj;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lduj;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lduj;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lduj;->b:Leir;

    if-nez v0, :cond_2

    new-instance v0, Leir;

    invoke-direct {v0}, Leir;-><init>()V

    iput-object v0, p0, Lduj;->b:Leir;

    :cond_2
    iget-object v0, p0, Lduj;->b:Leir;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lduj;->c:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_3

    if-ne v0, v3, :cond_4

    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lduj;->d:Ljava/lang/Integer;

    goto :goto_0

    :cond_4
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lduj;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lduj;->e:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_5

    if-eq v0, v3, :cond_5

    const/4 v1, 0x2

    if-eq v0, v1, :cond_5

    const/4 v1, 0x3

    if-ne v0, v1, :cond_6

    :cond_5
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lduj;->f:Ljava/lang/Integer;

    goto :goto_0

    :cond_6
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lduj;->f:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lduj;->g:Ljava/lang/Boolean;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 17941
    iget-object v0, p0, Lduj;->b:Leir;

    if-eqz v0, :cond_0

    .line 17942
    const/4 v0, 0x1

    iget-object v1, p0, Lduj;->b:Leir;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 17944
    :cond_0
    iget-object v0, p0, Lduj;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 17945
    const/4 v0, 0x2

    iget-object v1, p0, Lduj;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 17947
    :cond_1
    iget-object v0, p0, Lduj;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 17948
    const/4 v0, 0x3

    iget-object v1, p0, Lduj;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 17950
    :cond_2
    iget-object v0, p0, Lduj;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 17951
    const/4 v0, 0x4

    iget-object v1, p0, Lduj;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 17953
    :cond_3
    iget-object v0, p0, Lduj;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 17954
    const/4 v0, 0x5

    iget-object v1, p0, Lduj;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 17956
    :cond_4
    iget-object v0, p0, Lduj;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 17957
    const/4 v0, 0x6

    iget-object v1, p0, Lduj;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 17959
    :cond_5
    iget-object v0, p0, Lduj;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 17961
    return-void
.end method
