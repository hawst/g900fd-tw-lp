.class public final Ldem;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldem;


# instance fields
.field public b:Ldfi;

.field public c:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4631
    const/4 v0, 0x0

    new-array v0, v0, [Ldem;

    sput-object v0, Ldem;->a:[Ldem;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4632
    invoke-direct {p0}, Lepn;-><init>()V

    .line 4635
    const/4 v0, 0x0

    iput-object v0, p0, Ldem;->b:Ldfi;

    .line 4632
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 4654
    const/4 v0, 0x0

    .line 4655
    iget-object v1, p0, Ldem;->b:Ldfi;

    if-eqz v1, :cond_0

    .line 4656
    const/4 v0, 0x1

    iget-object v1, p0, Ldem;->b:Ldfi;

    .line 4657
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4659
    :cond_0
    iget-object v1, p0, Ldem;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 4660
    const/4 v1, 0x2

    iget-object v2, p0, Ldem;->c:Ljava/lang/Boolean;

    .line 4661
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 4663
    :cond_1
    iget-object v1, p0, Ldem;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4664
    iput v0, p0, Ldem;->cachedSize:I

    .line 4665
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 4628
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldem;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldem;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldem;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldem;->b:Ldfi;

    if-nez v0, :cond_2

    new-instance v0, Ldfi;

    invoke-direct {v0}, Ldfi;-><init>()V

    iput-object v0, p0, Ldem;->b:Ldfi;

    :cond_2
    iget-object v0, p0, Ldem;->b:Ldfi;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldem;->c:Ljava/lang/Boolean;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 4642
    iget-object v0, p0, Ldem;->b:Ldfi;

    if-eqz v0, :cond_0

    .line 4643
    const/4 v0, 0x1

    iget-object v1, p0, Ldem;->b:Ldfi;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 4645
    :cond_0
    iget-object v0, p0, Ldem;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 4646
    const/4 v0, 0x2

    iget-object v1, p0, Ldem;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 4648
    :cond_1
    iget-object v0, p0, Ldem;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 4650
    return-void
.end method
