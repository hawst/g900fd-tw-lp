.class public final Lacu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;)V
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lacu;->a:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 145
    invoke-static {}, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    const-string v0, "Babel"

    const-string v1, "updateButterBarsRunnable run"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    :cond_0
    iget-object v0, p0, Lacu;->a:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->c(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 165
    :cond_1
    return-void

    .line 153
    :cond_2
    const/4 v0, 0x0

    move v2, v3

    .line 154
    :goto_0
    iget-object v1, p0, Lacu;->a:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->d(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;)[Lacy;

    move-result-object v1

    array-length v1, v1

    if-ge v2, v1, :cond_1

    .line 155
    iget-object v1, p0, Lacu;->a:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->d(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;)[Lacy;

    move-result-object v1

    aget-object v1, v1, v2

    .line 156
    iget-object v4, v1, Lacy;->d:Landroid/view/View;

    .line 157
    if-nez v0, :cond_3

    invoke-virtual {v1}, Lacy;->a()Z

    move-result v5

    if-nez v5, :cond_4

    :cond_3
    iget-object v5, p0, Lacu;->a:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;

    .line 158
    invoke-static {v5}, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->a(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;)I

    move-result v5

    iget-object v6, p0, Lacu;->a:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;

    invoke-static {v6}, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->d(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;)[Lacy;

    move-result-object v6

    array-length v6, v6

    sub-int/2addr v6, v2

    if-ne v5, v6, :cond_5

    .line 160
    :cond_4
    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    move-object v0, v1

    .line 154
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 162
    :cond_5
    const/16 v1, 0x8

    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method
