.class public final Laza;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Landroid/database/Cursor;

.field final synthetic b:Lcom/google/android/apps/hangouts/phone/DebugActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/phone/DebugActivity;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 693
    iput-object p1, p0, Laza;->b:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    iput-object p2, p0, Laza;->a:Landroid/database/Cursor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 696
    iget-object v0, p0, Laza;->a:Landroid/database/Cursor;

    invoke-interface {v0, p3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 697
    iget-object v0, p0, Laza;->a:Landroid/database/Cursor;

    iget-object v1, p0, Laza;->a:Landroid/database/Cursor;

    const-string v3, "_id"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 699
    iget-object v0, p0, Laza;->b:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/DebugActivity;->e(Lcom/google/android/apps/hangouts/phone/DebugActivity;)Lyt;

    move-result-object v0

    invoke-virtual {v0}, Lyt;->e()Lzr;

    move-result-object v0

    const-string v1, "conversation_participants_view"

    const-string v3, "_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 709
    iget-object v0, p0, Laza;->b:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/phone/DebugActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v3, Lf;->eZ:I

    invoke-virtual {v0, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 710
    const v0, 0x102000a

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 711
    iget-object v4, p0, Laza;->b:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    invoke-static {v4, v1}, Lcom/google/android/apps/hangouts/phone/DebugActivity;->c(Lcom/google/android/apps/hangouts/phone/DebugActivity;Landroid/database/Cursor;)Landroid/widget/BaseAdapter;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 712
    iget-object v0, p0, Laza;->b:Lcom/google/android/apps/hangouts/phone/DebugActivity;

    invoke-static {v0, v3, v1, v2}, Lcom/google/android/apps/hangouts/phone/DebugActivity;->a(Lcom/google/android/apps/hangouts/phone/DebugActivity;Landroid/view/View;Landroid/database/Cursor;Ljava/lang/Runnable;)V

    .line 713
    return-void
.end method
