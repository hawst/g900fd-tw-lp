.class public final Lejd;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lejd;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Lejh;

.field public d:Leje;

.field public e:Lejf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 573
    const/4 v0, 0x0

    new-array v0, v0, [Lejd;

    sput-object v0, Lejd;->a:[Lejd;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 574
    invoke-direct {p0}, Lepn;-><init>()V

    .line 661
    iput-object v0, p0, Lejd;->b:Ljava/lang/Integer;

    .line 664
    iput-object v0, p0, Lejd;->c:Lejh;

    .line 667
    iput-object v0, p0, Lejd;->d:Leje;

    .line 670
    iput-object v0, p0, Lejd;->e:Lejf;

    .line 574
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 693
    const/4 v0, 0x0

    .line 694
    iget-object v1, p0, Lejd;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 695
    const/4 v0, 0x1

    iget-object v1, p0, Lejd;->b:Ljava/lang/Integer;

    .line 696
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 698
    :cond_0
    iget-object v1, p0, Lejd;->c:Lejh;

    if-eqz v1, :cond_1

    .line 699
    const/4 v1, 0x2

    iget-object v2, p0, Lejd;->c:Lejh;

    .line 700
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 702
    :cond_1
    iget-object v1, p0, Lejd;->d:Leje;

    if-eqz v1, :cond_2

    .line 703
    const/4 v1, 0x3

    iget-object v2, p0, Lejd;->d:Leje;

    .line 704
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 706
    :cond_2
    iget-object v1, p0, Lejd;->e:Lejf;

    if-eqz v1, :cond_3

    .line 707
    const/4 v1, 0x4

    iget-object v2, p0, Lejd;->e:Lejf;

    .line 708
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 710
    :cond_3
    iget-object v1, p0, Lejd;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 711
    iput v0, p0, Lejd;->cachedSize:I

    .line 712
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 570
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lejd;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lejd;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lejd;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lejd;->b:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lejd;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lejd;->c:Lejh;

    if-nez v0, :cond_4

    new-instance v0, Lejh;

    invoke-direct {v0}, Lejh;-><init>()V

    iput-object v0, p0, Lejd;->c:Lejh;

    :cond_4
    iget-object v0, p0, Lejd;->c:Lejh;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lejd;->d:Leje;

    if-nez v0, :cond_5

    new-instance v0, Leje;

    invoke-direct {v0}, Leje;-><init>()V

    iput-object v0, p0, Lejd;->d:Leje;

    :cond_5
    iget-object v0, p0, Lejd;->d:Leje;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lejd;->e:Lejf;

    if-nez v0, :cond_6

    new-instance v0, Lejf;

    invoke-direct {v0}, Lejf;-><init>()V

    iput-object v0, p0, Lejd;->e:Lejf;

    :cond_6
    iget-object v0, p0, Lejd;->e:Lejf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 675
    iget-object v0, p0, Lejd;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 676
    const/4 v0, 0x1

    iget-object v1, p0, Lejd;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 678
    :cond_0
    iget-object v0, p0, Lejd;->c:Lejh;

    if-eqz v0, :cond_1

    .line 679
    const/4 v0, 0x2

    iget-object v1, p0, Lejd;->c:Lejh;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 681
    :cond_1
    iget-object v0, p0, Lejd;->d:Leje;

    if-eqz v0, :cond_2

    .line 682
    const/4 v0, 0x3

    iget-object v1, p0, Lejd;->d:Leje;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 684
    :cond_2
    iget-object v0, p0, Lejd;->e:Lejf;

    if-eqz v0, :cond_3

    .line 685
    const/4 v0, 0x4

    iget-object v1, p0, Lejd;->e:Lejf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 687
    :cond_3
    iget-object v0, p0, Lejd;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 689
    return-void
.end method
