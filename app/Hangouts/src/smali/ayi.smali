.class public final Layi;
.super Layf;
.source "PG"


# instance fields
.field private d:Layk;

.field private final e:Lbme;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 45
    sget v0, Lf;->fj:I

    const/4 v1, 0x2

    new-array v1, v1, [I

    const/4 v2, 0x0

    sget v3, Lg;->ck:I

    aput v3, v1, v2

    const/4 v2, 0x1

    sget v3, Lg;->cl:I

    aput v3, v1, v2

    invoke-direct {p0, v0, v1}, Layf;-><init>(I[I)V

    .line 42
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lbme;

    invoke-static {v0, v1}, Lcyj;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbme;

    iput-object v0, p0, Layi;->e:Lbme;

    .line 46
    sget-object v0, Layk;->a:Layk;

    iput-object v0, p0, Layi;->d:Layk;

    .line 47
    return-void
.end method

.method public static b(Lyj;Landroid/app/Activity;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 128
    invoke-static {}, Lf;->v()Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "babel_voip_pstn_fmf"

    .line 130
    invoke-static {v2, v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 133
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "fmf_promo_shown"

    .line 134
    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_2

    .line 136
    invoke-static {}, Lbzd;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p1}, Lf;->c(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 138
    :cond_0
    invoke-virtual {p0}, Lyj;->ai()Z

    move-result v2

    if-nez v2, :cond_2

    .line 140
    invoke-static {p0}, Lf;->d(Lyj;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 142
    invoke-virtual {p0}, Lyj;->ae()Z

    move-result v2

    if-nez v2, :cond_1

    .line 143
    invoke-virtual {p0}, Lyj;->af()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 144
    invoke-virtual {p0}, Lyj;->ag()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 145
    invoke-virtual {p0}, Lyj;->ah()Z

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x7

    return v0
.end method

.method protected a(I)V
    .locals 3

    .prologue
    .line 150
    iget-object v0, p0, Layi;->c:Landroid/app/Activity;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 151
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "fmf_promo_shown"

    const/4 v2, 0x1

    .line 152
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 153
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 154
    sget v0, Lg;->cl:I

    if-ne p1, v0, :cond_1

    .line 155
    sget-object v0, Layj;->a:[I

    iget-object v1, p0, Layi;->d:Layk;

    invoke-virtual {v1}, Layk;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 199
    :goto_0
    invoke-super {p0, p1}, Layf;->a(I)V

    .line 200
    return-void

    .line 158
    :pswitch_0
    const-string v0, "babel_voip_pstn_calling_required_package_name"

    const-string v1, "com.google.android.apps.hangoutsdialer"

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 161
    iget-object v1, p0, Layi;->c:Landroid/app/Activity;

    invoke-static {v1, v0}, Lbbl;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 162
    const-string v0, "Babel"

    const-string v1, "Unable to launch play store intent."

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    :cond_0
    iget-object v0, p0, Layi;->e:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v1, 0x707

    .line 165
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 164
    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    goto :goto_0

    .line 169
    :pswitch_1
    iget-object v0, p0, Layi;->c:Landroid/app/Activity;

    iget-object v1, p0, Layi;->b:Lyj;

    invoke-static {v1}, Lbbl;->o(Lyj;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 170
    iget-object v0, p0, Layi;->e:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v1, 0x70b

    .line 171
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 170
    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    goto :goto_0

    .line 175
    :pswitch_2
    iget-object v0, p0, Layi;->c:Landroid/app/Activity;

    const-string v1, "babel_google_voice_add_min_balance_url"

    const-string v2, "https://www.google.com/voice/m/credit?p=add_credit&rd=1&f=1&b=1"

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lbbl;->h(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 178
    iget-object v0, p0, Layi;->e:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v1, 0x709

    .line 179
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 178
    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    goto :goto_0

    .line 184
    :cond_1
    sget-object v0, Layj;->a:[I

    iget-object v1, p0, Layi;->d:Layk;

    invoke-virtual {v1}, Layk;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 190
    :pswitch_3
    iget-object v0, p0, Layi;->e:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v1, 0x70a

    .line 191
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 190
    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    goto/16 :goto_0

    .line 186
    :pswitch_4
    iget-object v0, p0, Layi;->e:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v1, 0x706

    .line 187
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 186
    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    goto/16 :goto_0

    .line 194
    :pswitch_5
    iget-object v0, p0, Layi;->e:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v1, 0x708

    .line 195
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 194
    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    goto/16 :goto_0

    .line 155
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch

    .line 184
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method public a(Lyj;Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 114
    if-eqz p1, :cond_0

    invoke-static {p1, p2}, Layi;->b(Lyj;Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 119
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    invoke-super {p0, p1, p2}, Layf;->a(Lyj;Landroid/app/Activity;)V

    goto :goto_0
.end method

.method public b()Landroid/view/View;
    .locals 7

    .prologue
    .line 56
    invoke-super {p0}, Layf;->b()Landroid/view/View;

    move-result-object v4

    .line 59
    sget v0, Lg;->ci:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 60
    const-string v1, "android_first_minute"

    const-string v2, "https://www.google.com/support/hangouts/?hl=%locale%"

    invoke-static {v2, v1}, Lf;->b(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 61
    iget-object v2, p0, Layi;->c:Landroid/app/Activity;

    sget v3, Lh;->cL:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v6

    invoke-virtual {v2, v3, v5}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 62
    iget-object v2, p0, Layi;->c:Landroid/app/Activity;

    invoke-static {v0, v2, v1}, Lf;->a(Landroid/widget/TextView;Landroid/app/Activity;Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Layi;->c:Landroid/app/Activity;

    invoke-static {v0}, Lf;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Layk;->a:Layk;

    :goto_0
    iput-object v0, p0, Layi;->d:Layk;

    .line 66
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FMF Promo type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Layi;->d:Layk;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    sget v0, Lg;->cj:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 72
    sget-object v1, Layj;->a:[I

    iget-object v2, p0, Layi;->d:Layk;

    invoke-virtual {v2}, Layk;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 86
    sget v1, Lh;->cN:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 87
    iget-object v0, p0, Layi;->c:Landroid/app/Activity;

    sget v1, Lh;->cQ:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 88
    iget-object v0, p0, Layi;->c:Landroid/app/Activity;

    sget v2, Lh;->cO:I

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    move-object v3, v1

    .line 94
    :goto_1
    sget v0, Lg;->ck:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 95
    sget v1, Lg;->cl:I

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 97
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0xe

    if-lt v5, v6, :cond_3

    .line 98
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 99
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 109
    :goto_2
    return-object v4

    .line 65
    :cond_0
    iget-object v0, p0, Layi;->b:Lyj;

    invoke-virtual {v0}, Lyj;->ae()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Layk;->c:Layk;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Layi;->b:Lyj;

    invoke-virtual {v0}, Lyj;->af()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Layi;->b:Lyj;

    invoke-virtual {v0}, Lyj;->ag()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Layk;->b:Layk;

    goto :goto_0

    :cond_2
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected FMF state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Layi;->b:Lyj;

    invoke-virtual {v2}, Lyj;->af()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Layi;->b:Lyj;

    invoke-virtual {v2}, Lyj;->ag()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Layi;->b:Lyj;

    invoke-virtual {v2}, Lyj;->ah()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Unexpected FMF state"

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    sget-object v0, Layk;->c:Layk;

    goto/16 :goto_0

    .line 74
    :pswitch_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 75
    iget-object v0, p0, Layi;->c:Landroid/app/Activity;

    sget v1, Lh;->cJ:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 76
    iget-object v0, p0, Layi;->c:Landroid/app/Activity;

    sget v2, Lh;->cP:I

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    move-object v3, v1

    .line 77
    goto/16 :goto_1

    .line 79
    :pswitch_1
    sget v1, Lh;->cM:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 80
    iget-object v0, p0, Layi;->c:Landroid/app/Activity;

    sget v1, Lh;->cJ:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 81
    iget-object v0, p0, Layi;->c:Landroid/app/Activity;

    sget v2, Lh;->cK:I

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    move-object v3, v1

    .line 82
    goto/16 :goto_1

    .line 103
    :cond_3
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 104
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 105
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 106
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 72
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
