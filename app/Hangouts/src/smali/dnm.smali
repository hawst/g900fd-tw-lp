.class public final Ldnm;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldnm;


# instance fields
.field public b:Ldnt;

.field public c:Ljava/lang/String;

.field public d:[Ljava/lang/String;

.field public e:Ldnn;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1048
    const/4 v0, 0x0

    new-array v0, v0, [Ldnm;

    sput-object v0, Ldnm;->a:[Ldnm;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1049
    invoke-direct {p0}, Lepn;-><init>()V

    .line 1134
    iput-object v1, p0, Ldnm;->b:Ldnt;

    .line 1139
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Ldnm;->d:[Ljava/lang/String;

    .line 1142
    iput-object v1, p0, Ldnm;->e:Ldnn;

    .line 1049
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1168
    iget-object v0, p0, Ldnm;->b:Ldnt;

    if-eqz v0, :cond_4

    .line 1169
    const/4 v0, 0x1

    iget-object v2, p0, Ldnm;->b:Ldnt;

    .line 1170
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1172
    :goto_0
    iget-object v2, p0, Ldnm;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 1173
    const/4 v2, 0x2

    iget-object v3, p0, Ldnm;->c:Ljava/lang/String;

    .line 1174
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1176
    :cond_0
    iget-object v2, p0, Ldnm;->d:[Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Ldnm;->d:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 1178
    iget-object v3, p0, Ldnm;->d:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 1180
    invoke-static {v5}, Lepl;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 1178
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1182
    :cond_1
    add-int/2addr v0, v2

    .line 1183
    iget-object v1, p0, Ldnm;->d:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1185
    :cond_2
    iget-object v1, p0, Ldnm;->e:Ldnn;

    if-eqz v1, :cond_3

    .line 1186
    const/4 v1, 0x4

    iget-object v2, p0, Ldnm;->e:Ldnn;

    .line 1187
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1189
    :cond_3
    iget-object v1, p0, Ldnm;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1190
    iput v0, p0, Ldnm;->cachedSize:I

    .line 1191
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1045
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldnm;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldnm;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldnm;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldnm;->b:Ldnt;

    if-nez v0, :cond_2

    new-instance v0, Ldnt;

    invoke-direct {v0}, Ldnt;-><init>()V

    iput-object v0, p0, Ldnm;->b:Ldnt;

    :cond_2
    iget-object v0, p0, Ldnm;->b:Ldnt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldnm;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Ldnm;->d:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    iget-object v2, p0, Ldnm;->d:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Ldnm;->d:[Ljava/lang/String;

    :goto_1
    iget-object v1, p0, Ldnm;->d:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Ldnm;->d:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v1, p0, Ldnm;->d:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldnm;->e:Ldnn;

    if-nez v0, :cond_4

    new-instance v0, Ldnn;

    invoke-direct {v0}, Ldnn;-><init>()V

    iput-object v0, p0, Ldnm;->e:Ldnn;

    :cond_4
    iget-object v0, p0, Ldnm;->e:Ldnn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 1147
    iget-object v0, p0, Ldnm;->b:Ldnt;

    if-eqz v0, :cond_0

    .line 1148
    const/4 v0, 0x1

    iget-object v1, p0, Ldnm;->b:Ldnt;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1150
    :cond_0
    iget-object v0, p0, Ldnm;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1151
    const/4 v0, 0x2

    iget-object v1, p0, Ldnm;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1153
    :cond_1
    iget-object v0, p0, Ldnm;->d:[Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1154
    iget-object v1, p0, Ldnm;->d:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 1155
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lepl;->a(ILjava/lang/String;)V

    .line 1154
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1158
    :cond_2
    iget-object v0, p0, Ldnm;->e:Ldnn;

    if-eqz v0, :cond_3

    .line 1159
    const/4 v0, 0x4

    iget-object v1, p0, Ldnm;->e:Ldnn;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1161
    :cond_3
    iget-object v0, p0, Ldnm;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1163
    return-void
.end method
