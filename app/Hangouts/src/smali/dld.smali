.class public final Ldld;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldld;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:[Ljava/lang/String;

.field public d:Ljava/lang/Long;

.field public e:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2004
    const/4 v0, 0x0

    new-array v0, v0, [Ldld;

    sput-object v0, Ldld;->a:[Ldld;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2005
    invoke-direct {p0}, Lepn;-><init>()V

    .line 2010
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Ldld;->c:[Ljava/lang/String;

    .line 2005
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2040
    iget-object v0, p0, Ldld;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 2041
    const/4 v0, 0x1

    iget-object v2, p0, Ldld;->b:Ljava/lang/Integer;

    .line 2042
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2044
    :goto_0
    iget-object v2, p0, Ldld;->c:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Ldld;->c:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 2046
    iget-object v3, p0, Ldld;->c:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 2048
    invoke-static {v5}, Lepl;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 2046
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2050
    :cond_0
    add-int/2addr v0, v2

    .line 2051
    iget-object v1, p0, Ldld;->c:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2053
    :cond_1
    iget-object v1, p0, Ldld;->d:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 2054
    const/4 v1, 0x3

    iget-object v2, p0, Ldld;->d:Ljava/lang/Long;

    .line 2055
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2057
    :cond_2
    iget-object v1, p0, Ldld;->e:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 2058
    const/4 v1, 0x4

    iget-object v2, p0, Ldld;->e:Ljava/lang/Long;

    .line 2059
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2061
    :cond_3
    iget-object v1, p0, Ldld;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2062
    iput v0, p0, Ldld;->cachedSize:I

    .line 2063
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2001
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldld;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldld;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldld;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldld;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Ldld;->c:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    iget-object v2, p0, Ldld;->c:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Ldld;->c:[Ljava/lang/String;

    :goto_1
    iget-object v1, p0, Ldld;->c:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Ldld;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Ldld;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldld;->d:Ljava/lang/Long;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldld;->e:Ljava/lang/Long;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 2019
    iget-object v0, p0, Ldld;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 2020
    const/4 v0, 0x1

    iget-object v1, p0, Ldld;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 2022
    :cond_0
    iget-object v0, p0, Ldld;->c:[Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2023
    iget-object v1, p0, Ldld;->c:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 2024
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->a(ILjava/lang/String;)V

    .line 2023
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2027
    :cond_1
    iget-object v0, p0, Ldld;->d:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 2028
    const/4 v0, 0x3

    iget-object v1, p0, Ldld;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 2030
    :cond_2
    iget-object v0, p0, Ldld;->e:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 2031
    const/4 v0, 0x4

    iget-object v1, p0, Ldld;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 2033
    :cond_3
    iget-object v0, p0, Ldld;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 2035
    return-void
.end method
