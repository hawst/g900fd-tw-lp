.class public final Lejp;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lejp;


# instance fields
.field public b:[Ljava/lang/Integer;

.field public c:Lejr;

.field public d:Lejr;

.field public e:Lejq;

.field public f:[Ljava/lang/Double;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x0

    new-array v0, v0, [Lejp;

    sput-object v0, Lejp;->a:[Lejp;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 161
    invoke-direct {p0}, Lepn;-><init>()V

    .line 362
    sget-object v0, Lept;->m:[Ljava/lang/Integer;

    iput-object v0, p0, Lejp;->b:[Ljava/lang/Integer;

    .line 365
    iput-object v1, p0, Lejp;->c:Lejr;

    .line 368
    iput-object v1, p0, Lejp;->d:Lejr;

    .line 371
    iput-object v1, p0, Lejp;->e:Lejq;

    .line 374
    sget-object v0, Lept;->p:[Ljava/lang/Double;

    iput-object v0, p0, Lejp;->f:[Ljava/lang/Double;

    .line 161
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 405
    iget-object v1, p0, Lejp;->b:[Ljava/lang/Integer;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lejp;->b:[Ljava/lang/Integer;

    array-length v1, v1

    if-lez v1, :cond_1

    .line 407
    iget-object v2, p0, Lejp;->b:[Ljava/lang/Integer;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 409
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Lepl;->f(I)I

    move-result v4

    add-int/2addr v1, v4

    .line 407
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 412
    :cond_0
    iget-object v0, p0, Lejp;->b:[Ljava/lang/Integer;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    .line 414
    :cond_1
    iget-object v1, p0, Lejp;->c:Lejr;

    if-eqz v1, :cond_2

    .line 415
    const/4 v1, 0x2

    iget-object v2, p0, Lejp;->c:Lejr;

    .line 416
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 418
    :cond_2
    iget-object v1, p0, Lejp;->d:Lejr;

    if-eqz v1, :cond_3

    .line 419
    const/4 v1, 0x3

    iget-object v2, p0, Lejp;->d:Lejr;

    .line 420
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 422
    :cond_3
    iget-object v1, p0, Lejp;->e:Lejq;

    if-eqz v1, :cond_4

    .line 423
    const/4 v1, 0x4

    iget-object v2, p0, Lejp;->e:Lejq;

    .line 424
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 426
    :cond_4
    iget-object v1, p0, Lejp;->f:[Ljava/lang/Double;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lejp;->f:[Ljava/lang/Double;

    array-length v1, v1

    if-lez v1, :cond_5

    .line 427
    iget-object v1, p0, Lejp;->f:[Ljava/lang/Double;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x8

    .line 428
    add-int/2addr v0, v1

    .line 429
    iget-object v1, p0, Lejp;->f:[Ljava/lang/Double;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 431
    :cond_5
    iget-object v1, p0, Lejp;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 432
    iput v0, p0, Lejp;->cachedSize:I

    .line 433
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 157
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lejp;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lejp;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lejp;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Lejp;->b:[Ljava/lang/Integer;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Integer;

    iget-object v2, p0, Lejp;->b:[Ljava/lang/Integer;

    invoke-static {v2, v4, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lejp;->b:[Ljava/lang/Integer;

    :goto_1
    iget-object v1, p0, Lejp;->b:[Ljava/lang/Integer;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lejp;->b:[Ljava/lang/Integer;

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lejp;->b:[Ljava/lang/Integer;

    invoke-virtual {p1}, Lepk;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lejp;->c:Lejr;

    if-nez v0, :cond_3

    new-instance v0, Lejr;

    invoke-direct {v0}, Lejr;-><init>()V

    iput-object v0, p0, Lejp;->c:Lejr;

    :cond_3
    iget-object v0, p0, Lejp;->c:Lejr;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lejp;->d:Lejr;

    if-nez v0, :cond_4

    new-instance v0, Lejr;

    invoke-direct {v0}, Lejr;-><init>()V

    iput-object v0, p0, Lejp;->d:Lejr;

    :cond_4
    iget-object v0, p0, Lejp;->d:Lejr;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lejp;->e:Lejq;

    if-nez v0, :cond_5

    new-instance v0, Lejq;

    invoke-direct {v0}, Lejq;-><init>()V

    iput-object v0, p0, Lejp;->e:Lejq;

    :cond_5
    iget-object v0, p0, Lejp;->e:Lejq;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_5
    const/16 v0, 0x29

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Lejp;->f:[Ljava/lang/Double;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Double;

    iget-object v2, p0, Lejp;->f:[Ljava/lang/Double;

    invoke-static {v2, v4, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lejp;->f:[Ljava/lang/Double;

    :goto_2
    iget-object v1, p0, Lejp;->f:[Ljava/lang/Double;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_6

    iget-object v1, p0, Lejp;->f:[Ljava/lang/Double;

    invoke-virtual {p1}, Lepk;->b()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_6
    iget-object v1, p0, Lejp;->f:[Ljava/lang/Double;

    invoke-virtual {p1}, Lepk;->b()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x29 -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 379
    iget-object v1, p0, Lejp;->b:[Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 380
    iget-object v2, p0, Lejp;->b:[Ljava/lang/Integer;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 381
    const/4 v5, 0x1

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p1, v5, v4}, Lepl;->a(II)V

    .line 380
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 384
    :cond_0
    iget-object v1, p0, Lejp;->c:Lejr;

    if-eqz v1, :cond_1

    .line 385
    const/4 v1, 0x2

    iget-object v2, p0, Lejp;->c:Lejr;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 387
    :cond_1
    iget-object v1, p0, Lejp;->d:Lejr;

    if-eqz v1, :cond_2

    .line 388
    const/4 v1, 0x3

    iget-object v2, p0, Lejp;->d:Lejr;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 390
    :cond_2
    iget-object v1, p0, Lejp;->e:Lejq;

    if-eqz v1, :cond_3

    .line 391
    const/4 v1, 0x4

    iget-object v2, p0, Lejp;->e:Lejq;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 393
    :cond_3
    iget-object v1, p0, Lejp;->f:[Ljava/lang/Double;

    if-eqz v1, :cond_4

    .line 394
    iget-object v1, p0, Lejp;->f:[Ljava/lang/Double;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 395
    const/4 v4, 0x5

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    invoke-virtual {p1, v4, v5, v6}, Lepl;->a(ID)V

    .line 394
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 398
    :cond_4
    iget-object v0, p0, Lejp;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 400
    return-void
.end method
