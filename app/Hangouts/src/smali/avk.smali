.class public final Lavk;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Z

.field private static b:Lvd;

.field private static final c:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lbys;->f:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lavk;->a:Z

    .line 31
    const/4 v0, 0x0

    sput-object v0, Lavk;->b:Lvd;

    .line 32
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lavk;->c:Ljava/lang/Object;

    return-void
.end method

.method public static a()Lvd;
    .locals 7

    .prologue
    .line 35
    sget-object v0, Lavk;->b:Lvd;

    if-nez v0, :cond_2

    .line 36
    sget-object v1, Lavk;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 37
    :try_start_0
    sget-object v0, Lavk;->b:Lvd;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v0, :cond_1

    .line 39
    :try_start_1
    const-string v0, "VolleyUtils RQ initialization"

    invoke-static {v0}, Lbzq;->a(Ljava/lang/String;)V

    .line 40
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 45
    :try_start_2
    invoke-static {v0}, Lcwt;->a(Landroid/content/Context;)V
    :try_end_2
    .catch Lcfy; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcfx; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 52
    :goto_0
    :try_start_3
    invoke-static {}, Lf;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lcjj;->a(Ljava/lang/String;Landroid/content/Context;)Lcjj;

    move-result-object v2

    .line 53
    sget-boolean v3, Lavk;->a:Z

    if-eqz v3, :cond_0

    .line 54
    const-string v3, "Babel"

    invoke-virtual {v2, v3}, Lcjj;->a(Ljava/lang/String;)V

    .line 55
    const-string v3, "Babel"

    const-string v4, "initialize Volley request queue"

    invoke-static {v3, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    :cond_0
    new-instance v3, Lvp;

    new-instance v4, Lvs;

    invoke-direct {v4, v2}, Lvs;-><init>(Lorg/apache/http/client/HttpClient;)V

    invoke-direct {v3, v4}, Lvp;-><init>(Lvt;)V

    .line 58
    new-instance v2, Lavi;

    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "babel_volley_cache_size"

    const/high16 v6, 0x3200000

    invoke-static {v4, v5, v6}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    invoke-direct {v2, v0, v4}, Lavi;-><init>(Ljava/io/File;I)V

    .line 59
    new-instance v0, Lvd;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "babel_thread_pool_size"

    const/4 v6, 0x2

    invoke-static {v4, v5, v6}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    invoke-direct {v0, v2, v3, v4}, Lvd;-><init>(Luo;Luu;I)V

    .line 60
    sput-object v0, Lavk;->b:Lvd;

    invoke-virtual {v0}, Lvd;->a()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 62
    :try_start_4
    invoke-static {}, Lbzq;->a()V

    .line 65
    :cond_1
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 68
    :cond_2
    sget-object v0, Lavk;->b:Lvd;

    return-object v0

    .line 46
    :catch_0
    move-exception v2

    .line 47
    :try_start_5
    const-string v3, "Babel"

    const-string v4, "Temporarily unable to update security library"

    invoke-static {v3, v4, v2}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 48
    invoke-virtual {v2}, Lcfy;->a()I

    move-result v2

    invoke-static {v2, v0}, Lcfz;->a(ILandroid/content/Context;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 62
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {}, Lbzq;->a()V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 65
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    .line 49
    :catch_1
    move-exception v2

    .line 50
    :try_start_7
    const-string v3, "Babel"

    const-string v4, "Permanently unable to update security library"

    invoke-static {v3, v4, v2}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 72
    invoke-static {}, Lavk;->a()Lvd;

    .line 73
    sget-object v0, Lavk;->b:Lvd;

    invoke-virtual {v0}, Lvd;->b()Luo;

    move-result-object v0

    invoke-interface {v0, p0}, Luo;->b(Ljava/lang/String;)V

    .line 74
    return-void
.end method
