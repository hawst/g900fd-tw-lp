.class public final Ldxf;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldxf;


# instance fields
.field public b:Ldvm;

.field public c:Ljava/lang/Long;

.field public d:[Ldtp;

.field public e:[Ldxl;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/Boolean;

.field public h:Ljava/lang/Integer;

.field public i:Ljava/lang/Integer;

.field public j:[[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10002
    const/4 v0, 0x0

    new-array v0, v0, [Ldxf;

    sput-object v0, Ldxf;->a:[Ldxf;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10003
    invoke-direct {p0}, Lepn;-><init>()V

    .line 10006
    iput-object v1, p0, Ldxf;->b:Ldvm;

    .line 10011
    sget-object v0, Ldtp;->a:[Ldtp;

    iput-object v0, p0, Ldxf;->d:[Ldtp;

    .line 10014
    sget-object v0, Ldxl;->a:[Ldxl;

    iput-object v0, p0, Ldxf;->e:[Ldxl;

    .line 10021
    iput-object v1, p0, Ldxf;->h:Ljava/lang/Integer;

    .line 10026
    sget-object v0, Lept;->k:[[B

    iput-object v0, p0, Ldxf;->j:[[B

    .line 10003
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 10075
    iget-object v0, p0, Ldxf;->b:Ldvm;

    if-eqz v0, :cond_b

    .line 10076
    const/4 v0, 0x1

    iget-object v2, p0, Ldxf;->b:Ldvm;

    .line 10077
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 10079
    :goto_0
    iget-object v2, p0, Ldxf;->c:Ljava/lang/Long;

    if-eqz v2, :cond_0

    .line 10080
    const/4 v2, 0x2

    iget-object v3, p0, Ldxf;->c:Ljava/lang/Long;

    .line 10081
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 10083
    :cond_0
    iget-object v2, p0, Ldxf;->d:[Ldtp;

    if-eqz v2, :cond_2

    .line 10084
    iget-object v3, p0, Ldxf;->d:[Ldtp;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 10085
    if-eqz v5, :cond_1

    .line 10086
    const/4 v6, 0x3

    .line 10087
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 10084
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 10091
    :cond_2
    iget-object v2, p0, Ldxf;->i:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    .line 10092
    const/4 v2, 0x4

    iget-object v3, p0, Ldxf;->i:Ljava/lang/Integer;

    .line 10093
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 10095
    :cond_3
    iget-object v2, p0, Ldxf;->j:[[B

    if-eqz v2, :cond_5

    iget-object v2, p0, Ldxf;->j:[[B

    array-length v2, v2

    if-lez v2, :cond_5

    .line 10097
    iget-object v4, p0, Ldxf;->j:[[B

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_2
    if-ge v2, v5, :cond_4

    aget-object v6, v4, v2

    .line 10099
    invoke-static {v6}, Lepl;->b([B)I

    move-result v6

    add-int/2addr v3, v6

    .line 10097
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 10101
    :cond_4
    add-int/2addr v0, v3

    .line 10102
    iget-object v2, p0, Ldxf;->j:[[B

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 10104
    :cond_5
    iget-object v2, p0, Ldxf;->g:Ljava/lang/Boolean;

    if-eqz v2, :cond_6

    .line 10105
    const/4 v2, 0x6

    iget-object v3, p0, Ldxf;->g:Ljava/lang/Boolean;

    .line 10106
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 10108
    :cond_6
    iget-object v2, p0, Ldxf;->e:[Ldxl;

    if-eqz v2, :cond_8

    .line 10109
    iget-object v2, p0, Ldxf;->e:[Ldxl;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 10110
    if-eqz v4, :cond_7

    .line 10111
    const/4 v5, 0x7

    .line 10112
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 10109
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 10116
    :cond_8
    iget-object v1, p0, Ldxf;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 10117
    const/16 v1, 0x8

    iget-object v2, p0, Ldxf;->f:Ljava/lang/Integer;

    .line 10118
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 10120
    :cond_9
    iget-object v1, p0, Ldxf;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_a

    .line 10121
    const/16 v1, 0x9

    iget-object v2, p0, Ldxf;->h:Ljava/lang/Integer;

    .line 10122
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 10124
    :cond_a
    iget-object v1, p0, Ldxf;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10125
    iput v0, p0, Ldxf;->cachedSize:I

    .line 10126
    return v0

    :cond_b
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 9999
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldxf;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldxf;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldxf;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldxf;->b:Ldvm;

    if-nez v0, :cond_2

    new-instance v0, Ldvm;

    invoke-direct {v0}, Ldvm;-><init>()V

    iput-object v0, p0, Ldxf;->b:Ldvm;

    :cond_2
    iget-object v0, p0, Ldxf;->b:Ldvm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldxf;->c:Ljava/lang/Long;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldxf;->d:[Ldtp;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldtp;

    iget-object v3, p0, Ldxf;->d:[Ldtp;

    if-eqz v3, :cond_3

    iget-object v3, p0, Ldxf;->d:[Ldtp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Ldxf;->d:[Ldtp;

    :goto_2
    iget-object v2, p0, Ldxf;->d:[Ldtp;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Ldxf;->d:[Ldtp;

    new-instance v3, Ldtp;

    invoke-direct {v3}, Ldtp;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldxf;->d:[Ldtp;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Ldxf;->d:[Ldtp;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Ldxf;->d:[Ldtp;

    new-instance v3, Ldtp;

    invoke-direct {v3}, Ldtp;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldxf;->d:[Ldtp;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldxf;->i:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldxf;->j:[[B

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [[B

    iget-object v3, p0, Ldxf;->j:[[B

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Ldxf;->j:[[B

    :goto_3
    iget-object v2, p0, Ldxf;->j:[[B

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Ldxf;->j:[[B

    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_6
    iget-object v2, p0, Ldxf;->j:[[B

    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldxf;->g:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldxf;->e:[Ldxl;

    if-nez v0, :cond_8

    move v0, v1

    :goto_4
    add-int/2addr v2, v0

    new-array v2, v2, [Ldxl;

    iget-object v3, p0, Ldxf;->e:[Ldxl;

    if-eqz v3, :cond_7

    iget-object v3, p0, Ldxf;->e:[Ldxl;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    iput-object v2, p0, Ldxf;->e:[Ldxl;

    :goto_5
    iget-object v2, p0, Ldxf;->e:[Ldxl;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    iget-object v2, p0, Ldxf;->e:[Ldxl;

    new-instance v3, Ldxl;

    invoke-direct {v3}, Ldxl;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldxf;->e:[Ldxl;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_8
    iget-object v0, p0, Ldxf;->e:[Ldxl;

    array-length v0, v0

    goto :goto_4

    :cond_9
    iget-object v2, p0, Ldxf;->e:[Ldxl;

    new-instance v3, Ldxl;

    invoke-direct {v3}, Ldxl;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldxf;->e:[Ldxl;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldxf;->f:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_a

    const/4 v2, 0x1

    if-ne v0, v2, :cond_b

    :cond_a
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldxf;->h:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_b
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldxf;->h:Ljava/lang/Integer;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 10031
    iget-object v1, p0, Ldxf;->b:Ldvm;

    if-eqz v1, :cond_0

    .line 10032
    const/4 v1, 0x1

    iget-object v2, p0, Ldxf;->b:Ldvm;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 10034
    :cond_0
    iget-object v1, p0, Ldxf;->c:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 10035
    const/4 v1, 0x2

    iget-object v2, p0, Ldxf;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Lepl;->a(IJ)V

    .line 10037
    :cond_1
    iget-object v1, p0, Ldxf;->d:[Ldtp;

    if-eqz v1, :cond_3

    .line 10038
    iget-object v2, p0, Ldxf;->d:[Ldtp;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 10039
    if-eqz v4, :cond_2

    .line 10040
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 10038
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 10044
    :cond_3
    iget-object v1, p0, Ldxf;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 10045
    const/4 v1, 0x4

    iget-object v2, p0, Ldxf;->i:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 10047
    :cond_4
    iget-object v1, p0, Ldxf;->j:[[B

    if-eqz v1, :cond_5

    .line 10048
    iget-object v2, p0, Ldxf;->j:[[B

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 10049
    const/4 v5, 0x5

    invoke-virtual {p1, v5, v4}, Lepl;->a(I[B)V

    .line 10048
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 10052
    :cond_5
    iget-object v1, p0, Ldxf;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 10053
    const/4 v1, 0x6

    iget-object v2, p0, Ldxf;->g:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 10055
    :cond_6
    iget-object v1, p0, Ldxf;->e:[Ldxl;

    if-eqz v1, :cond_8

    .line 10056
    iget-object v1, p0, Ldxf;->e:[Ldxl;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_8

    aget-object v3, v1, v0

    .line 10057
    if-eqz v3, :cond_7

    .line 10058
    const/4 v4, 0x7

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 10056
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 10062
    :cond_8
    iget-object v0, p0, Ldxf;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 10063
    const/16 v0, 0x8

    iget-object v1, p0, Ldxf;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 10065
    :cond_9
    iget-object v0, p0, Ldxf;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    .line 10066
    const/16 v0, 0x9

    iget-object v1, p0, Ldxf;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 10068
    :cond_a
    iget-object v0, p0, Ldxf;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 10070
    return-void
.end method
