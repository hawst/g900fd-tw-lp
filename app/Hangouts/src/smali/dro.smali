.class public final Ldro;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldro;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ldui;

.field public d:Ldrq;

.field public e:Ldri;

.field public f:Ldut;

.field public g:Ljava/lang/Boolean;

.field public h:Ljava/lang/Boolean;

.field public i:Ljava/lang/Boolean;

.field public j:Ljava/lang/Integer;

.field public k:Ljava/lang/Integer;

.field public l:[B

.field public m:[B

.field public n:[B

.field public o:[B

.field public p:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18980
    const/4 v0, 0x0

    new-array v0, v0, [Ldro;

    sput-object v0, Ldro;->a:[Ldro;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 18981
    invoke-direct {p0}, Lepn;-><init>()V

    .line 18996
    iput-object v0, p0, Ldro;->b:Ljava/lang/Integer;

    .line 18999
    iput-object v0, p0, Ldro;->c:Ldui;

    .line 19002
    iput-object v0, p0, Ldro;->d:Ldrq;

    .line 19005
    iput-object v0, p0, Ldro;->e:Ldri;

    .line 19008
    iput-object v0, p0, Ldro;->f:Ldut;

    .line 19017
    iput-object v0, p0, Ldro;->j:Ljava/lang/Integer;

    .line 19020
    iput-object v0, p0, Ldro;->k:Ljava/lang/Integer;

    .line 18981
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 19086
    const/4 v0, 0x0

    .line 19087
    iget-object v1, p0, Ldro;->l:[B

    if-eqz v1, :cond_0

    .line 19088
    const/4 v0, 0x1

    iget-object v1, p0, Ldro;->l:[B

    .line 19089
    invoke-static {v0, v1}, Lepl;->b(I[B)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 19091
    :cond_0
    iget-object v1, p0, Ldro;->m:[B

    if-eqz v1, :cond_1

    .line 19092
    const/4 v1, 0x2

    iget-object v2, p0, Ldro;->m:[B

    .line 19093
    invoke-static {v1, v2}, Lepl;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 19095
    :cond_1
    iget-object v1, p0, Ldro;->n:[B

    if-eqz v1, :cond_2

    .line 19096
    const/4 v1, 0x3

    iget-object v2, p0, Ldro;->n:[B

    .line 19097
    invoke-static {v1, v2}, Lepl;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 19099
    :cond_2
    iget-object v1, p0, Ldro;->o:[B

    if-eqz v1, :cond_3

    .line 19100
    const/4 v1, 0x4

    iget-object v2, p0, Ldro;->o:[B

    .line 19101
    invoke-static {v1, v2}, Lepl;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 19103
    :cond_3
    iget-object v1, p0, Ldro;->p:[B

    if-eqz v1, :cond_4

    .line 19104
    const/4 v1, 0x5

    iget-object v2, p0, Ldro;->p:[B

    .line 19105
    invoke-static {v1, v2}, Lepl;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 19107
    :cond_4
    iget-object v1, p0, Ldro;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 19108
    const/4 v1, 0x7

    iget-object v2, p0, Ldro;->g:Ljava/lang/Boolean;

    .line 19109
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 19111
    :cond_5
    iget-object v1, p0, Ldro;->f:Ldut;

    if-eqz v1, :cond_6

    .line 19112
    const/16 v1, 0x8

    iget-object v2, p0, Ldro;->f:Ldut;

    .line 19113
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19115
    :cond_6
    iget-object v1, p0, Ldro;->c:Ldui;

    if-eqz v1, :cond_7

    .line 19116
    const/16 v1, 0x9

    iget-object v2, p0, Ldro;->c:Ldui;

    .line 19117
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19119
    :cond_7
    iget-object v1, p0, Ldro;->d:Ldrq;

    if-eqz v1, :cond_8

    .line 19120
    const/16 v1, 0xa

    iget-object v2, p0, Ldro;->d:Ldrq;

    .line 19121
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19123
    :cond_8
    iget-object v1, p0, Ldro;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    .line 19124
    const/16 v1, 0xb

    iget-object v2, p0, Ldro;->h:Ljava/lang/Boolean;

    .line 19125
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 19127
    :cond_9
    iget-object v1, p0, Ldro;->e:Ldri;

    if-eqz v1, :cond_a

    .line 19128
    const/16 v1, 0xc

    iget-object v2, p0, Ldro;->e:Ldri;

    .line 19129
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19131
    :cond_a
    iget-object v1, p0, Ldro;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_b

    .line 19132
    const/16 v1, 0xd

    iget-object v2, p0, Ldro;->b:Ljava/lang/Integer;

    .line 19133
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 19135
    :cond_b
    iget-object v1, p0, Ldro;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    .line 19136
    const/16 v1, 0xe

    iget-object v2, p0, Ldro;->i:Ljava/lang/Boolean;

    .line 19137
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 19139
    :cond_c
    iget-object v1, p0, Ldro;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    .line 19140
    const/16 v1, 0xf

    iget-object v2, p0, Ldro;->j:Ljava/lang/Integer;

    .line 19141
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 19143
    :cond_d
    iget-object v1, p0, Ldro;->k:Ljava/lang/Integer;

    if-eqz v1, :cond_e

    .line 19144
    const/16 v1, 0x10

    iget-object v2, p0, Ldro;->k:Ljava/lang/Integer;

    .line 19145
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 19147
    :cond_e
    iget-object v1, p0, Ldro;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19148
    iput v0, p0, Ldro;->cachedSize:I

    .line 19149
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 18977
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldro;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldro;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldro;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v0

    iput-object v0, p0, Ldro;->l:[B

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v0

    iput-object v0, p0, Ldro;->m:[B

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v0

    iput-object v0, p0, Ldro;->n:[B

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v0

    iput-object v0, p0, Ldro;->o:[B

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v0

    iput-object v0, p0, Ldro;->p:[B

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldro;->g:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Ldro;->f:Ldut;

    if-nez v0, :cond_2

    new-instance v0, Ldut;

    invoke-direct {v0}, Ldut;-><init>()V

    iput-object v0, p0, Ldro;->f:Ldut;

    :cond_2
    iget-object v0, p0, Ldro;->f:Ldut;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Ldro;->c:Ldui;

    if-nez v0, :cond_3

    new-instance v0, Ldui;

    invoke-direct {v0}, Ldui;-><init>()V

    iput-object v0, p0, Ldro;->c:Ldui;

    :cond_3
    iget-object v0, p0, Ldro;->c:Ldui;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_9
    iget-object v0, p0, Ldro;->d:Ldrq;

    if-nez v0, :cond_4

    new-instance v0, Ldrq;

    invoke-direct {v0}, Ldrq;-><init>()V

    iput-object v0, p0, Ldro;->d:Ldrq;

    :cond_4
    iget-object v0, p0, Ldro;->d:Ldrq;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldro;->h:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Ldro;->e:Ldri;

    if-nez v0, :cond_5

    new-instance v0, Ldri;

    invoke-direct {v0}, Ldri;-><init>()V

    iput-object v0, p0, Ldro;->e:Ldri;

    :cond_5
    iget-object v0, p0, Ldro;->e:Ldri;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_6

    if-eq v0, v3, :cond_6

    if-eq v0, v4, :cond_6

    const/4 v1, 0x3

    if-eq v0, v1, :cond_6

    const/4 v1, 0x4

    if-eq v0, v1, :cond_6

    const/4 v1, 0x5

    if-ne v0, v1, :cond_7

    :cond_6
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldro;->b:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_7
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldro;->b:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldro;->i:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_8

    if-eq v0, v3, :cond_8

    if-ne v0, v4, :cond_9

    :cond_8
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldro;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_9
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldro;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_a

    if-eq v0, v3, :cond_a

    if-ne v0, v4, :cond_b

    :cond_a
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldro;->k:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_b
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldro;->k:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x38 -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x58 -> :sswitch_a
        0x62 -> :sswitch_b
        0x68 -> :sswitch_c
        0x70 -> :sswitch_d
        0x78 -> :sswitch_e
        0x80 -> :sswitch_f
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 19035
    iget-object v0, p0, Ldro;->l:[B

    if-eqz v0, :cond_0

    .line 19036
    const/4 v0, 0x1

    iget-object v1, p0, Ldro;->l:[B

    invoke-virtual {p1, v0, v1}, Lepl;->a(I[B)V

    .line 19038
    :cond_0
    iget-object v0, p0, Ldro;->m:[B

    if-eqz v0, :cond_1

    .line 19039
    const/4 v0, 0x2

    iget-object v1, p0, Ldro;->m:[B

    invoke-virtual {p1, v0, v1}, Lepl;->a(I[B)V

    .line 19041
    :cond_1
    iget-object v0, p0, Ldro;->n:[B

    if-eqz v0, :cond_2

    .line 19042
    const/4 v0, 0x3

    iget-object v1, p0, Ldro;->n:[B

    invoke-virtual {p1, v0, v1}, Lepl;->a(I[B)V

    .line 19044
    :cond_2
    iget-object v0, p0, Ldro;->o:[B

    if-eqz v0, :cond_3

    .line 19045
    const/4 v0, 0x4

    iget-object v1, p0, Ldro;->o:[B

    invoke-virtual {p1, v0, v1}, Lepl;->a(I[B)V

    .line 19047
    :cond_3
    iget-object v0, p0, Ldro;->p:[B

    if-eqz v0, :cond_4

    .line 19048
    const/4 v0, 0x5

    iget-object v1, p0, Ldro;->p:[B

    invoke-virtual {p1, v0, v1}, Lepl;->a(I[B)V

    .line 19050
    :cond_4
    iget-object v0, p0, Ldro;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 19051
    const/4 v0, 0x7

    iget-object v1, p0, Ldro;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 19053
    :cond_5
    iget-object v0, p0, Ldro;->f:Ldut;

    if-eqz v0, :cond_6

    .line 19054
    const/16 v0, 0x8

    iget-object v1, p0, Ldro;->f:Ldut;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 19056
    :cond_6
    iget-object v0, p0, Ldro;->c:Ldui;

    if-eqz v0, :cond_7

    .line 19057
    const/16 v0, 0x9

    iget-object v1, p0, Ldro;->c:Ldui;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 19059
    :cond_7
    iget-object v0, p0, Ldro;->d:Ldrq;

    if-eqz v0, :cond_8

    .line 19060
    const/16 v0, 0xa

    iget-object v1, p0, Ldro;->d:Ldrq;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 19062
    :cond_8
    iget-object v0, p0, Ldro;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    .line 19063
    const/16 v0, 0xb

    iget-object v1, p0, Ldro;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 19065
    :cond_9
    iget-object v0, p0, Ldro;->e:Ldri;

    if-eqz v0, :cond_a

    .line 19066
    const/16 v0, 0xc

    iget-object v1, p0, Ldro;->e:Ldri;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 19068
    :cond_a
    iget-object v0, p0, Ldro;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_b

    .line 19069
    const/16 v0, 0xd

    iget-object v1, p0, Ldro;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 19071
    :cond_b
    iget-object v0, p0, Ldro;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    .line 19072
    const/16 v0, 0xe

    iget-object v1, p0, Ldro;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 19074
    :cond_c
    iget-object v0, p0, Ldro;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_d

    .line 19075
    const/16 v0, 0xf

    iget-object v1, p0, Ldro;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 19077
    :cond_d
    iget-object v0, p0, Ldro;->k:Ljava/lang/Integer;

    if-eqz v0, :cond_e

    .line 19078
    const/16 v0, 0x10

    iget-object v1, p0, Ldro;->k:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 19080
    :cond_e
    iget-object v0, p0, Ldro;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 19082
    return-void
.end method
