.class public final Ldst;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldst;


# instance fields
.field public b:Ldvn;

.field public c:[Ldro;

.field public d:Ljava/lang/Boolean;

.field public e:Ldsv;

.field public f:Ldsv;

.field public g:Ldsv;

.field public h:Ldsv;

.field public i:Ldsv;

.field public j:Ldsv;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19277
    const/4 v0, 0x0

    new-array v0, v0, [Ldst;

    sput-object v0, Ldst;->a:[Ldst;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 19278
    invoke-direct {p0}, Lepn;-><init>()V

    .line 19492
    iput-object v1, p0, Ldst;->b:Ldvn;

    .line 19495
    sget-object v0, Ldro;->a:[Ldro;

    iput-object v0, p0, Ldst;->c:[Ldro;

    .line 19500
    iput-object v1, p0, Ldst;->e:Ldsv;

    .line 19503
    iput-object v1, p0, Ldst;->f:Ldsv;

    .line 19506
    iput-object v1, p0, Ldst;->g:Ldsv;

    .line 19509
    iput-object v1, p0, Ldst;->h:Ldsv;

    .line 19512
    iput-object v1, p0, Ldst;->i:Ldsv;

    .line 19515
    iput-object v1, p0, Ldst;->j:Ldsv;

    .line 19278
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 19558
    iget-object v0, p0, Ldst;->b:Ldvn;

    if-eqz v0, :cond_9

    .line 19559
    const/4 v0, 0x1

    iget-object v2, p0, Ldst;->b:Ldvn;

    .line 19560
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 19562
    :goto_0
    iget-object v2, p0, Ldst;->c:[Ldro;

    if-eqz v2, :cond_1

    .line 19563
    iget-object v2, p0, Ldst;->c:[Ldro;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 19564
    if-eqz v4, :cond_0

    .line 19565
    const/4 v5, 0x2

    .line 19566
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 19563
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 19570
    :cond_1
    iget-object v1, p0, Ldst;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 19571
    const/4 v1, 0x3

    iget-object v2, p0, Ldst;->d:Ljava/lang/Boolean;

    .line 19572
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 19574
    :cond_2
    iget-object v1, p0, Ldst;->f:Ldsv;

    if-eqz v1, :cond_3

    .line 19575
    const/4 v1, 0x4

    iget-object v2, p0, Ldst;->f:Ldsv;

    .line 19576
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19578
    :cond_3
    iget-object v1, p0, Ldst;->g:Ldsv;

    if-eqz v1, :cond_4

    .line 19579
    const/4 v1, 0x5

    iget-object v2, p0, Ldst;->g:Ldsv;

    .line 19580
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19582
    :cond_4
    iget-object v1, p0, Ldst;->h:Ldsv;

    if-eqz v1, :cond_5

    .line 19583
    const/4 v1, 0x6

    iget-object v2, p0, Ldst;->h:Ldsv;

    .line 19584
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19586
    :cond_5
    iget-object v1, p0, Ldst;->i:Ldsv;

    if-eqz v1, :cond_6

    .line 19587
    const/4 v1, 0x7

    iget-object v2, p0, Ldst;->i:Ldsv;

    .line 19588
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19590
    :cond_6
    iget-object v1, p0, Ldst;->j:Ldsv;

    if-eqz v1, :cond_7

    .line 19591
    const/16 v1, 0x8

    iget-object v2, p0, Ldst;->j:Ldsv;

    .line 19592
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19594
    :cond_7
    iget-object v1, p0, Ldst;->e:Ldsv;

    if-eqz v1, :cond_8

    .line 19595
    const/16 v1, 0x9

    iget-object v2, p0, Ldst;->e:Ldsv;

    .line 19596
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19598
    :cond_8
    iget-object v1, p0, Ldst;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19599
    iput v0, p0, Ldst;->cachedSize:I

    .line 19600
    return v0

    :cond_9
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 19274
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldst;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldst;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldst;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldst;->b:Ldvn;

    if-nez v0, :cond_2

    new-instance v0, Ldvn;

    invoke-direct {v0}, Ldvn;-><init>()V

    iput-object v0, p0, Ldst;->b:Ldvn;

    :cond_2
    iget-object v0, p0, Ldst;->b:Ldvn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldst;->c:[Ldro;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldro;

    iget-object v3, p0, Ldst;->c:[Ldro;

    if-eqz v3, :cond_3

    iget-object v3, p0, Ldst;->c:[Ldro;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Ldst;->c:[Ldro;

    :goto_2
    iget-object v2, p0, Ldst;->c:[Ldro;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Ldst;->c:[Ldro;

    new-instance v3, Ldro;

    invoke-direct {v3}, Ldro;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldst;->c:[Ldro;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Ldst;->c:[Ldro;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Ldst;->c:[Ldro;

    new-instance v3, Ldro;

    invoke-direct {v3}, Ldro;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldst;->c:[Ldro;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldst;->d:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_4
    iget-object v0, p0, Ldst;->f:Ldsv;

    if-nez v0, :cond_6

    new-instance v0, Ldsv;

    invoke-direct {v0}, Ldsv;-><init>()V

    iput-object v0, p0, Ldst;->f:Ldsv;

    :cond_6
    iget-object v0, p0, Ldst;->f:Ldsv;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Ldst;->g:Ldsv;

    if-nez v0, :cond_7

    new-instance v0, Ldsv;

    invoke-direct {v0}, Ldsv;-><init>()V

    iput-object v0, p0, Ldst;->g:Ldsv;

    :cond_7
    iget-object v0, p0, Ldst;->g:Ldsv;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Ldst;->h:Ldsv;

    if-nez v0, :cond_8

    new-instance v0, Ldsv;

    invoke-direct {v0}, Ldsv;-><init>()V

    iput-object v0, p0, Ldst;->h:Ldsv;

    :cond_8
    iget-object v0, p0, Ldst;->h:Ldsv;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Ldst;->i:Ldsv;

    if-nez v0, :cond_9

    new-instance v0, Ldsv;

    invoke-direct {v0}, Ldsv;-><init>()V

    iput-object v0, p0, Ldst;->i:Ldsv;

    :cond_9
    iget-object v0, p0, Ldst;->i:Ldsv;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Ldst;->j:Ldsv;

    if-nez v0, :cond_a

    new-instance v0, Ldsv;

    invoke-direct {v0}, Ldsv;-><init>()V

    iput-object v0, p0, Ldst;->j:Ldsv;

    :cond_a
    iget-object v0, p0, Ldst;->j:Ldsv;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Ldst;->e:Ldsv;

    if-nez v0, :cond_b

    new-instance v0, Ldsv;

    invoke-direct {v0}, Ldsv;-><init>()V

    iput-object v0, p0, Ldst;->e:Ldsv;

    :cond_b
    iget-object v0, p0, Ldst;->e:Ldsv;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 19520
    iget-object v0, p0, Ldst;->b:Ldvn;

    if-eqz v0, :cond_0

    .line 19521
    const/4 v0, 0x1

    iget-object v1, p0, Ldst;->b:Ldvn;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 19523
    :cond_0
    iget-object v0, p0, Ldst;->c:[Ldro;

    if-eqz v0, :cond_2

    .line 19524
    iget-object v1, p0, Ldst;->c:[Ldro;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 19525
    if-eqz v3, :cond_1

    .line 19526
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 19524
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 19530
    :cond_2
    iget-object v0, p0, Ldst;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 19531
    const/4 v0, 0x3

    iget-object v1, p0, Ldst;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 19533
    :cond_3
    iget-object v0, p0, Ldst;->f:Ldsv;

    if-eqz v0, :cond_4

    .line 19534
    const/4 v0, 0x4

    iget-object v1, p0, Ldst;->f:Ldsv;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 19536
    :cond_4
    iget-object v0, p0, Ldst;->g:Ldsv;

    if-eqz v0, :cond_5

    .line 19537
    const/4 v0, 0x5

    iget-object v1, p0, Ldst;->g:Ldsv;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 19539
    :cond_5
    iget-object v0, p0, Ldst;->h:Ldsv;

    if-eqz v0, :cond_6

    .line 19540
    const/4 v0, 0x6

    iget-object v1, p0, Ldst;->h:Ldsv;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 19542
    :cond_6
    iget-object v0, p0, Ldst;->i:Ldsv;

    if-eqz v0, :cond_7

    .line 19543
    const/4 v0, 0x7

    iget-object v1, p0, Ldst;->i:Ldsv;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 19545
    :cond_7
    iget-object v0, p0, Ldst;->j:Ldsv;

    if-eqz v0, :cond_8

    .line 19546
    const/16 v0, 0x8

    iget-object v1, p0, Ldst;->j:Ldsv;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 19548
    :cond_8
    iget-object v0, p0, Ldst;->e:Ldsv;

    if-eqz v0, :cond_9

    .line 19549
    const/16 v0, 0x9

    iget-object v1, p0, Ldst;->e:Ldsv;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 19551
    :cond_9
    iget-object v0, p0, Ldst;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 19553
    return-void
.end method
