.class public final Lbyh;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Z

.field private static final b:Ljava/lang/String;

.field private static c:Lbxu;

.field private static d:I

.field private static e:Z

.field private static f:Z

.field private static g:I

.field private static h:I

.field private static final n:Ljava/util/concurrent/ThreadFactory;

.field private static final o:Ljava/util/concurrent/Executor;


# instance fields
.field private final i:I

.field private final j:Landroid/graphics/Paint;

.field private final k:Landroid/graphics/Rect;

.field private final l:Lex;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lex",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 49
    sget-object v0, Lbys;->r:Lcyp;

    sput-boolean v2, Lbyh;->a:Z

    .line 52
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v1, 0x1f170

    .line 53
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbyh;->b:Ljava/lang/String;

    .line 57
    sput-boolean v2, Lbyh;->e:Z

    .line 214
    new-instance v0, Lbyj;

    invoke-direct {v0}, Lbyj;-><init>()V

    sput-object v0, Lbyh;->n:Ljava/util/concurrent/ThreadFactory;

    .line 226
    const/4 v0, 0x2

    sget-object v1, Lbyh;->n:Ljava/util/concurrent/ThreadFactory;

    .line 227
    invoke-static {v0, v1}, Ljava/util/concurrent/Executors;->newFixedThreadPool(ILjava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lbyh;->o:Ljava/util/concurrent/Executor;

    .line 226
    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    invoke-static {}, Lbyh;->a()Lbxu;

    .line 72
    new-instance v0, Lex;

    sget v3, Lbyh;->d:I

    invoke-direct {v0, v3}, Lex;-><init>(I)V

    iput-object v0, p0, Lbyh;->l:Lex;

    .line 73
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v0, v3, :cond_1

    const-string v0, "babel_prefer_emoji_system_font_rendering"

    .line 74
    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lbyh;->m:Z

    .line 78
    iget-boolean v0, p0, Lbyh;->m:Z

    if-eqz v0, :cond_2

    .line 79
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->d()Lcom/google/android/apps/hangouts/phone/EsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v3, Lf;->cV:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lbyh;->i:I

    .line 81
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lbyh;->j:Landroid/graphics/Paint;

    .line 83
    iget-object v0, p0, Lbyh;->j:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 85
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lbyh;->k:Landroid/graphics/Rect;

    .line 86
    iget v0, p0, Lbyh;->i:I

    .line 89
    :cond_0
    iget-object v1, p0, Lbyh;->j:Landroid/graphics/Paint;

    add-int/lit8 v0, v0, -0x1

    int-to-float v3, v0

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 90
    iget-object v1, p0, Lbyh;->j:Landroid/graphics/Paint;

    sget-object v3, Lbyh;->b:Ljava/lang/String;

    sget-object v4, Lbyh;->b:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    iget-object v5, p0, Lbyh;->k:Landroid/graphics/Rect;

    invoke-virtual {v1, v3, v2, v4, v5}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 91
    iget-object v1, p0, Lbyh;->j:Landroid/graphics/Paint;

    sget-object v3, Lbyh;->b:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v1

    .line 92
    iget-object v3, p0, Lbyh;->k:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    iget v4, p0, Lbyh;->i:I

    if-ge v3, v4, :cond_0

    iget v3, p0, Lbyh;->i:I

    int-to-float v3, v3

    cmpl-float v1, v1, v3

    if-gez v1, :cond_0

    .line 98
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 74
    goto :goto_0

    .line 94
    :cond_2
    iput v2, p0, Lbyh;->i:I

    .line 95
    iput-object v4, p0, Lbyh;->k:Landroid/graphics/Rect;

    .line 96
    iput-object v4, p0, Lbyh;->j:Landroid/graphics/Paint;

    goto :goto_1
.end method

.method public static a()Lbxu;
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v2, 0x0

    .line 104
    sget-object v0, Lbyh;->c:Lbxu;

    if-nez v0, :cond_2

    .line 106
    invoke-static {}, Lbyh;->b()Landroid/graphics/BitmapFactory$Options;

    move-result-object v3

    .line 107
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 111
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->emoji_u00a9:I

    invoke-static {v4, v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 112
    invoke-static {v5}, Lcxg;->a(Landroid/graphics/Bitmap;)I

    move-result v6

    .line 113
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    .line 114
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    .line 119
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_3

    const/high16 v0, 0x20000

    move v1, v0

    .line 122
    :goto_0
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v9, "activity"

    invoke-virtual {v0, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 123
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v0

    mul-int/2addr v0, v1

    const/high16 v1, 0x400000

    .line 122
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    const/high16 v1, 0x1000000

    .line 121
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 128
    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0xf

    if-lt v0, v11, :cond_4

    const/4 v0, 0x1

    .line 130
    :goto_1
    div-int v9, v1, v6

    .line 131
    if-eqz v0, :cond_5

    .line 132
    const-string v0, "babel_emoji_max_pool_size_large"

    const/16 v10, 0x100

    invoke-static {v0, v10}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v9, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 140
    :goto_2
    sget-boolean v9, Lbyh;->a:Z

    if-nez v9, :cond_0

    const-string v9, "Babel"

    invoke-static {v9, v11}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 141
    :cond_0
    const-string v9, "Babel"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "[BitmapPoolICS ctor] maxItems="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", poolMemSize="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v10, ", memTakenPerBitmap="

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v9, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    :cond_1
    sput v0, Lbyh;->d:I

    .line 146
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, "getBitmapPool size="

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v6, Lbyh;->d:I

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    sget v0, Lbyh;->d:I

    const-string v1, "Emoji"

    invoke-static {v0, v2, v2, v2, v1}, Lbxu;->a(IIIILjava/lang/String;)Lbxu;

    move-result-object v0

    sput-object v0, Lbyh;->c:Lbxu;

    .line 153
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_2

    .line 154
    sget-object v0, Lbyh;->c:Lbxu;

    invoke-virtual {v0, v5}, Lbxu;->a(Landroid/graphics/Bitmap;)V

    .line 155
    new-instance v0, Lbyi;

    invoke-direct {v0, v7, v8, v4, v3}, Lbyi;-><init>(IILandroid/content/res/Resources;Landroid/graphics/BitmapFactory$Options;)V

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lbyi;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 159
    :cond_2
    sget-object v0, Lbyh;->c:Lbxu;

    return-object v0

    .line 119
    :cond_3
    const/high16 v0, 0x10000

    move v1, v0

    goto/16 :goto_0

    :cond_4
    move v0, v2

    .line 128
    goto/16 :goto_1

    .line 136
    :cond_5
    const-string v0, "babel_emoji_max_pool_size"

    const/16 v10, 0x80

    invoke-static {v0, v10}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v9, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto/16 :goto_2
.end method

.method static synthetic a(Landroid/widget/ImageView;)Lbyl;
    .locals 2

    .prologue
    .line 47
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    instance-of v1, v0, Lbyk;

    if-eqz v1, :cond_0

    check-cast v0, Lbyk;

    invoke-virtual {v0}, Lbyk;->a()Lbyl;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lbyh;)Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lbyh;->m:Z

    return v0
.end method

.method public static b()Landroid/graphics/BitmapFactory$Options;
    .locals 4

    .prologue
    const/16 v3, 0x80

    const/4 v2, 0x1

    .line 193
    sget-boolean v0, Lbyh;->e:Z

    if-nez v0, :cond_0

    .line 194
    sput v3, Lbyh;->g:I

    .line 195
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    .line 196
    const/4 v0, 0x0

    sput-boolean v0, Lbyh;->f:Z

    .line 197
    sput v3, Lbyh;->h:I

    .line 204
    :goto_0
    sput-boolean v2, Lbyh;->e:Z

    .line 206
    :cond_0
    sget-boolean v0, Lbyh;->f:Z

    sget v1, Lbyh;->g:I

    sget v2, Lbyh;->h:I

    invoke-static {v0, v1, v2}, Lbxu;->a(ZII)Landroid/graphics/BitmapFactory$Options;

    move-result-object v0

    return-object v0

    .line 199
    :cond_1
    sput-boolean v2, Lbyh;->f:Z

    .line 201
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->d()Lcom/google/android/apps/hangouts/phone/EsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lf;->cV:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lbyh;->h:I

    goto :goto_0
.end method

.method static synthetic b(Lbyh;)Lex;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lbyh;->l:Lex;

    return-object v0
.end method

.method static synthetic c(Lbyh;)I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lbyh;->i:I

    return v0
.end method

.method static synthetic d(Lbyh;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lbyh;->k:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic d()Lbxu;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lbyh;->c:Lbxu;

    return-object v0
.end method

.method static synthetic e(Lbyh;)Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lbyh;->j:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic e()Z
    .locals 1

    .prologue
    .line 47
    sget-boolean v0, Lbyh;->a:Z

    return v0
.end method


# virtual methods
.method public a([I)Lbyl;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 234
    new-instance v0, Lbyl;

    invoke-direct {v0, p0, p1}, Lbyl;-><init>(Lbyh;[I)V

    .line 235
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    sget-object v1, Lbyh;->o:Ljava/util/concurrent/Executor;

    new-array v2, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lbyl;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 236
    :goto_0
    return-object v0

    .line 235
    :cond_0
    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lbyl;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public a(Landroid/content/res/Resources;ILbyl;Landroid/widget/ImageView;)V
    .locals 1

    .prologue
    .line 248
    new-instance v0, Lbyk;

    invoke-direct {v0, p0, p1, p3}, Lbyk;-><init>(Lbyh;Landroid/content/res/Resources;Lbyl;)V

    invoke-virtual {p4, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 249
    invoke-virtual {p3, p2, p4}, Lbyl;->a(ILandroid/widget/ImageView;)V

    .line 250
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 230
    iget-object v0, p0, Lbyh;->l:Lex;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lex;->b_(I)V

    .line 231
    return-void
.end method
