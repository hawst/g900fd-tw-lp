.class public abstract Layf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbbk;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbbk",
        "<",
        "Layf;",
        ">;"
    }
.end annotation


# instance fields
.field protected a:Landroid/app/AlertDialog;

.field protected b:Lyj;

.field protected c:Landroid/app/Activity;

.field private d:I

.field private e:[I

.field private f:Landroid/view/View$OnClickListener;


# direct methods
.method protected constructor <init>(I[I)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    new-instance v0, Layg;

    invoke-direct {v0, p0}, Layg;-><init>(Layf;)V

    iput-object v0, p0, Layf;->f:Landroid/view/View$OnClickListener;

    .line 30
    iput p1, p0, Layf;->d:I

    .line 31
    iput-object p2, p0, Layf;->e:[I

    .line 32
    return-void
.end method


# virtual methods
.method public abstract a()I
.end method

.method public a(Layf;)I
    .locals 2

    .prologue
    .line 51
    invoke-virtual {p1}, Layf;->a()I

    move-result v0

    invoke-virtual {p0}, Layf;->a()I

    move-result v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 53
    invoke-virtual {p1}, Layf;->a()I

    move-result v0

    invoke-virtual {p0}, Layf;->a()I

    move-result v1

    sub-int/2addr v0, v1

    return v0

    .line 51
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(I)V
    .locals 0

    .prologue
    .line 57
    invoke-virtual {p0}, Layf;->c()V

    .line 58
    return-void
.end method

.method public a(Lyj;Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Layf;->a:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 108
    :goto_0
    return-void

    .line 96
    :cond_0
    iput-object p1, p0, Layf;->b:Lyj;

    .line 97
    iput-object p2, p0, Layf;->c:Landroid/app/Activity;

    .line 99
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 100
    invoke-virtual {p0}, Layf;->b()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 101
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Layf;->a:Landroid/app/AlertDialog;

    .line 102
    iget-object v0, p0, Layf;->a:Landroid/app/AlertDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 103
    iget-object v0, p0, Layf;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 106
    iget-object v0, p0, Layf;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {p2, v0}, Lbxm;->a(Landroid/app/Activity;Landroid/view/Window;)V

    goto :goto_0
.end method

.method public b()Landroid/view/View;
    .locals 7

    .prologue
    .line 62
    iget-object v0, p0, Layf;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p0, Layf;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 63
    iget-object v3, p0, Layf;->e:[I

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    aget v0, v3, v1

    .line 64
    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 65
    iget-object v5, p0, Layf;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0xe

    if-lt v5, v6, :cond_0

    .line 68
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setAllCaps(Z)V

    .line 63
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 71
    :cond_1
    return-object v2
.end method

.method public c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 81
    iget-object v0, p0, Layf;->a:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Layf;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 84
    :cond_0
    iput-object v1, p0, Layf;->a:Landroid/app/AlertDialog;

    .line 85
    iput-object v1, p0, Layf;->b:Lyj;

    .line 86
    iput-object v1, p0, Layf;->c:Landroid/app/Activity;

    .line 87
    return-void
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 21
    check-cast p1, Layf;

    invoke-virtual {p0, p1}, Layf;->a(Layf;)I

    move-result v0

    return v0
.end method
