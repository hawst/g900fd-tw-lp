.class public final Lexf;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lexf;


# instance fields
.field public b:Letw;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Integer;

.field public e:[Lexf;

.field public f:[Lexe;

.field public g:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 226
    const/4 v0, 0x0

    new-array v0, v0, [Lexf;

    sput-object v0, Lexf;->a:[Lexf;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 227
    invoke-direct {p0}, Lepn;-><init>()V

    .line 236
    iput-object v1, p0, Lexf;->b:Letw;

    .line 243
    sget-object v0, Lexf;->a:[Lexf;

    iput-object v0, p0, Lexf;->e:[Lexf;

    .line 246
    sget-object v0, Lexe;->a:[Lexe;

    iput-object v0, p0, Lexf;->f:[Lexe;

    .line 249
    iput-object v1, p0, Lexf;->g:Ljava/lang/Integer;

    .line 227
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 287
    iget-object v0, p0, Lexf;->b:Letw;

    if-eqz v0, :cond_7

    .line 288
    const/4 v0, 0x1

    iget-object v2, p0, Lexf;->b:Letw;

    .line 289
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 291
    :goto_0
    iget-object v2, p0, Lexf;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 292
    const/4 v2, 0x2

    iget-object v3, p0, Lexf;->c:Ljava/lang/String;

    .line 293
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 295
    :cond_0
    iget-object v2, p0, Lexf;->d:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 296
    const/4 v2, 0x3

    iget-object v3, p0, Lexf;->d:Ljava/lang/Integer;

    .line 297
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 299
    :cond_1
    iget-object v2, p0, Lexf;->e:[Lexf;

    if-eqz v2, :cond_3

    .line 300
    iget-object v3, p0, Lexf;->e:[Lexf;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    .line 301
    if-eqz v5, :cond_2

    .line 302
    const/4 v6, 0x4

    .line 303
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 300
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 307
    :cond_3
    iget-object v2, p0, Lexf;->f:[Lexe;

    if-eqz v2, :cond_5

    .line 308
    iget-object v2, p0, Lexf;->f:[Lexe;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 309
    if-eqz v4, :cond_4

    .line 310
    const/4 v5, 0x5

    .line 311
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 308
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 315
    :cond_5
    iget-object v1, p0, Lexf;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 316
    const/4 v1, 0x6

    iget-object v2, p0, Lexf;->g:Ljava/lang/Integer;

    .line 317
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 319
    :cond_6
    iget-object v1, p0, Lexf;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 320
    iput v0, p0, Lexf;->cachedSize:I

    .line 321
    return v0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 223
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lexf;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lexf;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lexf;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lexf;->b:Letw;

    if-nez v0, :cond_2

    new-instance v0, Letw;

    invoke-direct {v0}, Letw;-><init>()V

    iput-object v0, p0, Lexf;->b:Letw;

    :cond_2
    iget-object v0, p0, Lexf;->b:Letw;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lexf;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lexf;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lexf;->e:[Lexf;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lexf;

    iget-object v3, p0, Lexf;->e:[Lexf;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lexf;->e:[Lexf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Lexf;->e:[Lexf;

    :goto_2
    iget-object v2, p0, Lexf;->e:[Lexf;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Lexf;->e:[Lexf;

    new-instance v3, Lexf;

    invoke-direct {v3}, Lexf;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lexf;->e:[Lexf;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lexf;->e:[Lexf;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lexf;->e:[Lexf;

    new-instance v3, Lexf;

    invoke-direct {v3}, Lexf;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lexf;->e:[Lexf;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lexf;->f:[Lexe;

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lexe;

    iget-object v3, p0, Lexf;->f:[Lexe;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lexf;->f:[Lexe;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    iput-object v2, p0, Lexf;->f:[Lexe;

    :goto_4
    iget-object v2, p0, Lexf;->f:[Lexe;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    iget-object v2, p0, Lexf;->f:[Lexe;

    new-instance v3, Lexe;

    invoke-direct {v3}, Lexe;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lexf;->f:[Lexe;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v0, p0, Lexf;->f:[Lexe;

    array-length v0, v0

    goto :goto_3

    :cond_8
    iget-object v2, p0, Lexf;->f:[Lexe;

    new-instance v3, Lexe;

    invoke-direct {v3}, Lexe;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lexf;->f:[Lexe;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_9

    const/4 v2, 0x1

    if-eq v0, v2, :cond_9

    const/4 v2, 0x2

    if-ne v0, v2, :cond_a

    :cond_9
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lexf;->g:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_a
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lexf;->g:Ljava/lang/Integer;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 254
    iget-object v1, p0, Lexf;->b:Letw;

    if-eqz v1, :cond_0

    .line 255
    const/4 v1, 0x1

    iget-object v2, p0, Lexf;->b:Letw;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 257
    :cond_0
    iget-object v1, p0, Lexf;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 258
    const/4 v1, 0x2

    iget-object v2, p0, Lexf;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 260
    :cond_1
    iget-object v1, p0, Lexf;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 261
    const/4 v1, 0x3

    iget-object v2, p0, Lexf;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 263
    :cond_2
    iget-object v1, p0, Lexf;->e:[Lexf;

    if-eqz v1, :cond_4

    .line 264
    iget-object v2, p0, Lexf;->e:[Lexf;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 265
    if-eqz v4, :cond_3

    .line 266
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 264
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 270
    :cond_4
    iget-object v1, p0, Lexf;->f:[Lexe;

    if-eqz v1, :cond_6

    .line 271
    iget-object v1, p0, Lexf;->f:[Lexe;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 272
    if-eqz v3, :cond_5

    .line 273
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 271
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 277
    :cond_6
    iget-object v0, p0, Lexf;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 278
    const/4 v0, 0x6

    iget-object v1, p0, Lexf;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 280
    :cond_7
    iget-object v0, p0, Lexf;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 282
    return-void
.end method
