.class public final Lbiy;
.super Lbiw;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public final k:I

.field public final l:I

.field public m:I

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lesc;Leqn;)V
    .locals 12

    .prologue
    .line 808
    const/4 v2, 0x0

    iget-object v3, p2, Leqn;->g:Ljava/lang/String;

    iget-object v4, p2, Leqn;->d:Ljava/lang/String;

    iget-object v0, p2, Leqn;->k:[Lepu;

    .line 809
    invoke-static {v0}, Lbiy;->a([Lepu;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "image/image_search"

    move-object v0, p0

    move-object v1, p1

    .line 808
    invoke-direct/range {v0 .. v6}, Lbiw;-><init>(Lesc;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 810
    iget-object v0, p2, Leqn;->i:Leqc;

    .line 811
    if-eqz v0, :cond_1

    .line 812
    iget-object v1, v0, Leqc;->c:Ljava/lang/Integer;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v1

    iput v1, p0, Lbiy;->k:I

    .line 813
    iget-object v0, v0, Leqc;->d:Ljava/lang/Integer;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    iput v0, p0, Lbiy;->l:I

    .line 819
    :goto_0
    const/4 v2, 0x0

    .line 820
    const/4 v1, 0x0

    .line 821
    iget-object v4, p2, Leqn;->k:[Lepu;

    array-length v5, v4

    const/4 v0, 0x0

    move v11, v0

    move-object v0, v2

    move v2, v1

    move v1, v11

    :goto_1
    if-ge v1, v5, :cond_3

    aget-object v6, v4, v1

    .line 822
    iget-object v7, v6, Lepu;->b:[I

    array-length v8, v7

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v8, :cond_0

    aget v9, v7, v3

    .line 823
    const/16 v10, 0x151

    if-ne v9, v10, :cond_2

    .line 824
    sget-object v0, Lerq;->b:Lepo;

    invoke-virtual {v6, v0}, Lepu;->getExtension(Lepo;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lerq;

    .line 825
    if-eqz v0, :cond_2

    .line 826
    const/4 v2, 0x1

    .line 831
    :cond_0
    if-nez v2, :cond_3

    .line 832
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 815
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lbiy;->k:I

    .line 816
    const/4 v0, 0x0

    iput v0, p0, Lbiy;->l:I

    goto :goto_0

    .line 822
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    move-object v1, v0

    .line 835
    if-eqz v1, :cond_4

    iget-object v0, v1, Lerq;->g:Ljava/lang/String;

    :goto_3
    iput-object v0, p0, Lbiy;->o:Ljava/lang/String;

    .line 837
    if-eqz v1, :cond_5

    iget-object v0, v1, Lerq;->f:Ljava/lang/String;

    :goto_4
    iput-object v0, p0, Lbiy;->n:Ljava/lang/String;

    .line 840
    const/4 v0, 0x0

    iput-object v0, p0, Lbiy;->i:Ljava/lang/String;

    .line 841
    const/4 v0, 0x0

    iput-object v0, p0, Lbiy;->j:Ljava/lang/String;

    .line 843
    const/4 v0, 0x1

    iput v0, p0, Lbiy;->m:I

    .line 844
    return-void

    .line 835
    :cond_4
    const/4 v0, 0x0

    goto :goto_3

    .line 837
    :cond_5
    const/4 v0, 0x0

    goto :goto_4
.end method

.method public constructor <init>(Lesc;Lerj;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 762
    iget-object v4, p2, Lerj;->i:Ljava/lang/String;

    iget-object v0, p2, Lerj;->p:Ljava/lang/Integer;

    .line 764
    invoke-static {v0, v7}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    iget-object v1, p2, Lerj;->i:Ljava/lang/String;

    .line 763
    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move-object v6, v2

    :cond_0
    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move-object v5, v2

    .line 762
    invoke-direct/range {v0 .. v6}, Lbiw;-><init>(Lesc;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 767
    iget-object v0, p2, Lerj;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p2, Lerj;->f:Ljava/lang/String;

    :goto_1
    iput-object v0, p0, Lbiy;->i:Ljava/lang/String;

    .line 768
    iget-object v0, p2, Lerj;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p2, Lerj;->e:Ljava/lang/String;

    :goto_2
    iput-object v0, p0, Lbiy;->j:Ljava/lang/String;

    .line 769
    iget-object v0, p2, Lerj;->c:Leql;

    if-eqz v0, :cond_3

    .line 770
    iget-object v0, p2, Lerj;->c:Leql;

    iget-object v0, v0, Leql;->l:Ljava/lang/Integer;

    invoke-static {v0, v7}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    iput v0, p0, Lbiy;->k:I

    .line 771
    iget-object v0, p2, Lerj;->c:Leql;

    iget-object v0, v0, Leql;->m:Ljava/lang/Integer;

    invoke-static {v0, v7}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    iput v0, p0, Lbiy;->l:I

    .line 777
    :goto_3
    const/4 v0, 0x1

    iput v0, p0, Lbiy;->m:I

    .line 780
    iput-object v2, p0, Lbiy;->o:Ljava/lang/String;

    .line 781
    iput-object v2, p0, Lbiy;->n:Ljava/lang/String;

    .line 782
    return-void

    .line 763
    :pswitch_1
    const-string v0, "image/*"

    invoke-static {v1, v0}, Lf;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    :pswitch_2
    const-string v6, "image/gif"

    goto :goto_0

    :pswitch_3
    const-string v0, "video/*"

    invoke-static {v1, v0}, Lf;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lf;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v6, "video/*"

    goto :goto_0

    :cond_1
    move-object v0, v2

    .line 767
    goto :goto_1

    :cond_2
    move-object v0, v2

    .line 768
    goto :goto_2

    .line 773
    :cond_3
    iput v7, p0, Lbiy;->k:I

    .line 774
    iput v7, p0, Lbiy;->l:I

    goto :goto_3

    .line 763
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public constructor <init>([ILjava/lang/String;Ljava/lang/String;IILjava/lang/String;I)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v4, v2

    move-object v5, p6

    .line 867
    invoke-direct/range {v0 .. v5}, Lbiw;-><init>([ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 870
    iput-object p2, p0, Lbiy;->i:Ljava/lang/String;

    .line 871
    iput-object v2, p0, Lbiy;->j:Ljava/lang/String;

    .line 872
    iput p4, p0, Lbiy;->k:I

    .line 873
    iput p5, p0, Lbiy;->l:I

    .line 874
    iput p7, p0, Lbiy;->m:I

    .line 875
    iput-object v2, p0, Lbiy;->o:Ljava/lang/String;

    .line 876
    iput-object v2, p0, Lbiy;->n:Ljava/lang/String;

    .line 877
    return-void
.end method

.method private static a([Lepu;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 847
    array-length v4, p0

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_2

    aget-object v5, p0, v3

    .line 848
    iget-object v6, v5, Lepu;->b:[I

    array-length v7, v6

    move v1, v2

    :goto_1
    if-ge v1, v7, :cond_1

    aget v0, v6, v1

    .line 849
    const/16 v8, 0x151

    if-ne v0, v8, :cond_0

    .line 850
    sget-object v0, Lerq;->b:Lepo;

    .line 851
    invoke-virtual {v5, v0}, Lepu;->getExtension(Lepo;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lerq;

    .line 852
    if-eqz v0, :cond_0

    .line 853
    iget-object v0, v0, Lerq;->d:Ljava/lang/String;

    .line 858
    :goto_2
    return-object v0

    .line 848
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 847
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 858
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 881
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "id: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lbiy;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " canonicalId "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbiy;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " photoId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbiy;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " imageUrl: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbiy;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " width: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lbiy;->k:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " height: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lbiy;->l:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " contentType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbiy;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
