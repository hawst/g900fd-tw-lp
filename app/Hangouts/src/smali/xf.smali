.class final Lxf;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lxe;",
            ">;"
        }
    .end annotation
.end field

.field private b:I


# direct methods
.method public varargs constructor <init>([Lxe;)V
    .locals 2

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lxf;->a:Ljava/util/ArrayList;

    .line 22
    iget-object v0, p0, Lxf;->a:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 23
    iget-object v0, p0, Lxf;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lxf;->b:I

    .line 24
    return-void
.end method


# virtual methods
.method public a(FLxi;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 117
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_1

    .line 118
    iget-object v0, p0, Lxf;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxe;

    .line 119
    iget-object v2, p0, Lxf;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lxe;

    .line 120
    invoke-virtual {v1}, Lxe;->c()Lwt;

    move-result-object v2

    .line 121
    if-eqz v2, :cond_0

    .line 122
    invoke-interface {v2, p1}, Lwt;->getInterpolation(F)F

    move-result p1

    .line 124
    :cond_0
    invoke-virtual {v0}, Lxe;->b()F

    move-result v2

    sub-float v2, p1, v2

    .line 125
    invoke-virtual {v1}, Lxe;->b()F

    move-result v3

    invoke-virtual {v0}, Lxe;->b()F

    move-result v4

    sub-float/2addr v3, v4

    div-float/2addr v2, v3

    .line 126
    invoke-virtual {v0}, Lxe;->a()Ljava/lang/Object;

    move-result-object v0

    .line 127
    invoke-virtual {v1}, Lxe;->a()Ljava/lang/Object;

    move-result-object v1

    .line 126
    invoke-interface {p2, v2, v0, v1}, Lxi;->a(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 156
    :goto_0
    return-object v0

    .line 128
    :cond_1
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_3

    .line 129
    iget-object v0, p0, Lxf;->a:Ljava/util/ArrayList;

    iget v1, p0, Lxf;->b:I

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxe;

    .line 130
    iget-object v1, p0, Lxf;->a:Ljava/util/ArrayList;

    iget v2, p0, Lxf;->b:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lxe;

    .line 131
    invoke-virtual {v1}, Lxe;->c()Lwt;

    move-result-object v2

    .line 132
    if-eqz v2, :cond_2

    .line 133
    invoke-interface {v2, p1}, Lwt;->getInterpolation(F)F

    move-result p1

    .line 135
    :cond_2
    invoke-virtual {v0}, Lxe;->b()F

    move-result v2

    sub-float v2, p1, v2

    .line 136
    invoke-virtual {v1}, Lxe;->b()F

    move-result v3

    invoke-virtual {v0}, Lxe;->b()F

    move-result v4

    sub-float/2addr v3, v4

    div-float/2addr v2, v3

    .line 137
    invoke-virtual {v0}, Lxe;->a()Ljava/lang/Object;

    move-result-object v0

    .line 138
    invoke-virtual {v1}, Lxe;->a()Ljava/lang/Object;

    move-result-object v1

    .line 137
    invoke-interface {p2, v2, v0, v1}, Lxi;->a(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 140
    :cond_3
    iget-object v0, p0, Lxf;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxe;

    move-object v2, v0

    .line 141
    :goto_1
    iget v0, p0, Lxf;->b:I

    if-ge v1, v0, :cond_6

    .line 142
    iget-object v0, p0, Lxf;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxe;

    .line 143
    invoke-virtual {v0}, Lxe;->b()F

    move-result v3

    cmpg-float v3, p1, v3

    if-gez v3, :cond_5

    .line 144
    invoke-virtual {v0}, Lxe;->c()Lwt;

    move-result-object v1

    .line 145
    if-eqz v1, :cond_4

    .line 146
    invoke-interface {v1, p1}, Lwt;->getInterpolation(F)F

    move-result p1

    .line 148
    :cond_4
    invoke-virtual {v2}, Lxe;->b()F

    move-result v1

    sub-float v1, p1, v1

    .line 149
    invoke-virtual {v0}, Lxe;->b()F

    move-result v3

    invoke-virtual {v2}, Lxe;->b()F

    move-result v4

    sub-float/2addr v3, v4

    div-float/2addr v1, v3

    .line 150
    invoke-virtual {v2}, Lxe;->a()Ljava/lang/Object;

    move-result-object v2

    .line 151
    invoke-virtual {v0}, Lxe;->a()Ljava/lang/Object;

    move-result-object v0

    .line 150
    invoke-interface {p2, v1, v2, v0}, Lxi;->a(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto/16 :goto_0

    .line 141
    :cond_5
    add-int/lit8 v1, v1, 0x1

    move-object v2, v0

    goto :goto_1

    .line 156
    :cond_6
    iget-object v0, p0, Lxf;->a:Ljava/util/ArrayList;

    iget v1, p0, Lxf;->b:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxe;

    invoke-virtual {v0}, Lxe;->a()Ljava/lang/Object;

    move-result-object v0

    goto/16 :goto_0
.end method
