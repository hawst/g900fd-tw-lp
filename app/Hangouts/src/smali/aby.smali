.class public final Laby;
.super Lbor;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Laby;->a:Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;

    invoke-direct {p0}, Lbor;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILyj;Lbea;Ljava/lang/Exception;)V
    .locals 6

    .prologue
    .line 117
    iget-object v0, p0, Laby;->a:Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->getActivity()Ly;

    move-result-object v2

    .line 118
    iget-object v0, p0, Laby;->a:Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->e(Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;)I

    move-result v0

    if-ne v0, p1, :cond_0

    const-class v0, Lbdf;

    .line 119
    invoke-virtual {v0, p3}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Laby;->a:Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->f(Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;)V

    .line 121
    invoke-static {}, Lack;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    sget v0, Lh;->jL:I

    .line 124
    :goto_0
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lf;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Laby;->a:Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;

    iget-object v1, p0, Laby;->a:Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->a(Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;Landroid/view/View;)V

    .line 127
    :cond_0
    iget-object v0, p0, Laby;->a:Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->c(Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;)Lfe;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfe;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacb;

    .line 128
    if-eqz v0, :cond_1

    const-class v1, Lbde;

    invoke-virtual {v1, p3}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 129
    iget-object v1, p0, Laby;->a:Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;

    invoke-static {v1, p1}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->a(Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;I)V

    .line 130
    iget-object v1, p0, Laby;->a:Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->d(Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;)Labz;

    move-result-object v1

    invoke-virtual {v1}, Labz;->notifyDataSetChanged()V

    .line 132
    invoke-static {}, Lack;->j()Z

    move-result v1

    if-eqz v1, :cond_3

    sget v1, Lh;->ku:I

    .line 136
    :goto_1
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v0, v0, Lacb;->b:Ljava/lang/String;

    aput-object v0, v4, v5

    invoke-virtual {v3, v1, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 135
    invoke-static {v2, v0}, Lf;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 138
    :cond_1
    return-void

    .line 121
    :cond_2
    sget v0, Lh;->jM:I

    goto :goto_0

    .line 132
    :cond_3
    sget v1, Lh;->kv:I

    goto :goto_1
.end method

.method public a(ILyj;Lbos;)V
    .locals 2

    .prologue
    .line 100
    invoke-virtual {p3}, Lbos;->c()Lbfz;

    move-result-object v0

    .line 101
    iget-object v1, p0, Laby;->a:Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->e(Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;)I

    move-result v1

    if-ne v1, p1, :cond_1

    const-class v1, Lbhj;

    .line 102
    invoke-virtual {v1, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 103
    iget-object v0, p0, Laby;->a:Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->f(Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;)V

    .line 112
    :cond_0
    :goto_0
    return-void

    .line 106
    :cond_1
    const-class v1, Lbhi;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Laby;->a:Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;

    invoke-static {v0, p1}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->a(Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;I)V

    .line 108
    iget-object v0, p0, Laby;->a:Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->d(Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;)Labz;

    move-result-object v0

    invoke-virtual {v0}, Labz;->notifyDataSetChanged()V

    .line 110
    iget-object v0, p0, Laby;->a:Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;

    iget-object v1, p0, Laby;->a:Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->a(Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;Landroid/view/View;)V

    goto :goto_0
.end method
