.class public final Ldjr;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldjr;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ldjs;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Ldjr;

    sput-object v0, Ldjr;->a:[Ldjr;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lepn;-><init>()V

    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Ldjr;->c:Ldjs;

    .line 10
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 122
    const/4 v0, 0x0

    .line 123
    iget-object v1, p0, Ldjr;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 124
    const/4 v0, 0x1

    iget-object v1, p0, Ldjr;->b:Ljava/lang/String;

    .line 125
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 127
    :cond_0
    iget-object v1, p0, Ldjr;->c:Ldjs;

    if-eqz v1, :cond_1

    .line 128
    const/4 v1, 0x2

    iget-object v2, p0, Ldjr;->c:Ldjs;

    .line 129
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 131
    :cond_1
    iget-object v1, p0, Ldjr;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 132
    iput v0, p0, Ldjr;->cachedSize:I

    .line 133
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldjr;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldjr;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldjr;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldjr;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldjr;->c:Ldjs;

    if-nez v0, :cond_2

    new-instance v0, Ldjs;

    invoke-direct {v0}, Ldjs;-><init>()V

    iput-object v0, p0, Ldjr;->c:Ldjs;

    :cond_2
    iget-object v0, p0, Ldjr;->c:Ldjs;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 110
    iget-object v0, p0, Ldjr;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 111
    const/4 v0, 0x1

    iget-object v1, p0, Ldjr;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 113
    :cond_0
    iget-object v0, p0, Ldjr;->c:Ldjs;

    if-eqz v0, :cond_1

    .line 114
    const/4 v0, 0x2

    iget-object v1, p0, Ldjr;->c:Ldjs;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 116
    :cond_1
    iget-object v0, p0, Ldjr;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 118
    return-void
.end method
