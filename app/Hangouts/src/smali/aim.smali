.class final Laim;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final synthetic a:Laij;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lain;",
            ">;"
        }
    .end annotation
.end field

.field private d:I

.field private e:Z


# direct methods
.method private constructor <init>(Laij;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2459
    iput-object p1, p0, Laim;->a:Laij;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2460
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Laim;->b:Ljava/util/List;

    .line 2461
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Laim;->c:Ljava/util/List;

    .line 2463
    iput v1, p0, Laim;->d:I

    .line 2464
    iput-boolean v1, p0, Laim;->e:Z

    return-void
.end method

.method synthetic constructor <init>(Laij;B)V
    .locals 0

    .prologue
    .line 2459
    invoke-direct {p0, p1}, Laim;-><init>(Laij;)V

    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2480
    iget-boolean v0, p0, Laim;->e:Z

    if-eqz v0, :cond_0

    iget v0, p0, Laim;->d:I

    iget-object v1, p0, Laim;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 2481
    iget-object v0, p0, Laim;->a:Laij;

    iget-object v1, p0, Laim;->c:Ljava/util/List;

    invoke-virtual {v0, v1, v2, v2}, Laij;->a(Ljava/util/List;ZZ)V

    .line 2482
    iget-object v0, p0, Laim;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2483
    iget-object v0, p0, Laim;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2484
    const/4 v0, 0x0

    iput v0, p0, Laim;->d:I

    .line 2486
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2493
    iput-boolean v0, p0, Laim;->e:Z

    .line 2494
    iput v0, p0, Laim;->d:I

    .line 2495
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2498
    iget-object v0, p0, Laim;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2499
    iget v0, p0, Laim;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Laim;->d:I

    .line 2500
    return-void
.end method

.method public a(Lain;)Z
    .locals 2

    .prologue
    .line 2471
    iget-object v0, p0, Laim;->b:Ljava/util/List;

    iget-object v1, p1, Lain;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2472
    iget-object v0, p0, Laim;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2473
    invoke-direct {p0}, Laim;->c()V

    .line 2474
    const/4 v0, 0x1

    .line 2476
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 2503
    const/4 v0, 0x1

    iput-boolean v0, p0, Laim;->e:Z

    .line 2504
    invoke-direct {p0}, Laim;->c()V

    .line 2505
    return-void
.end method
