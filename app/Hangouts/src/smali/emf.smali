.class public final Lemf;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lemf;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Boolean;

.field public d:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1314
    const/4 v0, 0x0

    new-array v0, v0, [Lemf;

    sput-object v0, Lemf;->a:[Lemf;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1315
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1341
    const/4 v0, 0x0

    .line 1342
    iget-object v1, p0, Lemf;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1343
    const/4 v0, 0x1

    iget-object v1, p0, Lemf;->b:Ljava/lang/String;

    .line 1344
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1346
    :cond_0
    iget-object v1, p0, Lemf;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 1347
    const/4 v1, 0x2

    iget-object v2, p0, Lemf;->c:Ljava/lang/Boolean;

    .line 1348
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1350
    :cond_1
    iget-object v1, p0, Lemf;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 1351
    const/4 v1, 0x3

    iget-object v2, p0, Lemf;->d:Ljava/lang/Boolean;

    .line 1352
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1354
    :cond_2
    iget-object v1, p0, Lemf;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1355
    iput v0, p0, Lemf;->cachedSize:I

    .line 1356
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 1311
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lemf;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lemf;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lemf;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lemf;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lemf;->c:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lemf;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 1326
    iget-object v0, p0, Lemf;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1327
    const/4 v0, 0x1

    iget-object v1, p0, Lemf;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1329
    :cond_0
    iget-object v0, p0, Lemf;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1330
    const/4 v0, 0x2

    iget-object v1, p0, Lemf;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 1332
    :cond_1
    iget-object v0, p0, Lemf;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 1333
    const/4 v0, 0x3

    iget-object v1, p0, Lemf;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 1335
    :cond_2
    iget-object v0, p0, Lemf;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1337
    return-void
.end method
