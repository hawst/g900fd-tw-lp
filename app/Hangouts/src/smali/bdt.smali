.class public Lbdt;
.super Lbdp;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 146
    invoke-direct {p0}, Lbdp;-><init>()V

    .line 147
    iput-object p1, p0, Lbdt;->a:Ljava/lang/String;

    .line 148
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 3

    .prologue
    .line 153
    new-instance v0, Ldyp;

    invoke-direct {v0}, Ldyp;-><init>()V

    .line 154
    const-string v1, "conversation"

    iput-object v1, v0, Ldyp;->b:Ljava/lang/String;

    .line 155
    iget-object v1, p0, Lbdt;->a:Ljava/lang/String;

    iput-object v1, v0, Ldyp;->c:Ljava/lang/String;

    .line 157
    new-instance v1, Ldza;

    invoke-direct {v1}, Ldza;-><init>()V

    .line 158
    invoke-virtual {p0, p1, p2}, Lbdt;->c(Ljava/lang/String;I)Ldvm;

    move-result-object v2

    iput-object v2, v1, Ldza;->b:Ldvm;

    .line 159
    iput-object v0, v1, Ldza;->c:Ldyp;

    .line 160
    return-object v1
.end method

.method public a(Lyj;Lbph;)V
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lbdt;->a:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Lyj;Ljava/lang/String;)V

    .line 138
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165
    const-string v0, "hangouts/resolve"

    return-object v0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 172
    const-wide/16 v0, 0x2710

    return-wide v0
.end method
