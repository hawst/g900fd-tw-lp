.class public final Ldym;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldym;


# instance fields
.field public b:Ldvn;

.field public c:Ldzg;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1499
    const/4 v0, 0x0

    new-array v0, v0, [Ldym;

    sput-object v0, Ldym;->a:[Ldym;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1500
    invoke-direct {p0}, Lepn;-><init>()V

    .line 1503
    iput-object v0, p0, Ldym;->b:Ldvn;

    .line 1506
    iput-object v0, p0, Ldym;->c:Ldzg;

    .line 1500
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1523
    const/4 v0, 0x0

    .line 1524
    iget-object v1, p0, Ldym;->b:Ldvn;

    if-eqz v1, :cond_0

    .line 1525
    const/4 v0, 0x1

    iget-object v1, p0, Ldym;->b:Ldvn;

    .line 1526
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1528
    :cond_0
    iget-object v1, p0, Ldym;->c:Ldzg;

    if-eqz v1, :cond_1

    .line 1529
    const/4 v1, 0x2

    iget-object v2, p0, Ldym;->c:Ldzg;

    .line 1530
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1532
    :cond_1
    iget-object v1, p0, Ldym;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1533
    iput v0, p0, Ldym;->cachedSize:I

    .line 1534
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 1496
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldym;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldym;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldym;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldym;->b:Ldvn;

    if-nez v0, :cond_2

    new-instance v0, Ldvn;

    invoke-direct {v0}, Ldvn;-><init>()V

    iput-object v0, p0, Ldym;->b:Ldvn;

    :cond_2
    iget-object v0, p0, Ldym;->b:Ldvn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldym;->c:Ldzg;

    if-nez v0, :cond_3

    new-instance v0, Ldzg;

    invoke-direct {v0}, Ldzg;-><init>()V

    iput-object v0, p0, Ldym;->c:Ldzg;

    :cond_3
    iget-object v0, p0, Ldym;->c:Ldzg;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 1511
    iget-object v0, p0, Ldym;->b:Ldvn;

    if-eqz v0, :cond_0

    .line 1512
    const/4 v0, 0x1

    iget-object v1, p0, Ldym;->b:Ldvn;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1514
    :cond_0
    iget-object v0, p0, Ldym;->c:Ldzg;

    if-eqz v0, :cond_1

    .line 1515
    const/4 v0, 0x2

    iget-object v1, p0, Ldym;->c:Ldzg;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1517
    :cond_1
    iget-object v0, p0, Ldym;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1519
    return-void
.end method
