.class public final Lame;
.super Lpi;
.source "PG"

# interfaces
.implements Landroid/widget/Filterable;
.implements Law;
.implements Lbsb;
.implements Lbsl;
.implements Lbzl;
.implements Lcbo;
.implements Lcec;
.implements Lcfv;
.implements Lcfw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lpi;",
        "Landroid/widget/Filterable;",
        "Law",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lbsb;",
        "Lbsl;",
        "Lbzl;",
        "Lcbo;",
        "Lcec;",
        "Lcfv;",
        "Lcfw;"
    }
.end annotation


# static fields
.field private static final b:Z

.field private static s:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static t:Ljava/lang/String;

.field private static u:Ljava/lang/String;

.field private static v:Ljava/lang/String;


# instance fields
.field a:Lbor;

.field private final c:Landroid/content/Context;

.field private final d:Lt;

.field private final e:Lav;

.field private final f:Lyj;

.field private final g:I

.field private h:Lamk;

.field private i:Ljava/lang/String;

.field private final j:Lamd;

.field private final k:Laml;

.field private l:I

.field private m:Lcvi;

.field private n:Lcvo;

.field private o:Lbsj;

.field private p:Lcvz;

.field private final q:Landroid/os/Handler;

.field private r:Lamj;

.field private final w:Lcvr;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 86
    sget-object v0, Lbys;->d:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lame;->b:Z

    .line 197
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lame;->s:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lt;Lav;Lyj;I)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 233
    invoke-direct {p0, p1, v1}, Lpi;-><init>(Landroid/content/Context;B)V

    .line 130
    new-instance v0, Lamf;

    invoke-direct {v0, p0}, Lamf;-><init>(Lame;)V

    iput-object v0, p0, Lame;->a:Lbor;

    .line 154
    new-instance v0, Lamd;

    invoke-direct {v0}, Lamd;-><init>()V

    iput-object v0, p0, Lame;->j:Lamd;

    .line 173
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lame;->q:Landroid/os/Handler;

    .line 207
    new-instance v0, Lamg;

    invoke-direct {v0, p0}, Lamg;-><init>(Lame;)V

    iput-object v0, p0, Lame;->w:Lcvr;

    move v2, v1

    .line 234
    :goto_0
    const/16 v0, 0x9

    if-ge v2, v0, :cond_1

    .line 235
    const/4 v0, 0x3

    if-lt v2, v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p0, v0}, Lame;->a(Z)V

    .line 234
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    move v0, v1

    .line 235
    goto :goto_1

    .line 238
    :cond_1
    iput-object p1, p0, Lame;->c:Landroid/content/Context;

    .line 239
    iput-object p2, p0, Lame;->d:Lt;

    .line 240
    iput-object p3, p0, Lame;->e:Lav;

    .line 241
    iput-object p4, p0, Lame;->f:Lyj;

    .line 242
    iput p5, p0, Lame;->g:I

    .line 243
    new-instance v0, Laml;

    iget-object v1, p0, Lame;->j:Lamd;

    invoke-direct {v0, v1, p5}, Laml;-><init>(Lamd;I)V

    iput-object v0, p0, Lame;->k:Laml;

    .line 244
    iget-object v0, p0, Lame;->k:Laml;

    iget-object v1, p0, Lame;->f:Lyj;

    invoke-virtual {v1}, Lyj;->c()Lbdk;

    move-result-object v1

    invoke-virtual {v0, v1}, Laml;->a(Lbdk;)V

    .line 246
    sget-object v0, Lame;->t:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 247
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->jb:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lame;->t:Ljava/lang/String;

    .line 250
    :cond_2
    sget-object v0, Lame;->u:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 251
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->cA:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lame;->u:Ljava/lang/String;

    .line 253
    :cond_3
    sget-object v0, Lame;->v:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 254
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->aL:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lame;->v:Ljava/lang/String;

    .line 257
    :cond_4
    iget-object v0, p0, Lame;->f:Lyj;

    invoke-virtual {v0}, Lyj;->u()Z

    move-result v0

    if-nez v0, :cond_6

    .line 259
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    .line 260
    iget-object v0, p0, Lame;->f:Lyj;

    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v3

    .line 261
    sget-object v0, Lame;->s:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 262
    if-eqz v0, :cond_5

    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v4

    const-wide/32 v6, 0x1d4c0

    add-long/2addr v4, v6

    sub-long/2addr v4, v1

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-gez v0, :cond_6

    .line 264
    :cond_5
    const-string v0, "Babel"

    const-string v4, "Sending an empty query to prime search cache on server side"

    invoke-static {v0, v4}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    iget-object v0, p0, Lame;->f:Lyj;

    const-string v4, ""

    invoke-static {v0, v4}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->k(Lyj;Ljava/lang/String;)I

    .line 266
    sget-object v0, Lame;->s:Ljava/util/Map;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    :cond_6
    return-void
.end method

.method static synthetic a(Lame;)I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lame;->l:I

    return v0
.end method

.method static synthetic a(Lame;Lbhv;)V
    .locals 3

    .prologue
    .line 78
    sget-boolean v0, Lame;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Public profile search returns results: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lbhv;->f()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lame;->k:Laml;

    invoke-virtual {p1}, Lbhv;->f()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Laml;->a(Ljava/util/List;)Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0, v0}, Lame;->a(Ljava/util/Set;)V

    invoke-direct {p0}, Lame;->l()V

    return-void
.end method

.method private a(Landroid/database/Cursor;I)V
    .locals 1

    .prologue
    .line 647
    if-eqz p1, :cond_0

    invoke-virtual {p0, p2}, Lame;->c(I)Landroid/database/Cursor;

    move-result-object v0

    if-eq v0, p1, :cond_0

    .line 651
    invoke-virtual {p0, p2, p1}, Lame;->a(ILandroid/database/Cursor;)V

    .line 653
    :cond_0
    return-void
.end method

.method private a(Ljava/util/Set;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 634
    iget-object v0, p0, Lame;->m:Lcvi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lame;->m:Lcvi;

    invoke-virtual {v0}, Lcvi;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 644
    :cond_0
    :goto_0
    return-void

    .line 637
    :cond_1
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 640
    new-instance v0, Lcvm;

    invoke-direct {v0}, Lcvm;-><init>()V

    .line 641
    invoke-virtual {v0, p1}, Lcvm;->a(Ljava/util/Collection;)Lcvm;

    .line 642
    iget-object v1, p0, Lame;->m:Lcvi;

    iget-object v2, p0, Lame;->w:Lcvr;

    iget-object v3, p0, Lame;->f:Lyj;

    invoke-virtual {v3}, Lyj;->ak()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lame;->f:Lyj;

    .line 643
    invoke-virtual {v4}, Lyj;->al()Ljava/lang/String;

    move-result-object v4

    .line 642
    invoke-virtual {v1, v2, v3, v4, v0}, Lcvi;->a(Lcvr;Ljava/lang/String;Ljava/lang/String;Lcvm;)V

    goto :goto_0
.end method

.method static synthetic b(Lame;)Lamd;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lame;->j:Lamd;

    return-object v0
.end method

.method static synthetic c(Lame;)V
    .locals 0

    .prologue
    .line 78
    invoke-virtual {p0}, Lame;->c()V

    return-void
.end method

.method static synthetic d(Lame;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lame;->q:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic e(Lame;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Lame;->m()V

    return-void
.end method

.method private l()V
    .locals 2

    .prologue
    .line 660
    iget-object v0, p0, Lame;->k:Laml;

    invoke-virtual {v0}, Laml;->f()Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x8

    invoke-direct {p0, v0, v1}, Lame;->a(Landroid/database/Cursor;I)V

    .line 662
    iget-object v0, p0, Lame;->k:Laml;

    invoke-virtual {v0}, Laml;->e()Landroid/database/Cursor;

    move-result-object v0

    const/4 v1, 0x7

    invoke-direct {p0, v0, v1}, Lame;->a(Landroid/database/Cursor;I)V

    .line 663
    return-void
.end method

.method private m()V
    .locals 2

    .prologue
    .line 671
    iget-object v0, p0, Lame;->k:Laml;

    invoke-virtual {v0}, Laml;->a()Landroid/database/Cursor;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lame;->a(Landroid/database/Cursor;I)V

    .line 673
    iget-object v0, p0, Lame;->f:Lyj;

    invoke-virtual {v0}, Lyj;->s()Z

    move-result v0

    if-nez v0, :cond_0

    .line 674
    iget-object v0, p0, Lame;->k:Laml;

    invoke-virtual {v0}, Laml;->b()Landroid/database/Cursor;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lame;->a(Landroid/database/Cursor;I)V

    .line 677
    :cond_0
    iget-object v0, p0, Lame;->k:Laml;

    invoke-virtual {v0}, Laml;->c()Landroid/database/Cursor;

    move-result-object v0

    const/4 v1, 0x2

    invoke-direct {p0, v0, v1}, Lame;->a(Landroid/database/Cursor;I)V

    .line 678
    iget-object v0, p0, Lame;->k:Laml;

    invoke-virtual {v0}, Laml;->d()Landroid/database/Cursor;

    move-result-object v0

    const/4 v1, 0x3

    invoke-direct {p0, v0, v1}, Lame;->a(Landroid/database/Cursor;I)V

    .line 679
    invoke-direct {p0}, Lame;->l()V

    .line 680
    return-void
.end method

.method private n()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1094
    new-instance v0, Lbak;

    new-array v1, v4, [Ljava/lang/String;

    const-string v2, "_id"

    aput-object v2, v1, v3

    invoke-direct {v0, v1}, Lbak;-><init>([Ljava/lang/String;)V

    .line 1095
    new-array v1, v4, [Ljava/lang/Object;

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lbak;->a([Ljava/lang/Object;)V

    .line 1096
    const/4 v1, 0x4

    invoke-virtual {p0, v1, v0}, Lame;->a(ILandroid/database/Cursor;)V

    .line 1097
    return-void
.end method


# virtual methods
.method protected a(II)I
    .locals 0

    .prologue
    .line 694
    return p1
.end method

.method protected a(Landroid/content/Context;II)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 731
    packed-switch p2, :pswitch_data_0

    .line 754
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 736
    :pswitch_0
    iget-object v0, p0, Lame;->d:Lt;

    iget v1, p0, Lame;->g:I

    invoke-static {p1, v0, v4, v1}, Lceb;->createInstance(Landroid/content/Context;Lt;ZI)Lceb;

    move-result-object v0

    .line 738
    invoke-virtual {v0, p0}, Lceb;->setPersonSelectedListener(Lcec;)V

    goto :goto_0

    .line 742
    :pswitch_1
    new-instance v0, Lcba;

    invoke-direct {v0, p1}, Lcba;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 748
    :pswitch_2
    new-instance v0, Lcbn;

    iget-object v2, p0, Lame;->d:Lt;

    iget-object v3, p0, Lame;->f:Lyj;

    iget v5, p0, Lame;->g:I

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcbn;-><init>(Landroid/content/Context;Lt;Lyj;ZI)V

    .line 750
    invoke-virtual {v0, p0}, Lcbn;->a(Lcbo;)V

    goto :goto_0

    .line 731
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected a(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 699
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 700
    sget v1, Lf;->ea:I

    .line 701
    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 702
    packed-switch p2, :pswitch_data_0

    .line 723
    const-string v1, "New header view for unknown partition."

    invoke-static {v1}, Lcwz;->a(Ljava/lang/String;)V

    .line 725
    :goto_0
    return-object v0

    .line 704
    :pswitch_0
    sget v1, Lh;->io:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 707
    :pswitch_1
    sget v1, Lh;->in:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lame;->f:Lyj;

    .line 708
    invoke-virtual {v3}, Lyj;->m()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 707
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 711
    :pswitch_2
    sget v1, Lh;->ip:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 714
    :pswitch_3
    sget v1, Lh;->i:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 717
    :pswitch_4
    sget v1, Lh;->k:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 720
    :pswitch_5
    sget v1, Lh;->j:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 702
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Laea;)V
    .locals 5

    .prologue
    .line 990
    invoke-virtual {p1}, Laea;->n()Laee;

    move-result-object v1

    .line 991
    const-string v0, "Selected contact should have a selected item"

    invoke-static {v0, v1}, Lcwz;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 994
    invoke-virtual {v1}, Laee;->c()Lbdh;

    move-result-object v2

    .line 995
    if-eqz v2, :cond_1

    const/4 v0, 0x0

    .line 1001
    :goto_0
    if-nez v0, :cond_3

    .line 1002
    invoke-static {v2}, Lbcx;->a(Lbdh;)Lbcx;

    move-result-object v3

    .line 1003
    iget-object v0, v2, Lbdh;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, v2, Lbdh;->g:Ljava/lang/String;

    .line 1004
    :goto_1
    iget-object v2, v2, Lbdh;->h:Ljava/lang/String;

    move-object v4, v2

    move-object v2, v0

    move-object v0, v4

    .line 1011
    :goto_2
    invoke-virtual {v1}, Laee;->d()Lbcn;

    move-result-object v1

    .line 1012
    if-eqz v1, :cond_0

    .line 1013
    invoke-virtual {v3, v1}, Lbcx;->a(Lbcn;)V

    .line 1015
    :cond_0
    iget-object v1, p0, Lame;->h:Lamk;

    invoke-static {v3, v2, v0}, Lxs;->a(Lbcx;Ljava/lang/String;Ljava/lang/String;)Lxs;

    move-result-object v0

    invoke-interface {v1, v0}, Lamk;->a(Lxs;)V

    .line 1016
    return-void

    :cond_1
    move-object v0, v1

    .line 995
    check-cast v0, Laeh;

    goto :goto_0

    .line 1003
    :cond_2
    iget-object v0, v2, Lbdh;->e:Ljava/lang/String;

    goto :goto_1

    .line 1006
    :cond_3
    iget-object v0, v0, Laeh;->a:Ljava/lang/String;

    invoke-static {v0}, Lbcx;->a(Ljava/lang/String;)Lbcx;

    move-result-object v3

    .line 1007
    invoke-virtual {p1}, Laea;->e()Ljava/lang/String;

    move-result-object v2

    .line 1008
    invoke-virtual {p1}, Laea;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public a(Lamk;)V
    .locals 0

    .prologue
    .line 275
    iput-object p1, p0, Lame;->h:Lamk;

    .line 276
    return-void
.end method

.method public a(Landroid/view/View;I)V
    .locals 4

    .prologue
    .line 924
    invoke-virtual {p0, p2}, Lame;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 925
    if-nez v0, :cond_0

    .line 971
    :goto_0
    return-void

    .line 932
    :cond_0
    invoke-virtual {p0, p2}, Lame;->d(I)I

    move-result v1

    .line 933
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 938
    :pswitch_0
    instance-of v0, p1, Lceb;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 939
    check-cast v0, Lceb;

    .line 940
    invoke-virtual {v0, p1}, Lceb;->onClick(Landroid/view/View;)V

    goto :goto_0

    .line 942
    :cond_1
    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "PeopleSearchListAdapter expected PeopleListItemView for the "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " partition."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 952
    :pswitch_1
    instance-of v0, p1, Lcbn;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 953
    check-cast v0, Lcbn;

    .line 954
    invoke-virtual {v0, p1}, Lcbn;->onClick(Landroid/view/View;)V

    goto :goto_0

    .line 956
    :cond_2
    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "PeopleSearchListAdapter expected ContactListItemView for the "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " partition."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 963
    :pswitch_2
    invoke-static {}, Lxo;->newBuilder()Lxp;

    move-result-object v1

    .line 964
    const/4 v2, 0x1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 965
    invoke-virtual {v1, v2}, Lxp;->a(Ljava/lang/String;)Lxp;

    .line 966
    const/4 v2, 0x2

    .line 967
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 966
    invoke-static {v2}, Lxq;->a(I)Lxq;

    move-result-object v2

    invoke-virtual {v1, v2}, Lxp;->a(Lxq;)Lxp;

    .line 968
    const/4 v2, 0x3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lxp;->b(Ljava/lang/String;)Lxp;

    .line 969
    const/4 v2, 0x4

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {v1, v0}, Lxp;->a(I)Lxp;

    .line 970
    iget-object v0, p0, Lame;->h:Lamk;

    invoke-virtual {v1}, Lxp;->a()Lxo;

    move-result-object v1

    invoke-interface {v0, v1}, Lamk;->a(Lxo;)V

    goto/16 :goto_0

    .line 933
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected a(Landroid/view/View;ILandroid/database/Cursor;I)V
    .locals 9

    .prologue
    .line 778
    instance-of v0, p1, Lcax;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 779
    check-cast v0, Lcax;

    .line 780
    invoke-virtual {v0}, Lcax;->reset()V

    .line 782
    :cond_0
    sget-boolean v0, Lame;->b:Z

    if-eqz v0, :cond_1

    .line 783
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PeopleSearchListadapter. BindView. Partition: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Position: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 787
    :cond_1
    packed-switch p2, :pswitch_data_0

    .line 917
    :goto_0
    return-void

    .line 792
    :pswitch_0
    instance-of v0, p1, Lceb;

    if-nez v0, :cond_2

    .line 793
    const-string v0, "Should only have PeopleListItemViews in PARTITION_SUGGESTED_PEOPLE"

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 795
    check-cast v0, Lceb;

    .line 796
    iget-object v1, p0, Lame;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lceb;->setHighlightedText(Ljava/lang/String;)V

    .line 797
    new-instance v7, Lbdk;

    const/4 v1, 0x1

    .line 798
    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    .line 799
    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v7, v1, v2}, Lbdk;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 800
    const/4 v1, 0x3

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 802
    const/4 v1, 0x0

    invoke-static {v7, v1, v8}, Lbcx;->a(Lbdk;Ljava/lang/String;Ljava/lang/String;)Lbcx;

    move-result-object v1

    .line 804
    const/4 v2, 0x4

    .line 805
    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, p0, Lame;->f:Lyj;

    const/4 v5, 0x0

    if-nez p2, :cond_5

    const/4 v6, 0x1

    .line 804
    :goto_1
    invoke-virtual/range {v0 .. v6}, Lceb;->setInviteeId(Lbcx;Ljava/lang/String;ZLyj;ZZ)V

    .line 809
    invoke-virtual {v0, v8}, Lceb;->setContactName(Ljava/lang/String;)V

    .line 811
    const/4 v1, 0x0

    .line 812
    const/4 v2, 0x0

    .line 817
    invoke-static {}, Lbsc;->a()Lbsc;

    move-result-object v3

    .line 819
    :try_start_0
    invoke-virtual {v3}, Lbsc;->c()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 820
    iget-object v1, v7, Lbdk;->a:Ljava/lang/String;

    invoke-virtual {v3, v1}, Lbsc;->a(Ljava/lang/String;)Laea;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 826
    :goto_2
    invoke-virtual {v3}, Lbsc;->b()V

    .line 829
    const/4 v3, 0x2

    if-ne p2, v3, :cond_7

    .line 830
    if-nez v1, :cond_3

    .line 832
    const/4 v3, 0x6

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    .line 834
    if-eqz v3, :cond_3

    .line 835
    invoke-static {v3}, Laea;->a([B)Laea;

    move-result-object v1

    .line 852
    :cond_3
    :goto_3
    iget-object v3, p0, Lame;->j:Lamd;

    iget-object v4, p0, Lame;->n:Lcvo;

    .line 853
    invoke-virtual {v7}, Lbdk;->c()Ljava/lang/String;

    move-result-object v5

    .line 852
    invoke-virtual {v3, v4, v5}, Lamd;->a(Ladw;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 855
    invoke-virtual {v0, v3, v2}, Lceb;->setDetails(Ljava/lang/String;Ljava/lang/String;)V

    .line 857
    if-eqz v1, :cond_4

    .line 858
    iget v2, p0, Lame;->g:I

    .line 859
    invoke-static {v2}, Lf;->c(I)Z

    move-result v2

    .line 858
    invoke-virtual {v0, v1, v2}, Lceb;->setContactDetails(Laea;Z)V

    .line 862
    :cond_4
    invoke-virtual {v0}, Lceb;->updateContentDescription()V

    goto :goto_0

    .line 805
    :cond_5
    const/4 v6, 0x0

    goto :goto_1

    .line 823
    :cond_6
    :try_start_1
    const-string v4, "Babel"

    const-string v5, "GmsPeopleCache not ready. Skipping lookup contact details"

    invoke-static {v4, v5}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 826
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lbsc;->b()V

    throw v0

    .line 838
    :cond_7
    const/16 v3, 0x8

    if-eq p2, v3, :cond_8

    const/4 v3, 0x7

    if-ne p2, v3, :cond_3

    .line 842
    :cond_8
    const/4 v2, 0x7

    .line 843
    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/16 v2, 0x8

    .line 845
    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/16 v2, 0x9

    .line 847
    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 842
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_9

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_9

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v4, Lame;->t:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    :cond_9
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    move-object v2, v3

    goto :goto_3

    :cond_a
    move-object v2, v4

    goto :goto_3

    .line 866
    :pswitch_1
    instance-of v0, p1, Lcba;

    if-nez v0, :cond_b

    .line 867
    const-string v0, "Should only have CircleListItemViews in PARTITION_CIRCLES"

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 869
    :cond_b
    check-cast p1, Lcba;

    .line 870
    iget-object v0, p0, Lame;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcba;->a(Ljava/lang/String;)V

    .line 871
    const/4 v0, 0x1

    .line 872
    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    .line 873
    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v2, 0x3

    .line 874
    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x4

    .line 875
    invoke-interface {p3, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 871
    invoke-virtual {p1, v0, v1, v2, v3}, Lcba;->a(Ljava/lang/String;ILjava/lang/String;I)V

    goto/16 :goto_0

    .line 879
    :pswitch_2
    instance-of v0, p1, Lcbn;

    if-nez v0, :cond_c

    .line 880
    const-string v0, "Should only have ContactListItemViews in PARTITION_LOCAL_CONTACTS"

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_c
    move-object v0, p1

    .line 882
    check-cast v0, Lcbn;

    .line 883
    iget-object v1, p0, Lame;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcbn;->a(Ljava/lang/String;)V

    .line 885
    const/4 v1, 0x3

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    .line 887
    invoke-static {v1}, Laea;->a([B)Laea;

    move-result-object v4

    .line 889
    const/4 v1, 0x0

    .line 890
    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    .line 891
    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    .line 892
    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 889
    invoke-virtual/range {v0 .. v6}, Lcbn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Laea;ZZ)V

    goto/16 :goto_0

    .line 899
    :pswitch_3
    instance-of v0, p1, Lcbn;

    if-nez v0, :cond_d

    .line 900
    const-string v0, "Should only have ContactListItemViews in PARTITION_ADD_BY_PHONE"

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_d
    move-object v0, p1

    .line 902
    check-cast v0, Lcbn;

    .line 903
    iget-object v1, p0, Lame;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcbn;->a(Ljava/lang/String;)V

    .line 906
    const/4 v1, 0x0

    iget-object v2, p0, Lame;->i:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcbn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Laea;ZZ)V

    .line 907
    const/4 v1, 0x5

    if-ne p2, v1, :cond_e

    .line 908
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcbn;->a(I)V

    goto/16 :goto_0

    .line 909
    :cond_e
    const/4 v1, 0x6

    if-ne p2, v1, :cond_f

    .line 910
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcbn;->a(I)V

    goto/16 :goto_0

    .line 912
    :cond_f
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcbn;->a(I)V

    goto/16 :goto_0

    .line 787
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lbsj;Lcvx;Lcwf;)V
    .locals 1

    .prologue
    .line 612
    iget-object v0, p0, Lame;->k:Laml;

    invoke-virtual {v0, p2, p3}, Laml;->a(Lcvx;Lcwf;)V

    .line 613
    iget-object v0, p0, Lame;->r:Lamj;

    invoke-virtual {v0}, Lamj;->countDown()V

    .line 615
    invoke-direct {p0}, Lame;->m()V

    .line 616
    return-void
.end method

.method public a(Lceb;Laeh;)V
    .locals 4

    .prologue
    .line 979
    if-eqz p2, :cond_0

    iget-object v0, p2, Laeh;->a:Ljava/lang/String;

    .line 980
    invoke-static {v0}, Lbcx;->a(Ljava/lang/String;)Lbcx;

    move-result-object v0

    .line 982
    :goto_0
    invoke-virtual {p1}, Lceb;->getContactName()Ljava/lang/String;

    move-result-object v1

    .line 983
    invoke-virtual {p1}, Lceb;->getProfilePhotoUrl()Ljava/lang/String;

    move-result-object v2

    .line 985
    iget-object v3, p0, Lame;->h:Lamk;

    invoke-static {v0, v1, v2}, Lxs;->a(Lbcx;Ljava/lang/String;Ljava/lang/String;)Lxs;

    move-result-object v0

    invoke-interface {v3, v0}, Lamk;->a(Lxs;)V

    .line 986
    return-void

    .line 981
    :cond_0
    invoke-virtual {p1}, Lceb;->getInviteeId()Lbcx;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lcvz;)V
    .locals 3

    .prologue
    .line 590
    const-string v0, "Babel"

    const-string v1, "Circle ready for people search."

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    iget-object v0, p0, Lame;->p:Lcvz;

    if-eqz v0, :cond_0

    .line 593
    iget-object v0, p0, Lame;->p:Lcvz;

    invoke-virtual {v0}, Lcvz;->d()V

    .line 594
    const/4 v0, 0x0

    iput-object v0, p0, Lame;->p:Lcvz;

    .line 597
    :cond_0
    if-eqz p1, :cond_1

    .line 598
    iput-object p1, p0, Lame;->p:Lcvz;

    .line 599
    iget-object v0, p0, Lame;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 600
    iget-object v0, p0, Lame;->k:Laml;

    iget-object v1, p0, Lame;->p:Lcvz;

    iget-object v2, p0, Lame;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Laml;->a(Lcvz;Ljava/lang/String;)V

    .line 601
    invoke-direct {p0}, Lame;->m()V

    .line 604
    :cond_1
    return-void
.end method

.method public a(Ljava/lang/String;Lamj;)V
    .locals 10

    .prologue
    const/4 v5, 0x6

    const/4 v3, 0x5

    const/4 v4, 0x1

    const/4 v9, 0x0

    const/4 v7, 0x0

    .line 460
    iget-object v0, p0, Lame;->i:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 461
    invoke-virtual {p2}, Lamj;->a()V

    .line 515
    :cond_0
    :goto_0
    return-void

    .line 465
    :cond_1
    iget-object v0, p0, Lame;->k:Laml;

    invoke-virtual {v0, p1}, Laml;->a(Ljava/lang/String;)V

    .line 466
    iget-object v0, p0, Lame;->j:Lamd;

    invoke-virtual {v0}, Lamd;->a()V

    .line 471
    invoke-static {}, Lbsz;->a()Lbsz;

    move-result-object v0

    invoke-virtual {v0}, Lbsz;->b()V

    .line 474
    iput-object p1, p0, Lame;->i:Ljava/lang/String;

    .line 475
    iget-object v0, p0, Lame;->e:Lav;

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Lav;->a(I)V

    .line 476
    iget-object v0, p0, Lame;->o:Lbsj;

    if-eqz v0, :cond_2

    .line 477
    iget-object v0, p0, Lame;->o:Lbsj;

    invoke-virtual {v0}, Lbsj;->b()V

    .line 480
    :cond_2
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 481
    invoke-virtual {p0}, Lame;->b()V

    .line 482
    invoke-virtual {p2}, Lamj;->a()V

    goto :goto_0

    .line 484
    :cond_3
    iget-object v0, p0, Lame;->i:Ljava/lang/String;

    sget-object v1, Lame;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 486
    invoke-virtual {p2}, Lamj;->a()V

    .line 487
    iget-object v0, p0, Lame;->i:Ljava/lang/String;

    sget-object v1, Lame;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lame;->i:Ljava/lang/String;

    invoke-virtual {v1, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Laea;->a(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_0

    new-instance v0, Lcbn;

    iget-object v1, p0, Lame;->c:Landroid/content/Context;

    iget-object v2, p0, Lame;->d:Lt;

    iget-object v3, p0, Lame;->f:Lyj;

    iget v5, p0, Lame;->g:I

    invoke-direct/range {v0 .. v5}, Lcbn;-><init>(Landroid/content/Context;Lt;Lyj;ZI)V

    invoke-virtual {v0, p0}, Lcbn;->a(Lcbo;)V

    move-object v1, v9

    move-object v2, v6

    move-object v3, v9

    move-object v4, v9

    move v5, v7

    move v6, v7

    invoke-virtual/range {v0 .. v6}, Lcbn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Laea;ZZ)V

    invoke-virtual {v0, v8}, Lcbn;->a(I)V

    invoke-virtual {v0, v0}, Lcbn;->onClick(Landroid/view/View;)V

    iget-object v0, p0, Lame;->d:Lt;

    instance-of v0, v0, Labj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lame;->d:Lt;

    check-cast v0, Labj;

    invoke-virtual {v0}, Labj;->e()V

    goto :goto_0

    .line 490
    :cond_4
    iput-object p2, p0, Lame;->r:Lamj;

    .line 493
    iget-object v0, p0, Lame;->e:Lav;

    const/16 v1, 0x400

    invoke-virtual {v0, v1, v9, p0}, Lav;->a(ILandroid/os/Bundle;Law;)Ldg;

    .line 494
    iget-object v0, p0, Lame;->f:Lyj;

    invoke-virtual {v0}, Lyj;->u()Z

    move-result v0

    if-nez v0, :cond_6

    .line 495
    sget-boolean v0, Lame;->b:Z

    if-eqz v0, :cond_5

    .line 496
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Start public profile search with query: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 498
    :cond_5
    iget-object v0, p0, Lame;->f:Lyj;

    invoke-static {v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->k(Lyj;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lame;->l:I

    .line 502
    :cond_6
    iget-object v0, p0, Lame;->m:Lcvi;

    if-eqz v0, :cond_7

    .line 503
    new-instance v0, Lbsj;

    iget-object v1, p0, Lame;->m:Lcvi;

    iget-object v2, p0, Lame;->f:Lyj;

    invoke-direct {v0, v1, v2, p0, p1}, Lbsj;-><init>(Lcvi;Lyj;Lbsl;Ljava/lang/String;)V

    iput-object v0, p0, Lame;->o:Lbsj;

    .line 504
    iget-object v0, p0, Lame;->o:Lbsj;

    invoke-virtual {v0}, Lbsj;->a()V

    .line 507
    :cond_7
    iget-object v0, p0, Lame;->p:Lcvz;

    if-eqz v0, :cond_8

    .line 508
    iget-object v0, p0, Lame;->k:Laml;

    iget-object v1, p0, Lame;->p:Lcvz;

    iget-object v2, p0, Lame;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Laml;->a(Lcvz;Ljava/lang/String;)V

    .line 512
    :cond_8
    iget-object v0, p0, Lame;->i:Ljava/lang/String;

    invoke-static {v0}, Laea;->a(Ljava/lang/String;)I

    move-result v0

    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v1

    if-eqz v1, :cond_9

    move v0, v7

    :cond_9
    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    invoke-virtual {p0, v3, v9}, Lame;->a(ILandroid/database/Cursor;)V

    invoke-virtual {p0, v5, v9}, Lame;->a(ILandroid/database/Cursor;)V

    const/4 v0, 0x4

    invoke-virtual {p0, v0, v9}, Lame;->a(ILandroid/database/Cursor;)V

    goto/16 :goto_0

    :pswitch_1
    iget v0, p0, Lame;->g:I

    invoke-static {v0}, Lf;->d(I)Z

    move-result v0

    if-nez v0, :cond_a

    invoke-direct {p0}, Lame;->n()V

    :cond_a
    iget v0, p0, Lame;->g:I

    invoke-static {v0}, Lf;->c(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lame;->f:Lyj;

    invoke-virtual {v0}, Lyj;->u()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lbak;

    new-array v1, v4, [Ljava/lang/String;

    const-string v2, "_id"

    aput-object v2, v1, v7

    invoke-direct {v0, v1}, Lbak;-><init>([Ljava/lang/String;)V

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-virtual {v0, v1}, Lbak;->a([Ljava/lang/Object;)V

    invoke-virtual {p0, v3, v0}, Lame;->a(ILandroid/database/Cursor;)V

    goto/16 :goto_0

    :pswitch_2
    iget v0, p0, Lame;->g:I

    invoke-static {v0}, Lf;->d(I)Z

    move-result v0

    if-nez v0, :cond_b

    invoke-direct {p0}, Lame;->n()V

    :cond_b
    iget v0, p0, Lame;->g:I

    invoke-static {v0}, Lf;->c(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lame;->f:Lyj;

    invoke-virtual {v0}, Lyj;->u()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lbak;

    new-array v1, v4, [Ljava/lang/String;

    const-string v2, "_id"

    aput-object v2, v1, v7

    invoke-direct {v0, v1}, Lbak;-><init>([Ljava/lang/String;)V

    new-array v1, v4, [Ljava/lang/Object;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-virtual {v0, v1}, Lbak;->a([Ljava/lang/Object;)V

    invoke-virtual {p0, v5, v0}, Lame;->a(ILandroid/database/Cursor;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Ljava/lang/String;Lbdn;)V
    .locals 0

    .prologue
    .line 1043
    return-void
.end method

.method public f()I
    .locals 1

    .prologue
    .line 689
    const/16 v0, 0x9

    return v0
.end method

.method public f_()V
    .locals 0

    .prologue
    .line 1025
    invoke-virtual {p0}, Lame;->notifyDataSetChanged()V

    .line 1026
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 282
    new-instance v0, Lcvi;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0, p0}, Lcvi;-><init>(Landroid/content/Context;Lcfv;Lcfw;)V

    iput-object v0, p0, Lame;->m:Lcvi;

    .line 284
    iget-object v0, p0, Lame;->m:Lcvi;

    invoke-virtual {v0}, Lcvi;->a()V

    .line 286
    iget-object v0, p0, Lame;->a:Lbor;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbor;)V

    .line 287
    invoke-static {}, Lbzh;->b()Lbzh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbzh;->a(Lbzl;)V

    .line 290
    invoke-static {}, Lbsz;->a()Lbsz;

    move-result-object v0

    invoke-virtual {v0}, Lbsz;->b()V

    .line 291
    return-void
.end method

.method public getFilter()Landroid/widget/Filter;
    .locals 1

    .prologue
    .line 382
    new-instance v0, Lamh;

    invoke-direct {v0, p0}, Lamh;-><init>(Lame;)V

    return-object v0
.end method

.method public h()V
    .locals 2

    .prologue
    .line 296
    iget-object v0, p0, Lame;->m:Lcvi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lame;->m:Lcvi;

    invoke-virtual {v0}, Lcvi;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lame;->m:Lcvi;

    .line 297
    invoke-virtual {v0}, Lcvi;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 298
    const-string v0, "Babel"

    const-string v1, "Reconnecting people client for PeopleSearchListAdapter."

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    iget-object v0, p0, Lame;->m:Lcvi;

    invoke-virtual {v0}, Lcvi;->a()V

    .line 301
    :cond_0
    return-void
.end method

.method public i()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 307
    iget-object v0, p0, Lame;->p:Lcvz;

    if-eqz v0, :cond_0

    .line 308
    iget-object v0, p0, Lame;->p:Lcvz;

    invoke-virtual {v0}, Lcvz;->d()V

    .line 309
    iput-object v1, p0, Lame;->p:Lcvz;

    .line 312
    :cond_0
    iget-object v0, p0, Lame;->n:Lcvo;

    if-eqz v0, :cond_1

    .line 313
    iget-object v0, p0, Lame;->n:Lcvo;

    invoke-virtual {v0}, Lcvo;->b()V

    .line 314
    iput-object v1, p0, Lame;->n:Lcvo;

    .line 317
    :cond_1
    iget-object v0, p0, Lame;->m:Lcvi;

    if-eqz v0, :cond_4

    .line 318
    iget-object v0, p0, Lame;->m:Lcvi;

    invoke-virtual {v0}, Lcvi;->d()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lame;->m:Lcvi;

    invoke-virtual {v0}, Lcvi;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 319
    :cond_2
    iget-object v0, p0, Lame;->m:Lcvi;

    invoke-virtual {v0}, Lcvi;->b()V

    .line 321
    :cond_3
    iput-object v1, p0, Lame;->m:Lcvi;

    .line 324
    :cond_4
    iget-object v0, p0, Lame;->o:Lbsj;

    if-eqz v0, :cond_5

    .line 325
    iget-object v0, p0, Lame;->o:Lbsj;

    invoke-virtual {v0}, Lbsj;->b()V

    .line 328
    :cond_5
    iget-object v0, p0, Lame;->a:Lbor;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lbor;)V

    .line 329
    invoke-static {}, Lbzh;->b()Lbzh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbzh;->b(Lbzl;)V

    .line 330
    return-void
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 684
    iget-object v0, p0, Lame;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public j()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 333
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lame;->a(ILandroid/database/Cursor;)V

    .line 334
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v1}, Lame;->a(ILandroid/database/Cursor;)V

    .line 335
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v1}, Lame;->a(ILandroid/database/Cursor;)V

    .line 336
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v1}, Lame;->a(ILandroid/database/Cursor;)V

    .line 337
    const/4 v0, 0x7

    invoke-virtual {p0, v0, v1}, Lame;->a(ILandroid/database/Cursor;)V

    .line 338
    const/16 v0, 0x8

    invoke-virtual {p0, v0, v1}, Lame;->a(ILandroid/database/Cursor;)V

    .line 339
    const/4 v0, 0x5

    invoke-virtual {p0, v0, v1}, Lame;->a(ILandroid/database/Cursor;)V

    .line 340
    const/4 v0, 0x6

    invoke-virtual {p0, v0, v1}, Lame;->a(ILandroid/database/Cursor;)V

    .line 341
    const/4 v0, 0x4

    invoke-virtual {p0, v0, v1}, Lame;->a(ILandroid/database/Cursor;)V

    .line 342
    return-void
.end method

.method public k()V
    .locals 2

    .prologue
    .line 1020
    iget-object v0, p0, Lame;->h:Lamk;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lamk;->a(Lxs;)V

    .line 1021
    return-void
.end method

.method public notifyDataSetChanged()V
    .locals 2

    .prologue
    .line 1031
    const-string v0, "Babel"

    const-string v1, "PeopleSearchListadapter. notifyDataSetChanged"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1032
    invoke-super {p0}, Lpi;->notifyDataSetChanged()V

    .line 1033
    return-void
.end method

.method public notifyDataSetInvalidated()V
    .locals 2

    .prologue
    .line 1037
    const-string v0, "Babel"

    const-string v1, "PeopleSearchListadapter. notifyDataSetInvalidated"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1038
    invoke-super {p0}, Lpi;->notifyDataSetInvalidated()V

    .line 1039
    return-void
.end method

.method public onConnected(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 349
    const-string v0, "Babel"

    const-string v1, "People client connected for people search."

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    iget-object v0, p0, Lame;->m:Lcvi;

    if-nez v0, :cond_0

    .line 353
    const-string v0, "Babel"

    const-string v1, "People client connnected but PeopleSearchListAdapter is destroyed"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    :goto_0
    return-void

    .line 357
    :cond_0
    new-instance v0, Lcvo;

    iget-object v1, p0, Lame;->m:Lcvi;

    iget-object v2, p0, Lame;->f:Lyj;

    invoke-direct {v0, v1, v2, p0}, Lcvo;-><init>(Lcvi;Lyj;Lbsb;)V

    iput-object v0, p0, Lame;->n:Lcvo;

    .line 358
    iget-object v0, p0, Lame;->n:Lcvo;

    invoke-virtual {v0}, Lcvo;->a()V

    goto :goto_0
.end method

.method public onConnectionFailed(Lcft;)V
    .locals 3

    .prologue
    .line 374
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "People client connection failure for people search: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Ldg;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 551
    packed-switch p1, :pswitch_data_0

    move-object v0, v7

    .line 562
    :goto_0
    return-object v0

    .line 553
    :pswitch_0
    new-instance v0, Lbaj;

    invoke-virtual {p0}, Lame;->a()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lame;->f:Lyj;

    iget-object v3, p0, Lame;->f:Lyj;

    iget-object v4, p0, Lame;->i:Ljava/lang/String;

    .line 555
    invoke-static {v3, v4}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Lyj;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Laml;->a:[Ljava/lang/String;

    const-string v5, "chat_id != ? "

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v8, 0x0

    iget-object v9, p0, Lame;->f:Lyj;

    .line 558
    invoke-virtual {v9}, Lyj;->c()Lbdk;

    move-result-object v9

    iget-object v9, v9, Lbdk;->b:Ljava/lang/String;

    aput-object v9, v6, v8

    invoke-direct/range {v0 .. v7}, Lbaj;-><init>(Landroid/content/Context;Lyj;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 551
    :pswitch_data_0
    .packed-switch 0x400
        :pswitch_0
    .end packed-switch
.end method

.method public onDisconnected()V
    .locals 2

    .prologue
    .line 366
    const-string v0, "Babel"

    const-string v1, "People client disconnected for people search."

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    return-void
.end method

.method public synthetic onLoadFinished(Ldg;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 78
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p1}, Ldg;->l()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lame;->k:Laml;

    invoke-virtual {v0, p2}, Laml;->a(Landroid/database/Cursor;)Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0, v0}, Lame;->a(Ljava/util/Set;)V

    iget-object v0, p0, Lame;->r:Lamj;

    invoke-virtual {v0}, Lamj;->countDown()V

    invoke-direct {p0}, Lame;->m()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x400
        :pswitch_0
    .end packed-switch
.end method

.method public onLoaderReset(Ldg;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 583
    return-void
.end method
