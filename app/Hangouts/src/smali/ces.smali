.class final Lces;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lt;

.field final synthetic b:Lcer;


# direct methods
.method constructor <init>(Lcer;Lt;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lces;->b:Lcer;

    iput-object p2, p0, Lces;->a:Lt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 53
    iget-object v0, p0, Lces;->b:Lcer;

    invoke-static {v0}, Lcer;->a(Lcer;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 54
    const-string v0, "Babel"

    invoke-static {v0, v2}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    const-string v0, "Babel"

    const-string v1, "VideoAttachmentHandler could not load video"

    invoke-static {v0, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    :cond_0
    iget-object v0, p0, Lces;->b:Lcer;

    invoke-static {v0}, Lcer;->b(Lcer;)V

    .line 61
    :cond_1
    iget-object v0, p0, Lces;->b:Lcer;

    invoke-static {v0}, Lcer;->a(Lcer;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 62
    const-string v0, "Babel"

    invoke-static {v0, v2}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 63
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "VideoAttachmentHandler loaded urlString: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lces;->b:Lcer;

    .line 64
    invoke-static {v2}, Lcer;->a(Lcer;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 63
    invoke-static {v0, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    :cond_2
    iget-object v0, p0, Lces;->a:Lt;

    iget-object v1, p0, Lces;->b:Lcer;

    invoke-static {v1}, Lcer;->a(Lcer;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "video/mp4"

    invoke-static {v1, v2}, Lbbl;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lt;->startActivity(Landroid/content/Intent;)V

    .line 73
    :cond_3
    return-void
.end method
