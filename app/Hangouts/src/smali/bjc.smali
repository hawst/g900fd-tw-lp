.class public final Lbjc;
.super Lbis;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final b:I

.field public final f:Ljava/lang/String;

.field public final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbdk;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbjv;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbcj;",
            ">;"
        }
    .end annotation
.end field

.field public final j:Ljava/lang/String;

.field public final k:I

.field public final l:I

.field public final m:[I

.field public final n:Lbdk;

.field public final o:Lbjv;

.field public final p:Z

.field public final q:I

.field public final r:I

.field public final s:J

.field public t:J

.field public final u:[B

.field public final v:I

.field public final w:Ljava/lang/Boolean;

.field public final x:Z

.field public final y:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbcm;",
            ">;"
        }
    .end annotation
.end field

.field public final z:Lbcm;


# direct methods
.method public constructor <init>(Ldqa;)V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const-wide/16 v9, 0x0

    const/4 v8, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 322
    iget-object v0, p1, Ldqa;->b:Ldqf;

    iget-object v0, v0, Ldqf;->b:Ljava/lang/String;

    const-wide/16 v4, -0x1

    invoke-direct {p0, v0, v1, v4, v5}, Lbis;-><init>(Ljava/lang/String;Lbdk;J)V

    .line 325
    :try_start_0
    const-string v0, "MD5"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 328
    :goto_0
    if-eqz v0, :cond_0

    .line 329
    invoke-static {p1}, Ldqa;->toByteArray(Lepr;)[B

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/security/MessageDigest;->update([B)V

    .line 330
    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    iput-object v0, p0, Lbjc;->u:[B

    .line 334
    :goto_1
    iget-object v0, p1, Ldqa;->d:Ljava/lang/Integer;

    invoke-static {v0, v3}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    iput v0, p0, Lbjc;->b:I

    .line 335
    iget-object v0, p1, Ldqa;->e:Ljava/lang/String;

    iput-object v0, p0, Lbjc;->f:Ljava/lang/String;

    .line 336
    iget-object v2, p1, Ldqa;->h:[Ldxt;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    array-length v5, v2

    move v0, v3

    :goto_2
    if-ge v0, v5, :cond_1

    aget-object v6, v2, v0

    new-instance v7, Lbjv;

    invoke-direct {v7, v6}, Lbjv;-><init>(Ldxt;)V

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    .line 332
    :cond_0
    iput-object v1, p0, Lbjc;->u:[B

    goto :goto_1

    .line 336
    :cond_1
    iput-object v4, p0, Lbjc;->h:Ljava/util/List;

    .line 338
    iget-object v0, p1, Ldqa;->m:[Ldqh;

    .line 339
    invoke-static {v0}, Lbcj;->a([Ldqh;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbjc;->i:Ljava/util/List;

    .line 341
    iget-object v0, p1, Ldqa;->l:[Ldui;

    iget-object v2, p1, Ldqa;->m:[Ldqh;

    invoke-static {v0, v2}, Lbdk;->a([Ldui;[Ldqh;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbjc;->g:Ljava/util/List;

    .line 343
    iget-object v0, p1, Ldqa;->i:Ljava/lang/Boolean;

    invoke-static {v0, v3}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    iput-boolean v0, p0, Lbjc;->p:Z

    .line 344
    iget-object v0, p1, Ldqa;->j:Ljava/lang/Integer;

    invoke-static {v0, v3}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    iput v0, p0, Lbjc;->q:I

    .line 345
    iget-object v0, p1, Ldqa;->k:Ljava/lang/Integer;

    invoke-static {v0, v8}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    iput v0, p0, Lbjc;->r:I

    .line 348
    iget-object v0, p1, Ldqa;->g:Ldqb;

    if-eqz v0, :cond_b

    .line 349
    iget-object v4, p1, Ldqa;->g:Ldqb;

    .line 350
    iget-object v0, v4, Ldqb;->c:Ljava/lang/Long;

    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v5

    cmp-long v0, v5, v9

    if-eqz v0, :cond_2

    iget-object v0, v4, Ldqb;->c:Ljava/lang/Long;

    .line 352
    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v5

    .line 351
    invoke-static {v5, v6}, Lyt;->a(J)Ljava/lang/String;

    move-result-object v0

    :goto_3
    iput-object v0, p0, Lbjc;->j:Ljava/lang/String;

    .line 353
    iget-object v0, v4, Ldqb;->d:Ljava/lang/Integer;

    invoke-static {v0, v3}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    iput v0, p0, Lbjc;->k:I

    .line 354
    iget-object v0, v4, Ldqb;->b:Ljava/lang/Integer;

    invoke-static {v0, v3}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    iput v0, p0, Lbjc;->l:I

    .line 355
    iget-object v0, v4, Ldqb;->e:[I

    array-length v0, v0

    if-lez v0, :cond_3

    iget-object v0, v4, Ldqb;->e:[I

    :goto_4
    iput-object v0, p0, Lbjc;->m:[I

    .line 356
    new-instance v0, Lbdk;

    iget-object v2, v4, Ldqb;->f:Ldui;

    invoke-direct {v0, v2, v1}, Lbdk;-><init>(Ldui;Ljava/lang/String;)V

    iput-object v0, p0, Lbjc;->n:Lbdk;

    .line 357
    iget-object v0, v4, Ldqb;->h:Ldxt;

    if-eqz v0, :cond_4

    new-instance v0, Lbjv;

    iget-object v2, v4, Ldqb;->h:Ldxt;

    invoke-direct {v0, v2}, Lbjv;-><init>(Ldxt;)V

    :goto_5
    iput-object v0, p0, Lbjc;->o:Lbjv;

    .line 359
    iget-object v0, v4, Ldqb;->i:Ljava/lang/Long;

    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v5

    iput-wide v5, p0, Lbjc;->s:J

    .line 361
    iget-object v0, v4, Ldqb;->k:Ljava/lang/Long;

    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v5

    iput-wide v5, p0, Lbjc;->t:J

    .line 362
    iget-object v0, v4, Ldqb;->l:Ljava/lang/Integer;

    invoke-static {v0, v3}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    iput v0, p0, Lbjc;->v:I

    .line 363
    iget-object v0, v4, Ldqb;->m:Ljava/lang/Boolean;

    invoke-static {v0, v3}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    :goto_6
    iput-object v0, p0, Lbjc;->w:Ljava/lang/Boolean;

    .line 365
    iget-object v0, p1, Ldqa;->n:Ljava/lang/Boolean;

    invoke-static {v0, v3}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    iput-boolean v0, p0, Lbjc;->x:Z

    .line 366
    iget-object v0, v4, Ldqb;->n:[Ldqc;

    array-length v0, v0

    if-ne v0, v8, :cond_6

    .line 369
    iput-object v1, p0, Lbjc;->y:Ljava/util/List;

    .line 370
    new-instance v0, Lbcm;

    iget-object v1, v4, Ldqb;->n:[Ldqc;

    aget-object v1, v1, v3

    iget-object v1, v1, Ldqc;->b:Ldqw;

    invoke-direct {v0, v1}, Lbcm;-><init>(Ldqw;)V

    iput-object v0, p0, Lbjc;->z:Lbcm;

    .line 410
    :goto_7
    return-void

    :cond_2
    move-object v0, v1

    .line 351
    goto :goto_3

    :cond_3
    move-object v0, v1

    .line 355
    goto :goto_4

    :cond_4
    move-object v0, v1

    .line 357
    goto :goto_5

    :cond_5
    move-object v0, v1

    .line 363
    goto :goto_6

    .line 372
    :cond_6
    iget-object v0, v4, Ldqb;->n:[Ldqc;

    array-length v0, v0

    if-le v0, v8, :cond_a

    .line 373
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, v4, Ldqb;->n:[Ldqc;

    array-length v2, v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lbjc;->y:Ljava/util/List;

    move v2, v3

    move-object v0, v1

    .line 376
    :goto_8
    iget-object v1, v4, Ldqb;->n:[Ldqc;

    array-length v1, v1

    if-ge v2, v1, :cond_9

    .line 377
    new-instance v1, Lbcm;

    iget-object v5, v4, Ldqb;->n:[Ldqc;

    aget-object v5, v5, v2

    iget-object v5, v5, Ldqc;->b:Ldqw;

    invoke-direct {v1, v5}, Lbcm;-><init>(Ldqw;)V

    .line 379
    iget-object v5, p0, Lbjc;->y:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 382
    if-eqz v0, :cond_7

    iget-object v5, v4, Ldqb;->n:[Ldqc;

    aget-object v5, v5, v2

    iget-object v5, v5, Ldqc;->c:Ljava/lang/Boolean;

    invoke-static {v5, v3}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v5

    if-eqz v5, :cond_8

    :cond_7
    move-object v0, v1

    .line 376
    :cond_8
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_8

    .line 387
    :cond_9
    iput-object v0, p0, Lbjc;->z:Lbcm;

    goto :goto_7

    .line 390
    :cond_a
    iput-object v1, p0, Lbjc;->y:Ljava/util/List;

    .line 391
    iput-object v1, p0, Lbjc;->z:Lbcm;

    goto :goto_7

    .line 395
    :cond_b
    iput-object v1, p0, Lbjc;->j:Ljava/lang/String;

    .line 397
    const/16 v0, 0x1e

    iput v0, p0, Lbjc;->k:I

    .line 398
    iput v11, p0, Lbjc;->l:I

    .line 399
    iput-object v1, p0, Lbjc;->m:[I

    .line 400
    iput-object v1, p0, Lbjc;->n:Lbdk;

    .line 401
    iput-object v1, p0, Lbjc;->o:Lbjv;

    .line 402
    iput-wide v9, p0, Lbjc;->s:J

    .line 403
    iput-wide v9, p0, Lbjc;->t:J

    .line 404
    iput v11, p0, Lbjc;->v:I

    .line 405
    iput-object v1, p0, Lbjc;->w:Ljava/lang/Boolean;

    .line 406
    iput-boolean v3, p0, Lbjc;->x:Z

    .line 407
    iput-object v1, p0, Lbjc;->y:Ljava/util/List;

    .line 408
    iput-object v1, p0, Lbjc;->z:Lbcm;

    goto :goto_7
.end method

.method public constructor <init>(Ldqa;B)V
    .locals 0

    .prologue
    .line 419
    invoke-direct {p0, p1}, Lbjc;-><init>(Ldqa;)V

    .line 420
    return-void
.end method


# virtual methods
.method public a(J)V
    .locals 0

    .prologue
    .line 455
    iput-wide p1, p0, Lbjc;->t:J

    .line 456
    return-void
.end method

.method public b()Z
    .locals 4

    .prologue
    .line 459
    iget-wide v0, p0, Lbjc;->t:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 465
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "C["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lbjc;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 466
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; clientGenerated="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbjc;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 467
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; notificationLevel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lbjc;->k:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 468
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 469
    return-object v0
.end method
