.class public final Lepd;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lepd;


# instance fields
.field public b:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4334
    const/4 v0, 0x0

    new-array v0, v0, [Lepd;

    sput-object v0, Lepd;->a:[Lepd;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4335
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 2

    .prologue
    .line 4351
    const/4 v0, 0x0

    .line 4352
    iget-object v1, p0, Lepd;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 4353
    const/4 v0, 0x1

    iget-object v1, p0, Lepd;->b:Ljava/lang/Boolean;

    .line 4354
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Lepl;->g(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 4356
    :cond_0
    iget-object v1, p0, Lepd;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4357
    iput v0, p0, Lepd;->cachedSize:I

    .line 4358
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 4331
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lepd;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lepd;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lepd;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lepd;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 4342
    iget-object v0, p0, Lepd;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 4343
    const/4 v0, 0x1

    iget-object v1, p0, Lepd;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 4345
    :cond_0
    iget-object v0, p0, Lepd;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 4347
    return-void
.end method
