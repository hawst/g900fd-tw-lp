.class public Lww;
.super Lwk;
.source "PG"


# instance fields
.field b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lwx;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lwk;-><init>()V

    .line 15
    const/4 v0, 0x0

    iput-object v0, p0, Lww;->b:Ljava/util/ArrayList;

    .line 46
    return-void
.end method


# virtual methods
.method public a(Lwx;)V
    .locals 2

    .prologue
    .line 517
    iget-object v0, p0, Lww;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 518
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lww;->b:Ljava/util/ArrayList;

    .line 520
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1

    .line 523
    new-instance v1, Lwo;

    invoke-direct {v1, p1, p0}, Lwo;-><init>(Lwx;Lww;)V

    invoke-virtual {p0}, Lww;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    move-object v0, v1

    .line 531
    :goto_0
    new-instance v1, Landroid/util/Pair;

    invoke-direct {v1, p1, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 533
    iget-object v0, p0, Lww;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 534
    return-void

    .line 525
    :cond_1
    new-instance v1, Lxl;

    invoke-direct {v1, p1, p0}, Lxl;-><init>(Lwx;Lww;)V

    .line 527
    invoke-virtual {p0}, Lww;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxj;

    .line 528
    invoke-virtual {v0, v1}, Lxj;->a(Lxl;)V

    move-object v0, v1

    .line 529
    goto :goto_0
.end method

.method public varargs a([I)V
    .locals 2

    .prologue
    .line 203
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 204
    invoke-virtual {p0}, Lww;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p1}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    .line 209
    :goto_0
    return-void

    .line 206
    :cond_0
    invoke-virtual {p0}, Lww;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxj;

    .line 207
    invoke-virtual {v0, p1}, Lxj;->a([I)V

    goto :goto_0
.end method

.method public varargs a([Lws;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 610
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_1

    .line 611
    array-length v2, p1

    new-array v3, v2, [Landroid/animation/PropertyValuesHolder;

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v0, p1, v1

    invoke-virtual {v0}, Lws;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/PropertyValuesHolder;

    aput-object v0, v3, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lww;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v3}, Landroid/animation/ValueAnimator;->setValues([Landroid/animation/PropertyValuesHolder;)V

    .line 624
    :goto_1
    return-void

    .line 613
    :cond_1
    array-length v2, p1

    .line 614
    new-array v3, v2, [Lxh;

    move v1, v0

    .line 617
    :goto_2
    if-ge v1, v2, :cond_2

    .line 618
    aget-object v0, p1, v1

    .line 619
    invoke-virtual {v0}, Lws;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxh;

    aput-object v0, v3, v1

    .line 617
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 621
    :cond_2
    invoke-virtual {p0}, Lww;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxj;

    .line 622
    invoke-virtual {v0, v3}, Lxj;->a([Lxh;)V

    goto :goto_1
.end method

.method public synthetic b(J)Lwk;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0, p1, p2}, Lww;->d(J)Lww;

    move-result-object v0

    return-object v0
.end method

.method public d(J)Lww;
    .locals 0

    .prologue
    .line 285
    invoke-super {p0, p1, p2}, Lwk;->b(J)Lwk;

    .line 286
    return-object p0
.end method

.method public h()J
    .locals 2

    .prologue
    .line 321
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 322
    invoke-virtual {p0}, Lww;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getCurrentPlayTime()J

    move-result-wide v0

    .line 325
    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p0}, Lww;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxj;

    invoke-virtual {v0}, Lxj;->i()J

    move-result-wide v0

    goto :goto_0
.end method

.method public i()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 547
    iget-object v0, p0, Lww;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 565
    :cond_0
    :goto_0
    return-void

    .line 550
    :cond_1
    iget-object v0, p0, Lww;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 551
    iput-object v4, p0, Lww;->b:Ljava/util/ArrayList;

    .line 553
    iget-object v0, p0, Lww;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 554
    iget-object v0, p0, Lww;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 555
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v1, v3, :cond_2

    .line 556
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {p0}, Lww;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    check-cast v1, Lwo;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->removeUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    goto :goto_1

    .line 559
    :cond_2
    invoke-virtual {p0}, Lww;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lxj;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lxl;

    invoke-virtual {v1, v0}, Lxj;->b(Lxl;)V

    goto :goto_1

    .line 563
    :cond_3
    iput-object v4, p0, Lww;->a:Ljava/util/ArrayList;

    goto :goto_0
.end method
