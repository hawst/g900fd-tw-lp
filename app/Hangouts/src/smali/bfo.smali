.class public Lbfo;
.super Lbed;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:I


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 2315
    invoke-direct {p0, p1}, Lbed;-><init>(Ljava/lang/String;)V

    .line 2316
    iput p2, p0, Lbfo;->a:I

    .line 2317
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 2

    .prologue
    .line 2321
    sget-boolean v0, Lbea;->d:Z

    if-eqz v0, :cond_0

    .line 2322
    const-string v0, "Babel_RequestWriter"

    const-string v1, "typingRequest build protobuf"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2324
    :cond_0
    new-instance v0, Ldwp;

    invoke-direct {v0}, Ldwp;-><init>()V

    .line 2325
    invoke-virtual {p0, p1, p2}, Lbfo;->c(Ljava/lang/String;I)Ldvm;

    move-result-object v1

    iput-object v1, v0, Ldwp;->b:Ldvm;

    .line 2328
    iget-object v1, p0, Lbfo;->c:Ljava/lang/String;

    invoke-static {v1}, Lbea;->d(Ljava/lang/String;)Ldqf;

    move-result-object v1

    iput-object v1, v0, Ldwp;->c:Ldqf;

    .line 2329
    iget v1, p0, Lbfo;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Ldwp;->d:Ljava/lang/Integer;

    .line 2330
    return-object v0
.end method

.method public a(Lbea;)Z
    .locals 2

    .prologue
    .line 2351
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2352
    check-cast p1, Lbfo;

    .line 2353
    iget-object v0, p1, Lbfo;->c:Ljava/lang/String;

    iget-object v1, p0, Lbfo;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2335
    const-string v0, "conversations/settyping"

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2346
    const-string v0, "background_queue"

    return-object v0
.end method

.method public d()J
    .locals 3

    .prologue
    .line 2341
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0xa

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    return-wide v0
.end method
