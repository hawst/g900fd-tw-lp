.class public final Ldtd;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldtd;


# instance fields
.field public b:Ldtc;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21465
    const/4 v0, 0x0

    new-array v0, v0, [Ldtd;

    sput-object v0, Ldtd;->a:[Ldtd;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21466
    invoke-direct {p0}, Lepn;-><init>()V

    .line 21469
    const/4 v0, 0x0

    iput-object v0, p0, Ldtd;->b:Ldtc;

    .line 21466
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 2

    .prologue
    .line 21483
    const/4 v0, 0x0

    .line 21484
    iget-object v1, p0, Ldtd;->b:Ldtc;

    if-eqz v1, :cond_0

    .line 21485
    const/4 v0, 0x1

    iget-object v1, p0, Ldtd;->b:Ldtc;

    .line 21486
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 21488
    :cond_0
    iget-object v1, p0, Ldtd;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21489
    iput v0, p0, Ldtd;->cachedSize:I

    .line 21490
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 21462
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldtd;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldtd;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldtd;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldtd;->b:Ldtc;

    if-nez v0, :cond_2

    new-instance v0, Ldtc;

    invoke-direct {v0}, Ldtc;-><init>()V

    iput-object v0, p0, Ldtd;->b:Ldtc;

    :cond_2
    iget-object v0, p0, Ldtd;->b:Ldtc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 21474
    iget-object v0, p0, Ldtd;->b:Ldtc;

    if-eqz v0, :cond_0

    .line 21475
    const/4 v0, 0x1

    iget-object v1, p0, Ldtd;->b:Ldtc;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 21477
    :cond_0
    iget-object v0, p0, Ldtd;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 21479
    return-void
.end method
