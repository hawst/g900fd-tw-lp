.class public Latg;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field public final a:Landroid/view/ViewGroup;

.field public final b:Landroid/widget/ImageButton;

.field public final c:Landroid/widget/TextView;

.field public final d:Landroid/widget/ImageView;

.field final synthetic e:Late;

.field private final f:Laul;

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;


# direct methods
.method public constructor <init>(Late;Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 148
    iput-object p1, p0, Latg;->e:Late;

    .line 149
    invoke-direct {p0, p2, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 141
    iput-boolean v0, p0, Latg;->g:Z

    .line 142
    iput-boolean v0, p0, Latg;->h:Z

    .line 143
    iput-boolean v0, p0, Latg;->i:Z

    .line 150
    invoke-virtual {p0}, Latg;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v2, Lf;->fv:I

    invoke-static {v0, v2, p0}, Latg;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 151
    sget v0, Lg;->df:I

    invoke-virtual {p0, v0}, Latg;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Latg;->a:Landroid/view/ViewGroup;

    .line 153
    sget v0, Lg;->de:I

    invoke-virtual {p0, v0}, Latg;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Latg;->b:Landroid/widget/ImageButton;

    .line 155
    iget-object v0, p0, Latg;->b:Landroid/widget/ImageButton;

    new-instance v2, Lath;

    invoke-direct {v2, p0, p1}, Lath;-><init>(Latg;Late;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 162
    iget-object v0, p0, Latg;->b:Landroid/widget/ImageButton;

    new-instance v2, Lati;

    invoke-direct {v2, p0, p1}, Lati;-><init>(Latg;Late;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 170
    sget v0, Lg;->di:I

    invoke-virtual {p0, v0}, Latg;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Latg;->c:Landroid/widget/TextView;

    .line 171
    sget v0, Lg;->dj:I

    invoke-virtual {p0, v0}, Latg;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Latg;->d:Landroid/widget/ImageView;

    .line 174
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v0, v2, :cond_0

    new-instance v0, Laul;

    iget-object v1, p0, Latg;->a:Landroid/view/ViewGroup;

    invoke-direct {v0, p2, v1}, Laul;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;)V

    :goto_0
    iput-object v0, p0, Latg;->f:Laul;

    .line 177
    invoke-virtual {p0}, Latg;->b()V

    .line 178
    return-void

    :cond_0
    move-object v0, v1

    .line 174
    goto :goto_0
.end method

.method static synthetic a(Latg;)V
    .locals 3

    .prologue
    .line 133
    iget-object v0, p0, Latg;->e:Late;

    iget-object v0, v0, Late;->a:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    if-eqz v0, :cond_0

    iget-object v0, p0, Latg;->e:Late;

    iget-object v0, v0, Late;->e:Latd;

    invoke-virtual {v0}, Latd;->h()Laqv;

    move-result-object v0

    iget-object v1, p0, Latg;->e:Late;

    iget-object v1, v1, Late;->a:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isSelfEndpoint()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Laqv;->e(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Latg;->e:Late;

    iget-object v1, v1, Late;->a:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    invoke-virtual {v0, v1}, Laqv;->a(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V

    iget-object v1, p0, Latg;->e:Late;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Late;->a(Z)V

    new-instance v1, Latj;

    invoke-direct {v1, p0}, Latj;-><init>(Latg;)V

    invoke-virtual {v0, v1}, Laqv;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method static synthetic b(Latg;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 133
    iget-object v0, p0, Latg;->e:Late;

    iget-object v0, v0, Late;->f:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    iget-object v1, p0, Latg;->e:Late;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->c(Late;)Z

    move-result v0

    iget-boolean v1, p0, Latg;->g:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Latg;->h:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Latg;->i:Z

    if-eqz v1, :cond_1

    :cond_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Latg;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Latg;->b:Landroid/widget/ImageButton;

    sget v2, Lcom/google/android/apps/hangouts/R$drawable;->aw:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    :goto_0
    iget-boolean v1, p0, Latg;->i:Z

    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    iget-object v0, p0, Latg;->b:Landroid/widget/ImageButton;

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->bI:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    :goto_1
    return-void

    :cond_1
    iget-object v1, p0, Latg;->c:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Latg;->b:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto :goto_0

    :cond_2
    iget-boolean v1, p0, Latg;->h:Z

    if-eqz v1, :cond_3

    if-eqz v0, :cond_3

    iget-object v0, p0, Latg;->b:Landroid/widget/ImageButton;

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->ax:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Latg;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_1
.end method

.method private d()V
    .locals 2

    .prologue
    .line 284
    iget-object v0, p0, Latg;->b:Landroid/widget/ImageButton;

    iget-object v1, p0, Latg;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 285
    iget-object v1, p0, Latg;->c:Landroid/widget/TextView;

    iget-object v0, p0, Latg;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Latg;->k:Ljava/lang/String;

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 286
    return-void

    .line 285
    :cond_0
    iget-object v0, p0, Latg;->j:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method protected a(I)I
    .locals 0

    .prologue
    .line 258
    return p1
.end method

.method protected a()V
    .locals 2

    .prologue
    .line 215
    iget-object v0, p0, Latg;->e:Late;

    iget-object v0, v0, Late;->f:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;

    iget-object v1, p0, Latg;->e:Late;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->b(Late;)V

    .line 216
    iget-object v0, p0, Latg;->e:Late;

    iget-object v0, v0, Late;->e:Latd;

    invoke-virtual {v0}, Latd;->h()Laqv;

    move-result-object v0

    invoke-virtual {v0}, Laqv;->i()V

    .line 217
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 270
    iput-object p1, p0, Latg;->j:Ljava/lang/String;

    .line 271
    invoke-direct {p0}, Latg;->d()V

    .line 272
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 289
    iput-boolean p1, p0, Latg;->g:Z

    .line 290
    return-void
.end method

.method public b()V
    .locals 8

    .prologue
    .line 250
    iget-object v0, p0, Latg;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 251
    iget-object v0, p0, Latg;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p0}, Latg;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v3, "window"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xe

    if-lt v4, v5, :cond_0

    invoke-virtual {v0, v3}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    :goto_0
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    sget v4, Lf;->df:I

    const/4 v5, 0x1

    invoke-virtual {v2, v4, v0, v5}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    invoke-virtual {v0}, Landroid/util/TypedValue;->getFloat()F

    move-result v0

    sget v4, Lf;->di:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    sget v5, Lf;->dg:I

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    sget v6, Lf;->dM:I

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iget v6, v3, Landroid/graphics/Point;->y:I

    shl-int/lit8 v7, v4, 0x1

    sub-int/2addr v6, v7

    int-to-float v6, v6

    mul-float/2addr v0, v6

    float-to-int v0, v0

    iget v3, v3, Landroid/graphics/Point;->x:I

    shl-int/lit8 v4, v4, 0x1

    sub-int/2addr v3, v4

    div-int v2, v3, v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 252
    iget v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {p0, v0}, Latg;->a(I)I

    move-result v0

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 253
    iget-object v0, p0, Latg;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 254
    return-void

    .line 251
    :cond_0
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v4

    iput v4, v3, Landroid/graphics/Point;->x:I

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    iput v0, v3, Landroid/graphics/Point;->y:I

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 279
    iput-object p1, p0, Latg;->k:Ljava/lang/String;

    .line 280
    invoke-direct {p0}, Latg;->d()V

    .line 281
    return-void
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 293
    iput-boolean p1, p0, Latg;->h:Z

    .line 294
    iget-object v1, p0, Latg;->e:Late;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Late;->c(Z)V

    .line 295
    return-void

    .line 294
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Latg;->j:Ljava/lang/String;

    return-object v0
.end method

.method public c(Z)V
    .locals 0

    .prologue
    .line 298
    iput-boolean p1, p0, Latg;->i:Z

    .line 299
    return-void
.end method

.method public onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 263
    iget-object v0, p0, Latg;->e:Late;

    iget-object v1, v0, Late;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 264
    :try_start_0
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 265
    iget-object v0, p0, Latg;->e:Late;

    invoke-static {v0}, Late;->a(Late;)V

    .line 266
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
