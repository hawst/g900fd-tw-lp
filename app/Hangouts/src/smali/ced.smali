.class public Lced;
.super Lceb;
.source "PG"

# interfaces
.implements Landroid/widget/AbsListView$SelectionBoundsAdjuster;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Lt;ZI)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3, p4}, Lceb;-><init>(Landroid/content/Context;Lt;ZI)V

    .line 25
    return-void
.end method


# virtual methods
.method public adjustListItemSelectionBounds(Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 32
    iget-boolean v0, p0, Lced;->b:Z

    if-eqz v0, :cond_0

    .line 33
    iget v0, p1, Landroid/graphics/Rect;->top:I

    iget v1, p0, Lced;->c:I

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 35
    :cond_0
    return-void
.end method
