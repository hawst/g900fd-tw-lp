.class final Lcjo;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lcjn;


# direct methods
.method constructor <init>(Lcjn;)V
    .locals 0

    iput-object p1, p0, Lcjo;->a:Lcjn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    iget-object v0, p0, Lcjo;->a:Lcjn;

    invoke-static {p2}, Lcjq;->a(Landroid/os/IBinder;)Lcjp;

    move-result-object v1

    invoke-static {v0, v1}, Lcjn;->a(Lcjn;Lcjp;)Lcjp;

    iget-object v0, p0, Lcjo;->a:Lcjn;

    invoke-static {v0}, Lcjn;->a(Lcjn;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 4

    iget-object v0, p0, Lcjo;->a:Lcjn;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcjn;->a(Lcjn;Lcjp;)Lcjp;

    iget-object v0, p0, Lcjo;->a:Lcjn;

    invoke-static {v0}, Lcjn;->c(Lcjn;)Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcjn;->a()Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p0, Lcjo;->a:Lcjn;

    invoke-static {v2}, Lcjn;->b(Lcjn;)Landroid/content/ServiceConnection;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    return-void
.end method
