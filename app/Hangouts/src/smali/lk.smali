.class final Llk;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Lfr;

.field final synthetic b:Lli;

.field private c:Landroid/view/Menu;

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:I

.field private l:I

.field private m:Ljava/lang/CharSequence;

.field private n:Ljava/lang/CharSequence;

.field private o:I

.field private p:C

.field private q:C

.field private r:I

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:I

.field private w:I

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lli;Landroid/view/Menu;)V
    .locals 0

    .prologue
    .line 312
    iput-object p1, p0, Llk;->b:Lli;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 313
    iput-object p2, p0, Llk;->c:Landroid/view/Menu;

    .line 315
    invoke-virtual {p0}, Llk;->a()V

    .line 316
    return-void
.end method

.method private static a(Ljava/lang/String;)C
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 400
    if-nez p0, :cond_0

    .line 403
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/Class",
            "<*>;[",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 480
    :try_start_0
    iget-object v0, p0, Llk;->b:Lli;

    invoke-static {v0}, Lli;->a(Lli;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 481
    invoke-virtual {v0, p2}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    .line 482
    invoke-virtual {v0, p3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 484
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/view/MenuItem;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 408
    iget-boolean v0, p0, Llk;->s:Z

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    move-result-object v0

    iget-boolean v3, p0, Llk;->t:Z

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    move-result-object v0

    iget-boolean v3, p0, Llk;->u:Z

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    move-result-object v3

    iget v0, p0, Llk;->r:I

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setCheckable(Z)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v3, p0, Llk;->n:Ljava/lang/CharSequence;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    iget v3, p0, Llk;->o:I

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-char v3, p0, Llk;->p:C

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    move-result-object v0

    iget-char v3, p0, Llk;->q:C

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setNumericShortcut(C)Landroid/view/MenuItem;

    .line 417
    iget v0, p0, Llk;->v:I

    if-ltz v0, :cond_0

    .line 418
    iget v0, p0, Llk;->v:I

    invoke-static {p1, v0}, Lgh;->a(Landroid/view/MenuItem;I)V

    .line 421
    :cond_0
    iget-object v0, p0, Llk;->z:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 422
    iget-object v0, p0, Llk;->b:Lli;

    invoke-static {v0}, Lli;->a(Lli;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->isRestricted()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 423
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The android:onClick attribute cannot be used within a restricted context"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v0, v2

    .line 408
    goto :goto_0

    .line 426
    :cond_2
    new-instance v0, Llj;

    iget-object v3, p0, Llk;->b:Lli;

    invoke-static {v3}, Lli;->c(Lli;)Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, Llk;->z:Ljava/lang/String;

    invoke-direct {v0, v3, v4}, Llj;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 430
    :cond_3
    iget v0, p0, Llk;->r:I

    const/4 v3, 0x2

    if-lt v0, v3, :cond_4

    .line 432
    instance-of v0, p1, Lme;

    if-eqz v0, :cond_7

    move-object v0, p1

    .line 433
    check-cast v0, Lme;

    invoke-virtual {v0, v1}, Lme;->a(Z)V

    .line 440
    :cond_4
    :goto_1
    iget-object v0, p0, Llk;->x:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 441
    iget-object v0, p0, Llk;->x:Ljava/lang/String;

    invoke-static {}, Lli;->b()[Ljava/lang/Class;

    move-result-object v2

    iget-object v3, p0, Llk;->b:Lli;

    invoke-static {v3}, Lli;->d(Lli;)[Ljava/lang/Object;

    move-result-object v3

    invoke-direct {p0, v0, v2, v3}, Llk;->a(Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 443
    invoke-static {p1, v0}, Lgh;->a(Landroid/view/MenuItem;Landroid/view/View;)Landroid/view/MenuItem;

    .line 446
    :goto_2
    iget v0, p0, Llk;->w:I

    if-lez v0, :cond_5

    .line 447
    if-nez v1, :cond_5

    .line 448
    iget v0, p0, Llk;->w:I

    invoke-static {p1, v0}, Lgh;->b(Landroid/view/MenuItem;I)Landroid/view/MenuItem;

    .line 449
    :cond_5
    iget-object v0, p0, Llk;->a:Lfr;

    if-eqz v0, :cond_6

    .line 456
    iget-object v0, p0, Llk;->a:Lfr;

    invoke-static {p1, v0}, Lgh;->a(Landroid/view/MenuItem;Lfr;)Landroid/view/MenuItem;

    .line 458
    :cond_6
    return-void

    .line 434
    :cond_7
    instance-of v0, p1, Lmg;

    if-eqz v0, :cond_4

    move-object v0, p1

    .line 435
    check-cast v0, Lmg;

    invoke-virtual {v0}, Lmg;->b()V

    goto :goto_1

    :cond_8
    move v1, v2

    goto :goto_2
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 319
    iput v0, p0, Llk;->d:I

    .line 320
    iput v0, p0, Llk;->e:I

    .line 321
    iput v0, p0, Llk;->f:I

    .line 322
    iput v0, p0, Llk;->g:I

    .line 323
    iput-boolean v1, p0, Llk;->h:Z

    .line 324
    iput-boolean v1, p0, Llk;->i:Z

    .line 325
    return-void
.end method

.method public a(Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 331
    iget-object v0, p0, Llk;->b:Lli;

    invoke-static {v0}, Lli;->a(Lli;)Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lle;->z:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 333
    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Llk;->d:I

    .line 334
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Llk;->e:I

    .line 336
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Llk;->f:I

    .line 337
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Llk;->g:I

    .line 339
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Llk;->h:Z

    .line 340
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Llk;->i:Z

    .line 342
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 343
    return-void
.end method

.method public b()V
    .locals 5

    .prologue
    .line 461
    const/4 v0, 0x1

    iput-boolean v0, p0, Llk;->j:Z

    .line 462
    iget-object v0, p0, Llk;->c:Landroid/view/Menu;

    iget v1, p0, Llk;->d:I

    iget v2, p0, Llk;->k:I

    iget v3, p0, Llk;->l:I

    iget-object v4, p0, Llk;->m:Ljava/lang/CharSequence;

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-direct {p0, v0}, Llk;->a(Landroid/view/MenuItem;)V

    .line 463
    return-void
.end method

.method public b(Landroid/util/AttributeSet;)V
    .locals 7

    .prologue
    const/16 v6, 0xb

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 349
    iget-object v0, p0, Llk;->b:Lli;

    invoke-static {v0}, Lli;->a(Lli;)Landroid/content/Context;

    move-result-object v0

    sget-object v3, Lle;->A:[I

    invoke-virtual {v0, p1, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 352
    const/4 v0, 0x2

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Llk;->k:I

    .line 353
    const/4 v0, 0x5

    iget v4, p0, Llk;->e:I

    invoke-virtual {v3, v0, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 354
    const/4 v4, 0x6

    iget v5, p0, Llk;->f:I

    invoke-virtual {v3, v4, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v4

    .line 355
    const/high16 v5, -0x10000

    and-int/2addr v0, v5

    const v5, 0xffff

    and-int/2addr v4, v5

    or-int/2addr v0, v4

    iput v0, p0, Llk;->l:I

    .line 357
    const/4 v0, 0x7

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Llk;->m:Ljava/lang/CharSequence;

    .line 358
    const/16 v0, 0x8

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Llk;->n:Ljava/lang/CharSequence;

    .line 359
    invoke-virtual {v3, v2, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Llk;->o:I

    .line 360
    const/16 v0, 0x9

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Llk;->a(Ljava/lang/String;)C

    move-result v0

    iput-char v0, p0, Llk;->p:C

    .line 362
    const/16 v0, 0xa

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Llk;->a(Ljava/lang/String;)C

    move-result v0

    iput-char v0, p0, Llk;->q:C

    .line 364
    invoke-virtual {v3, v6}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 366
    invoke-virtual {v3, v6, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput v0, p0, Llk;->r:I

    .line 372
    :goto_1
    const/4 v0, 0x3

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Llk;->s:Z

    .line 373
    const/4 v0, 0x4

    iget-boolean v4, p0, Llk;->h:Z

    invoke-virtual {v3, v0, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Llk;->t:Z

    .line 374
    iget-boolean v0, p0, Llk;->i:Z

    invoke-virtual {v3, v1, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Llk;->u:Z

    .line 375
    const/16 v0, 0xd

    const/4 v4, -0x1

    invoke-virtual {v3, v0, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Llk;->v:I

    .line 376
    const/16 v0, 0xc

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llk;->z:Ljava/lang/String;

    .line 377
    const/16 v0, 0xe

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Llk;->w:I

    .line 378
    const/16 v0, 0xf

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llk;->x:Ljava/lang/String;

    .line 379
    const/16 v0, 0x10

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llk;->y:Ljava/lang/String;

    .line 381
    iget-object v0, p0, Llk;->y:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 382
    :goto_2
    if-eqz v1, :cond_3

    iget v0, p0, Llk;->w:I

    if-nez v0, :cond_3

    iget-object v0, p0, Llk;->x:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 383
    iget-object v0, p0, Llk;->y:Ljava/lang/String;

    invoke-static {}, Lli;->a()[Ljava/lang/Class;

    move-result-object v1

    iget-object v4, p0, Llk;->b:Lli;

    invoke-static {v4}, Lli;->b(Lli;)[Ljava/lang/Object;

    move-result-object v4

    invoke-direct {p0, v0, v1, v4}, Llk;->a(Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfr;

    iput-object v0, p0, Llk;->a:Lfr;

    .line 394
    :goto_3
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 396
    iput-boolean v2, p0, Llk;->j:Z

    .line 397
    return-void

    :cond_0
    move v0, v2

    .line 366
    goto :goto_0

    .line 370
    :cond_1
    iget v0, p0, Llk;->g:I

    iput v0, p0, Llk;->r:I

    goto :goto_1

    :cond_2
    move v1, v2

    .line 381
    goto :goto_2

    .line 387
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Llk;->a:Lfr;

    goto :goto_3
.end method

.method public c()Landroid/view/SubMenu;
    .locals 5

    .prologue
    .line 466
    const/4 v0, 0x1

    iput-boolean v0, p0, Llk;->j:Z

    .line 467
    iget-object v0, p0, Llk;->c:Landroid/view/Menu;

    iget v1, p0, Llk;->d:I

    iget v2, p0, Llk;->k:I

    iget v3, p0, Llk;->l:I

    iget-object v4, p0, Llk;->m:Ljava/lang/CharSequence;

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/view/Menu;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v0

    .line 468
    invoke-interface {v0}, Landroid/view/SubMenu;->getItem()Landroid/view/MenuItem;

    move-result-object v1

    invoke-direct {p0, v1}, Llk;->a(Landroid/view/MenuItem;)V

    .line 469
    return-object v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 473
    iget-boolean v0, p0, Llk;->j:Z

    return v0
.end method
