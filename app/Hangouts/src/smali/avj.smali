.class final Lavj;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:J

.field public e:J

.field public f:J

.field public g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 195
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 196
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lup;)V
    .locals 2

    .prologue
    .line 204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 205
    iput-object p1, p0, Lavj;->b:Ljava/lang/String;

    .line 206
    iget-object v0, p2, Lup;->a:[B

    array-length v0, v0

    iput v0, p0, Lavj;->a:I

    .line 207
    iget-object v0, p2, Lup;->b:Ljava/lang/String;

    iput-object v0, p0, Lavj;->c:Ljava/lang/String;

    .line 208
    iget-wide v0, p2, Lup;->c:J

    iput-wide v0, p0, Lavj;->d:J

    .line 209
    iget-wide v0, p2, Lup;->d:J

    iput-wide v0, p0, Lavj;->e:J

    .line 210
    iget-wide v0, p2, Lup;->e:J

    iput-wide v0, p0, Lavj;->f:J

    .line 211
    iget-object v0, p2, Lup;->f:Ljava/util/Map;

    iput-object v0, p0, Lavj;->g:Ljava/util/Map;

    .line 212
    return-void
.end method


# virtual methods
.method public a([B)Lup;
    .locals 3

    .prologue
    .line 243
    new-instance v0, Lup;

    invoke-direct {v0}, Lup;-><init>()V

    .line 244
    iput-object p1, v0, Lup;->a:[B

    .line 245
    iget-object v1, p0, Lavj;->c:Ljava/lang/String;

    iput-object v1, v0, Lup;->b:Ljava/lang/String;

    .line 246
    iget-wide v1, p0, Lavj;->d:J

    iput-wide v1, v0, Lup;->c:J

    .line 247
    iget-wide v1, p0, Lavj;->e:J

    iput-wide v1, v0, Lup;->d:J

    .line 248
    iget-wide v1, p0, Lavj;->f:J

    iput-wide v1, v0, Lup;->e:J

    .line 249
    iget-object v1, p0, Lavj;->g:Ljava/util/Map;

    iput-object v1, v0, Lup;->f:Ljava/util/Map;

    .line 250
    return-object v0
.end method

.method public a(Ljava/io/OutputStream;)Z
    .locals 2

    .prologue
    .line 258
    const v0, 0x20140131

    :try_start_0
    invoke-static {p1, v0}, Lavi;->a(Ljava/io/OutputStream;I)V

    .line 259
    iget v0, p0, Lavj;->a:I

    invoke-static {p1, v0}, Lavi;->a(Ljava/io/OutputStream;I)V

    .line 260
    iget-object v0, p0, Lavj;->b:Ljava/lang/String;

    invoke-static {p1, v0}, Lavi;->a(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 261
    iget-object v0, p0, Lavj;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-static {p1, v0}, Lavi;->a(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 262
    iget-wide v0, p0, Lavj;->d:J

    invoke-static {p1, v0, v1}, Lavi;->a(Ljava/io/OutputStream;J)V

    .line 263
    iget-wide v0, p0, Lavj;->e:J

    invoke-static {p1, v0, v1}, Lavi;->a(Ljava/io/OutputStream;J)V

    .line 264
    iget-wide v0, p0, Lavj;->f:J

    invoke-static {p1, v0, v1}, Lavi;->a(Ljava/io/OutputStream;J)V

    .line 265
    iget-object v0, p0, Lavj;->g:Ljava/util/Map;

    invoke-static {v0, p1}, Lavi;->a(Ljava/util/Map;Ljava/io/OutputStream;)V

    .line 266
    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V

    .line 267
    const/4 v0, 0x1

    .line 269
    :goto_1
    return v0

    .line 261
    :cond_0
    iget-object v0, p0, Lavj;->c:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 269
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_1
.end method
