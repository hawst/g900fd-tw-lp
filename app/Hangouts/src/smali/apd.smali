.class public final Lapd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;)V
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lapd;->a:Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lapx;)V
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 52
    invoke-virtual {p1}, Lapx;->o()Lcom/google/android/libraries/hangouts/video/CallState;

    move-result-object v0

    .line 53
    if-eqz v0, :cond_4

    .line 54
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->getCallStatistics()Lcom/google/android/libraries/hangouts/video/CallStatistics;

    move-result-object v1

    .line 55
    iget-object v2, p0, Lapd;->a:Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;

    invoke-static {v2}, Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;->b(Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 58
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->getSelf()Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    move-result-object v2

    .line 59
    if-eqz v2, :cond_0

    .line 60
    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/CallStatistics;->getLocalVideoStats()Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;

    move-result-object v3

    .line 61
    if-eqz v3, :cond_0

    .line 62
    iget-object v4, p0, Lapd;->a:Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;

    invoke-static {v4}, Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;->b(Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Local: %s\n%dx%d %dfps IN / %dfps OUT"

    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/Object;

    .line 64
    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v7

    iget v2, v3, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->frameWidth:I

    .line 65
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v6, v8

    iget v2, v3, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->frameHeight:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v6, v9

    iget v2, v3, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->frameRateInput:I

    .line 66
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v6, v10

    iget v2, v3, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->frameRateSent:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v6, v11

    .line 63
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 62
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->getRemoteEndpoints()Ljava/util/Collection;

    move-result-object v0

    .line 73
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 75
    invoke-virtual {v1, v0}, Lcom/google/android/libraries/hangouts/video/CallStatistics;->getEndpointVideoStats(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;

    move-result-object v3

    .line 76
    if-eqz v3, :cond_1

    .line 77
    iget-object v4, p0, Lapd;->a:Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;

    invoke-static {v4}, Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;->b(Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    iget-object v4, p0, Lapd;->a:Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;

    invoke-static {v4}, Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;->b(Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Remote: %s\n%dx%d %dfps IN / %dfps OUT"

    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/Object;

    .line 80
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v7

    iget v0, v3, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->frameWidth:I

    .line 81
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v8

    iget v0, v3, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->frameHeight:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v9

    iget v0, v3, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->frameRateRcvd:I

    .line 82
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v10

    iget v0, v3, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->frameRateDecoded:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v11

    .line 79
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 78
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    iget-object v0, p0, Lapd;->a:Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;->b(Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " | Renderer: %.2f IN / %.2f OUT"

    new-array v5, v9, [Ljava/lang/Object;

    iget v6, v3, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->frameRateRenderInput:F

    .line 86
    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v5, v7

    iget v3, v3, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->frameRateRenderOutput:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v5, v8

    .line 85
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 84
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 92
    :cond_2
    iget-object v0, p0, Lapd;->a:Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;->b(Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 93
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_3

    .line 94
    const-string v0, "Getting debug stats ..."

    .line 96
    :cond_3
    iget-object v1, p0, Lapd;->a:Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    :cond_4
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 33
    iget-object v0, p0, Lapd;->a:Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;->a(Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;)Lapk;

    move-result-object v0

    invoke-virtual {v0}, Lapk;->c()Lapx;

    move-result-object v0

    .line 34
    if-eqz v0, :cond_1

    .line 36
    const-string v1, "hangout_overlay"

    invoke-static {v1}, Lbys;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 37
    iget-object v1, p0, Lapd;->a:Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;->b(Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-nez v1, :cond_0

    .line 38
    iget-object v1, p0, Lapd;->a:Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;->a(Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 40
    :cond_0
    iget-object v1, p0, Lapd;->a:Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;->setVisibility(I)V

    .line 41
    invoke-direct {p0, v0}, Lapd;->a(Lapx;)V

    .line 47
    :goto_0
    iget-object v0, p0, Lapd;->a:Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;->d(Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lapd;->a:Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;->c(Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 49
    :cond_1
    return-void

    .line 43
    :cond_2
    iget-object v0, p0, Lapd;->a:Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;->setVisibility(I)V

    .line 44
    iget-object v0, p0, Lapd;->a:Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;->a(Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
