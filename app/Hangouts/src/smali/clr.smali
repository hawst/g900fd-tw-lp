.class final Lclr;
.super Lckv;


# instance fields
.field final synthetic a:Lclj;

.field private final b:Lcmt;


# direct methods
.method public constructor <init>(Lclj;Lcmt;)V
    .locals 0

    iput-object p1, p0, Lclr;->a:Lclj;

    invoke-direct {p0}, Lckv;-><init>()V

    iput-object p2, p0, Lclr;->b:Lcmt;

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;[Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 2

    const-string v0, "PeopleService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "People callback: status="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nresolution="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nholders="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p3}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-static {p1, p2}, Lclj;->b(ILandroid/os/Bundle;)Lcft;

    move-result-object v0

    iget-object v1, p0, Lclr;->b:Lcmt;

    invoke-virtual {v1, v0, p3}, Lcmt;->a(Lcft;[Lcom/google/android/gms/common/data/DataHolder;)V

    return-void
.end method
