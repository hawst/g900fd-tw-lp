.class public final Labz;
.super Ljd;
.source "PG"


# instance fields
.field final synthetic j:Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 235
    iput-object p1, p0, Labz;->j:Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;

    .line 236
    invoke-direct {p0, p2}, Ljd;-><init>(Landroid/content/Context;)V

    .line 237
    iput-object p2, p0, Labz;->d:Landroid/content/Context;

    .line 238
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 292
    invoke-static {p1}, Lcau;->a(Landroid/content/Context;)Lcau;

    move-result-object v0

    .line 293
    iget-object v1, p0, Labz;->j:Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->g(Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;)Lcav;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcau;->a(Lcav;)V

    .line 294
    return-object v0
.end method

.method public a(Landroid/view/View;Landroid/database/Cursor;)V
    .locals 3

    .prologue
    .line 299
    check-cast p1, Lcau;

    .line 300
    const/4 v0, 0x3

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcau;->a(Ljava/lang/String;)V

    .line 301
    const/4 v0, 0x4

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Labz;->j:Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;

    .line 302
    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->b(Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;)Lyj;

    move-result-object v1

    .line 301
    invoke-virtual {p1, v0, v1}, Lcau;->a(Ljava/lang/String;Lyj;)V

    .line 303
    new-instance v0, Lbdk;

    const/4 v1, 0x2

    .line 304
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    .line 305
    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lbdk;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    invoke-virtual {p1, v0}, Lcau;->a(Lbdk;)V

    .line 307
    return-void
.end method

.method public b(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 286
    invoke-super {p0, p1}, Ljd;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    .line 287
    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 244
    invoke-super {p0}, Ljd;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 249
    invoke-virtual {p0}, Labz;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 250
    if-nez p2, :cond_0

    iget-object v0, p0, Labz;->d:Landroid/content/Context;

    invoke-virtual {p0}, Labz;->a()Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p3}, Labz;->a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 259
    :cond_0
    :goto_0
    return-object p2

    .line 255
    :cond_1
    invoke-super {p0, p1, p2, p3}, Ljd;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 256
    const-class v0, Lcau;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, v1

    .line 257
    check-cast v0, Lcau;

    sget v2, Lg;->hT:I

    invoke-virtual {v0, v2}, Lcau;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    move v4, v5

    :goto_1
    iget-object v3, p0, Labz;->j:Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;

    invoke-static {v3}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->c(Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;)Lfe;

    move-result-object v3

    invoke-virtual {v3}, Lfe;->size()I

    move-result v3

    if-ge v4, v3, :cond_6

    iget-object v3, p0, Labz;->j:Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;

    invoke-static {v3}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->c(Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;)Lfe;

    move-result-object v3

    invoke-virtual {v3, v4}, Lfe;->c(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lacb;

    iget-object v3, v3, Lacb;->a:Lbdk;

    invoke-virtual {v0}, Lcau;->a()Lbdk;

    move-result-object v7

    invoke-virtual {v3, v7}, Lbdk;->a(Lbdk;)Z

    move-result v3

    if-eqz v3, :cond_3

    move v3, v6

    :goto_2
    if-eqz v3, :cond_4

    sget v0, Lh;->nx:I

    :goto_3
    if-eqz v3, :cond_5

    :goto_4
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setText(I)V

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setClickable(Z)V

    :cond_2
    move-object p2, v1

    .line 259
    goto :goto_0

    .line 257
    :cond_3
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    :cond_4
    sget v0, Lh;->nw:I

    goto :goto_3

    :cond_5
    move v5, v6

    goto :goto_4

    :cond_6
    move v3, v5

    goto :goto_2
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 312
    invoke-virtual {p0}, Labz;->a()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_0

    .line 313
    const/4 v0, 0x1

    .line 315
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Ljd;->isEmpty()Z

    move-result v0

    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 281
    const/4 v0, 0x0

    return v0
.end method
