.class public Lbqy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Ljava/lang/Object;

.field final b:Ljava/lang/Thread;

.field private final c:Ljava/lang/Object;

.field private final d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:Z

.field private g:Z

.field private h:Z

.field private final i:J

.field private final j:J

.field private volatile k:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;JJ)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lbqy;->b:Ljava/lang/Thread;

    .line 17
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbqy;->a:Ljava/lang/Object;

    .line 18
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbqy;->c:Ljava/lang/Object;

    .line 19
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbqy;->d:Ljava/lang/Object;

    .line 32
    iput-wide p2, p0, Lbqy;->i:J

    .line 33
    iput-wide p4, p0, Lbqy;->j:J

    .line 34
    iget-object v0, p0, Lbqy;->b:Ljava/lang/Thread;

    invoke-virtual {v0, p1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 35
    iget-object v0, p0, Lbqy;->b:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 36
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    if-nez p1, :cond_0

    .line 141
    const-string v0, "NULL"

    .line 143
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 119
    iget-boolean v0, p0, Lbqy;->k:Z

    if-eqz v0, :cond_0

    .line 136
    :goto_0
    return-void

    .line 122
    :cond_0
    iget-object v1, p0, Lbqy;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 123
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lbqy;->g:Z

    .line 124
    iget-object v0, p0, Lbqy;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 125
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 128
    iget-object v1, p0, Lbqy;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 129
    :try_start_1
    iget-boolean v0, p0, Lbqy;->h:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_1

    .line 131
    :try_start_2
    iget-object v0, p0, Lbqy;->d:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 135
    :cond_1
    :goto_1
    const/4 v0, 0x0

    :try_start_3
    iput-boolean v0, p0, Lbqy;->h:Z

    .line 136
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 125
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public b()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 148
    iput-boolean v0, p0, Lbqy;->k:Z

    .line 150
    iget-object v1, p0, Lbqy;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 151
    :try_start_0
    iget-object v0, p0, Lbqy;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 152
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153
    iget-object v1, p0, Lbqy;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 154
    :try_start_1
    iget-object v0, p0, Lbqy;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 155
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 159
    iget-object v1, p0, Lbqy;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 160
    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, Lbqy;->h:Z

    .line 161
    iget-object v0, p0, Lbqy;->d:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 162
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 165
    :try_start_3
    iget-object v0, p0, Lbqy;->b:Ljava/lang/Thread;

    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v1, v2}, Ljava/lang/Thread;->join(J)V

    .line 166
    iget-object v0, p0, Lbqy;->b:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167
    const-string v0, "Babel"

    const-string v1, "failed to stop watchdog thread in 1000 ms"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0

    .line 171
    :cond_0
    :goto_0
    return-void

    .line 152
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 155
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    .line 162
    :catchall_2
    move-exception v0

    monitor-exit v1

    throw v0

    .line 171
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public b(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 108
    iget-boolean v0, p0, Lbqy;->k:Z

    if-eqz v0, :cond_0

    .line 115
    :goto_0
    return-void

    .line 111
    :cond_0
    iget-object v1, p0, Lbqy;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 112
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lbqy;->f:Z

    .line 113
    iput-object p1, p0, Lbqy;->e:Ljava/lang/Object;

    .line 114
    iget-object v0, p0, Lbqy;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 115
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public run()V
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 40
    :goto_0
    iget-boolean v0, p0, Lbqy;->k:Z

    if-nez v0, :cond_0

    .line 42
    iget-object v2, p0, Lbqy;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 43
    :try_start_0
    iget-boolean v0, p0, Lbqy;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v0, :cond_1

    .line 45
    :try_start_1
    iget-object v0, p0, Lbqy;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V

    .line 46
    iget-boolean v0, p0, Lbqy;->k:Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v0, :cond_1

    .line 47
    :try_start_2
    monitor-exit v2

    .line 104
    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v0

    .line 52
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbqy;->f:Z

    .line 53
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 55
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 59
    iget-object v4, p0, Lbqy;->c:Ljava/lang/Object;

    monitor-enter v4

    move v0, v1

    .line 60
    :cond_2
    :goto_2
    :try_start_3
    iget-boolean v5, p0, Lbqy;->g:Z

    if-nez v5, :cond_6

    iget-boolean v5, p0, Lbqy;->k:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-nez v5, :cond_6

    .line 62
    :try_start_4
    iget-object v5, p0, Lbqy;->c:Ljava/lang/Object;

    iget-wide v6, p0, Lbqy;->i:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/Object;->wait(J)V

    .line 63
    iget-boolean v5, p0, Lbqy;->k:Z
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v5, :cond_3

    .line 64
    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 97
    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    .line 53
    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0

    .line 66
    :cond_3
    :try_start_6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 67
    sub-long/2addr v5, v2

    .line 68
    iget-wide v7, p0, Lbqy;->j:J

    cmp-long v7, v5, v7

    if-ltz v7, :cond_4

    .line 69
    const-string v5, "Babel"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "error, intent took more than "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v7, p0, Lbqy;->j:J

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ms; opcode "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lbqy;->e:Ljava/lang/Object;

    .line 71
    invoke-virtual {p0, v7}, Lbqy;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 69
    invoke-static {v5, v6}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v5

    const/4 v6, 0x3

    invoke-static {v5, v6}, Landroid/os/Process;->sendSignal(II)V

    .line 75
    invoke-static {}, Landroid/os/Debug;->isDebuggerConnected()Z

    move-result v5

    if-nez v5, :cond_2

    .line 77
    new-instance v5, Ljava/lang/IllegalStateException;

    const-string v6, "hung up in RTCS"

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 94
    :catch_1
    move-exception v5

    goto :goto_2

    .line 79
    :cond_4
    iget-wide v7, p0, Lbqy;->i:J

    cmp-long v7, v5, v7

    if-lez v7, :cond_2

    .line 80
    const-string v7, "Babel"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "warning "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "; intent took more than "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-wide v9, p0, Lbqy;->i:J

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ms; opcode "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lbqy;->e:Ljava/lang/Object;

    .line 83
    invoke-virtual {p0, v9}, Lbqy;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "; "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " so far"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 80
    invoke-static {v7, v5}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    if-nez v0, :cond_5

    .line 88
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v5

    const/4 v6, 0x3

    invoke-static {v5, v6}, Landroid/os/Process;->sendSignal(II)V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 90
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_2

    .line 96
    :cond_6
    const/4 v0, 0x0

    :try_start_7
    iput-boolean v0, p0, Lbqy;->g:Z

    .line 97
    monitor-exit v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 100
    iget-object v2, p0, Lbqy;->d:Ljava/lang/Object;

    monitor-enter v2

    .line 101
    const/4 v0, 0x1

    :try_start_8
    iput-boolean v0, p0, Lbqy;->h:Z

    .line 102
    iget-object v0, p0, Lbqy;->d:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 103
    monitor-exit v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto/16 :goto_0

    :catchall_2
    move-exception v0

    monitor-exit v2

    throw v0
.end method
