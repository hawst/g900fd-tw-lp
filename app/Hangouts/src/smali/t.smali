.class public Lt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/ComponentCallbacks;
.implements Landroid/view/View$OnCreateContextMenuListener;


# static fields
.field private static final a:Lfe;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lfe",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation
.end field


# instance fields
.field A:I

.field B:Laf;

.field C:Ly;

.field D:Laf;

.field E:Lt;

.field F:I

.field G:I

.field H:Ljava/lang/String;

.field I:Z

.field J:Z

.field K:Z

.field L:Z

.field M:Z

.field N:Z

.field O:Z

.field P:I

.field Q:Landroid/view/ViewGroup;

.field R:Landroid/view/View;

.field S:Landroid/view/View;

.field T:Z

.field U:Z

.field V:Lax;

.field W:Z

.field X:Z

.field j:I

.field k:Landroid/view/View;

.field l:I

.field m:Landroid/os/Bundle;

.field n:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation
.end field

.field o:I

.field p:Ljava/lang/String;

.field q:Landroid/os/Bundle;

.field r:Lt;

.field s:I

.field t:I

.field u:Z

.field v:Z

.field w:Z

.field x:Z

.field y:Z

.field z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 166
    new-instance v0, Lfe;

    invoke-direct {v0}, Lfe;-><init>()V

    sput-object v0, Lt;->a:Lfe;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, -0x1

    .line 371
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176
    const/4 v0, 0x0

    iput v0, p0, Lt;->j:I

    .line 192
    iput v1, p0, Lt;->o:I

    .line 204
    iput v1, p0, Lt;->s:I

    .line 275
    iput-boolean v2, p0, Lt;->N:Z

    .line 297
    iput-boolean v2, p0, Lt;->U:Z

    .line 372
    return-void
.end method

.method static a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 435
    :try_start_0
    sget-object v0, Lt;->a:Lfe;

    invoke-virtual {v0, p1}, Lfe;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 436
    if-nez v0, :cond_0

    .line 438
    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 439
    sget-object v1, Lt;->a:Lfe;

    invoke-virtual {v1, p1, v0}, Lfe;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 441
    :cond_0
    const-class v1, Lt;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 443
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static instantiate(Landroid/content/Context;Ljava/lang/String;)Lt;
    .locals 1

    .prologue
    .line 379
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lt;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Lt;

    move-result-object v0

    return-object v0
.end method

.method public static instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Lt;
    .locals 4

    .prologue
    .line 398
    :try_start_0
    sget-object v0, Lt;->a:Lfe;

    invoke-virtual {v0, p1}, Lfe;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 399
    if-nez v0, :cond_0

    .line 401
    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 402
    sget-object v1, Lt;->a:Lfe;

    invoke-virtual {v1, p1, v0}, Lfe;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 404
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt;

    .line 405
    if-eqz p2, :cond_1

    .line 406
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 407
    iput-object p2, v0, Lt;->q:Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 409
    :cond_1
    return-object v0

    .line 410
    :catch_0
    move-exception v0

    .line 411
    new-instance v1, Lv;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to instantiate fragment "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": make sure class name exists, is public, and has an empty constructor that is public"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lv;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 414
    :catch_1
    move-exception v0

    .line 415
    new-instance v1, Lv;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to instantiate fragment "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": make sure class name exists, is public, and has an empty constructor that is public"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lv;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 418
    :catch_2
    move-exception v0

    .line 419
    new-instance v1, Lv;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to instantiate fragment "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": make sure class name exists, is public, and has an empty constructor that is public"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lv;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1
.end method


# virtual methods
.method a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1501
    iget-object v0, p0, Lt;->D:Laf;

    if-eqz v0, :cond_0

    .line 1502
    iget-object v0, p0, Lt;->D:Laf;

    invoke-virtual {v0}, Laf;->noteStateNotSaved()V

    .line 1504
    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lt;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method final a(ILt;)V
    .locals 2

    .prologue
    .line 461
    iput p1, p0, Lt;->o:I

    .line 462
    if-eqz p2, :cond_0

    .line 463
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p2, Lt;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lt;->o:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lt;->p:Ljava/lang/String;

    .line 467
    :goto_0
    return-void

    .line 465
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "android:fragment:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lt;->o:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lt;->p:Ljava/lang/String;

    goto :goto_0
.end method

.method a(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 1559
    invoke-virtual {p0, p1}, Lt;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1560
    iget-object v0, p0, Lt;->D:Laf;

    if-eqz v0, :cond_0

    .line 1561
    iget-object v0, p0, Lt;->D:Laf;

    invoke-virtual {v0, p1}, Laf;->a(Landroid/content/res/Configuration;)V

    .line 1563
    :cond_0
    return-void
.end method

.method a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1477
    iget-object v0, p0, Lt;->D:Laf;

    if-eqz v0, :cond_0

    .line 1478
    iget-object v0, p0, Lt;->D:Laf;

    invoke-virtual {v0}, Laf;->noteStateNotSaved()V

    .line 1480
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lt;->O:Z

    .line 1481
    invoke-virtual {p0, p1}, Lt;->onCreate(Landroid/os/Bundle;)V

    .line 1482
    iget-boolean v0, p0, Lt;->O:Z

    if-nez v0, :cond_1

    .line 1483
    new-instance v0, Lct;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onCreate()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lct;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1486
    :cond_1
    if-eqz p1, :cond_3

    .line 1487
    const-string v0, "android:support:fragments"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    .line 1489
    if-eqz v0, :cond_3

    .line 1490
    iget-object v1, p0, Lt;->D:Laf;

    if-nez v1, :cond_2

    .line 1491
    invoke-virtual {p0}, Lt;->h_()V

    .line 1493
    :cond_2
    iget-object v1, p0, Lt;->D:Laf;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Laf;->a(Landroid/os/Parcelable;Ljava/util/ArrayList;)V

    .line 1494
    iget-object v0, p0, Lt;->D:Laf;

    invoke-virtual {v0}, Laf;->k()V

    .line 1497
    :cond_3
    return-void
.end method

.method a(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 1596
    const/4 v0, 0x0

    .line 1597
    iget-boolean v1, p0, Lt;->I:Z

    if-nez v1, :cond_1

    .line 1598
    iget-boolean v1, p0, Lt;->M:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lt;->N:Z

    if-eqz v1, :cond_0

    .line 1599
    const/4 v0, 0x1

    .line 1600
    invoke-virtual {p0, p1}, Lt;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 1602
    :cond_0
    iget-object v1, p0, Lt;->D:Laf;

    if-eqz v1, :cond_1

    .line 1603
    iget-object v1, p0, Lt;->D:Laf;

    invoke-virtual {v1, p1}, Laf;->a(Landroid/view/Menu;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 1606
    :cond_1
    return v0
.end method

.method a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z
    .locals 2

    .prologue
    .line 1582
    const/4 v0, 0x0

    .line 1583
    iget-boolean v1, p0, Lt;->I:Z

    if-nez v1, :cond_1

    .line 1584
    iget-boolean v1, p0, Lt;->M:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lt;->N:Z

    if-eqz v1, :cond_0

    .line 1585
    const/4 v0, 0x1

    .line 1586
    invoke-virtual {p0, p1, p2}, Lt;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 1588
    :cond_0
    iget-object v1, p0, Lt;->D:Laf;

    if-eqz v1, :cond_1

    .line 1589
    iget-object v1, p0, Lt;->D:Laf;

    invoke-virtual {v1, p1, p2}, Laf;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 1592
    :cond_1
    return v0
.end method

.method a(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1610
    iget-boolean v1, p0, Lt;->I:Z

    if-nez v1, :cond_2

    .line 1611
    iget-boolean v1, p0, Lt;->M:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lt;->N:Z

    if-eqz v1, :cond_1

    .line 1612
    invoke-virtual {p0, p1}, Lt;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1622
    :cond_0
    :goto_0
    return v0

    .line 1616
    :cond_1
    iget-object v1, p0, Lt;->D:Laf;

    if-eqz v1, :cond_2

    .line 1617
    iget-object v1, p0, Lt;->D:Laf;

    invoke-virtual {v1, p1}, Laf;->a(Landroid/view/MenuItem;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1622
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1508
    iget-object v0, p0, Lt;->D:Laf;

    if-eqz v0, :cond_0

    .line 1509
    iget-object v0, p0, Lt;->D:Laf;

    invoke-virtual {v0}, Laf;->noteStateNotSaved()V

    .line 1511
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lt;->O:Z

    .line 1512
    invoke-virtual {p0, p1}, Lt;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1513
    iget-boolean v0, p0, Lt;->O:Z

    if-nez v0, :cond_1

    .line 1514
    new-instance v0, Lct;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onActivityCreated()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lct;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1517
    :cond_1
    iget-object v0, p0, Lt;->D:Laf;

    if-eqz v0, :cond_2

    .line 1518
    iget-object v0, p0, Lt;->D:Laf;

    invoke-virtual {v0}, Laf;->l()V

    .line 1520
    :cond_2
    return-void
.end method

.method b(Landroid/view/Menu;)V
    .locals 1

    .prologue
    .line 1640
    iget-boolean v0, p0, Lt;->I:Z

    if-nez v0, :cond_1

    .line 1641
    iget-boolean v0, p0, Lt;->M:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lt;->N:Z

    if-eqz v0, :cond_0

    .line 1642
    invoke-virtual {p0, p1}, Lt;->onOptionsMenuClosed(Landroid/view/Menu;)V

    .line 1644
    :cond_0
    iget-object v0, p0, Lt;->D:Laf;

    if-eqz v0, :cond_1

    .line 1645
    iget-object v0, p0, Lt;->D:Laf;

    invoke-virtual {v0, p1}, Laf;->b(Landroid/view/Menu;)V

    .line 1648
    :cond_1
    return-void
.end method

.method b(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1626
    iget-boolean v1, p0, Lt;->I:Z

    if-nez v1, :cond_2

    .line 1627
    invoke-virtual {p0, p1}, Lt;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1636
    :cond_0
    :goto_0
    return v0

    .line 1630
    :cond_1
    iget-object v1, p0, Lt;->D:Laf;

    if-eqz v1, :cond_2

    .line 1631
    iget-object v1, p0, Lt;->D:Laf;

    invoke-virtual {v1, p1}, Laf;->b(Landroid/view/MenuItem;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1636
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1651
    invoke-virtual {p0, p1}, Lt;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1652
    iget-object v0, p0, Lt;->D:Laf;

    if-eqz v0, :cond_0

    .line 1653
    iget-object v0, p0, Lt;->D:Laf;

    invoke-virtual {v0}, Laf;->j()Landroid/os/Parcelable;

    move-result-object v0

    .line 1654
    if-eqz v0, :cond_0

    .line 1655
    const-string v1, "android:support:fragments"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1658
    :cond_0
    return-void
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1377
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mFragmentId=#"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1378
    iget v0, p0, Lt;->F:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1379
    const-string v0, " mContainerId=#"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1380
    iget v0, p0, Lt;->G:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1381
    const-string v0, " mTag="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lt;->H:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1382
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mState="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lt;->j:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 1383
    const-string v0, " mIndex="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lt;->o:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 1384
    const-string v0, " mWho="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lt;->p:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1385
    const-string v0, " mBackStackNesting="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lt;->A:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 1386
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mAdded="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lt;->u:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1387
    const-string v0, " mRemoving="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lt;->v:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1388
    const-string v0, " mResumed="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lt;->w:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1389
    const-string v0, " mFromLayout="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lt;->x:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1390
    const-string v0, " mInLayout="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lt;->y:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 1391
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mHidden="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lt;->I:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1392
    const-string v0, " mDetached="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lt;->J:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1393
    const-string v0, " mMenuVisible="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lt;->N:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1394
    const-string v0, " mHasMenu="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lt;->M:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 1395
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mRetainInstance="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lt;->K:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1396
    const-string v0, " mRetaining="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lt;->L:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1397
    const-string v0, " mUserVisibleHint="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lt;->U:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 1398
    iget-object v0, p0, Lt;->B:Laf;

    if-eqz v0, :cond_0

    .line 1399
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mFragmentManager="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1400
    iget-object v0, p0, Lt;->B:Laf;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1402
    :cond_0
    iget-object v0, p0, Lt;->C:Ly;

    if-eqz v0, :cond_1

    .line 1403
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mActivity="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1404
    iget-object v0, p0, Lt;->C:Ly;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1406
    :cond_1
    iget-object v0, p0, Lt;->E:Lt;

    if-eqz v0, :cond_2

    .line 1407
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mParentFragment="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1408
    iget-object v0, p0, Lt;->E:Lt;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1410
    :cond_2
    iget-object v0, p0, Lt;->q:Landroid/os/Bundle;

    if-eqz v0, :cond_3

    .line 1411
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mArguments="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lt;->q:Landroid/os/Bundle;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1413
    :cond_3
    iget-object v0, p0, Lt;->m:Landroid/os/Bundle;

    if-eqz v0, :cond_4

    .line 1414
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mSavedFragmentState="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1415
    iget-object v0, p0, Lt;->m:Landroid/os/Bundle;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1417
    :cond_4
    iget-object v0, p0, Lt;->n:Landroid/util/SparseArray;

    if-eqz v0, :cond_5

    .line 1418
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mSavedViewState="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1419
    iget-object v0, p0, Lt;->n:Landroid/util/SparseArray;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1421
    :cond_5
    iget-object v0, p0, Lt;->r:Lt;

    if-eqz v0, :cond_6

    .line 1422
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mTarget="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lt;->r:Lt;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    .line 1423
    const-string v0, " mTargetRequestCode="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1424
    iget v0, p0, Lt;->t:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 1426
    :cond_6
    iget v0, p0, Lt;->P:I

    if-eqz v0, :cond_7

    .line 1427
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mNextAnim="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lt;->P:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 1429
    :cond_7
    iget-object v0, p0, Lt;->Q:Landroid/view/ViewGroup;

    if-eqz v0, :cond_8

    .line 1430
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mContainer="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lt;->Q:Landroid/view/ViewGroup;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1432
    :cond_8
    iget-object v0, p0, Lt;->R:Landroid/view/View;

    if-eqz v0, :cond_9

    .line 1433
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mView="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lt;->R:Landroid/view/View;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1435
    :cond_9
    iget-object v0, p0, Lt;->S:Landroid/view/View;

    if-eqz v0, :cond_a

    .line 1436
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mInnerView="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lt;->R:Landroid/view/View;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1438
    :cond_a
    iget-object v0, p0, Lt;->k:Landroid/view/View;

    if-eqz v0, :cond_b

    .line 1439
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mAnimatingAway="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lt;->k:Landroid/view/View;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1440
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mStateAfterAnimating="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1441
    iget v0, p0, Lt;->l:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 1443
    :cond_b
    iget-object v0, p0, Lt;->V:Lax;

    if-eqz v0, :cond_c

    .line 1444
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Loader Manager:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1445
    iget-object v0, p0, Lt;->V:Lax;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p4}, Lax;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 1447
    :cond_c
    iget-object v0, p0, Lt;->D:Laf;

    if-eqz v0, :cond_d

    .line 1448
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Child "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lt;->D:Laf;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1449
    iget-object v0, p0, Lt;->D:Laf;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p4}, Laf;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 1451
    :cond_d
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 477
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method g_()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1194
    const/4 v0, -0x1

    iput v0, p0, Lt;->o:I

    .line 1195
    iput-object v2, p0, Lt;->p:Ljava/lang/String;

    .line 1196
    iput-boolean v1, p0, Lt;->u:Z

    .line 1197
    iput-boolean v1, p0, Lt;->v:Z

    .line 1198
    iput-boolean v1, p0, Lt;->w:Z

    .line 1199
    iput-boolean v1, p0, Lt;->x:Z

    .line 1200
    iput-boolean v1, p0, Lt;->y:Z

    .line 1201
    iput-boolean v1, p0, Lt;->z:Z

    .line 1202
    iput v1, p0, Lt;->A:I

    .line 1203
    iput-object v2, p0, Lt;->B:Laf;

    .line 1204
    iput-object v2, p0, Lt;->D:Laf;

    .line 1205
    iput-object v2, p0, Lt;->C:Ly;

    .line 1206
    iput v1, p0, Lt;->F:I

    .line 1207
    iput v1, p0, Lt;->G:I

    .line 1208
    iput-object v2, p0, Lt;->H:Ljava/lang/String;

    .line 1209
    iput-boolean v1, p0, Lt;->I:Z

    .line 1210
    iput-boolean v1, p0, Lt;->J:Z

    .line 1211
    iput-boolean v1, p0, Lt;->L:Z

    .line 1212
    iput-object v2, p0, Lt;->V:Lax;

    .line 1213
    iput-boolean v1, p0, Lt;->W:Z

    .line 1214
    iput-boolean v1, p0, Lt;->X:Z

    .line 1215
    return-void
.end method

.method public final getActivity()Ly;
    .locals 1

    .prologue
    .line 595
    iget-object v0, p0, Lt;->C:Ly;

    return-object v0
.end method

.method public final getArguments()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 542
    iget-object v0, p0, Lt;->q:Landroid/os/Bundle;

    return-object v0
.end method

.method public final getChildFragmentManager()Lae;
    .locals 2

    .prologue
    .line 660
    iget-object v0, p0, Lt;->D:Laf;

    if-nez v0, :cond_0

    .line 661
    invoke-virtual {p0}, Lt;->h_()V

    .line 662
    iget v0, p0, Lt;->j:I

    const/4 v1, 0x5

    if-lt v0, v1, :cond_1

    .line 663
    iget-object v0, p0, Lt;->D:Laf;

    invoke-virtual {v0}, Laf;->n()V

    .line 672
    :cond_0
    :goto_0
    iget-object v0, p0, Lt;->D:Laf;

    return-object v0

    .line 664
    :cond_1
    iget v0, p0, Lt;->j:I

    const/4 v1, 0x4

    if-lt v0, v1, :cond_2

    .line 665
    iget-object v0, p0, Lt;->D:Laf;

    invoke-virtual {v0}, Laf;->m()V

    goto :goto_0

    .line 666
    :cond_2
    iget v0, p0, Lt;->j:I

    const/4 v1, 0x2

    if-lt v0, v1, :cond_3

    .line 667
    iget-object v0, p0, Lt;->D:Laf;

    invoke-virtual {v0}, Laf;->l()V

    goto :goto_0

    .line 668
    :cond_3
    iget v0, p0, Lt;->j:I

    if-lez v0, :cond_0

    .line 669
    iget-object v0, p0, Lt;->D:Laf;

    invoke-virtual {v0}, Laf;->k()V

    goto :goto_0
.end method

.method public final getFragmentManager()Lae;
    .locals 1

    .prologue
    .line 652
    iget-object v0, p0, Lt;->B:Laf;

    return-object v0
.end method

.method public final getId()I
    .locals 1

    .prologue
    .line 513
    iget v0, p0, Lt;->F:I

    return v0
.end method

.method public getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 917
    iget-object v0, p0, Lt;->C:Ly;

    invoke-virtual {v0}, Ly;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    return-object v0
.end method

.method public getLoaderManager()Lav;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 861
    iget-object v0, p0, Lt;->V:Lax;

    if-eqz v0, :cond_0

    .line 862
    iget-object v0, p0, Lt;->V:Lax;

    .line 869
    :goto_0
    return-object v0

    .line 864
    :cond_0
    iget-object v0, p0, Lt;->C:Ly;

    if-nez v0, :cond_1

    .line 865
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not attached to Activity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 867
    :cond_1
    iput-boolean v3, p0, Lt;->X:Z

    .line 868
    iget-object v0, p0, Lt;->C:Ly;

    iget-object v1, p0, Lt;->p:Ljava/lang/String;

    iget-boolean v2, p0, Lt;->W:Z

    invoke-virtual {v0, v1, v2, v3}, Ly;->a(Ljava/lang/String;ZZ)Lax;

    move-result-object v0

    iput-object v0, p0, Lt;->V:Lax;

    .line 869
    iget-object v0, p0, Lt;->V:Lax;

    goto :goto_0
.end method

.method public final getParentFragment()Lt;
    .locals 1

    .prologue
    .line 680
    iget-object v0, p0, Lt;->E:Lt;

    return-object v0
.end method

.method public final getResources()Landroid/content/res/Resources;
    .locals 3

    .prologue
    .line 602
    iget-object v0, p0, Lt;->C:Ly;

    if-nez v0, :cond_0

    .line 603
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not attached to Activity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 605
    :cond_0
    iget-object v0, p0, Lt;->C:Ly;

    invoke-virtual {v0}, Ly;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public final getRetainInstance()Z
    .locals 1

    .prologue
    .line 791
    iget-boolean v0, p0, Lt;->K:Z

    return v0
.end method

.method public final getString(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 625
    invoke-virtual {p0}, Lt;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final varargs getString(I[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 638
    invoke-virtual {p0}, Lt;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 520
    iget-object v0, p0, Lt;->H:Ljava/lang/String;

    return-object v0
.end method

.method public final getTargetFragment()Lt;
    .locals 1

    .prologue
    .line 581
    iget-object v0, p0, Lt;->r:Lt;

    return-object v0
.end method

.method public final getTargetRequestCode()I
    .locals 1

    .prologue
    .line 588
    iget v0, p0, Lt;->t:I

    return v0
.end method

.method public final getText(I)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 615
    invoke-virtual {p0}, Lt;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getUserVisibleHint()Z
    .locals 1

    .prologue
    .line 854
    iget-boolean v0, p0, Lt;->U:Z

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 1044
    iget-object v0, p0, Lt;->R:Landroid/view/View;

    return-object v0
.end method

.method h_()V
    .locals 3

    .prologue
    .line 1464
    new-instance v0, Laf;

    invoke-direct {v0}, Laf;-><init>()V

    iput-object v0, p0, Lt;->D:Laf;

    .line 1465
    iget-object v0, p0, Lt;->D:Laf;

    iget-object v1, p0, Lt;->C:Ly;

    new-instance v2, Lu;

    invoke-direct {v2, p0}, Lu;-><init>(Lt;)V

    invoke-virtual {v0, v1, v2, p0}, Laf;->a(Ly;Lad;Lt;)V

    .line 1474
    return-void
.end method

.method public final hasOptionsMenu()Z
    .locals 1

    .prologue
    .line 750
    iget-boolean v0, p0, Lt;->M:Z

    return v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 484
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method i()V
    .locals 3

    .prologue
    .line 1523
    iget-object v0, p0, Lt;->D:Laf;

    if-eqz v0, :cond_0

    .line 1524
    iget-object v0, p0, Lt;->D:Laf;

    invoke-virtual {v0}, Laf;->noteStateNotSaved()V

    .line 1525
    iget-object v0, p0, Lt;->D:Laf;

    invoke-virtual {v0}, Laf;->h()Z

    .line 1527
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lt;->O:Z

    .line 1528
    invoke-virtual {p0}, Lt;->onStart()V

    .line 1529
    iget-boolean v0, p0, Lt;->O:Z

    if-nez v0, :cond_1

    .line 1530
    new-instance v0, Lct;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onStart()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lct;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1533
    :cond_1
    iget-object v0, p0, Lt;->D:Laf;

    if-eqz v0, :cond_2

    .line 1534
    iget-object v0, p0, Lt;->D:Laf;

    invoke-virtual {v0}, Laf;->m()V

    .line 1536
    :cond_2
    iget-object v0, p0, Lt;->V:Lax;

    if-eqz v0, :cond_3

    .line 1537
    iget-object v0, p0, Lt;->V:Lax;

    invoke-virtual {v0}, Lax;->g()V

    .line 1539
    :cond_3
    return-void
.end method

.method i_()V
    .locals 3

    .prologue
    .line 1542
    iget-object v0, p0, Lt;->D:Laf;

    if-eqz v0, :cond_0

    .line 1543
    iget-object v0, p0, Lt;->D:Laf;

    invoke-virtual {v0}, Laf;->noteStateNotSaved()V

    .line 1544
    iget-object v0, p0, Lt;->D:Laf;

    invoke-virtual {v0}, Laf;->h()Z

    .line 1546
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lt;->O:Z

    .line 1547
    invoke-virtual {p0}, Lt;->onResume()V

    .line 1548
    iget-boolean v0, p0, Lt;->O:Z

    if-nez v0, :cond_1

    .line 1549
    new-instance v0, Lct;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onResume()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lct;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1552
    :cond_1
    iget-object v0, p0, Lt;->D:Laf;

    if-eqz v0, :cond_2

    .line 1553
    iget-object v0, p0, Lt;->D:Laf;

    invoke-virtual {v0}, Laf;->n()V

    .line 1554
    iget-object v0, p0, Lt;->D:Laf;

    invoke-virtual {v0}, Laf;->h()Z

    .line 1556
    :cond_2
    return-void
.end method

.method public final isAdded()Z
    .locals 1

    .prologue
    .line 687
    iget-object v0, p0, Lt;->C:Ly;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lt;->u:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isDetached()Z
    .locals 1

    .prologue
    .line 696
    iget-boolean v0, p0, Lt;->J:Z

    return v0
.end method

.method public final isHidden()Z
    .locals 1

    .prologue
    .line 745
    iget-boolean v0, p0, Lt;->I:Z

    return v0
.end method

.method public final isInLayout()Z
    .locals 1

    .prologue
    .line 716
    iget-boolean v0, p0, Lt;->y:Z

    return v0
.end method

.method public final isMenuVisible()Z
    .locals 1

    .prologue
    .line 755
    iget-boolean v0, p0, Lt;->N:Z

    return v0
.end method

.method public final isRemoving()Z
    .locals 1

    .prologue
    .line 705
    iget-boolean v0, p0, Lt;->v:Z

    return v0
.end method

.method public final isResumed()Z
    .locals 1

    .prologue
    .line 724
    iget-boolean v0, p0, Lt;->w:Z

    return v0
.end method

.method public final isVisible()Z
    .locals 1

    .prologue
    .line 733
    invoke-virtual {p0}, Lt;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lt;->isHidden()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lt;->R:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lt;->R:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lt;->R:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method k()V
    .locals 1

    .prologue
    .line 1566
    invoke-virtual {p0}, Lt;->onLowMemory()V

    .line 1567
    iget-object v0, p0, Lt;->D:Laf;

    if-eqz v0, :cond_0

    .line 1568
    iget-object v0, p0, Lt;->D:Laf;

    invoke-virtual {v0}, Laf;->t()V

    .line 1570
    :cond_0
    return-void
.end method

.method l()V
    .locals 3

    .prologue
    .line 1661
    iget-object v0, p0, Lt;->D:Laf;

    if-eqz v0, :cond_0

    .line 1662
    iget-object v0, p0, Lt;->D:Laf;

    invoke-virtual {v0}, Laf;->o()V

    .line 1664
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lt;->O:Z

    .line 1665
    invoke-virtual {p0}, Lt;->onPause()V

    .line 1666
    iget-boolean v0, p0, Lt;->O:Z

    if-nez v0, :cond_1

    .line 1667
    new-instance v0, Lct;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onPause()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lct;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1670
    :cond_1
    return-void
.end method

.method m()V
    .locals 3

    .prologue
    .line 1673
    iget-object v0, p0, Lt;->D:Laf;

    if-eqz v0, :cond_0

    .line 1674
    iget-object v0, p0, Lt;->D:Laf;

    invoke-virtual {v0}, Laf;->p()V

    .line 1676
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lt;->O:Z

    .line 1677
    invoke-virtual {p0}, Lt;->onStop()V

    .line 1678
    iget-boolean v0, p0, Lt;->O:Z

    if-nez v0, :cond_1

    .line 1679
    new-instance v0, Lct;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onStop()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lct;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1682
    :cond_1
    return-void
.end method

.method n()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1685
    iget-object v0, p0, Lt;->D:Laf;

    if-eqz v0, :cond_0

    .line 1686
    iget-object v0, p0, Lt;->D:Laf;

    invoke-virtual {v0}, Laf;->q()V

    .line 1688
    :cond_0
    iget-boolean v0, p0, Lt;->W:Z

    if-eqz v0, :cond_2

    .line 1689
    iput-boolean v3, p0, Lt;->W:Z

    .line 1690
    iget-boolean v0, p0, Lt;->X:Z

    if-nez v0, :cond_1

    .line 1691
    const/4 v0, 0x1

    iput-boolean v0, p0, Lt;->X:Z

    .line 1692
    iget-object v0, p0, Lt;->C:Ly;

    iget-object v1, p0, Lt;->p:Ljava/lang/String;

    iget-boolean v2, p0, Lt;->W:Z

    invoke-virtual {v0, v1, v2, v3}, Ly;->a(Ljava/lang/String;ZZ)Lax;

    move-result-object v0

    iput-object v0, p0, Lt;->V:Lax;

    .line 1694
    :cond_1
    iget-object v0, p0, Lt;->V:Lax;

    if-eqz v0, :cond_2

    .line 1695
    iget-object v0, p0, Lt;->C:Ly;

    iget-boolean v0, v0, Ly;->h:Z

    if-nez v0, :cond_3

    .line 1696
    iget-object v0, p0, Lt;->V:Lax;

    invoke-virtual {v0}, Lax;->c()V

    .line 1702
    :cond_2
    :goto_0
    return-void

    .line 1698
    :cond_3
    iget-object v0, p0, Lt;->V:Lax;

    invoke-virtual {v0}, Lax;->d()V

    goto :goto_0
.end method

.method o()V
    .locals 3

    .prologue
    .line 1705
    iget-object v0, p0, Lt;->D:Laf;

    if-eqz v0, :cond_0

    .line 1706
    iget-object v0, p0, Lt;->D:Laf;

    invoke-virtual {v0}, Laf;->r()V

    .line 1708
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lt;->O:Z

    .line 1709
    invoke-virtual {p0}, Lt;->onDestroyView()V

    .line 1710
    iget-boolean v0, p0, Lt;->O:Z

    if-nez v0, :cond_1

    .line 1711
    new-instance v0, Lct;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onDestroyView()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lct;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1714
    :cond_1
    iget-object v0, p0, Lt;->V:Lax;

    if-eqz v0, :cond_2

    .line 1715
    iget-object v0, p0, Lt;->V:Lax;

    invoke-virtual {v0}, Lax;->f()V

    .line 1717
    :cond_2
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1061
    const/4 v0, 0x1

    iput-boolean v0, p0, Lt;->O:Z

    .line 1062
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 909
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 971
    const/4 v0, 0x1

    iput-boolean v0, p0, Lt;->O:Z

    .line 972
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 1132
    const/4 v0, 0x1

    iput-boolean v0, p0, Lt;->O:Z

    .line 1133
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 1364
    const/4 v0, 0x0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 996
    const/4 v0, 0x1

    iput-boolean v0, p0, Lt;->O:Z

    .line 997
    return-void
.end method

.method public onCreateAnimation(IZI)Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 978
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 1

    .prologue
    .line 1318
    invoke-virtual {p0}, Lt;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Ly;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 1319
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 0

    .prologue
    .line 1239
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1020
    const/4 v0, 0x0

    return-object v0
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1175
    iput-boolean v1, p0, Lt;->O:Z

    .line 1178
    iget-boolean v0, p0, Lt;->X:Z

    if-nez v0, :cond_0

    .line 1179
    iput-boolean v1, p0, Lt;->X:Z

    .line 1180
    iget-object v0, p0, Lt;->C:Ly;

    iget-object v1, p0, Lt;->p:Ljava/lang/String;

    iget-boolean v2, p0, Lt;->W:Z

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Ly;->a(Ljava/lang/String;ZZ)Lax;

    move-result-object v0

    iput-object v0, p0, Lt;->V:Lax;

    .line 1182
    :cond_0
    iget-object v0, p0, Lt;->V:Lax;

    if-eqz v0, :cond_1

    .line 1183
    iget-object v0, p0, Lt;->V:Lax;

    invoke-virtual {v0}, Lax;->h()V

    .line 1185
    :cond_1
    return-void
.end method

.method public onDestroyOptionsMenu()V
    .locals 0

    .prologue
    .line 1266
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 1167
    const/4 v0, 0x1

    iput-boolean v0, p0, Lt;->O:Z

    .line 1168
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 1222
    const/4 v0, 0x1

    iput-boolean v0, p0, Lt;->O:Z

    .line 1223
    return-void
.end method

.method public onHiddenChanged(Z)V
    .locals 0

    .prologue
    .line 766
    return-void
.end method

.method public onInflate(Landroid/app/Activity;Landroid/util/AttributeSet;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 963
    const/4 v0, 0x1

    iput-boolean v0, p0, Lt;->O:Z

    .line 964
    return-void
.end method

.method public onLowMemory()V
    .locals 1

    .prologue
    .line 1154
    const/4 v0, 0x1

    iput-boolean v0, p0, Lt;->O:Z

    .line 1155
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 1287
    const/4 v0, 0x0

    return v0
.end method

.method public onOptionsMenuClosed(Landroid/view/Menu;)V
    .locals 0

    .prologue
    .line 1298
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 1141
    const/4 v0, 0x1

    iput-boolean v0, p0, Lt;->O:Z

    .line 1142
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 0

    .prologue
    .line 1256
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 1106
    const/4 v0, 0x1

    iput-boolean v0, p0, Lt;->O:Z

    .line 1107
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1129
    return-void
.end method

.method public onStart()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1085
    iput-boolean v1, p0, Lt;->O:Z

    .line 1087
    iget-boolean v0, p0, Lt;->W:Z

    if-nez v0, :cond_1

    .line 1088
    iput-boolean v1, p0, Lt;->W:Z

    .line 1089
    iget-boolean v0, p0, Lt;->X:Z

    if-nez v0, :cond_0

    .line 1090
    iput-boolean v1, p0, Lt;->X:Z

    .line 1091
    iget-object v0, p0, Lt;->C:Ly;

    iget-object v1, p0, Lt;->p:Ljava/lang/String;

    iget-boolean v2, p0, Lt;->W:Z

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Ly;->a(Ljava/lang/String;ZZ)Lax;

    move-result-object v0

    iput-object v0, p0, Lt;->V:Lax;

    .line 1093
    :cond_0
    iget-object v0, p0, Lt;->V:Lax;

    if-eqz v0, :cond_1

    .line 1094
    iget-object v0, p0, Lt;->V:Lax;

    invoke-virtual {v0}, Lax;->b()V

    .line 1097
    :cond_1
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 1150
    const/4 v0, 0x1

    iput-boolean v0, p0, Lt;->O:Z

    .line 1151
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1034
    return-void
.end method

.method public onViewStateRestored(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1076
    const/4 v0, 0x1

    iput-boolean v0, p0, Lt;->O:Z

    .line 1077
    return-void
.end method

.method p()V
    .locals 3

    .prologue
    .line 1720
    iget-object v0, p0, Lt;->D:Laf;

    if-eqz v0, :cond_0

    .line 1721
    iget-object v0, p0, Lt;->D:Laf;

    invoke-virtual {v0}, Laf;->s()V

    .line 1723
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lt;->O:Z

    .line 1724
    invoke-virtual {p0}, Lt;->onDestroy()V

    .line 1725
    iget-boolean v0, p0, Lt;->O:Z

    if-nez v0, :cond_1

    .line 1726
    new-instance v0, Lct;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onDestroy()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lct;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1729
    :cond_1
    return-void
.end method

.method public registerForContextMenu(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1332
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    .line 1333
    return-void
.end method

.method public setArguments(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 531
    iget v0, p0, Lt;->o:I

    if-ltz v0, :cond_0

    .line 532
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Fragment already active"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 534
    :cond_0
    iput-object p1, p0, Lt;->q:Landroid/os/Bundle;

    .line 535
    return-void
.end method

.method public setHasOptionsMenu(Z)V
    .locals 1

    .prologue
    .line 802
    iget-boolean v0, p0, Lt;->M:Z

    if-eq v0, p1, :cond_0

    .line 803
    iput-boolean p1, p0, Lt;->M:Z

    .line 804
    invoke-virtual {p0}, Lt;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lt;->isHidden()Z

    move-result v0

    if-nez v0, :cond_0

    .line 805
    iget-object v0, p0, Lt;->C:Ly;

    invoke-virtual {v0}, Ly;->u_()V

    .line 808
    :cond_0
    return-void
.end method

.method public setInitialSavedState(Lw;)V
    .locals 2

    .prologue
    .line 554
    iget v0, p0, Lt;->o:I

    if-ltz v0, :cond_0

    .line 555
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Fragment already active"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 557
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p1, Lw;->a:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lw;->a:Landroid/os/Bundle;

    :goto_0
    iput-object v0, p0, Lt;->m:Landroid/os/Bundle;

    .line 559
    return-void

    .line 557
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setMenuVisibility(Z)V
    .locals 1

    .prologue
    .line 820
    iget-boolean v0, p0, Lt;->N:Z

    if-eq v0, p1, :cond_0

    .line 821
    iput-boolean p1, p0, Lt;->N:Z

    .line 822
    iget-boolean v0, p0, Lt;->M:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lt;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lt;->isHidden()Z

    move-result v0

    if-nez v0, :cond_0

    .line 823
    iget-object v0, p0, Lt;->C:Ly;

    invoke-virtual {v0}, Ly;->u_()V

    .line 826
    :cond_0
    return-void
.end method

.method public setRetainInstance(Z)V
    .locals 2

    .prologue
    .line 783
    if-eqz p1, :cond_0

    iget-object v0, p0, Lt;->E:Lt;

    if-eqz v0, :cond_0

    .line 784
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t retain fragements that are nested in other fragments"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 787
    :cond_0
    iput-boolean p1, p0, Lt;->K:Z

    .line 788
    return-void
.end method

.method public setTargetFragment(Lt;I)V
    .locals 0

    .prologue
    .line 573
    iput-object p1, p0, Lt;->r:Lt;

    .line 574
    iput p2, p0, Lt;->t:I

    .line 575
    return-void
.end method

.method public setUserVisibleHint(Z)V
    .locals 2

    .prologue
    .line 842
    iget-boolean v0, p0, Lt;->U:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    iget v0, p0, Lt;->j:I

    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    .line 843
    iget-object v0, p0, Lt;->B:Laf;

    invoke-virtual {v0, p0}, Laf;->a(Lt;)V

    .line 845
    :cond_0
    iput-boolean p1, p0, Lt;->U:Z

    .line 846
    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lt;->T:Z

    .line 847
    return-void

    .line 846
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 877
    iget-object v0, p0, Lt;->C:Ly;

    if-nez v0, :cond_0

    .line 878
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not attached to Activity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 880
    :cond_0
    iget-object v0, p0, Lt;->C:Ly;

    const/4 v1, -0x1

    invoke-virtual {v0, p0, p1, v1}, Ly;->a(Lt;Landroid/content/Intent;I)V

    .line 881
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 3

    .prologue
    .line 888
    iget-object v0, p0, Lt;->C:Ly;

    if-nez v0, :cond_0

    .line 889
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not attached to Activity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 891
    :cond_0
    iget-object v0, p0, Lt;->C:Ly;

    invoke-virtual {v0, p0, p1, p2}, Ly;->a(Lt;Landroid/content/Intent;I)V

    .line 892
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 489
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 490
    invoke-static {p0, v0}, Lf;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    .line 491
    iget v1, p0, Lt;->o:I

    if-ltz v1, :cond_0

    .line 492
    const-string v1, " #"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 493
    iget v1, p0, Lt;->o:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 495
    :cond_0
    iget v1, p0, Lt;->F:I

    if-eqz v1, :cond_1

    .line 496
    const-string v1, " id=0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 497
    iget v1, p0, Lt;->F:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 499
    :cond_1
    iget-object v1, p0, Lt;->H:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 500
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 501
    iget-object v1, p0, Lt;->H:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 503
    :cond_2
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 504
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unregisterForContextMenu(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1343
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    .line 1344
    return-void
.end method
