.class public final Lbjs;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field protected static a:Lbjs;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a([BLjava/lang/Class;Ljava/lang/reflect/Method;)Lbfz;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lbea;",
            ">([B",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/reflect/Method;",
            ")",
            "Lbfz;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 328
    if-eqz p2, :cond_0

    .line 330
    const/4 v0, 0x0

    const/4 v2, 0x1

    :try_start_0
    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {p2, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbfz;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    .line 340
    :goto_0
    return-object v0

    .line 331
    :catch_0
    move-exception v0

    .line 332
    const-string v2, "Babel_RequestWriter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "parseFrom method needs to be public"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 333
    goto :goto_0

    .line 334
    :catch_1
    move-exception v0

    .line 335
    const-string v2, "Babel_RequestWriter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not invoke parseFrom:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 336
    goto :goto_0

    .line 339
    :cond_0
    const-string v0, "Babel_RequestWriter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Received unknown response type for request: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 340
    goto :goto_0
.end method

.method public static a()Lbjs;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lbjs;->a:Lbjs;

    if-nez v0, :cond_0

    .line 62
    new-instance v0, Lbjs;

    invoke-direct {v0}, Lbjs;-><init>()V

    sput-object v0, Lbjs;->a:Lbjs;

    .line 64
    :cond_0
    sget-object v0, Lbjs;->a:Lbjs;

    return-object v0
.end method

.method static a(Ljava/lang/String;Lava;)Lcom/google/api/client/http/HttpRequestInitializer;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 251
    invoke-static {p0}, Lbkb;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 252
    invoke-static {p0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object p0, v0

    .line 255
    :cond_0
    invoke-virtual {p1, v1}, Lava;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 257
    if-eqz v1, :cond_1

    .line 258
    invoke-virtual {p1, v1}, Lava;->c(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    .line 261
    :cond_1
    new-instance v2, Lczi;

    invoke-direct {v2}, Lczi;-><init>()V

    invoke-virtual {v2, v1}, Lczi;->c(Ljava/lang/String;)Lczi;

    new-instance v3, Lbjt;

    invoke-direct {v3, v0, v1, v2, p0}, Lbjt;-><init>(Ljava/lang/Long;Ljava/lang/String;Lczi;Ljava/lang/String;)V

    .line 262
    return-object v3
.end method

.method static a(Lcom/google/api/client/http/HttpResponse;)[B
    .locals 5

    .prologue
    .line 100
    const/4 v1, 0x0

    .line 102
    :try_start_0
    invoke-virtual {p0}, Lcom/google/api/client/http/HttpResponse;->getContent()Ljava/io/InputStream;

    move-result-object v1

    .line 104
    new-instance v2, Ljava/io/BufferedInputStream;

    invoke-direct {v2, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 105
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 108
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->read()I

    move-result v0

    .line 109
    :goto_0
    const/4 v4, -0x1

    if-eq v0, v4, :cond_1

    .line 110
    int-to-byte v0, v0

    .line 111
    invoke-virtual {v3, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 112
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->read()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 114
    :catch_0
    move-exception v0

    .line 115
    :try_start_2
    const-string v2, "Babel_RequestWriter"

    const-string v3, "Error reading response stream"

    invoke-static {v2, v3, v0}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 116
    new-instance v2, Lbph;

    const/16 v3, 0x6a

    invoke-direct {v2, v3, v0}, Lbph;-><init>(ILjava/lang/Exception;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 122
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_0

    .line 124
    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 127
    :cond_0
    :goto_1
    throw v0

    .line 120
    :cond_1
    :try_start_4
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v0

    .line 122
    if-eqz v1, :cond_2

    .line 124
    :try_start_5
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    .line 127
    :cond_2
    :goto_2
    return-object v0

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method
