.class public final Ldxi;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldxi;


# instance fields
.field public b:Ldvn;

.field public c:Ljava/lang/Long;

.field public d:[Ldql;

.field public e:Ljava/lang/Long;

.field public f:Ldtc;

.field public g:[Ldro;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9651
    const/4 v0, 0x0

    new-array v0, v0, [Ldxi;

    sput-object v0, Ldxi;->a:[Ldxi;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 9652
    invoke-direct {p0}, Lepn;-><init>()V

    .line 9655
    iput-object v1, p0, Ldxi;->b:Ldvn;

    .line 9660
    sget-object v0, Ldql;->a:[Ldql;

    iput-object v0, p0, Ldxi;->d:[Ldql;

    .line 9665
    iput-object v1, p0, Ldxi;->f:Ldtc;

    .line 9668
    sget-object v0, Ldro;->a:[Ldro;

    iput-object v0, p0, Ldxi;->g:[Ldro;

    .line 9652
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 9706
    iget-object v0, p0, Ldxi;->b:Ldvn;

    if-eqz v0, :cond_7

    .line 9707
    const/4 v0, 0x1

    iget-object v2, p0, Ldxi;->b:Ldvn;

    .line 9708
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 9710
    :goto_0
    iget-object v2, p0, Ldxi;->c:Ljava/lang/Long;

    if-eqz v2, :cond_0

    .line 9711
    const/4 v2, 0x2

    iget-object v3, p0, Ldxi;->c:Ljava/lang/Long;

    .line 9712
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 9714
    :cond_0
    iget-object v2, p0, Ldxi;->d:[Ldql;

    if-eqz v2, :cond_2

    .line 9715
    iget-object v3, p0, Ldxi;->d:[Ldql;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 9716
    if-eqz v5, :cond_1

    .line 9717
    const/4 v6, 0x3

    .line 9718
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 9715
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 9722
    :cond_2
    iget-object v2, p0, Ldxi;->e:Ljava/lang/Long;

    if-eqz v2, :cond_3

    .line 9723
    const/4 v2, 0x4

    iget-object v3, p0, Ldxi;->e:Ljava/lang/Long;

    .line 9724
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 9726
    :cond_3
    iget-object v2, p0, Ldxi;->f:Ldtc;

    if-eqz v2, :cond_4

    .line 9727
    const/4 v2, 0x5

    iget-object v3, p0, Ldxi;->f:Ldtc;

    .line 9728
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 9730
    :cond_4
    iget-object v2, p0, Ldxi;->g:[Ldro;

    if-eqz v2, :cond_6

    .line 9731
    iget-object v2, p0, Ldxi;->g:[Ldro;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 9732
    if-eqz v4, :cond_5

    .line 9733
    const/4 v5, 0x6

    .line 9734
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 9731
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 9738
    :cond_6
    iget-object v1, p0, Ldxi;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9739
    iput v0, p0, Ldxi;->cachedSize:I

    .line 9740
    return v0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 9648
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldxi;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldxi;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldxi;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldxi;->b:Ldvn;

    if-nez v0, :cond_2

    new-instance v0, Ldvn;

    invoke-direct {v0}, Ldvn;-><init>()V

    iput-object v0, p0, Ldxi;->b:Ldvn;

    :cond_2
    iget-object v0, p0, Ldxi;->b:Ldvn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldxi;->c:Ljava/lang/Long;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldxi;->d:[Ldql;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldql;

    iget-object v3, p0, Ldxi;->d:[Ldql;

    if-eqz v3, :cond_3

    iget-object v3, p0, Ldxi;->d:[Ldql;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Ldxi;->d:[Ldql;

    :goto_2
    iget-object v2, p0, Ldxi;->d:[Ldql;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Ldxi;->d:[Ldql;

    new-instance v3, Ldql;

    invoke-direct {v3}, Ldql;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldxi;->d:[Ldql;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Ldxi;->d:[Ldql;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Ldxi;->d:[Ldql;

    new-instance v3, Ldql;

    invoke-direct {v3}, Ldql;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldxi;->d:[Ldql;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldxi;->e:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Ldxi;->f:Ldtc;

    if-nez v0, :cond_6

    new-instance v0, Ldtc;

    invoke-direct {v0}, Ldtc;-><init>()V

    iput-object v0, p0, Ldxi;->f:Ldtc;

    :cond_6
    iget-object v0, p0, Ldxi;->f:Ldtc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldxi;->g:[Ldro;

    if-nez v0, :cond_8

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ldro;

    iget-object v3, p0, Ldxi;->g:[Ldro;

    if-eqz v3, :cond_7

    iget-object v3, p0, Ldxi;->g:[Ldro;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    iput-object v2, p0, Ldxi;->g:[Ldro;

    :goto_4
    iget-object v2, p0, Ldxi;->g:[Ldro;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    iget-object v2, p0, Ldxi;->g:[Ldro;

    new-instance v3, Ldro;

    invoke-direct {v3}, Ldro;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldxi;->g:[Ldro;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_8
    iget-object v0, p0, Ldxi;->g:[Ldro;

    array-length v0, v0

    goto :goto_3

    :cond_9
    iget-object v2, p0, Ldxi;->g:[Ldro;

    new-instance v3, Ldro;

    invoke-direct {v3}, Ldro;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldxi;->g:[Ldro;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 9673
    iget-object v1, p0, Ldxi;->b:Ldvn;

    if-eqz v1, :cond_0

    .line 9674
    const/4 v1, 0x1

    iget-object v2, p0, Ldxi;->b:Ldvn;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 9676
    :cond_0
    iget-object v1, p0, Ldxi;->c:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 9677
    const/4 v1, 0x2

    iget-object v2, p0, Ldxi;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Lepl;->a(IJ)V

    .line 9679
    :cond_1
    iget-object v1, p0, Ldxi;->d:[Ldql;

    if-eqz v1, :cond_3

    .line 9680
    iget-object v2, p0, Ldxi;->d:[Ldql;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 9681
    if-eqz v4, :cond_2

    .line 9682
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 9680
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 9686
    :cond_3
    iget-object v1, p0, Ldxi;->e:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 9687
    const/4 v1, 0x4

    iget-object v2, p0, Ldxi;->e:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Lepl;->a(IJ)V

    .line 9689
    :cond_4
    iget-object v1, p0, Ldxi;->f:Ldtc;

    if-eqz v1, :cond_5

    .line 9690
    const/4 v1, 0x5

    iget-object v2, p0, Ldxi;->f:Ldtc;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 9692
    :cond_5
    iget-object v1, p0, Ldxi;->g:[Ldro;

    if-eqz v1, :cond_7

    .line 9693
    iget-object v1, p0, Ldxi;->g:[Ldro;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_7

    aget-object v3, v1, v0

    .line 9694
    if-eqz v3, :cond_6

    .line 9695
    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 9693
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 9699
    :cond_7
    iget-object v0, p0, Ldxi;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 9701
    return-void
.end method
