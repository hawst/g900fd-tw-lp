.class public final Ldqu;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldqu;


# instance fields
.field public b:Ldvm;

.field public c:Ljava/lang/Integer;

.field public d:Ldqf;

.field public e:Ljava/lang/Long;

.field public f:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21654
    const/4 v0, 0x0

    new-array v0, v0, [Ldqu;

    sput-object v0, Ldqu;->a:[Ldqu;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 21655
    invoke-direct {p0}, Lepn;-><init>()V

    .line 21658
    iput-object v0, p0, Ldqu;->b:Ldvm;

    .line 21661
    iput-object v0, p0, Ldqu;->c:Ljava/lang/Integer;

    .line 21664
    iput-object v0, p0, Ldqu;->d:Ldqf;

    .line 21669
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Ldqu;->f:[Ljava/lang/String;

    .line 21655
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 21698
    iget-object v0, p0, Ldqu;->b:Ldvm;

    if-eqz v0, :cond_5

    .line 21699
    const/4 v0, 0x1

    iget-object v2, p0, Ldqu;->b:Ldvm;

    .line 21700
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 21702
    :goto_0
    iget-object v2, p0, Ldqu;->d:Ldqf;

    if-eqz v2, :cond_0

    .line 21703
    const/4 v2, 0x2

    iget-object v3, p0, Ldqu;->d:Ldqf;

    .line 21704
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 21706
    :cond_0
    iget-object v2, p0, Ldqu;->e:Ljava/lang/Long;

    if-eqz v2, :cond_1

    .line 21707
    const/4 v2, 0x3

    iget-object v3, p0, Ldqu;->e:Ljava/lang/Long;

    .line 21708
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 21710
    :cond_1
    iget-object v2, p0, Ldqu;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    .line 21711
    const/4 v2, 0x4

    iget-object v3, p0, Ldqu;->c:Ljava/lang/Integer;

    .line 21712
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 21714
    :cond_2
    iget-object v2, p0, Ldqu;->f:[Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Ldqu;->f:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 21716
    iget-object v3, p0, Ldqu;->f:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_3

    aget-object v5, v3, v1

    .line 21718
    invoke-static {v5}, Lepl;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 21716
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 21720
    :cond_3
    add-int/2addr v0, v2

    .line 21721
    iget-object v1, p0, Ldqu;->f:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 21723
    :cond_4
    iget-object v1, p0, Ldqu;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21724
    iput v0, p0, Ldqu;->cachedSize:I

    .line 21725
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 21651
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldqu;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldqu;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldqu;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldqu;->b:Ldvm;

    if-nez v0, :cond_2

    new-instance v0, Ldvm;

    invoke-direct {v0}, Ldvm;-><init>()V

    iput-object v0, p0, Ldqu;->b:Ldvm;

    :cond_2
    iget-object v0, p0, Ldqu;->b:Ldvm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldqu;->d:Ldqf;

    if-nez v0, :cond_3

    new-instance v0, Ldqf;

    invoke-direct {v0}, Ldqf;-><init>()V

    iput-object v0, p0, Ldqu;->d:Ldqf;

    :cond_3
    iget-object v0, p0, Ldqu;->d:Ldqf;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldqu;->e:Ljava/lang/Long;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    :cond_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqu;->c:Ljava/lang/Integer;

    goto :goto_0

    :cond_5
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqu;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v1

    iget-object v0, p0, Ldqu;->f:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    iget-object v2, p0, Ldqu;->f:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Ldqu;->f:[Ljava/lang/String;

    :goto_1
    iget-object v1, p0, Ldqu;->f:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_6

    iget-object v1, p0, Ldqu;->f:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_6
    iget-object v1, p0, Ldqu;->f:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 21674
    iget-object v0, p0, Ldqu;->b:Ldvm;

    if-eqz v0, :cond_0

    .line 21675
    const/4 v0, 0x1

    iget-object v1, p0, Ldqu;->b:Ldvm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 21677
    :cond_0
    iget-object v0, p0, Ldqu;->d:Ldqf;

    if-eqz v0, :cond_1

    .line 21678
    const/4 v0, 0x2

    iget-object v1, p0, Ldqu;->d:Ldqf;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 21680
    :cond_1
    iget-object v0, p0, Ldqu;->e:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 21681
    const/4 v0, 0x3

    iget-object v1, p0, Ldqu;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(IJ)V

    .line 21683
    :cond_2
    iget-object v0, p0, Ldqu;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 21684
    const/4 v0, 0x4

    iget-object v1, p0, Ldqu;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 21686
    :cond_3
    iget-object v0, p0, Ldqu;->f:[Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 21687
    iget-object v1, p0, Ldqu;->f:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 21688
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Lepl;->a(ILjava/lang/String;)V

    .line 21687
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 21691
    :cond_4
    iget-object v0, p0, Ldqu;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 21693
    return-void
.end method
