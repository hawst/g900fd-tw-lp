.class public final Lekr;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lekr;


# instance fields
.field public b:Ljava/lang/Boolean;

.field public c:Lelh;

.field public d:Lelh;

.field public e:Lekt;

.field public f:Leli;

.field public g:Lelj;

.field public h:Lell;

.field public i:Lels;

.field public j:Leks;

.field public k:Leku;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 188
    const/4 v0, 0x0

    new-array v0, v0, [Lekr;

    sput-object v0, Lekr;->a:[Lekr;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 189
    invoke-direct {p0}, Lepn;-><init>()V

    .line 194
    iput-object v0, p0, Lekr;->c:Lelh;

    .line 197
    iput-object v0, p0, Lekr;->d:Lelh;

    .line 200
    iput-object v0, p0, Lekr;->e:Lekt;

    .line 203
    iput-object v0, p0, Lekr;->f:Leli;

    .line 206
    iput-object v0, p0, Lekr;->g:Lelj;

    .line 209
    iput-object v0, p0, Lekr;->h:Lell;

    .line 212
    iput-object v0, p0, Lekr;->i:Lels;

    .line 215
    iput-object v0, p0, Lekr;->j:Leks;

    .line 218
    iput-object v0, p0, Lekr;->k:Leku;

    .line 189
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 259
    const/4 v0, 0x0

    .line 260
    iget-object v1, p0, Lekr;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 261
    const/4 v0, 0x1

    iget-object v1, p0, Lekr;->b:Ljava/lang/Boolean;

    .line 262
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Lepl;->g(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 264
    :cond_0
    iget-object v1, p0, Lekr;->c:Lelh;

    if-eqz v1, :cond_1

    .line 265
    const/4 v1, 0x2

    iget-object v2, p0, Lekr;->c:Lelh;

    .line 266
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 268
    :cond_1
    iget-object v1, p0, Lekr;->d:Lelh;

    if-eqz v1, :cond_2

    .line 269
    const/4 v1, 0x3

    iget-object v2, p0, Lekr;->d:Lelh;

    .line 270
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 272
    :cond_2
    iget-object v1, p0, Lekr;->e:Lekt;

    if-eqz v1, :cond_3

    .line 273
    const/4 v1, 0x4

    iget-object v2, p0, Lekr;->e:Lekt;

    .line 274
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 276
    :cond_3
    iget-object v1, p0, Lekr;->f:Leli;

    if-eqz v1, :cond_4

    .line 277
    const/4 v1, 0x5

    iget-object v2, p0, Lekr;->f:Leli;

    .line 278
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 280
    :cond_4
    iget-object v1, p0, Lekr;->g:Lelj;

    if-eqz v1, :cond_5

    .line 281
    const/4 v1, 0x6

    iget-object v2, p0, Lekr;->g:Lelj;

    .line 282
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 284
    :cond_5
    iget-object v1, p0, Lekr;->h:Lell;

    if-eqz v1, :cond_6

    .line 285
    const/4 v1, 0x7

    iget-object v2, p0, Lekr;->h:Lell;

    .line 286
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 288
    :cond_6
    iget-object v1, p0, Lekr;->i:Lels;

    if-eqz v1, :cond_7

    .line 289
    const/16 v1, 0x8

    iget-object v2, p0, Lekr;->i:Lels;

    .line 290
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 292
    :cond_7
    iget-object v1, p0, Lekr;->j:Leks;

    if-eqz v1, :cond_8

    .line 293
    const/16 v1, 0x9

    iget-object v2, p0, Lekr;->j:Leks;

    .line 294
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 296
    :cond_8
    iget-object v1, p0, Lekr;->k:Leku;

    if-eqz v1, :cond_9

    .line 297
    const/16 v1, 0xa

    iget-object v2, p0, Lekr;->k:Leku;

    .line 298
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 300
    :cond_9
    iget-object v1, p0, Lekr;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 301
    iput v0, p0, Lekr;->cachedSize:I

    .line 302
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 185
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lekr;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lekr;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lekr;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lekr;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lekr;->c:Lelh;

    if-nez v0, :cond_2

    new-instance v0, Lelh;

    invoke-direct {v0}, Lelh;-><init>()V

    iput-object v0, p0, Lekr;->c:Lelh;

    :cond_2
    iget-object v0, p0, Lekr;->c:Lelh;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lekr;->d:Lelh;

    if-nez v0, :cond_3

    new-instance v0, Lelh;

    invoke-direct {v0}, Lelh;-><init>()V

    iput-object v0, p0, Lekr;->d:Lelh;

    :cond_3
    iget-object v0, p0, Lekr;->d:Lelh;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lekr;->e:Lekt;

    if-nez v0, :cond_4

    new-instance v0, Lekt;

    invoke-direct {v0}, Lekt;-><init>()V

    iput-object v0, p0, Lekr;->e:Lekt;

    :cond_4
    iget-object v0, p0, Lekr;->e:Lekt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lekr;->f:Leli;

    if-nez v0, :cond_5

    new-instance v0, Leli;

    invoke-direct {v0}, Leli;-><init>()V

    iput-object v0, p0, Lekr;->f:Leli;

    :cond_5
    iget-object v0, p0, Lekr;->f:Leli;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lekr;->g:Lelj;

    if-nez v0, :cond_6

    new-instance v0, Lelj;

    invoke-direct {v0}, Lelj;-><init>()V

    iput-object v0, p0, Lekr;->g:Lelj;

    :cond_6
    iget-object v0, p0, Lekr;->g:Lelj;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lekr;->h:Lell;

    if-nez v0, :cond_7

    new-instance v0, Lell;

    invoke-direct {v0}, Lell;-><init>()V

    iput-object v0, p0, Lekr;->h:Lell;

    :cond_7
    iget-object v0, p0, Lekr;->h:Lell;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lekr;->i:Lels;

    if-nez v0, :cond_8

    new-instance v0, Lels;

    invoke-direct {v0}, Lels;-><init>()V

    iput-object v0, p0, Lekr;->i:Lels;

    :cond_8
    iget-object v0, p0, Lekr;->i:Lels;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lekr;->j:Leks;

    if-nez v0, :cond_9

    new-instance v0, Leks;

    invoke-direct {v0}, Leks;-><init>()V

    iput-object v0, p0, Lekr;->j:Leks;

    :cond_9
    iget-object v0, p0, Lekr;->j:Leks;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Lekr;->k:Leku;

    if-nez v0, :cond_a

    new-instance v0, Leku;

    invoke-direct {v0}, Leku;-><init>()V

    iput-object v0, p0, Lekr;->k:Leku;

    :cond_a
    iget-object v0, p0, Lekr;->k:Leku;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 223
    iget-object v0, p0, Lekr;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 224
    const/4 v0, 0x1

    iget-object v1, p0, Lekr;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 226
    :cond_0
    iget-object v0, p0, Lekr;->c:Lelh;

    if-eqz v0, :cond_1

    .line 227
    const/4 v0, 0x2

    iget-object v1, p0, Lekr;->c:Lelh;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 229
    :cond_1
    iget-object v0, p0, Lekr;->d:Lelh;

    if-eqz v0, :cond_2

    .line 230
    const/4 v0, 0x3

    iget-object v1, p0, Lekr;->d:Lelh;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 232
    :cond_2
    iget-object v0, p0, Lekr;->e:Lekt;

    if-eqz v0, :cond_3

    .line 233
    const/4 v0, 0x4

    iget-object v1, p0, Lekr;->e:Lekt;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 235
    :cond_3
    iget-object v0, p0, Lekr;->f:Leli;

    if-eqz v0, :cond_4

    .line 236
    const/4 v0, 0x5

    iget-object v1, p0, Lekr;->f:Leli;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 238
    :cond_4
    iget-object v0, p0, Lekr;->g:Lelj;

    if-eqz v0, :cond_5

    .line 239
    const/4 v0, 0x6

    iget-object v1, p0, Lekr;->g:Lelj;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 241
    :cond_5
    iget-object v0, p0, Lekr;->h:Lell;

    if-eqz v0, :cond_6

    .line 242
    const/4 v0, 0x7

    iget-object v1, p0, Lekr;->h:Lell;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 244
    :cond_6
    iget-object v0, p0, Lekr;->i:Lels;

    if-eqz v0, :cond_7

    .line 245
    const/16 v0, 0x8

    iget-object v1, p0, Lekr;->i:Lels;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 247
    :cond_7
    iget-object v0, p0, Lekr;->j:Leks;

    if-eqz v0, :cond_8

    .line 248
    const/16 v0, 0x9

    iget-object v1, p0, Lekr;->j:Leks;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 250
    :cond_8
    iget-object v0, p0, Lekr;->k:Leku;

    if-eqz v0, :cond_9

    .line 251
    const/16 v0, 0xa

    iget-object v1, p0, Lekr;->k:Leku;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 253
    :cond_9
    iget-object v0, p0, Lekr;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 255
    return-void
.end method
