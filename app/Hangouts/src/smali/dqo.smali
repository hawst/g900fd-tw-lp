.class public final Ldqo;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldqo;


# instance fields
.field public b:Ldvn;

.field public c:Ljava/lang/Integer;

.field public d:Ldqa;

.field public e:[Ldtg;

.field public f:Ljava/lang/Boolean;

.field public g:Ljava/lang/Long;

.field public h:Ljava/lang/String;

.field public i:[[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2948
    const/4 v0, 0x0

    new-array v0, v0, [Ldqo;

    sput-object v0, Ldqo;->a:[Ldqo;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2949
    invoke-direct {p0}, Lepn;-><init>()V

    .line 2958
    iput-object v0, p0, Ldqo;->b:Ldvn;

    .line 2961
    iput-object v0, p0, Ldqo;->c:Ljava/lang/Integer;

    .line 2964
    iput-object v0, p0, Ldqo;->d:Ldqa;

    .line 2967
    sget-object v0, Ldtg;->a:[Ldtg;

    iput-object v0, p0, Ldqo;->e:[Ldtg;

    .line 2976
    sget-object v0, Lept;->k:[[B

    iput-object v0, p0, Ldqo;->i:[[B

    .line 2949
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 3018
    iget-object v0, p0, Ldqo;->b:Ldvn;

    if-eqz v0, :cond_9

    .line 3019
    const/4 v0, 0x1

    iget-object v2, p0, Ldqo;->b:Ldvn;

    .line 3020
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3022
    :goto_0
    iget-object v2, p0, Ldqo;->d:Ldqa;

    if-eqz v2, :cond_0

    .line 3023
    const/4 v2, 0x2

    iget-object v3, p0, Ldqo;->d:Ldqa;

    .line 3024
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3026
    :cond_0
    iget-object v2, p0, Ldqo;->e:[Ldtg;

    if-eqz v2, :cond_2

    .line 3027
    iget-object v3, p0, Ldqo;->e:[Ldtg;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 3028
    if-eqz v5, :cond_1

    .line 3029
    const/4 v6, 0x3

    .line 3030
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 3027
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 3034
    :cond_2
    iget-object v2, p0, Ldqo;->g:Ljava/lang/Long;

    if-eqz v2, :cond_3

    .line 3035
    const/4 v2, 0x4

    iget-object v3, p0, Ldqo;->g:Ljava/lang/Long;

    .line 3036
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 3038
    :cond_3
    iget-object v2, p0, Ldqo;->h:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 3039
    const/4 v2, 0x5

    iget-object v3, p0, Ldqo;->h:Ljava/lang/String;

    .line 3040
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3042
    :cond_4
    iget-object v2, p0, Ldqo;->i:[[B

    if-eqz v2, :cond_6

    iget-object v2, p0, Ldqo;->i:[[B

    array-length v2, v2

    if-lez v2, :cond_6

    .line 3044
    iget-object v3, p0, Ldqo;->i:[[B

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v1, v4, :cond_5

    aget-object v5, v3, v1

    .line 3046
    invoke-static {v5}, Lepl;->b([B)I

    move-result v5

    add-int/2addr v2, v5

    .line 3044
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3048
    :cond_5
    add-int/2addr v0, v2

    .line 3049
    iget-object v1, p0, Ldqo;->i:[[B

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3051
    :cond_6
    iget-object v1, p0, Ldqo;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 3052
    const/4 v1, 0x7

    iget-object v2, p0, Ldqo;->f:Ljava/lang/Boolean;

    .line 3053
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3055
    :cond_7
    iget-object v1, p0, Ldqo;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 3056
    const/16 v1, 0x8

    iget-object v2, p0, Ldqo;->c:Ljava/lang/Integer;

    .line 3057
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3059
    :cond_8
    iget-object v1, p0, Ldqo;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3060
    iput v0, p0, Ldqo;->cachedSize:I

    .line 3061
    return v0

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2945
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldqo;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldqo;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldqo;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldqo;->b:Ldvn;

    if-nez v0, :cond_2

    new-instance v0, Ldvn;

    invoke-direct {v0}, Ldvn;-><init>()V

    iput-object v0, p0, Ldqo;->b:Ldvn;

    :cond_2
    iget-object v0, p0, Ldqo;->b:Ldvn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldqo;->d:Ldqa;

    if-nez v0, :cond_3

    new-instance v0, Ldqa;

    invoke-direct {v0}, Ldqa;-><init>()V

    iput-object v0, p0, Ldqo;->d:Ldqa;

    :cond_3
    iget-object v0, p0, Ldqo;->d:Ldqa;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldqo;->e:[Ldtg;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldtg;

    iget-object v3, p0, Ldqo;->e:[Ldtg;

    if-eqz v3, :cond_4

    iget-object v3, p0, Ldqo;->e:[Ldtg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iput-object v2, p0, Ldqo;->e:[Ldtg;

    :goto_2
    iget-object v2, p0, Ldqo;->e:[Ldtg;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Ldqo;->e:[Ldtg;

    new-instance v3, Ldtg;

    invoke-direct {v3}, Ldtg;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldqo;->e:[Ldtg;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Ldqo;->e:[Ldtg;

    array-length v0, v0

    goto :goto_1

    :cond_6
    iget-object v2, p0, Ldqo;->e:[Ldtg;

    new-instance v3, Ldtg;

    invoke-direct {v3}, Ldtg;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldqo;->e:[Ldtg;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldqo;->g:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldqo;->h:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldqo;->i:[[B

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [[B

    iget-object v3, p0, Ldqo;->i:[[B

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Ldqo;->i:[[B

    :goto_3
    iget-object v2, p0, Ldqo;->i:[[B

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Ldqo;->i:[[B

    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_7
    iget-object v2, p0, Ldqo;->i:[[B

    invoke-virtual {p1}, Lepk;->k()[B

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldqo;->f:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_8

    const/4 v2, 0x1

    if-eq v0, v2, :cond_8

    const/4 v2, 0x2

    if-ne v0, v2, :cond_9

    :cond_8
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqo;->c:Ljava/lang/Integer;

    goto/16 :goto_0

    :cond_9
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqo;->c:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2981
    iget-object v1, p0, Ldqo;->b:Ldvn;

    if-eqz v1, :cond_0

    .line 2982
    const/4 v1, 0x1

    iget-object v2, p0, Ldqo;->b:Ldvn;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 2984
    :cond_0
    iget-object v1, p0, Ldqo;->d:Ldqa;

    if-eqz v1, :cond_1

    .line 2985
    const/4 v1, 0x2

    iget-object v2, p0, Ldqo;->d:Ldqa;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 2987
    :cond_1
    iget-object v1, p0, Ldqo;->e:[Ldtg;

    if-eqz v1, :cond_3

    .line 2988
    iget-object v2, p0, Ldqo;->e:[Ldtg;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 2989
    if-eqz v4, :cond_2

    .line 2990
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 2988
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2994
    :cond_3
    iget-object v1, p0, Ldqo;->g:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 2995
    const/4 v1, 0x4

    iget-object v2, p0, Ldqo;->g:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Lepl;->a(IJ)V

    .line 2997
    :cond_4
    iget-object v1, p0, Ldqo;->h:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 2998
    const/4 v1, 0x5

    iget-object v2, p0, Ldqo;->h:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 3000
    :cond_5
    iget-object v1, p0, Ldqo;->i:[[B

    if-eqz v1, :cond_6

    .line 3001
    iget-object v1, p0, Ldqo;->i:[[B

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 3002
    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, Lepl;->a(I[B)V

    .line 3001
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3005
    :cond_6
    iget-object v0, p0, Ldqo;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 3006
    const/4 v0, 0x7

    iget-object v1, p0, Ldqo;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 3008
    :cond_7
    iget-object v0, p0, Ldqo;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 3009
    const/16 v0, 0x8

    iget-object v1, p0, Ldqo;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 3011
    :cond_8
    iget-object v0, p0, Ldqo;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 3013
    return-void
.end method
