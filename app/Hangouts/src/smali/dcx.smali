.class public final Ldcx;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldcx;


# instance fields
.field public b:Ldcv;

.field public c:Ldnx;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 402
    const/4 v0, 0x0

    new-array v0, v0, [Ldcx;

    sput-object v0, Ldcx;->a:[Ldcx;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 403
    invoke-direct {p0}, Lepn;-><init>()V

    .line 406
    iput-object v0, p0, Ldcx;->b:Ldcv;

    .line 409
    iput-object v0, p0, Ldcx;->c:Ldnx;

    .line 403
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 426
    const/4 v0, 0x0

    .line 427
    iget-object v1, p0, Ldcx;->b:Ldcv;

    if-eqz v1, :cond_0

    .line 428
    const/4 v0, 0x1

    iget-object v1, p0, Ldcx;->b:Ldcv;

    .line 429
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 431
    :cond_0
    iget-object v1, p0, Ldcx;->c:Ldnx;

    if-eqz v1, :cond_1

    .line 432
    const/4 v1, 0x2

    iget-object v2, p0, Ldcx;->c:Ldnx;

    .line 433
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 435
    :cond_1
    iget-object v1, p0, Ldcx;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 436
    iput v0, p0, Ldcx;->cachedSize:I

    .line 437
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 399
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldcx;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldcx;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldcx;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldcx;->b:Ldcv;

    if-nez v0, :cond_2

    new-instance v0, Ldcv;

    invoke-direct {v0}, Ldcv;-><init>()V

    iput-object v0, p0, Ldcx;->b:Ldcv;

    :cond_2
    iget-object v0, p0, Ldcx;->b:Ldcv;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldcx;->c:Ldnx;

    if-nez v0, :cond_3

    new-instance v0, Ldnx;

    invoke-direct {v0}, Ldnx;-><init>()V

    iput-object v0, p0, Ldcx;->c:Ldnx;

    :cond_3
    iget-object v0, p0, Ldcx;->c:Ldnx;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 414
    iget-object v0, p0, Ldcx;->b:Ldcv;

    if-eqz v0, :cond_0

    .line 415
    const/4 v0, 0x1

    iget-object v1, p0, Ldcx;->b:Ldcv;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 417
    :cond_0
    iget-object v0, p0, Ldcx;->c:Ldnx;

    if-eqz v0, :cond_1

    .line 418
    const/4 v0, 0x2

    iget-object v1, p0, Ldcx;->c:Ldnx;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 420
    :cond_1
    iget-object v0, p0, Ldcx;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 422
    return-void
.end method
