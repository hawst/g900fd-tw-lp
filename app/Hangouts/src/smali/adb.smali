.class public final Ladb;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcvx;

.field private b:Les;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Les",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private c:Les;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Les",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ladc;

.field private final e:Lbsj;


# direct methods
.method public constructor <init>(Lcvx;Lbsj;Ladc;)V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 39
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    iput-object v0, p0, Ladb;->b:Les;

    .line 40
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    iput-object v0, p0, Ladb;->c:Les;

    .line 71
    iput-object p2, p0, Ladb;->e:Lbsj;

    .line 72
    iput-object p3, p0, Ladb;->d:Ladc;

    .line 73
    iput-object p1, p0, Ladb;->a:Lcvx;

    .line 74
    return-void
.end method

.method private varargs b()Ljava/lang/Void;
    .locals 7

    .prologue
    .line 50
    :try_start_0
    iget-object v0, p0, Ladb;->b:Les;

    invoke-virtual {v0}, Les;->clear()V

    iget-object v0, p0, Ladb;->c:Les;

    invoke-virtual {v0}, Les;->clear()V

    new-instance v3, Landroid/os/Debug$MemoryInfo;

    invoke-direct {v3}, Landroid/os/Debug$MemoryInfo;-><init>()V

    iget-object v0, p0, Ladb;->a:Lcvx;

    if-eqz v0, :cond_0

    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Loading contacts: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Ladb;->a:Lcvx;

    invoke-virtual {v2}, Lcvx;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget-object v0, p0, Ladb;->a:Lcvx;

    invoke-virtual {v0}, Lcvx;->a()I

    move-result v0

    if-ge v2, v0, :cond_0

    rem-int/lit8 v0, v2, 0x64

    if-nez v0, :cond_1

    invoke-static {v3}, Landroid/os/Debug;->getMemoryInfo(Landroid/os/Debug$MemoryInfo;)V

    invoke-virtual {v3}, Landroid/os/Debug$MemoryInfo;->getTotalPss()I

    move-result v0

    const/16 v1, 0x800

    if-ge v0, v1, :cond_1

    const-string v0, "Babel"

    const-string v1, "Low memory! Halting contact load."

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    :cond_0
    :goto_1
    const/4 v0, 0x0

    return-object v0

    .line 50
    :cond_1
    iget-object v0, p0, Ladb;->a:Lcvx;

    invoke-virtual {v0, v2}, Lcvx;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcvw;

    invoke-static {v0}, Laea;->b(Lcvw;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v0}, Lcvw;->d()Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcwg;

    invoke-virtual {p0}, Ladb;->isCancelled()Z

    move-result v5

    if-eqz v5, :cond_3

    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 52
    :catch_0
    move-exception v0

    const-string v0, "Babel"

    const-string v1, "ContactLookupProviderAsyncTask interrupted."

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    invoke-virtual {p0}, Ladb;->a()V

    goto :goto_1

    .line 50
    :cond_3
    :try_start_1
    invoke-interface {v1}, Lcwg;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lbzd;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v5, p0, Ladb;->b:Les;

    invoke-virtual {v5, v1}, Les;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p0, Ladb;->b:Les;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v1, v6}, Les;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_4
    invoke-interface {v0}, Lcvw;->b()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {p0}, Ladb;->isCancelled()Z

    move-result v4

    if-eqz v4, :cond_6

    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0

    :cond_6
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Ladb;->c:Les;

    invoke-virtual {v4, v0}, Les;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    iget-object v4, p0, Ladb;->c:Les;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Les;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    :cond_7
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcvw;
    .locals 3

    .prologue
    .line 120
    iget-object v0, p0, Ladb;->a:Lcvx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ladb;->c:Les;

    invoke-virtual {v0, p1}, Les;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    iget-object v1, p0, Ladb;->a:Lcvx;

    iget-object v0, p0, Ladb;->c:Les;

    .line 122
    invoke-virtual {v0, p1}, Les;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    .line 121
    invoke-virtual {v1, v0}, Lcvx;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcvw;

    .line 124
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x0

    iput-object v0, p0, Ladb;->a:Lcvx;

    .line 140
    iget-object v0, p0, Ladb;->b:Les;

    invoke-virtual {v0}, Les;->clear()V

    .line 141
    iget-object v0, p0, Ladb;->c:Les;

    invoke-virtual {v0}, Les;->clear()V

    .line 142
    return-void
.end method

.method public b(Ljava/lang/String;)Lcvw;
    .locals 3

    .prologue
    .line 128
    invoke-static {p1}, Lbzd;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 129
    iget-object v1, p0, Ladb;->a:Lcvx;

    if-eqz v1, :cond_0

    iget-object v1, p0, Ladb;->b:Les;

    invoke-virtual {v1, v0}, Les;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 130
    iget-object v1, p0, Ladb;->a:Lcvx;

    iget-object v2, p0, Ladb;->b:Les;

    .line 131
    invoke-virtual {v2, v0}, Les;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    .line 130
    invoke-virtual {v1, v0}, Lcvx;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcvw;

    .line 133
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ladb;->b()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 27
    invoke-virtual {p0}, Ladb;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ladb;->d:Ladc;

    iget-object v1, p0, Ladb;->e:Lbsj;

    invoke-interface {v0, v1, p0}, Ladc;->a(Lbsj;Ladb;)V

    :cond_0
    return-void
.end method
