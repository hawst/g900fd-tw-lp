.class public final Ldxs;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldxs;


# instance fields
.field public b:Ldvn;

.field public c:[Ldxp;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23911
    const/4 v0, 0x0

    new-array v0, v0, [Ldxs;

    sput-object v0, Ldxs;->a:[Ldxs;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23912
    invoke-direct {p0}, Lepn;-><init>()V

    .line 23915
    const/4 v0, 0x0

    iput-object v0, p0, Ldxs;->b:Ldvn;

    .line 23918
    sget-object v0, Ldxp;->a:[Ldxp;

    iput-object v0, p0, Ldxs;->c:[Ldxp;

    .line 23912
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 23940
    iget-object v0, p0, Ldxs;->b:Ldvn;

    if-eqz v0, :cond_2

    .line 23941
    const/4 v0, 0x1

    iget-object v2, p0, Ldxs;->b:Ldvn;

    .line 23942
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 23944
    :goto_0
    iget-object v2, p0, Ldxs;->c:[Ldxp;

    if-eqz v2, :cond_1

    .line 23945
    iget-object v2, p0, Ldxs;->c:[Ldxp;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 23946
    if-eqz v4, :cond_0

    .line 23947
    const/4 v5, 0x2

    .line 23948
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 23945
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 23952
    :cond_1
    iget-object v1, p0, Ldxs;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 23953
    iput v0, p0, Ldxs;->cachedSize:I

    .line 23954
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 23908
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldxs;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldxs;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldxs;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldxs;->b:Ldvn;

    if-nez v0, :cond_2

    new-instance v0, Ldvn;

    invoke-direct {v0}, Ldvn;-><init>()V

    iput-object v0, p0, Ldxs;->b:Ldvn;

    :cond_2
    iget-object v0, p0, Ldxs;->b:Ldvn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldxs;->c:[Ldxp;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldxp;

    iget-object v3, p0, Ldxs;->c:[Ldxp;

    if-eqz v3, :cond_3

    iget-object v3, p0, Ldxs;->c:[Ldxp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Ldxs;->c:[Ldxp;

    :goto_2
    iget-object v2, p0, Ldxs;->c:[Ldxp;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Ldxs;->c:[Ldxp;

    new-instance v3, Ldxp;

    invoke-direct {v3}, Ldxp;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldxs;->c:[Ldxp;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Ldxs;->c:[Ldxp;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Ldxs;->c:[Ldxp;

    new-instance v3, Ldxp;

    invoke-direct {v3}, Ldxp;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldxs;->c:[Ldxp;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 23923
    iget-object v0, p0, Ldxs;->b:Ldvn;

    if-eqz v0, :cond_0

    .line 23924
    const/4 v0, 0x1

    iget-object v1, p0, Ldxs;->b:Ldvn;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 23926
    :cond_0
    iget-object v0, p0, Ldxs;->c:[Ldxp;

    if-eqz v0, :cond_2

    .line 23927
    iget-object v1, p0, Ldxs;->c:[Ldxp;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 23928
    if-eqz v3, :cond_1

    .line 23929
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 23927
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 23933
    :cond_2
    iget-object v0, p0, Ldxs;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 23935
    return-void
.end method
