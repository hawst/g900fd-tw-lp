.class public final Lsr;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<M:",
        "Lcnx",
        "<TM;>;T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static c:Lsr;


# instance fields
.field final a:Ljava/lang/String;

.field final b:I

.field public final d:Lapk;

.field public final e:Lasj;

.field public final f:Landroid/content/Intent;

.field public final g:Landroid/content/Context;

.field public h:Landroid/app/AlarmManager;

.field public final i:Ljava/util/Random;

.field public j:I

.field public k:I

.field public final l:Ljava/lang/String;

.field public volatile m:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "T",
            "L;"
        }
    .end annotation
.end field

.field public final n:I

.field public final o:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final p:I

.field public final q:Z


# direct methods
.method public static a(Landroid/content/Intent;)Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 191
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    .line 192
    new-instance v1, Landroid/content/Intent;

    const-string v2, "start_next_hangout"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 193
    new-instance v2, Landroid/content/ComponentName;

    const-class v3, Lcom/google/android/apps/hangouts/hangout/StressMode$StressReceiver;

    invoke-direct {v2, v0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 194
    if-eqz p0, :cond_0

    .line 195
    const-string v2, "hangout_intent"

    invoke-virtual {v1, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 198
    :cond_0
    const/16 v2, 0x71

    .line 200
    invoke-static {v2}, Lbzb;->a(I)I

    move-result v2

    const/high16 v3, 0x8000000

    .line 198
    invoke-static {v0, v2, v1, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 205
    return-object v0
.end method

.method private b(Ljava/lang/Object;)I
    .locals 3

    iget v0, p0, Lsr;->p:I

    invoke-static {v0}, Lcod;->a(I)I

    move-result v0

    iget v1, p0, Lsr;->n:I

    packed-switch v1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lsr;->n:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    check-cast p1, Lcob;

    invoke-static {v0, p1}, Lcnw;->b(ILcob;)I

    move-result v0

    :goto_0
    return v0

    :pswitch_1
    check-cast p1, Lcob;

    invoke-static {v0, p1}, Lcnw;->c(ILcob;)I

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private b(Ljava/lang/Object;Lcnw;)V
    .locals 3

    :try_start_0
    iget v0, p0, Lsr;->p:I

    invoke-virtual {p2, v0}, Lcnw;->c(I)V

    iget v0, p0, Lsr;->n:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lsr;->n:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :pswitch_0
    :try_start_1
    check-cast p1, Lcob;

    iget v0, p0, Lsr;->p:I

    invoke-static {v0}, Lcod;->a(I)I

    move-result v0

    invoke-virtual {p2, p1}, Lcnw;->a(Lcob;)V

    const/4 v1, 0x4

    invoke-virtual {p2, v0, v1}, Lcnw;->c(II)V

    :goto_0
    return-void

    :pswitch_1
    check-cast p1, Lcob;

    invoke-virtual {p2, p1}, Lcnw;->b(Lcob;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private f()V
    .locals 7

    .prologue
    const/4 v3, 0x1

    .line 260
    iget-object v0, p0, Lsr;->f:Landroid/content/Intent;

    const-string v1, "hangout_room_info"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    .line 263
    iget-object v1, p0, Lsr;->f:Landroid/content/Intent;

    const-string v2, "hangout_invitee_users"

    .line 264
    invoke-virtual {v1, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 266
    iget-object v2, p0, Lsr;->f:Landroid/content/Intent;

    const-string v4, "hangout_invitee_circles"

    .line 267
    invoke-virtual {v2, v4}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    .line 269
    const/16 v4, 0x33

    .line 271
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    .line 269
    invoke-static/range {v0 .. v6}, Lbbl;->a(Lcom/google/android/libraries/hangouts/video/HangoutRequest;Ljava/util/ArrayList;Ljava/util/ArrayList;ZIJ)Landroid/content/Intent;

    move-result-object v0

    .line 272
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 273
    iget-object v1, p0, Lsr;->g:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 275
    iput v3, p0, Lsr;->j:I

    .line 276
    invoke-virtual {p0}, Lsr;->a()V

    .line 277
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)I
    .locals 4

    const/4 v0, 0x0

    iget-boolean v1, p0, Lsr;->q:Z

    if-eqz v1, :cond_1

    invoke-static {p1}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    invoke-static {p1, v1}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {p1, v1}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v3

    invoke-direct {p0, v3}, Lsr;->b(Ljava/lang/Object;)I

    move-result v3

    add-int/2addr v0, v3

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1}, Lsr;->b(Ljava/lang/Object;)I

    move-result v0

    :cond_2
    return v0
.end method

.method public a()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    .line 209
    sget-object v0, Lsr;->c:Lsr;

    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 213
    iget-object v0, p0, Lsr;->i:Ljava/util/Random;

    .line 214
    invoke-virtual {v0}, Ljava/util/Random;->nextGaussian()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    const-wide v2, 0x40c3880000000000L    # 10000.0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    const v1, 0xea60

    .line 213
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 216
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_0

    .line 217
    iget-object v1, p0, Lsr;->h:Landroid/app/AlarmManager;

    .line 218
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    int-to-long v4, v0

    add-long/2addr v2, v4

    iget-object v4, p0, Lsr;->f:Landroid/content/Intent;

    .line 219
    invoke-static {v4}, Lsr;->a(Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v4

    .line 217
    invoke-virtual {v1, v6, v2, v3, v4}, Landroid/app/AlarmManager;->setExact(IJLandroid/app/PendingIntent;)V

    .line 225
    :goto_0
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "State change from "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lsr;->j:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    int-to-float v0, v0

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v0, v3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "s"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    return-void

    .line 221
    :cond_0
    iget-object v1, p0, Lsr;->h:Landroid/app/AlarmManager;

    .line 222
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    int-to-long v4, v0

    add-long/2addr v2, v4

    iget-object v4, p0, Lsr;->f:Landroid/content/Intent;

    .line 223
    invoke-static {v4}, Lsr;->a(Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v4

    .line 221
    invoke-virtual {v1, v6, v2, v3, v4}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;Lcnw;)V
    .locals 3

    iget-boolean v0, p0, Lsr;->q:Z

    if-eqz v0, :cond_1

    invoke-static {p1}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_2

    invoke-static {p1, v0}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-direct {p0, v2, p2}, Lsr;->b(Ljava/lang/Object;Lcnw;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1, p2}, Lsr;->b(Ljava/lang/Object;Lcnw;)V

    :cond_2
    return-void
.end method

.method public b()V
    .locals 4

    .prologue
    .line 230
    sget-object v0, Lsr;->c:Lsr;

    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 233
    iget v0, p0, Lsr;->j:I

    packed-switch v0, :pswitch_data_0

    .line 257
    :goto_0
    return-void

    .line 235
    :pswitch_0
    invoke-direct {p0}, Lsr;->f()V

    goto :goto_0

    .line 238
    :pswitch_1
    iget-object v0, p0, Lsr;->i:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextFloat()F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x3fd3333333333333L    # 0.3

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    .line 239
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "android.intent.category.HOME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lsr;->g:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    const/4 v0, 0x2

    iput v0, p0, Lsr;->j:I

    invoke-virtual {p0}, Lsr;->a()V

    goto :goto_0

    .line 241
    :cond_0
    iget-object v0, p0, Lsr;->d:Lapk;

    invoke-virtual {v0}, Lapk;->c()Lapx;

    move-result-object v0

    if-nez v0, :cond_1

    .line 243
    invoke-virtual {p0}, Lsr;->a()V

    goto :goto_0

    .line 245
    :cond_1
    invoke-virtual {p0}, Lsr;->c()V

    goto :goto_0

    .line 250
    :pswitch_2
    iget-object v0, p0, Lsr;->i:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextFloat()F

    move-result v0

    float-to-double v0, v0

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    cmpg-double v0, v0, v2

    if-gez v0, :cond_2

    .line 251
    invoke-direct {p0}, Lsr;->f()V

    goto :goto_0

    .line 253
    :cond_2
    invoke-virtual {p0}, Lsr;->c()V

    goto :goto_0

    .line 233
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public c()V
    .locals 2

    .prologue
    .line 290
    iget-object v0, p0, Lsr;->d:Lapk;

    invoke-virtual {v0}, Lapk;->c()Lapx;

    move-result-object v0

    .line 291
    if-eqz v0, :cond_0

    .line 292
    const/16 v1, 0x3ec

    invoke-virtual {v0, v1}, Lapx;->c(I)V

    .line 294
    :cond_0
    return-void
.end method

.method public d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lsr;->l:Ljava/lang/String;

    return-object v0
.end method

.method public e()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lsr;->m:Ljava/lang/Object;

    return-void
.end method
