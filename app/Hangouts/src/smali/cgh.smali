.class public abstract Lcgh;
.super Ljava/lang/Object;

# interfaces
.implements Lcgk;
.implements Lcgo;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R::",
        "Lcgq;",
        ">",
        "Ljava/lang/Object;",
        "Lcgk",
        "<TR;>;",
        "Lcgo",
        "<TR;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/Object;

.field protected b:Lcgi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcgi",
            "<TR;>;"
        }
    .end annotation
.end field

.field public c:Lcig;

.field private final d:Ljava/util/concurrent/CountDownLatch;

.field private final e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "La;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcgr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcgr",
            "<TR;>;"
        }
    .end annotation
.end field

.field private volatile g:Lcgq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TR;"
        }
    .end annotation
.end field

.field private volatile h:Z

.field private i:Z

.field private j:Z


# direct methods
.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcgh;->a:Ljava/lang/Object;

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcgh;->d:Ljava/util/concurrent/CountDownLatch;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcgh;->e:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic a(Lcgh;)V
    .locals 2

    iget-object v1, p0, Lcgh;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Lcgh;->e()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/common/api/Status;->d:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, v0}, Lcgh;->a(Lcom/google/android/gms/common/api/Status;)Lcgq;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcgh;->a(Lcgq;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcgh;->j:Z

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private b(Lcgq;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    iput-object p1, p0, Lcgh;->g:Lcgq;

    const/4 v0, 0x0

    iput-object v0, p0, Lcgh;->c:Lcig;

    iget-object v0, p0, Lcgh;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    iget-object v0, p0, Lcgh;->g:Lcgq;

    invoke-interface {v0}, Lcgq;->c()Lcom/google/android/gms/common/api/Status;

    iget-object v0, p0, Lcgh;->f:Lcgr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcgh;->b:Lcgi;

    invoke-virtual {v0}, Lcgi;->a()V

    iget-boolean v0, p0, Lcgh;->i:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcgh;->b:Lcgi;

    iget-object v1, p0, Lcgh;->f:Lcgr;

    invoke-direct {p0}, Lcgh;->f()Lcgq;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcgi;->a(Lcgr;Lcgq;)V

    :cond_0
    iget-object v0, p0, Lcgh;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcgh;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method private e()Z
    .locals 4

    iget-object v0, p0, Lcgh;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()Lcgq;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TR;"
        }
    .end annotation

    iget-object v1, p0, Lcgh;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcgh;->h:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Result has already been consumed."

    invoke-static {v0, v2}, Lg;->a(ZLjava/lang/Object;)V

    invoke-direct {p0}, Lcgh;->e()Z

    move-result v0

    const-string v2, "Result is not ready."

    invoke-static {v0, v2}, Lg;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcgh;->g:Lcgq;

    invoke-virtual {p0}, Lcgh;->c()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public abstract a(Lcom/google/android/gms/common/api/Status;)Lcgq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/api/Status;",
            ")TR;"
        }
    .end annotation
.end method

.method public a()V
    .locals 2

    iget-object v1, p0, Lcgh;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcgh;->i:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcgh;->h:Z

    if-eqz v0, :cond_1

    :cond_0
    monitor-exit v1

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcgh;->c:Lcig;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    :try_start_1
    iget-object v0, p0, Lcgh;->c:Lcig;

    invoke-interface {v0}, Lcig;->a()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    :goto_1
    :try_start_2
    iget-object v0, p0, Lcgh;->g:Lcgq;

    invoke-static {v0}, Lf;->a(Lcgq;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcgh;->f:Lcgr;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcgh;->i:Z

    sget-object v0, Lcom/google/android/gms/common/api/Status;->e:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, v0}, Lcgh;->a(Lcom/google/android/gms/common/api/Status;)Lcgq;

    move-result-object v0

    invoke-direct {p0, v0}, Lcgh;->b(Lcgq;)V

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method protected a(Lcgi;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcgi",
            "<TR;>;)V"
        }
    .end annotation

    iput-object p1, p0, Lcgh;->b:Lcgi;

    return-void
.end method

.method public final a(Lcgq;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v3, p0, Lcgh;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-boolean v2, p0, Lcgh;->j:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcgh;->i:Z

    if-eqz v2, :cond_1

    :cond_0
    invoke-static {p1}, Lf;->a(Lcgq;)V

    monitor-exit v3

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcgh;->e()Z

    move-result v2

    if-nez v2, :cond_2

    move v2, v0

    :goto_1
    const-string v4, "Results have already been set"

    invoke-static {v2, v4}, Lg;->a(ZLjava/lang/Object;)V

    iget-boolean v2, p0, Lcgh;->h:Z

    if-nez v2, :cond_3

    :goto_2
    const-string v1, "Result has already been consumed"

    invoke-static {v0, v1}, Lg;->a(ZLjava/lang/Object;)V

    invoke-direct {p0, p1}, Lcgh;->b(Lcgq;)V

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_2
    move v2, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final a(Lcgr;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcgr",
            "<TR;>;)V"
        }
    .end annotation

    iget-boolean v0, p0, Lcgh;->h:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Result has already been consumed."

    invoke-static {v0, v1}, Lg;->a(ZLjava/lang/Object;)V

    iget-object v1, p0, Lcgh;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lcgh;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    monitor-exit v1

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcgh;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcgh;->b:Lcgi;

    invoke-direct {p0}, Lcgh;->f()Lcgq;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, Lcgi;->a(Lcgr;Lcgq;)V

    :goto_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    :try_start_1
    iput-object p1, p0, Lcgh;->f:Lcgr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method

.method public synthetic a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcgq;

    invoke-virtual {p0, p1}, Lcgh;->a(Lcgq;)V

    return-void
.end method

.method public b()Z
    .locals 2

    iget-object v1, p0, Lcgh;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcgh;->i:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected c()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcgh;->h:Z

    iput-object v1, p0, Lcgh;->g:Lcgq;

    iput-object v1, p0, Lcgh;->f:Lcgr;

    return-void
.end method
