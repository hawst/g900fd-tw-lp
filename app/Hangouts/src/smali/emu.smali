.class public final Lemu;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lemu;


# instance fields
.field public b:Leof;

.field public c:Lemc;

.field public d:Ljava/lang/Float;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1098
    const/4 v0, 0x0

    new-array v0, v0, [Lemu;

    sput-object v0, Lemu;->a:[Lemu;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1099
    invoke-direct {p0}, Lepn;-><init>()V

    .line 1102
    iput-object v0, p0, Lemu;->b:Leof;

    .line 1105
    iput-object v0, p0, Lemu;->c:Lemc;

    .line 1099
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1127
    const/4 v0, 0x0

    .line 1128
    iget-object v1, p0, Lemu;->b:Leof;

    if-eqz v1, :cond_0

    .line 1129
    const/4 v0, 0x1

    iget-object v1, p0, Lemu;->b:Leof;

    .line 1130
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1132
    :cond_0
    iget-object v1, p0, Lemu;->c:Lemc;

    if-eqz v1, :cond_1

    .line 1133
    const/4 v1, 0x2

    iget-object v2, p0, Lemu;->c:Lemc;

    .line 1134
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1136
    :cond_1
    iget-object v1, p0, Lemu;->d:Ljava/lang/Float;

    if-eqz v1, :cond_2

    .line 1137
    const/4 v1, 0x3

    iget-object v2, p0, Lemu;->d:Ljava/lang/Float;

    .line 1138
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 1140
    :cond_2
    iget-object v1, p0, Lemu;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1141
    iput v0, p0, Lemu;->cachedSize:I

    .line 1142
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 1095
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lemu;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lemu;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lemu;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lemu;->b:Leof;

    if-nez v0, :cond_2

    new-instance v0, Leof;

    invoke-direct {v0}, Leof;-><init>()V

    iput-object v0, p0, Lemu;->b:Leof;

    :cond_2
    iget-object v0, p0, Lemu;->b:Leof;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lemu;->c:Lemc;

    if-nez v0, :cond_3

    new-instance v0, Lemc;

    invoke-direct {v0}, Lemc;-><init>()V

    iput-object v0, p0, Lemu;->c:Lemc;

    :cond_3
    iget-object v0, p0, Lemu;->c:Lemc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lemu;->d:Ljava/lang/Float;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1d -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 1112
    iget-object v0, p0, Lemu;->b:Leof;

    if-eqz v0, :cond_0

    .line 1113
    const/4 v0, 0x1

    iget-object v1, p0, Lemu;->b:Leof;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1115
    :cond_0
    iget-object v0, p0, Lemu;->c:Lemc;

    if-eqz v0, :cond_1

    .line 1116
    const/4 v0, 0x2

    iget-object v1, p0, Lemu;->c:Lemc;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1118
    :cond_1
    iget-object v0, p0, Lemu;->d:Ljava/lang/Float;

    if-eqz v0, :cond_2

    .line 1119
    const/4 v0, 0x3

    iget-object v1, p0, Lemu;->d:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IF)V

    .line 1121
    :cond_2
    iget-object v0, p0, Lemu;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1123
    return-void
.end method
