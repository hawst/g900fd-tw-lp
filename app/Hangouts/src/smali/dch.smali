.class public final Ldch;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldch;


# instance fields
.field public b:Ljava/lang/Long;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 453
    const/4 v0, 0x0

    new-array v0, v0, [Ldch;

    sput-object v0, Ldch;->a:[Ldch;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 454
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 480
    const/4 v0, 0x0

    .line 481
    iget-object v1, p0, Ldch;->b:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 482
    const/4 v0, 0x1

    iget-object v1, p0, Ldch;->b:Ljava/lang/Long;

    .line 483
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lepl;->e(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 485
    :cond_0
    iget-object v1, p0, Ldch;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 486
    const/4 v1, 0x2

    iget-object v2, p0, Ldch;->c:Ljava/lang/String;

    .line 487
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 489
    :cond_1
    iget-object v1, p0, Ldch;->d:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 490
    const/4 v1, 0x3

    iget-object v2, p0, Ldch;->d:Ljava/lang/Long;

    .line 491
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 493
    :cond_2
    iget-object v1, p0, Ldch;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 494
    iput v0, p0, Ldch;->cachedSize:I

    .line 495
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 450
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldch;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldch;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldch;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldch;->b:Ljava/lang/Long;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldch;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldch;->d:Ljava/lang/Long;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 465
    iget-object v0, p0, Ldch;->b:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 466
    const/4 v0, 0x1

    iget-object v1, p0, Ldch;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 468
    :cond_0
    iget-object v0, p0, Ldch;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 469
    const/4 v0, 0x2

    iget-object v1, p0, Ldch;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 471
    :cond_1
    iget-object v0, p0, Ldch;->d:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 472
    const/4 v0, 0x3

    iget-object v1, p0, Ldch;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 474
    :cond_2
    iget-object v0, p0, Ldch;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 476
    return-void
.end method
