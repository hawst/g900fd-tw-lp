.class public Lbxu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcxf;


# static fields
.field protected static final b:Z

.field private static final e:Lcxe;


# instance fields
.field protected final a:Landroid/content/res/Resources;

.field protected final c:Ljava/lang/String;

.field public d:Lex;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lex",
            "<",
            "Landroid/graphics/Bitmap;",
            "Ljava/lang/Exception;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcxe;->a:Lcxe;

    sput-object v0, Lbxu;->e:Lcxe;

    .line 28
    sget-object v0, Lbys;->r:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lbxu;->b:Z

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lbxu;->a:Landroid/content/res/Resources;

    .line 59
    iput-object p1, p0, Lbxu;->c:Ljava/lang/String;

    .line 60
    sget-boolean v0, Lbxu;->b:Z

    if-eqz v0, :cond_0

    .line 61
    new-instance v0, Lex;

    const/16 v1, 0x1e

    invoke-direct {v0, v1}, Lex;-><init>(I)V

    iput-object v0, p0, Lbxu;->d:Lex;

    .line 63
    :cond_0
    return-void
.end method

.method public static a(ZII)Landroid/graphics/BitmapFactory$Options;
    .locals 2

    .prologue
    .line 94
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 95
    invoke-static {p0, p1, p2}, Lbxw;->b(ZII)Landroid/graphics/BitmapFactory$Options;

    move-result-object v0

    .line 97
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-boolean p0, v0, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    iput p1, v0, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    iput p2, v0, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    const/4 v1, 0x1

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    goto :goto_0
.end method

.method public static a(IIIILjava/lang/String;)Lbxu;
    .locals 6

    .prologue
    .line 47
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 48
    new-instance v0, Lbxw;

    move v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lbxw;-><init>(IIIILjava/lang/String;)V

    .line 53
    :goto_0
    sget-object v1, Lbxu;->e:Lcxe;

    invoke-virtual {v1, v0}, Lcxe;->a(Lcxf;)V

    .line 54
    return-object v0

    .line 51
    :cond_0
    new-instance v0, Lbxu;

    invoke-direct {v0, p4}, Lbxu;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public C_()V
    .locals 0

    .prologue
    .line 67
    invoke-virtual {p0}, Lbxu;->b()V

    .line 68
    return-void
.end method

.method public a(IILbxv;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 146
    if-eqz p3, :cond_0

    .line 147
    invoke-interface {p3, p1, p2}, Lbxv;->a(II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 149
    if-eqz v0, :cond_0

    .line 150
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 154
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public a(ILandroid/graphics/BitmapFactory$Options;II)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 112
    const/4 v0, 0x0

    .line 114
    :try_start_0
    iget-object v1, p0, Lbxu;->a:Landroid/content/res/Resources;

    invoke-static {v1, p1, p2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 121
    :goto_0
    return-object v0

    .line 116
    :catch_0
    move-exception v1

    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Decoding resource "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " failed."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 118
    :catch_1
    move-exception v1

    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Oom decoding resource "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    sget-object v1, Lbxu;->e:Lcxe;

    invoke-virtual {v1}, Lcxe;->a()V

    goto :goto_0
.end method

.method public a([BLandroid/graphics/BitmapFactory$Options;IILbxv;)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 135
    const/4 v0, 0x0

    .line 137
    const/4 v1, 0x0

    :try_start_0
    array-length v2, p1

    invoke-static {p1, v1, v2, p2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 141
    :goto_0
    return-object v0

    .line 139
    :catch_0
    move-exception v1

    const-string v1, "Babel"

    const-string v2, "Decoding byte array failed."

    invoke-static {v1, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 72
    return-void
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 163
    invoke-static {p1}, Lyn;->a(Landroid/graphics/Bitmap;)Z

    move-result v0

    invoke-static {v0}, Lcwz;->b(Z)V

    .line 166
    if-eqz p1, :cond_0

    .line 167
    invoke-virtual {p0, p1}, Lbxu;->c(Landroid/graphics/Bitmap;)V

    .line 171
    :goto_0
    return-void

    .line 169
    :cond_0
    const-string v0, "Babel"

    const-string v1, "BitmapPool receiving null bitmap"

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-static {v0, v1, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public a(II)Z
    .locals 1

    .prologue
    .line 186
    const/4 v0, 0x1

    return v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 179
    return-void
.end method

.method b(Landroid/graphics/Bitmap;)Z
    .locals 1

    .prologue
    .line 190
    const/4 v0, 0x0

    return v0
.end method

.method protected final c(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 194
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 195
    sget-boolean v0, Lbxu;->b:Z

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lbxu;->d:Lex;

    new-instance v1, Ljava/lang/Exception;

    invoke-direct {v1}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v0, p1, v1}, Lex;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    :cond_0
    return-void
.end method
