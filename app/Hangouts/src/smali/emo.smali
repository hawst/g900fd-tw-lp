.class public final Lemo;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lemo;


# instance fields
.field public b:Lenw;

.field public c:Lenx;

.field public d:Lenu;

.field public e:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1803
    const/4 v0, 0x0

    new-array v0, v0, [Lemo;

    sput-object v0, Lemo;->a:[Lemo;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1804
    invoke-direct {p0}, Lepn;-><init>()V

    .line 1807
    iput-object v0, p0, Lemo;->b:Lenw;

    .line 1810
    iput-object v0, p0, Lemo;->c:Lenx;

    .line 1813
    iput-object v0, p0, Lemo;->d:Lenu;

    .line 1804
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1838
    const/4 v0, 0x0

    .line 1839
    iget-object v1, p0, Lemo;->b:Lenw;

    if-eqz v1, :cond_0

    .line 1840
    const/4 v0, 0x1

    iget-object v1, p0, Lemo;->b:Lenw;

    .line 1841
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1843
    :cond_0
    iget-object v1, p0, Lemo;->c:Lenx;

    if-eqz v1, :cond_1

    .line 1844
    const/4 v1, 0x2

    iget-object v2, p0, Lemo;->c:Lenx;

    .line 1845
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1847
    :cond_1
    iget-object v1, p0, Lemo;->d:Lenu;

    if-eqz v1, :cond_2

    .line 1848
    const/4 v1, 0x3

    iget-object v2, p0, Lemo;->d:Lenu;

    .line 1849
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1851
    :cond_2
    iget-object v1, p0, Lemo;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 1852
    const/4 v1, 0x4

    iget-object v2, p0, Lemo;->e:Ljava/lang/Boolean;

    .line 1853
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1855
    :cond_3
    iget-object v1, p0, Lemo;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1856
    iput v0, p0, Lemo;->cachedSize:I

    .line 1857
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 1800
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lemo;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lemo;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lemo;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lemo;->b:Lenw;

    if-nez v0, :cond_2

    new-instance v0, Lenw;

    invoke-direct {v0}, Lenw;-><init>()V

    iput-object v0, p0, Lemo;->b:Lenw;

    :cond_2
    iget-object v0, p0, Lemo;->b:Lenw;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lemo;->c:Lenx;

    if-nez v0, :cond_3

    new-instance v0, Lenx;

    invoke-direct {v0}, Lenx;-><init>()V

    iput-object v0, p0, Lemo;->c:Lenx;

    :cond_3
    iget-object v0, p0, Lemo;->c:Lenx;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lemo;->d:Lenu;

    if-nez v0, :cond_4

    new-instance v0, Lenu;

    invoke-direct {v0}, Lenu;-><init>()V

    iput-object v0, p0, Lemo;->d:Lenu;

    :cond_4
    iget-object v0, p0, Lemo;->d:Lenu;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lemo;->e:Ljava/lang/Boolean;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 1820
    iget-object v0, p0, Lemo;->b:Lenw;

    if-eqz v0, :cond_0

    .line 1821
    const/4 v0, 0x1

    iget-object v1, p0, Lemo;->b:Lenw;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1823
    :cond_0
    iget-object v0, p0, Lemo;->c:Lenx;

    if-eqz v0, :cond_1

    .line 1824
    const/4 v0, 0x2

    iget-object v1, p0, Lemo;->c:Lenx;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1826
    :cond_1
    iget-object v0, p0, Lemo;->d:Lenu;

    if-eqz v0, :cond_2

    .line 1827
    const/4 v0, 0x3

    iget-object v1, p0, Lemo;->d:Lenu;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1829
    :cond_2
    iget-object v0, p0, Lemo;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 1830
    const/4 v0, 0x4

    iget-object v1, p0, Lemo;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 1832
    :cond_3
    iget-object v0, p0, Lemo;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1834
    return-void
.end method
