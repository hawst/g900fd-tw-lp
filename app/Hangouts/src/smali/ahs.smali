.class public final Lahs;
.super Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/hangouts/video/SafeAsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

.field private final b:Landroid/net/Uri;

.field private final c:Lyj;

.field private final d:Landroid/content/Context;

.field private e:Landroid/net/Uri;

.field private final f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:I

.field private i:Ljava/lang/String;

.field private final j:Lyh;

.field private k:Z

.field private final l:I

.field private m:Z

.field private n:Z

.field private o:Z

.field private final p:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Landroid/net/Uri;JLjava/lang/String;Lyj;ILyh;Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 5916
    iput-object p1, p0, Lahs;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 5919
    sget-wide v0, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;->MAX_NETWORK_OPERATION_TIME_MILLIS:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;-><init>(J)V

    .line 5894
    iput-boolean v2, p0, Lahs;->m:Z

    .line 5897
    iput-boolean v2, p0, Lahs;->n:Z

    .line 5898
    iput-boolean v2, p0, Lahs;->o:Z

    .line 5901
    new-instance v0, Laht;

    invoke-direct {v0, p0}, Laht;-><init>(Lahs;)V

    iput-object v0, p0, Lahs;->p:Ljava/lang/Runnable;

    .line 5920
    iput-object p2, p0, Lahs;->b:Landroid/net/Uri;

    .line 5921
    iput-object p6, p0, Lahs;->c:Lyj;

    .line 5922
    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-eqz v0, :cond_0

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lahs;->g:Ljava/lang/String;

    .line 5923
    iput-object p5, p0, Lahs;->f:Ljava/lang/String;

    .line 5924
    iput-object p9, p0, Lahs;->d:Landroid/content/Context;

    .line 5925
    iput-object p8, p0, Lahs;->j:Lyh;

    .line 5926
    iput p7, p0, Lahs;->l:I

    .line 5927
    invoke-virtual {p1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->S()Z

    move-result v0

    iput-boolean v0, p0, Lahs;->o:Z

    .line 5928
    return-void

    .line 5922
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private varargs a()Ljava/lang/Void;
    .locals 10

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 5940
    :try_start_0
    iget-object v0, p0, Lahs;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 5941
    iget-object v1, p0, Lahs;->j:Lyh;

    if-eqz v1, :cond_3

    .line 5948
    iget-object v0, p0, Lahs;->j:Lyh;

    iget-object v0, v0, Lyh;->g:Ljava/lang/String;

    if-nez v0, :cond_2

    const-string v0, ""

    :goto_0
    iput-object v0, p0, Lahs;->i:Ljava/lang/String;

    .line 5950
    iget-object v0, p0, Lahs;->j:Lyh;

    iget-object v0, v0, Lyh;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lahs;->e:Landroid/net/Uri;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-object v0, v6

    .line 6122
    :goto_1
    iget-object v1, p0, Lahs;->b:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    const-string v2, "file"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lahs;->b:Landroid/net/Uri;

    .line 6123
    invoke-static {v1}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6125
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lahs;->b:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 6126
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 6128
    :cond_0
    if-eqz v0, :cond_1

    .line 6130
    :try_start_1
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_8

    .line 6136
    :cond_1
    :goto_2
    return-object v6

    .line 5948
    :cond_2
    :try_start_2
    iget-object v0, p0, Lahs;->j:Lyh;

    iget-object v0, v0, Lyh;->g:Ljava/lang/String;

    goto :goto_0

    .line 5956
    :cond_3
    iget-object v1, p0, Lahs;->g:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 5957
    const-string v1, "Babel"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 5958
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Photo picker: given picasa photoId "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lahs;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5960
    :cond_4
    iget-object v2, p0, Lahs;->f:Ljava/lang/String;

    .line 5961
    iget-object v1, p0, Lahs;->c:Lyj;

    invoke-virtual {v1}, Lyj;->c()Lbdk;

    move-result-object v1

    iget-object v1, v1, Lbdk;->a:Ljava/lang/String;

    .line 5984
    :goto_3
    iget-object v3, p0, Lahs;->g:Ljava/lang/String;

    if-eqz v3, :cond_5

    .line 5985
    invoke-static {v2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 5986
    const-string v1, "Babel"

    const-string v2, "Photo picker: matching account ids, sending photoId"

    invoke-static {v1, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 6001
    :cond_5
    :goto_4
    iget-object v1, p0, Lahs;->b:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    const-string v2, "content"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lahs;->b:Landroid/net/Uri;

    .line 6002
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    const-string v2, "file"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 6004
    :cond_6
    const-string v1, "Babel"

    const-string v2, "Photo picker: local photo"

    invoke-static {v1, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 6006
    iget-object v1, p0, Lahs;->b:Landroid/net/Uri;

    invoke-static {v0, v1}, Lbyr;->b(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lahs;->i:Ljava/lang/String;

    .line 6007
    iget-object v1, p0, Lahs;->i:Ljava/lang/String;

    invoke-static {v1}, Lf;->c(Ljava/lang/String;)Z

    move-result v2

    .line 6008
    if-eqz v2, :cond_d

    iget-boolean v1, p0, Lahs;->o:Z

    if-nez v1, :cond_d

    .line 6009
    const-string v0, "Babel"

    const-string v1, "Unuploaded video attached to Hangouts conversation"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 6010
    const/4 v0, 0x1

    iput-boolean v0, p0, Lahs;->m:Z
    :try_end_2
    .catch Ljava/net/MalformedURLException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 6011
    iget-object v0, p0, Lahs;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "file"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lahs;->b:Landroid/net/Uri;

    .line 6123
    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6125
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lahs;->b:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 6126
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto/16 :goto_2

    .line 5962
    :cond_7
    :try_start_3
    const-string v1, "com.google.android.gallery3d.provider"

    iget-object v2, p0, Lahs;->b:Landroid/net/Uri;

    .line 5963
    invoke-virtual {v2}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    .line 5962
    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 5964
    const-string v1, "Babel"

    const-string v2, "Photo picker: gallery2 photo"

    invoke-static {v1, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 5965
    iget-object v1, p0, Lahs;->c:Lyj;

    invoke-virtual {v1}, Lyj;->b()Ljava/lang/String;

    move-result-object v9

    .line 5966
    iget-object v1, p0, Lahs;->b:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lahs;->g:Ljava/lang/String;
    :try_end_3
    .catch Ljava/net/MalformedURLException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 5970
    :try_start_4
    iget-object v1, p0, Lahs;->b:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "user_account"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v1

    .line 5972
    if-eqz v1, :cond_8

    :try_start_5
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 5973
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    move-result-object v2

    .line 5978
    :goto_5
    if-eqz v1, :cond_1b

    .line 5979
    :try_start_6
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_6
    .catch Ljava/net/MalformedURLException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_6 .. :try_end_6} :catch_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-object v1, v9

    goto/16 :goto_3

    .line 5975
    :cond_8
    :try_start_7
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Photo picker: could not read user data "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lahs;->b:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_5

    move-object v2, v6

    goto :goto_5

    .line 5978
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_6
    if-eqz v1, :cond_9

    .line 5979
    :try_start_8
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_9
    throw v0
    :try_end_8
    .catch Ljava/net/MalformedURLException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_8 .. :try_end_8} :catch_6
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 6109
    :catch_0
    move-exception v0

    move-object v1, v6

    .line 6110
    :goto_7
    :try_start_9
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Photo picker: bad url "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lahs;->b:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    .line 6122
    iget-object v0, p0, Lahs;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v2, "file"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lahs;->b:Landroid/net/Uri;

    .line 6123
    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 6125
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lahs;->b:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 6126
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 6128
    :cond_a
    if-eqz v1, :cond_1

    .line 6130
    :try_start_a
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1

    goto/16 :goto_2

    .line 6132
    :catch_1
    move-exception v0

    goto/16 :goto_2

    .line 5990
    :cond_b
    const/4 v3, 0x0

    :try_start_b
    iput-object v3, p0, Lahs;->g:Ljava/lang/String;

    .line 5993
    const-string v3, "Babel"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 5994
    const-string v3, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Photo picker: can\'t send picasa photoId, senderAcctId="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", currentAcctId="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_b
    .catch Ljava/net/MalformedURLException; {:try_start_b .. :try_end_b} :catch_0
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_b .. :try_end_b} :catch_6
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    goto/16 :goto_4

    .line 6111
    :catch_2
    move-exception v0

    move-object v1, v6

    .line 6112
    :goto_8
    :try_start_c
    const-string v2, "Babel"

    const-string v3, "Photo picker: IOEx loading image"

    invoke-static {v2, v3, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    .line 6122
    iget-object v0, p0, Lahs;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v2, "file"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lahs;->b:Landroid/net/Uri;

    .line 6123
    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 6125
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lahs;->b:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 6126
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 6128
    :cond_c
    if-eqz v1, :cond_1

    .line 6130
    :try_start_d
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_3

    goto/16 :goto_2

    .line 6132
    :catch_3
    move-exception v0

    goto/16 :goto_2

    .line 6013
    :cond_d
    if-nez v2, :cond_e

    :try_start_e
    iget-object v1, p0, Lahs;->i:Ljava/lang/String;

    invoke-static {v1}, Lf;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 6014
    const/4 v0, 0x1

    iput-boolean v0, p0, Lahs;->n:Z

    .line 6015
    const-string v0, "Babel"

    const-string v1, "Unsupported content type"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_e
    .catch Ljava/net/MalformedURLException; {:try_start_e .. :try_end_e} :catch_0
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_e .. :try_end_e} :catch_6
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 6016
    iget-object v0, p0, Lahs;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "file"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lahs;->b:Landroid/net/Uri;

    .line 6123
    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6125
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lahs;->b:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 6126
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto/16 :goto_2

    .line 6018
    :cond_e
    :try_start_f
    iget-object v1, p0, Lahs;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_f
    .catch Ljava/net/MalformedURLException; {:try_start_f .. :try_end_f} :catch_0
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_f .. :try_end_f} :catch_6
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    move-result-object v1

    .line 6020
    :try_start_10
    iget-object v3, p0, Lahs;->b:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    const-string v4, "content"

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 6021
    const-string v3, "Babel"

    const-string v4, "Media from Android media provider"

    invoke-static {v3, v4}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 6022
    if-eqz v2, :cond_f

    :goto_9
    iput v7, p0, Lahs;->h:I
    :try_end_10
    .catch Ljava/net/MalformedURLException; {:try_start_10 .. :try_end_10} :catch_c
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_b
    .catch Ljava/lang/SecurityException; {:try_start_10 .. :try_end_10} :catch_a
    .catchall {:try_start_10 .. :try_end_10} :catchall_2

    move v4, v2

    .line 6085
    :goto_a
    :try_start_11
    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    invoke-virtual {v2}, Ljava/util/Random;->nextLong()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    .line 6086
    iget-object v5, p0, Lahs;->c:Lyj;

    invoke-static {v5, v2, v3}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Lyj;J)Landroid/net/Uri;

    move-result-object v5

    .line 6087
    invoke-virtual {v0, v5}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;)Ljava/io/OutputStream;

    move-result-object v0

    .line 6090
    const-wide/16 v2, 0x0

    .line 6091
    const/16 v7, 0x400

    new-array v7, v7, [B

    .line 6092
    :goto_b
    const/4 v8, 0x0

    const/16 v9, 0x400

    invoke-virtual {v1, v7, v8, v9}, Ljava/io/InputStream;->read([BII)I

    move-result v8

    const/4 v9, -0x1

    if-eq v8, v9, :cond_14

    .line 6093
    const/4 v9, 0x0

    invoke-virtual {v0, v7, v9, v8}, Ljava/io/OutputStream;->write([BII)V
    :try_end_11
    .catch Ljava/net/MalformedURLException; {:try_start_11 .. :try_end_11} :catch_c
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_b
    .catch Ljava/lang/SecurityException; {:try_start_11 .. :try_end_11} :catch_a
    .catchall {:try_start_11 .. :try_end_11} :catchall_4

    .line 6094
    int-to-long v8, v8

    add-long/2addr v2, v8

    goto :goto_b

    .line 6022
    :cond_f
    :try_start_12
    iget-object v3, p0, Lahs;->b:Landroid/net/Uri;

    invoke-static {v0, v3}, Lbyr;->a(Landroid/content/ContentResolver;Landroid/net/Uri;)I

    move-result v7

    goto :goto_9

    .line 6024
    :cond_10
    const-string v3, "Babel"

    const-string v4, "Photo from a file"

    invoke-static {v3, v4}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 6027
    iget-object v3, p0, Lahs;->b:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lbyr;->a(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lahs;->h:I
    :try_end_12
    .catch Ljava/net/MalformedURLException; {:try_start_12 .. :try_end_12} :catch_c
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_b
    .catch Ljava/lang/SecurityException; {:try_start_12 .. :try_end_12} :catch_a
    .catchall {:try_start_12 .. :try_end_12} :catchall_2

    move v4, v2

    goto :goto_a

    .line 6031
    :cond_11
    :try_start_13
    const-string v1, "Babel"

    const-string v2, "Photo picker: remote photo"

    invoke-static {v1, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 6032
    iget-object v1, p0, Lahs;->g:Ljava/lang/String;

    if-eqz v1, :cond_12

    iget-boolean v1, p0, Lahs;->o:Z

    if-nez v1, :cond_12

    .line 6036
    const-string v1, "babel_thumbnail_photo_upload_size"

    const/16 v2, 0x280

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v1

    .line 6046
    :goto_c
    iget-object v2, p0, Lahs;->b:Landroid/net/Uri;

    .line 6047
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 6046
    invoke-static {v1, v2}, Lbam;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 6048
    const/4 v2, 0x0

    iput v2, p0, Lahs;->h:I

    .line 6051
    new-instance v2, Ljava/net/URL;

    invoke-direct {v2, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 6052
    invoke-virtual {v2}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    .line 6053
    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-virtual {v2}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_13
    .catch Ljava/net/MalformedURLException; {:try_start_13 .. :try_end_13} :catch_0
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_13 .. :try_end_13} :catch_6
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    .line 6054
    :try_start_14
    iget v2, p0, Lahs;->l:I

    packed-switch v2, :pswitch_data_0

    .line 6077
    :pswitch_0
    const-string v2, "image/jpeg"

    iput-object v2, p0, Lahs;->i:Ljava/lang/String;
    :try_end_14
    .catch Ljava/net/MalformedURLException; {:try_start_14 .. :try_end_14} :catch_c
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_b
    .catch Ljava/lang/SecurityException; {:try_start_14 .. :try_end_14} :catch_a
    .catchall {:try_start_14 .. :try_end_14} :catchall_3

    move v4, v7

    goto/16 :goto_a

    .line 6042
    :cond_12
    :try_start_15
    const-string v1, "babel_full_res_photo_upload_size"

    const/16 v2, 0x800

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I
    :try_end_15
    .catch Ljava/net/MalformedURLException; {:try_start_15 .. :try_end_15} :catch_0
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_15 .. :try_end_15} :catch_6
    .catchall {:try_start_15 .. :try_end_15} :catchall_1

    move-result v1

    goto :goto_c

    .line 6056
    :pswitch_1
    :try_start_16
    const-string v2, "image/jpeg"

    iput-object v2, p0, Lahs;->i:Ljava/lang/String;

    move v4, v7

    .line 6057
    goto/16 :goto_a

    .line 6059
    :pswitch_2
    const-string v2, "image/gif"

    iput-object v2, p0, Lahs;->i:Ljava/lang/String;

    move v4, v7

    .line 6060
    goto/16 :goto_a

    .line 6062
    :pswitch_3
    const-string v2, "video/mp4"

    iput-object v2, p0, Lahs;->i:Ljava/lang/String;

    .line 6064
    iget-boolean v2, p0, Lahs;->o:Z

    if-nez v2, :cond_1a

    iget-object v2, p0, Lahs;->g:Ljava/lang/String;

    if-nez v2, :cond_1a

    .line 6068
    const/4 v0, 0x1

    iput-boolean v0, p0, Lahs;->m:Z
    :try_end_16
    .catch Ljava/net/MalformedURLException; {:try_start_16 .. :try_end_16} :catch_c
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_b
    .catch Ljava/lang/SecurityException; {:try_start_16 .. :try_end_16} :catch_a
    .catchall {:try_start_16 .. :try_end_16} :catchall_3

    .line 6071
    iget-object v0, p0, Lahs;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v2, "file"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_13

    iget-object v0, p0, Lahs;->b:Landroid/net/Uri;

    .line 6123
    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 6125
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lahs;->b:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 6126
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 6128
    :cond_13
    :try_start_17
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_4

    goto/16 :goto_2

    :catch_4
    move-exception v0

    goto/16 :goto_2

    .line 6096
    :cond_14
    :try_start_18
    iput-object v5, p0, Lahs;->e:Landroid/net/Uri;

    .line 6098
    iget-boolean v0, p0, Lahs;->o:Z

    if-eqz v0, :cond_19

    if-eqz v4, :cond_19

    .line 6100
    invoke-static {}, Lbvm;->a()Lsm;

    move-result-object v0

    invoke-virtual {v0}, Lsm;->e()I

    move-result v0

    add-int/lit16 v0, v0, -0x1000

    int-to-long v4, v0

    cmp-long v0, v2, v4

    if-lez v0, :cond_19

    .line 6101
    const/4 v0, 0x1

    iput-boolean v0, p0, Lahs;->k:Z

    .line 6103
    iget-object v0, p0, Lahs;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    iget-object v0, p0, Lahs;->e:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->d(Ljava/lang/String;)V

    .line 6104
    const/4 v0, 0x0

    iput-object v0, p0, Lahs;->e:Landroid/net/Uri;
    :try_end_18
    .catch Ljava/net/MalformedURLException; {:try_start_18 .. :try_end_18} :catch_c
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_18} :catch_b
    .catch Ljava/lang/SecurityException; {:try_start_18 .. :try_end_18} :catch_a
    .catchall {:try_start_18 .. :try_end_18} :catchall_4

    .line 6105
    iget-object v0, p0, Lahs;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v2, "file"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_15

    iget-object v0, p0, Lahs;->b:Landroid/net/Uri;

    .line 6123
    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 6125
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lahs;->b:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 6126
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 6128
    :cond_15
    if-eqz v1, :cond_1

    .line 6130
    :try_start_19
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_19
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_19} :catch_5

    goto/16 :goto_2

    :catch_5
    move-exception v0

    goto/16 :goto_2

    .line 6113
    :catch_6
    move-exception v0

    move-object v1, v6

    .line 6120
    :goto_d
    :try_start_1a
    const-string v2, "Babel"

    const-string v3, "Photo picker: error trying to persist photo, aborting"

    invoke-static {v2, v3, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_4

    .line 6122
    iget-object v0, p0, Lahs;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v2, "file"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_16

    iget-object v0, p0, Lahs;->b:Landroid/net/Uri;

    .line 6123
    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 6125
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lahs;->b:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 6126
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 6128
    :cond_16
    if-eqz v1, :cond_1

    .line 6130
    :try_start_1b
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1b
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_1b} :catch_7

    goto/16 :goto_2

    .line 6132
    :catch_7
    move-exception v0

    goto/16 :goto_2

    .line 6122
    :catchall_1
    move-exception v0

    :goto_e
    iget-object v1, p0, Lahs;->b:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    const-string v2, "file"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_17

    iget-object v1, p0, Lahs;->b:Landroid/net/Uri;

    .line 6123
    invoke-static {v1}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 6125
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lahs;->b:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 6126
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 6128
    :cond_17
    if-eqz v6, :cond_18

    .line 6130
    :try_start_1c
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_1c
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_1c} :catch_9

    .line 6132
    :cond_18
    :goto_f
    throw v0

    :catch_8
    move-exception v0

    goto/16 :goto_2

    :catch_9
    move-exception v1

    goto :goto_f

    .line 6122
    :catchall_2
    move-exception v0

    move-object v6, v1

    goto :goto_e

    :catchall_3
    move-exception v0

    move-object v6, v1

    goto :goto_e

    :catchall_4
    move-exception v0

    move-object v6, v1

    goto :goto_e

    .line 6113
    :catch_a
    move-exception v0

    goto :goto_d

    .line 6111
    :catch_b
    move-exception v0

    goto/16 :goto_8

    .line 6109
    :catch_c
    move-exception v0

    goto/16 :goto_7

    .line 5978
    :catchall_5
    move-exception v0

    goto/16 :goto_6

    :cond_19
    move-object v0, v1

    goto/16 :goto_1

    :cond_1a
    move v4, v8

    goto/16 :goto_a

    :cond_1b
    move-object v1, v9

    goto/16 :goto_3

    :cond_1c
    move-object v1, v6

    move-object v2, v6

    goto/16 :goto_3

    .line 6054
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method protected synthetic doInBackgroundTimed([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 5880
    invoke-direct {p0}, Lahs;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onCancelled(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 5880
    iget-object v0, p0, Lahs;->e:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lahs;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    iget-object v0, p0, Lahs;->e:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->d(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lahs;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->L()V

    iget-object v0, p0, Lahs;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ar(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lahs;

    return-void
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 8

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 5880
    iget-object v0, p0, Lahs;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->q(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Landroid/os/Handler;

    move-result-object v0

    iget-object v4, p0, Lahs;->p:Ljava/lang/Runnable;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-boolean v0, p0, Lahs;->m:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lahs;->d:Landroid/content/Context;

    sget v4, Lh;->oq:I

    invoke-static {v0, v4, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lahs;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    iget v4, p0, Lahs;->h:I

    invoke-static {v0, v4}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->c(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;I)I

    iget-object v0, p0, Lahs;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    iget-object v4, p0, Lahs;->g:Ljava/lang/String;

    invoke-static {v0, v4}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lahs;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    iget-object v4, p0, Lahs;->i:Ljava/lang/String;

    invoke-static {v0, v4}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->e(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Ljava/lang/String;)Ljava/lang/String;

    const-string v0, "Babel"

    invoke-static {v0, v3}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "mPhotoUriToSend: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lahs;->e:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v4, p0, Lahs;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    iget-object v5, p0, Lahs;->d:Landroid/content/Context;

    iget-object v0, p0, Lahs;->e:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    iget v7, p0, Lahs;->h:I

    iget-object v0, p0, Lahs;->i:Ljava/lang/String;

    invoke-static {v0}, Lf;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lahs;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->S()Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    :goto_1
    invoke-static {v4, v5, v6, v7, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Landroid/content/Context;Ljava/lang/String;IZ)V

    iget-object v0, p0, Lahs;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->W(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lbwt;

    move-result-object v4

    iget-object v0, p0, Lahs;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->G(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    move v0, v1

    :goto_2
    iget-object v5, p0, Lahs;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v5}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->S()Z

    move-result v5

    invoke-virtual {v4, v0, v5}, Lbwt;->a(ZZ)V

    iget-object v0, p0, Lahs;->i:Ljava/lang/String;

    invoke-static {v0}, Lf;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    :goto_3
    iget-object v0, p0, Lahs;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->F(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a(I)V

    :cond_1
    iget-object v0, p0, Lahs;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aq(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Z

    iget-object v0, p0, Lahs;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ar(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lahs;

    return-void

    :cond_2
    iget-boolean v0, p0, Lahs;->k:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lahs;->d:Landroid/content/Context;

    sget v4, Lh;->ow:I

    invoke-static {v0, v4, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v2

    goto/16 :goto_0

    :cond_3
    iget-boolean v0, p0, Lahs;->n:Z

    if-eqz v0, :cond_5

    iget-object v4, p0, Lahs;->d:Landroid/content/Context;

    iget-boolean v0, p0, Lahs;->o:Z

    if-eqz v0, :cond_4

    sget v0, Lh;->kT:I

    :goto_4
    invoke-static {v4, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v2

    goto/16 :goto_0

    :cond_4
    sget v0, Lh;->kS:I

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lahs;->e:Landroid/net/Uri;

    if-nez v0, :cond_6

    iget-object v0, p0, Lahs;->d:Landroid/content/Context;

    sget v4, Lh;->Z:I

    invoke-static {v0, v4, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v2

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lahs;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    iget-object v4, p0, Lahs;->e:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->f(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Ljava/lang/String;)Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    :cond_7
    move v0, v2

    goto/16 :goto_1

    :cond_8
    move v0, v2

    goto :goto_2

    :cond_9
    iget-object v0, p0, Lahs;->i:Ljava/lang/String;

    invoke-static {v0}, Lf;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    move v1, v3

    goto :goto_3

    :cond_a
    move v1, v2

    goto :goto_3
.end method

.method protected onPreExecute()V
    .locals 4

    .prologue
    .line 5933
    iget-object v0, p0, Lahs;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->q(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lahs;->p:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 5934
    return-void
.end method
