.class public Lbhu;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final g:Ljava/lang/String;

.field private final h:J

.field private final i:Z


# direct methods
.method public constructor <init>(Landroid/net/Uri;JZ)V
    .locals 1

    .prologue
    .line 676
    invoke-direct {p0}, Lbfz;-><init>()V

    .line 677
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lbhu;->g:Ljava/lang/String;

    .line 678
    iput-wide p2, p0, Lbhu;->h:J

    .line 679
    iput-boolean p4, p0, Lbhu;->i:Z

    .line 680
    return-void

    .line 677
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lyt;Lbnl;)V
    .locals 4

    .prologue
    .line 685
    invoke-super {p0, p1, p2}, Lbfz;->a(Lyt;Lbnl;)V

    .line 686
    iget-object v0, p0, Lbhu;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbhu;->g:Ljava/lang/String;

    .line 687
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    iget-wide v1, p0, Lbhu;->h:J

    iget-boolean v3, p0, Lbhu;->i:Z

    .line 686
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Landroid/net/Uri;JZ)V

    .line 691
    return-void

    .line 687
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
