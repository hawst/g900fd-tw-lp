.class public final Ldij;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldij;


# instance fields
.field public b:Ljava/lang/Double;

.field public c:Ljava/lang/String;

.field public d:[Ldim;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 623
    const/4 v0, 0x0

    new-array v0, v0, [Ldij;

    sput-object v0, Ldij;->a:[Ldij;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 624
    invoke-direct {p0}, Lepn;-><init>()V

    .line 631
    sget-object v0, Ldim;->a:[Ldim;

    iput-object v0, p0, Ldij;->d:[Ldim;

    .line 624
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 653
    const/4 v0, 0x1

    iget-object v1, p0, Ldij;->b:Ljava/lang/Double;

    .line 655
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v0}, Lepl;->g(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x0

    .line 656
    iget-object v1, p0, Ldij;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 657
    const/4 v1, 0x2

    iget-object v2, p0, Ldij;->c:Ljava/lang/String;

    .line 658
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 660
    :cond_0
    iget-object v1, p0, Ldij;->d:[Ldim;

    if-eqz v1, :cond_2

    .line 661
    iget-object v2, p0, Ldij;->d:[Ldim;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 662
    if-eqz v4, :cond_1

    .line 663
    const/4 v5, 0x3

    .line 664
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 661
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 668
    :cond_2
    iget-object v1, p0, Ldij;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 669
    iput v0, p0, Ldij;->cachedSize:I

    .line 670
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 620
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldij;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldij;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldij;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->b()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Ldij;->b:Ljava/lang/Double;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldij;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldij;->d:[Ldim;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldim;

    iget-object v3, p0, Ldij;->d:[Ldim;

    if-eqz v3, :cond_2

    iget-object v3, p0, Ldij;->d:[Ldim;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Ldij;->d:[Ldim;

    :goto_2
    iget-object v2, p0, Ldij;->d:[Ldim;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Ldij;->d:[Ldim;

    new-instance v3, Ldim;

    invoke-direct {v3}, Ldim;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldij;->d:[Ldim;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ldij;->d:[Ldim;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Ldij;->d:[Ldim;

    new-instance v3, Ldim;

    invoke-direct {v3}, Ldim;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldij;->d:[Ldim;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 636
    const/4 v0, 0x1

    iget-object v1, p0, Ldij;->b:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->a(ID)V

    .line 637
    iget-object v0, p0, Ldij;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 638
    const/4 v0, 0x2

    iget-object v1, p0, Ldij;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 640
    :cond_0
    iget-object v0, p0, Ldij;->d:[Ldim;

    if-eqz v0, :cond_2

    .line 641
    iget-object v1, p0, Ldij;->d:[Ldim;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 642
    if-eqz v3, :cond_1

    .line 643
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 641
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 647
    :cond_2
    iget-object v0, p0, Ldij;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 649
    return-void
.end method
