.class public Lbfe;
.super Lbep;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 4539
    invoke-direct {p0}, Lbep;-><init>()V

    .line 4540
    iput-object p1, p0, Lbfe;->a:Ljava/lang/String;

    .line 4541
    iput-object p2, p0, Lbfe;->b:Ljava/lang/String;

    .line 4542
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 3

    .prologue
    .line 4546
    sget-boolean v0, Lbea;->d:Z

    if-eqz v0, :cond_0

    .line 4547
    const-string v0, "Babel_RequestWriter"

    const-string v1, "SendOffnetworkInvitationRequest build protobuf "

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4549
    :cond_0
    new-instance v0, Ldug;

    invoke-direct {v0}, Ldug;-><init>()V

    .line 4550
    iget-object v1, p0, Lbfe;->a:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 4551
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Ldug;->b:Ljava/lang/Integer;

    .line 4552
    iget-object v1, p0, Lbfe;->a:Ljava/lang/String;

    iput-object v1, v0, Ldug;->d:Ljava/lang/String;

    .line 4557
    :goto_0
    new-instance v1, Ldvw;

    invoke-direct {v1}, Ldvw;-><init>()V

    .line 4559
    invoke-virtual {p0, p1, p2}, Lbfe;->c(Ljava/lang/String;I)Ldvm;

    move-result-object v2

    iput-object v2, v1, Ldvw;->b:Ldvm;

    .line 4560
    iput-object v0, v1, Ldvw;->c:Ldug;

    .line 4561
    return-object v1

    .line 4554
    :cond_1
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Ldug;->b:Ljava/lang/Integer;

    .line 4555
    iget-object v1, p0, Lbfe;->b:Ljava/lang/String;

    iput-object v1, v0, Ldug;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(JI)Z
    .locals 1

    .prologue
    .line 4567
    const/4 v0, 0x0

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4572
    const-string v0, "devices/sendoffnetworkinvitation"

    return-object v0
.end method
