.class public final Lbfy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:Ljava/lang/String;

.field private e:J


# direct methods
.method public constructor <init>(IIILjava/lang/String;)V
    .locals 2

    .prologue
    .line 3045
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3046
    iput p1, p0, Lbfy;->a:I

    .line 3047
    iput p2, p0, Lbfy;->b:I

    .line 3048
    iput p3, p0, Lbfy;->c:I

    .line 3049
    iput-object p4, p0, Lbfy;->d:Ljava/lang/String;

    .line 3050
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lbfy;->e:J

    .line 3051
    return-void
.end method

.method private static a(I)Lehh;
    .locals 2

    .prologue
    .line 3067
    new-instance v0, Lehh;

    invoke-direct {v0}, Lehh;-><init>()V

    .line 3069
    const-string v1, "bbl"

    iput-object v1, v0, Lehh;->b:Ljava/lang/String;

    .line 3070
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lehh;->c:Ljava/lang/Integer;

    .line 3071
    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3054
    iget-object v0, p0, Lbfy;->d:Ljava/lang/String;

    return-object v0
.end method

.method public a(J)V
    .locals 0

    .prologue
    .line 3062
    iput-wide p1, p0, Lbfy;->e:J

    .line 3063
    return-void
.end method

.method public b()J
    .locals 2

    .prologue
    .line 3058
    iget-wide v0, p0, Lbfy;->e:J

    return-wide v0
.end method

.method public c()Ldsx;
    .locals 5

    .prologue
    .line 3092
    new-instance v0, Ldsx;

    invoke-direct {v0}, Ldsx;-><init>()V

    .line 3093
    new-instance v1, Lehf;

    invoke-direct {v1}, Lehf;-><init>()V

    iget v2, p0, Lbfy;->b:I

    invoke-static {v2}, Lbfy;->a(I)Lehh;

    move-result-object v2

    iput-object v2, v1, Lehf;->f:Lehh;

    iget v2, p0, Lbfy;->a:I

    invoke-static {v2}, Lbfy;->a(I)Lehh;

    move-result-object v2

    iput-object v2, v1, Lehf;->g:Lehh;

    iput-object v1, v0, Ldsx;->b:Lehf;

    .line 3094
    new-instance v1, Ldsy;

    invoke-direct {v1}, Ldsy;-><init>()V

    new-instance v2, Ldwr;

    invoke-direct {v2}, Ldwr;-><init>()V

    iget v3, p0, Lbfy;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Ldwr;->b:Ljava/lang/Integer;

    iget-wide v3, p0, Lbfy;->e:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v2, Ldwr;->c:Ljava/lang/Long;

    iput-object v2, v1, Ldsy;->e:Ldwr;

    iput-object v1, v0, Ldsx;->c:Ldsy;

    .line 3095
    return-object v0
.end method
