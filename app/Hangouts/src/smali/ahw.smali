.class public final Lahw;
.super Lbor;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V
    .locals 0

    .prologue
    .line 1115
    iput-object p1, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-direct {p0}, Lbor;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;B)V
    .locals 0

    .prologue
    .line 1115
    invoke-direct {p0, p1}, Lahw;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    return-void
.end method

.method private a(Lbkr;)V
    .locals 4

    .prologue
    .line 1143
    if-nez p1, :cond_0

    .line 1144
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "creating conversation with "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 1146
    invoke-static {v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lajk;

    move-result-object v2

    iget-object v2, v2, Lajk;->b:Lbdh;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " resulting in null ConversationResult"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1144
    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 1165
    :goto_0
    return-void

    .line 1151
    :cond_0
    new-instance v0, Lahc;

    iget-object v1, p1, Lbkr;->a:Ljava/lang/String;

    iget-object v2, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 1154
    invoke-static {v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lyj;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lahc;-><init>(Ljava/lang/String;Lyj;I)V

    .line 1156
    const/4 v1, 0x1

    iput-boolean v1, v0, Lahc;->b:Z

    .line 1157
    iget-object v1, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->X()Z

    move-result v1

    iput-boolean v1, v0, Lahc;->h:Z

    .line 1158
    iget-object v1, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->h(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Z

    move-result v1

    iput-boolean v1, v0, Lahc;->n:Z

    .line 1159
    iget-object v1, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->Y()Lyh;

    move-result-object v1

    iput-object v1, v0, Lahc;->e:Lyh;

    .line 1161
    iget-object v1, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->H()V

    .line 1163
    iget-object v1, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    iget-object v2, v0, Lahc;->e:Lyh;

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Lyh;)V

    .line 1164
    iget-object v1, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lahc;)V

    goto :goto_0
.end method

.method private b(Lyj;Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1180
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lyj;

    move-result-object v0

    invoke-virtual {p1, v0}, Lyj;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1212
    :cond_0
    :goto_0
    return-void

    .line 1186
    :cond_1
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Les;

    move-result-object v0

    invoke-virtual {v0, p2}, Les;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1192
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Les;

    move-result-object v0

    invoke-virtual {v0}, Les;->size()I

    move-result v0

    if-le v0, v3, :cond_4

    .line 1194
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Les;

    move-result-object v0

    invoke-virtual {v0}, Les;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1195
    invoke-static {v0, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1196
    new-instance v1, Lahc;

    iget-object v2, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 1199
    invoke-static {v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lyj;

    move-result-object v2

    invoke-direct {v1, v0, v2, v3}, Lahc;-><init>(Ljava/lang/String;Lyj;I)V

    .line 1202
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lahc;Z)V

    goto :goto_0

    .line 1205
    :cond_3
    const-string v0, "should never get here"

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1209
    :cond_4
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->j(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lahm;

    move-result-object v0

    invoke-interface {v0, p3}, Lahm;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method


# virtual methods
.method public a(ILbkr;Lbos;)V
    .locals 1

    .prologue
    .line 1120
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->f(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)I

    move-result v0

    if-eq p1, v0, :cond_0

    .line 1124
    :goto_0
    return-void

    .line 1123
    :cond_0
    invoke-direct {p0, p2}, Lahw;->a(Lbkr;)V

    goto :goto_0
.end method

.method public a(ILyj;Lbea;Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 1265
    instance-of v0, p3, Lbek;

    if-eqz v0, :cond_0

    .line 1266
    check-cast p3, Lbek;

    .line 1269
    iget-boolean v0, p3, Lbek;->b:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p3, Lbek;->a:Z

    if-nez v0, :cond_0

    .line 1270
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 1271
    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Les;

    move-result-object v0

    iget-object v1, p3, Lbek;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Les;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahf;

    .line 1272
    if-eqz v0, :cond_0

    .line 1273
    iget-object v1, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->I()V

    .line 1274
    const/4 v1, -0x1

    iput v1, v0, Lahf;->e:I

    .line 1275
    const-string v0, "Babel"

    const-string v1, "message load timeout"

    invoke-static {v0, v1}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1279
    :cond_0
    return-void
.end method

.method public a(ILyj;Lbos;)V
    .locals 2

    .prologue
    .line 1244
    invoke-virtual {p3}, Lbos;->c()Lbfz;

    move-result-object v0

    .line 1245
    instance-of v1, v0, Lbgr;

    if-eqz v1, :cond_0

    .line 1246
    invoke-virtual {v0}, Lbfz;->b()Lbea;

    move-result-object v0

    check-cast v0, Lbek;

    .line 1252
    iget-boolean v1, v0, Lbek;->b:Z

    if-eqz v1, :cond_0

    iget-boolean v1, v0, Lbek;->a:Z

    if-nez v1, :cond_0

    .line 1253
    iget-object v1, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 1254
    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Les;

    move-result-object v1

    iget-object v0, v0, Lbek;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Les;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahf;

    .line 1255
    if-eqz v0, :cond_0

    .line 1256
    const/4 v1, -0x1

    iput v1, v0, Lahf;->e:I

    .line 1260
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Lbdk;Ljava/lang/String;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    .line 1284
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->k(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 1285
    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lyj;

    move-result-object v0

    invoke-virtual {v0}, Lyj;->c()Lbdk;

    move-result-object v0

    invoke-virtual {v0, p2}, Lbdk;->a(Lbdk;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 1287
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->O()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 1288
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->F()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1289
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->l(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lyc;

    move-result-object v0

    invoke-virtual {v0, p2}, Lyc;->a(Lbdk;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1290
    const-string v0, "Babel"

    const-string v1, "Got typing status before mParticipantCache initialized. Dropping on the floor."

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1315
    :cond_0
    :goto_0
    return-void

    .line 1294
    :cond_1
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lbdk;)Lbdh;

    move-result-object v0

    .line 1295
    if-eqz v0, :cond_0

    .line 1296
    if-eqz p4, :cond_3

    .line 1297
    iget-object v1, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->o(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lbyz;

    move-result-object v1

    new-instance v2, Lahz;

    iget-object v3, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-direct {v2, v3, p3}, Lahz;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Ljava/lang/String;)V

    invoke-virtual {v1, p2, v2}, Lbyz;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1298
    iget-object v1, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->n(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    move-result-object v1

    invoke-virtual {v1, v0, v4}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->a(Lbdh;I)V

    .line 1305
    :goto_1
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1306
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Typing status for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " changed to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1309
    :cond_2
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->q(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->p(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1311
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->q(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->p(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1312
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->q(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->p(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x7918

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 1301
    :cond_3
    iget-object v1, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->o(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lbyz;

    move-result-object v1

    invoke-virtual {v1, p2}, Lbyz;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1302
    iget-object v1, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->n(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v4, v2}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->a(Lbdh;IZ)V

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Lbdk;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 1217
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->k(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 1218
    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lyj;

    move-result-object v0

    invoke-virtual {v0}, Lyj;->c()Lbdk;

    move-result-object v0

    invoke-virtual {v0, p2}, Lbdk;->a(Lbdk;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 1220
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->O()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 1221
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->F()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1222
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->l(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lyc;

    move-result-object v0

    invoke-virtual {v0, p2}, Lyc;->a(Lbdk;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1224
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->m(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lahr;

    iget-object v2, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-direct {v1, v2, p1, p2, p3}, Lahr;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Ljava/lang/String;Lbdk;Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1239
    :cond_0
    :goto_0
    return-void

    .line 1228
    :cond_1
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lbdk;)Lbdh;

    move-result-object v0

    .line 1229
    if-eqz v0, :cond_0

    .line 1230
    if-eqz p3, :cond_2

    .line 1231
    iget-object v1, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->n(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->a(Lbdh;I)V

    goto :goto_0

    .line 1234
    :cond_2
    iget-object v1, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->n(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v3, v2}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->a(Lbdh;IZ)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1319
    invoke-static {}, Lcwz;->a()V

    .line 1322
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->k(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1360
    :goto_0
    return-void

    .line 1326
    :cond_0
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->k(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1327
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1328
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onConversationIdChanged: oldId = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->k(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " newId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1331
    :cond_1
    new-instance v0, Lahc;

    iget-object v1, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 1332
    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lyj;

    move-result-object v1

    iget-object v2, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->r(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)I

    move-result v2

    invoke-direct {v0, p2, v1, v2}, Lahc;-><init>(Ljava/lang/String;Lyj;I)V

    .line 1333
    iget-object v1, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->h(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Z

    move-result v1

    iput-boolean v1, v0, Lahc;->n:Z

    .line 1334
    const/4 v1, 0x1

    iput-boolean v1, v0, Lahc;->b:Z

    .line 1335
    iget-object v1, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Lahc;)V

    .line 1340
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    iget-object v0, v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g:Lbai;

    check-cast v0, Laad;

    invoke-virtual {v0}, Laad;->e()V

    .line 1341
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->s(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    .line 1342
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->t(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    .line 1344
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 1345
    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lyj;

    move-result-object v0

    iget-object v1, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->k(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    move-result-object v1

    .line 1344
    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->i(Lyj;Ljava/lang/String;)I

    .line 1346
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->F()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1347
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->u(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    .line 1353
    :cond_2
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Les;

    move-result-object v0

    invoke-virtual {v0, p1}, Les;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahf;

    .line 1354
    if-eqz v0, :cond_3

    .line 1355
    iput-object p2, v0, Lahf;->a:Ljava/lang/String;

    .line 1356
    iget-object v1, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Les;

    move-result-object v1

    invoke-virtual {v1, p1}, Les;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1357
    iget-object v1, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Les;

    move-result-object v1

    invoke-virtual {v1, p2, v0}, Les;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1359
    :cond_3
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->v(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    goto/16 :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lbdk;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/16 v7, 0x8

    const/4 v2, 0x0

    .line 1382
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->k(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 1383
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->F()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1384
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->x(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lbza;

    move-result-object v3

    .line 1385
    if-eqz p2, :cond_3

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 1386
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    new-instance v1, Lbza;

    invoke-direct {v1}, Lbza;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Lbza;)Lbza;

    .line 1388
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdk;

    .line 1389
    iget-object v1, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lbdk;)Lbdh;

    move-result-object v5

    .line 1390
    if-eqz v5, :cond_0

    iget-object v1, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 1391
    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lyj;

    move-result-object v1

    invoke-virtual {v1}, Lyj;->c()Lbdk;

    move-result-object v1

    invoke-virtual {v1, v0}, Lbdk;->a(Lbdk;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1394
    if-nez v3, :cond_1

    move v1, v2

    .line 1398
    :goto_1
    iget-object v6, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v6}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->x(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lbza;

    move-result-object v6

    invoke-virtual {v6, v0}, Lbza;->add(Ljava/lang/Object;)Z

    .line 1399
    if-nez v1, :cond_2

    .line 1401
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->y(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    move-result-object v0

    invoke-virtual {v0, v5, v9}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->a(Lbdh;I)V

    goto :goto_0

    .line 1397
    :cond_1
    invoke-virtual {v3, v0}, Lbza;->contains(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_1

    .line 1403
    :cond_2
    if-eqz v3, :cond_0

    .line 1404
    invoke-virtual {v3, v0}, Lbza;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1408
    :cond_3
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Lbza;)Lbza;

    .line 1414
    :cond_4
    if-eqz v3, :cond_6

    invoke-virtual {v3}, Lbza;->size()I

    move-result v0

    if-lez v0, :cond_6

    .line 1415
    invoke-virtual {v3}, Lbza;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdk;

    .line 1416
    iget-object v3, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lbdk;)Lbdh;

    move-result-object v0

    .line 1417
    if-eqz v0, :cond_5

    .line 1418
    iget-object v3, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v3}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->y(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    move-result-object v3

    invoke-virtual {v3, v0, v9, v8}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->a(Lbdh;IZ)V

    goto :goto_2

    .line 1424
    :cond_6
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->x(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lbza;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 1425
    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->x(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lbza;

    move-result-object v0

    invoke-virtual {v0}, Lbza;->size()I

    move-result v0

    if-nez v0, :cond_9

    .line 1427
    :cond_7
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->z(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1428
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->y(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->setVisibility(I)V

    .line 1429
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->A(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1442
    :cond_8
    :goto_3
    return-void

    .line 1430
    :cond_9
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->x(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lbza;

    move-result-object v0

    invoke-virtual {v0}, Lbza;->size()I

    move-result v0

    if-ne v0, v8, :cond_a

    .line 1432
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->z(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1433
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->y(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->setVisibility(I)V

    .line 1434
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->A(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    .line 1437
    :cond_a
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->A(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1438
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->y(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->setVisibility(I)V

    .line 1439
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->z(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3
.end method

.method public a(Lyj;Lbkr;I)V
    .locals 3

    .prologue
    .line 1130
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1131
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Forked a new conversation "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p2, Lbkr;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " from an existing conversation "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for account "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1135
    :cond_0
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lyj;

    move-result-object v0

    invoke-virtual {p1, v0}, Lyj;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1140
    :goto_0
    return-void

    .line 1139
    :cond_1
    invoke-direct {p0, p2}, Lahw;->a(Lbkr;)V

    goto :goto_0
.end method

.method public a(Lyj;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1169
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lahw;->b(Lyj;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 1170
    return-void
.end method

.method public a(Lyj;Ljava/lang/String;Lbdk;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1449
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lyj;

    move-result-object v0

    if-ne v0, p1, :cond_1

    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->k(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 1450
    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lyj;

    move-result-object v0

    invoke-virtual {v0}, Lyj;->c()Lbdk;

    move-result-object v0

    invoke-virtual {v0, p3}, Lbdk;->a(Lbdk;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 1451
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->O()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->F()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1452
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1453
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Received easter egg for conversation: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " from: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1454
    invoke-virtual {p3}, Lbdk;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1453
    invoke-static {v0, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1457
    :cond_0
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->B(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/apps/hangouts/views/EasterEggView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1458
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->B(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/apps/hangouts/views/EasterEggView;

    move-result-object v0

    invoke-virtual {v0, p1, p4}, Lcom/google/android/apps/hangouts/views/EasterEggView;->a(Lyj;Ljava/lang/String;)V

    .line 1461
    :cond_1
    return-void
.end method

.method public a(Lyj;Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 1175
    invoke-direct {p0, p1, p2, p3}, Lahw;->b(Lyj;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 1176
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1466
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->k(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1489
    :cond_0
    :goto_0
    return-void

    .line 1471
    :cond_1
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->C(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 1472
    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Les;

    move-result-object v0

    invoke-virtual {v0}, Les;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1478
    :cond_2
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->f(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1484
    new-instance v0, Lahc;

    iget-object v1, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 1485
    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->k(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lyj;

    move-result-object v2

    invoke-direct {v0, v1, v2, v3}, Lahc;-><init>(Ljava/lang/String;Lyj;I)V

    .line 1486
    iput-boolean v3, v0, Lahc;->b:Z

    .line 1487
    iget-object v1, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->X()Z

    move-result v1

    iput-boolean v1, v0, Lahc;->h:Z

    .line 1488
    iget-object v1, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v1, v0, v3}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lahc;Z)V

    goto :goto_0
.end method

.method public l_()V
    .locals 1

    .prologue
    .line 1364
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->O()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->F()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1374
    :cond_0
    :goto_0
    return-void

    .line 1369
    :cond_1
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->w(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    .line 1373
    iget-object v0, p0, Lahw;->a:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    iget-object v0, v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g:Lbai;

    check-cast v0, Laad;

    invoke-virtual {v0}, Laad;->notifyDataSetChanged()V

    goto :goto_0
.end method
