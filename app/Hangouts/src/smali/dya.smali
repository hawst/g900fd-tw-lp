.class public final Ldya;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldya;


# instance fields
.field public b:Ljava/lang/Long;

.field public c:Ldyd;

.field public d:Ljava/lang/String;

.field public e:Ldyc;

.field public f:Ldxx;

.field public g:Ldxw;

.field public h:Ldyn;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    new-array v0, v0, [Ldya;

    sput-object v0, Ldya;->a:[Ldya;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-direct {p0}, Lepn;-><init>()V

    .line 42
    iput-object v0, p0, Ldya;->c:Ldyd;

    .line 47
    iput-object v0, p0, Ldya;->e:Ldyc;

    .line 50
    iput-object v0, p0, Ldya;->f:Ldxx;

    .line 53
    iput-object v0, p0, Ldya;->g:Ldxw;

    .line 56
    iput-object v0, p0, Ldya;->h:Ldyn;

    .line 37
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 88
    const/4 v0, 0x0

    .line 89
    iget-object v1, p0, Ldya;->b:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 90
    const/4 v0, 0x1

    iget-object v1, p0, Ldya;->b:Ljava/lang/Long;

    .line 91
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lepl;->e(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 93
    :cond_0
    iget-object v1, p0, Ldya;->c:Ldyd;

    if-eqz v1, :cond_1

    .line 94
    const/4 v1, 0x2

    iget-object v2, p0, Ldya;->c:Ldyd;

    .line 95
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 97
    :cond_1
    iget-object v1, p0, Ldya;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 98
    const/4 v1, 0x3

    iget-object v2, p0, Ldya;->d:Ljava/lang/String;

    .line 99
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 101
    :cond_2
    iget-object v1, p0, Ldya;->e:Ldyc;

    if-eqz v1, :cond_3

    .line 102
    const/4 v1, 0x4

    iget-object v2, p0, Ldya;->e:Ldyc;

    .line 103
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 105
    :cond_3
    iget-object v1, p0, Ldya;->f:Ldxx;

    if-eqz v1, :cond_4

    .line 106
    const/4 v1, 0x5

    iget-object v2, p0, Ldya;->f:Ldxx;

    .line 107
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 109
    :cond_4
    iget-object v1, p0, Ldya;->g:Ldxw;

    if-eqz v1, :cond_5

    .line 110
    const/4 v1, 0x6

    iget-object v2, p0, Ldya;->g:Ldxw;

    .line 111
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 113
    :cond_5
    iget-object v1, p0, Ldya;->h:Ldyn;

    if-eqz v1, :cond_6

    .line 114
    const/4 v1, 0x7

    iget-object v2, p0, Ldya;->h:Ldyn;

    .line 115
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 117
    :cond_6
    iget-object v1, p0, Ldya;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 118
    iput v0, p0, Ldya;->cachedSize:I

    .line 119
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 33
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldya;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldya;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldya;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldya;->b:Ljava/lang/Long;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldya;->c:Ldyd;

    if-nez v0, :cond_2

    new-instance v0, Ldyd;

    invoke-direct {v0}, Ldyd;-><init>()V

    iput-object v0, p0, Ldya;->c:Ldyd;

    :cond_2
    iget-object v0, p0, Ldya;->c:Ldyd;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldya;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldya;->e:Ldyc;

    if-nez v0, :cond_3

    new-instance v0, Ldyc;

    invoke-direct {v0}, Ldyc;-><init>()V

    iput-object v0, p0, Ldya;->e:Ldyc;

    :cond_3
    iget-object v0, p0, Ldya;->e:Ldyc;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ldya;->f:Ldxx;

    if-nez v0, :cond_4

    new-instance v0, Ldxx;

    invoke-direct {v0}, Ldxx;-><init>()V

    iput-object v0, p0, Ldya;->f:Ldxx;

    :cond_4
    iget-object v0, p0, Ldya;->f:Ldxx;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Ldya;->g:Ldxw;

    if-nez v0, :cond_5

    new-instance v0, Ldxw;

    invoke-direct {v0}, Ldxw;-><init>()V

    iput-object v0, p0, Ldya;->g:Ldxw;

    :cond_5
    iget-object v0, p0, Ldya;->g:Ldxw;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Ldya;->h:Ldyn;

    if-nez v0, :cond_6

    new-instance v0, Ldyn;

    invoke-direct {v0}, Ldyn;-><init>()V

    iput-object v0, p0, Ldya;->h:Ldyn;

    :cond_6
    iget-object v0, p0, Ldya;->h:Ldyn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 61
    iget-object v0, p0, Ldya;->b:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 62
    const/4 v0, 0x1

    iget-object v1, p0, Ldya;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 64
    :cond_0
    iget-object v0, p0, Ldya;->c:Ldyd;

    if-eqz v0, :cond_1

    .line 65
    const/4 v0, 0x2

    iget-object v1, p0, Ldya;->c:Ldyd;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 67
    :cond_1
    iget-object v0, p0, Ldya;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 68
    const/4 v0, 0x3

    iget-object v1, p0, Ldya;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 70
    :cond_2
    iget-object v0, p0, Ldya;->e:Ldyc;

    if-eqz v0, :cond_3

    .line 71
    const/4 v0, 0x4

    iget-object v1, p0, Ldya;->e:Ldyc;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 73
    :cond_3
    iget-object v0, p0, Ldya;->f:Ldxx;

    if-eqz v0, :cond_4

    .line 74
    const/4 v0, 0x5

    iget-object v1, p0, Ldya;->f:Ldxx;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 76
    :cond_4
    iget-object v0, p0, Ldya;->g:Ldxw;

    if-eqz v0, :cond_5

    .line 77
    const/4 v0, 0x6

    iget-object v1, p0, Ldya;->g:Ldxw;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 79
    :cond_5
    iget-object v0, p0, Ldya;->h:Ldyn;

    if-eqz v0, :cond_6

    .line 80
    const/4 v0, 0x7

    iget-object v1, p0, Ldya;->h:Ldyn;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 82
    :cond_6
    iget-object v0, p0, Ldya;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 84
    return-void
.end method
