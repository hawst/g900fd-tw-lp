.class abstract Lbat;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TT;>;"
    }
.end annotation


# instance fields
.field b:I

.field c:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 490
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 491
    sget v0, Lbau;->b:I

    iput v0, p0, Lbat;->b:I

    .line 493
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 490
    invoke-direct {p0}, Lbat;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract a()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method b()Z
    .locals 2

    .prologue
    .line 523
    sget v0, Lbau;->d:I

    iput v0, p0, Lbat;->b:I

    .line 524
    invoke-virtual {p0}, Lbat;->a()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lbat;->c:Ljava/lang/Object;

    .line 525
    iget v0, p0, Lbat;->b:I

    sget v1, Lbau;->c:I

    if-eq v0, v1, :cond_0

    .line 526
    sget v0, Lbau;->a:I

    iput v0, p0, Lbat;->b:I

    .line 527
    const/4 v0, 0x1

    .line 529
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasNext()Z
    .locals 2

    .prologue
    .line 508
    iget v0, p0, Lbat;->b:I

    sget v1, Lbau;->d:I

    if-ne v0, v1, :cond_0

    .line 509
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 512
    :cond_0
    sget-object v0, Lban;->a:[I

    iget v1, p0, Lbat;->b:I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 519
    invoke-virtual {p0}, Lbat;->b()Z

    move-result v0

    :goto_0
    return v0

    .line 514
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 516
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 512
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final next()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 534
    invoke-virtual {p0}, Lbat;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 535
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 537
    :cond_0
    sget v0, Lbau;->b:I

    iput v0, p0, Lbat;->b:I

    .line 538
    iget-object v0, p0, Lbat;->c:Ljava/lang/Object;

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 543
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
