.class public final Lsy;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ltl;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ltc;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ltn;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ltk;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ltg;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ltq;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ltj;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lth;

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ltm;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lto;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lti;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsz;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ltb;

.field private o:Lta;

.field private final p:I

.field private final q:Landroid/accounts/Account;

.field private r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsy;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x0

    .line 72
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 75
    sput-object v0, Lsy;->h:Ljava/util/Map;

    const-string v1, "X-AIM"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    sget-object v0, Lsy;->h:Ljava/util/Map;

    const-string v1, "X-MSN"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    sget-object v0, Lsy;->h:Ljava/util/Map;

    const-string v1, "X-YAHOO"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    sget-object v0, Lsy;->h:Ljava/util/Map;

    const-string v1, "X-ICQ"

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    sget-object v0, Lsy;->h:Ljava/util/Map;

    const-string v1, "X-JABBER"

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    sget-object v0, Lsy;->h:Ljava/util/Map;

    const-string v1, "X-SKYPE-USERNAME"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    sget-object v0, Lsy;->h:Ljava/util/Map;

    const-string v1, "X-GOOGLE-TALK"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    sget-object v0, Lsy;->h:Ljava/util/Map;

    const-string v1, "X-GOOGLE TALK"

    .line 83
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 82
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1848
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 1849
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lsy;->s:Ljava/util/List;

    .line 1848
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1753
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lsy;-><init>(B)V

    .line 1754
    return-void
.end method

.method private constructor <init>(B)V
    .locals 2

    .prologue
    .line 1757
    const/high16 v0, -0x40000000    # -2.0f

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lsy;-><init>(ILandroid/accounts/Account;)V

    .line 1758
    return-void
.end method

.method public constructor <init>(ILandroid/accounts/Account;)V
    .locals 1

    .prologue
    .line 1760
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1541
    new-instance v0, Lth;

    invoke-direct {v0}, Lth;-><init>()V

    iput-object v0, p0, Lsy;->i:Lth;

    .line 1761
    iput p1, p0, Lsy;->p:I

    .line 1762
    iput-object p2, p0, Lsy;->q:Landroid/accounts/Account;

    .line 1763
    return-void
.end method

.method private a(ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1766
    iget-object v2, p0, Lsy;->a:Ljava/util/List;

    if-nez v2, :cond_0

    .line 1767
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lsy;->a:Ljava/util/List;

    .line 1769
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 1770
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 1772
    const/4 v3, 0x6

    if-eq p1, v3, :cond_1

    iget v3, p0, Lsy;->p:I

    invoke-static {v3}, Lsx;->f(I)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    move-object v0, v2

    .line 1802
    :goto_0
    new-instance v1, Ltl;

    invoke-direct {v1, v0, p1, p3, p4}, Ltl;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    .line 1803
    iget-object v0, p0, Lsy;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1804
    return-void

    .line 1780
    :cond_2
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    move v3, v0

    .line 1781
    :goto_1
    if-ge v3, v5, :cond_a

    .line 1782
    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v6

    .line 1784
    const/16 v7, 0x70

    if-eq v6, v7, :cond_3

    const/16 v7, 0x50

    if-ne v6, v7, :cond_5

    .line 1785
    :cond_3
    const/16 v0, 0x2c

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v0, v1

    .line 1781
    :cond_4
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1787
    :cond_5
    const/16 v7, 0x77

    if-eq v6, v7, :cond_6

    const/16 v7, 0x57

    if-ne v6, v7, :cond_7

    .line 1788
    :cond_6
    const/16 v0, 0x3b

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v0, v1

    .line 1789
    goto :goto_2

    .line 1790
    :cond_7
    const/16 v7, 0x30

    if-gt v7, v6, :cond_8

    const/16 v7, 0x39

    if-le v6, v7, :cond_9

    :cond_8
    if-nez v3, :cond_4

    const/16 v7, 0x2b

    if-ne v6, v7, :cond_4

    .line 1791
    :cond_9
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 1794
    :cond_a
    if-nez v0, :cond_b

    .line 1795
    iget v0, p0, Lsy;->p:I

    invoke-static {v0}, Lud;->a(I)I

    move-result v0

    .line 1797
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1796
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-static {v2, v0}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Landroid/text/Editable;I)V

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1799
    :cond_b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 1945
    iget-object v0, p0, Lsy;->d:Ljava/util/List;

    if-nez v0, :cond_0

    move-object v0, p0

    move-object v2, v1

    move-object v3, p1

    move-object v4, v1

    .line 1948
    invoke-direct/range {v0 .. v5}, Lsy;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1960
    :goto_0
    return-void

    .line 1951
    :cond_0
    iget-object v0, p0, Lsy;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltk;

    .line 1952
    iget-object v3, v0, Ltk;->c:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 1953
    iput-object p1, v0, Ltk;->c:Ljava/lang/String;

    goto :goto_0

    :cond_2
    move-object v0, p0

    move-object v2, v1

    move-object v3, p1

    move-object v4, v1

    .line 1959
    invoke-direct/range {v0 .. v5}, Lsy;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7

    .prologue
    .line 1841
    iget-object v0, p0, Lsy;->d:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1842
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsy;->d:Ljava/util/List;

    .line 1844
    :cond_0
    iget-object v6, p0, Lsy;->d:Ljava/util/List;

    new-instance v0, Ltk;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Ltk;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1846
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/Collection;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 2416
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2452
    :cond_0
    :goto_0
    return-void

    .line 2419
    :cond_1
    const-string v0, "sip:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2420
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 2421
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 2426
    :cond_2
    const/4 v3, -0x1

    .line 2427
    const/4 v1, 0x0

    .line 2429
    if-eqz p2, :cond_b

    .line 2430
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v4, v2

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2431
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v8

    .line 2432
    const-string v9, "PREF"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    move v4, v5

    .line 2433
    goto :goto_1

    .line 2434
    :cond_3
    const-string v9, "HOME"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    move v3, v5

    .line 2435
    goto :goto_1

    .line 2436
    :cond_4
    const-string v9, "WORK"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    move v3, v6

    .line 2437
    goto :goto_1

    .line 2438
    :cond_5
    if-gez v3, :cond_a

    .line 2439
    const-string v1, "X-"

    invoke-virtual {v8, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2440
    invoke-virtual {v0, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :cond_6
    move v1, v2

    :goto_2
    move v3, v1

    move-object v1, v0

    .line 2446
    goto :goto_1

    :cond_7
    move v0, v3

    .line 2448
    :goto_3
    if-gez v0, :cond_8

    .line 2449
    const/4 v0, 0x3

    .line 2451
    :cond_8
    iget-object v2, p0, Lsy;->k:Ljava/util/List;

    if-nez v2, :cond_9

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lsy;->k:Ljava/util/List;

    :cond_9
    iget-object v2, p0, Lsy;->k:Ljava/util/List;

    new-instance v3, Lto;

    invoke-direct {v3, p1, v0, v1, v4}, Lto;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_a
    move-object v0, v1

    move v1, v3

    goto :goto_2

    :cond_b
    move v4, v2

    move v0, v3

    goto :goto_3
.end method

.method private a(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2065
    iget-object v0, p0, Lsy;->i:Lth;

    iget-object v0, v0, Lth;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lsy;->i:Lth;

    .line 2066
    iget-object v0, v0, Lth;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lsy;->i:Lth;

    .line 2067
    iget-object v0, v0, Lth;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2122
    :cond_0
    :goto_0
    return-void

    .line 2074
    :cond_1
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 2081
    if-le v0, v2, :cond_7

    move v1, v2

    .line 2085
    :goto_1
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_5

    move v5, v4

    .line 2087
    :goto_2
    if-ge v5, v1, :cond_6

    .line 2088
    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    move v0, v3

    .line 2093
    :goto_3
    if-eqz v0, :cond_5

    .line 2094
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 2095
    array-length v1, v0

    .line 2096
    if-ne v1, v2, :cond_3

    .line 2098
    iget-object v1, p0, Lsy;->i:Lth;

    aget-object v2, v0, v3

    iput-object v2, v1, Lth;->g:Ljava/lang/String;

    .line 2099
    iget-object v1, p0, Lsy;->i:Lth;

    aget-object v2, v0, v4

    iput-object v2, v1, Lth;->i:Ljava/lang/String;

    .line 2100
    iget-object v1, p0, Lsy;->i:Lth;

    aget-object v0, v0, v6

    iput-object v0, v1, Lth;->h:Ljava/lang/String;

    goto :goto_0

    .line 2087
    :cond_2
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_2

    .line 2101
    :cond_3
    if-ne v1, v6, :cond_4

    .line 2104
    iget-object v1, p0, Lsy;->i:Lth;

    aget-object v2, v0, v3

    iput-object v2, v1, Lth;->g:Ljava/lang/String;

    .line 2105
    iget-object v1, p0, Lsy;->i:Lth;

    aget-object v0, v0, v4

    iput-object v0, v1, Lth;->h:Ljava/lang/String;

    goto :goto_0

    .line 2107
    :cond_4
    iget-object v1, p0, Lsy;->i:Lth;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lth;->h:Ljava/lang/String;

    goto :goto_0

    .line 2113
    :cond_5
    packed-switch v1, :pswitch_data_0

    .line 2120
    :goto_4
    iget-object v1, p0, Lsy;->i:Lth;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lth;->g:Ljava/lang/String;

    goto :goto_0

    .line 2116
    :pswitch_0
    iget-object v1, p0, Lsy;->i:Lth;

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lth;->i:Ljava/lang/String;

    .line 2118
    :pswitch_1
    iget-object v1, p0, Lsy;->i:Lth;

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lth;->h:Ljava/lang/String;

    goto :goto_4

    :cond_6
    move v0, v4

    goto :goto_3

    :cond_7
    move v1, v0

    goto/16 :goto_1

    .line 2113
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private a(Ljava/util/List;Ljava/util/Map;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;>;Z)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 1880
    const-string v0, "SORT-AS"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    if-eqz v0, :cond_4

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    if-le v1, v5, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Incorrect multiple SORT_AS parameters detected: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget v1, p0, Lsy;->p:I

    invoke-static {v0, v1}, Lud;->a(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1881
    :goto_1
    if-nez p1, :cond_2

    .line 1882
    sget-object p1, Lsy;->s:Ljava/util/List;

    .line 1886
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    .line 1887
    packed-switch v6, :pswitch_data_0

    .line 1899
    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1902
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move v2, v5

    .line 1903
    :goto_2
    if-ge v2, v6, :cond_5

    .line 1904
    if-le v2, v5, :cond_3

    .line 1905
    const/16 v1, 0x20

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1907
    :cond_3
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1903
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    :cond_4
    move-object v4, v3

    .line 1880
    goto :goto_1

    .line 1889
    :pswitch_0
    const-string v0, ""

    move-object v2, v3

    move-object v1, v0

    .line 1912
    :goto_3
    iget-object v0, p0, Lsy;->d:Ljava/util/List;

    if-nez v0, :cond_6

    move-object v0, p0

    move v5, p3

    .line 1915
    invoke-direct/range {v0 .. v5}, Lsy;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1935
    :goto_4
    return-void

    .line 1894
    :pswitch_1
    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v2, v3

    move-object v1, v0

    .line 1896
    goto :goto_3

    .line 1909
    :cond_5
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v1, v0

    goto :goto_3

    .line 1919
    :cond_6
    iget-object v0, p0, Lsy;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_7
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltk;

    .line 1922
    iget-object v6, v0, Ltk;->a:Ljava/lang/String;

    if-nez v6, :cond_7

    .line 1923
    iget-object v6, v0, Ltk;->b:Ljava/lang/String;

    if-nez v6, :cond_7

    .line 1926
    iput-object v1, v0, Ltk;->a:Ljava/lang/String;

    .line 1927
    iput-object v2, v0, Ltk;->b:Ljava/lang/String;

    .line 1928
    iput-boolean p3, v0, Ltk;->d:Z

    goto :goto_4

    :cond_8
    move-object v0, p0

    move v5, p3

    .line 1934
    invoke-direct/range {v0 .. v5}, Lsy;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_4

    .line 1887
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Ljava/util/List;Lte;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Ltd;",
            ">;",
            "Lte;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1617
    if-eqz p0, :cond_1

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 1618
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltd;

    invoke-interface {v0}, Ltd;->a()Ltf;

    move-result-object v0

    invoke-interface {p1, v0}, Lte;->a(Ltf;)V

    .line 1619
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltd;

    .line 1620
    invoke-interface {p1, v0}, Lte;->a(Ltd;)Z

    goto :goto_0

    .line 1622
    :cond_0
    invoke-interface {p1}, Lte;->c()V

    .line 1624
    :cond_1
    return-void
.end method

.method private d()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2473
    const/4 v0, 0x0

    .line 2475
    iget-object v1, p0, Lsy;->i:Lth;

    iget-object v1, v1, Lth;->f:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2476
    iget-object v0, p0, Lsy;->i:Lth;

    iget-object v0, v0, Lth;->f:Ljava/lang/String;

    .line 2492
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 2493
    const-string v0, ""

    .line 2495
    :cond_1
    return-object v0

    .line 2477
    :cond_2
    iget-object v1, p0, Lsy;->i:Lth;

    invoke-virtual {v1}, Lth;->b()Z

    move-result v1

    if-nez v1, :cond_3

    .line 2478
    iget v0, p0, Lsy;->p:I

    iget-object v1, p0, Lsy;->i:Lth;

    iget-object v1, v1, Lth;->a:Ljava/lang/String;

    iget-object v2, p0, Lsy;->i:Lth;

    .line 2479
    iget-object v2, v2, Lth;->c:Ljava/lang/String;

    iget-object v3, p0, Lsy;->i:Lth;

    iget-object v3, v3, Lth;->b:Ljava/lang/String;

    iget-object v4, p0, Lsy;->i:Lth;

    iget-object v4, v4, Lth;->d:Ljava/lang/String;

    iget-object v5, p0, Lsy;->i:Lth;

    iget-object v5, v5, Lth;->e:Ljava/lang/String;

    .line 2478
    invoke-static/range {v0 .. v5}, Lud;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2480
    :cond_3
    iget-object v1, p0, Lsy;->i:Lth;

    invoke-virtual {v1}, Lth;->c()Z

    move-result v1

    if-nez v1, :cond_4

    .line 2481
    iget v0, p0, Lsy;->p:I

    iget-object v1, p0, Lsy;->i:Lth;

    .line 2482
    iget-object v1, v1, Lth;->g:Ljava/lang/String;

    iget-object v2, p0, Lsy;->i:Lth;

    iget-object v2, v2, Lth;->i:Ljava/lang/String;

    iget-object v3, p0, Lsy;->i:Lth;

    iget-object v3, v3, Lth;->h:Ljava/lang/String;

    .line 2481
    invoke-static {v0, v1, v2, v3}, Lud;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2483
    :cond_4
    iget-object v1, p0, Lsy;->b:Ljava/util/List;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lsy;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_5

    .line 2484
    iget-object v0, p0, Lsy;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltc;

    iget-object v0, v0, Ltc;->a:Ljava/lang/String;

    goto :goto_0

    .line 2485
    :cond_5
    iget-object v1, p0, Lsy;->a:Ljava/util/List;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lsy;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_6

    .line 2486
    iget-object v0, p0, Lsy;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltl;

    iget-object v0, v0, Ltl;->a:Ljava/lang/String;

    goto :goto_0

    .line 2487
    :cond_6
    iget-object v1, p0, Lsy;->c:Ljava/util/List;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lsy;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_7

    .line 2488
    iget-object v0, p0, Lsy;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltn;

    iget v1, p0, Lsy;->p:I

    invoke-virtual {v0, v1}, Ltn;->a(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2489
    :cond_7
    iget-object v1, p0, Lsy;->d:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lsy;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 2490
    iget-object v0, p0, Lsy;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltk;

    invoke-virtual {v0}, Ltk;->b()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 2502
    iget-object v0, p0, Lsy;->i:Lth;

    invoke-direct {p0}, Lsy;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lth;->k:Ljava/lang/String;

    .line 2503
    return-void
.end method

.method public a(Lsy;)V
    .locals 1

    .prologue
    .line 2455
    iget-object v0, p0, Lsy;->r:Ljava/util/List;

    if-nez v0, :cond_0

    .line 2456
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsy;->r:Ljava/util/List;

    .line 2458
    :cond_0
    iget-object v0, p0, Lsy;->r:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2459
    return-void
.end method

.method public a(Lub;)V
    .locals 13

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 2125
    invoke-virtual {p1}, Lub;->a()Ljava/lang/String;

    move-result-object v5

    .line 2126
    invoke-virtual {p1}, Lub;->b()Ljava/util/Map;

    move-result-object v6

    .line 2127
    invoke-virtual {p1}, Lub;->d()Ljava/util/List;

    move-result-object v9

    .line 2128
    invoke-virtual {p1}, Lub;->e()[B

    move-result-object v10

    .line 2130
    if-eqz v9, :cond_0

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    if-nez v10, :cond_2

    .line 2409
    :cond_1
    :goto_0
    return-void

    .line 2134
    :cond_2
    if-eqz v9, :cond_7

    .line 2135
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v4, :cond_5

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_3
    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v1, -0x1

    if-lez v0, :cond_3

    const-string v0, ";"

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_4
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2138
    :goto_3
    const-string v0, "VERSION"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2140
    const-string v0, "FN"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2141
    iget-object v0, p0, Lsy;->i:Lth;

    iput-object v1, v0, Lth;->f:Ljava/lang/String;

    goto :goto_0

    .line 2135
    :cond_5
    if-ne v1, v4, :cond_6

    invoke-interface {v9, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_2

    :cond_6
    const-string v0, ""

    goto :goto_2

    :cond_7
    move-object v1, v2

    goto :goto_3

    .line 2142
    :cond_8
    const-string v0, "NAME"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2145
    iget-object v0, p0, Lsy;->i:Lth;

    iget-object v0, v0, Lth;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2146
    iget-object v0, p0, Lsy;->i:Lth;

    iput-object v1, v0, Lth;->f:Ljava/lang/String;

    goto :goto_0

    .line 2148
    :cond_9
    const-string v0, "N"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2149
    iget v0, p0, Lsy;->p:I

    invoke-static {v0}, Lsx;->b(I)Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lsy;->i:Lth;

    iget-object v0, v0, Lth;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lsy;->i:Lth;

    iget-object v0, v0, Lth;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lsy;->i:Lth;

    iget-object v0, v0, Lth;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c

    :cond_a
    :goto_4
    if-eqz v9, :cond_1

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v1, 0x5

    if-le v0, v1, :cond_b

    const/4 v0, 0x5

    :cond_b
    packed-switch v0, :pswitch_data_0

    :goto_5
    iget-object v1, p0, Lsy;->i:Lth;

    invoke-interface {v9, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lth;->a:Ljava/lang/String;

    goto/16 :goto_0

    :cond_c
    const-string v0, "SORT-AS"

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    if-eqz v0, :cond_a

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    if-le v1, v4, :cond_d

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Incorrect multiple SORT_AS parameters detected: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_d
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget v1, p0, Lsy;->p:I

    invoke-static {v0, v1}, Lud;->a(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v8, :cond_e

    move v0, v8

    :cond_e
    packed-switch v0, :pswitch_data_1

    :goto_6
    iget-object v2, p0, Lsy;->i:Lth;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lth;->g:Ljava/lang/String;

    goto :goto_4

    :pswitch_0
    iget-object v2, p0, Lsy;->i:Lth;

    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lth;->i:Ljava/lang/String;

    :pswitch_1
    iget-object v2, p0, Lsy;->i:Lth;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lth;->h:Ljava/lang/String;

    goto :goto_6

    :pswitch_2
    iget-object v1, p0, Lsy;->i:Lth;

    const/4 v0, 0x4

    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lth;->e:Ljava/lang/String;

    :pswitch_3
    iget-object v1, p0, Lsy;->i:Lth;

    invoke-interface {v9, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lth;->d:Ljava/lang/String;

    :pswitch_4
    iget-object v1, p0, Lsy;->i:Lth;

    invoke-interface {v9, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lth;->c:Ljava/lang/String;

    :pswitch_5
    iget-object v1, p0, Lsy;->i:Lth;

    invoke-interface {v9, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lth;->b:Ljava/lang/String;

    goto/16 :goto_5

    .line 2150
    :cond_f
    const-string v0, "SORT-STRING"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 2151
    iget-object v0, p0, Lsy;->i:Lth;

    iput-object v1, v0, Lth;->j:Ljava/lang/String;

    goto/16 :goto_0

    .line 2152
    :cond_10
    const-string v0, "NICKNAME"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    const-string v0, "X-NICKNAME"

    .line 2153
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 2154
    :cond_11
    iget-object v0, p0, Lsy;->l:Ljava/util/List;

    if-nez v0, :cond_12

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsy;->l:Ljava/util/List;

    :cond_12
    iget-object v0, p0, Lsy;->l:Ljava/util/List;

    new-instance v2, Lti;

    invoke-direct {v2, v1}, Lti;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2155
    :cond_13
    const-string v0, "SOUND"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 2156
    const-string v0, "TYPE"

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 2157
    if-eqz v0, :cond_1

    const-string v2, "X-IRMC-N"

    .line 2158
    invoke-interface {v0, v2}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2163
    iget v0, p0, Lsy;->p:I

    invoke-static {v1, v0}, Lud;->a(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v0

    .line 2165
    invoke-direct {p0, v0}, Lsy;->a(Ljava/util/List;)V

    goto/16 :goto_0

    .line 2169
    :cond_14
    const-string v0, "ADR"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 2171
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_15
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_56

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2172
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_15

    move v0, v3

    .line 2177
    :goto_7
    if-nez v0, :cond_1

    .line 2181
    const/4 v1, -0x1

    .line 2184
    const-string v0, "TYPE"

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 2185
    if-eqz v0, :cond_55

    .line 2186
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move-object v5, v2

    move v6, v1

    move v1, v3

    :goto_8
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1b

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2187
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    .line 2188
    const-string v11, "PREF"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_16

    move v1, v4

    .line 2189
    goto :goto_8

    .line 2190
    :cond_16
    const-string v11, "HOME"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_17

    move-object v5, v2

    move v6, v4

    .line 2192
    goto :goto_8

    .line 2193
    :cond_17
    const-string v11, "WORK"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_18

    const-string v11, "COMPANY"

    .line 2195
    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_19

    :cond_18
    move-object v5, v2

    move v6, v7

    .line 2200
    goto :goto_8

    .line 2201
    :cond_19
    const-string v11, "PARCEL"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_54

    const-string v11, "DOM"

    .line 2202
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_54

    const-string v11, "INTL"

    .line 2203
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_54

    .line 2205
    if-gez v6, :cond_54

    .line 2207
    const-string v5, "X-"

    invoke-virtual {v10, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1a

    .line 2208
    invoke-virtual {v0, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    move-object v5, v0

    move v6, v3

    goto :goto_8

    :cond_1a
    move v5, v3

    :goto_9
    move v6, v5

    move-object v5, v0

    .line 2213
    goto :goto_8

    :cond_1b
    move v0, v6

    .line 2216
    :goto_a
    if-gez v0, :cond_53

    .line 2220
    :goto_b
    iget-object v0, p0, Lsy;->c:Ljava/util/List;

    if-nez v0, :cond_1c

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lsy;->c:Ljava/util/List;

    :cond_1c
    iget-object v0, p0, Lsy;->c:Ljava/util/List;

    iget v2, p0, Lsy;->p:I

    invoke-static {v9, v4, v5, v1, v2}, Ltn;->a(Ljava/util/List;ILjava/lang/String;ZI)Ltn;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2221
    :cond_1d
    const-string v0, "EMAIL"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 2222
    const/4 v5, -0x1

    .line 2225
    const-string v0, "TYPE"

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 2226
    if-eqz v0, :cond_52

    .line 2227
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v6, v3

    :goto_c
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_23

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2228
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    .line 2229
    const-string v11, "PREF"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1e

    move v6, v4

    .line 2230
    goto :goto_c

    .line 2231
    :cond_1e
    const-string v11, "HOME"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1f

    move v5, v4

    .line 2232
    goto :goto_c

    .line 2233
    :cond_1f
    const-string v11, "WORK"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_20

    move v5, v7

    .line 2234
    goto :goto_c

    .line 2235
    :cond_20
    const-string v11, "CELL"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_21

    .line 2236
    const/4 v5, 0x4

    goto :goto_c

    .line 2237
    :cond_21
    if-gez v5, :cond_51

    .line 2238
    const-string v2, "X-"

    invoke-virtual {v10, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_22

    .line 2239
    invoke-virtual {v0, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :cond_22
    move v2, v3

    :goto_d
    move v5, v2

    move-object v2, v0

    .line 2245
    goto :goto_c

    :cond_23
    move v0, v5

    .line 2247
    :goto_e
    if-gez v0, :cond_24

    move v0, v8

    .line 2250
    :cond_24
    iget-object v3, p0, Lsy;->b:Ljava/util/List;

    if-nez v3, :cond_25

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lsy;->b:Ljava/util/List;

    :cond_25
    iget-object v3, p0, Lsy;->b:Ljava/util/List;

    new-instance v4, Ltc;

    invoke-direct {v4, v1, v0, v2, v6}, Ltc;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2251
    :cond_26
    const-string v0, "ORG"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 2255
    const-string v0, "TYPE"

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 2256
    if-eqz v0, :cond_28

    .line 2257
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_27
    :goto_f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_28

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2258
    const-string v2, "PREF"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_27

    move v3, v4

    .line 2259
    goto :goto_f

    .line 2263
    :cond_28
    invoke-direct {p0, v9, v6, v3}, Lsy;->a(Ljava/util/List;Ljava/util/Map;Z)V

    goto/16 :goto_0

    .line 2264
    :cond_29
    const-string v0, "TITLE"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 2265
    invoke-direct {p0, v1}, Lsy;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2266
    :cond_2a
    const-string v0, "ROLE"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2269
    const-string v0, "PHOTO"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2b

    const-string v0, "LOGO"

    .line 2270
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 2271
    :cond_2b
    const-string v0, "VALUE"

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 2272
    if-eqz v0, :cond_2c

    const-string v1, "URL"

    invoke-interface {v0, v1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2275
    :cond_2c
    const-string v0, "TYPE"

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 2278
    if-eqz v0, :cond_2e

    .line 2279
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v1, v2

    :goto_10
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2f

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2280
    const-string v2, "PREF"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2d

    move v3, v4

    .line 2281
    goto :goto_10

    .line 2282
    :cond_2d
    if-nez v1, :cond_50

    :goto_11
    move-object v1, v0

    .line 2285
    goto :goto_10

    :cond_2e
    move-object v1, v2

    .line 2287
    :cond_2f
    iget-object v0, p0, Lsy;->j:Ljava/util/List;

    if-nez v0, :cond_30

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lsy;->j:Ljava/util/List;

    :cond_30
    new-instance v0, Ltm;

    invoke-direct {v0, v1, v10, v3}, Ltm;-><init>(Ljava/lang/String;[BZ)V

    iget-object v1, p0, Lsy;->j:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2289
    :cond_31
    const-string v0, "TEL"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_38

    .line 2292
    iget v0, p0, Lsy;->p:I

    invoke-static {v0}, Lsx;->c(I)Z

    move-result v0

    if-eqz v0, :cond_34

    .line 2295
    const-string v0, "sip:"

    invoke-virtual {v1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_32

    move v0, v4

    move-object v5, v2

    .line 2309
    :goto_12
    if-eqz v0, :cond_35

    .line 2310
    const-string v0, "TYPE"

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 2311
    invoke-direct {p0, v1, v0}, Lsy;->a(Ljava/lang/String;Ljava/util/Collection;)V

    goto/16 :goto_0

    .line 2297
    :cond_32
    const-string v0, "tel:"

    invoke-virtual {v1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 2298
    const/4 v0, 0x4

    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    move-object v5, v0

    move v0, v3

    goto :goto_12

    :cond_33
    move v0, v3

    move-object v5, v1

    .line 2303
    goto :goto_12

    :cond_34
    move v0, v3

    move-object v5, v1

    .line 2306
    goto :goto_12

    .line 2313
    :cond_35
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_1

    .line 2317
    const-string v0, "TYPE"

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 2318
    invoke-static {v0, v5}, Lud;->a(Ljava/util/Collection;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 2322
    instance-of v6, v1, Ljava/lang/Integer;

    if-eqz v6, :cond_36

    .line 2323
    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 2331
    :goto_13
    if-eqz v0, :cond_37

    const-string v6, "PREF"

    .line 2332
    invoke-interface {v0, v6}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 2338
    :goto_14
    invoke-direct {p0, v1, v5, v2, v4}, Lsy;->a(ILjava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 2327
    :cond_36
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    move v1, v3

    goto :goto_13

    :cond_37
    move v4, v3

    .line 2335
    goto :goto_14

    .line 2340
    :cond_38
    const-string v0, "X-SKYPE-PSTNNUMBER"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 2342
    const-string v0, "TYPE"

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 2343
    if-eqz v0, :cond_39

    const-string v5, "PREF"

    .line 2346
    invoke-interface {v0, v5}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 2351
    :goto_15
    const/4 v0, 0x7

    invoke-direct {p0, v0, v1, v2, v4}, Lsy;->a(ILjava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_0

    :cond_39
    move v4, v3

    .line 2349
    goto :goto_15

    .line 2352
    :cond_3a
    sget-object v0, Lsy;->h:Ljava/util/Map;

    invoke-interface {v0, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 2353
    sget-object v0, Lsy;->h:Ljava/util/Map;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 2355
    const/4 v2, -0x1

    .line 2356
    const-string v0, "TYPE"

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 2357
    if-eqz v0, :cond_3d

    .line 2358
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_16
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3d

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2359
    const-string v8, "PREF"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3b

    move v3, v4

    .line 2360
    goto :goto_16

    .line 2361
    :cond_3b
    if-gez v2, :cond_4f

    .line 2362
    const-string v8, "HOME"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3c

    move v2, v4

    .line 2363
    goto :goto_16

    .line 2364
    :cond_3c
    const-string v8, "WORK"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4f

    move v0, v7

    :goto_17
    move v2, v0

    .line 2368
    goto :goto_16

    :cond_3d
    move v0, v2

    .line 2370
    if-gez v0, :cond_4e

    .line 2373
    :goto_18
    iget-object v0, p0, Lsy;->e:Ljava/util/List;

    if-nez v0, :cond_3e

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsy;->e:Ljava/util/List;

    :cond_3e
    iget-object v0, p0, Lsy;->e:Ljava/util/List;

    new-instance v2, Ltg;

    invoke-direct {v2, v5, v1, v4, v3}, Ltg;-><init>(ILjava/lang/String;IZ)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2374
    :cond_3f
    const-string v0, "NOTE"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_41

    .line 2375
    iget-object v0, p0, Lsy;->g:Ljava/util/List;

    if-nez v0, :cond_40

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lsy;->g:Ljava/util/List;

    :cond_40
    iget-object v0, p0, Lsy;->g:Ljava/util/List;

    new-instance v2, Ltj;

    invoke-direct {v2, v1}, Ltj;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2376
    :cond_41
    const-string v0, "URL"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_43

    .line 2377
    iget-object v0, p0, Lsy;->f:Ljava/util/List;

    if-nez v0, :cond_42

    .line 2378
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lsy;->f:Ljava/util/List;

    .line 2380
    :cond_42
    iget-object v0, p0, Lsy;->f:Ljava/util/List;

    new-instance v2, Ltq;

    invoke-direct {v2, v1}, Ltq;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2381
    :cond_43
    const-string v0, "BDAY"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_44

    .line 2382
    new-instance v0, Ltb;

    invoke-direct {v0, v1}, Ltb;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lsy;->n:Ltb;

    goto/16 :goto_0

    .line 2383
    :cond_44
    const-string v0, "ANNIVERSARY"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_45

    .line 2384
    new-instance v0, Lta;

    invoke-direct {v0, v1}, Lta;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lsy;->o:Lta;

    goto/16 :goto_0

    .line 2385
    :cond_45
    const-string v0, "X-PHONETIC-FIRST-NAME"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_46

    .line 2386
    iget-object v0, p0, Lsy;->i:Lth;

    iput-object v1, v0, Lth;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 2387
    :cond_46
    const-string v0, "X-PHONETIC-MIDDLE-NAME"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_47

    .line 2388
    iget-object v0, p0, Lsy;->i:Lth;

    iput-object v1, v0, Lth;->i:Ljava/lang/String;

    goto/16 :goto_0

    .line 2389
    :cond_47
    const-string v0, "X-PHONETIC-LAST-NAME"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_48

    .line 2390
    iget-object v0, p0, Lsy;->i:Lth;

    iput-object v1, v0, Lth;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 2391
    :cond_48
    const-string v0, "IMPP"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_49

    .line 2393
    const-string v0, "sip:"

    invoke-virtual {v1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2394
    const-string v0, "TYPE"

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 2395
    invoke-direct {p0, v1, v0}, Lsy;->a(Ljava/lang/String;Ljava/util/Collection;)V

    goto/16 :goto_0

    .line 2397
    :cond_49
    const-string v0, "X-SIP"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4a

    .line 2398
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2399
    const-string v0, "TYPE"

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 2400
    invoke-direct {p0, v1, v0}, Lsy;->a(Ljava/lang/String;Ljava/util/Collection;)V

    goto/16 :goto_0

    .line 2402
    :cond_4a
    const-string v0, "X-ANDROID-CUSTOM"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2403
    iget v0, p0, Lsy;->p:I

    invoke-static {v1, v0}, Lud;->a(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v5

    .line 2405
    iget-object v0, p0, Lsy;->m:Ljava/util/List;

    if-nez v0, :cond_4b

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsy;->m:Ljava/util/List;

    :cond_4b
    iget-object v6, p0, Lsy;->m:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ge v0, v7, :cond_4c

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_19
    new-instance v1, Lsz;

    invoke-direct {v1, v0, v2}, Lsz;-><init>(Ljava/lang/String;Ljava/util/List;)V

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_4c
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x10

    if-ge v0, v1, :cond_4d

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    move v1, v0

    :goto_1a
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v5, v4, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    goto :goto_19

    :cond_4d
    const/16 v0, 0x10

    move v1, v0

    goto :goto_1a

    :cond_4e
    move v4, v0

    goto/16 :goto_18

    :cond_4f
    move v0, v2

    goto/16 :goto_17

    :cond_50
    move-object v0, v1

    goto/16 :goto_11

    :cond_51
    move-object v0, v2

    move v2, v5

    goto/16 :goto_d

    :cond_52
    move v6, v3

    move v0, v5

    goto/16 :goto_e

    :cond_53
    move v4, v0

    goto/16 :goto_b

    :cond_54
    move-object v0, v5

    move v5, v6

    goto/16 :goto_9

    :cond_55
    move-object v5, v2

    move v0, v1

    move v1, v3

    goto/16 :goto_a

    :cond_56
    move v0, v4

    goto/16 :goto_7

    .line 2149
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2602
    iget-object v0, p0, Lsy;->n:Ltb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lsy;->n:Ltb;

    iget-object v0, v0, Ltb;->a:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2645
    iget-object v0, p0, Lsy;->i:Lth;

    iget-object v0, v0, Lth;->k:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2646
    iget-object v0, p0, Lsy;->i:Lth;

    invoke-direct {p0}, Lsy;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lth;->k:Ljava/lang/String;

    .line 2648
    :cond_0
    iget-object v0, p0, Lsy;->i:Lth;

    iget-object v0, v0, Lth;->k:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1747
    new-instance v0, Ltp;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Ltp;-><init>(Lsy;B)V

    .line 1748
    invoke-interface {v0}, Lte;->a()V

    iget-object v1, p0, Lsy;->i:Lth;

    sget-object v1, Ltf;->a:Ltf;

    invoke-interface {v0, v1}, Lte;->a(Ltf;)V

    iget-object v1, p0, Lsy;->i:Lth;

    invoke-interface {v0, v1}, Lte;->a(Ltd;)Z

    invoke-interface {v0}, Lte;->c()V

    iget-object v1, p0, Lsy;->a:Ljava/util/List;

    invoke-static {v1, v0}, Lsy;->a(Ljava/util/List;Lte;)V

    iget-object v1, p0, Lsy;->b:Ljava/util/List;

    invoke-static {v1, v0}, Lsy;->a(Ljava/util/List;Lte;)V

    iget-object v1, p0, Lsy;->c:Ljava/util/List;

    invoke-static {v1, v0}, Lsy;->a(Ljava/util/List;Lte;)V

    iget-object v1, p0, Lsy;->d:Ljava/util/List;

    invoke-static {v1, v0}, Lsy;->a(Ljava/util/List;Lte;)V

    iget-object v1, p0, Lsy;->e:Ljava/util/List;

    invoke-static {v1, v0}, Lsy;->a(Ljava/util/List;Lte;)V

    iget-object v1, p0, Lsy;->j:Ljava/util/List;

    invoke-static {v1, v0}, Lsy;->a(Ljava/util/List;Lte;)V

    iget-object v1, p0, Lsy;->f:Ljava/util/List;

    invoke-static {v1, v0}, Lsy;->a(Ljava/util/List;Lte;)V

    iget-object v1, p0, Lsy;->k:Ljava/util/List;

    invoke-static {v1, v0}, Lsy;->a(Ljava/util/List;Lte;)V

    iget-object v1, p0, Lsy;->l:Ljava/util/List;

    invoke-static {v1, v0}, Lsy;->a(Ljava/util/List;Lte;)V

    iget-object v1, p0, Lsy;->g:Ljava/util/List;

    invoke-static {v1, v0}, Lsy;->a(Ljava/util/List;Lte;)V

    iget-object v1, p0, Lsy;->m:Ljava/util/List;

    invoke-static {v1, v0}, Lsy;->a(Ljava/util/List;Lte;)V

    iget-object v1, p0, Lsy;->n:Ltb;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lsy;->n:Ltb;

    invoke-virtual {v1}, Ltb;->a()Ltf;

    move-result-object v1

    invoke-interface {v0, v1}, Lte;->a(Ltf;)V

    iget-object v1, p0, Lsy;->n:Ltb;

    invoke-interface {v0, v1}, Lte;->a(Ltd;)Z

    invoke-interface {v0}, Lte;->c()V

    :cond_0
    iget-object v1, p0, Lsy;->o:Lta;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lsy;->o:Lta;

    invoke-virtual {v1}, Lta;->a()Ltf;

    move-result-object v1

    invoke-interface {v0, v1}, Lte;->a(Ltf;)V

    iget-object v1, p0, Lsy;->o:Lta;

    invoke-interface {v0, v1}, Lte;->a(Ltd;)Z

    invoke-interface {v0}, Lte;->c()V

    :cond_1
    invoke-interface {v0}, Lte;->b()V

    .line 1749
    invoke-virtual {v0}, Ltp;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
