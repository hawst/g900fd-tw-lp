.class public final Leus;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leus;


# instance fields
.field public b:Letw;

.field public c:Ljava/lang/Long;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:[Levy;

.field public g:Lexp;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 473
    const/4 v0, 0x0

    new-array v0, v0, [Leus;

    sput-object v0, Leus;->a:[Leus;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 474
    invoke-direct {p0}, Lepn;-><init>()V

    .line 477
    iput-object v1, p0, Leus;->b:Letw;

    .line 486
    sget-object v0, Levy;->a:[Levy;

    iput-object v0, p0, Leus;->f:[Levy;

    .line 489
    iput-object v1, p0, Leus;->g:Lexp;

    .line 474
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 523
    iget-object v0, p0, Leus;->b:Letw;

    if-eqz v0, :cond_6

    .line 524
    const/4 v0, 0x1

    iget-object v2, p0, Leus;->b:Letw;

    .line 525
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 527
    :goto_0
    iget-object v2, p0, Leus;->c:Ljava/lang/Long;

    if-eqz v2, :cond_0

    .line 528
    const/4 v2, 0x2

    iget-object v3, p0, Leus;->c:Ljava/lang/Long;

    .line 529
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 531
    :cond_0
    iget-object v2, p0, Leus;->d:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 532
    const/4 v2, 0x3

    iget-object v3, p0, Leus;->d:Ljava/lang/String;

    .line 533
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 535
    :cond_1
    iget-object v2, p0, Leus;->e:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 536
    const/4 v2, 0x4

    iget-object v3, p0, Leus;->e:Ljava/lang/String;

    .line 537
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 539
    :cond_2
    iget-object v2, p0, Leus;->f:[Levy;

    if-eqz v2, :cond_4

    .line 540
    iget-object v2, p0, Leus;->f:[Levy;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 541
    if-eqz v4, :cond_3

    .line 542
    const/4 v5, 0x5

    .line 543
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 540
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 547
    :cond_4
    iget-object v1, p0, Leus;->g:Lexp;

    if-eqz v1, :cond_5

    .line 548
    const/4 v1, 0x6

    iget-object v2, p0, Leus;->g:Lexp;

    .line 549
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 551
    :cond_5
    iget-object v1, p0, Leus;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 552
    iput v0, p0, Leus;->cachedSize:I

    .line 553
    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 470
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Leus;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Leus;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Leus;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Leus;->b:Letw;

    if-nez v0, :cond_2

    new-instance v0, Letw;

    invoke-direct {v0}, Letw;-><init>()V

    iput-object v0, p0, Leus;->b:Letw;

    :cond_2
    iget-object v0, p0, Leus;->b:Letw;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Leus;->c:Ljava/lang/Long;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leus;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leus;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leus;->f:[Levy;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Levy;

    iget-object v3, p0, Leus;->f:[Levy;

    if-eqz v3, :cond_3

    iget-object v3, p0, Leus;->f:[Levy;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Leus;->f:[Levy;

    :goto_2
    iget-object v2, p0, Leus;->f:[Levy;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Leus;->f:[Levy;

    new-instance v3, Levy;

    invoke-direct {v3}, Levy;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leus;->f:[Levy;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Leus;->f:[Levy;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Leus;->f:[Levy;

    new-instance v3, Levy;

    invoke-direct {v3}, Levy;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leus;->f:[Levy;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Leus;->g:Lexp;

    if-nez v0, :cond_6

    new-instance v0, Lexp;

    invoke-direct {v0}, Lexp;-><init>()V

    iput-object v0, p0, Leus;->g:Lexp;

    :cond_6
    iget-object v0, p0, Leus;->g:Lexp;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 494
    iget-object v0, p0, Leus;->b:Letw;

    if-eqz v0, :cond_0

    .line 495
    const/4 v0, 0x1

    iget-object v1, p0, Leus;->b:Letw;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 497
    :cond_0
    iget-object v0, p0, Leus;->c:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 498
    const/4 v0, 0x2

    iget-object v1, p0, Leus;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 500
    :cond_1
    iget-object v0, p0, Leus;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 501
    const/4 v0, 0x3

    iget-object v1, p0, Leus;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 503
    :cond_2
    iget-object v0, p0, Leus;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 504
    const/4 v0, 0x4

    iget-object v1, p0, Leus;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 506
    :cond_3
    iget-object v0, p0, Leus;->f:[Levy;

    if-eqz v0, :cond_5

    .line 507
    iget-object v1, p0, Leus;->f:[Levy;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 508
    if-eqz v3, :cond_4

    .line 509
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 507
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 513
    :cond_5
    iget-object v0, p0, Leus;->g:Lexp;

    if-eqz v0, :cond_6

    .line 514
    const/4 v0, 0x6

    iget-object v1, p0, Leus;->g:Lexp;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 516
    :cond_6
    iget-object v0, p0, Leus;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 518
    return-void
.end method
