.class public final Lcie;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lcid;


# direct methods
.method public constructor <init>(Lcid;)V
    .locals 0

    iput-object p1, p0, Lcie;->a:Lcid;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3

    iget-object v0, p0, Lcie;->a:Lcid;

    iget-object v0, v0, Lcid;->e:Lcic;

    invoke-static {v0}, Lcic;->a(Lcic;)Ljava/util/HashMap;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcie;->a:Lcid;

    iput-object p2, v0, Lcid;->c:Landroid/os/IBinder;

    iget-object v0, p0, Lcie;->a:Lcid;

    iput-object p1, v0, Lcid;->d:Landroid/content/ComponentName;

    iget-object v0, p0, Lcie;->a:Lcid;

    iget-object v0, v0, Lcid;->a:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lchw;

    invoke-virtual {v0, p1, p2}, Lchw;->onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcie;->a:Lcid;

    const/4 v2, 0x1

    iput v2, v0, Lcid;->b:I

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3

    iget-object v0, p0, Lcie;->a:Lcid;

    iget-object v0, v0, Lcid;->e:Lcic;

    invoke-static {v0}, Lcic;->a(Lcic;)Ljava/util/HashMap;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcie;->a:Lcid;

    const/4 v2, 0x0

    iput-object v2, v0, Lcid;->c:Landroid/os/IBinder;

    iget-object v0, p0, Lcie;->a:Lcid;

    iput-object p1, v0, Lcid;->d:Landroid/content/ComponentName;

    iget-object v0, p0, Lcie;->a:Lcid;

    iget-object v0, v0, Lcid;->a:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lchw;

    invoke-virtual {v0, p1}, Lchw;->onServiceDisconnected(Landroid/content/ComponentName;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcie;->a:Lcid;

    const/4 v2, 0x2

    iput v2, v0, Lcid;->b:I

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
