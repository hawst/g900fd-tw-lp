.class public Lbio;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method private constructor <init>(Ldxn;)V
    .locals 3

    .prologue
    .line 1004
    iget-object v0, p1, Ldxn;->b:Ldvn;

    const-wide/16 v1, -0x1

    invoke-direct {p0, v0, v1, v2}, Lbfz;-><init>(Ldvn;J)V

    .line 1005
    return-void
.end method

.method public static parseFrom([B)Lbfz;
    .locals 2

    .prologue
    .line 1009
    new-instance v0, Ldxn;

    invoke-direct {v0}, Ldxn;-><init>()V

    .line 1010
    invoke-static {v0, p0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Ldxn;

    .line 1011
    iget-object v1, v0, Ldxn;->b:Ldvn;

    invoke-static {v1}, Lbio;->a(Ldvn;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1012
    new-instance v1, Lbgk;

    iget-object v0, v0, Ldxn;->b:Ldvn;

    invoke-direct {v1, v0}, Lbgk;-><init>(Ldvn;)V

    move-object v0, v1

    .line 1014
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lbio;

    invoke-direct {v1, v0}, Lbio;-><init>(Ldxn;)V

    move-object v0, v1

    goto :goto_0
.end method
