.class public final Ldfc;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldfc;


# instance fields
.field public b:[Ldfb;

.field public c:Ldfi;

.field public d:Ldfi;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3531
    const/4 v0, 0x0

    new-array v0, v0, [Ldfc;

    sput-object v0, Ldfc;->a:[Ldfc;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3532
    invoke-direct {p0}, Lepn;-><init>()V

    .line 3535
    sget-object v0, Ldfb;->a:[Ldfb;

    iput-object v0, p0, Ldfc;->b:[Ldfb;

    .line 3538
    iput-object v1, p0, Ldfc;->c:Ldfi;

    .line 3541
    iput-object v1, p0, Ldfc;->d:Ldfi;

    .line 3532
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 3566
    iget-object v1, p0, Ldfc;->b:[Ldfb;

    if-eqz v1, :cond_1

    .line 3567
    iget-object v2, p0, Ldfc;->b:[Ldfb;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 3568
    if-eqz v4, :cond_0

    .line 3569
    const/4 v5, 0x1

    .line 3570
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 3567
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3574
    :cond_1
    iget-object v1, p0, Ldfc;->c:Ldfi;

    if-eqz v1, :cond_2

    .line 3575
    const/4 v1, 0x2

    iget-object v2, p0, Ldfc;->c:Ldfi;

    .line 3576
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3578
    :cond_2
    iget-object v1, p0, Ldfc;->d:Ldfi;

    if-eqz v1, :cond_3

    .line 3579
    const/4 v1, 0x3

    iget-object v2, p0, Ldfc;->d:Ldfi;

    .line 3580
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3582
    :cond_3
    iget-object v1, p0, Ldfc;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3583
    iput v0, p0, Ldfc;->cachedSize:I

    .line 3584
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3528
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldfc;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldfc;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldfc;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldfc;->b:[Ldfb;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldfb;

    iget-object v3, p0, Ldfc;->b:[Ldfb;

    if-eqz v3, :cond_2

    iget-object v3, p0, Ldfc;->b:[Ldfb;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Ldfc;->b:[Ldfb;

    :goto_2
    iget-object v2, p0, Ldfc;->b:[Ldfb;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Ldfc;->b:[Ldfb;

    new-instance v3, Ldfb;

    invoke-direct {v3}, Ldfb;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldfc;->b:[Ldfb;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ldfc;->b:[Ldfb;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Ldfc;->b:[Ldfb;

    new-instance v3, Ldfb;

    invoke-direct {v3}, Ldfb;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldfc;->b:[Ldfb;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldfc;->c:Ldfi;

    if-nez v0, :cond_5

    new-instance v0, Ldfi;

    invoke-direct {v0}, Ldfi;-><init>()V

    iput-object v0, p0, Ldfc;->c:Ldfi;

    :cond_5
    iget-object v0, p0, Ldfc;->c:Ldfi;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldfc;->d:Ldfi;

    if-nez v0, :cond_6

    new-instance v0, Ldfi;

    invoke-direct {v0}, Ldfi;-><init>()V

    iput-object v0, p0, Ldfc;->d:Ldfi;

    :cond_6
    iget-object v0, p0, Ldfc;->d:Ldfi;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 3546
    iget-object v0, p0, Ldfc;->b:[Ldfb;

    if-eqz v0, :cond_1

    .line 3547
    iget-object v1, p0, Ldfc;->b:[Ldfb;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 3548
    if-eqz v3, :cond_0

    .line 3549
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 3547
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3553
    :cond_1
    iget-object v0, p0, Ldfc;->c:Ldfi;

    if-eqz v0, :cond_2

    .line 3554
    const/4 v0, 0x2

    iget-object v1, p0, Ldfc;->c:Ldfi;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 3556
    :cond_2
    iget-object v0, p0, Ldfc;->d:Ldfi;

    if-eqz v0, :cond_3

    .line 3557
    const/4 v0, 0x3

    iget-object v1, p0, Ldfc;->d:Ldfi;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 3559
    :cond_3
    iget-object v0, p0, Ldfc;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 3561
    return-void
.end method
