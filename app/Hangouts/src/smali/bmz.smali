.class public final Lbmz;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Z

.field private static b:J

.field private static volatile c:I

.field private static volatile d:Z

.field private static volatile e:Z

.field private static final f:Ljava/lang/Object;

.field private static g:Lbmz;


# instance fields
.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:J

.field private k:I

.field private l:Ljava/lang/String;

.field private m:Z

.field private n:I

.field private final o:Lbme;

.field private p:I

.field private q:I

.field private r:J

.field private s:Z

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lbys;->k:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lbmz;->a:Z

    .line 143
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lbmz;->f:Ljava/lang/Object;

    .line 144
    const/4 v0, 0x0

    sput-object v0, Lbmz;->g:Lbmz;

    .line 195
    invoke-static {}, Lbmz;->n()V

    .line 198
    new-instance v0, Lbna;

    invoke-direct {v0}, Lbna;-><init>()V

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/Runnable;)V

    .line 204
    return-void
.end method

.method private constructor <init>()V
    .locals 7

    .prologue
    const-wide/16 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lbme;

    invoke-static {v0, v1}, Lcyj;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbme;

    iput-object v0, p0, Lbmz;->o:Lbme;

    .line 215
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "phone_verification"

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 217
    const-string v1, "account_name"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lbmz;->h:Ljava/lang/String;

    .line 218
    const-string v1, "phone_number"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lbmz;->i:Ljava/lang/String;

    .line 219
    const-string v1, "verification_time"

    invoke-interface {v0, v1, v5, v6}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lbmz;->j:J

    .line 220
    const-string v1, "verification_state"

    const/16 v2, 0x64

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lbmz;->k:I

    .line 221
    const-string v1, "verification_prefix"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lbmz;->l:Ljava/lang/String;

    .line 222
    const-string v1, "confirming_google_voice"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lbmz;->m:Z

    .line 223
    const-string v1, "num_retries"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lbmz;->n:I

    .line 225
    const-string v1, "reminder_check_mode"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lbmz;->p:I

    .line 226
    const-string v1, "num_reminders"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lbmz;->q:I

    .line 227
    const-string v1, "last_reminder_time"

    invoke-interface {v0, v1, v5, v6}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lbmz;->r:J

    .line 228
    const-string v1, "reminder_accepted"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lbmz;->s:Z

    .line 229
    const-string v1, "last_reminder_sim_number"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lbmz;->t:Ljava/lang/String;

    .line 230
    const-string v1, "last_reminder_user_number"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lbmz;->u:Ljava/lang/String;

    .line 231
    const-string v1, "last_successful_number"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbmz;->v:Ljava/lang/String;

    .line 232
    return-void
.end method

.method public static a()Lbmz;
    .locals 2

    .prologue
    .line 166
    sget-object v1, Lbmz;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 167
    :try_start_0
    sget-object v0, Lbmz;->g:Lbmz;

    if-nez v0, :cond_0

    .line 168
    new-instance v0, Lbmz;

    invoke-direct {v0}, Lbmz;-><init>()V

    sput-object v0, Lbmz;->g:Lbmz;

    .line 171
    :cond_0
    sget-object v0, Lbmz;->g:Lbmz;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 172
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 440
    const/4 v0, 0x0

    invoke-static {v0}, Lbkb;->f(Z)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 441
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v2

    .line 442
    invoke-static {v2}, Lbkb;->f(Lyj;)I

    move-result v3

    const/16 v4, 0x66

    if-eq v3, v4, :cond_1

    .line 444
    invoke-static {v2}, Lbkb;->m(Lyj;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 446
    :cond_1
    invoke-virtual {v2, p0}, Lyj;->d(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 450
    const-string v1, "Babel"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 451
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Number "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is google voice for account "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    :cond_2
    :goto_0
    return-object v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(ILjava/lang/String;Ljava/lang/String;IZ)V
    .locals 4

    .prologue
    .line 962
    sget-boolean v0, Lbmz;->a:Z

    if-eqz v0, :cond_0

    .line 963
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Setting verification state:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 966
    :cond_0
    iput p1, p0, Lbmz;->k:I

    .line 967
    iput-object p2, p0, Lbmz;->h:Ljava/lang/String;

    .line 968
    iput-object p3, p0, Lbmz;->i:Ljava/lang/String;

    .line 969
    iput-boolean p5, p0, Lbmz;->m:Z

    .line 970
    iput p4, p0, Lbmz;->n:I

    .line 972
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "phone_verification"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 975
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 977
    const-string v1, "account_name"

    iget-object v2, p0, Lbmz;->h:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 978
    const-string v1, "phone_number"

    iget-object v2, p0, Lbmz;->i:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 979
    const-string v1, "num_retries"

    iget v2, p0, Lbmz;->n:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 980
    const-string v1, "verification_state"

    iget v2, p0, Lbmz;->k:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 981
    const-string v1, "confirming_google_voice"

    iget-boolean v2, p0, Lbmz;->m:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 984
    iget v1, p0, Lbmz;->k:I

    const/16 v2, 0x65

    if-ne v1, v2, :cond_1

    .line 985
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lbmz;->j:J

    .line 986
    const-string v1, "verification_time"

    iget-wide v2, p0, Lbmz;->j:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 989
    iget-boolean v1, p0, Lbmz;->m:Z

    if-nez v1, :cond_1

    .line 990
    iget-object v1, p0, Lbmz;->l:Ljava/lang/String;

    invoke-static {v1}, Lbmz;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lbmz;->l:Ljava/lang/String;

    .line 991
    const-string v1, "verification_prefix"

    iget-object v2, p0, Lbmz;->l:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 995
    :cond_1
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 996
    return-void
.end method

.method private a(IZLjava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1022
    sget-boolean v0, Lbmz;->a:Z

    if-eqz v0, :cond_0

    .line 1023
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Setting reminder state. Reminder #:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1026
    :cond_0
    iput p1, p0, Lbmz;->q:I

    .line 1027
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lbmz;->r:J

    .line 1028
    iput-boolean p2, p0, Lbmz;->s:Z

    .line 1029
    iput-object p3, p0, Lbmz;->t:Ljava/lang/String;

    .line 1030
    iput-object p4, p0, Lbmz;->u:Ljava/lang/String;

    .line 1032
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "phone_verification"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1035
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1037
    const-string v1, "num_reminders"

    iget v2, p0, Lbmz;->q:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1038
    const-string v1, "last_reminder_time"

    iget-wide v2, p0, Lbmz;->r:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 1039
    const-string v1, "reminder_accepted"

    iget-boolean v2, p0, Lbmz;->s:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1040
    const-string v1, "last_reminder_sim_number"

    iget-object v2, p0, Lbmz;->t:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1041
    const-string v1, "last_reminder_user_number"

    iget-object v2, p0, Lbmz;->u:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1043
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1044
    return-void
.end method

.method private a(IZZ)V
    .locals 8

    .prologue
    .line 673
    monitor-enter p0

    .line 675
    const/16 v0, 0x67

    if-le p1, v0, :cond_0

    .line 676
    :try_start_0
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->g()V

    const/4 v0, 0x0

    invoke-static {v0}, Lbkb;->b(Z)V

    .line 679
    :cond_0
    if-eqz p3, :cond_2

    .line 682
    iget-object v2, p0, Lbmz;->h:Ljava/lang/String;

    iget-object v3, p0, Lbmz;->i:Ljava/lang/String;

    sget v4, Lbmz;->c:I

    iget-boolean v5, p0, Lbmz;->m:Z

    move-object v0, p0

    move v1, p1

    invoke-direct/range {v0 .. v5}, Lbmz;->a(ILjava/lang/String;Ljava/lang/String;IZ)V

    .line 687
    :goto_0
    const/16 v0, 0x69

    if-ne p1, v0, :cond_5

    .line 689
    iget v0, p0, Lbmz;->n:I

    sget v1, Lbmz;->c:I

    if-lt v0, v1, :cond_4

    .line 690
    const-string v0, "Babel"

    const-string v1, "Sending verification failure notification"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->od:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lbmz;->h:Ljava/lang/String;

    invoke-static {v1}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v3

    if-eqz p3, :cond_3

    sget v1, Lh;->oc:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3}, Lbbl;->b(Lyj;)Landroid/content/Intent;

    move-result-object v0

    :goto_1
    const v4, 0x4008000

    invoke-virtual {v0, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v4

    const/4 v5, 0x1

    const/16 v6, 0x9

    const/4 v7, 0x0

    invoke-static {v3, v5, v6, v7}, Lbzb;->a(Lyj;IILjava/lang/String;)I

    move-result v3

    const/high16 v5, 0x8000000

    invoke-static {v4, v3, v0, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v3, Lbk;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lbk;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v2}, Lbk;->a(Ljava/lang/CharSequence;)Lbk;

    move-result-object v4

    invoke-virtual {v4, v1}, Lbk;->b(Ljava/lang/CharSequence;)Lbk;

    move-result-object v1

    invoke-virtual {v1, v2}, Lbk;->c(Ljava/lang/CharSequence;)Lbk;

    move-result-object v1

    sget v2, Lcom/google/android/apps/hangouts/R$drawable;->cN:I

    invoke-virtual {v1, v2}, Lbk;->a(I)Lbk;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lbk;->c(I)Lbk;

    move-result-object v1

    invoke-virtual {v1, v0}, Lbk;->a(Landroid/app/PendingIntent;)Lbk;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbk;->a(Z)Lbk;

    invoke-virtual {v3}, Lbk;->e()Landroid/app/Notification;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v2, "notification"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-static {}, Lbmz;->r()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x9

    invoke-virtual {v0, v2, v3, v1}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 699
    :cond_1
    :goto_2
    monitor-exit p0

    return-void

    .line 684
    :cond_2
    invoke-direct {p0, p1}, Lbmz;->b(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 699
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 690
    :cond_3
    :try_start_1
    sget v1, Lh;->ob:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x0

    invoke-static {v3, v0}, Lbbl;->a(Lyj;Z)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    .line 691
    :cond_4
    if-eqz p2, :cond_1

    .line 692
    invoke-direct {p0}, Lbmz;->o()V

    goto :goto_2

    .line 694
    :cond_5
    const/16 v0, 0x68

    if-ne p1, v0, :cond_1

    .line 695
    const-string v0, "Babel"

    const-string v1, "Sending verifcation success notification"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->ol:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lh;->ok:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lbmz;->h:Ljava/lang/String;

    invoke-static {v2}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v2

    invoke-static {v2}, Lbbl;->b(Lyj;)Landroid/content/Intent;

    move-result-object v3

    const v4, 0x4008000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v4

    const/4 v5, 0x1

    const/16 v6, 0x9

    const/4 v7, 0x0

    invoke-static {v2, v5, v6, v7}, Lbzb;->a(Lyj;IILjava/lang/String;)I

    move-result v2

    const/high16 v5, 0x8000000

    invoke-static {v4, v2, v3, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    new-instance v3, Lbk;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lbk;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v1}, Lbk;->a(Ljava/lang/CharSequence;)Lbk;

    move-result-object v4

    invoke-virtual {v4, v0}, Lbk;->b(Ljava/lang/CharSequence;)Lbk;

    move-result-object v0

    invoke-virtual {v0, v1}, Lbk;->c(Ljava/lang/CharSequence;)Lbk;

    move-result-object v0

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->cJ:I

    invoke-virtual {v0, v1}, Lbk;->a(I)Lbk;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbk;->c(I)Lbk;

    move-result-object v0

    invoke-virtual {v0, v2}, Lbk;->a(Landroid/app/PendingIntent;)Lbk;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbk;->a(Z)Lbk;

    invoke-virtual {v3}, Lbk;->e()Landroid/app/Notification;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v2, "notification"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-static {}, Lbmz;->r()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x9

    invoke-virtual {v0, v2, v3, v1}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 696
    iget-object v0, p0, Lbmz;->o:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v1, 0x63e

    .line 697
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 696
    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_2
.end method

.method public static a(Landroid/content/Intent;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1143
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1144
    if-eqz v0, :cond_3

    .line 1145
    const-string v1, "Babel"

    const-string v3, "Captured some SMS messages"

    invoke-static {v1, v3}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1147
    const-string v1, "pdus"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 1148
    array-length v1, v0

    if-lez v1, :cond_3

    .line 1149
    aget-object v1, v0, v2

    check-cast v1, [B

    invoke-static {v1}, Landroid/telephony/gsm/SmsMessage;->createFromPdu([B)Landroid/telephony/gsm/SmsMessage;

    move-result-object v1

    .line 1155
    :try_start_0
    invoke-virtual {v1}, Landroid/telephony/gsm/SmsMessage;->getMessageBody()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1165
    invoke-static {}, Lbmz;->a()Lbmz;

    move-result-object v3

    invoke-direct {v3}, Lbmz;->q()Ljava/lang/String;

    move-result-object v3

    .line 1166
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v0, v2

    .line 1186
    :goto_0
    return v0

    .line 1156
    :catch_0
    move-exception v1

    .line 1157
    const-string v3, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "NPE in message.GetMessageBody() - pdus[0]: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v5, v0, v2

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 1158
    aget-object v3, v0, v2

    if-eqz v3, :cond_0

    .line 1159
    const-string v3, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "pdus[0] class: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v5, v0, v2

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 1160
    const-string v3, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "pdus[0] length: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v0, v0, v2

    check-cast v0, [B

    array-length v0, v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 1162
    :cond_0
    throw v1

    .line 1171
    :cond_1
    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 1172
    if-ltz v4, :cond_3

    .line 1173
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v4

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 1174
    sget-boolean v3, Lbmz;->a:Z

    if-eqz v3, :cond_2

    .line 1175
    const-string v3, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Received verification code from: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1176
    invoke-virtual {v1}, Landroid/telephony/gsm/SmsMessage;->getOriginatingAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", messageBody: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", code: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1175
    invoke-static {v3, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1180
    :cond_2
    invoke-static {}, Lbmz;->a()Lbmz;

    move-result-object v0

    invoke-direct {v0, v2}, Lbmz;->c(Ljava/lang/String;)V

    .line 1182
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_3
    move v0, v2

    .line 1186
    goto/16 :goto_0
.end method

.method private b(I)V
    .locals 3

    .prologue
    .line 1050
    sget-boolean v0, Lbmz;->a:Z

    if-eqz v0, :cond_0

    .line 1051
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Setting verification state:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1054
    :cond_0
    iput p1, p0, Lbmz;->k:I

    .line 1056
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "phone_verification"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1059
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1061
    const-string v1, "verification_state"

    iget v2, p0, Lbmz;->k:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1065
    const/16 v1, 0x68

    if-ne p1, v1, :cond_1

    iget-boolean v1, p0, Lbmz;->m:Z

    if-nez v1, :cond_1

    .line 1066
    iget-object v1, p0, Lbmz;->i:Ljava/lang/String;

    iput-object v1, p0, Lbmz;->v:Ljava/lang/String;

    .line 1067
    const-string v1, "last_successful_number"

    iget-object v2, p0, Lbmz;->v:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1070
    :cond_1
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1071
    return-void
.end method

.method public static b()Z
    .locals 1

    .prologue
    .line 207
    sget-boolean v0, Lbmz;->d:Z

    return v0
.end method

.method private static b(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 465
    invoke-static {v1}, Lbkb;->f(Z)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 466
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    .line 467
    invoke-virtual {v0, p0}, Lyj;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 472
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 816
    monitor-enter p0

    .line 817
    :try_start_0
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->g()V

    const/4 v0, 0x0

    invoke-static {v0}, Lbkb;->b(Z)V

    .line 819
    sget-boolean v0, Lbmz;->a:Z

    if-eqz v0, :cond_0

    .line 820
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Finishing phone verification.  Account: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbmz;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Phone: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbmz;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Verification Code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 826
    :cond_0
    iget-object v0, p0, Lbmz;->h:Ljava/lang/String;

    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    .line 827
    if-eqz v0, :cond_1

    invoke-static {v0}, Lbkb;->f(Lyj;)I

    move-result v1

    const/16 v2, 0x66

    if-eq v1, v2, :cond_2

    .line 829
    :cond_1
    const-string v0, "Babel"

    const-string v1, "Account not ready. Abort phone verification"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 830
    const/16 v0, 0x69

    invoke-direct {p0, v0}, Lbmz;->b(I)V

    .line 831
    monitor-exit p0

    .line 837
    :goto_0
    return-void

    .line 834
    :cond_2
    const/16 v1, 0x67

    invoke-direct {p0, v1}, Lbmz;->b(I)V

    .line 835
    iget-object v1, p0, Lbmz;->i:Ljava/lang/String;

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->d(Lyj;Ljava/lang/String;Ljava/lang/String;)V

    .line 837
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static c()Z
    .locals 1

    .prologue
    .line 300
    sget-boolean v0, Lbmz;->e:Z

    if-eqz v0, :cond_0

    .line 301
    const/4 v0, 0x1

    .line 303
    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lbzd;->f()Z

    move-result v0

    goto :goto_0
.end method

.method private static d(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    const/16 v7, 0x1a

    const/16 v6, 0xa

    .line 1003
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    .line 1006
    :cond_0
    invoke-virtual {v0, v7}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    int-to-char v1, v1

    add-int/lit8 v1, v1, 0x61

    int-to-char v1, v1

    .line 1007
    invoke-virtual {v0, v6}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    int-to-char v2, v2

    add-int/lit8 v2, v2, 0x30

    int-to-char v2, v2

    .line 1008
    invoke-virtual {v0, v7}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    int-to-char v3, v3

    add-int/lit8 v3, v3, 0x41

    int-to-char v3, v3

    .line 1009
    invoke-virtual {v0, v6}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    int-to-char v4, v4

    add-int/lit8 v4, v4, 0x30

    int-to-char v4, v4

    .line 1010
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1011
    invoke-static {v1, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1012
    return-object v1
.end method

.method static synthetic l()V
    .locals 0

    .prologue
    .line 64
    invoke-static {}, Lbmz;->n()V

    return-void
.end method

.method static synthetic m()Z
    .locals 1

    .prologue
    .line 64
    sget-boolean v0, Lbmz;->a:Z

    return v0
.end method

.method private static n()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 177
    const-string v0, "babel_phone_verification_enabled"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lbmz;->d:Z

    .line 180
    const-string v0, "babel_pv_assume_sim_exist"

    invoke-static {v0, v4}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lbmz;->e:Z

    .line 183
    const-string v0, "babel_phone_verification_timeout"

    const/16 v1, 0x12c

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    sput-wide v0, Lbmz;->b:J

    .line 187
    const-string v0, "babel_phone_verification_max_retries"

    invoke-static {v0, v4}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lbmz;->c:I

    .line 190
    return-void
.end method

.method private o()V
    .locals 6

    .prologue
    .line 508
    monitor-enter p0

    .line 509
    :try_start_0
    sget-boolean v0, Lbmz;->d:Z

    if-nez v0, :cond_0

    .line 510
    const-string v0, "Babel"

    const-string v1, "Phone verification not enabled."

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 511
    monitor-exit p0

    .line 537
    :goto_0
    return-void

    .line 514
    :cond_0
    invoke-virtual {p0}, Lbmz;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 515
    const-string v0, "Babel"

    const-string v1, "Phone verification is in progress. Skip new verification"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 516
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 537
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 519
    :cond_1
    :try_start_1
    iget v0, p0, Lbmz;->k:I

    const/16 v1, 0x69

    if-eq v0, v1, :cond_2

    .line 520
    monitor-exit p0

    goto :goto_0

    .line 523
    :cond_2
    iget v0, p0, Lbmz;->n:I

    sget v1, Lbmz;->c:I

    if-lt v0, v1, :cond_3

    .line 524
    monitor-exit p0

    goto :goto_0

    .line 527
    :cond_3
    const/16 v1, 0x65

    iget-object v2, p0, Lbmz;->h:Ljava/lang/String;

    iget-object v3, p0, Lbmz;->i:Ljava/lang/String;

    iget v0, p0, Lbmz;->n:I

    add-int/lit8 v4, v0, 0x1

    iget-boolean v5, p0, Lbmz;->m:Z

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lbmz;->a(ILjava/lang/String;Ljava/lang/String;IZ)V

    .line 530
    sget-boolean v0, Lbmz;->a:Z

    if-eqz v0, :cond_4

    .line 531
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Retrying phone verification.  Account: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbmz;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Phone: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbmz;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    :cond_4
    invoke-direct {p0}, Lbmz;->p()Z

    .line 537
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private p()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 588
    iget-object v2, p0, Lbmz;->h:Ljava/lang/String;

    invoke-static {v2}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v2

    .line 589
    if-eqz v2, :cond_0

    invoke-static {v2}, Lbkb;->f(Lyj;)I

    move-result v3

    const/16 v4, 0x66

    if-eq v3, v4, :cond_1

    .line 591
    :cond_0
    const-string v0, "Babel"

    const-string v2, "Account not ready. Skip phone verification"

    invoke-static {v0, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    const/16 v0, 0x69

    invoke-direct {p0, v0, v1, v1}, Lbmz;->a(IZZ)V

    move v0, v1

    .line 618
    :goto_0
    return v0

    .line 596
    :cond_1
    iget-boolean v1, p0, Lbmz;->m:Z

    if-nez v1, :cond_3

    .line 597
    sget-boolean v1, Lbmz;->a:Z

    if-eqz v1, :cond_2

    .line 598
    const-string v1, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Starting phone verification.  Account: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lbmz;->h:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Phone: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lbmz;->i:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    :cond_2
    iget-object v1, p0, Lbmz;->i:Ljava/lang/String;

    iget-object v3, p0, Lbmz;->l:Ljava/lang/String;

    invoke-static {v2, v1, v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;Ljava/lang/String;Ljava/lang/String;)V

    .line 605
    invoke-static {v0}, Lbkb;->b(Z)V

    sget-wide v1, Lbmz;->b:J

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(J)V

    goto :goto_0

    .line 607
    :cond_3
    sget-boolean v1, Lbmz;->a:Z

    if-eqz v1, :cond_4

    .line 608
    const-string v1, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Triggering google voice confirmation.  Account: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lbmz;->h:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Phone: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lbmz;->i:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 614
    :cond_4
    iget-object v1, p0, Lbmz;->i:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v2, v1, v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->d(Lyj;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 622
    monitor-enter p0

    .line 623
    :try_start_0
    iget-object v0, p0, Lbmz;->l:Ljava/lang/String;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 624
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static r()Ljava/lang/String;
    .locals 2

    .prologue
    .line 807
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":verification"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lyj;)Lbnb;
    .locals 4

    .prologue
    .line 1098
    invoke-virtual {p1}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lbmz;->h:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1099
    iget-object v0, p0, Lbmz;->i:Ljava/lang/String;

    sget v1, Lbzg;->c:I

    .line 1100
    invoke-static {v0, v1}, Lbzd;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 1101
    invoke-virtual {p0}, Lbmz;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1102
    new-instance v0, Lbnb;

    const/16 v2, 0x66

    invoke-direct {v0, v2, v1}, Lbnb;-><init>(ILjava/lang/String;)V

    .line 1133
    :goto_0
    return-object v0

    .line 1107
    :cond_0
    invoke-virtual {p1}, Lyj;->G()Ljava/util/ArrayList;

    move-result-object v0

    .line 1108
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 1109
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1110
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1111
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 1112
    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1114
    :cond_1
    sget v3, Lbzg;->c:I

    .line 1115
    invoke-static {v0, v3}, Lbzd;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 1114
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1117
    :cond_2
    new-instance v0, Lbnb;

    const/16 v2, 0x64

    .line 1118
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lbnb;-><init>(ILjava/lang/String;)V

    goto :goto_0

    .line 1121
    :cond_3
    invoke-static {}, Lbzd;->g()Ljava/lang/String;

    move-result-object v0

    .line 1122
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1124
    iget-object v0, p0, Lbmz;->v:Ljava/lang/String;

    .line 1127
    :cond_4
    const/4 v1, 0x0

    .line 1128
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 1129
    sget v1, Lbzg;->c:I

    .line 1130
    invoke-static {v0, v1}, Lbzd;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 1133
    :goto_2
    new-instance v1, Lbnb;

    const/16 v2, 0x65

    invoke-direct {v1, v2, v0}, Lbnb;-><init>(ILjava/lang/String;)V

    move-object v0, v1

    goto :goto_0

    :cond_5
    move-object v0, v1

    goto :goto_2
.end method

.method public a(I)V
    .locals 3

    .prologue
    .line 641
    monitor-enter p0

    .line 642
    :try_start_0
    iput p1, p0, Lbmz;->p:I

    .line 643
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "phone_verification"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 646
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 647
    const-string v1, "reminder_check_mode"

    iget v2, p0, Lbmz;->p:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 648
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 649
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(IZ)V
    .locals 1

    .prologue
    .line 634
    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, p1, v0, p2}, Lbmz;->a(IZZ)V

    .line 635
    return-void

    .line 634
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;ZZ)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 562
    monitor-enter p0

    .line 563
    :try_start_0
    sget-boolean v0, Lbmz;->d:Z

    if-nez v0, :cond_0

    .line 564
    const-string v0, "Babel"

    const-string v2, "Phone verification not enabled."

    invoke-static {v0, v2}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    monitor-exit p0

    move v0, v1

    .line 583
    :goto_0
    return v0

    .line 568
    :cond_0
    const-string v0, "Babel"

    const-string v2, "Canceling verifcation failure notification"

    invoke-static {v0, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v2, "notification"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-static {}, Lbmz;->r()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x9

    invoke-virtual {v0, v2, v3}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 570
    invoke-virtual {p0}, Lbmz;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 571
    const-string v0, "Babel"

    const-string v2, "Phone verification is in progress. Skip new verification"

    invoke-static {v0, v2}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 572
    monitor-exit p0

    move v0, v1

    goto :goto_0

    .line 575
    :cond_1
    iget v0, p0, Lbmz;->q:I

    .line 576
    if-eqz p3, :cond_2

    .line 577
    add-int/lit8 v0, v0, 0x1

    move v6, v0

    .line 580
    :goto_1
    const/16 v1, 0x65

    const/4 v4, 0x0

    move-object v0, p0

    move-object v2, p2

    move-object v3, p1

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lbmz;->a(ILjava/lang/String;Ljava/lang/String;IZ)V

    .line 581
    const/4 v0, 0x1

    invoke-static {}, Lbzd;->g()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v6, v0, v1, p1}, Lbmz;->a(IZLjava/lang/String;Ljava/lang/String;)V

    .line 583
    invoke-direct {p0}, Lbmz;->p()Z

    move-result v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 584
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    move v6, v0

    goto :goto_1
.end method

.method public d()I
    .locals 12

    .prologue
    const-wide/32 v8, 0xea60

    const/4 v1, 0x0

    const-wide/16 v6, 0x0

    const/4 v2, 0x0

    .line 310
    monitor-enter p0

    .line 311
    :try_start_0
    sget-boolean v0, Lbmz;->d:Z

    if-nez v0, :cond_0

    .line 312
    const-string v0, "Babel"

    const-string v1, "Phone verification not enabled."

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    monitor-exit p0

    move v0, v2

    .line 397
    :goto_0
    return v0

    .line 316
    :cond_0
    iget v0, p0, Lbmz;->q:I

    if-lez v0, :cond_1

    iget-boolean v0, p0, Lbmz;->s:Z

    if-nez v0, :cond_1

    .line 317
    const-string v0, "Babel"

    const-string v1, "User was asked once and said \'no\'. Skip reminding user."

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    monitor-exit p0

    move v0, v2

    goto :goto_0

    .line 321
    :cond_1
    invoke-static {}, Lbmz;->c()Z

    move-result v0

    if-nez v0, :cond_2

    .line 322
    const-string v0, "Babel"

    const-string v1, "Phone doesn\'t have sim. Skip reminding user."

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    monitor-exit p0

    move v0, v2

    goto :goto_0

    .line 326
    :cond_2
    invoke-virtual {p0}, Lbmz;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 327
    const-string v0, "Babel"

    const-string v1, "Phone verification is in progress. Skip reminding user."

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    monitor-exit p0

    move v0, v2

    goto :goto_0

    .line 331
    :cond_3
    iget v0, p0, Lbmz;->k:I

    const/16 v3, 0x69

    if-ne v0, v3, :cond_4

    iget v0, p0, Lbmz;->n:I

    sget v3, Lbmz;->c:I

    if-ge v0, v3, :cond_4

    .line 332
    const-string v0, "Babel"

    const-string v1, "Start retrying current verification. Skip reminding user."

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    invoke-direct {p0}, Lbmz;->o()V

    .line 334
    monitor-exit p0

    move v0, v2

    goto :goto_0

    .line 339
    :cond_4
    iget v0, p0, Lbmz;->q:I

    packed-switch v0, :pswitch_data_0

    const-wide/16 v3, -0x1

    move-wide v4, v3

    .line 340
    :goto_1
    iget v0, p0, Lbmz;->p:I

    packed-switch v0, :pswitch_data_1

    .line 360
    :cond_5
    :goto_2
    invoke-static {}, Lbzd;->g()Ljava/lang/String;

    move-result-object v0

    .line 361
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 363
    iget-object v0, p0, Lbmz;->v:Ljava/lang/String;

    move-object v3, v0

    .line 366
    :goto_3
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 367
    const/4 v0, 0x0

    invoke-static {v0}, Lbkb;->f(Z)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_6
    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v9

    invoke-static {v9}, Lbkb;->f(Lyj;)I

    move-result v10

    const/16 v11, 0x66

    if-eq v10, v11, :cond_7

    invoke-static {v9}, Lbkb;->m(Lyj;)Z

    move-result v10

    if-eqz v10, :cond_6

    :cond_7
    invoke-virtual {v9, v3}, Lyj;->b(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_a

    if-eqz v1, :cond_b

    const-string v9, "Babel"

    const/4 v10, 0x3

    invoke-static {v9, v10}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_8

    const-string v9, "Babel"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Two accounts are verified for same phone number. Accounts:"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v10, ","

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v10, " Phone:"

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v9, v0}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    const-string v0, "Babel"

    const-string v9, "Two accounts verifed for same phone"

    invoke-static {v0, v9}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_4

    .line 398
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :pswitch_0
    move-wide v4, v6

    .line 339
    goto/16 :goto_1

    :pswitch_1
    :try_start_1
    const-string v0, "babel_pv_first_reminder_gap"

    const v3, 0xa8c0

    invoke-static {v0, v3}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    int-to-long v3, v0

    mul-long/2addr v3, v8

    move-wide v4, v3

    goto/16 :goto_1

    :pswitch_2
    const-string v0, "babel_pv_first_reminder_gap"

    const v3, 0x1fa40

    invoke-static {v0, v3}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    int-to-long v3, v0

    mul-long/2addr v3, v8

    move-wide v4, v3

    goto/16 :goto_1

    :pswitch_3
    const-string v0, "babel_pv_first_reminder_gap"

    const v3, 0x3f480

    invoke-static {v0, v3}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    int-to-long v3, v0

    mul-long/2addr v3, v8

    move-wide v4, v3

    goto/16 :goto_1

    .line 342
    :pswitch_4
    cmp-long v0, v4, v6

    if-ltz v0, :cond_9

    iget-wide v8, p0, Lbmz;->r:J

    add-long/2addr v8, v4

    .line 343
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    cmp-long v0, v8, v10

    if-lez v0, :cond_5

    .line 344
    :cond_9
    const-string v0, "Babel"

    const-string v1, "Skipping phone verification since next reminder time is not up"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    monitor-exit p0

    move v0, v2

    goto/16 :goto_0

    .line 350
    :pswitch_5
    const-string v0, "Babel"

    const-string v3, "Force phone verification check since sim may have changed"

    invoke-static {v0, v3}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbmz;->a(I)V

    goto/16 :goto_2

    .line 355
    :pswitch_6
    const-string v0, "Babel"

    const-string v1, "Skip phone verification. Either phone is verified or there is no valid account for verification"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    monitor-exit p0

    move v0, v2

    goto/16 :goto_0

    :cond_a
    move-object v0, v1

    :cond_b
    move-object v1, v0

    .line 367
    goto/16 :goto_4

    :cond_c
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 368
    const-string v0, "Babel"

    const-string v1, "Phone already verified for current number. Skip verification."

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lbmz;->a(I)V

    .line 370
    monitor-exit p0

    move v0, v2

    goto/16 :goto_0

    .line 373
    :cond_d
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_e

    invoke-static {v3}, Lbmz;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 374
    const-string v0, "Babel"

    const-string v1, "All accounts opt out of current number. Skip verification."

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lbmz;->a(I)V

    .line 376
    monitor-exit p0

    move v0, v2

    goto/16 :goto_0

    .line 380
    :cond_e
    iget-wide v0, p0, Lbmz;->r:J

    cmp-long v0, v0, v6

    if-lez v0, :cond_f

    iget-object v0, p0, Lbmz;->t:Ljava/lang/String;

    .line 381
    invoke-static {}, Lbzd;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 382
    const-string v0, "Babel"

    const-string v1, "Sim number changed. Reset phone verification data."

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    const/4 v0, 0x0

    iput-object v0, p0, Lbmz;->h:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lbmz;->i:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lbmz;->j:J

    const/16 v0, 0x64

    iput v0, p0, Lbmz;->k:I

    const/4 v0, 0x0

    iput-object v0, p0, Lbmz;->l:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lbmz;->n:I

    const/4 v0, 0x0

    iput v0, p0, Lbmz;->p:I

    const/4 v0, 0x0

    iput v0, p0, Lbmz;->q:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lbmz;->r:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbmz;->s:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lbmz;->t:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lbmz;->u:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "phone_verification"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "phone_number"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "verification_time"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "verification_state"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "verification_prefix"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "confirming_google_voice"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "num_retries"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "reminder_check_mode"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "num_reminders"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "last_reminder_time"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "reminder_accepted"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "last_reminder_sim_number"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "last_reminder_user_number"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "last_successful_number"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 384
    const/16 v0, 0x67

    monitor-exit p0

    goto/16 :goto_0

    .line 387
    :cond_f
    cmp-long v0, v4, v6

    if-ltz v0, :cond_11

    iget-wide v0, p0, Lbmz;->r:J

    add-long/2addr v0, v4

    .line 388
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    cmp-long v0, v0, v3

    if-gez v0, :cond_11

    .line 389
    iget-wide v0, p0, Lbmz;->r:J

    cmp-long v0, v0, v6

    if-lez v0, :cond_10

    .line 390
    const/16 v0, 0x65

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 392
    :cond_10
    const/16 v0, 0x64

    monitor-exit p0

    goto/16 :goto_0

    .line 396
    :cond_11
    :try_start_2
    const-string v0, "Babel"

    const-string v1, "Skip phone verification since it exceeded max number of reminders."

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v0, v2

    goto/16 :goto_0

    :cond_12
    move-object v3, v0

    goto/16 :goto_3

    .line 339
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 340
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public e()V
    .locals 4

    .prologue
    .line 544
    monitor-enter p0

    .line 545
    :try_start_0
    iget v0, p0, Lbmz;->q:I

    add-int/lit8 v0, v0, 0x1

    const/4 v1, 0x0

    invoke-static {}, Lbzd;->g()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lbmz;->a(IZLjava/lang/String;Ljava/lang/String;)V

    .line 546
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public f()V
    .locals 2

    .prologue
    .line 656
    monitor-enter p0

    .line 659
    :try_start_0
    iget v0, p0, Lbmz;->p:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 660
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbmz;->a(I)V

    .line 662
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public g()V
    .locals 3

    .prologue
    .line 844
    const-string v0, "Babel"

    const-string v1, "Timing out phone verification"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 845
    monitor-enter p0

    .line 846
    :try_start_0
    iget v0, p0, Lbmz;->k:I

    const/16 v1, 0x65

    if-ge v0, v1, :cond_0

    .line 847
    monitor-exit p0

    .line 872
    :goto_0
    return-void

    .line 850
    :cond_0
    iget v0, p0, Lbmz;->k:I

    const/16 v1, 0x67

    if-le v0, v1, :cond_1

    .line 851
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 872
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 857
    :cond_1
    :try_start_1
    iget-object v0, p0, Lbmz;->l:Ljava/lang/String;

    iget-wide v1, p0, Lbmz;->j:J

    invoke-static {v0, v1, v2}, Lbnc;->a(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    .line 860
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 861
    invoke-direct {p0, v0}, Lbmz;->c(Ljava/lang/String;)V

    .line 862
    monitor-exit p0

    goto :goto_0

    .line 865
    :cond_2
    sget-boolean v0, Lbmz;->a:Z

    if-eqz v0, :cond_3

    .line 866
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Canceling out phone verification.  Account: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbmz;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Phone: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbmz;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 871
    :cond_3
    const/16 v0, 0x69

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lbmz;->a(IZZ)V

    .line 872
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public h()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 892
    monitor-enter p0

    .line 893
    :try_start_0
    iget v2, p0, Lbmz;->k:I

    const/16 v3, 0x65

    if-ge v2, v3, :cond_0

    .line 894
    monitor-exit p0

    .line 916
    :goto_0
    return v0

    .line 897
    :cond_0
    iget v2, p0, Lbmz;->k:I

    const/16 v3, 0x67

    if-le v2, v3, :cond_1

    .line 898
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 917
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 902
    :cond_1
    :try_start_1
    iget-wide v2, p0, Lbmz;->j:J

    sget-wide v4, Lbmz;->b:J

    add-long/2addr v2, v4

    const-wide/16 v4, 0x2710

    add-long/2addr v2, v4

    .line 903
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_3

    move v2, v1

    .line 905
    :goto_1
    if-eqz v2, :cond_4

    .line 906
    const/16 v1, 0x69

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, v3}, Lbmz;->a(IZZ)V

    .line 907
    sget-boolean v1, Lbmz;->a:Z

    if-eqz v1, :cond_2

    .line 908
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Timing out phone verification.  Account: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lbmz;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Phone: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lbmz;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 913
    :cond_2
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :cond_3
    move v2, v0

    .line 903
    goto :goto_1

    .line 916
    :cond_4
    monitor-exit p0

    move v0, v1

    goto :goto_0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 924
    iget-object v0, p0, Lbmz;->h:Ljava/lang/String;

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 931
    iget-object v0, p0, Lbmz;->i:Ljava/lang/String;

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 935
    iget-object v0, p0, Lbmz;->v:Ljava/lang/String;

    return-object v0
.end method
