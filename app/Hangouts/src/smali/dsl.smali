.class public final Ldsl;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldsl;


# instance fields
.field public b:Ldvn;

.field public c:Ljava/lang/Integer;

.field public d:Ldql;

.field public e:Lduw;

.field public f:[Ldvo;

.field public g:[Ldro;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10734
    const/4 v0, 0x0

    new-array v0, v0, [Ldsl;

    sput-object v0, Ldsl;->a:[Ldsl;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10735
    invoke-direct {p0}, Lepn;-><init>()V

    .line 10742
    iput-object v0, p0, Ldsl;->b:Ldvn;

    .line 10745
    iput-object v0, p0, Ldsl;->c:Ljava/lang/Integer;

    .line 10748
    iput-object v0, p0, Ldsl;->d:Ldql;

    .line 10751
    iput-object v0, p0, Ldsl;->e:Lduw;

    .line 10754
    sget-object v0, Ldvo;->a:[Ldvo;

    iput-object v0, p0, Ldsl;->f:[Ldvo;

    .line 10757
    sget-object v0, Ldro;->a:[Ldro;

    iput-object v0, p0, Ldsl;->g:[Ldro;

    .line 10735
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 10795
    iget-object v0, p0, Ldsl;->b:Ldvn;

    if-eqz v0, :cond_7

    .line 10796
    const/4 v0, 0x1

    iget-object v2, p0, Ldsl;->b:Ldvn;

    .line 10797
    invoke-static {v0, v2}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 10799
    :goto_0
    iget-object v2, p0, Ldsl;->d:Ldql;

    if-eqz v2, :cond_0

    .line 10800
    const/4 v2, 0x2

    iget-object v3, p0, Ldsl;->d:Ldql;

    .line 10801
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 10803
    :cond_0
    iget-object v2, p0, Ldsl;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 10804
    const/4 v2, 0x3

    iget-object v3, p0, Ldsl;->c:Ljava/lang/Integer;

    .line 10805
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 10807
    :cond_1
    iget-object v2, p0, Ldsl;->e:Lduw;

    if-eqz v2, :cond_2

    .line 10808
    const/4 v2, 0x4

    iget-object v3, p0, Ldsl;->e:Lduw;

    .line 10809
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 10811
    :cond_2
    iget-object v2, p0, Ldsl;->f:[Ldvo;

    if-eqz v2, :cond_4

    .line 10812
    iget-object v3, p0, Ldsl;->f:[Ldvo;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    .line 10813
    if-eqz v5, :cond_3

    .line 10814
    const/4 v6, 0x5

    .line 10815
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 10812
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 10819
    :cond_4
    iget-object v2, p0, Ldsl;->g:[Ldro;

    if-eqz v2, :cond_6

    .line 10820
    iget-object v2, p0, Ldsl;->g:[Ldro;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 10821
    if-eqz v4, :cond_5

    .line 10822
    const/4 v5, 0x7

    .line 10823
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 10820
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 10827
    :cond_6
    iget-object v1, p0, Ldsl;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10828
    iput v0, p0, Ldsl;->cachedSize:I

    .line 10829
    return v0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 10731
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Ldsl;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldsl;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ldsl;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldsl;->b:Ldvn;

    if-nez v0, :cond_2

    new-instance v0, Ldvn;

    invoke-direct {v0}, Ldvn;-><init>()V

    iput-object v0, p0, Ldsl;->b:Ldvn;

    :cond_2
    iget-object v0, p0, Ldsl;->b:Ldvn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldsl;->d:Ldql;

    if-nez v0, :cond_3

    new-instance v0, Ldql;

    invoke-direct {v0}, Ldql;-><init>()V

    iput-object v0, p0, Ldsl;->d:Ldql;

    :cond_3
    iget-object v0, p0, Ldsl;->d:Ldql;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-ne v0, v4, :cond_4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldsl;->c:Ljava/lang/Integer;

    goto :goto_0

    :cond_4
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldsl;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldsl;->e:Lduw;

    if-nez v0, :cond_5

    new-instance v0, Lduw;

    invoke-direct {v0}, Lduw;-><init>()V

    iput-object v0, p0, Ldsl;->e:Lduw;

    :cond_5
    iget-object v0, p0, Ldsl;->e:Lduw;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldsl;->f:[Ldvo;

    if-nez v0, :cond_7

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ldvo;

    iget-object v3, p0, Ldsl;->f:[Ldvo;

    if-eqz v3, :cond_6

    iget-object v3, p0, Ldsl;->f:[Ldvo;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    iput-object v2, p0, Ldsl;->f:[Ldvo;

    :goto_2
    iget-object v2, p0, Ldsl;->f:[Ldvo;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    iget-object v2, p0, Ldsl;->f:[Ldvo;

    new-instance v3, Ldvo;

    invoke-direct {v3}, Ldvo;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldsl;->f:[Ldvo;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_7
    iget-object v0, p0, Ldsl;->f:[Ldvo;

    array-length v0, v0

    goto :goto_1

    :cond_8
    iget-object v2, p0, Ldsl;->f:[Ldvo;

    new-instance v3, Ldvo;

    invoke-direct {v3}, Ldvo;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldsl;->f:[Ldvo;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_6
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Ldsl;->g:[Ldro;

    if-nez v0, :cond_a

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ldro;

    iget-object v3, p0, Ldsl;->g:[Ldro;

    if-eqz v3, :cond_9

    iget-object v3, p0, Ldsl;->g:[Ldro;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_9
    iput-object v2, p0, Ldsl;->g:[Ldro;

    :goto_4
    iget-object v2, p0, Ldsl;->g:[Ldro;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_b

    iget-object v2, p0, Ldsl;->g:[Ldro;

    new-instance v3, Ldro;

    invoke-direct {v3}, Ldro;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldsl;->g:[Ldro;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_a
    iget-object v0, p0, Ldsl;->g:[Ldro;

    array-length v0, v0

    goto :goto_3

    :cond_b
    iget-object v2, p0, Ldsl;->g:[Ldro;

    new-instance v3, Ldro;

    invoke-direct {v3}, Ldro;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Ldsl;->g:[Ldro;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x3a -> :sswitch_6
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 10762
    iget-object v1, p0, Ldsl;->b:Ldvn;

    if-eqz v1, :cond_0

    .line 10763
    const/4 v1, 0x1

    iget-object v2, p0, Ldsl;->b:Ldvn;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 10765
    :cond_0
    iget-object v1, p0, Ldsl;->d:Ldql;

    if-eqz v1, :cond_1

    .line 10766
    const/4 v1, 0x2

    iget-object v2, p0, Ldsl;->d:Ldql;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 10768
    :cond_1
    iget-object v1, p0, Ldsl;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 10769
    const/4 v1, 0x3

    iget-object v2, p0, Ldsl;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 10771
    :cond_2
    iget-object v1, p0, Ldsl;->e:Lduw;

    if-eqz v1, :cond_3

    .line 10772
    const/4 v1, 0x4

    iget-object v2, p0, Ldsl;->e:Lduw;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 10774
    :cond_3
    iget-object v1, p0, Ldsl;->f:[Ldvo;

    if-eqz v1, :cond_5

    .line 10775
    iget-object v2, p0, Ldsl;->f:[Ldvo;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 10776
    if-eqz v4, :cond_4

    .line 10777
    const/4 v5, 0x5

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 10775
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 10781
    :cond_5
    iget-object v1, p0, Ldsl;->g:[Ldro;

    if-eqz v1, :cond_7

    .line 10782
    iget-object v1, p0, Ldsl;->g:[Ldro;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_7

    aget-object v3, v1, v0

    .line 10783
    if-eqz v3, :cond_6

    .line 10784
    const/4 v4, 0x7

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 10782
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 10788
    :cond_7
    iget-object v0, p0, Ldsl;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 10790
    return-void
.end method
