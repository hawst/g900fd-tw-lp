.class public Lbey;
.super Lbed;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final a:Lbdk;

.field public final b:I

.field public final i:[I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2770
    invoke-direct {p0, p1}, Lbed;-><init>(Ljava/lang/String;)V

    .line 2771
    const/4 v0, 0x1

    iput v0, p0, Lbey;->b:I

    .line 2772
    const/4 v0, 0x0

    iput-object v0, p0, Lbey;->a:Lbdk;

    .line 2773
    const/4 v0, 0x0

    new-array v0, v0, [I

    iput-object v0, p0, Lbey;->i:[I

    .line 2774
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I[I)V
    .locals 1

    .prologue
    .line 2778
    invoke-direct {p0, p1}, Lbed;-><init>(Ljava/lang/String;)V

    .line 2779
    iput p2, p0, Lbey;->b:I

    .line 2780
    const/4 v0, 0x0

    iput-object v0, p0, Lbey;->a:Lbdk;

    .line 2781
    iput-object p3, p0, Lbey;->i:[I

    .line 2782
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 3

    .prologue
    .line 2786
    const-string v0, "Babel_RequestWriter"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2787
    const-string v0, "Babel_RequestWriter"

    const-string v1, "replyToInviteRequest build protobuf"

    invoke-static {v0, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2789
    :cond_0
    new-instance v0, Ldvk;

    invoke-direct {v0}, Ldvk;-><init>()V

    .line 2790
    invoke-virtual {p0, p1, p2}, Lbey;->c(Ljava/lang/String;I)Ldvm;

    move-result-object v1

    iput-object v1, v0, Ldvk;->b:Ldvm;

    .line 2791
    iget v1, p0, Lbey;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Ldvk;->e:Ljava/lang/Integer;

    .line 2792
    iget-object v1, p0, Lbey;->c:Ljava/lang/String;

    invoke-static {v1}, Lbea;->d(Ljava/lang/String;)Ldqf;

    move-result-object v1

    iput-object v1, v0, Ldvk;->c:Ldqf;

    .line 2793
    iget-object v1, p0, Lbey;->i:[I

    iget-object v2, p0, Lbey;->i:[I

    array-length v2, v2

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v1

    iput-object v1, v0, Ldvk;->f:[I

    .line 2794
    return-object v0
.end method

.method public a(Lyj;Lbph;)V
    .locals 2

    .prologue
    .line 2809
    invoke-super {p0, p1, p2}, Lbed;->a(Lyj;Lbph;)V

    .line 2811
    iget-boolean v0, p0, Lbey;->h:Z

    if-nez v0, :cond_0

    .line 2812
    invoke-virtual {p1}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lbey;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2815
    :cond_0
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2799
    const-string v0, "conversations/replytoinvite"

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2804
    const-string v0, "event_queue"

    return-object v0
.end method
