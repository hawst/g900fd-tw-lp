.class public final Lapv;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lapv;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lyj;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lapv;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->a(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)Lyj;

    move-result-object v0

    return-object v0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lapv;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->g(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setVisibility(I)V

    .line 143
    iget-object v0, p0, Lapv;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->h(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)V

    .line 144
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 134
    iget-object v1, p0, Lapv;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    iget-object v0, p0, Lapv;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->f(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, p1, v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->a(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;Ljava/lang/String;Z)V

    .line 135
    return-void

    .line 134
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ly;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lapv;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->b(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    move-result-object v0

    return-object v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lapv;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)I

    move-result v0

    return v0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lapv;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->d(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)V

    .line 102
    iget-object v0, p0, Lapv;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->a(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;I)V

    .line 103
    return-void
.end method

.method public e()Lae;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lapv;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->getFragmentManager()Lae;

    move-result-object v0

    return-object v0
.end method

.method public f()Lav;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lapv;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->getLoaderManager()Lav;

    move-result-object v0

    return-object v0
.end method

.method public g()Landroid/view/accessibility/AccessibilityManager;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lapv;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    iget-object v0, v0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->a:Landroid/view/accessibility/AccessibilityManager;

    return-object v0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lapv;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->r()I

    move-result v0

    return v0
.end method

.method public i()I
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 122
    iget-object v1, p0, Lapv;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->e(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)Lapk;

    move-result-object v1

    invoke-virtual {v1}, Lapk;->c()Lapx;

    move-result-object v1

    .line 123
    iget-object v2, p0, Lapv;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v2}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->a(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)Lyj;

    move-result-object v2

    invoke-virtual {v2}, Lyj;->s()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 124
    const/4 v0, 0x0

    .line 128
    :cond_0
    :goto_0
    return v0

    .line 125
    :cond_1
    if-eqz v1, :cond_0

    .line 128
    invoke-virtual {v1}, Lapx;->w()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lapv;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->f(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)Z

    move-result v0

    return v0
.end method

.method public k()V
    .locals 3

    .prologue
    .line 147
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HangoutFragment onExit with state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lapv;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v2}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    :cond_0
    iget-object v0, p0, Lapv;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->e(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)Lapk;

    move-result-object v0

    invoke-virtual {v0}, Lapk;->c()Lapx;

    move-result-object v0

    .line 151
    if-eqz v0, :cond_1

    .line 152
    const/16 v1, 0x3ec

    invoke-virtual {v0, v1}, Lapx;->c(I)V

    .line 155
    :cond_1
    iget-object v0, p0, Lapv;->a:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->b(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->p()V

    .line 156
    return-void
.end method
