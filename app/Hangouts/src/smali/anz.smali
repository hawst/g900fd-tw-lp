.class public final Lanz;
.super Lbor;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;)V
    .locals 0

    .prologue
    .line 527
    iput-object p1, p0, Lanz;->a:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    invoke-direct {p0}, Lbor;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILbkr;Lbos;)V
    .locals 6

    .prologue
    .line 536
    iget-object v0, p0, Lanz;->a:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->f(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;)I

    move-result v0

    if-eq p1, v0, :cond_0

    .line 564
    :goto_0
    return-void

    .line 542
    :cond_0
    :try_start_0
    iget-object v0, p0, Lanz;->a:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->getActivity()Ly;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-nez v0, :cond_1

    .line 563
    iget-object v0, p0, Lanz;->a:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->h(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;)Lbor;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lbor;)V

    goto :goto_0

    .line 545
    :cond_1
    :try_start_1
    invoke-virtual {p3}, Lbos;->b()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 546
    const/4 v0, 0x5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, Lanz;->a:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->d(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 547
    const/4 v0, 0x6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, Lanz;->a:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->d(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 548
    iget-object v0, p0, Lanz;->a:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->b(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;)Laod;

    move-result-object v0

    iget-object v1, p2, Lbkr;->a:Ljava/lang/String;

    iget-object v2, p2, Lbkr;->d:Lyj;

    iget v3, p2, Lbkr;->b:I

    iget-object v4, p0, Lanz;->a:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    .line 552
    invoke-static {v4}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->d(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;)I

    iget-boolean v4, p2, Lbkr;->c:Z

    iget-object v5, p0, Lanz;->a:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    .line 554
    invoke-static {v5}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->g(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;)Labj;

    move-result-object v5

    invoke-virtual {v5}, Labj;->d()Lxm;

    move-result-object v5

    .line 548
    invoke-interface/range {v0 .. v5}, Laod;->a(Ljava/lang/String;Lyj;IZLxm;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 563
    :goto_1
    iget-object v0, p0, Lanz;->a:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->h(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;)Lbor;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lbor;)V

    goto :goto_0

    .line 556
    :cond_2
    :try_start_2
    const-string v1, "Babel"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "couldn\'t create conversation; error code: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 557
    invoke-virtual {p3}, Lbos;->b()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p2, :cond_3

    iget-object v0, p2, Lbkr;->a:Ljava/lang/String;

    :goto_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 556
    invoke-static {v1, v0}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 559
    iget-object v0, p0, Lanz;->a:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->b(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;)Laod;

    move-result-object v0

    invoke-virtual {p3}, Lbos;->b()I

    move-result v1

    invoke-interface {v0, v1}, Laod;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 563
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lanz;->a:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->h(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;)Lbor;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lbor;)V

    throw v0

    .line 557
    :cond_3
    :try_start_3
    const-string v0, "NULL"
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2
.end method
