.class public Lbfn;
.super Lbep;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbzo",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbzo",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 4313
    invoke-direct {p0}, Lbep;-><init>()V

    .line 4314
    iput-object p1, p0, Lbfn;->a:Ljava/util/List;

    .line 4315
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lepr;
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 4319
    new-instance v4, Ldwm;

    invoke-direct {v4}, Ldwm;-><init>()V

    .line 4322
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 4324
    iget-object v0, p0, Lbfn;->a:Ljava/util/List;

    .line 4325
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ldvo;

    iput-object v0, v4, Ldwm;->c:[Ldvo;

    .line 4329
    iget-object v0, p0, Lbfn;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v3

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzo;

    .line 4330
    new-instance v7, Ldvo;

    invoke-direct {v7}, Ldvo;-><init>()V

    .line 4333
    iget-object v1, v0, Lbzo;->a:Ljava/io/Serializable;

    check-cast v1, Ljava/lang/Integer;

    invoke-static {v1, v3}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v1

    .line 4334
    iget-object v0, v0, Lbzo;->b:Ljava/io/Serializable;

    check-cast v0, Ljava/lang/Boolean;

    invoke-static {v0, v3}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    .line 4337
    sget-boolean v8, Lbea;->d:Z

    if-eqz v8, :cond_0

    .line 4338
    const-string v8, "{"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "} "

    .line 4339
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4342
    :cond_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v7, Ldvo;->b:Ljava/lang/Integer;

    .line 4343
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v7, Ldvo;->c:Ljava/lang/Boolean;

    .line 4345
    iget-object v0, v4, Ldwm;->c:[Ldvo;

    aput-object v7, v0, v2

    .line 4346
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    .line 4347
    goto :goto_0

    .line 4349
    :cond_1
    sget-boolean v0, Lbea;->d:Z

    if-eqz v0, :cond_2

    .line 4350
    const-string v0, "Babel_RequestWriter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SetRichPresenceEnabledStateRequest build protobuf. Values: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 4351
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 4350
    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4354
    :cond_2
    invoke-virtual {p0, p1, p2}, Lbfn;->c(Ljava/lang/String;I)Ldvm;

    move-result-object v0

    iput-object v0, v4, Ldwm;->b:Ldvm;

    .line 4355
    return-object v4
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4360
    const-string v0, "presence/setrichpresenceenabledstate"

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4365
    const-string v0, "event_queue"

    return-object v0
.end method
