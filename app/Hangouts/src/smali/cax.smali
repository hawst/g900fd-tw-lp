.class public abstract Lcax;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Landroid/widget/Checkable;


# static fields
.field private static final a:Landroid/text/style/StyleSpan;

.field private static b:Landroid/text/style/ForegroundColorSpan;


# instance fields
.field private c:Z

.field private d:Lcaz;

.field private final e:Lxg;

.field private final f:Landroid/text/SpannableStringBuilder;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 40
    new-instance v0, Landroid/text/style/StyleSpan;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    sput-object v0, Lcax;->a:Landroid/text/style/StyleSpan;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcax;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v0, p0, Lcax;->f:Landroid/text/SpannableStringBuilder;

    .line 72
    sget-object v0, Lcax;->b:Landroid/text/style/ForegroundColorSpan;

    if-nez v0, :cond_0

    .line 73
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 74
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    sget v2, Lf;->cF:I

    .line 75
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-direct {v1, v0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    sput-object v1, Lcax;->b:Landroid/text/style/ForegroundColorSpan;

    .line 77
    :cond_0
    const/4 v0, 0x0

    const-string v1, "alpha"

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Lxg;->a(Ljava/lang/Object;Ljava/lang/String;[F)Lxg;

    move-result-object v0

    iput-object v0, p0, Lcax;->e:Lxg;

    .line 78
    return-void

    .line 77
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method protected a(Laeh;)V
    .locals 0

    .prologue
    .line 341
    return-void
.end method

.method protected a(Landroid/view/ViewGroup;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ljava/util/ArrayList",
            "<",
            "Laeh;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 232
    invoke-virtual/range {p1 .. p1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 233
    if-eqz p2, :cond_a

    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_a

    .line 234
    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 235
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laeh;

    .line 236
    invoke-virtual/range {p0 .. p0}, Lcax;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    sget v3, Lf;->gk:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v2, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v9

    sget v2, Lg;->fj:I

    invoke-virtual {v9, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    sget v3, Lg;->fo:I

    invoke-virtual {v9, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v4, v1, Laeh;->a:Ljava/lang/String;

    invoke-static {v4}, Lbzd;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const/4 v4, 0x3

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v11

    const/4 v7, 0x0

    :goto_1
    if-ge v7, v11, :cond_9

    const/4 v6, 0x0

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v12

    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v13

    move v5, v7

    :goto_2
    if-ge v5, v12, :cond_7

    if-ge v6, v13, :cond_7

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v14

    invoke-virtual {v10, v5}, Ljava/lang/String;->charAt(I)C

    move-result v15

    invoke-static {v14}, Ljava/lang/Character;->isDigit(C)Z

    move-result v16

    const-string v4, " -()[].,+"

    invoke-virtual {v4, v14}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    const/16 v17, -0x1

    move/from16 v0, v17

    if-eq v4, v0, :cond_3

    const/4 v4, 0x1

    :goto_3
    invoke-static {v15}, Ljava/lang/Character;->isDigit(C)Z

    move-result v17

    if-nez v17, :cond_0

    if-nez v4, :cond_0

    if-eqz v16, :cond_7

    :cond_0
    if-eqz v16, :cond_5

    if-eqz v17, :cond_5

    if-eq v14, v15, :cond_4

    const/4 v4, 0x0

    :goto_4
    if-le v4, v7, :cond_8

    :goto_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcax;->f:Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v5, v10, v4}, Lcax;->a(Landroid/widget/TextView;Landroid/text/SpannableStringBuilder;Ljava/lang/String;I)V

    iget-object v2, v1, Laeh;->d:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, v1, Laeh;->d:Ljava/lang/String;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    new-instance v2, Lcay;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v1}, Lcay;-><init>(Lcax;Laeh;)V

    invoke-virtual {v9, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual/range {p0 .. p0}, Lcax;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget v4, Lh;->kl:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lf;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v1, Laeh;->d:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v1, v1, Laeh;->d:Ljava/lang/String;

    invoke-static {v3, v1}, Lf;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    invoke-static {v3, v10}, Lf;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto/16 :goto_0

    :cond_3
    const/4 v4, 0x0

    goto :goto_3

    :cond_4
    add-int/lit8 v6, v6, 0x1

    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_5
    if-nez v16, :cond_6

    add-int/lit8 v4, v6, 0x1

    move v6, v4

    :cond_6
    if-nez v17, :cond_c

    add-int/lit8 v4, v5, 0x1

    :goto_6
    move v5, v4

    goto/16 :goto_2

    :cond_7
    move v4, v5

    goto :goto_4

    :cond_8
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_1

    :cond_9
    const/4 v4, 0x0

    goto :goto_5

    .line 239
    :cond_a
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 241
    :cond_b
    return-void

    :cond_c
    move v4, v5

    goto :goto_6
.end method

.method protected a(Landroid/widget/TextView;Landroid/text/SpannableStringBuilder;Ljava/lang/String;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 215
    if-gtz p4, :cond_0

    .line 216
    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 225
    :goto_0
    return-void

    .line 220
    :cond_0
    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->clear()V

    .line 221
    invoke-virtual {p2, p3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 222
    sget-object v0, Lcax;->a:Landroid/text/style/StyleSpan;

    invoke-virtual {p2, v0, v1, p4, v1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 223
    sget-object v0, Lcax;->b:Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p2, v0, v1, p4, v1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 224
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected a(Landroid/widget/TextView;Ljava/lang/String;Landroid/text/SpannableStringBuilder;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 191
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 192
    :cond_0
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 208
    :goto_0
    return-void

    .line 196
    :cond_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 197
    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 198
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 202
    :cond_2
    invoke-virtual {p3}, Landroid/text/SpannableStringBuilder;->clear()V

    .line 203
    invoke-virtual {p3, p2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 204
    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, v0

    .line 205
    sget-object v2, Lcax;->a:Landroid/text/style/StyleSpan;

    invoke-virtual {p3, v2, v0, v1, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 206
    sget-object v2, Lcax;->b:Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p3, v2, v0, v1, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 207
    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected a(Z)V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 139
    sget v0, Lg;->am:I

    invoke-virtual {p0, v0}, Lcax;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 141
    instance-of v0, v1, Landroid/widget/Checkable;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 142
    check-cast v0, Landroid/widget/Checkable;

    iget-boolean v6, p0, Lcax;->c:Z

    invoke-interface {v0, v6}, Landroid/widget/Checkable;->setChecked(Z)V

    .line 145
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0xb

    if-lt v0, v6, :cond_6

    .line 146
    iget-object v0, p0, Lcax;->e:Lxg;

    invoke-virtual {v0}, Lxg;->b()V

    if-eqz p1, :cond_4

    iget-object v0, p0, Lcax;->e:Lxg;

    invoke-virtual {v0, v1}, Lxg;->a(Ljava/lang/Object;)V

    iget-object v6, p0, Lcax;->e:Lxg;

    const/4 v0, 0x2

    new-array v7, v0, [F

    invoke-virtual {v1}, Landroid/view/View;->getAlpha()F

    move-result v0

    aput v0, v7, v4

    iget-boolean v0, p0, Lcax;->c:Z

    if-eqz v0, :cond_2

    move v0, v2

    :goto_0
    aput v0, v7, v5

    invoke-virtual {v6, v7}, Lxg;->a([F)V

    invoke-virtual {v1}, Landroid/view/View;->getAlpha()F

    move-result v0

    iget-boolean v1, p0, Lcax;->c:Z

    if-eqz v1, :cond_3

    :goto_1
    sub-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget-object v1, p0, Lcax;->e:Lxg;

    const/high16 v2, 0x43480000    # 200.0f

    mul-float/2addr v0, v2

    float-to-int v0, v0

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Lxg;->b(J)Lxg;

    iget-object v0, p0, Lcax;->e:Lxg;

    invoke-virtual {v0}, Lxg;->a()V

    .line 151
    :goto_2
    sget v0, Lg;->eH:I

    invoke-virtual {p0, v0}, Lcax;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 152
    const/4 v2, 0x0

    iget-boolean v1, p0, Lcax;->c:Z

    if-eqz v1, :cond_8

    move v1, v5

    :goto_3
    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 153
    iget-boolean v1, p0, Lcax;->c:Z

    if-eqz v1, :cond_9

    const/high16 v1, -0x34000000    # -3.3554432E7f

    :goto_4
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 154
    iget-boolean v0, p0, Lcax;->c:Z

    if-eqz v0, :cond_1

    const/4 v4, -0x1

    :cond_1
    invoke-virtual {p0, v4}, Lcax;->setBackgroundColor(I)V

    .line 155
    return-void

    :cond_2
    move v0, v3

    .line 146
    goto :goto_0

    :cond_3
    move v2, v3

    goto :goto_1

    :cond_4
    iget-boolean v0, p0, Lcax;->c:Z

    if-eqz v0, :cond_5

    :goto_5
    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    goto :goto_2

    :cond_5
    move v2, v3

    goto :goto_5

    .line 148
    :cond_6
    iget-boolean v0, p0, Lcax;->c:Z

    if-eqz v0, :cond_7

    move v0, v4

    :goto_6
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_7
    const/4 v0, 0x4

    goto :goto_6

    :cond_8
    move v1, v4

    .line 152
    goto :goto_3

    .line 153
    :cond_9
    const/high16 v1, -0x66000000

    goto :goto_4
.end method

.method public isChecked()Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcax;->c:Z

    return v0
.end method

.method public isLocked(Laoe;)Z
    .locals 1

    .prologue
    .line 344
    const/4 v0, 0x0

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 82
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 83
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcax;->a(Z)V

    .line 84
    return-void
.end method

.method public onCancel()V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcax;->d:Lcaz;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcax;->d:Lcaz;

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Lcaz;->a(Lcax;Z)V

    .line 100
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 87
    invoke-virtual {p0}, Lcax;->toggle()V

    .line 88
    return-void
.end method

.method public reset()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 112
    invoke-virtual {p0, v0, v0}, Lcax;->setChecked(ZZ)V

    .line 113
    return-void
.end method

.method public resumeLoading()V
    .locals 0

    .prologue
    .line 184
    return-void
.end method

.method public final setChecked(Z)V
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcax;->setChecked(ZZ)V

    .line 118
    return-void
.end method

.method public final setChecked(ZZ)V
    .locals 2

    .prologue
    .line 121
    iget-boolean v0, p0, Lcax;->c:Z

    if-ne v0, p1, :cond_1

    .line 131
    :cond_0
    :goto_0
    return-void

    .line 125
    :cond_1
    iput-boolean p1, p0, Lcax;->c:Z

    .line 126
    invoke-virtual {p0, p2}, Lcax;->a(Z)V

    .line 128
    iget-object v0, p0, Lcax;->d:Lcaz;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcax;->d:Lcaz;

    iget-boolean v1, p0, Lcax;->c:Z

    invoke-interface {v0, p0, v1}, Lcaz;->a(Lcax;Z)V

    goto :goto_0
.end method

.method public setOnItemCheckedChangeListener(Lcaz;)V
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lcax;->d:Lcaz;

    .line 104
    return-void
.end method

.method public toggle()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 175
    iget-boolean v0, p0, Lcax;->c:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0, v1}, Lcax;->setChecked(ZZ)V

    .line 176
    return-void

    .line 175
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
