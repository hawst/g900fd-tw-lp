.class public final Lbjo;
.super Lbis;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final b:I

.field public final f:I


# direct methods
.method public constructor <init>(Ldwf;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1092
    iget-object v0, p1, Ldwf;->b:Ldqf;

    iget-object v0, v0, Ldqf;->b:Ljava/lang/String;

    new-instance v1, Lbdk;

    iget-object v2, p1, Ldwf;->c:Ldui;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lbdk;-><init>(Ldui;Ljava/lang/String;)V

    iget-object v2, p1, Ldwf;->d:Ljava/lang/Long;

    .line 1094
    invoke-static {v2}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v2

    .line 1092
    invoke-direct {p0, v0, v1, v2, v3}, Lbis;-><init>(Ljava/lang/String;Lbdk;J)V

    .line 1095
    iget-object v0, p1, Ldwf;->e:Ljava/lang/Integer;

    invoke-static {v0, v4}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    iput v0, p0, Lbjo;->b:I

    .line 1096
    iget-object v0, p1, Ldwf;->f:Ljava/lang/Integer;

    invoke-static {v0, v4}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    iput v0, p0, Lbjo;->f:I

    .line 1097
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILbdk;J)V
    .locals 1

    .prologue
    .line 1107
    invoke-direct {p0, p1, p3, p4, p5}, Lbis;-><init>(Ljava/lang/String;Lbdk;J)V

    .line 1108
    iput p2, p0, Lbjo;->b:I

    .line 1109
    const/16 v0, 0xa

    iput v0, p0, Lbjo;->f:I

    .line 1110
    return-void
.end method

.method public static a(Ljava/lang/String;ILbdk;J)Lbjo;
    .locals 6

    .prologue
    .line 1102
    new-instance v0, Lbjo;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lbjo;-><init>(Ljava/lang/String;ILbdk;J)V

    return-object v0
.end method
