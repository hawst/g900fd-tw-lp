.class public final Ldtf;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldtf;


# instance fields
.field public b:Leir;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25165
    const/4 v0, 0x0

    new-array v0, v0, [Ldtf;

    sput-object v0, Ldtf;->a:[Ldtf;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25166
    invoke-direct {p0}, Lepn;-><init>()V

    .line 25169
    const/4 v0, 0x0

    iput-object v0, p0, Ldtf;->b:Leir;

    .line 25166
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 2

    .prologue
    .line 25183
    const/4 v0, 0x0

    .line 25184
    iget-object v1, p0, Ldtf;->b:Leir;

    if-eqz v1, :cond_0

    .line 25185
    const/4 v0, 0x1

    iget-object v1, p0, Ldtf;->b:Leir;

    .line 25186
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 25188
    :cond_0
    iget-object v1, p0, Ldtf;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25189
    iput v0, p0, Ldtf;->cachedSize:I

    .line 25190
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 25162
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldtf;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldtf;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldtf;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldtf;->b:Leir;

    if-nez v0, :cond_2

    new-instance v0, Leir;

    invoke-direct {v0}, Leir;-><init>()V

    iput-object v0, p0, Ldtf;->b:Leir;

    :cond_2
    iget-object v0, p0, Ldtf;->b:Leir;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 25174
    iget-object v0, p0, Ldtf;->b:Leir;

    if-eqz v0, :cond_0

    .line 25175
    const/4 v0, 0x1

    iget-object v1, p0, Ldtf;->b:Leir;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 25177
    :cond_0
    iget-object v0, p0, Ldtf;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 25179
    return-void
.end method
