.class public final Leui;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leui;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Leuh;

.field public d:Leuh;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    new-array v0, v0, [Leui;

    sput-object v0, Leui;->a:[Leui;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 83
    invoke-direct {p0}, Lepn;-><init>()V

    .line 88
    iput-object v0, p0, Leui;->c:Leuh;

    .line 91
    iput-object v0, p0, Leui;->d:Leuh;

    .line 83
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 111
    const/4 v0, 0x0

    .line 112
    iget-object v1, p0, Leui;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 113
    const/4 v0, 0x1

    iget-object v1, p0, Leui;->b:Ljava/lang/String;

    .line 114
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 116
    :cond_0
    iget-object v1, p0, Leui;->c:Leuh;

    if-eqz v1, :cond_1

    .line 117
    const/4 v1, 0x2

    iget-object v2, p0, Leui;->c:Leuh;

    .line 118
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 120
    :cond_1
    iget-object v1, p0, Leui;->d:Leuh;

    if-eqz v1, :cond_2

    .line 121
    const/4 v1, 0x3

    iget-object v2, p0, Leui;->d:Leuh;

    .line 122
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 124
    :cond_2
    iget-object v1, p0, Leui;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 125
    iput v0, p0, Leui;->cachedSize:I

    .line 126
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 79
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Leui;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Leui;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Leui;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leui;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Leui;->c:Leuh;

    if-nez v0, :cond_2

    new-instance v0, Leuh;

    invoke-direct {v0}, Leuh;-><init>()V

    iput-object v0, p0, Leui;->c:Leuh;

    :cond_2
    iget-object v0, p0, Leui;->c:Leuh;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Leui;->d:Leuh;

    if-nez v0, :cond_3

    new-instance v0, Leuh;

    invoke-direct {v0}, Leuh;-><init>()V

    iput-object v0, p0, Leui;->d:Leuh;

    :cond_3
    iget-object v0, p0, Leui;->d:Leuh;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Leui;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 97
    const/4 v0, 0x1

    iget-object v1, p0, Leui;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 99
    :cond_0
    iget-object v0, p0, Leui;->c:Leuh;

    if-eqz v0, :cond_1

    .line 100
    const/4 v0, 0x2

    iget-object v1, p0, Leui;->c:Leuh;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 102
    :cond_1
    iget-object v0, p0, Leui;->d:Leuh;

    if-eqz v0, :cond_2

    .line 103
    const/4 v0, 0x3

    iget-object v1, p0, Leui;->d:Leuh;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 105
    :cond_2
    iget-object v0, p0, Leui;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 107
    return-void
.end method
