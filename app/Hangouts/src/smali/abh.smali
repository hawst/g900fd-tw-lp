.class final Labh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcvr;


# instance fields
.field final synthetic a:Labg;


# direct methods
.method constructor <init>(Labg;)V
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, Labh;->a:Labg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcft;Lcwf;)V
    .locals 7

    .prologue
    .line 196
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 197
    const-string v0, "Babel"

    const-string v1, "AudienceAdapter: onPeopleLoaded"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    invoke-virtual {p1}, Lcft;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_1

    .line 221
    :cond_0
    :goto_0
    return-void

    .line 202
    :cond_1
    const/4 v0, 0x0

    .line 203
    invoke-virtual {p2}, Lcwf;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcwe;

    .line 204
    iget-object v5, p0, Labh;->a:Labg;

    invoke-static {v5}, Labg;->a(Labg;)Lamd;

    move-result-object v5

    .line 205
    invoke-interface {v0}, Lcwe;->g()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0}, Lcwe;->i()[Ljava/lang/String;

    move-result-object v0

    .line 204
    invoke-virtual {v5, v6, v0}, Lamd;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    or-int/2addr v0, v1

    move v1, v0

    .line 206
    goto :goto_1

    .line 207
    :cond_2
    invoke-virtual {p2}, Lcwf;->d()V

    .line 211
    if-eqz v1, :cond_3

    .line 212
    iget-object v0, p0, Labh;->a:Labg;

    invoke-static {v0}, Labg;->b(Labg;)V

    .line 213
    iget-object v0, p0, Labh;->a:Labg;

    invoke-virtual {v0}, Labg;->notifyDataSetChanged()V

    .line 216
    :cond_3
    invoke-static {}, Labg;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "AudienceAdapter: onPeopleLoadedTotal: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 218
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 219
    invoke-virtual {p2}, Lcwf;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " people."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 217
    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
