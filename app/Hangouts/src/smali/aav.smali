.class final Laav;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Laav;",
        ">;"
    }
.end annotation


# static fields
.field private static e:J


# instance fields
.field public final a:J

.field public final b:J

.field public final c:Lbdk;

.field public final d:Lbjr;


# direct methods
.method public constructor <init>(J)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-wide p1, p0, Laav;->a:J

    .line 53
    iput-object v2, p0, Laav;->c:Lbdk;

    .line 54
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Laav;->b:J

    .line 55
    iput-object v2, p0, Laav;->d:Lbjr;

    .line 56
    return-void
.end method

.method public constructor <init>(Lbjr;)V
    .locals 4

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Laav;->d:Lbjr;

    .line 46
    iget-wide v0, p1, Lbjr;->e:J

    iput-wide v0, p0, Laav;->a:J

    .line 47
    iget-object v0, p1, Lbjr;->d:Lbdk;

    iput-object v0, p0, Laav;->c:Lbdk;

    .line 48
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x64

    mul-long/2addr v0, v2

    sget-wide v2, Laav;->e:J

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    sget-wide v0, Laav;->e:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    :cond_0
    sput-wide v0, Laav;->e:J

    iput-wide v0, p0, Laav;->b:J

    .line 49
    return-void
.end method


# virtual methods
.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 37
    check-cast p1, Laav;

    iget-wide v0, p0, Laav;->a:J

    iget-wide v2, p1, Laav;->a:J

    sub-long/2addr v0, v2

    cmp-long v2, v0, v4

    if-nez v2, :cond_1

    iget-wide v0, p0, Laav;->b:J

    iget-wide v2, p1, Laav;->b:J

    sub-long/2addr v0, v2

    cmp-long v2, v0, v4

    if-nez v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    div-long/2addr v0, v2

    long-to-int v0, v0

    goto :goto_0

    :cond_1
    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    div-long/2addr v0, v2

    long-to-int v0, v0

    goto :goto_0
.end method
