.class public final Lcdr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;

.field final synthetic b:Lbdh;

.field final synthetic c:Lcdu;

.field final synthetic d:I

.field final synthetic e:I

.field final synthetic f:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;Lbdh;Lcdu;II)V
    .locals 0

    .prologue
    .line 762
    iput-object p1, p0, Lcdr;->f:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    iput-object p2, p0, Lcdr;->a:Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;

    iput-object p3, p0, Lcdr;->b:Lbdh;

    iput-object p4, p0, Lcdr;->c:Lcdu;

    iput p5, p0, Lcdr;->d:I

    iput p6, p0, Lcdr;->e:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 767
    iget-object v0, p0, Lcdr;->a:Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->c()I

    move-result v0

    .line 768
    iget-object v1, p0, Lcdr;->a:Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->f(I)V

    .line 769
    if-eqz v0, :cond_1

    .line 771
    iget-object v1, p0, Lcdr;->a:Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->e(I)V

    .line 776
    iget-object v1, p0, Lcdr;->f:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    iget-object v2, p0, Lcdr;->a:Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;

    iget-object v3, p0, Lcdr;->f:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    invoke-static {v3, v0}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->a(Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;I)I

    move-result v3

    iget-object v4, p0, Lcdr;->b:Lbdh;

    iget-object v4, v4, Lbdh;->h:Ljava/lang/String;

    invoke-static {v1, v2, v3, v0, v4}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->a(Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;IILjava/lang/String;)V

    .line 784
    :goto_0
    iget-object v1, p0, Lcdr;->f:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    iget-object v2, p0, Lcdr;->b:Lbdh;

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->a(Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;Lbdh;)V

    .line 785
    iget-object v1, p0, Lcdr;->c:Lcdu;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcdu;->a(Z)V

    .line 786
    invoke-static {}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 787
    iget-object v1, p0, Lcdr;->f:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    iget-object v2, p0, Lcdr;->a:Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Move (end) "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcdr;->d:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " -> "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcdr;->e:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "(actual "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->a(Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;Ljava/lang/String;)V

    .line 792
    :cond_0
    return-void

    .line 780
    :cond_1
    iget-object v1, p0, Lcdr;->f:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->g(Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;)Lbyz;

    move-result-object v1

    monitor-enter v1

    .line 781
    :try_start_0
    iget-object v2, p0, Lcdr;->f:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    invoke-static {v2}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->g(Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;)Lbyz;

    move-result-object v2

    iget-object v3, p0, Lcdr;->b:Lbdh;

    iget-object v3, v3, Lbdh;->b:Lbdk;

    invoke-virtual {v2, v3}, Lbyz;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 782
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
