.class public final Lexu;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lexu;


# instance fields
.field public b:[Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:[Lext;

.field public e:Ljava/lang/Boolean;

.field public f:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 440
    const/4 v0, 0x0

    new-array v0, v0, [Lexu;

    sput-object v0, Lexu;->a:[Lexu;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 441
    invoke-direct {p0}, Lepn;-><init>()V

    .line 444
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Lexu;->b:[Ljava/lang/String;

    .line 449
    sget-object v0, Lext;->a:[Lext;

    iput-object v0, p0, Lexu;->d:[Lext;

    .line 441
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 486
    iget-object v0, p0, Lexu;->b:[Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lexu;->b:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_6

    .line 488
    iget-object v3, p0, Lexu;->b:[Ljava/lang/String;

    array-length v4, v3

    move v0, v1

    move v2, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    .line 490
    invoke-static {v5}, Lepl;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 488
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 493
    :cond_0
    iget-object v0, p0, Lexu;->b:[Ljava/lang/String;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v2

    .line 495
    :goto_1
    iget-object v2, p0, Lexu;->c:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 496
    const/4 v2, 0x2

    iget-object v3, p0, Lexu;->c:Ljava/lang/String;

    .line 497
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 499
    :cond_1
    iget-object v2, p0, Lexu;->d:[Lext;

    if-eqz v2, :cond_3

    .line 500
    iget-object v2, p0, Lexu;->d:[Lext;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 501
    if-eqz v4, :cond_2

    .line 502
    const/4 v5, 0x3

    .line 503
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 500
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 507
    :cond_3
    iget-object v1, p0, Lexu;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 508
    const/4 v1, 0x4

    iget-object v2, p0, Lexu;->e:Ljava/lang/Boolean;

    .line 509
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 511
    :cond_4
    iget-object v1, p0, Lexu;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 512
    const/4 v1, 0x5

    iget-object v2, p0, Lexu;->f:Ljava/lang/Boolean;

    .line 513
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 515
    :cond_5
    iget-object v1, p0, Lexu;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 516
    iput v0, p0, Lexu;->cachedSize:I

    .line 517
    return v0

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 437
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lexu;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lexu;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lexu;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lexu;->b:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Lexu;->b:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Lexu;->b:[Ljava/lang/String;

    :goto_1
    iget-object v2, p0, Lexu;->b:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lexu;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lexu;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lexu;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lexu;->d:[Lext;

    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    add-int/2addr v2, v0

    new-array v2, v2, [Lext;

    iget-object v3, p0, Lexu;->d:[Lext;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lexu;->d:[Lext;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Lexu;->d:[Lext;

    :goto_3
    iget-object v2, p0, Lexu;->d:[Lext;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Lexu;->d:[Lext;

    new-instance v3, Lext;

    invoke-direct {v3}, Lext;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lexu;->d:[Lext;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lexu;->d:[Lext;

    array-length v0, v0

    goto :goto_2

    :cond_5
    iget-object v2, p0, Lexu;->d:[Lext;

    new-instance v3, Lext;

    invoke-direct {v3}, Lext;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lexu;->d:[Lext;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lexu;->e:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lexu;->f:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 458
    iget-object v1, p0, Lexu;->b:[Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 459
    iget-object v2, p0, Lexu;->b:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 460
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Lepl;->a(ILjava/lang/String;)V

    .line 459
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 463
    :cond_0
    iget-object v1, p0, Lexu;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 464
    const/4 v1, 0x2

    iget-object v2, p0, Lexu;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 466
    :cond_1
    iget-object v1, p0, Lexu;->d:[Lext;

    if-eqz v1, :cond_3

    .line 467
    iget-object v1, p0, Lexu;->d:[Lext;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 468
    if-eqz v3, :cond_2

    .line 469
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 467
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 473
    :cond_3
    iget-object v0, p0, Lexu;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 474
    const/4 v0, 0x4

    iget-object v1, p0, Lexu;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 476
    :cond_4
    iget-object v0, p0, Lexu;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 477
    const/4 v0, 0x5

    iget-object v1, p0, Lexu;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 479
    :cond_5
    iget-object v0, p0, Lexu;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 481
    return-void
.end method
