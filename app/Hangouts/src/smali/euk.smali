.class public final Leuk;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Leuk;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:[Leul;

.field public e:Levt;

.field public f:Levt;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 492
    const/4 v0, 0x0

    new-array v0, v0, [Leuk;

    sput-object v0, Leuk;->a:[Leuk;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 493
    invoke-direct {p0}, Lepn;-><init>()V

    .line 581
    sget-object v0, Leul;->a:[Leul;

    iput-object v0, p0, Leuk;->d:[Leul;

    .line 584
    iput-object v1, p0, Leuk;->e:Levt;

    .line 587
    iput-object v1, p0, Leuk;->f:Levt;

    .line 493
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 618
    iget-object v0, p0, Leuk;->b:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 619
    const/4 v0, 0x1

    iget-object v2, p0, Leuk;->b:Ljava/lang/String;

    .line 620
    invoke-static {v0, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 622
    :goto_0
    iget-object v2, p0, Leuk;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 623
    const/4 v2, 0x2

    iget-object v3, p0, Leuk;->c:Ljava/lang/String;

    .line 624
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 626
    :cond_0
    iget-object v2, p0, Leuk;->d:[Leul;

    if-eqz v2, :cond_2

    .line 627
    iget-object v2, p0, Leuk;->d:[Leul;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 628
    if-eqz v4, :cond_1

    .line 629
    const/4 v5, 0x3

    .line 630
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 627
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 634
    :cond_2
    iget-object v1, p0, Leuk;->e:Levt;

    if-eqz v1, :cond_3

    .line 635
    const/4 v1, 0x4

    iget-object v2, p0, Leuk;->e:Levt;

    .line 636
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 638
    :cond_3
    iget-object v1, p0, Leuk;->f:Levt;

    if-eqz v1, :cond_4

    .line 639
    const/4 v1, 0x5

    iget-object v2, p0, Leuk;->f:Levt;

    .line 640
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 642
    :cond_4
    iget-object v1, p0, Leuk;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 643
    iput v0, p0, Leuk;->cachedSize:I

    .line 644
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 489
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Leuk;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Leuk;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Leuk;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leuk;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leuk;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Leuk;->d:[Leul;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Leul;

    iget-object v3, p0, Leuk;->d:[Leul;

    if-eqz v3, :cond_2

    iget-object v3, p0, Leuk;->d:[Leul;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Leuk;->d:[Leul;

    :goto_2
    iget-object v2, p0, Leuk;->d:[Leul;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Leuk;->d:[Leul;

    new-instance v3, Leul;

    invoke-direct {v3}, Leul;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leuk;->d:[Leul;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Leuk;->d:[Leul;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Leuk;->d:[Leul;

    new-instance v3, Leul;

    invoke-direct {v3}, Leul;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Leuk;->d:[Leul;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Leuk;->e:Levt;

    if-nez v0, :cond_5

    new-instance v0, Levt;

    invoke-direct {v0}, Levt;-><init>()V

    iput-object v0, p0, Leuk;->e:Levt;

    :cond_5
    iget-object v0, p0, Leuk;->e:Levt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Leuk;->f:Levt;

    if-nez v0, :cond_6

    new-instance v0, Levt;

    invoke-direct {v0}, Levt;-><init>()V

    iput-object v0, p0, Leuk;->f:Levt;

    :cond_6
    iget-object v0, p0, Leuk;->f:Levt;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 5

    .prologue
    .line 592
    iget-object v0, p0, Leuk;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 593
    const/4 v0, 0x1

    iget-object v1, p0, Leuk;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 595
    :cond_0
    iget-object v0, p0, Leuk;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 596
    const/4 v0, 0x2

    iget-object v1, p0, Leuk;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 598
    :cond_1
    iget-object v0, p0, Leuk;->d:[Leul;

    if-eqz v0, :cond_3

    .line 599
    iget-object v1, p0, Leuk;->d:[Leul;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 600
    if-eqz v3, :cond_2

    .line 601
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 599
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 605
    :cond_3
    iget-object v0, p0, Leuk;->e:Levt;

    if-eqz v0, :cond_4

    .line 606
    const/4 v0, 0x4

    iget-object v1, p0, Leuk;->e:Levt;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 608
    :cond_4
    iget-object v0, p0, Leuk;->f:Levt;

    if-eqz v0, :cond_5

    .line 609
    const/4 v0, 0x5

    iget-object v1, p0, Leuk;->f:Levt;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 611
    :cond_5
    iget-object v0, p0, Leuk;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 613
    return-void
.end method
