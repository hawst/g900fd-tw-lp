.class public final Ldfs;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldfs;


# instance fields
.field public b:Ldfi;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3754
    const/4 v0, 0x0

    new-array v0, v0, [Ldfs;

    sput-object v0, Ldfs;->a:[Ldfs;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3755
    invoke-direct {p0}, Lepn;-><init>()V

    .line 3771
    iput-object v0, p0, Ldfs;->b:Ldfi;

    .line 3774
    iput-object v0, p0, Ldfs;->c:Ljava/lang/Integer;

    .line 3755
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 3796
    const/4 v0, 0x0

    .line 3797
    iget-object v1, p0, Ldfs;->b:Ldfi;

    if-eqz v1, :cond_0

    .line 3798
    const/4 v0, 0x1

    iget-object v1, p0, Ldfs;->b:Ldfi;

    .line 3799
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3801
    :cond_0
    iget-object v1, p0, Ldfs;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 3802
    const/4 v1, 0x2

    iget-object v2, p0, Ldfs;->c:Ljava/lang/Integer;

    .line 3803
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3805
    :cond_1
    iget-object v1, p0, Ldfs;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 3806
    const/4 v1, 0x3

    iget-object v2, p0, Ldfs;->d:Ljava/lang/Boolean;

    .line 3807
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3809
    :cond_2
    iget-object v1, p0, Ldfs;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3810
    iput v0, p0, Ldfs;->cachedSize:I

    .line 3811
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 3751
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldfs;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldfs;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldfs;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldfs;->b:Ldfi;

    if-nez v0, :cond_2

    new-instance v0, Ldfi;

    invoke-direct {v0}, Ldfi;-><init>()V

    iput-object v0, p0, Ldfs;->b:Ldfi;

    :cond_2
    iget-object v0, p0, Ldfs;->b:Ldfi;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-eq v0, v1, :cond_3

    const/4 v1, 0x5

    if-eq v0, v1, :cond_3

    const/4 v1, 0x6

    if-eq v0, v1, :cond_3

    const/4 v1, 0x7

    if-eq v0, v1, :cond_3

    const/16 v1, 0x8

    if-eq v0, v1, :cond_3

    const/16 v1, 0x9

    if-eq v0, v1, :cond_3

    const/16 v1, 0xa

    if-ne v0, v1, :cond_4

    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldfs;->c:Ljava/lang/Integer;

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldfs;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldfs;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 3781
    iget-object v0, p0, Ldfs;->b:Ldfi;

    if-eqz v0, :cond_0

    .line 3782
    const/4 v0, 0x1

    iget-object v1, p0, Ldfs;->b:Ldfi;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 3784
    :cond_0
    iget-object v0, p0, Ldfs;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 3785
    const/4 v0, 0x2

    iget-object v1, p0, Ldfs;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 3787
    :cond_1
    iget-object v0, p0, Ldfs;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 3788
    const/4 v0, 0x3

    iget-object v1, p0, Ldfs;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 3790
    :cond_2
    iget-object v0, p0, Ldfs;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 3792
    return-void
.end method
