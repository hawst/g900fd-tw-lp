.class public final Ldzb;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldzb;


# instance fields
.field public b:Ldvn;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2006
    const/4 v0, 0x0

    new-array v0, v0, [Ldzb;

    sput-object v0, Ldzb;->a:[Ldzb;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2007
    invoke-direct {p0}, Lepn;-><init>()V

    .line 2015
    iput-object v0, p0, Ldzb;->b:Ldvn;

    .line 2020
    iput-object v0, p0, Ldzb;->d:Ljava/lang/Integer;

    .line 2007
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 2040
    const/4 v0, 0x0

    .line 2041
    iget-object v1, p0, Ldzb;->b:Ldvn;

    if-eqz v1, :cond_0

    .line 2042
    const/4 v0, 0x1

    iget-object v1, p0, Ldzb;->b:Ldvn;

    .line 2043
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2045
    :cond_0
    iget-object v1, p0, Ldzb;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 2046
    const/4 v1, 0x2

    iget-object v2, p0, Ldzb;->c:Ljava/lang/String;

    .line 2047
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2049
    :cond_1
    iget-object v1, p0, Ldzb;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 2050
    const/4 v1, 0x3

    iget-object v2, p0, Ldzb;->d:Ljava/lang/Integer;

    .line 2051
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2053
    :cond_2
    iget-object v1, p0, Ldzb;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2054
    iput v0, p0, Ldzb;->cachedSize:I

    .line 2055
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 2003
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldzb;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldzb;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldzb;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldzb;->b:Ldvn;

    if-nez v0, :cond_2

    new-instance v0, Ldvn;

    invoke-direct {v0}, Ldvn;-><init>()V

    iput-object v0, p0, Ldzb;->b:Ldvn;

    :cond_2
    iget-object v0, p0, Ldzb;->b:Ldvn;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldzb;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldzb;->d:Ljava/lang/Integer;

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldzb;->d:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 2025
    iget-object v0, p0, Ldzb;->b:Ldvn;

    if-eqz v0, :cond_0

    .line 2026
    const/4 v0, 0x1

    iget-object v1, p0, Ldzb;->b:Ldvn;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 2028
    :cond_0
    iget-object v0, p0, Ldzb;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2029
    const/4 v0, 0x2

    iget-object v1, p0, Ldzb;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 2031
    :cond_1
    iget-object v0, p0, Ldzb;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 2032
    const/4 v0, 0x3

    iget-object v1, p0, Ldzb;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 2034
    :cond_2
    iget-object v0, p0, Ldzb;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 2036
    return-void
.end method
