.class public abstract Lbfz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final a:Z

.field private static final g:Lbht;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field protected b:Lbea;

.field public final c:Lbht;

.field public final d:J

.field public final e:J

.field public f:Z

.field private h:J

.field private i:J

.field private j:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 209
    sget-object v0, Lbys;->h:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lbfz;->a:Z

    .line 212
    new-instance v0, Lbht;

    invoke-direct {v0}, Lbht;-><init>()V

    sput-object v0, Lbfz;->g:Lbht;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 247
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 248
    sget-object v0, Lbfz;->g:Lbht;

    iput-object v0, p0, Lbfz;->c:Lbht;

    .line 249
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lbfz;->d:J

    .line 250
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lbfz;->e:J

    .line 251
    return-void
.end method

.method public constructor <init>(Ldcw;)V
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    const-wide/16 v0, -0x1

    const/4 v5, 0x0

    .line 256
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 257
    if-eqz p1, :cond_2

    .line 258
    new-instance v4, Lbht;

    invoke-direct {v4, p1}, Lbht;-><init>(Ldcw;)V

    iput-object v4, p0, Lbfz;->c:Lbht;

    .line 259
    iget-object v4, p1, Ldcw;->c:Letn;

    if-eqz v4, :cond_0

    iget-object v4, p1, Ldcw;->c:Letn;

    iget-object v4, v4, Letn;->b:[Leto;

    if-eqz v4, :cond_0

    iget-object v4, p1, Ldcw;->c:Letn;

    iget-object v4, v4, Letn;->b:[Leto;

    array-length v4, v4

    if-lez v4, :cond_0

    iget-object v4, p1, Ldcw;->c:Letn;

    iget-object v4, v4, Letn;->b:[Leto;

    aget-object v4, v4, v5

    iget-object v4, v4, Leto;->b:Ljava/lang/Long;

    if-eqz v4, :cond_0

    iget-object v0, p1, Ldcw;->c:Letn;

    iget-object v0, v0, Letn;->b:[Leto;

    aget-object v0, v0, v5

    iget-object v0, v0, Leto;->b:Ljava/lang/Long;

    .line 263
    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v0

    :cond_0
    iput-wide v0, p0, Lbfz;->d:J

    .line 265
    iget-object v0, p1, Ldcw;->e:Leyp;

    if-eqz v0, :cond_1

    iget-object v0, p1, Ldcw;->e:Leyp;

    iget-object v0, v0, Leyp;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    iget-object v0, p1, Ldcw;->e:Leyp;

    iget-object v0, v0, Leyp;->b:Ljava/lang/Integer;

    .line 267
    invoke-static {v0, v5}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    int-to-long v0, v0

    :goto_0
    iput-wide v0, p0, Lbfz;->e:J

    .line 273
    :goto_1
    return-void

    :cond_1
    move-wide v0, v2

    .line 267
    goto :goto_0

    .line 269
    :cond_2
    sget-object v4, Lbfz;->g:Lbht;

    iput-object v4, p0, Lbfz;->c:Lbht;

    .line 270
    iput-wide v0, p0, Lbfz;->d:J

    .line 271
    iput-wide v2, p0, Lbfz;->e:J

    goto :goto_1
.end method

.method public constructor <init>(Ldvn;J)V
    .locals 2

    .prologue
    .line 278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 279
    new-instance v0, Lbht;

    invoke-direct {v0, p1}, Lbht;-><init>(Ldvn;)V

    iput-object v0, p0, Lbfz;->c:Lbht;

    .line 280
    iput-wide p2, p0, Lbfz;->d:J

    .line 281
    iget-object v0, p1, Ldvn;->g:Ljava/lang/Long;

    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v0

    iput-wide v0, p0, Lbfz;->e:J

    .line 282
    return-void
.end method

.method protected static a(Lyj;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 308
    const-string v0, "babel_background_polling"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 310
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 311
    const-string v0, "from_background_polling:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 313
    const/16 v0, 0x715

    invoke-static {p0, v0}, Lf;->a(Lyj;I)V

    .line 320
    :cond_0
    :goto_0
    return-void

    .line 316
    :cond_1
    const/16 v0, 0x714

    invoke-static {p0, v0}, Lf;->a(Lyj;I)V

    goto :goto_0
.end method

.method protected static a(Ldcw;)Z
    .locals 1

    .prologue
    .line 300
    if-eqz p0, :cond_0

    iget-object v0, p0, Ldcw;->b:Letl;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected static a(Ldvn;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 293
    iget-object v2, p0, Ldvn;->b:Ljava/lang/Integer;

    invoke-static {v2, v1}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v2

    .line 294
    if-eq v2, v0, :cond_0

    if-eqz v2, :cond_0

    const/4 v3, 0x6

    if-eq v2, v3, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method static synthetic a([Ldro;[Ldql;)[Lbdh;
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 207
    if-nez p0, :cond_0

    move-object v0, v5

    :goto_0
    return-object v0

    :cond_0
    move v0, v1

    move v2, v1

    :goto_1
    array-length v3, p0

    if-ge v0, v3, :cond_a

    aget-object v3, p0, v0

    iget-object v3, v3, Ldro;->b:Ljava/lang/Integer;

    if-eqz v3, :cond_6

    aget-object v3, p0, v0

    iget-object v3, v3, Ldro;->b:Ljava/lang/Integer;

    invoke-static {v3, v1}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v3

    if-ne v3, v10, :cond_6

    if-eqz p1, :cond_7

    move v3, v1

    :goto_2
    array-length v4, p1

    if-ge v3, v4, :cond_7

    aget-object v4, p1, v3

    if-eqz v4, :cond_3

    aget-object v4, p1, v3

    iget-object v4, v4, Ldql;->d:Ldqa;

    :goto_3
    if-eqz v4, :cond_4

    iget-object v4, v4, Ldqa;->m:[Ldqh;

    move-object v7, v4

    :goto_4
    if-eqz v7, :cond_5

    move v4, v1

    :goto_5
    array-length v8, v7

    if-ge v4, v8, :cond_5

    aget-object v8, v7, v4

    iget-object v8, v8, Ldqh;->b:Ldui;

    iget-object v8, v8, Ldui;->c:Ljava/lang/String;

    aget-object v9, p0, v0

    iget-object v9, v9, Ldro;->c:Ldui;

    iget-object v9, v9, Ldui;->c:Ljava/lang/String;

    invoke-static {v8, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    aget-object v8, v7, v4

    iget-object v8, v8, Ldqh;->e:Leir;

    if-eqz v8, :cond_2

    aget-object v8, p0, v0

    iget-object v8, v8, Ldro;->d:Ldrq;

    if-nez v8, :cond_1

    aget-object v8, p0, v0

    new-instance v9, Ldrq;

    invoke-direct {v9}, Ldrq;-><init>()V

    iput-object v9, v8, Ldro;->d:Ldrq;

    :cond_1
    aget-object v8, p0, v0

    iget-object v8, v8, Ldro;->d:Ldrq;

    new-array v9, v6, [Ljava/lang/String;

    iput-object v9, v8, Ldrq;->g:[Ljava/lang/String;

    aget-object v8, p0, v0

    iget-object v8, v8, Ldro;->d:Ldrq;

    iget-object v8, v8, Ldrq;->g:[Ljava/lang/String;

    aget-object v9, v7, v4

    iget-object v9, v9, Ldqh;->e:Leir;

    iget-object v9, v9, Leir;->b:Ljava/lang/String;

    aput-object v9, v8, v1

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    :cond_3
    move-object v4, v5

    goto :goto_3

    :cond_4
    move-object v7, v5

    goto :goto_4

    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_6
    move v3, v1

    :goto_6
    array-length v4, p0

    if-ge v3, v4, :cond_7

    if-eq v3, v0, :cond_9

    aget-object v4, p0, v3

    if-eqz v4, :cond_9

    aget-object v4, p0, v3

    iget-object v4, v4, Ldro;->b:Ljava/lang/Integer;

    if-eqz v4, :cond_8

    aget-object v4, p0, v3

    iget-object v4, v4, Ldro;->b:Ljava/lang/Integer;

    invoke-static {v4, v1}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v4

    if-ne v4, v10, :cond_8

    move v4, v6

    :goto_7
    if-eqz v4, :cond_9

    aget-object v4, p0, v0

    iget-object v4, v4, Ldro;->c:Ldui;

    iget-object v4, v4, Ldui;->c:Ljava/lang/String;

    aget-object v7, p0, v3

    iget-object v7, v7, Ldro;->c:Ldui;

    iget-object v7, v7, Ldui;->c:Ljava/lang/String;

    invoke-static {v4, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_9

    aput-object v5, p0, v0

    add-int/lit8 v2, v2, 0x1

    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    :cond_8
    move v4, v1

    goto :goto_7

    :cond_9
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_a
    if-lez v2, :cond_d

    array-length v0, p0

    sub-int/2addr v0, v2

    new-array v2, v0, [Ldro;

    move v0, v1

    :goto_8
    array-length v3, p0

    if-ge v0, v3, :cond_c

    aget-object v3, p0, v0

    if-eqz v3, :cond_b

    aget-object v3, p0, v0

    aput-object v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    :cond_b
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_c
    move-object p0, v2

    :cond_d
    invoke-static {p0, v5}, Lbdh;->a([Ldro;Lbcn;)[Lbdh;

    move-result-object v0

    goto/16 :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lbfz;->b:Lbea;

    iget v0, v0, Lbea;->f:I

    return v0
.end method

.method public a(J)V
    .locals 0

    .prologue
    .line 5062
    iput-wide p1, p0, Lbfz;->h:J

    .line 5063
    return-void
.end method

.method public a(Lbea;)V
    .locals 0

    .prologue
    .line 304
    iput-object p1, p0, Lbfz;->b:Lbea;

    .line 305
    return-void
.end method

.method public a(Lyt;Lbnl;)V
    .locals 1

    .prologue
    .line 236
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbfz;->f:Z

    .line 237
    return-void
.end method

.method public b()Lbea;
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lbfz;->b:Lbea;

    return-object v0
.end method

.method public b(J)V
    .locals 0

    .prologue
    .line 5070
    iput-wide p1, p0, Lbfz;->i:J

    .line 5071
    return-void
.end method

.method public c()J
    .locals 2

    .prologue
    .line 5058
    iget-wide v0, p0, Lbfz;->h:J

    return-wide v0
.end method

.method public c(J)V
    .locals 0

    .prologue
    .line 5078
    iput-wide p1, p0, Lbfz;->j:J

    .line 5079
    return-void
.end method

.method public d()J
    .locals 2

    .prologue
    .line 5066
    iget-wide v0, p0, Lbfz;->i:J

    return-wide v0
.end method

.method public e()J
    .locals 2

    .prologue
    .line 5074
    iget-wide v0, p0, Lbfz;->j:J

    return-wide v0
.end method
