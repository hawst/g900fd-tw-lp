.class abstract Lbgj;
.super Lbfz;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final g:Ljava/lang/String;

.field private final h:J


# direct methods
.method public constructor <init>(Ldvn;JJLjava/lang/String;)V
    .locals 0

    .prologue
    .line 397
    invoke-direct {p0, p1, p2, p3}, Lbfz;-><init>(Ldvn;J)V

    .line 398
    iput-object p6, p0, Lbgj;->g:Ljava/lang/String;

    .line 399
    iput-wide p4, p0, Lbgj;->h:J

    .line 400
    return-void
.end method


# virtual methods
.method public a(Lyt;Lbnl;)V
    .locals 10

    .prologue
    .line 405
    invoke-super {p0, p1, p2}, Lbfz;->a(Lyt;Lbnl;)V

    .line 406
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 407
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "processEventResponse response status: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbgj;->c:Lbht;

    iget v2, v2, Lbht;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " error description"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbgj;->c:Lbht;

    iget-object v2, v2, Lbht;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    :cond_0
    iget-object v0, p0, Lbgj;->b:Lbea;

    check-cast v0, Lbei;

    iget-object v1, v0, Lbei;->i:Ljava/lang/String;

    .line 413
    iget-object v0, p0, Lbgj;->b:Lbea;

    check-cast v0, Lbei;

    iget-object v0, v0, Lbei;->c:Ljava/lang/String;

    .line 414
    if-eqz v1, :cond_2

    iget-object v2, p0, Lbgj;->g:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 415
    invoke-virtual {p1}, Lyt;->a()V

    .line 417
    :try_start_0
    iget-object v2, p0, Lbgj;->g:Ljava/lang/String;

    iget-wide v3, p0, Lbgj;->d:J

    iget-wide v5, p0, Lbgj;->h:J

    sget-boolean v7, Lyt;->a:Z

    if-eqz v7, :cond_1

    const-string v7, "Babel_db"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "updateMessageId, conversationId "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", messageClientGeneratedId="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", eventId="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", ts="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    const-string v8, "message_id"

    invoke-virtual {v7, v8, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "timestamp"

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v7, v8, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-wide/16 v3, 0x0

    cmp-long v3, v5, v3

    if-lez v3, :cond_3

    const-string v3, "expiration_timestamp"

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v7, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :goto_0
    const-string v3, "status"

    const/4 v4, 0x4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v7, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    if-nez v0, :cond_4

    const-string v1, "Babel_db"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "attempt to update a message id ["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] for nonexistant conversation id ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    :goto_1
    invoke-virtual {p1}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 421
    invoke-virtual {p1}, Lyt;->c()V

    .line 426
    :cond_2
    iget-wide v1, p0, Lbgj;->d:J

    invoke-virtual {p1, v0, v1, v2}, Lyt;->i(Ljava/lang/String;J)V

    .line 427
    invoke-virtual {p1}, Lyt;->f()Lyj;

    move-result-object v0

    const-wide/16 v1, 0x0

    const/4 v3, 0x4

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;JIZ)V

    .line 429
    return-void

    .line 417
    :cond_3
    :try_start_1
    const-string v3, "expiration_timestamp"

    invoke-virtual {v7, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 421
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Lyt;->c()V

    throw v0

    .line 417
    :cond_4
    :try_start_2
    iget-object v2, p1, Lyt;->b:Lzr;

    const-string v3, "messages"

    const-string v4, "message_id=? AND conversation_id=?"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    const/4 v1, 0x1

    aput-object v0, v5, v1

    invoke-virtual {v2, v3, v7, v4, v5}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method
