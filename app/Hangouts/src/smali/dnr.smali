.class public final Ldnr;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldnr;


# instance fields
.field public b:Ldna;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2518
    const/4 v0, 0x0

    new-array v0, v0, [Ldnr;

    sput-object v0, Ldnr;->a:[Ldnr;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2519
    invoke-direct {p0}, Lepn;-><init>()V

    .line 2522
    iput-object v0, p0, Ldnr;->b:Ldna;

    .line 2525
    iput-object v0, p0, Ldnr;->c:Ljava/lang/Integer;

    .line 2519
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 2552
    const/4 v0, 0x0

    .line 2553
    iget-object v1, p0, Ldnr;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 2554
    const/4 v0, 0x1

    iget-object v1, p0, Ldnr;->c:Ljava/lang/Integer;

    .line 2555
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lepl;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2557
    :cond_0
    iget-object v1, p0, Ldnr;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 2558
    const/4 v1, 0x2

    iget-object v2, p0, Ldnr;->d:Ljava/lang/String;

    .line 2559
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2561
    :cond_1
    iget-object v1, p0, Ldnr;->b:Ldna;

    if-eqz v1, :cond_2

    .line 2562
    const/4 v1, 0x3

    iget-object v2, p0, Ldnr;->b:Ldna;

    .line 2563
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2565
    :cond_2
    iget-object v1, p0, Ldnr;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 2566
    const/4 v1, 0x4

    iget-object v2, p0, Ldnr;->e:Ljava/lang/Boolean;

    .line 2567
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2569
    :cond_3
    iget-object v1, p0, Ldnr;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2570
    iput v0, p0, Ldnr;->cachedSize:I

    .line 2571
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2515
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldnr;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldnr;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldnr;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldnr;->c:Ljava/lang/Integer;

    goto :goto_0

    :cond_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldnr;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldnr;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ldnr;->b:Ldna;

    if-nez v0, :cond_4

    new-instance v0, Ldna;

    invoke-direct {v0}, Ldna;-><init>()V

    iput-object v0, p0, Ldnr;->b:Ldna;

    :cond_4
    iget-object v0, p0, Ldnr;->b:Ldna;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldnr;->e:Ljava/lang/Boolean;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 2534
    iget-object v0, p0, Ldnr;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 2535
    const/4 v0, 0x1

    iget-object v1, p0, Ldnr;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 2537
    :cond_0
    iget-object v0, p0, Ldnr;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2538
    const/4 v0, 0x2

    iget-object v1, p0, Ldnr;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 2540
    :cond_1
    iget-object v0, p0, Ldnr;->b:Ldna;

    if-eqz v0, :cond_2

    .line 2541
    const/4 v0, 0x3

    iget-object v1, p0, Ldnr;->b:Ldna;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 2543
    :cond_2
    iget-object v0, p0, Ldnr;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 2544
    const/4 v0, 0x4

    iget-object v1, p0, Ldnr;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 2546
    :cond_3
    iget-object v0, p0, Ldnr;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 2548
    return-void
.end method
