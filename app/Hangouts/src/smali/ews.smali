.class public final Lews;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Lews;


# instance fields
.field public b:[Levu;

.field public c:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    new-array v0, v0, [Lews;

    sput-object v0, Lews;->a:[Lews;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0}, Lepn;-><init>()V

    .line 96
    sget-object v0, Levu;->a:[Levu;

    iput-object v0, p0, Lews;->b:[Levu;

    .line 99
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Lews;->c:[Ljava/lang/String;

    .line 93
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 123
    iget-object v0, p0, Lews;->b:[Levu;

    if-eqz v0, :cond_1

    .line 124
    iget-object v3, p0, Lews;->b:[Levu;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 125
    if-eqz v5, :cond_0

    .line 126
    const/4 v6, 0x1

    .line 127
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 124
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 131
    :cond_2
    iget-object v2, p0, Lews;->c:[Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lews;->c:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 133
    iget-object v3, p0, Lews;->c:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_3

    aget-object v5, v3, v1

    .line 135
    invoke-static {v5}, Lepl;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 133
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 137
    :cond_3
    add-int/2addr v0, v2

    .line 138
    iget-object v1, p0, Lews;->c:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 140
    :cond_4
    iget-object v1, p0, Lews;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 141
    iput v0, p0, Lews;->cachedSize:I

    .line 142
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 89
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lews;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lews;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lews;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lews;->b:[Levu;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Levu;

    iget-object v3, p0, Lews;->b:[Levu;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lews;->b:[Levu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lews;->b:[Levu;

    :goto_2
    iget-object v2, p0, Lews;->b:[Levu;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lews;->b:[Levu;

    new-instance v3, Levu;

    invoke-direct {v3}, Levu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lews;->b:[Levu;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lews;->b:[Levu;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lews;->b:[Levu;

    new-instance v3, Levu;

    invoke-direct {v3}, Levu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lews;->b:[Levu;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    iget-object v0, p0, Lews;->c:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Lews;->c:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Lews;->c:[Ljava/lang/String;

    :goto_3
    iget-object v2, p0, Lews;->c:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Lews;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lepk;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_5
    iget-object v2, p0, Lews;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 104
    iget-object v1, p0, Lews;->b:[Levu;

    if-eqz v1, :cond_1

    .line 105
    iget-object v2, p0, Lews;->b:[Levu;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 106
    if-eqz v4, :cond_0

    .line 107
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 105
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 111
    :cond_1
    iget-object v1, p0, Lews;->c:[Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 112
    iget-object v1, p0, Lews;->c:[Ljava/lang/String;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 113
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lepl;->a(ILjava/lang/String;)V

    .line 112
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 116
    :cond_2
    iget-object v0, p0, Lews;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 118
    return-void
.end method
