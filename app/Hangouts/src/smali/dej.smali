.class public final Ldej;
.super Lepn;
.source "PG"


# static fields
.field public static final a:[Ldej;


# instance fields
.field public b:Ldgp;

.field public c:Ldfa;

.field public d:Ljava/lang/Boolean;

.field public e:Ljava/lang/Boolean;

.field public f:Ljava/lang/Boolean;

.field public g:Ldel;

.field public h:Ldgd;

.field public i:Ldgm;

.field public j:Ljava/lang/String;

.field public k:Ldgb;

.field public l:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7521
    const/4 v0, 0x0

    new-array v0, v0, [Ldej;

    sput-object v0, Ldej;->a:[Ldej;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7522
    invoke-direct {p0}, Lepn;-><init>()V

    .line 7525
    iput-object v0, p0, Ldej;->b:Ldgp;

    .line 7528
    iput-object v0, p0, Ldej;->c:Ldfa;

    .line 7537
    iput-object v0, p0, Ldej;->g:Ldel;

    .line 7540
    iput-object v0, p0, Ldej;->h:Ldgd;

    .line 7543
    iput-object v0, p0, Ldej;->i:Ldgm;

    .line 7548
    iput-object v0, p0, Ldej;->k:Ldgb;

    .line 7522
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 7594
    const/4 v0, 0x0

    .line 7595
    iget-object v1, p0, Ldej;->b:Ldgp;

    if-eqz v1, :cond_0

    .line 7596
    const/4 v0, 0x1

    iget-object v1, p0, Ldej;->b:Ldgp;

    .line 7597
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 7599
    :cond_0
    iget-object v1, p0, Ldej;->c:Ldfa;

    if-eqz v1, :cond_1

    .line 7600
    const/4 v1, 0x2

    iget-object v2, p0, Ldej;->c:Ldfa;

    .line 7601
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7603
    :cond_1
    iget-object v1, p0, Ldej;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 7604
    const/4 v1, 0x3

    iget-object v2, p0, Ldej;->d:Ljava/lang/Boolean;

    .line 7605
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7607
    :cond_2
    iget-object v1, p0, Ldej;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 7608
    const/4 v1, 0x4

    iget-object v2, p0, Ldej;->e:Ljava/lang/Boolean;

    .line 7609
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7611
    :cond_3
    iget-object v1, p0, Ldej;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 7612
    const/4 v1, 0x5

    iget-object v2, p0, Ldej;->f:Ljava/lang/Boolean;

    .line 7613
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7615
    :cond_4
    iget-object v1, p0, Ldej;->g:Ldel;

    if-eqz v1, :cond_5

    .line 7616
    const/4 v1, 0x6

    iget-object v2, p0, Ldej;->g:Ldel;

    .line 7617
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7619
    :cond_5
    iget-object v1, p0, Ldej;->h:Ldgd;

    if-eqz v1, :cond_6

    .line 7620
    const/4 v1, 0x7

    iget-object v2, p0, Ldej;->h:Ldgd;

    .line 7621
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7623
    :cond_6
    iget-object v1, p0, Ldej;->i:Ldgm;

    if-eqz v1, :cond_7

    .line 7624
    const/16 v1, 0x8

    iget-object v2, p0, Ldej;->i:Ldgm;

    .line 7625
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7627
    :cond_7
    iget-object v1, p0, Ldej;->j:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 7628
    const/16 v1, 0x9

    iget-object v2, p0, Ldej;->j:Ljava/lang/String;

    .line 7629
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7631
    :cond_8
    iget-object v1, p0, Ldej;->k:Ldgb;

    if-eqz v1, :cond_9

    .line 7632
    const/16 v1, 0xa

    iget-object v2, p0, Ldej;->k:Ldgb;

    .line 7633
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7635
    :cond_9
    iget-object v1, p0, Ldej;->l:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    .line 7636
    const/16 v1, 0xb

    iget-object v2, p0, Ldej;->l:Ljava/lang/Boolean;

    .line 7637
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7639
    :cond_a
    iget-object v1, p0, Ldej;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7640
    iput v0, p0, Ldej;->cachedSize:I

    .line 7641
    return v0
.end method

.method public synthetic mergeFrom(Lepk;)Lepr;
    .locals 2

    .prologue
    .line 7518
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Ldej;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldej;->unknownFieldData:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Ldej;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldej;->b:Ldgp;

    if-nez v0, :cond_2

    new-instance v0, Ldgp;

    invoke-direct {v0}, Ldgp;-><init>()V

    iput-object v0, p0, Ldej;->b:Ldgp;

    :cond_2
    iget-object v0, p0, Ldej;->b:Ldgp;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldej;->c:Ldfa;

    if-nez v0, :cond_3

    new-instance v0, Ldfa;

    invoke-direct {v0}, Ldfa;-><init>()V

    iput-object v0, p0, Ldej;->c:Ldfa;

    :cond_3
    iget-object v0, p0, Ldej;->c:Ldfa;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldej;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldej;->e:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldej;->f:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Ldej;->g:Ldel;

    if-nez v0, :cond_4

    new-instance v0, Ldel;

    invoke-direct {v0}, Ldel;-><init>()V

    iput-object v0, p0, Ldej;->g:Ldel;

    :cond_4
    iget-object v0, p0, Ldej;->g:Ldel;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Ldej;->h:Ldgd;

    if-nez v0, :cond_5

    new-instance v0, Ldgd;

    invoke-direct {v0}, Ldgd;-><init>()V

    iput-object v0, p0, Ldej;->h:Ldgd;

    :cond_5
    iget-object v0, p0, Ldej;->h:Ldgd;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Ldej;->i:Ldgm;

    if-nez v0, :cond_6

    new-instance v0, Ldgm;

    invoke-direct {v0}, Ldgm;-><init>()V

    iput-object v0, p0, Ldej;->i:Ldgm;

    :cond_6
    iget-object v0, p0, Ldej;->i:Ldgm;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldej;->j:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Ldej;->k:Ldgb;

    if-nez v0, :cond_7

    new-instance v0, Ldgb;

    invoke-direct {v0}, Ldgb;-><init>()V

    iput-object v0, p0, Ldej;->k:Ldgb;

    :cond_7
    iget-object v0, p0, Ldej;->k:Ldgb;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldej;->l:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
    .end sparse-switch
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 7555
    iget-object v0, p0, Ldej;->b:Ldgp;

    if-eqz v0, :cond_0

    .line 7556
    const/4 v0, 0x1

    iget-object v1, p0, Ldej;->b:Ldgp;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 7558
    :cond_0
    iget-object v0, p0, Ldej;->c:Ldfa;

    if-eqz v0, :cond_1

    .line 7559
    const/4 v0, 0x2

    iget-object v1, p0, Ldej;->c:Ldfa;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 7561
    :cond_1
    iget-object v0, p0, Ldej;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 7562
    const/4 v0, 0x3

    iget-object v1, p0, Ldej;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 7564
    :cond_2
    iget-object v0, p0, Ldej;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 7565
    const/4 v0, 0x4

    iget-object v1, p0, Ldej;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 7567
    :cond_3
    iget-object v0, p0, Ldej;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 7568
    const/4 v0, 0x5

    iget-object v1, p0, Ldej;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 7570
    :cond_4
    iget-object v0, p0, Ldej;->g:Ldel;

    if-eqz v0, :cond_5

    .line 7571
    const/4 v0, 0x6

    iget-object v1, p0, Ldej;->g:Ldel;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 7573
    :cond_5
    iget-object v0, p0, Ldej;->h:Ldgd;

    if-eqz v0, :cond_6

    .line 7574
    const/4 v0, 0x7

    iget-object v1, p0, Ldej;->h:Ldgd;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 7576
    :cond_6
    iget-object v0, p0, Ldej;->i:Ldgm;

    if-eqz v0, :cond_7

    .line 7577
    const/16 v0, 0x8

    iget-object v1, p0, Ldej;->i:Ldgm;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 7579
    :cond_7
    iget-object v0, p0, Ldej;->j:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 7580
    const/16 v0, 0x9

    iget-object v1, p0, Ldej;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 7582
    :cond_8
    iget-object v0, p0, Ldej;->k:Ldgb;

    if-eqz v0, :cond_9

    .line 7583
    const/16 v0, 0xa

    iget-object v1, p0, Ldej;->k:Ldgb;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 7585
    :cond_9
    iget-object v0, p0, Ldej;->l:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    .line 7586
    const/16 v0, 0xb

    iget-object v1, p0, Ldej;->l:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 7588
    :cond_a
    iget-object v0, p0, Ldej;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 7590
    return-void
.end method
