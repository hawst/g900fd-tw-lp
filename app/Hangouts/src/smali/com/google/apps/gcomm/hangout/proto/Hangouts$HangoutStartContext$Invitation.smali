.class public final Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;
.super Lepn;
.source "PG"


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;


# instance fields
.field public invitationType:Ljava/lang/Integer;

.field public inviterGaiaId:Ljava/lang/String;

.field public inviterPhoneNumber:Ljava/lang/String;

.field public inviterProfileName:Ljava/lang/String;

.field public isGroupInvitation:Ljava/lang/Boolean;

.field public isInviterPstnParticipant:Ljava/lang/Boolean;

.field public isInviterTrusted:Ljava/lang/Boolean;

.field public phoneNumber:Ljava/lang/String;

.field public shouldAutoAccept:Ljava/lang/Boolean;

.field public timestamp:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 217
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;

    sput-object v0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->EMPTY_ARRAY:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 218
    invoke-direct {p0}, Lepn;-><init>()V

    .line 225
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->invitationType:Ljava/lang/Integer;

    .line 218
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 276
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->timestamp:Ljava/lang/Long;

    .line 278
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lepl;->e(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 279
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->inviterGaiaId:Ljava/lang/String;

    .line 280
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 281
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->invitationType:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 282
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->invitationType:Ljava/lang/Integer;

    .line 283
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 285
    :cond_0
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->inviterProfileName:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 286
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->inviterProfileName:Ljava/lang/String;

    .line 287
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 289
    :cond_1
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->shouldAutoAccept:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 290
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->shouldAutoAccept:Ljava/lang/Boolean;

    .line 291
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 293
    :cond_2
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->phoneNumber:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 294
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->phoneNumber:Ljava/lang/String;

    .line 295
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 297
    :cond_3
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->inviterPhoneNumber:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 298
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->inviterPhoneNumber:Ljava/lang/String;

    .line 299
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 301
    :cond_4
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->isInviterPstnParticipant:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 302
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->isInviterPstnParticipant:Ljava/lang/Boolean;

    .line 303
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 305
    :cond_5
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->isGroupInvitation:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 306
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->isGroupInvitation:Ljava/lang/Boolean;

    .line 307
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 309
    :cond_6
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->isInviterTrusted:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 310
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->isInviterTrusted:Ljava/lang/Boolean;

    .line 311
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lepl;->g(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 313
    :cond_7
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 314
    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->cachedSize:I

    .line 315
    return v0
.end method

.method public mergeFrom(Lepk;)Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;
    .locals 2

    .prologue
    .line 323
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    .line 324
    sparse-switch v0, :sswitch_data_0

    .line 328
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    .line 329
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->unknownFieldData:Ljava/util/List;

    .line 332
    :cond_1
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 334
    :sswitch_0
    return-object p0

    .line 339
    :sswitch_1
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->timestamp:Ljava/lang/Long;

    goto :goto_0

    .line 343
    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->inviterGaiaId:Ljava/lang/String;

    goto :goto_0

    .line 347
    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    .line 348
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 351
    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->invitationType:Ljava/lang/Integer;

    goto :goto_0

    .line 353
    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->invitationType:Ljava/lang/Integer;

    goto :goto_0

    .line 358
    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->inviterProfileName:Ljava/lang/String;

    goto :goto_0

    .line 362
    :sswitch_5
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->shouldAutoAccept:Ljava/lang/Boolean;

    goto :goto_0

    .line 366
    :sswitch_6
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->phoneNumber:Ljava/lang/String;

    goto :goto_0

    .line 370
    :sswitch_7
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->inviterPhoneNumber:Ljava/lang/String;

    goto :goto_0

    .line 374
    :sswitch_8
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->isInviterPstnParticipant:Ljava/lang/Boolean;

    goto :goto_0

    .line 378
    :sswitch_9
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->isGroupInvitation:Ljava/lang/Boolean;

    goto :goto_0

    .line 382
    :sswitch_a
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->isInviterTrusted:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 324
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lepk;)Lepr;
    .locals 1

    .prologue
    .line 214
    invoke-virtual {p0, p1}, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->mergeFrom(Lepk;)Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 244
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->timestamp:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 245
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->inviterGaiaId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 246
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->invitationType:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 247
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->invitationType:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 249
    :cond_0
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->inviterProfileName:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 250
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->inviterProfileName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 252
    :cond_1
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->shouldAutoAccept:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 253
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->shouldAutoAccept:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 255
    :cond_2
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->phoneNumber:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 256
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->phoneNumber:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 258
    :cond_3
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->inviterPhoneNumber:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 259
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->inviterPhoneNumber:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 261
    :cond_4
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->isInviterPstnParticipant:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 262
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->isInviterPstnParticipant:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 264
    :cond_5
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->isGroupInvitation:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 265
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->isGroupInvitation:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 267
    :cond_6
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->isInviterTrusted:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 268
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->isInviterTrusted:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(IZ)V

    .line 270
    :cond_7
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 272
    return-void
.end method
