.class public interface abstract Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Source;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final ACCOUNT_SIGNIN_REDIRECT:I = 0x40

.field public static final AMBIENT_START_LINK:I = 0x48

.field public static final ANDROID_MOBILE_URL:I = 0x61

.field public static final ANDROID_NFC_TAP:I = 0x60

.field public static final ANDROID_PEOPLE_ACTIVITY:I = 0x62

.field public static final ANDROID_TELEPHONY_CELLULAR_INCOMING:I = 0x68

.field public static final ANDROID_TELEPHONY_CELLULAR_OUTGOING:I = 0x67

.field public static final ANDROID_TELEPHONY_INCOMING:I = 0x56

.field public static final ANDROID_TELEPHONY_OUTGOING:I = 0x55

.field public static final BIGTOP_TASK_ASSIST:I = 0x4c

.field public static final CALENDAR:I = 0x1c

.field public static final CONVERSATION:I = 0x3f

.field public static final CROWDSURF_POST:I = 0x4d

.field public static final CROWDSURF_WIDGET:I = 0x4e

.field public static final EMAIL_INVITE:I = 0x57

.field public static final EMAIL_INVITE_ENTERPRISE:I = 0x58

.field public static final EXTERNAL:I = 0x33

.field public static final FLIPPY:I = 0x7

.field public static final GMAIL:I = 0x6

.field public static final GMAIL_MOLE:I = 0xa

.field public static final GMAIL_MOLE_LINK:I = 0x11

.field public static final GMAIL_NOTIFICATION:I = 0xb

.field public static final GMAIL_PEOPLE_WIDGET:I = 0x9

.field public static final GMAIL_PERSON_HEADER:I = 0xc

.field public static final GMAIL_PERSON_HEADER_CIRCLE:I = 0x13

.field public static final GMAIL_RING_WIDGET:I = 0x12

.field public static final GMAIL_ROSTER:I = 0xd

.field public static final GO_MEET:I = 0x30

.field public static final GWS_LOCAL_SEARCH:I = 0x54

.field public static final HANGOUT_BUTTON:I = 0x5c

.field public static final HELPCENTER_HOA:I = 0x42

.field public static final HOA_PARTNER_VERBLING:I = 0x52

.field public static final HOA_SHORT_URL:I = 0x51

.field public static final INVITE:I = 0x4

.field public static final LIVE_HANGOUTS_WITHIN_HANGOUTS:I = 0x46

.field public static final MINIBAR_JOIN:I = 0x3

.field public static final MINIBAR_START:I = 0x2

.field public static final MOBILE_COMPOSE:I = 0x3d

.field public static final MOBILE_COMPOSE_AUDIO_ONLY:I = 0x63

.field public static final MOBILE_CONVERSATION_LIST:I = 0x38

.field public static final MOBILE_CONVERSATION_LIST_MISSED_RESTART:I = 0x39

.field public static final MOBILE_CONVERSATION_LIST_ONGOING_AUDIO_ONLY_HANGOUT:I = 0x65

.field public static final MOBILE_CONVERSATION_LIST_ONGOING_HANGOUT:I = 0x3a

.field public static final MOBILE_EXTERNAL_API:I = 0x3b

.field public static final MOBILE_ONGOING_HANGOUT_BAR:I = 0x3c

.field public static final MOBILE_RING:I = 0x3e

.field public static final MOBILE_RING_AUDIO_ONLY:I = 0x64

.field public static final ORKUT_CHAT_DIRECT_LINK:I = 0x44

.field public static final OUTLOOK_PLUGIN_SCHEDULED:I = 0x5f

.field public static final OUTLOOK_PLUGIN_UNSCHEDULED:I = 0x5e

.field public static final OZ_AMBIENT_LANDING:I = 0x4a

.field public static final OZ_CONSUMER:I = 0x1d

.field public static final OZ_CONSUMER_AMBIENT:I = 0x2f

.field public static final OZ_CONTACTS:I = 0x53

.field public static final OZ_DIRECT_LINK:I = 0x43

.field public static final OZ_EVENT:I = 0x1a

.field public static final OZ_EVENT_CALENDAR:I = 0x1b

.field public static final OZ_HANGOUTS_APPS:I = 0x1e

.field public static final OZ_HOA_EVENT:I = 0x4b

.field public static final OZ_HOVER_CARD:I = 0x18

.field public static final OZ_LANDING_PAGE:I = 0xe

.field public static final OZ_LANDING_PAGE_JOIN:I = 0x19

.field public static final OZ_LIVE_HANGOUT:I = 0x1f

.field public static final OZ_NOTIFICATION:I = 0x20

.field public static final OZ_OTHER:I = 0x21

.field public static final OZ_PLUSPAGE_DASHBOARD:I = 0x49

.field public static final OZ_PROFILE_CARD:I = 0xf

.field public static final OZ_RHS:I = 0x16

.field public static final OZ_ROSTER:I = 0x10

.field public static final OZ_RTRIBBON:I = 0x17

.field public static final OZ_SHAREBOX_AMBIENT:I = 0x2d

.field public static final OZ_SQUARE:I = 0x15

.field public static final OZ_SQUARE_POST:I = 0x50

.field public static final OZ_WABEL_BOTTOM:I = 0x2e

.field public static final PLUGIN_INSTALL:I = 0x32

.field public static final QUASAR_DIRECT_LINK:I = 0x45

.field public static final REFRESH:I = 0x31

.field public static final REOPEN_IN_NEW_WINDOW:I = 0x66

.field public static final SANDBAR:I = 0x0

.field public static final SANDBAR_ENTERPRISE:I = 0x59

.field public static final STREAM:I = 0x1

.field public static final TALKGADGET_DIRECT_LINK:I = 0x47

.field public static final TEE:I = 0x34

.field public static final TINKERBELL_INCOMING:I = 0x5b

.field public static final TINKERBELL_OUTGOING:I = 0x5a

.field public static final TOAST:I = 0x2c

.field public static final TRUMAN_BROADCAST:I = 0x5d

.field public static final VIDEO_CALL_ERROR:I = 0x41

.field public static final VOICE_FRONTEND:I = 0x4f

.field public static final WABEL_HOST_GMAIL:I = 0x36

.field public static final WABEL_HOST_OZ:I = 0x35

.field public static final WABEL_HOST_QUASAR:I = 0x37

.field public static final WABEL_MOLE_GMAIL:I = 0x26

.field public static final WABEL_MOLE_OZ:I = 0x23

.field public static final WABEL_MOLE_QUASAR:I = 0x29

.field public static final WABEL_ROSTER_GMAIL:I = 0x27

.field public static final WABEL_ROSTER_OZ:I = 0x25

.field public static final WABEL_ROSTER_QUASAR:I = 0x2a

.field public static final WABEL_YELLOW_BAR_GMAIL:I = 0x28

.field public static final WABEL_YELLOW_BAR_OZ:I = 0x24

.field public static final WABEL_YELLOW_BAR_QUASAR:I = 0x2b

.field public static final YOUTUBE:I = 0x5

.field public static final YOUTUBE_LIVE:I = 0x22

.field public static final YOUTUBE_PARTNER_MAILOUT:I = 0x8

.field public static final YOUTUBE_UPLOAD_PAGE:I = 0x14
