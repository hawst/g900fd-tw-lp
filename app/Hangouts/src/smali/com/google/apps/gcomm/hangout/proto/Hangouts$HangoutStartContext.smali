.class public final Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;
.super Lepn;
.source "PG"


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;


# instance fields
.field public activityId:Ljava/lang/String;

.field public appData:Ljava/lang/String;

.field public appId:Ljava/lang/String;

.field public appIntegrationId:Ljava/lang/String;

.field public callback:Ljava/lang/Integer;

.field public circleId:Ljava/lang/String;

.field public contextId:Ljava/lang/String;

.field public conversationId:Ljava/lang/String;

.field public create:Ljava/lang/Boolean;

.field public currentOccupantAvatar:[Ljava/lang/String;

.field public dEPRECATEDCallback:Ljava/lang/Boolean;

.field public encodedCalendarId:Ljava/lang/String;

.field public externalEventId:Ljava/lang/String;

.field public externalKey:Lcom/google/apps/gcomm/hangout/proto/Hangouts$ExternalEntityKey;

.field public externalUserId:Ljava/lang/String;

.field public flippy:Ljava/lang/Boolean;

.field public gatewayAppId:Ljava/lang/String;

.field public hangoutId:Ljava/lang/String;

.field public hangoutType:Ljava/lang/Integer;

.field public initialApp:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;

.field public invitation:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;

.field public invitedOid:Ljava/lang/String;

.field public invitee:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;

.field public latencyMarks:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;

.field public mediaType:Ljava/lang/Integer;

.field public nick:Ljava/lang/String;

.field public numCircles:Ljava/lang/Long;

.field public participantLogId:Ljava/lang/String;

.field public profileId:[Ljava/lang/String;

.field public referringUrl:Ljava/lang/String;

.field public roomAppId:[Ljava/lang/String;

.field public shouldAutoInvite:Ljava/lang/Boolean;

.field public shouldAutoJoin:Ljava/lang/Boolean;

.field public shouldHideInviteButton:Ljava/lang/Boolean;

.field public shouldMuteVideo:Ljava/lang/Boolean;

.field public source:Ljava/lang/Integer;

.field public squareId:Ljava/lang/String;

.field public startOption:[I

.field public tag:[Ljava/lang/String;

.field public topic:Ljava/lang/String;

.field public widgetPublisherId:Ljava/lang/String;

.field public xsrfToken:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;

    sput-object v0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->EMPTY_ARRAY:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 75
    invoke-direct {p0}, Lepn;-><init>()V

    .line 795
    iput-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->hangoutType:Ljava/lang/Integer;

    .line 804
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->profileId:[Ljava/lang/String;

    .line 817
    iput-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->source:Ljava/lang/Integer;

    .line 820
    iput-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->invitation:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;

    .line 827
    iput-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->latencyMarks:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;

    .line 830
    iput-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->callback:Ljava/lang/Integer;

    .line 833
    iput-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->externalKey:Lcom/google/apps/gcomm/hangout/proto/Hangouts$ExternalEntityKey;

    .line 836
    sget-object v0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;->EMPTY_ARRAY:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->invitee:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;

    .line 857
    sget-object v0, Lept;->e:[I

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->startOption:[I

    .line 860
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->currentOccupantAvatar:[Ljava/lang/String;

    .line 869
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->roomAppId:[Ljava/lang/String;

    .line 872
    iput-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->mediaType:Ljava/lang/Integer;

    .line 879
    sget-object v0, Lept;->j:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->tag:[Ljava/lang/String;

    .line 886
    sget-object v0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;->EMPTY_ARRAY:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->initialApp:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;

    .line 75
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1039
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->hangoutId:Ljava/lang/String;

    .line 1041
    invoke-static {v0, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1042
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->hangoutType:Ljava/lang/Integer;

    .line 1043
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1044
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->topic:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 1045
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->topic:Ljava/lang/String;

    .line 1046
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1048
    :cond_0
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->referringUrl:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 1049
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->referringUrl:Ljava/lang/String;

    .line 1050
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1052
    :cond_1
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->circleId:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 1053
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->circleId:Ljava/lang/String;

    .line 1054
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1056
    :cond_2
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->profileId:[Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->profileId:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 1058
    iget-object v4, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->profileId:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_0
    if-ge v2, v5, :cond_3

    aget-object v6, v4, v2

    .line 1060
    invoke-static {v6}, Lepl;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 1058
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1062
    :cond_3
    add-int/2addr v0, v3

    .line 1063
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->profileId:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1065
    :cond_4
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->activityId:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 1066
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->activityId:Ljava/lang/String;

    .line 1067
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1069
    :cond_5
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->appId:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 1070
    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->appId:Ljava/lang/String;

    .line 1071
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1073
    :cond_6
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->appData:Ljava/lang/String;

    if-eqz v2, :cond_7

    .line 1074
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->appData:Ljava/lang/String;

    .line 1075
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1077
    :cond_7
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->flippy:Ljava/lang/Boolean;

    if-eqz v2, :cond_8

    .line 1078
    const/16 v2, 0xa

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->flippy:Ljava/lang/Boolean;

    .line 1079
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1081
    :cond_8
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->dEPRECATEDCallback:Ljava/lang/Boolean;

    if-eqz v2, :cond_9

    .line 1082
    const/16 v2, 0xb

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->dEPRECATEDCallback:Ljava/lang/Boolean;

    .line 1083
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1085
    :cond_9
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->source:Ljava/lang/Integer;

    if-eqz v2, :cond_a

    .line 1086
    const/16 v2, 0xc

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->source:Ljava/lang/Integer;

    .line 1087
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1089
    :cond_a
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->invitation:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;

    if-eqz v2, :cond_b

    .line 1090
    const/16 v2, 0xd

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->invitation:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;

    .line 1091
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1093
    :cond_b
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->create:Ljava/lang/Boolean;

    if-eqz v2, :cond_c

    .line 1094
    const/16 v2, 0xe

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->create:Ljava/lang/Boolean;

    .line 1095
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1097
    :cond_c
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->nick:Ljava/lang/String;

    if-eqz v2, :cond_d

    .line 1098
    const/16 v2, 0xf

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->nick:Ljava/lang/String;

    .line 1099
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1101
    :cond_d
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->latencyMarks:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;

    if-eqz v2, :cond_e

    .line 1102
    const/16 v2, 0x10

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->latencyMarks:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;

    .line 1103
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1105
    :cond_e
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->callback:Ljava/lang/Integer;

    if-eqz v2, :cond_f

    .line 1106
    const/16 v2, 0x11

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->callback:Ljava/lang/Integer;

    .line 1107
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1109
    :cond_f
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->externalKey:Lcom/google/apps/gcomm/hangout/proto/Hangouts$ExternalEntityKey;

    if-eqz v2, :cond_10

    .line 1110
    const/16 v2, 0x12

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->externalKey:Lcom/google/apps/gcomm/hangout/proto/Hangouts$ExternalEntityKey;

    .line 1111
    invoke-static {v2, v3}, Lepl;->d(ILepr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1113
    :cond_10
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->invitee:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;

    if-eqz v2, :cond_12

    .line 1114
    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->invitee:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_12

    aget-object v5, v3, v2

    .line 1115
    if-eqz v5, :cond_11

    .line 1116
    const/16 v6, 0x13

    .line 1117
    invoke-static {v6, v5}, Lepl;->d(ILepr;)I

    move-result v5

    add-int/2addr v0, v5

    .line 1114
    :cond_11
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1121
    :cond_12
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->shouldAutoInvite:Ljava/lang/Boolean;

    if-eqz v2, :cond_13

    .line 1122
    const/16 v2, 0x14

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->shouldAutoInvite:Ljava/lang/Boolean;

    .line 1123
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1125
    :cond_13
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->contextId:Ljava/lang/String;

    if-eqz v2, :cond_14

    .line 1126
    const/16 v2, 0x15

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->contextId:Ljava/lang/String;

    .line 1127
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1129
    :cond_14
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->shouldMuteVideo:Ljava/lang/Boolean;

    if-eqz v2, :cond_15

    .line 1130
    const/16 v2, 0x16

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->shouldMuteVideo:Ljava/lang/Boolean;

    .line 1131
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1133
    :cond_15
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->externalUserId:Ljava/lang/String;

    if-eqz v2, :cond_16

    .line 1134
    const/16 v2, 0x17

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->externalUserId:Ljava/lang/String;

    .line 1135
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1137
    :cond_16
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->conversationId:Ljava/lang/String;

    if-eqz v2, :cond_17

    .line 1138
    const/16 v2, 0x18

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->conversationId:Ljava/lang/String;

    .line 1139
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1141
    :cond_17
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->squareId:Ljava/lang/String;

    if-eqz v2, :cond_18

    .line 1142
    const/16 v2, 0x19

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->squareId:Ljava/lang/String;

    .line 1143
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1145
    :cond_18
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->invitedOid:Ljava/lang/String;

    if-eqz v2, :cond_19

    .line 1146
    const/16 v2, 0x1a

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->invitedOid:Ljava/lang/String;

    .line 1147
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1149
    :cond_19
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->widgetPublisherId:Ljava/lang/String;

    if-eqz v2, :cond_1a

    .line 1150
    const/16 v2, 0x1b

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->widgetPublisherId:Ljava/lang/String;

    .line 1151
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1153
    :cond_1a
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->shouldHideInviteButton:Ljava/lang/Boolean;

    if-eqz v2, :cond_1b

    .line 1154
    const/16 v2, 0x1c

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->shouldHideInviteButton:Ljava/lang/Boolean;

    .line 1155
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1157
    :cond_1b
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->startOption:[I

    if-eqz v2, :cond_1d

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->startOption:[I

    array-length v2, v2

    if-lez v2, :cond_1d

    .line 1159
    iget-object v4, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->startOption:[I

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_2
    if-ge v2, v5, :cond_1c

    aget v6, v4, v2

    .line 1161
    invoke-static {v6}, Lepl;->f(I)I

    move-result v6

    add-int/2addr v3, v6

    .line 1159
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1163
    :cond_1c
    add-int/2addr v0, v3

    .line 1164
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->startOption:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 1166
    :cond_1d
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->currentOccupantAvatar:[Ljava/lang/String;

    if-eqz v2, :cond_1f

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->currentOccupantAvatar:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1f

    .line 1168
    iget-object v4, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->currentOccupantAvatar:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_3
    if-ge v2, v5, :cond_1e

    aget-object v6, v4, v2

    .line 1170
    invoke-static {v6}, Lepl;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 1168
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1172
    :cond_1e
    add-int/2addr v0, v3

    .line 1173
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->currentOccupantAvatar:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 1175
    :cond_1f
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->numCircles:Ljava/lang/Long;

    if-eqz v2, :cond_20

    .line 1176
    const/16 v2, 0x1f

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->numCircles:Ljava/lang/Long;

    .line 1177
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lepl;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 1179
    :cond_20
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->externalEventId:Ljava/lang/String;

    if-eqz v2, :cond_21

    .line 1180
    const/16 v2, 0x20

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->externalEventId:Ljava/lang/String;

    .line 1181
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1183
    :cond_21
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->gatewayAppId:Ljava/lang/String;

    if-eqz v2, :cond_22

    .line 1184
    const/16 v2, 0x21

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->gatewayAppId:Ljava/lang/String;

    .line 1185
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1187
    :cond_22
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->roomAppId:[Ljava/lang/String;

    if-eqz v2, :cond_24

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->roomAppId:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_24

    .line 1189
    iget-object v4, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->roomAppId:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_4
    if-ge v2, v5, :cond_23

    aget-object v6, v4, v2

    .line 1191
    invoke-static {v6}, Lepl;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 1189
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 1193
    :cond_23
    add-int/2addr v0, v3

    .line 1194
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->roomAppId:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 1196
    :cond_24
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->mediaType:Ljava/lang/Integer;

    if-eqz v2, :cond_25

    .line 1197
    const/16 v2, 0x23

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->mediaType:Ljava/lang/Integer;

    .line 1198
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lepl;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1200
    :cond_25
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->appIntegrationId:Ljava/lang/String;

    if-eqz v2, :cond_26

    .line 1201
    const/16 v2, 0x24

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->appIntegrationId:Ljava/lang/String;

    .line 1202
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1204
    :cond_26
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->shouldAutoJoin:Ljava/lang/Boolean;

    if-eqz v2, :cond_27

    .line 1205
    const/16 v2, 0x25

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->shouldAutoJoin:Ljava/lang/Boolean;

    .line 1206
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lepl;->g(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1208
    :cond_27
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->tag:[Ljava/lang/String;

    if-eqz v2, :cond_29

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->tag:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_29

    .line 1210
    iget-object v4, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->tag:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_5
    if-ge v2, v5, :cond_28

    aget-object v6, v4, v2

    .line 1212
    invoke-static {v6}, Lepl;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 1210
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 1214
    :cond_28
    add-int/2addr v0, v3

    .line 1215
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->tag:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 1217
    :cond_29
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->encodedCalendarId:Ljava/lang/String;

    if-eqz v2, :cond_2a

    .line 1218
    const/16 v2, 0x27

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->encodedCalendarId:Ljava/lang/String;

    .line 1219
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1221
    :cond_2a
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->xsrfToken:Ljava/lang/String;

    if-eqz v2, :cond_2b

    .line 1222
    const/16 v2, 0x28

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->xsrfToken:Ljava/lang/String;

    .line 1223
    invoke-static {v2, v3}, Lepl;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1225
    :cond_2b
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->initialApp:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;

    if-eqz v2, :cond_2d

    .line 1226
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->initialApp:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;

    array-length v3, v2

    :goto_6
    if-ge v1, v3, :cond_2d

    aget-object v4, v2, v1

    .line 1227
    if-eqz v4, :cond_2c

    .line 1228
    const/16 v5, 0x29

    .line 1229
    invoke-static {v5, v4}, Lepl;->d(ILepr;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1226
    :cond_2c
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 1233
    :cond_2d
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->participantLogId:Ljava/lang/String;

    if-eqz v1, :cond_2e

    .line 1234
    const/16 v1, 0x2a

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->participantLogId:Ljava/lang/String;

    .line 1235
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1237
    :cond_2e
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1238
    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->cachedSize:I

    .line 1239
    return v0
.end method

.method public mergeFrom(Lepk;)Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;
    .locals 8

    .prologue
    const/16 v7, 0x32

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1247
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    .line 1248
    sparse-switch v0, :sswitch_data_0

    .line 1252
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->unknownFieldData:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1253
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->unknownFieldData:Ljava/util/List;

    .line 1256
    :cond_1
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->unknownFieldData:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1258
    :sswitch_0
    return-object p0

    .line 1263
    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->hangoutId:Ljava/lang/String;

    goto :goto_0

    .line 1267
    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    .line 1268
    if-eqz v0, :cond_2

    if-eq v0, v4, :cond_2

    if-eq v0, v5, :cond_2

    if-ne v0, v6, :cond_3

    .line 1272
    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->hangoutType:Ljava/lang/Integer;

    goto :goto_0

    .line 1274
    :cond_3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->hangoutType:Ljava/lang/Integer;

    goto :goto_0

    .line 1279
    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->topic:Ljava/lang/String;

    goto :goto_0

    .line 1283
    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->referringUrl:Ljava/lang/String;

    goto :goto_0

    .line 1287
    :sswitch_5
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->circleId:Ljava/lang/String;

    goto :goto_0

    .line 1291
    :sswitch_6
    invoke-static {p1, v7}, Lept;->a(Lepk;I)I

    move-result v2

    .line 1292
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->profileId:[Ljava/lang/String;

    array-length v0, v0

    .line 1293
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 1294
    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->profileId:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1295
    iput-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->profileId:[Ljava/lang/String;

    .line 1296
    :goto_1
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->profileId:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 1297
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->profileId:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 1298
    invoke-virtual {p1}, Lepk;->a()I

    .line 1296
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1301
    :cond_4
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->profileId:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_0

    .line 1305
    :sswitch_7
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->activityId:Ljava/lang/String;

    goto/16 :goto_0

    .line 1309
    :sswitch_8
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->appId:Ljava/lang/String;

    goto/16 :goto_0

    .line 1313
    :sswitch_9
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->appData:Ljava/lang/String;

    goto/16 :goto_0

    .line 1317
    :sswitch_a
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->flippy:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 1321
    :sswitch_b
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->dEPRECATEDCallback:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 1325
    :sswitch_c
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    .line 1326
    if-eqz v0, :cond_5

    const/16 v2, 0x59

    if-eq v0, v2, :cond_5

    if-eq v0, v4, :cond_5

    if-eq v0, v5, :cond_5

    if-eq v0, v6, :cond_5

    const/4 v2, 0x4

    if-eq v0, v2, :cond_5

    const/4 v2, 0x5

    if-eq v0, v2, :cond_5

    const/4 v2, 0x6

    if-eq v0, v2, :cond_5

    const/4 v2, 0x7

    if-eq v0, v2, :cond_5

    const/16 v2, 0x8

    if-eq v0, v2, :cond_5

    const/16 v2, 0xa

    if-eq v0, v2, :cond_5

    const/16 v2, 0x11

    if-eq v0, v2, :cond_5

    const/16 v2, 0xb

    if-eq v0, v2, :cond_5

    const/16 v2, 0x9

    if-eq v0, v2, :cond_5

    const/16 v2, 0xc

    if-eq v0, v2, :cond_5

    const/16 v2, 0x13

    if-eq v0, v2, :cond_5

    const/16 v2, 0x12

    if-eq v0, v2, :cond_5

    const/16 v2, 0xd

    if-eq v0, v2, :cond_5

    const/16 v2, 0xe

    if-eq v0, v2, :cond_5

    const/16 v2, 0xf

    if-eq v0, v2, :cond_5

    const/16 v2, 0x10

    if-eq v0, v2, :cond_5

    const/16 v2, 0x14

    if-eq v0, v2, :cond_5

    const/16 v2, 0x15

    if-eq v0, v2, :cond_5

    const/16 v2, 0x16

    if-eq v0, v2, :cond_5

    const/16 v2, 0x17

    if-eq v0, v2, :cond_5

    const/16 v2, 0x18

    if-eq v0, v2, :cond_5

    const/16 v2, 0x19

    if-eq v0, v2, :cond_5

    const/16 v2, 0x1a

    if-eq v0, v2, :cond_5

    const/16 v2, 0x1b

    if-eq v0, v2, :cond_5

    const/16 v2, 0x1c

    if-eq v0, v2, :cond_5

    const/16 v2, 0x1d

    if-eq v0, v2, :cond_5

    const/16 v2, 0x1e

    if-eq v0, v2, :cond_5

    const/16 v2, 0x1f

    if-eq v0, v2, :cond_5

    const/16 v2, 0x20

    if-eq v0, v2, :cond_5

    const/16 v2, 0x21

    if-eq v0, v2, :cond_5

    const/16 v2, 0x22

    if-eq v0, v2, :cond_5

    const/16 v2, 0x23

    if-eq v0, v2, :cond_5

    const/16 v2, 0x24

    if-eq v0, v2, :cond_5

    const/16 v2, 0x25

    if-eq v0, v2, :cond_5

    const/16 v2, 0x26

    if-eq v0, v2, :cond_5

    const/16 v2, 0x27

    if-eq v0, v2, :cond_5

    const/16 v2, 0x28

    if-eq v0, v2, :cond_5

    const/16 v2, 0x29

    if-eq v0, v2, :cond_5

    const/16 v2, 0x2a

    if-eq v0, v2, :cond_5

    const/16 v2, 0x2b

    if-eq v0, v2, :cond_5

    const/16 v2, 0x2c

    if-eq v0, v2, :cond_5

    const/16 v2, 0x2d

    if-eq v0, v2, :cond_5

    const/16 v2, 0x2e

    if-eq v0, v2, :cond_5

    const/16 v2, 0x2f

    if-eq v0, v2, :cond_5

    const/16 v2, 0x30

    if-eq v0, v2, :cond_5

    const/16 v2, 0x31

    if-eq v0, v2, :cond_5

    if-eq v0, v7, :cond_5

    const/16 v2, 0x33

    if-eq v0, v2, :cond_5

    const/16 v2, 0x34

    if-eq v0, v2, :cond_5

    const/16 v2, 0x35

    if-eq v0, v2, :cond_5

    const/16 v2, 0x36

    if-eq v0, v2, :cond_5

    const/16 v2, 0x37

    if-eq v0, v2, :cond_5

    const/16 v2, 0x38

    if-eq v0, v2, :cond_5

    const/16 v2, 0x39

    if-eq v0, v2, :cond_5

    const/16 v2, 0x3a

    if-eq v0, v2, :cond_5

    const/16 v2, 0x3b

    if-eq v0, v2, :cond_5

    const/16 v2, 0x3c

    if-eq v0, v2, :cond_5

    const/16 v2, 0x3d

    if-eq v0, v2, :cond_5

    const/16 v2, 0x3e

    if-eq v0, v2, :cond_5

    const/16 v2, 0x3f

    if-eq v0, v2, :cond_5

    const/16 v2, 0x40

    if-eq v0, v2, :cond_5

    const/16 v2, 0x41

    if-eq v0, v2, :cond_5

    const/16 v2, 0x42

    if-eq v0, v2, :cond_5

    const/16 v2, 0x43

    if-eq v0, v2, :cond_5

    const/16 v2, 0x44

    if-eq v0, v2, :cond_5

    const/16 v2, 0x45

    if-eq v0, v2, :cond_5

    const/16 v2, 0x46

    if-eq v0, v2, :cond_5

    const/16 v2, 0x47

    if-eq v0, v2, :cond_5

    const/16 v2, 0x48

    if-eq v0, v2, :cond_5

    const/16 v2, 0x49

    if-eq v0, v2, :cond_5

    const/16 v2, 0x4a

    if-eq v0, v2, :cond_5

    const/16 v2, 0x4b

    if-eq v0, v2, :cond_5

    const/16 v2, 0x4c

    if-eq v0, v2, :cond_5

    const/16 v2, 0x4d

    if-eq v0, v2, :cond_5

    const/16 v2, 0x4e

    if-eq v0, v2, :cond_5

    const/16 v2, 0x4f

    if-eq v0, v2, :cond_5

    const/16 v2, 0x50

    if-eq v0, v2, :cond_5

    const/16 v2, 0x51

    if-eq v0, v2, :cond_5

    const/16 v2, 0x52

    if-eq v0, v2, :cond_5

    const/16 v2, 0x53

    if-eq v0, v2, :cond_5

    const/16 v2, 0x54

    if-eq v0, v2, :cond_5

    const/16 v2, 0x55

    if-eq v0, v2, :cond_5

    const/16 v2, 0x56

    if-eq v0, v2, :cond_5

    const/16 v2, 0x57

    if-eq v0, v2, :cond_5

    const/16 v2, 0x58

    if-eq v0, v2, :cond_5

    const/16 v2, 0x5a

    if-eq v0, v2, :cond_5

    const/16 v2, 0x5b

    if-eq v0, v2, :cond_5

    const/16 v2, 0x5c

    if-eq v0, v2, :cond_5

    const/16 v2, 0x5d

    if-eq v0, v2, :cond_5

    const/16 v2, 0x5e

    if-eq v0, v2, :cond_5

    const/16 v2, 0x5f

    if-eq v0, v2, :cond_5

    const/16 v2, 0x60

    if-eq v0, v2, :cond_5

    const/16 v2, 0x61

    if-eq v0, v2, :cond_5

    const/16 v2, 0x62

    if-eq v0, v2, :cond_5

    const/16 v2, 0x63

    if-eq v0, v2, :cond_5

    const/16 v2, 0x64

    if-eq v0, v2, :cond_5

    const/16 v2, 0x65

    if-eq v0, v2, :cond_5

    const/16 v2, 0x66

    if-eq v0, v2, :cond_5

    const/16 v2, 0x67

    if-eq v0, v2, :cond_5

    const/16 v2, 0x68

    if-ne v0, v2, :cond_6

    .line 1431
    :cond_5
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->source:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 1433
    :cond_6
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->source:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 1438
    :sswitch_d
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->invitation:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;

    if-nez v0, :cond_7

    .line 1439
    new-instance v0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;

    invoke-direct {v0}, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;-><init>()V

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->invitation:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;

    .line 1441
    :cond_7
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->invitation:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    .line 1445
    :sswitch_e
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->create:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 1449
    :sswitch_f
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->nick:Ljava/lang/String;

    goto/16 :goto_0

    .line 1453
    :sswitch_10
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->latencyMarks:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;

    if-nez v0, :cond_8

    .line 1454
    new-instance v0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;

    invoke-direct {v0}, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;-><init>()V

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->latencyMarks:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;

    .line 1456
    :cond_8
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->latencyMarks:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    .line 1460
    :sswitch_11
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    .line 1461
    if-eqz v0, :cond_9

    if-eq v0, v4, :cond_9

    if-ne v0, v5, :cond_a

    .line 1464
    :cond_9
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->callback:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 1466
    :cond_a
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->callback:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 1471
    :sswitch_12
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->externalKey:Lcom/google/apps/gcomm/hangout/proto/Hangouts$ExternalEntityKey;

    if-nez v0, :cond_b

    .line 1472
    new-instance v0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$ExternalEntityKey;

    invoke-direct {v0}, Lcom/google/apps/gcomm/hangout/proto/Hangouts$ExternalEntityKey;-><init>()V

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->externalKey:Lcom/google/apps/gcomm/hangout/proto/Hangouts$ExternalEntityKey;

    .line 1474
    :cond_b
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->externalKey:Lcom/google/apps/gcomm/hangout/proto/Hangouts$ExternalEntityKey;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    .line 1478
    :sswitch_13
    const/16 v0, 0x9a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    .line 1479
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->invitee:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;

    if-nez v0, :cond_d

    move v0, v1

    .line 1480
    :goto_2
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;

    .line 1481
    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->invitee:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;

    if-eqz v3, :cond_c

    .line 1482
    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->invitee:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1484
    :cond_c
    iput-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->invitee:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;

    .line 1485
    :goto_3
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->invitee:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_e

    .line 1486
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->invitee:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;

    new-instance v3, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;

    invoke-direct {v3}, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;-><init>()V

    aput-object v3, v2, v0

    .line 1487
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->invitee:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    .line 1488
    invoke-virtual {p1}, Lepk;->a()I

    .line 1485
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 1479
    :cond_d
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->invitee:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;

    array-length v0, v0

    goto :goto_2

    .line 1491
    :cond_e
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->invitee:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;

    new-instance v3, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;

    invoke-direct {v3}, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;-><init>()V

    aput-object v3, v2, v0

    .line 1492
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->invitee:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    .line 1496
    :sswitch_14
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->shouldAutoInvite:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 1500
    :sswitch_15
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->contextId:Ljava/lang/String;

    goto/16 :goto_0

    .line 1504
    :sswitch_16
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->shouldMuteVideo:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 1508
    :sswitch_17
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->externalUserId:Ljava/lang/String;

    goto/16 :goto_0

    .line 1512
    :sswitch_18
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->conversationId:Ljava/lang/String;

    goto/16 :goto_0

    .line 1516
    :sswitch_19
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->squareId:Ljava/lang/String;

    goto/16 :goto_0

    .line 1520
    :sswitch_1a
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->invitedOid:Ljava/lang/String;

    goto/16 :goto_0

    .line 1524
    :sswitch_1b
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->widgetPublisherId:Ljava/lang/String;

    goto/16 :goto_0

    .line 1528
    :sswitch_1c
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->shouldHideInviteButton:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 1532
    :sswitch_1d
    const/16 v0, 0xe8

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    .line 1533
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->startOption:[I

    array-length v0, v0

    .line 1534
    add-int/2addr v2, v0

    new-array v2, v2, [I

    .line 1535
    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->startOption:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1536
    iput-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->startOption:[I

    .line 1537
    :goto_4
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->startOption:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_f

    .line 1538
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->startOption:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v3

    aput v3, v2, v0

    .line 1539
    invoke-virtual {p1}, Lepk;->a()I

    .line 1537
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1542
    :cond_f
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->startOption:[I

    invoke-virtual {p1}, Lepk;->f()I

    move-result v3

    aput v3, v2, v0

    goto/16 :goto_0

    .line 1546
    :sswitch_1e
    const/16 v0, 0xf2

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    .line 1547
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->currentOccupantAvatar:[Ljava/lang/String;

    array-length v0, v0

    .line 1548
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 1549
    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->currentOccupantAvatar:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1550
    iput-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->currentOccupantAvatar:[Ljava/lang/String;

    .line 1551
    :goto_5
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->currentOccupantAvatar:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_10

    .line 1552
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->currentOccupantAvatar:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 1553
    invoke-virtual {p1}, Lepk;->a()I

    .line 1551
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 1556
    :cond_10
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->currentOccupantAvatar:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    .line 1560
    :sswitch_1f
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->numCircles:Ljava/lang/Long;

    goto/16 :goto_0

    .line 1564
    :sswitch_20
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->externalEventId:Ljava/lang/String;

    goto/16 :goto_0

    .line 1568
    :sswitch_21
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->gatewayAppId:Ljava/lang/String;

    goto/16 :goto_0

    .line 1572
    :sswitch_22
    const/16 v0, 0x112

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    .line 1573
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->roomAppId:[Ljava/lang/String;

    array-length v0, v0

    .line 1574
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 1575
    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->roomAppId:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1576
    iput-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->roomAppId:[Ljava/lang/String;

    .line 1577
    :goto_6
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->roomAppId:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_11

    .line 1578
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->roomAppId:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 1579
    invoke-virtual {p1}, Lepk;->a()I

    .line 1577
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 1582
    :cond_11
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->roomAppId:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    .line 1586
    :sswitch_23
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    .line 1587
    if-eq v0, v4, :cond_12

    if-ne v0, v5, :cond_13

    .line 1589
    :cond_12
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->mediaType:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 1591
    :cond_13
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->mediaType:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 1596
    :sswitch_24
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->appIntegrationId:Ljava/lang/String;

    goto/16 :goto_0

    .line 1600
    :sswitch_25
    invoke-virtual {p1}, Lepk;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->shouldAutoJoin:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 1604
    :sswitch_26
    const/16 v0, 0x132

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    .line 1605
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->tag:[Ljava/lang/String;

    array-length v0, v0

    .line 1606
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 1607
    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->tag:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1608
    iput-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->tag:[Ljava/lang/String;

    .line 1609
    :goto_7
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->tag:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_14

    .line 1610
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->tag:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 1611
    invoke-virtual {p1}, Lepk;->a()I

    .line 1609
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 1614
    :cond_14
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->tag:[Ljava/lang/String;

    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    .line 1618
    :sswitch_27
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->encodedCalendarId:Ljava/lang/String;

    goto/16 :goto_0

    .line 1622
    :sswitch_28
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->xsrfToken:Ljava/lang/String;

    goto/16 :goto_0

    .line 1626
    :sswitch_29
    const/16 v0, 0x14a

    invoke-static {p1, v0}, Lept;->a(Lepk;I)I

    move-result v2

    .line 1627
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->initialApp:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;

    if-nez v0, :cond_16

    move v0, v1

    .line 1628
    :goto_8
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;

    .line 1629
    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->initialApp:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;

    if-eqz v3, :cond_15

    .line 1630
    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->initialApp:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1632
    :cond_15
    iput-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->initialApp:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;

    .line 1633
    :goto_9
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->initialApp:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_17

    .line 1634
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->initialApp:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;

    new-instance v3, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;

    invoke-direct {v3}, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;-><init>()V

    aput-object v3, v2, v0

    .line 1635
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->initialApp:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lepk;->a(Lepr;)V

    .line 1636
    invoke-virtual {p1}, Lepk;->a()I

    .line 1633
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 1627
    :cond_16
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->initialApp:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;

    array-length v0, v0

    goto :goto_8

    .line 1639
    :cond_17
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->initialApp:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;

    new-instance v3, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;

    invoke-direct {v3}, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;-><init>()V

    aput-object v3, v2, v0

    .line 1640
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->initialApp:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto/16 :goto_0

    .line 1644
    :sswitch_2a
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->participantLogId:Ljava/lang/String;

    goto/16 :goto_0

    .line 1248
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x6a -> :sswitch_d
        0x70 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x88 -> :sswitch_11
        0x92 -> :sswitch_12
        0x9a -> :sswitch_13
        0xa0 -> :sswitch_14
        0xaa -> :sswitch_15
        0xb0 -> :sswitch_16
        0xba -> :sswitch_17
        0xc2 -> :sswitch_18
        0xca -> :sswitch_19
        0xd2 -> :sswitch_1a
        0xda -> :sswitch_1b
        0xe0 -> :sswitch_1c
        0xe8 -> :sswitch_1d
        0xf2 -> :sswitch_1e
        0xf8 -> :sswitch_1f
        0x102 -> :sswitch_20
        0x10a -> :sswitch_21
        0x112 -> :sswitch_22
        0x118 -> :sswitch_23
        0x122 -> :sswitch_24
        0x128 -> :sswitch_25
        0x132 -> :sswitch_26
        0x13a -> :sswitch_27
        0x142 -> :sswitch_28
        0x14a -> :sswitch_29
        0x152 -> :sswitch_2a
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lepk;)Lepr;
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0, p1}, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->mergeFrom(Lepk;)Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lepl;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 893
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->hangoutId:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 894
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->hangoutType:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 895
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->topic:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 896
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->topic:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 898
    :cond_0
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->referringUrl:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 899
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->referringUrl:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 901
    :cond_1
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->circleId:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 902
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->circleId:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 904
    :cond_2
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->profileId:[Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 905
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->profileId:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 906
    const/4 v5, 0x6

    invoke-virtual {p1, v5, v4}, Lepl;->a(ILjava/lang/String;)V

    .line 905
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 909
    :cond_3
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->activityId:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 910
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->activityId:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 912
    :cond_4
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->appId:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 913
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->appId:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 915
    :cond_5
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->appData:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 916
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->appData:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 918
    :cond_6
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->flippy:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 919
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->flippy:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 921
    :cond_7
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->dEPRECATEDCallback:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 922
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->dEPRECATEDCallback:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 924
    :cond_8
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->source:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 925
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->source:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 927
    :cond_9
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->invitation:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;

    if-eqz v1, :cond_a

    .line 928
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->invitation:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 930
    :cond_a
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->create:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    .line 931
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->create:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 933
    :cond_b
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->nick:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 934
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->nick:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 936
    :cond_c
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->latencyMarks:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;

    if-eqz v1, :cond_d

    .line 937
    const/16 v1, 0x10

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->latencyMarks:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 939
    :cond_d
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->callback:Ljava/lang/Integer;

    if-eqz v1, :cond_e

    .line 940
    const/16 v1, 0x11

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->callback:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 942
    :cond_e
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->externalKey:Lcom/google/apps/gcomm/hangout/proto/Hangouts$ExternalEntityKey;

    if-eqz v1, :cond_f

    .line 943
    const/16 v1, 0x12

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->externalKey:Lcom/google/apps/gcomm/hangout/proto/Hangouts$ExternalEntityKey;

    invoke-virtual {p1, v1, v2}, Lepl;->b(ILepr;)V

    .line 945
    :cond_f
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->invitee:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;

    if-eqz v1, :cond_11

    .line 946
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->invitee:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_11

    aget-object v4, v2, v1

    .line 947
    if-eqz v4, :cond_10

    .line 948
    const/16 v5, 0x13

    invoke-virtual {p1, v5, v4}, Lepl;->b(ILepr;)V

    .line 946
    :cond_10
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 952
    :cond_11
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->shouldAutoInvite:Ljava/lang/Boolean;

    if-eqz v1, :cond_12

    .line 953
    const/16 v1, 0x14

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->shouldAutoInvite:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 955
    :cond_12
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->contextId:Ljava/lang/String;

    if-eqz v1, :cond_13

    .line 956
    const/16 v1, 0x15

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->contextId:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 958
    :cond_13
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->shouldMuteVideo:Ljava/lang/Boolean;

    if-eqz v1, :cond_14

    .line 959
    const/16 v1, 0x16

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->shouldMuteVideo:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 961
    :cond_14
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->externalUserId:Ljava/lang/String;

    if-eqz v1, :cond_15

    .line 962
    const/16 v1, 0x17

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->externalUserId:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 964
    :cond_15
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->conversationId:Ljava/lang/String;

    if-eqz v1, :cond_16

    .line 965
    const/16 v1, 0x18

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->conversationId:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 967
    :cond_16
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->squareId:Ljava/lang/String;

    if-eqz v1, :cond_17

    .line 968
    const/16 v1, 0x19

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->squareId:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 970
    :cond_17
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->invitedOid:Ljava/lang/String;

    if-eqz v1, :cond_18

    .line 971
    const/16 v1, 0x1a

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->invitedOid:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 973
    :cond_18
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->widgetPublisherId:Ljava/lang/String;

    if-eqz v1, :cond_19

    .line 974
    const/16 v1, 0x1b

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->widgetPublisherId:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 976
    :cond_19
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->shouldHideInviteButton:Ljava/lang/Boolean;

    if-eqz v1, :cond_1a

    .line 977
    const/16 v1, 0x1c

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->shouldHideInviteButton:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 979
    :cond_1a
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->startOption:[I

    if-eqz v1, :cond_1b

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->startOption:[I

    array-length v1, v1

    if-lez v1, :cond_1b

    .line 980
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->startOption:[I

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_1b

    aget v4, v2, v1

    .line 981
    const/16 v5, 0x1d

    invoke-virtual {p1, v5, v4}, Lepl;->a(II)V

    .line 980
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 984
    :cond_1b
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->currentOccupantAvatar:[Ljava/lang/String;

    if-eqz v1, :cond_1c

    .line 985
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->currentOccupantAvatar:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_3
    if-ge v1, v3, :cond_1c

    aget-object v4, v2, v1

    .line 986
    const/16 v5, 0x1e

    invoke-virtual {p1, v5, v4}, Lepl;->a(ILjava/lang/String;)V

    .line 985
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 989
    :cond_1c
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->numCircles:Ljava/lang/Long;

    if-eqz v1, :cond_1d

    .line 990
    const/16 v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->numCircles:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Lepl;->b(IJ)V

    .line 992
    :cond_1d
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->externalEventId:Ljava/lang/String;

    if-eqz v1, :cond_1e

    .line 993
    const/16 v1, 0x20

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->externalEventId:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 995
    :cond_1e
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->gatewayAppId:Ljava/lang/String;

    if-eqz v1, :cond_1f

    .line 996
    const/16 v1, 0x21

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->gatewayAppId:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 998
    :cond_1f
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->roomAppId:[Ljava/lang/String;

    if-eqz v1, :cond_20

    .line 999
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->roomAppId:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_4
    if-ge v1, v3, :cond_20

    aget-object v4, v2, v1

    .line 1000
    const/16 v5, 0x22

    invoke-virtual {p1, v5, v4}, Lepl;->a(ILjava/lang/String;)V

    .line 999
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1003
    :cond_20
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->mediaType:Ljava/lang/Integer;

    if-eqz v1, :cond_21

    .line 1004
    const/16 v1, 0x23

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->mediaType:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(II)V

    .line 1006
    :cond_21
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->appIntegrationId:Ljava/lang/String;

    if-eqz v1, :cond_22

    .line 1007
    const/16 v1, 0x24

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->appIntegrationId:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 1009
    :cond_22
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->shouldAutoJoin:Ljava/lang/Boolean;

    if-eqz v1, :cond_23

    .line 1010
    const/16 v1, 0x25

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->shouldAutoJoin:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lepl;->a(IZ)V

    .line 1012
    :cond_23
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->tag:[Ljava/lang/String;

    if-eqz v1, :cond_24

    .line 1013
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->tag:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_5
    if-ge v1, v3, :cond_24

    aget-object v4, v2, v1

    .line 1014
    const/16 v5, 0x26

    invoke-virtual {p1, v5, v4}, Lepl;->a(ILjava/lang/String;)V

    .line 1013
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 1017
    :cond_24
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->encodedCalendarId:Ljava/lang/String;

    if-eqz v1, :cond_25

    .line 1018
    const/16 v1, 0x27

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->encodedCalendarId:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 1020
    :cond_25
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->xsrfToken:Ljava/lang/String;

    if-eqz v1, :cond_26

    .line 1021
    const/16 v1, 0x28

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->xsrfToken:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lepl;->a(ILjava/lang/String;)V

    .line 1023
    :cond_26
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->initialApp:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;

    if-eqz v1, :cond_28

    .line 1024
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->initialApp:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;

    array-length v2, v1

    :goto_6
    if-ge v0, v2, :cond_28

    aget-object v3, v1, v0

    .line 1025
    if-eqz v3, :cond_27

    .line 1026
    const/16 v4, 0x29

    invoke-virtual {p1, v4, v3}, Lepl;->b(ILepr;)V

    .line 1024
    :cond_27
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 1030
    :cond_28
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->participantLogId:Ljava/lang/String;

    if-eqz v0, :cond_29

    .line 1031
    const/16 v0, 0x2a

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->participantLogId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1033
    :cond_29
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1035
    return-void
.end method
