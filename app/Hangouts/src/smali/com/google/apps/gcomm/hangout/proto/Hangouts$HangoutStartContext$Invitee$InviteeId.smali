.class public final Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;
.super Lepn;
.source "PG"


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;


# instance fields
.field public circleId:Ljava/lang/String;

.field public email:Ljava/lang/String;

.field public phoneNumber:Ljava/lang/String;

.field public profileId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 499
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;

    sput-object v0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;->EMPTY_ARRAY:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 500
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 531
    const/4 v0, 0x0

    .line 532
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;->profileId:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 533
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;->profileId:Ljava/lang/String;

    .line 534
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 536
    :cond_0
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;->phoneNumber:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 537
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;->phoneNumber:Ljava/lang/String;

    .line 538
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 540
    :cond_1
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;->circleId:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 541
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;->circleId:Ljava/lang/String;

    .line 542
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 544
    :cond_2
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;->email:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 545
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;->email:Ljava/lang/String;

    .line 546
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 548
    :cond_3
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 549
    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;->cachedSize:I

    .line 550
    return v0
.end method

.method public mergeFrom(Lepk;)Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;
    .locals 2

    .prologue
    .line 558
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    .line 559
    sparse-switch v0, :sswitch_data_0

    .line 563
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    .line 564
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;->unknownFieldData:Ljava/util/List;

    .line 567
    :cond_1
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 569
    :sswitch_0
    return-object p0

    .line 574
    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;->profileId:Ljava/lang/String;

    goto :goto_0

    .line 578
    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;->phoneNumber:Ljava/lang/String;

    goto :goto_0

    .line 582
    :sswitch_3
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;->circleId:Ljava/lang/String;

    goto :goto_0

    .line 586
    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;->email:Ljava/lang/String;

    goto :goto_0

    .line 559
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lepk;)Lepr;
    .locals 1

    .prologue
    .line 496
    invoke-virtual {p0, p1}, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;->mergeFrom(Lepk;)Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 513
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;->profileId:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 514
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;->profileId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 516
    :cond_0
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;->phoneNumber:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 517
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;->phoneNumber:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 519
    :cond_1
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;->circleId:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 520
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;->circleId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 522
    :cond_2
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;->email:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 523
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;->email:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 525
    :cond_3
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 527
    return-void
.end method
