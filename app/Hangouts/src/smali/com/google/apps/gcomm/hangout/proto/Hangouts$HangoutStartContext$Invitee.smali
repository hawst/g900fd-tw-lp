.class public final Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;
.super Lepn;
.source "PG"


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;


# instance fields
.field public inviteeId:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;

.field public profileId:Ljava/lang/String;

.field public profileName:Ljava/lang/String;

.field public shortDisplayName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 493
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;

    sput-object v0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;->EMPTY_ARRAY:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 494
    invoke-direct {p0}, Lepn;-><init>()V

    .line 600
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;->inviteeId:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;

    .line 494
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 625
    const/4 v0, 0x0

    .line 626
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;->profileId:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 627
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;->profileId:Ljava/lang/String;

    .line 628
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 630
    :cond_0
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;->profileName:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 631
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;->profileName:Ljava/lang/String;

    .line 632
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 634
    :cond_1
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;->inviteeId:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;

    if-eqz v1, :cond_2

    .line 635
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;->inviteeId:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;

    .line 636
    invoke-static {v1, v2}, Lepl;->d(ILepr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 638
    :cond_2
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;->shortDisplayName:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 639
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;->shortDisplayName:Ljava/lang/String;

    .line 640
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 642
    :cond_3
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 643
    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;->cachedSize:I

    .line 644
    return v0
.end method

.method public mergeFrom(Lepk;)Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;
    .locals 2

    .prologue
    .line 652
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    .line 653
    sparse-switch v0, :sswitch_data_0

    .line 657
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    .line 658
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;->unknownFieldData:Ljava/util/List;

    .line 661
    :cond_1
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 663
    :sswitch_0
    return-object p0

    .line 668
    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;->profileId:Ljava/lang/String;

    goto :goto_0

    .line 672
    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;->profileName:Ljava/lang/String;

    goto :goto_0

    .line 676
    :sswitch_3
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;->inviteeId:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;

    if-nez v0, :cond_2

    .line 677
    new-instance v0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;

    invoke-direct {v0}, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;-><init>()V

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;->inviteeId:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;

    .line 679
    :cond_2
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;->inviteeId:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    .line 683
    :sswitch_4
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;->shortDisplayName:Ljava/lang/String;

    goto :goto_0

    .line 653
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lepk;)Lepr;
    .locals 1

    .prologue
    .line 490
    invoke-virtual {p0, p1}, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;->mergeFrom(Lepk;)Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 607
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;->profileId:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 608
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;->profileId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 610
    :cond_0
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;->profileName:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 611
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;->profileName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 613
    :cond_1
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;->inviteeId:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;

    if-eqz v0, :cond_2

    .line 614
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;->inviteeId:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee$InviteeId;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 616
    :cond_2
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;->shortDisplayName:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 617
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;->shortDisplayName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 619
    :cond_3
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitee;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 621
    return-void
.end method
