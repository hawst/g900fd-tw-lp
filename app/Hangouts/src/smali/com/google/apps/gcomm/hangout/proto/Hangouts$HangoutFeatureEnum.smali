.class public final Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutFeatureEnum;
.super Lepn;
.source "PG"


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutFeatureEnum;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1885
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutFeatureEnum;

    sput-object v0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutFeatureEnum;->EMPTY_ARRAY:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutFeatureEnum;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1886
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public mergeFrom(Lepk;)Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutFeatureEnum;
    .locals 2

    .prologue
    .line 1905
    :cond_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    .line 1906
    packed-switch v0, :pswitch_data_0

    .line 1910
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutFeatureEnum;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1911
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutFeatureEnum;->unknownFieldData:Ljava/util/List;

    .line 1914
    :cond_1
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutFeatureEnum;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1916
    :pswitch_0
    return-object p0

    .line 1906
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lepk;)Lepr;
    .locals 1

    .prologue
    .line 1882
    invoke-virtual {p0, p1}, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutFeatureEnum;->mergeFrom(Lepk;)Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutFeatureEnum;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lepl;)V
    .locals 1

    .prologue
    .line 1896
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutFeatureEnum;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1898
    return-void
.end method
