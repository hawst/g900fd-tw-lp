.class public final Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;
.super Lepn;
.source "PG"


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;


# instance fields
.field public clientLaunch:Ljava/lang/Long;

.field public serverCreateRedirectEnd:Ljava/lang/Long;

.field public serverCreateRoomEnd:Ljava/lang/Long;

.field public serverCreateRoomStart:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 394
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;

    sput-object v0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;->EMPTY_ARRAY:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 395
    invoke-direct {p0}, Lepn;-><init>()V

    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 426
    const/4 v0, 0x0

    .line 427
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;->clientLaunch:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 428
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;->clientLaunch:Ljava/lang/Long;

    .line 429
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lepl;->e(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 431
    :cond_0
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;->serverCreateRoomStart:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 432
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;->serverCreateRoomStart:Ljava/lang/Long;

    .line 433
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 435
    :cond_1
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;->serverCreateRoomEnd:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 436
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;->serverCreateRoomEnd:Ljava/lang/Long;

    .line 437
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 439
    :cond_2
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;->serverCreateRedirectEnd:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 440
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;->serverCreateRedirectEnd:Ljava/lang/Long;

    .line 441
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 443
    :cond_3
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 444
    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;->cachedSize:I

    .line 445
    return v0
.end method

.method public mergeFrom(Lepk;)Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;
    .locals 2

    .prologue
    .line 453
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    .line 454
    sparse-switch v0, :sswitch_data_0

    .line 458
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    .line 459
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;->unknownFieldData:Ljava/util/List;

    .line 462
    :cond_1
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 464
    :sswitch_0
    return-object p0

    .line 469
    :sswitch_1
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;->clientLaunch:Ljava/lang/Long;

    goto :goto_0

    .line 473
    :sswitch_2
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;->serverCreateRoomStart:Ljava/lang/Long;

    goto :goto_0

    .line 477
    :sswitch_3
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;->serverCreateRoomEnd:Ljava/lang/Long;

    goto :goto_0

    .line 481
    :sswitch_4
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;->serverCreateRedirectEnd:Ljava/lang/Long;

    goto :goto_0

    .line 454
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lepk;)Lepr;
    .locals 1

    .prologue
    .line 391
    invoke-virtual {p0, p1}, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;->mergeFrom(Lepk;)Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 408
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;->clientLaunch:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 409
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;->clientLaunch:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 411
    :cond_0
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;->serverCreateRoomStart:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 412
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;->serverCreateRoomStart:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 414
    :cond_1
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;->serverCreateRoomEnd:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 415
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;->serverCreateRoomEnd:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 417
    :cond_2
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;->serverCreateRedirectEnd:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 418
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;->serverCreateRedirectEnd:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 420
    :cond_3
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$LatencyMarks;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 422
    return-void
.end method
