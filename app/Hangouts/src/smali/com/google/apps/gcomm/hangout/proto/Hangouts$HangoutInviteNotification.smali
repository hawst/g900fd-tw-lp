.class public final Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;
.super Lepn;
.source "PG"


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;


# instance fields
.field public command:Ljava/lang/Integer;

.field public context:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;

.field public dismissReason:Ljava/lang/Integer;

.field public hangoutType:Ljava/lang/Integer;

.field public invitationId:Ljava/lang/Long;

.field public notificationType:Ljava/lang/Integer;

.field public status:Ljava/lang/Integer;

.field public userProvidedMessage:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1656
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;

    sput-object v0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->EMPTY_ARRAY:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1657
    invoke-direct {p0}, Lepn;-><init>()V

    .line 1694
    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->context:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;

    .line 1697
    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->status:Ljava/lang/Integer;

    .line 1700
    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->command:Ljava/lang/Integer;

    .line 1703
    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->notificationType:Ljava/lang/Integer;

    .line 1706
    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->hangoutType:Ljava/lang/Integer;

    .line 1709
    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->dismissReason:Ljava/lang/Integer;

    .line 1657
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 1746
    const/4 v0, 0x0

    .line 1747
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->context:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;

    if-eqz v1, :cond_0

    .line 1748
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->context:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;

    .line 1749
    invoke-static {v0, v1}, Lepl;->d(ILepr;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1751
    :cond_0
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->status:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 1752
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->status:Ljava/lang/Integer;

    .line 1753
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1755
    :cond_1
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->command:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 1756
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->command:Ljava/lang/Integer;

    .line 1757
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1759
    :cond_2
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->notificationType:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 1760
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->notificationType:Ljava/lang/Integer;

    .line 1761
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1763
    :cond_3
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->hangoutType:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 1764
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->hangoutType:Ljava/lang/Integer;

    .line 1765
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1767
    :cond_4
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->dismissReason:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 1768
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->dismissReason:Ljava/lang/Integer;

    .line 1769
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1771
    :cond_5
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->userProvidedMessage:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 1772
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->userProvidedMessage:Ljava/lang/String;

    .line 1773
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1775
    :cond_6
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->invitationId:Ljava/lang/Long;

    if-eqz v1, :cond_7

    .line 1776
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->invitationId:Ljava/lang/Long;

    .line 1777
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lepl;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1779
    :cond_7
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1780
    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->cachedSize:I

    .line 1781
    return v0
.end method

.method public mergeFrom(Lepk;)Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1789
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    .line 1790
    sparse-switch v0, :sswitch_data_0

    .line 1794
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1795
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->unknownFieldData:Ljava/util/List;

    .line 1798
    :cond_1
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1800
    :sswitch_0
    return-object p0

    .line 1805
    :sswitch_1
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->context:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;

    if-nez v0, :cond_2

    .line 1806
    new-instance v0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;

    invoke-direct {v0}, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;-><init>()V

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->context:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;

    .line 1808
    :cond_2
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->context:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;

    invoke-virtual {p1, v0}, Lepk;->a(Lepr;)V

    goto :goto_0

    .line 1812
    :sswitch_2
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    .line 1813
    if-eqz v0, :cond_3

    if-eq v0, v3, :cond_3

    if-eq v0, v4, :cond_3

    if-ne v0, v5, :cond_4

    .line 1817
    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->status:Ljava/lang/Integer;

    goto :goto_0

    .line 1819
    :cond_4
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->status:Ljava/lang/Integer;

    goto :goto_0

    .line 1824
    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    .line 1825
    if-eqz v0, :cond_5

    if-ne v0, v3, :cond_6

    .line 1827
    :cond_5
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->command:Ljava/lang/Integer;

    goto :goto_0

    .line 1829
    :cond_6
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->command:Ljava/lang/Integer;

    goto :goto_0

    .line 1834
    :sswitch_4
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    .line 1835
    if-eqz v0, :cond_7

    if-eq v0, v3, :cond_7

    if-eq v0, v4, :cond_7

    if-ne v0, v5, :cond_8

    .line 1839
    :cond_7
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->notificationType:Ljava/lang/Integer;

    goto :goto_0

    .line 1841
    :cond_8
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->notificationType:Ljava/lang/Integer;

    goto :goto_0

    .line 1846
    :sswitch_5
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    .line 1847
    if-eqz v0, :cond_9

    if-ne v0, v3, :cond_a

    .line 1849
    :cond_9
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->hangoutType:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 1851
    :cond_a
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->hangoutType:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 1856
    :sswitch_6
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    .line 1857
    if-eqz v0, :cond_b

    if-eq v0, v3, :cond_b

    if-eq v0, v4, :cond_b

    if-eq v0, v5, :cond_b

    const/4 v1, 0x4

    if-ne v0, v1, :cond_c

    .line 1862
    :cond_b
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->dismissReason:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 1864
    :cond_c
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->dismissReason:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 1869
    :sswitch_7
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->userProvidedMessage:Ljava/lang/String;

    goto/16 :goto_0

    .line 1873
    :sswitch_8
    invoke-virtual {p1}, Lepk;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->invitationId:Ljava/lang/Long;

    goto/16 :goto_0

    .line 1790
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x28 -> :sswitch_4
        0x30 -> :sswitch_5
        0x38 -> :sswitch_6
        0x42 -> :sswitch_7
        0x48 -> :sswitch_8
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lepk;)Lepr;
    .locals 1

    .prologue
    .line 1653
    invoke-virtual {p0, p1}, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->mergeFrom(Lepk;)Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lepl;)V
    .locals 3

    .prologue
    .line 1716
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->context:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;

    if-eqz v0, :cond_0

    .line 1717
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->context:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;

    invoke-virtual {p1, v0, v1}, Lepl;->b(ILepr;)V

    .line 1719
    :cond_0
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->status:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1720
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->status:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1722
    :cond_1
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->command:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 1723
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->command:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1725
    :cond_2
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->notificationType:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1726
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->notificationType:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1728
    :cond_3
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->hangoutType:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 1729
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->hangoutType:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1731
    :cond_4
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->dismissReason:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 1732
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->dismissReason:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 1734
    :cond_5
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->userProvidedMessage:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1735
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->userProvidedMessage:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 1737
    :cond_6
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->invitationId:Ljava/lang/Long;

    if-eqz v0, :cond_7

    .line 1738
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->invitationId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lepl;->b(IJ)V

    .line 1740
    :cond_7
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 1742
    return-void
.end method
