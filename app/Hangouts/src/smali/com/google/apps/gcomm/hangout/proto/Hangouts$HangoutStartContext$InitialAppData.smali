.class public final Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;
.super Lepn;
.source "PG"


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;


# instance fields
.field public appId:Ljava/lang/String;

.field public startData:Ljava/lang/String;

.field public startType:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 695
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;

    sput-object v0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;->EMPTY_ARRAY:[Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 696
    invoke-direct {p0}, Lepn;-><init>()V

    .line 709
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;->startType:Ljava/lang/Integer;

    .line 696
    return-void
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 729
    const/4 v0, 0x0

    .line 730
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;->appId:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 731
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;->appId:Ljava/lang/String;

    .line 732
    invoke-static {v0, v1}, Lepl;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 734
    :cond_0
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;->startData:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 735
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;->startData:Ljava/lang/String;

    .line 736
    invoke-static {v1, v2}, Lepl;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 738
    :cond_1
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;->startType:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 739
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;->startType:Ljava/lang/Integer;

    .line 740
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lepl;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 742
    :cond_2
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lept;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 743
    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;->cachedSize:I

    .line 744
    return v0
.end method

.method public mergeFrom(Lepk;)Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 752
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lepk;->a()I

    move-result v0

    .line 753
    sparse-switch v0, :sswitch_data_0

    .line 757
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    .line 758
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;->unknownFieldData:Ljava/util/List;

    .line 761
    :cond_1
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lept;->a(Ljava/util/List;Lepk;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 763
    :sswitch_0
    return-object p0

    .line 768
    :sswitch_1
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;->appId:Ljava/lang/String;

    goto :goto_0

    .line 772
    :sswitch_2
    invoke-virtual {p1}, Lepk;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;->startData:Ljava/lang/String;

    goto :goto_0

    .line 776
    :sswitch_3
    invoke-virtual {p1}, Lepk;->f()I

    move-result v0

    .line 777
    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 780
    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;->startType:Ljava/lang/Integer;

    goto :goto_0

    .line 782
    :cond_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;->startType:Ljava/lang/Integer;

    goto :goto_0

    .line 753
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lepk;)Lepr;
    .locals 1

    .prologue
    .line 692
    invoke-virtual {p0, p1}, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;->mergeFrom(Lepk;)Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lepl;)V
    .locals 2

    .prologue
    .line 714
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;->appId:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 715
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;->appId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 717
    :cond_0
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;->startData:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 718
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;->startData:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lepl;->a(ILjava/lang/String;)V

    .line 720
    :cond_1
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;->startType:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 721
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;->startType:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lepl;->a(II)V

    .line 723
    :cond_2
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$InitialAppData;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lept;->a(Ljava/util/List;Lepl;)V

    .line 725
    return-void
.end method
