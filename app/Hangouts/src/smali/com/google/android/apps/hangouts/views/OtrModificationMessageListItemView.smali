.class public Lcom/google/android/apps/hangouts/views/OtrModificationMessageListItemView;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Lcdg;


# static fields
.field private static c:[[I


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:J


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 36
    new-array v0, v5, [[I

    new-array v1, v6, [I

    sget v2, Lh;->my:I

    aput v2, v1, v3

    sget v2, Lh;->mA:I

    aput v2, v1, v4

    sget v2, Lh;->mz:I

    aput v2, v1, v5

    aput-object v1, v0, v3

    new-array v1, v6, [I

    sget v2, Lh;->mB:I

    aput v2, v1, v3

    sget v2, Lh;->mD:I

    aput v2, v1, v4

    sget v2, Lh;->mC:I

    aput v2, v1, v5

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/hangouts/views/OtrModificationMessageListItemView;->c:[[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/hangouts/views/OtrModificationMessageListItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/google/android/apps/hangouts/views/OtrModificationMessageListItemView;->b:J

    return-wide v0
.end method

.method public a(IILjava/lang/String;Z)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 48
    if-ne p1, v0, :cond_1

    move v3, v1

    .line 49
    :goto_0
    const/4 v2, 0x3

    if-ne p2, v2, :cond_2

    move v2, v0

    .line 51
    :goto_1
    if-nez v2, :cond_0

    if-eqz p4, :cond_4

    .line 52
    :cond_0
    sget-object v4, Lcom/google/android/apps/hangouts/views/OtrModificationMessageListItemView;->c:[[I

    aget-object v3, v4, v3

    if-eqz v2, :cond_3

    :goto_2
    aget v0, v3, v0

    .line 53
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/OtrModificationMessageListItemView;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 58
    :goto_3
    return-void

    :cond_1
    move v3, v0

    .line 48
    goto :goto_0

    :cond_2
    move v2, v1

    .line 49
    goto :goto_1

    :cond_3
    move v0, v1

    .line 52
    goto :goto_2

    .line 55
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/OtrModificationMessageListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 56
    iget-object v4, p0, Lcom/google/android/apps/hangouts/views/OtrModificationMessageListItemView;->a:Landroid/widget/TextView;

    sget-object v5, Lcom/google/android/apps/hangouts/views/OtrModificationMessageListItemView;->c:[[I

    aget-object v3, v5, v3

    const/4 v5, 0x2

    aget v3, v3, v5

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p3, v0, v1

    invoke-virtual {v2, v3, v0}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method

.method public a(J)V
    .locals 0

    .prologue
    .line 61
    iput-wide p1, p0, Lcom/google/android/apps/hangouts/views/OtrModificationMessageListItemView;->b:J

    .line 62
    return-void
.end method

.method public b()Landroid/view/View;
    .locals 0

    .prologue
    .line 78
    return-object p0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OtrModificationMessageListItemView;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/OtrModificationMessageListItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 69
    return-void
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 33
    sget v0, Lg;->hm:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/OtrModificationMessageListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/OtrModificationMessageListItemView;->a:Landroid/widget/TextView;

    .line 34
    return-void
.end method
