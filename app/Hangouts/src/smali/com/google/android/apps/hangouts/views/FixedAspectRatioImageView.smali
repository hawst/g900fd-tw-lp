.class public Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;
.super Landroid/widget/ImageView;
.source "PG"


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 29
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    if-eqz p2, :cond_0

    .line 31
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    .line 35
    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 36
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->a:I

    .line 37
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->b:I

    .line 38
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 40
    :cond_0
    return-void

    .line 31
    nop

    :array_0
    .array-data 4
        0x101011f
        0x1010120
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    return-void
.end method


# virtual methods
.method public a(II)V
    .locals 0

    .prologue
    .line 49
    iput p1, p0, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->c:I

    .line 50
    iput p2, p0, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->d:I

    .line 51
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->requestLayout()V

    .line 52
    return-void
.end method

.method protected onMeasure(II)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/high16 v6, -0x80000000

    const/high16 v5, 0x40000000    # 2.0f

    .line 95
    iget v0, p0, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->c:I

    if-eqz v0, :cond_b

    iget v0, p0, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->d:I

    if-eqz v0, :cond_b

    .line 96
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 97
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 98
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    .line 99
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 100
    if-ne v3, v5, :cond_1

    if-ne v4, v5, :cond_1

    .line 156
    :cond_0
    :goto_0
    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->setMeasuredDimension(II)V

    .line 157
    return-void

    .line 103
    :cond_1
    if-ne v3, v5, :cond_3

    .line 105
    iget v0, p0, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->d:I

    mul-int/2addr v0, v1

    iget v3, p0, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->c:I

    div-int/2addr v0, v3

    .line 107
    if-ne v4, v6, :cond_2

    if-gt v0, v2, :cond_0

    :cond_2
    move v2, v0

    goto :goto_0

    .line 110
    :cond_3
    if-ne v4, v5, :cond_4

    .line 116
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x12

    if-ge v0, v4, :cond_a

    .line 118
    iget v0, p0, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->a:I

    if-lez v0, :cond_9

    .line 119
    iget v0, p0, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->a:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 122
    :goto_1
    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 125
    :goto_2
    iget v2, p0, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->c:I

    mul-int/2addr v2, v0

    iget v4, p0, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->d:I

    div-int/2addr v2, v4

    .line 127
    if-ne v3, v6, :cond_8

    if-le v2, v1, :cond_8

    .line 131
    :goto_3
    iget v2, p0, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->a:I

    if-le v1, v2, :cond_2

    .line 132
    iget v1, p0, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->a:I

    move v2, v0

    goto :goto_0

    .line 136
    :cond_4
    iget v0, p0, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->a:I

    if-eqz v0, :cond_7

    iget v0, p0, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->a:I

    if-le v1, v0, :cond_7

    .line 137
    iget v1, p0, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->a:I

    move v0, v1

    .line 144
    :goto_4
    if-eqz v4, :cond_6

    .line 147
    :goto_5
    iget v1, p0, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->c:I

    iget v3, p0, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->d:I

    if-le v1, v3, :cond_5

    .line 149
    iget v1, p0, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->d:I

    mul-int/2addr v1, v0

    iget v2, p0, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->c:I

    div-int v2, v1, v2

    move v1, v0

    goto :goto_0

    .line 152
    :cond_5
    iget v0, p0, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->c:I

    mul-int/2addr v0, v2

    iget v1, p0, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->d:I

    div-int v1, v0, v1

    goto :goto_0

    :cond_6
    move v2, v0

    goto :goto_5

    :cond_7
    move v0, v1

    goto :goto_4

    :cond_8
    move v1, v2

    goto :goto_3

    :cond_9
    move v0, v1

    goto :goto_1

    :cond_a
    move v0, v2

    goto :goto_2

    :cond_b
    move v1, v2

    goto :goto_0
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 69
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 72
    if-eqz p1, :cond_1

    .line 73
    iget v0, p0, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->c:I

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->d:I

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 74
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->a(II)V

    .line 77
    :cond_1
    return-void
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 81
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 83
    if-eqz p1, :cond_1

    .line 84
    iget v0, p0, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->c:I

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->d:I

    .line 85
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 86
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->a(II)V

    .line 89
    :cond_1
    return-void
.end method

.method public setMaxHeight(I)V
    .locals 0

    .prologue
    .line 57
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setMaxHeight(I)V

    .line 58
    iput p1, p0, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->b:I

    .line 59
    return-void
.end method

.method public setMaxWidth(I)V
    .locals 0

    .prologue
    .line 63
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setMaxHeight(I)V

    .line 64
    iput p1, p0, Lcom/google/android/apps/hangouts/views/FixedAspectRatioImageView;->a:I

    .line 65
    return-void
.end method
