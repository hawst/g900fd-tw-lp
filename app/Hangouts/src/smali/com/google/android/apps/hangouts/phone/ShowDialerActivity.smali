.class public Lcom/google/android/apps/hangouts/phone/ShowDialerActivity;
.super Landroid/app/Activity;
.source "PG"


# instance fields
.field public a:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onResume()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 19
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 21
    new-instance v0, Lbbv;

    invoke-direct {v0, p0}, Lbbv;-><init>(Lcom/google/android/apps/hangouts/phone/ShowDialerActivity;)V

    invoke-static {p0}, Ldj;->a(Landroid/content/Context;)Ldj;

    move-result-object v2

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "com.google.android.apps.hangouts.phone.block_external_interruption"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v3}, Ldj;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.google.android.apps.hangouts.phone.notify_external_interruption"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ldj;->b(Landroid/content/Intent;)V

    invoke-virtual {v2, v0}, Ldj;->a(Landroid/content/BroadcastReceiver;)V

    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/ShowDialerActivity;->a:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 22
    const/4 v0, 0x0

    invoke-static {v0}, Lbbl;->b(Lyj;)Landroid/content/Intent;

    move-result-object v0

    .line 23
    const-string v2, "com.google.android.apps.hangouts.phone.recentcalls"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 24
    const-string v2, "use_dialer_activity"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 25
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/ShowDialerActivity;->startActivity(Landroid/content/Intent;)V

    .line 27
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/ShowDialerActivity;->finish()V

    .line 28
    return-void

    :cond_1
    move v0, v1

    .line 21
    goto :goto_0
.end method
