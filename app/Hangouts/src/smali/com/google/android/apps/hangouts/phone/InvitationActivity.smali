.class public Lcom/google/android/apps/hangouts/phone/InvitationActivity;
.super Lakn;
.source "PG"

# interfaces
.implements Lahm;
.implements Lald;


# instance fields
.field private r:Lyj;

.field private s:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lakn;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/InvitationActivity;->a(Ljava/lang/Runnable;)V

    .line 94
    return-void
.end method

.method public a(Lbkn;)V
    .locals 1

    .prologue
    .line 106
    const-string v0, "Should not get called"

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    .line 107
    return-void
.end method

.method public a(Lccp;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 100
    const-string v0, "Should not get called"

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    .line 101
    return-void
.end method

.method public a(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/InvitationActivity;->finish()V

    .line 86
    if-eqz p1, :cond_0

    .line 87
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 89
    :cond_0
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/InvitationActivity;->finish()V

    .line 81
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 117
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 67
    new-instance v0, Lahc;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/InvitationActivity;->r:Lyj;

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lahc;-><init>(Ljava/lang/String;Lyj;I)V

    .line 69
    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/InvitationActivity;->r:Lyj;

    iget v2, v0, Lahc;->d:I

    invoke-static {v1, p1, v2}, Lbbl;->a(Lyj;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    .line 71
    const-string v2, "conversation_parameters"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 72
    const-string v0, "conversation_opened_impression"

    const/16 v2, 0x664

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 74
    invoke-virtual {p0, v1}, Lcom/google/android/apps/hangouts/phone/InvitationActivity;->startActivity(Landroid/content/Intent;)V

    .line 75
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/InvitationActivity;->finish()V

    .line 76
    return-void
.end method

.method public c_(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 112
    return-void
.end method

.method protected k()Lyj;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/InvitationActivity;->r:Lyj;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 30
    invoke-super {p0, p1}, Lakn;->onCreate(Landroid/os/Bundle;)V

    .line 32
    sget v0, Lf;->fG:I

    sget v1, Lg;->t:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/hangouts/phone/InvitationActivity;->a(II)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/InvitationActivity;->setContentView(Landroid/view/View;)V

    .line 35
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/InvitationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 36
    const-string v0, "account_name"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 37
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/InvitationActivity;->r:Lyj;

    .line 38
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/InvitationActivity;->r:Lyj;

    invoke-static {v0}, Lbkb;->e(Lyj;)V

    .line 40
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/InvitationActivity;->e()Lae;

    move-result-object v0

    .line 41
    sget v2, Lg;->dL:I

    invoke-virtual {v0, v2}, Lae;->a(I)Lt;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/InvitationActivity;->s:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    .line 43
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/InvitationActivity;->s:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    invoke-virtual {v0, p0, p0}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->setHostInterface(Lald;Lahm;)V

    .line 45
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/InvitationActivity;->s:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->initialize(Landroid/os/Bundle;)V

    .line 46
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 50
    invoke-super {p0, p1}, Lakn;->onNewIntent(Landroid/content/Intent;)V

    .line 51
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/InvitationActivity;->s:Lcom/google/android/apps/hangouts/fragments/InvitationFragment;

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->initialize(Landroid/os/Bundle;)V

    .line 52
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 56
    invoke-super {p0}, Lakn;->onPause()V

    .line 57
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/InvitationActivity;->l()V

    .line 58
    return-void
.end method
