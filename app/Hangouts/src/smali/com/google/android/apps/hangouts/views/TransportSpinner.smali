.class public Lcom/google/android/apps/hangouts/views/TransportSpinner;
.super Landroid/widget/Spinner;
.source "PG"


# instance fields
.field private final a:Landroid/widget/ArrayAdapter;

.field private b:Z

.field private c:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

.field private d:[Lajk;

.field private e:Z

.field private f:Landroid/view/View;

.field private g:Lyj;

.field private h:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lbzl;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    new-instance v0, Lcel;

    invoke-direct {v0, p0}, Lcel;-><init>(Lcom/google/android/apps/hangouts/views/TransportSpinner;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->i:Lbzl;

    .line 88
    new-instance v0, Lcen;

    sget v1, Lf;->eH:I

    invoke-direct {v0, p0, p1, v1}, Lcen;-><init>(Lcom/google/android/apps/hangouts/views/TransportSpinner;Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->a:Landroid/widget/ArrayAdapter;

    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->b:Z

    .line 90
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->d:[Lajk;

    .line 91
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->h:Ljava/util/HashMap;

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->a:Landroid/widget/ArrayAdapter;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 93
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/views/TransportSpinner;)Landroid/view/View;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->f:Landroid/view/View;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/views/TransportSpinner;Landroid/view/View;)Landroid/view/View;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->f:Landroid/view/View;

    return-object p1
.end method

.method private a()V
    .locals 1

    .prologue
    .line 274
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->b:Z

    .line 275
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->d:[Lajk;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->a([Lajk;)V

    .line 276
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/views/TransportSpinner;Ljava/lang/String;Landroid/widget/ImageView;)V
    .locals 2

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lbzh;->b()Lbzh;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->g:Lyj;

    invoke-virtual {v0, p1, v1}, Lbzh;->a(Ljava/lang/String;Lyj;)Lbdn;

    move-result-object v0

    :cond_0
    if-eqz v0, :cond_1

    iget-boolean v1, v0, Lbdn;->b:Z

    if-eqz v1, :cond_1

    iget-boolean v0, v0, Lbdn;->a:Z

    if-nez v0, :cond_2

    :cond_1
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->bU:I

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    return-void

    :cond_2
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->bV:I

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/views/TransportSpinner;)Lajk;
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->c()Lajk;

    move-result-object v0

    return-object v0
.end method

.method private b()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 297
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->d:[Lajk;

    if-eqz v0, :cond_e

    .line 300
    iget-object v6, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->d:[Lajk;

    array-length v7, v6

    move v4, v5

    move v0, v5

    move v1, v5

    :goto_0
    if-ge v4, v7, :cond_3

    aget-object v8, v6, v4

    .line 301
    iget-object v9, v8, Lajk;->c:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_2

    move v0, v3

    .line 306
    :cond_0
    :goto_1
    if-eqz v0, :cond_1

    if-nez v1, :cond_3

    .line 307
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 303
    :cond_2
    invoke-virtual {v8}, Lajk;->a()I

    move-result v8

    invoke-static {v8}, Lf;->d(I)Z

    move-result v8

    if-eqz v8, :cond_0

    move v1, v3

    .line 304
    goto :goto_1

    .line 311
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 313
    if-eqz v1, :cond_d

    if-eqz v0, :cond_d

    .line 314
    sget v0, Lh;->nu:I

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 318
    :goto_2
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->c()Lajk;

    move-result-object v1

    .line 321
    if-eqz v1, :cond_4

    iget-object v6, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->g:Lyj;

    if-eqz v6, :cond_4

    .line 322
    iget-object v6, v1, Lajk;->c:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 323
    sget v1, Lh;->nt:I

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 351
    :cond_4
    :goto_3
    if-eqz v2, :cond_c

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_c

    move-object v0, v2

    .line 361
    :cond_5
    :goto_4
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 362
    return-void

    .line 325
    :cond_6
    iget-object v6, v1, Lajk;->b:Lbdh;

    if-eqz v6, :cond_4

    .line 326
    invoke-static {}, Lbzh;->b()Lbzh;

    move-result-object v6

    iget-object v7, v1, Lajk;->b:Lbdh;

    .line 327
    invoke-virtual {v7}, Lbdh;->a()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->g:Lyj;

    .line 326
    invoke-virtual {v6, v7, v8}, Lbzh;->a(Ljava/lang/String;Lyj;)Lbdn;

    move-result-object v6

    .line 329
    iget-object v7, v1, Lajk;->g:Lyv;

    if-eqz v7, :cond_7

    .line 330
    iget-object v2, v1, Lajk;->g:Lyv;

    iget-object v2, v2, Lyv;->o:Ljava/lang/String;

    .line 332
    :cond_7
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 333
    iget-object v2, v1, Lajk;->d:Ljava/lang/String;

    .line 335
    :cond_8
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 336
    sget v1, Lh;->nq:I

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 339
    :cond_9
    if-eqz v6, :cond_a

    iget-boolean v1, v6, Lbdn;->b:Z

    if-eqz v1, :cond_a

    iget-boolean v1, v6, Lbdn;->a:Z

    if-nez v1, :cond_b

    .line 340
    :cond_a
    sget v1, Lh;->nv:I

    new-array v6, v3, [Ljava/lang/Object;

    aput-object v2, v6, v5

    invoke-virtual {v4, v1, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 344
    :cond_b
    sget v1, Lh;->ns:I

    new-array v6, v3, [Ljava/lang/Object;

    aput-object v2, v6, v5

    invoke-virtual {v4, v1, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 353
    :cond_c
    if-eqz v2, :cond_5

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 354
    sget v1, Lh;->np:I

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v2, v6, v5

    aput-object v0, v6, v3

    invoke-virtual {v4, v1, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :cond_d
    move-object v0, v2

    goto/16 :goto_2

    :cond_e
    move-object v0, v2

    goto :goto_4
.end method

.method private c()Lajk;
    .locals 3

    .prologue
    .line 369
    const/4 v0, 0x0

    .line 370
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->d:[Lajk;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->d:[Lajk;

    array-length v1, v1

    if-lez v1, :cond_0

    .line 371
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->getSelectedItemPosition()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->a:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 372
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lajk;

    .line 374
    :cond_0
    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/apps/hangouts/views/TransportSpinner;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->h:Ljava/util/HashMap;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/apps/hangouts/views/TransportSpinner;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->b()V

    return-void
.end method

.method public static synthetic e(Lcom/google/android/apps/hangouts/views/TransportSpinner;)[Lajk;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->d:[Lajk;

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/apps/hangouts/views/TransportSpinner;)Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->e:Z

    return v0
.end method

.method public static synthetic g(Lcom/google/android/apps/hangouts/views/TransportSpinner;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->a()V

    return-void
.end method


# virtual methods
.method public a(Lajk;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 145
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->a:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v2

    move v1, v0

    .line 146
    :goto_0
    if-ge v1, v2, :cond_2

    .line 147
    invoke-virtual {p0, v1}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v3

    .line 148
    instance-of v4, v3, Lajk;

    if-eqz v4, :cond_1

    if-ne v3, p1, :cond_1

    .line 150
    invoke-virtual {p0, v1}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->setSelection(I)V

    .line 161
    :cond_0
    :goto_1
    return-void

    .line 146
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 156
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->b:Z

    if-nez v1, :cond_0

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->d:[Lajk;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->d:[Lajk;

    .line 157
    :goto_2
    array-length v2, v1

    if-ge v0, v2, :cond_6

    aget-object v2, v1, v0

    if-nez p1, :cond_3

    if-eqz v2, :cond_4

    :cond_3
    if-eqz p1, :cond_5

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    :goto_3
    if-ltz v0, :cond_0

    .line 158
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->a()V

    .line 159
    invoke-virtual {p0, p1}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->a(Lajk;)V

    goto :goto_1

    .line 157
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_6
    const/4 v0, -0x1

    goto :goto_3
.end method

.method public a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->c:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 125
    return-void
.end method

.method public a(Lyj;)V
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->g:Lyj;

    .line 117
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 133
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->e:Z

    if-eq v0, p1, :cond_0

    .line 134
    iput-boolean p1, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->e:Z

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->f:Landroid/view/View;

    sget v1, Lg;->gU:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 138
    iget-boolean v1, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->e:Z

    if-eqz v1, :cond_1

    sget v1, Lh;->gP:I

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 141
    :cond_0
    return-void

    .line 138
    :cond_1
    sget v1, Lh;->lj:I

    goto :goto_0
.end method

.method public a([Lajk;)V
    .locals 12

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 185
    iput-object p1, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->d:[Lajk;

    .line 186
    invoke-static {}, Lbtf;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 187
    iput-object v4, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->d:[Lajk;

    .line 189
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->a:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    .line 190
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 191
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->c:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    if-eqz v0, :cond_5

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->c:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a()Lyj;

    move-result-object v0

    .line 193
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lyj;->v()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    .line 194
    :goto_0
    if-nez v0, :cond_5

    .line 198
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->d:[Lajk;

    if-eqz v0, :cond_5

    iget-object v5, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->d:[Lajk;

    array-length v6, v5

    move v1, v3

    move v0, v3

    :goto_1
    if-ge v1, v6, :cond_3

    aget-object v7, v5, v1

    iget-object v7, v7, Lajk;->c:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    add-int/lit8 v0, v0, 0x1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move v0, v3

    .line 193
    goto :goto_0

    .line 198
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->d:[Lajk;

    array-length v1, v1

    if-eq v0, v1, :cond_5

    new-array v6, v0, [Lajk;

    iget-object v7, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->d:[Lajk;

    array-length v8, v7

    move v5, v3

    move v1, v3

    :goto_2
    if-ge v5, v8, :cond_4

    aget-object v9, v7, v5

    iget-object v0, v9, Lajk;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_11

    add-int/lit8 v0, v1, 0x1

    aput-object v9, v6, v1

    :goto_3
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move v1, v0

    goto :goto_2

    :cond_4
    iput-object v6, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->d:[Lajk;

    .line 202
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->d:[Lajk;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->d:[Lajk;

    array-length v0, v0

    if-lez v0, :cond_e

    .line 203
    invoke-virtual {p0, v3}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->setVisibility(I)V

    .line 204
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->d:[Lajk;

    array-length v0, v0

    if-le v0, v2, :cond_6

    move v0, v2

    :goto_4
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->setEnabled(Z)V

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->d:[Lajk;

    .line 207
    iget-object v6, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->d:[Lajk;

    array-length v7, v6

    move v5, v3

    move v1, v3

    :goto_5
    if-ge v5, v7, :cond_8

    aget-object v0, v6, v5

    .line 208
    iget-object v8, v0, Lajk;->c:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_10

    .line 209
    iget-object v0, v0, Lajk;->g:Lyv;

    if-eqz v0, :cond_7

    move v0, v2

    :goto_6
    or-int/2addr v0, v1

    .line 207
    :goto_7
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move v1, v0

    goto :goto_5

    :cond_6
    move v0, v3

    .line 204
    goto :goto_4

    :cond_7
    move v0, v3

    .line 209
    goto :goto_6

    .line 217
    :cond_8
    iget-object v9, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->d:[Lajk;

    array-length v10, v9

    move v8, v3

    move-object v0, v4

    move v5, v3

    :goto_8
    if-ge v8, v10, :cond_c

    aget-object v4, v9, v8

    .line 218
    iget-boolean v6, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->b:Z

    if-nez v6, :cond_b

    .line 219
    iget-object v6, v4, Lajk;->c:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_9

    move v6, v2

    .line 220
    :goto_9
    iget-object v7, v4, Lajk;->g:Lyv;

    if-eqz v7, :cond_a

    move v7, v2

    .line 221
    :goto_a
    iget-boolean v11, v4, Lajk;->j:Z

    .line 222
    if-nez v11, :cond_b

    if-eqz v1, :cond_b

    if-eqz v6, :cond_b

    if-nez v7, :cond_b

    move v4, v2

    .line 217
    :goto_b
    add-int/lit8 v5, v8, 0x1

    move v8, v5

    move v5, v4

    goto :goto_8

    :cond_9
    move v6, v3

    .line 219
    goto :goto_9

    :cond_a
    move v7, v3

    .line 220
    goto :goto_a

    .line 228
    :cond_b
    iget-object v6, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->a:Landroid/widget/ArrayAdapter;

    invoke-virtual {v6, v4, v3}, Landroid/widget/ArrayAdapter;->insert(Ljava/lang/Object;I)V

    .line 229
    iget-boolean v6, v4, Lajk;->j:Z

    if-eqz v6, :cond_f

    move-object v0, v4

    move v4, v5

    .line 230
    goto :goto_b

    .line 233
    :cond_c
    if-eqz v5, :cond_d

    .line 234
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->a:Landroid/widget/ArrayAdapter;

    new-instance v2, Lcem;

    invoke-direct {v2, p0}, Lcem;-><init>(Lcom/google/android/apps/hangouts/views/TransportSpinner;)V

    invoke-virtual {v1, v2, v3}, Landroid/widget/ArrayAdapter;->insert(Ljava/lang/Object;I)V

    .line 237
    :cond_d
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->a(Lajk;)V

    .line 238
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->b()V

    .line 242
    :goto_c
    return-void

    .line 240
    :cond_e
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->setVisibility(I)V

    goto :goto_c

    :cond_f
    move v4, v5

    goto :goto_b

    :cond_10
    move v0, v1

    goto :goto_7

    :cond_11
    move v0, v1

    goto/16 :goto_3
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 97
    invoke-super {p0}, Landroid/widget/Spinner;->onAttachedToWindow()V

    .line 98
    invoke-static {}, Lbzh;->b()Lbzh;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->i:Lbzl;

    invoke-virtual {v0, v1}, Lbzh;->a(Lbzl;)V

    .line 99
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 103
    invoke-super {p0}, Landroid/widget/Spinner;->onDetachedFromWindow()V

    .line 104
    invoke-static {}, Lbzh;->b()Lbzh;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->i:Lbzl;

    invoke-virtual {v0, v1}, Lbzh;->b(Lbzl;)V

    .line 105
    return-void
.end method

.method public performClick()Z
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->c:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/TransportSpinner;->c:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->U()V

    .line 112
    :cond_0
    invoke-super {p0}, Landroid/widget/Spinner;->performClick()Z

    move-result v0

    return v0
.end method

.method public setSelection(I)V
    .locals 1

    .prologue
    .line 169
    invoke-virtual {p0, p1}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    .line 170
    instance-of v0, v0, Lajk;

    if-eqz v0, :cond_0

    .line 171
    invoke-super {p0, p1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 175
    :cond_0
    return-void
.end method
