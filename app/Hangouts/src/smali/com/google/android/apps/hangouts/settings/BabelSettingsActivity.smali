.class public Lcom/google/android/apps/hangouts/settings/BabelSettingsActivity;
.super Landroid/preference/PreferenceActivity;
.source "PG"

# interfaces
.implements Lbte;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Landroid/preference/PreferenceActivity$Header;

.field private c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 40
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-class v2, Lbtd;

    .line 41
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Lbud;

    .line 42
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-class v2, Lbuo;

    .line 43
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-class v2, Lbur;

    .line 44
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-class v2, Lcom/google/android/apps/hangouts/settings/SmsMmsSettingsFragment;

    .line 45
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 40
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivity;->a:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 167
    invoke-static {}, Lbkb;->j()Lyj;

    move-result-object v0

    .line 168
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivity;->c:Ljava/lang/String;

    .line 169
    return-void

    .line 168
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 137
    if-eqz v0, :cond_0

    .line 138
    sget v1, Lh;->G:I

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 142
    :goto_0
    return-void

    .line 140
    :cond_0
    const-string v0, "Babel"

    const-string v1, "ActionBar is null in clearAccountName"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 127
    if-eqz v0, :cond_0

    .line 128
    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 132
    :goto_0
    return-void

    .line 130
    :cond_0
    const-string v0, "Babel"

    const-string v1, "ActionBar is null in setAccountName"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected isValidFragment(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 121
    sget-object v0, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivity;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public onAttachFragment(Landroid/app/Fragment;)V
    .locals 1

    .prologue
    .line 146
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onAttachFragment(Landroid/app/Fragment;)V

    .line 147
    instance-of v0, p1, Lbtd;

    if-eqz v0, :cond_0

    .line 148
    check-cast p1, Lbtd;

    .line 149
    invoke-virtual {p1, p0}, Lbtd;->a(Lbte;)V

    .line 151
    :cond_0
    return-void
.end method

.method public onBuildHeaders(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/preference/PreferenceActivity$Header;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 176
    invoke-static {}, Lbkb;->h()Ljava/util/List;

    move-result-object v2

    .line 177
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    .line 180
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivity;->b:Landroid/preference/PreferenceActivity$Header;

    .line 181
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    .line 182
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 183
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v4

    .line 184
    new-instance v5, Landroid/preference/PreferenceActivity$Header;

    invoke-direct {v5}, Landroid/preference/PreferenceActivity$Header;-><init>()V

    .line 185
    invoke-virtual {v4}, Lyj;->f()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Landroid/preference/PreferenceActivity$Header;->title:Ljava/lang/CharSequence;

    .line 186
    invoke-static {v4}, Lbkb;->l(Lyj;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 187
    sget v4, Lh;->fD:I

    iput v4, v5, Landroid/preference/PreferenceActivity$Header;->summaryRes:I

    .line 194
    :goto_1
    invoke-interface {p1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 197
    iget-object v4, p0, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivity;->c:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivity;->c:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    iput-object v5, p0, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivity;->b:Landroid/preference/PreferenceActivity$Header;

    .line 181
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 189
    :cond_1
    const-class v4, Lbtd;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v5, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    .line 190
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 191
    const-string v6, "account_name"

    invoke-virtual {v4, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    iput-object v4, v5, Landroid/preference/PreferenceActivity$Header;->fragmentArguments:Landroid/os/Bundle;

    goto :goto_1

    .line 203
    :cond_2
    sget v0, Lf;->ig:I

    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivity;->loadHeadersFromResource(ILjava/util/List;)V

    .line 206
    invoke-static {}, Lbzd;->d()Z

    move-result v0

    if-nez v0, :cond_4

    .line 207
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    .line 208
    iget-wide v2, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    sget v4, Lg;->gX:I

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    .line 209
    invoke-interface {p1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 214
    :cond_4
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 58
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 59
    const-string v0, "settings_acl"

    invoke-virtual {v3, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 61
    const-string v0, "settings_goto_rstatus"

    invoke-virtual {v3, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 64
    if-nez v4, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivity;->onIsHidingHeaders()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, ":android:show_fragment"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v2

    :goto_0
    if-nez v0, :cond_1

    .line 65
    :cond_0
    const-string v0, "account_name"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 70
    if-eqz v4, :cond_4

    .line 71
    const-class v0, Lbud;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 72
    const-string v4, ":android:no_headers"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 77
    :goto_1
    const-string v4, ":android:show_fragment"

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 79
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 80
    invoke-static {}, Lf;->t()Ljava/util/List;

    move-result-object v0

    .line 81
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_5

    .line 82
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    sget v1, Lh;->lw:I

    .line 83
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 89
    :goto_2
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 90
    const-string v4, "account_name"

    invoke-virtual {v1, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    const-string v0, "show_general"

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 92
    const-string v0, ":android:show_fragment_args"

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 93
    invoke-virtual {p0, v3}, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivity;->setIntent(Landroid/content/Intent;)V

    .line 95
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivity;->b()V

    .line 96
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 98
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 99
    if-eqz v0, :cond_6

    .line 100
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 107
    :goto_3
    if-eqz v5, :cond_2

    .line 108
    const-string v0, "account_name"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 109
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 110
    const-string v0, "account_name"

    invoke-virtual {v2, v0, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    const-class v0, Lbur;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    sget v3, Lh;->kO:I

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivity;->startPreferencePanel(Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/app/Fragment;I)V

    .line 116
    :cond_2
    invoke-virtual {p0, v6}, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivity;->setResult(I)V

    .line 117
    return-void

    :cond_3
    move v0, v6

    .line 64
    goto :goto_0

    .line 74
    :cond_4
    const-class v0, Lbtd;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 85
    :cond_5
    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto :goto_2

    .line 102
    :cond_6
    const-string v0, "Babel"

    const-string v1, "ActionBar is null in onCreate - Home up is not enabled"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_7
    move-object v0, v1

    goto :goto_2
.end method

.method public onGetInitialHeader()Landroid/preference/PreferenceActivity$Header;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivity;->b:Landroid/preference/PreferenceActivity$Header;

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onGetInitialHeader()Landroid/preference/PreferenceActivity$Header;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivity;->b:Landroid/preference/PreferenceActivity$Header;

    goto :goto_0
.end method

.method public onHeaderClick(Landroid/preference/PreferenceActivity$Header;I)V
    .locals 4

    .prologue
    .line 218
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->onHeaderClick(Landroid/preference/PreferenceActivity$Header;I)V

    .line 219
    iget-wide v0, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    sget v2, Lg;->m:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 220
    invoke-static {}, Lbkb;->j()Lyj;

    move-result-object v0

    invoke-static {v0}, Lbbl;->p(Lyj;)Landroid/content/Intent;

    move-result-object v0

    .line 221
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivity;->startActivity(Landroid/content/Intent;)V

    .line 223
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 232
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 238
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 234
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivity;->finish()V

    .line 235
    const/4 v0, 0x1

    goto :goto_0

    .line 232
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 160
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 162
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivity;->b()V

    .line 163
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivity;->invalidateHeaders()V

    .line 164
    return-void
.end method
