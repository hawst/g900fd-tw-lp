.class public Lcom/google/android/apps/hangouts/phone/EsApplication;
.super Landroid/app/Application;
.source "PG"

# interfaces
.implements Lcyl;
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# static fields
.field public static a:I

.field private static final b:Z

.field private static final c:Lcxe;

.field private static volatile d:Landroid/content/Context;

.field private static volatile e:Lcom/google/android/apps/hangouts/phone/EsApplication;

.field private static f:Z

.field private static k:Z

.field private static volatile n:Z

.field private static final o:Ljava/lang/Object;

.field private static volatile p:Z

.field private static final r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private static u:Z

.field private static v:[Landroid/media/MediaPlayer;

.field private static w:Ljava/lang/Object;


# instance fields
.field private g:Ljava/lang/Thread$UncaughtExceptionHandler;

.field private volatile h:Lcyj;

.field private final i:Ljava/lang/Object;

.field private j:Landroid/os/Handler;

.field private l:Z

.field private volatile m:Z

.field private q:Z

.field private volatile s:I

.field private final t:Landroid/telephony/PhoneStateListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 100
    sget-object v0, Lbys;->g:Lcyp;

    sput-boolean v1, Lcom/google/android/apps/hangouts/phone/EsApplication;->b:Z

    .line 118
    sget-object v0, Lcxe;->a:Lcxe;

    sput-object v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->c:Lcxe;

    .line 122
    sput-boolean v1, Lcom/google/android/apps/hangouts/phone/EsApplication;->f:Z

    .line 137
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->o:Ljava/lang/Object;

    .line 257
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->r:Ljava/util/List;

    .line 1066
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->w:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 95
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 126
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/EsApplication;->i:Ljava/lang/Object;

    .line 143
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/phone/EsApplication;->q:Z

    .line 562
    iput v1, p0, Lcom/google/android/apps/hangouts/phone/EsApplication;->s:I

    .line 568
    new-instance v0, Lbac;

    invoke-direct {v0, p0}, Lbac;-><init>(Lcom/google/android/apps/hangouts/phone/EsApplication;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/EsApplication;->t:Landroid/telephony/PhoneStateListener;

    return-void
.end method

.method private static A()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v1, 0x0

    const/4 v8, 0x6

    .line 748
    new-array v2, v8, [I

    fill-array-data v2, :array_0

    .line 749
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v8}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v1

    .line 750
    :goto_0
    if-ge v0, v8, :cond_0

    aget v4, v2, v0

    .line 751
    sget-object v5, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    int-to-long v6, v4

    invoke-virtual {v5, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v4

    long-to-int v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 750
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 753
    :cond_0
    invoke-static {}, Lf;->w()Z

    move-result v0

    .line 754
    if-eqz v0, :cond_1

    .line 755
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 756
    const/4 v0, 0x5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v9, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 757
    const/4 v0, 0x2

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v3, v0, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 759
    :cond_1
    return-object v3

    .line 748
    :array_0
    .array-data 4
        0x1
        0x2
        0x4
        0x8
        0x18
        0x48
    .end array-data
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/phone/EsApplication;I)I
    .locals 0

    .prologue
    .line 95
    iput p1, p0, Lcom/google/android/apps/hangouts/phone/EsApplication;->s:I

    return p1
.end method

.method public static a(Ljava/lang/String;I)I
    .locals 1

    .prologue
    .line 237
    sget-boolean v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->p:Z

    if-nez v0, :cond_0

    .line 238
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->w()V

    .line 240
    :cond_0
    sget-object v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, p0, p1}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static a(Ljava/lang/String;J)J
    .locals 2

    .prologue
    .line 230
    sget-boolean v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->p:Z

    if-nez v0, :cond_0

    .line 231
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->w()V

    .line 233
    :cond_0
    sget-object v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, p0, p1, p2}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static a()Landroid/content/Context;
    .locals 2

    .prologue
    .line 150
    sget-object v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->d:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 151
    const-string v0, "Babel"

    const-string v1, "sContext should not be null"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    :cond_0
    sget-object v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->d:Landroid/content/Context;

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 251
    sget-boolean v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->p:Z

    if-nez v0, :cond_0

    .line 252
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->w()V

    .line 254
    :cond_0
    sget-object v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, p0, p1}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 201
    sput-object p0, Lcom/google/android/apps/hangouts/phone/EsApplication;->d:Landroid/content/Context;

    .line 202
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/phone/EsApplication;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->y()V

    return-void
.end method

.method public static a(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 261
    sget-object v1, Lcom/google/android/apps/hangouts/phone/EsApplication;->r:Ljava/util/List;

    monitor-enter v1

    .line 262
    :try_start_0
    sget-object v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->r:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 263
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1113
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 1114
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " [called on main thread]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-static {v0, v1, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1116
    :cond_0
    return-void
.end method

.method public static synthetic a(Lyj;I)V
    .locals 0

    .prologue
    .line 95
    invoke-static {p0, p1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->b(Lyj;I)V

    return-void
.end method

.method public static a(Lyj;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 780
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->i()Ljava/util/List;

    move-result-object v0

    .line 781
    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 782
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 783
    const-string v0, "Babel"

    const-string v1, "Unrecognized DND choice"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 787
    :goto_0
    return-void

    .line 786
    :cond_0
    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->b(Lyj;I)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Z)Z
    .locals 1

    .prologue
    .line 244
    sget-boolean v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->p:Z

    if-nez v0, :cond_0

    .line 245
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->w()V

    .line 247
    :cond_0
    sget-object v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, p0, p1}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/phone/EsApplication;)Ljava/lang/Thread$UncaughtExceptionHandler;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/EsApplication;->g:Ljava/lang/Thread$UncaughtExceptionHandler;

    return-object v0
.end method

.method private static b(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 852
    invoke-static {}, Lym;->a()V

    .line 854
    if-eqz p0, :cond_1

    const-string v0, "2.3"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 858
    invoke-static {}, Lbzd;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 859
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->dn:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 861
    invoke-static {}, Lbkb;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 862
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    .line 863
    if-eqz v0, :cond_0

    .line 864
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v3

    .line 865
    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lf;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x0

    .line 864
    invoke-virtual {v3, v0, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 866
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 871
    :cond_1
    return-void
.end method

.method private static b(Lyj;I)V
    .locals 4

    .prologue
    .line 790
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->A()Ljava/util/List;

    move-result-object v0

    .line 791
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    .line 792
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    int-to-long v2, v0

    .line 791
    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    .line 793
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 794
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide v0

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;J)V

    .line 795
    return-void
.end method

.method public static b()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 158
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "EsApplication"

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 160
    const-string v1, "first_run"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 161
    if-eqz v1, :cond_0

    .line 162
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "first_run"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 164
    :cond_0
    return v1
.end method

.method public static c(Lyj;)V
    .locals 2

    .prologue
    .line 803
    const-wide/16 v0, -0x1

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;J)V

    .line 804
    return-void
.end method

.method public static d()Lcom/google/android/apps/hangouts/phone/EsApplication;
    .locals 1

    .prologue
    .line 205
    sget-object v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->e:Lcom/google/android/apps/hangouts/phone/EsApplication;

    return-object v0
.end method

.method public static d(I)V
    .locals 5

    .prologue
    .line 1086
    sget-boolean v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->u:Z

    if-eqz v0, :cond_2

    .line 1087
    sget-object v1, Lcom/google/android/apps/hangouts/phone/EsApplication;->w:Ljava/lang/Object;

    monitor-enter v1

    .line 1089
    :try_start_0
    sget-object v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->v:[Landroid/media/MediaPlayer;

    if-nez v0, :cond_0

    .line 1090
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/media/MediaPlayer;

    .line 1091
    sput-object v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->v:[Landroid/media/MediaPlayer;

    const/4 v2, 0x0

    .line 1092
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v3

    sget v4, Lf;->hP:I

    invoke-static {v3, v4}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v3

    aput-object v3, v0, v2

    .line 1093
    sget-object v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->v:[Landroid/media/MediaPlayer;

    const/4 v2, 0x1

    .line 1094
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v3

    sget v4, Lf;->hH:I

    invoke-static {v3, v4}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v3

    aput-object v3, v0, v2

    .line 1095
    sget-object v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->v:[Landroid/media/MediaPlayer;

    const/4 v2, 0x1

    aget-object v0, v0, v2

    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2, v3}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 1096
    sget-object v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->v:[Landroid/media/MediaPlayer;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    const v2, 0x3e99999a    # 0.3f

    const v3, 0x3e99999a    # 0.3f

    invoke-virtual {v0, v2, v3}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 1098
    :cond_0
    sget-object v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->v:[Landroid/media/MediaPlayer;

    aget-object v0, v0, p0

    if-eqz v0, :cond_1

    .line 1099
    sget-object v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->v:[Landroid/media/MediaPlayer;

    aget-object v0, v0, p0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1108
    :cond_1
    :goto_0
    :try_start_1
    monitor-exit v1

    .line 1110
    :cond_2
    return-void

    .line 1101
    :catch_0
    move-exception v0

    .line 1102
    const-string v2, "Babel"

    const-string v3, "MediaPlayer exception"

    invoke-static {v2, v3, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1108
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 1103
    :catch_1
    move-exception v0

    .line 1104
    :try_start_2
    const-string v2, "Babel"

    const-string v3, "MediaPlayer exception"

    invoke-static {v2, v3, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1105
    :catch_2
    move-exception v0

    .line 1106
    const-string v2, "Babel"

    const-string v3, "MediaPlayer exception"

    invoke-static {v2, v3, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public static d(Z)V
    .locals 3

    .prologue
    .line 1076
    sget-boolean v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->u:Z

    if-eq v0, p0, :cond_0

    .line 1077
    sput-boolean p0, Lcom/google/android/apps/hangouts/phone/EsApplication;->u:Z

    .line 1079
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "EsApplication"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1081
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "debug_noise"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1083
    :cond_0
    return-void
.end method

.method private static e(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 992
    packed-switch p0, :pswitch_data_0

    .line 1000
    const-string v0, "Babel"

    const-string v1, "unsupported promo type!"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 1001
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 994
    :pswitch_0
    const-string v0, "pstn_oobe_promo_shown"

    goto :goto_0

    .line 996
    :pswitch_1
    const-string v0, "transport_spinner_promo_shown"

    goto :goto_0

    .line 998
    :pswitch_2
    const-string v0, "autoswitch_transport_promo_shown"

    goto :goto_0

    .line 992
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static e()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 373
    invoke-static {}, Lbkb;->m()Z

    move-result v0

    .line 374
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 375
    new-instance v3, Landroid/content/ComponentName;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v4

    const-string v5, "com.google.android.apps.hangouts.phone.ShareIntentSmsOnlyActivity"

    invoke-direct {v3, v4, v5}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v3, v0, v1}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 381
    return-void

    .line 375
    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public static synthetic e(Z)Z
    .locals 0

    .prologue
    .line 95
    sput-boolean p0, Lcom/google/android/apps/hangouts/phone/EsApplication;->u:Z

    return p0
.end method

.method public static i()Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 763
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 764
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->A()Ljava/util/List;

    move-result-object v0

    .line 765
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 766
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 767
    invoke-static {v0, v9}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    .line 768
    int-to-long v4, v0

    sget-object v6, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v7, 0x1

    invoke-virtual {v6, v7, v8}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-gez v4, :cond_0

    .line 769
    sget v4, Lf;->hg:I

    new-array v5, v10, [Ljava/lang/Object;

    .line 770
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v9

    .line 769
    invoke-virtual {v1, v4, v0, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 772
    :cond_0
    sget-object v4, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    int-to-long v5, v0

    invoke-virtual {v4, v5, v6}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v4

    long-to-int v0, v4

    .line 773
    sget v4, Lf;->hf:I

    new-array v5, v10, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-virtual {v1, v4, v0, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 776
    :cond_1
    return-object v2
.end method

.method public static q()Z
    .locals 2

    .prologue
    .line 1056
    sget v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->a:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    sget-boolean v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->k:Z

    if-eqz v0, :cond_1

    .line 1058
    :cond_0
    const/4 v0, 0x0

    .line 1061
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static r()Z
    .locals 1

    .prologue
    .line 1072
    sget-boolean v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->u:Z

    return v0
.end method

.method public static synthetic s()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 95
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "babel_"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcww;->b(Landroid/content/ContentResolver;[Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/apps/hangouts/phone/EsApplication;->o:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    sput-boolean v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->p:Z

    sget-object v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->o:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->x()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static synthetic t()Z
    .locals 1

    .prologue
    .line 95
    sget-boolean v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->n:Z

    return v0
.end method

.method public static synthetic u()V
    .locals 3

    .prologue
    .line 95
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->x()V

    sget-object v1, Lcom/google/android/apps/hangouts/phone/EsApplication;->r:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public static synthetic v()V
    .locals 0

    .prologue
    .line 95
    invoke-static {}, Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;->a()V

    return-void
.end method

.method private static w()V
    .locals 2

    .prologue
    .line 217
    :goto_0
    sget-object v1, Lcom/google/android/apps/hangouts/phone/EsApplication;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 218
    :try_start_0
    sget-boolean v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->p:Z

    if-eqz v0, :cond_0

    .line 219
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    .line 222
    :cond_0
    :try_start_1
    sget-object v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->o:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 225
    :goto_1
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method private static x()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 282
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 283
    const-string v1, "babel_asserts"

    invoke-static {v0, v1, v3}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v1

    .line 285
    const-string v2, "babel_expensive_asserts"

    invoke-static {v0, v2, v4}, Lbym;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    .line 288
    invoke-static {v1, v0}, Lcwz;->a(ZZ)V

    .line 290
    invoke-static {}, Lf;->w()Z

    move-result v0

    sput-boolean v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->n:Z

    .line 296
    invoke-static {}, Lbys;->a()V

    .line 300
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/DebugService;->a()V

    .line 303
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "smsmms"

    invoke-virtual {v0, v1, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 306
    const-string v1, "babel_enable_merged_conversations"

    invoke-static {v1, v3}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v1

    .line 310
    const-string v2, "enable_merged_conversations_key_from_gservices"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 311
    if-eq v2, v1, :cond_0

    .line 312
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "enable_merged_conversations_key_from_gservices"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 313
    invoke-static {}, Lbtf;->j()V

    .line 314
    invoke-static {}, Lbtf;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 315
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j()V

    .line 320
    :cond_0
    :goto_0
    return-void

    .line 317
    :cond_1
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->k()V

    goto :goto_0
.end method

.method private y()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 410
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "airplane_mode_on"

    .line 409
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 412
    :cond_0
    if-nez v0, :cond_1

    .line 413
    const-string v0, "wifi"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 414
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v0

    .line 416
    :cond_1
    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/EsApplication;->l:Z

    .line 417
    sget-boolean v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->b:Z

    if-eqz v0, :cond_2

    .line 418
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updated connected to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/google/android/apps/hangouts/phone/EsApplication;->l:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    :cond_2
    return-void
.end method

.method private z()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 543
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Application$ActivityLifecycleCallbacks;

    .line 542
    invoke-static {v0, v1}, Lcyj;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application$ActivityLifecycleCallbacks;

    .line 544
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    goto :goto_0

    .line 546
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lyj;)J
    .locals 4

    .prologue
    const-wide/16 v0, 0x0

    .line 671
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v2

    if-nez v2, :cond_1

    .line 676
    :cond_0
    :goto_0
    return-wide v0

    .line 674
    :cond_1
    invoke-static {p1}, Lbsx;->a(Lyj;)Lbsx;

    move-result-object v2

    const-string v3, "last_invite_seen_timestamp"

    invoke-virtual {v2, v3, v0, v1}, Lbsx;->a(Ljava/lang/String;J)J

    move-result-wide v0

    goto :goto_0
.end method

.method public a(J)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x6

    const/4 v6, 0x1

    .line 687
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 688
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 689
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 691
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 692
    new-instance v0, Ljava/util/Date;

    const-wide/16 v4, 0x3e8

    div-long v4, p1, v4

    invoke-direct {v0, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v3, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 694
    const-string v0, ""

    .line 695
    invoke-virtual {v3, v7}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-virtual {v2, v7}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-le v4, v2, :cond_0

    .line 696
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x7

    .line 697
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    .line 696
    invoke-virtual {v3, v2, v6, v4}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 700
    :cond_0
    invoke-static {p0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v2

    .line 701
    sget v4, Lh;->ci:I

    new-array v5, v6, [Ljava/lang/Object;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 703
    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    .line 701
    invoke-virtual {v1, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 705
    return-object v0
.end method

.method public a(I)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 925
    const-string v1, "EsApplication"

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 927
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 928
    const-string v2, "gms_core_valid"

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 929
    const-string v0, "gms_core_status_code"

    invoke-interface {v1, v0, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 930
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 931
    return-void
.end method

.method public a(Landroid/app/Activity;Lyj;Ljava/lang/Runnable;)V
    .locals 4

    .prologue
    .line 716
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 717
    new-instance v2, Landroid/widget/ArrayAdapter;

    const v0, 0x1090011

    invoke-direct {v2, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 719
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->i()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 720
    invoke-virtual {v2, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 723
    :cond_0
    sget v0, Lh;->cf:I

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 725
    new-instance v0, Lbag;

    invoke-direct {v0, p0, p2, p3}, Lbag;-><init>(Lcom/google/android/apps/hangouts/phone/EsApplication;Lyj;Ljava/lang/Runnable;)V

    .line 734
    new-instance v3, Lbah;

    invoke-direct {v3, p0, p3}, Lbah;-><init>(Lcom/google/android/apps/hangouts/phone/EsApplication;Ljava/lang/Runnable;)V

    .line 742
    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 743
    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 744
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 745
    return-void
.end method

.method public a(Lyj;J)V
    .locals 2

    .prologue
    .line 807
    invoke-static {}, Lcwz;->b()V

    .line 808
    if-nez p1, :cond_0

    .line 813
    :goto_0
    return-void

    .line 812
    :cond_0
    invoke-static {p1}, Lbsx;->a(Lyj;)Lbsx;

    move-result-object v0

    const-string v1, "dnd_expiration"

    invoke-virtual {v0, v1, p2, p3}, Lbsx;->b(Ljava/lang/String;J)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 889
    const-string v0, "EsApplication"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 890
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 891
    const-string v1, "processing_push"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 892
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 893
    return-void
.end method

.method public b(J)J
    .locals 2

    .prologue
    .line 711
    const-wide/16 v0, 0x3e8

    div-long v0, p1, v0

    return-wide v0
.end method

.method public b(Lyj;)J
    .locals 4

    .prologue
    const-wide/16 v0, 0x0

    .line 680
    if-nez p1, :cond_0

    .line 683
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {p1}, Lbsx;->a(Lyj;)Lbsx;

    move-result-object v2

    const-string v3, "dnd_expiration"

    invoke-virtual {v2, v3, v0, v1}, Lbsx;->a(Ljava/lang/String;J)J

    move-result-wide v0

    goto :goto_0
.end method

.method public b(Lyj;J)V
    .locals 2

    .prologue
    .line 816
    invoke-static {}, Lcwz;->b()V

    .line 817
    invoke-static {p1}, Lcwz;->b(Ljava/lang/Object;)V

    .line 819
    invoke-static {p1}, Lbsx;->a(Lyj;)Lbsx;

    move-result-object v0

    const-string v1, "last_invite_seen_timestamp"

    invoke-virtual {v0, v1, p2, p3}, Lbsx;->b(Ljava/lang/String;J)V

    .line 821
    return-void
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 972
    const-string v0, "EsApplication"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 974
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 975
    const-string v1, "talk_still_alive"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 976
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 977
    return-void
.end method

.method public b(I)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1009
    invoke-static {p1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->e(I)Ljava/lang/String;

    move-result-object v1

    .line 1010
    if-eqz v1, :cond_0

    .line 1011
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v2

    const-string v3, "EsApplication"

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 1013
    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 1015
    :cond_0
    return v0
.end method

.method public c()Lcyj;
    .locals 4

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/EsApplication;->h:Lcyj;

    if-nez v0, :cond_2

    .line 170
    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/EsApplication;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 171
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/EsApplication;->h:Lcyj;

    if-nez v0, :cond_1

    .line 172
    new-instance v0, Lcyj;

    invoke-direct {v0, p0}, Lcyj;-><init>(Landroid/content/Context;)V

    .line 174
    const-class v2, Lcyt;

    new-instance v3, Lbcg;

    invoke-direct {v3}, Lbcg;-><init>()V

    invoke-virtual {v0, v2, v3}, Lcyj;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcyj;

    .line 175
    new-instance v2, Lcyn;

    invoke-direct {v2, p0}, Lcyn;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Lcyj;->a(Lcym;)Lcyj;

    .line 176
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v2, v3, :cond_0

    .line 177
    const-class v2, Landroid/app/Application$ActivityLifecycleCallbacks;

    invoke-static {}, Lbkk;->c()Lbkk;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcyj;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 179
    :cond_0
    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/EsApplication;->h:Lcyj;

    .line 181
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 183
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/EsApplication;->h:Lcyj;

    return-object v0

    .line 181
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public c(I)V
    .locals 4

    .prologue
    .line 1022
    invoke-static {p1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->e(I)Ljava/lang/String;

    move-result-object v0

    .line 1023
    if-eqz v0, :cond_0

    .line 1024
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    const-string v2, "EsApplication"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1026
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 1027
    const/4 v2, 0x1

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1028
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1030
    :cond_0
    return-void
.end method

.method public c(Z)V
    .locals 0

    .prologue
    .line 988
    iput-boolean p1, p0, Lcom/google/android/apps/hangouts/phone/EsApplication;->q:Z

    .line 989
    return-void
.end method

.method public f()I
    .locals 1

    .prologue
    .line 565
    iget v0, p0, Lcom/google/android/apps/hangouts/phone/EsApplication;->s:I

    return v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 606
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/EsApplication;->m:Z

    return v0
.end method

.method public h()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 610
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/EsApplication;->m:Z

    if-nez v0, :cond_0

    .line 611
    iput-boolean v2, p0, Lcom/google/android/apps/hangouts/phone/EsApplication;->m:Z

    .line 612
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lbae;

    invoke-direct {v1, p0}, Lbae;-><init>(Lcom/google/android/apps/hangouts/phone/EsApplication;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 613
    new-instance v0, Lbad;

    invoke-direct {v0, p0}, Lbad;-><init>(Lcom/google/android/apps/hangouts/phone/EsApplication;)V

    new-array v1, v2, [Landroid/content/Context;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-virtual {v0, v1}, Lbad;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 615
    :cond_0
    return-void
.end method

.method public j()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 909
    const-string v0, "EsApplication"

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 911
    const-string v1, "gms_core_valid"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public k()I
    .locals 3

    .prologue
    .line 915
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->l()Z

    move-result v0

    invoke-static {v0}, Lcwz;->a(Z)V

    .line 916
    const-string v0, "EsApplication"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 918
    const-string v1, "gms_core_status_code"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public l()Z
    .locals 2

    .prologue
    .line 937
    const-string v0, "EsApplication"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 939
    const-string v1, "gms_core_status_code"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public m()Z
    .locals 3

    .prologue
    .line 943
    const-string v0, "EsApplication"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 945
    const-string v1, "first_upgrade"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public n()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 949
    const-string v0, "EsApplication"

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 951
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 952
    const-string v1, "first_upgrade"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 953
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 954
    return-void
.end method

.method public o()Z
    .locals 3

    .prologue
    .line 962
    const-string v0, "EsApplication"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 964
    const-string v1, "talk_still_alive"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public onCreate()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 427
    const-string v0, "EsApplication.onCreate"

    invoke-static {v0}, Lbzq;->a(Ljava/lang/String;)V

    .line 428
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 440
    const-string v0, "Babel_profile"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 444
    const-string v0, "babelStartup"

    const/high16 v1, 0x8000000

    invoke-static {v0, v1}, Landroid/os/Debug;->startMethodTracing(Ljava/lang/String;I)V

    .line 445
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lazz;

    invoke-direct {v1, p0}, Lazz;-><init>(Lcom/google/android/apps/hangouts/phone/EsApplication;)V

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 455
    :cond_0
    sput-object p0, Lcom/google/android/apps/hangouts/phone/EsApplication;->e:Lcom/google/android/apps/hangouts/phone/EsApplication;

    .line 456
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->d:Landroid/content/Context;

    .line 459
    :try_start_0
    sget-object v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/libraries/social/jni/crashreporter/NativeCrashHandler;->a(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_1

    .line 466
    :goto_0
    invoke-static {}, Lcwz;->a()V

    sget-boolean v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->f:Z

    if-nez v0, :cond_4

    invoke-static {}, Lbzd;->a()V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lazx;

    invoke-direct {v1}, Lazx;-><init>()V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    const-string v1, "otherBackgroundInitialization"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    const-string v0, "babel_server_request_timeout"

    const v1, 0x9c40

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;->setNetworkOperationTimeout(J)V

    invoke-static {}, Lbld;->b()V

    invoke-static {}, Lbkb;->a()I

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lbkb;->d()V

    :cond_1
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "EsApplication"

    invoke-virtual {v0, v1, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "last_seen_version_name"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lbci;->a()Lbci;

    move-result-object v2

    iget-object v2, v2, Lbci;->a:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    :cond_2
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "last_seen_version_name"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    invoke-static {v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->b(Ljava/lang/String;)V

    new-instance v0, Lazy;

    invoke-direct {v0}, Lazy;-><init>()V

    new-array v2, v5, [Ljava/lang/String;

    aput-object v1, v2, v4

    invoke-virtual {v0, v2}, Lazy;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    invoke-static {}, Lape;->f()V

    :cond_3
    invoke-static {}, Lape;->a()V

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbvm;->a(Landroid/content/Context;)V

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->e()V

    invoke-static {}, Lbuz;->a()V

    sput-boolean v5, Lcom/google/android/apps/hangouts/phone/EsApplication;->f:Z

    .line 469
    :cond_4
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 470
    const-string v1, "com.google.gservices.intent.action.GSERVICES_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 471
    const-string v1, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 472
    const-string v1, "android.net.wifi.supplicant.CONNECTION_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 474
    new-instance v1, Lbaa;

    invoke-direct {v1, p0}, Lbaa;-><init>(Lcom/google/android/apps/hangouts/phone/EsApplication;)V

    .line 493
    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 494
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->y()V

    .line 496
    const-string v0, "Babel_strictmode"

    invoke-static {v0}, Lbys;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 500
    new-instance v0, Landroid/os/StrictMode$ThreadPolicy$Builder;

    invoke-direct {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;-><init>()V

    .line 501
    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->detectAll()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->penaltyLog()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->build()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    .line 500
    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 504
    :cond_5
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/EsApplication;->g:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 505
    invoke-static {p0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 507
    invoke-static {}, Lbrc;->a()V

    .line 509
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/GcmIntentService;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "EsApplication"

    invoke-virtual {p0, v0, v4}, Lcom/google/android/apps/hangouts/phone/EsApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "processing_push"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 510
    const-string v0, "Babel"

    const-string v1, "wasProcessingPush is set; force sync"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 511
    invoke-virtual {p0, v4}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Z)V

    .line 512
    invoke-static {v4}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Z)V

    .line 516
    :cond_6
    sget-object v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->d:Landroid/content/Context;

    const-string v1, "activity"

    .line 517
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 518
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v1

    sput v1, Lcom/google/android/apps/hangouts/phone/EsApplication;->a:I

    .line 519
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_7

    .line 520
    invoke-virtual {v0}, Landroid/app/ActivityManager;->isLowRamDevice()Z

    move-result v0

    sput-boolean v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->k:Z

    .line 524
    :cond_7
    invoke-static {}, Lcom/google/android/apps/hangouts/sms/SmsReceiver;->a()V

    .line 527
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 528
    if-eqz v0, :cond_8

    .line 529
    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/EsApplication;->t:Landroid/telephony/PhoneStateListener;

    invoke-virtual {v0, v1, v5}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 532
    :cond_8
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_9

    .line 533
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->z()V

    .line 536
    :cond_9
    const-string v0, "Babel"

    const-string v1, "EsApplication.onCreate ended"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 537
    invoke-static {}, Lbzq;->a()V

    .line 538
    return-void

    .line 460
    :catch_0
    move-exception v0

    .line 461
    const-string v1, "Babel"

    const-string v2, "Error installing NativeCrashHandler"

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 462
    :catch_1
    move-exception v0

    .line 463
    const-string v1, "Babel"

    const-string v2, "Failed to load NativeCrashHandler"

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0
.end method

.method public onLowMemory()V
    .locals 2

    .prologue
    .line 1034
    const-string v0, "Babel"

    const-string v1, "EsApplication.onLowMemory"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1035
    sget-object v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->c:Lcxe;

    invoke-virtual {v0}, Lcxe;->a()V

    .line 1036
    invoke-super {p0}, Landroid/app/Application;->onLowMemory()V

    .line 1037
    return-void
.end method

.method public onTerminate()V
    .locals 3

    .prologue
    .line 556
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 557
    if-eqz v0, :cond_0

    .line 558
    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/EsApplication;->t:Landroid/telephony/PhoneStateListener;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 560
    :cond_0
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 1042
    sget-boolean v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->b:Z

    if-eqz v0, :cond_0

    .line 1043
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "EsApplication.onTrimMemory level="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1045
    :cond_0
    sget-object v0, Lcom/google/android/apps/hangouts/phone/EsApplication;->c:Lcxe;

    invoke-virtual {v0, p1}, Lcxe;->a(I)V

    .line 1046
    invoke-super {p0, p1}, Landroid/app/Application;->onTrimMemory(I)V

    .line 1047
    return-void
.end method

.method public p()Z
    .locals 1

    .prologue
    .line 984
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/EsApplication;->q:Z

    return v0
.end method

.method public uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 639
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    if-eq v0, p1, :cond_0

    const/4 v0, 0x1

    .line 640
    :goto_0
    if-eqz v0, :cond_3

    .line 641
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Uncaught exception in background thread "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 647
    invoke-static {}, Lzo;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 648
    const-string v0, "Babel"

    const-string v1, "An account has just been deactivated, which put background threads at a risk of failure. Letting this thread live."

    invoke-static {v0, v1, p2}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 666
    :goto_1
    return-void

    .line 639
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 653
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/EsApplication;->j:Landroid/os/Handler;

    if-nez v0, :cond_2

    .line 654
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/EsApplication;->j:Landroid/os/Handler;

    .line 656
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/EsApplication;->j:Landroid/os/Handler;

    new-instance v1, Lbaf;

    invoke-direct {v1, p0, p1, p2}, Lbaf;-><init>(Lcom/google/android/apps/hangouts/phone/EsApplication;Ljava/lang/Thread;Ljava/lang/Throwable;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    .line 664
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/EsApplication;->g:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v0, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
