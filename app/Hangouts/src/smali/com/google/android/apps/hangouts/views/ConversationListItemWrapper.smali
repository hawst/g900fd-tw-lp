.class public Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Lauw;


# instance fields
.field final a:Ljava/lang/Runnable;

.field private b:I

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;->b:I

    .line 28
    new-instance v0, Lcbu;

    invoke-direct {v0, p0}, Lcbu;-><init>(Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;->a:Ljava/lang/Runnable;

    .line 40
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;)Landroid/view/View;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;->c:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;->f()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 49
    sget v0, Lg;->aJ:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 50
    iput-object p1, p0, Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;->c:Landroid/view/View;

    .line 51
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;->c:Landroid/view/View;

    instance-of v0, v0, Lbzz;

    return v0
.end method

.method public c()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;->a:Ljava/lang/Runnable;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;->getMeasuredHeight()I

    move-result v1

    const-string v2, "animatedHeight"

    const/4 v3, 0x2

    new-array v3, v3, [I

    aput v1, v3, v4

    const/4 v1, 0x1

    aput v4, v3, v1

    invoke-static {p0, v2, v3}, Lwr;->a(Ljava/lang/Object;Ljava/lang/String;[I)Lwr;

    move-result-object v1

    new-instance v2, Lwp;

    invoke-direct {v2, v4}, Lwp;-><init>(B)V

    invoke-virtual {v1, v2}, Lwr;->a(Lwt;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v1, v2, v3}, Lwr;->c(J)Lwr;

    new-instance v2, Lcbv;

    invoke-direct {v2, p0, v0}, Lcbv;-><init>(Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;Ljava/lang/Runnable;)V

    invoke-virtual {v1, v2}, Lwr;->a(Lwl;)V

    invoke-virtual {v1}, Lwr;->b()V

    .line 70
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;->d:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 213
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 218
    return-void
.end method

.method public f()Landroid/view/View;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;->c:Landroid/view/View;

    return-object v0
.end method

.method public g()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 85
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;->b:I

    .line 87
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;->c:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;->setAlpha(F)V

    .line 91
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;->e()V

    .line 92
    return-void
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 44
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 45
    sget v0, Lg;->aI:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;->d:Landroid/view/View;

    .line 46
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 222
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 224
    iget v0, p0, Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 225
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;->setMeasuredDimension(II)V

    .line 229
    :goto_0
    return-void

    .line 227
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;->b:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method public setAnimatedHeight(I)V
    .locals 0

    .prologue
    .line 206
    iput p1, p0, Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;->b:I

    .line 207
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/ConversationListItemWrapper;->requestLayout()V

    .line 208
    return-void
.end method
