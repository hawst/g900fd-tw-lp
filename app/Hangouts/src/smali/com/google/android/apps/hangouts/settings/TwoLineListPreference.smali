.class public Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;
.super Landroid/preference/ListPreference;
.source "PG"


# instance fields
.field private a:[Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    return-void
.end method


# virtual methods
.method public a([Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;->a:[Ljava/lang/CharSequence;

    .line 51
    return-void
.end method

.method protected onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 55
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;->findIndexOfValue(Ljava/lang/String;)I

    .line 56
    new-instance v0, Lbuy;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lf;->gB:I

    .line 57
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;->a:[Ljava/lang/CharSequence;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lbuy;-><init>(Lcom/google/android/apps/hangouts/settings/TwoLineListPreference;Landroid/content/Context;I[Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)V

    .line 58
    invoke-virtual {p1, v0, p0}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 59
    invoke-virtual {p1, v6, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 60
    invoke-super {p0, p1}, Landroid/preference/ListPreference;->onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V

    .line 61
    return-void
.end method
