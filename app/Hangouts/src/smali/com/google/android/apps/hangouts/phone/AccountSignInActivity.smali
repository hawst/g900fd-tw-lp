.class public Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;
.super Lakn;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field private static final r:Z


# instance fields
.field private volatile A:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private volatile B:I

.field private C:Z

.field private final s:Lavo;

.field private volatile t:Lyj;

.field private volatile u:Lyj;

.field private volatile v:Z

.field private volatile w:Lyj;

.field private volatile x:Lyj;

.field private volatile y:Lyj;

.field private volatile z:Landroid/content/Intent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lbys;->g:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->r:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 41
    invoke-direct {p0}, Lakn;-><init>()V

    .line 68
    new-instance v0, Lavo;

    invoke-direct {v0, p0, v1}, Lavo;-><init>(Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;B)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->s:Lavo;

    .line 73
    iput-object v2, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->u:Lyj;

    .line 76
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->v:Z

    .line 90
    iput-object v2, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->A:Ljava/util/List;

    .line 95
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->C:Z

    .line 817
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;)V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->s:Lavo;

    invoke-static {v0}, Lbkb;->b(Lbkc;)V

    new-instance v0, Lavn;

    invoke-direct {v0, p0}, Lavn;-><init>(Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;)V

    invoke-static {p0, v0}, Lf;->a(Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;Lyj;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 41
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "retryAccount account: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lyj;->Y()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->showDialog(I)V

    iput-object v3, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->u:Lyj;

    iput-boolean v4, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->v:Z

    iput-object v3, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->x:Lyj;

    iput-object v3, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->w:Lyj;

    iput-object v3, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->y:Lyj;

    iput-object v3, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->A:Ljava/util/List;

    iput v4, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->B:I

    iput-object p1, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->t:Lyj;

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->t:Lyj;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lbkb;->a(Lyj;Z)V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;)Lyj;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->y:Lyj;

    return-object v0
.end method

.method private b(Lyj;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 191
    if-nez p1, :cond_1

    move-object v1, v0

    .line 192
    :goto_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->A:Ljava/util/List;

    .line 193
    const/4 v2, 0x0

    .line 196
    const/4 v3, 0x1

    invoke-static {v3}, Lbkb;->g(Z)Ljava/util/List;

    move-result-object v3

    .line 197
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v4, v2

    move-object v2, v0

    :cond_0
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 198
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 199
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v3

    .line 202
    if-eqz v3, :cond_6

    .line 203
    invoke-virtual {v3}, Lyj;->u()Z

    move-result v6

    if-eqz v6, :cond_2

    move-object v2, v3

    .line 205
    goto :goto_1

    .line 191
    :cond_1
    invoke-virtual {p1}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 208
    :cond_2
    invoke-static {v3}, Lbkb;->l(Lyj;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 209
    iget-object v3, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->A:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 211
    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->A:Ljava/util/List;

    invoke-interface {v3, v4, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 212
    add-int/lit8 v0, v4, 0x1

    :goto_2
    move v4, v0

    .line 215
    goto :goto_1

    .line 218
    :cond_4
    if-eqz v2, :cond_5

    invoke-static {}, Lbkb;->m()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 221
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->A:Ljava/util/List;

    invoke-virtual {v2}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v4, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 224
    :cond_5
    return-void

    :cond_6
    move v0, v4

    goto :goto_2
.end method

.method private c(I)V
    .locals 2

    .prologue
    .line 303
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->A:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_2

    .line 304
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->A:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 305
    add-int/lit8 p1, p1, 0x1

    .line 306
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    .line 307
    invoke-virtual {v0}, Lyj;->u()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lbkb;->m()Z

    move-result v1

    if-nez v1, :cond_1

    .line 308
    sget-boolean v0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->r:Z

    if-eqz v0, :cond_0

    .line 309
    const-string v0, "Babel"

    const-string v1, "logonRemainingAccount skipping sms_only account"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 313
    :cond_1
    if-eqz v0, :cond_0

    .line 314
    invoke-static {v0}, Lbkb;->l(Lyj;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 315
    invoke-static {v0}, Lbkb;->k(Lyj;)V

    goto :goto_0

    .line 319
    :cond_2
    return-void
.end method

.method public static synthetic c(Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->o()V

    return-void
.end method

.method private c(Lyj;)V
    .locals 4

    .prologue
    .line 409
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->dismissDialog(I)V

    .line 413
    invoke-virtual {p1}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    .line 414
    if-nez v0, :cond_0

    .line 415
    const-string v0, "Babel"

    const-string v1, "picked account is removed"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->setResult(I)V

    .line 417
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->finish()V

    .line 430
    :goto_0
    return-void

    .line 421
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->z:Landroid/content/Intent;

    if-eqz v1, :cond_1

    .line 422
    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->z:Landroid/content/Intent;

    const-string v2, "account_name"

    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 423
    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->z:Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->startActivity(Landroid/content/Intent;)V

    .line 426
    :cond_1
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 427
    const-string v2, "account_name"

    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 428
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->setResult(ILandroid/content/Intent;)V

    .line 429
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->finish()V

    goto :goto_0
.end method

.method public static synthetic d(Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->t()V

    return-void
.end method

.method public static synthetic e(Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;)Lavo;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->s:Lavo;

    return-object v0
.end method

.method private n()Lyj;
    .locals 4

    .prologue
    .line 265
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->B:I

    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->A:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 266
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->A:Ljava/util/List;

    iget v1, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->B:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 267
    iget v1, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->B:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->B:I

    .line 268
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    .line 269
    sget-boolean v1, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->r:Z

    if-eqz v1, :cond_1

    .line 270
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getNextAccount: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    :cond_1
    if-eqz v0, :cond_0

    .line 273
    invoke-virtual {v0}, Lyj;->u()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 274
    invoke-static {}, Lbkb;->m()Z

    move-result v1

    if-nez v1, :cond_2

    .line 275
    sget-boolean v0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->r:Z

    if-eqz v0, :cond_0

    .line 276
    const-string v0, "Babel"

    const-string v1, "getNextAccount skipping sms_only account, sms not enabled"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 280
    :cond_2
    invoke-static {v0}, Lbkb;->l(Lyj;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 281
    const-string v1, "Babel"

    const-string v2, "Exausted all logged-on accounts. Try logged off accounts"

    invoke-static {v1, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    iget v1, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->B:I

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, v1}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->c(I)V

    .line 288
    :cond_3
    sget-boolean v1, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->r:Z

    if-eqz v1, :cond_4

    .line 289
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getNextAccount returning: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    :cond_4
    :goto_1
    return-object v0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private o()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 326
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->y:Lyj;

    if-eqz v0, :cond_1

    .line 378
    :cond_0
    :goto_0
    return-void

    .line 330
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->u:Lyj;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->v:Z

    if-eqz v0, :cond_2

    .line 333
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->q()Z

    move-result v0

    if-nez v0, :cond_0

    .line 335
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->u:Lyj;

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->c(Lyj;)V

    goto :goto_0

    .line 338
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->t:Lyj;

    invoke-static {v0}, Lbkb;->f(Lyj;)I

    move-result v0

    .line 341
    const/16 v2, 0x64

    if-ne v0, v2, :cond_3

    .line 344
    const-string v0, "Babel"

    const-string v2, "Account is in initialize state, restart setup"

    invoke-static {v0, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->t:Lyj;

    invoke-static {v0, v1}, Lbkb;->a(Lyj;Z)V

    goto :goto_0

    .line 349
    :cond_3
    const/16 v2, 0x65

    if-eq v0, v2, :cond_0

    .line 354
    const/16 v2, 0x66

    if-ne v0, v2, :cond_5

    .line 356
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->t:Lyj;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->u:Lyj;

    .line 359
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->v:Z

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->q()Z

    move-result v0

    if-nez v0, :cond_0

    .line 360
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->u:Lyj;

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->c(Lyj;)V

    goto :goto_0

    .line 365
    :cond_5
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "Babel"

    invoke-static {v3, v7}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_6

    const-string v3, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "handleAccountError state: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mAccount: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->t:Lyj;

    invoke-virtual {v5}, Lyj;->Y()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    const/16 v3, 0xa

    invoke-virtual {p0, v3}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->dismissDialog(I)V

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const-string v0, "error_message"

    sget v3, Lh;->lf:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v7, v2}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->showDialog(ILandroid/os/Bundle;)Z

    move v0, v1

    :cond_7
    :goto_1
    if-nez v0, :cond_8

    iput-boolean v6, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->C:Z

    .line 366
    :cond_8
    :goto_2
    if-eqz v0, :cond_12

    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->p()Z

    move-result v2

    if-eqz v2, :cond_12

    .line 367
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->n()Lyj;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->t:Lyj;

    .line 368
    iget-object v2, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->t:Lyj;

    if-nez v2, :cond_11

    .line 369
    const-string v0, "Babel"

    const-string v2, "All valid accounts removed. Exit"

    invoke-static {v0, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    invoke-virtual {p0, v1}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->setResult(I)V

    .line 371
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->finish()V

    goto/16 :goto_0

    .line 365
    :pswitch_1
    const-string v0, "error_title"

    sget v3, Lh;->lh:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "error_message"

    sget v3, Lh;->lc:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->showDialog(ILandroid/os/Bundle;)Z

    move v0, v1

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->t:Lyj;

    invoke-static {v0, v6}, Lbkb;->b(Lyj;Z)V

    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->p()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->s()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->r()V

    goto :goto_1

    :cond_9
    const-string v3, "error_message"

    sget v4, Lh;->ld:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x4

    invoke-virtual {p0, v3, v2}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->showDialog(ILandroid/os/Bundle;)Z

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->w:Lyj;

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->t:Lyj;

    invoke-virtual {v0}, Lyj;->r()Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->t:Lyj;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->w:Lyj;

    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->x:Lyj;

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->t:Lyj;

    invoke-virtual {v0}, Lyj;->A()Z

    move-result v0

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->t:Lyj;

    invoke-virtual {v0}, Lyj;->r()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->t:Lyj;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->x:Lyj;

    :cond_b
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->p()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->r()V

    goto/16 :goto_1

    :pswitch_4
    const-string v0, "error_message"

    sget v3, Lh;->le:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v6, v2}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->showDialog(ILandroid/os/Bundle;)Z

    move v0, v1

    goto/16 :goto_1

    :pswitch_5
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->t:Lyj;

    invoke-static {v0}, Lbkb;->h(Lyj;)Ljava/lang/Exception;

    move-result-object v0

    if-eqz v0, :cond_e

    instance-of v3, v0, Lbxl;

    if-eqz v3, :cond_e

    check-cast v0, Lbxl;

    iget v3, v0, Lbxl;->b:I

    if-eqz v3, :cond_d

    sget-boolean v2, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->r:Z

    if-eqz v2, :cond_c

    const-string v2, "Babel"

    const-string v3, "Gms setup is not complete, showing Gms install dialog"

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->e()Lae;

    move-result-object v2

    invoke-virtual {v2}, Lae;->a()Lao;

    move-result-object v2

    iget v0, v0, Lbxl;->b:I

    invoke-static {v0}, Lf;->a(I)Ls;

    move-result-object v0

    const-string v3, "gmscore dialog"

    invoke-virtual {v0, v2, v3}, Ls;->a(Lao;Ljava/lang/String;)I

    move v0, v1

    goto/16 :goto_2

    :cond_d
    iget-object v0, v0, Lbxl;->a:Landroid/content/Intent;

    if-eqz v0, :cond_e

    invoke-virtual {v0}, Landroid/content/Intent;->getFlags()I

    move-result v2

    const v3, -0x10000001

    and-int/2addr v2, v3

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const/16 v2, 0x3e8

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->startActivityForResult(Landroid/content/Intent;I)V

    move v0, v1

    goto/16 :goto_2

    :cond_e
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->p()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->s()Z

    move-result v3

    if-eqz v3, :cond_f

    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->r()V

    goto/16 :goto_2

    :cond_f
    const-string v3, "error_message"

    sget v4, Lh;->lb:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x2

    invoke-virtual {p0, v3, v2}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->showDialog(ILandroid/os/Bundle;)Z

    goto/16 :goto_2

    :pswitch_6
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->p()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->s()Z

    move-result v3

    if-eqz v3, :cond_10

    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->r()V

    goto/16 :goto_1

    :cond_10
    const-string v3, "error_message"

    sget v4, Lh;->lf:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v7, v2}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->showDialog(ILandroid/os/Bundle;)Z

    goto/16 :goto_1

    .line 374
    :cond_11
    iget-object v2, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->t:Lyj;

    invoke-static {v2, v1}, Lbkb;->a(Lyj;Z)V

    .line 376
    :cond_12
    if-nez v0, :cond_2

    goto/16 :goto_0

    .line 365
    nop

    :pswitch_data_0
    .packed-switch 0x67
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_4
        :pswitch_2
        :pswitch_3
        :pswitch_6
    .end packed-switch
.end method

.method private p()Z
    .locals 2

    .prologue
    .line 381
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->A:Ljava/util/List;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->B:I

    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->A:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static q()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 388
    invoke-static {v1}, Lbkb;->g(Z)Ljava/util/List;

    move-result-object v0

    .line 390
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 391
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    .line 392
    if-eqz v0, :cond_0

    .line 393
    invoke-static {v0}, Lbkb;->f(Lyj;)I

    move-result v0

    .line 394
    const/16 v3, 0x65

    if-ne v0, v3, :cond_0

    .line 396
    const/4 v0, 0x1

    .line 401
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private r()V
    .locals 6

    .prologue
    const/4 v2, 0x3

    .line 588
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 590
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->x:Lyj;

    if-eqz v0, :cond_1

    .line 591
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->x:Lyj;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->y:Lyj;

    .line 592
    const-string v0, "Babel"

    invoke-static {v0, v2}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 593
    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "showOOBE mFirstOnTheRecordPromptAccount: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->y:Lyj;

    .line 594
    invoke-virtual {v3}, Lyj;->Y()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 593
    invoke-static {v0, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 596
    :cond_0
    const-string v0, "error_title"

    sget v2, Lh;->ie:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 597
    const-string v0, "error_message"

    sget v2, Lh;->ic:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 598
    const-string v0, "positive_button"

    sget v2, Lh;->id:I

    .line 599
    invoke-virtual {p0, v2}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 598
    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 600
    const/4 v0, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->showDialog(ILandroid/os/Bundle;)Z

    .line 621
    :goto_0
    return-void

    .line 601
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->w:Lyj;

    if-eqz v0, :cond_4

    .line 602
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->w:Lyj;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->y:Lyj;

    .line 603
    const-string v0, "Babel"

    invoke-static {v0, v2}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 604
    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "showOOBE mFirstDomainRestrictedAccount: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->y:Lyj;

    .line 605
    invoke-virtual {v3}, Lyj;->Y()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 604
    invoke-static {v0, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 608
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->w:Lyj;

    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    .line 609
    const/16 v2, 0x40

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 610
    if-ltz v2, :cond_3

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 612
    :cond_3
    const-string v2, "error_title"

    sget v3, Lh;->ih:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    const-string v2, "error_message"

    sget v3, Lh;->ig:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {p0, v3, v4}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    const-string v0, "positive_button"

    sget v2, Lh;->if:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 616
    const-string v0, "negative_button"

    sget v2, Lh;->ii:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 617
    const/4 v0, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->showDialog(ILandroid/os/Bundle;)Z

    goto :goto_0

    .line 619
    :cond_4
    const-string v0, "Babel"

    const-string v1, "showOOBE has nothing to show!"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private s()Z
    .locals 1

    .prologue
    .line 627
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->x:Lyj;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->w:Lyj;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private t()V
    .locals 3

    .prologue
    .line 790
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->setResult(I)V

    .line 791
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->z:Landroid/content/Intent;

    if-nez v0, :cond_0

    .line 792
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->finish()V

    .line 812
    :goto_0
    return-void

    .line 796
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "prev_account_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 797
    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->t:Lyj;

    invoke-virtual {v1}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 798
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 799
    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->z:Landroid/content/Intent;

    const-string v2, "account_name"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 800
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->z:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->startActivity(Landroid/content/Intent;)V

    .line 801
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->finish()V

    goto :goto_0

    .line 809
    :cond_1
    const-string v0, "Babel"

    const-string v1, "Clear all account states after all account sign-in failed"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 810
    invoke-static {}, Lbkb;->w()V

    .line 811
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->finish()V

    goto :goto_0
.end method


# virtual methods
.method protected k()Lyj;
    .locals 1

    .prologue
    .line 258
    const/4 v0, 0x0

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 735
    packed-switch p1, :pswitch_data_0

    .line 767
    :goto_0
    return-void

    .line 737
    :pswitch_0
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 739
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->t:Lyj;

    invoke-static {v0, v1}, Lbkb;->b(Lyj;Z)V

    .line 740
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 741
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->n()Lyj;

    move-result-object v0

    .line 742
    if-eqz v0, :cond_0

    .line 743
    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->t:Lyj;

    .line 744
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->t:Lyj;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbkb;->a(Lyj;Z)V

    goto :goto_0

    .line 749
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->t()V

    goto :goto_0

    .line 753
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->t:Lyj;

    invoke-static {v0, v1}, Lbkb;->a(Lyj;Z)V

    goto :goto_0

    .line 759
    :pswitch_1
    const-string v0, "Babel"

    const-string v1, "Received notification from gmsCore installation. Restarting babel"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 760
    const/4 v0, 0x0

    invoke-static {v0}, Lbbl;->b(Lyj;)Landroid/content/Intent;

    move-result-object v0

    .line 761
    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 762
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->startActivity(Landroid/content/Intent;)V

    .line 763
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->finish()V

    goto :goto_0

    .line 735
    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 782
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->t()V

    .line 783
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 0

    .prologue
    .line 774
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->t()V

    .line 775
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 102
    invoke-super {p0, p1}, Lakn;->onCreate(Landroid/os/Bundle;)V

    .line 104
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "account_name"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->t:Lyj;

    const-string v4, "try_other_accounts"

    invoke-virtual {v0, v4, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    const-string v5, "setup_all_accounts"

    invoke-virtual {v0, v5, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->v:Z

    const-string v5, "Babel"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "Babel"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "validAccountInIntent accountName: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " mAccount: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v6, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->t:Lyj;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " tryOtherAccounts: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " mSetupAllAccounts: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v6, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->v:Z

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    if-eqz v4, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->t:Lyj;

    invoke-direct {p0, v3}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->b(Lyj;)V

    iget-object v3, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->t:Lyj;

    if-nez v3, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->n()Lyj;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->t:Lyj;

    :cond_1
    sget-boolean v3, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->r:Z

    if-eqz v3, :cond_2

    const-string v3, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "validAccountInIntent mAccount: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->t:Lyj;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->t:Lyj;

    if-nez v3, :cond_3

    const-string v0, "Babel"

    const-string v3, "All valid accounts removed"

    invoke-static {v0, v3}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->finish()V

    move v0, v2

    :goto_0
    if-nez v0, :cond_5

    .line 125
    :goto_1
    return-void

    .line 104
    :cond_3
    const-string v3, "intent"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->z:Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->z:Landroid/content/Intent;

    if-nez v0, :cond_4

    sget v0, Lf;->hX:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->setTheme(I)V

    :cond_4
    move v0, v1

    goto :goto_0

    .line 111
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->t:Lyj;

    invoke-virtual {v0}, Lyj;->i()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->A:Ljava/util/List;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->A:Ljava/util/List;

    .line 112
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_9

    :cond_6
    move v0, v1

    .line 113
    :goto_2
    sget-boolean v1, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->r:Z

    if-eqz v1, :cond_7

    .line 114
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AccountSigninActivity onCreate mAccount: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->t:Lyj;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " clean: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    :cond_7
    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->t:Lyj;

    invoke-static {v1, v0}, Lbkb;->a(Lyj;Z)V

    .line 119
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->v:Z

    if-eqz v0, :cond_8

    .line 121
    invoke-static {}, Lbkb;->e()V

    .line 124
    :cond_8
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->showDialog(I)V

    goto :goto_1

    :cond_9
    move v0, v2

    .line 112
    goto :goto_2
.end method

.method public onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 659
    if-nez p2, :cond_0

    move-object v3, v0

    .line 660
    :goto_0
    if-nez p2, :cond_1

    move-object v2, v0

    .line 662
    :goto_1
    packed-switch p1, :pswitch_data_0

    .line 727
    :goto_2
    :pswitch_0
    return-object v0

    .line 659
    :cond_0
    const-string v1, "error_title"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    goto :goto_0

    .line 660
    :cond_1
    const-string v1, "error_message"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    goto :goto_1

    .line 668
    :pswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 669
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 670
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 671
    sget v1, Lh;->hZ:I

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 673
    invoke-virtual {v0, p0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 675
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_2

    .line 680
    :pswitch_2
    if-nez p2, :cond_4

    move-object v1, v0

    .line 682
    :goto_3
    if-nez p2, :cond_5

    .line 684
    :goto_4
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 685
    invoke-virtual {v4, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 686
    invoke-virtual {v4, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 687
    invoke-virtual {v4, p0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 689
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 690
    invoke-virtual {v4, v0, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 693
    :cond_2
    const/4 v0, 0x5

    if-ne p1, v0, :cond_6

    .line 694
    new-instance v0, Lavl;

    invoke-direct {v0, p0}, Lavl;-><init>(Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;)V

    invoke-virtual {v4, v1, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 715
    :cond_3
    :goto_5
    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_2

    .line 680
    :cond_4
    const-string v1, "positive_button"

    .line 681
    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    .line 682
    :cond_5
    const-string v0, "negative_button"

    .line 683
    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 702
    :cond_6
    const/4 v0, 0x6

    if-ne p1, v0, :cond_3

    .line 703
    new-instance v0, Lavm;

    invoke-direct {v0, p0}, Lavm;-><init>(Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;)V

    invoke-virtual {v4, v1, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_5

    .line 719
    :pswitch_3
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 720
    sget v1, Lh;->lg:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 721
    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 722
    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto/16 :goto_2

    .line 662
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 129
    invoke-super {p0}, Lakn;->onDestroy()V

    .line 130
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 248
    invoke-super {p0}, Lakn;->onPause()V

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->s:Lavo;

    invoke-static {v0}, Lbkb;->b(Lbkc;)V

    .line 251
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 232
    invoke-super {p0}, Lakn;->onResume()V

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->s:Lavo;

    invoke-static {v0}, Lbkb;->a(Lbkc;)V

    .line 238
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->C:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 239
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/AccountSignInActivity;->o()V

    .line 241
    :cond_0
    return-void
.end method
