.class public Lcom/google/android/apps/hangouts/phone/HangoutUrlHandlerActivity;
.super Ly;
.source "PG"

# interfaces
.implements Labd;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ly;-><init>()V

    return-void
.end method


# virtual methods
.method public a_(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 39
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/HangoutUrlHandlerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 38
    invoke-static {v0, p1, v1}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->fromUri(Landroid/net/Uri;Ljava/lang/String;Landroid/app/PendingIntent;)Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v0

    .line 40
    if-nez v0, :cond_0

    .line 41
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lh;->ex:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    sget v1, Lh;->hZ:I

    new-instance v2, Lbbh;

    invoke-direct {v2, p0}, Lbbh;-><init>(Lcom/google/android/apps/hangouts/phone/HangoutUrlHandlerActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lbbi;

    invoke-direct {v1, p0}, Lbbi;-><init>(Lcom/google/android/apps/hangouts/phone/HangoutUrlHandlerActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 48
    :goto_0
    return-void

    .line 43
    :cond_0
    const/4 v3, 0x0

    const/16 v4, 0x33

    .line 44
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    move-object v2, v1

    .line 43
    invoke-static/range {v0 .. v6}, Lbbl;->a(Lcom/google/android/libraries/hangouts/video/HangoutRequest;Ljava/util/ArrayList;Ljava/util/ArrayList;ZIJ)Landroid/content/Intent;

    move-result-object v0

    .line 45
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/HangoutUrlHandlerActivity;->startActivity(Landroid/content/Intent;)V

    .line 46
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/HangoutUrlHandlerActivity;->finish()V

    goto :goto_0
.end method

.method public n_()V
    .locals 0

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/HangoutUrlHandlerActivity;->finish()V

    .line 53
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 28
    invoke-super {p0, p1}, Ly;->onCreate(Landroid/os/Bundle;)V

    .line 30
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/HangoutUrlHandlerActivity;->e()Lae;

    move-result-object v0

    invoke-virtual {v0}, Lae;->a()Lao;

    move-result-object v0

    .line 31
    invoke-static {}, Laay;->a()Laay;

    move-result-object v1

    .line 32
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lao;->a(Lt;Ljava/lang/String;)Lao;

    .line 33
    invoke-virtual {v0}, Lao;->b()I

    .line 34
    return-void
.end method
