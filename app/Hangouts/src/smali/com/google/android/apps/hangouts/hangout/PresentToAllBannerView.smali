.class public Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Lapu;


# instance fields
.field private final a:Landroid/widget/Button;

.field private final b:Lauj;

.field private final c:Lapk;

.field private final d:Lasc;

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 49
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    invoke-static {}, Lapk;->b()Lapk;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;->c:Lapk;

    .line 44
    new-instance v0, Lasc;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lasc;-><init>(Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;B)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;->d:Lasc;

    .line 46
    iput-boolean v2, p0, Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;->e:Z

    .line 50
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 51
    sget v1, Lf;->fw:I

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 53
    sget v1, Lg;->fy:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;->a:Landroid/widget/Button;

    .line 54
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;->a:Landroid/widget/Button;

    new-instance v1, Larz;

    invoke-direct {v1, p0}, Larz;-><init>(Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;->a:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setAllCaps(Z)V

    .line 65
    :cond_0
    new-instance v0, Lauj;

    invoke-direct {v0}, Lauj;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;->b:Lauj;

    .line 66
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;)V
    .locals 5

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    rsub-int/lit8 v2, v1, 0x0

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xe

    if-ge v3, v4, :cond_2

    const/4 v1, 0x0

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    new-instance v3, Lasa;

    invoke-direct {v3, p0, v0, v1, v2}, Lasa;-><init>(Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;Landroid/widget/RelativeLayout$LayoutParams;II)V

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lf;->dN:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {v3, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;->b:Lauj;

    invoke-virtual {v0, v3}, Lauj;->trackAnimation(Landroid/view/animation/Animation;)V

    invoke-virtual {p0, v3}, Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;)V
    .locals 4

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;->getHeight()I

    move-result v0

    neg-int v1, v0

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-ge v2, v3, :cond_2

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    new-instance v2, Lasb;

    invoke-direct {v2, p0, v0, v1}, Lasb;-><init>(Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;Landroid/widget/RelativeLayout$LayoutParams;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lf;->dN:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {v2, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;->b:Lauj;

    invoke-virtual {v0, v2}, Lauj;->trackAnimation(Landroid/view/animation/Animation;)V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method


# virtual methods
.method public a(I)V
    .locals 1

    .prologue
    .line 168
    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    .line 169
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;->setVisibility(I)V

    .line 171
    :cond_0
    return-void
.end method

.method public a(Lapv;)V
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;->c:Lapk;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;->d:Lasc;

    invoke-virtual {v0, v1}, Lapk;->a(Lapo;)V

    .line 159
    return-void
.end method

.method public m_()V
    .locals 2

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;->c:Lapk;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;->d:Lasc;

    invoke-virtual {v0, v1}, Lapk;->b(Lapo;)V

    .line 164
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 174
    return-void
.end method
