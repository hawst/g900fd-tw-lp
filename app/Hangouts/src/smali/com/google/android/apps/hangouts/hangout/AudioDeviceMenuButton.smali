.class public Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;
.super Landroid/widget/ImageButton;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# instance fields
.field private final a:Lapk;

.field private final b:Laot;

.field private c:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

.field private d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/libraries/hangouts/video/AudioDevice;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroid/view/animation/Animation;

.field private f:Laos;

.field private g:Lkg;

.field private final h:Lbme;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 138
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 128
    invoke-static {}, Lapk;->b()Lapk;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->a:Lapk;

    .line 129
    new-instance v0, Laot;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Laot;-><init>(Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;B)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->b:Laot;

    .line 139
    invoke-virtual {p0, p0}, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 143
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lf;->bh:I

    .line 142
    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->e:Landroid/view/animation/Animation;

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->e:Landroid/view/animation/Animation;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setRepeatCount(I)V

    .line 145
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->e:Landroid/view/animation/Animation;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setRepeatMode(I)V

    .line 147
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lbme;

    invoke-static {v0, v1}, Lcyj;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbme;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->h:Lbme;

    .line 148
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->c()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/libraries/hangouts/video/AudioDeviceState;)Z
    .locals 1

    .prologue
    .line 33
    invoke-static {p0}, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->b(Lcom/google/android/libraries/hangouts/video/AudioDeviceState;)Z

    move-result v0

    return v0
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;)Lcom/google/android/libraries/hangouts/video/AudioDeviceState;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->c:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    return-object v0
.end method

.method private static b(Lcom/google/android/libraries/hangouts/video/AudioDeviceState;)Z
    .locals 1

    .prologue
    .line 217
    sget-object v0, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->BLUETOOTH_TURNING_ON:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->BLUETOOTH_TURNING_OFF:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic c(Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->d:Ljava/util/Set;

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->f:Laos;

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->f:Laos;

    invoke-virtual {v0}, Laos;->d()V

    .line 168
    :cond_0
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getLocalState()Lcom/google/android/libraries/hangouts/video/LocalState;

    move-result-object v0

    .line 169
    if-nez v0, :cond_1

    .line 170
    iput-object v1, p0, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->c:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    .line 171
    iput-object v1, p0, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->d:Ljava/util/Set;

    .line 172
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->setEnabled(Z)V

    .line 202
    :goto_0
    return-void

    .line 176
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->setEnabled(Z)V

    .line 177
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/LocalState;->getAudioDeviceState()Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->c:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    .line 178
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/LocalState;->getAvailableAudioDevices()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->d:Ljava/util/Set;

    .line 180
    sget-object v0, Laor;->a:[I

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->c:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 197
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->c:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->b(Lcom/google/android/libraries/hangouts/video/AudioDeviceState;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 198
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->e:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 184
    :pswitch_0
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->aL:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->setImageResource(I)V

    goto :goto_1

    .line 187
    :pswitch_1
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->cp:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->setImageResource(I)V

    goto :goto_1

    .line 190
    :pswitch_2
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->bA:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->setImageResource(I)V

    goto :goto_1

    .line 193
    :pswitch_3
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->aM:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->setImageResource(I)V

    goto :goto_1

    .line 200
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->clearAnimation()V

    goto :goto_0

    .line 180
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static synthetic d(Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;)Laos;
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->f:Laos;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;)Lkg;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->g:Lkg;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->a:Lapk;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->b:Laot;

    invoke-virtual {v0, v1}, Lapk;->a(Lapo;)V

    .line 156
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->c()V

    .line 157
    return-void
.end method

.method public a(Lkg;)V
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->g:Lkg;

    .line 152
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->a:Lapk;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->b:Laot;

    invoke-virtual {v0, v1}, Lapk;->b(Lapo;)V

    .line 161
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->h:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v1, 0x5fb

    .line 207
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 206
    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    .line 208
    new-instance v0, Laos;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Laos;-><init>(Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;Landroid/content/Context;Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->f:Laos;

    .line 210
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->f:Laos;

    invoke-virtual {v0}, Laos;->c()V

    .line 211
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->g:Lkg;

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/AudioDeviceMenuButton;->g:Lkg;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lkg;->a(Z)V

    .line 214
    :cond_0
    return-void
.end method
