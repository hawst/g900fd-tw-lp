.class public final Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Z

.field private static i:I


# instance fields
.field private final b:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z

.field private d:Landroid/widget/AbsListView;

.field private final e:Lccw;

.field private f:Lccx;

.field private g:Landroid/os/Handler;

.field private h:Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lbys;->s:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->a:Z

    return-void
.end method

.method public constructor <init>(Landroid/widget/AbsListView;Lccw;Landroid/os/Handler;)V
    .locals 2

    .prologue
    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Ljava/util/PriorityQueue;

    invoke-direct {v0}, Ljava/util/PriorityQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->b:Ljava/util/Queue;

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->c:Z

    .line 175
    iput-object p1, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->d:Landroid/widget/AbsListView;

    .line 176
    iput-object p2, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->e:Lccw;

    .line 177
    iput-object p3, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->g:Landroid/os/Handler;

    .line 179
    invoke-virtual {p1}, Landroid/widget/AbsListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lf;->dQ:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->i:I

    .line 180
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;)Lccw;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->e:Lccw;

    return-object v0
.end method

.method private a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 215
    if-eqz p1, :cond_0

    .line 216
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->c:Z

    .line 217
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->g:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 219
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;)Z
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->c:Z

    return v0
.end method

.method static synthetic c()I
    .locals 1

    .prologue
    .line 22
    sget v0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->i:I

    return v0
.end method

.method static synthetic c(Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;)Lccx;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->f:Lccx;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;)V
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->a(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public a()Lccw;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->e:Lccw;

    return-object v0
.end method

.method public a(Lccx;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->f:Lccx;

    .line 53
    return-void
.end method

.method public a(Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;)V
    .locals 0

    .prologue
    .line 183
    iput-object p1, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->h:Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;

    .line 184
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->e:Lccw;

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->e:Lccw;

    invoke-interface {v0}, Lccw;->a()V

    .line 67
    :cond_0
    invoke-static {p1}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->c(Z)V

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->h:Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->h:Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->d:Landroid/widget/AbsListView;

    if-ne v0, v1, :cond_1

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->h:Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->g()V

    .line 72
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->e:Lccw;

    if-eqz v0, :cond_2

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->e:Lccw;

    invoke-interface {v0}, Lccw;->b()V

    .line 75
    :cond_2
    return-void
.end method

.method public b()Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->h:Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;

    return-object v0
.end method

.method public b(Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;)V
    .locals 3

    .prologue
    .line 195
    sget-boolean v0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->a:Z

    if-eqz v0, :cond_0

    .line 196
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "enqueueForAnimation "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    :cond_0
    new-instance v0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->d:Landroid/widget/AbsListView;

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;-><init>(Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;Landroid/widget/AbsListView;)V

    iget-boolean v1, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->c:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->b:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 199
    :goto_0
    return-void

    .line 198
    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
