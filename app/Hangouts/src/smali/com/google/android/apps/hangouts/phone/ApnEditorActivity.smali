.class public Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;
.super Landroid/preference/PreferenceActivity;
.source "PG"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field private static a:Ljava/lang/String;

.field private static final o:[Ljava/lang/String;


# instance fields
.field private b:Landroid/preference/EditTextPreference;

.field private c:Landroid/preference/EditTextPreference;

.field private d:Landroid/preference/EditTextPreference;

.field private e:Landroid/preference/EditTextPreference;

.field private f:Landroid/preference/EditTextPreference;

.field private g:Landroid/preference/EditTextPreference;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Landroid/database/Cursor;

.field private k:Z

.field private l:Z

.field private m:Landroid/content/res/Resources;

.field private n:Ljava/lang/String;

.field private p:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 64
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "mmsc"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "mcc"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "mnc"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "numeric"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "mmsproxy"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "mmsport"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->o:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->j:Landroid/database/Cursor;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->n:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    invoke-static {p0}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Z)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 291
    iget-object v2, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->b:Landroid/preference/EditTextPreference;

    invoke-virtual {v2}, Landroid/preference/EditTextPreference;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 292
    iget-object v3, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->d:Landroid/preference/EditTextPreference;

    invoke-virtual {v3}, Landroid/preference/EditTextPreference;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 293
    iget-object v4, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->e:Landroid/preference/EditTextPreference;

    invoke-virtual {v4}, Landroid/preference/EditTextPreference;->getText()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 295
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->c()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    if-nez p1, :cond_0

    .line 296
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->showDialog(I)V

    .line 340
    :goto_0
    return v0

    .line 301
    :cond_0
    new-instance v5, Lavr;

    invoke-direct {v5, p0, v2, v3, v4}, Lavr;-><init>(Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-array v2, v1, [Ljava/lang/Void;

    const/4 v3, 0x0

    aput-object v3, v2, v0

    .line 338
    invoke-virtual {v5, v2}, Lavr;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move v0, v1

    .line 340
    goto :goto_0
.end method

.method public static synthetic a()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->o:[Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;)Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->p:Landroid/database/sqlite/SQLiteDatabase;

    return-object v0
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 407
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 408
    :cond_0
    sget-object p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->a:Ljava/lang/String;

    .line 410
    :cond_1
    return-object p0
.end method

.method private b()V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v2, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 178
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->k:Z

    if-eqz v0, :cond_2

    .line 179
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->d:Landroid/preference/EditTextPreference;

    invoke-virtual {v0, v3}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->e:Landroid/preference/EditTextPreference;

    invoke-virtual {v0, v3}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    .line 181
    invoke-static {}, Lbzd;->i()Ljava/lang/String;

    move-result-object v0

    .line 183
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v5, :cond_0

    .line 185
    invoke-virtual {v0, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 187
    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 189
    iget-object v2, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->d:Landroid/preference/EditTextPreference;

    invoke-virtual {v2, v1}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    .line 190
    iget-object v2, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->e:Landroid/preference/EditTextPreference;

    invoke-virtual {v2, v0}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    .line 191
    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->h:Ljava/lang/String;

    .line 192
    iput-object v1, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->i:Ljava/lang/String;

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->b:Landroid/preference/EditTextPreference;

    invoke-virtual {v0, v3}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    .line 195
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->f:Landroid/preference/EditTextPreference;

    invoke-virtual {v0, v3}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->g:Landroid/preference/EditTextPreference;

    invoke-virtual {v0, v3}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    .line 197
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->c:Landroid/preference/EditTextPreference;

    invoke-virtual {v0, v3}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    .line 209
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->b:Landroid/preference/EditTextPreference;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->b:Landroid/preference/EditTextPreference;

    invoke-virtual {v1}, Landroid/preference/EditTextPreference;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 210
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->f:Landroid/preference/EditTextPreference;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->f:Landroid/preference/EditTextPreference;

    invoke-virtual {v1}, Landroid/preference/EditTextPreference;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 211
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->g:Landroid/preference/EditTextPreference;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->g:Landroid/preference/EditTextPreference;

    invoke-virtual {v1}, Landroid/preference/EditTextPreference;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->c:Landroid/preference/EditTextPreference;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->c:Landroid/preference/EditTextPreference;

    invoke-virtual {v1}, Landroid/preference/EditTextPreference;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->d:Landroid/preference/EditTextPreference;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->d:Landroid/preference/EditTextPreference;

    invoke-virtual {v1}, Landroid/preference/EditTextPreference;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 214
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->e:Landroid/preference/EditTextPreference;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->e:Landroid/preference/EditTextPreference;

    invoke-virtual {v1}, Landroid/preference/EditTextPreference;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 215
    return-void

    .line 198
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->l:Z

    if-eqz v0, :cond_1

    .line 199
    iput-boolean v2, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->l:Z

    .line 201
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->b:Landroid/preference/EditTextPreference;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->j:Landroid/database/Cursor;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->f:Landroid/preference/EditTextPreference;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->j:Landroid/database/Cursor;

    const/4 v2, 0x6

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    .line 203
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->g:Landroid/preference/EditTextPreference;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->j:Landroid/database/Cursor;

    const/4 v2, 0x7

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    .line 204
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->c:Landroid/preference/EditTextPreference;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->j:Landroid/database/Cursor;

    const/4 v2, 0x2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->d:Landroid/preference/EditTextPreference;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->j:Landroid/database/Cursor;

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->e:Landroid/preference/EditTextPreference;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->j:Landroid/database/Cursor;

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public static synthetic c(Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->j:Landroid/database/Cursor;

    return-object v0
.end method

.method private c()Ljava/lang/String;
    .locals 4

    .prologue
    .line 344
    const/4 v0, 0x0

    .line 346
    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->b:Landroid/preference/EditTextPreference;

    invoke-virtual {v1}, Landroid/preference/EditTextPreference;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 347
    iget-object v2, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->d:Landroid/preference/EditTextPreference;

    invoke-virtual {v2}, Landroid/preference/EditTextPreference;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 348
    iget-object v3, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->e:Landroid/preference/EditTextPreference;

    invoke-virtual {v3}, Landroid/preference/EditTextPreference;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 350
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_1

    .line 351
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->m:Landroid/content/res/Resources;

    sget v1, Lh;->cD:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 358
    :cond_0
    :goto_0
    return-object v0

    .line 352
    :cond_1
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_2

    .line 353
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->m:Landroid/content/res/Resources;

    sget v1, Lh;->cF:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 354
    :cond_2
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v1

    const v2, 0xfffe

    and-int/2addr v1, v2

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    .line 355
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->m:Landroid/content/res/Resources;

    sget v1, Lh;->cG:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 415
    if-eqz p0, :cond_0

    sget-object v0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 416
    :cond_0
    const-string p0, ""

    .line 418
    :cond_1
    return-object p0
.end method

.method public static synthetic d(Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->b()V

    return-void
.end method

.method public static synthetic e(Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;)Landroid/preference/EditTextPreference;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->f:Landroid/preference/EditTextPreference;

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;)Landroid/preference/EditTextPreference;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->g:Landroid/preference/EditTextPreference;

    return-object v0
.end method

.method public static synthetic g(Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;)Landroid/preference/EditTextPreference;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->c:Landroid/preference/EditTextPreference;

    return-object v0
.end method

.method public static synthetic h(Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->h:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic i(Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->i:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic j(Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;)Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->k:Z

    return v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 90
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 92
    sget v0, Lf;->hY:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->addPreferencesFromResource(I)V

    .line 94
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v3, Lh;->p:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->a:Ljava/lang/String;

    .line 95
    const-string v0, "apn_name"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/EditTextPreference;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->b:Landroid/preference/EditTextPreference;

    .line 96
    const-string v0, "apn_mms_proxy"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/EditTextPreference;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->f:Landroid/preference/EditTextPreference;

    .line 97
    const-string v0, "apn_mms_port"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/EditTextPreference;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->g:Landroid/preference/EditTextPreference;

    .line 98
    const-string v0, "apn_mmsc"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/EditTextPreference;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->c:Landroid/preference/EditTextPreference;

    .line 99
    const-string v0, "apn_mcc"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/EditTextPreference;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->d:Landroid/preference/EditTextPreference;

    .line 100
    const-string v0, "apn_mnc"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/EditTextPreference;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->e:Landroid/preference/EditTextPreference;

    .line 102
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->m:Landroid/content/res/Resources;

    .line 104
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 106
    if-nez p1, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->l:Z

    .line 107
    const-string v0, "apn_row_id"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->n:Ljava/lang/String;

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->n:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->k:Z

    .line 110
    invoke-static {}, Lxw;->a()Lxw;

    move-result-object v0

    invoke-virtual {v0}, Lxw;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->p:Landroid/database/sqlite/SQLiteDatabase;

    .line 112
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->k:Z

    if-eqz v0, :cond_3

    .line 113
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->b()V

    .line 141
    :goto_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v0, v2, :cond_0

    .line 142
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 144
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 106
    goto :goto_0

    :cond_2
    move v0, v2

    .line 108
    goto :goto_1

    .line 116
    :cond_3
    new-instance v0, Lavq;

    invoke-direct {v0, p0}, Lavq;-><init>(Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;)V

    new-array v3, v1, [Ljava/lang/Void;

    const/4 v4, 0x0

    aput-object v4, v3, v2

    .line 138
    invoke-virtual {v0, v3}, Lavq;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_2
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 364
    if-nez p1, :cond_0

    .line 365
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->c()Ljava/lang/String;

    move-result-object v0

    .line 367
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x104000a

    const/4 v3, 0x0

    .line 368
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 369
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 370
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 373
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 226
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 228
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->k:Z

    if-nez v0, :cond_0

    .line 229
    sget v0, Lh;->gm:I

    invoke-interface {p1, v2, v3, v2, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->bP:I

    .line 230
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 232
    :cond_0
    const/4 v0, 0x2

    sget v1, Lh;->gx:I

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x108004e

    .line 233
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 234
    const/4 v0, 0x3

    sget v1, Lh;->go:I

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x1080038

    .line 235
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 236
    return v3
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 156
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->j:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->j:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 159
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->j:Landroid/database/Cursor;

    .line 161
    :cond_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 265
    packed-switch p1, :pswitch_data_0

    .line 273
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 267
    :pswitch_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 268
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->finish()V

    .line 270
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 265
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x1

    .line 241
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 260
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :cond_0
    :goto_0
    return v0

    .line 243
    :sswitch_0
    new-instance v1, Lavs;

    invoke-direct {v1, p0}, Lavs;-><init>(Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;)V

    new-array v2, v0, [Ljava/lang/Void;

    const/4 v3, 0x0

    aput-object v3, v2, v4

    invoke-virtual {v1, v2}, Lavs;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->finish()V

    goto :goto_0

    .line 247
    :sswitch_1
    invoke-direct {p0, v4}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->a(Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 248
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->finish()V

    goto :goto_0

    .line 253
    :sswitch_2
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->finish()V

    goto :goto_0

    .line 257
    :sswitch_3
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->onBackPressed()V

    goto :goto_0

    .line 241
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x102002c -> :sswitch_3
    .end sparse-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 172
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 173
    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 174
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPause()V

    .line 175
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 219
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    .line 221
    const/4 v0, 0x1

    return v0
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 1

    .prologue
    .line 378
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->onPrepareDialog(ILandroid/app/Dialog;)V

    .line 380
    if-nez p1, :cond_0

    .line 381
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->c()Ljava/lang/String;

    move-result-object v0

    .line 383
    if-eqz v0, :cond_0

    .line 384
    check-cast p2, Landroid/app/AlertDialog;

    invoke-virtual {p2, v0}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 387
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 165
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 166
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 167
    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 168
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 278
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 279
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->j:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 280
    const-string v0, "pos"

    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->j:Landroid/database/Cursor;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 282
    :cond_0
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 423
    invoke-virtual {p0, p2}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 424
    if-eqz v0, :cond_0

    .line 425
    const-string v1, ""

    invoke-interface {p1, p2, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/hangouts/phone/ApnEditorActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 427
    :cond_0
    return-void
.end method
