.class public Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;
.super Landroid/widget/RelativeLayout;
.source "PG"

# interfaces
.implements Laac;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Lyj;

.field private e:Lcom/google/android/apps/hangouts/views/FadeImageView;

.field private f:Lzx;

.field private g:Ljava/lang/String;

.field private h:Lbzn;

.field private i:Z

.field private j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2681
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2682
    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2775
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->f:Lzx;

    if-eqz v0, :cond_0

    .line 2776
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->f:Lzx;

    invoke-virtual {v0}, Lzx;->b()V

    .line 2777
    iput-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->f:Lzx;

    .line 2779
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->e:Lcom/google/android/apps/hangouts/views/FadeImageView;

    const/4 v1, 0x0

    invoke-static {}, Lyn;->c()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/hangouts/views/FadeImageView;->a(ZLandroid/graphics/Bitmap;)V

    .line 2780
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->h:Lbzn;

    if-eqz v0, :cond_1

    .line 2781
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->h:Lbzn;

    invoke-virtual {v0}, Lbzn;->b()V

    .line 2782
    iput-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->h:Lbzn;

    .line 2784
    :cond_1
    iput-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->g:Ljava/lang/String;

    .line 2785
    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    .line 2768
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2769
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->d:Lyj;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->a:Ljava/lang/String;

    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->j:Z

    iget-boolean v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->i:Z

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ljava/lang/String;ZZZ)I

    .line 2772
    :cond_0
    return-void
.end method

.method public a(Lbzn;Lbyd;ZLzx;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2736
    invoke-static {p2}, Lcwz;->a(Ljava/lang/Object;)V

    .line 2737
    invoke-static {}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2738
    const-string v2, "Babel_medialoader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "InviteListItem setImageBitmap "

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p1, :cond_2

    move-object v0, v1

    .line 2739
    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "gifImage="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez p2, :cond_3

    move-object v0, v1

    .line 2740
    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " success="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " loadedFromCache="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2738
    invoke-static {v2, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2744
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->f:Lzx;

    if-eq v0, p4, :cond_4

    .line 2746
    if-eqz p1, :cond_1

    .line 2747
    invoke-virtual {p1}, Lbzn;->b()V

    .line 2760
    :cond_1
    :goto_2
    return-void

    .line 2739
    :cond_2
    invoke-virtual {p1}, Lbzn;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2740
    :cond_3
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2751
    :cond_4
    iput-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->f:Lzx;

    .line 2753
    if-eqz p3, :cond_1

    .line 2756
    iput-object p1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->h:Lbzn;

    .line 2758
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->e:Lcom/google/android/apps/hangouts/views/FadeImageView;

    if-nez p5, :cond_5

    const/4 v0, 0x1

    :goto_3
    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->h:Lbzn;

    invoke-virtual {v2}, Lbzn;->e()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/hangouts/views/FadeImageView;->a(ZLandroid/graphics/Bitmap;)V

    goto :goto_2

    :cond_5
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public a(Ljava/lang/String;Lyj;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2708
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2709
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->b()V

    .line 2730
    :cond_0
    :goto_0
    return-void

    .line 2712
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->g:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2716
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->b()V

    .line 2717
    iput-object p1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->g:Ljava/lang/String;

    .line 2719
    new-instance v0, Lzx;

    new-instance v1, Lbyq;

    invoke-direct {v1, p1, p2}, Lbyq;-><init>(Ljava/lang/String;Lyj;)V

    .line 2721
    invoke-static {}, Lyn;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Lbyq;->a(I)Lbyq;

    move-result-object v1

    .line 2722
    invoke-virtual {v1, v3}, Lbyq;->d(Z)Lbyq;

    move-result-object v1

    .line 2723
    invoke-virtual {v1, v3}, Lbyq;->b(Z)Lbyq;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->a:Ljava/lang/String;

    invoke-direct {v0, v1, p0, v3, v2}, Lzx;-><init>(Lbyq;Laac;ZLjava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->f:Lzx;

    .line 2726
    invoke-static {}, Lbsn;->c()Lbsn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->f:Lzx;

    invoke-virtual {v0, v1}, Lbsn;->a(Lbrv;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2728
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->f:Lzx;

    goto :goto_0
.end method

.method public a(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2691
    iput-object p1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->d:Lyj;

    .line 2692
    iput-object p2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->a:Ljava/lang/String;

    .line 2693
    iput-object p3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->b:Ljava/lang/String;

    .line 2694
    iput-object p4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->c:Ljava/lang/String;

    .line 2695
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 2792
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->b()V

    .line 2793
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 2794
    return-void
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 2686
    sget v0, Lg;->G:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/FadeImageView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment$InviteListItem;->e:Lcom/google/android/apps/hangouts/views/FadeImageView;

    .line 2687
    return-void
.end method
