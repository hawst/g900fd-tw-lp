.class public Lcom/google/android/apps/hangouts/listui/SwipeableListView;
.super Landroid/widget/ListView;
.source "PG"

# interfaces
.implements Lauv;


# instance fields
.field private a:Lauq;

.field private b:Z

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 20
    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->b:Z

    .line 22
    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->c:Z

    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->a(Landroid/content/Context;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 30
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->b:Z

    .line 22
    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->c:Z

    .line 31
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->a(Landroid/content/Context;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 20
    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->b:Z

    .line 22
    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->c:Z

    .line 36
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->a(Landroid/content/Context;)V

    .line 37
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 45
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v4, v0, Landroid/util/DisplayMetrics;->density:F

    .line 46
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v0

    int-to-float v5, v0

    .line 47
    new-instance v0, Lauq;

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v2

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lauq;-><init>(Landroid/content/res/Resources;Landroid/view/VelocityTracker;Lauv;FF)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->a:Lauq;

    .line 49
    return-void
.end method


# virtual methods
.method public a(Landroid/view/MotionEvent;)Landroid/view/View;
    .locals 6

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->getChildCount()I

    move-result v2

    .line 122
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v3, v0

    .line 123
    const/4 v0, 0x0

    move v1, v0

    .line 125
    :goto_0
    if-ge v1, v2, :cond_1

    .line 126
    invoke-virtual {p0, v1}, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 127
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_0

    .line 128
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v4

    if-lt v3, v4, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v4

    if-gt v3, v4, :cond_0

    .line 134
    :goto_1
    return-object v0

    .line 125
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 134
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a()V
    .locals 2

    .prologue
    .line 64
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xc

    if-lt v0, v1, :cond_0

    .line 65
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->b:Z

    .line 67
    :cond_0
    return-void
.end method

.method public a(Lauw;)Z
    .locals 1

    .prologue
    .line 139
    invoke-interface {p1}, Lauw;->b()Z

    move-result v0

    return v0
.end method

.method public b(Lauw;)V
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->requestDisallowInterceptTouchEvent(Z)V

    .line 169
    invoke-interface {p1}, Lauw;->d()V

    .line 170
    return-void
.end method

.method public c(Lauw;)V
    .locals 0

    .prologue
    .line 144
    if-eqz p1, :cond_0

    .line 145
    invoke-interface {p1}, Lauw;->c()V

    .line 147
    :cond_0
    return-void
.end method

.method public d(Lauw;)V
    .locals 0

    .prologue
    .line 175
    invoke-interface {p1}, Lauw;->e()V

    .line 176
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 53
    invoke-super {p0, p1}, Landroid/widget/ListView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 54
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 55
    iget-object v1, p0, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->a:Lauq;

    invoke-virtual {v1, v0}, Lauq;->a(F)V

    .line 56
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v0

    int-to-float v0, v0

    .line 57
    iget-object v1, p0, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->a:Lauq;

    invoke-virtual {v1, v0}, Lauq;->b(F)V

    .line 58
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v1, 0x1

    .line 71
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->b:Z

    if-eqz v0, :cond_4

    .line 82
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->c:Z

    if-eqz v0, :cond_2

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->a:Lauq;

    invoke-virtual {v0, p1}, Lauq;->b(Landroid/view/MotionEvent;)Z

    move v0, v1

    .line 107
    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-eq v2, v1, :cond_0

    .line 108
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v3, :cond_1

    .line 110
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->c:Z

    .line 114
    :cond_1
    :goto_1
    return v0

    .line 88
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->a:Lauq;

    invoke-virtual {v0, p1}, Lauq;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 89
    if-eqz v0, :cond_3

    .line 90
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->c:Z

    .line 96
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 97
    invoke-virtual {v0, v3}, Landroid/view/MotionEvent;->setAction(I)V

    .line 98
    invoke-super {p0, v0}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 100
    iget-object v0, p0, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->a:Lauq;

    invoke-virtual {v0, p1}, Lauq;->b(Landroid/view/MotionEvent;)Z

    move v0, v1

    .line 102
    goto :goto_0

    .line 104
    :cond_3
    invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 114
    :cond_4
    invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_1
.end method
