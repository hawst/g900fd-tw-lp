.class public Lcom/google/android/apps/hangouts/hangout/renderer/GLView;
.super Landroid/opengl/GLSurfaceView;
.source "PG"

# interfaces
.implements Lapu;


# static fields
.field public static final a:Z


# instance fields
.field private b:Lapv;

.field private c:Landroid/view/ViewGroup;

.field private d:Lauj;

.field private final e:Latd;

.field private f:Latt;

.field private g:Lcom/google/android/libraries/hangouts/video/RendererManager;

.field private h:Lcom/google/android/libraries/hangouts/video/DecoderManager;

.field private final i:Landroid/os/Handler;

.field private j:Laqv;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 126
    sget-object v0, Lbys;->e:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->a:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 140
    invoke-direct {p0, p1}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;)V

    .line 131
    new-instance v0, Latd;

    invoke-direct {v0, p0}, Latd;-><init>(Lcom/google/android/apps/hangouts/hangout/renderer/GLView;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->e:Latd;

    .line 136
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->i:Landroid/os/Handler;

    .line 141
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 144
    invoke-direct {p0, p1, p2}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 131
    new-instance v0, Latd;

    invoke-direct {v0, p0}, Latd;-><init>(Lcom/google/android/apps/hangouts/hangout/renderer/GLView;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->e:Latd;

    .line 136
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->i:Landroid/os/Handler;

    .line 145
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/hangout/renderer/GLView;)Lapv;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->b:Lapv;

    return-object v0
.end method

.method public static b(I)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 237
    packed-switch p0, :pswitch_data_0

    .line 247
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Bad rotation "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    :goto_0
    :pswitch_0
    return v0

    .line 241
    :pswitch_1
    const/16 v0, 0x5a

    goto :goto_0

    .line 243
    :pswitch_2
    const/16 v0, 0xb4

    goto :goto_0

    .line 245
    :pswitch_3
    const/16 v0, 0x10e

    goto :goto_0

    .line 237
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/hangout/renderer/GLView;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->i:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/apps/hangouts/hangout/renderer/GLView;)Lcom/google/android/libraries/hangouts/video/RendererManager;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->g:Lcom/google/android/libraries/hangouts/video/RendererManager;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/apps/hangouts/hangout/renderer/GLView;)Lcom/google/android/libraries/hangouts/video/DecoderManager;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->h:Lcom/google/android/libraries/hangouts/video/DecoderManager;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/apps/hangouts/hangout/renderer/GLView;)Lauj;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->d:Lauj;

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/apps/hangouts/hangout/renderer/GLView;)Laqv;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->j:Laqv;

    return-object v0
.end method

.method public static synthetic g(Lcom/google/android/apps/hangouts/hangout/renderer/GLView;)Latt;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->f:Latt;

    return-object v0
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 205
    return-void
.end method

.method public a(Lapv;)V
    .locals 1

    .prologue
    .line 190
    iput-object p1, p0, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->b:Lapv;

    .line 191
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->f:Latt;

    invoke-virtual {v0}, Latt;->a()V

    .line 196
    invoke-super {p0}, Landroid/opengl/GLSurfaceView;->onResume()V

    .line 197
    return-void
.end method

.method public a(Laqv;Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 148
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->setEGLContextClientVersion(I)V

    .line 149
    iput-object p2, p0, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->c:Landroid/view/ViewGroup;

    .line 150
    new-instance v0, Lcom/google/android/libraries/hangouts/video/RendererManager;

    invoke-direct {v0}, Lcom/google/android/libraries/hangouts/video/RendererManager;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->g:Lcom/google/android/libraries/hangouts/video/RendererManager;

    .line 151
    new-instance v0, Lcom/google/android/libraries/hangouts/video/DecoderManager;

    invoke-direct {v0}, Lcom/google/android/libraries/hangouts/video/DecoderManager;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->h:Lcom/google/android/libraries/hangouts/video/DecoderManager;

    .line 153
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->f:Latt;

    if-eqz v0, :cond_0

    .line 154
    const-string v0, "Babel"

    const-string v1, "GLView.initialize called twice!"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    :cond_0
    new-instance v0, Lauj;

    invoke-direct {v0}, Lauj;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->d:Lauj;

    .line 158
    iput-object p1, p0, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->j:Laqv;

    .line 161
    new-instance v0, Latt;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->e:Latd;

    invoke-direct {v0, v1}, Latt;-><init>(Latd;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->f:Latt;

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->f:Latt;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V

    .line 164
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->setRenderMode(I)V

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->j:Laqv;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->d:Lauj;

    invoke-virtual {v0, v1}, Laqv;->a(Lauj;)V

    .line 169
    new-instance v0, Lata;

    invoke-direct {v0, p0}, Lata;-><init>(Lcom/google/android/apps/hangouts/hangout/renderer/GLView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 177
    sget-boolean v0, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->a:Z

    if-eqz v0, :cond_1

    .line 178
    new-instance v0, Latb;

    invoke-direct {v0, p0}, Latb;-><init>(Lcom/google/android/apps/hangouts/hangout/renderer/GLView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 186
    :cond_1
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->f:Latt;

    invoke-virtual {v0}, Latt;->h()V

    .line 201
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->f:Latt;

    invoke-virtual {v0}, Latt;->i()V

    .line 214
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->f:Latt;

    invoke-virtual {v0}, Latt;->j()V

    .line 228
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->g:Lcom/google/android/libraries/hangouts/video/RendererManager;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/RendererManager;->release()V

    .line 229
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->h:Lcom/google/android/libraries/hangouts/video/DecoderManager;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/DecoderManager;->release()V

    .line 230
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->f:Latt;

    invoke-virtual {v0}, Latt;->k()V

    .line 234
    return-void
.end method

.method public m_()V
    .locals 1

    .prologue
    .line 221
    invoke-super {p0}, Landroid/opengl/GLSurfaceView;->onPause()V

    .line 223
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->f:Latt;

    invoke-virtual {v0}, Latt;->b()V

    .line 224
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->f:Latt;

    invoke-virtual {v0, p1}, Latt;->a(Landroid/content/res/Configuration;)V

    .line 210
    return-void
.end method
