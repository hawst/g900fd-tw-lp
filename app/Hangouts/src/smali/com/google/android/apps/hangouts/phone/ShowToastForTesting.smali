.class public Lcom/google/android/apps/hangouts/phone/ShowToastForTesting;
.super Landroid/app/Activity;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 15
    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .prologue
    .line 19
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 20
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/ShowToastForTesting;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "toast_text"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 21
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 22
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/ShowToastForTesting;->finish()V

    .line 23
    return-void
.end method
