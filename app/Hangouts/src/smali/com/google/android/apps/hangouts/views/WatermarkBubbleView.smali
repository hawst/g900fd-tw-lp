.class public Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:Landroid/widget/ImageView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:La;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 17
    iput v0, p0, Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;->a:I

    .line 18
    iput v0, p0, Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;->b:I

    .line 19
    iput v0, p0, Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;->c:I

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;->g:La;

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;->a:I

    .line 33
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;->setOrientation(I)V

    .line 34
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 69
    iget v0, p0, Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;->a:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;->b:I

    if-lez v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;->d:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v1, p0, Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;->a:I

    iget v2, p0, Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;->b:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 73
    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 60
    iput p1, p0, Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;->a:I

    .line 61
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;->a()V

    .line 62
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 49
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;->f:Landroid/widget/TextView;

    if-eqz p2, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 54
    invoke-static {v0, p1}, Lf;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;->f:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lf;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 57
    return-void

    .line 49
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 38
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 39
    sget v0, Lg;->q:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;->d:Landroid/widget/ImageView;

    .line 40
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;->d:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;->b:I

    .line 41
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;->d:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;->c:I

    .line 42
    sget v0, Lg;->eH:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;->e:Landroid/widget/TextView;

    .line 43
    sget v0, Lg;->hE:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;->f:Landroid/widget/TextView;

    .line 44
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;->a()V

    .line 45
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;->g:La;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/WatermarkBubbleView;->g:La;

    .line 84
    :cond_0
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 85
    return-void
.end method
