.class public Lcom/google/android/apps/hangouts/phone/PackageReplacedReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# static fields
.field private static final a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lbys;->g:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/hangouts/phone/PackageReplacedReceiver;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 210
    invoke-static {}, Lcom/google/android/apps/hangouts/sms/SmsReceiver;->a()V

    .line 212
    invoke-static {}, Lbld;->j()V

    .line 214
    invoke-static {p0}, Lf;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 216
    invoke-static {}, Lbkb;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 217
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v2

    .line 218
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lyj;->u()Z

    move-result v3

    if-nez v3, :cond_0

    .line 219
    invoke-virtual {v2}, Lyj;->ab()I

    move-result v2

    if-nez v2, :cond_0

    .line 220
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Ljava/lang/String;)I

    goto :goto_0

    .line 224
    :cond_1
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 12

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 57
    sget-boolean v0, Lcom/google/android/apps/hangouts/phone/PackageReplacedReceiver;->a:Z

    if-eqz v0, :cond_0

    .line 58
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "handlePackageChanged action:"

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " packageName:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    :cond_0
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v6

    .line 64
    const-string v0, "com.google.android.gms"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 65
    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    .line 67
    if-eqz v0, :cond_2

    const-string v1, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 69
    invoke-static {v2, v2, v3}, Lf;->a(ZZZ)I

    .line 206
    :cond_1
    :goto_0
    return-void

    .line 73
    :cond_2
    if-eqz v0, :cond_15

    .line 74
    const-string v0, "Babel"

    const-string v1, "Gms package replaced. Will trigger a restart of babel"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->d()Lcom/google/android/apps/hangouts/phone/EsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->j()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 79
    const-string v0, "Babel"

    const-string v1, "Skip forced warm sync since previous gmscore is valid."

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 82
    :goto_1
    invoke-static {v2}, Lakn;->b(Z)V

    .line 84
    const-string v1, "Babel"

    const-string v4, "Clearing account states after gmscore upgraded"

    invoke-static {v1, v4}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v4, v0

    .line 85
    invoke-static {}, Lbkb;->w()V

    .line 90
    :goto_2
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->d()Lcom/google/android/apps/hangouts/phone/EsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->m()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 92
    sget-boolean v0, Lcom/google/android/apps/hangouts/phone/PackageReplacedReceiver;->a:Z

    if-eqz v0, :cond_3

    .line 93
    const-string v0, "Babel"

    const-string v1, "First upgrade from Talk"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    :cond_3
    new-instance v0, Lbwv;

    invoke-direct {v0}, Lbwv;-><init>()V

    .line 98
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lbwv;->a(Landroid/content/ContentResolver;)Ljava/util/List;

    move-result-object v8

    .line 101
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwx;

    .line 102
    iget-boolean v0, v0, Lbwx;->c:Z

    if-eqz v0, :cond_4

    move v5, v2

    .line 111
    :goto_3
    invoke-static {v2}, Lbkb;->g(Z)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_5
    :goto_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 113
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_6
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_12

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbwx;

    .line 114
    iget-boolean v11, v1, Lbwx;->c:Z

    if-eqz v11, :cond_6

    iget-object v1, v1, Lbwx;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 116
    sget-boolean v1, Lcom/google/android/apps/hangouts/phone/PackageReplacedReceiver;->a:Z

    if-eqz v1, :cond_7

    .line 117
    const-string v1, "Babel"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Found account "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " signed in with Talk."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v1, v10}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    move v1, v2

    .line 124
    :goto_5
    if-nez v1, :cond_5

    .line 125
    sget-boolean v1, Lcom/google/android/apps/hangouts/phone/PackageReplacedReceiver;->a:Z

    if-eqz v1, :cond_8

    .line 126
    const-string v1, "Babel"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Account "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " not in Talk; signing out of Babel."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v1, v10}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    :cond_8
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    .line 130
    if-eqz v0, :cond_5

    .line 131
    invoke-static {v0, v3}, Lbkb;->b(Lyj;Z)V

    goto :goto_4

    .line 135
    :cond_9
    if-eqz v5, :cond_b

    .line 137
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->h()I

    .line 142
    :goto_6
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->d()Lcom/google/android/apps/hangouts/phone/EsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->n()V

    .line 153
    :cond_a
    const-string v0, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    move v0, v3

    .line 157
    :goto_7
    invoke-static {v0, v2}, Lf;->a(ZZ)Z

    move-result v0

    if-nez v0, :cond_c

    .line 158
    const-string v0, "Babel"

    const-string v1, "GmsCore check failed during package upgrade"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 140
    :cond_b
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->d()Lcom/google/android/apps/hangouts/phone/EsApplication;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/hangouts/phone/EsApplication;->b(Z)V

    goto :goto_6

    .line 165
    :cond_c
    sget-boolean v0, Lcom/google/android/apps/hangouts/phone/PackageReplacedReceiver;->a:Z

    if-eqz v0, :cond_d

    .line 166
    const-string v0, "Babel"

    const-string v1, "[PackageReplaced] sign-in to accounts"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    :cond_d
    invoke-static {v2}, Lbkb;->a(Z)V

    .line 170
    if-eqz v7, :cond_e

    .line 171
    const-string v0, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    move v4, v2

    .line 179
    :cond_e
    :goto_8
    invoke-static {}, Lbrc;->b()V

    .line 180
    invoke-static {}, Lbrc;->a()V

    .line 185
    invoke-static {}, Lbkb;->t()V

    .line 187
    if-nez v4, :cond_1

    .line 192
    const-string v0, "Babel"

    const-string v1, "Force warm sync for all signed in accounts after package upgrades"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    invoke-static {v2}, Lbkb;->f(Z)Ljava/util/ArrayList;

    move-result-object v0

    .line 194
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_f
    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 195
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    .line 197
    if-eqz v0, :cond_f

    .line 198
    invoke-static {v0, v3, v3, v3, v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ZZIZ)V

    goto :goto_9

    .line 175
    :cond_10
    invoke-static {v6}, Lcom/google/android/apps/hangouts/phone/PackageReplacedReceiver;->a(Landroid/content/Context;)V

    goto :goto_8

    :cond_11
    move v0, v2

    goto :goto_7

    :cond_12
    move v1, v3

    goto/16 :goto_5

    :cond_13
    move v5, v3

    goto/16 :goto_3

    :cond_14
    move v0, v3

    goto/16 :goto_1

    :cond_15
    move v4, v3

    goto/16 :goto_2
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 36
    sget-boolean v0, Lcom/google/android/apps/hangouts/phone/PackageReplacedReceiver;->a:Z

    if-eqz v0, :cond_0

    .line 37
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PackageReplacedReceiver "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    .line 40
    const-string v1, "com.google.android.gms"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 41
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 42
    if-nez v1, :cond_2

    if-nez v2, :cond_2

    .line 53
    :cond_1
    :goto_0
    return-void

    .line 46
    :cond_2
    if-nez v1, :cond_3

    const-string v1, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 51
    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 50
    invoke-static {v1, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
