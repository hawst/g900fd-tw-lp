.class public Lcom/google/android/apps/hangouts/hangout/HangoutActivity;
.super Lakn;
.source "PG"


# instance fields
.field final r:Landroid/content/BroadcastReceiver;

.field private s:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

.field private t:Z

.field private u:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lakn;-><init>()V

    .line 47
    new-instance v0, Lapr;

    invoke-direct {v0, p0}, Lapr;-><init>(Lcom/google/android/apps/hangouts/hangout/HangoutActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->r:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private s()V
    .locals 2

    .prologue
    .line 163
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->k()Lyj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->s:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->q()Ljava/lang/String;

    move-result-object v1

    .line 162
    invoke-static {v0, v1}, Lbbl;->a(Lyj;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 167
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->startActivity(Landroid/content/Intent;)V

    .line 168
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->finish()V

    .line 169
    sget v0, Lf;->be:I

    sget v1, Lf;->bf:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->overridePendingTransition(II)V

    .line 170
    return-void
.end method


# virtual methods
.method public a(Lt;)V
    .locals 1

    .prologue
    .line 115
    instance-of v0, p1, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    if-eqz v0, :cond_0

    .line 116
    check-cast p1, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    iput-object p1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->s:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    .line 118
    :cond_0
    return-void
.end method

.method protected a(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 129
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 130
    const v2, 0x102002c

    if-ne v1, v2, :cond_1

    .line 131
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->o:Lbme;

    invoke-interface {v1}, Lbme;->a()Laux;

    move-result-object v1

    const/16 v2, 0x631

    .line 132
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 131
    invoke-virtual {v1, v2}, Laux;->a(Ljava/lang/Integer;)V

    .line 133
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->s()V

    .line 140
    :cond_0
    :goto_0
    return v0

    .line 135
    :cond_1
    sget v2, Lg;->es:I

    if-eq v1, v2, :cond_0

    .line 140
    invoke-super {p0, p1}, Lakn;->a(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public k()Lyj;
    .locals 1

    .prologue
    .line 237
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->o()Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getAccountName()Ljava/lang/String;

    move-result-object v0

    .line 236
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    return-object v0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 154
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->t:Z

    return v0
.end method

.method public o()Lcom/google/android/libraries/hangouts/video/HangoutRequest;
    .locals 2

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "hangout_room_info"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    return-object v0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->s:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 196
    :goto_0
    return-void

    .line 186
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->k()Lyj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->s:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->q()Ljava/lang/String;

    move-result-object v1

    .line 185
    invoke-static {v0, v1}, Lbbl;->a(Lyj;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 187
    invoke-static {p0, v0}, Laz;->a(Landroid/app/Activity;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 190
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->finish()V

    goto :goto_0

    .line 194
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->s()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 57
    invoke-super {p0, p1}, Lakn;->onCreate(Landroid/os/Bundle;)V

    .line 63
    invoke-static {}, Lape;->c()Ljava/lang/Boolean;

    move-result-object v0

    if-nez v0, :cond_0

    .line 64
    invoke-static {p1}, Lcwz;->b(Ljava/lang/Object;)V

    .line 68
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->k()Lyj;

    move-result-object v0

    invoke-static {v0}, Lbbl;->b(Lyj;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->startActivity(Landroid/content/Intent;)V

    .line 69
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->finish()V

    .line 97
    :goto_0
    return-void

    .line 73
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    invoke-static {}, Lbxm;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 74
    const-string v0, "Device has NFC. Adding NfcHangoutFragment."

    invoke-static {v0}, Lf;->j(Ljava/lang/String;)V

    .line 75
    new-instance v0, Larr;

    invoke-direct {v0}, Larr;-><init>()V

    .line 76
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->e()Lae;

    move-result-object v1

    .line 77
    invoke-virtual {v1}, Lae;->a()Lao;

    move-result-object v1

    .line 78
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lao;->a(Lt;Ljava/lang/String;)Lao;

    move-result-object v0

    invoke-virtual {v0}, Lao;->b()I

    .line 81
    :cond_1
    sget v0, Lf;->fn:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->setContentView(I)V

    .line 83
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x688080

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 90
    if-eqz p1, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->t:Z

    .line 94
    invoke-static {p0}, Ldj;->a(Landroid/content/Context;)Ldj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->r:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.google.android.apps.hangouts.phone.notify_external_interruption"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Ldj;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    goto :goto_0

    .line 90
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 122
    invoke-super {p0}, Lakn;->onDestroy()V

    .line 123
    invoke-static {p0}, Ldj;->a(Landroid/content/Context;)Ldj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->r:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Ldj;->a(Landroid/content/BroadcastReceiver;)V

    .line 125
    return-void
.end method

.method public onMenuOpened(ILandroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 145
    if-eqz p2, :cond_0

    .line 146
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->o:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v1, 0x630

    .line 147
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 146
    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    .line 149
    :cond_0
    invoke-super {p0, p1, p2}, Lakn;->onMenuOpened(ILandroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onRestart()V
    .locals 1

    .prologue
    .line 109
    invoke-super {p0}, Lakn;->onRestart()V

    .line 110
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->t:Z

    .line 111
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 101
    invoke-super {p0}, Lakn;->onStart()V

    .line 104
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->a(Landroid/content/Intent;)V

    .line 105
    return-void
.end method

.method public overridePendingTransition(II)V
    .locals 0

    .prologue
    .line 231
    invoke-super {p0, p1, p2}, Lakn;->overridePendingTransition(II)V

    .line 232
    return-void
.end method

.method public p()V
    .locals 3

    .prologue
    .line 200
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->u:Z

    if-eqz v0, :cond_0

    .line 226
    :goto_0
    return-void

    .line 203
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->u:Z

    .line 204
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "hangout_pstn_call"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 208
    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 212
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->k()Lyj;

    move-result-object v0

    invoke-static {v0}, Lbbl;->b(Lyj;)Landroid/content/Intent;

    move-result-object v0

    .line 223
    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->startActivity(Landroid/content/Intent;)V

    .line 224
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->finish()V

    .line 225
    sget v0, Lf;->be:I

    sget v1, Lf;->bf:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->overridePendingTransition(II)V

    goto :goto_0

    .line 214
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->k()Lyj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->s:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    .line 215
    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->f()Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getConversationId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 214
    invoke-static {v0, v1, v2}, Lbbl;->a(Lyj;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 217
    const-string v1, "conversation_opened_impression"

    const/16 v2, 0x667

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1
.end method

.method public q()Lcom/google/android/apps/hangouts/hangout/HangoutFragment;
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->s:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    return-object v0
.end method

.method public r()V
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->s:Lcom/google/android/apps/hangouts/hangout/HangoutFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->f()Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->sendCallCompleteIntent()V

    .line 246
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->p()V

    .line 247
    return-void
.end method
