.class public Lcom/google/android/apps/hangouts/fragments/InvitationFragment;
.super Lakl;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Law;
.implements Lcfv;
.implements Lcfw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lakl;",
        "Landroid/view/View$OnClickListener;",
        "Law",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcfv;",
        "Lcfw;"
    }
.end annotation


# static fields
.field private static aj:Ljava/lang/String;

.field private static ak:Ljava/lang/String;

.field private static al:Ljava/lang/String;


# instance fields
.field private Y:Lcom/google/android/apps/hangouts/views/AvatarView;

.field private Z:Landroid/widget/ImageView;

.field private final a:Ljava/lang/String;

.field private aa:Landroid/widget/TextView;

.field private final ab:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private ac:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

.field private ad:Landroid/widget/Button;

.field private ae:Lcvi;

.field private af:Lcvo;

.field private ag:Lcom/google/android/apps/hangouts/views/FixedParticipantsGalleryView;

.field private ah:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbdk;",
            ">;"
        }
    .end annotation
.end field

.field private final ai:Lbor;

.field private final am:Lcvr;

.field private final an:Lbsb;

.field private final ao:Landroid/view/View$OnClickListener;

.field private b:Lbme;

.field private c:Lald;

.field private d:Lahm;

.field private e:Ljava/lang/String;

.field private f:Lbdk;

.field private g:Ljava/lang/String;

.field private h:Lyj;

.field private i:I

.field public mInviterCircleIds:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mMyCircles:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 285
    const-string v0, "inviter_circle_ids"

    sput-object v0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->aj:Ljava/lang/String;

    .line 286
    const-string v0, "my_circles_keys"

    sput-object v0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ak:Ljava/lang/String;

    .line 287
    const-string v0, "my_circles_vals"

    sput-object v0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->al:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 87
    invoke-direct {p0}, Lakl;-><init>()V

    .line 91
    const-string v0, "Babel"

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->a:Ljava/lang/String;

    .line 114
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ab:Ljava/util/ArrayList;

    .line 193
    new-instance v0, Lakv;

    invoke-direct {v0, p0}, Lakv;-><init>(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ai:Lbor;

    .line 657
    new-instance v0, Lala;

    invoke-direct {v0, p0}, Lala;-><init>(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->am:Lcvr;

    .line 679
    new-instance v0, Lalb;

    invoke-direct {v0, p0}, Lalb;-><init>(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->an:Lbsb;

    .line 720
    new-instance v0, Lalc;

    invoke-direct {v0, p0}, Lalc;-><init>(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ao:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)Lbdk;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->f:Lbdk;

    return-object v0
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 699
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->f:Lbdk;

    if-nez v0, :cond_0

    .line 714
    :goto_0
    return-void

    .line 702
    :cond_0
    iput-object v1, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->mMyCircles:Ljava/util/Map;

    .line 703
    iput-object v1, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->mInviterCircleIds:Ljava/util/HashSet;

    .line 704
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->b()V

    .line 705
    new-instance v0, Lcvm;

    invoke-direct {v0}, Lcvm;-><init>()V

    .line 706
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 707
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "g:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->f:Lbdk;

    iget-object v3, v3, Lbdk;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 708
    invoke-virtual {v0, v1}, Lcvm;->a(Ljava/util/Collection;)Lcvm;

    .line 709
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ae:Lcvi;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->am:Lcvr;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->h:Lyj;

    invoke-virtual {v3}, Lyj;->ak()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->h:Lyj;

    .line 710
    invoke-virtual {v4}, Lyj;->al()Ljava/lang/String;

    move-result-object v4

    .line 709
    invoke-virtual {v1, v2, v3, v4, v0}, Lcvi;->a(Lcvr;Ljava/lang/String;Ljava/lang/String;Lcvm;)V

    .line 712
    new-instance v0, Lcvo;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ae:Lcvi;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->h:Lyj;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->an:Lbsb;

    invoke-direct {v0, v1, v2, v3}, Lcvo;-><init>(Lcvi;Lyj;Lbsb;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->af:Lcvo;

    .line 713
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->af:Lcvo;

    invoke-virtual {v0}, Lcvo;->a()V

    goto :goto_0
.end method

.method private a(Lahm;)V
    .locals 2

    .prologue
    .line 181
    iput-object p1, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->d:Lahm;

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ac:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ac:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->d:Lahm;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lahm;)V

    .line 185
    :cond_0
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ab:Ljava/util/ArrayList;

    return-object v0
.end method

.method private b()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 770
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ad:Landroid/widget/Button;

    if-nez v0, :cond_0

    .line 801
    :goto_0
    return-void

    .line 774
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ad:Landroid/widget/Button;

    sget v1, Lh;->n:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 775
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->mMyCircles:Ljava/util/Map;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->mInviterCircleIds:Ljava/util/HashSet;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->mMyCircles:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 777
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ad:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 778
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ad:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 780
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ad:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 781
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ad:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 782
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->mInviterCircleIds:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 783
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ad:Landroid/widget/Button;

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->f:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 784
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ad:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lf;->cH:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 785
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ad:Landroid/widget/Button;

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->aO:I

    invoke-virtual {v0, v1, v5, v5, v5}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 787
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->mInviterCircleIds:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-ne v0, v3, :cond_3

    .line 788
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->mInviterCircleIds:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 789
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ad:Landroid/widget/Button;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->mMyCircles:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 791
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ad:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lh;->at:I

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->mInviterCircleIds:Ljava/util/HashSet;

    .line 792
    invoke-virtual {v4}, Ljava/util/HashSet;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    .line 791
    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 795
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ad:Landroid/widget/Button;

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->g:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 796
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ad:Landroid/widget/Button;

    invoke-virtual {v0, v5, v5, v5, v5}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 797
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ad:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lf;->cI:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    goto/16 :goto_0
.end method

.method public static synthetic c(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->Z:Landroid/widget/ImageView;

    return-object v0
.end method

.method private c()V
    .locals 4

    .prologue
    .line 804
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ah:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ag:Lcom/google/android/apps/hangouts/views/FixedParticipantsGalleryView;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->i:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 807
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ag:Lcom/google/android/apps/hangouts/views/FixedParticipantsGalleryView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/FixedParticipantsGalleryView;->setVisibility(I)V

    .line 808
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ag:Lcom/google/android/apps/hangouts/views/FixedParticipantsGalleryView;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->h:Lyj;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ah:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->f:Lbdk;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/hangouts/views/FixedParticipantsGalleryView;->a(Lyj;Ljava/util/List;Lbdk;)V

    .line 816
    :goto_0
    return-void

    .line 813
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ag:Lcom/google/android/apps/hangouts/views/FixedParticipantsGalleryView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/FixedParticipantsGalleryView;->setVisibility(I)V

    .line 814
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ag:Lcom/google/android/apps/hangouts/views/FixedParticipantsGalleryView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/FixedParticipantsGalleryView;->a()V

    goto :goto_0
.end method

.method public static synthetic d(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->e:Ljava/lang/String;

    return-object v0
.end method

.method private d(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 620
    if-nez p1, :cond_0

    .line 625
    :goto_0
    return-void

    .line 623
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 624
    sget v1, Lg;->bS:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const/16 v0, 0x8

    goto :goto_1
.end method

.method public static synthetic e(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)Lcom/google/android/apps/hangouts/fragments/ConversationFragment;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ac:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)Lyj;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->h:Lyj;

    return-object v0
.end method

.method public static synthetic g(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->i:I

    return v0
.end method

.method public static synthetic h(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->g:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic i(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)Lald;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->c:Lald;

    return-object v0
.end method

.method public static synthetic j(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->b()V

    return-void
.end method


# virtual methods
.method public getConversationId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 628
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getConversationType()I
    .locals 1

    .prologue
    .line 632
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->i:I

    return v0
.end method

.method public getInviterId()Lbdk;
    .locals 1

    .prologue
    .line 636
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->f:Lbdk;

    return-object v0
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/16 v1, 0x3e9

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 341
    const-string v0, "conversation_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->e:Ljava/lang/String;

    .line 342
    invoke-static {p1}, Lbbl;->a(Landroid/os/Bundle;)Lbdk;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->f:Lbdk;

    .line 343
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->f:Lbdk;

    if-nez v0, :cond_0

    .line 344
    new-instance v0, Ljava/security/InvalidParameterException;

    const-string v1, "InvitationFragment requires a valid inviter id"

    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 346
    :cond_0
    const-string v0, "account_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 347
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->h:Lyj;

    .line 348
    const-string v0, "conversation_type"

    invoke-virtual {p1, v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->i:I

    .line 351
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->getLoaderManager()Lav;

    move-result-object v0

    .line 352
    invoke-virtual {v0, v1}, Lav;->a(I)V

    .line 354
    invoke-virtual {v0, v1, v4, p0}, Lav;->a(ILandroid/os/Bundle;Law;)Ldg;

    move-result-object v0

    .line 355
    if-eqz v0, :cond_1

    .line 356
    invoke-virtual {v0}, Ldg;->p()V

    .line 359
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->d(Landroid/view/View;)V

    .line 361
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->f:Lbdk;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->f:Lbdk;

    iget-object v0, v0, Lbdk;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 362
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->h:Lyj;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->f:Lbdk;

    iget-object v1, v1, Lbdk;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lyj;Ljava/lang/String;)V

    .line 365
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 366
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->h:Lyj;

    const-string v1, "invite_timestamp"

    const-wide/16 v2, 0x0

    .line 367
    invoke-virtual {p1, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    .line 366
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lyj;J)V

    .line 370
    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->b()V

    .line 371
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->Z:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    .line 372
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->Z:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 374
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->Y:Lcom/google/android/apps/hangouts/views/AvatarView;

    if-eqz v0, :cond_5

    .line 375
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->Y:Lcom/google/android/apps/hangouts/views/AvatarView;

    invoke-virtual {v0, v4, v4}, Lcom/google/android/apps/hangouts/views/AvatarView;->a(Ljava/lang/String;Lyj;)V

    .line 377
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->aa:Landroid/widget/TextView;

    if-eqz v0, :cond_6

    .line 378
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->aa:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 380
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ab:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 381
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 384
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ae:Lcvi;

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->f:Lbdk;

    if-eqz v0, :cond_8

    .line 385
    new-instance v0, Lcvi;

    .line 386
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0, p0}, Lcvi;-><init>(Landroid/content/Context;Lcfv;Lcfw;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ae:Lcvi;

    .line 390
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ae:Lcvi;

    invoke-virtual {v0}, Lcvi;->a()V

    .line 392
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ae:Lcvi;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ae:Lcvi;

    invoke-virtual {v0}, Lcvi;->d()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 393
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->a()V

    .line 396
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->h:Lyj;

    if-eqz v0, :cond_b

    .line 397
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ac:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    if-eqz v0, :cond_a

    .line 399
    new-instance v0, Lahc;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->e:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->h:Lyj;

    iget v3, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->i:I

    invoke-direct {v0, v1, v2, v3}, Lahc;-><init>(Ljava/lang/String;Lyj;I)V

    .line 404
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ac:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lahc;Z)V

    .line 411
    :cond_a
    new-instance v0, Lakx;

    invoke-direct {v0, p0}, Lakx;-><init>(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)V

    new-array v1, v5, [Ljava/lang/Void;

    .line 437
    invoke-virtual {v0, v1}, Lakx;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 439
    :cond_b
    return-void
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 616
    const/4 v0, 0x0

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 744
    const/16 v0, 0x65

    if-ne p1, v0, :cond_1

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 746
    invoke-static {p3}, Lchf;->a(Landroid/content/Intent;)Ljava/util/ArrayList;

    move-result-object v0

    .line 748
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->mInviterCircleIds:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    .line 749
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    .line 750
    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->mInviterCircleIds:Ljava/util/HashSet;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 753
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ad:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 754
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ae:Lcvi;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ae:Lcvi;

    invoke-virtual {v0}, Lcvi;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 755
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->a()V

    .line 758
    :cond_1
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 462
    invoke-super {p0, p1}, Lakl;->onAttach(Landroid/app/Activity;)V

    .line 463
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 464
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->h:Lyj;

    .line 465
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->h:Lyj;

    if-nez v0, :cond_0

    .line 466
    const-string v0, "Babel"

    const-string v1, "Account is no longer valid, giving up invitation fragment"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    :cond_0
    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .prologue
    .line 819
    const/4 v0, 0x0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 568
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->h:Lyj;

    if-nez v0, :cond_1

    .line 609
    :cond_0
    :goto_0
    return-void

    .line 573
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 577
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lg;->fG:I

    if-ne v0, v1, :cond_2

    .line 578
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->h:Lyj;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->e:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, v3, v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ljava/lang/String;ZZZ)I

    .line 580
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->c:Lald;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->e:Ljava/lang/String;

    invoke-interface {v0, v1}, Lald;->c(Ljava/lang/String;)V

    .line 581
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->b:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v1, 0x60f

    .line 582
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 581
    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    goto :goto_0

    .line 583
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lg;->fZ:I

    if-ne v0, v1, :cond_3

    .line 584
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->g:Ljava/lang/String;

    .line 585
    invoke-static {v0}, Laia;->a(Ljava/lang/String;)Laia;

    move-result-object v0

    .line 587
    new-instance v1, Lakz;

    invoke-direct {v1, p0}, Lakz;-><init>(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)V

    invoke-virtual {v0, v1}, Laia;->a(Laic;)V

    .line 601
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->getFragmentManager()Lae;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Laia;->a(Lae;Ljava/lang/String;)V

    goto :goto_0

    .line 603
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->Y:Lcom/google/android/apps/hangouts/views/AvatarView;

    if-ne p1, v0, :cond_0

    .line 604
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->f:Lbdk;

    if-eqz v0, :cond_0

    .line 605
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->f:Lbdk;

    iget-object v0, v0, Lbdk;->a:Ljava/lang/String;

    invoke-static {v0}, Lbbl;->c(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 606
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onConnected(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 647
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->getActivity()Ly;

    move-result-object v0

    if-nez v0, :cond_1

    .line 648
    const-string v0, "Babel"

    const-string v1, "People client connected but InvitationFragment is detached."

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 655
    :cond_0
    :goto_0
    return-void

    .line 652
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ae:Lcvi;

    if-eqz v0, :cond_0

    .line 653
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->a()V

    goto :goto_0
.end method

.method public onConnectionFailed(Lcft;)V
    .locals 0

    .prologue
    .line 641
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 294
    invoke-super {p0, p1}, Lakl;->onCreate(Landroid/os/Bundle;)V

    .line 295
    if-eqz p1, :cond_1

    .line 296
    sget-object v0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->aj:Ljava/lang/String;

    .line 297
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 298
    if-eqz v0, :cond_0

    .line 299
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->mInviterCircleIds:Ljava/util/HashSet;

    .line 300
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->mInviterCircleIds:Ljava/util/HashSet;

    invoke-static {v1, v0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 302
    :cond_0
    sget-object v0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ak:Ljava/lang/String;

    .line 303
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 304
    sget-object v0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->al:Ljava/lang/String;

    .line 305
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 306
    if-eqz v1, :cond_1

    if-eqz v2, :cond_1

    array-length v0, v1

    array-length v3, v2

    if-ne v0, v3, :cond_1

    .line 309
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->mMyCircles:Ljava/util/Map;

    .line 310
    const/4 v0, 0x0

    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_1

    .line 311
    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->mMyCircles:Ljava/util/Map;

    aget-object v4, v1, v0

    aget-object v5, v2, v0

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 310
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 315
    :cond_1
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Ldg;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 475
    packed-switch p1, :pswitch_data_0

    .line 486
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 477
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->h:Lyj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->f:Lbdk;

    if-eqz v0, :cond_0

    .line 478
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->h:Lyj;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->e:Ljava/lang/String;

    sget v2, Lyd;->b:I

    invoke-static {v0, v1, v2}, Lyc;->a(Lyj;Ljava/lang/String;I)Ldg;

    move-result-object v0

    goto :goto_0

    .line 475
    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 134
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lbme;

    invoke-static {v0, v1}, Lcyj;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbme;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->b:Lbme;

    .line 135
    sget v0, Lf;->fH:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 140
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->getActivity()Ly;

    move-result-object v0

    .line 141
    invoke-virtual {v0}, Ly;->e()Lae;

    move-result-object v0

    sget v2, Lg;->dK:I

    .line 142
    invoke-virtual {v0, v2}, Lae;->a(I)Lt;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ac:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->d:Lahm;

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->a(Lahm;)V

    .line 146
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ac:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->C()V

    .line 148
    sget v0, Lg;->ba:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->Z:Landroid/widget/ImageView;

    .line 149
    sget v0, Lg;->fz:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/AvatarView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->Y:Lcom/google/android/apps/hangouts/views/AvatarView;

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->Y:Lcom/google/android/apps/hangouts/views/AvatarView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/hangouts/views/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 152
    sget v0, Lg;->eH:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->aa:Landroid/widget/TextView;

    .line 154
    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ab:Ljava/util/ArrayList;

    sget v0, Lg;->eX:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 155
    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ab:Ljava/util/ArrayList;

    sget v0, Lg;->eY:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 156
    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ab:Ljava/util/ArrayList;

    sget v0, Lg;->eZ:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 158
    sget v0, Lg;->fZ:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 159
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 160
    sget v0, Lg;->fG:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 161
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 163
    sget v0, Lg;->aq:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ad:Landroid/widget/Button;

    .line 164
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->b()V

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ad:Landroid/widget/Button;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ao:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 167
    sget v0, Lg;->ct:I

    .line 168
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/FixedParticipantsGalleryView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ag:Lcom/google/android/apps/hangouts/views/FixedParticipantsGalleryView;

    .line 169
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->c()V

    .line 170
    invoke-direct {p0, v1}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->d(Landroid/view/View;)V

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ai:Lbor;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbor;)V

    .line 173
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->b:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v2, 0x60e

    .line 174
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 173
    invoke-virtual {v0, v2}, Laux;->a(Ljava/lang/Integer;)V

    .line 176
    return-object v1
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 269
    invoke-super {p0}, Lakl;->onDestroyView()V

    .line 270
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ai:Lbor;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lbor;)V

    .line 272
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->af:Lcvo;

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->af:Lcvo;

    invoke-virtual {v0}, Lcvo;->b()V

    .line 274
    iput-object v1, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->af:Lcvo;

    .line 277
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ae:Lcvi;

    if-eqz v0, :cond_3

    .line 278
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ae:Lcvi;

    invoke-virtual {v0}, Lcvi;->d()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ae:Lcvi;

    invoke-virtual {v0}, Lcvi;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 279
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ae:Lcvi;

    invoke-virtual {v0}, Lcvi;->b()V

    .line 281
    :cond_2
    iput-object v1, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ae:Lcvi;

    .line 283
    :cond_3
    return-void
.end method

.method public onDisconnected()V
    .locals 0

    .prologue
    .line 718
    return-void
.end method

.method public onHiddenChanged(Z)V
    .locals 5

    .prologue
    const/16 v4, 0x3e9

    const/4 v3, 0x0

    .line 828
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onHiddenChanged: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 829
    invoke-super {p0, p1}, Lakl;->onHiddenChanged(Z)V

    .line 831
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->getActivity()Ly;

    move-result-object v0

    if-nez v0, :cond_2

    .line 840
    :cond_0
    const-string v0, "Babel"

    const-string v1, "onHiddenChanged: Too early. Bailing"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 863
    :cond_1
    :goto_0
    return-void

    .line 844
    :cond_2
    if-eqz p1, :cond_1

    .line 845
    iput-object v3, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->e:Ljava/lang/String;

    .line 846
    iput-object v3, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->f:Lbdk;

    .line 847
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->Z:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 848
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ag:Lcom/google/android/apps/hangouts/views/FixedParticipantsGalleryView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/FixedParticipantsGalleryView;->a()V

    .line 854
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->getLoaderManager()Lav;

    move-result-object v0

    invoke-virtual {v0, v4}, Lav;->b(I)Ldg;

    move-result-object v0

    .line 855
    if-eqz v0, :cond_3

    .line 856
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->getLoaderManager()Lav;

    move-result-object v0

    invoke-virtual {v0, v4}, Lav;->a(I)V

    .line 861
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ac:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->E()V

    goto :goto_0
.end method

.method public onLoadFinished(Ldg;Landroid/database/Cursor;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 494
    invoke-virtual {p1}, Ldg;->l()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 555
    :cond_0
    :goto_0
    return-void

    .line 496
    :pswitch_0
    if-eqz p2, :cond_0

    .line 497
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->h:Lyj;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->e:Ljava/lang/String;

    .line 498
    new-instance v2, Lyc;

    invoke-direct {v2}, Lyc;-><init>()V

    invoke-virtual {v2, v0, v1}, Lyc;->d(Lyj;Ljava/lang/String;)V

    sget v0, Lyd;->b:I

    invoke-virtual {v2, p2, v0}, Lyc;->a(Landroid/database/Cursor;I)I

    .line 500
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 501
    invoke-virtual {v2}, Lyc;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    .line 502
    iget-object v4, v0, Lbdh;->b:Lbdk;

    invoke-virtual {v2, v4}, Lyc;->c(Lbdk;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 503
    iget-object v4, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->f:Lbdk;

    iget-object v5, v0, Lbdh;->b:Lbdk;

    invoke-virtual {v4, v5}, Lbdk;->a(Lbdk;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 507
    iget-object v0, v0, Lbdh;->b:Lbdk;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 511
    :cond_2
    iget-object v4, v0, Lbdh;->e:Ljava/lang/String;

    iput-object v4, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->g:Ljava/lang/String;

    .line 515
    iget-object v4, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->g:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 517
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->getActivity()Ly;

    move-result-object v4

    sget v5, Lh;->dF:I

    invoke-virtual {v4, v5}, Ly;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->g:Ljava/lang/String;

    .line 520
    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->Y:Lcom/google/android/apps/hangouts/views/AvatarView;

    iget-object v0, v0, Lbdh;->h:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->h:Lyj;

    invoke-virtual {v4, v0, v5}, Lcom/google/android/apps/hangouts/views/AvatarView;->a(Ljava/lang/String;Lyj;)V

    .line 521
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->aa:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->g:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 523
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->getActivity()Ly;

    move-result-object v0

    sget v4, Lh;->jB:I

    invoke-virtual {v0, v4}, Ly;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    .line 525
    new-array v0, v10, [Ljava/lang/CharSequence;

    iget-object v5, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->g:Ljava/lang/String;

    aput-object v5, v0, v9

    .line 526
    invoke-static {v4, v0}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 525
    invoke-static {v0}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v5

    .line 527
    invoke-virtual {v5}, Landroid/text/SpannableString;->length()I

    move-result v0

    const-class v6, Landroid/text/style/URLSpan;

    invoke-virtual {v5, v9, v0, v6}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    .line 529
    new-instance v6, Landroid/text/SpannableStringBuilder;

    new-array v7, v10, [Ljava/lang/CharSequence;

    iget-object v8, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->g:Ljava/lang/String;

    aput-object v8, v7, v9

    .line 530
    invoke-static {v4, v7}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-direct {v6, v4}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 531
    array-length v4, v0

    if-lez v4, :cond_1

    .line 532
    invoke-virtual {v6}, Landroid/text/SpannableStringBuilder;->clearSpans()V

    .line 533
    aget-object v0, v0, v9

    .line 534
    invoke-virtual {v5, v0}, Landroid/text/SpannableString;->getSpanStart(Ljava/lang/Object;)I

    move-result v4

    .line 535
    invoke-virtual {v5, v0}, Landroid/text/SpannableString;->getSpanEnd(Ljava/lang/Object;)I

    move-result v0

    .line 536
    new-instance v5, Laky;

    invoke-direct {v5, p0}, Laky;-><init>(Lcom/google/android/apps/hangouts/fragments/InvitationFragment;)V

    const/16 v7, 0x21

    invoke-virtual {v6, v5, v4, v0, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_1

    .line 546
    :cond_4
    iput-object v1, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ah:Ljava/util/List;

    .line 547
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->c()V

    .line 552
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->getLoaderManager()Lav;

    move-result-object v0

    .line 553
    const/16 v1, 0x3e9

    invoke-virtual {v0, v1}, Lav;->a(I)V

    goto/16 :goto_0

    .line 494
    nop

    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic onLoadFinished(Ldg;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 87
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->onLoadFinished(Ldg;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Ldg;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 564
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 319
    invoke-super {p0, p1}, Lakl;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 320
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->mInviterCircleIds:Ljava/util/HashSet;

    if-eqz v0, :cond_0

    .line 321
    sget-object v1, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->aj:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->mInviterCircleIds:Ljava/util/HashSet;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->mInviterCircleIds:Ljava/util/HashSet;

    .line 323
    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 321
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 326
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->mMyCircles:Ljava/util/Map;

    if-eqz v0, :cond_2

    .line 327
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->mMyCircles:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    new-array v3, v0, [Ljava/lang/String;

    .line 328
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->mMyCircles:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    new-array v4, v0, [Ljava/lang/String;

    .line 329
    const/4 v0, 0x0

    .line 330
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->mMyCircles:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 331
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aput-object v1, v3, v2

    .line 332
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v4, v2

    .line 333
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    .line 334
    goto :goto_0

    .line 335
    :cond_1
    sget-object v0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ak:Ljava/lang/String;

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 336
    sget-object v0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->al:Ljava/lang/String;

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 338
    :cond_2
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 446
    invoke-super {p0}, Lakl;->onStart()V

    .line 450
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ae:Lcvi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ae:Lcvi;

    invoke-virtual {v0}, Lcvi;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ae:Lcvi;

    .line 451
    invoke-virtual {v0}, Lcvi;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 452
    const-string v0, "Babel"

    const-string v1, "Reconnecting people client for InvitationFragment."

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->ae:Lcvi;

    invoke-virtual {v0}, Lcvi;->a()V

    .line 455
    :cond_0
    return-void
.end method

.method public setHostInterface(Lald;Lahm;)V
    .locals 0

    .prologue
    .line 189
    iput-object p1, p0, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->c:Lald;

    .line 190
    invoke-direct {p0, p2}, Lcom/google/android/apps/hangouts/fragments/InvitationFragment;->a(Lahm;)V

    .line 191
    return-void
.end method
