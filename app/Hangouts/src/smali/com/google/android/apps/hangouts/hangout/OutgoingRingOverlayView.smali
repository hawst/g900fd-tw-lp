.class public Lcom/google/android/apps/hangouts/hangout/OutgoingRingOverlayView;
.super Laou;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/apps/hangouts/views/FixedParticipantsView;

.field private final b:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Laou;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 29
    sget v1, Lf;->fu:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 30
    sget v0, Lg;->ct:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/FixedParticipantsView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingRingOverlayView;->a:Lcom/google/android/apps/hangouts/views/FixedParticipantsView;

    .line 31
    sget v0, Lg;->fd:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingRingOverlayView;->b:Landroid/widget/TextView;

    .line 32
    return-void
.end method


# virtual methods
.method a(Ljava/util/List;)Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 54
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 86
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/OutgoingRingOverlayView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lh;->eD:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    .line 88
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    invoke-virtual {v0, v3}, Lbdh;->a(Z)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    .line 89
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    invoke-virtual {v0, v3}, Lbdh;->a(Z)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    .line 90
    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    invoke-virtual {v0, v3}, Lbdh;->a(Z)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v5

    .line 91
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v6

    .line 86
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 94
    :goto_0
    return-object v0

    .line 56
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/OutgoingRingOverlayView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lh;->ey:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 60
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/OutgoingRingOverlayView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lh;->ez:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    .line 61
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    invoke-virtual {v0, v3}, Lbdh;->a(Z)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    .line 60
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 65
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/OutgoingRingOverlayView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lh;->eA:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    .line 66
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    invoke-virtual {v0, v3}, Lbdh;->a(Z)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    .line 67
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    invoke-virtual {v0, v3}, Lbdh;->a(Z)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    .line 65
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 71
    :pswitch_3
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/OutgoingRingOverlayView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lh;->eB:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    .line 72
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    invoke-virtual {v0, v3}, Lbdh;->a(Z)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    .line 73
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    invoke-virtual {v0, v3}, Lbdh;->a(Z)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    .line 74
    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    invoke-virtual {v0, v3}, Lbdh;->a(Z)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v5

    .line 71
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 78
    :pswitch_4
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/OutgoingRingOverlayView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lh;->eC:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    .line 79
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    invoke-virtual {v0, v3}, Lbdh;->a(Z)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    .line 80
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    invoke-virtual {v0, v3}, Lbdh;->a(Z)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    .line 81
    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    invoke-virtual {v0, v3}, Lbdh;->a(Z)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v5

    .line 82
    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    invoke-virtual {v0, v3}, Lbdh;->a(Z)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v6

    .line 78
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 54
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public a(Lapx;)V
    .locals 3

    .prologue
    .line 36
    invoke-virtual {p1}, Lapx;->K()Z

    move-result v0

    if-nez v0, :cond_0

    .line 37
    invoke-virtual {p1}, Lapx;->B()Ljava/util/List;

    move-result-object v0

    .line 38
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingRingOverlayView;->a:Lcom/google/android/apps/hangouts/views/FixedParticipantsView;

    invoke-virtual {p1}, Lapx;->j()Lyj;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/hangouts/views/FixedParticipantsView;->a(Lyj;Ljava/util/List;)V

    .line 40
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingRingOverlayView;->b:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/hangout/OutgoingRingOverlayView;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 41
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/hangout/OutgoingRingOverlayView;->setVisibility(I)V

    .line 43
    :cond_0
    return-void
.end method

.method d()V
    .locals 1

    .prologue
    .line 47
    invoke-super {p0}, Laou;->d()V

    .line 48
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingRingOverlayView;->a:Lcom/google/android/apps/hangouts/views/FixedParticipantsView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/FixedParticipantsView;->a()V

    .line 49
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/hangout/OutgoingRingOverlayView;->setVisibility(I)V

    .line 50
    return-void
.end method
