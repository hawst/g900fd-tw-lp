.class public Lcom/google/android/apps/hangouts/realtimechat/GcmIntentService;
.super Landroid/app/IntentService;
.source "PG"


# static fields
.field private static final a:Z

.field private static final b:Ljava/lang/Object;

.field private static c:Landroid/os/PowerManager$WakeLock;

.field private static final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lbys;->k:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/GcmIntentService;->a:Z

    .line 50
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/hangouts/realtimechat/GcmIntentService;->b:Ljava/lang/Object;

    .line 52
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    sput v0, Lcom/google/android/apps/hangouts/realtimechat/GcmIntentService;->d:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 88
    const-string v0, "GcmIntentService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 89
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0, p1}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 93
    return-void
.end method

.method public static a(Landroid/content/Intent;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    const-string v0, "proto"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 327
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[GcmIntentService] "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    return-void
.end method

.method public static a()Z
    .locals 2

    .prologue
    .line 82
    const-string v0, "babel_gcm_guard_push"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 289
    const-string v0, "requestGcmRegistrationId"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/GcmIntentService;->a(Ljava/lang/String;)V

    .line 290
    new-instance v0, Lblb;

    invoke-direct {v0}, Lblb;-><init>()V

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Void;

    const/4 v2, 0x0

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object v3, v1, v2

    .line 304
    invoke-virtual {v0, v1}, Lblb;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 305
    return-void
.end method

.method static b(Landroid/content/Intent;)V
    .locals 5

    .prologue
    .line 60
    const-string v0, "timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    mul-long/2addr v1, v3

    invoke-virtual {p0, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 61
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    .line 62
    sget-object v2, Lcom/google/android/apps/hangouts/realtimechat/GcmIntentService;->b:Ljava/lang/Object;

    monitor-enter v2

    .line 63
    :try_start_0
    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/GcmIntentService;->c:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_1

    .line 64
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/GcmIntentService;->a:Z

    if-eqz v0, :cond_0

    .line 65
    const-string v0, "initializing wakelock"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/GcmIntentService;->b(Ljava/lang/String;)V

    .line 67
    :cond_0
    const-string v0, "power"

    .line 68
    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 69
    const/4 v3, 0x1

    const-string v4, "hangouts_gcm"

    invoke-virtual {v0, v3, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/realtimechat/GcmIntentService;->c:Landroid/os/PowerManager$WakeLock;

    .line 71
    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/GcmIntentService;->a:Z

    if-eqz v0, :cond_2

    .line 73
    const-string v0, "acquiring wakelock"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/GcmIntentService;->b(Ljava/lang/String;)V

    .line 75
    :cond_2
    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/GcmIntentService;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 76
    const-string v0, "pid"

    sget v2, Lcom/google/android/apps/hangouts/realtimechat/GcmIntentService;->d:I

    invoke-virtual {p0, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 77
    const-class v0, Lcom/google/android/apps/hangouts/realtimechat/GcmIntentService;

    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 78
    invoke-virtual {v1, p0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 79
    return-void

    .line 71
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method private static b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 331
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[GcmIntentService] "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    return-void
.end method

.method public static c()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 308
    new-instance v0, Lblc;

    invoke-direct {v0}, Lblc;-><init>()V

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Void;

    const/4 v2, 0x0

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object v3, v1, v2

    .line 319
    invoke-virtual {v0, v1}, Lblc;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 320
    return-void
.end method


# virtual methods
.method public final onHandleIntent(Landroid/content/Intent;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 97
    if-nez p1, :cond_1

    .line 98
    const-string v0, "Babel"

    const-string v1, "GcmIntentService.onHandleIntent called with null intent"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    :cond_0
    :goto_0
    return-void

    .line 101
    :cond_1
    sget v2, Lcom/google/android/apps/hangouts/realtimechat/GcmIntentService;->d:I

    const-string v3, "pid"

    const/4 v4, -0x1

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    if-ne v2, v3, :cond_6

    move v6, v0

    .line 103
    :goto_1
    :try_start_0
    sget-boolean v2, Lcom/google/android/apps/hangouts/realtimechat/GcmIntentService;->a:Z

    if-eqz v2, :cond_2

    .line 104
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onHandleIntent "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " respectWakeLock "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/hangouts/realtimechat/GcmIntentService;->b(Ljava/lang/String;)V

    .line 108
    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.google.android.c2dm.intent.RECEIVE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_3

    .line 110
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v4, v2

    const-string v2, "message_type"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "send_event"

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_7

    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/GcmIntentService;->a:Z

    if-eqz v0, :cond_3

    const-string v0, "Babel"

    const-string v1, "Dropping send_event from gcm"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 120
    :cond_3
    :goto_2
    :try_start_2
    invoke-static {}, Lbya;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 121
    invoke-static {}, Lbya;->c()V

    .line 124
    :cond_4
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/GcmIntentService;->a:Z

    if-eqz v0, :cond_5

    .line 125
    const-string v0, "releasing wakelock"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/GcmIntentService;->b(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 128
    :cond_5
    if-eqz v6, :cond_0

    .line 129
    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/GcmIntentService;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0

    :cond_6
    move v6, v1

    .line 101
    goto :goto_1

    .line 110
    :cond_7
    :try_start_3
    const-string v3, "deleted_messages"

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-static {}, Lbya;->b()Z

    move-result v0

    if-eqz v0, :cond_8

    new-instance v0, Lbyc;

    invoke-direct {v0}, Lbyc;-><init>()V

    const-string v1, "gcm_dirty_ping"

    invoke-virtual {v0, v1}, Lbyc;->a(Ljava/lang/String;)Lbyc;

    move-result-object v0

    invoke-virtual {v0}, Lbyc;->b()V

    :cond_8
    const-string v0, "Babel"

    const-string v1, "got deleted_messages tickle from GCM"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Z)V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 111
    :catch_0
    move-exception v0

    .line 114
    :try_start_4
    const-string v1, "babel_gcm_guard_push"

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 115
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->d()Lcom/google/android/apps/hangouts/phone/EsApplication;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Z)V

    .line 117
    :cond_9
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 128
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_a

    .line 129
    sget-object v1, Lcom/google/android/apps/hangouts/realtimechat/GcmIntentService;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_a
    throw v0

    .line 110
    :cond_b
    :try_start_5
    const-string v2, "type"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v2, "hangout"

    invoke-static {v7, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v7, :cond_d

    const-string v2, "call/"

    invoke-virtual {v7, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    move v3, v0

    :goto_3
    if-eqz v7, :cond_c

    const-string v2, "babel:proto"

    invoke-static {v7, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_e

    :cond_c
    move v2, v0

    :goto_4
    const-string v0, "babel:synctickle"

    invoke-static {v7, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v8, :cond_f

    const-string v0, "focus_account_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_5
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_12

    const-string v0, "Babel"

    const-string v1, "gcm push received for empty recipient"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lbya;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lbyc;

    invoke-direct {v0}, Lbyc;-><init>()V

    const-string v1, "gcm_error_missing_participant"

    invoke-virtual {v0, v1}, Lbyc;->a(Ljava/lang/String;)Lbyc;

    move-result-object v0

    invoke-virtual {v0}, Lbyc;->b()V

    goto/16 :goto_2

    :cond_d
    move v3, v1

    goto :goto_3

    :cond_e
    move v2, v1

    goto :goto_4

    :cond_f
    if-nez v3, :cond_10

    if-nez v2, :cond_10

    if-eqz v9, :cond_11

    :cond_10
    const-string v0, "recipient"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    :cond_11
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "gcm push with unknown type = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lbya;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lbyc;

    invoke-direct {v0}, Lbyc;-><init>()V

    const-string v1, "gcm_error_unknown"

    invoke-virtual {v0, v1}, Lbyc;->a(Ljava/lang/String;)Lbyc;

    move-result-object v0

    invoke-virtual {v0, v7}, Lbyc;->b(Ljava/lang/String;)Lbyc;

    move-result-object v0

    invoke-virtual {v0}, Lbyc;->b()V

    goto/16 :goto_2

    :cond_12
    invoke-static {v0}, Lbdk;->b(Ljava/lang/String;)Lbdk;

    move-result-object v7

    invoke-static {v7}, Lbkb;->a(Lbdk;)Lyj;

    move-result-object v1

    if-nez v1, :cond_14

    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "gcm push received for invalid account: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lbya;->b()Z

    move-result v1

    if-eqz v1, :cond_13

    new-instance v1, Lbyc;

    invoke-direct {v1}, Lbyc;-><init>()V

    const-string v2, "gcm_error_unknown_participant"

    invoke-virtual {v1, v2}, Lbyc;->a(Ljava/lang/String;)Lbyc;

    move-result-object v1

    invoke-virtual {v1, v7}, Lbyc;->a(Lbdk;)Lbyc;

    move-result-object v1

    invoke-virtual {v1}, Lbyc;->b()V

    :cond_13
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->d(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_14
    invoke-static {v1}, Lbkb;->l(Lyj;)Z

    move-result v0

    if-eqz v0, :cond_17

    const-string v0, "Babel"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_15

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "gcm push received for logged off account: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/GcmIntentService;->a(Ljava/lang/String;)V

    :cond_15
    invoke-static {}, Lbya;->b()Z

    move-result v0

    if-eqz v0, :cond_16

    new-instance v0, Lbyc;

    invoke-direct {v0}, Lbyc;-><init>()V

    const-string v2, "gcm_error_logged_out_participant"

    invoke-virtual {v0, v2}, Lbyc;->a(Ljava/lang/String;)Lbyc;

    move-result-object v0

    invoke-virtual {v0, v1}, Lbyc;->a(Lyj;)Lbyc;

    move-result-object v0

    invoke-virtual {v0}, Lbyc;->b()V

    :cond_16
    invoke-static {v1}, Lbkb;->n(Lyj;)V

    goto/16 :goto_2

    :cond_17
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/GcmIntentService;->a:Z

    if-eqz v0, :cond_18

    if-eqz v8, :cond_1b

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, "gcm hangout push received for account: "

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lyj;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/GcmIntentService;->b(Ljava/lang/String;)V

    :cond_18
    :goto_6
    invoke-static {}, Lbya;->b()Z

    move-result v0

    if-eqz v0, :cond_1a

    new-instance v0, Lbyc;

    invoke-direct {v0}, Lbyc;-><init>()V

    if-eqz v8, :cond_1d

    const-string v7, "gcm_video_ring"

    invoke-virtual {v0, v7}, Lbyc;->a(Ljava/lang/String;)Lbyc;

    :cond_19
    :goto_7
    invoke-virtual {v0, v1}, Lbyc;->a(Lyj;)Lbyc;

    move-result-object v0

    invoke-virtual {v0}, Lbyc;->b()V

    :cond_1a
    if-eqz v2, :cond_1f

    const-string v0, "proto"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "timestamp"

    const-wide/16 v7, 0x0

    invoke-virtual {p1, v2, v7, v8}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Ljava/lang/String;Lyj;JJ)I

    goto/16 :goto_2

    :cond_1b
    if-eqz v2, :cond_1c

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, "gcm heavy tickle push received for account: "

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lyj;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/GcmIntentService;->b(Ljava/lang/String;)V

    goto :goto_6

    :cond_1c
    if-eqz v9, :cond_18

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, "gcm sync tickle push received for account: "

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lyj;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/GcmIntentService;->b(Ljava/lang/String;)V

    goto :goto_6

    :cond_1d
    if-eqz v2, :cond_1e

    const-string v7, "gcm_heavy"

    invoke-virtual {v0, v7}, Lbyc;->a(Ljava/lang/String;)Lbyc;

    goto :goto_7

    :cond_1e
    if-eqz v9, :cond_19

    const-string v7, "gcm_sync"

    invoke-virtual {v0, v7}, Lbyc;->a(Ljava/lang/String;)Lbyc;

    goto :goto_7

    :cond_1f
    if-eqz v9, :cond_20

    const/4 v0, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {v1, v0, v2, v3, v4}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ZZIZ)V

    goto/16 :goto_2

    :cond_20
    if-eqz v8, :cond_22

    invoke-static {v1}, Lbkb;->f(Lyj;)I

    move-result v0

    const/16 v2, 0x66

    if-eq v0, v2, :cond_21

    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Hangout notification for account that is not setup yet: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_21
    invoke-static {p1}, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->a(Landroid/content/Intent;)V

    goto/16 :goto_2

    :cond_22
    if-eqz v3, :cond_3

    invoke-static {}, Lapk;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lapk;->b()Lapk;

    move-result-object v0

    invoke-virtual {v0, p1}, Lapk;->a(Landroid/content/Intent;)V
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_2
.end method
