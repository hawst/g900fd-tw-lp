.class public Lcom/google/android/apps/hangouts/settings/RichStatusSettingsActivityGingerbread;
.super Lbux;
.source "PG"


# instance fields
.field private a:Lbus;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lbux;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/apps/hangouts/settings/RichStatusSettingsActivityGingerbread;->a:Lbus;

    invoke-virtual {v0, p1, p2}, Lbus;->a(II)V

    .line 57
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 26
    invoke-super {p0, p1}, Lbux;->onCreate(Landroid/os/Bundle;)V

    .line 28
    new-instance v0, Lbus;

    invoke-direct {v0, p0}, Lbus;-><init>(Lbtz;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/settings/RichStatusSettingsActivityGingerbread;->a:Lbus;

    .line 29
    iget-object v0, p0, Lcom/google/android/apps/hangouts/settings/RichStatusSettingsActivityGingerbread;->a:Lbus;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/settings/RichStatusSettingsActivityGingerbread;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "account_name"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbus;->a(Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lcom/google/android/apps/hangouts/settings/RichStatusSettingsActivityGingerbread;->a:Lbus;

    invoke-virtual {v0}, Lbus;->c()Lbor;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbor;)V

    .line 32
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 48
    invoke-super {p0}, Lbux;->onDestroy()V

    .line 49
    iget-object v0, p0, Lcom/google/android/apps/hangouts/settings/RichStatusSettingsActivityGingerbread;->a:Lbus;

    invoke-virtual {v0}, Lbus;->b()V

    .line 51
    iget-object v0, p0, Lcom/google/android/apps/hangouts/settings/RichStatusSettingsActivityGingerbread;->a:Lbus;

    invoke-virtual {v0}, Lbus;->c()Lbor;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lbor;)V

    .line 52
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 39
    invoke-super {p0}, Lbux;->onResume()V

    .line 40
    iget-object v0, p0, Lcom/google/android/apps/hangouts/settings/RichStatusSettingsActivityGingerbread;->a:Lbus;

    invoke-virtual {v0}, Lbus;->a()V

    .line 41
    return-void
.end method
