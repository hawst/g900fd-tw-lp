.class public Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;
.super Landroid/widget/RelativeLayout;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final a:Z


# instance fields
.field private b:Landroid/os/Handler;

.field private c:I

.field private d:I

.field private e:Lcom/google/android/apps/hangouts/views/AvatarView;

.field private f:Lbdk;

.field private g:Lcom/google/android/apps/hangouts/views/RichStatusView;

.field private h:I

.field private i:Lcfa;

.field private j:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lbys;->s:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->a:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 176
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 177
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 180
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 181
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 184
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->b:Landroid/os/Handler;

    .line 44
    iput v2, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->c:I

    .line 48
    iput v2, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->d:I

    .line 171
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->h:I

    .line 346
    new-instance v0, Lcdp;

    invoke-direct {v0, p0}, Lcdp;-><init>(Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->j:Ljava/lang/Runnable;

    .line 185
    invoke-virtual {p0, p0}, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 186
    return-void
.end method

.method public static a(Landroid/view/LayoutInflater;Ljava/lang/String;Lbdk;ZI)Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;
    .locals 3

    .prologue
    .line 190
    sget v0, Lf;->gf:I

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;

    .line 192
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->a(Ljava/lang/String;Lbdk;ZI)V

    .line 193
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;)Lcom/google/android/apps/hangouts/views/RichStatusView;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->g:Lcom/google/android/apps/hangouts/views/RichStatusView;

    return-object v0
.end method

.method public static a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    invoke-static {p0}, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->b(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 61
    :pswitch_0
    const-string v0, "UNKNOWN"

    :goto_0
    return-object v0

    .line 53
    :pswitch_1
    const-string v0, "NULL"

    goto :goto_0

    .line 55
    :pswitch_2
    const-string v0, "STATE_WATERMARK"

    goto :goto_0

    .line 57
    :pswitch_3
    const-string v0, "STATE_FOCUSED"

    goto :goto_0

    .line 59
    :pswitch_4
    const-string v0, "STATE_TYPING"

    goto :goto_0

    .line 51
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public static b(I)I
    .locals 2

    .prologue
    .line 78
    shr-int/lit8 v0, p0, 0x1

    or-int/2addr v0, p0

    .line 79
    shr-int/lit8 v1, v0, 0x2

    or-int/2addr v0, v1

    .line 80
    shr-int/lit8 v1, v0, 0x1

    sub-int/2addr v0, v1

    return v0
.end method

.method private b(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 303
    iget-object v3, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->e:Lcom/google/android/apps/hangouts/views/AvatarView;

    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/google/android/apps/hangouts/views/AvatarView;->a(Z)V

    .line 304
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->g:Lcom/google/android/apps/hangouts/views/RichStatusView;

    if-nez p1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/RichStatusView;->c(Z)V

    .line 305
    return-void

    :cond_0
    move v0, v2

    .line 303
    goto :goto_0

    :cond_1
    move v1, v2

    .line 304
    goto :goto_1
.end method

.method private c(Z)V
    .locals 2

    .prologue
    .line 312
    iget v0, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->h:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 319
    :goto_0
    return-void

    .line 318
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->g:Lcom/google/android/apps/hangouts/views/RichStatusView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/hangouts/views/RichStatusView;->b(Z)V

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->c:I

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->b(I)I

    move-result v0

    return v0
.end method

.method public a(Lbdn;)V
    .locals 4

    .prologue
    .line 355
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->g:Lcom/google/android/apps/hangouts/views/RichStatusView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/hangouts/views/RichStatusView;->a(Lbdn;)J

    move-result-wide v0

    .line 356
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->h()V

    .line 358
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->b:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->j:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 359
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 360
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->b:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->j:Ljava/lang/Runnable;

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 362
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Lbdk;ZI)V
    .locals 2

    .prologue
    .line 198
    iput-object p2, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->f:Lbdk;

    .line 199
    new-instance v0, Lcfa;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1, p3}, Lcfa;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->i:Lcfa;

    .line 201
    sget v0, Lg;->gq:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/RichStatusView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->g:Lcom/google/android/apps/hangouts/views/RichStatusView;

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->g:Lcom/google/android/apps/hangouts/views/RichStatusView;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->f:Lbdk;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/RichStatusView;->a(Lbdk;)V

    .line 204
    invoke-virtual {p0, p4}, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->g(I)V

    .line 205
    return-void
.end method

.method public a(Ljava/lang/String;Lyj;)V
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->e:Lcom/google/android/apps/hangouts/views/AvatarView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/hangouts/views/AvatarView;->a(Ljava/lang/String;Lyj;)V

    .line 235
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->g:Lcom/google/android/apps/hangouts/views/RichStatusView;

    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 209
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->g:Lcom/google/android/apps/hangouts/views/RichStatusView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/hangouts/views/RichStatusView;->a(Z)V

    .line 210
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 140
    iget v0, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->c:I

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 144
    iget v0, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->d:I

    return v0
.end method

.method public c(I)I
    .locals 2

    .prologue
    .line 96
    iget v0, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->c:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    return v0
.end method

.method public d(I)I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->c:I

    or-int/2addr v0, p1

    return v0
.end method

.method public d()Z
    .locals 2

    .prologue
    .line 152
    iget v0, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->c:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e(I)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 114
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->a()I

    move-result v0

    .line 115
    iput p1, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->c:I

    .line 116
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->a()I

    move-result v1

    .line 118
    sget-boolean v2, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->a:Z

    if-eqz v2, :cond_0

    .line 119
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[AvatarView] new state  "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 120
    invoke-static {v1}, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->f:Lbdk;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 119
    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    :cond_0
    if-eq v1, v0, :cond_1

    .line 125
    packed-switch v1, :pswitch_data_0

    .line 127
    :cond_1
    :goto_0
    :pswitch_0
    return-void

    .line 125
    :pswitch_1
    packed-switch v0, :pswitch_data_1

    invoke-direct {p0, v6}, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->b(Z)V

    invoke-direct {p0, v5}, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->c(Z)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, v6}, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->b(Z)V

    invoke-direct {p0, v5}, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->c(Z)V

    goto :goto_0

    :pswitch_3
    packed-switch v0, :pswitch_data_2

    :goto_1
    invoke-direct {p0, v6}, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->c(Z)V

    invoke-direct {p0, v6}, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->b(Z)V

    goto :goto_0

    :pswitch_4
    invoke-direct {p0, v6}, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->c(Z)V

    invoke-direct {p0, v6}, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->b(Z)V

    goto :goto_0

    :pswitch_5
    invoke-direct {p0, v6}, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->c(Z)V

    invoke-direct {p0, v6}, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->b(Z)V

    goto :goto_1

    :pswitch_6
    packed-switch v0, :pswitch_data_3

    :goto_2
    :pswitch_7
    invoke-direct {p0, v5}, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->b(Z)V

    invoke-direct {p0, v5}, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->c(Z)V

    goto :goto_0

    :pswitch_8
    invoke-direct {p0, v5}, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->b(Z)V

    invoke-direct {p0, v5}, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->c(Z)V

    goto :goto_0

    :pswitch_9
    invoke-direct {p0, v5}, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->b(Z)V

    invoke-direct {p0, v5}, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->c(Z)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x4
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x2
        :pswitch_9
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 157
    iget v0, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->c:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->i:Lcfa;

    invoke-virtual {v0}, Lcfa;->a()V

    .line 332
    return-void
.end method

.method public f(I)V
    .locals 0

    .prologue
    .line 148
    iput p1, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->d:I

    .line 149
    return-void
.end method

.method public g()Lbdk;
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->f:Lbdk;

    return-object v0
.end method

.method public g(I)V
    .locals 3

    .prologue
    .line 224
    iget v0, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->h:I

    if-eq v0, p1, :cond_0

    .line 225
    iput p1, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->h:I

    .line 226
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->g:Lcom/google/android/apps/hangouts/views/RichStatusView;

    iget v0, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->h:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->b:I

    .line 227
    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/hangouts/views/RichStatusView;->setBackgroundResource(I)V

    .line 231
    :cond_0
    return-void

    .line 226
    :cond_1
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->a:I

    goto :goto_0
.end method

.method public h()V
    .locals 4

    .prologue
    .line 365
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 366
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->i:Lcfa;

    invoke-virtual {v1}, Lcfa;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 368
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lh;->oV:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 367
    invoke-static {v0, v1}, Lf;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 369
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->g:Lcom/google/android/apps/hangouts/views/RichStatusView;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/views/RichStatusView;->c()Lbdn;

    move-result-object v1

    .line 370
    if-eqz v1, :cond_4

    iget-boolean v2, v1, Lbdn;->e:Z

    if-eqz v2, :cond_4

    .line 371
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 372
    invoke-virtual {v1}, Lbdn;->a()Ljava/lang/String;

    move-result-object v3

    .line 373
    if-eqz v3, :cond_0

    .line 374
    invoke-static {v2, v3}, Lf;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 376
    :cond_0
    invoke-virtual {v1}, Lbdn;->b()Ljava/lang/String;

    move-result-object v3

    .line 377
    if-eqz v3, :cond_1

    .line 378
    invoke-static {v2, v3}, Lf;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 380
    :cond_1
    invoke-virtual {v1}, Lbdn;->c()Ljava/lang/String;

    move-result-object v1

    .line 381
    if-eqz v1, :cond_2

    .line 382
    invoke-static {v2, v1}, Lf;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 392
    :cond_2
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v1, v3, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->i:Lcfa;

    invoke-virtual {v1}, Lcfa;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 393
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->i:Lcfa;

    invoke-virtual {v1}, Lcfa;->d()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/view/View;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 395
    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_4

    .line 396
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 399
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 400
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->i:Lcfa;

    invoke-virtual {v0, p0}, Lcfa;->a(Landroid/view/View;)V

    .line 325
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->g:Lcom/google/android/apps/hangouts/views/RichStatusView;

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->g:Lcom/google/android/apps/hangouts/views/RichStatusView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/RichStatusView;->a()V

    .line 328
    :cond_0
    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 214
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 215
    sget v0, Lg;->H:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/AvatarView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->e:Lcom/google/android/apps/hangouts/views/AvatarView;

    .line 216
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->e:Lcom/google/android/apps/hangouts/views/AvatarView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/AvatarView;->a(Z)V

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->g:Lcom/google/android/apps/hangouts/views/RichStatusView;

    if-nez v0, :cond_0

    .line 219
    sget v0, Lg;->gq:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/RichStatusView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->g:Lcom/google/android/apps/hangouts/views/RichStatusView;

    .line 221
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 340
    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    .line 342
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OverlayedAvatarView;->i:Lcfa;

    invoke-virtual {v0, p0}, Lcfa;->b(Landroid/view/View;)V

    .line 343
    return-void
.end method
