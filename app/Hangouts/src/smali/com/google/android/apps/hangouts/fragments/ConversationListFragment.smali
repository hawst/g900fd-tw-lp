.class public Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;
.super Lakk;
.source "PG"

# interfaces
.implements Labf;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Law;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lakk",
        "<",
        "Lcom/google/android/apps/hangouts/listui/SwipeableListView;",
        "Laip;",
        ">;",
        "Labf;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Law",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static as:I

.field private static final b:Z


# instance fields
.field private Y:[Ljava/lang/Long;

.field private Z:Ljava/lang/String;

.field public a:Laij;

.field private aA:Laii;

.field private aa:Z

.field private ab:Z

.field private ac:Landroid/view/View;

.field private ad:Lcom/google/android/apps/hangouts/listui/ActionableToastBar;

.field private ae:Ljava/lang/Runnable;

.field private af:I

.field private ag:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lain;",
            ">;"
        }
    .end annotation
.end field

.field private ah:Lair;

.field private ai:I

.field private aj:Z

.field private ak:Z

.field private al:Landroid/view/View;

.field private am:Z

.field private an:Landroid/widget/LinearLayout;

.field private ao:Landroid/widget/ImageView;

.field private ap:Landroid/widget/TextView;

.field private aq:Laiu;

.field private ar:Z

.field private at:I

.field private au:Z

.field private av:J

.field private aw:J

.field private final ax:Landroid/os/Handler;

.field private final ay:Ljava/lang/Runnable;

.field private az:Lbcd;

.field private c:Lbme;

.field private d:Lyj;

.field private e:Landroid/net/Uri;

.field private f:Landroid/net/Uri;

.field private i:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 114
    sget-object v0, Lbys;->d:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->b:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 110
    invoke-direct {p0}, Lakk;-><init>()V

    .line 251
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->af:I

    .line 264
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ai:I

    .line 290
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->au:Z

    .line 293
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->av:J

    .line 299
    const-wide/16 v0, -0x2

    iput-wide v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->aw:J

    .line 353
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ax:Landroid/os/Handler;

    .line 385
    new-instance v0, Laid;

    invoke-direct {v0, p0}, Laid;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ay:Ljava/lang/Runnable;

    .line 2797
    return-void
.end method

.method public static a(Landroid/database/Cursor;)J
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 1957
    const/16 v1, 0x2a

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1960
    if-eqz v1, :cond_2

    .line 1961
    const-wide/16 v2, -0x1

    .line 1963
    const-string v4, "\\|"

    invoke-virtual {v1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 1964
    array-length v6, v8

    .line 1965
    array-length v9, v8

    move v7, v0

    move v4, v0

    move v5, v0

    :goto_0
    if-ge v7, v9, :cond_1

    aget-object v0, v8, v7

    .line 1967
    :try_start_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 1968
    cmp-long v10, v0, v2

    if-lez v10, :cond_0

    move v2, v4

    .line 1975
    :goto_1
    add-int/lit8 v4, v4, 0x1

    .line 1965
    add-int/lit8 v3, v7, 0x1

    move v7, v3

    move v5, v2

    move-wide v2, v0

    goto :goto_0

    :catch_0
    move-exception v0

    :cond_0
    move-wide v0, v2

    move v2, v5

    goto :goto_1

    :cond_1
    move v0, v6

    .line 1978
    :goto_2
    int-to-long v1, v5

    int-to-long v3, v0

    const/16 v0, 0x20

    shl-long/2addr v3, v0

    or-long v0, v1, v3

    return-wide v0

    :cond_2
    move v5, v0

    goto :goto_2
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;Lbcd;)Lbcd;
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->az:Lbcd;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ay:Ljava/lang/Runnable;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ae:Ljava/lang/Runnable;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->f(Landroid/view/View;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;Ljd;Landroid/view/MenuItem;[J)V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a(Ljd;Landroid/view/MenuItem;[J)V

    return-void
.end method

.method private a(Ljd;Landroid/view/MenuItem;[J)V
    .locals 17

    .prologue
    .line 2035
    invoke-virtual/range {p1 .. p1}, Ljd;->a()Landroid/database/Cursor;

    move-result-object v5

    .line 2036
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2037
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 2039
    invoke-interface/range {p2 .. p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lg;->fI:I

    if-ne v1, v2, :cond_0

    .line 2040
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a:Laij;

    invoke-virtual {v1}, Laij;->a()V

    .line 2043
    :cond_0
    const/4 v1, 0x0

    .line 2044
    invoke-interface/range {p2 .. p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sget v3, Lg;->fX:I

    if-ne v2, v3, :cond_13

    .line 2045
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object v2, v1

    .line 2048
    :goto_0
    move-object/from16 v0, p3

    array-length v8, v0

    const/4 v1, 0x0

    move v4, v1

    :goto_1
    if-ge v4, v8, :cond_c

    aget-wide v9, p3, v4

    .line 2049
    const/4 v1, 0x0

    move v3, v1

    :goto_2
    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-ge v3, v1, :cond_b

    .line 2050
    invoke-interface {v5, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 2051
    const/4 v1, 0x0

    invoke-interface {v5, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    cmp-long v1, v11, v9

    if-nez v1, :cond_6

    .line 2052
    invoke-static {v5}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    .line 2054
    const/4 v11, 0x4

    invoke-interface {v5, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    .line 2056
    invoke-interface/range {p2 .. p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v13

    sget v14, Lg;->fP:I

    if-ne v13, v14, :cond_1

    .line 2057
    invoke-static {v1}, Lf;->f(Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2058
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    const/16 v13, 0xa

    invoke-static {v12, v1, v13}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;Ljava/lang/String;I)I

    goto :goto_3

    .line 2061
    :cond_1
    invoke-interface/range {p2 .. p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v13

    sget v14, Lg;->fT:I

    if-ne v13, v14, :cond_2

    .line 2062
    invoke-static {v1}, Lf;->f(Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_4
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2063
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    const/16 v13, 0x1e

    invoke-static {v12, v1, v13}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;Ljava/lang/String;I)I

    goto :goto_4

    .line 2066
    :cond_2
    invoke-interface/range {p2 .. p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v13

    sget v14, Lg;->fO:I

    if-ne v13, v14, :cond_3

    .line 2071
    invoke-static {v1}, Lf;->f(Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_5
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2072
    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 2074
    :cond_3
    invoke-interface/range {p2 .. p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v13

    sget v14, Lg;->fK:I

    if-ne v13, v14, :cond_4

    .line 2081
    invoke-static {v1}, Lf;->f(Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_6
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2082
    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2083
    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 2085
    :cond_4
    invoke-interface/range {p2 .. p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v13

    sget v14, Lg;->fL:I

    if-ne v13, v14, :cond_7

    .line 2086
    move-object/from16 v0, p3

    array-length v11, v0

    const/4 v12, 0x1

    if-le v11, v12, :cond_5

    .line 2087
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    .line 2089
    :cond_5
    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    .line 2090
    const-string v12, "conversation_name"

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->Z:Ljava/lang/String;

    invoke-virtual {v11, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2092
    const-string v12, "conversation_id"

    invoke-virtual {v11, v12, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2094
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->getActivity()Ly;

    move-result-object v1

    const/4 v12, 0x1

    invoke-virtual {v1, v12, v11}, Ly;->showDialog(ILandroid/os/Bundle;)Z

    .line 2049
    :cond_6
    :goto_7
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto/16 :goto_2

    .line 2095
    :cond_7
    invoke-interface/range {p2 .. p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v13

    sget v14, Lg;->fS:I

    if-ne v13, v14, :cond_8

    .line 2096
    invoke-static {v1}, Lf;->f(Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_8
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2097
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a:Laij;

    invoke-virtual {v14, v1, v11, v12}, Laij;->b(Ljava/lang/String;J)V

    goto :goto_8

    .line 2100
    :cond_8
    invoke-interface/range {p2 .. p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v13

    sget v14, Lg;->fI:I

    if-ne v13, v14, :cond_9

    .line 2102
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a:Laij;

    invoke-virtual {v13, v1}, Laij;->a(Ljava/lang/String;)V

    .line 2104
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a:Laij;

    invoke-virtual {v13, v1, v11, v12}, Laij;->a(Ljava/lang/String;J)V

    goto :goto_7

    .line 2106
    :cond_9
    invoke-interface/range {p2 .. p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v11

    sget v12, Lg;->fX:I

    if-ne v11, v12, :cond_a

    .line 2107
    const/4 v1, 0x1

    .line 2108
    invoke-interface {v5, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2110
    invoke-static {v1}, Lf;->f(Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_9
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2112
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 2114
    :cond_a
    invoke-interface/range {p2 .. p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v11

    sget v12, Lg;->fR:I

    if-ne v11, v12, :cond_6

    .line 2115
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 2116
    const-string v12, "self_watermark"

    const/4 v13, 0x0

    .line 2117
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    .line 2116
    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2118
    const-string v12, "chat_watermark"

    const/4 v13, 0x0

    .line 2120
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    .line 2118
    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2122
    const-string v12, "hangout_watermark"

    const/4 v13, 0x0

    .line 2124
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    .line 2122
    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2126
    const-string v12, "has_chat_notifications"

    const/4 v13, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2127
    const-string v12, "has_video_notifications"

    const/4 v13, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2128
    new-instance v12, Lyt;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    invoke-direct {v12, v13}, Lyt;-><init>(Lyj;)V

    .line 2130
    invoke-virtual {v12}, Lyt;->e()Lzr;

    move-result-object v12

    const-string v13, "conversations"

    const-string v14, "conversation_id=?"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    aput-object v1, v15, v16

    invoke-virtual {v12, v13, v11, v14, v15}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v11

    .line 2135
    const-string v12, "Babel"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "modifying "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v13, " changing notification status updated "

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v11, " rows"

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v12, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2140
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    invoke-static {v1}, Lyp;->a(Lyj;)V

    .line 2141
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    const-wide/16 v11, 0x0

    const/4 v13, 0x1

    const/4 v14, 0x0

    invoke-static {v1, v11, v12, v13, v14}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;JIZ)V

    goto/16 :goto_7

    .line 2048
    :cond_b
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto/16 :goto_1

    .line 2148
    :cond_c
    invoke-interface/range {p2 .. p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v3, Lg;->fX:I

    if-ne v1, v3, :cond_d

    .line 2150
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    .line 2152
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    .line 2150
    invoke-static {v3, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lyj;[Ljava/lang/String;)V

    .line 2155
    :cond_d
    invoke-interface/range {p2 .. p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lg;->fI:I

    if-ne v1, v2, :cond_e

    .line 2156
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a:Laij;

    invoke-virtual {v1}, Laij;->b()V

    .line 2159
    :cond_e
    invoke-interface/range {p2 .. p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lg;->fO:I

    if-eq v1, v2, :cond_f

    .line 2160
    invoke-interface/range {p2 .. p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lg;->fK:I

    if-ne v1, v2, :cond_12

    .line 2161
    :cond_f
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v6, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->i:[Ljava/lang/String;

    .line 2162
    invoke-interface/range {p2 .. p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lg;->fO:I

    if-ne v1, v2, :cond_11

    .line 2163
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->e()V

    .line 2175
    :cond_10
    :goto_a
    return-void

    .line 2166
    :cond_11
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/Long;

    invoke-interface {v7, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Long;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->Y:[Ljava/lang/Long;

    .line 2167
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    invoke-virtual {v1}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->f(Ljava/lang/String;)V

    goto :goto_a

    .line 2172
    :cond_12
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->az:Lbcd;

    if-eqz v1, :cond_10

    .line 2173
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->az:Lbcd;

    invoke-virtual {v1}, Lbcd;->c()V

    goto :goto_a

    :cond_13
    move-object v2, v1

    goto/16 :goto_0
.end method

.method private a(JZ)Z
    .locals 6

    .prologue
    const-wide/16 v4, -0x2

    .line 2870
    sget-boolean v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->b:Z

    if-eqz v0, :cond_0

    .line 2871
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Updating continuation end timestamp for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->af:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->av:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->aw:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2875
    :cond_0
    const/4 v0, 0x0

    .line 2876
    const-wide/16 v1, -0x3

    cmp-long v1, p1, v1

    if-nez v1, :cond_2

    .line 2877
    iput-wide v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->av:J

    .line 2879
    const/4 v0, 0x1

    .line 2883
    :goto_0
    iput-wide v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->aw:J

    .line 2884
    if-eqz p3, :cond_1

    .line 2885
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->x()Z

    .line 2887
    :cond_1
    return v0

    .line 2881
    :cond_2
    iput-wide p1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->av:J

    goto :goto_0
.end method

.method public static b(Landroid/database/Cursor;)J
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v4, -0x1

    .line 1988
    const/16 v0, 0x24

    .line 1989
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1993
    invoke-static {v0}, Lf;->f(Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    move v3, v4

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1994
    if-ne v3, v4, :cond_0

    .line 1996
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    const/4 v6, 0x1

    if-ne v0, v6, :cond_0

    move v3, v1

    .line 2003
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 2004
    goto :goto_0

    .line 2006
    :cond_1
    if-ne v3, v4, :cond_2

    .line 2009
    :goto_2
    int-to-long v2, v2

    int-to-long v0, v1

    const/16 v4, 0x20

    shl-long/2addr v0, v4

    or-long/2addr v0, v2

    return-wide v0

    :catch_0
    move-exception v0

    goto :goto_1

    :cond_2
    move v2, v3

    goto :goto_2
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ax:Landroid/os/Handler;

    return-object v0
.end method

.method public static b(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2822
    const/16 v0, 0x63

    if-gt p0, v0, :cond_0

    .line 2824
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 2826
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "99+"

    goto :goto_0
.end method

.method public static synthetic c(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)Lair;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ah:Lair;

    return-object v0
.end method

.method public static synthetic c(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    invoke-static {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)I
    .locals 1

    .prologue
    .line 110
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ai:I

    return v0
.end method

.method public static synthetic d(Landroid/view/View;)Landroid/view/View;
    .locals 1

    .prologue
    .line 110
    sget v0, Lg;->aL:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private static d(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2017
    const/4 v0, 0x1

    .line 2018
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2019
    invoke-static {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a(Landroid/database/Cursor;)J

    move-result-wide v1

    long-to-int v1, v1

    .line 2017
    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->c(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 110
    invoke-static {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->g(Ljava/lang/String;)V

    return-void
.end method

.method private e(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 413
    if-eqz p1, :cond_0

    const-string v0, "last_archived"

    .line 414
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 418
    const-string v0, "last_archived"

    .line 419
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ag:Ljava/util/ArrayList;

    .line 422
    :cond_0
    return-void
.end method

.method public static synthetic e(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 110
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[ConversationListFragment] "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic e(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)Z
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ab:Z

    return v0
.end method

.method private f(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/16 v2, 0x8

    .line 428
    sget-boolean v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->b:Z

    if-eqz v0, :cond_0

    .line 429
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "updateView isLoading="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isEmpty="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->isEmpty()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->g(Ljava/lang/String;)V

    .line 431
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 432
    invoke-virtual {p0, p1}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->e(Landroid/view/View;)V

    .line 433
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ac:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 453
    :cond_1
    :goto_0
    return-void

    .line 434
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ai:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    .line 435
    invoke-virtual {p0, p1}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->c(Landroid/view/View;)V

    .line 436
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ac:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 438
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->b(Landroid/view/View;)V

    .line 442
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->z()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 443
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ac:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 447
    :goto_1
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ai:I

    if-ne v0, v3, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->am:Z

    if-nez v0, :cond_1

    .line 448
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->c:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v1, 0x60c

    .line 449
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 448
    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    .line 450
    iput-boolean v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->am:Z

    goto :goto_0

    .line 445
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ac:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public static synthetic f(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)Z
    .locals 1

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->z()Z

    move-result v0

    return v0
.end method

.method private static g(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2664
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[ConversationListFragment] "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2665
    return-void
.end method

.method public static synthetic g(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)Z
    .locals 1

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->y()Z

    move-result v0

    return v0
.end method

.method public static synthetic h(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)Z
    .locals 1

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->x()Z

    move-result v0

    return v0
.end method

.method public static synthetic i(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->u()V

    return-void
.end method

.method public static synthetic j(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)Lbcd;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->az:Lbcd;

    return-object v0
.end method

.method public static synthetic k(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)Lyj;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    return-object v0
.end method

.method public static synthetic l(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)Z
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ar:Z

    return v0
.end method

.method public static synthetic m(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)Lcom/google/android/apps/hangouts/listui/ActionableToastBar;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ad:Lcom/google/android/apps/hangouts/listui/ActionableToastBar;

    return-object v0
.end method

.method public static synthetic n(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ae:Ljava/lang/Runnable;

    return-object v0
.end method

.method public static synthetic s()Z
    .locals 1

    .prologue
    .line 110
    sget-boolean v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->b:Z

    return v0
.end method

.method public static synthetic t()I
    .locals 1

    .prologue
    .line 110
    sget v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->as:I

    return v0
.end method

.method private u()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x1

    .line 320
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ai:I

    if-eq v0, v2, :cond_1

    .line 351
    :cond_0
    :goto_0
    return-void

    .line 324
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 328
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ar:Z

    if-eqz v0, :cond_0

    .line 332
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->af:I

    if-eq v0, v3, :cond_0

    .line 336
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->y()Z

    move-result v0

    if-nez v0, :cond_0

    .line 341
    sget-boolean v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->b:Z

    if-eqz v0, :cond_2

    .line 342
    const-string v0, "Babel"

    const-string v1, "switching filter from high affinity to low affinity"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    :cond_2
    iput v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->af:I

    .line 346
    const-wide/16 v0, -0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a(JZ)Z

    .line 349
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->getLoaderManager()Lav;

    move-result-object v0

    .line 350
    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lav;->b(ILandroid/os/Bundle;Law;)Ldg;

    goto :goto_0
.end method

.method private v()Z
    .locals 2

    .prologue
    .line 1710
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ak:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ai:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private w()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1718
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->v()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1719
    sget-boolean v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->b:Z

    if-eqz v0, :cond_0

    .line 1720
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ConversationListFragment startLoading: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->g(Ljava/lang/String;)V

    .line 1724
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->getLoaderManager()Lav;

    move-result-object v0

    .line 1725
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2, p0}, Lav;->b(ILandroid/os/Bundle;Law;)Ldg;

    .line 1726
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2, p0}, Lav;->b(ILandroid/os/Bundle;Law;)Ldg;

    .line 1727
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->au:Z

    .line 1729
    :cond_1
    return-void
.end method

.method private x()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2368
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->z()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2388
    :cond_0
    :goto_0
    return v0

    .line 2372
    :cond_1
    sget-boolean v1, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->b:Z

    if-eqz v1, :cond_2

    .line 2373
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Requesting more conversations at "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->av:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " (last "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->aw:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2378
    :cond_2
    iget-wide v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->aw:J

    iget-wide v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->av:J

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    .line 2383
    iget-wide v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->av:J

    iput-wide v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->aw:J

    .line 2384
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    iget v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->af:I

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lyj;I)I

    .line 2386
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private y()Z
    .locals 4

    .prologue
    .line 2891
    iget-wide v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->aw:J

    const-wide/16 v2, -0x2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private z()Z
    .locals 4

    .prologue
    .line 2896
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    invoke-virtual {v0}, Lyj;->y()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->av:J

    const-wide/16 v2, -0x2

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(I)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2286
    sget-boolean v2, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->b:Z

    if-eqz v2, :cond_0

    .line 2287
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "setDisplayMode: displayMode="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ai:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", newMode="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", account="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2291
    :cond_0
    iget v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ai:I

    if-ne p1, v2, :cond_1

    .line 2360
    :goto_0
    return-void

    .line 2294
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->getLoaderManager()Lav;

    move-result-object v3

    .line 2295
    iput p1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ai:I

    .line 2299
    iget v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ai:I

    packed-switch v2, :pswitch_data_0

    .line 2318
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "setDisplayMode called with unknown mode: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ai:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcwz;->a(Ljava/lang/String;)V

    move v2, v0

    move v0, v1

    .line 2325
    :goto_1
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    if-eqz v0, :cond_2

    .line 2326
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ar:Z

    .line 2327
    sget v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->as:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->as:I

    .line 2328
    new-instance v4, Laih;

    invoke-direct {v4, p0, v0, v3}, Laih;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;ILav;)V

    new-array v0, v1, [Ljava/lang/Void;

    .line 2351
    invoke-virtual {v4, v0}, Laih;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 2354
    :cond_2
    const-wide/16 v0, -0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a(JZ)Z

    .line 2357
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->w()V

    .line 2359
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->r()V

    goto :goto_0

    .line 2301
    :pswitch_0
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->aj:Z

    .line 2305
    :pswitch_1
    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->af:I

    move v0, v1

    move v2, v1

    .line 2306
    goto :goto_1

    .line 2308
    :pswitch_2
    const/4 v2, 0x3

    iput v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->af:I

    .line 2311
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->aj:Z

    move v2, v1

    .line 2312
    goto :goto_1

    .line 2314
    :pswitch_3
    const/4 v2, 0x2

    iput v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->af:I

    .line 2315
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->aj:Z

    move v2, v0

    move v0, v1

    .line 2316
    goto :goto_1

    .line 2299
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lair;)V
    .locals 0

    .prologue
    .line 316
    iput-object p1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ah:Lair;

    .line 317
    return-void
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2189
    const-string v1, "leave_conversation"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2190
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->i:[Ljava/lang/String;

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 2191
    iget-object v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    invoke-static {v4, v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->g(Lyj;Ljava/lang/String;)I

    .line 2190
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2193
    :cond_0
    const-string v1, "delete_conversation"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2194
    invoke-virtual {p0, p1}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d(Landroid/os/Bundle;)Lyj;

    move-result-object v1

    .line 2195
    if-eqz v1, :cond_1

    .line 2196
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->i:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 2197
    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->i:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 2198
    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->Y:[Ljava/lang/Long;

    aget-object v3, v3, v0

    .line 2202
    invoke-static {v3}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v3

    .line 2199
    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ljava/lang/String;J)I

    .line 2196
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2207
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->az:Lbcd;

    if-eqz v0, :cond_2

    .line 2208
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->az:Lbcd;

    invoke-virtual {v0}, Lbcd;->c()V

    .line 2210
    :cond_2
    return-void
.end method

.method protected a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 534
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->isEmpty()Z

    move-result v0

    .line 535
    sget-boolean v1, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->b:Z

    if-eqz v1, :cond_0

    .line 536
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "doShowEmptyViewProgress cursorIsEmpty="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->g(Ljava/lang/String;)V

    .line 538
    :cond_0
    if-eqz v0, :cond_1

    .line 539
    const v0, 0x1020004

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 540
    sget v0, Lg;->eb:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 542
    :cond_1
    return-void
.end method

.method public a(Ldg;Landroid/database/Cursor;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    move-object v0, p1

    .line 1406
    check-cast v0, Lbaj;

    invoke-virtual {v0}, Lbaj;->A()Lyj;

    move-result-object v5

    .line 1407
    if-eqz v5, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    if-eqz v0, :cond_0

    .line 1408
    invoke-virtual {v5}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    invoke-virtual {v1}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1409
    :cond_0
    const-string v0, "Babel"

    const-string v1, "onLoadFinished called for mismatched account"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1554
    :cond_1
    :goto_0
    return-void

    .line 1412
    :cond_2
    if-nez p2, :cond_3

    .line 1413
    const-string v0, "Babel"

    const-string v1, "onLoadFinished returned with null data"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1416
    :cond_3
    invoke-virtual {p1}, Ldg;->l()I

    move-result v0

    if-ne v0, v3, :cond_16

    .line 1417
    const-string v0, "ConversationListFragment.list load finished"

    invoke-static {v0}, Lbzq;->a(Ljava/lang/String;)V

    .line 1418
    iput-boolean v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ab:Z

    .line 1419
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->g:Lbai;

    check-cast v0, Laip;

    invoke-virtual {v0, p2}, Laip;->a(Landroid/database/Cursor;)V

    .line 1420
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->aq:Laiu;

    invoke-virtual {v0}, Laiu;->notifyDataSetChanged()V

    .line 1424
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1425
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->x()Z

    .line 1427
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->f(Landroid/view/View;)V

    .line 1429
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ai()V

    .line 1431
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_5

    .line 1432
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->az:Lbcd;

    if-eqz v0, :cond_5

    .line 1434
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->az:Lbcd;

    invoke-virtual {v0}, Lbcd;->b()V

    .line 1439
    :cond_5
    if-eqz p2, :cond_6

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_a

    :cond_6
    move v0, v3

    :goto_1
    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->aa:Z

    if-nez v0, :cond_7

    .line 1440
    iput-boolean v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->aa:Z

    .line 1448
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a:Laij;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a:Laij;

    .line 1449
    iget-object v0, v0, Laij;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ad:Lcom/google/android/apps/hangouts/listui/ActionableToastBar;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ad:Lcom/google/android/apps/hangouts/listui/ActionableToastBar;

    .line 1450
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->getVisibility()I

    move-result v0

    if-nez v0, :cond_c

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1451
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a:Laij;

    .line 1452
    iget-object v1, v0, Laij;->a:Ljava/util/ArrayList;

    .line 1454
    :cond_8
    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1456
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 1457
    :cond_9
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1458
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lain;

    .line 1459
    iget-object v0, v0, Lain;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1460
    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    goto :goto_2

    :cond_a
    move v0, v4

    .line 1439
    goto :goto_1

    .line 1464
    :cond_b
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_10

    .line 1465
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ad:Lcom/google/android/apps/hangouts/listui/ActionableToastBar;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->a(Z)V

    .line 1475
    :cond_c
    :goto_3
    check-cast p1, Lbaj;

    .line 1476
    invoke-virtual {p1}, Lbaj;->j()Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->f:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 1477
    const-wide/16 v0, 0x0

    .line 1480
    :cond_d
    const/4 v2, 0x4

    .line 1482
    invoke-interface {p2, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 1480
    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 1483
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_d

    .line 1485
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->d()Lcom/google/android/apps/hangouts/phone/EsApplication;

    move-result-object v6

    .line 1486
    invoke-virtual {v6, v5}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Lyj;)J

    move-result-wide v7

    .line 1487
    sget-boolean v2, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->b:Z

    if-eqz v2, :cond_e

    .line 1488
    const-string v9, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v10, "new invite timestamp: "

    invoke-direct {v2, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v10, "; old "

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v10, "; greater? "

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    cmp-long v2, v0, v7

    if-lez v2, :cond_11

    move v2, v3

    :goto_4
    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v9, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1494
    :cond_e
    cmp-long v2, v0, v7

    if-lez v2, :cond_f

    .line 1495
    new-instance v2, Laig;

    invoke-direct {v2, p0, v6, v5}, Laig;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;Lcom/google/android/apps/hangouts/phone/EsApplication;Lyj;)V

    new-array v3, v3, [Ljava/lang/Long;

    .line 1504
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v2, v3}, Laig;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1530
    :cond_f
    :goto_5
    invoke-static {}, Lbzq;->a()V

    goto/16 :goto_0

    .line 1468
    :cond_10
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_8

    goto :goto_3

    :cond_11
    move v2, v4

    .line 1488
    goto :goto_4

    .line 1506
    :cond_12
    invoke-virtual {p1}, Lbaj;->j()Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->e:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1509
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 1511
    :cond_13
    const/16 v0, 0xe

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 1512
    const/16 v0, 0x16

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 1513
    if-eq v1, v7, :cond_15

    if-lez v0, :cond_15

    move v4, v0

    .line 1522
    :cond_14
    :goto_6
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->at:I

    if-eq v0, v4, :cond_f

    .line 1523
    iput v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->at:I

    .line 1524
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->getActivity()Ly;

    move-result-object v0

    .line 1525
    if-eqz v0, :cond_f

    .line 1526
    invoke-virtual {v0}, Ly;->u_()V

    goto :goto_5

    .line 1517
    :cond_15
    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_14

    .line 1518
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_13

    goto :goto_6

    .line 1531
    :cond_16
    invoke-virtual {p1}, Ldg;->l()I

    move-result v0

    if-ne v0, v7, :cond_1

    .line 1535
    const-wide/16 v0, -0x1

    .line 1536
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_17

    .line 1537
    invoke-interface {p2, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 1542
    :cond_17
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->isEmpty()Z

    move-result v2

    .line 1543
    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a(JZ)Z

    move-result v0

    .line 1545
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->u()V

    .line 1550
    if-eqz v2, :cond_18

    if-nez v0, :cond_1

    .line 1551
    :cond_18
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->f(Landroid/view/View;)V

    goto/16 :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2214
    const-string v0, "leave_conversation"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2215
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->az:Lbcd;

    if-eqz v0, :cond_0

    .line 2216
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->az:Lbcd;

    invoke-virtual {v0}, Lbcd;->c()V

    .line 2219
    :cond_0
    return-void
.end method

.method public a(Lyj;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1675
    iput-object p1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    .line 1676
    sget-boolean v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->b:Z

    if-eqz v0, :cond_0

    .line 1677
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ConversationListFragment setAccount: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->g(Ljava/lang/String;)V

    .line 1679
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->getActivity()Ly;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1680
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->d:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    .line 1681
    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Landroid/net/Uri;Lyj;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->e:Landroid/net/Uri;

    .line 1682
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->f:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    .line 1683
    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Landroid/net/Uri;Lyj;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->f:Landroid/net/Uri;

    .line 1684
    new-instance v0, Laij;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    invoke-direct {v0, p0, v1}, Laij;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;Lyj;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a:Laij;

    .line 1685
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ag:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 1688
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a:Laij;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ag:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Laij;->a(Ljava/util/ArrayList;)V

    .line 1689
    iput-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ag:Ljava/util/ArrayList;

    .line 1692
    :cond_1
    iput-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->i:[Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->Y:[Ljava/lang/Long;

    iput-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->Z:Ljava/lang/String;

    iput-boolean v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->aa:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->av:J

    const-wide/16 v0, -0x2

    iput-wide v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->aw:J

    iput-boolean v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ab:Z

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->r()V

    .line 1693
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->w()V

    .line 1695
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->h:Landroid/widget/AbsListView;

    if-eqz v0, :cond_2

    .line 1696
    new-instance v0, Laip;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->getActivity()Ly;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    invoke-direct {v0, p0, v1, v2, p0}, Laip;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;Landroid/content/Context;Lyj;Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->g:Lbai;

    .line 1698
    new-instance v1, Laiu;

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->g:Lbai;

    check-cast v0, Laip;

    invoke-direct {v1, p0, v0}, Laiu;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;Laip;)V

    iput-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->aq:Laiu;

    .line 1699
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->h:Landroid/widget/AbsListView;

    check-cast v0, Lcom/google/android/apps/hangouts/listui/SwipeableListView;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->aq:Laiu;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1702
    :cond_2
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 2282
    iput-boolean p1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->aj:Z

    .line 2283
    return-void
.end method

.method protected a()Z
    .locals 1

    .prologue
    .line 565
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->y()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ab:Z

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1380
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->v()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->au:Z

    if-nez v0, :cond_1

    .line 1381
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->au:Z

    .line 1383
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->getLoaderManager()Lav;

    move-result-object v0

    invoke-virtual {v0, v1}, Lav;->b(I)Ldg;

    move-result-object v0

    .line 1385
    if-eqz v0, :cond_0

    .line 1386
    invoke-virtual {v0}, Ldg;->r()V

    .line 1388
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->getLoaderManager()Lav;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lav;->b(I)Ldg;

    move-result-object v0

    .line 1390
    if-eqz v0, :cond_1

    .line 1391
    invoke-virtual {v0}, Ldg;->r()V

    .line 1394
    :cond_1
    return-void
.end method

.method protected b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 552
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ao:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 553
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->an:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ao:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 554
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ao:Landroid/widget/ImageView;

    .line 557
    :cond_0
    invoke-super {p0, p1}, Lakk;->b(Landroid/view/View;)V

    .line 558
    sget v0, Lg;->eb:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 559
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2226
    const-string v0, "leave_conversation"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2227
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->az:Lbcd;

    if-eqz v0, :cond_0

    .line 2228
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->az:Lbcd;

    invoke-virtual {v0}, Lbcd;->c()V

    .line 2231
    :cond_0
    return-void
.end method

.method public c(Ljava/lang/String;)Lyh;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2256
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->g:Lbai;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 2274
    :goto_0
    return-object v0

    .line 2259
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->g:Lbai;

    check-cast v0, Laip;

    invoke-virtual {v0}, Laip;->a()Landroid/database/Cursor;

    move-result-object v6

    .line 2260
    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2262
    :cond_1
    invoke-static {v6}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 2263
    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2264
    new-instance v0, Lyh;

    const/16 v1, 0x19

    .line 2265
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x1f

    .line 2266
    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x20

    .line 2267
    invoke-interface {v6, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x21

    .line 2268
    invoke-interface {v6, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/16 v5, 0x22

    .line 2269
    invoke-interface {v6, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/16 v7, 0x23

    .line 2270
    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lyh;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2272
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_3
    move-object v0, v1

    .line 2274
    goto :goto_0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 1397
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->au:Z

    if-eqz v0, :cond_0

    .line 1398
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->au:Z

    .line 1399
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->getLoaderManager()Lav;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lav;->b(I)Ldg;

    move-result-object v0

    invoke-virtual {v0}, Ldg;->p()V

    .line 1400
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->getLoaderManager()Lav;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lav;->b(I)Ldg;

    move-result-object v0

    invoke-virtual {v0}, Ldg;->p()V

    .line 1402
    :cond_0
    return-void
.end method

.method protected c(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 465
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ai:I

    packed-switch v0, :pswitch_data_0

    move-object v0, v3

    move v1, v5

    move v2, v5

    move v4, v5

    .line 502
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 509
    invoke-static {p1, v3, v6}, Lf;->a(Landroid/view/View;Landroid/view/accessibility/AccessibilityManager;Ljava/lang/CharSequence;)V

    .line 512
    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ap:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 518
    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ao:Landroid/widget/ImageView;

    if-nez v3, :cond_0

    .line 519
    new-instance v3, Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->an:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ao:Landroid/widget/ImageView;

    .line 520
    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ao:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 521
    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->an:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ao:Landroid/widget/ImageView;

    invoke-virtual {v2, v3, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 522
    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->an:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 523
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->an:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 526
    :cond_0
    invoke-super {p0, p1}, Lakk;->c(Landroid/view/View;)V

    .line 527
    return-void

    .line 467
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    invoke-virtual {v0}, Lyj;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v4

    .line 469
    :goto_1
    if-eqz v0, :cond_2

    .line 470
    sget v1, Lh;->hA:I

    .line 471
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->bt:I

    .line 483
    :goto_2
    new-instance v2, Laie;

    invoke-direct {v2, p0}, Laie;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)V

    move-object v7, v2

    move v2, v0

    move-object v0, v7

    move v8, v4

    move v4, v1

    move v1, v8

    .line 489
    goto :goto_0

    :cond_1
    move v0, v5

    .line 467
    goto :goto_1

    .line 473
    :cond_2
    invoke-static {}, Lbkb;->m()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 474
    sget v1, Lh;->hx:I

    .line 475
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->bt:I

    goto :goto_2

    .line 477
    :cond_3
    sget v1, Lh;->hw:I

    .line 478
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->bs:I

    goto :goto_2

    .line 491
    :pswitch_1
    sget v1, Lh;->hy:I

    .line 492
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->cz:I

    .line 493
    iget-boolean v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->am:Z

    if-nez v2, :cond_4

    .line 494
    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->c:Lbme;

    invoke-interface {v2}, Lbme;->a()Laux;

    move-result-object v2

    const/16 v6, 0x60d

    .line 495
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 494
    invoke-virtual {v2, v6}, Laux;->a(Ljava/lang/Integer;)V

    .line 496
    iput-boolean v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->am:Z

    move v2, v0

    move v4, v1

    move-object v0, v3

    move v1, v5

    goto/16 :goto_0

    .line 500
    :pswitch_2
    sget v1, Lh;->hu:I

    .line 501
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->cy:I

    move v2, v0

    move v4, v1

    move-object v0, v3

    move v1, v5

    goto/16 :goto_0

    :cond_4
    move v2, v0

    move v4, v1

    move-object v0, v3

    move v1, v5

    goto/16 :goto_0

    .line 465
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public d()V
    .locals 2

    .prologue
    .line 1665
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->al:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1666
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->al:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 1667
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->al:Landroid/view/View;

    .line 1669
    :cond_0
    return-void
.end method

.method public e()V
    .locals 4

    .prologue
    .line 2178
    sget v0, Lh;->jK:I

    .line 2179
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget v1, Lh;->jJ:I

    .line 2180
    invoke-virtual {p0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lh;->jI:I

    .line 2181
    invoke-virtual {p0, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v3, Lh;->kt:I

    .line 2182
    invoke-virtual {p0, v3}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2178
    invoke-static {v0, v1, v2, v3}, Labe;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Labe;

    move-result-object v0

    .line 2183
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Labe;->setTargetFragment(Lt;I)V

    .line 2184
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->getFragmentManager()Lae;

    move-result-object v1

    const-string v2, "leave_conversation"

    invoke-virtual {v0, v1, v2}, Labe;->a(Lae;Ljava/lang/String;)V

    .line 2185
    return-void
.end method

.method public f()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2238
    iget v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ai:I

    packed-switch v1, :pswitch_data_0

    .line 2248
    :goto_0
    :pswitch_0
    return v0

    .line 2241
    :pswitch_1
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a(I)V

    .line 2242
    const/4 v0, 0x1

    goto :goto_0

    .line 2238
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 613
    invoke-interface {p1}, Landroid/view/MenuItem;->getGroupId()I

    move-result v0

    sget v3, Lg;->aR:I

    if-eq v0, v3, :cond_0

    move v0, v1

    .line 629
    :goto_0
    return v0

    .line 619
    :cond_0
    :try_start_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v0

    check-cast v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 624
    if-nez v0, :cond_1

    move v0, v1

    .line 625
    goto :goto_0

    .line 621
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0

    .line 627
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->g:Lbai;

    new-array v4, v2, [J

    iget-wide v5, v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;->id:J

    aput-wide v5, v4, v1

    invoke-direct {p0, v3, p1, v4}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a(Ljd;Landroid/view/MenuItem;[J)V

    move v0, v2

    .line 629
    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 357
    const-string v0, "ConversationListFragment.onCreate"

    invoke-static {v0}, Lbzq;->a(Ljava/lang/String;)V

    .line 358
    invoke-super {p0, p1}, Lakk;->onCreate(Landroid/os/Bundle;)V

    .line 359
    invoke-virtual {p0, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->setHasOptionsMenu(Z)V

    .line 360
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->e(Landroid/os/Bundle;)V

    .line 362
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 363
    new-instance v0, Laii;

    invoke-direct {v0, p0}, Laii;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->aA:Laii;

    .line 365
    :cond_0
    iput-boolean v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ak:Z

    .line 366
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->w()V

    .line 367
    invoke-static {}, Lbzq;->a()V

    .line 368
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->am:Z

    .line 369
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 573
    :try_start_0
    check-cast p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 578
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 579
    sget v1, Lf;->gT:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 581
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->h:Landroid/widget/AbsListView;

    check-cast v0, Lcom/google/android/apps/hangouts/listui/SwipeableListView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    iget v1, p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 582
    instance-of v1, v0, Landroid/database/Cursor;

    if-nez v1, :cond_0

    .line 609
    :goto_0
    return-void

    .line 585
    :cond_0
    check-cast v0, Landroid/database/Cursor;

    .line 586
    const/4 v1, 0x6

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 587
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 588
    const/4 v1, 0x7

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 590
    :cond_1
    iput-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->Z:Ljava/lang/String;

    .line 591
    const/4 v4, 0x3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 592
    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 593
    const/16 v6, 0x24

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 594
    invoke-static {v0}, Lf;->c(I)Z

    move-result v6

    .line 596
    invoke-interface {p1, v1}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    .line 598
    sget v0, Lg;->fL:I

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-ne v4, v7, :cond_3

    move v0, v2

    :goto_1
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 600
    sget v0, Lg;->fO:I

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-ne v4, v7, :cond_4

    if-nez v6, :cond_4

    move v0, v2

    :goto_2
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 602
    sget v0, Lg;->fK:I

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eq v4, v2, :cond_2

    if-eqz v6, :cond_5

    :cond_2
    move v0, v2

    :goto_3
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 604
    const/16 v0, 0xa

    if-ne v5, v0, :cond_6

    move v0, v2

    .line 605
    :goto_4
    sget v1, Lg;->fP:I

    invoke-interface {p1, v1}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-nez v0, :cond_7

    :goto_5
    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 606
    sget v1, Lg;->fT:I

    invoke-interface {p1, v1}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 607
    sget v0, Lg;->fR:I

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 608
    invoke-static {}, Lf;->w()Z

    move-result v1

    .line 607
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    :cond_3
    move v0, v3

    .line 598
    goto :goto_1

    :cond_4
    move v0, v3

    .line 600
    goto :goto_2

    :cond_5
    move v0, v3

    .line 602
    goto :goto_3

    :cond_6
    move v0, v3

    .line 604
    goto :goto_4

    :cond_7
    move v2, v3

    .line 605
    goto :goto_5

    .line 575
    :catch_0
    move-exception v0

    goto/16 :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Ldg;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1267
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 1271
    packed-switch p1, :pswitch_data_0

    .line 1374
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Loader created for unknown id: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    .line 1375
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1273
    :pswitch_0
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ai:I

    packed-switch v0, :pswitch_data_1

    .line 1362
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Loader created for unknown displayMode: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ai:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    .line 1363
    const/4 v0, 0x0

    goto :goto_0

    .line 1275
    :pswitch_1
    new-instance v0, Lbaj;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->getActivity()Ly;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->e:Landroid/net/Uri;

    sget-object v4, Laiq;->a:[Ljava/lang/String;

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "(%s >= 0 OR %s IS NULL) AND %s = %d AND %s > 0"

    const/4 v7, 0x5

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string v9, "is_pending_leave"

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-string v9, "is_pending_leave"

    aput-object v9, v7, v8

    const/4 v8, 0x2

    const-string v9, "view"

    aput-object v9, v7, v8

    const/4 v8, 0x3

    const/4 v9, 0x1

    .line 1282
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x4

    const-string v9, "sort_timestamp"

    aput-object v9, v7, v8

    .line 1277
    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const-string v7, "invite_time_aggregate DESC,call_media_type DESC, sort_timestamp DESC"

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lbaj;-><init>(Landroid/content/Context;Lyj;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;B)V

    goto :goto_0

    .line 1290
    :pswitch_2
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ar:Z

    if-eqz v0, :cond_0

    .line 1293
    new-instance v0, Lbaj;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->getActivity()Ly;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->f:Landroid/net/Uri;

    sget-object v4, Laiq;->a:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "inviter_affinity, sort_timestamp DESC"

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lbaj;-><init>(Landroid/content/Context;Lyj;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;B)V

    goto :goto_0

    .line 1303
    :cond_0
    new-instance v0, Lbaj;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->getActivity()Ly;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->f:Landroid/net/Uri;

    sget-object v4, Laiq;->a:[Ljava/lang/String;

    const-string v5, "inviter_affinity=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "1"

    .line 1309
    aput-object v8, v6, v7

    const-string v7, "inviter_affinity, sort_timestamp DESC"

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lbaj;-><init>(Landroid/content/Context;Lyj;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;B)V

    goto/16 :goto_0

    .line 1315
    :pswitch_3
    new-instance v0, Lbaj;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->getActivity()Ly;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->e:Landroid/net/Uri;

    sget-object v4, Laiq;->a:[Ljava/lang/String;

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "(%s >= 0 OR %s IS NULL) AND %s = %d AND %s > 0 AND %s != %s"

    const/4 v7, 0x7

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string v9, "is_pending_leave"

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-string v9, "is_pending_leave"

    aput-object v9, v7, v8

    const/4 v8, 0x2

    const-string v9, "view"

    aput-object v9, v7, v8

    const/4 v8, 0x3

    const/4 v9, 0x2

    .line 1324
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x4

    const-string v9, "sort_timestamp"

    aput-object v9, v7, v8

    const/4 v8, 0x5

    const-string v9, "status"

    aput-object v9, v7, v8

    const/4 v8, 0x6

    const/4 v9, 0x1

    .line 1327
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    .line 1319
    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const-string v7, "call_media_type DESC, sort_timestamp DESC"

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lbaj;-><init>(Landroid/content/Context;Lyj;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;B)V

    goto/16 :goto_0

    .line 1334
    :pswitch_4
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "(%s >= 0 OR %s IS NULL) AND %s = %d AND %s > 0 AND %s == %d"

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "is_pending_leave"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "is_pending_leave"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "view"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const/4 v4, 0x1

    .line 1339
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "sort_timestamp"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "status"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const/4 v4, 0x2

    .line 1342
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 1334
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 1343
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->aj:Z

    if-eqz v0, :cond_1

    .line 1344
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "transport_type"

    const-string v2, "3"

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " OR "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "transport_type"

    const-string v2, "2"

    .line 1347
    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1351
    :cond_1
    new-instance v0, Lbaj;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->getActivity()Ly;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->e:Landroid/net/Uri;

    sget-object v4, Laiq;->a:[Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "call_media_type DESC, sort_timestamp DESC"

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lbaj;-><init>(Landroid/content/Context;Lyj;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;B)V

    goto/16 :goto_0

    .line 1367
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    iget v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->af:I

    .line 1368
    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Lyj;I)Landroid/net/Uri;

    move-result-object v3

    .line 1370
    new-instance v0, Lbaj;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->getActivity()Ly;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    sget-object v4, Lais;->a:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lbaj;-><init>(Landroid/content/Context;Lyj;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;B)V

    goto/16 :goto_0

    .line 1271
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_5
    .end packed-switch

    .line 1273
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/16 v5, 0xb

    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 1182
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lbme;

    invoke-static {v0, v1}, Lcyj;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbme;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->c:Lbme;

    .line 1183
    sget v0, Lf;->eJ:I

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1185
    invoke-direct {p0, p3}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->e(Landroid/os/Bundle;)V

    .line 1187
    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/listui/SwipeableListView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->h:Landroid/widget/AbsListView;

    .line 1188
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->h:Landroid/widget/AbsListView;

    check-cast v0, Lcom/google/android/apps/hangouts/listui/SwipeableListView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->a()V

    .line 1190
    sget v0, Lf;->fO:I

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->h:Landroid/widget/AbsListView;

    invoke-virtual {p1, v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 1192
    sget v0, Lg;->ee:I

    .line 1193
    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ac:Landroid/view/View;

    .line 1195
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->h:Landroid/widget/AbsListView;

    check-cast v0, Lcom/google/android/apps/hangouts/listui/SwipeableListView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->addFooterView(Landroid/view/View;)V

    .line 1196
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ac:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1197
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->h:Landroid/widget/AbsListView;

    check-cast v0, Lcom/google/android/apps/hangouts/listui/SwipeableListView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1198
    new-instance v0, Laip;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->getActivity()Ly;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    invoke-direct {v0, p0, v2, v3, p0}, Laip;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;Landroid/content/Context;Lyj;Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->g:Lbai;

    .line 1200
    sget v0, Lg;->iu:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->an:Landroid/widget/LinearLayout;

    .line 1201
    sget v0, Lg;->iv:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ap:Landroid/widget/TextView;

    .line 1203
    new-instance v2, Laiu;

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->g:Lbai;

    check-cast v0, Laip;

    invoke-direct {v2, p0, v0}, Laiu;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;Laip;)V

    iput-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->aq:Laiu;

    .line 1204
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->h:Landroid/widget/AbsListView;

    check-cast v0, Lcom/google/android/apps/hangouts/listui/SwipeableListView;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->aq:Laiu;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1205
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->h:Landroid/widget/AbsListView;

    check-cast v0, Lcom/google/android/apps/hangouts/listui/SwipeableListView;

    new-instance v2, Laif;

    invoke-direct {v2, p0}, Laif;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1232
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->q()I

    move-result v0

    if-eq v0, v4, :cond_0

    .line 1233
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v5, :cond_3

    .line 1234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->h:Landroid/widget/AbsListView;

    check-cast v0, Lcom/google/android/apps/hangouts/listui/SwipeableListView;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->setChoiceMode(I)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->h:Landroid/widget/AbsListView;

    check-cast v0, Lcom/google/android/apps/hangouts/listui/SwipeableListView;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->aA:Laii;

    invoke-virtual {v2}, Laii;->a()Landroid/widget/AbsListView$MultiChoiceModeListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->setMultiChoiceModeListener(Landroid/widget/AbsListView$MultiChoiceModeListener;)V

    .line 1240
    :cond_0
    :goto_0
    sget v0, Lg;->hV:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ad:Lcom/google/android/apps/hangouts/listui/ActionableToastBar;

    .line 1241
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ae:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 1242
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ae:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1243
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ae:Ljava/lang/Runnable;

    .line 1246
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v5, :cond_2

    move-object v0, v1

    .line 1247
    check-cast v0, Landroid/view/ViewGroup;

    new-instance v2, Landroid/animation/LayoutTransition;

    invoke-direct {v2}, Landroid/animation/LayoutTransition;-><init>()V

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 1249
    :cond_2
    return-object v1

    .line 1236
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->h:Landroid/widget/AbsListView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->registerForContextMenu(Landroid/view/View;)V

    goto :goto_0
.end method

.method public bridge synthetic onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;
    .locals 1

    .prologue
    .line 110
    invoke-super {p0, p1, p2, p3, p4}, Lakk;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic onDestroyView()V
    .locals 0

    .prologue
    .line 110
    invoke-super {p0}, Lakk;->onDestroyView()V

    return-void
.end method

.method public bridge synthetic onDetach()V
    .locals 0

    .prologue
    .line 110
    invoke-super {p0}, Lakk;->onDetach()V

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1573
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->h:Landroid/widget/AbsListView;

    check-cast v2, Lcom/google/android/apps/hangouts/listui/SwipeableListView;

    invoke-virtual {v2}, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->getHeaderViewsCount()I

    move-result v2

    sub-int v3, p3, v2

    .line 1575
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->h:Landroid/widget/AbsListView;

    check-cast v2, Lcom/google/android/apps/hangouts/listui/SwipeableListView;

    invoke-virtual {v2}, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    invoke-interface {v2, v3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    .line 1578
    if-nez v2, :cond_1

    .line 1662
    :cond_0
    :goto_0
    return-void

    .line 1582
    :cond_1
    instance-of v3, v2, Landroid/database/Cursor;

    if-eqz v3, :cond_7

    move-object v9, v2

    .line 1583
    check-cast v9, Landroid/database/Cursor;

    .line 1585
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d()V

    .line 1587
    sget v2, Lg;->aL:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1588
    if-eqz v2, :cond_2

    .line 1589
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/View;->setSelected(Z)V

    .line 1590
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->al:Landroid/view/View;

    .line 1593
    :cond_2
    invoke-static {v9}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    .line 1594
    new-instance v3, Lbdk;

    const/16 v2, 0x10

    .line 1595
    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/16 v5, 0x11

    .line 1596
    invoke-interface {v9, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v2, v5}, Lbdk;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1597
    const/16 v2, 0x16

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v5, 0x1

    if-le v2, v5, :cond_4

    .line 1598
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    invoke-static {v2}, Lbbl;->e(Lyj;)Landroid/content/Intent;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->startActivity(Landroid/content/Intent;)V

    .line 1599
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->c:Lbme;

    invoke-interface {v2}, Lbme;->a()Laux;

    move-result-object v2

    const/16 v3, 0x615

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Laux;->a(Ljava/lang/Integer;)V

    .line 1658
    :cond_3
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->az:Lbcd;

    if-eqz v2, :cond_0

    .line 1659
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->az:Lbcd;

    invoke-virtual {v2}, Lbcd;->c()V

    goto :goto_0

    .line 1601
    :cond_4
    const/16 v2, 0xe

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v5, 0x1

    if-ne v2, v5, :cond_5

    .line 1603
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ah:Lair;

    if-eqz v2, :cond_3

    .line 1604
    const/4 v2, 0x3

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 1605
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ah:Lair;

    const/4 v6, 0x4

    .line 1606
    invoke-interface {v9, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 1605
    invoke-interface/range {v2 .. v7}, Lair;->a(Lbdk;Ljava/lang/String;IJ)V

    goto :goto_1

    .line 1609
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ah:Lair;

    if-eqz v2, :cond_3

    .line 1610
    const/4 v2, 0x3

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 1611
    const/16 v3, 0x19

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1612
    const/16 v5, 0x1f

    invoke-interface {v9, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 1613
    if-nez v3, :cond_6

    .line 1614
    const-string v3, ""

    .line 1616
    :cond_6
    const/16 v5, 0x20

    .line 1617
    invoke-interface {v9, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1618
    const/16 v6, 0x21

    .line 1619
    invoke-interface {v9, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 1620
    const/16 v7, 0x22

    .line 1621
    invoke-interface {v9, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1622
    const/16 v8, 0x23

    .line 1623
    invoke-interface {v9, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1624
    const/16 v11, 0x1d

    .line 1625
    invoke-interface {v9, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    .line 1627
    const/4 v13, 0x6

    invoke-interface {v9, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 1628
    const/4 v14, 0x7

    invoke-interface {v9, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 1630
    const/16 v15, 0x2c

    .line 1631
    invoke-interface {v9, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 1632
    const/16 v16, 0x2d

    .line 1633
    move/from16 v0, v16

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 1634
    const/16 v17, 0x24

    move/from16 v0, v17

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-static {v9}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a(Landroid/database/Cursor;)J

    move-result-wide v18

    move-wide/from16 v0, v18

    long-to-int v9, v0

    move-object/from16 v0, v17

    invoke-static {v0, v9}, Lcom/google/android/apps/hangouts/content/EsProvider;->c(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    .line 1636
    new-instance v17, Lahc;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d:Lyj;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v0, v4, v1, v2, v9}, Lahc;-><init>(Ljava/lang/String;Lyj;II)V

    .line 1640
    new-instance v2, Lyh;

    move-object v4, v10

    invoke-direct/range {v2 .. v8}, Lyh;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    iput-object v2, v0, Lahc;->e:Lyh;

    .line 1642
    move-object/from16 v0, v17

    iput-object v13, v0, Lahc;->f:Ljava/lang/String;

    .line 1643
    move-object/from16 v0, v17

    iput-object v14, v0, Lahc;->g:Ljava/lang/String;

    .line 1644
    move-object/from16 v0, v17

    iput-wide v11, v0, Lahc;->i:J

    .line 1645
    move-object/from16 v0, v17

    iput-object v15, v0, Lahc;->j:Ljava/lang/String;

    .line 1646
    move-object/from16 v0, v16

    move-object/from16 v1, v17

    iput-object v0, v1, Lahc;->k:Ljava/lang/String;

    .line 1647
    const/4 v2, 0x1

    move-object/from16 v0, v17

    iput-boolean v2, v0, Lahc;->l:Z

    .line 1648
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ah:Lair;

    move-object/from16 v0, v17

    invoke-interface {v2, v0}, Lair;->a(Lahc;)V

    goto/16 :goto_1

    .line 1651
    :cond_7
    instance-of v3, v2, Lait;

    if-eqz v3, :cond_3

    .line 1652
    check-cast v2, Lait;

    .line 1653
    iget-object v3, v2, Lait;->d:Ljava/lang/Runnable;

    if-eqz v3, :cond_3

    .line 1654
    iget-object v2, v2, Lait;->d:Ljava/lang/Runnable;

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    goto/16 :goto_1
.end method

.method public synthetic onLoadFinished(Ldg;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 110
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a(Ldg;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Ldg;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1559
    invoke-virtual {p1}, Ldg;->l()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1560
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->g:Lbai;

    if-eqz v0, :cond_0

    .line 1561
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->g:Lbai;

    check-cast v0, Laip;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Laip;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 1563
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 1564
    if-eqz v0, :cond_1

    .line 1565
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->b(Landroid/view/View;)V

    .line 1568
    :cond_1
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 2844
    invoke-super {p0}, Lakk;->onPause()V

    .line 2845
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->b()V

    .line 2846
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->h:Landroid/widget/AbsListView;

    if-eqz v0, :cond_0

    .line 2848
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->h:Landroid/widget/AbsListView;

    check-cast v0, Lcom/google/android/apps/hangouts/listui/SwipeableListView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/listui/SwipeableListView;->invalidateViews()V

    .line 2850
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->r()V

    .line 2851
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 2832
    invoke-super {p0}, Lakk;->onResume()V

    .line 2833
    const-string v0, "Babel"

    const-string v1, "Resuming ConversationListFragment"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2835
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->getLoaderManager()Lav;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lav;->b(I)Ldg;

    move-result-object v0

    .line 2836
    if-eqz v0, :cond_0

    .line 2837
    invoke-virtual {v0}, Ldg;->y()V

    .line 2839
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->c()V

    .line 2840
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 406
    invoke-super {p0, p1}, Lakk;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 407
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a:Laij;

    if-eqz v0, :cond_0

    .line 408
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a:Laij;

    invoke-virtual {v0, p1}, Laij;->a(Landroid/os/Bundle;)V

    .line 410
    :cond_0
    return-void
.end method

.method public bridge synthetic onScroll(Landroid/widget/AbsListView;III)V
    .locals 0

    .prologue
    .line 110
    invoke-super {p0, p1, p2, p3, p4}, Lakk;->onScroll(Landroid/widget/AbsListView;III)V

    return-void
.end method

.method public bridge synthetic onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 110
    invoke-super {p0, p1, p2}, Lakk;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 373
    invoke-super {p0}, Lakk;->onStart()V

    .line 374
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->g:Lbai;

    check-cast v0, Laip;

    invoke-virtual {v0}, Laip;->d()V

    .line 375
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ay:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 378
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 379
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->x()Z

    .line 382
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->f(Landroid/view/View;)V

    .line 383
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 396
    invoke-super {p0}, Lakk;->onStop()V

    .line 398
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ad:Lcom/google/android/apps/hangouts/listui/ActionableToastBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->a(Z)V

    .line 400
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ax:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ay:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 401
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->g:Lbai;

    check-cast v0, Laip;

    invoke-virtual {v0}, Laip;->c()V

    .line 402
    return-void
.end method

.method public q()I
    .locals 1

    .prologue
    .line 2392
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ai:I

    return v0
.end method

.method public r()V
    .locals 2

    .prologue
    .line 2811
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ad:Lcom/google/android/apps/hangouts/listui/ActionableToastBar;

    if-eqz v0, :cond_0

    .line 2812
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->ad:Lcom/google/android/apps/hangouts/listui/ActionableToastBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->a(Z)V

    .line 2814
    :cond_0
    return-void
.end method

.method public setUserVisibleHint(Z)V
    .locals 1

    .prologue
    .line 2855
    invoke-super {p0, p1}, Lakk;->setUserVisibleHint(Z)V

    .line 2857
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->az:Lbcd;

    if-eqz v0, :cond_0

    .line 2858
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->az:Lbcd;

    invoke-virtual {v0}, Lbcd;->c()V

    .line 2860
    :cond_0
    return-void
.end method
