.class public Lcom/google/android/apps/hangouts/phone/ConversationIntentWhitelistActivity;
.super Lcom/google/android/apps/hangouts/phone/ConversationIntentSecureActivity;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/ConversationIntentSecureActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 17
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/ConversationIntentWhitelistActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object v1

    .line 18
    if-nez v1, :cond_0

    .line 19
    const-string v1, "Babel"

    const-string v2, "must use startActivityForResult"

    invoke-static {v1, v2}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    :goto_0
    return v0

    .line 23
    :cond_0
    invoke-static {p0, v1}, Lbzp;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 24
    const-string v1, "Babel"

    const-string v2, "Bad signature"

    invoke-static {v1, v2}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 28
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
