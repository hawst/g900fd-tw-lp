.class public Lcom/google/android/apps/hangouts/settings/NotificationsSettingsActivityGingerbread;
.super Lbux;
.source "PG"


# instance fields
.field private a:Lbup;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lbux;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 28
    invoke-super {p0, p1}, Lbux;->onCreate(Landroid/os/Bundle;)V

    .line 30
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/settings/NotificationsSettingsActivityGingerbread;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 31
    new-instance v0, Lbup;

    const-string v2, "notification_pref_key"

    .line 32
    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "ringtone_pref_key"

    .line 33
    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "ringtone_type"

    const/4 v5, 0x7

    .line 34
    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    const-string v5, "vibrate_pref_key"

    .line 35
    invoke-virtual {v1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lbup;-><init>(Lbtz;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/settings/NotificationsSettingsActivityGingerbread;->a:Lbup;

    .line 36
    iget-object v0, p0, Lcom/google/android/apps/hangouts/settings/NotificationsSettingsActivityGingerbread;->a:Lbup;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/settings/NotificationsSettingsActivityGingerbread;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "account_name"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbup;->a(Ljava/lang/String;)V

    .line 37
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/hangouts/settings/NotificationsSettingsActivityGingerbread;->a:Lbup;

    const-string v0, "Babel"

    const-string v1, "NotificationSettingsUI onDestroy"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    invoke-super {p0}, Lbux;->onDestroy()V

    .line 55
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 44
    invoke-super {p0}, Lbux;->onResume()V

    .line 45
    iget-object v0, p0, Lcom/google/android/apps/hangouts/settings/NotificationsSettingsActivityGingerbread;->a:Lbup;

    const-string v0, "Babel"

    const-string v1, "NotificationSettingsUI onResume"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    return-void
.end method
