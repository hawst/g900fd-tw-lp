.class public Lcom/google/android/apps/hangouts/phone/DndDialogActivity;
.super Landroid/app/Activity;
.source "PG"


# instance fields
.field private a:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 18
    new-instance v0, Lazt;

    invoke-direct {v0, p0}, Lazt;-><init>(Lcom/google/android/apps/hangouts/phone/DndDialogActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/DndDialogActivity;->a:Ljava/lang/Runnable;

    .line 16
    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 4

    .prologue
    .line 33
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 35
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->d()Lcom/google/android/apps/hangouts/phone/EsApplication;

    move-result-object v0

    .line 36
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/DndDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "account_name"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 37
    invoke-static {v1}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v1

    .line 38
    if-eqz v1, :cond_1

    .line 40
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/DndDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "dnd_duration_choice"

    invoke-static {v2, v3}, Lbbl;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 41
    if-eqz v2, :cond_0

    .line 42
    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Lyj;Ljava/lang/String;)V

    .line 43
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/DndDialogActivity;->finish()V

    .line 51
    :goto_0
    return-void

    .line 45
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/hangouts/phone/DndDialogActivity;->a:Ljava/lang/Runnable;

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Landroid/app/Activity;Lyj;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 48
    :cond_1
    const-string v0, "Babel"

    const-string v1, "Unexpected null account in DndDialogActivity.onStart"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/DndDialogActivity;->finish()V

    goto :goto_0
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 27
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 28
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/DndDialogActivity;->finish()V

    .line 29
    return-void
.end method
