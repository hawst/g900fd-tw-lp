.class public Lcom/google/android/apps/hangouts/phone/OobePromoActivity;
.super Landroid/app/Activity;
.source "PG"

# interfaces
.implements Lcdo;


# instance fields
.field private a:Lcom/google/android/apps/hangouts/views/OobePromoStagingView;

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/hangouts/phone/OobePromoActivity;->b:I

    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 87
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->d()Lcom/google/android/apps/hangouts/phone/EsApplication;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/hangouts/phone/OobePromoActivity;->b:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->c(I)V

    .line 88
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 75
    invoke-static {}, Lbbp;->a()Lbbp;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/hangouts/phone/OobePromoActivity;->b:I

    invoke-virtual {v0}, Lbbp;->b()V

    .line 76
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/OobePromoActivity;->finish()V

    .line 77
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/OobePromoActivity;->c()V

    .line 78
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/OobePromoActivity;->finish()V

    .line 83
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/OobePromoActivity;->c()V

    .line 84
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 30
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 31
    sget v0, Lf;->gb:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/OobePromoActivity;->setContentView(I)V

    .line 32
    sget v0, Lg;->hb:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/OobePromoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/OobePromoActivity;->a:Lcom/google/android/apps/hangouts/views/OobePromoStagingView;

    .line 33
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/OobePromoActivity;->a:Lcom/google/android/apps/hangouts/views/OobePromoStagingView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->a(Lcdo;)V

    .line 35
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/OobePromoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "oobe_promo_render_data"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lbbq;

    .line 36
    if-eqz v0, :cond_0

    .line 37
    invoke-static {}, Lbbp;->a()Lbbp;

    move-result-object v1

    iget v0, v0, Lbbq;->e:I

    invoke-virtual {v1, v0}, Lbbp;->a(I)V

    .line 39
    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 55
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 56
    invoke-static {}, Lbbp;->a()Lbbp;

    move-result-object v0

    invoke-virtual {v0}, Lbbp;->c()V

    .line 57
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 49
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 50
    invoke-virtual {p0, p1}, Lcom/google/android/apps/hangouts/phone/OobePromoActivity;->setIntent(Landroid/content/Intent;)V

    .line 51
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 43
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 44
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/OobePromoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "oobe_promo_render_data"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lbbq;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/OobePromoActivity;->finish()V

    .line 45
    :cond_0
    :goto_0
    return-void

    .line 44
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/OobePromoActivity;->a:Lcom/google/android/apps/hangouts/views/OobePromoStagingView;

    if-eqz v1, :cond_0

    iget v1, v0, Lbbq;->e:I

    iput v1, p0, Lcom/google/android/apps/hangouts/phone/OobePromoActivity;->b:I

    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/OobePromoActivity;->a:Lcom/google/android/apps/hangouts/views/OobePromoStagingView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->a(Lbbq;)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/OobePromoActivity;->a:Lcom/google/android/apps/hangouts/views/OobePromoStagingView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->invalidate()V

    goto :goto_0
.end method
