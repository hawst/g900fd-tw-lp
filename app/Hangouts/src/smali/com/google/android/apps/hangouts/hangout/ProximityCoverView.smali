.class public Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;
.super Landroid/view/View;
.source "PG"


# instance fields
.field a:Lasd;

.field private final b:Landroid/hardware/SensorManager;

.field private final c:Landroid/hardware/Sensor;

.field private final d:Lasg;

.field private final e:Lase;

.field private f:Z

.field private g:F

.field private h:F

.field private i:Z

.field private j:Lasf;

.field private k:Landroid/os/PowerManager$WakeLock;

.field private l:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 147
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 86
    new-instance v1, Lasg;

    invoke-direct {v1, p0, v0}, Lasg;-><init>(Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;B)V

    iput-object v1, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->d:Lasg;

    .line 87
    new-instance v1, Lase;

    invoke-direct {v1, p0, v0}, Lase;-><init>(Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;B)V

    iput-object v1, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->e:Lase;

    .line 89
    const v1, 0x7f7fffff    # Float.MAX_VALUE

    iput v1, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->g:F

    .line 90
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->h:F

    .line 92
    iput-object v2, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->j:Lasf;

    .line 93
    iput-object v2, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->k:Landroid/os/PowerManager$WakeLock;

    .line 94
    const-string v1, "babel_proximity_wakelock_whitelist"

    invoke-static {v1, v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "babel_proximity_wakelock_blacklist"

    invoke-static {v1, v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->l:Z

    .line 148
    const-string v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->b:Landroid/hardware/SensorManager;

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->b:Landroid/hardware/SensorManager;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->c:Landroid/hardware/Sensor;

    .line 151
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->c()V

    .line 152
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;F)F
    .locals 0

    .prologue
    .line 33
    iput p1, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->h:F

    return p1
.end method

.method private a(ZZ)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 219
    iget-boolean v2, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->l:Z

    if-nez v2, :cond_1

    move v2, v1

    :goto_0
    if-nez v2, :cond_0

    .line 220
    if-eqz p1, :cond_5

    if-eqz p2, :cond_5

    :goto_1
    iget-boolean v2, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->i:Z

    if-eq v2, v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->j:Lasf;

    invoke-interface {v2, v0}, Lasf;->a(Z)V

    if-eqz v0, :cond_6

    :goto_2
    invoke-virtual {p0, v1}, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->setVisibility(I)V

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->i:Z

    .line 222
    :cond_0
    return-void

    .line 219
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->i:Z

    if-ne v2, p1, :cond_2

    move v2, v0

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_4

    invoke-direct {p0}, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->f()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->l:Z

    :goto_3
    iget-boolean v2, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->l:Z

    if-eqz v2, :cond_3

    iput-boolean p1, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->i:Z

    :cond_3
    iget-boolean v2, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->l:Z

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->g()V

    goto :goto_3

    :cond_5
    move v0, v1

    .line 220
    goto :goto_1

    :cond_6
    const/16 v1, 0x8

    goto :goto_2
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;F)F
    .locals 0

    .prologue
    .line 33
    iput p1, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->g:F

    return p1
.end method

.method private f()Z
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 99
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->l:Z

    if-nez v0, :cond_1

    .line 135
    :cond_0
    :goto_0
    return v3

    .line 103
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->k:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_2

    .line 104
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "power"

    .line 105
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 106
    const-class v1, Landroid/os/PowerManager;

    const-string v4, "PROXIMITY_SCREEN_OFF_WAKE_LOCK"

    invoke-virtual {v1, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 107
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    const/4 v4, 0x0

    invoke-static {v1, v4}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v4

    .line 108
    const-string v1, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "PROXIMITY_SCREEN_OFF_WAKE_LOCK:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    if-eqz v4, :cond_0

    .line 113
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x11

    if-lt v1, v5, :cond_3

    .line 114
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v5, "isWakeLockLevelSupported"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    sget-object v8, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v8, v6, v7

    invoke-virtual {v1, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 116
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    .line 117
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    .line 116
    invoke-virtual {v1, v0, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    const/4 v5, 0x0

    invoke-static {v1, v5}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v1

    .line 118
    const-string v5, "Babel"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "isWakeLockLevelSupported:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    :goto_1
    if-eqz v1, :cond_0

    .line 128
    const-string v1, "Babel"

    invoke-virtual {v0, v4, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->k:Landroid/os/PowerManager$WakeLock;

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->k:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    :cond_2
    move v3, v2

    .line 131
    goto/16 :goto_0

    .line 120
    :cond_3
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v5, "getSupportedWakeLockFlags"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Class;

    invoke-virtual {v1, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 121
    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    const/4 v5, 0x0

    invoke-static {v1, v5}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v1

    .line 122
    const-string v5, "Babel"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "getSupportedWakeLockFlags:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 123
    and-int/2addr v1, v4

    if-eqz v1, :cond_4

    move v1, v2

    goto :goto_1

    :cond_4
    move v1, v3

    goto :goto_1

    .line 132
    :catch_0
    move-exception v0

    .line 133
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Failed to acquire proximity and keyguard locks: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->g()V

    goto/16 :goto_0
.end method

.method private g()V
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->k:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->k:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 142
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->k:Landroid/os/PowerManager$WakeLock;

    .line 144
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->c:Landroid/hardware/Sensor;

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->b:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->d:Lasg;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->c:Landroid/hardware/Sensor;

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 162
    :cond_0
    invoke-static {}, Lapk;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 163
    invoke-static {}, Lapk;->b()Lapk;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->e:Lase;

    invoke-virtual {v0, v1}, Lapk;->a(Lapo;)V

    .line 164
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->f:Z

    .line 167
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->c()V

    .line 168
    return-void
.end method

.method public a(Lasd;)V
    .locals 0

    .prologue
    .line 249
    iput-object p1, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->a:Lasd;

    .line 250
    return-void
.end method

.method public a(Lasf;)V
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->j:Lasf;

    invoke-static {v0}, Lcwz;->a(Ljava/lang/Object;)V

    .line 241
    iput-object p1, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->j:Lasf;

    .line 242
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->c:Landroid/hardware/Sensor;

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->b:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->d:Lasg;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 174
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->f:Z

    if-eqz v0, :cond_1

    .line 175
    invoke-static {}, Lapk;->b()Lapk;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->e:Lase;

    invoke-virtual {v0, v1}, Lapk;->b(Lapo;)V

    .line 176
    iput-boolean v2, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->f:Z

    .line 179
    :cond_1
    invoke-direct {p0, v2, v2}, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->a(ZZ)V

    .line 180
    return-void
.end method

.method public c()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/high16 v7, 0x40a00000    # 5.0f

    .line 232
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->a:Lasd;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->a:Lasd;

    invoke-interface {v0}, Lasd;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 234
    :goto_0
    const-wide/16 v3, 0x0

    iget v5, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->g:F

    float-to-double v5, v5

    cmpg-double v3, v3, v5

    if-gtz v3, :cond_2

    iget v3, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->g:F

    const/high16 v4, 0x3f800000    # 1.0f

    cmpg-float v3, v3, v4

    if-ltz v3, :cond_0

    iget v3, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->g:F

    cmpg-float v3, v3, v7

    if-gtz v3, :cond_2

    iget v3, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->h:F

    cmpl-float v3, v3, v7

    if-lez v3, :cond_2

    .line 236
    :cond_0
    :goto_1
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->a(ZZ)V

    .line 237
    return-void

    :cond_1
    move v0, v2

    .line 232
    goto :goto_0

    :cond_2
    move v1, v2

    .line 234
    goto :goto_1
.end method

.method public d()V
    .locals 1

    .prologue
    .line 245
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->j:Lasf;

    .line 246
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 253
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->a:Lasd;

    .line 254
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .prologue
    .line 228
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->i:Z

    return v0
.end method
