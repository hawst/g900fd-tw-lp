.class public Lcom/google/android/apps/hangouts/phone/MoodSettingActivity;
.super Lakn;
.source "PG"

# interfaces
.implements Lalg;


# instance fields
.field private final r:Lbme;

.field private s:Lyj;

.field private t:Lcom/google/android/apps/hangouts/fragments/MoodPickerFragment;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Lakn;-><init>()V

    .line 34
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lbme;

    invoke-static {v0, v1}, Lcyj;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbme;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/MoodSettingActivity;->r:Lbme;

    .line 33
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 100
    invoke-virtual {p0, p1}, Lcom/google/android/apps/hangouts/phone/MoodSettingActivity;->setResult(I)V

    .line 101
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/MoodSettingActivity;->finish()V

    .line 102
    return-void
.end method

.method public a(Lt;)V
    .locals 1

    .prologue
    .line 61
    instance-of v0, p1, Lcom/google/android/apps/hangouts/fragments/MoodPickerFragment;

    if-eqz v0, :cond_0

    .line 62
    check-cast p1, Lcom/google/android/apps/hangouts/fragments/MoodPickerFragment;

    iput-object p1, p0, Lcom/google/android/apps/hangouts/phone/MoodSettingActivity;->t:Lcom/google/android/apps/hangouts/fragments/MoodPickerFragment;

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/MoodSettingActivity;->t:Lcom/google/android/apps/hangouts/fragments/MoodPickerFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/hangouts/fragments/MoodPickerFragment;->a(Lalg;)V

    .line 65
    :cond_0
    return-void
.end method

.method protected a(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 83
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_0

    .line 84
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/MoodSettingActivity;->onBackPressed()V

    .line 85
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/hangouts/phone/MoodSettingActivity;->setResult(I)V

    .line 95
    :goto_0
    return v0

    .line 87
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lg;->ar:I

    if-ne v1, v2, :cond_1

    .line 88
    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/MoodSettingActivity;->s:Lyj;

    const-string v2, ""

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->d(Lyj;Ljava/lang/String;)V

    .line 89
    const/4 v1, -0x1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/hangouts/phone/MoodSettingActivity;->setResult(I)V

    .line 90
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/MoodSettingActivity;->finish()V

    .line 91
    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/MoodSettingActivity;->r:Lbme;

    invoke-interface {v1}, Lbme;->a()Laux;

    move-result-object v1

    const/16 v2, 0x647

    .line 92
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 91
    invoke-virtual {v1, v2}, Laux;->a(Ljava/lang/Integer;)V

    goto :goto_0

    .line 95
    :cond_1
    invoke-super {p0, p1}, Lakn;->a(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected k()Lyj;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/MoodSettingActivity;->s:Lyj;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 45
    invoke-super {p0, p1}, Lakn;->onCreate(Landroid/os/Bundle;)V

    .line 48
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/MoodSettingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 47
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/MoodSettingActivity;->s:Lyj;

    .line 50
    sget v0, Lf;->fX:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/MoodSettingActivity;->setContentView(I)V

    .line 52
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/MoodSettingActivity;->g()Lkd;

    move-result-object v0

    .line 53
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lkd;->a(Z)V

    .line 54
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/MoodSettingActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    sget v1, Lf;->hb:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 73
    invoke-super {p0, p1}, Lakn;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method
