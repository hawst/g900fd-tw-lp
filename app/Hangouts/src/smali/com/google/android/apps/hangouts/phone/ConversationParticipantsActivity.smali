.class public Lcom/google/android/apps/hangouts/phone/ConversationParticipantsActivity;
.super Lakn;
.source "PG"


# instance fields
.field private r:Lyj;

.field private s:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lakn;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lt;)V
    .locals 1

    .prologue
    .line 45
    instance-of v0, p1, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    if-eqz v0, :cond_0

    .line 46
    check-cast p1, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    iput-object p1, p0, Lcom/google/android/apps/hangouts/phone/ConversationParticipantsActivity;->s:Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;

    .line 48
    :cond_0
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 57
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 63
    invoke-super {p0, p1}, Lakn;->a(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 59
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/ConversationParticipantsActivity;->onBackPressed()V

    .line 60
    const/4 v0, 0x1

    goto :goto_0

    .line 57
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected k()Lyj;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ConversationParticipantsActivity;->r:Lyj;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 28
    invoke-super {p0, p1}, Lakn;->onCreate(Landroid/os/Bundle;)V

    .line 31
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/ConversationParticipantsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 30
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/ConversationParticipantsActivity;->r:Lyj;

    .line 33
    sget v0, Lf;->eP:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/ConversationParticipantsActivity;->setContentView(I)V

    .line 35
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/ConversationParticipantsActivity;->g()Lkd;

    move-result-object v0

    .line 36
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lkd;->a(Z)V

    .line 37
    sget v1, Lh;->bm:I

    invoke-virtual {v0, v1}, Lkd;->b(I)V

    .line 38
    return-void
.end method
