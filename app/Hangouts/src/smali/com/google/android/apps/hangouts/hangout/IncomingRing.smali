.class public final Lcom/google/android/apps/hangouts/hangout/IncomingRing;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Laac;
.implements Lbrj;
.implements Lbrn;


# static fields
.field private static final a:Z

.field private static final b:[J

.field private static c:Ljava/lang/String;

.field private static d:Lcom/google/android/apps/hangouts/hangout/IncomingRing;


# instance fields
.field private final A:Lbyv;

.field private final B:Ljava/lang/String;

.field private final C:Lbdh;

.field private D:Landroid/graphics/Bitmap;

.field private E:Z

.field private F:Z

.field private final G:Ljava/lang/Runnable;

.field private final e:Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;

.field private final f:Lyj;

.field private final g:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

.field private final h:Ljava/lang/String;

.field private final i:J

.field private final j:J

.field private final k:J

.field private l:Ljava/lang/String;

.field private final m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private final o:Landroid/app/NotificationManager;

.field private p:Lbk;

.field private q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbdk;",
            ">;"
        }
    .end annotation
.end field

.field private final r:[Ljava/lang/String;

.field private s:I

.field private t:I

.field private final u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private v:Ljava/lang/String;

.field private w:I

.field private final x:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Laqp;",
            ">;"
        }
    .end annotation
.end field

.field private y:Landroid/os/Vibrator;

.field private final z:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 120
    sget-object v0, Lbys;->d:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->a:Z

    .line 126
    const/4 v0, 0x2

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->b:[J

    return-void

    :array_0
    .array-data 8
        0x3e8
        0x3e8
    .end array-data
.end method

.method private constructor <init>(Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;JLcom/google/android/libraries/hangouts/video/HangoutRequest;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 195
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->r:[Ljava/lang/String;

    .line 151
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->u:Ljava/util/List;

    .line 154
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->x:Ljava/util/ArrayList;

    .line 156
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->z:Landroid/os/Handler;

    .line 157
    new-instance v0, Lbyv;

    const-string v1, "Babel"

    invoke-direct {v0, v1}, Lbyv;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->A:Lbyv;

    .line 165
    new-instance v0, Laqm;

    invoke-direct {v0, p0}, Laqm;-><init>(Lcom/google/android/apps/hangouts/hangout/IncomingRing;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->G:Ljava/lang/Runnable;

    .line 196
    iput-object p1, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->e:Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;

    .line 197
    iput-object p4, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->g:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    .line 198
    iput-object p5, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->h:Ljava/lang/String;

    .line 199
    iput-object p6, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->m:Ljava/lang/String;

    .line 200
    iput-object p7, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->B:Ljava/lang/String;

    .line 201
    iput-wide p2, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->i:J

    .line 202
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->k:J

    .line 203
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->j:J

    .line 204
    iput-object p8, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->l:Ljava/lang/String;

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->g:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getCallMediaType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->F:Z

    .line 207
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->g:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getAccountName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->f:Lyj;

    .line 209
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->e:Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->o:Landroid/app/NotificationManager;

    .line 212
    invoke-static {p9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 213
    :goto_1
    iput-object v3, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->C:Lbdh;

    .line 215
    return-void

    :cond_0
    move v0, v2

    .line 205
    goto :goto_0

    :cond_1
    move-object v0, p7

    move-object v1, p9

    move-object v4, v3

    move v5, v2

    .line 213
    invoke-static/range {v0 .. v5}, Lbdh;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;I)Lbdh;

    move-result-object v3

    goto :goto_1
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/hangout/IncomingRing;Landroid/os/Vibrator;)Landroid/os/Vibrator;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->y:Landroid/os/Vibrator;

    return-object p1
.end method

.method public static a()Lcom/google/android/apps/hangouts/hangout/IncomingRing;
    .locals 1

    .prologue
    .line 179
    sget-object v0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->d:Lcom/google/android/apps/hangouts/hangout/IncomingRing;

    return-object v0
.end method

.method static a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 841
    sget-object v0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->d:Lcom/google/android/apps/hangouts/hangout/IncomingRing;

    if-eqz v0, :cond_0

    const-string v0, "from_notification"

    const/4 v1, 0x0

    .line 842
    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 845
    sget-object v0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->d:Lcom/google/android/apps/hangouts/hangout/IncomingRing;

    invoke-direct {v0}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->x()V

    .line 847
    :cond_0
    return-void
.end method

.method public static a(Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;JLcom/google/android/libraries/hangouts/video/HangoutRequest;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 186
    new-instance v0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;-><init>(Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;JLcom/google/android/libraries/hangouts/video/HangoutRequest;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    iget-object v1, v0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->m:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->f:Lyj;

    invoke-static {v1}, Lbrk;->a(Lyj;)Lbrk;

    move-result-object v1

    new-instance v2, Lyf;

    iget-object v3, v0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->m:Ljava/lang/String;

    invoke-direct {v2, v3, v0}, Lyf;-><init>(Ljava/lang/String;Lbrn;)V

    invoke-virtual {v1, v2}, Lbrk;->a(Lbrv;)Z

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->i()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->B:Ljava/lang/String;

    const/4 v2, 0x1

    iget-object v3, v0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->f:Lyj;

    invoke-static {v1, v2, v3, v0}, Lbrf;->a(Ljava/lang/String;ZLyj;Lbrj;)Lyb;

    :goto_0
    iget-object v1, v0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->e:Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->e()V

    iget-object v1, v0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->e:Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v3

    iget-object v1, v0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->f:Lyj;

    const/4 v4, 0x3

    const/4 v5, 0x3

    iget-object v6, v0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->g:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-virtual {v6}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getConversationId()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v4, v5, v6}, Lbzb;->a(Lyj;IILjava/lang/String;)I

    move-result v1

    add-int/lit8 v4, v1, 0x1

    add-int/lit8 v5, v1, 0x2

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->f()Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v6

    invoke-direct {v0}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->t()I

    move-result v7

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    invoke-static {v6, v7, v8, v9}, Lbbl;->a(Lcom/google/android/libraries/hangouts/video/HangoutRequest;IJ)Landroid/content/Intent;

    move-result-object v6

    const/high16 v7, 0x8000000

    invoke-static {v3, v1, v6, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    sget-object v1, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->c:Ljava/lang/String;

    invoke-static {v1}, Lbbl;->d(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const/high16 v7, 0x8000000

    invoke-static {v3, v4, v1, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-static {}, Lbbl;->g()Landroid/content/Intent;

    move-result-object v1

    const/high16 v7, 0x8000000

    invoke-static {v3, v5, v1, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    new-instance v1, Lbk;

    iget-object v7, v0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->e:Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;

    invoke-direct {v1, v7}, Lbk;-><init>(Landroid/content/Context;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-virtual {v1, v7, v8}, Lbk;->a(J)Lbk;

    move-result-object v7

    iget-boolean v1, v0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->F:Z

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->i()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_1
    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->cO:I

    :goto_1
    invoke-virtual {v7, v1}, Lbk;->a(I)Lbk;

    move-result-object v1

    const/4 v7, 0x4

    invoke-virtual {v1, v7}, Lbk;->b(I)Lbk;

    move-result-object v1

    invoke-virtual {v1}, Lbk;->b()Lbk;

    move-result-object v1

    const/4 v7, 0x0

    invoke-virtual {v1, v7}, Lbk;->a(Z)Lbk;

    move-result-object v1

    const/4 v7, 0x2

    invoke-virtual {v1, v7}, Lbk;->c(I)Lbk;

    move-result-object v1

    invoke-virtual {v1, v5}, Lbk;->a(Landroid/app/PendingIntent;)Lbk;

    move-result-object v1

    sget v7, Lcom/google/android/apps/hangouts/R$drawable;->bd:I

    sget v8, Lh;->fo:I

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v7, v8, v4}, Lbk;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Lbk;

    move-result-object v1

    sget v4, Lcom/google/android/apps/hangouts/R$drawable;->bf:I

    sget v7, Lh;->fn:I

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v4, v2, v6}, Lbk;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Lbk;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->p:Lbk;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_4

    iget-object v1, v0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->p:Lbk;

    invoke-virtual {v1, v5}, Lbk;->c(Landroid/app/PendingIntent;)Lbk;

    :goto_2
    invoke-direct {v0}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->v()V

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->E:Z

    iget-object v1, v0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->e:Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lh;->fs:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v3, Lf;->bP:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    iget-object v1, v0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->e:Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;

    const-string v4, "audio"

    invoke-virtual {v1, v4}, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x1

    :goto_3
    new-instance v4, Laqn;

    invoke-direct {v4, v0, v2, v3, v1}, Laqn;-><init>(Lcom/google/android/apps/hangouts/hangout/IncomingRing;Ljava/lang/String;ZZ)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v4, v1}, Laqn;->executeOnThreadPool([Ljava/lang/Object;)Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;

    iget-object v1, v0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->f:Lyj;

    invoke-static {v1, v0}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->a(Lyj;Lcom/google/android/apps/hangouts/hangout/IncomingRing;)V

    iget-object v1, v0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->z:Landroid/os/Handler;

    iget-object v0, v0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->G:Ljava/lang/Runnable;

    const-wide/32 v2, 0x88b8

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 190
    return-void

    .line 189
    :cond_2
    iget-object v1, v0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->h:Ljava/lang/String;

    invoke-static {v1}, Lbdk;->b(Ljava/lang/String;)Lbdk;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->f:Lyj;

    invoke-static {v1, v2, v0}, Lbrf;->a(Lbdk;Lyj;Lbri;)Lyb;

    goto/16 :goto_0

    :cond_3
    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->cK:I

    goto/16 :goto_1

    :cond_4
    invoke-static {}, Lbbl;->g()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_2

    :cond_5
    const/4 v1, 0x0

    goto :goto_3
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/hangout/IncomingRing;)V
    .locals 3

    .prologue
    .line 82
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->a(ZZI)V

    return-void
.end method

.method static a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 879
    sput-object p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->c:Ljava/lang/String;

    .line 880
    return-void
.end method

.method private static a(Lyj;Lcom/google/android/apps/hangouts/hangout/IncomingRing;)V
    .locals 4

    .prologue
    .line 173
    sput-object p1, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->d:Lcom/google/android/apps/hangouts/hangout/IncomingRing;

    .line 174
    const-wide/16 v0, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {p0, v0, v1, v2, v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;JIZ)V

    .line 176
    return-void
.end method

.method private a(ZZI)V
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x2

    const/4 v3, 0x0

    .line 525
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->E:Z

    if-eqz v0, :cond_2

    .line 526
    sget-object v0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->d:Lcom/google/android/apps/hangouts/hangout/IncomingRing;

    if-ne v0, p0, :cond_0

    const-string v0, "same"

    .line 528
    :goto_0
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Stop called twice. ActiveRing "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 529
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->E:Z

    invoke-static {v0}, Lcwz;->b(Z)V

    .line 566
    :goto_1
    return-void

    .line 526
    :cond_0
    sget-object v0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->d:Lcom/google/android/apps/hangouts/hangout/IncomingRing;

    if-nez v0, :cond_1

    const-string v0, "null"

    goto :goto_0

    :cond_1
    const-string v0, "different"

    goto :goto_0

    .line 532
    :cond_2
    iput-boolean v10, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->E:Z

    .line 533
    invoke-static {p3, v4, v11}, Lcwz;->a(III)V

    new-instance v0, Ldzk;

    invoke-direct {v0}, Ldzk;-><init>()V

    iget-wide v5, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->i:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v0, Ldzk;->b:Ljava/lang/Long;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->f()Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getHangoutId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Ldzk;->c:Ljava/lang/String;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v0, Ldzk;->h:Ljava/lang/Integer;

    new-instance v2, Ldzl;

    invoke-direct {v2}, Ldzl;-><init>()V

    iget-wide v5, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->j:J

    const-wide/16 v7, 0x3e8

    mul-long/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iput-object v5, v2, Ldzl;->b:Ljava/lang/Long;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    iget-wide v7, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->k:J

    sub-long/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iput-object v5, v2, Ldzl;->c:Ljava/lang/Long;

    iput-object v2, v0, Ldzk;->i:Ldzl;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->f:Lyj;

    invoke-static {v2, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ldzk;)I

    .line 536
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 537
    if-eqz p2, :cond_5

    move v9, v4

    .line 539
    :goto_2
    new-instance v0, Lbkn;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->B:Ljava/lang/String;

    const/16 v8, 0x3e

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-direct/range {v0 .. v8}, Lbkn;-><init>(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 544
    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->f:Lyj;

    invoke-virtual {v2}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v9, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Ljava/lang/String;ILbkn;)I

    .line 547
    if-nez p2, :cond_3

    .line 548
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->i()Z

    move-result v0

    invoke-static {v0}, Lcwz;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->e:Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->D:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->D:Landroid/graphics/Bitmap;

    :goto_3
    iget-object v4, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->f:Lyj;

    invoke-static {v4, v10, v1, v3}, Lbzb;->a(Lyj;IILjava/lang/String;)I

    move-result v4

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->f:Lyj;

    invoke-static {v6}, Lbbl;->o(Lyj;)Landroid/content/Intent;

    move-result-object v6

    const/high16 v7, 0x8000000

    invoke-static {v5, v4, v6, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    new-instance v5, Lbk;

    iget-object v6, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->e:Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;

    invoke-direct {v5, v6}, Lbk;-><init>(Landroid/content/Context;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lbk;->a(J)Lbk;

    move-result-object v5

    invoke-virtual {v5, v10}, Lbk;->a(Z)Lbk;

    move-result-object v5

    sget v6, Lh;->oE:I

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lbk;->c(Ljava/lang/CharSequence;)Lbk;

    move-result-object v5

    sget v6, Lcom/google/android/apps/hangouts/R$drawable;->cM:I

    invoke-virtual {v5, v6}, Lbk;->a(I)Lbk;

    move-result-object v5

    invoke-virtual {v5, v11}, Lbk;->b(I)Lbk;

    move-result-object v5

    invoke-virtual {v5, v1}, Lbk;->c(I)Lbk;

    move-result-object v5

    invoke-virtual {v5, v0}, Lbk;->a(Landroid/graphics/Bitmap;)Lbk;

    move-result-object v0

    invoke-virtual {v0, v4}, Lbk;->a(Landroid/app/PendingIntent;)Lbk;

    move-result-object v0

    sget v4, Lh;->oE:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lbk;->a(Ljava/lang/CharSequence;)Lbk;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->l:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->B:Ljava/lang/String;

    :goto_4
    invoke-virtual {v2, v0}, Lbk;->b(Ljava/lang/CharSequence;)Lbk;

    move-result-object v0

    invoke-virtual {v0}, Lbk;->e()Landroid/app/Notification;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->o:Landroid/app/NotificationManager;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->e:Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;

    invoke-virtual {v5}, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":missed_pstn_notification:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->f:Lyj;

    invoke-virtual {v5}, Lyj;->j()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4, v1, v0}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 550
    :cond_3
    iput-object v3, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->D:Landroid/graphics/Bitmap;

    .line 553
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->o:Landroid/app/NotificationManager;

    sget-object v1, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->c:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 555
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->z:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->G:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 557
    invoke-virtual {p0, p1}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->a(Z)V

    .line 559
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->x:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laqp;

    .line 560
    invoke-interface {v0}, Laqp;->a()V

    goto :goto_5

    :cond_5
    move v9, v1

    .line 537
    goto/16 :goto_2

    .line 548
    :cond_6
    invoke-static {}, Lyn;->q()Landroid/graphics/Bitmap;

    move-result-object v0

    goto/16 :goto_3

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->l:Ljava/lang/String;

    goto :goto_4

    .line 562
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->f:Lyj;

    invoke-static {v0, v3}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->a(Lyj;Lcom/google/android/apps/hangouts/hangout/IncomingRing;)V

    .line 565
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->e:Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->c()V

    goto/16 :goto_1
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/hangout/IncomingRing;)Lyj;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->f:Lyj;

    return-object v0
.end method

.method static b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 883
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":hangouts_ring_notification"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->c:Ljava/lang/String;

    .line 884
    return-void
.end method

.method public static synthetic c(Lcom/google/android/apps/hangouts/hangout/IncomingRing;)Landroid/net/Uri;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 82
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->F:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Landroid/provider/Settings$System;->DEFAULT_RINGTONE_URI:Landroid/net/Uri;

    new-array v1, v5, [Ljava/lang/String;

    sget v2, Lf;->hI:I

    invoke-static {v2}, Lf;->g(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lf;->a(Landroid/net/Uri;[Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->u()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->f:Lyj;

    invoke-static {v1}, Lf;->b(Lyj;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Landroid/provider/Settings$System;->DEFAULT_RINGTONE_URI:Landroid/net/Uri;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    aput-object v0, v3, v4

    aput-object v1, v3, v5

    const/4 v0, 0x2

    sget v1, Lf;->hN:I

    invoke-static {v1}, Lf;->g(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    invoke-static {v2, v3}, Lf;->a(Landroid/net/Uri;[Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public static synthetic d(Lcom/google/android/apps/hangouts/hangout/IncomingRing;)Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->E:Z

    return v0
.end method

.method public static synthetic e(Lcom/google/android/apps/hangouts/hangout/IncomingRing;)Lbyv;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->A:Lbyv;

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/apps/hangouts/hangout/IncomingRing;)Landroid/os/Vibrator;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->y:Landroid/os/Vibrator;

    return-object v0
.end method

.method static p()V
    .locals 3

    .prologue
    .line 871
    sget-object v0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->d:Lcom/google/android/apps/hangouts/hangout/IncomingRing;

    invoke-static {v0}, Lcwz;->a(Ljava/lang/Object;)V

    .line 873
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 874
    sget-object v1, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->c:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 876
    return-void
.end method

.method public static synthetic q()Z
    .locals 1

    .prologue
    .line 82
    sget-boolean v0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->a:Z

    return v0
.end method

.method public static synthetic r()[J
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->b:[J

    return-object v0
.end method

.method static synthetic s()Lcom/google/android/apps/hangouts/hangout/IncomingRing;
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->d:Lcom/google/android/apps/hangouts/hangout/IncomingRing;

    return-object v0
.end method

.method private t()I
    .locals 1

    .prologue
    .line 608
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private u()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 691
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->m:Ljava/lang/String;

    if-nez v0, :cond_1

    move-object v0, v6

    .line 710
    :cond_0
    :goto_0
    return-object v0

    .line 694
    :cond_1
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->m:Ljava/lang/String;

    aput-object v0, v4, v1

    .line 695
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->e:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->f:Lyj;

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Landroid/net/Uri;Lyj;)Landroid/net/Uri;

    move-result-object v1

    .line 698
    :try_start_0
    invoke-static {}, Lzo;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Laqo;->a:[Ljava/lang/String;

    const-string v3, "conversation_id=?"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 702
    if-eqz v1, :cond_2

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 703
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 706
    if-eqz v1, :cond_0

    .line 707
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 706
    :cond_2
    if-eqz v1, :cond_3

    .line 707
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    move-object v0, v6

    .line 710
    goto :goto_0

    .line 706
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v6, :cond_4

    .line 707
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 706
    :catchall_1
    move-exception v0

    move-object v6, v1

    goto :goto_1
.end method

.method private v()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 770
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->e:Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->n:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->n:Ljava/lang/String;

    .line 771
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->p:Lbk;

    invoke-virtual {v2, v0}, Lbk;->a(Ljava/lang/CharSequence;)Lbk;

    move-result-object v2

    invoke-virtual {v2, v0}, Lbk;->c(Ljava/lang/CharSequence;)Lbk;

    .line 772
    iget-object v3, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->p:Lbk;

    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->e:Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;

    .line 773
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    sget v0, Lh;->oD:I

    move-object v1, v2

    :goto_1
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 772
    :goto_2
    invoke-virtual {v3, v0}, Lbk;->b(Ljava/lang/CharSequence;)Lbk;

    .line 774
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->x:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laqp;

    invoke-interface {v0}, Laqp;->b()V

    goto :goto_3

    .line 770
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->l:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->i()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->B:Ljava/lang/String;

    goto :goto_0

    :cond_1
    sget v2, Lh;->dF:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->l:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v3, Lh;->cA:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move v0, v1

    :goto_4
    iget v4, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->t:I

    if-ge v0, v4, :cond_3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->r:[Ljava/lang/String;

    aget-object v5, v5, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 773
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->n:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->l:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->F:Z

    if-eqz v0, :cond_5

    sget v0, Lh;->oP:I

    :goto_5
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->l:Ljava/lang/String;

    aput-object v5, v4, v1

    invoke-virtual {v2, v0, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_5
    sget v0, Lh;->eV:I

    goto :goto_5

    :cond_6
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->F:Z

    if-eqz v0, :cond_7

    sget v0, Lh;->oO:I

    move-object v1, v2

    goto/16 :goto_1

    :cond_7
    sget v0, Lh;->eq:I

    move-object v1, v2

    goto/16 :goto_1

    .line 775
    :cond_8
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->w()V

    .line 776
    return-void
.end method

.method private w()V
    .locals 4

    .prologue
    .line 780
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->p:Lbk;

    invoke-virtual {v0}, Lbk;->e()Landroid/app/Notification;

    move-result-object v0

    .line 781
    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v1, v1, 0x4

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 782
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->o:Landroid/app/NotificationManager;

    sget-object v2, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->c:Ljava/lang/String;

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3, v0}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 784
    return-void
.end method

.method private x()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 831
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->m:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 832
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->f:Lyj;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->m:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ljava/lang/String;I)V

    .line 837
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0, v3, v3}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->a(ZZI)V

    .line 838
    return-void
.end method


# virtual methods
.method a(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v4, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 364
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 366
    sget v1, Lh;->en:I

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->l:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->B:Ljava/lang/String;

    :goto_0
    aput-object v0, v3, v2

    invoke-virtual {p1, v1, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 420
    :goto_1
    return-object v0

    .line 366
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->l:Ljava/lang/String;

    goto :goto_0

    .line 369
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->n:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 370
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->l:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 372
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->F:Z

    if-eqz v0, :cond_2

    sget v0, Lh;->oK:I

    :goto_2
    new-array v1, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->n:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    sget v0, Lh;->el:I

    goto :goto_2

    .line 378
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->F:Z

    if-eqz v0, :cond_4

    sget v0, Lh;->oJ:I

    :goto_3
    new-array v1, v6, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->n:Ljava/lang/String;

    aput-object v3, v1, v2

    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->l:Ljava/lang/String;

    aput-object v2, v1, v5

    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    sget v0, Lh;->ek:I

    goto :goto_3

    .line 382
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->l:Ljava/lang/String;

    if-nez v0, :cond_7

    .line 384
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->F:Z

    if-eqz v0, :cond_6

    sget v0, Lh;->oL:I

    :goto_4
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_6
    sget v0, Lh;->em:I

    goto :goto_4

    .line 389
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->q:Ljava/util/List;

    if-nez v0, :cond_8

    move v1, v2

    .line 390
    :goto_5
    if-nez v1, :cond_a

    .line 392
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->F:Z

    if-eqz v0, :cond_9

    sget v0, Lh;->oM:I

    :goto_6
    new-array v1, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->l:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 389
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    move v1, v0

    goto :goto_5

    .line 392
    :cond_9
    sget v0, Lh;->eo:I

    goto :goto_6

    .line 397
    :cond_a
    iget v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->t:I

    if-ge v0, v1, :cond_c

    .line 399
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->F:Z

    if-eqz v0, :cond_b

    sget v0, Lf;->hE:I

    :goto_7
    new-array v3, v6, [Ljava/lang/Object;

    .line 402
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->l:Ljava/lang/String;

    aput-object v2, v3, v5

    .line 399
    invoke-virtual {p1, v0, v1, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_b
    sget v0, Lf;->hm:I

    goto :goto_7

    .line 404
    :cond_c
    if-le v1, v6, :cond_e

    .line 406
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->F:Z

    if-eqz v0, :cond_d

    sget v0, Lf;->hD:I

    :goto_8
    add-int/lit8 v3, v1, -0x1

    new-array v4, v4, [Ljava/lang/Object;

    add-int/lit8 v1, v1, -0x1

    .line 409
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v2

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->l:Ljava/lang/String;

    aput-object v1, v4, v5

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->r:[Ljava/lang/String;

    aget-object v1, v1, v2

    aput-object v1, v4, v6

    .line 406
    invoke-virtual {p1, v0, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_d
    sget v0, Lf;->hl:I

    goto :goto_8

    .line 412
    :cond_e
    if-ne v1, v5, :cond_10

    .line 414
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->F:Z

    if-eqz v0, :cond_f

    sget v0, Lh;->oI:I

    :goto_9
    new-array v1, v6, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->l:Ljava/lang/String;

    aput-object v3, v1, v2

    iget-object v3, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->r:[Ljava/lang/String;

    aget-object v2, v3, v2

    aput-object v2, v1, v5

    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_f
    sget v0, Lh;->ej:I

    goto :goto_9

    .line 420
    :cond_10
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->F:Z

    if-eqz v0, :cond_11

    sget v0, Lh;->oN:I

    :goto_a
    new-array v1, v4, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->l:Ljava/lang/String;

    aput-object v3, v1, v2

    iget-object v3, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->r:[Ljava/lang/String;

    aget-object v2, v3, v2

    aput-object v2, v1, v5

    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->r:[Ljava/lang/String;

    aget-object v2, v2, v5

    aput-object v2, v1, v6

    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_11
    sget v0, Lh;->ep:I

    goto :goto_a
.end method

.method a(Laqp;)V
    .locals 1

    .prologue
    .line 463
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->x:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 464
    return-void
.end method

.method public a(Lbzn;Lbyd;ZLzx;Z)V
    .locals 2

    .prologue
    .line 445
    invoke-static {p2}, Lcwz;->a(Ljava/lang/Object;)V

    .line 446
    if-nez p3, :cond_1

    .line 460
    :cond_0
    :goto_0
    return-void

    .line 451
    :cond_1
    invoke-virtual {p1}, Lbzn;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 452
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->p:Lbk;

    invoke-virtual {v1, v0}, Lbk;->a(Landroid/graphics/Bitmap;)Lbk;

    .line 453
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->w()V

    .line 457
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 458
    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->D:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;ILaan;Lyj;)V
    .locals 6

    .prologue
    .line 285
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->a(Ljava/lang/String;Ljava/lang/String;Lyb;Ljava/lang/String;Lyj;)V

    .line 286
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lyb;Ljava/lang/String;Lyj;)V
    .locals 10

    .prologue
    .line 266
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->B:Ljava/lang/String;

    invoke-virtual {p3}, Lyb;->d()Lbcn;

    move-result-object v1

    iget-object v1, v1, Lbcn;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    if-eqz v0, :cond_5

    .line 267
    if-eqz p1, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 268
    iput-object p1, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->l:Ljava/lang/String;

    .line 270
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->u:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 275
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->w:I

    if-eq v0, v1, :cond_3

    iget v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->s:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->u:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->u:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->u:Ljava/util/List;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->f:Lyj;

    invoke-static {}, Lyn;->b()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->m:Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->v:Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v5, p0

    invoke-static/range {v0 .. v9}, Lxy;->a(Ljava/util/List;ILyj;ILjava/lang/String;Laac;Ljava/lang/Object;Ljava/lang/String;ZZ)Lzx;

    move-result-object v1

    if-eqz v1, :cond_3

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x14

    if-le v0, v2, :cond_6

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v1, v0}, Lzx;->a(Z)V

    invoke-virtual {v1}, Lzx;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->v:Ljava/lang/String;

    invoke-static {}, Lbsn;->c()Lbsn;

    move-result-object v0

    invoke-virtual {v0, v1}, Lbsn;->a(Lbrv;)Z

    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->w:I

    .line 276
    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->v()V

    .line 279
    return-void

    .line 266
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->h:Ljava/lang/String;

    invoke-virtual {p3}, Lyb;->d()Lbcn;

    move-result-object v1

    iget-object v1, v1, Lbcn;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 271
    :cond_5
    iget v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->t:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_1

    .line 272
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->r:[Ljava/lang/String;

    iget v1, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->t:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->t:I

    aput-object p4, v0, v1

    .line 273
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->u:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 275
    :cond_6
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public a(Lyb;)V
    .locals 0

    .prologue
    .line 322
    return-void
.end method

.method public a(Lyv;)V
    .locals 5

    .prologue
    .line 243
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->f:Lyj;

    invoke-virtual {v0}, Lyj;->c()Lbdk;

    move-result-object v1

    .line 244
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p1, Lyv;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->q:Ljava/util/List;

    .line 245
    iget-object v0, p1, Lyv;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    .line 246
    iget-object v0, v0, Lbdh;->b:Lbdk;

    .line 247
    invoke-virtual {v1, v0}, Lbdk;->a(Lbdk;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->h:Ljava/lang/String;

    iget-object v4, v0, Lbdk;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 248
    iget v3, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->s:I

    const/4 v4, 0x3

    if-ge v3, v4, :cond_1

    .line 249
    iget-object v3, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->f:Lyj;

    invoke-static {v0, v3, p0}, Lbrf;->a(Lbdk;Lyj;Lbri;)Lyb;

    .line 250
    iget v3, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->s:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->s:I

    .line 252
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->q:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 255
    :cond_2
    iget-object v0, p1, Lyv;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->n:Ljava/lang/String;

    .line 256
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->v()V

    .line 257
    return-void
.end method

.method a(Z)V
    .locals 1

    .prologue
    .line 471
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->A:Lbyv;

    invoke-virtual {v0, p1}, Lbyv;->a(Z)V

    .line 472
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->y:Landroid/os/Vibrator;

    if-eqz v0, :cond_0

    .line 473
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->y:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 475
    :cond_0
    return-void
.end method

.method b(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 437
    sget v0, Lh;->ei:I

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->f:Lyj;

    .line 438
    invoke-virtual {v3}, Lyj;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 437
    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 261
    return-void
.end method

.method b(Laqp;)V
    .locals 1

    .prologue
    .line 467
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->x:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 468
    return-void
.end method

.method c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 428
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 429
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 430
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->B:Ljava/lang/String;

    .line 433
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method d()Lyj;
    .locals 1

    .prologue
    .line 569
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->f:Lyj;

    return-object v0
.end method

.method e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbdk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 574
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->q:Ljava/util/List;

    return-object v0
.end method

.method public f()Lcom/google/android/libraries/hangouts/video/HangoutRequest;
    .locals 1

    .prologue
    .line 584
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->g:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    return-object v0
.end method

.method g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 592
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->h:Ljava/lang/String;

    return-object v0
.end method

.method h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 596
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->l:Ljava/lang/String;

    return-object v0
.end method

.method i()Z
    .locals 1

    .prologue
    .line 600
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->B:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method j()Z
    .locals 1

    .prologue
    .line 604
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->F:Z

    return v0
.end method

.method k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 613
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->i()Z

    move-result v0

    invoke-static {v0}, Lcwz;->a(Z)V

    .line 614
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->B:Ljava/lang/String;

    return-object v0
.end method

.method public l()V
    .locals 9

    .prologue
    const/4 v3, 0x1

    .line 787
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->x()V

    .line 790
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->f()Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->C:Lbdh;

    const/16 v5, 0x3e

    .line 791
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->t()I

    move-result v6

    .line 792
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v7

    move v4, v3

    .line 789
    invoke-static/range {v0 .. v8}, Lbbl;->a(Lcom/google/android/libraries/hangouts/video/HangoutRequest;Ljava/util/ArrayList;Lbdh;ZZIIJ)Landroid/content/Intent;

    move-result-object v0

    .line 793
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 794
    return-void
.end method

.method public m()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 815
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->m:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 816
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->f:Lyj;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->m:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ljava/lang/String;I)V

    .line 819
    :cond_0
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->a(ZZI)V

    .line 820
    return-void
.end method

.method n()V
    .locals 3

    .prologue
    .line 823
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->a(ZZI)V

    .line 824
    return-void
.end method

.method public o()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 827
    const/4 v0, 0x1

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->a(ZZI)V

    .line 828
    return-void
.end method
