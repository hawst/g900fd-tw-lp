.class public Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;
.super Lt;
.source "PG"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Laog;
.implements Law;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lt;",
        "Landroid/text/TextWatcher;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/view/View$OnKeyListener;",
        "Landroid/view/View$OnLongClickListener;",
        "Laog;",
        "Law",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private Y:Z

.field private a:Laom;

.field private b:Landroid/widget/EditText;

.field private c:Z

.field private d:Landroid/view/View;

.field private e:Lcom/google/android/apps/hangouts/fragments/dialpad/DialpadFragment;

.field private f:Laof;

.field private g:Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;

.field private h:Ljava/lang/String;

.field private i:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lt;-><init>()V

    .line 91
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->h:Ljava/lang/String;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;)Laom;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->a:Laom;

    return-object v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    .line 249
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->b:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 252
    :cond_0
    return-void
.end method

.method private e()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 343
    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->d:Landroid/view/View;

    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->f()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 345
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->a:Laom;

    if-eqz v0, :cond_2

    .line 346
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->a:Laom;

    .line 347
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->f()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, ""

    iget-object v4, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->h:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    move v2, v1

    .line 346
    :cond_1
    invoke-interface {v0, v2}, Laom;->a(Z)V

    .line 349
    :cond_2
    return-void

    :cond_3
    move v0, v2

    .line 343
    goto :goto_0
.end method

.method private f()Z
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->clear()V

    .line 311
    return-void
.end method

.method public a(I)V
    .locals 4

    .prologue
    const/16 v3, 0x51

    .line 236
    packed-switch p1, :pswitch_data_0

    .line 244
    :goto_0
    return-void

    .line 239
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    add-int/lit8 v2, v0, -0x1

    invoke-interface {v1, v2, v0}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 240
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v3, v0}, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->a(IZ)V

    .line 241
    const/4 v0, 0x0

    invoke-virtual {p0, v3, v0}, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->a(IZ)V

    goto :goto_0

    .line 236
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
    .end packed-switch
.end method

.method public a(IZ)V
    .locals 3

    .prologue
    .line 228
    if-eqz p2, :cond_0

    .line 229
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->b:Landroid/widget/EditText;

    new-instance v1, Landroid/view/KeyEvent;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p1}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v0, p1, v1}, Landroid/widget/EditText;->onKeyDown(ILandroid/view/KeyEvent;)Z

    .line 231
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->d()V

    .line 232
    return-void
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 371
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 372
    const/4 v0, 0x1

    .line 373
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->h:Ljava/lang/String;

    .line 374
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->e()V

    .line 376
    :cond_0
    return-void
.end method

.method public a(Laom;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->a:Laom;

    .line 100
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->e()V

    .line 101
    return-void
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->b:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->a:Laom;

    if-eqz v0, :cond_2

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 117
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->g:Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;

    if-eqz v1, :cond_1

    .line 118
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->g:Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->a:Laom;

    .line 119
    invoke-interface {v2}, Laom;->f()Lyj;

    move-result-object v2

    .line 118
    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;->a(Ljava/lang/String;Lyj;)V

    .line 121
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->a:Laom;

    invoke-interface {v1, v0}, Laom;->a(Ljava/lang/String;)V

    .line 123
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->e()V

    .line 124
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 314
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 315
    const-string v0, ""

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->b:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->b:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 322
    :goto_0
    return-void

    .line 315
    :cond_0
    const-string v0, "Babel_dialer"

    const-string v1, "Dialer button enabled without a last-dialed number"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 317
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->a:Laom;

    if-eqz v0, :cond_2

    .line 318
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->a:Laom;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Laom;->b(Ljava/lang/String;)V

    .line 320
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->a()V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 104
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 386
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->Y:Z

    if-nez v0, :cond_0

    .line 387
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->i:Z

    .line 396
    :goto_0
    return-void

    .line 390
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->i:Z

    .line 391
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->b:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 394
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->f:Laof;

    invoke-virtual {v0}, Laof;->b()V

    .line 395
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->g:Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;->a()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 269
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 270
    sget v1, Lg;->bh:I

    if-ne v0, v1, :cond_1

    .line 271
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->f:Laof;

    invoke-virtual {v0}, Laof;->c()V

    .line 272
    const/16 v0, 0x43

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->a(IZ)V

    .line 280
    :cond_0
    :goto_0
    return-void

    .line 273
    :cond_1
    sget v1, Lg;->bv:I

    if-ne v0, v1, :cond_2

    .line 274
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 275
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setCursorVisible(Z)V

    goto :goto_0

    .line 278
    :cond_2
    const-string v0, "Babel_dialer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected onClick() event from: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 128
    invoke-super {p0, p1}, Lt;->onCreate(Landroid/os/Bundle;)V

    .line 129
    new-instance v0, Laof;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->getActivity()Ly;

    move-result-object v1

    invoke-direct {v0, v1}, Laof;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->f:Laof;

    .line 130
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->getLoaderManager()Lav;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lav;->b(ILandroid/os/Bundle;Law;)Ldg;

    move-result-object v0

    invoke-virtual {v0}, Ldg;->q()V

    .line 131
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Ldg;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 363
    if-ne p1, v1, :cond_0

    .line 364
    invoke-static {}, Lbkb;->j()Lyj;

    move-result-object v0

    invoke-static {v0, v1, v1}, Lf;->a(Lyj;ZI)Ldb;

    move-result-object v0

    .line 366
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 136
    invoke-super {p0, p1, p2, p3}, Lt;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 137
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->Y:Z

    .line 139
    sget v0, Lf;->gi:I

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 141
    sget v0, Lg;->bv:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->b:Landroid/widget/EditText;

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->b:Landroid/widget/EditText;

    sget-object v2, Laoo;->a:Laoo;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 145
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 146
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 147
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->getActivity()Ly;

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->b:Landroid/widget/EditText;

    new-instance v2, Laon;

    invoke-static {}, Lbzd;->h()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Laon;-><init>(Ljava/lang/String;Landroid/widget/TextView;)V

    new-array v0, v4, [Ljava/lang/Void;

    invoke-virtual {v2, v0}, Laon;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 149
    sget v0, Lg;->bh:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->d:Landroid/view/View;

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->d:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->d:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->d:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 155
    :cond_0
    sget v0, Lg;->gY:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 156
    new-instance v2, Laol;

    invoke-direct {v2, p0}, Laol;-><init>(Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 169
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->getChildFragmentManager()Lae;

    move-result-object v0

    const-class v2, Lcom/google/android/apps/hangouts/fragments/dialpad/DialpadFragment;

    .line 170
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    .line 169
    invoke-virtual {v0, v2}, Lae;->a(Ljava/lang/String;)Lt;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/fragments/dialpad/DialpadFragment;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->e:Lcom/google/android/apps/hangouts/fragments/dialpad/DialpadFragment;

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->e:Lcom/google/android/apps/hangouts/fragments/dialpad/DialpadFragment;

    if-nez v0, :cond_1

    .line 172
    new-instance v0, Lcom/google/android/apps/hangouts/fragments/dialpad/DialpadFragment;

    invoke-direct {v0}, Lcom/google/android/apps/hangouts/fragments/dialpad/DialpadFragment;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->e:Lcom/google/android/apps/hangouts/fragments/dialpad/DialpadFragment;

    .line 173
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->getChildFragmentManager()Lae;

    move-result-object v0

    invoke-virtual {v0}, Lae;->a()Lao;

    move-result-object v0

    sget v2, Lg;->bs:I

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->e:Lcom/google/android/apps/hangouts/fragments/dialpad/DialpadFragment;

    const-class v4, Lcom/google/android/apps/hangouts/fragments/dialpad/DialpadFragment;

    .line 174
    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, Lao;->a(ILt;Ljava/lang/String;)Lao;

    move-result-object v0

    .line 175
    invoke-virtual {v0}, Lao;->b()I

    .line 178
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->e:Lcom/google/android/apps/hangouts/fragments/dialpad/DialpadFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/hangouts/fragments/dialpad/DialpadFragment;->a(Laog;)V

    .line 180
    sget v0, Lg;->fF:I

    .line 181
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->g:Lcom/google/android/apps/hangouts/views/VoiceRatesAndBalanceView;

    .line 183
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->e()V

    .line 184
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->d()V

    .line 186
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->i:Z

    if-eqz v0, :cond_2

    .line 187
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->c()V

    .line 190
    :cond_2
    return-object v1
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 222
    invoke-super {p0}, Lt;->onDestroy()V

    .line 223
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->f:Laof;

    invoke-virtual {v0}, Laof;->a()V

    .line 224
    return-void
.end method

.method public onHiddenChanged(Z)V
    .locals 1

    .prologue
    .line 357
    invoke-super {p0, p1}, Lt;->onHiddenChanged(Z)V

    .line 358
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 359
    :cond_0
    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 256
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 257
    sget v1, Lg;->bv:I

    if-ne v0, v1, :cond_0

    .line 258
    const/16 v0, 0x42

    if-ne p2, v0, :cond_0

    .line 259
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->b()V

    .line 260
    const/4 v0, 0x1

    .line 264
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic onLoadFinished(Ldg;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 33
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p2}, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Ldg;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 379
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 284
    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    .line 285
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    .line 286
    sget v4, Lg;->bh:I

    if-ne v3, v4, :cond_0

    .line 287
    invoke-interface {v2}, Landroid/text/Editable;->clear()V

    .line 288
    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->d:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setPressed(Z)V

    .line 294
    :goto_0
    return v0

    .line 290
    :cond_0
    sget v2, Lg;->bv:I

    if-ne v3, v2, :cond_1

    .line 291
    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setCursorVisible(Z)V

    move v0, v1

    .line 292
    goto :goto_0

    :cond_1
    move v0, v1

    .line 294
    goto :goto_0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 208
    invoke-super {p0}, Lt;->onPause()V

    .line 209
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 195
    invoke-super {p0}, Lt;->onResume()V

    .line 199
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->h:Ljava/lang/String;

    .line 201
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->b:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 203
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->e()V

    .line 204
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 213
    invoke-super {p0}, Lt;->onStop()V

    .line 214
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->c:Z

    if-eqz v0, :cond_0

    .line 215
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->c:Z

    .line 216
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->a()V

    .line 218
    :cond_0
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 107
    return-void
.end method
