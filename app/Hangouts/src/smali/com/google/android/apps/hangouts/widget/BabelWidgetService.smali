.class public Lcom/google/android/apps/hangouts/widget/BabelWidgetService;
.super Landroid/widget/RemoteViewsService;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field private static final a:Z

.field private static final b:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lbys;->t:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/hangouts/widget/BabelWidgetService;->a:Z

    .line 64
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/hangouts/widget/BabelWidgetService;->b:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Landroid/widget/RemoteViewsService;-><init>()V

    .line 77
    return-void
.end method

.method public static synthetic a()Z
    .locals 1

    .prologue
    .line 57
    sget-boolean v0, Lcom/google/android/apps/hangouts/widget/BabelWidgetService;->a:Z

    return v0
.end method

.method public static synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/google/android/apps/hangouts/widget/BabelWidgetService;->b:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method public onGetViewFactory(Landroid/content/Intent;)Landroid/widget/RemoteViewsService$RemoteViewsFactory;
    .locals 3

    .prologue
    .line 68
    sget-boolean v0, Lcom/google/android/apps/hangouts/widget/BabelWidgetService;->a:Z

    if-eqz v0, :cond_0

    .line 69
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "BabelWidgetService.onGetViewFactory intent: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    :cond_0
    new-instance v0, Lcfe;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/widget/BabelWidgetService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcfe;-><init>(Landroid/content/Context;Landroid/content/Intent;)V

    return-object v0
.end method
