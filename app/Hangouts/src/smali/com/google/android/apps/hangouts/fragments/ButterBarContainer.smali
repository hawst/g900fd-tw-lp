.class public Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;
.super Landroid/widget/FrameLayout;
.source "PG"


# static fields
.field private static final a:Z


# instance fields
.field private b:I

.field private c:[Lacy;

.field private final d:Landroid/os/Handler;

.field private e:Lyj;

.field private f:Ljava/lang/Thread;

.field private final g:Lbor;

.field private final h:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lbys;->d:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->a:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 96
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->b:I

    .line 48
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->d:Landroid/os/Handler;

    .line 55
    new-instance v0, Lact;

    invoke-direct {v0, p0}, Lact;-><init>(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->g:Lbor;

    .line 142
    new-instance v0, Lacu;

    invoke-direct {v0, p0}, Lacu;-><init>(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->h:Ljava/lang/Runnable;

    .line 97
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 100
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->b:I

    .line 48
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->d:Landroid/os/Handler;

    .line 55
    new-instance v0, Lact;

    invoke-direct {v0, p0}, Lact;-><init>(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->g:Lbor;

    .line 142
    new-instance v0, Lacu;

    invoke-direct {v0, p0}, Lacu;-><init>(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->h:Ljava/lang/Runnable;

    .line 101
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;)I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->b:I

    return v0
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;)I
    .locals 2

    .prologue
    .line 41
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->b:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->b:I

    return v0
.end method

.method public static synthetic c()Z
    .locals 1

    .prologue
    .line 41
    sget-boolean v0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->a:Z

    return v0
.end method

.method public static synthetic c(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;)Z
    .locals 4

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->e:Lyj;

    invoke-static {v0}, Lbsx;->b(Lyj;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-boolean v0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "Babel"

    const-string v1, "updateButterBarsRunnable delaying for MetadataLoader."

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->f:Ljava/lang/Thread;

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "Babel"

    const-string v1, "updateButterBarsRunnable kicking off MetadataLoader thread."

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lacv;

    invoke-direct {v1, p0}, Lacv;-><init>(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->f:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->f:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->h:Ljava/lang/Runnable;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->f:Ljava/lang/Thread;

    sget-boolean v0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->a:Z

    if-eqz v0, :cond_4

    const-string v0, "Babel"

    const-string v1, "updateButterBarsRunnable MetadataLoader loaded."

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static synthetic d(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;)[Lacy;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->c:[Lacy;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;)Lyj;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->e:Lyj;

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->h:Ljava/lang/Runnable;

    return-object v0
.end method

.method public static synthetic g(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->d:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->c:[Lacy;

    array-length v0, v0

    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->b:I

    .line 139
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->b()V

    .line 140
    return-void
.end method

.method public a(Lyj;)V
    .locals 1

    .prologue
    .line 132
    iput-object p1, p0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->e:Lyj;

    .line 133
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->f:Ljava/lang/Thread;

    .line 134
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->b()V

    .line 135
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->h:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 217
    return-void
.end method

.method public onAttachedToWindow()V
    .locals 1

    .prologue
    .line 120
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->g:Lbor;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbor;)V

    .line 122
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->b()V

    .line 123
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 127
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->g:Lbor;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lbor;)V

    .line 129
    return-void
.end method

.method public onFinishInflate()V
    .locals 3

    .prologue
    .line 105
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 107
    invoke-static {}, Lbkb;->j()Lyj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->e:Lyj;

    .line 110
    const/4 v0, 0x4

    new-array v0, v0, [Lacy;

    const/4 v1, 0x0

    new-instance v2, Lacz;

    invoke-direct {v2, p0, p0}, Lacz;-><init>(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;Landroid/view/ViewGroup;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lacw;

    invoke-direct {v2, p0, p0}, Lacw;-><init>(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;Landroid/view/ViewGroup;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lacx;

    invoke-direct {v2, p0, p0}, Lacx;-><init>(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;Landroid/view/ViewGroup;)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lada;

    invoke-direct {v2, p0, p0}, Lada;-><init>(Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;Landroid/view/ViewGroup;)V

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->c:[Lacy;

    .line 116
    return-void
.end method
