.class public Lcom/google/android/apps/hangouts/phone/ShareLocationActivity;
.super Lakn;
.source "PG"

# interfaces
.implements Laaq;
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private r:Laap;

.field private s:Landroid/widget/ImageButton;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lakn;-><init>()V

    .line 208
    return-void
.end method

.method public static n()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 46
    const-string v1, "babel_location_sharing_enabled"

    invoke-static {v1, v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, Lbxp;->a:F

    const/high16 v2, 0x40000000    # 2.0f

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static o()I
    .locals 2

    .prologue
    .line 56
    const-string v0, "babel_location_static_map_size"

    const/16 v1, 0x190

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private p()V
    .locals 5

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ShareLocationActivity;->r:Laap;

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ShareLocationActivity;->r:Laap;

    invoke-virtual {v0}, Laap;->c()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    .line 178
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 179
    const-string v2, "marker_options"

    iget-object v3, v0, Lcom/google/android/gms/maps/model/CameraPosition;->a:Lcom/google/android/gms/maps/model/LatLng;

    new-instance v4, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v4}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    invoke-virtual {v4, v3}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 180
    const-string v2, "camera_position"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 181
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/hangouts/phone/ShareLocationActivity;->setResult(ILandroid/content/Intent;)V

    .line 182
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/ShareLocationActivity;->finish()V

    .line 184
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ShareLocationActivity;->s:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 167
    return-void
.end method

.method protected a(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 188
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_0

    .line 189
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/ShareLocationActivity;->onBackPressed()V

    .line 195
    :goto_0
    return v0

    .line 191
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lg;->gK:I

    if-ne v1, v2, :cond_1

    .line 192
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/ShareLocationActivity;->p()V

    goto :goto_0

    .line 195
    :cond_1
    invoke-super {p0, p1}, Lakn;->a(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 171
    sget v0, Lg;->hQ:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/ShareLocationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 172
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 173
    return-void
.end method

.method protected k()Lyj;
    .locals 1

    .prologue
    .line 201
    const/4 v0, 0x0

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ShareLocationActivity;->s:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_0

    .line 160
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/ShareLocationActivity;->p()V

    .line 162
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 67
    invoke-super {p0, p1}, Lakn;->onCreate(Landroid/os/Bundle;)V

    .line 68
    sget v0, Lf;->gs:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/ShareLocationActivity;->setContentView(I)V

    .line 70
    new-instance v0, Laap;

    .line 71
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/ShareLocationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Laap;-><init>(Landroid/content/Context;Laaq;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/ShareLocationActivity;->r:Laap;

    .line 72
    sget v0, Lg;->hQ:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/ShareLocationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ShareLocationActivity;->r:Laap;

    invoke-static {}, Laap;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v2, "map"

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v2, "location_reminded"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    sget v0, Lg;->by:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v2, Lbbt;

    invoke-direct {v2, p0, v1}, Lbbt;-><init>(Lcom/google/android/apps/hangouts/phone/ShareLocationActivity;Landroid/view/View;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lg;->d:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v2, Lbbu;

    invoke-direct {v2, p0, v1}, Lbbu;-><init>(Lcom/google/android/apps/hangouts/phone/ShareLocationActivity;Landroid/view/View;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/ShareLocationActivity;->g()Lkd;

    move-result-object v0

    .line 75
    invoke-virtual {v0, v4}, Lkd;->a(Z)V

    .line 76
    sget v1, Lh;->kY:I

    invoke-virtual {v0, v1}, Lkd;->b(I)V

    .line 78
    sget v0, Lg;->ej:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/ShareLocationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/ShareLocationActivity;->s:Landroid/widget/ImageButton;

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ShareLocationActivity;->s:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ShareLocationActivity;->s:Landroid/widget/ImageButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 81
    return-void

    .line 72
    :cond_1
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v2, "map"

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "location_reminded"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/ShareLocationActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 112
    sget v1, Lf;->hd:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 113
    invoke-super {p0, p1}, Lakn;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onDestroy()V
    .locals 5

    .prologue
    .line 100
    invoke-super {p0}, Lakn;->onDestroy()V

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ShareLocationActivity;->r:Laap;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ShareLocationActivity;->r:Laap;

    invoke-virtual {v0}, Laap;->c()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    .line 103
    if-eqz v0, :cond_0

    .line 104
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    const-string v2, "map"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "latitude"

    iget-object v3, v0, Lcom/google/android/gms/maps/model/CameraPosition;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v3, v3, Lcom/google/android/gms/maps/model/LatLng;->a:D

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v2, "longitude"

    iget-object v3, v0, Lcom/google/android/gms/maps/model/CameraPosition;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v3, v3, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v2, "zoom"

    iget v0, v0, Lcom/google/android/gms/maps/model/CameraPosition;->b:F

    invoke-static {v0}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 107
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 94
    invoke-super {p0}, Lakn;->onPause()V

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ShareLocationActivity;->r:Laap;

    invoke-virtual {v0}, Laap;->d()V

    .line 96
    return-void
.end method

.method protected onResume()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 85
    invoke-super {p0}, Lakn;->onResume()V

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ShareLocationActivity;->r:Laap;

    invoke-virtual {v0}, Laap;->b()V

    .line 87
    iget-object v2, p0, Lcom/google/android/apps/hangouts/phone/ShareLocationActivity;->r:Laap;

    .line 88
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/ShareLocationActivity;->e()Lae;

    move-result-object v0

    sget v3, Lg;->ep:I

    invoke-virtual {v0, v3}, Lae;->a(I)Lt;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/SupportMapFragment;

    .line 89
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v3

    const-string v4, "map"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "latitude"

    invoke-interface {v3, v4, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "longitude"

    invoke-interface {v3, v5, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "zoom"

    invoke-interface {v3, v6, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 87
    :cond_0
    :goto_0
    invoke-virtual {v2, v0, v1}, Laap;->a(Lcom/google/android/gms/maps/SupportMapFragment;Lcom/google/android/gms/maps/model/CameraPosition;)V

    .line 90
    return-void

    .line 89
    :cond_1
    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    new-instance v3, Lcom/google/android/gms/maps/model/LatLng;

    invoke-direct {v3, v6, v7, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-static {v3, v1}, Lcom/google/android/gms/maps/model/CameraPosition;->a(Lcom/google/android/gms/maps/model/LatLng;F)Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v1

    goto :goto_0
.end method
