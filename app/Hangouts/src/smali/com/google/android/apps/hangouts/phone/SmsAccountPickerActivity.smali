.class public Lcom/google/android/apps/hangouts/phone/SmsAccountPickerActivity;
.super Ly;
.source "PG"

# interfaces
.implements Labd;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ly;-><init>()V

    return-void
.end method


# virtual methods
.method public a_(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 38
    const-string v0, "--None--"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 39
    invoke-static {}, Lbkb;->p()Lyj;

    move-result-object v0

    invoke-static {v0}, Lbkb;->d(Lyj;)V

    .line 46
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/SmsAccountPickerActivity;->finish()V

    .line 47
    return-void

    .line 41
    :cond_1
    invoke-static {p1}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    .line 42
    if-eqz v0, :cond_0

    .line 43
    invoke-static {v0}, Lbkb;->d(Lyj;)V

    goto :goto_0
.end method

.method public n_()V
    .locals 0

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/SmsAccountPickerActivity;->finish()V

    .line 51
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 24
    invoke-super {p0, p1}, Ly;->onCreate(Landroid/os/Bundle;)V

    .line 26
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/SmsAccountPickerActivity;->e()Lae;

    move-result-object v1

    invoke-virtual {v1}, Lae;->a()Lao;

    move-result-object v8

    .line 27
    sget v1, Lh;->lm:I

    const/4 v2, -0x1

    sget v3, Lh;->ll:I

    move v4, v0

    move v6, v5

    move v7, v0

    invoke-static/range {v0 .. v7}, Laay;->a(ZIIIZZZZ)Laay;

    move-result-object v0

    .line 33
    const/4 v1, 0x0

    invoke-virtual {v8, v0, v1}, Lao;->a(Lt;Ljava/lang/String;)Lao;

    .line 34
    invoke-virtual {v8}, Lao;->b()I

    .line 35
    return-void
.end method
