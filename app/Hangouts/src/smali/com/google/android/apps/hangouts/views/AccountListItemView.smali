.class public Lcom/google/android/apps/hangouts/views/AccountListItemView;
.super Landroid/widget/RelativeLayout;
.source "PG"


# static fields
.field private static a:Z

.field private static b:Ljava/lang/String;


# instance fields
.field private c:Lcom/google/android/apps/hangouts/views/AvatarView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    sget-boolean v0, Lcom/google/android/apps/hangouts/views/AccountListItemView;->a:Z

    if-nez v0, :cond_0

    .line 33
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/AccountListItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 34
    sget v1, Lh;->fD:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/views/AccountListItemView;->b:Ljava/lang/String;

    .line 35
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/apps/hangouts/views/AccountListItemView;->a:Z

    .line 37
    :cond_0
    return-void
.end method

.method private a(IZ)V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AccountListItemView;->e:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 87
    if-eqz p2, :cond_0

    const/16 v0, 0x66

    if-ne p1, v0, :cond_2

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AccountListItemView;->e:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 94
    :cond_1
    :goto_0
    return-void

    .line 90
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AccountListItemView;->e:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AccountListItemView;->e:Landroid/widget/TextView;

    sget-object v1, Lcom/google/android/apps/hangouts/views/AccountListItemView;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Lyj;)V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AccountListItemView;->c:Lcom/google/android/apps/hangouts/views/AvatarView;

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AccountListItemView;->c:Lcom/google/android/apps/hangouts/views/AvatarView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/hangouts/views/AvatarView;->a(Ljava/lang/String;Lyj;)V

    .line 77
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AccountListItemView;->d:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AccountListItemView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 67
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/views/AccountListItemView;->b(Ljava/lang/String;)V

    .line 68
    invoke-direct {p0, v0, v0}, Lcom/google/android/apps/hangouts/views/AccountListItemView;->a(IZ)V

    .line 69
    invoke-direct {p0, v1, v1}, Lcom/google/android/apps/hangouts/views/AccountListItemView;->a(Ljava/lang/String;Lyj;)V

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AccountListItemView;->f:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 71
    return-void
.end method

.method public a(Lyj;Z)V
    .locals 2

    .prologue
    .line 54
    invoke-virtual {p1}, Lyj;->f()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/views/AccountListItemView;->b(Ljava/lang/String;)V

    .line 55
    invoke-static {p1}, Lbkb;->f(Lyj;)I

    move-result v0

    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/hangouts/views/AccountListItemView;->a(IZ)V

    .line 56
    invoke-virtual {p1}, Lyj;->F()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/hangouts/views/AccountListItemView;->a(Ljava/lang/String;Lyj;)V

    .line 57
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/AccountListItemView;->f:Landroid/view/View;

    .line 58
    invoke-virtual {p1}, Lyj;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lbkb;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 57
    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 61
    return-void

    .line 58
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 44
    sget v0, Lg;->F:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/AccountListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/AvatarView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AccountListItemView;->c:Lcom/google/android/apps/hangouts/views/AvatarView;

    .line 45
    sget v0, Lg;->f:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/AccountListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AccountListItemView;->d:Landroid/widget/TextView;

    .line 46
    sget v0, Lg;->g:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/AccountListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AccountListItemView;->e:Landroid/widget/TextView;

    .line 47
    sget v0, Lg;->gQ:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/AccountListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AccountListItemView;->f:Landroid/view/View;

    .line 48
    return-void
.end method
