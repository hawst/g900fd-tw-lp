.class public Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;
.super Lakl;
.source "PG"

# interfaces
.implements Labs;
.implements Land;
.implements Lank;
.implements Laom;


# instance fields
.field private Y:Landroid/view/View;

.field private Z:I

.field a:Landroid/view/accessibility/AccessibilityManager;

.field private aa:I

.field private ab:Ladi;

.field private ac:Z

.field private final ad:Lbme;

.field private ae:Z

.field private af:Z

.field private b:Landroid/widget/EditText;

.field private c:Landroid/widget/FrameLayout;

.field private d:Landroid/widget/FrameLayout;

.field private e:Landroid/widget/FrameLayout;

.field private f:Lanj;

.field private g:Lana;

.field private h:Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;

.field private i:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 49
    invoke-direct {p0}, Lakl;-><init>()V

    .line 79
    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->Z:I

    .line 80
    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->aa:I

    .line 85
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lbme;

    invoke-static {v0, v1}, Lcyj;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbme;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->ad:Lbme;

    .line 84
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;)Lanj;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->f:Lanj;

    return-object v0
.end method

.method private a(I)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 384
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->Z:I

    if-eq p1, v0, :cond_0

    .line 385
    packed-switch p1, :pswitch_data_0

    .line 396
    :goto_0
    iput p1, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->Z:I

    .line 402
    :cond_0
    return-void

    .line 387
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->d:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 388
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0

    .line 392
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->s()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->aa:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    .line 393
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->d:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 395
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0

    .line 385
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private a(IZ)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 405
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->aa:I

    if-ne p1, v0, :cond_0

    .line 447
    :goto_0
    return-void

    .line 409
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 441
    :cond_1
    :goto_1
    iput p1, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->aa:I

    goto :goto_0

    .line 411
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->i:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 412
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->Y:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 413
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->e:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 414
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->b:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 415
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setVisibility(I)V

    .line 416
    if-eqz p2, :cond_2

    .line 417
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->e:Landroid/widget/FrameLayout;

    .line 418
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->getActivity()Ly;

    move-result-object v1

    sget v2, Lf;->bh:I

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 417
    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 421
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->s()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->Z:I

    if-nez v0, :cond_1

    .line 422
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->d:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_1

    .line 426
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->i:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 427
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->Y:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 428
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->e:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 429
    if-eqz p2, :cond_3

    .line 430
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->e:Landroid/widget/FrameLayout;

    .line 431
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->getActivity()Ly;

    move-result-object v1

    sget v2, Lf;->bg:I

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 430
    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 434
    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->s()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 435
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->d:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 437
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    .line 438
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setVisibility(I)V

    .line 440
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lf;->b(Landroid/view/View;)V

    goto/16 :goto_1

    .line 409
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;I)V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->a(IZ)V

    return-void
.end method

.method private static b(I)I
    .locals 2

    .prologue
    .line 554
    packed-switch p0, :pswitch_data_0

    .line 560
    const-string v0, "Babel"

    const-string v1, "Unsupported call action type for CallContactPickerFragment!"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 561
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 556
    :pswitch_0
    const/4 v0, 0x2

    goto :goto_0

    .line 558
    :pswitch_1
    const/4 v0, 0x3

    goto :goto_0

    .line 554
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;)V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->a(I)V

    return-void
.end method

.method public static synthetic c(Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;)Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->h:Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;

    return-object v0
.end method

.method private q()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 284
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->getActivity()Ly;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "dialpad_visible"

    .line 285
    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 287
    if-eqz v2, :cond_0

    :goto_0
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->a(IZ)V

    .line 288
    return-void

    :cond_0
    move v0, v1

    .line 287
    goto :goto_0
.end method

.method private r()V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v5, -0x1

    .line 321
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->getActivity()Ly;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 322
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "dialpad_visible"

    iget v4, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->aa:I

    if-ne v4, v0, :cond_0

    .line 323
    :goto_0
    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 324
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 326
    invoke-direct {p0, v5, v1}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->a(IZ)V

    .line 327
    invoke-direct {p0, v5}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->a(I)V

    .line 328
    return-void

    :cond_0
    move v0, v1

    .line 322
    goto :goto_0
.end method

.method private s()Z
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 451
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->a:Landroid/view/accessibility/AccessibilityManager;

    if-nez v1, :cond_1

    .line 460
    :cond_0
    :goto_0
    return v0

    .line 455
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->a:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    .line 457
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v2, v3, :cond_2

    .line 458
    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->a:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 337
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->ae:Z

    if-nez v0, :cond_1

    .line 338
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->af:Z

    .line 365
    :cond_0
    :goto_0
    return-void

    .line 341
    :cond_1
    iput-boolean v2, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->af:Z

    .line 346
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->f:Lanj;

    invoke-virtual {v0}, Lanj;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 350
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->g:Lana;

    invoke-virtual {v3}, Lana;->e()Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v1

    .line 354
    :cond_2
    if-eqz v0, :cond_0

    .line 358
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->ac:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 359
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->f:Lanj;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v0, v2}, Lanj;->a(Ljava/lang/CharSequence;)V

    .line 363
    :goto_2
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->ac:Z

    goto :goto_0

    .line 361
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->b:Landroid/widget/EditText;

    const-string v3, ""

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->h:Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->c()V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->g:Lana;

    invoke-virtual {v0}, Lana;->q()V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->f:Lanj;

    invoke-virtual {v0}, Lanj;->q()V

    invoke-direct {p0, v2}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->a(I)V

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method public a(Ladi;)V
    .locals 0

    .prologue
    .line 332
    iput-object p1, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->ab:Ladi;

    .line 333
    return-void
.end method

.method public a(Lcaw;)V
    .locals 9

    .prologue
    .line 465
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->ab:Ladi;

    if-eqz v0, :cond_0

    .line 468
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->b:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 470
    invoke-virtual {p1}, Lcaw;->a()Laea;

    move-result-object v0

    .line 471
    invoke-virtual {p1}, Lcaw;->g()I

    move-result v4

    .line 472
    invoke-virtual {p1}, Lcaw;->d()Ljava/lang/String;

    move-result-object v5

    .line 473
    invoke-virtual {p1}, Lcaw;->e()Ljava/lang/String;

    move-result-object v6

    .line 474
    invoke-virtual {p1}, Lcaw;->f()Ljava/lang/String;

    move-result-object v7

    .line 475
    invoke-virtual {p1}, Lcaw;->b()Ljava/lang/String;

    move-result-object v3

    .line 479
    invoke-virtual {v0}, Laea;->j()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Laeh;

    .line 480
    new-instance v0, Lbkn;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->ab:Ladi;

    .line 481
    invoke-interface {v1}, Ladi;->a()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->b(I)I

    move-result v1

    iget-object v2, v2, Laeh;->a:Ljava/lang/String;

    const/16 v8, 0x3d

    invoke-direct/range {v0 .. v8}, Lbkn;-><init>(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 485
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->ab:Ladi;

    invoke-interface {v1, v0}, Ladi;->a(Lbkn;)V

    .line 487
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 510
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 511
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->b:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 512
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 521
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->Y:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 522
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 526
    const/4 v0, 0x1

    invoke-direct {p0, v4, v0}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->a(IZ)V

    .line 527
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->ab:Ladi;

    invoke-interface {v0}, Ladi;->a()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->b(I)I

    move-result v1

    .line 528
    new-instance v0, Lbkn;

    const/16 v8, 0x3d

    move-object v2, p1

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-direct/range {v0 .. v8}, Lbkn;-><init>(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 534
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->ab:Ladi;

    invoke-interface {v1, v0}, Ladi;->a(Lbkn;)V

    .line 536
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->ad:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v1, 0x35a

    .line 537
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 536
    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    .line 538
    return-void
.end method

.method public b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 368
    iget v2, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->aa:I

    if-ne v2, v0, :cond_0

    .line 369
    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->a(IZ)V

    .line 372
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 491
    invoke-direct {p0, v0, v0}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->a(IZ)V

    .line 492
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 502
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->a(I)V

    .line 503
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 516
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->a(IZ)V

    .line 517
    return-void
.end method

.method public f()Lyj;
    .locals 2

    .prologue
    .line 542
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->getActivity()Ly;

    move-result-object v0

    check-cast v0, Lakn;

    .line 543
    invoke-virtual {v0}, Lakn;->k()Lyj;

    move-result-object v0

    .line 545
    if-nez v0, :cond_0

    .line 546
    const-string v0, "Babel"

    const-string v1, "Account is no longer valid in CallContactPickerFragment."

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 547
    const/4 v0, 0x0

    .line 550
    :cond_0
    return-object v0
.end method

.method protected isEmpty()Z
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 234
    invoke-super {p0, p1}, Lakl;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 236
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->e:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 237
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 238
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->b:Landroid/widget/EditText;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 242
    :goto_0
    return-void

    .line 240
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->b:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 124
    invoke-virtual {p0, v1}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->setHasOptionsMenu(Z)V

    .line 126
    sget v0, Lf;->er:I

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 127
    sget v0, Lg;->aF:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->b:Landroid/widget/EditText;

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->b:Landroid/widget/EditText;

    new-instance v3, Ladd;

    invoke-direct {v3, p0}, Ladd;-><init>(Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->b:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getInputType()I

    move-result v3

    const/high16 v5, 0x80000

    or-int/2addr v3, v5

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setInputType(I)V

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->b:Landroid/widget/EditText;

    new-instance v3, Lade;

    invoke-direct {v3, p0}, Lade;-><init>(Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 158
    sget v0, Lg;->dV:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->i:Landroid/view/View;

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->i:Landroid/view/View;

    new-instance v3, Ladf;

    invoke-direct {v3, p0}, Ladf;-><init>(Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 167
    sget v0, Lg;->bq:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->Y:Landroid/view/View;

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->Y:Landroid/view/View;

    new-instance v3, Ladg;

    invoke-direct {v3, p0}, Ladg;-><init>(Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->Y:Landroid/view/View;

    new-instance v3, Ladh;

    invoke-direct {v3, p0}, Ladh;-><init>(Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 183
    sget v0, Lg;->ei:I

    .line 184
    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->c:Landroid/widget/FrameLayout;

    .line 185
    sget v0, Lg;->ge:I

    .line 186
    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->d:Landroid/widget/FrameLayout;

    .line 187
    sget v0, Lg;->fk:I

    .line 188
    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->e:Landroid/widget/FrameLayout;

    .line 190
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->getChildFragmentManager()Lae;

    move-result-object v0

    const-class v3, Lanj;

    .line 191
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lae;->a(Ljava/lang/String;)Lt;

    move-result-object v0

    check-cast v0, Lanj;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->f:Lanj;

    .line 192
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->getChildFragmentManager()Lae;

    move-result-object v0

    const-class v3, Lana;

    .line 193
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lae;->a(Ljava/lang/String;)Lt;

    move-result-object v0

    check-cast v0, Lana;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->g:Lana;

    .line 194
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->getChildFragmentManager()Lae;

    move-result-object v0

    const-class v3, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;

    .line 195
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lae;->a(Ljava/lang/String;)Lt;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->h:Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;

    .line 198
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->f:Lanj;

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->g:Lana;

    if-nez v3, :cond_4

    move v3, v1

    :goto_1
    if-ne v0, v3, :cond_5

    move v0, v1

    :goto_2
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->f:Lanj;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->h:Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;

    if-nez v3, :cond_7

    move v3, v1

    :goto_4
    if-ne v0, v3, :cond_0

    move v2, v1

    :cond_0
    invoke-static {v2}, Lcwz;->a(Z)V

    .line 201
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->f:Lanj;

    if-nez v0, :cond_1

    .line 202
    new-instance v0, Lanj;

    invoke-direct {v0}, Lanj;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->f:Lanj;

    .line 203
    new-instance v0, Lana;

    invoke-direct {v0}, Lana;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->g:Lana;

    .line 204
    new-instance v0, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;

    invoke-direct {v0}, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->h:Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;

    .line 205
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->getChildFragmentManager()Lae;

    move-result-object v0

    invoke-virtual {v0}, Lae;->a()Lao;

    move-result-object v0

    sget v2, Lg;->ei:I

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->f:Lanj;

    const-class v5, Lanj;

    .line 207
    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    .line 206
    invoke-virtual {v0, v2, v3, v5}, Lao;->a(ILt;Ljava/lang/String;)Lao;

    move-result-object v0

    sget v2, Lg;->ge:I

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->g:Lana;

    const-class v5, Lana;

    .line 209
    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    .line 208
    invoke-virtual {v0, v2, v3, v5}, Lao;->a(ILt;Ljava/lang/String;)Lao;

    move-result-object v0

    sget v2, Lg;->fk:I

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->h:Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;

    const-class v5, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;

    .line 211
    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    .line 210
    invoke-virtual {v0, v2, v3, v5}, Lao;->a(ILt;Ljava/lang/String;)Lao;

    move-result-object v0

    .line 212
    invoke-virtual {v0}, Lao;->b()I

    .line 215
    :cond_1
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "accessibility"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->a:Landroid/view/accessibility/AccessibilityManager;

    .line 218
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->ae:Z

    .line 219
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->af:Z

    if-eqz v0, :cond_2

    .line 220
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->a()V

    .line 223
    :cond_2
    return-object v4

    :cond_3
    move v0, v2

    .line 198
    goto/16 :goto_0

    :cond_4
    move v3, v2

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_2

    :cond_6
    move v0, v2

    .line 199
    goto :goto_3

    :cond_7
    move v3, v2

    goto :goto_4
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 228
    invoke-super {p0}, Lakl;->onDestroyView()V

    .line 229
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->ae:Z

    .line 230
    return-void
.end method

.method public onHiddenChanged(Z)V
    .locals 0

    .prologue
    .line 298
    invoke-super {p0, p1}, Lakl;->onHiddenChanged(Z)V

    .line 299
    if-eqz p1, :cond_0

    .line 300
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->r()V

    .line 304
    :goto_0
    return-void

    .line 302
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->q()V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 273
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 274
    new-instance v1, Lamz;

    .line 275
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->f()Lyj;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lamz;-><init>(Landroid/content/res/Resources;Lyj;)V

    .line 276
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->getFragmentManager()Lae;

    move-result-object v2

    invoke-virtual {v1, v2}, Lamz;->a(Lae;)V

    .line 280
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lakl;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 308
    invoke-super {p0}, Lakl;->onPause()V

    .line 309
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->r()V

    .line 317
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 292
    invoke-super {p0}, Lakl;->onResume()V

    .line 293
    const-string v0, "Babel"

    const-string v1, "Resuming CallContactPickerFragment"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 246
    invoke-super {p0}, Lakl;->onStart()V

    .line 248
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->f:Lanj;

    invoke-virtual {v0, p0}, Lanj;->a(Labs;)V

    .line 249
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->g:Lana;

    invoke-virtual {v0, p0}, Lana;->a(Labs;)V

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->f:Lanj;

    invoke-virtual {v0, p0}, Lanj;->a(Lank;)V

    .line 251
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->g:Lana;

    invoke-virtual {v0, p0}, Lana;->a(Land;)V

    .line 252
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->h:Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/hangouts/fragments/dialpad/PhoneCallDialerFragment;->a(Laom;)V

    .line 254
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->q()V

    .line 256
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->a()V

    .line 258
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->ad:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v1, 0x356

    .line 259
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 258
    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    .line 260
    return-void
.end method

.method public setUserVisibleHint(Z)V
    .locals 2

    .prologue
    .line 264
    invoke-super {p0, p1}, Lakl;->setUserVisibleHint(Z)V

    .line 265
    if-eqz p1, :cond_0

    .line 266
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->ad:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v1, 0x356

    .line 267
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 266
    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    .line 269
    :cond_0
    return-void
.end method
