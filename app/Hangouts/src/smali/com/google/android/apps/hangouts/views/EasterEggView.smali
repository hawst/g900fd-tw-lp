.class public Lcom/google/android/apps/hangouts/views/EasterEggView;
.super Landroid/widget/FrameLayout;
.source "PG"


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final g:Ljava/util/Random;

.field private static final j:[I


# instance fields
.field private b:Landroid/os/Handler;

.field private c:Ljava/lang/Runnable;

.field private d:Ljava/lang/Runnable;

.field private e:I

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcby;",
            ">;"
        }
    .end annotation
.end field

.field private final h:[Ljava/lang/String;

.field private final i:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 48
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "ponies"

    aput-object v1, v0, v2

    const-string v1, "ponystream"

    aput-object v1, v0, v3

    const-string v1, "pitchforks"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/hangouts/views/EasterEggView;->a:[Ljava/lang/String;

    .line 199
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/google/android/apps/hangouts/views/EasterEggView;->g:Ljava/util/Random;

    .line 205
    new-array v0, v5, [I

    sget v1, Lf;->aZ:I

    aput v1, v0, v2

    sget v1, Lf;->bb:I

    aput v1, v0, v3

    sget v1, Lf;->ba:I

    aput v1, v0, v4

    sput-object v0, Lcom/google/android/apps/hangouts/views/EasterEggView;->j:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 212
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/hangouts/views/EasterEggView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 213
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 216
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 60
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/EasterEggView;->b:Landroid/os/Handler;

    .line 196
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/EasterEggView;->f:Ljava/util/List;

    .line 218
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 219
    sget v1, Lf;->bo:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/hangouts/views/EasterEggView;->h:[Ljava/lang/String;

    .line 220
    sget v1, Lf;->bn:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/EasterEggView;->i:[Ljava/lang/String;

    .line 221
    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 71
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x2f

    if-eq v2, v3, :cond_2

    :cond_0
    move-object v0, v1

    .line 81
    :cond_1
    :goto_0
    return-object v0

    .line 75
    :cond_2
    sget-object v3, Lcom/google/android/apps/hangouts/views/EasterEggView;->a:[Ljava/lang/String;

    array-length v4, v3

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_3

    aget-object v0, v3, v2

    .line 76
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "/"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 75
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 81
    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/views/EasterEggView;)Ljava/util/List;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/EasterEggView;->f:Ljava/util/List;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/views/EasterEggView;Lyj;[Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/hangouts/views/EasterEggView;->a(Lyj;[Ljava/lang/String;I)V

    return-void
.end method

.method private a(Lyj;[Ljava/lang/String;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 229
    if-nez p2, :cond_0

    .line 243
    :goto_0
    return-void

    .line 234
    :cond_0
    sget-object v0, Lcom/google/android/apps/hangouts/views/EasterEggView;->g:Ljava/util/Random;

    array-length v1, p2

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    .line 236
    new-instance v1, Lbyq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "//ssl.gstatic.com/chat/babble/ee/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v0, p2, v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0, p1}, Lbyq;-><init>(Ljava/lang/String;Lyj;)V

    .line 238
    invoke-virtual {v1, v4}, Lbyq;->a(Z)Lbyq;

    .line 239
    invoke-virtual {v1, v4}, Lbyq;->c(Z)Lbyq;

    .line 240
    invoke-virtual {v1, v4}, Lbyq;->d(Z)Lbyq;

    .line 242
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/EasterEggView;->f:Ljava/util/List;

    new-instance v2, Lcby;

    invoke-direct {v2, p0, v1, p3}, Lcby;-><init>(Lcom/google/android/apps/hangouts/views/EasterEggView;Lbyq;I)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static synthetic b()Ljava/util/Random;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/google/android/apps/hangouts/views/EasterEggView;->g:Ljava/util/Random;

    return-object v0
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 91
    const-string v0, "ponystream"

    invoke-static {p0, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    const/4 v0, 0x0

    .line 95
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/views/EasterEggView;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/EasterEggView;->h:[Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/apps/hangouts/views/EasterEggView;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/EasterEggView;->b:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic c()[I
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/google/android/apps/hangouts/views/EasterEggView;->j:[I

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/apps/hangouts/views/EasterEggView;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/EasterEggView;->i:[Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/apps/hangouts/views/EasterEggView;)I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/google/android/apps/hangouts/views/EasterEggView;->e:I

    return v0
.end method

.method public static synthetic f(Lcom/google/android/apps/hangouts/views/EasterEggView;)I
    .locals 2

    .prologue
    .line 41
    iget v0, p0, Lcom/google/android/apps/hangouts/views/EasterEggView;->e:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/google/android/apps/hangouts/views/EasterEggView;->e:I

    return v0
.end method

.method public static synthetic g(Lcom/google/android/apps/hangouts/views/EasterEggView;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/EasterEggView;->d:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 305
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/EasterEggView;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 306
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 307
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcby;

    .line 308
    invoke-virtual {v0}, Lcby;->a()V

    .line 309
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 312
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/EasterEggView;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 314
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/EasterEggView;->c:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 315
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/EasterEggView;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/EasterEggView;->c:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 316
    iput-object v2, p0, Lcom/google/android/apps/hangouts/views/EasterEggView;->c:Ljava/lang/Runnable;

    .line 319
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/EasterEggView;->d:Ljava/lang/Runnable;

    if-eqz v0, :cond_2

    .line 320
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/EasterEggView;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/EasterEggView;->d:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 321
    iput-object v2, p0, Lcom/google/android/apps/hangouts/views/EasterEggView;->d:Ljava/lang/Runnable;

    .line 323
    :cond_2
    return-void

    .line 312
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(Lyj;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 247
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/EasterEggView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "babel_easter_eggs"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcww;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 302
    :cond_0
    :goto_0
    return-void

    .line 254
    :cond_1
    const-string v0, "ponies"

    invoke-static {v0, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 255
    sget-object v0, Lcom/google/android/apps/hangouts/views/EasterEggView;->g:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextBoolean()Z

    move-result v0

    if-eqz v0, :cond_2

    sget v0, Lf;->aY:I

    .line 257
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/EasterEggView;->h:[Ljava/lang/String;

    invoke-direct {p0, p1, v1, v0}, Lcom/google/android/apps/hangouts/views/EasterEggView;->a(Lyj;[Ljava/lang/String;I)V

    goto :goto_0

    .line 255
    :cond_2
    sget v0, Lf;->aX:I

    goto :goto_1

    .line 258
    :cond_3
    const-string v0, "ponystream"

    invoke-static {v0, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 259
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/EasterEggView;->c:Ljava/lang/Runnable;

    if-eqz v0, :cond_4

    .line 260
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/EasterEggView;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/EasterEggView;->c:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 261
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/EasterEggView;->c:Ljava/lang/Runnable;

    goto :goto_0

    .line 263
    :cond_4
    new-instance v0, Lcbw;

    invoke-direct {v0, p0, p1}, Lcbw;-><init>(Lcom/google/android/apps/hangouts/views/EasterEggView;Lyj;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/EasterEggView;->c:Ljava/lang/Runnable;

    .line 275
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/EasterEggView;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/EasterEggView;->c:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 277
    :cond_5
    const-string v0, "pitchforks"

    invoke-static {v0, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 278
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/EasterEggView;->d:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    .line 279
    sget-object v0, Lcom/google/android/apps/hangouts/views/EasterEggView;->g:Ljava/util/Random;

    const/16 v1, 0x14

    .line 280
    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x14

    iput v0, p0, Lcom/google/android/apps/hangouts/views/EasterEggView;->e:I

    .line 282
    new-instance v0, Lcbx;

    invoke-direct {v0, p0, p1}, Lcbx;-><init>(Lcom/google/android/apps/hangouts/views/EasterEggView;Lyj;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/EasterEggView;->d:Ljava/lang/Runnable;

    .line 299
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/EasterEggView;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/EasterEggView;->d:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 0

    .prologue
    .line 225
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 226
    return-void
.end method
