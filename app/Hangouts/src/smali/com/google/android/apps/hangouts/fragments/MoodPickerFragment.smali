.class public Lcom/google/android/apps/hangouts/fragments/MoodPickerFragment;
.super Lt;
.source "PG"


# static fields
.field private static b:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Lalg;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lt;-><init>()V

    .line 212
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/MoodPickerFragment;)Lalg;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/MoodPickerFragment;->a:Lalg;

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 44
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    const-string v0, "Babel"

    const-string v1, "getContentDescriptionForMood empty mood string"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    const/4 v0, 0x0

    .line 50
    :goto_0
    return-object v0

    .line 48
    :cond_0
    invoke-static {}, Lcom/google/android/apps/hangouts/fragments/MoodPickerFragment;->c()V

    .line 49
    const/4 v0, 0x0

    invoke-static {p0, v0}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v0

    .line 50
    sget-object v1, Lcom/google/android/apps/hangouts/fragments/MoodPickerFragment;->b:Ljava/util/LinkedHashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public static synthetic a()V
    .locals 0

    .prologue
    .line 37
    invoke-static {}, Lcom/google/android/apps/hangouts/fragments/MoodPickerFragment;->c()V

    return-void
.end method

.method public static synthetic b()Ljava/util/LinkedHashMap;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/google/android/apps/hangouts/fragments/MoodPickerFragment;->b:Ljava/util/LinkedHashMap;

    return-object v0
.end method

.method private static c()V
    .locals 7

    .prologue
    .line 54
    sget-object v0, Lcom/google/android/apps/hangouts/fragments/MoodPickerFragment;->b:Ljava/util/LinkedHashMap;

    if-nez v0, :cond_1

    .line 55
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 56
    sget v1, Lf;->by:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 57
    sget v2, Lf;->bi:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 59
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->length()I

    move-result v3

    .line 62
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2}, Landroid/content/res/TypedArray;->length()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v0, v4}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 64
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0, v3}, Ljava/util/LinkedHashMap;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/hangouts/fragments/MoodPickerFragment;->b:Ljava/util/LinkedHashMap;

    .line 65
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    .line 66
    sget-object v4, Lcom/google/android/apps/hangouts/fragments/MoodPickerFragment;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x10

    invoke-static {v5, v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 67
    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 66
    invoke-virtual {v4, v5, v6}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 70
    :cond_0
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 71
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 73
    :cond_1
    return-void
.end method


# virtual methods
.method public a(Lalg;)V
    .locals 0

    .prologue
    .line 295
    iput-object p1, p0, Lcom/google/android/apps/hangouts/fragments/MoodPickerFragment;->a:Lalg;

    .line 296
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 247
    invoke-super {p0, p1}, Lt;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 248
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 239
    invoke-super {p0, p1}, Lt;->onCreate(Landroid/os/Bundle;)V

    .line 240
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 256
    sget v0, Lf;->fW:I

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 257
    sget v0, Lg;->eF:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    .line 259
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/MoodPickerFragment;->getActivity()Ly;

    move-result-object v1

    check-cast v1, Lakn;

    .line 260
    invoke-virtual {v1}, Lakn;->k()Lyj;

    move-result-object v2

    .line 262
    invoke-virtual {v1}, Lakn;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v5}, Lale;->a(Landroid/content/res/Resources;)Lale;

    move-result-object v5

    .line 263
    invoke-virtual {v0, v5}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 264
    new-instance v6, Lalf;

    invoke-direct {v6, p0, v5, v2}, Lalf;-><init>(Lcom/google/android/apps/hangouts/fragments/MoodPickerFragment;Lale;Lyj;)V

    invoke-virtual {v0, v6}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 267
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v6

    .line 269
    invoke-virtual {v2}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    .line 268
    invoke-static {v2}, Lf;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 270
    invoke-virtual {v1}, Lakn;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 271
    sget v6, Lh;->kI:I

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 272
    const-string v6, ""

    invoke-interface {v2, v1, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 274
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 275
    invoke-virtual {v1, v3}, Ljava/lang/String;->codePointAt(I)I

    move-result v6

    .line 276
    invoke-virtual {v5}, Lale;->getCount()I

    move-result v7

    move v2, v3

    .line 277
    :goto_0
    if-ge v2, v7, :cond_0

    .line 279
    invoke-static {v5}, Lale;->a(Lale;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 278
    invoke-static {v1, v3}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v1

    .line 280
    if-ne v1, v6, :cond_1

    .line 281
    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setSelection(I)V

    .line 282
    invoke-virtual {v5, v2}, Lale;->a(I)V

    .line 288
    :cond_0
    return-object v4

    .line 277
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0
.end method
