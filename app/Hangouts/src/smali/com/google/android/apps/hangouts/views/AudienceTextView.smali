.class public Lcom/google/android/apps/hangouts/views/AudienceTextView;
.super Landroid/widget/AutoCompleteTextView;
.source "PG"


# instance fields
.field private a:Lcab;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 42
    const v0, 0x2000006

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->setImeOptions(I)V

    .line 46
    :goto_0
    return-void

    .line 44
    :cond_0
    const v0, 0x10000006

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->setImeOptions(I)V

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/views/AudienceTextView;)Lcab;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceTextView;->a:Lcab;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/views/AudienceTextView;Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->convertSelectionToString(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lcab;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/google/android/apps/hangouts/views/AudienceTextView;->a:Lcab;

    .line 59
    return-void
.end method

.method public onCheckIsTextEditor()Z
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x1

    return v0
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 2

    .prologue
    .line 114
    invoke-super {p0, p1}, Landroid/widget/AutoCompleteTextView;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v1

    .line 115
    if-eqz v1, :cond_0

    .line 116
    new-instance v0, Lcaa;

    invoke-direct {v0, p0, v1}, Lcaa;-><init>(Lcom/google/android/apps/hangouts/views/AudienceTextView;Landroid/view/inputmethod/InputConnection;)V

    .line 118
    invoke-virtual {v0, p0}, Lcaa;->a(Lcom/google/android/apps/hangouts/views/AudienceTextView;)V

    .line 122
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
