.class public Lcom/google/android/apps/hangouts/views/AudienceView;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final a:Z


# instance fields
.field private final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcah;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/view/ViewGroup;

.field private final d:Lcom/google/android/apps/hangouts/views/AudienceTextView;

.field private e:Ljava/lang/String;

.field private f:Lcah;

.field private final g:Z

.field private h:Laoe;

.field private i:Z

.field private j:Ljava/lang/Runnable;

.field private k:Landroid/widget/PopupWindow;

.field private l:Landroid/view/View;

.field private m:Lyj;

.field private n:Z

.field private o:Landroid/animation/Animator$AnimatorListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 92
    sget-object v0, Lbys;->s:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/hangouts/views/AudienceView;->a:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 135
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 96
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->b:Ljava/util/ArrayList;

    .line 100
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->e:Ljava/lang/String;

    .line 105
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->g:Z

    .line 124
    iput-object v3, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->k:Landroid/widget/PopupWindow;

    .line 125
    iput-object v3, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->l:Landroid/view/View;

    .line 173
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->g:Z

    if-eqz v0, :cond_0

    .line 174
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/AudienceView;->g()Landroid/animation/Animator$AnimatorListener;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->o:Landroid/animation/Animator$AnimatorListener;

    .line 177
    :cond_0
    sget v0, Lf;->ef:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/views/AudienceView;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/AudienceView;->addView(Landroid/view/View;)V

    .line 179
    sget v0, Lg;->fi:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/AudienceView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->c:Landroid/view/ViewGroup;

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/AudienceTextView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->d:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->d:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->setThreshold(I)V

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->d:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/AudienceView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lf;->cM:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->setDropDownWidth(I)V

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->d:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    new-instance v1, Lcac;

    invoke-direct {v1, p0}, Lcac;-><init>(Lcom/google/android/apps/hangouts/views/AudienceView;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->a(Lcab;)V

    .line 194
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->d:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    new-instance v1, Lcad;

    invoke-direct {v1, p0}, Lcad;-><init>(Lcom/google/android/apps/hangouts/views/AudienceView;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 204
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/AudienceView;->j()V

    .line 136
    return-void

    :cond_1
    move v0, v1

    .line 105
    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 145
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 96
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->b:Ljava/util/ArrayList;

    .line 100
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->e:Ljava/lang/String;

    .line 105
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->g:Z

    .line 124
    iput-object v3, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->k:Landroid/widget/PopupWindow;

    .line 125
    iput-object v3, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->l:Landroid/view/View;

    .line 173
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->g:Z

    if-eqz v0, :cond_0

    .line 174
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/AudienceView;->g()Landroid/animation/Animator$AnimatorListener;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->o:Landroid/animation/Animator$AnimatorListener;

    .line 177
    :cond_0
    sget v0, Lf;->ef:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/views/AudienceView;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/AudienceView;->addView(Landroid/view/View;)V

    .line 179
    sget v0, Lg;->fi:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/AudienceView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->c:Landroid/view/ViewGroup;

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/AudienceTextView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->d:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->d:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->setThreshold(I)V

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->d:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/AudienceView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lf;->cM:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->setDropDownWidth(I)V

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->d:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    new-instance v1, Lcac;

    invoke-direct {v1, p0}, Lcac;-><init>(Lcom/google/android/apps/hangouts/views/AudienceView;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->a(Lcab;)V

    .line 194
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->d:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    new-instance v1, Lcad;

    invoke-direct {v1, p0}, Lcad;-><init>(Lcom/google/android/apps/hangouts/views/AudienceView;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 204
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/AudienceView;->j()V

    .line 146
    return-void

    :cond_1
    move v0, v1

    .line 105
    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 169
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 96
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->b:Ljava/util/ArrayList;

    .line 100
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->e:Ljava/lang/String;

    .line 105
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->g:Z

    .line 124
    iput-object v3, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->k:Landroid/widget/PopupWindow;

    .line 125
    iput-object v3, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->l:Landroid/view/View;

    .line 173
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->g:Z

    if-eqz v0, :cond_0

    .line 174
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/AudienceView;->g()Landroid/animation/Animator$AnimatorListener;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->o:Landroid/animation/Animator$AnimatorListener;

    .line 177
    :cond_0
    sget v0, Lf;->ef:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/views/AudienceView;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/AudienceView;->addView(Landroid/view/View;)V

    .line 179
    sget v0, Lg;->fi:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/AudienceView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->c:Landroid/view/ViewGroup;

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/AudienceTextView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->d:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->d:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->setThreshold(I)V

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->d:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/AudienceView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lf;->cM:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->setDropDownWidth(I)V

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->d:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    new-instance v1, Lcac;

    invoke-direct {v1, p0}, Lcac;-><init>(Lcom/google/android/apps/hangouts/views/AudienceView;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->a(Lcab;)V

    .line 194
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->d:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    new-instance v1, Lcad;

    invoke-direct {v1, p0}, Lcad;-><init>(Lcom/google/android/apps/hangouts/views/AudienceView;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 204
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/AudienceView;->j()V

    .line 170
    return-void

    :cond_1
    move v0, v1

    .line 105
    goto :goto_0
.end method

.method private a(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 726
    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    .line 727
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/AudienceView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 728
    return-void
.end method

.method private a(Landroid/view/View;II)V
    .locals 1

    .prologue
    .line 568
    sget v0, Lg;->dz:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 569
    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/hangouts/views/AudienceView;->a(Landroid/view/View;I)V

    .line 571
    sget v0, Lg;->du:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 572
    invoke-virtual {v0, p3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 573
    return-void
.end method

.method private a(Landroid/view/View;Lcah;Z)V
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 634
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/AudienceView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 635
    const-string v0, ""

    .line 636
    const/4 v1, 0x0

    .line 640
    iget-object v4, p2, Lcah;->b:Lxo;

    if-eqz v4, :cond_3

    .line 641
    iget-object v0, p2, Lcah;->b:Lxo;

    .line 643
    invoke-virtual {v0}, Lxo;->c()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 644
    invoke-virtual {v0}, Lxo;->d()Ljava/lang/String;

    move-result-object v0

    move v4, v2

    move-object v5, v1

    move-object v1, v0

    move v0, v2

    .line 668
    :goto_0
    if-eqz v0, :cond_9

    sget v0, Lh;->hp:I

    new-array v7, v2, [Ljava/lang/Object;

    aput-object v1, v7, v3

    .line 669
    invoke-virtual {v6, v0, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 670
    :goto_1
    iget-object v6, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->m:Lyj;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/AudienceView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v8, p1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v8

    if-gtz v8, :cond_0

    if-eqz p3, :cond_a

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/AudienceView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    sget v9, Lh;->aA:I

    new-array v10, v2, [Ljava/lang/Object;

    aput-object v0, v10, v3

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_2
    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    sget v0, Lg;->fz:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/AvatarView;

    if-eqz v0, :cond_1

    invoke-virtual {v0, v5, v6}, Lcom/google/android/apps/hangouts/views/AvatarView;->a(Ljava/lang/String;Lyj;)V

    if-eqz v4, :cond_b

    :goto_3
    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/views/AvatarView;->a(I)V

    invoke-virtual {v0, v3}, Lcom/google/android/apps/hangouts/views/AvatarView;->setVisibility(I)V

    :cond_1
    sget v0, Lg;->eo:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz p3, :cond_c

    sget v0, Lf;->cb:I

    :goto_4
    invoke-direct {p0, v2, v0}, Lcom/google/android/apps/hangouts/views/AudienceView;->a(Landroid/view/View;I)V

    sget v0, Lg;->ht:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v2, Lf;->ca:I

    invoke-virtual {v7, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 672
    return-void

    .line 647
    :cond_2
    sget v0, Lh;->fX:I

    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move v4, v2

    move-object v5, v1

    move-object v1, v0

    move v0, v3

    .line 649
    goto :goto_0

    :cond_3
    iget-object v4, p2, Lcah;->a:Lxs;

    if-eqz v4, :cond_d

    .line 650
    iget-object v4, p2, Lcah;->a:Lxs;

    .line 651
    invoke-virtual {v4}, Lxs;->c()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 652
    invoke-virtual {v4}, Lxs;->d()Ljava/lang/String;

    move-result-object v0

    .line 658
    :cond_4
    :goto_5
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 659
    const v0, 0x104000e

    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 661
    :cond_5
    invoke-virtual {v4}, Lxs;->g()Ljava/lang/String;

    move-result-object v1

    .line 662
    sget-boolean v5, Lcom/google/android/apps/hangouts/views/AudienceView;->a:Z

    if-eqz v5, :cond_6

    .line 663
    const-string v5, "Babel"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Displaying chip text \'"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\' for person:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 664
    invoke-virtual {v4}, Lxs;->i()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 663
    invoke-static {v5, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    move v4, v3

    move-object v5, v1

    move-object v1, v0

    move v0, v2

    goto/16 :goto_0

    .line 653
    :cond_7
    invoke-virtual {v4}, Lxs;->e()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 654
    invoke-virtual {v4}, Lxs;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 655
    :cond_8
    invoke-virtual {v4}, Lxs;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 656
    invoke-virtual {v4}, Lxs;->b()Lbcx;

    move-result-object v0

    iget-object v0, v0, Lbcx;->d:Ljava/lang/String;

    goto :goto_5

    :cond_9
    move-object v0, v1

    .line 669
    goto/16 :goto_1

    .line 670
    :cond_a
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/AudienceView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    sget v9, Lh;->ay:I

    new-array v10, v2, [Ljava/lang/Object;

    aput-object v0, v10, v3

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_b
    move v2, v3

    goto/16 :goto_3

    :cond_c
    sget v0, Lf;->bZ:I

    goto/16 :goto_4

    :cond_d
    move v4, v3

    move-object v5, v1

    move-object v1, v0

    move v0, v2

    goto/16 :goto_0
.end method

.method private a(Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 622
    if-eqz p2, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 623
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->g:Z

    if-eqz v0, :cond_0

    .line 624
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0}, Landroid/view/View;->setScaleY(F)V

    .line 626
    :cond_0
    return-void

    .line 622
    :cond_1
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/views/AudienceView;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 59
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v2, v0, -0x1

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcah;

    if-eqz v0, :cond_0

    iget-object v3, v0, Lcah;->b:Lxo;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->h:Laoe;

    iget-object v0, v0, Lcah;->b:Lxo;

    invoke-virtual {v0}, Lxo;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v5, v5, v0}, Laoe;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    :goto_0
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    invoke-direct {p0, v1}, Lcom/google/android/apps/hangouts/views/AudienceView;->b(Z)V

    :cond_0
    return-void

    :cond_1
    iget-object v3, v0, Lcah;->a:Lxs;

    if-eqz v3, :cond_2

    iget-object v0, v0, Lcah;->a:Lxs;

    invoke-virtual {v0}, Lxs;->b()Lbcx;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->h:Laoe;

    iget-object v4, v0, Lbcx;->a:Ljava/lang/String;

    iget-object v0, v0, Lbcx;->b:Ljava/lang/String;

    invoke-interface {v3, v4, v0, v5}, Laoe;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method private b(I)Landroid/view/View;
    .locals 2

    .prologue
    .line 787
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/AudienceView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/views/AudienceView;)Lcom/google/android/apps/hangouts/views/AudienceTextView;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->d:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    return-object v0
.end method

.method private b(Landroid/view/View;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 605
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 607
    const-string v0, "scaleY"

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-static {v0, v1}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    .line 608
    const/4 v1, 0x1

    new-array v1, v1, [Landroid/animation/PropertyValuesHolder;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-static {p1, v1}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 609
    const-wide/16 v1, 0xc8

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 610
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->o:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 611
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 612
    return-void

    .line 607
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private b(Z)V
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 499
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->m:Lyj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->m:Lyj;

    invoke-virtual {v0}, Lyj;->s()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->i:Z

    if-eqz v0, :cond_2

    :cond_1
    move p1, v1

    .line 502
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->n:Z

    if-nez v0, :cond_3

    .line 503
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/AudienceView;->h()V

    .line 565
    :goto_0
    return-void

    :cond_3
    move v2, v1

    move v3, v1

    .line 507
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_b

    .line 508
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcah;

    .line 509
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/AudienceView;->i()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-le v3, v4, :cond_4

    .line 510
    sget v4, Lf;->eg:I

    invoke-direct {p0, v4}, Lcom/google/android/apps/hangouts/views/AudienceView;->b(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v5, v4, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 513
    :cond_4
    iget-object v5, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->c:Landroid/view/ViewGroup;

    add-int/lit8 v4, v3, 0x1

    invoke-virtual {v5, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 514
    iget-object v5, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->f:Lcah;

    invoke-virtual {v0, v5}, Lcah;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 515
    invoke-direct {p0, v3, v1}, Lcom/google/android/apps/hangouts/views/AudienceView;->a(Landroid/view/View;Z)V

    move v0, v2

    move v2, v4

    .line 507
    :goto_2
    add-int/lit8 v0, v0, 0x1

    move v3, v2

    move v2, v0

    goto :goto_1

    .line 517
    :cond_5
    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_7

    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 518
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->g:Z

    if-eqz v0, :cond_6

    if-eqz p1, :cond_6

    .line 519
    invoke-direct {p0, v3}, Lcom/google/android/apps/hangouts/views/AudienceView;->b(Landroid/view/View;)V

    move v0, v4

    .line 527
    :goto_3
    add-int/lit8 v2, v2, -0x1

    move v8, v2

    move v2, v0

    move v0, v8

    goto :goto_2

    .line 524
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 525
    add-int/lit8 v0, v4, -0x1

    goto :goto_3

    .line 528
    :cond_7
    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_8

    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    instance-of v5, v5, Ljava/lang/Runnable;

    if-eqz v5, :cond_9

    .line 529
    :cond_8
    invoke-direct {p0, v3, v7}, Lcom/google/android/apps/hangouts/views/AudienceView;->a(Landroid/view/View;Z)V

    .line 530
    invoke-direct {p0, v3, v0, v1}, Lcom/google/android/apps/hangouts/views/AudienceView;->a(Landroid/view/View;Lcah;Z)V

    .line 533
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->g:Z

    if-eqz v0, :cond_a

    if-eqz p1, :cond_a

    .line 534
    const-string v0, "scaleY"

    const/4 v5, 0x2

    new-array v5, v5, [F

    fill-array-data v5, :array_0

    invoke-static {v0, v5}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    new-array v5, v7, [Landroid/animation/PropertyValuesHolder;

    aput-object v0, v5, v1

    invoke-static {v3, v5}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v5, 0xc8

    invoke-virtual {v0, v5, v6}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    move v0, v2

    move v2, v4

    goto :goto_2

    .line 538
    :cond_9
    invoke-direct {p0, v3, v0, v1}, Lcom/google/android/apps/hangouts/views/AudienceView;->a(Landroid/view/View;Lcah;Z)V

    :cond_a
    move v0, v2

    move v2, v4

    goto :goto_2

    .line 543
    :cond_b
    :goto_4
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/AudienceView;->i()I

    move-result v0

    if-ge v3, v0, :cond_d

    .line 544
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 545
    iget-boolean v2, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->g:Z

    if-eqz v2, :cond_c

    if-eqz p1, :cond_c

    .line 546
    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/views/AudienceView;->b(Landroid/view/View;)V

    .line 547
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 549
    :cond_c
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_4

    .line 553
    :cond_d
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->i:Z

    if-eqz v0, :cond_e

    .line 556
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/AudienceView;->i()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    if-ge v0, v2, :cond_e

    .line 557
    sget v0, Lf;->ed:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/views/AudienceView;->b(I)Landroid/view/View;

    move-result-object v2

    .line 558
    sget v0, Lf;->ca:I

    sget v4, Lcom/google/android/apps/hangouts/R$drawable;->cf:I

    invoke-direct {p0, v2, v0, v4}, Lcom/google/android/apps/hangouts/views/AudienceView;->a(Landroid/view/View;II)V

    .line 559
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/AudienceView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v0, Lg;->eo:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    sget v5, Lf;->bX:I

    invoke-direct {p0, v0, v5}, Lcom/google/android/apps/hangouts/views/AudienceView;->a(Landroid/view/View;I)V

    sget v0, Lh;->o:I

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget v0, Lg;->ht:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v6, Lf;->bY:I

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v0, Lh;->az:I

    new-array v6, v7, [Ljava/lang/Object;

    aput-object v5, v6, v1

    invoke-virtual {v4, v0, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->j:Ljava/lang/Runnable;

    invoke-virtual {v2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 560
    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 561
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 564
    :cond_e
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/AudienceView;->h()V

    goto/16 :goto_0

    .line 534
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public static synthetic c(Lcom/google/android/apps/hangouts/views/AudienceView;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->c:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/apps/hangouts/views/AudienceView;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/AudienceView;->j()V

    return-void
.end method

.method public static synthetic e(Lcom/google/android/apps/hangouts/views/AudienceView;)Lcah;
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->f:Lcah;

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/apps/hangouts/views/AudienceView;)Landroid/widget/PopupWindow;
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->k:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method private g()Landroid/animation/Animator$AnimatorListener;
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 209
    new-instance v0, Lcae;

    invoke-direct {v0, p0}, Lcae;-><init>(Lcom/google/android/apps/hangouts/views/AudienceView;)V

    return-object v0
.end method

.method public static synthetic g(Lcom/google/android/apps/hangouts/views/AudienceView;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->b:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static synthetic h(Lcom/google/android/apps/hangouts/views/AudienceView;)Landroid/widget/PopupWindow;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->k:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method private h()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 576
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/AudienceView;->j()V

    .line 578
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->h:Laoe;

    if-eqz v0, :cond_0

    .line 579
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->h:Laoe;

    invoke-interface {v0}, Laoe;->a()V

    .line 584
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->i:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->m:Lyj;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->m:Lyj;

    .line 587
    invoke-virtual {v0}, Lyj;->s()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->b:Ljava/util/ArrayList;

    .line 588
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 589
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->d:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->setEnabled(Z)V

    .line 590
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->d:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    if-eqz v0, :cond_3

    :goto_1
    invoke-virtual {v2, v1}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->setVisibility(I)V

    .line 591
    return-void

    :cond_2
    move v0, v1

    .line 588
    goto :goto_0

    .line 590
    :cond_3
    const/16 v1, 0x8

    goto :goto_1
.end method

.method private i()I
    .locals 1

    .prologue
    .line 618
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 794
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 795
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->d:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 799
    :goto_0
    return-void

    .line 797
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->d:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->d:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->requestFocus()Z

    .line 347
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->d:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    invoke-static {v0}, Lf;->a(Landroid/view/View;)V

    .line 348
    return-void
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 360
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/AudienceView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->e:Ljava/lang/String;

    .line 361
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/AudienceView;->j()V

    .line 362
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->d:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 363
    return-void
.end method

.method public a(Lame;)V
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->d:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 240
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->d:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->d:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->setDropDownAnchor(I)V

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->d:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->setDropDownVerticalOffset(I)V

    .line 159
    :cond_0
    return-void
.end method

.method public a(Laoe;)V
    .locals 0

    .prologue
    .line 351
    iput-object p1, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->h:Laoe;

    .line 352
    return-void
.end method

.method public a(Lxm;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 391
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->b:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 392
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/AudienceView;->b()Lxm;

    move-result-object v1

    invoke-virtual {v1}, Lxm;->c()Ljava/util/List;

    move-result-object v1

    .line 393
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/AudienceView;->b()Lxm;

    move-result-object v2

    invoke-virtual {v2}, Lxm;->a()Ljava/util/List;

    move-result-object v2

    .line 395
    iget-object v3, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->b:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 398
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcah;

    .line 399
    iget-object v4, v0, Lcah;->b:Lxo;

    if-eqz v4, :cond_1

    iget-object v4, v0, Lcah;->b:Lxo;

    invoke-static {p1, v4}, Lf;->a(Lxm;Lxo;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 400
    iget-object v4, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->b:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 402
    :cond_1
    iget-object v4, v0, Lcah;->a:Lxs;

    if-eqz v4, :cond_0

    iget-object v4, v0, Lcah;->a:Lxs;

    invoke-static {p1, v4}, Lf;->a(Lxm;Lxs;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 403
    iget-object v4, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->b:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 408
    :cond_2
    invoke-virtual {p1}, Lxm;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxo;

    .line 409
    invoke-static {v1, v0}, Lf;->a(Ljava/lang/Iterable;Lxo;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 410
    new-instance v4, Lcah;

    invoke-direct {v4, v5}, Lcah;-><init>(B)V

    .line 411
    iput-object v0, v4, Lcah;->b:Lxo;

    .line 412
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 417
    :cond_4
    invoke-virtual {p1}, Lxm;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxs;

    .line 418
    invoke-static {v2, v0}, Lf;->a(Ljava/lang/Iterable;Lxs;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 419
    new-instance v3, Lcah;

    invoke-direct {v3, v5}, Lcah;-><init>(B)V

    .line 420
    iput-object v0, v3, Lcah;->a:Lxs;

    .line 421
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 425
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/AudienceView;->e()V

    .line 426
    return-void
.end method

.method public a(Lxo;)V
    .locals 2

    .prologue
    .line 434
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/AudienceView;->b()Lxm;

    move-result-object v0

    invoke-virtual {v0}, Lxm;->c()Ljava/util/List;

    move-result-object v0

    .line 435
    invoke-static {v0, p1}, Lf;->a(Ljava/lang/Iterable;Lxo;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 443
    :goto_0
    return-void

    .line 439
    :cond_0
    new-instance v0, Lcah;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcah;-><init>(B)V

    .line 440
    iput-object p1, v0, Lcah;->b:Lxo;

    .line 441
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 442
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/AudienceView;->e()V

    goto :goto_0
.end method

.method public a(Lxs;)V
    .locals 2

    .prologue
    .line 451
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/AudienceView;->b()Lxm;

    move-result-object v0

    invoke-virtual {v0}, Lxm;->a()Ljava/util/List;

    move-result-object v0

    .line 452
    invoke-static {v0, p1}, Lf;->a(Ljava/lang/Iterable;Lxs;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 460
    :goto_0
    return-void

    .line 456
    :cond_0
    new-instance v0, Lcah;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcah;-><init>(B)V

    .line 457
    iput-object p1, v0, Lcah;->a:Lxs;

    .line 458
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 459
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/AudienceView;->e()V

    goto :goto_0
.end method

.method public a(Lyj;)V
    .locals 0

    .prologue
    .line 865
    iput-object p1, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->m:Lyj;

    .line 866
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 235
    iput-boolean p1, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->n:Z

    .line 236
    return-void
.end method

.method public a(ZLjava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 872
    iput-boolean p1, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->i:Z

    .line 873
    iput-object p2, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->j:Ljava/lang/Runnable;

    .line 875
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/views/AudienceView;->b(Z)V

    .line 876
    return-void
.end method

.method public b()Lxm;
    .locals 4

    .prologue
    .line 369
    invoke-static {}, Lxm;->newBuilder()Lxn;

    move-result-object v1

    .line 370
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcah;

    .line 371
    iget-object v3, v0, Lcah;->b:Lxo;

    if-eqz v3, :cond_1

    .line 372
    iget-object v0, v0, Lcah;->b:Lxo;

    invoke-virtual {v1, v0}, Lxn;->a(Lxo;)Lxn;

    goto :goto_0

    .line 373
    :cond_1
    iget-object v3, v0, Lcah;->a:Lxs;

    if-eqz v3, :cond_0

    .line 374
    iget-object v0, v0, Lcah;->a:Lxs;

    invoke-virtual {v1, v0}, Lxn;->a(Lxs;)Lxn;

    goto :goto_0

    .line 377
    :cond_2
    invoke-virtual {v1}, Lxn;->a()Lxm;

    move-result-object v0

    return-object v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 381
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 382
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/AudienceView;->e()V

    .line 383
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 473
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->d:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 474
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 493
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/views/AudienceView;->b(Z)V

    .line 494
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 861
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->d:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->dismissDropDown()V

    .line 862
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 265
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    .line 266
    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 268
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->c:Landroid/view/ViewGroup;

    invoke-static {p1, v0}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 271
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->i:Z

    if-eqz v0, :cond_1

    .line 343
    :cond_0
    :goto_0
    return-void

    .line 274
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/AudienceView;->a()V

    goto :goto_0

    .line 277
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Ljava/lang/Runnable;

    if-eqz v0, :cond_3

    .line 278
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 279
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 283
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->i:Z

    if-nez v0, :cond_0

    .line 286
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 294
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcah;

    .line 295
    iget-object v1, v0, Lcah;->b:Lxo;

    if-eqz v1, :cond_5

    .line 296
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->h:Laoe;

    iget-object v2, v0, Lcah;->b:Lxo;

    invoke-virtual {v2}, Lxo;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v6, v6, v2}, Laoe;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 306
    :cond_4
    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->f:Lcah;

    .line 307
    iput-object p1, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->l:Landroid/view/View;

    .line 313
    sget v0, Lf;->ed:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/views/AudienceView;->b(I)Landroid/view/View;

    move-result-object v0

    .line 314
    sget v1, Lf;->cc:I

    sget v2, Lcom/google/android/apps/hangouts/R$drawable;->bg:I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/hangouts/views/AudienceView;->a(Landroid/view/View;II)V

    .line 316
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->f:Lcah;

    invoke-direct {p0, v0, v1, v5}, Lcom/google/android/apps/hangouts/views/AudienceView;->a(Landroid/view/View;Lcah;Z)V

    .line 317
    new-instance v1, Landroid/widget/PopupWindow;

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v3

    invoke-direct {v1, v0, v2, v3}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    iput-object v1, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->k:Landroid/widget/PopupWindow;

    .line 318
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->k:Landroid/widget/PopupWindow;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 319
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->k:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v5}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 321
    const/4 v1, 0x2

    new-array v1, v1, [I

    .line 322
    invoke-virtual {p1, v1}, Landroid/view/View;->getLocationInWindow([I)V

    .line 323
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->k:Landroid/widget/PopupWindow;

    aget v3, v1, v4

    aget v1, v1, v5

    invoke-virtual {v2, p0, v4, v3, v1}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 324
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->k:Landroid/widget/PopupWindow;

    new-instance v2, Lcaf;

    invoke-direct {v2, p0}, Lcaf;-><init>(Lcom/google/android/apps/hangouts/views/AudienceView;)V

    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 332
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->f:Lcah;

    .line 333
    new-instance v2, Lcag;

    invoke-direct {v2, p0, v1}, Lcag;-><init>(Lcom/google/android/apps/hangouts/views/AudienceView;Lcah;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 299
    :cond_5
    iget-object v1, v0, Lcah;->a:Lxs;

    if-eqz v1, :cond_4

    .line 300
    iget-object v1, v0, Lcah;->a:Lxs;

    invoke-virtual {v1}, Lxs;->b()Lbcx;

    move-result-object v1

    .line 301
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->h:Laoe;

    iget-object v3, v1, Lbcx;->a:Ljava/lang/String;

    iget-object v1, v1, Lbcx;->d:Ljava/lang/String;

    invoke-interface {v2, v3, v1, v6}, Laoe;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    goto/16 :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 851
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 853
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->l:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->k:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    .line 854
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 855
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->l:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->getLocationInWindow([I)V

    .line 856
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->k:Landroid/widget/PopupWindow;

    const/4 v2, 0x0

    aget v2, v0, v2

    const/4 v3, 0x1

    aget v0, v0, v3

    invoke-virtual {v1, v2, v0, v4, v4}, Landroid/widget/PopupWindow;->update(IIII)V

    .line 858
    :cond_0
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 254
    check-cast p1, Lcai;

    .line 255
    invoke-virtual {p1}, Lcai;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 256
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->d:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    iget-object v1, p1, Lcai;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 257
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->d:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    iget-object v1, p1, Lcai;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->setSelection(I)V

    .line 258
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 259
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->b:Ljava/util/ArrayList;

    iget-object v1, p1, Lcai;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 260
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/AudienceView;->e()V

    .line 261
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 244
    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 245
    new-instance v1, Lcai;

    invoke-direct {v1, v0}, Lcai;-><init>(Landroid/os/Parcelable;)V

    .line 246
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->d:Lcom/google/android/apps/hangouts/views/AudienceTextView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/AudienceTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcai;->a:Ljava/lang/String;

    .line 247
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudienceView;->b:Ljava/util/ArrayList;

    iput-object v0, v1, Lcai;->b:Ljava/util/ArrayList;

    .line 249
    return-object v1
.end method
