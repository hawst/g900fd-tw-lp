.class public Lcom/google/android/apps/hangouts/phone/ConversationIntentSecureActivity;
.super Landroid/app/Activity;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Z
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x1

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 18

    .prologue
    .line 29
    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 31
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/hangouts/phone/ConversationIntentSecureActivity;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 32
    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/phone/ConversationIntentSecureActivity;->setResult(I)V

    .line 33
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/hangouts/phone/ConversationIntentSecureActivity;->finish()V

    .line 104
    :goto_0
    return-void

    .line 37
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/hangouts/phone/ConversationIntentSecureActivity;->getIntent()Landroid/content/Intent;

    move-result-object v13

    .line 38
    if-nez v13, :cond_1

    .line 39
    const-string v1, "Babel"

    const-string v2, "No intent attached"

    invoke-static {v1, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/phone/ConversationIntentSecureActivity;->setResult(I)V

    .line 41
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/hangouts/phone/ConversationIntentSecureActivity;->finish()V

    goto :goto_0

    .line 45
    :cond_1
    invoke-virtual {v13}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    .line 46
    const-string v1, "account_name"

    invoke-virtual {v13, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 47
    const-string v2, "conversation_id"

    invoke-virtual {v13, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 48
    const-string v2, "participant_gaia"

    invoke-virtual {v13, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 49
    const-string v3, "participant_name"

    invoke-virtual {v13, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 50
    const-string v4, "auto_join_call_policy"

    invoke-virtual {v13, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 51
    const-string v4, "android.intent.extra.TEXT"

    invoke-virtual {v13, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_2

    const-string v4, "android.intent.extra.TEXT"

    invoke-static {v13, v4}, Lbbl;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 52
    :cond_2
    const-string v4, "wearable_watermark"

    const-wide/16 v5, 0x0

    invoke-virtual {v13, v4, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v9

    .line 54
    const-string v4, "is_sms"

    const/4 v5, 0x0

    invoke-virtual {v13, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v11

    .line 55
    const-string v4, "requires_mms"

    const/4 v5, 0x0

    invoke-virtual {v13, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v12

    .line 56
    const-string v4, "hangout_uri"

    invoke-virtual {v13, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/net/Uri;

    .line 58
    const-string v5, "hangout_call_end_intent"

    invoke-virtual {v13, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Landroid/app/PendingIntent;

    .line 60
    if-eqz v2, :cond_3

    if-eqz v4, :cond_3

    const-string v6, "Babel"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "Got intent with non-null gaiaId and "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-static {v6, v0}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v6, 0x0

    :goto_1
    if-nez v6, :cond_5

    .line 61
    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/phone/ConversationIntentSecureActivity;->setResult(I)V

    .line 62
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/hangouts/phone/ConversationIntentSecureActivity;->finish()V

    goto/16 :goto_0

    .line 60
    :cond_3
    const-string v6, "android.intent.action.SENDTO"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    const-string v6, "Babel"

    const-string v16, "SENDTO action must include message text"

    move-object/from16 v0, v16

    invoke-static {v6, v0}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v6, 0x0

    goto :goto_1

    :cond_4
    const/4 v6, 0x1

    goto :goto_1

    .line 67
    :cond_5
    if-nez v4, :cond_6

    const-string v6, "start_video"

    const/16 v16, 0x0

    .line 68
    move/from16 v0, v16

    invoke-virtual {v13, v6, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 69
    :cond_6
    const-string v6, "hangout_start_source"

    const/16 v7, 0x3b

    invoke-virtual {v13, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .line 71
    const-string v7, "never"

    invoke-virtual {v7, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 72
    invoke-static/range {v1 .. v6}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/app/PendingIntent;I)Landroid/content/Intent;

    move-result-object v1

    .line 100
    :cond_7
    :goto_2
    const/high16 v2, 0x4000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 101
    const/4 v2, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/phone/ConversationIntentSecureActivity;->setResult(I)V

    .line 102
    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/phone/ConversationIntentSecureActivity;->startActivity(Landroid/content/Intent;)V

    .line 103
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/hangouts/phone/ConversationIntentSecureActivity;->finish()V

    goto/16 :goto_0

    .line 77
    :cond_8
    invoke-static/range {v1 .. v6}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/app/PendingIntent;I)Landroid/content/Intent;

    move-result-object v1

    goto :goto_2

    .line 81
    :cond_9
    const-string v4, "android.intent.action.SENDTO"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    move-object v4, v1

    move-object v5, v14

    move-object v6, v2

    move-object v7, v3

    .line 82
    invoke-static/range {v4 .. v12}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZZ)Landroid/content/Intent;

    move-result-object v1

    .line 84
    const-string v2, "otr_state"

    invoke-virtual {v13, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 85
    const-string v2, "otr_state"

    const-string v3, "otr_state"

    const/4 v4, 0x0

    .line 86
    invoke-virtual {v13, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 85
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_2

    .line 88
    :cond_a
    const-string v4, "com.google.android.apps.babel.realtimechat.update-watermark"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 89
    invoke-static {v1, v14, v11}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->a(Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    goto :goto_2

    .line 91
    :cond_b
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_c

    .line 92
    invoke-static {v1, v14, v8}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    goto :goto_2

    .line 94
    :cond_c
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_d

    .line 95
    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v8, v4}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    goto :goto_2

    .line 98
    :cond_d
    invoke-static {v1}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->b(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    goto :goto_2
.end method
