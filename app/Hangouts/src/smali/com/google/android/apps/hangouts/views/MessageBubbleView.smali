.class public Lcom/google/android/apps/hangouts/views/MessageBubbleView;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field private a:I

.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 16
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 11
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->a:I

    .line 20
    return-void
.end method


# virtual methods
.method public a(FI)V
    .locals 1

    .prologue
    .line 36
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p1, v0

    if-nez v0, :cond_0

    .line 37
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->a:I

    .line 42
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->requestLayout()V

    .line 43
    return-void

    .line 39
    :cond_0
    iget v0, p0, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->b:I

    sub-int/2addr v0, p2

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    add-int/2addr v0, p2

    iput v0, p0, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->a:I

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 24
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 26
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->getMeasuredWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->b:I

    .line 28
    iget v0, p0, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->a:I

    if-lez v0, :cond_0

    .line 29
    iget v0, p0, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->a:I

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->setMeasuredDimension(II)V

    .line 33
    :goto_0
    return-void

    .line 31
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->setMeasuredDimension(II)V

    goto :goto_0
.end method
