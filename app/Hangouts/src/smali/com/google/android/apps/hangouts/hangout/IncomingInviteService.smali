.class public Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;
.super Lbou;
.source "PG"


# static fields
.field private static final a:Z

.field private static final b:Ljava/lang/Object;

.field private static c:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lbys;->e:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->a:Z

    .line 63
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->b:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lbou;-><init>()V

    return-void
.end method

.method private static a(Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;Lyj;)Lcom/google/android/libraries/hangouts/video/HangoutRequest;
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 351
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->conversationId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 352
    const-string v4, "conversation"

    .line 353
    iget-object v5, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->conversationId:Ljava/lang/String;

    .line 355
    :goto_0
    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->nick:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v8, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->nick:Ljava/lang/String;

    .line 357
    :goto_1
    new-instance v0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-virtual {p1}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->mediaType:Ljava/lang/Integer;

    .line 358
    invoke-static {v3, v2}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v3

    iget-object v7, p0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->hangoutId:Ljava/lang/String;

    move-object v9, v6

    invoke-direct/range {v0 .. v9}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;)V

    return-object v0

    :cond_0
    move-object v8, v6

    .line 355
    goto :goto_1

    :cond_1
    move-object v4, v6

    move-object v5, v6

    goto :goto_0
.end method

.method public static a(Landroid/content/Intent;)V
    .locals 5

    .prologue
    .line 71
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    .line 72
    sget-object v2, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->b:Ljava/lang/Object;

    monitor-enter v2

    .line 73
    :try_start_0
    sget-object v0, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->c:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_1

    .line 74
    sget-boolean v0, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->a:Z

    if-eqz v0, :cond_0

    .line 75
    const-string v0, "initializing wakelock"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->c(Ljava/lang/String;)V

    .line 77
    :cond_0
    const-string v0, "power"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 78
    const/4 v3, 0x1

    const-string v4, "babel_hoinv"

    invoke-virtual {v0, v3, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->c:Landroid/os/PowerManager$WakeLock;

    .line 80
    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    sget-boolean v0, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->a:Z

    if-eqz v0, :cond_2

    .line 82
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "acquiring wakelock for startService "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->c(Ljava/lang/String;)V

    .line 84
    :cond_2
    sget-object v0, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 85
    const-class v0, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;

    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 86
    invoke-virtual {v1, p0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 87
    return-void

    .line 80
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method private static a(Lyj;Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;I)V
    .locals 6

    .prologue
    .line 428
    new-instance v0, Ldzk;

    invoke-direct {v0}, Ldzk;-><init>()V

    .line 429
    if-eqz p1, :cond_0

    .line 430
    iget-object v1, p1, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->invitationId:Ljava/lang/Long;

    iput-object v1, v0, Ldzk;->b:Ljava/lang/Long;

    .line 431
    iget-object v1, p1, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->context:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;

    if-eqz v1, :cond_0

    .line 432
    iget-object v1, p1, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->context:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;

    iget-object v1, v1, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->hangoutId:Ljava/lang/String;

    iput-object v1, v0, Ldzk;->c:Ljava/lang/String;

    .line 436
    :cond_0
    new-instance v1, Ldzl;

    invoke-direct {v1}, Ldzl;-><init>()V

    .line 438
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Ldzl;->b:Ljava/lang/Long;

    .line 439
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Ldzl;->d:Ljava/lang/Integer;

    .line 440
    iput-object v1, v0, Ldzk;->i:Ldzl;

    .line 442
    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ldzk;)I

    .line 443
    return-void
.end method

.method private static b(Landroid/content/Intent;)Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 331
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 333
    if-nez v0, :cond_0

    .line 334
    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Missing HangoutInviteNotification: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 343
    :goto_0
    return-object v0

    .line 338
    :cond_0
    const/4 v2, 0x0

    invoke-static {v0, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 340
    :try_start_0
    new-instance v2, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;

    invoke-direct {v2}, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;-><init>()V

    invoke-static {v2, v0}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v0

    check-cast v0, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;
    :try_end_0
    .catch Lepq; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 342
    :catch_0
    move-exception v0

    const-string v0, "Babel"

    const-string v2, "Invalid BatchCommand message received"

    invoke-static {v0, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 343
    goto :goto_0
.end method

.method private static b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 446
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[IncomingInviteService] "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    return-void
.end method

.method private static c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 450
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[IncomingInviteService] "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    return-void
.end method


# virtual methods
.method protected a()I
    .locals 1

    .prologue
    .line 91
    const/16 v0, 0x1388

    return v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 128
    invoke-super {p0}, Lbou;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    sget-object v0, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 130
    const/4 v0, 0x1

    .line 132
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 138
    invoke-super {p0}, Lbou;->c()V

    .line 139
    sget-object v0, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 140
    return-void
.end method

.method protected d()V
    .locals 1

    .prologue
    .line 144
    invoke-super {p0}, Lbou;->d()V

    .line 145
    sget-object v0, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    invoke-static {v0}, Lcwz;->b(Z)V

    .line 146
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 151
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 421
    invoke-super {p0}, Lbou;->onCreate()V

    .line 422
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->b(Ljava/lang/String;)V

    .line 423
    invoke-static {}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->p()V

    .line 424
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 19

    .prologue
    .line 100
    sget-object v2, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v2

    invoke-static {v2}, Lcwz;->a(Z)V

    .line 102
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->a(I)V

    .line 105
    :try_start_0
    invoke-static/range {p1 .. p1}, Lcwz;->b(Ljava/lang/Object;)V

    .line 108
    if-eqz p1, :cond_0

    .line 110
    const-string v2, "Babel"

    const-string v3, "Hangout Invitation Receiver got invitation GCM"

    invoke-static {v2, v3}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "focus_account_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbdk;->b(Ljava/lang/String;)Lbdk;

    move-result-object v2

    invoke-static {v2}, Lbkb;->a(Lbdk;)Lyj;

    move-result-object v4

    const-string v2, "id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v2, "Babel"

    const-string v3, "Missing hangoutInviteId"

    invoke-static {v2, v3}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-static {v4, v2, v3}, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->a(Lyj;Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 118
    :cond_0
    :goto_0
    sget-object v2, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 120
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->c()V

    .line 122
    const/4 v2, 0x2

    return v2

    .line 110
    :cond_1
    :try_start_1
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->b(Landroid/content/Intent;)Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;

    move-result-object v5

    if-nez v5, :cond_2

    const/4 v2, 0x3

    invoke-static {v4, v5, v2}, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->a(Lyj;Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 118
    :catchall_0
    move-exception v2

    sget-object v3, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 120
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->c()V

    throw v2

    .line 110
    :cond_2
    :try_start_2
    iget-object v3, v5, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->context:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;

    invoke-static {v3, v4}, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->a(Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;Lyj;)Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v13

    iget-object v2, v5, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->command:Ljava/lang/Integer;

    if-nez v2, :cond_3

    const-string v2, "Babel"

    const-string v3, "Ignoring hangoutInviteNotification without any command"

    invoke-static {v2, v3}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x3

    invoke-static {v4, v5, v2}, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->a(Lyj;Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;I)V

    goto :goto_0

    :cond_3
    iget-object v2, v5, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->command:Ljava/lang/Integer;

    const/4 v6, 0x0

    invoke-static {v2, v6}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v2

    const/4 v6, 0x1

    if-ne v2, v6, :cond_7

    iget-object v2, v5, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->context:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;

    const-string v3, "Babel"

    const/4 v6, 0x3

    invoke-static {v3, v6}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_4

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "Got hangoutInviteNotification DISMISS:\nreason: "

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v5, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->dismissReason:Ljava/lang/Integer;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\nHangoutId: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, v2, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->hangoutId:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->b(Ljava/lang/String;)V

    :cond_4
    invoke-static {v2, v4}, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->a(Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;Lyj;)Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v2

    invoke-static {}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->a()Lcom/google/android/apps/hangouts/hangout/IncomingRing;

    move-result-object v3

    if-nez v3, :cond_5

    const-string v2, "Babel"

    const-string v3, "Ignoring dismiss command since ring activity is not running."

    invoke-static {v2, v3}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    invoke-virtual {v3}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->f()Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    const-string v4, "Babel"

    const-string v5, "Cancelling hangout ringing."

    invoke-static {v4, v5}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getInviteeNick()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcwz;->a(Ljava/lang/Object;)V

    invoke-virtual {v3}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->n()V

    goto/16 :goto_0

    :cond_6
    const-string v2, "Babel"

    const-string v3, "Ignoring hangout ring cancellation since hangout ids don\'t match"

    invoke-static {v2, v3}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    iget-object v12, v3, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->invitation:Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;

    if-nez v12, :cond_8

    const-string v2, "Babel"

    const-string v3, "Ignoring hangoutStartContext without invitation"

    invoke-static {v2, v3}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x3

    invoke-static {v4, v5, v2}, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->a(Lyj;Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;I)V

    goto/16 :goto_0

    :cond_8
    iget-object v6, v12, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->inviterGaiaId:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    iget-object v2, v12, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->timestamp:Ljava/lang/Long;

    if-nez v2, :cond_a

    :cond_9
    const-string v2, "Babel"

    const-string v3, "Ignoring hangoutStartContext without invitation data"

    invoke-static {v2, v3}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x3

    invoke-static {v4, v5, v2}, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->a(Lyj;Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;I)V

    goto/16 :goto_0

    :cond_a
    iget-object v2, v12, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->invitationType:Ljava/lang/Integer;

    const/4 v7, 0x0

    invoke-static {v2, v7}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v2

    if-eqz v2, :cond_c

    const/4 v7, 0x1

    if-eq v2, v7, :cond_c

    const/4 v7, 0x2

    if-eq v2, v7, :cond_c

    const-string v2, "Babel"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_b

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Ignoring unsupported InvitationType "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v12, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->invitationType:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->b(Ljava/lang/String;)V

    :cond_b
    const/4 v2, 0x5

    invoke-static {v4, v5, v2}, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->a(Lyj;Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;I)V

    goto/16 :goto_0

    :cond_c
    invoke-virtual {v13}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getInviteeNick()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_d

    const/4 v2, 0x3

    invoke-static {v4, v5, v2}, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->a(Lyj;Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;I)V

    goto/16 :goto_0

    :cond_d
    iget-object v2, v3, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->hangoutType:Ljava/lang/Integer;

    const/4 v7, 0x0

    invoke-static {v2, v7}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v2

    if-eqz v2, :cond_f

    const/4 v7, 0x3

    if-eq v2, v7, :cond_f

    const/4 v7, 0x2

    if-eq v2, v7, :cond_f

    const-string v3, "Babel"

    const/4 v6, 0x3

    invoke-static {v3, v6}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_e

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "Using ding for unsupported hangout type: "

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->b(Ljava/lang/String;)V

    :cond_e
    const/4 v2, 0x4

    invoke-static {v4, v5, v2}, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->a(Lyj;Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;I)V

    goto/16 :goto_0

    :cond_f
    invoke-virtual {v4}, Lyj;->aj()Z

    move-result v2

    if-nez v2, :cond_10

    const/4 v2, 0x7

    invoke-static {v4, v5, v2}, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->a(Lyj;Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;I)V

    goto/16 :goto_0

    :cond_10
    iget-object v2, v3, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->conversationId:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_11

    iget-object v7, v3, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->conversationId:Ljava/lang/String;

    :goto_1
    iget-object v2, v5, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->invitationId:Ljava/lang/Long;

    invoke-static {v2}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v8

    iget-object v2, v12, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->timestamp:Ljava/lang/Long;

    invoke-static {v2}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v10

    const-wide/16 v14, 0x3e8

    mul-long/2addr v10, v14

    iget-object v2, v5, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;->notificationType:Ljava/lang/Integer;

    const/4 v14, 0x0

    invoke-static {v2, v14}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v2

    if-eqz v2, :cond_12

    new-instance v2, Laql;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v11}, Laql;-><init>(Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;Lyj;Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;Ljava/lang/String;Ljava/lang/String;JJ)V

    invoke-static {v2}, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;->executeOnThreadPool(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    :cond_11
    const/4 v7, 0x0

    goto :goto_1

    :cond_12
    invoke-static {}, Lape;->c()Ljava/lang/Boolean;

    move-result-object v2

    const/4 v10, 0x0

    invoke-static {v2, v10}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v2

    if-eqz v2, :cond_13

    invoke-static {}, Lapk;->b()Lapk;

    move-result-object v2

    invoke-virtual {v2}, Lapk;->c()Lapx;

    move-result-object v2

    if-eqz v2, :cond_13

    const/4 v2, 0x1

    :goto_2
    if-eqz v2, :cond_16

    const-string v2, "Babel"

    const-string v3, "Downgrading ring to ding because of ongoing ring/call"

    invoke-static {v2, v3}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x1

    invoke-static {v4, v5, v2}, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->a(Lyj;Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;I)V

    goto/16 :goto_0

    :cond_13
    invoke-static {}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->a()Lcom/google/android/apps/hangouts/hangout/IncomingRing;

    move-result-object v2

    if-eqz v2, :cond_14

    const/4 v2, 0x1

    goto :goto_2

    :cond_14
    const-string v2, "phone"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v2

    if-eqz v2, :cond_15

    const/4 v2, 0x1

    goto :goto_2

    :cond_15
    const/4 v2, 0x0

    goto :goto_2

    :cond_16
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v10, Lf;->bO:I

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v10

    sget v11, Lh;->eX:I

    invoke-virtual {v2, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4}, Lyj;->b()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lf;->k(Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v11

    invoke-interface {v11, v2, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_17

    const-string v2, "Babel"

    const-string v3, "Downgrading ring to ding because of user does not want to be notified"

    invoke-static {v2, v3}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x6

    invoke-static {v4, v5, v2}, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->a(Lyj;Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutInviteNotification;I)V

    goto/16 :goto_0

    :cond_17
    const-string v2, "Babel"

    const/4 v4, 0x3

    invoke-static {v2, v4}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_18

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Got ring hangoutInviteNotification:\nInviterGaiaId: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\nHangoutId: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v3, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext;->hangoutId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;->b(Ljava/lang/String;)V

    :cond_18
    iget-object v2, v12, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->isInviterPstnParticipant:Ljava/lang/Boolean;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v2

    if-eqz v2, :cond_19

    iget-object v0, v12, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->inviterPhoneNumber:Ljava/lang/String;

    move-object/from16 v16, v0

    :goto_3
    iget-object v2, v12, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->isInviterPstnParticipant:Ljava/lang/Boolean;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v2

    if-eqz v2, :cond_1a

    const/16 v17, 0x0

    :goto_4
    const-string v2, "inviter_jid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v10, p0

    move-wide v11, v8

    move-object v14, v6

    move-object v15, v7

    invoke-static/range {v10 .. v18}, Lcom/google/android/apps/hangouts/hangout/IncomingRing;->a(Lcom/google/android/apps/hangouts/hangout/IncomingInviteService;JLcom/google/android/libraries/hangouts/video/HangoutRequest;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_19
    const/16 v16, 0x0

    goto :goto_3

    :cond_1a
    iget-object v0, v12, Lcom/google/apps/gcomm/hangout/proto/Hangouts$HangoutStartContext$Invitation;->inviterProfileName:Ljava/lang/String;

    move-object/from16 v17, v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4
.end method
