.class public Lcom/google/android/apps/hangouts/views/ScrollViewCustom;
.super Landroid/widget/ScrollView;
.source "PG"


# instance fields
.field private a:I

.field private b:I

.field private c:Landroid/os/Handler;

.field private d:Z

.field private e:Lwr;

.field private f:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 28
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/apps/hangouts/views/ScrollViewCustom;->a:I

    .line 22
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/ScrollViewCustom;->c:Landroid/os/Handler;

    .line 72
    new-instance v0, Lcej;

    invoke-direct {v0, p0}, Lcej;-><init>(Lcom/google/android/apps/hangouts/views/ScrollViewCustom;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/ScrollViewCustom;->f:Ljava/lang/Runnable;

    .line 29
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lwj;->F:[I

    invoke-virtual {v0, p2, v1, v2, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 32
    const/4 v0, 0x0

    :try_start_0
    iget v2, p0, Lcom/google/android/apps/hangouts/views/ScrollViewCustom;->a:I

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/hangouts/views/ScrollViewCustom;->a:I

    .line 34
    const/4 v0, 0x1

    iget v2, p0, Lcom/google/android/apps/hangouts/views/ScrollViewCustom;->b:I

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/hangouts/views/ScrollViewCustom;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 37
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 39
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ScrollViewCustom;->e:Lwr;

    if-nez v0, :cond_0

    .line 40
    const-string v0, "height"

    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-static {p0, v0, v1}, Lwr;->a(Ljava/lang/Object;Ljava/lang/String;[I)Lwr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/ScrollViewCustom;->e:Lwr;

    .line 41
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ScrollViewCustom;->e:Lwr;

    new-instance v1, Lcei;

    invoke-direct {v1, p0}, Lcei;-><init>(Lcom/google/android/apps/hangouts/views/ScrollViewCustom;)V

    invoke-virtual {v0, v1}, Lwr;->a(Lwl;)V

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ScrollViewCustom;->e:Lwr;

    const-wide/16 v1, 0xc8

    invoke-virtual {v0, v1, v2}, Lwr;->c(J)Lwr;

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ScrollViewCustom;->e:Lwr;

    invoke-virtual {v0, p0}, Lwr;->b(Ljava/lang/Object;)V

    .line 65
    :cond_0
    return-void

    .line 37
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0

    .line 40
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/views/ScrollViewCustom;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ScrollViewCustom;->f:Ljava/lang/Runnable;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/views/ScrollViewCustom;Z)Z
    .locals 0

    .prologue
    .line 19
    iput-boolean p1, p0, Lcom/google/android/apps/hangouts/views/ScrollViewCustom;->d:Z

    return p1
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/views/ScrollViewCustom;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ScrollViewCustom;->c:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/apps/hangouts/views/ScrollViewCustom;)I
    .locals 1

    .prologue
    .line 19
    iget v0, p0, Lcom/google/android/apps/hangouts/views/ScrollViewCustom;->a:I

    return v0
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 81
    invoke-virtual {p0, v4}, Lcom/google/android/apps/hangouts/views/ScrollViewCustom;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    .line 82
    iget v1, p0, Lcom/google/android/apps/hangouts/views/ScrollViewCustom;->a:I

    iget v2, p0, Lcom/google/android/apps/hangouts/views/ScrollViewCustom;->b:I

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 83
    iget v2, p0, Lcom/google/android/apps/hangouts/views/ScrollViewCustom;->a:I

    .line 84
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 85
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 83
    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 86
    invoke-super {p0, p1, v2}, Landroid/widget/ScrollView;->onMeasure(II)V

    .line 88
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/ScrollViewCustom;->getMeasuredHeight()I

    move-result v2

    if-eq v1, v2, :cond_1

    iget-boolean v2, p0, Lcom/google/android/apps/hangouts/views/ScrollViewCustom;->d:Z

    if-nez v2, :cond_1

    .line 89
    iput-boolean v5, p0, Lcom/google/android/apps/hangouts/views/ScrollViewCustom;->d:Z

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ScrollViewCustom;->e:Lwr;

    const/4 v2, 0x2

    new-array v2, v2, [I

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/ScrollViewCustom;->getMeasuredHeight()I

    move-result v3

    aput v3, v2, v4

    aput v1, v2, v5

    invoke-virtual {v0, v2}, Lwr;->a([I)V

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ScrollViewCustom;->e:Lwr;

    invoke-virtual {v0}, Lwr;->b()V

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 92
    :cond_1
    iget v1, p0, Lcom/google/android/apps/hangouts/views/ScrollViewCustom;->a:I

    if-le v0, v1, :cond_0

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ScrollViewCustom;->c:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/ScrollViewCustom;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public setHeight(I)V
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/ScrollViewCustom;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 69
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/ScrollViewCustom;->requestLayout()V

    .line 70
    return-void
.end method
