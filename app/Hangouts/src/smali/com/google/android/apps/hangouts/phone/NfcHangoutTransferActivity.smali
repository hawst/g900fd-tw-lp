.class public Lcom/google/android/apps/hangouts/phone/NfcHangoutTransferActivity;
.super Ly;
.source "PG"

# interfaces
.implements Labd;


# instance fields
.field private n:Lcom/google/android/libraries/hangouts/video/HangoutRequest;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ly;-><init>()V

    return-void
.end method


# virtual methods
.method public a_(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 53
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/NfcHangoutTransferActivity;->n:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->cloneForCallTransferReceive(Ljava/lang/String;)Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/NfcHangoutTransferActivity;->n:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    .line 54
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/NfcHangoutTransferActivity;->n:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    const/4 v3, 0x0

    const/16 v4, 0x33

    .line 56
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    move-object v2, v1

    .line 54
    invoke-static/range {v0 .. v6}, Lbbl;->a(Lcom/google/android/libraries/hangouts/video/HangoutRequest;Ljava/util/ArrayList;Ljava/util/ArrayList;ZIJ)Landroid/content/Intent;

    move-result-object v0

    .line 57
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/NfcHangoutTransferActivity;->startActivity(Landroid/content/Intent;)V

    .line 58
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/NfcHangoutTransferActivity;->finish()V

    .line 59
    return-void
.end method

.method public n_()V
    .locals 0

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/NfcHangoutTransferActivity;->finish()V

    .line 64
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 30
    invoke-super {p0, p1}, Ly;->onCreate(Landroid/os/Bundle;)V

    .line 32
    invoke-static {}, Lbxm;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/NfcHangoutTransferActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "android.nfc.action.NDEF_DISCOVERED"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "vnd.android.nfc://ext/com.google.android.apps.hangouts:hangoutrequest"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 33
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/NfcHangoutTransferActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lf;->a(Landroid/content/Intent;)Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/NfcHangoutTransferActivity;->n:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    .line 39
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/NfcHangoutTransferActivity;->n:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getAccountName()Ljava/lang/String;

    move-result-object v0

    .line 40
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 41
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/NfcHangoutTransferActivity;->a_(Ljava/lang/String;)V

    .line 49
    :goto_1
    return-void

    .line 32
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 35
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/NfcHangoutTransferActivity;->finish()V

    goto :goto_1

    .line 45
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/NfcHangoutTransferActivity;->e()Lae;

    move-result-object v0

    invoke-virtual {v0}, Lae;->a()Lao;

    move-result-object v0

    .line 46
    invoke-static {}, Laay;->a()Laay;

    move-result-object v1

    .line 47
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lao;->a(Lt;Ljava/lang/String;)Lao;

    .line 48
    invoke-virtual {v0}, Lao;->b()I

    goto :goto_1
.end method
