.class public final Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;
.super Latc;
.source "PG"


# instance fields
.field public final a:Latd;

.field private final d:I

.field private final e:Lapk;

.field private final f:Latm;

.field private final g:Latn;

.field private h:Lasx;

.field private i:Latu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Latu",
            "<*>;"
        }
    .end annotation
.end field

.field private final j:Ljava/lang/Object;

.field private k:Landroid/os/Handler;

.field private final l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Late;",
            ">;"
        }
    .end annotation
.end field

.field private m:J

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private final s:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea$HorizontalScrollNotifier;

.field private final t:Landroid/widget/LinearLayout;

.field private final u:Landroid/widget/LinearLayout;

.field private final v:Landroid/widget/FrameLayout;

.field private final w:Lcxb;


# direct methods
.method public constructor <init>(Latd;Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 211
    invoke-direct {p0}, Latc;-><init>()V

    .line 177
    invoke-static {}, Lapk;->b()Lapk;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->e:Lapk;

    .line 178
    new-instance v0, Latm;

    invoke-direct {v0, p0, v3}, Latm;-><init>(Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;B)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->f:Latm;

    .line 183
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->i:Latu;

    .line 185
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->k:Landroid/os/Handler;

    .line 187
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->l:Ljava/util/ArrayList;

    .line 208
    new-instance v0, Lcxb;

    const-string v1, "ParticipantTrayGLArea"

    invoke-direct {v0, v1}, Lcxb;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->w:Lcxb;

    .line 212
    iput-object p1, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->a:Latd;

    .line 213
    iput-object p2, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->j:Ljava/lang/Object;

    .line 214
    iput-boolean v3, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->r:Z

    .line 215
    invoke-virtual {p1}, Latd;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 216
    sget v1, Lf;->dO:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->d:I

    .line 219
    invoke-virtual {p1}, Latd;->b()Landroid/view/ViewGroup;

    move-result-object v1

    .line 220
    sget v0, Lg;->gy:I

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea$HorizontalScrollNotifier;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->s:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea$HorizontalScrollNotifier;

    .line 221
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->s:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea$HorizontalScrollNotifier;

    new-instance v2, Latk;

    invoke-direct {v2, p0}, Latk;-><init>(Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea$HorizontalScrollNotifier;->a(Lato;)V

    .line 243
    sget v0, Lg;->fg:I

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->t:Landroid/widget/LinearLayout;

    .line 247
    sget v0, Lg;->gg:I

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->u:Landroid/widget/LinearLayout;

    .line 251
    sget v0, Lg;->gB:I

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->v:Landroid/widget/FrameLayout;

    .line 254
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->q:Z

    .line 255
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    .line 257
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->t:Landroid/widget/LinearLayout;

    .line 258
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 259
    const/4 v1, 0x3

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 260
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 261
    iput-boolean v3, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->q:Z

    .line 264
    :cond_0
    new-instance v0, Latn;

    invoke-direct {v0, p0, v3}, Latn;-><init>(Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;B)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->g:Latn;

    .line 265
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->a:Latd;

    invoke-virtual {v0}, Latd;->h()Laqv;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->g:Latn;

    invoke-virtual {v0, v1}, Laqv;->a(Larn;)V

    .line 266
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;)V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->a(Z)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Z)V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->a(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;ZZ)V

    return-void
.end method

.method private a(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;ZZ)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 406
    invoke-static {}, Lcwz;->a()V

    .line 407
    if-nez p1, :cond_0

    if-nez p1, :cond_4

    if-eqz p2, :cond_4

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 409
    iget-object v9, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->j:Ljava/lang/Object;

    monitor-enter v9

    .line 412
    if-eqz p2, :cond_6

    .line 413
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->i:Latu;

    if-nez v0, :cond_a

    .line 414
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_5

    .line 415
    new-instance v0, Lauf;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->a:Latd;

    iget-object v4, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->h:Lasx;

    iget-object v5, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->j:Ljava/lang/Object;

    iget v6, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->b:I

    iget v7, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->c:I

    iget-object v8, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->v:Landroid/widget/FrameLayout;

    move-object v2, p1

    move-object v3, p0

    invoke-direct/range {v0 .. v8}, Lauf;-><init>(Latd;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;Lasx;Ljava/lang/Object;IILandroid/view/ViewGroup;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->i:Latu;

    .line 423
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->i:Latu;

    .line 425
    :goto_2
    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    .line 428
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->i:Latu;

    invoke-virtual {v1, p1}, Latu;->a(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V

    .line 439
    :cond_1
    :goto_3
    if-eqz v0, :cond_2

    .line 440
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->l:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 443
    :cond_2
    if-eqz p3, :cond_3

    .line 445
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->l()V

    .line 449
    :cond_3
    invoke-direct {p0, p3}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->a(Z)V

    .line 450
    monitor-exit v9

    return-void

    .line 407
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 419
    :cond_5
    new-instance v0, Lauc;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->a:Latd;

    iget-object v4, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->h:Lasx;

    iget-object v5, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->j:Ljava/lang/Object;

    iget v6, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->b:I

    iget v7, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->c:I

    iget-object v8, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->v:Landroid/widget/FrameLayout;

    move-object v2, p1

    move-object v3, p0

    invoke-direct/range {v0 .. v8}, Lauc;-><init>(Latd;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;Lasx;Ljava/lang/Object;IILandroid/view/ViewGroup;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->i:Latu;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 450
    :catchall_0
    move-exception v0

    monitor-exit v9

    throw v0

    .line 431
    :cond_6
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Late;

    invoke-virtual {v0}, Late;->l()Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    move-result-object v3

    if-ne p1, v3, :cond_7

    .line 432
    :goto_4
    if-nez v0, :cond_9

    .line 433
    new-instance v0, Latp;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->a:Latd;

    iget-object v4, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->h:Lasx;

    iget-object v5, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->j:Ljava/lang/Object;

    iget v6, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->b:I

    iget v7, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->c:I

    iget-object v8, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->u:Landroid/widget/LinearLayout;

    move-object v1, p1

    move-object v3, p0

    invoke-direct/range {v0 .. v8}, Latp;-><init>(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Latd;Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;Lasx;Ljava/lang/Object;IILandroid/widget/LinearLayout;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    :cond_8
    move-object v0, v1

    .line 431
    goto :goto_4

    :cond_9
    move-object v0, v1

    goto :goto_3

    :cond_a
    move-object v0, v1

    goto :goto_2
.end method

.method private a(Z)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 716
    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->j:Ljava/lang/Object;

    monitor-enter v2

    .line 717
    :try_start_0
    iget-boolean v3, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->p:Z

    .line 718
    iget-object v4, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->e:Lapk;

    invoke-virtual {v4}, Lapk;->c()Lapx;

    move-result-object v4

    .line 719
    iget-object v5, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->l:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-le v5, v0, :cond_0

    if-eqz v4, :cond_3

    .line 720
    invoke-virtual {v4}, Lapx;->K()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 723
    :cond_0
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->p:Z

    .line 724
    iget-object v4, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->u:Landroid/widget/LinearLayout;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 725
    iget-object v4, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->v:Landroid/widget/FrameLayout;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 741
    :goto_0
    if-eqz p1, :cond_1

    iget-boolean v4, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->p:Z

    if-eq v3, v4, :cond_1

    iget-boolean v3, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->n:Z

    if-nez v3, :cond_1

    .line 742
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->n:Z

    .line 743
    iget-boolean v3, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->p:Z

    if-nez v3, :cond_5

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->o:Z

    .line 744
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->m:J

    .line 748
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-ne v0, v6, :cond_2

    .line 749
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->g()V

    .line 751
    :cond_2
    monitor-exit v2

    return-void

    .line 726
    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->l:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_4

    iget-object v4, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->i:Latu;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->i:Latu;

    .line 727
    invoke-virtual {v4}, Latu;->x()Z

    move-result v4

    if-nez v4, :cond_4

    .line 730
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->p:Z

    .line 731
    iget-object v4, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->u:Landroid/widget/LinearLayout;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 732
    iget-object v4, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->v:Landroid/widget/FrameLayout;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 751
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 735
    :cond_4
    const/4 v4, 0x1

    :try_start_1
    iput-boolean v4, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->p:Z

    .line 736
    iget-object v4, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->u:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 737
    iget-object v4, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->v:Landroid/widget/FrameLayout;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :cond_5
    move v0, v1

    .line 743
    goto :goto_1
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;Z)Z
    .locals 0

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->q:Z

    return p1
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->j:Ljava/lang/Object;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;)Lapk;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->e:Lapk;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;)Lasx;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->h:Lasx;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->m()V

    return-void
.end method

.method public static synthetic f(Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->v:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method private l()V
    .locals 1

    .prologue
    .line 454
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->h:Lasx;

    invoke-virtual {v0}, Lasx;->d()V

    .line 455
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->n()V

    .line 456
    return-void
.end method

.method private m()V
    .locals 2

    .prologue
    .line 532
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Late;

    .line 533
    invoke-virtual {v0}, Late;->y()V

    goto :goto_0

    .line 535
    :cond_0
    return-void
.end method

.method private n()V
    .locals 5

    .prologue
    .line 559
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->q:Z

    if-eqz v0, :cond_0

    .line 560
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->m()V

    .line 561
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->v:Landroid/widget/FrameLayout;

    .line 562
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->k:Landroid/os/Handler;

    new-instance v2, Latl;

    invoke-direct {v2, p0, v0}, Latl;-><init>(Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;Landroid/view/View;)V

    const-wide/16 v3, 0x3e8

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 569
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Late;
    .locals 3

    .prologue
    .line 477
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Late;

    .line 478
    invoke-virtual {v0}, Late;->l()Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    move-result-object v2

    .line 479
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getEndpointMucJid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 483
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 291
    invoke-static {}, Lcwz;->a()V

    .line 292
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->e:Lapk;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->f:Latm;

    invoke-virtual {v0, v1}, Lapk;->a(Lapo;)V

    .line 295
    const/4 v0, 0x0

    invoke-direct {p0, v0, v3, v2}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->a(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;ZZ)V

    .line 297
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 298
    :try_start_0
    invoke-static {}, Lcwz;->a()V

    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->r:Z

    invoke-static {v0}, Lcwz;->b(Z)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->r:Z

    .line 300
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->e:Lapk;

    invoke-virtual {v0}, Lapk;->c()Lapx;

    .line 304
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getCurrentCall()Lcom/google/android/libraries/hangouts/video/CallState;

    move-result-object v0

    .line 305
    if-eqz v0, :cond_2

    .line 306
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->getSelf()Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    move-result-object v2

    .line 307
    if-eqz v2, :cond_0

    .line 308
    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-direct {p0, v2, v3, v4}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->a(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;ZZ)V

    .line 311
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->getRemoteEndpoints()Ljava/util/Collection;

    move-result-object v0

    .line 312
    if-eqz v0, :cond_2

    .line 313
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 314
    invoke-static {v0}, Lapx;->a(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 315
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {p0, v0, v3, v4}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->a(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;ZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 327
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 321
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Late;

    .line 322
    invoke-virtual {v0}, Late;->a()V

    goto :goto_1

    .line 325
    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->l()V

    .line 326
    invoke-static {}, Lcwz;->a()V

    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->r:Z

    invoke-static {v0}, Lcwz;->a(Z)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->r:Z

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->f()V

    .line 327
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public a(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 379
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Late;

    .line 380
    invoke-virtual {v0, p1}, Late;->a(Landroid/content/res/Configuration;)V

    goto :goto_0

    .line 382
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->n()V

    .line 383
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 666
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLeft()I

    move-result v0

    .line 670
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->v:Landroid/widget/FrameLayout;

    if-ne p1, v1, :cond_1

    .line 671
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLeft()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->u:Landroid/widget/LinearLayout;

    .line 672
    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v1, v0

    .line 673
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->v:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v0

    add-int/2addr v0, v1

    .line 679
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->s:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea$HorizontalScrollNotifier;

    invoke-virtual {v2}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea$HorizontalScrollNotifier;->getScrollX()I

    move-result v2

    .line 680
    iget-object v3, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->s:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea$HorizontalScrollNotifier;

    invoke-virtual {v3}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea$HorizontalScrollNotifier;->getWidth()I

    move-result v3

    add-int/2addr v3, v2

    .line 681
    if-le v2, v1, :cond_2

    .line 683
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->s:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea$HorizontalScrollNotifier;

    invoke-virtual {v0, v1, v4}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea$HorizontalScrollNotifier;->smoothScrollTo(II)V

    .line 689
    :cond_0
    :goto_1
    return-void

    .line 675
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v1

    add-int/2addr v1, v0

    .line 676
    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v2

    add-int/2addr v0, v2

    goto :goto_0

    .line 684
    :cond_2
    if-le v0, v3, :cond_0

    .line 686
    sub-int v1, v3, v2

    .line 687
    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->s:Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea$HorizontalScrollNotifier;

    sub-int/2addr v0, v1

    invoke-virtual {v2, v0, v4}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea$HorizontalScrollNotifier;->smoothScrollTo(II)V

    goto :goto_1
.end method

.method public a(Lasx;)V
    .locals 0

    .prologue
    .line 285
    iput-object p1, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->h:Lasx;

    .line 286
    return-void
.end method

.method public a(Late;)V
    .locals 3

    .prologue
    .line 460
    invoke-static {}, Lcwz;->a()V

    .line 461
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 462
    :try_start_0
    invoke-virtual {p1}, Late;->b()V

    .line 463
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->l:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 464
    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->l:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-ne v2, p1, :cond_1

    .line 465
    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->l:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 470
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->a(Z)V

    .line 471
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->n()V

    .line 472
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    .line 463
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 472
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 354
    invoke-static {}, Lcwz;->a()V

    .line 355
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->e:Lapk;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->f:Latm;

    invoke-virtual {v0, v1}, Lapk;->b(Lapo;)V

    .line 356
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 357
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Late;

    .line 358
    invoke-virtual {v0}, Late;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 362
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 360
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 361
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->i:Latu;

    .line 362
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public b(Late;)V
    .locals 3

    .prologue
    .line 693
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 694
    :try_start_0
    invoke-virtual {p1}, Late;->l()Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    move-result-object v0

    .line 695
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isConnecting()Z

    move-result v2

    if-nez v2, :cond_0

    .line 696
    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->g:Latn;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getEndpointMucJid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Latn;->b(Ljava/lang/String;)V

    .line 698
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->a(Z)V

    .line 699
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 395
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Late;

    .line 396
    invoke-virtual {v0}, Late;->w()V

    goto :goto_0

    .line 398
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->n()V

    .line 399
    return-void
.end method

.method public c(Late;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 756
    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->l:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-gt v2, v1, :cond_1

    .line 768
    :cond_0
    :goto_0
    return v0

    .line 760
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->l:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->i:Latu;

    if-ne v2, p1, :cond_0

    .line 764
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->e:Lapk;

    invoke-virtual {v2}, Lapk;->c()Lapx;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->e:Lapk;

    .line 765
    invoke-virtual {v2}, Lapk;->c()Lapx;

    move-result-object v2

    invoke-virtual {v2}, Lapx;->K()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    .line 768
    goto :goto_0
.end method

.method public d()Late;
    .locals 3

    .prologue
    .line 489
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Late;

    .line 490
    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->i:Latu;

    if-eq v0, v2, :cond_0

    invoke-virtual {v0}, Late;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 495
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->i:Latu;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->i:Latu;

    invoke-virtual {v0}, Latu;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->i:Latu;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()V
    .locals 2

    .prologue
    .line 539
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Late;

    .line 540
    invoke-virtual {v0}, Late;->r()V

    goto :goto_0

    .line 542
    :cond_0
    return-void
.end method

.method public f()V
    .locals 10

    .prologue
    .line 612
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->e:Lapk;

    invoke-virtual {v1}, Lapk;->d()Lcom/google/android/libraries/hangouts/video/CallState;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->e:Lapk;

    .line 617
    invoke-virtual {v1}, Lapk;->d()Lcom/google/android/libraries/hangouts/video/CallState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/CallState;->getMediaState()I

    move-result v1

    const/4 v2, 0x6

    if-ge v1, v2, :cond_1

    .line 657
    :cond_0
    :goto_0
    return-void

    .line 621
    :cond_1
    iget-object v7, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->j:Ljava/lang/Object;

    monitor-enter v7

    .line 622
    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->r:Z

    if-eqz v1, :cond_2

    .line 625
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 657
    :catchall_0
    move-exception v1

    monitor-exit v7

    throw v1

    .line 628
    :cond_2
    :try_start_1
    new-instance v8, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->l:Ljava/util/ArrayList;

    .line 629
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {v8, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 630
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_3
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Late;

    .line 631
    invoke-virtual {v1}, Late;->p()I

    move-result v2

    if-eqz v2, :cond_3

    .line 632
    invoke-virtual {v1}, Late;->x()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 636
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->getIncomingPrimaryVideoSpec()Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v2

    move-object v6, v2

    .line 641
    :goto_2
    move-object v0, v1

    check-cast v0, Latp;

    move-object v3, v0

    .line 646
    new-instance v1, Lcom/google/android/libraries/hangouts/video/VideoViewRequest;

    .line 647
    invoke-virtual {v3}, Latp;->p()I

    move-result v2

    .line 648
    invoke-virtual {v3}, Latp;->A()Lcom/google/android/libraries/hangouts/video/Renderer;

    move-result-object v3

    .line 649
    invoke-virtual {v6}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->getSize()Lcom/google/android/libraries/hangouts/video/Size;

    move-result-object v4

    iget v4, v4, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    .line 650
    invoke-virtual {v6}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->getSize()Lcom/google/android/libraries/hangouts/video/Size;

    move-result-object v5

    iget v5, v5, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    .line 651
    invoke-virtual {v6}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->getFrameRate()I

    move-result v6

    invoke-direct/range {v1 .. v6}, Lcom/google/android/libraries/hangouts/video/VideoViewRequest;-><init>(ILcom/google/android/libraries/hangouts/video/Renderer;III)V

    .line 646
    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 637
    :cond_4
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->getIncomingSecondaryVideoSpec()Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v2

    move-object v6, v2

    goto :goto_2

    .line 654
    :cond_5
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lcom/google/android/libraries/hangouts/video/VideoViewRequest;

    .line 655
    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 656
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/libraries/hangouts/video/VideoChat;->requestVideoViews([Lcom/google/android/libraries/hangouts/video/VideoViewRequest;)V

    .line 657
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public g()V
    .locals 2

    .prologue
    .line 703
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->a:Latd;

    invoke-virtual {v0}, Latd;->h()Laqv;

    move-result-object v0

    invoke-virtual {v0}, Laqv;->j()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 705
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->a:Latd;

    invoke-virtual {v0}, Latd;->h()Laqv;

    move-result-object v0

    invoke-virtual {v0}, Laqv;->d()V

    .line 707
    :cond_0
    return-void
.end method

.method public h()V
    .locals 3

    .prologue
    .line 333
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 334
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Late;

    .line 335
    invoke-virtual {v0}, Late;->h()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 337
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public i()V
    .locals 3

    .prologue
    .line 343
    invoke-static {}, Lcwz;->a()V

    .line 344
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 345
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Late;

    .line 346
    invoke-virtual {v0}, Late;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 348
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public j()V
    .locals 3

    .prologue
    .line 368
    invoke-static {}, Lcwz;->a()V

    .line 369
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 370
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Late;

    .line 371
    invoke-virtual {v0}, Late;->j()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 373
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public k()V
    .locals 2

    .prologue
    .line 388
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Late;

    .line 389
    invoke-virtual {v0}, Late;->k()V

    goto :goto_0

    .line 391
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->n()V

    .line 392
    return-void
.end method

.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 6

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 551
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->p:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iget-boolean v2, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->n:Z

    if-eqz v2, :cond_3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->m:J

    sub-long/2addr v2, v4

    long-to-float v0, v2

    iget v2, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->d:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    float-to-double v2, v0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpl-double v2, v2, v4

    if-lez v2, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->n:Z

    move v0, v1

    :cond_0
    iget-boolean v2, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->o:Z

    if-eqz v2, :cond_3

    sub-float v0, v1, v0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->w:Lcxb;

    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Late;

    invoke-virtual {v0, v1}, Late;->a(F)V

    invoke-virtual {v0, p1}, Late;->onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V

    goto :goto_2

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->w:Lcxb;

    .line 552
    return-void

    :cond_3
    move v1, v0

    goto :goto_1
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 3

    .prologue
    .line 519
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 520
    :try_start_0
    invoke-super {p0, p1, p2, p3}, Latc;->onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V

    .line 521
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Late;

    .line 522
    invoke-virtual {v0, p1, p2, p3}, Late;->onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 524
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 3

    .prologue
    .line 510
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 511
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/renderer/ParticipantTrayGLArea;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Late;

    .line 512
    invoke-virtual {v0, p1, p2}, Late;->onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 514
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
