.class public Lcom/google/android/apps/hangouts/content/EsProvider;
.super Landroid/content/ContentProvider;
.source "PG"


# static fields
.field public static final A:Landroid/net/Uri;

.field public static final B:Landroid/net/Uri;

.field public static final C:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final D:Z

.field private static final E:Ljava/lang/String;

.field private static final F:Ljava/lang/String;

.field private static final G:Ljava/lang/String;

.field private static final H:Ljava/lang/String;

.field private static final I:Landroid/net/Uri;

.field private static final J:Landroid/net/Uri;

.field private static final K:Landroid/net/Uri;

.field private static final L:Landroid/net/Uri;

.field private static final M:Landroid/content/UriMatcher;

.field private static final N:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final O:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final P:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final Q:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final R:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final S:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final T:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final U:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final V:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final W:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final X:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final Y:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final Z:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Ljava/lang/String;

.field private static final aa:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final ab:Ljava/lang/StringBuilder;

.field static final b:Ljava/lang/String;

.field public static final c:Ljava/lang/String;

.field public static final d:Landroid/net/Uri;

.field public static final e:Landroid/net/Uri;

.field public static final f:Landroid/net/Uri;

.field public static final g:Landroid/net/Uri;

.field public static final h:Landroid/net/Uri;

.field public static final i:Landroid/net/Uri;

.field public static final j:Landroid/net/Uri;

.field public static final k:Landroid/net/Uri;

.field public static final l:Landroid/net/Uri;

.field public static final m:Landroid/net/Uri;

.field public static final n:Landroid/net/Uri;

.field public static final o:Landroid/net/Uri;

.field public static final p:Landroid/net/Uri;

.field public static final q:Landroid/net/Uri;

.field public static final r:Landroid/net/Uri;

.field public static final s:Landroid/net/Uri;

.field public static final t:Landroid/net/Uri;

.field public static final u:Landroid/net/Uri;

.field public static final v:Landroid/net/Uri;

.field public static final w:Landroid/net/Uri;

.field public static final x:Landroid/net/Uri;

.field public static final y:Landroid/net/Uri;

.field public static final z:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 43
    sget-object v0, Lbys;->c:Lcyp;

    sput-boolean v6, Lcom/google/android/apps/hangouts/content/EsProvider;->D:Z

    .line 59
    :try_start_0
    const-string v0, "com.google.android.apps.hangouts.defaultbuild.EsProvider"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 60
    const-string v0, "com.google.android.apps.hangouts.content.EsProvider"
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    :goto_0
    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->a:Ljava/lang/String;

    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "content://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->E:Ljava/lang/String;

    .line 1505
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, " SELECT conversations._id as _id, "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "conversations.conversation_id"

    .line 1509
    invoke-static {v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as conversation_id, conversations.notification_level as notification_level, latest_message_timestamp as latest_message_timestamp, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "conversations.latest_message_expiration_timestamp as latest_message_expiration_timestamp, conversations.metadata_present as metadata_present, conversations.name as "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "name, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "conversations.generated_name"

    .line 1521
    invoke-static {v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as generated_name, conversations.conversation_type as conversation_type, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "conversations.transport_type"

    .line 1525
    invoke-static {v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as transport_type, conversations.default_transport_phone as default_transport_phone, conversations.sms_service_center as "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "sms_service_center, conversations.sms_thread_id as sms_thread_id, (select merge_key from merge_keys where merge_keys.conversation_id=conversations"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".conversation_id)  as merge_key, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "conversations.snippet_type"

    const-string v2, "7"

    .line 1541
    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/content/EsProvider;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as snippet_type, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "conversations.snippet_text"

    .line 1543
    invoke-static {v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1542
    const-string v2, "\"\""

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/content/EsProvider;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as snippet_text, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "conversations.snippet_voicemail_duration"

    .line 1545
    invoke-static {v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\"\""

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/content/EsProvider;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as snippet_voicemail_duration, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "conversations.snippet_image_url"

    .line 1549
    invoke-static {v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1548
    const-string v2, "\"\""

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/content/EsProvider;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as snippet_image_url, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "conversations.snippet_author_gaia_id"

    .line 1551
    const-string v2, "\"\""

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/content/EsProvider;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as snippet_author_gaia_id, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "conversations.snippet_author_chat_id"

    .line 1554
    const-string v2, "\"\""

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/content/EsProvider;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as snippet_author_chat_id, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "conversations.snippet_status"

    .line 1557
    const-string v2, "\"\""

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/content/EsProvider;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as snippet_status, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "IFNULL(author_alias.full_name, author_alias.fallback_name)"

    .line 1560
    invoke-static {v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\"\""

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/content/EsProvider;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as latest_message_author_full_name, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "IFNULL(IFNULL(author_alias.first_name, author_alias.full_name), author_alias.fallback_name)"

    .line 1564
    invoke-static {v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\"\""

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/content/EsProvider;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as latest_message_author_first_name, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "author_alias.profile_photo_url"

    .line 1570
    invoke-static {v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1569
    const-string v2, "\"\""

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/content/EsProvider;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as latest_message_author_profile_photo_url, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "conversations.latest_message_timestamp"

    .line 1572
    const-string v2, "\"\""

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/content/EsProvider;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as snippet_selector, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "conversations.snippet_participant_keys"

    .line 1576
    const-string v2, "\"\""

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/content/EsProvider;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as snippet_participant_keys, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "conversations.snippet_sms_type"

    .line 1579
    const-string v2, "\"\""

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/content/EsProvider;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as snippet_sms_type, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "conversations.latest_message_expiration_timestamp"

    .line 1582
    const-string v2, "\"\""

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/content/EsProvider;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as latest_message_expiration_timestamp, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "conversations.previous_latest_timestamp"

    .line 1585
    const-string v2, "\"\""

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/content/EsProvider;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as previous_latest_timestamp, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "conversations.snippet_new_conversation_name"

    .line 1589
    invoke-static {v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1588
    const-string v2, "\"\""

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/content/EsProvider;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as snippet_new_conversation_name, conversations.status as status, conversations.view as "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "view, conversations.inviter_gaia_id as inviter_gaia_id, conversations.inviter_chat_id as inviter_chat_id, conversations."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "inviter_affinity as inviter_affinity, conversations.disposition as disposition, conversations.is_pending_leave as is_pending_leave, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "conversations.packed_avatar_urls"

    .line 1606
    invoke-static {v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as packed_avatar_urls, conversations.self_avatar_url as self_avatar_url, conversations.self_watermark as "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "self_watermark, conversations.chat_watermark as chat_watermark, conversations.hangout_watermark as hangout_watermark, conversations."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "is_draft as is_draft, conversations.sequence_number as sequence_number, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "conversations.call_media_type"

    .line 1620
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "max(%s)"

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v1, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as call_media_type, conversations.has_joined_hangout as has_joined_hangout, conversations.last_hangout_event_time as "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "last_hangout_event_time, conversations.draft as draft, conversations.draft_subject as draft_subject, conversations."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "draft_attachment_url as draft_attachment_url, conversations.draft_photo_rotation as draft_photo_rotation, conversations.draft_picasa_id as draft_picasa_id, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "conversations.draft_content_type as draft_content_type, conversations.otr_status as otr_status, conversations.otr_toggle as "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "otr_toggle, conversations.last_otr_modification_time as last_otr_modification_time, conversations.continuation_token as continuation_token, conversations."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "continuation_event_timestamp as continuation_event_timestamp, conversations.has_oldest_message as has_oldest_message, conversations.chat_ringtone_uri as chat_ringtone_uri,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "conversations.hangout_ringtone_uri as hangout_ringtone_uri,(SELECT count() FROM messages WHERE messages.conversation_id=conversations.conversation_id AND (messages.alert_status=1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " OR messages.alert_status=3) )  as failed_message_count, max(%s)  as sort_timestamp,  %s "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as blocked, IFNULL(inviter_alias.full_name, inviter_alias.fallback_name)  as inviter_full_name, IFNULL("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "inviter_alias.first_name, inviter_alias.fallback_name)  as inviter_first_name, inviter_alias."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "profile_photo_url as inviter_profile_photo_url, inviter_alias.participant_type as inviter_type, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "conversations.self_watermark < conversations.latest_message_timestamp"

    .line 1679
    invoke-static {v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as has_unread, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "IFNULL(inviter_alias.full_name, inviter_alias.fallback_name) "

    .line 1683
    invoke-static {v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\"\""

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/content/EsProvider;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as inviter_full_name, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "IFNULL(inviter_alias.first_name, inviter_alias.fallback_name) "

    .line 1687
    invoke-static {v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\"\""

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/content/EsProvider;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as inviter_first_name, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "inviter_alias.profile_photo_url"

    .line 1692
    invoke-static {v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1691
    const-string v2, "\"\""

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/content/EsProvider;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as inviter_profile_photo_url, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "inviter_alias.participant_type"

    .line 1695
    invoke-static {v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1694
    const-string v2, "\"\""

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/content/EsProvider;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as inviter_type,  %s AS row_count,  %s AS inviter_aggregate,  %s AS invite_time_aggregate FROM %s %s  LEFT JOIN participants "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "author_alias ON (conversations.snippet_author_chat_id=author_alias.chat_id)  LEFT JOIN participants inviter_alias ON (conversations"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "inviter_chat_id=inviter_alias.chat_id) , (SELECT (julianday(\'now\') - 2440587.5) * 86400000000 AS CURRENT_TIMESTAMP) time_alias"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->F:Ljava/lang/String;

    .line 1726
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "group_concat(IFNULL(%s, %s), %s)"

    new-array v2, v9, [Ljava/lang/Object;

    const-string v3, "inviter_alias.full_name"

    aput-object v3, v2, v6

    const-string v3, "inviter_alias.fallback_name"

    aput-object v3, v2, v7

    const-string v3, "\"|\""

    aput-object v3, v2, v8

    .line 1727
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->G:Ljava/lang/String;

    .line 1731
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "group_concat(%s, %s)"

    new-array v2, v8, [Ljava/lang/Object;

    const-string v3, "conversations.sort_timestamp"

    aput-object v3, v2, v6

    const-string v3, "\"|\""

    aput-object v3, v2, v7

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->H:Ljava/lang/String;

    .line 1766
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CREATE VIEW conversations_view AS "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    sget-object v2, Lcom/google/android/apps/hangouts/content/EsProvider;->F:Ljava/lang/String;

    const/4 v3, 0x7

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "\"9223372036854775807\""

    .line 1769
    aput-object v4, v3, v6

    const-string v4, "0"

    aput-object v4, v3, v7

    const-string v4, "sum(1)"

    aput-object v4, v3, v8

    sget-object v4, Lcom/google/android/apps/hangouts/content/EsProvider;->G:Ljava/lang/String;

    aput-object v4, v3, v9

    sget-object v4, Lcom/google/android/apps/hangouts/content/EsProvider;->H:Ljava/lang/String;

    aput-object v4, v3, v10

    const/4 v4, 0x5

    const-string v5, "(select conversations.* from conversations order by sort_timestamp asc) as conversations "

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string v5, ""

    aput-object v5, v3, v4

    .line 1768
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WHERE conversations.status=1 AND inviter_affinity=1 GROUP BY conversations."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "status UNION "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    sget-object v2, Lcom/google/android/apps/hangouts/content/EsProvider;->F:Ljava/lang/String;

    const/4 v3, 0x7

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "conversations.sort_timestamp"

    aput-object v4, v3, v6

    const-string v4, "CASE WHEN conversations.conversation_type == 1 AND blocked_alias.conversation_id ==conversations.conversation_id THEN 1 ELSE 0 END"

    aput-object v4, v3, v7

    const-string v4, "1"

    aput-object v4, v3, v8

    const-string v4, "\"\""

    aput-object v4, v3, v9

    const-string v4, "\"\""

    aput-object v4, v3, v10

    const/4 v4, 0x5

    const-string v5, "conversations"

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string v5, " LEFT JOIN (SELECT DISTINCT conversation_participants.conversation_id AS conversation_id FROM conversation_participants LEFT JOIN participants ON (participants._id=participant_row_id) WHERE participants.blocked==1) AS blocked_alias ON conversations.conversation_id=blocked_alias.conversation_id"

    aput-object v5, v3, v4

    .line 1784
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WHERE conversations.status=2 GROUP BY merge_key"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->b:Ljava/lang/String;

    .line 1801
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CREATE VIEW invites_view AS "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    sget-object v2, Lcom/google/android/apps/hangouts/content/EsProvider;->F:Ljava/lang/String;

    const/4 v3, 0x7

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "conversations.sort_timestamp"

    aput-object v4, v3, v6

    const-string v4, "0"

    aput-object v4, v3, v7

    const-string v4, "1"

    aput-object v4, v3, v8

    const-string v4, "\"\""

    aput-object v4, v3, v9

    const-string v4, "\"\""

    aput-object v4, v3, v10

    const/4 v4, 0x5

    const-string v5, "conversations"

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string v5, ""

    aput-object v5, v3, v4

    .line 1803
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WHERE conversations.status=1 AND conversations.view=1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " GROUP BY conversations.conversation_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->c:Ljava/lang/String;

    .line 2127
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "conversations"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->d:Landroid/net/Uri;

    .line 2128
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "conversation"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->e:Landroid/net/Uri;

    .line 2130
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "invites_view"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->f:Landroid/net/Uri;

    .line 2132
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "suggested_contacts"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2133
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->g:Landroid/net/Uri;

    .line 2135
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message_notifications_view"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2136
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->h:Landroid/net/Uri;

    .line 2138
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "blocked_people"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->i:Landroid/net/Uri;

    .line 2140
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "dismissed_contacts"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2141
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->j:Landroid/net/Uri;

    .line 2143
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "event_suggestions/conversation"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2144
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->k:Landroid/net/Uri;

    .line 2146
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "recent_calls"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->l:Landroid/net/Uri;

    .line 2149
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "messages/conversation"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2150
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->m:Landroid/net/Uri;

    .line 2152
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "messages/conversations"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2153
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->n:Landroid/net/Uri;

    .line 2155
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "conversation_images_view/conversation"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2156
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->I:Landroid/net/Uri;

    .line 2158
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "imagescratchspace"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2159
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->J:Landroid/net/Uri;

    .line 2161
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "conversation_participants"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2162
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->K:Landroid/net/Uri;

    .line 2164
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "conversation_participants/merged_conversations"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2165
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->L:Landroid/net/Uri;

    .line 2167
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "sms_phone_numbers"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2168
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->o:Landroid/net/Uri;

    .line 2170
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->g:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/query"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2171
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->p:Landroid/net/Uri;

    .line 2173
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "realtimechat_metadata"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2174
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->q:Landroid/net/Uri;

    .line 2177
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "album_view_by_user"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2178
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->r:Landroid/net/Uri;

    .line 2179
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "album_view_by_album_and_owner"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2180
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->s:Landroid/net/Uri;

    .line 2185
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "photo_view"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->t:Landroid/net/Uri;

    .line 2186
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "photo_view_by_photo"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2187
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->u:Landroid/net/Uri;

    .line 2189
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "photo_view_by_album"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2190
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->v:Landroid/net/Uri;

    .line 2191
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "photo_view_by_null_circle"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2192
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->w:Landroid/net/Uri;

    .line 2193
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "photo_view_by_circle"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2194
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->x:Landroid/net/Uri;

    .line 2195
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "photo_view_of_user_by_user"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2196
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->y:Landroid/net/Uri;

    .line 2197
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "photo_view_by_stream_and_owner"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2198
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->z:Landroid/net/Uri;

    .line 2199
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "photo_view_by_activity"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2200
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->A:Landroid/net/Uri;

    .line 2201
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "photo_notification_count"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2202
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->B:Landroid/net/Uri;

    .line 2255
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    .line 2257
    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->M:Landroid/content/UriMatcher;

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->a:Ljava/lang/String;

    const-string v2, "conversations"

    const/16 v3, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2258
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->M:Landroid/content/UriMatcher;

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->a:Ljava/lang/String;

    const-string v2, "conversation"

    const/16 v3, 0x66

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2259
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->M:Landroid/content/UriMatcher;

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->a:Ljava/lang/String;

    const-string v2, "invites_view"

    const/16 v3, 0x65

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2261
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->M:Landroid/content/UriMatcher;

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->a:Ljava/lang/String;

    const-string v2, "conversation_participants/conversation/*"

    const/16 v3, 0x6e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2264
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->M:Landroid/content/UriMatcher;

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->a:Ljava/lang/String;

    const-string v2, "conversation_participants/merged_conversations/*"

    const/16 v3, 0x6f

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2267
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->M:Landroid/content/UriMatcher;

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->a:Ljava/lang/String;

    const-string v2, "sms_phone_numbers"

    const/16 v3, 0xdc

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2269
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->M:Landroid/content/UriMatcher;

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->a:Ljava/lang/String;

    const-string v2, "blocked_people"

    const/16 v3, 0xbe

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2271
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->M:Landroid/content/UriMatcher;

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->a:Ljava/lang/String;

    const-string v2, "dismissed_contacts"

    const/16 v3, 0xe6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2273
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->M:Landroid/content/UriMatcher;

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->a:Ljava/lang/String;

    const-string v2, "message_notifications_view"

    const/16 v3, 0xa0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2276
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->M:Landroid/content/UriMatcher;

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->a:Ljava/lang/String;

    const-string v2, "messages/conversation/*"

    const/16 v3, 0x78

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2279
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->M:Landroid/content/UriMatcher;

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->a:Ljava/lang/String;

    const-string v2, "messages/conversations/*"

    const/16 v3, 0x79

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2282
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->M:Landroid/content/UriMatcher;

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->a:Ljava/lang/String;

    const-string v2, "imagescratchspace/*"

    const/16 v3, 0x8c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2285
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->M:Landroid/content/UriMatcher;

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->a:Ljava/lang/String;

    const-string v2, "conversation_images_view/conversation/*"

    const/16 v3, 0x82

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2288
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->M:Landroid/content/UriMatcher;

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->a:Ljava/lang/String;

    const-string v2, "realtimechat_metadata/*"

    const/16 v3, 0xaa

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2290
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->M:Landroid/content/UriMatcher;

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->a:Ljava/lang/String;

    const-string v2, "suggested_contacts"

    const/16 v3, 0xb4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2291
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->M:Landroid/content/UriMatcher;

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->a:Ljava/lang/String;

    const-string v2, "suggested_contacts/query/*"

    const/16 v3, 0xb5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2293
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->M:Landroid/content/UriMatcher;

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->a:Ljava/lang/String;

    const-string v2, "suggested_contacts/query"

    const/16 v3, 0xb5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2296
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->M:Landroid/content/UriMatcher;

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->a:Ljava/lang/String;

    const-string v2, "event_suggestions/conversation/*"

    const/16 v3, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2299
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->M:Landroid/content/UriMatcher;

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->a:Ljava/lang/String;

    const-string v2, "recent_calls"

    const/16 v3, 0xd2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2320
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    .line 2321
    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->N:Ljava/util/Map;

    const-string v1, "circle_sync_time"

    const-string v2, "circle_sync_time"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2323
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->N:Ljava/util/Map;

    const-string v1, "last_sync_time"

    const-string v2, "last_sync_time"

    .line 2324
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2325
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->N:Ljava/util/Map;

    const-string v1, "people_sync_time"

    const-string v2, "people_sync_time"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2327
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->N:Ljava/util/Map;

    const-string v1, "people_last_update_token"

    const-string v2, "people_last_update_token"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2329
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->N:Ljava/util/Map;

    const-string v1, "avatars_downloaded"

    const-string v2, "avatars_downloaded"

    .line 2330
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2331
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->N:Ljava/util/Map;

    const-string v1, "audience_data"

    const-string v2, "audience_data"

    .line 2332
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2333
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->N:Ljava/util/Map;

    const-string v1, "user_gaia_id"

    const-string v2, "user_gaia_id"

    .line 2334
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2335
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->N:Ljava/util/Map;

    const-string v1, "user_chat_id"

    const-string v2, "user_chat_id"

    .line 2336
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2337
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->N:Ljava/util/Map;

    const-string v1, "contacts_sync_version"

    const-string v2, "contacts_sync_version"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2339
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->N:Ljava/util/Map;

    const-string v1, "push_notifications"

    const-string v2, "push_notifications"

    .line 2340
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2342
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    .line 2343
    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->O:Ljava/util/Map;

    const-string v1, "person_id"

    const-string v2, "person_id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2344
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->O:Ljava/util/Map;

    const-string v1, "contact_update_time"

    const-string v2, "contact_update_time"

    .line 2345
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2346
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->O:Ljava/util/Map;

    const-string v1, "contact_proto"

    const-string v2, "contact_proto"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2347
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->O:Ljava/util/Map;

    const-string v1, "profile_update_time"

    const-string v2, "profile_update_time"

    .line 2348
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2349
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->O:Ljava/util/Map;

    const-string v1, "profile_proto"

    const-string v2, "profile_proto"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2351
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    .line 2352
    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2353
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "conversation_id"

    const-string v2, "conversation_id"

    .line 2354
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2355
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "notification_level"

    const-string v2, "notification_level"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2357
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "latest_message_timestamp"

    const-string v2, "latest_message_timestamp"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2359
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "latest_message_expiration_timestamp"

    const-string v2, "latest_message_expiration_timestamp"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2361
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "metadata_present"

    const-string v2, "metadata_present"

    .line 2362
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2363
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "name"

    const-string v2, "name"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2364
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "generated_name"

    const-string v2, "generated_name"

    .line 2365
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2366
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "conversation_type"

    const-string v2, "conversation_type"

    .line 2367
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2368
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "transport_type"

    const-string v2, "transport_type"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2370
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "default_transport_phone"

    const-string v2, "default_transport_phone"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2372
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "sms_service_center"

    const-string v2, "sms_service_center"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2374
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "sms_thread_id"

    const-string v2, "sms_thread_id"

    .line 2375
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2376
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "snippet_type"

    const-string v2, "snippet_type"

    .line 2377
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2378
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "snippet_text"

    const-string v2, "snippet_text"

    .line 2379
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2380
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "snippet_image_url"

    const-string v2, "snippet_image_url"

    .line 2381
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2382
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "snippet_author_gaia_id"

    const-string v2, "snippet_author_gaia_id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2384
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "snippet_author_chat_id"

    const-string v2, "snippet_author_chat_id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2386
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "latest_message_author_full_name"

    const-string v2, "latest_message_author_full_name"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2388
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "latest_message_author_first_name"

    const-string v2, "latest_message_author_first_name"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2390
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "latest_message_author_profile_photo_url"

    const-string v2, "latest_message_author_profile_photo_url"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2392
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "snippet_status"

    const-string v2, "snippet_status"

    .line 2393
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2394
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "previous_latest_timestamp"

    const-string v2, "previous_latest_timestamp"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2396
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "snippet_new_conversation_name"

    const-string v2, "snippet_new_conversation_name"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2398
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "snippet_participant_keys"

    const-string v2, "snippet_participant_keys"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2400
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "snippet_sms_type"

    const-string v2, "snippet_sms_type"

    .line 2401
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2402
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "status"

    const-string v2, "status"

    .line 2403
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2404
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "view"

    const-string v2, "view"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2405
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "inviter_gaia_id"

    const-string v2, "inviter_gaia_id"

    .line 2406
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2407
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "inviter_chat_id"

    const-string v2, "inviter_chat_id"

    .line 2408
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2409
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "inviter_affinity"

    const-string v2, "inviter_affinity"

    .line 2410
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2411
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "inviter_full_name"

    const-string v2, "inviter_full_name"

    .line 2412
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2413
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "inviter_first_name"

    const-string v2, "inviter_first_name"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2415
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "inviter_profile_photo_url"

    const-string v2, "inviter_profile_photo_url"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2417
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "inviter_type"

    const-string v2, "inviter_type"

    .line 2418
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2419
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "is_pending_leave"

    const-string v2, "is_pending_leave"

    .line 2420
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2421
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "packed_avatar_urls"

    const-string v2, "packed_avatar_urls"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2423
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "self_avatar_url"

    const-string v2, "self_avatar_url"

    .line 2424
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2425
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "self_watermark"

    const-string v2, "self_watermark"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2427
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "is_draft"

    const-string v2, "is_draft"

    .line 2428
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2429
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "sequence_number"

    const-string v2, "sequence_number"

    .line 2430
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2431
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "call_media_type"

    const-string v2, "call_media_type"

    .line 2432
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2433
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "has_joined_hangout"

    const-string v2, "has_joined_hangout"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2435
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "last_hangout_event_time"

    const-string v2, "last_hangout_event_time"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2437
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "continuation_token"

    const-string v2, "continuation_token"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2439
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "continuation_event_timestamp"

    const-string v2, "continuation_event_timestamp"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2441
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "has_oldest_message"

    const-string v2, "has_oldest_message"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2443
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "sort_timestamp"

    const-string v2, "sort_timestamp"

    .line 2444
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2445
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "row_count"

    const-string v2, "row_count"

    .line 2446
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2447
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "inviter_aggregate"

    const-string v2, "inviter_aggregate"

    .line 2448
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2449
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "invite_time_aggregate"

    const-string v2, "invite_time_aggregate"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2451
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "draft"

    const-string v2, "draft"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2452
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "draft_subject"

    const-string v2, "draft_subject"

    .line 2453
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2454
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "draft_attachment_url"

    const-string v2, "draft_attachment_url"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2456
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "draft_photo_rotation"

    const-string v2, "draft_photo_rotation"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2458
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "draft_picasa_id"

    const-string v2, "draft_picasa_id"

    .line 2459
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2460
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "draft_content_type"

    const-string v2, "draft_content_type"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2462
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "otr_status"

    const-string v2, "otr_status"

    .line 2463
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2464
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "otr_toggle"

    const-string v2, "otr_toggle"

    .line 2465
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2466
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "last_otr_modification_time"

    const-string v2, "last_otr_modification_time"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2468
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "has_unread"

    const-string v2, "has_unread"

    .line 2469
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2470
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "failed_message_count"

    const-string v2, "failed_message_count"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2472
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "disposition"

    const-string v2, "disposition"

    .line 2473
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2474
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "blocked"

    const-string v2, "blocked"

    .line 2475
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2476
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "merge_key"

    const-string v2, "merge_key"

    .line 2477
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2478
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "snippet_selector"

    const-string v2, "snippet_selector"

    .line 2479
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2480
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "chat_ringtone_uri"

    const-string v2, "chat_ringtone_uri"

    .line 2481
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2482
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "hangout_ringtone_uri"

    const-string v2, "hangout_ringtone_uri"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2484
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    const-string v1, "snippet_voicemail_duration"

    const-string v2, "snippet_voicemail_duration"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2487
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    .line 2488
    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->Q:Ljava/util/Map;

    const-string v1, "_id"

    const-string v2, "_id"

    .line 2489
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2490
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->Q:Ljava/util/Map;

    const-string v1, "conversation_id"

    const-string v2, "conversation_id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2492
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->Q:Ljava/util/Map;

    const-string v1, "gaia_id"

    const-string v2, "gaia_id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2494
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->Q:Ljava/util/Map;

    const-string v1, "chat_id"

    const-string v2, "chat_id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2496
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->Q:Ljava/util/Map;

    const-string v1, "phone_id"

    const-string v2, "phone_id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2498
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->Q:Ljava/util/Map;

    const-string v1, "circle_id"

    const-string v2, "circle_id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2500
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->Q:Ljava/util/Map;

    const-string v1, "first_name"

    const-string v2, "first_name"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2502
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->Q:Ljava/util/Map;

    const-string v1, "full_name"

    const-string v2, "full_name"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2504
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->Q:Ljava/util/Map;

    const-string v1, "fallback_name"

    const-string v2, "fallback_name"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2506
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->Q:Ljava/util/Map;

    const-string v1, "participant_type"

    const-string v2, "participant_type"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2508
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->Q:Ljava/util/Map;

    const-string v1, "active"

    const-string v2, "active"

    .line 2509
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2510
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->Q:Ljava/util/Map;

    const-string v1, "profile_photo_url"

    const-string v2, "profile_photo_url"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2512
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->Q:Ljava/util/Map;

    const-string v1, "batch_gebi_tag"

    const-string v2, "batch_gebi_tag"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2514
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->Q:Ljava/util/Map;

    const-string v1, "sequence"

    const-string v2, "sequence"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2516
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->Q:Ljava/util/Map;

    const-string v1, "blocked"

    const-string v2, "blocked"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2519
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    .line 2520
    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->R:Ljava/util/Map;

    const-string v1, "_id"

    const-string v2, "_id"

    .line 2521
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2522
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->R:Ljava/util/Map;

    const-string v1, "gaia_id"

    const-string v2, "gaia_id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2523
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->R:Ljava/util/Map;

    const-string v1, "chat_id"

    const-string v2, "chat_id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2524
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->R:Ljava/util/Map;

    const-string v1, "name"

    const-string v2, "name"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2525
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->R:Ljava/util/Map;

    const-string v1, "profile_photo_url"

    const-string v2, "profile_photo_url"

    .line 2526
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2528
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    .line 2529
    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->S:Ljava/util/Map;

    const-string v1, "_id"

    const-string v2, "_id"

    .line 2530
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2531
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->S:Ljava/util/Map;

    const-string v1, "gaia_id"

    const-string v2, "gaia_id"

    .line 2532
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2533
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->S:Ljava/util/Map;

    const-string v1, "chat_id"

    const-string v2, "chat_id"

    .line 2534
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2535
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->S:Ljava/util/Map;

    const-string v1, "name"

    const-string v2, "name"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2536
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->S:Ljava/util/Map;

    const-string v1, "profile_photo_url"

    const-string v2, "profile_photo_url"

    .line 2537
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2539
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    .line 2540
    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->W:Ljava/util/Map;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2541
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->W:Ljava/util/Map;

    const-string v1, "gaia_id"

    const-string v2, "gaia_id"

    .line 2542
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2543
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->W:Ljava/util/Map;

    const-string v1, "chat_id"

    const-string v2, "chat_id"

    .line 2544
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2545
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->W:Ljava/util/Map;

    const-string v1, "name"

    const-string v2, "name"

    .line 2546
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2547
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->W:Ljava/util/Map;

    const-string v1, "first_name"

    const-string v2, "first_name"

    .line 2548
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2549
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->W:Ljava/util/Map;

    const-string v1, "sequence"

    const-string v2, "sequence"

    .line 2550
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2551
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->W:Ljava/util/Map;

    const-string v1, "suggestion_type"

    const-string v2, "suggestion_type"

    .line 2552
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2553
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->W:Ljava/util/Map;

    const-string v1, "profile_photo_url"

    const-string v2, "profile_photo_url"

    .line 2554
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2555
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->W:Ljava/util/Map;

    const-string v1, "packed_circle_ids"

    const-string v2, "packed_circle_ids"

    .line 2556
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2558
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    .line 2559
    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2560
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "message_id"

    const-string v2, "message_id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2561
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "conversation_id"

    const-string v2, "conversation_id"

    .line 2562
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2563
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "author_chat_id"

    const-string v2, "author_chat_id"

    .line 2564
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2565
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "author_gaia_id"

    const-string v2, "author_gaia_id"

    .line 2566
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2567
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "text"

    const-string v2, "text"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2568
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "timestamp"

    const-string v2, "timestamp"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2569
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "status"

    const-string v2, "status"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2570
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "type"

    const-string v2, "type"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2571
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "local_url"

    const-string v2, "local_url"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2572
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "remote_url"

    const-string v2, "remote_url"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2573
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "attachment_content_type"

    const-string v2, "attachment_content_type"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2575
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "width_pixels"

    const-string v2, "width_pixels"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2576
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "height_pixels"

    const-string v2, "height_pixels"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2577
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "stream_id"

    const-string v2, "stream_id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2578
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "image_id"

    const-string v2, "image_id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2579
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "album_id"

    const-string v2, "album_id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2580
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "attachment_name"

    const-string v2, "attachment_name"

    .line 2581
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2582
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "attachment_description"

    const-string v2, "attachment_description"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2584
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "latitude"

    const-string v2, "latitude"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2585
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "longitude"

    const-string v2, "longitude"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2586
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "attachment_target_url"

    const-string v2, "attachment_target_url"

    .line 2587
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2588
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "attachment_target_url_name"

    const-string v2, "attachment_target_url_name"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2590
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "attachment_target_url_description"

    const-string v2, "attachment_target_url_description"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2592
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "expiration_timestamp"

    const-string v2, "expiration_timestamp"

    .line 2593
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2594
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "alert_status"

    const-string v2, "alert_status"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2595
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "off_the_record"

    const-string v2, "off_the_record"

    .line 2596
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2597
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "external_ids"

    const-string v2, "external_ids"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2598
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "sms_message_size"

    const-string v2, "sms_message_size"

    .line 2599
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2600
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "sms_priority"

    const-string v2, "sms_priority"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2601
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "sms_timestamp_sent"

    const-string v2, "sms_timestamp_sent"

    .line 2602
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2603
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "mms_subject"

    const-string v2, "mms_subject"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2604
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "sms_raw_sender"

    const-string v2, "sms_raw_sender"

    .line 2605
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2606
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "sms_raw_recipients"

    const-string v2, "sms_raw_recipients"

    .line 2607
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2608
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "transport_type"

    const-string v2, "transport_type"

    .line 2609
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2610
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "transport_phone"

    const-string v2, "transport_phone"

    .line 2611
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2612
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "persisted"

    const-string v2, "persisted"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2613
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "sms_message_status"

    const-string v2, "sms_message_status"

    .line 2614
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2615
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "sms_type"

    const-string v2, "sms_type"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2616
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "stream_url"

    const-string v2, "stream_url"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2617
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "stream_expiration"

    const-string v2, "stream_expiration"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2619
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "voicemail_length"

    const-string v2, "voicemail_length"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2621
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "new_conversation_name"

    const-string v2, "new_conversation_name"

    .line 2622
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2623
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "participant_keys"

    const-string v2, "participant_keys"

    .line 2624
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2625
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "forwarded_mms_url"

    const-string v2, "forwarded_mms_url"

    .line 2626
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2627
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "forwarded_mms_count"

    const-string v2, "forwarded_mms_count"

    .line 2628
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2629
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "sending_error"

    const-string v2, "sending_error"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2630
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    const-string v1, "call_media_type"

    const-string v2, "call_media_type"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2633
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    .line 2634
    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->U:Ljava/util/Map;

    const-string v1, "conversation_id"

    const-string v2, "conversation_id"

    .line 2635
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2636
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->U:Ljava/util/Map;

    const-string v1, "event_id"

    const-string v2, "event_id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2637
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->U:Ljava/util/Map;

    const-string v1, "suggestion_id"

    const-string v2, "suggestion_id"

    .line 2638
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2639
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->U:Ljava/util/Map;

    const-string v1, "timestamp"

    const-string v2, "timestamp"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2640
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->U:Ljava/util/Map;

    const-string v1, "expiration_time_usec"

    const-string v2, "expiration_time_usec"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2642
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->U:Ljava/util/Map;

    const-string v1, "type"

    const-string v2, "type"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2643
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->U:Ljava/util/Map;

    const-string v1, "gem_asset_url"

    const-string v2, "gem_asset_url"

    .line 2644
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2645
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->U:Ljava/util/Map;

    const-string v1, "gem_horizontal_alignment"

    const-string v2, "gem_horizontal_alignment"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2648
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    .line 2649
    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->V:Ljava/util/Map;

    const-string v1, "uri"

    const-string v2, "uri"

    .line 2650
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2651
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->V:Ljava/util/Map;

    const-string v1, "_display_name"

    const-string v2, "_display_name"

    .line 2652
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2653
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->V:Ljava/util/Map;

    const-string v1, "contentUri"

    const-string v2, "contentUri"

    .line 2654
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2655
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->V:Ljava/util/Map;

    const-string v1, "thumbnailUri"

    const-string v2, "thumbnailUri"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2657
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->V:Ljava/util/Map;

    const-string v1, "contentType"

    const-string v2, "contentType"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2659
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->V:Ljava/util/Map;

    const-string v1, "conversation_id"

    const-string v2, "conversation_id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2661
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->V:Ljava/util/Map;

    const-string v1, "date"

    const-string v2, "date"

    .line 2662
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2663
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->V:Ljava/util/Map;

    const-string v1, "author"

    const-string v2, "author"

    .line 2664
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2665
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->V:Ljava/util/Map;

    const-string v1, "iconUri"

    const-string v2, "iconUri"

    .line 2666
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->V:Ljava/util/Map;

    const-string v1, "hashtag"

    const-string v2, "hashtag"

    .line 2668
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2669
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->V:Ljava/util/Map;

    const-string v1, "sourceName"

    const-string v2, "sourceName"

    .line 2670
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->V:Ljava/util/Map;

    const-string v1, "sourceDescription"

    const-string v2, "sourceDescription"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2673
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->V:Ljava/util/Map;

    const-string v1, "sourceUrl"

    const-string v2, "sourceUrl"

    .line 2674
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2676
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    .line 2677
    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "_id"

    const-string v2, "_id"

    .line 2678
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2679
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "message_id"

    const-string v2, "message_id"

    .line 2680
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2681
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "conversation_id"

    const-string v2, "conversation_id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2683
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "author_chat_id"

    const-string v2, "author_chat_id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2685
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "author_gaia_id"

    const-string v2, "author_gaia_id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2687
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "text"

    const-string v2, "text"

    .line 2688
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2689
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "local_url"

    const-string v2, "local_url"

    .line 2690
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2691
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "remote_url"

    const-string v2, "remote_url"

    .line 2692
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2693
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "attachment_content_type"

    const-string v2, "attachment_content_type"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2695
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "width_pixels"

    const-string v2, "width_pixels"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2697
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "height_pixels"

    const-string v2, "height_pixels"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2699
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "stream_id"

    const-string v2, "stream_id"

    .line 2700
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2701
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "image_id"

    const-string v2, "image_id"

    .line 2702
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2703
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "attachment_name"

    const-string v2, "attachment_name"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2705
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "latitude"

    const-string v2, "latitude"

    .line 2706
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2707
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "longitude"

    const-string v2, "longitude"

    .line 2708
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2709
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "attachment_target_url"

    const-string v2, "attachment_target_url"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2711
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "timestamp"

    const-string v2, "timestamp"

    .line 2712
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2713
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "status"

    const-string v2, "status"

    .line 2714
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2715
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "type"

    const-string v2, "type"

    .line 2716
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2717
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "transport_type"

    const-string v2, "transport_type"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2719
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "transport_phone"

    const-string v2, "transport_phone"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2721
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "notification_level"

    const-string v2, "notification_level"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2723
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "author_full_name"

    const-string v2, "author_full_name"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2725
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "author_first_name"

    const-string v2, "author_first_name"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2727
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "author_profile_photo_url"

    const-string v2, "author_profile_photo_url"

    .line 2728
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2730
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "conversation_notification_level"

    const-string v2, "conversation_notification_level"

    .line 2731
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2733
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "conversation_type"

    const-string v2, "conversation_type"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2735
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "conversation_name"

    const-string v2, "conversation_name"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2737
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "generated_name"

    const-string v2, "generated_name"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2739
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "conversation_status"

    const-string v2, "conversation_status"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2741
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "conversation_view"

    const-string v2, "conversation_view"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2743
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "conversation_pending_leave"

    const-string v2, "conversation_pending_leave"

    .line 2744
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2746
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "conversation_has_chat_notifications"

    const-string v2, "conversation_has_chat_notifications"

    .line 2747
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2749
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "conversation_has_video_notifications"

    const-string v2, "conversation_has_video_notifications"

    .line 2750
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2752
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "chat_watermark"

    const-string v2, "chat_watermark"

    .line 2753
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2755
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "hangout_watermark"

    const-string v2, "hangout_watermark"

    .line 2756
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2758
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "self_watermark"

    const-string v2, "self_watermark"

    .line 2759
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2761
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "new_conversation_name"

    const-string v2, "new_conversation_name"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2763
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "participant_keys"

    const-string v2, "participant_keys"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2765
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "merge_key"

    const-string v2, "merge_key"

    .line 2766
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2767
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "sms_type"

    const-string v2, "sms_type"

    .line 2768
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2769
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "chat_ringtone_uri"

    const-string v2, "chat_ringtone_uri"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2771
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "hangout_ringtone_uri"

    const-string v2, "hangout_ringtone_uri"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2773
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "otr_status"

    const-string v2, "otr_status"

    .line 2774
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2775
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "call_media_type"

    const-string v2, "call_media_type"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2777
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    const-string v1, "wearable_watermark"

    const-string v2, "wearable_watermark"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2782
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    .line 2783
    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->Y:Ljava/util/Map;

    const-string v1, "key"

    const-string v2, "key"

    .line 2784
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2785
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->Y:Ljava/util/Map;

    const-string v1, "value"

    const-string v2, "value"

    .line 2786
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2788
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    .line 2789
    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->C:Ljava/util/Map;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2790
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->C:Ljava/util/Map;

    const-string v1, "name"

    const-string v2, "name"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2791
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->C:Ljava/util/Map;

    const-string v1, "numeric"

    const-string v2, "numeric"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2792
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->C:Ljava/util/Map;

    const-string v1, "mcc"

    const-string v2, "mcc"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2793
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->C:Ljava/util/Map;

    const-string v1, "mnc"

    const-string v2, "mnc"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2794
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->C:Ljava/util/Map;

    const-string v1, "apn"

    const-string v2, "apn"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2795
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->C:Ljava/util/Map;

    const-string v1, "user"

    const-string v2, "user"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2796
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->C:Ljava/util/Map;

    const-string v1, "server"

    const-string v2, "server"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2797
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->C:Ljava/util/Map;

    const-string v1, "password"

    const-string v2, "password"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2798
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->C:Ljava/util/Map;

    const-string v1, "proxy"

    const-string v2, "proxy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2799
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->C:Ljava/util/Map;

    const-string v1, "port"

    const-string v2, "port"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2800
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->C:Ljava/util/Map;

    const-string v1, "mmsproxy"

    const-string v2, "mmsproxy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2801
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->C:Ljava/util/Map;

    const-string v1, "mmsport"

    const-string v2, "mmsport"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2802
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->C:Ljava/util/Map;

    const-string v1, "mmsc"

    const-string v2, "mmsc"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2803
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->C:Ljava/util/Map;

    const-string v1, "authtype"

    const-string v2, "authtype"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2804
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->C:Ljava/util/Map;

    const-string v1, "type"

    const-string v2, "type"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2805
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->C:Ljava/util/Map;

    const-string v1, "current"

    const-string v2, "current"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2806
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->C:Ljava/util/Map;

    const-string v1, "protocol"

    const-string v2, "protocol"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2807
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->C:Ljava/util/Map;

    const-string v1, "roaming_protocol"

    const-string v2, "roaming_protocol"

    .line 2808
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2809
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->C:Ljava/util/Map;

    const-string v1, "carrier_enabled"

    const-string v2, "carrier_enabled"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2810
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->C:Ljava/util/Map;

    const-string v1, "bearer"

    const-string v2, "bearer"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2811
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->C:Ljava/util/Map;

    const-string v1, "mvno_type"

    const-string v2, "mvno_type"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2812
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->C:Ljava/util/Map;

    const-string v1, "mvno_match_data"

    const-string v2, "mvno_match_data"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2815
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    .line 2816
    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->Z:Ljava/util/Map;

    const-string v1, "chat_id"

    const-string v2, "conversation_participants_view.chat_id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2818
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->Z:Ljava/util/Map;

    const-string v1, "latest_message_timestamp"

    const-string v2, "conversations.latest_message_timestamp"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2821
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    .line 2822
    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->aa:Ljava/util/Map;

    const-string v1, "_id"

    const-string v2, "recent_calls._id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2824
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->aa:Ljava/util/Map;

    const-string v1, "contact_id"

    const-string v2, "contact_id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2825
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->aa:Ljava/util/Map;

    const-string v1, "phone_number"

    const-string v2, "phone_number"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2827
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->aa:Ljava/util/Map;

    const-string v1, "call_timestamp"

    const-string v2, "call_timestamp"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2829
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->aa:Ljava/util/Map;

    const-string v1, "call_type"

    const-string v2, "call_type"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2830
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->aa:Ljava/util/Map;

    const-string v1, "contact_type"

    const-string v2, "contact_type"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3873
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sput-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->ab:Ljava/lang/StringBuilder;

    return-void

    .line 62
    :catch_0
    move-exception v0

    const-string v0, "com.google.android.apps.hangouts.content.EsProviderAltBuild"

    goto/16 :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 3885
    return-void
.end method

.method public static a(Ljava/lang/String;I)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 3827
    if-nez p0, :cond_1

    .line 3838
    :cond_0
    :goto_0
    return v0

    .line 3830
    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/apps/hangouts/content/EsProvider;->c(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 3831
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 3833
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private static a(Landroid/net/Uri$Builder;Lyj;)Landroid/net/Uri$Builder;
    .locals 2

    .prologue
    .line 3330
    const-string v0, "account"

    invoke-virtual {p1}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/net/Uri;Lyj;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 3337
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Landroid/net/Uri$Builder;Lyj;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lyj;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 3448
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->K:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 3449
    const-string v1, "account"

    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3450
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lyj;I)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 3322
    invoke-static {p1}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/content/EsProvider;->buildRealtimechatMetadataUri(Lyj;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lyj;J)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 3384
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->J:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 3385
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3386
    const-string v1, "account"

    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3387
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lyj;Ljava/lang/String;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 3288
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3289
    const-string v0, "Babel_db"

    const-string v1, "buildSuggestedPeopleQueryUri: query is empty"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 3290
    const/4 v0, 0x0

    .line 3297
    :goto_0
    return-object v0

    .line 3293
    :cond_0
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->p:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 3294
    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3295
    const-string v1, "limit"

    const-string v2, "10"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3296
    invoke-static {v0, p0}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Landroid/net/Uri$Builder;Lyj;)Landroid/net/Uri$Builder;

    .line 3297
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lyj;Ljava/util/ArrayList;)Landroid/net/Uri;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lyj;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/net/Uri;"
        }
    .end annotation

    .prologue
    .line 3414
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->L:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    .line 3415
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 3416
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3417
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_0

    .line 3418
    const-string v4, "+"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3420
    :cond_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 3422
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3423
    const-string v0, "account"

    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3424
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 3311
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "continuation_end_timestamp"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 3858
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "quote("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 3866
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "(\'|\' || "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " || \'|\') LIKE \'%|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "|%\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a()V
    .locals 3

    .prologue
    .line 3267
    invoke-static {}, Lbkb;->B()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3268
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    .line 3269
    if-eqz v0, :cond_0

    .line 3272
    invoke-static {v0}, Lzo;->a(Lyj;)Lzo;

    move-result-object v0

    .line 3273
    invoke-virtual {v0}, Lzo;->d()Lzr;

    move-result-object v0

    .line 3274
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v0, v2}, Lzr;->a(Ljava/util/Locale;)V

    goto :goto_0

    .line 3277
    :cond_1
    return-void
.end method

.method private static a(Ljava/lang/String;Lzw;)V
    .locals 12

    .prologue
    const/16 v11, 0x7c

    const/16 v10, 0x27

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 3919
    if-nez p0, :cond_0

    .line 3991
    :goto_0
    return-void

    .line 3925
    :cond_0
    sget-object v6, Lcom/google/android/apps/hangouts/content/EsProvider;->ab:Ljava/lang/StringBuilder;

    monitor-enter v6

    .line 3926
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v7

    .line 3931
    const/4 v0, -0x1

    move v4, v2

    move v1, v5

    .line 3934
    :goto_1
    if-ge v4, v7, :cond_3

    .line 3935
    add-int/lit8 v3, v4, 0x1

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 3936
    if-eqz v1, :cond_2

    .line 3937
    add-int/lit8 v0, v0, 0x1

    .line 3939
    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->ab:Ljava/lang/StringBuilder;

    const/4 v8, 0x0

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 3940
    if-eq v4, v10, :cond_9

    .line 3941
    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->ab:Ljava/lang/StringBuilder;

    invoke-interface {p1, v0, v1}, Lzw;->a(ILjava/lang/StringBuilder;)Z

    move v1, v3

    .line 3944
    :goto_2
    if-ge v1, v7, :cond_1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-eq v3, v11, :cond_1

    .line 3945
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3947
    :cond_1
    if-eq v1, v7, :cond_3

    .line 3948
    add-int/lit8 v3, v1, 0x1

    move v4, v3

    move v1, v5

    .line 3951
    goto :goto_1

    .line 3955
    :cond_2
    if-ne v4, v10, :cond_6

    .line 3959
    if-ne v3, v7, :cond_4

    .line 3961
    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->ab:Ljava/lang/StringBuilder;

    invoke-interface {p1, v0, v1}, Lzw;->a(ILjava/lang/StringBuilder;)Z

    .line 3991
    :cond_3
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    .line 3965
    :cond_4
    :try_start_1
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v8

    if-ne v8, v10, :cond_5

    .line 3967
    sget-object v8, Lcom/google/android/apps/hangouts/content/EsProvider;->ab:Ljava/lang/StringBuilder;

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 3968
    add-int/lit8 v3, v3, 0x1

    move v4, v3

    .line 3969
    goto :goto_1

    .line 3974
    :cond_5
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    const/16 v4, 0x7c

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v4

    invoke-static {v1, v4}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3975
    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->ab:Ljava/lang/StringBuilder;

    invoke-interface {p1, v0, v1}, Lzw;->a(ILjava/lang/StringBuilder;)Z

    .line 3976
    add-int/lit8 v3, v3, 0x1

    move v4, v3

    move v1, v5

    .line 3977
    goto :goto_1

    .line 3979
    :cond_6
    sget-object v8, Lcom/google/android/apps/hangouts/content/EsProvider;->ab:Ljava/lang/StringBuilder;

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 3980
    if-ne v3, v7, :cond_8

    .line 3981
    sget-boolean v4, Lcom/google/android/apps/hangouts/content/EsProvider;->D:Z

    if-eqz v4, :cond_7

    .line 3982
    const-string v4, "Babel_db"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "[EsProvider] splitQuotedString called with: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", which does not escape quotes."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v8}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 3986
    :cond_7
    sget-object v4, Lcom/google/android/apps/hangouts/content/EsProvider;->ab:Ljava/lang/StringBuilder;

    invoke-interface {p1, v0, v4}, Lzw;->a(ILjava/lang/StringBuilder;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_8
    :goto_3
    move v4, v3

    .line 3990
    goto/16 :goto_1

    :cond_9
    move v1, v2

    goto :goto_3
.end method

.method private static varargs a([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3148
    array-length v0, p1

    if-nez v0, :cond_0

    .line 3159
    :goto_0
    return-object p0

    .line 3151
    :cond_0
    if-nez p0, :cond_2

    move v0, v1

    .line 3152
    :goto_1
    array-length v3, p1

    .line 3154
    add-int v2, v0, v3

    new-array v2, v2, [Ljava/lang/String;

    .line 3155
    invoke-static {p1, v1, v2, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3156
    if-lez v0, :cond_1

    .line 3157
    invoke-static {p0, v1, v2, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    move-object p0, v2

    .line 3159
    goto :goto_0

    .line 3151
    :cond_2
    array-length v0, p0

    goto :goto_1
.end method

.method public static b(Ljava/lang/String;I)J
    .locals 4

    .prologue
    const-wide/16 v0, 0x0

    .line 3843
    if-nez p0, :cond_1

    .line 3854
    :cond_0
    :goto_0
    return-wide v0

    .line 3846
    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/apps/hangouts/content/EsProvider;->c(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    .line 3847
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 3849
    :try_start_0
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public static b(Lyj;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 3459
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->o:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 3460
    const-string v1, "account"

    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3461
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lyj;I)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 3495
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->l:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 3496
    const-string v1, "account"

    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3497
    if-lez p1, :cond_0

    .line 3498
    const-string v1, "limit"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3500
    :cond_0
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lyj;Ljava/lang/String;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 3346
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->m:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 3347
    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3348
    const-string v1, "account"

    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3349
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 3862
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "group_concat("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", \"|\") "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1487
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "group_concat(CASE WHEN conversations.latest_message_expiration_timestamp < time_alias.current_timestamp THEN "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WHEN "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " IS NULL THEN "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ELSE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " END, \"|\") "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 3531
    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "CREATE TABLE conversations (_id INTEGER PRIMARY KEY, conversation_id TEXT, conversation_type INT, latest_message_timestamp INT DEFAULT(0), latest_message_expiration_timestamp INT, metadata_present INT,notification_level INT, name TEXT, generated_name TEXT, snippet_type INT, snippet_text TEXT, snippet_image_url TEXT, snippet_author_gaia_id TEXT, snippet_author_chat_id TEXT, snippet_message_row_id INT, snippet_selector INT, snippet_status INT, snippet_new_conversation_name TEXT, snippet_participant_keys TEXT, snippet_sms_type TEXT, previous_latest_timestamp INT, status INT, view INT, inviter_gaia_id TEXT, inviter_chat_id TEXT, inviter_affinity INT, is_pending_leave INT, account_id INT, is_otr INT, packed_avatar_urls TEXT, self_avatar_url TEXT, self_watermark INT DEFAULT(0), chat_watermark INT DEFAULT(0), hangout_watermark INT DEFAULT(0), is_draft INT, sequence_number INT, call_media_type INT DEFAULT(0), has_joined_hangout INT, has_chat_notifications DEFAULT(0),has_video_notifications DEFAULT(0),last_hangout_event_time INT, draft TEXT, draft_subject TEXT, draft_attachment_url TEXT, draft_photo_rotation INT, draft_picasa_id TEXT, draft_content_type TEXT, otr_status INT, otr_toggle INT, last_otr_modification_time INT, continuation_token BLOB, continuation_event_timestamp INT, has_oldest_message INT DEFAULT(0), sort_timestamp INT, first_peak_scroll_time INT, first_peak_scroll_to_message_timestamp INT, second_peak_scroll_time INT, second_peak_scroll_to_message_timestamp INT, conversation_hash BLOB, disposition INT DEFAULT(0), has_persistent_events INT DEFAULT(-1), transport_type INT DEFAULT(1), default_transport_phone TEXT, sms_service_center TEXT, is_temporary INT DEFAULT (0), sms_thread_id INT DEFAULT (-1), chat_ringtone_uri TEXT, hangout_ringtone_uri TEXT, snippet_voicemail_duration INT DEFAULT (0), wearable_watermark INT DEFAULT(0), UNIQUE (conversation_id ));"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "CREATE TABLE conversation_participants (_id INTEGER PRIMARY KEY, participant_row_id INT, participant_type INT, conversation_id TEXT, sequence INT, active INT, UNIQUE (conversation_id,participant_row_id) ON CONFLICT REPLACE, FOREIGN KEY (conversation_id) REFERENCES conversations(conversation_id) ON DELETE CASCADE ON UPDATE CASCADE, FOREIGN KEY (participant_row_id) REFERENCES participants(_id));"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "CREATE TABLE participants (_id INTEGER PRIMARY KEY, participant_type INT DEFAULT 1, gaia_id TEXT, chat_id TEXT, phone_id TEXT, circle_id TEXT, first_name TEXT, full_name TEXT, fallback_name TEXT, profile_photo_url TEXT, batch_gebi_tag STRING DEFAULT(\'-1\'), blocked INT DEFAULT(0), UNIQUE (circle_id) ON CONFLICT REPLACE, UNIQUE (chat_id) ON CONFLICT REPLACE, UNIQUE (gaia_id) ON CONFLICT REPLACE);"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "CREATE TABLE suggested_contacts (_id INTEGER PRIMARY KEY, gaia_id TEXT, chat_id TEXT, name TEXT, first_name TEXT, packed_circle_ids TEXT, profile_photo_url TEXT, sequence INT, suggestion_type INT);"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "CREATE TABLE messages (_id INTEGER PRIMARY KEY, message_id TEXT, message_type INT, conversation_id TEXT, author_chat_id TEXT, author_gaia_id TEXT, text TEXT, timestamp INT, status INT, type INT, local_url TEXT, remote_url TEXT, attachment_content_type TEXT, width_pixels INT, height_pixels INT, stream_id TEXT, image_id TEXT, album_id TEXT, latitude DOUBLE, longitude DOUBLE, notification_level INT, expiration_timestamp INT, notified_for_failure INT DEFAULT(0), alert_status INT DEfAULT(0), off_the_record INT DEFAULT(0), transport_type INT NOT NULL DEFAULT(1), transport_phone TEXT, external_ids TEXT, sms_timestamp_sent INT DEFAULT(0), sms_priority INT DEFAULT(0), sms_message_size INT DEFAULT(0), mms_subject TEXT, sms_raw_sender TEXT, sms_raw_recipients TEXT, persisted INT DEFAULT(1), sms_message_status INT DEFAULT(-1), sms_type INT DEFAULT(-1), stream_url TEXT, attachment_target_url TEXT, attachment_name TEXT, image_rotation INT DEFAULT (0), new_conversation_name TEXT, participant_keys TEXT, forwarded_mms_url TEXT, forwarded_mms_count INT DEFAULT(0), attachment_description TEXT, attachment_target_url_description TEXT, attachment_target_url_name TEXT, sending_error INT DEFAULT(0), stream_expiration INT, voicemail_length INT DEFAULT (0), call_media_type INT DEFAULT(0), FOREIGN KEY (conversation_id) REFERENCES conversations(conversation_id) ON DELETE CASCADE ON UPDATE CASCADE,UNIQUE (conversation_id,message_id) ON CONFLICT REPLACE);"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "CREATE TABLE event_suggestions (_id INTEGER PRIMARY KEY, conversation_id TEXT, event_id TEXT, suggestion_id TEXT, timestamp INT, expiration_time_usec INT, type INT, gem_asset_url STRING, gem_horizontal_alignment INT, FOREIGN KEY (conversation_id) REFERENCES conversations(conversation_id) ON DELETE CASCADE ON UPDATE CASCADE, UNIQUE (conversation_id,suggestion_id) ON CONFLICT REPLACE);"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "CREATE TABLE multipart_attachments (_id INTEGER PRIMARY KEY, message_id TEXT, conversation_id TEXT, url TEXT, content_type TEXT, width INT, height INT, FOREIGN KEY (message_id, conversation_id) REFERENCES messages(message_id, conversation_id) ON DELETE CASCADE ON UPDATE CASCADE);"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "CREATE TABLE blocked_people (_id INTEGER PRIMARY KEY, gaia_id TEXT, chat_id TEXT, name TEXT, profile_photo_url TEXT, UNIQUE (chat_id) ON CONFLICT REPLACE, UNIQUE (gaia_id) ON CONFLICT REPLACE);"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "CREATE TABLE dismissed_contacts (_id INTEGER PRIMARY KEY, gaia_id TEXT, chat_id TEXT, name TEXT, profile_photo_url TEXT, UNIQUE (chat_id) ON CONFLICT REPLACE, UNIQUE (gaia_id) ON CONFLICT REPLACE);"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "CREATE TABLE realtimechat_metadata (key TEXT UNIQUE, value TEXT)"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "CREATE TABLE search (search_key TEXT NOT NULL,continuation_token TEXT,PRIMARY KEY (search_key));"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "CREATE TABLE mms_notification_inds (_id INTEGER PRIMARY KEY, content_location TEXT, transaction_id TEXT, from_address TEXT, message_size INT DEFAULT(0), expiry INT);"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "CREATE TABLE transport_events (_id INTEGER PRIMARY KEY, upload_key TEXT, local_timestamp INT, request_trace_id INT, client_generated_id INT, event_id TEXT, event_type INT, chat_action INT, message_row_id INT, notified INT, was_newest INT, past_watermark INT, dnd INT, in_focused_conversation INT, active_client_state INT, notification_level INT, event_reason INT);"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "CREATE TABLE merge_keys (_id INTEGER PRIMARY KEY, conversation_id TEXT, merge_key TEXT, UNIQUE (conversation_id) ON CONFLICT REPLACE, FOREIGN KEY (conversation_id) REFERENCES conversations(conversation_id) ON DELETE CASCADE ON UPDATE CASCADE  );"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "CREATE TABLE recent_calls (_id INTEGER PRIMARY KEY, normalized_number TEXT NOT NULL, phone_number TEXT, contact_id TEXT, call_timestamp INT, call_type INT, contact_type INT);"

    aput-object v2, v0, v1

    .line 3548
    return-object v0
.end method

.method public static buildRealtimechatMetadataUri(Lyj;Ljava/lang/String;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 3301
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->q:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 3302
    if-nez p1, :cond_0

    const-string p1, ""

    :cond_0
    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3303
    const-string v1, "account"

    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3304
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static c(Lyj;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 3470
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->i:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 3471
    const-string v1, "account"

    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3472
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static c(Lyj;Ljava/lang/String;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 3360
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->n:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 3361
    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3362
    const-string v1, "account"

    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3363
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static c(Ljava/lang/String;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 3759
    invoke-static {p0}, Lcom/google/android/apps/hangouts/content/EsProvider;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3763
    :try_start_0
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3766
    invoke-virtual {v3}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    .line 3767
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v4

    if-nez v4, :cond_0

    .line 3768
    const-string v3, "Babel_db"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[TempFileProvider] tempStoreFd: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "does not exist!"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 3785
    :goto_0
    return-object v0

    .line 3774
    :cond_0
    const-string v1, "r"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3775
    const/high16 v1, 0x10000000

    .line 3781
    :goto_1
    invoke-static {v3, v1}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 3777
    :cond_1
    const/high16 v1, 0x3c000000    # 0.0078125f

    goto :goto_1

    .line 3782
    :catch_0
    move-exception v1

    .line 3783
    const-string v3, "Babel_db"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "getTempStoreFd: error creating pfd for "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static c(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 4022
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4023
    const-string v0, ""

    .line 4035
    :goto_0
    return-object v0

    .line 4027
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x27

    if-eq v0, v1, :cond_1

    const-string v0, "NULL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4028
    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/apps/hangouts/content/EsProvider;->d(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 4031
    :cond_2
    const-string v0, "\\|"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 4032
    array-length v1, v0

    if-lt p1, v1, :cond_3

    .line 4033
    const-string v0, ""

    goto :goto_0

    .line 4035
    :cond_3
    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public static c(Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 3998
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 3999
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 4000
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x27

    if-eq v2, v3, :cond_0

    const-string v2, "NULL"

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 4001
    :cond_0
    new-instance v0, Lzu;

    invoke-direct {v0, v1}, Lzu;-><init>(Ljava/util/ArrayList;)V

    .line 4008
    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Ljava/lang/String;Lzw;)V

    .line 4015
    :cond_1
    return-object v1

    .line 4010
    :cond_2
    const-string v2, "\\|"

    invoke-virtual {p0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 4011
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4010
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static c()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 3642
    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "CREATE INDEX index_conversations_status ON conversations(status)"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "CREATE INDEX index_merge_keys_merge_key_NEW ON merge_keys(merge_key)"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "CREATE INDEX index_merge_keys_merge_conversation_id ON merge_keys(conversation_id); "

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "CREATE INDEX index_conversations_view ON conversations(view)"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "CREATE INDEX index_conversations_sort_timestamp ON conversations(sort_timestamp)"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "CREATE INDEX index_conversations_chat_notifications ON conversations(has_chat_notifications)"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "CREATE INDEX index_conversations_video_notifications ON conversations(has_video_notifications)"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "CREATE INDEX index_participants_chat_id ON participants(chat_id)"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "CREATE INDEX index_participants_blocked ON participants(blocked)"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "CREATE INDEX index_messages_conversation_id_alert_status ON messages(conversation_id, alert_status)"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "CREATE INDEX index_messages_conversation_id_timestamp ON messages(conversation_id, timestamp)"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "CREATE INDEX index_event_suggestions_conversation_id_expiration_time_timestamp  ON event_suggestions(conversation_id, expiration_time_usec, timestamp)"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "CREATE INDEX index_multipart_attachments_conversation_id_message_id ON multipart_attachments(conversation_id, message_id)"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "CREATE INDEX index_conversation_participants_sequence ON conversation_participants(sequence)"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "CREATE INDEX index_transport_events_upload_key ON transport_events(upload_key)"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "CREATE INDEX index_recent_calls_sequence ON recent_calls(call_timestamp)"

    aput-object v2, v0, v1

    .line 3660
    return-object v0
.end method

.method public static d(Lyj;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 3481
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->j:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 3482
    const-string v1, "account"

    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3483
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static d(Lyj;Ljava/lang/String;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 3372
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->I:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 3373
    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3374
    const-string v1, "account"

    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3375
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static d(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 3754
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/scratch/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".temp.jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static d(Ljava/lang/String;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 3909
    new-instance v0, Lzv;

    invoke-direct {v0, p1}, Lzv;-><init>(I)V

    .line 3910
    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Ljava/lang/String;Lzw;)V

    .line 3911
    iget-object v0, v0, Lzv;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static d()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 3667
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "conversation_participants_view"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "participants_view"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "conversations_view"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "invites_view"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "messages_view"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "message_notifications_view"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "conversation_images_view"

    aput-object v2, v0, v1

    .line 3676
    return-object v0
.end method

.method public static e(Lyj;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 3396
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->k:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 3397
    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3398
    invoke-static {v0, p0}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Landroid/net/Uri$Builder;Lyj;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static e(Lyj;)V
    .locals 3

    .prologue
    .line 3709
    sget-boolean v0, Lcom/google/android/apps/hangouts/content/EsProvider;->D:Z

    if-eqz v0, :cond_0

    .line 3710
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[EsProvider] deleteDatabase for: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lyj;->Y()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3712
    :cond_0
    invoke-static {p0}, Lzo;->a(Lyj;)Lzo;

    move-result-object v0

    invoke-virtual {v0, p0}, Lzo;->b(Lyj;)V

    .line 3713
    return-void
.end method

.method public static e()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 3683
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "CREATE VIEW conversation_participants_view AS SELECT participants._id as _id, conversation_participants.conversation_id as conversation_id, conversation_participants.sequence as sequence, conversation_participants.active as active, participants.circle_id as circle_id, participants.gaia_id as gaia_id, participants.chat_id as chat_id, participants.phone_id as phone_id, participants.fallback_name as fallback_name,  IFNULL(participants.full_name, participants.fallback_name)  as full_name,  IFNULL(IFNULL(participants.first_name, participants.full_name), participants.fallback_name)  as first_name, participants.profile_photo_url as profile_photo_url, participants.batch_gebi_tag as batch_gebi_tag, participants.participant_type as participant_type, participants.blocked as blocked  FROM conversation_participants LEFT JOIN participants ON (conversation_participants.participant_row_id=participants._id)"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "CREATE VIEW participants_view AS SELECT participants._id, participants.circle_id, participants.gaia_id, participants.chat_id, participants.phone_id,  IFNULL(participants.full_name, participants.fallback_name)  as full_name,  IFNULL(participants.first_name, participants.fallback_name)  as first_name, participants.fallback_name, participants.profile_photo_url, participants.batch_gebi_tag, participants.participant_type, participants.blocked  FROM participants"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/apps/hangouts/content/EsProvider;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/apps/hangouts/content/EsProvider;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "CREATE VIEW messages_view AS SELECT messages._id as _id, messages.message_id as message_id, messages.conversation_id as conversation_id, messages.author_chat_id as author_chat_id, messages.author_gaia_id as author_gaia_id, messages.text as text, messages.timestamp as timestamp, messages.status as status, messages.type as type, messages.local_url as local_url, messages.remote_url as remote_url, messages.attachment_content_type as attachment_content_type, messages.width_pixels as width_pixels, messages.height_pixels as height_pixels, messages.stream_id as stream_id, messages.image_id as image_id, messages.album_id as album_id,messages.attachment_name as attachment_name, messages.attachment_description as attachment_description, messages.latitude as latitude, messages.longitude as longitude,messages.attachment_target_url as attachment_target_url,messages.attachment_target_url_name as attachment_target_url_name,messages.attachment_target_url_description as attachment_target_url_description,messages.expiration_timestamp as expiration_timestamp, messages.alert_status as alert_status, messages.off_the_record as off_the_record, messages.external_ids as external_ids, messages.sms_message_size as sms_message_size, messages.sms_priority as sms_priority, messages.sms_timestamp_sent as sms_timestamp_sent, messages.mms_subject as mms_subject, messages.sms_raw_sender as sms_raw_sender, messages.sms_raw_recipients as sms_raw_recipients, messages.persisted as persisted, messages.transport_type as transport_type, messages.transport_phone as transport_phone, messages.sms_message_status as sms_message_status, messages.sms_type as sms_type, messages.stream_url as stream_url, messages.stream_expiration as stream_expiration, messages.voicemail_length as voicemail_length, messages.image_rotation as image_rotation, messages.new_conversation_name as new_conversation_name, messages.participant_keys as participant_keys, messages.forwarded_mms_url as forwarded_mms_url, messages.forwarded_mms_count as forwarded_mms_count, messages.sending_error as sending_error, messages.call_media_type as call_media_type FROM messages WHERE expiration_timestamp IS NULL OR expiration_timestamp >= (julianday(\'now\') - 2440587.5) * 86400000000"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "CREATE VIEW message_notifications_view AS SELECT messages._id as _id, messages.message_id as message_id, messages.conversation_id as conversation_id, messages.author_chat_id as author_chat_id, messages.author_gaia_id as author_gaia_id, messages.text as text, messages.local_url as local_url, messages.remote_url as remote_url, messages.attachment_content_type as attachment_content_type, messages.width_pixels as width_pixels, messages.height_pixels as height_pixels, messages.stream_id as stream_id, messages.image_id as image_id, messages.album_id as album_id, messages.attachment_name as attachment_name, messages.latitude as latitude, messages.longitude as longitude,messages.attachment_target_url as attachment_target_url,messages.timestamp as timestamp, messages.status as status, messages.type as type, messages.transport_type as transport_type, messages.transport_phone as transport_phone, messages.notification_level as notification_level, messages.notified_for_failure as notified_for_failure, messages.new_conversation_name as new_conversation_name, messages.participant_keys as participant_keys, messages.sms_type as sms_type, (select merge_key from merge_keys where merge_keys.conversation_id=messages.conversation_id)  as merge_key, author_alias.full_name as author_full_name, author_alias.first_name as author_first_name, author_alias.profile_photo_url as author_profile_photo_url, conversations.notification_level as conversation_notification_level, conversations.status as conversation_status, conversations.view as conversation_view, conversations.is_pending_leave as conversation_pending_leave, conversations.has_chat_notifications as conversation_has_chat_notifications, conversations.has_video_notifications as conversation_has_video_notifications, conversations.name as conversation_name, conversations.generated_name as generated_name, conversations.conversation_type as conversation_type, conversations.chat_watermark as chat_watermark, conversations.hangout_watermark as hangout_watermark, conversations.self_watermark as self_watermark ,conversations.chat_ringtone_uri as chat_ringtone_uri, conversations.hangout_ringtone_uri as hangout_ringtone_uri, conversations.otr_status as otr_status, conversations.call_media_type as call_media_type, conversations.wearable_watermark as wearable_watermark FROM messages LEFT JOIN conversation_participants_view author_alias ON (messages.author_chat_id=author_alias.chat_id AND messages.conversation_id=author_alias.conversation_id) LEFT JOIN conversations ON messages.conversation_id=conversations.conversation_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "CREATE VIEW conversation_images_view AS SELECT  CASE WHEN multipart_attachments.url NOT NULL THEN multipart_attachments.url WHEN messages.remote_url NOT NULL THEN messages.remote_url ELSE messages.local_url END  as uri, messages.text as _display_name,  CASE WHEN multipart_attachments.url NOT NULL THEN multipart_attachments.url WHEN messages.remote_url NOT NULL THEN messages.remote_url ELSE messages.local_url END  as contentUri,  CASE WHEN messages.remote_url NOT NULL THEN messages.remote_url ELSE messages.local_url END  as thumbnailUri, \'image/jpeg\' as contentType, messages.attachment_content_type as realContentType, messages.conversation_id as conversation_id, messages.timestamp as date, conversation_participants_view.full_name as author, conversation_participants_view.profile_photo_url as iconUri, messages.attachment_target_url as sourceUrl, messages.attachment_target_url_name as sourceName, messages.attachment_target_url_description as sourceDescription, messages.attachment_description as hashtag  FROM messages LEFT JOIN conversation_participants_view ON (messages.author_chat_id=conversation_participants_view.chat_id AND messages.conversation_id=conversation_participants_view.conversation_id) LEFT OUTER JOIN multipart_attachments USING (conversation_id, message_id)  WHERE (expiration_timestamp IS NULL OR expiration_timestamp >= (julianday(\'now\') - 2440587.5) * 86400000000) AND (remote_url NOT NULL OR local_url NOT NULL OR multipart_attachments.url NOT NULL) AND (attachment_content_type LIKE \'image/%\' OR attachment_content_type=\'multipart/mixed\')"

    aput-object v2, v0, v1

    .line 3692
    return-object v0
.end method

.method public static f(Lyj;Ljava/lang/String;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 3407
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->K:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 3408
    const-string v1, "conversation"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3409
    const-string v1, "account"

    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3410
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static f()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 3696
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "CREATE TRIGGER MESSAGE_ERROR_TRIGGER  AFTER UPDATE OF status ON messages WHEN  NEW.status!=OLD.status BEGIN  UPDATE messages SET alert_status = ( CASE  WHEN NEW.status=3 THEN 3 WHEN NEW.status=4 THEN 0 ELSE 0 END) WHERE messages.message_id=NEW.message_id; END "

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "CREATE TRIGGER CONVERSATION_MERGE_KEY_TRIGGER  AFTER INSERT ON conversations FOR EACH ROW  BEGIN  INSERT INTO merge_keys ( conversation_id, merge_key)  VALUES (NEW.conversation_id, \"CONV:\"||NEW.conversation_id); END; "

    aput-object v2, v0, v1

    .line 3700
    return-object v0
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 3205
    sget-boolean v0, Lcom/google/android/apps/hangouts/content/EsProvider;->D:Z

    if-eqz v0, :cond_0

    .line 3206
    const-string v0, "Babel_db"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "delete "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3208
    :cond_0
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->M:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 3222
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 3210
    :pswitch_0
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3211
    invoke-static {v0}, Lcom/google/android/apps/hangouts/content/EsProvider;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3213
    :try_start_0
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3214
    invoke-virtual {v3}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 3224
    :goto_0
    return v0

    .line 3216
    :catch_0
    move-exception v1

    .line 3217
    const-string v2, "Babel_db"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "delete: error deleting "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 3224
    const/4 v0, 0x0

    goto :goto_0

    .line 3208
    nop

    :pswitch_data_0
    .packed-switch 0x8c
        :pswitch_0
    .end packed-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 3232
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->M:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 3257
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown URI: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3234
    :sswitch_0
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.hangouts.conversations"

    .line 3254
    :goto_0
    return-object v0

    .line 3238
    :sswitch_1
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.hangouts.participants"

    goto :goto_0

    .line 3242
    :sswitch_2
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.hangouts.blocked_people"

    goto :goto_0

    .line 3246
    :sswitch_3
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.hangouts.dismissed_contacts"

    goto :goto_0

    .line 3250
    :sswitch_4
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.hangouts.messages"

    goto :goto_0

    .line 3254
    :sswitch_5
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.hangouts.message_notifications"

    goto :goto_0

    .line 3232
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0x6e -> :sswitch_1
        0x78 -> :sswitch_4
        0xa0 -> :sswitch_5
        0xbe -> :sswitch_2
        0xe6 -> :sswitch_3
    .end sparse-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 3167
    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3168
    if-nez v1, :cond_0

    .line 3169
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Every URI must have the \'account\' parameter specified.\nURI: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3174
    :cond_0
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->M:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 3188
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Insert not supported "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3176
    :pswitch_0
    invoke-static {v1}, Lzo;->a(Ljava/lang/String;)Lzo;

    move-result-object v0

    .line 3178
    invoke-virtual {v0}, Lzo;->d()Lzr;

    move-result-object v0

    .line 3179
    const-string v2, "realtimechat_metadata"

    invoke-virtual {v0, v2, p2}, Lzr;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3181
    sget-object v0, Lcom/google/android/apps/hangouts/content/EsProvider;->q:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    .line 3182
    const-string v0, "key"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3183
    if-nez v0, :cond_1

    const-string v0, ""

    :cond_1
    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3184
    const-string v0, "account"

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3185
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0

    .line 3174
    nop

    :pswitch_data_0
    .packed-switch 0xaa
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 2839
    const/4 v0, 0x1

    return v0
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 3

    .prologue
    .line 3790
    const-string v0, "Babel_db"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3791
    const-string v0, "Babel_db"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "openFile "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 3793
    :cond_0
    const/4 v0, 0x0

    .line 3794
    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->M:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    .line 3795
    packed-switch v1, :pswitch_data_0

    .line 3801
    :goto_0
    return-object v0

    .line 3797
    :pswitch_0
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3798
    invoke-static {v0, p2}, Lcom/google/android/apps/hangouts/content/EsProvider;->c(Ljava/lang/String;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    goto :goto_0

    .line 3795
    :pswitch_data_0
    .packed-switch 0x8c
        :pswitch_0
    .end packed-switch
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 17

    .prologue
    .line 2848
    const-string v1, "account"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 2849
    if-nez v9, :cond_0

    .line 2850
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Every URI must have the \'account\' parameter specified.\nURI: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2855
    :cond_0
    const-string v3, ""

    .line 2856
    const-string v1, "limit"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2860
    new-instance v1, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v1}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 2862
    sget-object v2, Lcom/google/android/apps/hangouts/content/EsProvider;->M:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 3077
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown URI "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2864
    :sswitch_0
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    .line 2865
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x2

    if-gt v3, v4, :cond_3

    const-string v2, ""

    .line 2866
    :goto_0
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v2, "Babel_db"

    const-string v3, "prepareSuggestedPeopleSearchQuery: query is empty"

    invoke-static {v2, v3}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 2867
    :goto_1
    sget-object v2, Lcom/google/android/apps/hangouts/content/EsProvider;->W:Ljava/util/Map;

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 2870
    const-string v2, "UPPER(name)"

    move-object/from16 v4, p4

    .line 3082
    :goto_2
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    move-object/from16 v14, p5

    .line 3087
    :goto_3
    :try_start_0
    const-string v2, "EsProvider query"

    invoke-static {v2}, Lbzq;->a(Ljava/lang/String;)V

    .line 3088
    sget-boolean v2, Lcom/google/android/apps/hangouts/content/EsProvider;->D:Z

    if-eqz v2, :cond_1

    .line 3089
    const-string v10, "Babel_db"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[EsProvider] URI:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", match: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/hangouts/content/EsProvider;->M:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", QUERY: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v7, p5

    .line 3090
    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQuery([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 3089
    invoke-static {v10, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 3095
    :cond_1
    invoke-static {v9}, Lzo;->a(Ljava/lang/String;)Lzo;

    move-result-object v2

    .line 3096
    invoke-virtual {v2}, Lzo;->c()Lzr;

    move-result-object v10

    move-object v9, v1

    move-object/from16 v11, p2

    move-object/from16 v12, p3

    move-object v13, v4

    move-object v15, v8

    .line 3094
    invoke-static/range {v9 .. v15}, Lzr;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Lzr;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 3100
    sget-boolean v2, Lcom/google/android/apps/hangouts/content/EsProvider;->D:Z

    if-eqz v2, :cond_2

    .line 3101
    const-string v2, "Babel_db"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[EsProvider] QUERY results: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 3104
    :cond_2
    invoke-static {}, Lzo;->a()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-interface {v1, v2, v0}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3107
    invoke-static {}, Lbzq;->a()V

    :goto_4
    return-object v1

    .line 2865
    :cond_3
    const/4 v3, 0x2

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 2866
    :cond_4
    const-string v3, "suggested_contacts"

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v3, "("

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "% "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "%"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "name LIKE "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v3, " OR "

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "name LIKE "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v2, ")"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 2875
    :sswitch_1
    const-string v2, "conversations_view"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 2876
    sget-object v2, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    move-object/from16 v4, p4

    move-object/from16 v2, p5

    .line 2878
    goto/16 :goto_2

    .line 2882
    :sswitch_2
    const-string v2, "conversations"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    move-object/from16 v4, p4

    move-object/from16 v2, p5

    .line 2884
    goto/16 :goto_2

    .line 2888
    :sswitch_3
    const-string v2, "invites_view"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 2889
    sget-object v2, Lcom/google/android/apps/hangouts/content/EsProvider;->P:Ljava/util/Map;

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    move-object/from16 v4, p4

    move-object/from16 v2, p5

    .line 2891
    goto/16 :goto_2

    .line 2895
    :sswitch_4
    const-string v2, "suggested_contacts"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 2896
    sget-object v2, Lcom/google/android/apps/hangouts/content/EsProvider;->W:Ljava/util/Map;

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    move-object/from16 v4, p4

    move-object/from16 v2, p5

    .line 2898
    goto/16 :goto_2

    .line 2902
    :sswitch_5
    const-string v2, "conversation_participants_view JOIN conversations ON (conversation_participants_view.conversation_id=conversations.conversation_id)"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 2910
    const-string v2, "conversation_type"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 2911
    const-string v2, "=1"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 2912
    const-string v2, " AND "

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 2913
    const-string v2, "participant_type"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 2914
    const-string v2, "=3"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 2915
    const-string v2, " AND "

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 2916
    const-string v2, "latest_message_timestamp"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 2917
    const-string v2, "!= 0"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 2918
    sget-object v2, Lcom/google/android/apps/hangouts/content/EsProvider;->Z:Ljava/util/Map;

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    move-object/from16 v4, p4

    move-object/from16 v2, p5

    .line 2920
    goto/16 :goto_2

    .line 2924
    :sswitch_6
    const-string v2, "conversation_participants_view"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 2925
    const-string v2, "conversation_id"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 2926
    const-string v2, "=?"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 2927
    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    const/4 v5, 0x2

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    aput-object v2, v3, v4

    move-object/from16 v0, p4

    invoke-static {v0, v3}, Lcom/google/android/apps/hangouts/content/EsProvider;->a([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p4

    .line 2928
    sget-object v2, Lcom/google/android/apps/hangouts/content/EsProvider;->Q:Ljava/util/Map;

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    move-object/from16 v4, p4

    move-object/from16 v2, p5

    .line 2930
    goto/16 :goto_2

    .line 2934
    :sswitch_7
    const-string v2, "conversation_participants_view"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 2935
    const-string v2, "conversation_id"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 2936
    const-string v2, " in ("

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 2938
    const/4 v3, 0x1

    .line 2939
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    const/4 v4, 0x2

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v4, "\\+"

    invoke-static {v2, v4}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4

    const/4 v2, 0x0

    move/from16 v16, v2

    move v2, v3

    move/from16 v3, v16

    :goto_5
    if-ge v3, v5, :cond_6

    aget-object v6, v4, v3

    .line 2940
    if-eqz v2, :cond_5

    .line 2941
    const/4 v2, 0x0

    .line 2945
    :goto_6
    const-string v7, "\'"

    invoke-virtual {v1, v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 2946
    invoke-virtual {v1, v6}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 2947
    const-string v6, "\'"

    invoke-virtual {v1, v6}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 2939
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 2943
    :cond_5
    const-string v7, ","

    invoke-virtual {v1, v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto :goto_6

    .line 2949
    :cond_6
    const-string v2, ")"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 2951
    sget-object v2, Lcom/google/android/apps/hangouts/content/EsProvider;->Q:Ljava/util/Map;

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    move-object/from16 v4, p4

    move-object/from16 v2, p5

    .line 2953
    goto/16 :goto_2

    .line 2957
    :sswitch_8
    const-string v2, "blocked_people"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 2958
    sget-object v2, Lcom/google/android/apps/hangouts/content/EsProvider;->R:Ljava/util/Map;

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    move-object/from16 v4, p4

    move-object/from16 v2, p5

    .line 2960
    goto/16 :goto_2

    .line 2964
    :sswitch_9
    const-string v2, "dismissed_contacts"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 2965
    sget-object v2, Lcom/google/android/apps/hangouts/content/EsProvider;->S:Ljava/util/Map;

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    move-object/from16 v4, p4

    move-object/from16 v2, p5

    .line 2967
    goto/16 :goto_2

    .line 2971
    :sswitch_a
    const-string v2, "message_notifications_view"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 2972
    sget-object v2, Lcom/google/android/apps/hangouts/content/EsProvider;->X:Ljava/util/Map;

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    move-object/from16 v4, p4

    move-object/from16 v2, p5

    .line 2974
    goto/16 :goto_2

    .line 2978
    :sswitch_b
    const-string v2, "messages_view"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 2979
    const-string v2, "status"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 2980
    const-string v2, " != 5 AND "

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 2981
    const-string v2, "conversation_id"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 2982
    const-string v2, "=?"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 2983
    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    const/4 v5, 0x2

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    aput-object v2, v3, v4

    move-object/from16 v0, p4

    invoke-static {v0, v3}, Lcom/google/android/apps/hangouts/content/EsProvider;->a([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p4

    .line 2984
    sget-object v2, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    move-object/from16 v4, p4

    move-object/from16 v2, p5

    .line 2986
    goto/16 :goto_2

    .line 2990
    :sswitch_c
    const-string v2, "event_suggestions"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 2991
    const-string v2, "conversation_id"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 2992
    const-string v2, "=? AND ("

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 2993
    const-string v2, "expiration_time_usec"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 2994
    const-string v2, "<= 0 OR "

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 2995
    const-string v2, "expiration_time_usec"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 2996
    const-string v2, "> (julianday(\'now\') - 2440587.5) * 86400000000)"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 2997
    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    const/4 v6, 0x2

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    aput-object v2, v4, v5

    move-object/from16 v0, p4

    invoke-static {v0, v4}, Lcom/google/android/apps/hangouts/content/EsProvider;->a([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p4

    .line 2998
    sget-object v2, Lcom/google/android/apps/hangouts/content/EsProvider;->U:Ljava/util/Map;

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    move-object/from16 v4, p4

    move-object v2, v3

    .line 2999
    goto/16 :goto_2

    .line 3003
    :sswitch_d
    const-string v2, "messages_view"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3004
    const-string v2, "status != 5 AND conversation_id IN (SELECT conversation_id FROM conversations WHERE conversation_id=? OR conversation_id IN (SELECT conversation_id FROM merge_keys WHERE merge_key IN (SELECT merge_key FROM merge_keys WHERE conversation_id=?)))"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3015
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 3016
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    const/4 v4, 0x1

    aput-object v2, v3, v4

    move-object/from16 v0, p4

    invoke-static {v0, v3}, Lcom/google/android/apps/hangouts/content/EsProvider;->a([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p4

    .line 3018
    sget-object v2, Lcom/google/android/apps/hangouts/content/EsProvider;->T:Ljava/util/Map;

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    move-object/from16 v4, p4

    move-object/from16 v2, p5

    .line 3021
    goto/16 :goto_2

    .line 3026
    :sswitch_e
    const-string v2, "conversation_images_view"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3028
    const-string v2, "conversation_id IN (SELECT conversation_id FROM merge_keys WHERE merge_key=( SELECT merge_key FROM merge_keys WHERE conversation_id=?))"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3045
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->q()Z

    move-result v2

    if-nez v2, :cond_7

    .line 3047
    const-string v2, " AND "

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3048
    const-string v2, "realContentType"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3049
    const-string v2, " != \'image/gif\'"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3051
    :cond_7
    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    const/4 v5, 0x2

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    aput-object v2, v3, v4

    move-object/from16 v0, p4

    invoke-static {v0, v3}, Lcom/google/android/apps/hangouts/content/EsProvider;->a([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p4

    .line 3052
    sget-object v2, Lcom/google/android/apps/hangouts/content/EsProvider;->V:Ljava/util/Map;

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 3053
    const-string v2, "date ASC"

    move-object/from16 v4, p4

    .line 3054
    goto/16 :goto_2

    .line 3058
    :sswitch_f
    const-string v2, "realtimechat_metadata"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3059
    const-string v2, "key"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3060
    const-string v2, "=?"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3061
    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    const/4 v6, 0x1

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    aput-object v2, v4, v5

    move-object/from16 v0, p4

    invoke-static {v0, v4}, Lcom/google/android/apps/hangouts/content/EsProvider;->a([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p4

    .line 3062
    sget-object v2, Lcom/google/android/apps/hangouts/content/EsProvider;->Y:Ljava/util/Map;

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    move-object/from16 v4, p4

    move-object v2, v3

    .line 3063
    goto/16 :goto_2

    .line 3067
    :sswitch_10
    const-string v2, "recent_calls"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3068
    sget-object v2, Lcom/google/android/apps/hangouts/content/EsProvider;->aa:Ljava/util/Map;

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    move-object/from16 v4, p4

    move-object/from16 v2, p5

    .line 3070
    goto/16 :goto_2

    .line 3074
    :sswitch_11
    const/4 v1, 0x0

    goto/16 :goto_4

    .line 3107
    :catchall_0
    move-exception v1

    invoke-static {}, Lbzq;->a()V

    throw v1

    :cond_8
    move-object v14, v2

    goto/16 :goto_3

    .line 2862
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_1
        0x65 -> :sswitch_3
        0x66 -> :sswitch_2
        0x6e -> :sswitch_6
        0x6f -> :sswitch_7
        0x78 -> :sswitch_b
        0x79 -> :sswitch_d
        0x82 -> :sswitch_e
        0x8c -> :sswitch_11
        0xa0 -> :sswitch_a
        0xaa -> :sswitch_f
        0xb4 -> :sswitch_4
        0xb5 -> :sswitch_0
        0xbe -> :sswitch_8
        0xc8 -> :sswitch_c
        0xd2 -> :sswitch_10
        0xdc -> :sswitch_5
        0xe6 -> :sswitch_9
    .end sparse-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 3197
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Update not supported: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
