.class public Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;
.super Lakl;
.source "PG"

# interfaces
.implements Labf;
.implements Landroid/view/View$OnClickListener;
.implements Law;
.implements Lcfv;
.implements Lcfw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lakl;",
        "Labf;",
        "Landroid/view/View$OnClickListener;",
        "Law",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcfv;",
        "Lcfw;"
    }
.end annotation


# instance fields
.field private Y:I

.field private Z:I

.field public a:Ljava/lang/String;

.field private aA:Lbme;

.field private final aB:Lbsb;

.field private final aC:Lcvr;

.field private aa:Ljava/lang/String;

.field private ab:I

.field private ac:Lcvi;

.field private ad:Lcvo;

.field private final ae:Lyc;

.field private af:J

.field private ag:Z

.field private ah:Laja;

.field private ai:Laja;

.field private aj:Laja;

.field private ak:Laja;

.field private al:Laja;

.field private am:Laja;

.field private an:Lbdh;

.field private ao:Z

.field private ap:Z

.field private aq:Z

.field private ar:Ljava/lang/String;

.field private as:Ljava/lang/String;

.field private at:Lbdh;

.field private au:Ljava/lang/String;

.field private av:Ljava/lang/String;

.field private aw:Lbxq;

.field private ax:Ljava/lang/String;

.field private ay:Z

.field private az:Landroid/app/Activity;

.field private b:Ljava/lang/String;

.field private c:[Ljava/lang/String;

.field private d:Lyj;

.field private e:Landroid/widget/ListView;

.field private f:Ladx;

.field private g:Lajc;

.field private h:Laiz;

.field private i:Laiz;

.field public mCircleIdsMapping:Ljava/util/Map;
    .annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field public mMyCircles:Ljava/util/Map;
    .annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 99
    invoke-direct {p0}, Lakl;-><init>()V

    .line 135
    new-instance v0, Lyc;

    invoke-direct {v0}, Lyc;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ae:Lyc;

    .line 176
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ay:Z

    .line 231
    new-instance v0, Laix;

    invoke-direct {v0, p0}, Laix;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->aB:Lbsb;

    .line 1294
    new-instance v0, Laiy;

    invoke-direct {v0, p0}, Laiy;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->aC:Lcvr;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;)Ladx;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->f:Ladx;

    return-object v0
.end method

.method private a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1392
    if-eqz p1, :cond_0

    .line 1393
    invoke-static {p1}, Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;->b(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 1394
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->getActivity()Ly;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v0

    .line 1395
    if-eqz v0, :cond_0

    .line 1396
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->getActivity()Ly;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/Ringtone;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1399
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->li:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ax:Ljava/lang/String;

    return-object p1
.end method

.method private static a(Ljava/lang/String;Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1460
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1461
    const/4 p0, 0x0

    .line 1463
    :cond_0
    :goto_0
    return-object p0

    :cond_1
    if-nez p0, :cond_0

    const-string p0, ""

    goto :goto_0
.end method

.method private static a(Laja;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1384
    iget-object v0, p0, Laja;->h:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1385
    iput-object p1, p0, Laja;->b:Ljava/lang/String;

    .line 1386
    iget-object v0, p0, Laja;->h:Landroid/view/View;

    sget v1, Lg;->hi:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1387
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1389
    :cond_0
    return-void
.end method

.method public static synthetic a(Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 99
    invoke-virtual {p0, p1}, Landroid/view/View;->setAlpha(F)V

    return-void
.end method

.method public static synthetic a(Landroid/widget/TextView;I)V
    .locals 3

    .prologue
    .line 99
    invoke-virtual {p0}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v0

    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    move-result v1

    invoke-static {v0}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {v0}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    invoke-static {p1, v1, v2, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method private a(Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;II)V
    .locals 1

    .prologue
    .line 690
    invoke-static {p1, p2, p3, p5}, Lbbl;->a(Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;I)Landroid/content/Intent;

    move-result-object v0

    .line 692
    invoke-virtual {p0, v0, p4}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 693
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;)Lbxq;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->aw:Lbxq;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;)Lyj;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->d:Lyj;

    return-object v0
.end method

.method private c()Z
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 1132
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ae:Lyc;

    invoke-virtual {v1}, Lyc;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1175
    :cond_0
    :goto_0
    return v0

    .line 1138
    :cond_1
    iget v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->Y:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    .line 1139
    sget v0, Lh;->jx:I

    .line 1140
    const/4 v4, 0x4

    .line 1141
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ae:Lyc;

    invoke-virtual {v1}, Lyc;->e()Ljava/util/ArrayList;

    move-result-object v3

    move v1, v0

    .line 1158
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->getActivity()Ly;

    move-result-object v7

    .line 1159
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->d:Lyj;

    .line 1161
    invoke-virtual {v7, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->b:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->Z:I

    .line 1159
    invoke-static/range {v0 .. v5}, Lbbl;->a(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;II)Landroid/content/Intent;

    move-result-object v1

    .line 1168
    invoke-virtual {v7}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "share_intent"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 1169
    if-eqz v0, :cond_2

    .line 1170
    const-string v2, "conversation_id"

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1171
    const-string v2, "share_intent"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1173
    :cond_2
    const/16 v0, 0x65

    invoke-virtual {v7, v1, v0}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1174
    iput-boolean v6, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ay:Z

    move v0, v6

    .line 1175
    goto :goto_0

    .line 1143
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->at:Lbdh;

    if-eqz v1, :cond_0

    .line 1146
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->Z:I

    invoke-static {v0}, Lf;->c(I)Z

    move-result v0

    if-eqz v0, :cond_4

    sget v0, Lh;->hr:I

    .line 1148
    :goto_2
    const/4 v4, 0x3

    .line 1154
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1155
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->at:Lbdh;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v1, v0

    goto :goto_1

    .line 1146
    :cond_4
    sget v0, Lh;->hq:I

    goto :goto_2
.end method

.method public static synthetic d(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;)Lbdh;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->an:Lbdh;

    return-object v0
.end method

.method private d()V
    .locals 6

    .prologue
    .line 1317
    new-instance v1, Lcvm;

    invoke-direct {v1}, Lcvm;-><init>()V

    .line 1318
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 1319
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ae:Lyc;

    invoke-virtual {v0}, Lyc;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    .line 1320
    iget-object v4, v0, Lbdh;->b:Lbdk;

    if-eqz v4, :cond_0

    .line 1321
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "g:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lbdh;->b:Lbdk;

    iget-object v0, v0, Lbdk;->a:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1324
    :cond_1
    invoke-virtual {v1, v2}, Lcvm;->a(Ljava/util/Collection;)Lcvm;

    .line 1325
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ac:Lcvi;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->aC:Lcvr;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->d:Lyj;

    invoke-virtual {v3}, Lyj;->ak()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->d:Lyj;

    .line 1326
    invoke-virtual {v4}, Lyj;->al()Ljava/lang/String;

    move-result-object v4

    .line 1325
    invoke-virtual {v0, v2, v3, v4, v1}, Lcvi;->a(Lcvr;Ljava/lang/String;Ljava/lang/String;Lcvm;)V

    .line 1327
    new-instance v0, Lcvo;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ac:Lcvi;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->d:Lyj;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->aB:Lbsb;

    invoke-direct {v0, v1, v2, v3}, Lcvo;-><init>(Lcvi;Lyj;Lbsb;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ad:Lcvo;

    .line 1328
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ad:Lcvo;

    invoke-virtual {v0}, Lcvo;->a()V

    .line 1329
    return-void
.end method

.method private e()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1403
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->au:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1404
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->au:Ljava/lang/String;

    invoke-static {v0}, Lf;->p(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1406
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->b()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public static synthetic e(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;)Z
    .locals 1

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ay:Z

    return v0
.end method

.method private f()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1416
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->d:Lyj;

    invoke-static {v0}, Lf;->b(Lyj;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lf;->p(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;)Z
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ay:Z

    return v0
.end method

.method private q()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1420
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->av:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1421
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->av:Ljava/lang/String;

    invoke-static {v0}, Lf;->p(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1423
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->f()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Intent;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1379
    const-string v0, "android.intent.extra.ringtone.PICKED_URI"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    .line 1380
    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 646
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->an:Lbdh;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->an:Lbdh;

    iget-object v0, v0, Lbdh;->e:Ljava/lang/String;

    .line 648
    :goto_0
    iget v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->Y:I

    if-ne v2, v3, :cond_0

    .line 649
    iget-boolean v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ap:Z

    if-eqz v1, :cond_2

    .line 650
    sget v1, Lh;->jc:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 657
    :cond_0
    :goto_1
    sget v2, Lh;->jg:I

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    .line 658
    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget v2, Lh;->je:I

    .line 659
    invoke-virtual {p0, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v3, Lh;->ab:I

    .line 660
    invoke-virtual {p0, v3}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 657
    invoke-static {v0, v1, v2, v3}, Labe;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Labe;

    move-result-object v0

    .line 661
    invoke-virtual {v0, p0, v4}, Labe;->setTargetFragment(Lt;I)V

    .line 662
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->getFragmentManager()Lae;

    move-result-object v1

    const-string v2, "block_user"

    invoke-virtual {v0, v1, v2}, Labe;->a(Lae;Ljava/lang/String;)V

    .line 663
    return-void

    :cond_1
    move-object v0, v1

    .line 646
    goto :goto_0

    .line 651
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->aq:Z

    if-eqz v1, :cond_3

    .line 652
    sget v1, Lh;->jd:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 654
    :cond_3
    sget v1, Lh;->jf:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 595
    const-string v0, "block_user"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 596
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->Y:I

    if-ne v0, v4, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->an:Lbdh;

    if-eqz v0, :cond_4

    .line 599
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->d:Lyj;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->c:[Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;[Ljava/lang/String;)V

    .line 600
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->c:[Ljava/lang/String;

    array-length v0, v0

    new-array v2, v0, [J

    move v0, v1

    .line 601
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->c:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 602
    iget-wide v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->af:J

    aput-wide v5, v2, v0

    .line 601
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 604
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->d:Lyj;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->c:[Ljava/lang/String;

    invoke-static {v0, v3, v2, v4, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;[Ljava/lang/String;[JZZ)I

    .line 608
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ae:Lyc;

    invoke-virtual {v0}, Lyc;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lbdh;

    .line 609
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->an:Lbdh;

    if-eq v3, v0, :cond_1

    .line 610
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->d:Lyj;

    iget-object v1, v3, Lbdh;->b:Lbdk;

    iget-object v1, v1, Lbdk;->a:Ljava/lang/String;

    iget-object v2, v3, Lbdh;->b:Lbdk;

    iget-object v2, v2, Lbdk;->b:Ljava/lang/String;

    iget-object v3, v3, Lbdh;->e:Ljava/lang/String;

    move v5, v4

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)I

    goto :goto_1

    .line 620
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->getActivity()Ly;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->d:Lyj;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->an:Lbdh;

    iget-object v2, v2, Lbdh;->e:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->an:Lbdh;

    iget-object v3, v3, Lbdh;->b:Lbdk;

    iget-object v3, v3, Lbdk;->a:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->an:Lbdh;

    iget-object v4, v4, Lbdh;->b:Lbdk;

    iget-object v4, v4, Lbdk;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lf;->a(Ly;Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 628
    :cond_3
    :goto_2
    return-void

    .line 625
    :cond_4
    const-string v0, "Babel"

    const-string v1, "no participants found when trying to block"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public a(Ldg;Landroid/database/Cursor;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 1191
    invoke-virtual {p1}, Ldg;->l()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1251
    :goto_0
    return-void

    .line 1193
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ac:Lcvi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ac:Lcvi;

    invoke-virtual {v0}, Lcvi;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1194
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->d()V

    .line 1197
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ae:Lyc;

    sget v1, Lyd;->b:I

    invoke-virtual {v0, p2, v1}, Lyc;->a(Landroid/database/Cursor;I)I

    .line 1198
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->Y:I

    if-ne v0, v3, :cond_8

    .line 1200
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ae:Lyc;

    invoke-virtual {v0}, Lyc;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    .line 1201
    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v6, v0, Lbdh;->i:Ljava/lang/Boolean;

    invoke-virtual {v5, v6}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1202
    iput-boolean v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ao:Z

    .line 1204
    :cond_2
    iget-object v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->an:Lbdh;

    if-eqz v5, :cond_3

    iget-object v5, v0, Lbdh;->c:Ljava/lang/String;

    .line 1205
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1207
    :cond_3
    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->an:Lbdh;

    .line 1208
    iget-object v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->an:Lbdh;

    iget-object v5, v5, Lbdh;->e:Ljava/lang/String;

    if-nez v5, :cond_4

    .line 1212
    iget-object v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->an:Lbdh;

    const-string v6, ""

    iput-object v6, v5, Lbdh;->e:Ljava/lang/String;

    .line 1215
    :cond_4
    add-int/lit8 v1, v1, 0x1

    .line 1216
    iget-object v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->at:Lbdh;

    if-nez v5, :cond_1

    .line 1217
    iget v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->Z:I

    invoke-static {v5}, Lf;->c(I)Z

    move-result v5

    if-eqz v5, :cond_5

    iget-object v6, v0, Lbdh;->b:Lbdk;

    iget-object v6, v6, Lbdk;->b:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ar:Ljava/lang/String;

    invoke-static {v6, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_6

    :cond_5
    if-nez v5, :cond_1

    iget-object v5, v0, Lbdh;->b:Lbdk;

    iget-object v5, v5, Lbdk;->a:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->as:Ljava/lang/String;

    invoke-static {v5, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_6
    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->at:Lbdh;

    goto :goto_1

    .line 1220
    :cond_7
    if-le v1, v3, :cond_a

    move v0, v3

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ap:Z

    .line 1221
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ap:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->an:Lbdh;

    if-eqz v0, :cond_8

    .line 1222
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->an:Lbdh;

    iget-object v0, v0, Lbdh;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    move v0, v3

    :goto_3
    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->aq:Z

    .line 1225
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->aj:Laja;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->an:Lbdh;

    if-eqz v0, :cond_9

    .line 1226
    invoke-static {}, Lec;->a()Lec;

    move-result-object v0

    .line 1227
    iget-boolean v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ao:Z

    if-eqz v1, :cond_c

    .line 1228
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->aj:Laja;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->getActivity()Ly;

    move-result-object v4

    sget v5, Lh;->bp:I

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->an:Lbdh;

    iget-object v6, v6, Lbdh;->e:Ljava/lang/String;

    sget-object v7, Lel;->e:Lek;

    .line 1230
    invoke-virtual {v0, v6, v7}, Lec;->a(Ljava/lang/String;Lek;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v2

    .line 1228
    invoke-virtual {v4, v5, v3}, Ly;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Laja;->a:Ljava/lang/String;

    .line 1240
    :goto_4
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->aj:Laja;

    iget-object v0, v0, Laja;->h:Landroid/view/View;

    if-eqz v0, :cond_9

    .line 1241
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->aj:Laja;

    iget-object v0, v0, Laja;->h:Landroid/view/View;

    sget v1, Lg;->hz:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1242
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->aj:Laja;

    iget-object v1, v1, Laja;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1246
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->g:Lajc;

    invoke-virtual {v0, p2}, Lajc;->a(Landroid/database/Cursor;)V

    .line 1247
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->d()V

    goto/16 :goto_0

    :cond_a
    move v0, v2

    .line 1220
    goto :goto_2

    :cond_b
    move v0, v2

    .line 1222
    goto :goto_3

    .line 1234
    :cond_c
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->aj:Laja;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->getActivity()Ly;

    move-result-object v4

    sget v5, Lh;->bd:I

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->an:Lbdh;

    iget-object v6, v6, Lbdh;->e:Ljava/lang/String;

    sget-object v7, Lel;->e:Lek;

    .line 1236
    invoke-virtual {v0, v6, v7}, Lec;->a(Ljava/lang/String;Lek;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v2

    .line 1234
    invoke-virtual {v4, v5, v3}, Ly;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Laja;->a:Ljava/lang/String;

    goto :goto_4

    .line 1191
    :pswitch_data_0
    .packed-switch 0x401
        :pswitch_0
    .end packed-switch
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 632
    return-void
.end method

.method public b()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 1410
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->Z:I

    .line 1411
    invoke-static {v0}, Lbmf;->a(I)I

    move-result v0

    .line 1412
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->d:Lyj;

    invoke-static {v1, v0}, Lbne;->b(Lyj;I)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 643
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 696
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->Y:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    .line 698
    :goto_0
    if-nez v0, :cond_2

    .line 699
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "should not call setConversationName on a conversation which is not a group conversation "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 717
    :cond_0
    :goto_1
    return-void

    .line 696
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 703
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->aa:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 706
    iput-object p1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->aa:Ljava/lang/String;

    .line 707
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->d:Lyj;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->aa:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Lyj;Ljava/lang/String;Ljava/lang/String;)I

    .line 709
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ah:Laja;

    iget-object v0, v0, Laja;->h:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 710
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ah:Laja;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lh;->bk:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Laja;->a:Ljava/lang/String;

    .line 711
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ah:Laja;

    iput-object p1, v0, Laja;->b:Ljava/lang/String;

    .line 712
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ah:Laja;

    iget-object v0, v0, Laja;->h:Landroid/view/View;

    sget v1, Lg;->hi:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 713
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ah:Laja;

    iget-object v1, v1, Laja;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 714
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ah:Laja;

    iget-object v0, v0, Laja;->h:Landroid/view/View;

    sget v1, Lg;->hi:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 715
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method protected isEmpty()Z
    .locals 1

    .prologue
    .line 1124
    const/4 v0, 0x0

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1333
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 1334
    packed-switch p1, :pswitch_data_0

    .line 1368
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Bad request code "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    .line 1372
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->g:Lajc;

    if-eqz v0, :cond_1

    .line 1373
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->g:Lajc;

    invoke-virtual {v0}, Lajc;->notifyDataSetChanged()V

    .line 1376
    :cond_1
    return-void

    .line 1337
    :pswitch_0
    invoke-static {p3}, Lchf;->a(Landroid/content/Intent;)Ljava/util/ArrayList;

    move-result-object v2

    .line 1339
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->mCircleIdsMapping:Ljava/util/Map;

    if-eqz v0, :cond_1

    .line 1348
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->mCircleIdsMapping:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ax:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 1349
    if-nez v0, :cond_4

    .line 1350
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1351
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->mCircleIdsMapping:Ljava/util/Map;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ax:Ljava/lang/String;

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 1353
    :goto_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1354
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    .line 1355
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1357
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->g:Lajc;

    if-eqz v0, :cond_0

    .line 1358
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->g:Lajc;

    invoke-virtual {v0}, Lajc;->notifyDataSetChanged()V

    goto :goto_0

    .line 1362
    :pswitch_1
    invoke-virtual {p0, p3}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->av:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->aA:Lbme;

    invoke-interface {v1}, Lbme;->a()Laux;

    move-result-object v1

    const/16 v2, 0x640

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Laux;->a(Ljava/lang/Integer;)V

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->c:[Ljava/lang/String;

    aget-object v1, v1, v3

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->av:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->f()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->a(Ljava/lang/String;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->d:Lyj;

    invoke-static {v2, v1, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->g(Lyj;Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->am:Laja;

    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->q()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->a(Laja;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1365
    :pswitch_2
    invoke-virtual {p0, p3}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->c:[Ljava/lang/String;

    aget-object v1, v1, v3

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->au:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->b()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->a(Ljava/lang/String;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->d:Lyj;

    invoke-static {v2, v1, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Lyj;Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->al:Laja;

    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->e()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->a(Laja;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_4
    move-object v1, v0

    goto/16 :goto_1

    .line 1334
    nop

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 283
    invoke-super {p0, p1}, Lakl;->onAttach(Landroid/app/Activity;)V

    .line 284
    iput-object p1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->az:Landroid/app/Activity;

    .line 286
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 287
    const-string v1, "conversation_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->b:Ljava/lang/String;

    .line 288
    const-string v1, "conversation_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->aa:Ljava/lang/String;

    .line 289
    const-string v1, "notification_level"

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ab:I

    .line 291
    const-string v1, "conversation_type"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->Y:I

    .line 293
    const-string v1, "account_name"

    .line 294
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 293
    invoke-static {v1}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->d:Lyj;

    .line 295
    const-string v1, "transport_type"

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->d:Lyj;

    .line 296
    invoke-virtual {v2}, Lyj;->W()I

    move-result v2

    .line 295
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->Z:I

    .line 297
    const-string v1, "latest_timestamp"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->af:J

    .line 299
    const-string v1, "has_unknown_sender"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ag:Z

    .line 300
    const-string v1, "chat_ringtone_uri"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->au:Ljava/lang/String;

    .line 301
    const-string v1, "hangout_ringtone_uri"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->av:Ljava/lang/String;

    .line 302
    const-string v1, "merged_conversation_ids"

    .line 303
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->c:[Ljava/lang/String;

    .line 304
    const-string v1, "preferred_chat_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ar:Ljava/lang/String;

    .line 305
    const-string v1, "preferred_gaia_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->as:Ljava/lang/String;

    .line 306
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->at:Lbdh;

    .line 307
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->c:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_0

    .line 308
    const-string v0, "Babel"

    const-string v1, "should not have mMergedConversationIds.length == 0"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->aa:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 311
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/16 v1, 0x1e

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 509
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-static {v0, v4}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v0

    .line 510
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->getActivity()Ly;

    move-result-object v6

    .line 511
    packed-switch v0, :pswitch_data_0

    .line 576
    :cond_0
    :goto_0
    return-void

    .line 513
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->getFragmentManager()Lae;

    move-result-object v0

    invoke-virtual {v0}, Lae;->a()Lao;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->aa:Ljava/lang/String;

    invoke-static {v1}, Lanf;->a(Ljava/lang/String;)Lanf;

    move-result-object v1

    invoke-virtual {v1, p0, v4}, Lanf;->setTargetFragment(Lt;I)V

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lanf;->a(Lao;Ljava/lang/String;)I

    goto :goto_0

    .line 517
    :pswitch_1
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ab:I

    if-ne v0, v1, :cond_1

    const/16 v0, 0xa

    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ab:I

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ai:Laja;

    iput-boolean v4, v0, Laja;->e:Z

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->c:[Ljava/lang/String;

    array-length v1, v0

    :goto_2
    if-ge v4, v1, :cond_2

    aget-object v2, v0, v4

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->d:Lyj;

    iget v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ab:I

    invoke-static {v3, v2, v5}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;Ljava/lang/String;I)I

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_1
    iput v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ab:I

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ai:Laja;

    iput-boolean v5, v0, Laja;->e:Z

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ai:Laja;

    iget-object v0, v0, Laja;->h:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ai:Laja;

    iget-object v0, v0, Laja;->h:Landroid/view/View;

    sget v1, Lg;->al:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ai:Laja;

    iget-boolean v1, v1, Laja;->e:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->al:Laja;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->al:Laja;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ai:Laja;

    iget-boolean v1, v1, Laja;->e:Z

    invoke-virtual {v0, v1}, Laja;->a(Z)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->am:Laja;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->am:Laja;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ai:Laja;

    iget-boolean v1, v1, Laja;->e:Z

    invoke-virtual {v0, v1}, Laja;->a(Z)V

    goto :goto_0

    .line 521
    :pswitch_2
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ao:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->an:Lbdh;

    if-eqz v0, :cond_5

    .line 522
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->d:Lyj;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->an:Lbdh;

    iget-object v1, v1, Lbdh;->b:Lbdk;

    iget-object v1, v1, Lbdk;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->an:Lbdh;

    iget-object v2, v2, Lbdh;->b:Lbdk;

    iget-object v2, v2, Lbdk;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->an:Lbdh;

    iget-object v3, v3, Lbdh;->e:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)I

    .line 529
    sget v0, Lh;->kw:I

    new-array v1, v5, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->an:Lbdh;

    iget-object v2, v2, Lbdh;->e:Ljava/lang/String;

    aput-object v2, v1, v4

    invoke-virtual {v6, v0, v1}, Ly;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 531
    invoke-static {v6, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 533
    iput-boolean v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ao:Z

    .line 535
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->aj:Laja;

    if-eqz v0, :cond_4

    .line 536
    invoke-static {}, Lec;->a()Lec;

    move-result-object v0

    .line 537
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->aj:Laja;

    sget v2, Lh;->bd:I

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->an:Lbdh;

    iget-object v5, v5, Lbdh;->e:Ljava/lang/String;

    sget-object v7, Lel;->e:Lek;

    .line 538
    invoke-virtual {v0, v5, v7}, Lec;->a(Ljava/lang/String;Lek;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    .line 537
    invoke-virtual {v6, v2, v3}, Ly;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Laja;->a:Ljava/lang/String;

    .line 541
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->aj:Laja;

    iget-object v0, v0, Laja;->h:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 542
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->aj:Laja;

    iget-object v0, v0, Laja;->h:Landroid/view/View;

    sget v1, Lg;->hz:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 543
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->aj:Laja;

    iget-object v1, v1, Laja;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 546
    :cond_4
    const-string v0, "Babel"

    const-string v1, "no participants found when trying to unblock"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 550
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->a()V

    goto/16 :goto_0

    .line 555
    :pswitch_3
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ay:Z

    if-nez v0, :cond_0

    .line 556
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->c()Z

    goto/16 :goto_0

    .line 562
    :pswitch_4
    sget v0, Lh;->fw:I

    .line 563
    invoke-virtual {v6, v0}, Ly;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 564
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->e()Landroid/net/Uri;

    move-result-object v2

    .line 565
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->b()Landroid/net/Uri;

    move-result-object v3

    const/16 v4, 0x67

    const/4 v5, 0x2

    move-object v0, p0

    .line 562
    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->a(Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;II)V

    goto/16 :goto_0

    .line 569
    :pswitch_5
    sget v0, Lh;->ft:I

    .line 570
    invoke-virtual {v6, v0}, Ly;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 571
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->q()Landroid/net/Uri;

    move-result-object v2

    .line 572
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->f()Landroid/net/Uri;

    move-result-object v3

    const/16 v4, 0x66

    move-object v0, p0

    .line 569
    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->a(Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;II)V

    goto/16 :goto_0

    .line 511
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public onConnected(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1279
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->getActivity()Ly;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1280
    const-string v0, "Babel"

    const-string v1, "People client connected but ConversationParaticipantsFragment is detached."

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1288
    :cond_0
    :goto_0
    return-void

    .line 1285
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ac:Lcvi;

    if-eqz v0, :cond_0

    .line 1286
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->d()V

    goto :goto_0
.end method

.method public onConnectionFailed(Lcft;)V
    .locals 0

    .prologue
    .line 1273
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 183
    invoke-super {p0, p1}, Lakl;->onCreate(Landroid/os/Bundle;)V

    .line 185
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0, v1}, Ly;->setResult(I)V

    .line 187
    invoke-virtual {p0, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->setHasOptionsMenu(Z)V

    .line 189
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lbme;

    invoke-static {v0, v1}, Lcyj;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbme;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->aA:Lbme;

    .line 197
    const/4 v0, 0x2

    :try_start_0
    new-array v0, v0, [Ljava/lang/reflect/Field;

    const/4 v1, 0x0

    .line 198
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "mMyCircles"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 199
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "mCircleIdsMapping"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    aput-object v2, v0, v1

    .line 200
    new-instance v1, Lbxq;

    new-instance v2, Laiw;

    invoke-direct {v2, p0}, Laiw;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;)V

    invoke-direct {v1, v0, p0, v2}, Lbxq;-><init>([Ljava/lang/reflect/Field;Ljava/lang/Object;Lbxr;)V

    iput-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->aw:Lbxq;
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    .line 220
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ac:Lcvi;

    if-nez v0, :cond_0

    .line 221
    new-instance v0, Lcvi;

    .line 222
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0, p0}, Lcvi;-><init>(Landroid/content/Context;Lcfv;Lcfw;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ac:Lcvi;

    .line 226
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ac:Lcvi;

    invoke-virtual {v0}, Lcvi;->a()V

    .line 228
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ae:Lyc;

    invoke-virtual {v0, v4, v4}, Lyc;->d(Lyj;Ljava/lang/String;)V

    .line 229
    return-void

    .line 216
    :catch_0
    move-exception v0

    .line 217
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "NO SUCH METHOD -- you\'re gonna have a bad day"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Ldg;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1180
    packed-switch p1, :pswitch_data_0

    .line 1186
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1182
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ae:Lyc;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->d:Lyj;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lyc;->d(Lyj;Ljava/lang/String;)V

    .line 1183
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ae:Lyc;

    sget v1, Lyd;->a:I

    invoke-virtual {v0, v1}, Lyc;->a(I)Ldg;

    move-result-object v0

    goto :goto_0

    .line 1180
    nop

    :pswitch_data_0
    .packed-switch 0x401
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 1022
    sget v0, Lf;->gU:I

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1023
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 20

    .prologue
    .line 949
    sget v2, Lf;->eQ:I

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v17

    .line 950
    sget v2, Lg;->eg:I

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->e:Landroid/widget/ListView;

    .line 951
    new-instance v2, Lajc;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->getActivity()Ly;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lajc;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;Landroid/app/Activity;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->g:Lajc;

    .line 952
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->Z:I

    invoke-static {v2}, Lf;->c(I)Z

    move-result v18

    .line 954
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 955
    if-nez v18, :cond_0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->Y:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 956
    sget v2, Lh;->bk:I

    .line 957
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->aa:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 958
    sget v2, Lh;->bn:I

    move v3, v2

    .line 960
    :goto_0
    new-instance v2, Laja;

    .line 961
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->getActivity()Ly;

    move-result-object v4

    invoke-virtual {v4, v3}, Ly;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->aa:Ljava/lang/String;

    sget v6, Lcom/google/android/apps/hangouts/R$drawable;->bk:I

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v9}, Laja;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;Ljava/lang/String;Ljava/lang/String;IZZI)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ah:Laja;

    .line 964
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ah:Laja;

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 966
    :cond_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ab:I

    const/16 v3, 0x1e

    if-ne v2, v3, :cond_4

    const/4 v8, 0x1

    .line 967
    :goto_1
    new-instance v2, Laja;

    .line 968
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->getActivity()Ly;

    move-result-object v3

    sget v4, Lh;->bl:I

    invoke-virtual {v3, v4}, Ly;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    sget v6, Lcom/google/android/apps/hangouts/R$drawable;->cq:I

    const/4 v7, 0x1

    const/4 v9, 0x2

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v9}, Laja;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;Ljava/lang/String;Ljava/lang/String;IZZI)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ai:Laja;

    .line 971
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ai:Laja;

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 973
    new-instance v9, Laja;

    .line 974
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->getActivity()Ly;

    move-result-object v2

    sget v3, Lh;->fw:I

    invoke-virtual {v2, v3}, Ly;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 975
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->e()Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v12

    sget v13, Lcom/google/android/apps/hangouts/R$drawable;->co:I

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x5

    move-object/from16 v10, p0

    invoke-direct/range {v9 .. v16}, Laja;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;Ljava/lang/String;Ljava/lang/String;IZZI)V

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->al:Laja;

    .line 977
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->al:Laja;

    invoke-virtual {v2, v8}, Laja;->a(Z)V

    .line 978
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->al:Laja;

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 980
    new-instance v9, Laja;

    .line 981
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->getActivity()Ly;

    move-result-object v2

    sget v3, Lh;->ft:I

    invoke-virtual {v2, v3}, Ly;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 982
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->q()Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v12

    sget v13, Lcom/google/android/apps/hangouts/R$drawable;->co:I

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x6

    move-object/from16 v10, p0

    invoke-direct/range {v9 .. v16}, Laja;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;Ljava/lang/String;Ljava/lang/String;IZZI)V

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->am:Laja;

    .line 984
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->am:Laja;

    invoke-virtual {v2, v8}, Laja;->a(Z)V

    .line 985
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->am:Laja;

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 987
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->Y:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 988
    new-instance v2, Laja;

    const/4 v4, 0x0

    const/4 v5, 0x0

    sget v6, Lcom/google/android/apps/hangouts/R$drawable;->bJ:I

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x3

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v9}, Laja;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;Ljava/lang/String;Ljava/lang/String;IZZI)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->aj:Laja;

    .line 990
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->aj:Laja;

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 995
    :cond_1
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 996
    if-nez v18, :cond_2

    .line 997
    new-instance v2, Laja;

    .line 998
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->getActivity()Ly;

    move-result-object v3

    sget v4, Lh;->l:I

    invoke-virtual {v3, v4}, Ly;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    sget v6, Lcom/google/android/apps/hangouts/R$drawable;->ce:I

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x4

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v9}, Laja;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;Ljava/lang/String;Ljava/lang/String;IZZI)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ak:Laja;

    .line 1000
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ak:Laja;

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1002
    :cond_2
    new-instance v2, Laiz;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->getActivity()Ly;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v2, v0, v1}, Laiz;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;Ljava/util/ArrayList;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->h:Laiz;

    .line 1003
    new-instance v2, Laiz;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->getActivity()Ly;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v10}, Laiz;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;Ljava/util/ArrayList;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->i:Laiz;

    .line 1005
    new-instance v2, Ladx;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->getActivity()Ly;

    move-result-object v3

    invoke-direct {v2, v3}, Ladx;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->f:Ladx;

    .line 1006
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->f:Ladx;

    new-instance v3, Lajb;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->f:Ladx;

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->h:Laiz;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4, v5, v6}, Lajb;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;Ladx;ZLandroid/widget/BaseAdapter;)V

    invoke-virtual {v2, v3}, Ladx;->a(Ladz;)V

    .line 1008
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->f:Ladx;

    new-instance v3, Lajf;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->f:Ladx;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->g:Lajc;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4, v5}, Lajf;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;Ladx;Landroid/widget/BaseAdapter;)V

    invoke-virtual {v2, v3}, Ladx;->a(Ladz;)V

    .line 1010
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->Y:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    .line 1011
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->f:Ladx;

    new-instance v3, Lajb;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->f:Ladx;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->i:Laiz;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4, v5, v6}, Lajb;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;Ladx;ZLandroid/widget/BaseAdapter;)V

    invoke-virtual {v2, v3}, Ladx;->a(Ladz;)V

    .line 1014
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->e:Landroid/widget/ListView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->f:Ladx;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1016
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->getLoaderManager()Lav;

    move-result-object v2

    const/16 v3, 0x401

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    move-object/from16 v0, p0

    invoke-virtual {v2, v3, v4, v0}, Lav;->a(ILandroid/os/Bundle;Law;)Ldg;

    move-result-object v2

    invoke-virtual {v2}, Ldg;->p()V

    .line 1017
    return-object v17

    .line 966
    :cond_4
    const/4 v8, 0x0

    goto/16 :goto_1

    :cond_5
    move v3, v2

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 257
    invoke-super {p0}, Lakl;->onDestroy()V

    .line 259
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ad:Lcvo;

    if-eqz v0, :cond_0

    .line 260
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ad:Lcvo;

    invoke-virtual {v0}, Lcvo;->b()V

    .line 261
    iput-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ad:Lcvo;

    .line 264
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ac:Lcvi;

    if-eqz v0, :cond_3

    .line 265
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ac:Lcvi;

    invoke-virtual {v0}, Lcvi;->d()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ac:Lcvi;

    invoke-virtual {v0}, Lcvi;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 266
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ac:Lcvi;

    invoke-virtual {v0}, Lcvi;->b()V

    .line 268
    :cond_2
    iput-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ac:Lcvi;

    .line 271
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->e:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 272
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->f:Ladx;

    invoke-virtual {v0}, Ladx;->a()V

    .line 273
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->g:Lajc;

    invoke-virtual {v0}, Lajc;->e()V

    .line 274
    return-void
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 278
    invoke-super {p0}, Lakl;->onDestroyView()V

    .line 279
    return-void
.end method

.method public onDisconnected()V
    .locals 0

    .prologue
    .line 1292
    return-void
.end method

.method public synthetic onLoadFinished(Ldg;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 99
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->a(Ldg;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Ldg;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1263
    invoke-virtual {p1}, Ldg;->l()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1268
    :goto_0
    return-void

    .line 1265
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->g:Lajc;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lajc;->a(Landroid/database/Cursor;)V

    goto :goto_0

    .line 1263
    nop

    :pswitch_data_0
    .packed-switch 0x401
        :pswitch_0
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 1047
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lg;->fN:I

    if-eq v1, v2, :cond_0

    .line 1048
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lg;->fQ:I

    if-ne v1, v2, :cond_3

    .line 1050
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ay:Z

    if-eqz v1, :cond_2

    .line 1081
    :cond_1
    :goto_0
    return v0

    .line 1053
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->c()Z

    move-result v0

    goto :goto_0

    .line 1054
    :cond_3
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lg;->fM:I

    if-ne v1, v2, :cond_6

    .line 1055
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ae:Lyc;

    invoke-virtual {v1}, Lyc;->c()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1059
    iget v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->Y:I

    if-ne v1, v4, :cond_4

    move v1, v6

    .line 1060
    :goto_1
    if-nez v1, :cond_5

    .line 1061
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "should not call forkGroupConversation on a conversation which is not a group conversation "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    move v1, v0

    .line 1059
    goto :goto_1

    .line 1065
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->getActivity()Ly;

    move-result-object v1

    .line 1066
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->d:Lyj;

    sget v2, Lh;->jx:I

    .line 1068
    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ae:Lyc;

    .line 1070
    invoke-virtual {v3}, Lyc;->e()Ljava/util/ArrayList;

    move-result-object v3

    iget v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->Z:I

    .line 1066
    invoke-static/range {v0 .. v5}, Lbbl;->a(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;II)Landroid/content/Intent;

    move-result-object v0

    .line 1073
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->startActivity(Landroid/content/Intent;)V

    .line 1074
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->finish()V

    move v0, v6

    .line 1075
    goto :goto_0

    .line 1076
    :cond_6
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lg;->gH:I

    if-ne v1, v2, :cond_1

    .line 1077
    invoke-static {}, Lbbl;->e()Landroid/content/Intent;

    move-result-object v0

    .line 1078
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->startActivity(Landroid/content/Intent;)V

    move v0, v6

    .line 1079
    goto :goto_0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 1114
    invoke-super {p0}, Lakl;->onPause()V

    .line 1115
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1027
    sget v0, Lg;->fN:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 1028
    sget v0, Lg;->fQ:I

    .line 1029
    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    .line 1030
    sget v0, Lg;->fM:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    .line 1031
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->Z:I

    invoke-static {v0}, Lf;->c(I)Z

    move-result v6

    .line 1032
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->Y:I

    const/4 v7, 0x2

    if-ne v0, v7, :cond_2

    .line 1033
    if-nez v6, :cond_0

    move v0, v1

    :goto_0
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1034
    invoke-interface {v4, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1035
    if-nez v6, :cond_1

    :goto_1
    invoke-interface {v5, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1043
    :goto_2
    return-void

    :cond_0
    move v0, v2

    .line 1033
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1035
    goto :goto_1

    .line 1037
    :cond_2
    invoke-interface {v3, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1038
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->d:Lyj;

    invoke-virtual {v0}, Lyj;->s()Z

    move-result v0

    if-nez v0, :cond_4

    if-eqz v6, :cond_3

    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->Z:I

    .line 1039
    invoke-static {v0}, Lbvx;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ag:Z

    if-nez v0, :cond_4

    .line 1038
    :goto_3
    invoke-interface {v4, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1041
    invoke-interface {v5, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_2

    :cond_4
    move v1, v2

    .line 1039
    goto :goto_3
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 1102
    invoke-super {p0}, Lakl;->onResume()V

    .line 1103
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ay:Z

    .line 1104
    invoke-static {}, Lbkb;->j()Lyj;

    move-result-object v0

    .line 1105
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->d:Lyj;

    invoke-virtual {v1}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1106
    :cond_0
    const-string v0, "Babel"

    const-string v1, "user signed out of current account"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1108
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->finish()V

    .line 1110
    :cond_1
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1119
    invoke-super {p0, p1}, Lakl;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1120
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 1089
    invoke-super {p0}, Lakl;->onStart()V

    .line 1093
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ac:Lcvi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ac:Lcvi;

    invoke-virtual {v0}, Lcvi;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ac:Lcvi;

    .line 1094
    invoke-virtual {v0}, Lcvi;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1095
    const-string v0, "Babel"

    const-string v1, "Reconnecting people client for ConversationParticipantsFragment."

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1096
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationParticipantsFragment;->ac:Lcvi;

    invoke-virtual {v0}, Lcvi;->a()V

    .line 1098
    :cond_0
    return-void
.end method
