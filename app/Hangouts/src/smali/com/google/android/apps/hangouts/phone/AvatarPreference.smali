.class public Lcom/google/android/apps/hangouts/phone/AvatarPreference;
.super Landroid/preference/Preference;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lyj;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/hangouts/phone/AvatarPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    sget v0, Lf;->gm:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/AvatarPreference;->setLayoutResource(I)V

    .line 28
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Lyj;)V
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/AvatarPreference;->a:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 45
    iput-object p1, p0, Lcom/google/android/apps/hangouts/phone/AvatarPreference;->a:Ljava/lang/String;

    .line 46
    iput-object p2, p0, Lcom/google/android/apps/hangouts/phone/AvatarPreference;->b:Lyj;

    .line 47
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/AvatarPreference;->notifyChanged()V

    .line 49
    :cond_0
    return-void
.end method

.method public onBindView(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 32
    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    .line 33
    sget v0, Lg;->E:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/AvatarView;

    .line 34
    if-eqz v0, :cond_0

    .line 35
    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/AvatarPreference;->a:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 36
    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/AvatarPreference;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/phone/AvatarPreference;->b:Lyj;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/hangouts/views/AvatarView;->a(Ljava/lang/String;Lyj;)V

    .line 41
    :cond_0
    :goto_0
    return-void

    .line 38
    :cond_1
    invoke-virtual {v0, v2, v2}, Lcom/google/android/apps/hangouts/views/AvatarView;->a(Ljava/lang/String;Lyj;)V

    goto :goto_0
.end method
