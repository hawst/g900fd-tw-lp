.class public final Lcom/google/android/apps/hangouts/phone/GmsInstallActivity;
.super Ly;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ly;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 57
    const/4 v0, 0x0

    invoke-static {v0}, Lakn;->b(Z)V

    .line 58
    const/4 v0, 0x0

    .line 59
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/GmsInstallActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "from_main_launcher"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 58
    invoke-static {v0, v1}, Lbbl;->b(Lyj;Z)Landroid/content/Intent;

    move-result-object v0

    .line 60
    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 61
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/GmsInstallActivity;->startActivity(Landroid/content/Intent;)V

    .line 62
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/GmsInstallActivity;->finish()V

    .line 63
    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 47
    invoke-super {p0, p1, p2, p3}, Ly;->onActivityResult(IILandroid/content/Intent;)V

    .line 48
    packed-switch p1, :pswitch_data_0

    .line 54
    :goto_0
    return-void

    .line 50
    :pswitch_0
    const-string v0, "Babel"

    const-string v1, "Received notification from gmsCore installation. Restarting babel"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/GmsInstallActivity;->a()V

    goto :goto_0

    .line 48
    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 25
    invoke-super {p0, p1}, Ly;->onCreate(Landroid/os/Bundle;)V

    .line 26
    const-string v0, "Babel"

    const-string v1, "GmsInstallActivity.onCreate"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    invoke-static {v2, v2, v2}, Lf;->a(ZZZ)I

    move-result v0

    .line 32
    if-nez v0, :cond_0

    .line 33
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/GmsInstallActivity;->a()V

    .line 43
    :goto_0
    return-void

    .line 39
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/GmsInstallActivity;->e()Lae;

    move-result-object v1

    invoke-virtual {v1}, Lae;->a()Lao;

    move-result-object v1

    .line 40
    invoke-static {v0}, Lf;->a(I)Ls;

    move-result-object v0

    .line 42
    const-string v2, "gmscore dialog"

    invoke-virtual {v0, v1, v2}, Ls;->a(Lao;Ljava/lang/String;)I

    goto :goto_0
.end method
