.class public Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;
.super Landroid/widget/RelativeLayout;
.source "PG"

# interfaces
.implements Lcdg;


# static fields
.field private static b:Z

.field private static c:I


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbdk;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/widget/ImageView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Lcom/google/android/apps/hangouts/views/FixedParticipantsGalleryView;

.field private h:Ljava/lang/CharSequence;

.field private i:Ljava/lang/CharSequence;

.field private j:J

.field private k:I

.field private l:Lyj;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->b:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    sget-boolean v0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->b:Z

    if-nez v0, :cond_0

    .line 45
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 46
    sget v1, Lf;->cI:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->c:I

    .line 47
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->b:Z

    .line 49
    :cond_0
    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 146
    iget-wide v0, p0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->j:J

    return-wide v0
.end method

.method public a(J)V
    .locals 0

    .prologue
    .line 105
    iput-wide p1, p0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->j:J

    .line 106
    return-void
.end method

.method public a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lyj;Ljava/util/List;II)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            "Lyj;",
            "Ljava/util/List",
            "<",
            "Lbdk;",
            ">;II)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 61
    iput-object p1, p0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->h:Ljava/lang/CharSequence;

    .line 62
    iput-object p2, p0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->i:Ljava/lang/CharSequence;

    .line 63
    iput p5, p0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->k:I

    .line 64
    iput-object p3, p0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->l:Lyj;

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->e:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->c:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->a:Ljava/util/List;

    if-nez v0, :cond_6

    if-nez p4, :cond_6

    move v2, v3

    .line 98
    :goto_0
    if-eqz v2, :cond_0

    .line 99
    iput-object p4, p0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->a:Ljava/util/List;

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->h:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->i:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->l:Lyj;

    invoke-virtual {v0}, Lyj;->c()Lbdk;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->a:Ljava/util/List;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->a:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    move v1, v0

    :goto_1
    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->a:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v4, :cond_2

    :cond_1
    move v3, v4

    :cond_2
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->bB:I

    if-eqz v3, :cond_b

    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->bD:I

    :cond_3
    :goto_2
    iget-object v3, p0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->d:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    iget v0, p0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->k:I

    if-eq v0, v4, :cond_4

    if-eqz v1, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->a:Ljava/util/List;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-gt v0, v1, :cond_c

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->g:Lcom/google/android/apps/hangouts/views/FixedParticipantsGalleryView;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->l:Lyj;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v5}, Lcom/google/android/apps/hangouts/views/FixedParticipantsGalleryView;->a(Lyj;Ljava/util/List;Lbdk;)V

    .line 102
    :cond_5
    :goto_3
    return-void

    .line 86
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->a:Ljava/util/List;

    if-eqz v0, :cond_7

    if-eqz p4, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->a:Ljava/util/List;

    .line 87
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_8

    :cond_7
    move v2, v4

    .line 88
    goto :goto_0

    :cond_8
    move v2, v3

    .line 90
    :goto_4
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_d

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdk;

    invoke-interface {p4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbdk;

    invoke-virtual {v0, v1}, Lbdk;->a(Lbdk;)Z

    move-result v0

    if-nez v0, :cond_9

    move v2, v4

    .line 93
    goto/16 :goto_0

    .line 90
    :cond_9
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    :cond_a
    move v1, v3

    .line 101
    goto :goto_1

    :cond_b
    if-ne p6, v4, :cond_3

    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->bz:I

    goto :goto_2

    :cond_c
    if-eqz v2, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->g:Lcom/google/android/apps/hangouts/views/FixedParticipantsGalleryView;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->l:Lyj;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->a:Ljava/util/List;

    invoke-virtual {v0, v1, v2, v5}, Lcom/google/android/apps/hangouts/views/FixedParticipantsGalleryView;->a(Lyj;Ljava/util/List;Lbdk;)V

    goto :goto_3

    :cond_d
    move v2, v3

    goto/16 :goto_0
.end method

.method public b()Landroid/view/View;
    .locals 0

    .prologue
    .line 151
    return-object p0
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 53
    sget v0, Lg;->du:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->d:Landroid/widget/ImageView;

    .line 54
    sget v0, Lg;->hm:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->e:Landroid/widget/TextView;

    .line 55
    sget v0, Lg;->dd:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/FixedParticipantsGalleryView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->g:Lcom/google/android/apps/hangouts/views/FixedParticipantsGalleryView;

    .line 56
    sget v0, Lg;->aa:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/HangoutEventMessageListItemView;->f:Landroid/widget/TextView;

    .line 57
    return-void
.end method
