.class public Lcom/google/android/apps/hangouts/views/ComposeMessageView;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Landroid/widget/TextView$OnEditorActionListener;


# static fields
.field private static final C:[Landroid/text/InputFilter;

.field private static E:Z

.field private static F:Z

.field private static G:Z

.field private static final J:Landroid/view/animation/Animation;

.field private static final K:Landroid/view/animation/Animation;

.field private static final L:Landroid/view/animation/Animation;

.field private static final M:Landroid/view/animation/Animation;

.field private static O:I

.field private static P:I

.field private static Q:I

.field private static R:I

.field private static S:I

.field public static final a:Z

.field private static q:Ljava/lang/String;


# instance fields
.field private A:Landroid/view/View$OnLongClickListener;

.field private B:[Landroid/text/InputFilter;

.field private D:Z

.field private final H:Ljava/lang/Runnable;

.field private final I:Lcbm;

.field private final N:Ljava/lang/Runnable;

.field private final T:Landroid/view/View$OnClickListener;

.field private final b:Lbme;

.field private final c:Landroid/view/View;

.field private final d:Landroid/widget/ImageButton;

.field private final e:Landroid/widget/ImageButton;

.field private final f:Landroid/widget/ImageButton;

.field private final g:Lcom/google/android/apps/hangouts/views/TransportSpinner;

.field private h:I

.field private final i:Landroid/widget/EditText;

.field private final j:Landroid/widget/TextView;

.field private final k:Landroid/widget/TextView;

.field private l:I

.field private m:Lcbl;

.field private n:I

.field private o:J

.field private final p:Landroid/os/Handler;

.field private final r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcbk;",
            ">;"
        }
    .end annotation
.end field

.field private s:Z

.field private t:Lt;

.field private u:Lyj;

.field private v:Lbxn;

.field private w:Z

.field private final x:Landroid/view/View;

.field private final y:Landroid/widget/TextView;

.field private z:Lbwt;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    .line 89
    sget-object v0, Lbys;->s:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a:Z

    .line 124
    const-string v0, "Babel"

    sput-object v0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->q:Ljava/lang/String;

    .line 145
    const/4 v0, 0x0

    new-array v0, v0, [Landroid/text/InputFilter;

    sput-object v0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->C:[Landroid/text/InputFilter;

    .line 268
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/high16 v8, -0x40800000    # -1.0f

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 276
    sput-object v0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->J:Landroid/view/animation/Animation;

    const-wide/16 v1, 0xc8

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 277
    sget-object v0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->J:Landroid/view/animation/Animation;

    new-instance v1, Lwq;

    invoke-direct {v1}, Lwq;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 281
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/high16 v6, -0x40800000    # -1.0f

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 289
    sput-object v0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->K:Landroid/view/animation/Animation;

    const-wide/16 v1, 0xc8

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 290
    sget-object v0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->K:Landroid/view/animation/Animation;

    new-instance v1, Lwq;

    invoke-direct {v1}, Lwq;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 294
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 302
    sput-object v0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->L:Landroid/view/animation/Animation;

    const-wide/16 v1, 0xc8

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 303
    sget-object v0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->L:Landroid/view/animation/Animation;

    new-instance v1, Lwq;

    invoke-direct {v1}, Lwq;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 306
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 314
    sput-object v0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->M:Landroid/view/animation/Animation;

    const-wide/16 v1, 0xc8

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 315
    sget-object v0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->M:Landroid/view/animation/Animation;

    new-instance v1, Lwq;

    invoke-direct {v1}, Lwq;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 323
    invoke-static {}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->r()V

    .line 326
    new-instance v0, Lcbc;

    invoke-direct {v0}, Lcbc;-><init>()V

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/Runnable;)V

    .line 332
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 513
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 107
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lbme;

    .line 106
    invoke-static {v0, v1}, Lcyj;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbme;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->b:Lbme;

    .line 118
    iput v3, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->l:I

    .line 122
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->p:Landroid/os/Handler;

    .line 127
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->r:Ljava/util/List;

    .line 131
    iput-boolean v3, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->s:Z

    .line 133
    iput-object v2, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->t:Lt;

    .line 134
    iput-object v2, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->u:Lyj;

    .line 147
    iput-boolean v3, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->D:Z

    .line 153
    new-instance v0, Lcbb;

    invoke-direct {v0, p0}, Lcbb;-><init>(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->H:Ljava/lang/Runnable;

    .line 182
    new-instance v0, Lcbm;

    invoke-direct {v0, p0, v3}, Lcbm;-><init>(Lcom/google/android/apps/hangouts/views/ComposeMessageView;B)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->I:Lcbm;

    .line 347
    new-instance v0, Lcbd;

    invoke-direct {v0, p0}, Lcbd;-><init>(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->N:Ljava/lang/Runnable;

    .line 1489
    new-instance v0, Lcbj;

    invoke-direct {v0, p0}, Lcbj;-><init>(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->T:Landroid/view/View$OnClickListener;

    .line 514
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 515
    sget v1, Lf;->ex:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 516
    iput v3, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->l:I

    .line 517
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->n:I

    .line 518
    sget v0, Lg;->eC:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->x:Landroid/view/View;

    .line 519
    sget v0, Lg;->hr:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->y:Landroid/widget/TextView;

    .line 520
    sget v0, Lg;->eE:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    .line 522
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    new-instance v2, Lcbf;

    invoke-direct {v2, p0}, Lcbf;-><init>(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 550
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 552
    sget v0, Lg;->au:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->j:Landroid/widget/TextView;

    .line 553
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setClickable(Z)V

    .line 554
    sget v0, Lg;->at:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->k:Landroid/widget/TextView;

    .line 555
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setClickable(Z)V

    .line 557
    new-instance v2, Lcbg;

    invoke-direct {v2, p0}, Lcbg;-><init>(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)V

    .line 577
    sget v0, Lg;->ga:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->d:Landroid/widget/ImageButton;

    .line 578
    sget v0, Lg;->gb:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->c:Landroid/view/View;

    .line 579
    sget v0, Lg;->fH:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->f:Landroid/widget/ImageButton;

    .line 580
    sget v0, Lg;->hM:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/TransportSpinner;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->g:Lcom/google/android/apps/hangouts/views/TransportSpinner;

    .line 581
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->g:Lcom/google/android/apps/hangouts/views/TransportSpinner;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 583
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->d:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 584
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->f:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 587
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->w()V

    .line 589
    sget v0, Lg;->fU:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->e:Landroid/widget/ImageButton;

    .line 590
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->e:Landroid/widget/ImageButton;

    new-instance v1, Lcbh;

    invoke-direct {v1, p0}, Lcbh;-><init>(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 600
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->u()V

    .line 602
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lf;->ce:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->O:I

    sget v1, Lf;->ci:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->P:I

    sget v1, Lf;->ch:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->Q:I

    sget v1, Lf;->cg:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->R:I

    sget v1, Lf;->cf:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->S:I

    .line 603
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/views/ComposeMessageView;Lbxn;)Lbxn;
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->v:Lbxn;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x1388

    const/4 v4, 0x3

    const/4 v5, 0x1

    .line 87
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    iget v3, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->n:I

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_1
    iget v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->n:I

    if-ne v0, v5, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->p:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->H:Ljava/lang/Runnable;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->p:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->H:Ljava/lang/Runnable;

    invoke-virtual {v0, v3, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    iput-wide v1, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->o:J

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_0

    :pswitch_0
    if-lez v0, :cond_0

    iput v5, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->n:I

    iget v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->n:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->d(I)V

    goto :goto_1

    :pswitch_1
    if-nez v0, :cond_3

    iput v4, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->n:I

    goto :goto_1

    :cond_3
    iput v5, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->n:I

    iget v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->n:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->d(I)V

    goto :goto_1

    :pswitch_2
    if-nez v0, :cond_4

    iput v4, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->n:I

    iget v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->n:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->d(I)V

    goto :goto_1

    :cond_4
    iget-wide v3, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->o:J

    sub-long v3, v1, v3

    cmp-long v0, v3, v6

    if-lez v0, :cond_0

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->n:I

    iget v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->n:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->d(I)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/views/ComposeMessageView;Ljava/lang/CharSequence;II)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->m:Lcbl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->m:Lcbl;

    invoke-interface {v0}, Lcbl;->a()I

    move-result v0

    invoke-static {v0}, Lf;->c(I)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->z:Lbwt;

    invoke-virtual {v0}, Lbwt;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    if-le p2, p3, :cond_6

    move v0, v1

    :goto_1
    if-eqz v0, :cond_0

    :cond_2
    invoke-static {p1, v2}, Landroid/telephony/SmsMessage;->calculateLength(Ljava/lang/CharSequence;Z)[I

    move-result-object v3

    aget v4, v3, v2

    const/4 v0, 0x2

    aget v5, v3, v0

    invoke-static {}, Lbvm;->a()Lsm;

    move-result-object v0

    invoke-virtual {v0}, Lsm;->p()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-static {}, Lbvm;->a()Lsm;

    move-result-object v0

    invoke-virtual {v0}, Lsm;->q()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v6, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->z:Lbwt;

    if-le v4, v1, :cond_7

    move v0, v1

    :goto_2
    invoke-virtual {v6, v0}, Lbwt;->b(Z)V

    :goto_3
    invoke-static {}, Lbvm;->a()Lsm;

    move-result-object v0

    invoke-virtual {v0}, Lsm;->c()I

    move-result v0

    if-lez v0, :cond_4

    aget v3, v3, v1

    add-int v6, v3, v5

    const/16 v7, 0x8c

    if-ge v6, v7, :cond_3

    div-int/lit8 v0, v0, 0x2

    :cond_3
    if-le v3, v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->z:Lbwt;

    invoke-virtual {v0, v1}, Lbwt;->b(Z)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->z:Lbwt;

    invoke-virtual {v0}, Lbwt;->f()Z

    move-result v0

    if-nez v0, :cond_c

    if-gt v4, v1, :cond_5

    const/16 v0, 0xa

    if-gt v5, v0, :cond_c

    :cond_5
    move v0, v1

    :goto_4
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a()V

    if-eqz v0, :cond_b

    if-le v4, v1, :cond_a

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_5
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->y:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->y:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_6
    move v0, v2

    goto :goto_1

    :cond_7
    move v0, v2

    goto :goto_2

    :cond_8
    invoke-static {}, Lbvm;->a()Lsm;

    move-result-object v0

    invoke-virtual {v0}, Lsm;->b()I

    move-result v0

    iget-object v6, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->z:Lbwt;

    if-lez v0, :cond_9

    if-le v4, v0, :cond_9

    move v0, v1

    :goto_6
    invoke-virtual {v6, v0}, Lbwt;->b(Z)V

    goto :goto_3

    :cond_9
    move v0, v2

    goto :goto_6

    :cond_a
    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->y:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_c
    move v0, v2

    goto :goto_4
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/views/ComposeMessageView;Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 87
    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a(Ljava/lang/String;JLjava/lang/String;I)V

    return-void
.end method

.method private a(Ljava/lang/String;JLjava/lang/String;I)V
    .locals 6

    .prologue
    .line 1149
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->m:Lcbl;

    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 1150
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->m:Lcbl;

    if-eqz v0, :cond_0

    .line 1151
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->m:Lcbl;

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcbl;->a(Ljava/lang/String;JLjava/lang/String;I)V

    .line 1153
    :cond_0
    return-void
.end method

.method public static a(Landroid/net/Uri;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1483
    invoke-virtual {p0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->c(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1485
    invoke-virtual {p0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v2}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->c(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->w()V

    return-void
.end method

.method public static c(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1451
    packed-switch p0, :pswitch_data_0

    .line 1472
    const-string v0, ""

    .line 1475
    :goto_0
    return-object v0

    .line 1453
    :pswitch_0
    const-string v0, "camera-p.jpg"

    goto :goto_0

    .line 1458
    :pswitch_1
    const/4 v0, 0x0

    .line 1459
    invoke-static {v0}, Landroid/media/CamcorderProfile;->get(I)Landroid/media/CamcorderProfile;

    move-result-object v0

    iget v0, v0, Landroid/media/CamcorderProfile;->fileFormat:I

    .line 1461
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1462
    const-string v0, "camera-p.mp4"

    goto :goto_0

    .line 1463
    :cond_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1464
    const-string v0, "camera-p.3gp"

    goto :goto_0

    .line 1466
    :cond_1
    sget-object v0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->q:Ljava/lang/String;

    const-string v1, "Saved video file is not mp4 or 3gpp"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1467
    const-string v0, "camera-p.3gp"

    goto :goto_0

    .line 1451
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static synthetic c(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Lyj;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->u:Lyj;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Lt;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->t:Lt;

    return-object v0
.end method

.method private d(I)V
    .locals 1

    .prologue
    .line 1163
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->m:Lcbl;

    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 1164
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->m:Lcbl;

    if-eqz v0, :cond_0

    .line 1165
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->m:Lcbl;

    invoke-interface {v0, p1}, Lcbl;->a(I)V

    .line 1167
    :cond_0
    return-void
.end method

.method public static synthetic e(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Lbxn;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->v:Lbxn;

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Lcbl;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->m:Lcbl;

    return-object v0
.end method

.method public static synthetic g(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    return-object v0
.end method

.method public static synthetic h(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Lcbm;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->I:Lcbm;

    return-object v0
.end method

.method public static synthetic i(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->p:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic j(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Z
    .locals 1

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->v()Z

    move-result v0

    return v0
.end method

.method public static synthetic k(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->N:Ljava/lang/Runnable;

    return-object v0
.end method

.method public static synthetic l(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Z
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 87
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->t:Lt;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->u:Lyj;

    invoke-virtual {v2}, Lyj;->s()Z

    move-result v2

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->l:I

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->l:I

    if-eq v2, v3, :cond_0

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    sget v2, Lh;->av:I

    invoke-static {v1, v2}, Lf;->a(Landroid/content/Context;I)V

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->u:Lyj;

    invoke-virtual {v2}, Lyj;->l()Z

    move-result v2

    if-nez v2, :cond_2

    iget v2, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->l:I

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->l:I

    if-eq v2, v3, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->b:Lbme;

    invoke-interface {v1}, Lbme;->a()Laux;

    move-result-object v1

    const/16 v2, 0x65e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Laux;->a(Ljava/lang/Integer;)V

    const-string v1, "g_plus_upgrade_photo"

    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->u:Lyj;

    invoke-static {v1, v2}, Lbbl;->a(Ljava/lang/String;Lyj;)Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->t:Lt;

    invoke-virtual {v2, v1}, Lt;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public static synthetic m(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->x()V

    return-void
.end method

.method public static synthetic n(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)Ljava/util/List;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->r:Ljava/util/List;

    return-object v0
.end method

.method public static synthetic o()V
    .locals 0

    .prologue
    .line 87
    invoke-static {}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->r()V

    return-void
.end method

.method public static synthetic p()Z
    .locals 1

    .prologue
    .line 87
    sget-boolean v0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->E:Z

    return v0
.end method

.method public static synthetic q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->q:Ljava/lang/String;

    return-object v0
.end method

.method private static r()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 336
    const-string v0, "babel_use_gplus_photo_picker"

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->E:Z

    .line 339
    const-string v0, "babel_prefer_system_emoji_ime"

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->F:Z

    .line 342
    const-string v0, "babel_gv_sms"

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->G:Z

    .line 345
    return-void
.end method

.method private s()V
    .locals 5

    .prologue
    .line 626
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->g:Lcom/google/android/apps/hangouts/views/TransportSpinner;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 627
    const/4 v0, 0x0

    .line 632
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->x:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->x:Landroid/view/View;

    .line 634
    invoke-virtual {v2}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->x:Landroid/view/View;

    .line 635
    invoke-virtual {v3}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->x:Landroid/view/View;

    .line 636
    invoke-virtual {v4}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    .line 632
    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 637
    return-void

    .line 629
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lf;->cN:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_0
.end method

.method private t()V
    .locals 3

    .prologue
    .line 878
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbk;

    .line 879
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcbk;->cancel(Z)Z

    goto :goto_0

    .line 881
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 882
    return-void
.end method

.method private u()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 960
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->e:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 961
    sget-boolean v0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->F:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v2, v3, :cond_2

    const-string v2, "input_method"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->getCurrentInputMethodSubtype()Landroid/view/inputmethod/InputMethodSubtype;

    move-result-object v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_3

    .line 962
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->e:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 963
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->s()V

    .line 975
    :cond_0
    :goto_2
    return-void

    .line 961
    :cond_1
    const-string v2, "EmojiCapable"

    invoke-virtual {v0, v2}, Landroid/view/inputmethod/InputMethodSubtype;->containsExtraValueKey(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    .line 967
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->e:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 968
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->s()V

    .line 969
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->s:Z

    if-eqz v0, :cond_4

    .line 970
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->e:Landroid/widget/ImageButton;

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->bK:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_2

    .line 972
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->e:Landroid/widget/ImageButton;

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->bl:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_2
.end method

.method private v()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 991
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->w:Z

    if-nez v0, :cond_1

    .line 1010
    :cond_0
    :goto_0
    return v2

    .line 996
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    if-nez v0, :cond_3

    move v0, v1

    .line 1006
    :goto_1
    iget v3, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->l:I

    if-nez v3, :cond_2

    if-eqz v0, :cond_0

    :cond_2
    move v2, v1

    .line 1010
    goto :goto_0

    .line 998
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 999
    goto :goto_1

    .line 1000
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 1001
    goto :goto_1

    :cond_5
    move v0, v2

    .line 1003
    goto :goto_1
.end method

.method private w()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x4

    .line 1014
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->d:Landroid/widget/ImageButton;

    if-nez v0, :cond_0

    .line 1051
    :goto_0
    return-void

    .line 1018
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->v()Z

    move-result v0

    .line 1026
    if-eqz v0, :cond_4

    .line 1027
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->f:Landroid/widget/ImageButton;

    .line 1028
    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    .line 1029
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    .line 1030
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->c:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1031
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->f:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1032
    if-eqz v0, :cond_2

    .line 1033
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->c:Landroid/view/View;

    sget-object v1, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->J:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1034
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->f:Landroid/widget/ImageButton;

    sget-object v1, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->M:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1050
    :cond_2
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a()V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1029
    goto :goto_1

    .line 1038
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->f:Landroid/widget/ImageButton;

    .line 1039
    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-eq v0, v3, :cond_6

    .line 1040
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->f:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1041
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1042
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->f:Landroid/widget/ImageButton;

    sget-object v1, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->L:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1043
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->c:Landroid/view/View;

    sget-object v1, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->K:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1045
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->d:Landroid/widget/ImageButton;

    iget v1, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->h:I

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->ck:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_2
.end method

.method private x()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 1090
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->m:Lcbl;

    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 1091
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->m:Lcbl;

    if-eqz v0, :cond_3

    .line 1092
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    if-nez v0, :cond_4

    const/4 v0, 0x0

    move-object v1, v0

    .line 1094
    :goto_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->l:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->m:Lcbl;

    .line 1095
    invoke-interface {v0}, Lcbl;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1096
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->t:Lt;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->l:I

    if-ne v0, v4, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->t:Lt;

    instance-of v0, v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->t:Lt;

    check-cast v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->Z()I

    move-result v0

    :goto_1
    const/4 v3, 0x2

    if-ne v0, v3, :cond_5

    const/16 v0, 0x660

    :goto_2
    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->b:Lbme;

    invoke-interface {v2}, Lbme;->a()Laux;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Laux;->a(Ljava/lang/Integer;)V

    .line 1097
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->m:Lcbl;

    invoke-interface {v0, v1}, Lcbl;->a(Ljava/lang/String;)V

    .line 1099
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1101
    :cond_3
    return-void

    .line 1092
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    .line 1093
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 1096
    :cond_5
    if-ne v0, v4, :cond_6

    const/16 v0, 0x661

    goto :goto_2

    :cond_6
    move v0, v2

    goto :goto_2

    :cond_7
    move v0, v2

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 14

    .prologue
    const/16 v12, 0x8

    const/4 v11, 0x3

    const/4 v10, 0x4

    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 644
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->d:Landroid/widget/ImageButton;

    if-nez v0, :cond_0

    .line 667
    :goto_0
    return-void

    .line 647
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 648
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->m:Lcbl;

    if-nez v2, :cond_1

    .line 649
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->c:Landroid/view/View;

    sget v2, Lh;->kn:I

    .line 650
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 649
    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 653
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->m:Lcbl;

    invoke-interface {v2}, Lcbl;->a()I

    move-result v2

    .line 654
    invoke-static {v2}, Lf;->c(I)Z

    move-result v2

    .line 655
    if-eqz v2, :cond_5

    .line 656
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->z:Lbwt;

    invoke-virtual {v2}, Lbwt;->f()Z

    move-result v2

    .line 657
    iget-object v4, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->g:Lcom/google/android/apps/hangouts/views/TransportSpinner;

    invoke-virtual {v4, v2}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->a(Z)V

    .line 658
    iget-object v4, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->d:Landroid/widget/ImageButton;

    if-eqz v2, :cond_4

    sget v2, Lh;->kq:I

    .line 659
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 658
    :goto_1
    invoke-virtual {v4, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 666
    :goto_2
    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->m:Lcbl;

    invoke-interface {v0}, Lcbl;->a()I

    move-result v4

    invoke-static {v4}, Lf;->c(I)Z

    move-result v5

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->m:Lcbl;

    invoke-interface {v0}, Lcbl;->e()Ljava/util/Collection;

    move-result-object v6

    if-eqz v6, :cond_19

    invoke-interface {v6}, Ljava/util/Collection;->size()I

    move-result v0

    :goto_3
    iget-boolean v7, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->D:Z

    if-eqz v7, :cond_6

    sget v0, Lh;->kc:I

    move-object v13, v2

    move v2, v0

    move-object v0, v13

    :goto_4
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_2
    sget-boolean v2, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->G:Z

    if-eqz v2, :cond_16

    if-eqz v5, :cond_16

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    const/16 v6, 0x30

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setGravity(I)V

    iget-object v5, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    const-string v6, ""

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->j:Landroid/widget/TextView;

    sget v6, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->S:I

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v5, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->j:Landroid/widget/TextView;

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-ne v4, v11, :cond_13

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v4, Lh;->ko:I

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_5
    iget-object v4, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->k:Landroid/widget/TextView;

    sget v5, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->S:I

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v4, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->k:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_15

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_6
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_7
    sget v0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->O:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->setBackgroundColor(I)V

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    iget v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->h:I

    if-ne v0, v3, :cond_18

    sget v0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->P:I

    :goto_8
    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setTextColor(I)V

    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->u()V

    goto/16 :goto_0

    .line 659
    :cond_4
    sget v2, Lh;->kr:I

    .line 660
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 662
    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->d:Landroid/widget/ImageButton;

    sget v4, Lh;->kn:I

    .line 663
    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 662
    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 666
    :cond_6
    iget v7, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->l:I

    if-ne v7, v3, :cond_7

    sget v0, Lh;->iE:I

    move-object v13, v2

    move v2, v0

    move-object v0, v13

    goto/16 :goto_4

    :cond_7
    iget v7, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->l:I

    if-ne v7, v11, :cond_8

    sget v0, Lh;->ou:I

    move-object v13, v2

    move v2, v0

    move-object v0, v13

    goto/16 :goto_4

    :cond_8
    iget v7, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->l:I

    if-ne v7, v10, :cond_9

    sget v0, Lh;->fZ:I

    move-object v13, v2

    move v2, v0

    move-object v0, v13

    goto/16 :goto_4

    :cond_9
    iget v7, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->l:I

    const/4 v8, 0x6

    if-ne v7, v8, :cond_a

    sget v0, Lh;->nL:I

    move-object v13, v2

    move v2, v0

    move-object v0, v13

    goto/16 :goto_4

    :cond_a
    if-eqz v5, :cond_10

    if-ne v0, v3, :cond_e

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    iget-object v6, v0, Lbdh;->c:Ljava/lang/String;

    invoke-static {v6}, Laea;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->z:Lbwt;

    invoke-virtual {v0}, Lbwt;->f()Z

    move-result v0

    if-eqz v0, :cond_b

    sget v0, Lh;->jW:I

    :goto_9
    move-object v13, v2

    move v2, v0

    move-object v0, v13

    goto/16 :goto_4

    :cond_b
    sget v0, Lh;->jX:I

    goto :goto_9

    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->z:Lbwt;

    invoke-virtual {v0}, Lbwt;->f()Z

    move-result v0

    if-eqz v0, :cond_d

    sget v0, Lh;->ka:I

    :goto_a
    invoke-static {}, Lec;->a()Lec;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->getContext()Landroid/content/Context;

    move-result-object v7

    new-array v8, v3, [Ljava/lang/Object;

    invoke-static {v6}, Lbzd;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    sget-object v9, Lel;->a:Lek;

    invoke-virtual {v2, v6, v9}, Lec;->a(Ljava/lang/String;Lek;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v8, v1

    invoke-virtual {v7, v0, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object v13, v2

    move v2, v0

    move-object v0, v13

    goto/16 :goto_4

    :cond_d
    sget v0, Lh;->kb:I

    goto :goto_a

    :cond_e
    if-le v0, v3, :cond_f

    sget v0, Lh;->jZ:I

    move-object v13, v2

    move v2, v0

    move-object v0, v13

    goto/16 :goto_4

    :cond_f
    sget v0, Lh;->jX:I

    move-object v13, v2

    move v2, v0

    move-object v0, v13

    goto/16 :goto_4

    :cond_10
    iget v6, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->h:I

    if-ne v6, v3, :cond_11

    sget v0, Lh;->kf:I

    move-object v13, v2

    move v2, v0

    move-object v0, v13

    goto/16 :goto_4

    :cond_11
    if-le v0, v3, :cond_12

    sget v0, Lh;->jY:I

    move-object v13, v2

    move v2, v0

    move-object v0, v13

    goto/16 :goto_4

    :cond_12
    sget v0, Lh;->jV:I

    move-object v13, v2

    move v2, v0

    move-object v0, v13

    goto/16 :goto_4

    :cond_13
    const/4 v0, 0x2

    if-ne v4, v0, :cond_14

    move v0, v3

    :goto_b
    invoke-static {v0}, Lcwz;->a(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v4, Lh;->kp:I

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    :cond_14
    move v0, v1

    goto :goto_b

    :cond_15
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_6

    :cond_16
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setGravity(I)V

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    iget v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->h:I

    if-ne v0, v3, :cond_17

    if-nez v5, :cond_17

    sget v0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->R:I

    :goto_c
    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setHintTextColor(I)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_7

    :cond_17
    sget v0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->S:I

    goto :goto_c

    :cond_18
    sget v0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->Q:I

    goto/16 :goto_8

    :cond_19
    move v0, v1

    goto/16 :goto_3
.end method

.method public a(I)V
    .locals 3

    .prologue
    .line 702
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->z:Lbwt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->m:Lcbl;

    if-eqz v0, :cond_0

    .line 703
    iput p1, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->l:I

    .line 704
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->z:Lbwt;

    iget v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->l:I

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->m:Lcbl;

    .line 705
    invoke-interface {v2}, Lcbl;->a()I

    move-result v2

    invoke-static {v2}, Lf;->b(I)Z

    move-result v2

    .line 704
    invoke-virtual {v1, v0, v2}, Lbwt;->a(ZZ)V

    .line 706
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->w()V

    .line 708
    :cond_0
    return-void

    .line 704
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v2, 0x2

    const/4 v5, 0x0

    const/4 v0, -0x1

    .line 717
    packed-switch p1, :pswitch_data_0

    .line 791
    :cond_0
    :goto_0
    return-void

    .line 719
    :pswitch_0
    if-ne p2, v0, :cond_3

    if-eqz p3, :cond_3

    .line 720
    const-string v0, "photo_url"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 724
    const-string v0, "picasa_photo_id"

    const-wide/16 v2, 0x0

    invoke-virtual {p3, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 725
    const-string v0, "account_gaia_id"

    .line 726
    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 727
    const-string v0, "media_type"

    invoke-virtual {p3, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 729
    sget-boolean v0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a:Z

    if-eqz v0, :cond_1

    .line 730
    const-string v0, "Babel"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "onActivityResult url: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " picasaPhotoId: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " accountGaiaId: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " mediaType: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 736
    :cond_1
    if-nez v1, :cond_2

    .line 739
    invoke-virtual {p3}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v1

    .line 740
    if-nez v1, :cond_2

    .line 741
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 742
    if-eqz v0, :cond_2

    .line 743
    const-string v6, "android.intent.extra.STREAM"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 744
    if-eqz v0, :cond_2

    .line 745
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 750
    :cond_2
    if-eqz v1, :cond_0

    move-object v0, p0

    .line 751
    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a(Ljava/lang/String;JLjava/lang/String;I)V

    goto :goto_0

    .line 754
    :cond_3
    if-ne p2, v2, :cond_0

    .line 755
    const-string v0, "Babel"

    const-string v1, "Handle REQUEST_CHOOSE_PHOTO failure, result=2, cannot access accounts"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 762
    :pswitch_1
    if-ne p2, v0, :cond_0

    .line 763
    new-instance v0, Lcbk;

    iget v1, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->h:I

    const/4 v2, 0x1

    invoke-direct {v0, p0, v1, v2, p3}, Lcbk;-><init>(Lcom/google/android/apps/hangouts/views/ComposeMessageView;IILandroid/content/Intent;)V

    .line 765
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->r:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 766
    new-array v1, v5, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcbk;->executeOnThreadPool([Ljava/lang/Object;)Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;

    goto/16 :goto_0

    .line 771
    :pswitch_2
    if-ne p2, v0, :cond_0

    .line 772
    new-instance v0, Lcbk;

    iget v1, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->h:I

    invoke-direct {v0, p0, v1, v2, p3}, Lcbk;-><init>(Lcom/google/android/apps/hangouts/views/ComposeMessageView;IILandroid/content/Intent;)V

    .line 774
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->r:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 775
    new-array v1, v5, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcbk;->executeOnThreadPool([Ljava/lang/Object;)Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;

    goto/16 :goto_0

    .line 780
    :pswitch_3
    if-ne p2, v0, :cond_0

    if-eqz p3, :cond_0

    .line 781
    const-string v0, "marker_options"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 783
    const-string v1, "camera_position"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/maps/model/CameraPosition;

    .line 786
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->m:Lcbl;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->m:Lcbl;

    invoke-interface {v2, v0, v1}, Lcbl;->a(Lcom/google/android/gms/maps/model/MarkerOptions;Lcom/google/android/gms/maps/model/CameraPosition;)V

    goto/16 :goto_0

    .line 717
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(Lbwt;)V
    .locals 0

    .prologue
    .line 1400
    iput-object p1, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->z:Lbwt;

    .line 1401
    return-void
.end method

.method public a(Lcbl;)V
    .locals 0

    .prologue
    .line 908
    iput-object p1, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->m:Lcbl;

    .line 909
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 4

    .prologue
    .line 930
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v0

    .line 931
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v1

    .line 932
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-interface {v2, v3, v0, p1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 934
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 918
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 919
    if-eqz p2, :cond_0

    .line 920
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->selectAll()V

    .line 922
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a()V

    .line 923
    return-void
.end method

.method public a(Lt;Lyj;)V
    .locals 1

    .prologue
    .line 694
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->t()V

    .line 695
    iput-object p1, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->t:Lt;

    .line 696
    iput-object p2, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->u:Lyj;

    .line 697
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->g:Lcom/google/android/apps/hangouts/views/TransportSpinner;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->a(Lyj;)V

    .line 698
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->w()V

    .line 699
    return-void
.end method

.method public a([Lajk;)V
    .locals 1

    .prologue
    .line 689
    sget v0, Lg;->hM:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/TransportSpinner;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->a([Lajk;)V

    .line 690
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->s()V

    .line 691
    return-void
.end method

.method public b()Lcom/google/android/apps/hangouts/views/TransportSpinner;
    .locals 1

    .prologue
    .line 685
    sget v0, Lg;->hM:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/TransportSpinner;

    return-object v0
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 1171
    iput p1, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->h:I

    .line 1172
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a()V

    .line 1173
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 885
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 886
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 896
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 897
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    invoke-static {v0}, Lf;->a(Landroid/view/View;)V

    .line 898
    const/4 v0, 0x1

    .line 900
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 904
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 905
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 912
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->m:Lcbl;

    .line 913
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->p:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->H:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 914
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->p:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->I:Lcbm;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 915
    return-void
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 926
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()V
    .locals 4

    .prologue
    .line 937
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    new-instance v1, Landroid/view/KeyEvent;

    const/4 v2, 0x0

    const/16 v3, 0x43

    invoke-direct {v1, v2, v3}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    .line 938
    return-void
.end method

.method public i()V
    .locals 4

    .prologue
    .line 941
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    new-instance v1, Landroid/view/KeyEvent;

    const/4 v2, 0x0

    const/16 v3, 0x3e

    invoke-direct {v1, v2, v3}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    .line 942
    return-void
.end method

.method public j()V
    .locals 1

    .prologue
    .line 945
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->w:Z

    .line 946
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->w()V

    .line 947
    return-void
.end method

.method public k()V
    .locals 0

    .prologue
    .line 1177
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a()V

    .line 1178
    return-void
.end method

.method public l()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1404
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->m:Lcbl;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->m:Lcbl;

    .line 1405
    invoke-interface {v0}, Lcbl;->a()I

    move-result v0

    invoke-static {v0}, Lf;->c(I)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 1406
    :goto_0
    if-eqz v0, :cond_4

    .line 1407
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->A:Landroid/view/View$OnLongClickListener;

    if-nez v0, :cond_0

    .line 1408
    new-instance v0, Lcbi;

    invoke-direct {v0, p0}, Lcbi;-><init>(Lcom/google/android/apps/hangouts/views/ComposeMessageView;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->A:Landroid/view/View$OnLongClickListener;

    .line 1416
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->f:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->A:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1417
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->d:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->A:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1419
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->B:[Landroid/text/InputFilter;

    if-nez v0, :cond_1

    .line 1420
    new-array v0, v1, [Landroid/text/InputFilter;

    new-instance v1, Landroid/text/InputFilter$LengthFilter;

    .line 1421
    invoke-static {}, Lbvm;->a()Lsm;

    move-result-object v3

    invoke-virtual {v3}, Lsm;->n()I

    move-result v3

    invoke-direct {v1, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->B:[Landroid/text/InputFilter;

    .line 1423
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->B:[Landroid/text/InputFilter;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 1429
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->m:Lcbl;

    if-eqz v0, :cond_2

    .line 1430
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->g:Lcom/google/android/apps/hangouts/views/TransportSpinner;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->m:Lcbl;

    invoke-interface {v1}, Lcbl;->b()Lajk;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->a(Lajk;)V

    .line 1433
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a()V

    .line 1434
    return-void

    :cond_3
    move v0, v2

    .line 1405
    goto :goto_0

    .line 1425
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->f:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1426
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    sget-object v1, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->C:[Landroid/text/InputFilter;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    goto :goto_1
.end method

.method public m()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1502
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->D:Z

    .line 1503
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 1504
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 1505
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 1506
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->T:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1508
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->f:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    .line 1509
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->f:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1511
    :cond_1
    return-void
.end method

.method public n()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1517
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->D:Z

    if-eqz v0, :cond_1

    .line 1518
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->D:Z

    .line 1519
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 1520
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 1521
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 1522
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1524
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->f:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    .line 1525
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->f:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1528
    :cond_1
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 872
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->t()V

    .line 873
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 874
    return-void
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 982
    const/4 v0, 0x4

    if-eq p2, v0, :cond_0

    if-eqz p3, :cond_1

    .line 983
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    .line 984
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 985
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->x()V

    .line 987
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 608
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->m:Lcbl;

    if-eqz v0, :cond_1

    .line 609
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    .line 610
    instance-of v1, v0, Lajk;

    if-eqz v1, :cond_1

    .line 611
    check-cast v0, Lajk;

    .line 615
    iget-object v1, v0, Lajk;->b:Lbdh;

    if-nez v1, :cond_0

    iget-object v1, v0, Lajk;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 616
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->m:Lcbl;

    invoke-interface {v1, v0}, Lcbl;->a(Lajk;)V

    .line 621
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->s()V

    .line 622
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 641
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0

    .prologue
    .line 1438
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onWindowFocusChanged(Z)V

    .line 1439
    if-eqz p1, :cond_0

    .line 1441
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->u()V

    .line 1443
    :cond_0
    return-void
.end method

.method public setVisibility(I)V
    .locals 1

    .prologue
    .line 1308
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1313
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1314
    return-void
.end method
