.class public Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;
.super Lakn;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Lbrn;


# static fields
.field private static final r:Z


# instance fields
.field private A:Z

.field private B:I

.field private C:J

.field private D:Z

.field private E:Z

.field private F:Z

.field private G:Landroid/net/Uri;

.field private H:Z

.field private I:I

.field private J:I

.field private final K:Lbor;

.field private s:Landroid/content/Intent;

.field private t:Lyj;

.field private u:[Ljava/lang/String;

.field private v:[Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lbys;->g:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->r:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 61
    invoke-direct {p0}, Lakn;-><init>()V

    .line 98
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->u:[Ljava/lang/String;

    .line 99
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->v:[Ljava/lang/String;

    .line 283
    new-instance v0, Lawa;

    invoke-direct {v0, p0}, Lawa;-><init>(Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->K:Lbor;

    .line 560
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;)I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->J:I

    return v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 212
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 213
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 214
    const-string v1, "account_name"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 215
    const-string v1, "conversation_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 216
    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 217
    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/app/PendingIntent;I)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 235
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 236
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 237
    const-string v1, "account_name"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 239
    if-eqz p1, :cond_1

    .line 240
    const-string v1, "participant_gaia"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 241
    const-string v1, "participant_name"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 242
    const-string v1, "start_video"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 248
    :goto_0
    if-eqz p4, :cond_0

    .line 249
    const-string v1, "hangout_call_end_intent"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 251
    :cond_0
    const-string v1, "hangout_auto_join"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 252
    const-string v1, "hangout_start_source"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 254
    const-string v1, "extra_hangout_start_time"

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 255
    return-object v0

    .line 244
    :cond_1
    invoke-static {p3}, Lcwz;->b(Ljava/lang/Object;)V

    .line 245
    const-string v1, "hangout_uri"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZZ)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 169
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 170
    const-string v1, "android.intent.action.SENDTO"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 171
    const-string v1, "account_name"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 172
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 173
    const-string v1, "conversation_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 178
    :goto_0
    const-string v1, "start_video"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 179
    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 180
    const-string v1, "wearable_watermark"

    invoke-virtual {v0, v1, p5, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 181
    const-string v1, "is_sms"

    invoke-virtual {v0, v1, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 182
    const-string v1, "requires_mms"

    invoke-virtual {v0, v1, p8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 183
    return-object v0

    .line 175
    :cond_0
    const-string v1, "participant_gaia"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 176
    const-string v1, "participant_name"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 140
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 141
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 142
    const-string v1, "account_name"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 143
    const-string v1, "participant_gaia"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 144
    const-string v1, "participant_name"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 145
    const-string v1, "start_video"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 146
    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 147
    const-string v1, "force_group"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 148
    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 195
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 196
    const-string v1, "com.google.android.apps.babel.realtimechat.update-watermark"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 197
    const-string v1, "account_name"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 198
    const-string v1, "conversation_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 199
    const-string v1, "is_sms"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 200
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->w:Ljava/lang/String;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;I)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->c(I)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->v:[Ljava/lang/String;

    return-object p1
.end method

.method public static b(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 120
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 121
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 122
    const-string v1, "account_name"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 123
    return-object v0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/app/PendingIntent;I)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 273
    invoke-static/range {p0 .. p5}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/app/PendingIntent;I)Landroid/content/Intent;

    move-result-object v0

    .line 275
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 276
    const-string v1, "hangout_auto_join"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 277
    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;I)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->d(I)V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;)Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->F:Z

    return v0
.end method

.method public static synthetic c(Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;)Lyj;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->t:Lyj;

    return-object v0
.end method

.method private c(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 731
    invoke-static {p0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 732
    invoke-virtual {p0, v1}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->setResult(I)V

    .line 733
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->finish()V

    .line 734
    return-void
.end method

.method private c(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 555
    invoke-virtual {p0, p1}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->startActivity(Landroid/content/Intent;)V

    .line 556
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->setResult(I)V

    .line 557
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->finish()V

    .line 558
    return-void
.end method

.method public static synthetic c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 61
    invoke-static {p0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->d(Ljava/lang/String;)V

    return-void
.end method

.method private d(I)V
    .locals 16

    .prologue
    .line 759
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->y:Z

    if-eqz v1, :cond_4

    .line 760
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->x:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcwz;->a(Z)V

    .line 762
    move-object/from16 v0, p0

    iget-wide v1, v0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->C:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    .line 763
    new-instance v1, Lyt;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->t:Lyj;

    invoke-direct {v1, v2}, Lyt;-><init>(Lyj;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->w:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->C:J

    invoke-virtual {v1, v2, v3, v4}, Lyt;->d(Ljava/lang/String;J)I

    .line 766
    :cond_0
    sget-boolean v1, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->r:Z

    if-eqz v1, :cond_1

    .line 767
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Auto-sending message to conversation "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->w:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->d(Ljava/lang/String;)V

    .line 769
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lbqz;

    invoke-static {v1, v2}, Lcyj;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbqz;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->t:Lyj;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->w:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->x:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->E:Z

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->B:I

    invoke-interface/range {v1 .. v15}, Lbqz;->a(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IILjava/lang/String;Ljava/lang/String;ZLcom/google/android/gms/maps/model/MarkerOptions;Lcom/google/android/gms/maps/model/CameraPosition;I)V

    .line 775
    move-object/from16 v0, p0

    iget-wide v1, v0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->C:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_2

    .line 776
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->t:Lyj;

    const-wide/16 v2, 0x0

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->D:Z

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;JIZ)V

    .line 779
    :cond_2
    const/4 v1, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->setResult(I)V

    .line 780
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->finish()V

    .line 796
    :goto_1
    return-void

    .line 760
    :cond_3
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 781
    :cond_4
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->z:Z

    if-eqz v1, :cond_5

    .line 782
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->t:Lyj;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->w:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->i(Lyj;Ljava/lang/String;)I

    .line 783
    const/4 v1, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->setResult(I)V

    .line 784
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->finish()V

    goto :goto_1

    .line 786
    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->t:Lyj;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->w:Ljava/lang/String;

    move/from16 v0, p1

    invoke-static {v1, v2, v0}, Lbbl;->a(Lyj;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    .line 788
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->x:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 789
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 790
    const-string v3, "android.intent.extra.TEXT"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->x:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 791
    const-string v3, "conversation_id"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->w:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 792
    const-string v3, "share_intent"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 794
    :cond_6
    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->c(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method private static d(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 727
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[BabelGatewayActivity] "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 728
    return-void
.end method

.method public static synthetic d(Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;)Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->H:Z

    return v0
.end method

.method public static synthetic e(Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;)I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->I:I

    return v0
.end method

.method public static synthetic f(Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->u:[Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic g(Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->v:[Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic h(Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->q()V

    return-void
.end method

.method public static synthetic n()Z
    .locals 1

    .prologue
    .line 61
    sget-boolean v0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->r:Z

    return v0
.end method

.method private o()V
    .locals 3

    .prologue
    .line 393
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->G:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 394
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->s:Landroid/content/Intent;

    const-string v1, "hangout_call_end_intent"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    .line 396
    if-eqz v0, :cond_0

    .line 398
    :try_start_0
    invoke-virtual {v0}, Landroid/app/PendingIntent;->send()V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    .line 404
    :cond_0
    :goto_0
    return-void

    .line 399
    :catch_0
    move-exception v0

    .line 400
    const-string v1, "Babel"

    const-string v2, "Call complete intent could not be sent"

    invoke-static {v1, v2, v0}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private p()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 407
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->t:Lyj;

    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 408
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->G:Landroid/net/Uri;

    if-eqz v0, :cond_2

    .line 409
    iget-object v3, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->G:Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->s:Landroid/content/Intent;

    const-string v2, "hangout_call_end_intent"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/app/PendingIntent;

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->t:Lyj;

    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0, v2}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->fromUri(Landroid/net/Uri;Ljava/lang/String;Landroid/app/PendingIntent;)Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "Babel"

    const-string v1, "invalid hangout request"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v2, :cond_0

    :try_start_0
    invoke-virtual {v2}, Landroid/app/PendingIntent;->send()V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    sget v0, Lh;->dS:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->c(I)V

    .line 416
    :goto_1
    return-void

    .line 409
    :catch_0
    move-exception v0

    const-string v1, "Babel"

    const-string v2, "callCompletionIntent failed"

    invoke-static {v1, v2, v0}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_1
    iget-boolean v3, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->H:Z

    iget v4, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->I:I

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    move-object v2, v1

    invoke-static/range {v0 .. v6}, Lbbl;->a(Lcom/google/android/libraries/hangouts/video/HangoutRequest;Ljava/util/ArrayList;Ljava/util/ArrayList;ZIJ)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->startActivity(Landroid/content/Intent;)V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->finish()V

    goto :goto_1

    .line 410
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->w:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->u:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_8

    .line 411
    :cond_3
    sget-boolean v0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->r:Z

    if-eqz v0, :cond_4

    const-string v0, "getConversation"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->d(Ljava/lang/String;)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->t:Lyj;

    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->w:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->t:Lyj;

    invoke-static {v0}, Lbrk;->a(Lyj;)Lbrk;

    move-result-object v0

    new-instance v1, Lyf;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->w:Ljava/lang/String;

    invoke-direct {v1, v2, p0}, Lyf;-><init>(Ljava/lang/String;Lbrn;)V

    invoke-virtual {v0, v1}, Lbrk;->a(Lbrv;)Z

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->u:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->v:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_6

    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->q()V

    goto :goto_1

    :cond_6
    const-string v0, "Babel"

    const-string v1, "BabelGatewayActivity.createConversation: incoming intent has no participant name"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lawb;

    invoke-direct {v0, p0}, Lawb;-><init>(Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;)V

    invoke-virtual {v0}, Lawb;->g()V

    goto :goto_1

    :cond_7
    const-string v0, "BabelGatewayActivity.createConversation: Participant id is null and conversation id is null"

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 413
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->t:Lyj;

    invoke-static {v0}, Lbbl;->b(Lyj;)Landroid/content/Intent;

    move-result-object v0

    .line 414
    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->c(Landroid/content/Intent;)V

    goto/16 :goto_1
.end method

.method private q()V
    .locals 5

    .prologue
    .line 641
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->u:[Ljava/lang/String;

    array-length v0, v0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->v:[Ljava/lang/String;

    array-length v1, v1

    if-eq v0, v1, :cond_0

    .line 642
    const-string v0, "BabelGatewayActivity.createConversationWithParticipants: different number of participant gaia ids and names"

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    .line 665
    :goto_0
    return-void

    .line 647
    :cond_0
    invoke-static {}, Lxm;->newBuilder()Lxn;

    move-result-object v1

    .line 648
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->u:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 649
    invoke-static {}, Lxs;->newBuilder()Lxt;

    move-result-object v2

    .line 650
    iget-object v3, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->v:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Lxt;->a(Ljava/lang/String;)V

    .line 651
    iget-object v3, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->u:[Ljava/lang/String;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->v:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-static {v3, v4}, Lbcx;->a(Ljava/lang/String;Ljava/lang/String;)Lbcx;

    move-result-object v3

    .line 653
    invoke-virtual {v2, v3}, Lxt;->a(Lbcx;)V

    .line 654
    invoke-virtual {v2}, Lxt;->a()Lxs;

    move-result-object v2

    invoke-virtual {v1, v2}, Lxn;->a(Lxs;)Lxn;

    .line 648
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 657
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->K:Lbor;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbor;)V

    .line 662
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->t:Lyj;

    .line 663
    invoke-virtual {v1}, Lxn;->a()Lxm;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->A:Z

    iget-boolean v3, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->F:Z

    iget-boolean v4, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->A:Z

    .line 662
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Lxm;ZZ)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->J:I

    goto :goto_0
.end method


# virtual methods
.method public a(Lyv;)V
    .locals 1

    .prologue
    .line 742
    iget-object v0, p1, Lyv;->s:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->w:Ljava/lang/String;

    .line 743
    iget v0, p1, Lyv;->b:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->d(I)V

    .line 744
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 751
    sget v0, Lh;->cH:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->c(I)V

    .line 752
    return-void
.end method

.method public k()Lyj;
    .locals 1

    .prologue
    .line 723
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->t:Lyj;

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 500
    invoke-super {p0, p1, p2, p3}, Lakn;->onActivityResult(IILandroid/content/Intent;)V

    .line 501
    sget-boolean v0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->r:Z

    if-eqz v0, :cond_0

    .line 502
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onActivityResult "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " result "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->d(Ljava/lang/String;)V

    .line 504
    :cond_0
    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_1

    .line 505
    const/4 v0, -0x1

    if-ne p2, v0, :cond_2

    .line 506
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->p()V

    .line 515
    :cond_1
    :goto_0
    return-void

    .line 510
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->o()V

    .line 512
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->setResult(I)V

    .line 513
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->finish()V

    goto :goto_0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 712
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->setResult(I)V

    .line 713
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->finish()V

    .line 714
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .prologue
    .line 703
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->setResult(I)V

    .line 704
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->finish()V

    .line 705
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 325
    sget-boolean v0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->r:Z

    if-eqz v0, :cond_0

    .line 326
    const-string v0, "BabelGatewayActivity.onCreate()"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->d(Ljava/lang/String;)V

    .line 328
    :cond_0
    invoke-super {p0, p1}, Lakn;->onCreate(Landroid/os/Bundle;)V

    .line 334
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->s:Landroid/content/Intent;

    .line 336
    sget-boolean v0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->r:Z

    if-eqz v0, :cond_1

    .line 337
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "intent.getType() = "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->s:Landroid/content/Intent;

    invoke-virtual {v4}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->d(Ljava/lang/String;)V

    .line 340
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->s:Landroid/content/Intent;

    const-string v4, "participant_gaia"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 341
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 342
    const-string v4, "\\|"

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->u:[Ljava/lang/String;

    .line 344
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->s:Landroid/content/Intent;

    const-string v4, "participant_name"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 345
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 346
    const-string v4, "\\|"

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->v:[Ljava/lang/String;

    .line 348
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->s:Landroid/content/Intent;

    const-string v4, "conversation_id"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->w:Ljava/lang/String;

    .line 349
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->s:Landroid/content/Intent;

    const-string v4, "android.intent.extra.TEXT"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->x:Ljava/lang/String;

    .line 350
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->s:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v4, "android.intent.action.SENDTO"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->y:Z

    .line 351
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->s:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v4, "com.google.android.apps.babel.realtimechat.update-watermark"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->z:Z

    .line 352
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->s:Landroid/content/Intent;

    const-string v4, "otr_state"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 353
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->s:Landroid/content/Intent;

    const-string v4, "otr_state"

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 355
    iput v2, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->B:I

    .line 364
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->s:Landroid/content/Intent;

    const-string v4, "wearable_watermark"

    const-wide/16 v5, 0x0

    .line 365
    invoke-virtual {v0, v4, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->C:J

    .line 366
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->s:Landroid/content/Intent;

    const-string v4, "is_sms"

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->D:Z

    .line 367
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->s:Landroid/content/Intent;

    const-string v4, "requires_mms"

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->E:Z

    .line 368
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->s:Landroid/content/Intent;

    const-string v4, "start_video"

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->F:Z

    .line 369
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->s:Landroid/content/Intent;

    const-string v4, "force_group"

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->A:Z

    .line 370
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->s:Landroid/content/Intent;

    const-string v4, "hangout_uri"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->G:Landroid/net/Uri;

    .line 371
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->s:Landroid/content/Intent;

    const-string v4, "hangout_auto_join"

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->H:Z

    .line 372
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->s:Landroid/content/Intent;

    const-string v4, "hangout_start_source"

    const/16 v5, 0x33

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->I:I

    .line 375
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->s:Landroid/content/Intent;

    const-string v4, "account_name"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_8

    move v0, v1

    .line 376
    :goto_1
    sget-boolean v2, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->r:Z

    if-eqz v2, :cond_4

    .line 377
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "checkAccount returned "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->d(Ljava/lang/String;)V

    .line 380
    :cond_4
    if-nez v0, :cond_c

    .line 381
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->p()V

    .line 387
    :cond_5
    :goto_2
    return-void

    .line 357
    :cond_6
    iput v1, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->B:I

    goto/16 :goto_0

    .line 361
    :cond_7
    iput v3, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->B:I

    goto/16 :goto_0

    .line 375
    :cond_8
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->t:Lyj;

    iget-object v4, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->t:Lyj;

    if-nez v4, :cond_9

    sget v2, Lh;->da:I

    new-array v4, v1, [Ljava/lang/Object;

    aput-object v0, v4, v3

    invoke-virtual {p0, v2, v4}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v4, "error_title"

    sget v5, Lh;->db:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "error_message"

    invoke-virtual {v2, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v3, v2}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->showDialog(ILandroid/os/Bundle;)Z

    move v0, v1

    goto :goto_1

    :cond_9
    iget-object v4, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->t:Lyj;

    invoke-static {v4}, Lbkb;->f(Lyj;)I

    move-result v4

    const/16 v5, 0x66

    if-eq v4, v5, :cond_b

    sget-boolean v4, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->r:Z

    if-eqz v4, :cond_a

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "account "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " is in state "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->t:Lyj;

    invoke-static {v4}, Lbkb;->f(Lyj;)I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->d(Ljava/lang/String;)V

    :cond_a
    const/4 v0, 0x0

    invoke-static {v0}, Lbbl;->a(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    const-string v4, "account_name"

    iget-object v5, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->t:Lyj;

    invoke-virtual {v5}, Lyj;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "try_other_accounts"

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/16 v3, 0x3e8

    invoke-virtual {p0, v0, v3}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->startActivityForResult(Landroid/content/Intent;I)V

    move v0, v2

    goto/16 :goto_1

    :cond_b
    move v0, v3

    goto/16 :goto_1

    .line 383
    :cond_c
    if-ne v0, v1, :cond_5

    .line 384
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->o()V

    goto/16 :goto_2
.end method

.method public onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 681
    if-nez p2, :cond_0

    move-object v2, v0

    .line 682
    :goto_0
    if-nez p2, :cond_1

    move-object v1, v0

    .line 684
    :goto_1
    packed-switch p1, :pswitch_data_0

    .line 695
    :goto_2
    return-object v0

    .line 681
    :cond_0
    const-string v1, "error_title"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    goto :goto_0

    .line 682
    :cond_1
    const-string v1, "error_message"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 686
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 687
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 688
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 689
    sget v1, Lh;->hZ:I

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 690
    invoke-virtual {v0, p0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 691
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_2

    .line 684
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 669
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelGatewayActivity;->K:Lbor;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lbor;)V

    .line 670
    invoke-super {p0}, Lakn;->onDestroy()V

    .line 671
    return-void
.end method
