.class public Lcom/google/android/apps/hangouts/views/RichStatusView;
.super Landroid/widget/LinearLayout;
.source "PG"


# static fields
.field private static final a:Z

.field private static e:I

.field private static f:I

.field private static g:I

.field private static volatile k:Z


# instance fields
.field private final b:[Lceh;

.field private c:Landroid/graphics/drawable/AnimationDrawable;

.field private d:Lcca;

.field private h:Lbdk;

.field private i:Lbdn;

.field private j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lbys;->s:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/hangouts/views/RichStatusView;->a:Z

    .line 72
    const/4 v0, -0x1

    sput v0, Lcom/google/android/apps/hangouts/views/RichStatusView;->g:I

    .line 78
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/apps/hangouts/views/RichStatusView;->k:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/hangouts/views/RichStatusView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 83
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 86
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 63
    const/4 v0, 0x4

    new-array v0, v0, [Lceh;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->b:[Lceh;

    .line 87
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->h:Lbdk;

    .line 88
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->j:Z

    .line 91
    sget v0, Lcom/google/android/apps/hangouts/views/RichStatusView;->g:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 93
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/RichStatusView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lf;->dU:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/hangouts/views/RichStatusView;->e:I

    .line 96
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/RichStatusView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lf;->dT:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/hangouts/views/RichStatusView;->f:I

    .line 98
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/RichStatusView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lf;->dz:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/hangouts/views/RichStatusView;->g:I

    .line 101
    :cond_0
    return-void
.end method

.method private a(Lceh;Z)V
    .locals 3

    .prologue
    .line 442
    iget-boolean v0, p1, Lceh;->c:Z

    if-eq v0, p2, :cond_0

    .line 443
    new-instance v0, Lceg;

    sget v1, Lcom/google/android/apps/hangouts/views/RichStatusView;->g:I

    invoke-direct {v0, p0, p1, v1, p2}, Lceg;-><init>(Lcom/google/android/apps/hangouts/views/RichStatusView;Lceh;IZ)V

    .line 445
    sget v1, Lcom/google/android/apps/hangouts/views/RichStatusView;->e:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lceg;->setDuration(J)V

    .line 446
    new-instance v1, Lcef;

    invoke-direct {v1, p0, p1, p2}, Lcef;-><init>(Lcom/google/android/apps/hangouts/views/RichStatusView;Lceh;Z)V

    invoke-virtual {v0, v1}, Lceg;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 448
    iget-object v1, p1, Lceh;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 449
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/RichStatusView;->e()V

    .line 450
    iput-boolean p2, p1, Lceh;->c:Z

    .line 452
    :cond_0
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/views/RichStatusView;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/RichStatusView;->e()V

    return-void
.end method

.method private b(Lbdn;)V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 236
    invoke-static {p1}, Lcwz;->b(Ljava/lang/Object;)V

    .line 237
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->j:Z

    invoke-static {v0}, Lcwz;->a(Z)V

    .line 239
    iput-object p1, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->i:Lbdn;

    .line 241
    sget-boolean v0, Lcom/google/android/apps/hangouts/views/RichStatusView;->a:Z

    if-eqz v0, :cond_0

    .line 242
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Updating Presence for participant: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->h:Lbdk;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 243
    invoke-virtual {p1}, Lbdn;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 242
    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    :cond_0
    iget v0, p1, Lbdn;->f:I

    if-nez v0, :cond_2

    .line 247
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->b:[Lceh;

    aget-object v0, v0, v3

    invoke-direct {p0, v0, v5}, Lcom/google/android/apps/hangouts/views/RichStatusView;->a(Lceh;Z)V

    .line 259
    :goto_0
    iget-object v0, p1, Lbdn;->h:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lbdn;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 260
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->b:[Lceh;

    aget-object v0, v0, v6

    invoke-direct {p0, v0, v5}, Lcom/google/android/apps/hangouts/views/RichStatusView;->a(Lceh;Z)V

    .line 272
    :goto_1
    iget v0, p1, Lbdn;->g:I

    packed-switch v0, :pswitch_data_0

    .line 290
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->b:[Lceh;

    aget-object v0, v0, v4

    invoke-direct {p0, v0, v5}, Lcom/google/android/apps/hangouts/views/RichStatusView;->a(Lceh;Z)V

    .line 293
    :goto_2
    return-void

    .line 249
    :cond_2
    iget v0, p1, Lbdn;->f:I

    if-ne v0, v3, :cond_3

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->b:[Lceh;

    aget-object v0, v0, v3

    iget-object v0, v0, Lceh;->b:Landroid/view/View;

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->ay:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 256
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->b:[Lceh;

    aget-object v0, v0, v3

    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/hangouts/views/RichStatusView;->a(Lceh;Z)V

    goto :goto_0

    .line 253
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->b:[Lceh;

    aget-object v0, v0, v3

    iget-object v0, v0, Lceh;->b:Landroid/view/View;

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->aD:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_3

    .line 262
    :cond_4
    iget-object v0, p1, Lbdn;->h:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    .line 263
    invoke-static {}, Lccc;->a()Lccc;

    move-result-object v1

    invoke-virtual {v1, v0}, Lccc;->a(I)I

    move-result v0

    .line 264
    const/4 v1, -0x1

    if-eq v0, v1, :cond_5

    .line 265
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->b:[Lceh;

    aget-object v1, v1, v6

    iget-object v1, v1, Lceh;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 266
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->b:[Lceh;

    aget-object v0, v0, v6

    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/hangouts/views/RichStatusView;->a(Lceh;Z)V

    goto :goto_1

    .line 268
    :cond_5
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Couldn\'t find Emoji for mood: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lbdn;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 274
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->b:[Lceh;

    aget-object v0, v0, v4

    iget-object v0, v0, Lceh;->b:Landroid/view/View;

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->aB:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 276
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->b:[Lceh;

    aget-object v0, v0, v4

    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/hangouts/views/RichStatusView;->a(Lceh;Z)V

    goto :goto_2

    .line 279
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->b:[Lceh;

    aget-object v0, v0, v4

    iget-object v0, v0, Lceh;->b:Landroid/view/View;

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->aC:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 281
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->b:[Lceh;

    aget-object v0, v0, v4

    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/hangouts/views/RichStatusView;->a(Lceh;Z)V

    goto/16 :goto_2

    .line 284
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->b:[Lceh;

    aget-object v0, v0, v4

    iget-object v0, v0, Lceh;->b:Landroid/view/View;

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->aA:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 286
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->b:[Lceh;

    aget-object v0, v0, v4

    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/hangouts/views/RichStatusView;->a(Lceh;Z)V

    goto/16 :goto_2

    .line 272
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static synthetic d()I
    .locals 1

    .prologue
    .line 37
    sget v0, Lcom/google/android/apps/hangouts/views/RichStatusView;->g:I

    return v0
.end method

.method private e()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    move v0, v1

    .line 332
    :goto_0
    const/4 v2, 0x4

    if-ge v0, v2, :cond_2

    .line 333
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->b:[Lceh;

    aget-object v2, v2, v0

    iget-object v2, v2, Lceh;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 334
    const/4 v0, 0x1

    .line 339
    :goto_1
    if-eqz v0, :cond_1

    .line 340
    invoke-virtual {p0, v1}, Lcom/google/android/apps/hangouts/views/RichStatusView;->setVisibility(I)V

    .line 344
    :goto_2
    return-void

    .line 332
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 342
    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/RichStatusView;->setVisibility(I)V

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public a(Lbdn;)J
    .locals 7

    .prologue
    const-wide/16 v2, 0x0

    .line 182
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->i:Lbdn;

    .line 184
    sget v0, Lcom/google/android/apps/hangouts/views/RichStatusView;->f:I

    int-to-long v0, v0

    .line 186
    sget-boolean v4, Lcom/google/android/apps/hangouts/views/RichStatusView;->k:Z

    if-eqz v4, :cond_0

    iget-boolean v4, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->j:Z

    if-nez v4, :cond_1

    :cond_0
    move-wide v0, v2

    .line 228
    :goto_0
    return-wide v0

    .line 190
    :cond_1
    if-nez p1, :cond_5

    .line 191
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->h:Lbdk;

    if-nez v0, :cond_2

    move-wide v0, v2

    .line 192
    goto :goto_0

    .line 195
    :cond_2
    invoke-static {}, Lbzh;->b()Lbzh;

    move-result-object v4

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->h:Lbdk;

    iget-object v0, v0, Lbdk;->a:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lbzh;->a(Ljava/lang/String;)J

    move-result-wide v0

    .line 199
    cmp-long v5, v0, v2

    if-lez v5, :cond_3

    sget v5, Lcom/google/android/apps/hangouts/views/RichStatusView;->f:I

    int-to-long v5, v5

    cmp-long v5, v0, v5

    if-gez v5, :cond_3

    .line 201
    sget v5, Lcom/google/android/apps/hangouts/views/RichStatusView;->f:I

    int-to-long v5, v5

    sub-long v0, v5, v0

    .line 209
    invoke-static {}, Lbkb;->j()Lyj;

    move-result-object v5

    .line 210
    if-nez v5, :cond_4

    move-wide v0, v2

    .line 213
    goto :goto_0

    :cond_3
    move-wide v0, v2

    .line 204
    goto :goto_0

    .line 216
    :cond_4
    iget-object v6, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->h:Lbdk;

    iget-object v6, v6, Lbdk;->a:Ljava/lang/String;

    invoke-virtual {v4, v6, v5}, Lbzh;->a(Ljava/lang/String;Lyj;)Lbdn;

    move-result-object p1

    .line 217
    if-nez p1, :cond_5

    move-wide v0, v2

    .line 218
    goto :goto_0

    .line 222
    :cond_5
    iget-boolean v4, p1, Lbdn;->e:Z

    if-nez v4, :cond_6

    move-wide v0, v2

    .line 223
    goto :goto_0

    .line 226
    :cond_6
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/views/RichStatusView;->b(Lbdn;)V

    goto :goto_0
.end method

.method public a()V
    .locals 4

    .prologue
    .line 153
    sget-boolean v0, Lcom/google/android/apps/hangouts/views/RichStatusView;->k:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->j:Z

    if-nez v0, :cond_1

    .line 176
    :cond_0
    :goto_0
    return-void

    .line 159
    :cond_1
    invoke-static {}, Lbkb;->j()Lyj;

    move-result-object v0

    .line 161
    if-eqz v0, :cond_3

    .line 163
    sget-boolean v1, Lcom/google/android/apps/hangouts/views/RichStatusView;->a:Z

    if-eqz v1, :cond_2

    .line 164
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Requesting rich status for participant: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->h:Lbdk;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "from account: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->h:Lbdk;

    if-eqz v1, :cond_0

    .line 168
    invoke-static {}, Lbzh;->b()Lbzh;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->h:Lbdk;

    iget-object v2, v2, Lbdk;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lbzh;->b(Ljava/lang/String;Lyj;)V

    goto :goto_0

    .line 172
    :cond_3
    sget-boolean v0, Lcom/google/android/apps/hangouts/views/RichStatusView;->a:Z

    if-eqz v0, :cond_0

    .line 173
    const-string v0, "Babel"

    const-string v1, "Dropping rich status request due to null account."

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lbdk;)V
    .locals 3

    .prologue
    .line 105
    iput-object p1, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->h:Lbdk;

    .line 107
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/RichStatusView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "babel_richstatus"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcww;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/apps/hangouts/views/RichStatusView;->k:Z

    .line 110
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 113
    iput-boolean p1, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->j:Z

    .line 114
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 232
    new-instance v0, Lbdn;

    invoke-direct {v0}, Lbdn;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/views/RichStatusView;->b(Lbdn;)V

    .line 233
    return-void
.end method

.method public b(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 296
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->b:[Lceh;

    aget-object v0, v0, v1

    iget-boolean v0, v0, Lceh;->c:Z

    if-eq v0, p1, :cond_0

    .line 297
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->b:[Lceh;

    aget-object v0, v0, v1

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/hangouts/views/RichStatusView;->a(Lceh;Z)V

    .line 299
    if-eqz p1, :cond_1

    .line 300
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->c:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 306
    :cond_0
    :goto_0
    return-void

    .line 303
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->c:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    goto :goto_0
.end method

.method public c()Lbdn;
    .locals 1

    .prologue
    .line 455
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->i:Lbdn;

    return-object v0
.end method

.method public c(Z)V
    .locals 3

    .prologue
    .line 310
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 311
    if-eqz p1, :cond_0

    const v0, 0x3ecccccd    # 0.4f

    :goto_0
    const/4 v1, 0x0

    :goto_1
    const/4 v2, 0x4

    if-ge v1, v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->b:[Lceh;

    aget-object v2, v2, v1

    iget-object v2, v2, Lceh;->b:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setAlpha(F)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0

    .line 313
    :cond_1
    return-void
.end method

.method public onFinishInflate()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 118
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->b:[Lceh;

    new-instance v1, Lceh;

    sget v2, Lg;->gt:I

    sget v3, Lg;->bI:I

    invoke-direct {v1, p0, v2, v3}, Lceh;-><init>(Lcom/google/android/apps/hangouts/views/RichStatusView;II)V

    aput-object v1, v0, v5

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->b:[Lceh;

    const/4 v1, 0x1

    new-instance v2, Lceh;

    sget v3, Lg;->gp:I

    sget v4, Lg;->go:I

    invoke-direct {v2, p0, v3, v4}, Lceh;-><init>(Lcom/google/android/apps/hangouts/views/RichStatusView;II)V

    aput-object v2, v0, v1

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->b:[Lceh;

    new-instance v1, Lceh;

    sget v2, Lg;->gn:I

    sget v3, Lg;->gm:I

    invoke-direct {v1, p0, v2, v3}, Lceh;-><init>(Lcom/google/android/apps/hangouts/views/RichStatusView;II)V

    aput-object v1, v0, v6

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->b:[Lceh;

    const/4 v1, 0x3

    new-instance v2, Lceh;

    sget v3, Lg;->gs:I

    sget v4, Lg;->gr:I

    invoke-direct {v2, p0, v3, v4}, Lceh;-><init>(Lcom/google/android/apps/hangouts/views/RichStatusView;II)V

    aput-object v2, v0, v1

    .line 130
    new-instance v1, Lcca;

    .line 131
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/RichStatusView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lakn;

    invoke-direct {v1, v0}, Lcca;-><init>(Lakn;)V

    iput-object v1, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->d:Lcca;

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->b:[Lceh;

    aget-object v0, v0, v5

    iget-object v0, v0, Lceh;->b:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    .line 135
    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 137
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->d:Lcca;

    invoke-virtual {v1}, Lcca;->a()Landroid/graphics/drawable/AnimationDrawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->c:Landroid/graphics/drawable/AnimationDrawable;

    .line 138
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/RichStatusView;->c:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 140
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 141
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/RichStatusView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->az:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/RichStatusView;->setDividerDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0, v6}, Lcom/google/android/apps/hangouts/views/RichStatusView;->setShowDividers(I)V

    .line 143
    :cond_0
    return-void
.end method
