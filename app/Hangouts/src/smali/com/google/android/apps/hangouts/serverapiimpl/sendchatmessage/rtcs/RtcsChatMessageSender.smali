.class public Lcom/google/android/apps/hangouts/serverapiimpl/sendchatmessage/rtcs/RtcsChatMessageSender;
.super Lxv;
.source "PG"

# interfaces
.implements Lbqz;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 21
    const-string v0, "rtcs_chatmessagesender"

    const-string v1, "api_chatmessagesender"

    const-string v2, "Use RealTimeChatService to send chat messages"

    invoke-direct {p0, v0, v1, v2}, Lxv;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    return-void
.end method


# virtual methods
.method protected a(Ljava/lang/Class;Lcyj;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Lcyj;",
            ")V"
        }
    .end annotation

    .prologue
    .line 38
    const-class v0, Lbqz;

    if-ne p1, v0, :cond_0

    .line 39
    const-class v0, Lbqz;

    invoke-virtual {p2, v0, p0}, Lcyj;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcyj;

    .line 41
    :cond_0
    return-void
.end method

.method public a(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IILjava/lang/String;Ljava/lang/String;ZLcom/google/android/gms/maps/model/MarkerOptions;Lcom/google/android/gms/maps/model/CameraPosition;I)V
    .locals 0

    .prologue
    .line 31
    invoke-static/range {p1 .. p14}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IILjava/lang/String;Ljava/lang/String;ZLcom/google/android/gms/maps/model/MarkerOptions;Lcom/google/android/gms/maps/model/CameraPosition;I)I

    .line 34
    return-void
.end method
