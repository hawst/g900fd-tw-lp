.class public Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;
.super Landroid/appwidget/AppWidgetProvider;
.source "PG"


# static fields
.field public static final a:Ljava/util/Random;

.field public static final b:Landroid/net/Uri;

.field private static final c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 45
    sget-object v0, Lbys;->t:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->c:Z

    .line 57
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->a:Ljava/util/Random;

    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "widget://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/apps/hangouts/content/EsProvider;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->b:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    return-void
.end method

.method public static a(Lyj;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 540
    sget-object v0, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 541
    if-eqz p0, :cond_0

    .line 542
    const-string v1, "account"

    .line 543
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    .line 542
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 545
    :cond_0
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 123
    const-string v0, "widgetPrefs"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 125
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "widget-account-"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 126
    return-object v0
.end method

.method private static a(Landroid/appwidget/AppWidgetManager;I)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 209
    sget v0, Lg;->aP:I

    invoke-virtual {p0, p1, v0}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged(II)V

    .line 210
    return-void
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 71
    sget-boolean v0, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->c:Z

    if-eqz v0, :cond_0

    .line 72
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "BabelWidgetProvider.saveWidgetInformation id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " account: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    :cond_0
    const-string v0, "widgetPrefs"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 77
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 79
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "widget-account-"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 80
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 81
    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/widget/RemoteViews;ILyj;)V
    .locals 8

    .prologue
    const/high16 v7, 0x8000000

    const/16 v6, 0xb

    const/4 v0, 0x1

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 286
    if-nez p3, :cond_1

    .line 287
    sget-boolean v0, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->c:Z

    if-eqz v0, :cond_0

    .line 288
    const-string v0, "Babel"

    const-string v1, "BabelWidgetProvider.configureValidAccountWidget: null account"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    :cond_0
    :goto_0
    return-void

    .line 292
    :cond_1
    invoke-static {}, Lbkb;->A()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-le v3, v0, :cond_5

    .line 293
    :goto_1
    sget-boolean v3, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->c:Z

    if-eqz v3, :cond_2

    .line 294
    const-string v3, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "BabelWidgetProvider.configureValidAccountWidget: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " account: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " hasMultiAccount: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    :cond_2
    sget v3, Lg;->in:I

    invoke-virtual {p1, v3, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 302
    sget v3, Lg;->ig:I

    if-eqz v0, :cond_6

    move v0, v1

    :goto_2
    invoke-virtual {p1, v3, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 304
    sget v0, Lg;->ig:I

    invoke-virtual {p3}, Lyj;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v0, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 305
    sget v0, Lg;->is:I

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 306
    sget v0, Lg;->ih:I

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 307
    sget v0, Lg;->aP:I

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 308
    sget v0, Lg;->ii:I

    invoke-virtual {p1, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 309
    sget v0, Lg;->iq:I

    invoke-virtual {p1, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 314
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/google/android/apps/hangouts/widget/BabelWidgetService;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 316
    const-string v2, "appWidgetId"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 317
    const-string v2, "content"

    invoke-virtual {p3}, Lyj;->b()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 318
    const-string v2, "account_name"

    invoke-virtual {p3}, Lyj;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 320
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v2, v3, :cond_7

    .line 321
    sget v2, Lg;->aP:I

    invoke-virtual {p1, v2, v0}, Landroid/widget/RemoteViews;->setRemoteAdapter(ILandroid/content/Intent;)V

    .line 327
    :cond_3
    :goto_3
    sget v0, Lg;->in:I

    sget v2, Lh;->q:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 330
    invoke-static {p3}, Lbbl;->c(Lyj;)Landroid/content/Intent;

    move-result-object v0

    .line 332
    sget-object v2, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->a:Ljava/util/Random;

    .line 333
    invoke-virtual {v2}, Ljava/util/Random;->nextInt()I

    move-result v2

    .line 332
    invoke-static {p0, v2, v0, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 335
    sget v2, Lg;->ik:I

    invoke-virtual {p1, v2, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 338
    invoke-static {p3}, Lbbl;->a(Lyj;)Landroid/content/Intent;

    move-result-object v0

    .line 339
    sget-object v2, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->a:Ljava/util/Random;

    .line 340
    invoke-virtual {v2}, Ljava/util/Random;->nextInt()I

    move-result v2

    .line 339
    invoke-static {p0, v2, v0, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 342
    sget v2, Lg;->ih:I

    invoke-virtual {p1, v2, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 345
    invoke-static {p0}, Lcu;->a(Landroid/content/Context;)Lcu;

    move-result-object v0

    .line 346
    const-class v2, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;

    invoke-virtual {v0, v2}, Lcu;->a(Ljava/lang/Class;)Lcu;

    .line 351
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 352
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v3

    const-string v4, "com.google.android.talk.SigningInActivity"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 353
    const-string v3, "android.intent.action.VIEW"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 354
    invoke-virtual {v0, v2}, Lcu;->a(Landroid/content/Intent;)Lcu;

    .line 356
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v2, v6, :cond_4

    .line 357
    sget v2, Lg;->aP:I

    invoke-virtual {v0, v1}, Lcu;->b(I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Landroid/widget/RemoteViews;->setPendingIntentTemplate(ILandroid/app/PendingIntent;)V

    .line 360
    :cond_4
    invoke-virtual {p3}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p2, v0}, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->a(Landroid/content/Context;ILjava/lang/String;)V

    .line 361
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    invoke-virtual {v0, p2, p1}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    goto/16 :goto_0

    :cond_5
    move v0, v1

    .line 292
    goto/16 :goto_1

    :cond_6
    move v0, v2

    .line 302
    goto/16 :goto_2

    .line 322
    :cond_7
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v2, v6, :cond_3

    .line 324
    sget v2, Lg;->aP:I

    invoke-virtual {p1, p2, v2, v0}, Landroid/widget/RemoteViews;->setRemoteAdapter(IILandroid/content/Intent;)V

    goto :goto_3
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 386
    sget-boolean v0, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->c:Z

    if-eqz v0, :cond_0

    .line 387
    const-string v0, "Babel"

    const-string v1, "BabelWidgetProvider.notifyDatasetChanged"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.hangouts.intent.action.ACTION_NOTIFY_DATASET_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 390
    if-eqz p1, :cond_1

    .line 391
    const-string v1, "account_name"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 393
    :cond_1
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 394
    return-void
.end method

.method private static a(Landroid/content/Context;[I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 556
    sget-boolean v0, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->c:Z

    if-eqz v0, :cond_0

    .line 557
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "removeAccountPreferences: "

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz p1, :cond_1

    array-length v0, p1

    if-lez v0, :cond_1

    aget v0, p1, v1

    .line 558
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 557
    invoke-static {v2, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 561
    :cond_0
    const-string v0, "widgetPrefs"

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 563
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    move v0, v1

    .line 565
    :goto_1
    array-length v1, p1

    if-ge v0, v1, :cond_2

    .line 567
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "widget-account-"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget v3, p1, v0

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 565
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 558
    :cond_1
    const-string v0, "EMPTY"

    goto :goto_0

    .line 569
    :cond_2
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 570
    return-void
.end method

.method private static a(Landroid/widget/RemoteViews;)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 274
    sget v0, Lg;->in:I

    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 275
    sget v0, Lg;->ig:I

    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 276
    sget v0, Lg;->is:I

    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 277
    sget v0, Lg;->ih:I

    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 278
    sget v0, Lg;->aP:I

    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 279
    return-void
.end method

.method public static a(Landroid/net/Uri;)Z
    .locals 2

    .prologue
    .line 549
    if-nez p0, :cond_0

    .line 550
    const/4 v0, 0x0

    .line 552
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private static b(Landroid/appwidget/AppWidgetManager;I)I
    .locals 8

    .prologue
    const/4 v1, 0x3

    const/4 v0, 0x1

    .line 463
    sget-boolean v2, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->c:Z

    if-eqz v2, :cond_0

    .line 464
    const-string v2, "Babel"

    const-string v3, "BabelWidgetProvider.getWidgetSize"

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    :cond_0
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_5

    .line 470
    invoke-virtual {p0, p1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetOptions(I)Landroid/os/Bundle;

    move-result-object v3

    const-string v2, "appWidgetMinWidth"

    invoke-virtual {v3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const-string v4, "appWidgetMinHeight"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v4, v4, 0x1e

    div-int/lit8 v4, v4, 0x46

    add-int/lit8 v2, v2, 0x1e

    div-int/lit8 v5, v2, 0x46

    sget-boolean v2, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->c:Z

    if-eqz v2, :cond_1

    const-string v2, "Babel"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "BabelWidgetProvider.getWidgetSize row: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " columns: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const/4 v2, 0x2

    if-ne v4, v0, :cond_4

    :goto_0
    const-string v1, "widgetSizeKey"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    if-eq v1, v0, :cond_3

    const-string v2, "widgetSizeKey"

    invoke-virtual {v3, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {p0, p1, v3}, Landroid/appwidget/AppWidgetManager;->updateAppWidgetOptions(ILandroid/os/Bundle;)V

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_2

    invoke-static {p0, p1}, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->a(Landroid/appwidget/AppWidgetManager;I)V

    :cond_2
    sget-boolean v2, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->c:Z

    if-eqz v2, :cond_3

    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "BabelWidgetProvider.getWidgetSize old size: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " new size saved: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    :goto_1
    return v0

    :cond_4
    if-lt v5, v1, :cond_6

    move v0, v1

    goto :goto_0

    :cond_5
    const/4 v0, 0x4

    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;I)Landroid/widget/RemoteViews;
    .locals 3

    .prologue
    .line 449
    packed-switch p1, :pswitch_data_0

    .line 457
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    sget v2, Lf;->gL:I

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    :goto_0
    return-object v0

    .line 452
    :pswitch_0
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    sget v2, Lf;->gL:I

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    goto :goto_0

    .line 449
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 402
    sget-boolean v1, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->c:Z

    if-eqz v1, :cond_0

    .line 403
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "BabelWidgetProvider.accountStateChanged: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    :cond_0
    const-string v1, "widgetPrefs"

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 408
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    .line 409
    new-instance v3, Landroid/content/ComponentName;

    const-class v4, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;

    invoke-direct {v3, p0, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v3}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v2

    array-length v3, v2

    :goto_0
    if-ge v0, v3, :cond_3

    aget v4, v2, v0

    .line 412
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "widget-account-"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v1, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 416
    if-eqz p1, :cond_1

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 417
    :cond_1
    invoke-static {p0, v4, v5}, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->c(Landroid/content/Context;ILjava/lang/String;)V

    .line 409
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 420
    :cond_3
    return-void
.end method

.method public static b(Landroid/content/Context;ILjava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 104
    invoke-static {p2}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v1

    .line 105
    if-eqz v1, :cond_1

    .line 106
    const-string v2, "widgetPrefs"

    invoke-virtual {p0, v2, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 108
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "widget-account-"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 110
    sget-boolean v3, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->c:Z

    if-eqz v3, :cond_0

    .line 111
    const-string v3, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "BabelWidgetProvider.isWidgetConfigured account: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " accountLabel: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " EsAccountsData.isLoggedOff: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 113
    invoke-static {v1}, Lym;->k(Lyj;)Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 111
    invoke-static {v3, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    :cond_0
    if-eqz v2, :cond_1

    invoke-static {v1}, Lym;->k(Lyj;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 116
    const/4 v0, 0x1

    .line 119
    :cond_1
    return v0
.end method

.method public static c(Landroid/content/Context;ILjava/lang/String;)V
    .locals 10

    .prologue
    const/high16 v9, 0x8000000

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 216
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    .line 217
    invoke-static {v2, p1}, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->b(Landroid/appwidget/AppWidgetManager;I)I

    move-result v3

    .line 218
    invoke-static {p0, v3}, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->b(Landroid/content/Context;I)Landroid/widget/RemoteViews;

    move-result-object v4

    .line 221
    invoke-static {p2}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v5

    .line 222
    sget-boolean v6, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->c:Z

    if-eqz v6, :cond_0

    .line 223
    const-string v6, "Babel"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "BabelWidgetProvider.updateWidget appWidgetId: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " account: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " accountName: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " size: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    :cond_0
    if-eqz v5, :cond_2

    .line 230
    invoke-virtual {v5}, Lyj;->u()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {}, Lbkb;->m()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 231
    :cond_1
    invoke-virtual {v5}, Lyj;->u()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 232
    invoke-static {}, Lbkb;->n()Lyj;

    move-result-object v3

    if-eq v3, v5, :cond_5

    .line 235
    :cond_2
    invoke-static {v4}, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->a(Landroid/widget/RemoteViews;)V

    .line 236
    sget v3, Lg;->ii:I

    invoke-virtual {v4, v3, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 238
    new-instance v3, Landroid/content/Intent;

    const-class v5, Lcom/google/android/apps/hangouts/phone/WidgetAccountPickerActivity;

    invoke-direct {v3, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 239
    const-string v5, "appWidgetId"

    invoke-virtual {v3, v5, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 240
    invoke-virtual {v3, v1}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 241
    const/high16 v5, 0x40000000    # 2.0f

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 242
    invoke-static {p0, v0, v3, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 244
    sget v5, Lg;->ii:I

    invoke-virtual {v4, v5, v3}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 246
    sget-boolean v3, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->c:Z

    if-eqz v3, :cond_3

    .line 247
    const-string v3, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "BabelWidgetProvider.updateWidget appWidgetId: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " going into configure state"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    :cond_3
    new-array v1, v1, [I

    aput p1, v1, v0

    .line 252
    invoke-static {p0, v1}, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->a(Landroid/content/Context;[I)V

    .line 267
    :goto_0
    invoke-virtual {v2, p1, v4}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 268
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v1, v3, :cond_4

    if-eqz v0, :cond_4

    .line 269
    invoke-static {v2, p1}, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->a(Landroid/appwidget/AppWidgetManager;I)V

    .line 271
    :cond_4
    return-void

    .line 253
    :cond_5
    invoke-static {v5}, Lym;->k(Lyj;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 256
    invoke-static {v4}, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->a(Landroid/widget/RemoteViews;)V

    .line 257
    sget v1, Lg;->iq:I

    invoke-virtual {v4, v1, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 259
    invoke-static {v5}, Lbbl;->b(Lyj;)Landroid/content/Intent;

    move-result-object v1

    .line 260
    invoke-static {p0, v0, v1, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 262
    sget v3, Lg;->iq:I

    invoke-virtual {v4, v3, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    goto :goto_0

    .line 264
    :cond_6
    invoke-static {p0, v4, p1, v5}, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILyj;)V

    move v0, v1

    .line 265
    goto :goto_0
.end method


# virtual methods
.method public onAppWidgetOptionsChanged(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILandroid/os/Bundle;)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 427
    invoke-static {p2, p3}, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->b(Landroid/appwidget/AppWidgetManager;I)I

    move-result v0

    .line 429
    sget-boolean v1, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->c:Z

    if-eqz v1, :cond_0

    .line 430
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "BabelWidgetProvider.onAppWidgetOptionsChanged new size: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    :cond_0
    const-string v1, "widgetPrefs"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 438
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "widget-account-"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 439
    if-eqz v1, :cond_1

    .line 440
    invoke-static {v1}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v1

    .line 441
    invoke-static {p1, v0}, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->b(Landroid/content/Context;I)Landroid/widget/RemoteViews;

    move-result-object v0

    invoke-static {p1, v0, p3, v1}, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILyj;)V

    .line 445
    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/appwidget/AppWidgetProvider;->onAppWidgetOptionsChanged(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILandroid/os/Bundle;)V

    .line 446
    return-void
.end method

.method public onDeleted(Landroid/content/Context;[I)V
    .locals 2

    .prologue
    .line 88
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onDeleted(Landroid/content/Context;[I)V

    .line 90
    sget-boolean v0, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->c:Z

    if-eqz v0, :cond_0

    .line 91
    const-string v0, "Babel"

    const-string v1, "BabelWidgetProvider.onDeleted"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    :cond_0
    invoke-static {p1, p2}, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->a(Landroid/content/Context;[I)V

    .line 95
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 157
    sget-boolean v0, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->c:Z

    if-eqz v0, :cond_0

    .line 158
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "BabelWidgetProvider.onReceive intent: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 166
    const-string v1, "com.google.android.apps.hangouts.intent.action.ACTION_NOTIFY_DATASET_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 167
    const-string v0, "account_name"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 168
    if-nez v1, :cond_2

    .line 198
    :cond_1
    :goto_0
    return-void

    .line 171
    :cond_2
    const-string v0, "widgetPrefs"

    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 174
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 176
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v5

    .line 177
    new-instance v0, Landroid/content/ComponentName;

    const-class v6, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;

    invoke-direct {v0, p1, v6}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v5, v0}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v6

    array-length v7, v6

    move v0, v2

    :goto_1
    if-ge v0, v7, :cond_4

    aget v8, v6, v0

    .line 180
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "widget-account-"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-interface {v3, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 183
    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 184
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v4, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 177
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 187
    :cond_4
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 188
    new-array v0, v2, [Ljava/lang/Integer;

    invoke-interface {v4, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Integer;

    .line 189
    array-length v1, v0

    new-array v3, v1, [I

    move v1, v2

    .line 190
    :goto_2
    array-length v4, v0

    if-ge v1, v4, :cond_5

    .line 191
    aget-object v4, v0, v1

    invoke-static {v4, v2}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v4

    aput v4, v3, v1

    .line 190
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 193
    :cond_5
    sget v0, Lg;->aP:I

    invoke-virtual {v5, v3, v0}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged([II)V

    goto :goto_0

    .line 196
    :cond_6
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 134
    invoke-super {p0, p1, p2, p3}, Landroid/appwidget/AppWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 136
    sget-boolean v1, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->c:Z

    if-eqz v1, :cond_0

    .line 137
    const-string v1, "Babel"

    const-string v2, "BabelWidgetProvider.onUpdate"

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    :cond_0
    const-string v1, "widgetPrefs"

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 144
    :goto_0
    array-length v2, p3

    if-ge v0, v2, :cond_1

    .line 146
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "widget-account-"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget v3, p3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 151
    aget v3, p3, v0

    invoke-static {p1, v3, v2}, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->c(Landroid/content/Context;ILjava/lang/String;)V

    .line 144
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 153
    :cond_1
    return-void
.end method
