.class public Lcom/google/android/apps/hangouts/hangout/ToastView;
.super Landroid/view/View;
.source "PG"

# interfaces
.implements Lapu;


# instance fields
.field private final a:Lapk;

.field private final b:Lasp;

.field private c:Lapv;

.field private final d:Landroid/os/Handler;

.field private final e:Ljava/lang/Runnable;

.field private f:Landroid/widget/Toast;

.field private g:Lasw;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 347
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 332
    invoke-static {}, Lapk;->b()Lapk;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ToastView;->a:Lapk;

    .line 333
    new-instance v0, Lasp;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lasp;-><init>(Lcom/google/android/apps/hangouts/hangout/ToastView;B)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ToastView;->b:Lasp;

    .line 335
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ToastView;->d:Landroid/os/Handler;

    .line 336
    new-instance v0, Lask;

    invoke-direct {v0, p0}, Lask;-><init>(Lcom/google/android/apps/hangouts/hangout/ToastView;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ToastView;->e:Ljava/lang/Runnable;

    .line 348
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/hangout/ToastView;)Lapk;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ToastView;->a:Lapk;

    return-object v0
.end method

.method private declared-synchronized a(Lasw;)V
    .locals 4

    .prologue
    .line 379
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lasw;->b()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 380
    if-nez v1, :cond_1

    .line 410
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 386
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ToastView;->g:Lasw;

    if-eqz v0, :cond_2

    .line 387
    invoke-virtual {p1}, Lasw;->c()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ToastView;->g:Lasw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ToastView;->g:Lasw;

    .line 388
    invoke-virtual {v0}, Lasw;->c()I

    move-result v0

    invoke-virtual {p1}, Lasw;->c()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 389
    :cond_2
    iput-object p1, p0, Lcom/google/android/apps/hangouts/hangout/ToastView;->g:Lasw;

    .line 391
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ToastView;->c:Lapv;

    .line 392
    invoke-virtual {v0}, Lapv;->g()Landroid/view/accessibility/AccessibilityManager;

    move-result-object v0

    .line 391
    invoke-static {p0, v0, v1}, Lf;->a(Landroid/view/View;Landroid/view/accessibility/AccessibilityManager;Ljava/lang/CharSequence;)V

    .line 395
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ToastView;->g:Lasw;

    invoke-virtual {v0}, Lasw;->c()I

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    .line 400
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/ToastView;->f:Landroid/widget/Toast;

    if-eqz v2, :cond_3

    .line 401
    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/ToastView;->f:Landroid/widget/Toast;

    invoke-virtual {v2}, Landroid/widget/Toast;->cancel()V

    .line 403
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/ToastView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ToastView;->f:Landroid/widget/Toast;

    .line 404
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ToastView;->f:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 407
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ToastView;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/ToastView;->e:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 408
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ToastView;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/ToastView;->e:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 379
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 395
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/hangout/ToastView;Lasw;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/hangout/ToastView;->a(Lasw;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/EnterEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 26
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isSelfEndpoint()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isConnecting()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/EnterEvent;->mayBePreExistingEndpoint()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v2, v0

    :goto_0
    if-nez v2, :cond_2

    :goto_1
    return v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private declared-synchronized b()V
    .locals 1

    .prologue
    .line 413
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ToastView;->f:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 414
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ToastView;->f:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 415
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ToastView;->f:Landroid/widget/Toast;

    .line 417
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ToastView;->g:Lasw;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 418
    monitor-exit p0

    return-void

    .line 413
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/hangout/ToastView;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/hangout/ToastView;->b()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 2

    .prologue
    .line 363
    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    .line 364
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ToastView;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/ToastView;->e:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 365
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ToastView;->e:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 367
    :cond_0
    return-void
.end method

.method public a(Lapv;)V
    .locals 2

    .prologue
    .line 352
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ToastView;->a:Lapk;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/ToastView;->b:Lasp;

    invoke-virtual {v0, v1}, Lapk;->a(Lapo;)V

    .line 353
    iput-object p1, p0, Lcom/google/android/apps/hangouts/hangout/ToastView;->c:Lapv;

    .line 354
    return-void
.end method

.method public b(I)V
    .locals 3

    .prologue
    .line 374
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/ToastView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 375
    new-instance v1, Lasv;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2, v0}, Lasv;-><init>(Lcom/google/android/apps/hangouts/hangout/ToastView;ILjava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/hangouts/hangout/ToastView;->a(Lasw;)V

    .line 376
    return-void
.end method

.method public m_()V
    .locals 2

    .prologue
    .line 358
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/ToastView;->a:Lapk;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/ToastView;->b:Lasp;

    invoke-virtual {v0, v1}, Lapk;->b(Lapo;)V

    .line 359
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 371
    return-void
.end method
