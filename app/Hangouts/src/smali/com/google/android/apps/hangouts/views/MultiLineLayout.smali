.class public Lcom/google/android/apps/hangouts/views/MultiLineLayout;
.super Landroid/view/ViewGroup;
.source "PG"


# instance fields
.field private final a:Lcdk;

.field private final b:Lcdl;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 22
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    new-instance v0, Lcdk;

    invoke-direct {v0, p0, v1}, Lcdk;-><init>(Lcom/google/android/apps/hangouts/views/MultiLineLayout;B)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/MultiLineLayout;->a:Lcdk;

    .line 19
    new-instance v0, Lcdl;

    invoke-direct {v0, p0, v1}, Lcdl;-><init>(Lcom/google/android/apps/hangouts/views/MultiLineLayout;B)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/MultiLineLayout;->b:Lcdl;

    .line 23
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/views/MultiLineLayout;II)V
    .locals 0

    .prologue
    .line 17
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/hangouts/views/MultiLineLayout;->setMeasuredDimension(II)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/views/MultiLineLayout;Landroid/view/View;II)V
    .locals 0

    .prologue
    .line 17
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/hangouts/views/MultiLineLayout;->measureChild(Landroid/view/View;II)V

    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MultiLineLayout;->a:Lcdk;

    sub-int v1, p4, p2

    invoke-virtual {v0, v1}, Lcdk;->a(I)V

    .line 29
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MultiLineLayout;->b:Lcdl;

    invoke-virtual {v0, p1, p2}, Lcdl;->a(II)V

    .line 34
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MultiLineLayout;->b:Lcdl;

    const v1, 0x7fffffff

    invoke-static {v1, p1}, Lcom/google/android/apps/hangouts/views/MultiLineLayout;->resolveSize(II)I

    move-result v1

    invoke-virtual {v0, v1}, Lcdl;->a(I)V

    .line 35
    return-void
.end method
