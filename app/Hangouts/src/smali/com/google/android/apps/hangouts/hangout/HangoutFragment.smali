.class public Lcom/google/android/apps/hangouts/hangout/HangoutFragment;
.super Lt;
.source "PG"


# static fields
.field private static final b:Z


# instance fields
.field private final Y:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lapu;",
            ">;"
        }
    .end annotation
.end field

.field private Z:Laqv;

.field public a:Landroid/view/accessibility/AccessibilityManager;

.field private aa:Lcom/google/android/apps/hangouts/hangout/renderer/GLView;

.field private ab:Lcom/google/android/apps/hangouts/hangout/OutgoingRingOverlayView;

.field private ac:Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;

.field private ad:Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;

.field private ae:Lcom/google/android/apps/hangouts/fragments/dialpad/InCallDialpadFragment;

.field private af:Z

.field private ag:Landroid/widget/Button;

.field private ah:Landroid/view/View;

.field private ai:Lyj;

.field private aj:I

.field private final ak:Lapv;

.field private al:Z

.field private am:Z

.field private an:Landroid/view/Menu;

.field private c:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

.field private d:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

.field private e:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

.field private f:Z

.field private g:Lbme;

.field private final h:Lapk;

.field private i:Lapw;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 339
    sget-object v0, Lbys;->e:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->b:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 78
    invoke-direct {p0}, Lt;-><init>()V

    .line 369
    invoke-static {}, Lapk;->b()Lapk;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->h:Lapk;

    .line 373
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->Y:Ljava/util/ArrayList;

    .line 386
    new-instance v0, Lapv;

    invoke-direct {v0, p0}, Lapv;-><init>(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ak:Lapv;

    return-void
.end method

.method private a(ZZ)I
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v4, 0x2

    const/4 v2, 0x3

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 642
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->h:Lapk;

    invoke-virtual {v0}, Lapk;->c()Lapx;

    move-result-object v0

    if-nez v0, :cond_9

    .line 645
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->f:Z

    if-eqz v0, :cond_4

    .line 647
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->e:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-static {v0, v3}, Lcom/google/android/libraries/hangouts/video/ExitHistory;->getHistory(Landroid/content/Context;Lcom/google/android/libraries/hangouts/video/HangoutRequest;)Lcom/google/android/libraries/hangouts/video/ExitHistory;

    move-result-object v0

    .line 649
    if-nez v0, :cond_0

    .line 653
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->finish()V

    move v0, v1

    .line 696
    :goto_0
    return v0

    .line 656
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/ExitHistory;->getEndCause()I

    move-result v3

    .line 657
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/ExitHistory;->exitReported()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {v3}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 658
    :cond_1
    const-string v0, "Babel"

    invoke-static {v0, v2}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 659
    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Hangout previously exited: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 661
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->p()V

    move v0, v1

    .line 662
    goto :goto_0

    .line 664
    :cond_3
    invoke-direct {p0, v9, v3}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->a(Ljava/lang/String;I)V

    .line 665
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->e:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-static {v0, v1}, Lcom/google/android/libraries/hangouts/video/ExitHistory;->setExitReported(Landroid/content/Context;Lcom/google/android/libraries/hangouts/video/HangoutRequest;)V

    move v0, v2

    .line 666
    goto :goto_0

    .line 668
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v5, "hangout_auto_join"

    invoke-virtual {v0, v5, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 670
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ai:Lyj;

    if-eqz v1, :cond_6

    if-nez p1, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ai:Lyj;

    .line 671
    invoke-static {v1}, Lym;->o(Lyj;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 672
    iput-boolean v3, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->al:Z

    .line 673
    new-instance v1, Larp;

    invoke-direct {v1}, Larp;-><init>()V

    .line 674
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->getFragmentManager()Lae;

    move-result-object v4

    invoke-virtual {v1, v4, v9}, Larp;->a(Lae;Ljava/lang/String;)V

    .line 675
    if-eqz v0, :cond_5

    move v0, v2

    goto :goto_0

    :cond_5
    move v0, v3

    goto :goto_0

    .line 678
    :cond_6
    if-nez v0, :cond_7

    if-eqz p2, :cond_8

    .line 679
    :cond_7
    invoke-direct {p0, p2}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->a(Z)V

    move v0, v4

    .line 680
    goto :goto_0

    :cond_8
    move v0, v3

    .line 682
    goto :goto_0

    .line 684
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->h:Lapk;

    invoke-virtual {v0}, Lapk;->c()Lapx;

    move-result-object v5

    .line 685
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->getActivity()Ly;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    .line 684
    invoke-virtual {v5, v0}, Lapx;->a(Lcom/google/android/apps/hangouts/hangout/HangoutActivity;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 686
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->h:Lapk;

    invoke-virtual {v0}, Lapk;->c()Lapx;

    move-result-object v0

    invoke-virtual {v0}, Lapx;->c()Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->e:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    .line 687
    iput-boolean v3, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->f:Z

    move v0, v4

    .line 688
    goto/16 :goto_0

    .line 690
    :cond_a
    sget-boolean v0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->b:Z

    if-eqz v0, :cond_b

    .line 691
    const-string v0, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Already joined: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->h:Lapk;

    .line 692
    invoke-virtual {v6}, Lapk;->c()Lapx;

    move-result-object v6

    invoke-virtual {v6}, Lapx;->c()Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 691
    invoke-static {v0, v5}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 694
    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->d:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->sendCallCompleteIntent()V

    .line 695
    sget v0, Lh;->ew:I

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v5, "Babel"

    invoke-static {v5, v2}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_c

    const-string v5, "Babel"

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "showError:%s (%s)"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v1

    iget-object v8, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    aput-object v8, v4, v3

    invoke-static {v6, v7, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    invoke-static {v0, v1}, Laps;->a(Ljava/lang/String;I)Laps;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->getFragmentManager()Lae;

    move-result-object v1

    invoke-virtual {v0, v1, v9}, Laps;->a(Lae;Ljava/lang/String;)V

    move v0, v2

    .line 696
    goto/16 :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;Lcom/google/android/libraries/hangouts/video/HangoutRequest;)Lcom/google/android/libraries/hangouts/video/HangoutRequest;
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->e:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)Lyj;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ai:Lyj;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;I)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->b(I)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 78
    invoke-static {}, Lbkb;->j()Lyj;

    move-result-object v0

    invoke-static {p1, v0}, Lbbl;->a(Ljava/lang/String;Lyj;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0xc8

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->a(Ljava/lang/String;I)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method private a(Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 887
    invoke-static {p1, p2}, Laps;->a(Ljava/lang/String;I)Laps;

    move-result-object v0

    .line 889
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->getFragmentManager()Lae;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Laps;->a(Lae;Ljava/lang/String;)V

    .line 890
    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 932
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->af:Z

    if-ne v0, p2, :cond_0

    .line 961
    :goto_0
    return-void

    .line 935
    :cond_0
    invoke-static {}, Lbxm;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 936
    if-eqz p2, :cond_1

    .line 937
    invoke-static {p1}, Lcwz;->b(Ljava/lang/Object;)V

    .line 938
    invoke-static {p1}, Lbbl;->k(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 939
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 954
    :cond_1
    :goto_1
    iput-boolean p2, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->af:Z

    .line 955
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->s()V

    .line 956
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->t()V

    .line 958
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->af:Z

    if-eqz v0, :cond_4

    const/16 v0, 0x62e

    .line 960
    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->g:Lbme;

    invoke-interface {v1}, Lbme;->a()Laux;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Laux;->a(Ljava/lang/Integer;)V

    goto :goto_0

    .line 944
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->e()Lae;

    move-result-object v0

    invoke-virtual {v0}, Lae;->a()Lao;

    move-result-object v0

    .line 945
    if-eqz p2, :cond_3

    .line 946
    invoke-static {p1}, Lcwz;->b(Ljava/lang/Object;)V

    .line 947
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ae:Lcom/google/android/apps/hangouts/fragments/dialpad/InCallDialpadFragment;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/hangouts/fragments/dialpad/InCallDialpadFragment;->a(Ljava/lang/String;)V

    .line 948
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ae:Lcom/google/android/apps/hangouts/fragments/dialpad/InCallDialpadFragment;

    invoke-virtual {v0, v1}, Lao;->c(Lt;)Lao;

    .line 952
    :goto_3
    invoke-virtual {v0}, Lao;->b()I

    goto :goto_1

    .line 950
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ae:Lcom/google/android/apps/hangouts/fragments/dialpad/InCallDialpadFragment;

    invoke-virtual {v0, v1}, Lao;->b(Lt;)Lao;

    goto :goto_3

    .line 958
    :cond_4
    const/16 v0, 0x62f

    goto :goto_2
.end method

.method private a(Z)V
    .locals 13

    .prologue
    .line 505
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 507
    const-string v1, "hangout_invitee_users"

    .line 508
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    .line 510
    const-string v1, "hangout_invitee_circles"

    .line 511
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v4

    check-cast v4, Ljava/util/ArrayList;

    .line 513
    const-string v1, "hangout_participants"

    .line 514
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    .line 516
    const-string v1, "pstn_caller"

    .line 517
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Lbdh;

    .line 519
    const-string v1, "hangout_start_source"

    const/16 v2, 0x33

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    .line 522
    const-string v1, "hangout_pstn_call"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v12

    .line 526
    const-string v1, "extra_hangout_start_time"

    const-wide/16 v8, 0x0

    invoke-virtual {v0, v1, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v9

    .line 528
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->d:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    .line 529
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getCallMediaType()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v0, 0x1

    move v11, v0

    .line 532
    :goto_0
    const/4 v0, 0x1

    if-ne v12, v0, :cond_3

    .line 533
    const/4 v8, 0x1

    .line 542
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->h:Lapk;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->d:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->clone()Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v1

    move v2, p1

    invoke-virtual/range {v0 .. v10}, Lapk;->a(Lcom/google/android/libraries/hangouts/video/HangoutRequest;ZLjava/util/List;Ljava/util/List;Ljava/util/List;Lbdh;IIJ)V

    .line 547
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->g:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    invoke-virtual {v0}, Laux;->a()V

    .line 549
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->h:Lapk;

    invoke-virtual {v0}, Lapk;->c()Lapx;

    move-result-object v1

    .line 550
    if-eqz v11, :cond_0

    .line 551
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/CameraInterface;->getInstance()Lcom/google/android/libraries/hangouts/video/CameraInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CameraInterface;->hasFrontCamera()Z

    move-result v0

    if-nez v0, :cond_5

    :cond_0
    const/4 v0, 0x1

    .line 550
    :goto_2
    invoke-virtual {v1, v0}, Lapx;->f(Z)V

    .line 553
    invoke-virtual {v1, v12}, Lapx;->b(I)V

    .line 554
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->am:Z

    if-eqz v0, :cond_1

    .line 555
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lapx;->g(Z)V

    .line 556
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->am:Z

    .line 559
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->f:Z

    .line 560
    return-void

    .line 529
    :cond_2
    const/4 v0, 0x0

    move v11, v0

    goto :goto_0

    .line 535
    :cond_3
    if-nez v11, :cond_4

    .line 536
    const/4 v8, 0x2

    goto :goto_1

    .line 538
    :cond_4
    const/4 v8, 0x3

    goto :goto_1

    .line 551
    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static synthetic a(I)Z
    .locals 1

    .prologue
    .line 78
    invoke-static {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c(I)Z

    move-result v0

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;Z)Z
    .locals 0

    .prologue
    .line 78
    iput-boolean p1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->am:Z

    return p1
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)Lcom/google/android/apps/hangouts/hangout/HangoutActivity;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    return-object v0
.end method

.method private b(I)V
    .locals 7

    .prologue
    const/16 v6, 0x80

    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 830
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 831
    const-string v0, "Babel"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "changeUiState: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " (old was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->aj:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 834
    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 835
    iget v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->aj:I

    if-ne p1, v0, :cond_2

    .line 870
    :cond_1
    return-void

    .line 838
    :cond_2
    iput p1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->aj:I

    .line 840
    const/16 v0, 0x632

    .line 841
    packed-switch p1, :pswitch_data_0

    .line 855
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Call in unknown UI state: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 858
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->g:Lbme;

    invoke-interface {v1}, Lbme;->a()Laux;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Laux;->a(Ljava/lang/Integer;)V

    .line 860
    if-ne p1, v5, :cond_3

    .line 861
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ak:Lapv;

    invoke-virtual {v0}, Lapv;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 862
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v6, v6}, Landroid/view/Window;->setFlags(II)V

    .line 867
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->Y:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapu;

    .line 868
    invoke-interface {v0, p1}, Lapu;->a(I)V

    goto :goto_1

    .line 843
    :pswitch_0
    const/16 v0, 0x633

    .line 844
    goto :goto_0

    .line 846
    :pswitch_1
    const/16 v0, 0x634

    .line 847
    goto :goto_0

    .line 849
    :pswitch_2
    const/16 v0, 0x635

    .line 850
    goto :goto_0

    .line 852
    :pswitch_3
    const/16 v0, 0x636

    .line 853
    goto :goto_0

    .line 841
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private b(Z)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 736
    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->f:Z

    .line 737
    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->al:Z

    .line 742
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->a(ZZ)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->b(I)V

    .line 743
    return-void
.end method

.method public static synthetic c(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->aj:I

    return v0
.end method

.method private static c(I)Z
    .locals 1

    .prologue
    .line 911
    const/16 v0, 0x3ec

    if-eq p0, v0, :cond_0

    const/16 v0, 0x3ed

    if-eq p0, v0, :cond_0

    const/16 v0, 0x3f3

    if-eq p0, v0, :cond_0

    const/16 v0, 0x3f2

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic d(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)V
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->a(Z)V

    return-void
.end method

.method public static synthetic e(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)Lapk;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->h:Lapk;

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)Z
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->af:Z

    return v0
.end method

.method public static synthetic g(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ag:Landroid/widget/Button;

    return-object v0
.end method

.method public static synthetic h(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->s()V

    return-void
.end method

.method public static synthetic i(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)Z
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->al:Z

    return v0
.end method

.method public static synthetic j(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->t()V

    return-void
.end method

.method public static synthetic k(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)Lbme;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->g:Lbme;

    return-object v0
.end method

.method public static synthetic l(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)Lcom/google/android/libraries/hangouts/video/HangoutRequest;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->e:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    return-object v0
.end method

.method public static synthetic m(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)Laqv;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->Z:Laqv;

    return-object v0
.end method

.method private s()V
    .locals 2

    .prologue
    .line 964
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->af:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ag:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 965
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ah:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 969
    :goto_0
    return-void

    .line 967
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ah:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private t()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 972
    invoke-static {}, Lbxm;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 985
    :goto_0
    return-void

    .line 975
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->r()I

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    .line 976
    :goto_1
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getLocalState()Lcom/google/android/libraries/hangouts/video/LocalState;

    move-result-object v3

    .line 977
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/google/android/libraries/hangouts/video/LocalState;->getAudioDeviceState()Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->EARPIECE_ON:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    .line 978
    invoke-virtual {v3, v4}, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v2, v1

    .line 980
    :cond_1
    if-nez v0, :cond_2

    if-nez v2, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->af:Z

    if-eqz v0, :cond_4

    .line 981
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->setRequestedOrientation(I)V

    goto :goto_0

    :cond_3
    move v0, v2

    .line 975
    goto :goto_1

    .line 983
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->setRequestedOrientation(I)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 564
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->an:Landroid/view/Menu;

    if-eqz v0, :cond_1

    .line 565
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->an:Landroid/view/Menu;

    sget v3, Lg;->dR:I

    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 566
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ak:Lapv;

    invoke-virtual {v0}, Lapv;->i()I

    move-result v4

    .line 567
    if-eqz v4, :cond_2

    move v0, v1

    :goto_0
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 568
    if-eqz v4, :cond_0

    .line 569
    const/4 v0, 0x2

    if-ne v4, v0, :cond_3

    move v0, v1

    :goto_1
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 571
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->h:Lapk;

    invoke-virtual {v0}, Lapk;->g()Z

    move-result v3

    .line 572
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->h:Lapk;

    invoke-virtual {v0}, Lapk;->h()Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    move-result-object v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 573
    :goto_2
    iget-object v4, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->an:Landroid/view/Menu;

    sget v5, Lg;->dx:I

    invoke-interface {v4, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    .line 574
    if-eqz v3, :cond_5

    if-eqz v0, :cond_5

    :goto_3
    invoke-interface {v4, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 576
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 567
    goto :goto_0

    :cond_3
    move v0, v2

    .line 569
    goto :goto_1

    :cond_4
    move v0, v2

    .line 572
    goto :goto_2

    :cond_5
    move v1, v2

    .line 574
    goto :goto_3
.end method

.method public b()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 701
    iget-boolean v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->af:Z

    if-eqz v1, :cond_0

    .line 702
    const/4 v1, 0x0

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->a(Ljava/lang/String;Z)V

    .line 703
    const/4 v0, 0x1

    .line 705
    :cond_0
    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 731
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->al:Z

    .line 732
    const/4 v0, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->a(ZZ)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->b(I)V

    .line 733
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 746
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->b(Z)V

    .line 747
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 750
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->b(Z)V

    .line 751
    return-void
.end method

.method f()Lcom/google/android/libraries/hangouts/video/HangoutRequest;
    .locals 1

    .prologue
    .line 893
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->e:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 710
    if-nez p1, :cond_2

    .line 711
    if-ne p2, v2, :cond_1

    .line 712
    invoke-direct {p0, v1, v1}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->a(ZZ)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->b(I)V

    .line 713
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->al:Z

    .line 728
    :cond_0
    :goto_0
    return-void

    .line 715
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->p()V

    goto :goto_0

    .line 717
    :cond_2
    const/4 v0, 0x1

    if-ne p1, v0, :cond_3

    .line 718
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->af:Z

    goto :goto_0

    .line 719
    :cond_3
    const/16 v0, 0xc8

    if-ne p1, v0, :cond_0

    .line 720
    if-ne p2, v2, :cond_4

    .line 721
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->al:Z

    .line 722
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->f:Z

    .line 723
    invoke-direct {p0, v1, v1}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->a(ZZ)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->b(I)V

    goto :goto_0

    .line 725
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->p()V

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 396
    invoke-super {p0, p1}, Lt;->onAttach(Landroid/app/Activity;)V

    .line 398
    check-cast p1, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    iput-object p1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    .line 399
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 493
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->Y:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapu;

    .line 494
    invoke-interface {v0, p1}, Lapu;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    goto :goto_0

    .line 496
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v3, 0x3

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 403
    invoke-super {p0, p1}, Lt;->onCreate(Landroid/os/Bundle;)V

    .line 405
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->o()Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->d:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    .line 406
    const-string v0, "Babel"

    invoke-static {v0, v3}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 407
    const-string v0, "Babel"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "HangoutFragment.onCreate: this=%s activity=%s hangoutRequest=%s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p0, v3, v6

    iget-object v4, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    aput-object v4, v3, v7

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->d:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    aput-object v5, v3, v4

    .line 408
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 407
    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    :cond_0
    if-nez p1, :cond_1

    .line 415
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->d:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->clone()Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->e:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    .line 424
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->k()Lyj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ai:Lyj;

    .line 425
    invoke-virtual {p0, v7}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->setHasOptionsMenu(Z)V

    .line 426
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->a:Landroid/view/accessibility/AccessibilityManager;

    .line 428
    return-void

    .line 417
    :cond_1
    const-string v0, "HangoutFragment_current_request"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->e:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    .line 419
    const-string v0, "HangoutFragment_waiting_for_result"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->al:Z

    .line 420
    const-string v0, "HangoutFragment_hangout_initiated"

    invoke-virtual {p1, v0, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->f:Z

    .line 421
    const-string v0, "HangoutFragment_audio_muted_awaiting_result"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->am:Z

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 580
    sget v0, Lf;->gW:I

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 581
    iput-object p1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->an:Landroid/view/Menu;

    .line 582
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    .prologue
    .line 433
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lbme;

    invoke-static {v0, v1}, Lcyj;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbme;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->g:Lbme;

    .line 436
    sget v0, Lf;->fq:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 438
    new-instance v1, Laqv;

    invoke-direct {v1, p0, v0}, Laqv;-><init>(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;Landroid/view/ViewGroup;)V

    iput-object v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->Z:Laqv;

    .line 440
    sget v1, Lg;->fx:I

    .line 441
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/hangouts/hangout/PresentToAllBannerView;

    .line 443
    sget v2, Lg;->cr:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;

    iput-object v2, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->aa:Lcom/google/android/apps/hangouts/hangout/renderer/GLView;

    .line 444
    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->aa:Lcom/google/android/apps/hangouts/hangout/renderer/GLView;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->Z:Laqv;

    invoke-virtual {v2, v3, v0}, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->a(Laqv;Landroid/view/ViewGroup;)V

    .line 446
    sget v2, Lg;->fC:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;

    iput-object v2, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ad:Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;

    .line 447
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->getFragmentManager()Lae;

    move-result-object v2

    sget v3, Lg;->dE:I

    invoke-virtual {v2, v3}, Lae;->a(I)Lt;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/hangouts/fragments/dialpad/InCallDialpadFragment;

    iput-object v2, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ae:Lcom/google/android/apps/hangouts/fragments/dialpad/InCallDialpadFragment;

    .line 449
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->getActivity()Ly;

    move-result-object v2

    invoke-virtual {v2}, Ly;->e()Lae;

    move-result-object v2

    .line 450
    invoke-virtual {v2}, Lae;->a()Lao;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ae:Lcom/google/android/apps/hangouts/fragments/dialpad/InCallDialpadFragment;

    .line 451
    invoke-virtual {v2, v3}, Lao;->b(Lt;)Lao;

    move-result-object v2

    .line 452
    invoke-virtual {v2}, Lao;->b()I

    .line 453
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->af:Z

    .line 455
    sget v2, Lg;->cJ:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ah:Landroid/view/View;

    .line 457
    sget v2, Lg;->fc:I

    .line 458
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/hangouts/hangout/OutgoingRingOverlayView;

    iput-object v2, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ab:Lcom/google/android/apps/hangouts/hangout/OutgoingRingOverlayView;

    .line 459
    sget v2, Lg;->fb:I

    .line 460
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;

    iput-object v2, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ac:Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;

    .line 461
    sget v2, Lg;->cF:I

    .line 462
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/hangouts/hangout/BroadcastOverlayView;

    .line 463
    sget v3, Lg;->hB:I

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/hangouts/hangout/ToastView;

    .line 464
    sget v4, Lg;->dX:I

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ag:Landroid/widget/Button;

    .line 466
    sget v4, Lg;->J:I

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 467
    new-instance v5, Lapt;

    invoke-direct {v5, p0}, Lapt;-><init>(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 474
    new-instance v4, Lapp;

    iget-object v5, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    iget-object v6, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    .line 475
    invoke-virtual {v6}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->g()Lkd;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ai:Lyj;

    iget-object v8, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->d:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    .line 476
    invoke-virtual {v8}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getConversationId()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lapp;-><init>(Landroid/content/Context;Lkd;Lyj;Ljava/lang/String;)V

    .line 478
    iget-object v5, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->Y:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 479
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->Y:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ab:Lcom/google/android/apps/hangouts/hangout/OutgoingRingOverlayView;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 480
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->Y:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ac:Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 481
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->Y:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 482
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->Y:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 483
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->Y:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->aa:Lcom/google/android/apps/hangouts/hangout/renderer/GLView;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 484
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->Y:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->Z:Laqv;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 485
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->Y:Ljava/util/ArrayList;

    new-instance v2, Lapb;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->getActivity()Ly;

    move-result-object v3

    invoke-direct {v2, v3}, Lapb;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 486
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->Y:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 488
    return-object v0
.end method

.method public onDestroy()V
    .locals 6

    .prologue
    .line 819
    invoke-super {p0}, Lt;->onDestroy()V

    .line 821
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 822
    const-string v0, "Babel"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "HangoutFragment.onDestroy: this=%s activity=%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 826
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->aa:Lcom/google/android/apps/hangouts/hangout/renderer/GLView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->d()V

    .line 827
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 591
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lg;->dR:I

    if-ne v0, v1, :cond_1

    .line 592
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->g:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v1, 0x379

    .line 593
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 592
    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    .line 594
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->Z:Laqv;

    invoke-virtual {v0}, Laqv;->c()V

    .line 602
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 595
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lg;->dx:I

    if-ne v0, v1, :cond_0

    .line 596
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->h:Lapk;

    invoke-virtual {v0}, Lapk;->h()Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    move-result-object v0

    .line 597
    if-eqz v0, :cond_0

    .line 598
    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->Z:Laqv;

    invoke-virtual {v1, v0}, Laqv;->a(Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 773
    invoke-super {p0}, Lt;->onPause()V

    .line 775
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ad:Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->b()V

    .line 776
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ad:Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->d()V

    .line 777
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ad:Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->e()V

    .line 778
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ab:Lcom/google/android/apps/hangouts/hangout/OutgoingRingOverlayView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/OutgoingRingOverlayView;->c()V

    .line 779
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ac:Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->c()V

    .line 780
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->aa:Lcom/google/android/apps/hangouts/hangout/renderer/GLView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->c()V

    .line 781
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 0

    .prologue
    .line 586
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->a()V

    .line 587
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 755
    invoke-super {p0}, Lt;->onResume()V

    .line 757
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->aa:Lcom/google/android/apps/hangouts/hangout/renderer/GLView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/renderer/GLView;->b()V

    .line 758
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ab:Lcom/google/android/apps/hangouts/hangout/OutgoingRingOverlayView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/OutgoingRingOverlayView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 759
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ab:Lcom/google/android/apps/hangouts/hangout/OutgoingRingOverlayView;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->Z:Laqv;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/hangout/OutgoingRingOverlayView;->a(Laow;)V

    .line 761
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ac:Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 762
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ac:Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->Z:Laqv;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->a(Laow;)V

    .line 764
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ad:Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->Z:Laqv;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->a(Lasf;)V

    .line 765
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ad:Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->Z:Laqv;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->a(Lasd;)V

    .line 766
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ad:Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->a()V

    .line 768
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->t()V

    .line 769
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 785
    const-string v0, "HangoutFragment_current_request"

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->e:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 786
    const-string v0, "HangoutFragment_waiting_for_result"

    iget-boolean v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->al:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 787
    const-string v0, "HangoutFragment_hangout_initiated"

    iget-boolean v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->f:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 788
    const-string v0, "HangoutFragment_audio_muted_awaiting_result"

    iget-boolean v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->am:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 789
    return-void
.end method

.method public onStart()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x0

    .line 607
    invoke-super {p0}, Lt;->onStart()V

    .line 608
    const-string v0, "Babel"

    invoke-static {v0, v7}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 609
    const-string v0, "Babel"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "HangoutFragment.onStart: this=%s activity=%s hangoutRequest=%s"

    new-array v3, v7, [Ljava/lang/Object;

    aput-object p0, v3, v6

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->d:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    aput-object v5, v3, v4

    .line 610
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 609
    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    :cond_0
    new-instance v0, Lapw;

    invoke-direct {v0, p0, v6}, Lapw;-><init>(Lcom/google/android/apps/hangouts/hangout/HangoutFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->i:Lapw;

    .line 616
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->h:Lapk;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->i:Lapw;

    invoke-virtual {v0, v1}, Lapk;->a(Lapo;)V

    .line 618
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->h:Lapk;

    invoke-virtual {v0}, Lapk;->c()Lapx;

    move-result-object v0

    if-nez v0, :cond_1

    .line 620
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/CameraInterface;->getInstance()Lcom/google/android/libraries/hangouts/video/CameraInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CameraInterface;->resetCurrentCamera()V

    .line 623
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->al:Z

    if-eqz v0, :cond_3

    .line 625
    iput v7, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->aj:I

    .line 630
    :goto_0
    iget v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->aj:I

    if-nez v0, :cond_4

    .line 639
    :cond_2
    return-void

    .line 627
    :cond_3
    invoke-direct {p0, v6, v6}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->a(ZZ)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->aj:I

    goto :goto_0

    .line 634
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->Y:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapu;

    .line 637
    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->ak:Lapv;

    invoke-interface {v0, v2}, Lapu;->a(Lapv;)V

    goto :goto_1
.end method

.method public onStop()V
    .locals 6

    .prologue
    .line 793
    invoke-super {p0}, Lt;->onStop()V

    .line 795
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 796
    const-string v0, "Babel"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "HangoutFragment.onStop: this=%s activity=%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 800
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->i:Lapw;

    if-eqz v0, :cond_1

    .line 801
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->h:Lapk;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->i:Lapw;

    invoke-virtual {v0, v1}, Lapk;->b(Lapo;)V

    .line 802
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->i:Lapw;

    .line 805
    :cond_1
    iget v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->aj:I

    if-nez v0, :cond_3

    .line 812
    :cond_2
    return-void

    .line 809
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->Y:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapu;

    .line 810
    invoke-interface {v0}, Lapu;->m_()V

    goto :goto_0
.end method

.method q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 903
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->f()Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getConversationId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public r()I
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 924
    iget v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->aj:I

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->h:Lapk;

    invoke-virtual {v0}, Lapk;->c()Lapx;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 925
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->h:Lapk;

    invoke-virtual {v0}, Lapk;->c()Lapx;

    move-result-object v0

    invoke-virtual {v0}, Lapx;->J()I

    move-result v0

    .line 927
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/HangoutFragment;->c:Lcom/google/android/apps/hangouts/hangout/HangoutActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/HangoutActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "hangout_pstn_call"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method
