.class public Lcom/google/android/apps/hangouts/views/AudioAttachmentView;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;
.implements Lcde;


# static fields
.field private static final a:Z


# instance fields
.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private e:Landroid/view/View;

.field private f:Landroid/widget/SeekBar;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Lcas;

.field private j:Lcat;

.field private k:Lcar;

.field private l:Lcao;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lbys;->a:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->a:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 65
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 66
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/views/AudioAttachmentView;)Lcas;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->i:Lcas;

    return-object v0
.end method

.method private static a(Landroid/widget/TextView;I)V
    .locals 5

    .prologue
    .line 689
    div-int/lit16 v0, p1, 0x3e8

    .line 690
    rem-int/lit8 v1, v0, 0x3c

    .line 691
    div-int/lit8 v0, v0, 0x3c

    rem-int/lit8 v0, v0, 0x3c

    .line 692
    const-string v2, "%02d:%02d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 693
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/views/AudioAttachmentView;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->i()V

    return-void
.end method

.method public static synthetic c(Lcom/google/android/apps/hangouts/views/AudioAttachmentView;)Lcar;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->k:Lcar;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/apps/hangouts/views/AudioAttachmentView;)Lcao;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->l:Lcao;

    return-object v0
.end method

.method public static synthetic h()Z
    .locals 1

    .prologue
    .line 37
    sget-boolean v0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->a:Z

    return v0
.end method

.method private i()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 640
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->l:Lcao;

    invoke-virtual {v0}, Lcao;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 641
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 642
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 648
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->l:Lcao;

    invoke-virtual {v0}, Lcao;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 685
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->c()V

    .line 686
    return-void

    .line 644
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 645
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 651
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 652
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 653
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->g:Landroid/widget/TextView;

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->a(Landroid/widget/TextView;I)V

    .line 654
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->l:Lcao;

    invoke-virtual {v0}, Lcao;->e()I

    move-result v0

    .line 655
    if-eqz v0, :cond_1

    .line 656
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->h:Landroid/widget/TextView;

    invoke-static {v1, v0}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->a(Landroid/widget/TextView;I)V

    goto :goto_1

    .line 658
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->h:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 665
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->b:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 666
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 667
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->f:Landroid/widget/SeekBar;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 668
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->g:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lh;->B:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 669
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->h:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 673
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->b:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 674
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 675
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->b()V

    goto :goto_1

    .line 679
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 680
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 681
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->b()V

    goto :goto_1

    .line 648
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 631
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->l:Lcao;

    if-eqz v0, :cond_0

    .line 632
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->l:Lcao;

    invoke-virtual {v0}, Lcao;->h()V

    .line 636
    :goto_0
    return-void

    .line 634
    :cond_0
    const-string v0, "Babel"

    const-string v1, "playButtonClicked: audioPlaybackController is null"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(II)V
    .locals 3

    .prologue
    .line 805
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 806
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->f:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    .line 807
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->f:Landroid/widget/SeekBar;

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 808
    return-void
.end method

.method public a(Lcar;)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->k:Lcar;

    .line 102
    return-void
.end method

.method public a(Lcat;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->j:Lcat;

    .line 89
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 762
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 770
    :goto_0
    return-void

    .line 765
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->l:Lcao;

    if-eqz v0, :cond_1

    .line 766
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->l:Lcao;

    invoke-virtual {v0, p1}, Lcao;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 768
    :cond_1
    const-string v0, "Babel"

    const-string v1, "updateUrl: audioPlaybackController is null"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lcas;)V
    .locals 2

    .prologue
    .line 721
    iput-object p3, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->i:Lcas;

    .line 722
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->setLongClickable(Z)V

    .line 724
    invoke-interface {p3}, Lcas;->a()Lcao;

    move-result-object v0

    .line 725
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcao;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 726
    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->l:Lcao;

    .line 735
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->l:Lcao;

    invoke-virtual {v0, p0}, Lcao;->a(Lcom/google/android/apps/hangouts/views/AudioAttachmentView;)V

    .line 736
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->l:Lcao;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->k:Lcar;

    invoke-virtual {v0, v1}, Lcao;->a(Lcar;)V

    .line 737
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->i()V

    .line 738
    return-void

    .line 727
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->l:Lcao;

    if-eqz v0, :cond_2

    .line 728
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->l:Lcao;

    invoke-virtual {v0}, Lcao;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 729
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->l:Lcao;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcao;->a(Lcom/google/android/apps/hangouts/views/AudioAttachmentView;)V

    .line 730
    new-instance v0, Lcao;

    invoke-direct {v0, p0, p2, p1}, Lcao;-><init>(Lcom/google/android/apps/hangouts/views/AudioAttachmentView;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->l:Lcao;

    goto :goto_0

    .line 733
    :cond_2
    new-instance v0, Lcao;

    invoke-direct {v0, p0, p2, p1}, Lcao;-><init>(Lcom/google/android/apps/hangouts/views/AudioAttachmentView;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->l:Lcao;

    goto :goto_0
.end method

.method b()V
    .locals 3

    .prologue
    .line 696
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->l:Lcao;

    invoke-virtual {v0}, Lcao;->f()I

    move-result v0

    .line 697
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->l:Lcao;

    invoke-virtual {v1}, Lcao;->e()I

    move-result v1

    .line 698
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->g:Landroid/widget/TextView;

    invoke-static {v2, v0}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->a(Landroid/widget/TextView;I)V

    .line 699
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->h:Landroid/widget/TextView;

    invoke-static {v2, v1}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->a(Landroid/widget/TextView;I)V

    .line 700
    if-eqz v1, :cond_0

    .line 701
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->f:Landroid/widget/SeekBar;

    mul-int/lit8 v0, v0, 0x64

    div-int/2addr v0, v1

    invoke-virtual {v2, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 705
    :goto_0
    return-void

    .line 703
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->f:Landroid/widget/SeekBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0
.end method

.method c()V
    .locals 2

    .prologue
    .line 708
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->j:Lcat;

    if-eqz v0, :cond_1

    .line 709
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->l:Lcao;

    invoke-virtual {v0}, Lcao;->b()I

    move-result v0

    .line 710
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 712
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->j:Lcat;

    invoke-interface {v0}, Lcat;->a()V

    .line 717
    :cond_1
    :goto_0
    return-void

    .line 714
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->j:Lcat;

    invoke-interface {v0}, Lcat;->b()V

    goto :goto_0
.end method

.method public d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 742
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->l:Lcao;

    if-eqz v0, :cond_0

    .line 743
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->l:Lcao;

    invoke-virtual {v0, v1}, Lcao;->a(Lcom/google/android/apps/hangouts/views/AudioAttachmentView;)V

    .line 744
    iput-object v1, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->l:Lcao;

    .line 746
    :cond_0
    return-void
.end method

.method public e()V
    .locals 0

    .prologue
    .line 750
    return-void
.end method

.method public f()V
    .locals 0

    .prologue
    .line 754
    return-void
.end method

.method public g()V
    .locals 0

    .prologue
    .line 758
    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 574
    sget v0, Lg;->fh:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->c:Landroid/view/View;

    .line 575
    sget v0, Lg;->ha:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->d:Landroid/view/View;

    .line 576
    sget v0, Lg;->gZ:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->e:Landroid/view/View;

    .line 577
    sget v0, Lg;->fs:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->f:Landroid/widget/SeekBar;

    .line 578
    sget v0, Lg;->bb:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->g:Landroid/widget/TextView;

    .line 579
    sget v0, Lg;->hK:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->h:Landroid/widget/TextView;

    .line 581
    sget v0, Lg;->fr:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->b:Landroid/view/View;

    .line 582
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->b:Landroid/view/View;

    new-instance v1, Lcak;

    invoke-direct {v1, p0}, Lcak;-><init>(Lcom/google/android/apps/hangouts/views/AudioAttachmentView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 589
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->c:Landroid/view/View;

    new-instance v1, Lcal;

    invoke-direct {v1, p0}, Lcal;-><init>(Lcom/google/android/apps/hangouts/views/AudioAttachmentView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 600
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->d:Landroid/view/View;

    new-instance v1, Lcam;

    invoke-direct {v1, p0}, Lcam;-><init>(Lcom/google/android/apps/hangouts/views/AudioAttachmentView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 613
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->e:Landroid/view/View;

    new-instance v1, Lcan;

    invoke-direct {v1, p0}, Lcan;-><init>(Lcom/google/android/apps/hangouts/views/AudioAttachmentView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 626
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->f:Landroid/widget/SeekBar;

    invoke-virtual {v0, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 627
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->g:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->a(Landroid/widget/TextView;I)V

    .line 628
    return-void
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 2

    .prologue
    .line 774
    if-eqz p3, :cond_0

    .line 775
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->l:Lcao;

    if-eqz v0, :cond_1

    .line 776
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->l:Lcao;

    invoke-virtual {v0}, Lcao;->e()I

    move-result v0

    mul-int/2addr v0, p2

    div-int/lit8 v0, v0, 0x64

    .line 777
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->g:Landroid/widget/TextView;

    invoke-static {v1, v0}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->a(Landroid/widget/TextView;I)V

    .line 782
    :cond_0
    :goto_0
    return-void

    .line 779
    :cond_1
    const-string v0, "Babel"

    const-string v1, "onProgressChanged: audioPlaybackController is null"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2

    .prologue
    .line 786
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->l:Lcao;

    if-eqz v0, :cond_0

    .line 787
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->l:Lcao;

    invoke-virtual {v0}, Lcao;->i()V

    .line 791
    :goto_0
    return-void

    .line 789
    :cond_0
    const-string v0, "Babel"

    const-string v1, "onStartTrackingTouch: audioPlaybackController is null"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 5

    .prologue
    .line 795
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->l:Lcao;

    if-eqz v0, :cond_1

    .line 796
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->l:Lcao;

    .line 797
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->l:Lcao;

    invoke-virtual {v2}, Lcao;->e()I

    move-result v2

    mul-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x64

    sget-boolean v2, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->a:Z

    if-eqz v2, :cond_0

    const-string v2, "sendSeek"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "new position is "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcao;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcao;->a(I)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "position_in_milliseconds"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, v0, Lcao;->a:Landroid/content/Context;

    invoke-virtual {v0, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 798
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->l:Lcao;

    invoke-virtual {v0}, Lcao;->h()V

    .line 802
    :goto_0
    return-void

    .line 800
    :cond_1
    const-string v0, "Babel"

    const-string v1, "onStopTrackingTouch: audioPlaybackController is null"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
