.class public Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;
.super Laou;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/apps/hangouts/views/FixedParticipantsView;

.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/view/View;

.field private final e:Landroid/view/View;

.field private final f:Larv;

.field private final g:Lapk;

.field private h:Lapx;

.field private final i:Lbme;

.field private j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 53
    invoke-direct {p0, p1, p2}, Laou;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    iput-boolean v3, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->j:Z

    .line 54
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 56
    sget v1, Lf;->fo:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 58
    sget v0, Lg;->ct:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/FixedParticipantsView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->a:Lcom/google/android/apps/hangouts/views/FixedParticipantsView;

    .line 60
    sget v0, Lg;->fd:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->b:Landroid/widget/TextView;

    .line 61
    sget v0, Lg;->ag:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->c:Landroid/widget/TextView;

    .line 62
    sget v0, Lg;->ce:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->d:Landroid/view/View;

    .line 63
    sget v0, Lg;->cf:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->e:Landroid/view/View;

    .line 65
    invoke-static {}, Lapk;->b()Lapk;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->g:Lapk;

    .line 66
    new-instance v0, Larv;

    invoke-direct {v0, p0, v3}, Larv;-><init>(Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;B)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->f:Larv;

    .line 68
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lbme;

    invoke-static {v0, v1}, Lcyj;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbme;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->i:Lbme;

    .line 69
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->g()V

    return-void
.end method

.method private g()V
    .locals 9

    .prologue
    const/16 v4, 0x8

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->h:Lapx;

    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->h:Lapx;

    invoke-virtual {v0}, Lapx;->B()Ljava/util/List;

    move-result-object v0

    .line 124
    iget-object v3, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->h:Lapx;

    invoke-virtual {v3}, Lapx;->J()I

    move-result v3

    if-ne v3, v2, :cond_6

    .line 125
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v2, :cond_6

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->h:Lapx;

    invoke-virtual {v0}, Lapx;->S()Ljava/util/List;

    move-result-object v0

    .line 127
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v2, :cond_6

    .line 128
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laqd;

    .line 129
    invoke-virtual {v0}, Laqd;->b()Ljava/lang/String;

    move-result-object v3

    .line 130
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 131
    iget-object v5, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lh;->cc:I

    new-array v8, v2, [Ljava/lang/Object;

    aput-object v3, v8, v1

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    instance-of v3, v0, Laqf;

    if-eqz v3, :cond_5

    .line 134
    check-cast v0, Laqf;

    invoke-virtual {v0}, Laqf;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    move v3, v2

    .line 141
    :goto_0
    iget-boolean v5, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->j:Z

    if-nez v5, :cond_0

    .line 142
    iput-boolean v2, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->j:Z

    .line 143
    iget-object v5, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->i:Lbme;

    invoke-interface {v5}, Lbme;->a()Laux;

    move-result-object v5

    const/16 v6, 0x65f

    .line 144
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 143
    invoke-virtual {v5, v6}, Laux;->a(Ljava/lang/Integer;)V

    .line 149
    :cond_0
    :goto_1
    iget-object v5, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->c:Landroid/widget/TextView;

    if-eqz v2, :cond_2

    move v2, v1

    :goto_2
    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 150
    iget-object v5, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->d:Landroid/view/View;

    if-eqz v3, :cond_3

    move v2, v1

    :goto_3
    invoke-virtual {v5, v2}, Landroid/view/View;->setVisibility(I)V

    .line 151
    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->e:Landroid/view/View;

    if-eqz v0, :cond_4

    :goto_4
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 152
    return-void

    :cond_1
    move v0, v2

    move v3, v1

    .line 137
    goto :goto_0

    :cond_2
    move v2, v4

    .line 149
    goto :goto_2

    :cond_3
    move v2, v4

    .line 150
    goto :goto_3

    :cond_4
    move v1, v4

    .line 151
    goto :goto_4

    :cond_5
    move v0, v1

    move v3, v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v3, v1

    move v2, v1

    goto :goto_1
.end method


# virtual methods
.method public a(Lapx;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0x8

    const/4 v4, 0x1

    .line 73
    invoke-virtual {p1}, Lapx;->K()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {p1}, Lapx;->B()Ljava/util/List;

    move-result-object v1

    .line 75
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v4, :cond_1

    .line 76
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/apps/hangouts/R$drawable;->av:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 77
    :goto_0
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_2

    .line 78
    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->a:Lcom/google/android/apps/hangouts/views/FixedParticipantsView;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/hangouts/views/FixedParticipantsView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 83
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->a:Lcom/google/android/apps/hangouts/views/FixedParticipantsView;

    invoke-virtual {p1}, Lapx;->j()Lyj;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/hangouts/views/FixedParticipantsView;->a(Lyj;Ljava/util/List;)V

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lh;->ey:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 88
    invoke-virtual {p1}, Lapx;->J()I

    move-result v0

    if-ne v0, v4, :cond_3

    .line 89
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v4, :cond_3

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->g:Lapk;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->f:Larv;

    invoke-virtual {v0, v1}, Lapk;->a(Lapo;)V

    .line 91
    iput-object p1, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->h:Lapx;

    .line 92
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->g()V

    .line 98
    :goto_2
    invoke-virtual {p0, v6}, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->setVisibility(I)V

    .line 100
    :cond_0
    return-void

    .line 76
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 80
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->a:Lcom/google/android/apps/hangouts/views/FixedParticipantsView;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/hangouts/views/FixedParticipantsView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 94
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->d:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->e:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method d()V
    .locals 2

    .prologue
    .line 109
    invoke-super {p0}, Laou;->d()V

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->h:Lapx;

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->g:Lapk;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->f:Larv;

    invoke-virtual {v0, v1}, Lapk;->b(Lapo;)V

    .line 112
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->h:Lapx;

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->a:Lcom/google/android/apps/hangouts/views/FixedParticipantsView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/FixedParticipantsView;->a()V

    .line 115
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/hangout/OutgoingAudioOnlyRingOverlayView;->setVisibility(I)V

    .line 116
    return-void
.end method

.method protected e()Z
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x0

    return v0
.end method
