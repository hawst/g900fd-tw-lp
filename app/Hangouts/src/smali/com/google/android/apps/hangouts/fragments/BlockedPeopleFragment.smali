.class public Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;
.super Lakl;
.source "PG"

# interfaces
.implements Law;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lakl;",
        "Law",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lyj;

.field private b:Landroid/widget/ListView;

.field private c:Labz;

.field private d:Z

.field private e:I

.field private f:Lfe;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lfe",
            "<",
            "Ljava/lang/Integer;",
            "Lacb;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcav;

.field private final h:Lbor;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lakl;-><init>()V

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->d:Z

    .line 52
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->e:I

    .line 69
    new-instance v0, Labx;

    invoke-direct {v0, p0}, Labx;-><init>(Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->g:Lcav;

    .line 96
    new-instance v0, Laby;

    invoke-direct {v0, p0}, Laby;-><init>(Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->h:Lbor;

    .line 232
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->b()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;I)V
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->f:Lfe;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfe;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->f:Lfe;

    invoke-virtual {v0}, Lfe;->size()I

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->c()V

    :cond_0
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->d(Landroid/view/View;)V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;)Lyj;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->a:Lyj;

    return-object v0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 154
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->d:Z

    if-nez v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->h:Lbor;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbor;)V

    .line 156
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->d:Z

    .line 158
    :cond_0
    return-void
.end method

.method public static synthetic c(Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;)Lfe;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->f:Lfe;

    return-object v0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 161
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->d:Z

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->h:Lbor;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lbor;)V

    .line 163
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->d:Z

    .line 165
    :cond_0
    return-void
.end method

.method public static synthetic d(Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;)Labz;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->c:Labz;

    return-object v0
.end method

.method private d(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 348
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 349
    invoke-virtual {p0, p1}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->e(Landroid/view/View;)V

    .line 359
    :goto_0
    return-void

    .line 350
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 351
    invoke-virtual {p0, p1}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->c(Landroid/view/View;)V

    .line 352
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->N:I

    .line 353
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 354
    const/4 v1, 0x0

    invoke-static {p1, v1, v0}, Lf;->a(Landroid/view/View;Landroid/view/accessibility/AccessibilityManager;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 357
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->b(Landroid/view/View;)V

    goto :goto_0
.end method

.method public static synthetic e(Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;)I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->e:I

    return v0
.end method

.method public static synthetic f(Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;)V
    .locals 1

    .prologue
    .line 43
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->e:I

    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->c()V

    return-void
.end method

.method public static synthetic g(Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;)Lcav;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->g:Lcav;

    return-object v0
.end method


# virtual methods
.method protected a(Landroid/view/View;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 366
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 367
    const v0, 0x1020004

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 368
    sget v0, Lg;->eb:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 369
    sget v0, Lg;->eg:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 371
    :cond_0
    return-void
.end method

.method public a(Ldg;Landroid/database/Cursor;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 411
    invoke-virtual {p1}, Ldg;->l()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 417
    :goto_0
    return-void

    .line 413
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->c:Labz;

    invoke-virtual {v0, p2}, Labz;->a(Landroid/database/Cursor;)V

    .line 414
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->d(Landroid/view/View;)V

    goto :goto_0

    .line 411
    nop

    :pswitch_data_0
    .packed-switch 0x402
        :pswitch_0
    .end packed-switch
.end method

.method protected a()Z
    .locals 1

    .prologue
    .line 389
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->c:Labz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->c:Labz;

    invoke-virtual {v0}, Labz;->a()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->e:I

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 380
    invoke-super {p0, p1}, Lakl;->b(Landroid/view/View;)V

    .line 381
    sget v0, Lg;->eb:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 382
    sget v0, Lg;->eg:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 383
    return-void
.end method

.method protected isEmpty()Z
    .locals 1

    .prologue
    .line 341
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->c:Labz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->c:Labz;

    invoke-virtual {v0}, Labz;->a()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->c:Labz;

    invoke-virtual {v0}, Labz;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 221
    invoke-super {p0, p1}, Lakl;->onAttach(Landroid/app/Activity;)V

    .line 223
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 224
    const-string v1, "account_name"

    .line 225
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 224
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->a:Lyj;

    .line 226
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 190
    new-instance v0, Lfe;

    invoke-direct {v0}, Lfe;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->f:Lfe;

    .line 191
    invoke-super {p0, p1}, Lakl;->onCreate(Landroid/os/Bundle;)V

    .line 192
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Ldg;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 394
    packed-switch p1, :pswitch_data_0

    move-object v0, v5

    .line 406
    :goto_0
    return-object v0

    .line 396
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->a:Lyj;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/content/EsProvider;->c(Lyj;)Landroid/net/Uri;

    move-result-object v3

    .line 397
    new-instance v0, Lbaj;

    .line 398
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->getActivity()Ly;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->a:Lyj;

    sget-object v4, Laca;->a:[Ljava/lang/String;

    const-string v7, "name ASC"

    move-object v6, v5

    invoke-direct/range {v0 .. v7}, Lbaj;-><init>(Landroid/content/Context;Lyj;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 394
    nop

    :pswitch_data_0
    .packed-switch 0x402
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 323
    sget v0, Lf;->ek:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 324
    sget v0, Lg;->eg:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->b:Landroid/widget/ListView;

    .line 325
    new-instance v0, Labz;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->getActivity()Ly;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Labz;-><init>(Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->c:Labz;

    .line 326
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->b:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->c:Labz;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 328
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->getLoaderManager()Lav;

    move-result-object v0

    const/16 v2, 0x402

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0, v2, v3, p0}, Lav;->a(ILandroid/os/Bundle;Law;)Ldg;

    move-result-object v0

    invoke-virtual {v0}, Ldg;->p()V

    .line 329
    return-object v1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 196
    invoke-super {p0}, Lakl;->onDestroy()V

    .line 197
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->c()V

    .line 198
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->b:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 199
    iput-object v1, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->f:Lfe;

    .line 200
    return-void
.end method

.method public synthetic onLoadFinished(Ldg;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 43
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->a(Ldg;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Ldg;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 421
    invoke-virtual {p1}, Ldg;->l()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 426
    :goto_0
    return-void

    .line 423
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->c:Labz;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Labz;->a(Landroid/database/Cursor;)V

    goto :goto_0

    .line 421
    nop

    :pswitch_data_0
    .packed-switch 0x402
        :pswitch_0
    .end packed-switch
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 334
    invoke-super {p0, p1}, Lakl;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 335
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 204
    invoke-super {p0}, Lakl;->onStart()V

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->a:Lyj;

    invoke-virtual {v0}, Lyj;->u()Z

    move-result v0

    if-nez v0, :cond_0

    .line 207
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->b()V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->a:Lyj;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->g(Lyj;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->e:I

    .line 209
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->d(Landroid/view/View;)V

    .line 210
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 214
    invoke-super {p0}, Lakl;->onStop()V

    .line 215
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->c()V

    .line 216
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/BlockedPeopleFragment;->f:Lfe;

    invoke-virtual {v0}, Lfe;->clear()V

    .line 217
    return-void
.end method
