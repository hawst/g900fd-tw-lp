.class public Lcom/google/android/apps/hangouts/views/PresenceView;
.super Landroid/widget/ImageSwitcher;
.source "PG"


# static fields
.field private static final a:Z

.field private static b:Landroid/widget/ViewSwitcher$ViewFactory;

.field private static c:Landroid/view/animation/Animation;

.field private static d:Landroid/view/animation/Animation;


# instance fields
.field private e:I

.field private f:Lbzh;

.field private g:Lbdk;

.field private h:Z

.field private i:Lyj;

.field private j:Lbdn;

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lbys;->s:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/hangouts/views/PresenceView;->a:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/hangouts/views/PresenceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 59
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageSwitcher;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    iput v2, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->k:I

    .line 61
    if-eqz p2, :cond_0

    .line 63
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lwj;->E:[I

    invoke-virtual {v0, p2, v1, v2, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 69
    const/4 v0, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 75
    :cond_0
    sget-object v0, Lcom/google/android/apps/hangouts/views/PresenceView;->b:Landroid/widget/ViewSwitcher$ViewFactory;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    new-instance v2, Lcee;

    invoke-direct {v2, v0, v1}, Lcee;-><init>(Landroid/content/Context;Landroid/widget/FrameLayout$LayoutParams;)V

    sput-object v2, Lcom/google/android/apps/hangouts/views/PresenceView;->b:Landroid/widget/ViewSwitcher$ViewFactory;

    const v1, 0x10a0002

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/hangouts/views/PresenceView;->c:Landroid/view/animation/Animation;

    const v1, 0x10a0003

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/views/PresenceView;->d:Landroid/view/animation/Animation;

    .line 77
    :cond_1
    sget-object v0, Lcom/google/android/apps/hangouts/views/PresenceView;->b:Landroid/widget/ViewSwitcher$ViewFactory;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/PresenceView;->setFactory(Landroid/widget/ViewSwitcher$ViewFactory;)V

    .line 78
    invoke-static {}, Lbzh;->b()Lbzh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->f:Lbzh;

    .line 79
    return-void

    .line 71
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 144
    sget-boolean v0, Lcom/google/android/apps/hangouts/views/PresenceView;->a:Z

    if-eqz v0, :cond_0

    .line 145
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "PresenceView resetPerson "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->g:Lbdk;

    if-nez v0, :cond_1

    const-string v0, " no current participantId "

    .line 147
    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 145
    invoke-static {v1, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    :cond_0
    iput-object v3, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->g:Lbdk;

    .line 150
    iput-boolean v4, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->h:Z

    .line 151
    iput-object v3, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->i:Lyj;

    .line 152
    iput-object v3, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->j:Lbdn;

    .line 153
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/PresenceView;->reset()V

    .line 154
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/PresenceView;->setVisibility(I)V

    .line 155
    iput v4, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->k:I

    .line 156
    return-void

    .line 145
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->g:Lbdk;

    .line 147
    invoke-virtual {v0}, Lbdk;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lbdk;ZLyj;)V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 111
    sget-boolean v0, Lcom/google/android/apps/hangouts/views/PresenceView;->a:Z

    if-eqz v0, :cond_0

    .line 112
    const-string v2, "Babel"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "PresenceView setPerson isPhone= "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " , "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-nez p1, :cond_2

    const-string v0, " no participantId set,"

    .line 113
    :goto_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->g:Lbdk;

    if-nez v0, :cond_3

    const-string v0, " no previous participantId "

    .line 115
    :goto_1
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 112
    invoke-static {v2, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    :cond_0
    if-nez p1, :cond_4

    .line 118
    const-string v0, "Babel"

    const-string v1, "participantId should not be null"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    :cond_1
    :goto_2
    return-void

    .line 113
    :cond_2
    invoke-virtual {p1}, Lbdk;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->g:Lbdk;

    .line 115
    invoke-virtual {v0}, Lbdk;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 122
    :cond_4
    if-nez p3, :cond_5

    .line 123
    const-string v0, "Babel"

    const-string v1, "account should not be null"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 128
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->g:Lbdk;

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->g:Lbdk;

    invoke-virtual {v0, p1}, Lbdk;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 130
    const/4 v0, 0x1

    .line 133
    :goto_3
    iput-object p1, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->g:Lbdk;

    .line 134
    iput-boolean p2, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->h:Z

    .line 135
    iput-object p3, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->i:Lyj;

    .line 137
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->g:Lbdk;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->f:Lbzh;

    iget-object v4, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->g:Lbdk;

    iget-object v4, v4, Lbdk;->a:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->i:Lyj;

    invoke-virtual {v2, v4, v5}, Lbzh;->a(Ljava/lang/String;Lyj;)Lbdn;

    move-result-object v2

    :goto_4
    sget-boolean v4, Lcom/google/android/apps/hangouts/views/PresenceView;->a:Z

    if-eqz v4, :cond_6

    const-string v5, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "PresenceView redraw mIsPhone = "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v6, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->h:Z

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v4, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->g:Lbdk;

    if-nez v4, :cond_a

    const-string v4, " no current participantId, "

    :goto_5
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " animate="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    if-nez v2, :cond_b

    const-string v4, "no current presence, "

    :goto_6
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v4, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->j:Lbdn;

    if-nez v4, :cond_c

    const-string v4, "no previous presence"

    :goto_7
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    iput-object v2, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->j:Lbdn;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->i:Lyj;

    invoke-static {v2}, Lbtf;->a(Lyj;)Z

    move-result v2

    if-eqz v2, :cond_13

    iget-boolean v2, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->h:Z

    if-nez v2, :cond_d

    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->j:Lbdn;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->j:Lbdn;

    iget-boolean v2, v2, Lbdn;->b:Z

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->j:Lbdn;

    iget-boolean v2, v2, Lbdn;->a:Z

    if-nez v2, :cond_d

    :cond_7
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->j:Lbdn;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->j:Lbdn;

    iget-boolean v2, v2, Lbdn;->d:Z

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->j:Lbdn;

    iget-boolean v2, v2, Lbdn;->c:Z

    if-nez v2, :cond_d

    :cond_8
    invoke-virtual {p0, v8}, Lcom/google/android/apps/hangouts/views/PresenceView;->setVisibility(I)V

    iput v1, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->k:I

    goto/16 :goto_2

    :cond_9
    move-object v2, v3

    goto :goto_4

    :cond_a
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->g:Lbdk;

    invoke-virtual {v7}, Lbdk;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ", "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_5

    :cond_b
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lbdn;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ", "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_6

    :cond_c
    iget-object v4, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->j:Lbdn;

    invoke-virtual {v4}, Lbdn;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_7

    :cond_d
    if-eqz v0, :cond_e

    sget-object v0, Lcom/google/android/apps/hangouts/views/PresenceView;->c:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/PresenceView;->setInAnimation(Landroid/view/animation/Animation;)V

    sget-object v0, Lcom/google/android/apps/hangouts/views/PresenceView;->d:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/PresenceView;->setOutAnimation(Landroid/view/animation/Animation;)V

    :goto_8
    invoke-virtual {p0, v1}, Lcom/google/android/apps/hangouts/views/PresenceView;->setVisibility(I)V

    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->h:Z

    if-eqz v0, :cond_f

    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->cn:I

    :goto_9
    iget v1, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->k:I

    if-eq v1, v0, :cond_1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/PresenceView;->setImageResource(I)V

    iput v0, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->k:I

    goto/16 :goto_2

    :cond_e
    invoke-virtual {p0, v3}, Lcom/google/android/apps/hangouts/views/PresenceView;->setInAnimation(Landroid/view/animation/Animation;)V

    invoke-virtual {p0, v3}, Lcom/google/android/apps/hangouts/views/PresenceView;->setOutAnimation(Landroid/view/animation/Animation;)V

    goto :goto_8

    :cond_f
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->j:Lbdn;

    iget-boolean v0, v0, Lbdn;->c:Z

    if-eqz v0, :cond_11

    iget v0, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->e:I

    if-nez v0, :cond_10

    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->cx:I

    goto :goto_9

    :cond_10
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->cx:I

    goto :goto_9

    :cond_11
    iget v0, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->e:I

    if-nez v0, :cond_12

    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->cb:I

    goto :goto_9

    :cond_12
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->cc:I

    goto :goto_9

    :cond_13
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->j:Lbdn;

    if-eqz v2, :cond_14

    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->j:Lbdn;

    iget-boolean v2, v2, Lbdn;->b:Z

    if-nez v2, :cond_15

    :cond_14
    iget-boolean v2, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->h:Z

    if-nez v2, :cond_15

    invoke-virtual {p0, v8}, Lcom/google/android/apps/hangouts/views/PresenceView;->setVisibility(I)V

    iput v1, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->k:I

    goto/16 :goto_2

    :cond_15
    if-eqz v0, :cond_17

    sget-object v0, Lcom/google/android/apps/hangouts/views/PresenceView;->c:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/PresenceView;->setInAnimation(Landroid/view/animation/Animation;)V

    sget-object v0, Lcom/google/android/apps/hangouts/views/PresenceView;->d:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/PresenceView;->setOutAnimation(Landroid/view/animation/Animation;)V

    :goto_a
    invoke-virtual {p0, v1}, Lcom/google/android/apps/hangouts/views/PresenceView;->setVisibility(I)V

    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->h:Z

    if-eqz v0, :cond_18

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->cn:I

    :cond_16
    :goto_b
    iget v0, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->k:I

    if-eq v0, v1, :cond_1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/hangouts/views/PresenceView;->setImageResource(I)V

    iput v1, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->k:I

    goto/16 :goto_2

    :cond_17
    invoke-virtual {p0, v3}, Lcom/google/android/apps/hangouts/views/PresenceView;->setInAnimation(Landroid/view/animation/Animation;)V

    invoke-virtual {p0, v3}, Lcom/google/android/apps/hangouts/views/PresenceView;->setOutAnimation(Landroid/view/animation/Animation;)V

    goto :goto_a

    :cond_18
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->j:Lbdn;

    iget-boolean v0, v0, Lbdn;->a:Z

    if-eqz v0, :cond_16

    iget v0, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->e:I

    if-nez v0, :cond_19

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->cb:I

    goto :goto_b

    :cond_19
    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->cc:I

    goto :goto_b

    :cond_1a
    move v0, v1

    goto/16 :goto_3
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->j:Lbdn;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->j:Lbdn;

    iget-boolean v0, v0, Lbdn;->b:Z

    if-nez v0, :cond_1

    .line 249
    :cond_0
    const/4 v0, 0x0

    .line 251
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public c()Z
    .locals 2

    .prologue
    .line 258
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->j:Lbdn;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->j:Lbdn;

    iget-boolean v0, v0, Lbdn;->b:Z

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->j:Lbdn;

    iget-boolean v0, v0, Lbdn;->a:Z

    .line 262
    :goto_0
    return v0

    .line 261
    :cond_0
    const-string v0, "Babel"

    const-string v1, "Accessing PresenceView.isReachable without available reachability."

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Z
    .locals 2

    .prologue
    .line 266
    iget v0, p0, Lcom/google/android/apps/hangouts/views/PresenceView;->k:I

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->cn:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
