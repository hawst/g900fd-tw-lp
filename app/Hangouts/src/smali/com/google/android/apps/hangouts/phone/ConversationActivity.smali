.class public Lcom/google/android/apps/hangouts/phone/ConversationActivity;
.super Lakn;
.source "PG"

# interfaces
.implements Lahm;
.implements Lbkp;


# instance fields
.field private r:Lyj;

.field private s:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lakn;-><init>()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/phone/ConversationActivity;)Lcom/google/android/apps/hangouts/fragments/ConversationFragment;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ConversationActivity;->s:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    return-object v0
.end method

.method private c(Landroid/content/Intent;)Lahc;
    .locals 4

    .prologue
    .line 104
    const-string v0, "conversation_parameters"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lahc;

    .line 106
    if-nez v0, :cond_0

    .line 107
    const-string v0, "conversation_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 108
    const-string v0, "conversation_type"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 110
    new-instance v0, Lahc;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/phone/ConversationActivity;->r:Lyj;

    invoke-direct {v0, v1, v3, v2}, Lahc;-><init>(Ljava/lang/String;Lyj;I)V

    .line 112
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/ConversationActivity;->a(Ljava/lang/Runnable;)V

    .line 137
    return-void
.end method

.method public a(Lbkn;)V
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ConversationActivity;->r:Lyj;

    invoke-static {p1, v0, p0, p0}, Lf;->a(Lbkn;Lyj;Ly;Lbkp;)V

    .line 148
    return-void
.end method

.method public a(Lccp;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ConversationActivity;->r:Lyj;

    const-class v1, Lcom/google/android/apps/hangouts/phone/BabelPhotoViewActivity;

    new-instance v2, Lpm;

    invoke-direct {v2, p0, v1, v6}, Lpm;-><init>(Landroid/content/Context;Ljava/lang/Class;B)V

    invoke-static {v0, p3}, Lcom/google/android/apps/hangouts/content/EsProvider;->d(Lyj;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lpm;->b(Ljava/lang/String;)Lpm;

    invoke-virtual {v2, p2}, Lpm;->a(Ljava/lang/String;)Lpm;

    const/4 v1, 0x2

    new-array v1, v1, [I

    invoke-virtual {p1, v1}, Lccp;->getLocationOnScreen([I)V

    aget v3, v1, v6

    const/4 v4, 0x1

    aget v1, v1, v4

    invoke-virtual {p1}, Lccp;->getMeasuredWidth()I

    move-result v4

    invoke-virtual {p1}, Lccp;->getMeasuredHeight()I

    move-result v5

    invoke-virtual {v2, v3, v1, v4, v5}, Lpm;->a(IIII)Lpm;

    sget-object v1, Laye;->a:[Ljava/lang/String;

    invoke-virtual {v2, v1}, Lpm;->a([Ljava/lang/String;)Lpm;

    invoke-virtual {v2}, Lpm;->a()Lpm;

    invoke-virtual {v2, v6}, Lpm;->a(Z)Lpm;

    const/high16 v1, 0x41000000    # 8.0f

    invoke-virtual {v2, v1}, Lpm;->a(F)Lpm;

    invoke-virtual {v2}, Lpm;->b()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "account_name"

    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0, v6, v6}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 143
    return-void
.end method

.method public a(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 128
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/ConversationActivity;->finish()V

    .line 129
    if-eqz p1, :cond_0

    .line 130
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 132
    :cond_0
    return-void
.end method

.method public a(Lt;)V
    .locals 1

    .prologue
    .line 66
    instance-of v0, p1, Lajs;

    if-eqz v0, :cond_0

    .line 71
    check-cast p1, Lajs;

    new-instance v0, Layp;

    invoke-direct {v0, p0}, Layp;-><init>(Lcom/google/android/apps/hangouts/phone/ConversationActivity;)V

    invoke-virtual {p1, v0}, Lajs;->a(Laka;)V

    .line 95
    :cond_0
    return-void
.end method

.method public b(Lbkn;)V
    .locals 3

    .prologue
    .line 163
    invoke-virtual {p1}, Lbkn;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 171
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized action: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lbkn;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    :goto_0
    :pswitch_0
    return-void

    .line 168
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ConversationActivity;->r:Lyj;

    invoke-virtual {p1, v0}, Lbkn;->a(Lyj;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/ConversationActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 163
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 157
    invoke-virtual {p0, p1}, Lcom/google/android/apps/hangouts/phone/ConversationActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 158
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/ConversationActivity;->g()Lkd;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkd;->a(Ljava/lang/CharSequence;)V

    .line 159
    return-void
.end method

.method public c_(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 152
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/ConversationActivity;->g()Lkd;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkd;->b(Ljava/lang/CharSequence;)V

    .line 153
    return-void
.end method

.method protected k()Lyj;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ConversationActivity;->r:Lyj;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 32
    invoke-super {p0, p1}, Lakn;->onCreate(Landroid/os/Bundle;)V

    .line 34
    sget v0, Lf;->eD:I

    sget v1, Lg;->s:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/hangouts/phone/ConversationActivity;->a(II)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/ConversationActivity;->setContentView(Landroid/view/View;)V

    .line 37
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/ConversationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 38
    const-string v0, "account_name"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 39
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/ConversationActivity;->r:Lyj;

    .line 42
    const-string v0, "conversation_opened_impression"

    const/4 v2, -0x1

    .line 43
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 44
    if-ltz v0, :cond_0

    .line 45
    iget-object v2, p0, Lcom/google/android/apps/hangouts/phone/ConversationActivity;->o:Lbme;

    invoke-interface {v2}, Lbme;->a()Laux;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Laux;->a(Ljava/lang/Integer;)V

    .line 53
    :cond_0
    const-string v0, "share_intent"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 54
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ConversationActivity;->r:Lyj;

    invoke-static {v0}, Lbkb;->e(Lyj;)V

    .line 57
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/ConversationActivity;->e()Lae;

    move-result-object v0

    .line 58
    sget v2, Lg;->aO:I

    invoke-virtual {v0, v2}, Lae;->a(I)Lt;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/ConversationActivity;->s:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 60
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ConversationActivity;->s:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lahm;)V

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ConversationActivity;->s:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-direct {p0, v1}, Lcom/google/android/apps/hangouts/phone/ConversationActivity;->c(Landroid/content/Intent;)Lahc;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lahc;Z)V

    .line 62
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 99
    invoke-super {p0, p1}, Lakn;->onNewIntent(Landroid/content/Intent;)V

    .line 100
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ConversationActivity;->s:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/phone/ConversationActivity;->c(Landroid/content/Intent;)Lahc;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lahc;)V

    .line 101
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 117
    invoke-super {p0}, Lakn;->onPause()V

    .line 118
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/ConversationActivity;->l()V

    .line 119
    return-void
.end method
