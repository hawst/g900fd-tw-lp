.class public Lcom/google/android/apps/hangouts/views/OobePromoStagingView;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field private a:Landroid/graphics/Paint;

.field private b:Landroid/graphics/drawable/Drawable;

.field private c:Lbbq;

.field private d:I

.field private e:I

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Lcdo;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 64
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 65
    sget v1, Lf;->gc:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 66
    sget v0, Lg;->dJ:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->f:Landroid/widget/TextView;

    .line 67
    sget v0, Lg;->dI:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->g:Landroid/widget/TextView;

    .line 68
    sget v0, Lg;->eR:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 69
    new-instance v1, Lcdn;

    invoke-direct {v1, p0}, Lcdn;-><init>(Lcom/google/android/apps/hangouts/views/OobePromoStagingView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->a:Landroid/graphics/Paint;

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->a:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->a:Landroid/graphics/Paint;

    const v1, 0xffffff

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->a:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 87
    return-void
.end method

.method private a()I
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->c:Lbbq;

    iget v0, v0, Lbbq;->c:I

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->c:Lbbq;

    iget v1, v1, Lbbq;->d:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/views/OobePromoStagingView;)Lcdo;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->h:Lcdo;

    return-object v0
.end method


# virtual methods
.method public a(Lbbq;)V
    .locals 3

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->c:Lbbq;

    invoke-static {p1, v0}, Lbbq;->a(Lbbq;Lbbq;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 95
    iput-object p1, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->c:Lbbq;

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->c:Lbbq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->c:Lbbq;

    iget v0, v0, Lbbq;->e:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "Babel"

    const-string v1, "unsupported oobe promo type!"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    :cond_0
    :goto_0
    return-void

    .line 96
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->f:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lh;->oG:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->g:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lh;->oF:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_1
    const-string v0, "Babel"

    const-string v1, "unexpected promo type: OOBE_TYPE_NONE"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->f:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->g:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lcdo;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->h:Lcdo;

    .line 91
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v5, 0x0

    .line 144
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->getMeasuredHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 146
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 147
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v3, Lf;->cv:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->c:Lbbq;

    if-eqz v0, :cond_2

    .line 150
    const/4 v0, 0x2

    new-array v0, v0, [I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->getLocationInWindow([I)V

    iget-object v3, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->c:Lbbq;

    iget v3, v3, Lbbq;->a:I

    aget v4, v0, v5

    sub-int/2addr v3, v4

    iput v3, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->d:I

    iget-object v3, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->c:Lbbq;

    iget v3, v3, Lbbq;->b:I

    const/4 v4, 0x1

    aget v0, v0, v4

    sub-int v0, v3, v0

    iput v0, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->e:I

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->f:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->g:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->f:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iget v3, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->e:I

    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->a()I

    move-result v4

    shl-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    invoke-virtual {v0, v5, v3, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    iget-object v3, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->f:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 151
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->a()I

    move-result v0

    .line 152
    iget v3, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->d:I

    iget-object v4, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->c:Lbbq;

    iget v4, v4, Lbbq;->c:I

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    .line 153
    iget v4, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->e:I

    iget-object v5, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->c:Lbbq;

    iget v5, v5, Lbbq;->d:I

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    .line 154
    int-to-float v5, v3

    int-to-float v6, v4

    int-to-float v7, v0

    iget-object v8, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->a:Landroid/graphics/Paint;

    invoke-virtual {v2, v5, v6, v7, v8}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 155
    const/4 v2, 0x0

    invoke-virtual {p1, v1, v9, v9, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 157
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->b:Landroid/graphics/drawable/Drawable;

    if-nez v2, :cond_1

    .line 158
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v5, Lcom/google/android/apps/hangouts/R$drawable;->cH:I

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->b:Landroid/graphics/drawable/Drawable;

    .line 162
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->b:Landroid/graphics/drawable/Drawable;

    sub-int v5, v3, v0

    sub-int v6, v4, v0

    add-int/2addr v3, v0

    add-int/2addr v0, v4

    invoke-virtual {v2, v5, v6, v3, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 166
    :cond_2
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 167
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 168
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 172
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v8, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->c:Lbbq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->c:Lbbq;

    iget v0, v0, Lbbq;->e:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->h:Lcdo;

    if-eqz v0, :cond_0

    .line 175
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 176
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    .line 177
    new-instance v2, Landroid/graphics/Rect;

    iget v3, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->d:I

    iget v4, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->e:I

    iget v5, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->d:I

    iget-object v6, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->c:Lbbq;

    iget v6, v6, Lbbq;->c:I

    add-int/2addr v5, v6

    iget v6, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->e:I

    iget-object v7, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->c:Lbbq;

    iget v7, v7, Lbbq;->d:I

    add-int/2addr v6, v7

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 180
    invoke-virtual {v2, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->h:Lcdo;

    invoke-interface {v0}, Lcdo;->a()V

    .line 186
    :cond_0
    :goto_0
    return v8

    .line 183
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/OobePromoStagingView;->h:Lcdo;

    invoke-interface {v0}, Lcdo;->b()V

    goto :goto_0
.end method
