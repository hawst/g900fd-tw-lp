.class public Lcom/google/android/apps/hangouts/views/MessageListItemView;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Lcdg;


# static fields
.field private static a:Ljava/lang/String;

.field private static final af:Landroid/view/animation/Animation;

.field private static final ag:Landroid/view/animation/Animation;

.field private static b:Z

.field private static c:I

.field private static d:Ljava/lang/String;

.field private static e:Ljava/lang/String;

.field private static f:Ljava/lang/String;

.field private static g:Ljava/lang/String;

.field private static h:Ljava/lang/String;


# instance fields
.field private A:Landroid/view/ViewGroup;

.field private B:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcde;",
            ">;"
        }
    .end annotation
.end field

.field private C:J

.field private D:Z

.field private E:Z

.field private final F:Lcom/google/android/apps/hangouts/views/MessageBubbleView;

.field private final G:Landroid/widget/FrameLayout;

.field private H:I

.field private I:Z

.field private J:I

.field private K:Ljava/lang/String;

.field private L:Ljava/lang/String;

.field private M:Ljava/lang/String;

.field private N:Ljava/lang/String;

.field private O:Z

.field private P:F

.field private Q:I

.field private R:I

.field private S:I

.field private T:J

.field private U:J

.field private V:Ljava/lang/String;

.field private W:Ljava/lang/String;

.field private Z:Ljava/lang/String;

.field private aa:I

.field private ab:Ljava/lang/String;

.field private ac:Ljava/lang/String;

.field private ad:Ljava/lang/String;

.field private ae:Lyj;

.field private ah:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lbvy;",
            ">;"
        }
    .end annotation
.end field

.field private ai:Z

.field private i:Landroid/widget/TextView;

.field private j:Lcom/google/android/apps/hangouts/views/ScalingTextView;

.field private k:Landroid/view/View;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/widget/ImageView;

.field private n:Landroid/view/View;

.field private o:Lcom/google/android/apps/hangouts/views/ScalingTextView;

.field private p:Landroid/view/View;

.field private q:Lcom/google/android/apps/hangouts/views/AvatarView;

.field private r:Lbdk;

.field private s:I

.field private t:Ljava/lang/String;

.field private u:J

.field private v:Lccu;

.field private w:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    .line 81
    const-string v0, "Babel"

    sput-object v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->a:Ljava/lang/String;

    .line 182
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 189
    sput-object v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->af:Landroid/view/animation/Animation;

    const-wide/16 v1, 0xc8

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 190
    sget-object v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->af:Landroid/view/animation/Animation;

    new-instance v1, Lwq;

    invoke-direct {v1}, Lwq;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 193
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 200
    sput-object v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->ag:Landroid/view/animation/Animation;

    const-wide/16 v1, 0x15e

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 201
    sget-object v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->ag:Landroid/view/animation/Animation;

    new-instance v1, Lwq;

    invoke-direct {v1}, Lwq;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 202
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 251
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 252
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 255
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 119
    iput-boolean v2, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->E:Z

    .line 133
    iput v2, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->H:I

    .line 135
    iput-boolean v2, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->I:Z

    .line 145
    iput-boolean v2, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->O:Z

    .line 147
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->P:F

    .line 593
    iput-boolean v2, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->ai:Z

    .line 257
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 258
    sget v0, Lf;->fT:I

    .line 259
    invoke-virtual {v1, v0, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/MessageBubbleView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->F:Lcom/google/android/apps/hangouts/views/MessageBubbleView;

    .line 260
    sget v0, Lf;->fS:I

    .line 261
    invoke-virtual {v1, v0, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->G:Landroid/widget/FrameLayout;

    .line 263
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->G:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->addView(Landroid/view/View;)V

    .line 264
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->F:Lcom/google/android/apps/hangouts/views/MessageBubbleView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->addView(Landroid/view/View;)V

    .line 266
    sget-boolean v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->b:Z

    if-nez v0, :cond_0

    .line 267
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 268
    sget v1, Lf;->dp:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/hangouts/views/MessageListItemView;->c:I

    .line 269
    sget v1, Lh;->cA:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/hangouts/views/MessageListItemView;->d:Ljava/lang/String;

    .line 270
    sget v1, Lh;->om:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/hangouts/views/MessageListItemView;->e:Ljava/lang/String;

    .line 271
    sget v1, Lh;->oo:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/hangouts/views/MessageListItemView;->f:Ljava/lang/String;

    .line 272
    sget v1, Lh;->on:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/hangouts/views/MessageListItemView;->g:Ljava/lang/String;

    .line 273
    sget v1, Lh;->nE:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->h:Ljava/lang/String;

    .line 274
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->b:Z

    .line 276
    :cond_0
    return-void
.end method

.method private A()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 813
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->I:Z

    if-nez v0, :cond_0

    .line 814
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->I:Z

    .line 815
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->k:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 816
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->p:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 817
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->n:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 818
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->o:Lcom/google/android/apps/hangouts/views/ScalingTextView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/views/ScalingTextView;->a(F)V

    .line 819
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->o:Lcom/google/android/apps/hangouts/views/ScalingTextView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/views/ScalingTextView;->b(F)V

    .line 821
    :cond_0
    return-void
.end method

.method private B()V
    .locals 3

    .prologue
    .line 1045
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->B:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 1046
    const/4 v0, 0x0

    move v1, v0

    .line 1047
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->B:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1048
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->B:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcde;

    .line 1049
    instance-of v2, v0, Lcfc;

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->z:Z

    if-nez v2, :cond_1

    .line 1050
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->B:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1051
    invoke-interface {v0}, Lcde;->d()V

    .line 1052
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->A:Landroid/view/ViewGroup;

    check-cast v0, Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0

    .line 1054
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 1056
    goto :goto_0

    .line 1057
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->z:Z

    if-nez v0, :cond_3

    .line 1058
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->B:Ljava/util/List;

    .line 1059
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->A:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1062
    :cond_3
    return-void
.end method

.method private C()V
    .locals 8

    .prologue
    const/high16 v7, 0x41a00000    # 20.0f

    const/high16 v4, 0x41300000    # 11.0f

    const/high16 v3, 0x40800000    # 4.0f

    const v6, 0x4039999a    # 2.9f

    const/4 v5, 0x1

    .line 1088
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1089
    iget-boolean v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->E:Z

    if-eqz v1, :cond_1

    .line 1090
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->l:Landroid/widget/TextView;

    sget v2, Lf;->cA:I

    .line 1091
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 1090
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1092
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->j:Lcom/google/android/apps/hangouts/views/ScalingTextView;

    sget v2, Lf;->cA:I

    .line 1093
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 1092
    invoke-virtual {v1, v2}, Lcom/google/android/apps/hangouts/views/ScalingTextView;->setTextColor(I)V

    .line 1094
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->i:Landroid/widget/TextView;

    sget v2, Lf;->cx:I

    .line 1095
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 1094
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1104
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->i:Landroid/widget/TextView;

    sget v2, Lf;->cy:I

    .line 1105
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 1104
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLinkTextColor(I)V

    .line 1108
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->requestLayout()V

    .line 1109
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->D:Z

    iget-boolean v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->E:Z

    iget v2, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->Q:I

    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->F:Lcom/google/android/apps/hangouts/views/MessageBubbleView;

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->cF:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->setBackgroundResource(I)V

    .line 1110
    :cond_0
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->D:Z

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->x()Z

    move-result v2

    xor-int/2addr v0, v2

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->F:Lcom/google/android/apps/hangouts/views/MessageBubbleView;

    invoke-static {v5, v4, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    invoke-static {v5, v3, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v3, v3

    invoke-static {v5, v7, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    float-to-int v4, v4

    invoke-static {v5, v6, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v2, v3, v4, v1}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->setPadding(IIII)V

    .line 1111
    :goto_2
    return-void

    .line 1097
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->l:Landroid/widget/TextView;

    sget v2, Lf;->cz:I

    .line 1098
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 1097
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1099
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->j:Lcom/google/android/apps/hangouts/views/ScalingTextView;

    sget v2, Lf;->cz:I

    .line 1100
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 1099
    invoke-virtual {v1, v2}, Lcom/google/android/apps/hangouts/views/ScalingTextView;->setTextColor(I)V

    .line 1101
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->i:Landroid/widget/TextView;

    sget v2, Lf;->cw:I

    .line 1102
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 1101
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 1109
    :cond_2
    if-nez v1, :cond_3

    if-eqz v0, :cond_3

    invoke-static {v2}, Lf;->d(I)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->F:Lcom/google/android/apps/hangouts/views/MessageBubbleView;

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->cC:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->setBackgroundResource(I)V

    goto :goto_1

    :cond_3
    if-nez v1, :cond_4

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->F:Lcom/google/android/apps/hangouts/views/MessageBubbleView;

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->cG:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->setBackgroundResource(I)V

    goto :goto_1

    :cond_4
    if-eqz v1, :cond_5

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->F:Lcom/google/android/apps/hangouts/views/MessageBubbleView;

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->cE:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->setBackgroundResource(I)V

    goto :goto_1

    :cond_5
    if-nez v1, :cond_0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->F:Lcom/google/android/apps/hangouts/views/MessageBubbleView;

    sget v1, Lcom/google/android/apps/hangouts/R$drawable;->cD:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->setBackgroundResource(I)V

    goto/16 :goto_1

    .line 1110
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->F:Lcom/google/android/apps/hangouts/views/MessageBubbleView;

    invoke-static {v5, v7, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    invoke-static {v5, v3, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v3, v3

    invoke-static {v5, v4, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    float-to-int v4, v4

    invoke-static {v5, v6, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v2, v3, v4, v1}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->setPadding(IIII)V

    goto :goto_2
.end method

.method private D()Lbvy;
    .locals 2

    .prologue
    .line 1424
    new-instance v0, Lbvy;

    invoke-direct {v0}, Lbvy;-><init>()V

    .line 1425
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->x:Ljava/lang/String;

    iput-object v1, v0, Lbvy;->a:Ljava/lang/String;

    .line 1426
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->ac:Ljava/lang/String;

    iput-object v1, v0, Lbvy;->b:Ljava/lang/String;

    .line 1427
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/views/MessageListItemView;Ljava/lang/Long;)Landroid/net/Uri;
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 79
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "lookup"

    aput-object v3, v2, v6

    const-string v3, "_id"

    aput-object v3, v2, v4

    const-string v3, "_id= ?"

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    const-string v1, "lookup"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    invoke-static {v1, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/views/MessageListItemView;)Lcom/google/android/apps/hangouts/fragments/ConversationFragment;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->w:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    return-object v0
.end method

.method private static a(Ljava/util/List;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1764
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 1765
    :cond_0
    const/4 v0, 0x0

    .line 1771
    :goto_0
    return-object v0

    .line 1767
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1768
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1769
    invoke-static {v0}, Lbzd;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1771
    :cond_2
    const-string v0, ", "

    invoke-static {v0}, Lebe;->a(Ljava/lang/String;)Lebe;

    move-result-object v0

    invoke-virtual {v0, v1}, Lebe;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1334
    invoke-static {p2}, Lf;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, Lf;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "hangouts/gv_voicemail"

    .line 1335
    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1337
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->B:Ljava/util/List;

    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 1338
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->B:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcde;

    .line 1339
    invoke-interface {v0, p3}, Lcde;->a(Ljava/lang/String;)V

    .line 1340
    instance-of v1, v0, Lcdf;

    if-eqz v1, :cond_1

    .line 1341
    check-cast v0, Lcdf;

    invoke-interface {v0, p4}, Lcdf;->d_(Ljava/lang/String;)V

    .line 1344
    :cond_1
    return-void
.end method

.method private static a(Landroid/widget/TextView;)V
    .locals 1

    .prologue
    .line 1065
    if-eqz p0, :cond_0

    .line 1066
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1068
    :cond_0
    return-void
.end method

.method private a(Lcde;)V
    .locals 2

    .prologue
    .line 1405
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->B:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1406
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->B:Ljava/util/List;

    .line 1407
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->A:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1409
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->A:Landroid/view/ViewGroup;

    move-object v0, p1

    check-cast v0, Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1410
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->B:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1411
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 789
    if-nez p1, :cond_0

    .line 790
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->l:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 795
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->j:Lcom/google/android/apps/hangouts/views/ScalingTextView;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/hangouts/views/ScalingTextView;->setText(Ljava/lang/CharSequence;)V

    .line 796
    return-void

    .line 792
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->l:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 793
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->l:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;IIZLyj;Lcom/google/android/apps/hangouts/fragments/ConversationFragment;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;DDLjava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 12

    .prologue
    .line 1353
    invoke-static {p2}, Lf;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1354
    new-instance v2, Lccp;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lccp;-><init>(Landroid/content/Context;)V

    .line 1355
    iget-object v8, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->v:Lccu;

    move-object/from16 v3, p6

    move/from16 v4, p5

    move-object v5, p1

    move v6, p3

    move/from16 v7, p4

    move-object v9, p0

    move-object v10, p2

    move-object/from16 v11, p18

    invoke-virtual/range {v2 .. v11}, Lccp;->a(Lyj;ZLjava/lang/String;IILccu;Lcom/google/android/apps/hangouts/views/MessageListItemView;Ljava/lang/String;Ljava/lang/String;)V

    .line 1357
    invoke-direct {p0, v2}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->a(Lcde;)V

    .line 1402
    :goto_0
    return-void

    .line 1358
    :cond_0
    const-string v2, "hangouts/location"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1359
    new-instance v2, Lccr;

    .line 1360
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lccr;-><init>(Landroid/content/Context;)V

    .line 1362
    invoke-static {p1}, Lf;->n(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v3, p6

    move/from16 v4, p5

    move-object/from16 v6, p12

    move-wide/from16 v7, p13

    move-wide/from16 v9, p15

    move-object/from16 v11, p7

    .line 1361
    invoke-virtual/range {v2 .. v11}, Lccr;->a(Lyj;ZLjava/lang/String;Ljava/lang/String;DDLt;)V

    .line 1364
    invoke-direct {p0, v2}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->a(Lcde;)V

    goto :goto_0

    .line 1365
    :cond_1
    invoke-static {p2}, Lf;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1366
    if-eqz p8, :cond_2

    .line 1367
    new-instance v2, Lcet;

    .line 1368
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcet;-><init>(Landroid/content/Context;)V

    move-object/from16 v3, p6

    move/from16 v4, p5

    move-object v5, p1

    move-object v6, p2

    move-object/from16 v7, p7

    .line 1369
    invoke-virtual/range {v2 .. v7}, Lcet;->a(Lyj;ZLjava/lang/String;Ljava/lang/String;Lt;)V

    .line 1371
    invoke-direct {p0, v2}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->a(Lcde;)V

    goto :goto_0

    .line 1373
    :cond_2
    new-instance v2, Lcer;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcer;-><init>(Landroid/content/Context;)V

    move-object/from16 v3, p6

    move/from16 v4, p5

    move-object v5, p1

    move-object/from16 v6, p7

    move-object/from16 v7, p9

    move-object/from16 v8, p10

    move-object/from16 v9, p11

    .line 1374
    invoke-virtual/range {v2 .. v9}, Lcer;->a(Lyj;ZLjava/lang/String;Lt;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1376
    invoke-direct {p0, v2}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->a(Lcde;)V

    goto :goto_0

    .line 1378
    :cond_3
    invoke-static {p2}, Lf;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1380
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    sget v3, Lf;->eh:I

    iget-object v4, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->A:Landroid/view/ViewGroup;

    const/4 v5, 0x0

    .line 1381
    invoke-virtual {v2, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;

    .line 1382
    iget-object v3, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->t:Ljava/lang/String;

    invoke-virtual/range {p7 .. p7}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->r()Laha;

    move-result-object v4

    invoke-virtual {v2, p1, v3, v4}, Lcom/google/android/apps/hangouts/views/AudioAttachmentView;->a(Ljava/lang/String;Ljava/lang/String;Lcas;)V

    .line 1383
    invoke-direct {p0, v2}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->a(Lcde;)V

    goto/16 :goto_0

    .line 1384
    :cond_4
    const-string v2, "hangouts/gv_voicemail"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1385
    new-instance v2, Lcew;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcew;-><init>(Landroid/content/Context;)V

    .line 1386
    iget-object v8, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->t:Ljava/lang/String;

    .line 1387
    invoke-virtual/range {p7 .. p7}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->r()Laha;

    move-result-object v9

    move-object/from16 v3, p6

    move-object/from16 v4, p11

    move/from16 v5, p19

    move-object/from16 v6, p20

    move-object/from16 v7, p9

    .line 1386
    invoke-virtual/range {v2 .. v9}, Lcew;->a(Lyj;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcas;)V

    .line 1388
    invoke-direct {p0, v2}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->a(Lcde;)V

    goto/16 :goto_0

    .line 1389
    :cond_5
    const-string v2, "hangouts/*"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1390
    new-instance v2, Lcck;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcck;-><init>(Landroid/content/Context;)V

    move-object/from16 v3, p6

    move/from16 v4, p5

    move-object v5, p1

    move v6, p3

    move/from16 v7, p4

    move-object/from16 v8, p17

    move-object/from16 v9, p7

    move-object/from16 v10, p12

    .line 1391
    invoke-virtual/range {v2 .. v10}, Lcck;->a(Lyj;ZLjava/lang/String;IILjava/lang/String;Lt;Ljava/lang/String;)V

    .line 1393
    invoke-direct {p0, v2}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->a(Lcde;)V

    goto/16 :goto_0

    .line 1394
    :cond_6
    invoke-static {p2}, Lf;->d(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1395
    new-instance v2, Lcep;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcep;-><init>(Landroid/content/Context;)V

    .line 1396
    move-object/from16 v0, p6

    move-object/from16 v1, p7

    invoke-virtual {v2, v0, p1, v1}, Lcep;->a(Lyj;Ljava/lang/String;Lt;)V

    .line 1397
    invoke-direct {p0, v2}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->a(Lcde;)V

    goto/16 :goto_0

    .line 1399
    :cond_7
    sget-object v2, Lcom/google/android/apps/hangouts/views/MessageListItemView;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "We do not recognize the contentType "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for image url "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " and are not handling the attachment"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private a(Lyj;Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Z)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 653
    new-instance v0, Landroid/text/SpannableString;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->d()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 654
    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v2

    const-class v3, Landroid/text/style/URLSpan;

    invoke-virtual {v0, v1, v2, v3}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    .line 655
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->z:Z

    .line 656
    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 657
    invoke-virtual {v3}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v3

    .line 658
    invoke-static {v3}, Lcfc;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 659
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->z:Z

    .line 660
    new-instance v4, Lcfc;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Lcfc;-><init>(Landroid/content/Context;)V

    .line 661
    invoke-virtual {v4, p1, p3, v3, p2}, Lcfc;->a(Lyj;ZLjava/lang/String;Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    .line 662
    invoke-direct {p0, v4}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->a(Lcde;)V

    .line 656
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 665
    :cond_1
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/views/MessageListItemView;)Lcom/google/android/apps/hangouts/views/AvatarView;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->q:Lcom/google/android/apps/hangouts/views/AvatarView;

    return-object v0
.end method

.method public static synthetic b(Ljava/lang/String;)Ljava/lang/Long;
    .locals 1

    .prologue
    .line 79
    invoke-static {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->e(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic c(Ljava/lang/String;)Ljava/lang/Long;
    .locals 1

    .prologue
    .line 79
    invoke-static {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->d(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/apps/hangouts/views/MessageListItemView;)Z
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->ai:Z

    return v0
.end method

.method public static synthetic d(Lcom/google/android/apps/hangouts/views/MessageListItemView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->n:Landroid/view/View;

    return-object v0
.end method

.method private static d(Ljava/lang/String;)Ljava/lang/Long;
    .locals 4

    .prologue
    .line 428
    invoke-static {}, Lbsc;->a()Lbsc;

    move-result-object v1

    .line 431
    :try_start_0
    invoke-virtual {v1, p0}, Lbsc;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 433
    if-eqz v0, :cond_1

    .line 434
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcvw;

    .line 435
    invoke-interface {v0}, Lcvw;->b()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 441
    invoke-virtual {v1}, Lbsc;->b()V

    .line 444
    :goto_0
    return-object v0

    .line 441
    :cond_1
    invoke-virtual {v1}, Lbsc;->b()V

    .line 444
    const/4 v0, 0x0

    goto :goto_0

    .line 441
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lbsc;->b()V

    throw v0
.end method

.method public static synthetic e(Lcom/google/android/apps/hangouts/views/MessageListItemView;)Lcom/google/android/apps/hangouts/views/ScalingTextView;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->j:Lcom/google/android/apps/hangouts/views/ScalingTextView;

    return-object v0
.end method

.method private static e(Ljava/lang/String;)Ljava/lang/Long;
    .locals 3

    .prologue
    .line 453
    invoke-static {}, Lbsc;->a()Lbsc;

    move-result-object v1

    .line 456
    :try_start_0
    invoke-virtual {v1, p0}, Lbsc;->a(Ljava/lang/String;)Laea;

    move-result-object v0

    .line 457
    if-eqz v0, :cond_0

    .line 458
    invoke-virtual {v0}, Laea;->d()Ljava/util/ArrayList;

    move-result-object v0

    .line 459
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 460
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 464
    invoke-virtual {v1}, Lbsc;->b()V

    .line 467
    :goto_0
    return-object v0

    .line 464
    :cond_0
    invoke-virtual {v1}, Lbsc;->b()V

    .line 467
    const/4 v0, 0x0

    goto :goto_0

    .line 464
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lbsc;->b()V

    throw v0
.end method

.method public static synthetic f(Lcom/google/android/apps/hangouts/views/MessageListItemView;)Z
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->O:Z

    return v0
.end method

.method private static x()Z
    .locals 2

    .prologue
    .line 332
    invoke-static {}, Lf;->y()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private y()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 571
    const-string v0, "babel_force_gb_copy_paste_textview"

    invoke-static {v0, v3}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 574
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    if-nez v0, :cond_0

    .line 575
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextIsSelectable(Z)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->F:Lcom/google/android/apps/hangouts/views/MessageBubbleView;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->F:Lcom/google/android/apps/hangouts/views/MessageBubbleView;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->setLongClickable(Z)V

    .line 581
    :goto_0
    return-void

    .line 577
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setLongClickable(Z)V

    .line 578
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->F:Lcom/google/android/apps/hangouts/views/MessageBubbleView;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->setClickable(Z)V

    .line 579
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->F:Lcom/google/android/apps/hangouts/views/MessageBubbleView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->setLongClickable(Z)V

    goto :goto_0
.end method

.method private z()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 623
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->K:Ljava/lang/String;

    .line 626
    :goto_0
    const-string v3, "\u00a0"

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 627
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 631
    :cond_0
    iget v3, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->R:I

    if-eqz v3, :cond_1

    iget v3, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->R:I

    if-ne v3, v2, :cond_2

    :cond_1
    move v1, v2

    .line 633
    :cond_2
    if-eqz v1, :cond_4

    .line 635
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->V:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 638
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->V:Ljava/lang/String;

    invoke-static {v1, v0}, Lbvx;->a(Ljava/lang/String;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 646
    :cond_3
    :goto_1
    invoke-static {}, Lccc;->a()Lccc;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->i:Landroid/widget/TextView;

    invoke-virtual {v1, v0, v2}, Lccc;->a(Ljava/lang/CharSequence;Landroid/widget/TextView;)Landroid/text/SpannableString;

    move-result-object v1

    if-nez v1, :cond_5

    .line 647
    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->i:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 648
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->i:Landroid/widget/TextView;

    const/16 v1, 0xf

    invoke-static {v0, v1}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;I)Z

    .line 649
    return-void

    .line 644
    :cond_4
    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    goto :goto_1

    :cond_5
    move-object v0, v1

    goto :goto_2
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 1521
    iget-wide v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->C:J

    return-wide v0
.end method

.method public a(F)V
    .locals 2

    .prologue
    .line 1530
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->D:Z

    if-eqz v0, :cond_0

    .line 1531
    iput p1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->P:F

    .line 1534
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->getMeasuredWidth()I

    move-result v0

    sget v1, Lcom/google/android/apps/hangouts/views/MessageListItemView;->c:I

    add-int/2addr v0, v1

    .line 1535
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->F:Lcom/google/android/apps/hangouts/views/MessageBubbleView;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->a(FI)V

    .line 1536
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->requestLayout()V

    .line 1538
    :cond_0
    return-void
.end method

.method public a(Landroid/database/Cursor;ILyj;Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Z)V
    .locals 30

    .prologue
    .line 1115
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/hangouts/views/MessageListItemView;->ae:Lyj;

    .line 1116
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->s:I

    .line 1117
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->ah:Ljava/util/ArrayList;

    .line 1118
    const/16 v4, 0x12

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_f

    const/4 v4, 0x1

    .line 1119
    :goto_0
    const/16 v5, 0x8

    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_10

    const/4 v5, 0x1

    .line 1124
    :goto_1
    const/4 v6, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1125
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->t:Ljava/lang/String;

    invoke-static {v6, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1127
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->i()V

    .line 1128
    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->t:Ljava/lang/String;

    .line 1131
    :cond_0
    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->u:J

    .line 1132
    new-instance v6, Lbdk;

    const/4 v7, 0x4

    .line 1133
    move-object/from16 v0, p1

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x3

    .line 1134
    move-object/from16 v0, p1

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lbdk;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->r:Lbdk;

    .line 1136
    const/4 v6, -0x1

    move-object/from16 v0, p0

    iput v6, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->aa:I

    .line 1137
    const/16 v6, 0x17

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->ab:Ljava/lang/String;

    .line 1138
    const/16 v6, 0x1f

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    move-object/from16 v0, p0

    iput v6, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->R:I

    .line 1139
    const/4 v7, 0x0

    .line 1140
    const/4 v8, 0x0

    .line 1141
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->R:I

    if-eqz v6, :cond_1

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->R:I

    const/4 v9, 0x1

    if-ne v6, v9, :cond_11

    :cond_1
    const/4 v12, 0x1

    .line 1143
    :goto_2
    if-eqz v12, :cond_15

    .line 1144
    const/16 v6, 0x15

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    move-object/from16 v0, p0

    iput v6, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->S:I

    .line 1145
    const/16 v6, 0x14

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    move-object/from16 v0, p0

    iput-wide v9, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->T:J

    .line 1146
    const/16 v6, 0x16

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    const-wide/16 v13, 0x3e8

    div-long/2addr v9, v13

    move-object/from16 v0, p0

    iput-wide v9, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->U:J

    .line 1147
    const/16 v6, 0x19

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->W:Ljava/lang/String;

    .line 1148
    const/16 v6, 0x1a

    .line 1149
    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1148
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_12

    const/4 v6, 0x0

    :goto_3
    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->Z:Ljava/lang/String;

    .line 1150
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->R:I

    if-nez v6, :cond_2

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->D:Z

    if-eqz v6, :cond_2

    .line 1151
    const/16 v6, 0x1c

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    move-object/from16 v0, p0

    iput v6, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->aa:I

    .line 1153
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->getContext()Landroid/content/Context;

    move-result-object v6

    const/16 v9, 0x18

    .line 1154
    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 1153
    invoke-static {v6, v9}, Lbvx;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 1155
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->V:Ljava/lang/String;

    invoke-static {v9, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3a

    .line 1156
    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->V:Ljava/lang/String;

    .line 1157
    const/4 v6, 0x1

    .line 1159
    :goto_4
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_39

    .line 1160
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 1161
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget v10, Lh;->lY:I

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    :goto_5
    move-object/from16 v25, v7

    move v7, v6

    .line 1168
    :goto_6
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->Q:I

    const/4 v8, 0x2

    if-ne v6, v8, :cond_3

    .line 1169
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->D:Z

    if-eqz v6, :cond_3

    .line 1170
    const/4 v6, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->ad:Ljava/lang/String;

    .line 1174
    :cond_3
    const/16 v6, 0x22

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 1175
    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->E:Z

    if-ne v8, v4, :cond_4

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->D:Z

    if-ne v8, v5, :cond_4

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->Q:I

    if-eq v8, v6, :cond_5

    .line 1176
    :cond_4
    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->E:Z

    .line 1177
    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->D:Z

    .line 1178
    move-object/from16 v0, p0

    iput v6, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->Q:I

    .line 1179
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->C()V

    .line 1182
    :cond_5
    const/4 v4, 0x5

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1184
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->K:Ljava/lang/String;

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 1185
    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->K:Ljava/lang/String;

    .line 1186
    const/4 v7, 0x1

    .line 1188
    :cond_6
    if-eqz v7, :cond_7

    .line 1189
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->K:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_17

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->V:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_17

    .line 1190
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->i:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1197
    :cond_7
    :goto_7
    const/16 v4, 0x9

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0xa

    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    if-eqz v4, :cond_8

    sget-object v6, Lbvx;->a:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-static {v4}, Lbvx;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->ah:Ljava/util/ArrayList;

    :cond_8
    if-eqz v5, :cond_9

    sget-object v6, Lbvx;->a:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_9

    invoke-static {v5}, Lbvx;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->ah:Ljava/util/ArrayList;

    :cond_9
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->x:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_18

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->x:Ljava/lang/String;

    :goto_8
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->y:Ljava/lang/String;

    const/16 v8, 0x1d

    move-object/from16 v0, p1

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->y:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_a

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_b

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_b

    :cond_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->y:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->y:Ljava/lang/String;

    invoke-static {v4, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1a

    :cond_b
    const/4 v4, 0x1

    move/from16 v27, v4

    .line 1198
    :goto_9
    if-nez v27, :cond_c

    .line 1199
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->B()V

    .line 1201
    :cond_c
    const/16 v4, 0x13

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1202
    if-nez v4, :cond_d

    const-string v4, ""

    :cond_d
    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->ac:Ljava/lang/String;

    .line 1203
    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->K()Lbdh;

    move-result-object v28

    .line 1205
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->x:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_e

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->y:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_23

    .line 1206
    :cond_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->ac:Ljava/lang/String;

    const-string v5, "multipart/mixed"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1c

    .line 1207
    const/4 v4, 0x0

    .line 1208
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->ah:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v29

    move/from16 v26, v4

    :goto_a
    invoke-interface/range {v29 .. v29}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1d

    invoke-interface/range {v29 .. v29}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lbvy;

    .line 1209
    if-eqz v27, :cond_1b

    .line 1210
    iget-object v5, v4, Lbvy;->b:Ljava/lang/String;

    iget-object v4, v4, Lbvy;->a:Ljava/lang/String;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-direct {v0, v1, v5, v4, v6}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1230
    :goto_b
    add-int/lit8 v4, v26, 0x1

    move/from16 v26, v4

    .line 1231
    goto :goto_a

    .line 1118
    :cond_f
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 1119
    :cond_10
    const/4 v5, 0x0

    goto/16 :goto_1

    .line 1141
    :cond_11
    const/4 v12, 0x0

    goto/16 :goto_2

    .line 1148
    :cond_12
    const-string v9, ","

    invoke-virtual {v6, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v6, v9

    if-nez v6, :cond_13

    const/4 v6, 0x0

    goto/16 :goto_3

    :cond_13
    new-instance v10, Ljava/util/ArrayList;

    array-length v6, v9

    invoke-direct {v10, v6}, Ljava/util/ArrayList;-><init>(I)V

    array-length v11, v9

    const/4 v6, 0x0

    :goto_c
    if-ge v6, v11, :cond_14

    aget-object v13, v9, v6

    invoke-static {v13}, Lbzd;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-interface {v10, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v6, v6, 0x1

    goto :goto_c

    :cond_14
    const-string v6, ", "

    invoke-static {v6}, Lebe;->a(Ljava/lang/String;)Lebe;

    move-result-object v6

    invoke-virtual {v6, v10}, Lebe;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_3

    .line 1164
    :cond_15
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->R:I

    const/4 v9, 0x2

    if-ne v6, v9, :cond_16

    .line 1165
    const/16 v6, 0x16

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    const-wide/16 v13, 0x3e8

    div-long/2addr v9, v13

    move-object/from16 v0, p0

    iput-wide v9, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->U:J

    :cond_16
    move-object/from16 v25, v8

    goto/16 :goto_6

    .line 1192
    :cond_17
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->z()V

    .line 1193
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    move/from16 v3, p5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->a(Lyj;Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Z)V

    goto/16 :goto_7

    .line 1197
    :cond_18
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_19

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->x:Ljava/lang/String;

    goto/16 :goto_8

    :cond_19
    const-string v7, ""

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->x:Ljava/lang/String;

    goto/16 :goto_8

    :cond_1a
    const/4 v4, 0x0

    move/from16 v27, v4

    goto/16 :goto_9

    .line 1213
    :cond_1b
    iget-object v5, v4, Lbvy;->a:Ljava/lang/String;

    iget-object v6, v4, Lbvy;->b:Ljava/lang/String;

    iget v7, v4, Lbvy;->c:I

    iget v8, v4, Lbvy;->d:I

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const-wide/16 v17, 0x0

    const-wide/16 v19, 0x0

    const/16 v21, 0x0

    const/4 v4, 0x2

    .line 1225
    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x0

    const/16 v24, 0x0

    move-object/from16 v4, p0

    move/from16 v9, p5

    move-object/from16 v10, p3

    move-object/from16 v11, p4

    .line 1213
    invoke-direct/range {v4 .. v24}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->a(Ljava/lang/String;Ljava/lang/String;IIZLyj;Lcom/google/android/apps/hangouts/fragments/ConversationFragment;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;DDLjava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    goto/16 :goto_b

    .line 1233
    :cond_1c
    const/16 v4, 0x1e

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 1234
    if-eqz v27, :cond_21

    .line 1235
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->ac:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->x:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v6, v15}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1262
    :cond_1d
    :goto_d
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->D:Z

    if-nez v4, :cond_1e

    const/4 v4, 0x1

    move/from16 v0, p2

    if-ne v0, v4, :cond_24

    .line 1263
    :cond_1e
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->L:Ljava/lang/String;

    .line 1264
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->M:Ljava/lang/String;

    .line 1270
    :goto_e
    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->W()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_26

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->D:Z

    if-nez v4, :cond_26

    .line 1272
    if-eqz v28, :cond_25

    .line 1273
    move-object/from16 v0, v28

    iget-object v4, v0, Lbdh;->b:Lbdk;

    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->d(Lbdk;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v5, p0

    .line 1278
    :goto_f
    move-object/from16 v0, p3

    invoke-virtual {v5, v4, v0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->a(Ljava/lang/String;Lyj;)V

    .line 1283
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->j()V

    .line 1285
    const/4 v4, 0x7

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->J:I

    .line 1287
    const/4 v4, 0x6

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->C:J

    .line 1288
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->C:J

    .line 1289
    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Lf;->a(JZ)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1291
    const/16 v4, 0x1b

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-nez v4, :cond_28

    const/16 v4, 0x8

    .line 1292
    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_28

    const/4 v4, 0x1

    .line 1295
    :goto_10
    const/16 v5, 0x23

    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 1296
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->L:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->J:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->aa:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->l:Landroid/widget/TextView;

    if-eqz v5, :cond_1f

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->j:Lcom/google/android/apps/hangouts/views/ScalingTextView;

    if-eqz v5, :cond_1f

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->R:I

    const/4 v8, 0x2

    if-ne v7, v8, :cond_29

    const/4 v7, 0x1

    move v8, v7

    :goto_11
    packed-switch v12, :pswitch_data_0

    move v7, v5

    :goto_12
    if-eqz v7, :cond_33

    if-eqz v8, :cond_32

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->A()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->o:Lcom/google/android/apps/hangouts/views/ScalingTextView;

    sget v5, Lh;->cB:I

    invoke-virtual {v4, v5}, Lcom/google/android/apps/hangouts/views/ScalingTextView;->setText(I)V

    .line 1303
    :cond_1f
    :goto_13
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->l:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getVisibility()I

    move-result v4

    if-nez v4, :cond_36

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->l:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    .line 1304
    :goto_14
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->j:Lcom/google/android/apps/hangouts/views/ScalingTextView;

    .line 1306
    invoke-virtual {v5}, Lcom/google/android/apps/hangouts/views/ScalingTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->K:Ljava/lang/String;

    .line 1303
    invoke-static/range {v25 .. v25}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_20

    const-string v25, ""

    :cond_20
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->getContext()Landroid/content/Context;

    move-result-object v7

    sget v8, Lh;->gK:I

    const/4 v9, 0x4

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v25, v9, v10

    const/4 v10, 0x1

    aput-object v6, v9, v10

    const/4 v6, 0x2

    aput-object v4, v9, v6

    const/4 v4, 0x3

    aput-object v5, v9, v4

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1310
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->f()Z

    move-result v4

    if-eqz v4, :cond_37

    .line 1311
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->F:Lcom/google/android/apps/hangouts/views/MessageBubbleView;

    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1312
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->F:Lcom/google/android/apps/hangouts/views/MessageBubbleView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->setLongClickable(Z)V

    .line 1313
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->i:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setLongClickable(Z)V

    .line 1314
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->i:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setClickable(Z)V

    .line 1318
    :goto_15
    return-void

    .line 1237
    :cond_21
    const/16 v4, 0xb

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 1238
    const/16 v4, 0xc

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 1239
    const/16 v4, 0xd

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 1240
    const/16 v4, 0xe

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v17

    .line 1241
    const/16 v4, 0xf

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v19

    .line 1242
    if-eqz v28, :cond_22

    invoke-virtual/range {v28 .. v28}, Lbdh;->c()Ljava/lang/String;

    move-result-object v24

    .line 1243
    :goto_16
    const/16 v4, 0x10

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 1244
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->x:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->ac:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->y:Ljava/lang/String;

    const/4 v4, 0x4

    .line 1249
    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    const/4 v4, 0x2

    .line 1252
    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    const/16 v4, 0x25

    .line 1253
    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v23

    move-object/from16 v4, p0

    move/from16 v9, p5

    move-object/from16 v10, p3

    move-object/from16 v11, p4

    .line 1244
    invoke-direct/range {v4 .. v24}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->a(Ljava/lang/String;Ljava/lang/String;IIZLyj;Lcom/google/android/apps/hangouts/fragments/ConversationFragment;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;DDLjava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    goto/16 :goto_d

    .line 1242
    :cond_22
    const/16 v24, 0x0

    goto :goto_16

    .line 1258
    :cond_23
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->B()V

    goto/16 :goto_d

    .line 1266
    :cond_24
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->r:Lbdk;

    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->f(Lbdk;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->L:Ljava/lang/String;

    .line 1267
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->r:Lbdk;

    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g(Lbdk;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->M:Ljava/lang/String;

    goto/16 :goto_e

    .line 1275
    :cond_25
    const/4 v4, 0x0

    move-object/from16 v5, p0

    goto/16 :goto_f

    .line 1278
    :cond_26
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->D:Z

    if-eqz v4, :cond_27

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->N:Ljava/lang/String;

    move-object/from16 v5, p0

    goto/16 :goto_f

    :cond_27
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->r:Lbdk;

    .line 1280
    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->d(Lbdk;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v5, p0

    goto/16 :goto_f

    .line 1292
    :cond_28
    const/4 v4, 0x0

    goto/16 :goto_10

    .line 1296
    :cond_29
    const/4 v7, 0x0

    move v8, v7

    goto/16 :goto_11

    :pswitch_0
    const/4 v7, 0x1

    :goto_17
    if-eqz v8, :cond_2a

    sget v4, Lh;->hg:I

    move-object/from16 v5, p0

    :goto_18
    iget-object v9, v5, Lcom/google/android/apps/hangouts/views/MessageListItemView;->l:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v9, 0x0

    invoke-direct {v5, v4, v9}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_12

    :cond_2a
    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->Q:I

    const/4 v10, 0x2

    if-ne v5, v10, :cond_2b

    sget-object v4, Lcom/google/android/apps/hangouts/views/MessageListItemView;->e:Ljava/lang/String;

    :goto_19
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    sget v12, Lh;->S:I

    const/4 v5, 0x3

    new-array v14, v5, [Ljava/lang/Object;

    const/4 v15, 0x0

    if-nez v11, :cond_2f

    const-string v5, ""

    :goto_1a
    aput-object v5, v14, v15

    const/4 v5, 0x1

    aput-object v9, v14, v5

    const/4 v5, 0x2

    aput-object v4, v14, v5

    invoke-virtual {v10, v12, v14}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v4}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-nez v13, :cond_30

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->m:Landroid/widget/ImageView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_12

    :cond_2b
    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->R:I

    if-nez v5, :cond_2c

    sget-object v4, Lcom/google/android/apps/hangouts/views/MessageListItemView;->f:Ljava/lang/String;

    goto :goto_19

    :cond_2c
    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->R:I

    const/4 v10, 0x1

    if-ne v5, v10, :cond_2d

    sget-object v4, Lcom/google/android/apps/hangouts/views/MessageListItemView;->g:Ljava/lang/String;

    goto :goto_19

    :cond_2d
    if-eqz v4, :cond_2e

    sget-object v4, Lcom/google/android/apps/hangouts/views/MessageListItemView;->h:Ljava/lang/String;

    goto :goto_19

    :cond_2e
    const-string v4, ""

    goto :goto_19

    :cond_2f
    const-string v5, " \u00b7 "

    goto :goto_1a

    :cond_30
    const/16 v4, 0x40

    if-ne v13, v4, :cond_38

    const/4 v4, 0x1

    :goto_1b
    move v6, v4

    goto/16 :goto_12

    :pswitch_1
    packed-switch v10, :pswitch_data_1

    if-eqz v8, :cond_31

    sget v4, Lh;->ir:I

    move v7, v5

    move-object/from16 v5, p0

    goto :goto_18

    :pswitch_2
    sget v4, Lh;->ha:I

    move v7, v5

    move-object/from16 v5, p0

    goto :goto_18

    :pswitch_3
    sget v4, Lh;->he:I

    move v7, v5

    move-object/from16 v5, p0

    goto :goto_18

    :pswitch_4
    sget v4, Lh;->gU:I

    move v7, v5

    move-object/from16 v5, p0

    goto/16 :goto_18

    :pswitch_5
    sget v4, Lh;->hb:I

    move v7, v5

    move-object/from16 v5, p0

    goto/16 :goto_18

    :pswitch_6
    sget v4, Lh;->gY:I

    move v7, v5

    move-object/from16 v5, p0

    goto/16 :goto_18

    :pswitch_7
    sget v4, Lh;->gR:I

    move v7, v5

    move-object/from16 v5, p0

    goto/16 :goto_18

    :pswitch_8
    sget v4, Lh;->gS:I

    move v7, v5

    move-object/from16 v5, p0

    goto/16 :goto_18

    :pswitch_9
    sget v4, Lh;->gT:I

    move v7, v5

    move-object/from16 v5, p0

    goto/16 :goto_18

    :pswitch_a
    sget v4, Lh;->gW:I

    move v7, v5

    move-object/from16 v5, p0

    goto/16 :goto_18

    :pswitch_b
    sget v4, Lh;->gX:I

    move v7, v5

    move-object/from16 v5, p0

    goto/16 :goto_18

    :pswitch_c
    sget v4, Lh;->gZ:I

    move v7, v5

    move-object/from16 v5, p0

    goto/16 :goto_18

    :pswitch_d
    sget v4, Lh;->hc:I

    move v7, v5

    move-object/from16 v5, p0

    goto/16 :goto_18

    :pswitch_e
    sget v4, Lh;->hd:I

    move v7, v5

    move-object/from16 v5, p0

    goto/16 :goto_18

    :pswitch_f
    sget v4, Lh;->gV:I

    move v7, v5

    move-object/from16 v5, p0

    goto/16 :goto_18

    :cond_31
    sget v4, Lh;->is:I

    move v7, v5

    move-object/from16 v5, p0

    goto/16 :goto_18

    :cond_32
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->A()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->o:Lcom/google/android/apps/hangouts/views/ScalingTextView;

    sget v5, Lh;->cC:I

    invoke-virtual {v4, v5}, Lcom/google/android/apps/hangouts/views/ScalingTextView;->setText(I)V

    goto/16 :goto_13

    :cond_33
    if-eqz v6, :cond_34

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->k:Landroid/view/View;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->n:Landroid/view/View;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->p:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_13

    :cond_34
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->I:Z

    if-eqz v4, :cond_35

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->I:Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->p:Landroid/view/View;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->k:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->k:Landroid/view/View;

    sget-object v5, Lcom/google/android/apps/hangouts/views/MessageListItemView;->ag:Landroid/view/animation/Animation;

    invoke-virtual {v4, v5}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->H:I

    goto/16 :goto_13

    :cond_35
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->p:Landroid/view/View;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->k:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_13

    .line 1303
    :cond_36
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->r:Lbdk;

    .line 1304
    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->f(Lbdk;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_14

    .line 1316
    :cond_37
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->y()V

    goto/16 :goto_15

    :cond_38
    move v4, v6

    goto/16 :goto_1b

    :pswitch_10
    move v7, v5

    goto/16 :goto_17

    :cond_39
    move-object v7, v8

    goto/16 :goto_5

    :cond_3a
    move v6, v7

    goto/16 :goto_4

    .line 1296
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_10
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x7b
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_b
        :pswitch_9
        :pswitch_c
        :pswitch_e
        :pswitch_7
        :pswitch_a
        :pswitch_8
        :pswitch_d
        :pswitch_f
    .end packed-switch
.end method

.method public a(Lccu;)V
    .locals 0

    .prologue
    .line 279
    iput-object p1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->v:Lccu;

    .line 280
    return-void
.end method

.method public a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V
    .locals 0

    .prologue
    .line 283
    iput-object p1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->w:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    .line 284
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1631
    iput-object p1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->N:Ljava/lang/String;

    .line 1632
    return-void
.end method

.method public a(Ljava/lang/String;Lyj;)V
    .locals 1

    .prologue
    .line 965
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->q:Lcom/google/android/apps/hangouts/views/AvatarView;

    if-eqz v0, :cond_0

    .line 966
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->q:Lcom/google/android/apps/hangouts/views/AvatarView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/hangouts/views/AvatarView;->a(Ljava/lang/String;Lyj;)V

    .line 968
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v3, 0x0

    .line 836
    iget v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->H:I

    if-eqz v0, :cond_1

    .line 837
    if-eqz p1, :cond_0

    .line 838
    iput v2, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->H:I

    .line 840
    :cond_0
    iget v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->H:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    iput v3, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->H:I

    .line 842
    :cond_1
    return-void

    .line 840
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->n:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->n:Landroid/view/View;

    sget-object v1, Lcom/google/android/apps/hangouts/views/MessageListItemView;->af:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    const-string v0, "sendingStatusDelayedShrinkPercentage"

    new-array v1, v2, [F

    fill-array-data v1, :array_0

    invoke-static {p0, v0, v1}, Lwr;->a(Ljava/lang/Object;Ljava/lang/String;[F)Lwr;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lf;->dQ:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lwr;->c(J)Lwr;

    const-wide/16 v1, 0x15e

    invoke-virtual {v0, v1, v2}, Lwr;->a(J)V

    new-instance v1, Lwp;

    invoke-direct {v1, v3}, Lwp;-><init>(B)V

    invoke-virtual {v0, v1}, Lwr;->a(Lwt;)V

    new-instance v1, Lcda;

    invoke-direct {v1, p0}, Lcda;-><init>(Lcom/google/android/apps/hangouts/views/MessageListItemView;)V

    invoke-virtual {v0, v1}, Lwr;->a(Lwl;)V

    invoke-virtual {v0}, Lwr;->b()V

    goto :goto_0

    :pswitch_1
    const-string v0, "sendingStatusShrinkPercentage"

    new-array v1, v2, [F

    fill-array-data v1, :array_1

    invoke-static {p0, v0, v1}, Lwr;->a(Ljava/lang/Object;Ljava/lang/String;[F)Lwr;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lf;->dQ:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lwr;->c(J)Lwr;

    new-instance v1, Lwp;

    invoke-direct {v1, v3}, Lwp;-><init>(B)V

    invoke-virtual {v0, v1}, Lwr;->a(Lwt;)V

    new-instance v1, Lcdb;

    invoke-direct {v1, p0}, Lcdb;-><init>(Lcom/google/android/apps/hangouts/views/MessageListItemView;)V

    invoke-virtual {v0, v1}, Lwr;->a(Lwl;)V

    invoke-virtual {v0}, Lwr;->b()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method public b()Landroid/view/View;
    .locals 0

    .prologue
    .line 1526
    return-object p0
.end method

.method public b(Z)V
    .locals 5

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 1546
    iget-boolean v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->O:Z

    if-eqz v1, :cond_0

    .line 1568
    :goto_0
    return-void

    .line 1550
    :cond_0
    iget v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->aa:I

    if-nez v1, :cond_2

    move p1, v0

    .line 1560
    :cond_1
    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->j:Lcom/google/android/apps/hangouts/views/ScalingTextView;

    if-eqz p1, :cond_3

    iget-boolean v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->I:Z

    if-nez v1, :cond_3

    move v1, v2

    :goto_2
    invoke-virtual {v4, v1}, Lcom/google/android/apps/hangouts/views/ScalingTextView;->a(F)V

    .line 1561
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->j:Lcom/google/android/apps/hangouts/views/ScalingTextView;

    if-eqz p1, :cond_4

    :goto_3
    invoke-virtual {v1, v2}, Lcom/google/android/apps/hangouts/views/ScalingTextView;->b(F)V

    .line 1562
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->j:Lcom/google/android/apps/hangouts/views/ScalingTextView;

    if-eqz p1, :cond_5

    const/4 v1, 0x4

    :goto_4
    invoke-virtual {v2, v1}, Lcom/google/android/apps/hangouts/views/ScalingTextView;->setVisibility(I)V

    .line 1563
    if-nez p1, :cond_6

    iget v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->aa:I

    if-nez v1, :cond_6

    .line 1564
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->m:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 1553
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->I:Z

    if-eqz v1, :cond_1

    .line 1555
    const/4 p1, 0x1

    goto :goto_1

    :cond_3
    move v1, v3

    .line 1560
    goto :goto_2

    :cond_4
    move v2, v3

    .line 1561
    goto :goto_3

    :cond_5
    move v1, v0

    .line 1562
    goto :goto_4

    .line 1566
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->m:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public c()Lcom/google/android/apps/hangouts/views/MessageBubbleView;
    .locals 1

    .prologue
    .line 488
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->F:Lcom/google/android/apps/hangouts/views/MessageBubbleView;

    return-object v0
.end method

.method public d()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 525
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->i:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 532
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->K:Ljava/lang/String;

    return-object v0
.end method

.method public f()Z
    .locals 3

    .prologue
    .line 618
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->D:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->J:I

    iget-wide v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->C:J

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->c(I)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->R:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()V
    .locals 2

    .prologue
    .line 974
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->B:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 975
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->B:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcde;

    .line 976
    invoke-interface {v0}, Lcde;->e()V

    goto :goto_0

    .line 979
    :cond_0
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    .line 985
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->B:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 986
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->B:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcde;

    .line 987
    invoke-interface {v0}, Lcde;->f()V

    goto :goto_0

    .line 990
    :cond_0
    return-void
.end method

.method public i()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 994
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->i:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 995
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->i:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 996
    if-eqz v0, :cond_1

    instance-of v1, v0, Landroid/text/SpannableString;

    if-nez v1, :cond_0

    instance-of v1, v0, Landroid/text/SpannedString;

    if-eqz v1, :cond_1

    .line 998
    :cond_0
    invoke-static {}, Lccc;->a()Lccc;

    move-result-object v1

    check-cast v0, Landroid/text/Spanned;

    invoke-virtual {v1, v0}, Lccc;->a(Landroid/text/Spanned;)V

    .line 1001
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1011
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1012
    iput-object v2, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->K:Ljava/lang/String;

    .line 1013
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1016
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->l:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->a(Landroid/widget/TextView;)V

    .line 1017
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->j:Lcom/google/android/apps/hangouts/views/ScalingTextView;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->a(Landroid/widget/TextView;)V

    .line 1018
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->n:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 1019
    iput v3, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->H:I

    .line 1021
    iput-object v2, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->V:Ljava/lang/String;

    .line 1022
    iput-wide v4, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->T:J

    .line 1023
    iput v3, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->S:I

    .line 1024
    iput-object v2, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->r:Lbdk;

    .line 1025
    iput-object v2, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->L:Ljava/lang/String;

    .line 1026
    iput-object v2, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->M:Ljava/lang/String;

    .line 1027
    iput-boolean v3, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->O:Z

    .line 1028
    invoke-virtual {p0, v3}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->b(Z)V

    .line 1029
    iput-boolean v3, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->z:Z

    .line 1030
    iput-object v2, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->W:Ljava/lang/String;

    .line 1031
    iput-wide v4, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->U:J

    .line 1032
    iput-object v2, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->Z:Ljava/lang/String;

    .line 1033
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->aa:I

    .line 1034
    iput-object v2, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->r:Lbdk;

    .line 1040
    iput-object v2, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->x:Ljava/lang/String;

    .line 1041
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->B()V

    .line 1042
    return-void
.end method

.method public j()V
    .locals 1

    .prologue
    .line 1071
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->P:F

    .line 1072
    return-void
.end method

.method public k()Lbvy;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1414
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->ah:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 1420
    :cond_0
    :goto_0
    return-object v0

    .line 1417
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->ac:Ljava/lang/String;

    invoke-static {v1}, Lf;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->ac:Ljava/lang/String;

    invoke-static {v1}, Lf;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1420
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->D()Lbvy;

    move-result-object v0

    goto :goto_0
.end method

.method public l()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbvy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1431
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->ah:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 1432
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->ah:Ljava/util/ArrayList;

    .line 1436
    :goto_0
    return-object v0

    .line 1434
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1435
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->D()Lbvy;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 1441
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->ac:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->ac:Ljava/lang/String;

    invoke-static {v0}, Lf;->e(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()I
    .locals 1

    .prologue
    .line 1445
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->ah:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 1446
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->ah:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1451
    :goto_0
    return v0

    .line 1448
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->x:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1449
    const/4 v0, 0x1

    goto :goto_0

    .line 1451
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()I
    .locals 1

    .prologue
    .line 1455
    iget v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->R:I

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 340
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->v:Lccu;

    if-nez v0, :cond_1

    .line 352
    :cond_0
    :goto_0
    return-void

    .line 343
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 344
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->v:Lccu;

    iget v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->s:I

    invoke-interface {v0, v1}, Lccu;->b(I)V

    goto :goto_0

    .line 347
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->q:Lcom/google/android/apps/hangouts/views/AvatarView;

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->D:Z

    if-nez v0, :cond_0

    .line 348
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->r:Lbdk;

    if-nez v0, :cond_3

    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->W:Ljava/lang/String;

    new-instance v2, Lccy;

    invoke-direct {v2, p0, v0, v1}, Lccy;-><init>(Lcom/google/android/apps/hangouts/views/MessageListItemView;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v2, v0}, Lccy;->executeOnThreadPool([Ljava/lang/Object;)Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->r:Lbdk;

    iget-object v0, v0, Lbdk;->a:Ljava/lang/String;

    goto :goto_1
.end method

.method public onFinishInflate()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 540
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->G:Landroid/widget/FrameLayout;

    sget v1, Lg;->H:I

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/AvatarView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->q:Lcom/google/android/apps/hangouts/views/AvatarView;

    .line 541
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->q:Lcom/google/android/apps/hangouts/views/AvatarView;

    if-eqz v0, :cond_0

    .line 542
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->q:Lcom/google/android/apps/hangouts/views/AvatarView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/views/AvatarView;->setClickable(Z)V

    .line 543
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->q:Lcom/google/android/apps/hangouts/views/AvatarView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/hangouts/views/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 544
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->q:Lcom/google/android/apps/hangouts/views/AvatarView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/views/AvatarView;->setLongClickable(Z)V

    .line 545
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->q:Lcom/google/android/apps/hangouts/views/AvatarView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/hangouts/views/AvatarView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 547
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->F:Lcom/google/android/apps/hangouts/views/MessageBubbleView;

    sget v1, Lg;->eB:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->i:Landroid/widget/TextView;

    .line 548
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->F:Lcom/google/android/apps/hangouts/views/MessageBubbleView;

    sget v1, Lg;->hv:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/ScalingTextView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->j:Lcom/google/android/apps/hangouts/views/ScalingTextView;

    .line 549
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->F:Lcom/google/android/apps/hangouts/views/MessageBubbleView;

    sget v1, Lg;->eH:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->l:Landroid/widget/TextView;

    .line 550
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->F:Lcom/google/android/apps/hangouts/views/MessageBubbleView;

    sget v1, Lg;->gR:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->m:Landroid/widget/ImageView;

    .line 551
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->F:Lcom/google/android/apps/hangouts/views/MessageBubbleView;

    sget v1, Lg;->aa:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->k:Landroid/view/View;

    .line 552
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->F:Lcom/google/android/apps/hangouts/views/MessageBubbleView;

    sget v1, Lg;->gS:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->p:Landroid/view/View;

    .line 553
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->i:Landroid/widget/TextView;

    new-instance v1, Lccz;

    invoke-direct {v1, p0}, Lccz;-><init>(Lcom/google/android/apps/hangouts/views/MessageListItemView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 561
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->C()V

    .line 563
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->y()V

    .line 565
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->F:Lcom/google/android/apps/hangouts/views/MessageBubbleView;

    sget v1, Lg;->gC:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->n:Landroid/view/View;

    .line 566
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->F:Lcom/google/android/apps/hangouts/views/MessageBubbleView;

    sget v1, Lg;->gD:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/ScalingTextView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->o:Lcom/google/android/apps/hangouts/views/ScalingTextView;

    .line 567
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->F:Lcom/google/android/apps/hangouts/views/MessageBubbleView;

    sget v1, Lg;->r:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->A:Landroid/view/ViewGroup;

    .line 568
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 607
    iget-boolean v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->ai:Z

    if-eqz v1, :cond_0

    .line 608
    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->ai:Z

    .line 609
    const/4 v0, 0x1

    .line 611
    :cond_0
    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 7

    .prologue
    .line 306
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->D:Z

    invoke-static {}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->x()Z

    move-result v1

    xor-int v2, v0, v1

    .line 308
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->G:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v3

    .line 309
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->G:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v4

    .line 310
    if-eqz v2, :cond_1

    sub-int v0, p4, p2

    .line 311
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-float v1, v3

    iget v5, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->P:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    sub-int/2addr v0, v1

    move v1, v0

    .line 313
    :goto_0
    if-eqz v2, :cond_2

    sub-int v0, p5, p3

    .line 314
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->getPaddingBottom()I

    move-result v5

    sub-int/2addr v0, v5

    sub-int/2addr v0, v4

    .line 316
    :goto_1
    iget-object v5, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->G:Landroid/widget/FrameLayout;

    add-int v6, v1, v3

    add-int/2addr v4, v0

    invoke-virtual {v5, v1, v0, v6, v4}, Landroid/widget/FrameLayout;->layout(IIII)V

    .line 319
    iget-object v4, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->F:Lcom/google/android/apps/hangouts/views/MessageBubbleView;

    invoke-virtual {v4}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->getMeasuredWidth()I

    move-result v4

    .line 320
    iget-object v5, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->F:Lcom/google/android/apps/hangouts/views/MessageBubbleView;

    invoke-virtual {v5}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->getMeasuredHeight()I

    move-result v5

    .line 321
    if-eqz v2, :cond_3

    sub-int/2addr v1, v4

    sget v3, Lcom/google/android/apps/hangouts/views/MessageListItemView;->c:I

    add-int/2addr v1, v3

    add-int/lit8 v1, v1, -0x10

    .line 324
    :goto_2
    if-eqz v2, :cond_0

    .line 325
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->getPaddingTop()I

    move-result v0

    add-int/2addr v0, p3

    .line 327
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->F:Lcom/google/android/apps/hangouts/views/MessageBubbleView;

    add-int v3, v1, v4

    add-int v4, v0, v5

    invoke-virtual {v2, v1, v0, v3, v4}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->layout(IIII)V

    .line 329
    return-void

    .line 312
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->getPaddingLeft()I

    move-result v0

    move v1, v0

    goto :goto_0

    .line 315
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->getPaddingTop()I

    move-result v0

    goto :goto_1

    .line 321
    :cond_3
    add-int/2addr v1, v3

    sget v3, Lcom/google/android/apps/hangouts/views/MessageListItemView;->c:I

    sub-int/2addr v1, v3

    add-int/lit8 v1, v1, 0x10

    goto :goto_2
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 4

    .prologue
    .line 1928
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->q:Lcom/google/android/apps/hangouts/views/AvatarView;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->M:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1929
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->v:Lccu;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->M:Ljava/lang/String;

    const-string v2, "\\s+"

    const-string v3, "_"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lccu;->c(Ljava/lang/String;)V

    .line 1930
    const/4 v0, 0x1

    .line 1932
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 288
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 290
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 291
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->G:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v1, v1}, Landroid/widget/FrameLayout;->measure(II)V

    .line 292
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->G:Landroid/widget/FrameLayout;

    .line 293
    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v2

    sub-int v2, v0, v2

    add-int/lit8 v2, v2, -0x10

    .line 295
    sget v3, Lcom/google/android/apps/hangouts/views/MessageListItemView;->c:I

    add-int/2addr v2, v3

    const/high16 v3, -0x80000000

    .line 296
    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 298
    iget-object v3, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->F:Lcom/google/android/apps/hangouts/views/MessageBubbleView;

    invoke-virtual {v3, v2, v1}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->measure(II)V

    .line 300
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->G:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->F:Lcom/google/android/apps/hangouts/views/MessageBubbleView;

    invoke-virtual {v2}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->getMeasuredHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 301
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->getPaddingTop()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->setMeasuredDimension(II)V

    .line 302
    return-void
.end method

.method public p()I
    .locals 1

    .prologue
    .line 1459
    iget v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->Q:I

    return v0
.end method

.method public q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1464
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->ab:Ljava/lang/String;

    return-object v0
.end method

.method public r()I
    .locals 1

    .prologue
    .line 1469
    iget v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->s:I

    return v0
.end method

.method public s()V
    .locals 2

    .prologue
    .line 1511
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->B:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1512
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->B:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcde;

    .line 1513
    invoke-interface {v0}, Lcde;->g()V

    goto :goto_0

    .line 1516
    :cond_0
    return-void
.end method

.method public setSelected(Z)V
    .locals 1

    .prologue
    .line 1627
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->F:Lcom/google/android/apps/hangouts/views/MessageBubbleView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/hangouts/views/MessageBubbleView;->setSelected(Z)V

    .line 1628
    return-void
.end method

.method public setSendingStatusDelayedShrinkPercentage(F)V
    .locals 1

    .prologue
    .line 895
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->o:Lcom/google/android/apps/hangouts/views/ScalingTextView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/hangouts/views/ScalingTextView;->a(F)V

    .line 896
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->o:Lcom/google/android/apps/hangouts/views/ScalingTextView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/hangouts/views/ScalingTextView;->b(F)V

    .line 897
    return-void
.end method

.method public setSendingStatusShrinkPercentage(F)V
    .locals 1

    .prologue
    .line 935
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->j:Lcom/google/android/apps/hangouts/views/ScalingTextView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/hangouts/views/ScalingTextView;->a(F)V

    .line 936
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->o:Lcom/google/android/apps/hangouts/views/ScalingTextView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/hangouts/views/ScalingTextView;->a(F)V

    .line 937
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->o:Lcom/google/android/apps/hangouts/views/ScalingTextView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/hangouts/views/ScalingTextView;->b(F)V

    .line 938
    return-void
.end method

.method public setTimeHidePercentage(F)V
    .locals 1

    .prologue
    .line 1621
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->j:Lcom/google/android/apps/hangouts/views/ScalingTextView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/hangouts/views/ScalingTextView;->a(F)V

    .line 1622
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->j:Lcom/google/android/apps/hangouts/views/ScalingTextView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/hangouts/views/ScalingTextView;->b(F)V

    .line 1623
    return-void
.end method

.method public t()V
    .locals 2

    .prologue
    .line 1574
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->j:Lcom/google/android/apps/hangouts/views/ScalingTextView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/ScalingTextView;->getVisibility()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->O:Z

    if-eqz v0, :cond_1

    .line 1618
    :cond_0
    :goto_0
    return-void

    .line 1578
    :cond_1
    iget v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->aa:I

    if-eqz v0, :cond_0

    .line 1583
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->O:Z

    .line 1584
    new-instance v0, Lcdc;

    invoke-direct {v0, p0}, Lcdc;-><init>(Lcom/google/android/apps/hangouts/views/MessageListItemView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public u()Ljava/lang/String;
    .locals 11

    .prologue
    const-wide/16 v9, 0x0

    const/16 v8, 0xa

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1645
    iget v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->R:I

    packed-switch v0, :pswitch_data_0

    .line 1656
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1647
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget v4, Lh;->gM:I

    new-array v5, v1, [Ljava/lang/Object;

    sget v6, Lh;->nh:I

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v0, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v4, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->D:Z

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->Z:Ljava/lang/String;

    if-eqz v4, :cond_0

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    sget v4, Lh;->nj:I

    new-array v5, v1, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->Z:Ljava/lang/String;

    aput-object v6, v5, v2

    invoke-virtual {v0, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    :goto_1
    iget-boolean v4, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->D:Z

    if-nez v4, :cond_1

    iget-wide v4, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->U:J

    cmp-long v4, v4, v9

    if-eqz v4, :cond_1

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    sget v4, Lh;->kW:I

    new-array v5, v1, [Ljava/lang/Object;

    iget-wide v6, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->U:J

    invoke-static {v6, v7}, Lf;->a(J)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v0, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-wide v4, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->C:J

    invoke-static {v4, v5}, Lf;->a(J)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    iget-boolean v5, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->D:Z

    if-eqz v5, :cond_3

    sget v5, Lh;->kW:I

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v4, v1, v2

    invoke-virtual {v0, v5, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->W:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    sget v4, Lh;->cW:I

    new-array v5, v1, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->W:Ljava/lang/String;

    invoke-static {v6}, Lbzd;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v0, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_3
    sget v5, Lh;->kz:I

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v4, v1, v2

    invoke-virtual {v0, v5, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 1649
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget v0, Lh;->gM:I

    new-array v5, v1, [Ljava/lang/Object;

    sget v6, Lh;->hk:I

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v3, v0, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->W:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->W:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    sget v0, Lh;->fB:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_3
    sget v5, Lh;->cW:I

    new-array v6, v1, [Ljava/lang/Object;

    aput-object v0, v6, v2

    invoke-virtual {v3, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->Z:Ljava/lang/String;

    if-eqz v0, :cond_5

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    sget v0, Lh;->nj:I

    new-array v5, v1, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->Z:Ljava/lang/String;

    aput-object v6, v5, v2

    invoke-virtual {v3, v0, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-wide v5, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->C:J

    invoke-static {v5, v6}, Lf;->a(J)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-boolean v5, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->D:Z

    if-nez v5, :cond_9

    sget v5, Lh;->kz:I

    new-array v6, v1, [Ljava/lang/Object;

    aput-object v0, v6, v2

    invoke-virtual {v3, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->V:Ljava/lang/String;

    invoke-static {v0, v5}, Lbvx;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    sget v0, Lh;->lY:I

    new-array v5, v1, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->V:Ljava/lang/String;

    aput-object v6, v5, v2

    invoke-virtual {v3, v0, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    sget v5, Lh;->iW:I

    new-array v6, v1, [Ljava/lang/Object;

    iget v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->S:I

    packed-switch v0, :pswitch_data_1

    :pswitch_2
    sget v0, Lh;->iY:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_5
    aput-object v0, v6, v2

    invoke-virtual {v3, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v5, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->T:J

    cmp-long v0, v5, v9

    if-lez v0, :cond_7

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    sget v0, Lh;->gL:I

    new-array v1, v1, [Ljava/lang/Object;

    iget-wide v5, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->T:J

    invoke-static {v5, v6}, Lbvx;->b(J)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-virtual {v3, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->W:Ljava/lang/String;

    invoke-static {v0}, Lbzd;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    :cond_9
    sget v5, Lh;->kW:I

    new-array v6, v1, [Ljava/lang/Object;

    aput-object v0, v6, v2

    invoke-virtual {v3, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    :pswitch_3
    sget v0, Lh;->iV:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    :pswitch_4
    sget v0, Lh;->iX:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 1651
    :pswitch_5
    iget v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->Q:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_d

    .line 1652
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget v4, Lh;->gM:I

    new-array v5, v1, [Ljava/lang/Object;

    sget v6, Lh;->do:I

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v0, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v4, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->D:Z

    if-eqz v4, :cond_c

    iget-object v4, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->Z:Ljava/lang/String;

    if-nez v4, :cond_a

    iget-object v4, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->ad:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_a

    new-instance v4, Lyt;

    iget-object v5, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->ae:Lyj;

    invoke-direct {v4, v5}, Lyt;-><init>(Lyj;)V

    iget-object v5, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->ad:Ljava/lang/String;

    invoke-static {v4, v5}, Lyp;->f(Lyt;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->Z:Ljava/lang/String;

    :cond_a
    iget-object v4, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->Z:Ljava/lang/String;

    if-eqz v4, :cond_b

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    sget v4, Lh;->nj:I

    new-array v5, v1, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->Z:Ljava/lang/String;

    aput-object v6, v5, v2

    invoke-virtual {v0, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_b
    :goto_6
    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    sget v4, Lh;->kW:I

    new-array v1, v1, [Ljava/lang/Object;

    iget-wide v5, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->C:J

    invoke-static {v5, v6}, Lf;->a(J)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-virtual {v0, v4, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_c
    iget-object v4, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->w:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    iget-object v5, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->r:Lbdk;

    invoke-virtual {v4, v5}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lbdk;)Lbdh;

    move-result-object v4

    invoke-virtual {v4}, Lbdh;->c()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->W:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->W:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_b

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    sget v4, Lh;->cW:I

    new-array v5, v1, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->W:Ljava/lang/String;

    invoke-static {v6}, Lbzd;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v0, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    .line 1654
    :cond_d
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget v0, Lh;->gM:I

    new-array v3, v1, [Ljava/lang/Object;

    sget v6, Lh;->fv:I

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v2

    invoke-virtual {v4, v0, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->w:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->W()I

    move-result v0

    if-ne v0, v1, :cond_f

    move v0, v1

    :goto_7
    iget-boolean v3, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->D:Z

    if-eqz v3, :cond_10

    if-eqz v0, :cond_10

    iget-object v3, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->w:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->K()Lbdh;

    move-result-object v3

    :goto_8
    if-eqz v3, :cond_e

    iget-object v6, v3, Lbdh;->e:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_e

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-boolean v6, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->D:Z

    if-eqz v6, :cond_11

    if-eqz v0, :cond_11

    sget v0, Lh;->nj:I

    new-array v6, v1, [Ljava/lang/Object;

    iget-object v3, v3, Lbdh;->e:Ljava/lang/String;

    aput-object v3, v6, v2

    invoke-virtual {v4, v0, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_e
    :goto_9
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    sget v0, Lh;->kW:I

    new-array v1, v1, [Ljava/lang/Object;

    iget-wide v6, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->C:J

    invoke-static {v6, v7}, Lf;->a(J)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v4, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_f
    move v0, v2

    goto :goto_7

    :cond_10
    iget-object v3, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->w:Lcom/google/android/apps/hangouts/fragments/ConversationFragment;

    iget-object v6, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->r:Lbdk;

    invoke-virtual {v3, v6}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lbdk;)Lbdh;

    move-result-object v3

    goto :goto_8

    :cond_11
    sget v0, Lh;->cW:I

    new-array v6, v1, [Ljava/lang/Object;

    iget-object v3, v3, Lbdh;->e:Ljava/lang/String;

    aput-object v3, v6, v2

    invoke-virtual {v4, v0, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_9

    .line 1645
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_5
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 1649
    :pswitch_data_1
    .packed-switch 0x80
        :pswitch_4
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public v()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1901
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->V:Ljava/lang/String;

    return-object v0
.end method

.method public w()J
    .locals 2

    .prologue
    .line 1923
    iget-wide v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemView;->u:J

    return-wide v0
.end method
