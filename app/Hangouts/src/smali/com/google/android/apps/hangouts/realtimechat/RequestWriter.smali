.class public Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;
.super Lbou;
.source "PG"


# static fields
.field public static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<",
            "Lbea;",
            ">;",
            "Lbpi;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Z

.field private static volatile h:I

.field private static volatile i:J

.field private static volatile j:I

.field private static volatile k:Z


# instance fields
.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lbpn;",
            ">;"
        }
    .end annotation
.end field

.field private volatile d:Z

.field private e:Landroid/net/ConnectivityManager;

.field private f:Lbvs;

.field private final g:Ljava/util/Random;

.field private l:Lbpj;

.field private m:Lbpk;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 221
    sget-object v0, Lbys;->k:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->b:Z

    .line 278
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->m()V

    .line 281
    new-instance v0, Lbpg;

    invoke-direct {v0}, Lbpg;-><init>()V

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/Runnable;)V

    .line 2394
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->p()Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a:Ljava/util/Map;

    .line 2395
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 219
    invoke-direct {p0}, Lbou;-><init>()V

    .line 237
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->c:Ljava/util/Map;

    .line 243
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->g:Ljava/util/Random;

    .line 2295
    return-void
.end method

.method private a(J)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 780
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->g()Landroid/content/Intent;

    move-result-object v0

    .line 781
    const-string v1, "backoff_period"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 782
    const/16 v1, 0x67

    .line 784
    invoke-static {v1}, Lbzb;->a(I)I

    move-result v1

    const/high16 v2, 0x10000000

    .line 782
    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 789
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;)Lbpk;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->m:Lbpk;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;Lbpk;)Lbpk;
    .locals 0

    .prologue
    .line 219
    iput-object p1, p0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->m:Lbpk;

    return-object p1
.end method

.method public static a([B)Ljava/io/Serializable;
    .locals 3

    .prologue
    .line 320
    :try_start_0
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 321
    new-instance v1, Ljava/io/ObjectInputStream;

    invoke-direct {v1, v0}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 322
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/Serializable;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 327
    :goto_0
    return-object v0

    .line 323
    :catch_0
    move-exception v0

    .line 325
    const-string v1, "Babel_RequestWriter"

    const-string v2, "error decoding serialized stream"

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 327
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic a(Lbpp;Lbph;)V
    .locals 3

    .prologue
    .line 219
    iget-object v0, p0, Lbpp;->c:Ljava/lang/String;

    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lbpp;->a()Lbea;

    move-result-object v1

    invoke-virtual {v1, v0, p1}, Lbea;->a(Lyj;Lbph;)V

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Lbea;Lbph;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "Babel_RequestWriter"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Babel_RequestWriter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Skipping request failure for invalid account"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbpp;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Lbpp;Z)V
    .locals 3

    .prologue
    .line 1686
    iget-object v1, p1, Lbpp;->b:Ljava/lang/String;

    .line 1688
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->o()V

    .line 1689
    monitor-enter p0

    .line 1690
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->c:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpn;

    .line 1692
    if-nez v0, :cond_0

    .line 1693
    new-instance v0, Lbpn;

    invoke-direct {v0, p0, v1}, Lbpn;-><init>(Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->c:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1695
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1696
    invoke-virtual {v0, p1, p2}, Lbpn;->a(Lbpp;Z)V

    .line 1697
    return-void

    .line 1695
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;J)V
    .locals 3

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpn;

    iput-wide p1, v0, Lbpn;->g:J

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lbpn;->a(Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;Lbpp;)V
    .locals 1

    .prologue
    .line 219
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a(Lbpp;Z)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 219
    if-eqz p1, :cond_1

    :try_start_0
    invoke-static {p1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->o()V

    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpn;

    invoke-virtual {v0, v1, p2}, Lbpn;->a(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method private a(Z)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 715
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 716
    new-instance v0, Landroid/content/ComponentName;

    const-class v3, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter$NetworkStateReceiver;

    invoke-direct {v0, p0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v0}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v3

    .line 718
    if-eqz p1, :cond_2

    move v0, v1

    .line 720
    :goto_0
    if-eq v3, v0, :cond_1

    .line 721
    sget-boolean v3, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->b:Z

    if-eqz v3, :cond_0

    .line 722
    const-string v3, "Babel_RequestWriter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "network: Toggling network state receiver -- enabled: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 725
    :cond_0
    new-instance v3, Landroid/content/ComponentName;

    const-class v4, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter$NetworkStateReceiver;

    invoke-direct {v3, p0, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v3, v0, v1}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 730
    :cond_1
    return-void

    .line 718
    :cond_2
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 440
    :goto_0
    if-eqz p0, :cond_0

    .line 441
    invoke-virtual {p0}, Ljava/lang/Class;->getInterfaces()[Ljava/lang/Class;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 442
    const-class v5, Ljava/io/Serializable;

    if-ne v4, v5, :cond_1

    .line 443
    const/4 v0, 0x1

    .line 448
    :cond_0
    return v0

    .line 441
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 446
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object p0

    goto :goto_0
.end method

.method private a(Ljava/lang/Class;Ljava/lang/reflect/Type;ILjava/util/Set;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class;",
            "Ljava/lang/reflect/Type;",
            "I",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Class;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 375
    if-nez p3, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->b(Ljava/lang/Class;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 376
    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " does not specify serialVersionUID"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 382
    :cond_0
    const-class v2, Ljava/util/List;

    if-eq p1, v2, :cond_1

    const-class v2, Ljava/util/Set;

    if-eq p1, v2, :cond_1

    const-class v2, Ljava/util/Map;

    if-eq p1, v2, :cond_1

    .line 384
    invoke-static {p1}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a(Ljava/lang/Class;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 385
    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " doesn\'t implement Serializable"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 391
    :cond_1
    instance-of v2, p2, Ljava/lang/reflect/ParameterizedType;

    if-eqz v2, :cond_3

    .line 392
    check-cast p2, Ljava/lang/reflect/ParameterizedType;

    .line 393
    invoke-interface {p2}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v4

    array-length v5, v4

    move v3, v1

    move v2, v0

    :goto_0
    if-ge v3, v5, :cond_2

    aget-object v0, v4, v3

    .line 394
    instance-of v6, v0, Ljava/lang/Class;

    if-eqz v6, :cond_7

    .line 395
    check-cast v0, Ljava/lang/Class;

    .line 396
    const/4 v6, 0x0

    add-int/lit8 v7, p3, 0x1

    invoke-direct {p0, v0, v6, v7, p4}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a(Ljava/lang/Class;Ljava/lang/reflect/Type;ILjava/util/Set;)Z

    move-result v0

    and-int/2addr v0, v2

    .line 393
    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v2

    .line 405
    :cond_3
    :try_start_0
    const-string v2, "writeObject"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Ljava/io/ObjectOutputStream;

    aput-object v5, v3, v4

    invoke-virtual {p1, v2, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    .line 406
    const-string v2, "readObject"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Ljava/io/ObjectInputStream;

    aput-object v5, v3, v4

    invoke-virtual {p1, v2, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 432
    :cond_4
    if-nez v0, :cond_5

    .line 433
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " may not be serializable"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    :cond_5
    return v0

    .line 408
    :catch_0
    move-exception v2

    invoke-virtual {p1}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v2

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 409
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getModifiers()I

    move-result v5

    .line 410
    invoke-static {v5}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v6

    if-nez v6, :cond_6

    .line 411
    invoke-static {v5}, Ljava/lang/reflect/Modifier;->isTransient(I)Z

    move-result v5

    if-nez v5, :cond_6

    .line 414
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->isPrimitive()Z

    move-result v5

    if-nez v5, :cond_6

    .line 417
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v5

    const-class v6, Ljava/lang/String;

    if-eq v5, v6, :cond_6

    .line 423
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v5

    .line 424
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getGenericType()Ljava/lang/reflect/Type;

    move-result-object v4

    add-int/lit8 v6, p3, 0x1

    .line 422
    invoke-direct {p0, v5, v4, v6, p4}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a(Ljava/lang/Class;Ljava/lang/reflect/Type;ILjava/util/Set;)Z

    move-result v4

    and-int/2addr v0, v4

    .line 427
    if-eqz v0, :cond_4

    .line 428
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_7
    move v0, v2

    goto :goto_1
.end method

.method public static a(Lbea;)[B
    .locals 1

    .prologue
    .line 331
    invoke-static {p0}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a(Ljava/io/Serializable;)[B

    move-result-object v0

    return-object v0
.end method

.method public static a(Lbfz;)[B
    .locals 1

    .prologue
    .line 335
    invoke-static {p0}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a(Ljava/io/Serializable;)[B

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/io/Serializable;)[B
    .locals 4

    .prologue
    .line 298
    :try_start_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 299
    new-instance v1, Ljava/io/ObjectOutputStream;

    invoke-direct {v1, v0}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 300
    invoke-virtual {v1, p0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 301
    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->close()V

    .line 302
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 314
    :goto_0
    return-object v0

    .line 312
    :catch_0
    move-exception v0

    .line 313
    const-string v1, "Babel_RequestWriter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "got exception serializing object "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 314
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b([B)Lbea;
    .locals 1

    .prologue
    .line 339
    invoke-static {p0}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a([B)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lbea;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;)Z
    .locals 1

    .prologue
    .line 219
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->n()Z

    move-result v0

    return v0
.end method

.method private static b(Ljava/lang/Class;)Z
    .locals 1

    .prologue
    .line 453
    :try_start_0
    const-string v0, "serialVersionUID"

    invoke-virtual {p0, v0}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 454
    if-eqz v0, :cond_0

    .line 455
    invoke-virtual {v0}, Ljava/lang/reflect/Field;->getModifiers()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isStatic(I)Z
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_0

    .line 456
    const/4 v0, 0x1

    .line 460
    :goto_0
    return v0

    :catch_0
    move-exception v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c([B)Lbfz;
    .locals 1

    .prologue
    .line 343
    invoke-static {p0}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a([B)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lbfz;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;)Z
    .locals 1

    .prologue
    .line 219
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->d:Z

    return v0
.end method

.method public static synthetic d(Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;)Ljava/util/Random;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->g:Ljava/util/Random;

    return-object v0
.end method

.method public static g()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 1619
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1620
    return-object v0
.end method

.method public static synthetic i()V
    .locals 0

    .prologue
    .line 219
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->m()V

    return-void
.end method

.method public static synthetic j()Z
    .locals 1

    .prologue
    .line 219
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->b:Z

    return v0
.end method

.method public static synthetic k()J
    .locals 2

    .prologue
    .line 219
    sget-wide v0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->i:J

    return-wide v0
.end method

.method public static synthetic l()I
    .locals 1

    .prologue
    .line 219
    sget v0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->h:I

    return v0
.end method

.method private static m()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 255
    const-string v0, "babel_max_unexpected_error_retries"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->h:I

    .line 259
    const-string v0, "babel_request_writer_failure_injection_percent"

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;J)J

    move-result-wide v0

    .line 263
    sput-wide v0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->i:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 264
    const-string v0, "Babel_RequestWriter"

    const-string v1, "******************************************************"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    const-string v0, "Babel_RequestWriter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "failureInjectionPercent set to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-wide v2, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->i:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    const-string v0, "Babel_RequestWriter"

    const-string v1, "******************************************************"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    :cond_0
    invoke-static {}, Lf;->w()Z

    move-result v0

    sput-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->k:Z

    .line 270
    const-string v0, "babel_requestwriter_ms"

    const/16 v1, 0x1388

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->j:I

    .line 273
    return-void
.end method

.method private n()Z
    .locals 4

    .prologue
    .line 532
    iget-object v0, p0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->e:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 533
    iget-boolean v1, p0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->d:Z

    .line 534
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->d:Z

    .line 535
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->d:Z

    if-eq v0, v1, :cond_0

    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->b:Z

    if-eqz v0, :cond_0

    .line 536
    const-string v0, "Babel_RequestWriter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "network: updateNetworkAvailable was "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " now "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->d:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->d:Z

    return v0

    .line 534
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private o()V
    .locals 3

    .prologue
    .line 1628
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->k:Z

    if-nez v0, :cond_1

    .line 1640
    :cond_0
    return-void

    .line 1631
    :cond_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    .line 1632
    iget-object v0, p0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpn;

    .line 1635
    iget-object v0, v0, Lbpn;->b:Ljava/lang/Thread;

    if-ne v0, v1, :cond_2

    .line 1636
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "don\'t get RequestWriter lock from network queue thread"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static p()Ljava/util/Map;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<",
            "Lbea;",
            ">;",
            "Lbpi;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v10, 0x7c

    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 2303
    new-array v1, v10, [Ljava/lang/Class;

    const-class v2, Lbee;

    aput-object v2, v1, v0

    const-class v2, Lbgd;

    aput-object v2, v1, v3

    const/4 v2, 0x2

    const-class v3, Lbfb;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-class v3, Lbhw;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Lbfv;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-class v3, Lbio;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-class v3, Lbeb;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-class v3, Lbgb;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-class v3, Lbew;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-class v3, Lbhp;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-class v3, Lbex;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-class v3, Lbhq;

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-class v3, Lbfg;

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-class v3, Lbia;

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-class v3, Lbfj;

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-class v3, Lbie;

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-class v3, Lbfo;

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-class v3, Lbij;

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-class v3, Lbes;

    aput-object v3, v1, v2

    const/16 v2, 0x13

    const-class v3, Lbhh;

    aput-object v3, v1, v2

    const/16 v2, 0x14

    const-class v3, Lbey;

    aput-object v3, v1, v2

    const/16 v2, 0x15

    const-class v3, Lbhr;

    aput-object v3, v1, v2

    const/16 v2, 0x16

    const-class v3, Lbev;

    aput-object v3, v1, v2

    const/16 v2, 0x17

    const-class v3, Lbhn;

    aput-object v3, v1, v2

    const/16 v2, 0x18

    const-class v3, Lbft;

    aput-object v3, v1, v2

    const/16 v2, 0x19

    const-class v3, Lbim;

    aput-object v3, v1, v2

    const/16 v2, 0x1a

    const-class v3, Lbek;

    aput-object v3, v1, v2

    const/16 v2, 0x1b

    const-class v3, Lbgr;

    aput-object v3, v1, v2

    const/16 v2, 0x1c

    const-class v3, Lbfs;

    aput-object v3, v1, v2

    const/16 v2, 0x1d

    const-class v3, Lbil;

    aput-object v3, v1, v2

    const/16 v2, 0x1e

    const-class v3, Lbeo;

    aput-object v3, v1, v2

    const/16 v2, 0x1f

    const-class v3, Lbhb;

    aput-object v3, v1, v2

    const/16 v2, 0x20

    const-class v3, Lbeg;

    aput-object v3, v1, v2

    const/16 v2, 0x21

    const-class v3, Lbgg;

    aput-object v3, v1, v2

    const/16 v2, 0x22

    const-class v3, Lbfu;

    aput-object v3, v1, v2

    const/16 v2, 0x23

    const-class v3, Lbin;

    aput-object v3, v1, v2

    const/16 v2, 0x24

    const-class v3, Lbfa;

    aput-object v3, v1, v2

    const/16 v2, 0x25

    const-class v3, Lbhv;

    aput-object v3, v1, v2

    const/16 v2, 0x26

    const-class v3, Lbel;

    aput-object v3, v1, v2

    const/16 v2, 0x27

    const-class v3, Lbgs;

    aput-object v3, v1, v2

    const/16 v2, 0x28

    const-class v3, Lbet;

    aput-object v3, v1, v2

    const/16 v2, 0x29

    const-class v3, Lbhl;

    aput-object v3, v1, v2

    const/16 v2, 0x2a

    const-class v3, Lber;

    aput-object v3, v1, v2

    const/16 v2, 0x2b

    const-class v3, Lbhg;

    aput-object v3, v1, v2

    const/16 v2, 0x2c

    const-class v3, Lbfi;

    aput-object v3, v1, v2

    const/16 v2, 0x2d

    const-class v3, Lbid;

    aput-object v3, v1, v2

    const/16 v2, 0x2e

    const-class v3, Lbfl;

    aput-object v3, v1, v2

    const/16 v2, 0x2f

    const-class v3, Lbig;

    aput-object v3, v1, v2

    const/16 v2, 0x30

    const-class v3, Lbfm;

    aput-object v3, v1, v2

    const/16 v2, 0x31

    const-class v3, Lbih;

    aput-object v3, v1, v2

    const/16 v2, 0x32

    const-class v3, Lben;

    aput-object v3, v1, v2

    const/16 v2, 0x33

    const-class v3, Lbha;

    aput-object v3, v1, v2

    const/16 v2, 0x34

    const-class v3, Lbec;

    aput-object v3, v1, v2

    const/16 v2, 0x35

    const-class v3, Lbgc;

    aput-object v3, v1, v2

    const/16 v2, 0x36

    const-class v3, Lbdu;

    aput-object v3, v1, v2

    const/16 v2, 0x37

    const-class v3, Lbgv;

    aput-object v3, v1, v2

    const/16 v2, 0x38

    const-class v3, Lbdv;

    aput-object v3, v1, v2

    const/16 v2, 0x39

    const-class v3, Lbgw;

    aput-object v3, v1, v2

    const/16 v2, 0x3a

    const-class v3, Lbdr;

    aput-object v3, v1, v2

    const/16 v2, 0x3b

    const-class v3, Lbge;

    aput-object v3, v1, v2

    const/16 v2, 0x3c

    const-class v3, Lbdt;

    aput-object v3, v1, v2

    const/16 v2, 0x3d

    const-class v3, Lbgu;

    aput-object v3, v1, v2

    const/16 v2, 0x3e

    const-class v3, Lbdz;

    aput-object v3, v1, v2

    const/16 v2, 0x3f

    const-class v3, Lbhs;

    aput-object v3, v1, v2

    const/16 v2, 0x40

    const-class v3, Lbdw;

    aput-object v3, v1, v2

    const/16 v2, 0x41

    const-class v3, Lbhe;

    aput-object v3, v1, v2

    const/16 v2, 0x42

    const-class v3, Lbdb;

    aput-object v3, v1, v2

    const/16 v2, 0x43

    const-class v3, Lbgp;

    aput-object v3, v1, v2

    const/16 v2, 0x44

    const-class v3, Lbdg;

    aput-object v3, v1, v2

    const/16 v2, 0x45

    const-class v3, Lbib;

    aput-object v3, v1, v2

    const/16 v2, 0x46

    const-class v3, Lbef;

    aput-object v3, v1, v2

    const/16 v2, 0x47

    const-class v3, Lbgf;

    aput-object v3, v1, v2

    const/16 v2, 0x48

    const-class v3, Lbdc;

    aput-object v3, v1, v2

    const/16 v2, 0x49

    const-class v3, Lbgy;

    aput-object v3, v1, v2

    const/16 v2, 0x4a

    const-class v3, Lbfh;

    aput-object v3, v1, v2

    const/16 v2, 0x4b

    const-class v3, Lbic;

    aput-object v3, v1, v2

    const/16 v2, 0x4c

    const-class v3, Lbfr;

    aput-object v3, v1, v2

    const/16 v2, 0x4d

    const-class v3, Lbik;

    aput-object v3, v1, v2

    const/16 v2, 0x4e

    const-class v3, Lbdq;

    aput-object v3, v1, v2

    const/16 v2, 0x4f

    const-class v3, Lbga;

    aput-object v3, v1, v2

    const/16 v2, 0x50

    const-class v3, Lbds;

    aput-object v3, v1, v2

    const/16 v2, 0x51

    const-class v3, Lbgn;

    aput-object v3, v1, v2

    const/16 v2, 0x52

    const-class v3, Lbdx;

    aput-object v3, v1, v2

    const/16 v2, 0x53

    const-class v3, Lbhf;

    aput-object v3, v1, v2

    const/16 v2, 0x54

    const-class v3, Lbdy;

    aput-object v3, v1, v2

    const/16 v2, 0x55

    const-class v3, Lbho;

    aput-object v3, v1, v2

    const/16 v2, 0x56

    const-class v3, Lbej;

    aput-object v3, v1, v2

    const/16 v2, 0x57

    const-class v3, Lbgl;

    aput-object v3, v1, v2

    const/16 v2, 0x58

    const-class v3, Lbfe;

    aput-object v3, v1, v2

    const/16 v2, 0x59

    const-class v3, Lbhy;

    aput-object v3, v1, v2

    const/16 v2, 0x5a

    const-class v3, Lbdf;

    aput-object v3, v1, v2

    const/16 v2, 0x5b

    const-class v3, Lbhj;

    aput-object v3, v1, v2

    const/16 v2, 0x5c

    const-class v3, Lbde;

    aput-object v3, v1, v2

    const/16 v2, 0x5d

    const-class v3, Lbhi;

    aput-object v3, v1, v2

    const/16 v2, 0x5e

    const-class v3, Lbeu;

    aput-object v3, v1, v2

    const/16 v2, 0x5f

    const-class v3, Lbhm;

    aput-object v3, v1, v2

    const/16 v2, 0x60

    const-class v3, Lbff;

    aput-object v3, v1, v2

    const/16 v2, 0x61

    const-class v3, Lbhz;

    aput-object v3, v1, v2

    const/16 v2, 0x62

    const-class v3, Lbfc;

    aput-object v3, v1, v2

    const/16 v2, 0x63

    const-class v3, Lbhx;

    aput-object v3, v1, v2

    const/16 v2, 0x64

    const-class v3, Lbez;

    aput-object v3, v1, v2

    const/16 v2, 0x65

    const-class v3, Lbhu;

    aput-object v3, v1, v2

    const/16 v2, 0x66

    const-class v3, Lbfn;

    aput-object v3, v1, v2

    const/16 v2, 0x67

    const-class v3, Lbii;

    aput-object v3, v1, v2

    const/16 v2, 0x68

    const-class v3, Lbfw;

    aput-object v3, v1, v2

    const/16 v2, 0x69

    const-class v3, Lbip;

    aput-object v3, v1, v2

    const/16 v2, 0x6a

    const-class v3, Lbfk;

    aput-object v3, v1, v2

    const/16 v2, 0x6b

    const-class v3, Lbif;

    aput-object v3, v1, v2

    const/16 v2, 0x6c

    const-class v3, Lbeh;

    aput-object v3, v1, v2

    const/16 v2, 0x6d

    const-class v3, Lbgh;

    aput-object v3, v1, v2

    const/16 v2, 0x6e

    const-class v3, Lbem;

    aput-object v3, v1, v2

    const/16 v2, 0x6f

    const-class v3, Lbgt;

    aput-object v3, v1, v2

    const/16 v2, 0x70

    const-class v3, Lbdd;

    aput-object v3, v1, v2

    const/16 v2, 0x71

    const-class v3, Lbhc;

    aput-object v3, v1, v2

    const/16 v2, 0x72

    const-class v3, Lbda;

    aput-object v3, v1, v2

    const/16 v2, 0x73

    const-class v3, Lbgm;

    aput-object v3, v1, v2

    const/16 v2, 0x74

    const-class v3, Lbcv;

    aput-object v3, v1, v2

    const/16 v2, 0x75

    const-class v3, Lbhd;

    aput-object v3, v1, v2

    const/16 v2, 0x76

    const-class v3, Lbcs;

    aput-object v3, v1, v2

    const/16 v2, 0x77

    const-class v3, Lbgi;

    aput-object v3, v1, v2

    const/16 v2, 0x78

    const-class v3, Lbct;

    aput-object v3, v1, v2

    const/16 v2, 0x79

    const-class v3, Lbgo;

    aput-object v3, v1, v2

    const/16 v2, 0x7a

    const-class v3, Lbcu;

    aput-object v3, v1, v2

    const/16 v2, 0x7b

    const-class v3, Lbgx;

    aput-object v3, v1, v2

    .line 2369
    new-instance v2, Les;

    invoke-direct {v2}, Les;-><init>()V

    .line 2371
    :goto_0
    if-ge v0, v10, :cond_1

    .line 2372
    aget-object v3, v1, v0

    .line 2373
    add-int/lit8 v4, v0, 0x1

    aget-object v4, v1, v4

    .line 2375
    new-instance v5, Lbpi;

    invoke-direct {v5}, Lbpi;-><init>()V

    .line 2376
    iput-object v4, v5, Lbpi;->a:Ljava/lang/Class;

    .line 2380
    const-class v6, Lbep;

    invoke-virtual {v6, v3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2382
    :try_start_0
    const-string v6, "parseFrom"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v9, [B

    aput-object v9, v7, v8

    invoke-virtual {v4, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    iput-object v6, v5, Lbpi;->b:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2388
    :cond_0
    :goto_1
    invoke-interface {v2, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2371
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 2384
    :catch_0
    move-exception v6

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "couldn\'t find parseFrom method for "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 2390
    :cond_1
    return-object v2
.end method


# virtual methods
.method protected a()I
    .locals 1

    .prologue
    .line 291
    sget v0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->j:I

    return v0
.end method

.method public a(Lbea;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1700
    if-nez p1, :cond_0

    .line 1701
    const-string v0, "Babel_RequestWriter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[RequestWriter] invalid request "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 1702
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid request "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1708
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lbea;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1710
    new-instance v0, Lbpp;

    .line 1716
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object v2, p2

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lbpp;-><init>(Ljava/lang/String;Ljava/lang/String;Lbea;J)V

    .line 1717
    iget-object v1, p0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->m:Lbpk;

    invoke-virtual {v1, v0}, Lbpk;->a(Lbpp;)J

    move-result-wide v1

    iput-wide v1, v0, Lbpp;->a:J

    .line 1721
    invoke-virtual {p1}, Lbea;->r()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a(Lbpp;Z)V

    .line 1722
    return-void
.end method

.method public h()V
    .locals 0

    .prologue
    .line 1624
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->n()Z

    .line 1625
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 544
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 465
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->b:Z

    if-eqz v0, :cond_0

    .line 466
    const-string v0, "Babel_RequestWriter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RequestWriter.onCreate "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    :cond_0
    invoke-super {p0}, Lbou;->onCreate()V

    .line 476
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcwt;->a(Landroid/content/Context;)V
    :try_end_0
    .catch Lcfy; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcfx; {:try_start_0 .. :try_end_0} :catch_1

    .line 489
    :goto_0
    sget-object v0, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v1, "eng"

    .line 490
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v1, "userdebug"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v0, v3

    .line 491
    :goto_1
    if-eqz v0, :cond_4

    .line 493
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 495
    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v3

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 496
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Class;

    invoke-direct {p0, v1, v7, v4, v5}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a(Ljava/lang/Class;Ljava/lang/reflect/Type;ILjava/util/Set;)Z

    move-result v1

    and-int/2addr v1, v2

    .line 497
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpi;

    iget-object v0, v0, Lbpi;->a:Ljava/lang/Class;

    invoke-direct {p0, v0, v7, v4, v5}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a(Ljava/lang/Class;Ljava/lang/reflect/Type;ILjava/util/Set;)Z

    move-result v0

    and-int/2addr v0, v1

    move v2, v0

    .line 498
    goto :goto_2

    .line 477
    :catch_0
    move-exception v0

    .line 483
    invoke-virtual {v0}, Lcfy;->a()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 482
    invoke-static {v1, v2}, Lcfz;->a(ILandroid/content/Context;)V

    .line 484
    const-string v1, "Babel_RequestWriter"

    const-string v2, "GPS repairable exception when trying to installIfNeeded"

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 485
    :catch_1
    move-exception v0

    .line 486
    const-string v1, "Babel_RequestWriter"

    const-string v2, "GPS not available when trying to installIfNeeded"

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_2
    move v0, v4

    .line 490
    goto :goto_1

    .line 499
    :cond_3
    if-nez v2, :cond_4

    .line 500
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "not all ServerRequest and ServerResponse classes are set up correctly for serialization"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 507
    :cond_4
    new-instance v0, Lbpj;

    invoke-direct {v0, p0}, Lbpj;-><init>(Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->l:Lbpj;

    .line 508
    iget-object v0, p0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->l:Lbpj;

    invoke-virtual {v0}, Lbpj;->start()V

    .line 510
    const-string v0, "connectivity"

    .line 511
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->e:Landroid/net/ConnectivityManager;

    .line 512
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->n()Z

    .line 516
    invoke-static {p0}, Lbvq;->a(Landroid/content/Context;)Lbvs;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->f:Lbvs;

    .line 518
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->e()V

    .line 520
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 521
    iput v3, v0, Landroid/os/Message;->arg1:I

    .line 522
    iget-object v1, p0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->l:Lbpj;

    iget-object v1, v1, Lbpj;->a:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 527
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 528
    const-wide/16 v1, 0x1f4

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a(J)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 529
    return-void
.end method

.method public onDestroy()V
    .locals 12

    .prologue
    const-wide v5, 0x7fffffffffffffffL

    .line 734
    invoke-super {p0}, Lbou;->onDestroy()V

    .line 736
    const-wide/16 v0, 0x1f4

    .line 738
    iget-object v2, p0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->l:Lbpj;

    iget-object v2, v2, Lbpj;->a:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Looper;->quit()V

    .line 740
    iget-object v2, p0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->c:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-wide v3, v5

    move-wide v10, v0

    move-wide v1, v10

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpn;

    .line 741
    invoke-virtual {v0}, Lbpn;->e()V

    .line 742
    invoke-virtual {v0}, Lbpn;->b()J

    move-result-wide v8

    invoke-static {v3, v4, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v3

    .line 744
    invoke-virtual {v0}, Lbpn;->c()I

    move-result v8

    if-lez v8, :cond_4

    .line 745
    iget-wide v8, v0, Lbpn;->g:J

    invoke-static {v1, v2, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    :goto_1
    move-wide v1, v0

    .line 747
    goto :goto_0

    .line 748
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 750
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->b:Z

    if-eqz v0, :cond_1

    .line 751
    const-string v0, "Babel_RequestWriter"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Recycling request writer. Will restart after "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 754
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->m:Lbpk;

    if-eqz v0, :cond_2

    .line 755
    iget-object v0, p0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->m:Lbpk;

    invoke-virtual {v0}, Lbpk;->close()V

    .line 756
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->m:Lbpk;

    .line 761
    :cond_2
    cmp-long v0, v3, v5

    if-gez v0, :cond_3

    .line 764
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 765
    const/4 v5, 0x3

    .line 767
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    add-long/2addr v3, v6

    .line 768
    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a(J)Landroid/app/PendingIntent;

    move-result-object v1

    .line 765
    invoke-virtual {v0, v5, v3, v4, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 769
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a(Z)V

    .line 775
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->f:Lbvs;

    invoke-static {p0, v0}, Lbvq;->a(Landroid/content/Context;Lbvs;)V

    .line 777
    return-void

    .line 772
    :cond_3
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a(Z)V

    goto :goto_2

    :cond_4
    move-wide v0, v1

    goto :goto_1
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 5

    .prologue
    .line 697
    if-eqz p1, :cond_0

    .line 700
    invoke-virtual {p0, p3}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a(I)V

    .line 703
    const-string v0, "timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    mul-long/2addr v1, v3

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 704
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 705
    invoke-virtual {v0}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "intent"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 707
    iget-object v1, p0, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->l:Lbpj;

    iget-object v1, v1, Lbpj;->a:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 708
    const-string v0, "Babel"

    const-string v1, "mHandler.sendMessage returned false"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 711
    :cond_0
    const/4 v0, 0x2

    return v0
.end method
