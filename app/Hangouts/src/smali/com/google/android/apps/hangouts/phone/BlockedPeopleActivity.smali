.class public Lcom/google/android/apps/hangouts/phone/BlockedPeopleActivity;
.super Lakn;
.source "PG"


# instance fields
.field private r:Lyj;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lakn;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 39
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 45
    invoke-super {p0, p1}, Lakn;->a(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 41
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BlockedPeopleActivity;->onBackPressed()V

    .line 42
    const/4 v0, 0x1

    goto :goto_0

    .line 39
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected k()Lyj;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BlockedPeopleActivity;->r:Lyj;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 21
    invoke-super {p0, p1}, Lakn;->onCreate(Landroid/os/Bundle;)V

    .line 24
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BlockedPeopleActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 23
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/BlockedPeopleActivity;->r:Lyj;

    .line 26
    sget v0, Lf;->ej:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/BlockedPeopleActivity;->setContentView(I)V

    .line 28
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BlockedPeopleActivity;->g()Lkd;

    move-result-object v0

    .line 29
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lkd;->a(Z)V

    .line 30
    return-void
.end method
