.class public Lcom/google/android/apps/hangouts/settings/BabelSettingsActivityGingerbread;
.super Lbux;
.source "PG"


# instance fields
.field private a:Lbtf;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lbux;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;IZLandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 116
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 117
    invoke-virtual {v0, p1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 118
    if-eqz p2, :cond_0

    .line 119
    invoke-virtual {v0, p2}, Landroid/preference/Preference;->setSummary(I)V

    .line 121
    :cond_0
    invoke-virtual {v0, p3}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 122
    invoke-virtual {v0, p4}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 123
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivityGingerbread;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 124
    return-void
.end method

.method private a(Ljava/lang/String;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 112
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivityGingerbread;->a(Ljava/lang/String;IZLandroid/content/Intent;)V

    .line 113
    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 138
    invoke-super {p0, p1, p2, p3}, Lbux;->onActivityResult(IILandroid/content/Intent;)V

    .line 139
    packed-switch p1, :pswitch_data_0

    .line 149
    :cond_0
    :goto_0
    return-void

    .line 141
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivityGingerbread;->a:Lbtf;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivityGingerbread;->a:Lbtf;

    const-string v1, "com.google.android.gms.people.profile.EXTRA_AVATAR_URL"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbtf;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 139
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 34
    invoke-super {p0, p1}, Lbux;->onCreate(Landroid/os/Bundle;)V

    .line 36
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivityGingerbread;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 37
    const-string v1, "settings_acl"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 39
    const-string v2, "settings_goto_rstatus"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 41
    if-nez v1, :cond_0

    if-eqz v2, :cond_2

    .line 42
    :cond_0
    const-string v2, "account_name"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 45
    if-eqz v1, :cond_3

    .line 46
    invoke-static {v4}, Lbbl;->j(Lyj;)Landroid/content/Intent;

    move-result-object v0

    .line 51
    :goto_0
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 52
    const-string v1, "account_name"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 53
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivityGingerbread;->startActivity(Landroid/content/Intent;)V

    .line 55
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivityGingerbread;->finish()V

    .line 58
    :cond_2
    invoke-virtual {p0, v3}, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivityGingerbread;->setResult(I)V

    .line 59
    return-void

    .line 48
    :cond_3
    invoke-static {v4}, Lbbl;->i(Lyj;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 63
    invoke-super {p0}, Lbux;->onResume()V

    .line 64
    invoke-static {}, Lbkb;->A()Ljava/util/ArrayList;

    move-result-object v0

    .line 65
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 66
    invoke-virtual {p0, v4}, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivityGingerbread;->setResult(I)V

    .line 67
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivityGingerbread;->finish()V

    .line 69
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivityGingerbread;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 70
    const-string v2, "account_name"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 72
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivityGingerbread;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    .line 73
    if-eqz v2, :cond_1

    .line 74
    invoke-virtual {v2}, Landroid/preference/PreferenceScreen;->removeAll()V

    .line 76
    :cond_1
    if-nez v1, :cond_6

    .line 78
    sget v1, Lf;->id:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivityGingerbread;->addPreferencesFromResource(I)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v2

    invoke-virtual {v2}, Lyj;->u()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {v2}, Lbkb;->l(Lyj;)Z

    move-result v2

    if-eqz v2, :cond_3

    sget v2, Lh;->fD:I

    const/4 v3, 0x0

    invoke-direct {p0, v0, v2, v4, v3}, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivityGingerbread;->a(Ljava/lang/String;IZLandroid/content/Intent;)V

    goto :goto_0

    :cond_3
    invoke-static {v0}, Lbbl;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivityGingerbread;->a(Ljava/lang/String;Landroid/content/Intent;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivityGingerbread;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->ge:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lbkb;->j()Lyj;

    move-result-object v1

    invoke-static {v1}, Lbbl;->p(Lyj;)Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivityGingerbread;->a(Ljava/lang/String;Landroid/content/Intent;)V

    invoke-static {}, Lbzd;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/hangouts/settings/SmsMmsSettingsActivityGingerbread;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivityGingerbread;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lh;->lX:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivityGingerbread;->a(Ljava/lang/String;Landroid/content/Intent;)V

    .line 83
    :cond_5
    :goto_1
    return-void

    .line 81
    :cond_6
    const-string v0, "Babel"

    const-string v2, "BabelSettingsActivity.initSettings"

    invoke-static {v0, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lbua;

    invoke-direct {v0, p0, p0, v1}, Lbua;-><init>(Lbtz;Landroid/app/Activity;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivityGingerbread;->a:Lbtf;

    iget-object v0, p0, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivityGingerbread;->a:Lbtf;

    invoke-virtual {v0}, Lbtf;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/hangouts/settings/BabelSettingsActivityGingerbread;->a:Lbtf;

    invoke-virtual {v0}, Lbtf;->e()V

    goto :goto_1
.end method
