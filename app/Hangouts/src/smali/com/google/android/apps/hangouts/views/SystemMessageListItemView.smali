.class public Lcom/google/android/apps/hangouts/views/SystemMessageListItemView;
.super Landroid/widget/RelativeLayout;
.source "PG"

# interfaces
.implements Lcdg;


# static fields
.field private static a:Z

.field private static b:I

.field private static c:I


# instance fields
.field private d:Landroid/widget/TextView;

.field private e:Ljava/lang/CharSequence;

.field private f:Ljava/lang/CharSequence;

.field private g:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/hangouts/views/SystemMessageListItemView;->a:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/hangouts/views/SystemMessageListItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    sget-boolean v0, Lcom/google/android/apps/hangouts/views/SystemMessageListItemView;->a:Z

    if-nez v0, :cond_0

    .line 31
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 32
    sget v1, Lf;->cC:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/hangouts/views/SystemMessageListItemView;->b:I

    .line 33
    sget v1, Lf;->cB:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/hangouts/views/SystemMessageListItemView;->c:I

    .line 34
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/apps/hangouts/views/SystemMessageListItemView;->a:Z

    .line 36
    :cond_0
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/SystemMessageListItemView;->d:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<i>"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/SystemMessageListItemView;->e:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</i>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 71
    iget-wide v0, p0, Lcom/google/android/apps/hangouts/views/SystemMessageListItemView;->g:J

    return-wide v0
.end method

.method public a(J)V
    .locals 0

    .prologue
    .line 58
    iput-wide p1, p0, Lcom/google/android/apps/hangouts/views/SystemMessageListItemView;->g:J

    .line 59
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/google/android/apps/hangouts/views/SystemMessageListItemView;->e:Ljava/lang/CharSequence;

    .line 45
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/SystemMessageListItemView;->c()V

    .line 46
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 62
    if-eqz p1, :cond_0

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/SystemMessageListItemView;->d:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/hangouts/views/SystemMessageListItemView;->c:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 67
    :goto_0
    return-void

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/SystemMessageListItemView;->d:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/hangouts/views/SystemMessageListItemView;->b:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public b()Landroid/view/View;
    .locals 0

    .prologue
    .line 76
    return-object p0
.end method

.method public b(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/apps/hangouts/views/SystemMessageListItemView;->f:Ljava/lang/CharSequence;

    .line 50
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/SystemMessageListItemView;->c()V

    .line 51
    return-void
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 40
    sget v0, Lg;->hm:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/SystemMessageListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/SystemMessageListItemView;->d:Landroid/widget/TextView;

    .line 41
    return-void
.end method
