.class public final Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;
.super Lakn;
.source "PG"

# interfaces
.implements Ladi;
.implements Lair;
.implements Lalv;
.implements Lbkp;
.implements Lcfv;
.implements Lcfw;


# static fields
.field private static O:Z

.field private static P:J

.field private static T:Z

.field public static r:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Laxv;",
            ">;"
        }
    .end annotation
.end field

.field private static final s:Z


# instance fields
.field private A:Landroid/view/MenuItem;

.field private B:Landroid/widget/TextView;

.field private C:Z

.field private final D:Landroid/os/Handler;

.field private E:Landroid/os/Bundle;

.field private F:Z

.field private G:Z

.field private H:Landroid/support/v4/widget/DrawerLayout;

.field private I:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

.field private J:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;

.field private K:Landroid/support/v4/view/ViewPager;

.field private L:Laxw;

.field private M:Landroid/graphics/Bitmap;

.field private N:Ljava/lang/Runnable;

.field private Q:J

.field private final R:Lki;

.field private S:Z

.field private t:Lyj;

.field private u:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

.field private v:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

.field private w:Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 160
    sget-object v0, Lbys;->g:Lcyp;

    sput-boolean v1, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->s:Z

    .line 1094
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->s()V

    .line 1096
    new-instance v0, Laxq;

    invoke-direct {v0}, Laxq;-><init>()V

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/Runnable;)V

    .line 1158
    sput-boolean v1, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->T:Z

    .line 2728
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->r:Ljava/util/LinkedList;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 152
    invoke-direct {p0}, Lakn;-><init>()V

    .line 199
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->C:Z

    .line 201
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->D:Landroid/os/Handler;

    .line 205
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->F:Z

    .line 224
    new-instance v0, Lawc;

    invoke-direct {v0, p0}, Lawc;-><init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->R:Lki;

    .line 589
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->S:Z

    .line 2680
    return-void
.end method

.method private A()V
    .locals 2

    .prologue
    .line 2314
    const-string v0, "Babel"

    const-string v1, "Adding a new account"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 2315
    new-instance v0, Laxg;

    invoke-direct {v0, p0}, Laxg;-><init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)V

    invoke-static {p0, v0}, Lf;->a(Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;)V

    .line 2349
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->finish()V

    .line 2350
    return-void
.end method

.method private B()V
    .locals 1

    .prologue
    .line 2856
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->n()V

    .line 2857
    const-string v0, "phone_calls"

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->d(Ljava/lang/String;)Z

    .line 2858
    return-void
.end method

.method private C()Z
    .locals 2

    .prologue
    .line 2872
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->H:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->I:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->I:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    .line 2873
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2874
    :cond_0
    const/4 v0, 0x0

    .line 2876
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->H:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->I:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->j(Landroid/view/View;)Z

    move-result v0

    goto :goto_0
.end method

.method private D()V
    .locals 1

    .prologue
    .line 2881
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->H:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_0

    .line 2882
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->H:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->b()V

    .line 2884
    :cond_0
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;Lyj;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 152
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->b(Lyj;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/graphics/Bitmap;)V
    .locals 4

    .prologue
    .line 2298
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->g()Lkd;

    move-result-object v0

    .line 2299
    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->M:Landroid/graphics/Bitmap;

    if-nez v1, :cond_0

    .line 2300
    invoke-static {}, Lbyr;->b()Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->M:Landroid/graphics/Bitmap;

    .line 2302
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->M:Landroid/graphics/Bitmap;

    invoke-static {p1, v1}, Lbyr;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    .line 2303
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->M:Landroid/graphics/Bitmap;

    invoke-direct {v1, v2, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, Lkd;->a(Landroid/graphics/drawable/Drawable;)V

    .line 2304
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)V
    .locals 0

    .prologue
    .line 152
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->D()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;Lahc;)V
    .locals 0

    .prologue
    .line 152
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->b(Lahc;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 152
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->c(Landroid/content/Intent;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 152
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->a(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;Z)V
    .locals 0

    .prologue
    .line 152
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Z)V

    return-void
.end method

.method public static a(Ljava/io/PrintWriter;)V
    .locals 7

    .prologue
    .line 2732
    const/4 v2, 0x0

    .line 2733
    const-wide/16 v0, 0x0

    .line 2734
    sget-object v3, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->r:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v3, v2

    move-wide v5, v0

    move-wide v1, v5

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laxv;

    .line 2735
    invoke-virtual {v0, v3, p0, v1, v2}, Laxv;->a(ILjava/io/PrintWriter;J)V

    .line 2736
    iget-wide v0, v0, Laxv;->a:J

    .line 2737
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-wide v5, v0

    move-wide v1, v5

    .line 2738
    goto :goto_0

    .line 2739
    :cond_0
    return-void
.end method

.method private a(Lyj;ZZ)Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2096
    if-eqz p1, :cond_0

    .line 2097
    invoke-virtual {p1}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object p1

    .line 2099
    :cond_0
    sget-boolean v0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->s:Z

    if-eqz v0, :cond_1

    .line 2100
    const-string v0, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "setAccount account: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", tryOthers: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", setupAll: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2104
    :cond_1
    if-nez p1, :cond_2

    if-nez p2, :cond_2

    .line 2105
    const-string v0, "Babel"

    const-string v1, "Tried to start Babel with an invalid intent account"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 2106
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->finish()V

    .line 2196
    :goto_0
    return v2

    .line 2113
    :cond_2
    if-nez p1, :cond_3

    .line 2115
    invoke-static {v2}, Lbkb;->e(Z)Lyj;

    move-result-object v0

    .line 2116
    if-nez v0, :cond_1a

    .line 2117
    invoke-static {v1}, Lbkb;->e(Z)Lyj;

    move-result-object v3

    if-nez v3, :cond_1a

    .line 2120
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->A()V

    goto :goto_0

    .line 2124
    :cond_3
    invoke-static {p1}, Lbkb;->l(Lyj;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2125
    invoke-static {p1}, Lbkb;->k(Lyj;)V

    :cond_4
    move-object v3, p1

    .line 2128
    :goto_1
    if-eqz v3, :cond_12

    invoke-static {v3}, Lbkb;->f(Lyj;)I

    move-result v0

    const/16 v5, 0x66

    if-ne v0, v5, :cond_12

    .line 2130
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    if-ne v0, v3, :cond_6

    .line 2131
    sget-boolean v0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->s:Z

    if-eqz v0, :cond_5

    .line 2132
    const-string v0, "Babel"

    const-string v2, "setAccount not doing anything"

    invoke-static {v0, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    move v2, v1

    .line 2134
    goto :goto_0

    .line 2137
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    if-eqz v0, :cond_19

    .line 2138
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-static {v0, v2}, Lbpx;->a(Lyj;Z)V

    move v0, v1

    .line 2142
    :goto_2
    iput-object v3, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    .line 2143
    sget-boolean v3, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->s:Z

    if-eqz v3, :cond_7

    .line 2144
    const-string v3, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Set account to: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-virtual {v6}, Lyj;->b()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2149
    :cond_7
    if-eqz v0, :cond_8

    iget-object v3, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-virtual {v3}, Lyj;->u()Z

    move-result v3

    if-nez v3, :cond_8

    .line 2150
    invoke-static {p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->h(Lyj;)V

    .line 2152
    :cond_8
    iget-object v3, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-static {v3, v1}, Lbpx;->a(Lyj;Z)V

    .line 2154
    iget-object v3, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-static {v3}, Lbkb;->e(Lyj;)V

    .line 2156
    iget-object v3, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->u:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    if-eqz v3, :cond_9

    .line 2157
    iget-object v3, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->u:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    iget-object v5, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-virtual {v3, v5}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a(Lyj;)V

    .line 2161
    :cond_9
    iget-object v3, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->v:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    if-eqz v3, :cond_a

    .line 2162
    iget-object v3, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->v:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->d()V

    .line 2165
    :cond_a
    iget-object v3, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->w:Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;

    if-eqz v3, :cond_b

    .line 2166
    iget-object v3, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->w:Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->a()V

    .line 2169
    :cond_b
    iget-object v3, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->J:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;

    if-eqz v3, :cond_c

    .line 2170
    iget-object v3, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->J:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;

    iget-object v5, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-virtual {v3, v5}, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;->a(Lyj;)V

    .line 2173
    :cond_c
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->g()Lkd;

    move-result-object v5

    if-eqz v5, :cond_f

    iget-object v3, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-virtual {v3}, Lyj;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_d

    invoke-virtual {v5, v3}, Lkd;->a(Ljava/lang/CharSequence;)V

    :cond_d
    iget-object v3, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-virtual {v3}, Lyj;->O()Z

    move-result v3

    if-eqz v3, :cond_11

    move-object v3, v4

    :goto_3
    invoke-virtual {v5, v3}, Lkd;->b(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-static {}, Lbkb;->p()Lyj;

    move-result-object v6

    invoke-virtual {v3, v6}, Lyj;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    sget v3, Lh;->lw:I

    invoke-virtual {v5, v3}, Lkd;->b(I)V

    invoke-virtual {v5, v4}, Lkd;->b(Ljava/lang/CharSequence;)V

    :cond_e
    iget-object v3, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-virtual {v3}, Lyj;->F()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_18

    new-instance v3, Lzx;

    new-instance v4, Lbyq;

    iget-object v5, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-virtual {v5}, Lyj;->F()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-direct {v4, v5, v6}, Lbyq;-><init>(Ljava/lang/String;Lyj;)V

    invoke-static {}, Lyn;->b()I

    move-result v5

    invoke-virtual {v4, v5}, Lbyq;->a(I)Lbyq;

    move-result-object v4

    invoke-virtual {v4, v1}, Lbyq;->d(Z)Lbyq;

    move-result-object v4

    invoke-virtual {v4, v1}, Lbyq;->b(Z)Lbyq;

    move-result-object v4

    invoke-virtual {v4, v2}, Lbyq;->f(Z)Lbyq;

    move-result-object v4

    new-instance v5, Laxe;

    invoke-direct {v5, p0}, Laxe;-><init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)V

    iget-object v6, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-direct {v3, v4, v5, v1, v6}, Lzx;-><init>(Lbyq;Laac;ZLjava/lang/Object;)V

    invoke-static {}, Lbsn;->c()Lbsn;

    move-result-object v4

    invoke-virtual {v4, v3}, Lbsn;->a(Lbrv;)Z

    move-result v3

    if-eqz v3, :cond_18

    move v3, v2

    :goto_4
    if-eqz v3, :cond_f

    invoke-static {}, Lyn;->c()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->a(Landroid/graphics/Bitmap;)V

    .line 2176
    :cond_f
    iget-object v3, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-static {v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->d(Lyj;)V

    .line 2179
    if-eqz v0, :cond_10

    .line 2180
    invoke-direct {p0, v2}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->d(Z)V

    .line 2181
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->n()V

    .line 2182
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t()V

    :cond_10
    move v2, v1

    .line 2184
    goto/16 :goto_0

    .line 2173
    :cond_11
    iget-object v3, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-virtual {v3}, Lyj;->b()Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    .line 2185
    :cond_12
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->b()Z

    move-result v0

    if-nez v0, :cond_13

    if-nez p1, :cond_13

    if-nez p3, :cond_13

    if-eqz p2, :cond_13

    .line 2191
    const-string v0, "Babel"

    const-string v1, "Starting account picker"

    invoke-static {v0, v1}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2192
    invoke-direct {p0, v4}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->b(Lyj;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lbbl;->b(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->finish()V

    goto/16 :goto_0

    .line 2195
    :cond_13
    invoke-direct {p0, v4}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->b(Lyj;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lbbl;->a(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    if-nez p1, :cond_17

    :goto_5
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_14

    const-string v1, "account_name"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_14
    const-string v1, "try_other_accounts"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "setup_all_accounts"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    if-eqz v1, :cond_15

    const-string v1, "prev_account_name"

    iget-object v3, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-virtual {v3}, Lyj;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_15
    sget-boolean v1, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->s:Z

    if-eqz v1, :cond_16

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "signInAccount account and FINISH: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->f(Ljava/lang/String;)V

    :cond_16
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->finish()V

    goto/16 :goto_0

    :cond_17
    invoke-virtual {p1}, Lyj;->b()Ljava/lang/String;

    move-result-object v4

    goto :goto_5

    :cond_18
    move v3, v1

    goto/16 :goto_4

    :cond_19
    move v0, v2

    goto/16 :goto_2

    :cond_1a
    move-object v3, v0

    goto/16 :goto_1
.end method

.method private b(Lyj;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 2353
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->z:Z

    invoke-static {p1, v0}, Lbbl;->b(Lyj;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->K:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method private b(Lahc;)V
    .locals 3

    .prologue
    .line 2625
    sget-boolean v0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->s:Z

    if-eqz v0, :cond_0

    .line 2626
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "openConversation conversationId: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, Lahc;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Lahc;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->f(Ljava/lang/String;)V

    .line 2630
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    iget-object v1, p1, Lahc;->a:Ljava/lang/String;

    iget v2, p1, Lahc;->d:I

    invoke-static {v0, v1, v2}, Lbbl;->a(Lyj;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "conversation_parameters"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-boolean v1, p1, Lahc;->l:Z

    if-eqz v1, :cond_1

    const-string v1, "conversation_opened_impression"

    const/16 v2, 0x662

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 2631
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "openConversation "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, Lahc;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Ljava/lang/String;)V

    .line 2632
    return-void

    .line 2630
    :cond_1
    const-string v1, "conversation_opened_impression"

    const/16 v2, 0x663

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1372
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1373
    const-string v1, "smsmms"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1375
    sget v2, Lh;->lR:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1376
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 1377
    invoke-interface {v1, v0, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1378
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1379
    return-void
.end method

.method private c(Ljava/lang/String;)Lkh;
    .locals 4

    .prologue
    .line 1626
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->g()Lkd;

    move-result-object v2

    .line 1627
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2}, Lkd;->d()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 1628
    invoke-virtual {v2, v0}, Lkd;->d(I)Lkh;

    move-result-object v1

    .line 1629
    invoke-virtual {v1}, Lkh;->e()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v0, v1

    .line 1633
    :goto_1
    return-object v0

    .line 1627
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1633
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private c(Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 688
    const-string v0, "reset_chat_notifications"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 689
    const/4 v0, 0x1

    .line 691
    :goto_0
    const-string v2, "reset_hangout_notifications"

    invoke-virtual {p1, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 692
    or-int/lit8 v0, v0, 0x2

    .line 694
    :cond_0
    const-string v2, "reset_failed_notifications"

    invoke-virtual {p1, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 695
    or-int/lit8 v0, v0, 0x4

    .line 697
    :cond_1
    if-eqz v0, :cond_2

    .line 701
    const-string v2, "conversation_id"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 702
    const-string v3, "conversation_id_set"

    .line 703
    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 704
    iget-object v4, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    .line 705
    invoke-static {v3, v2}, Lbxz;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 704
    invoke-static {v4, v2, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ljava/lang/String;I)V

    .line 710
    :cond_2
    const-string v0, "reset_signin_failed_notifications"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 711
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-static {v0}, Lbne;->b(Lyj;)V

    .line 713
    :cond_3
    return-void

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public static synthetic c(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)Z
    .locals 1

    .prologue
    .line 152
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->C:Z

    return v0
.end method

.method private d(Z)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1110
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-virtual {v0}, Lyj;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1155
    :cond_0
    :goto_0
    return-void

    .line 1115
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Z)V

    .line 1117
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "connectivity"

    .line 1118
    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 1119
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 1120
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 1123
    :goto_1
    if-eqz v0, :cond_0

    .line 1129
    const-string v0, "babel_request_voice_account_data_delay_ms"

    const-wide/16 v2, 0x7530

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;J)J

    move-result-wide v2

    .line 1132
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 1133
    iget-wide v6, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->Q:J

    add-long/2addr v2, v6

    cmp-long v0, v2, v4

    if-gtz v0, :cond_0

    .line 1137
    iput-wide v4, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->Q:J

    .line 1138
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Laxr;

    invoke-direct {v2, p0, p1}, Laxr;-><init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;Z)V

    invoke-static {v0, p0, v1, v2}, Lf;->a(Ljava/lang/String;Ly;ZLblx;)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1120
    goto :goto_1
.end method

.method private d(Landroid/content/Intent;)Z
    .locals 9

    .prologue
    const/16 v3, 0x66

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 725
    if-eqz p1, :cond_8

    .line 726
    const-string v0, "start_phone_verification"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 727
    const-string v0, "from_settings"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 734
    :goto_0
    iget-boolean v5, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->z:Z

    if-nez v5, :cond_1

    if-nez v2, :cond_1

    .line 735
    const-string v0, "Babel"

    const-string v2, "Babel home not started from main launcher. Skip phone verification."

    invoke-static {v0, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 800
    :cond_0
    :goto_1
    return v1

    .line 742
    :cond_1
    if-eqz v2, :cond_6

    .line 743
    invoke-static {}, Lbmz;->a()Lbmz;

    move-result-object v2

    invoke-virtual {v2}, Lbmz;->h()Z

    move-result v2

    if-nez v2, :cond_0

    .line 747
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 748
    if-eqz v0, :cond_2

    .line 749
    invoke-static {v1}, Lamm;->b(Z)Ljava/lang/String;

    move-result-object v1

    .line 753
    iget-object v5, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-virtual {v5}, Lyj;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 754
    const-string v5, "Babel"

    const-string v6, "Starting phone verification UI from settings."

    invoke-static {v5, v6}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 773
    :goto_2
    if-eqz v0, :cond_5

    const/16 v0, 0x68

    :goto_3
    move-object v8, v2

    move v2, v0

    move-object v0, v8

    .line 794
    :goto_4
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e()Lae;

    move-result-object v3

    invoke-virtual {v3}, Lae;->a()Lao;

    move-result-object v3

    .line 795
    invoke-static {v2, v1, v0}, Lamm;->a(ILjava/lang/String;Ljava/util/ArrayList;)Lamm;

    move-result-object v0

    .line 797
    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->o:Lbme;

    invoke-interface {v1}, Lbme;->a()Laux;

    move-result-object v1

    const/16 v2, 0x63c

    .line 798
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 797
    invoke-virtual {v1, v2}, Laux;->a(Ljava/lang/Integer;)V

    .line 799
    const-string v1, "verification"

    invoke-virtual {v0, v3, v1}, Ls;->a(Lao;Ljava/lang/String;)I

    move v1, v4

    .line 800
    goto :goto_1

    .line 756
    :cond_2
    invoke-static {v4}, Lamm;->b(Z)Ljava/lang/String;

    move-result-object v5

    .line 761
    invoke-static {}, Lbmz;->a()Lbmz;

    move-result-object v6

    invoke-virtual {v6}, Lbmz;->i()Ljava/lang/String;

    move-result-object v6

    .line 762
    invoke-static {v6}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v7

    .line 764
    if-eqz v7, :cond_3

    invoke-static {v7}, Lbkb;->f(Lyj;)I

    move-result v7

    if-eq v7, v3, :cond_4

    .line 766
    :cond_3
    const-string v0, "Babel"

    const-string v2, "Retrying account is not ready. Skip phone verification"

    invoke-static {v0, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 769
    :cond_4
    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 770
    const-string v1, "Babel"

    const-string v6, "Starting phone verification UI from notifcations."

    invoke-static {v1, v6}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v5

    goto :goto_2

    :cond_5
    move v0, v3

    .line 773
    goto :goto_3

    .line 776
    :cond_6
    invoke-static {}, Lbmz;->a()Lbmz;

    move-result-object v0

    invoke-virtual {v0}, Lbmz;->d()I

    move-result v3

    .line 777
    if-eqz v3, :cond_0

    .line 781
    const-string v0, "Babel"

    const-string v2, "Starting phone verification UI from reminders"

    invoke-static {v0, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 782
    invoke-static {v1}, Lamm;->b(Z)Ljava/lang/String;

    move-result-object v2

    .line 787
    invoke-static {v2}, Lamm;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 788
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-nez v5, :cond_7

    .line 789
    const-string v0, "Babel"

    const-string v2, "No accounts ready for phone verification. Skipping."

    invoke-static {v0, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_7
    move-object v1, v2

    move v2, v3

    goto :goto_4

    :cond_8
    move v0, v1

    move v2, v1

    goto/16 :goto_0
.end method

.method public static synthetic d(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)Z
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->C:Z

    return v0
.end method

.method private d(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1642
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->c(Ljava/lang/String;)Lkh;

    move-result-object v0

    .line 1643
    if-eqz v0, :cond_0

    .line 1644
    invoke-virtual {v0}, Lkh;->f()V

    .line 1645
    const/4 v0, 0x1

    .line 1647
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic e(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)Lbme;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->o:Lbme;

    return-object v0
.end method

.method private static e(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2718
    const-string v0, "Babel"

    invoke-static {v0, p0}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2720
    invoke-static {}, Lbys;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2721
    sget-object v0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->r:Ljava/util/LinkedList;

    new-instance v1, Laxv;

    invoke-direct {v1, p0}, Laxv;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 2722
    :goto_0
    sget-object v0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->r:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/16 v1, 0x28

    if-le v0, v1, :cond_0

    .line 2723
    sget-object v0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->r:Ljava/util/LinkedList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 2726
    :cond_0
    return-void
.end method

.method private e(Z)V
    .locals 5

    .prologue
    .line 2259
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-static {v0}, Lf;->d(Lyj;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2260
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->g()Lkd;

    move-result-object v1

    .line 2262
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->B:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 2263
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v2, Lf;->gy:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lkf;

    const/4 v0, -0x2

    const/16 v4, 0x15

    invoke-direct {v3, v0, v4}, Lkf;-><init>(II)V

    sget v0, Lg;->hD:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->B:Landroid/widget/TextView;

    sget v0, Lg;->hC:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    new-instance v4, Laxf;

    invoke-direct {v4, p0}, Laxf;-><init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)V

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1, v2, v3}, Lkd;->a(Landroid/view/View;Lkf;)V

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lkd;->b(Z)V

    .line 2266
    :cond_0
    if-eqz p1, :cond_1

    .line 2267
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lkd;->b(Z)V

    .line 2269
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->B:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-virtual {v1}, Lyj;->ad()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2271
    :cond_2
    return-void
.end method

.method private static e(Landroid/content/Intent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1397
    if-nez p0, :cond_1

    .line 1405
    :cond_0
    :goto_0
    return v0

    .line 1400
    :cond_1
    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 1401
    if-eqz v1, :cond_0

    .line 1404
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1405
    if-eqz v1, :cond_0

    const-string v2, "content://com.google.android.providers.talk"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static synthetic f(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)Lyj;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    return-object v0
.end method

.method private static f(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2802
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[BabelHomeActivity] "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2803
    return-void
.end method

.method private static f(Landroid/content/Intent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1417
    if-nez p0, :cond_1

    .line 1430
    :cond_0
    :goto_0
    return v0

    .line 1420
    :cond_1
    const-string v2, "android.intent.action.RESPOND_VIA_MESSAGE"

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 1421
    goto :goto_0

    .line 1423
    :cond_2
    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 1424
    if-eqz v2, :cond_0

    .line 1427
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1428
    if-eqz v2, :cond_0

    const-string v3, "sms:"

    .line 1429
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "smsto:"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "mms:"

    .line 1430
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "mmsto:"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public static synthetic g(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 152
    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->S:Z

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    const/4 v0, 0x1

    invoke-static {v0}, Lbkb;->f(Z)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    iget v3, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    int-to-long v3, v3

    new-instance v5, Laxl;

    invoke-direct {v5, p0, v3, v4, v0}, Laxl;-><init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;JLyj;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {v5, v0}, Laxl;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "failed to find current version for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method

.method private g(Landroid/content/Intent;)Z
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1434
    new-instance v0, Lyh;

    const-string v1, "sms_body"

    .line 1435
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "subject"

    .line 1436
    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v5, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Lyh;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 1437
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lbvx;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 1438
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1443
    iget-object v2, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    const/4 v7, 0x3

    move-object v4, v3

    move-object v5, v3

    move v6, v9

    invoke-static/range {v2 .. v7}, Lbbl;->a(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;II)Landroid/content/Intent;

    move-result-object v1

    .line 1446
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 1447
    const-string v3, "android.intent.extra.TEXT"

    iget-object v4, v0, Lyh;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1448
    const-string v3, "draft_subject"

    iget-object v0, v0, Lyh;->c:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1449
    const-string v0, "share_intent"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1451
    const/16 v0, 0x3e9

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1452
    sget v0, Lf;->aV:I

    sget v1, Lf;->aW:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->overridePendingTransition(II)V

    .line 1505
    :goto_0
    return v9

    .line 1456
    :cond_0
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 1457
    const-string v1, ";"

    invoke-virtual {v2, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    array-length v7, v6

    move v1, v4

    :goto_1
    if-ge v1, v7, :cond_1

    aget-object v8, v6, v1

    .line 1460
    invoke-static {v8, v3, v3}, Lbdh;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lbdh;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1457
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1463
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->k()Lyj;

    move-result-object v1

    .line 1464
    invoke-static {v5}, Lf;->a(Ljava/util/Collection;)Lxm;

    move-result-object v3

    .line 1462
    invoke-static {v1, v3, v4}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Lxm;Z)I

    move-result v1

    .line 1468
    new-instance v3, Lawd;

    invoke-direct {v3, p0, v1, v2, v0}, Lawd;-><init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;ILjava/lang/String;Lyh;)V

    invoke-static {v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbor;)V

    goto :goto_0
.end method

.method public static synthetic h(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)Z
    .locals 1

    .prologue
    .line 152
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->S:Z

    return v0
.end method

.method public static synthetic i(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)Z
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->S:Z

    return v0
.end method

.method public static synthetic j(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->N:Ljava/lang/Runnable;

    return-object v0
.end method

.method public static synthetic k(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->D:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic l(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)V
    .locals 0

    .prologue
    .line 152
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t()V

    return-void
.end method

.method public static synthetic m(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->J:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;

    return-object v0
.end method

.method public static synthetic n(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)Lbme;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->o:Lbme;

    return-object v0
.end method

.method public static synthetic o()Z
    .locals 1

    .prologue
    .line 152
    sget-boolean v0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->O:Z

    return v0
.end method

.method public static synthetic p()J
    .locals 2

    .prologue
    .line 152
    sget-wide v0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->P:J

    return-wide v0
.end method

.method public static synthetic q()V
    .locals 0

    .prologue
    .line 152
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->s()V

    return-void
.end method

.method public static synthetic r()V
    .locals 1

    .prologue
    .line 152
    const-string v0, "resetToZeroState"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Ljava/lang/String;)V

    return-void
.end method

.method private static s()V
    .locals 3

    .prologue
    .line 1085
    const-string v0, "babel_poll_conversation_data"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->O:Z

    .line 1088
    const-string v0, "babel_poll_conversation_data_frequency_ms"

    const-wide/32 v1, 0xea60

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->P:J

    .line 1091
    return-void
.end method

.method private t()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1171
    sget-boolean v0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->T:Z

    if-nez v0, :cond_0

    sput-boolean v5, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->T:Z

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcyj;->a(Landroid/content/Context;)Lcyj;

    move-result-object v0

    const-class v1, Lbbk;

    const/4 v2, 0x7

    new-array v2, v2, [Lbbk;

    const/4 v3, 0x0

    new-instance v4, Layl;

    invoke-direct {v4}, Layl;-><init>()V

    aput-object v4, v2, v3

    new-instance v3, Layh;

    invoke-direct {v3}, Layh;-><init>()V

    aput-object v3, v2, v5

    const/4 v3, 0x2

    new-instance v4, Lbbn;

    invoke-direct {v4}, Lbbn;-><init>()V

    aput-object v4, v2, v3

    const/4 v3, 0x3

    new-instance v4, Lbbo;

    invoke-direct {v4}, Lbbo;-><init>()V

    aput-object v4, v2, v3

    const/4 v3, 0x4

    new-instance v4, Lbbg;

    invoke-direct {v4}, Lbbg;-><init>()V

    aput-object v4, v2, v3

    const/4 v3, 0x5

    new-instance v4, Lazs;

    invoke-direct {v4}, Lazs;-><init>()V

    aput-object v4, v2, v3

    const/4 v3, 0x6

    new-instance v4, Layi;

    invoke-direct {v4}, Layi;-><init>()V

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcyj;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 1174
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lbbk;

    .line 1173
    invoke-static {v0, v1}, Lcyj;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    .line 1175
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 1176
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbbk;

    .line 1177
    iget-object v2, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-interface {v0, v2, p0}, Lbbk;->a(Lyj;Landroid/app/Activity;)V

    goto :goto_0

    .line 1179
    :cond_1
    return-void
.end method

.method private u()V
    .locals 2

    .prologue
    .line 1329
    sget v0, Lh;->lr:I

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1330
    return-void
.end method

.method private v()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1334
    invoke-static {}, Lbkb;->j()Lyj;

    move-result-object v2

    invoke-static {v2}, Lbkb;->c(Lyj;)Lyj;

    move-result-object v3

    .line 1335
    if-eqz v3, :cond_0

    .line 1336
    invoke-static {v3}, Lbkb;->a(Lyj;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v0

    .line 1337
    :goto_0
    if-eqz v2, :cond_1

    invoke-direct {p0, v3, v1, v1}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->a(Lyj;ZZ)Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    .line 1336
    goto :goto_0

    :cond_1
    move v0, v1

    .line 1337
    goto :goto_1
.end method

.method private w()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1364
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1365
    const-string v1, "smsmms"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1367
    sget v2, Lh;->lR:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1368
    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static x()V
    .locals 2

    .prologue
    .line 1409
    const-string v0, "Babel"

    const-string v1, "Got Talk intent, triggering signout"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 1410
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->h()I

    .line 1411
    return-void
.end method

.method private y()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1652
    const/4 v0, 0x0

    .line 1653
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->g()Lkd;

    move-result-object v1

    .line 1654
    invoke-virtual {v1}, Lkd;->a()I

    move-result v2

    .line 1655
    if-ltz v2, :cond_0

    .line 1656
    invoke-virtual {v1, v2}, Lkd;->d(I)Lkh;

    move-result-object v1

    .line 1657
    if-eqz v1, :cond_0

    .line 1658
    invoke-virtual {v1}, Lkh;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1659
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    invoke-static {v1}, Lcwz;->b(Z)V

    .line 1662
    :cond_0
    return-object v0
.end method

.method private static z()V
    .locals 1

    .prologue
    .line 2307
    const-string v0, "resetToZeroState"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Ljava/lang/String;)V

    .line 2308
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 336
    const/4 v0, 0x1

    return v0
.end method

.method public a(Lahc;)V
    .locals 0

    .prologue
    .line 2621
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->b(Lahc;)V

    .line 2622
    return-void
.end method

.method public a(Lbdk;Ljava/lang/String;IJ)V
    .locals 6

    .prologue
    .line 2666
    sget-boolean v0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->s:Z

    if-eqz v0, :cond_0

    .line 2667
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "openInvite "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->f(Ljava/lang/String;)V

    .line 2670
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    move-object v1, p2

    move-object v2, p1

    move v3, p3

    move-wide v4, p4

    invoke-static/range {v0 .. v5}, Lbbl;->a(Lyj;Ljava/lang/String;Lbdk;IJ)Landroid/content/Intent;

    move-result-object v0

    .line 2677
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 2678
    return-void
.end method

.method public a(Lbkn;)V
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-static {p1, v0, p0, p0}, Lf;->a(Lbkn;Lyj;Ly;Lbkp;)V

    .line 332
    return-void
.end method

.method public a(Ljava/lang/String;ZII)V
    .locals 9

    .prologue
    const/4 v6, 0x1

    .line 2660
    const-string v0, "openHangout"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Ljava/lang/String;)V

    new-instance v0, Lbbw;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    move-object v1, p0

    move-object v3, p1

    move v4, p2

    move v5, p3

    move v7, p4

    move v8, v6

    invoke-direct/range {v0 .. v8}, Lbbw;-><init>(Landroid/app/Activity;Lyj;Ljava/lang/String;ZIZIZ)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lbbw;->executeOnThreadPool([Ljava/lang/Object;)Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;

    .line 2661
    return-void
.end method

.method public a(Lt;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2061
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onAttachFragment "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Ljava/lang/String;)V

    .line 2062
    invoke-super {p0, p1}, Lakn;->a(Lt;)V

    .line 2063
    instance-of v0, p1, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 2064
    check-cast v0, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->u:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    .line 2065
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->u:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a(Lair;)V

    .line 2066
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    if-eqz v0, :cond_0

    .line 2067
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->u:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a(Lyj;)V

    .line 2069
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->u:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a(I)V

    .line 2078
    :cond_1
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "/onAttachFragment "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Ljava/lang/String;)V

    .line 2079
    return-void

    .line 2070
    :cond_2
    instance-of v0, p1, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    if-eqz v0, :cond_3

    move-object v0, p1

    .line 2071
    check-cast v0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->v:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    .line 2072
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->v:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->v:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    new-instance v1, Laxh;

    invoke-direct {v1, p0}, Laxh;-><init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->a(Laod;)V

    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->G:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->v:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->b()V

    iput-boolean v2, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->G:Z

    goto :goto_0

    .line 2073
    :cond_3
    instance-of v0, p1, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 2074
    check-cast v0, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->w:Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;

    .line 2075
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->w:Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->a(Ladi;)V

    goto :goto_0
.end method

.method protected a(Landroid/view/MenuItem;)Z
    .locals 9

    .prologue
    const-wide/16 v5, 0x0

    const/4 v0, 0x1

    .line 1667
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lg;->fY:I

    if-ne v1, v2, :cond_1

    .line 1668
    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->o:Lbme;

    invoke-interface {v1}, Lbme;->a()Laux;

    move-result-object v1

    const/16 v2, 0x64e

    .line 1669
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 1668
    invoke-virtual {v1, v2}, Laux;->a(Ljava/lang/Integer;)V

    .line 1670
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->o_()V

    .line 1672
    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->v:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    if-eqz v1, :cond_0

    .line 1673
    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->v:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->b()V

    .line 1684
    :goto_0
    return v0

    .line 1676
    :cond_0
    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->G:Z

    goto :goto_0

    .line 1679
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lg;->bd:I

    if-ne v0, v1, :cond_3

    .line 1680
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->o:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v1, 0x64f

    .line 1681
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 1680
    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    .line 1682
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/ArrayAdapter;

    const v2, 0x1090003

    invoke-direct {v1, p0, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    new-instance v2, Lawe;

    const-string v3, "Debug Activity"

    invoke-direct {v2, p0, v3}, Lawe;-><init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    new-instance v2, Lawf;

    const-string v3, "Debug Bitmaps Activity"

    invoke-direct {v2, p0, v3}, Lawf;-><init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    new-instance v2, Lawg;

    const-string v3, "Request Warm Sync"

    invoke-direct {v2, p0, v3}, Lawg;-><init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    new-instance v2, Lawh;

    const-string v3, "Tickle GCM"

    invoke-direct {v2, p0, v3}, Lawh;-><init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    new-instance v2, Lawj;

    const-string v3, "Rewind 10 days"

    invoke-direct {v2, p0, v3}, Lawj;-><init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    new-instance v2, Lawl;

    const-string v3, "Refresh from contacts"

    invoke-direct {v2, p0, v3}, Lawl;-><init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    new-instance v2, Lawm;

    const-string v3, "Dump Database"

    invoke-direct {v2, p0, v3}, Lawm;-><init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    new-instance v2, Lawn;

    const-string v3, "Toggle Noise"

    invoke-direct {v2, p0, v3}, Lawn;-><init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    new-instance v2, Lawo;

    const-string v3, "Run DB Cleaner"

    invoke-direct {v2, p0, v3}, Lawo;-><init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-virtual {v2}, Lyj;->x()Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Lawq;

    const-string v3, "Re-import SMS"

    invoke-direct {v2, p0, v3}, Lawq;-><init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    new-instance v2, Lawr;

    const-string v3, "Sync SMS"

    invoke-direct {v2, p0, v3}, Lawr;-><init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    new-instance v2, Laws;

    const-string v3, "Load SMS/MMS from dump file"

    invoke-direct {v2, p0, v3}, Laws;-><init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    new-instance v2, Lawt;

    const-string v3, "Email SMS/MMS dump file"

    invoke-direct {v2, p0, v3}, Lawt;-><init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    new-instance v2, Lawu;

    const-string v3, "Load test APN OTA"

    invoke-direct {v2, p0, v3}, Lawu;-><init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    :cond_2
    new-instance v2, Laww;

    const-string v3, "Activate all Butter Bars"

    invoke-direct {v2, p0, v3}, Laww;-><init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    const-string v2, "babel_rtcs_watchdog_warning"

    invoke-static {v2, v5, v6}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;J)J

    move-result-wide v2

    const-string v4, "babel_rtcs_watchdog_error"

    invoke-static {v4, v5, v6}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;J)J

    move-result-wide v4

    new-instance v6, Lawx;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Test RTCS watchdog (warning "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, p0, v7, v2, v3}, Lawx;-><init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;Ljava/lang/String;J)V

    invoke-virtual {v1, v6}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    new-instance v2, Lawy;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "Test RTCS watchdog (error "

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ")"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p0, v3, v4, v5}, Lawy;-><init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;Ljava/lang/String;J)V

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    new-instance v2, Lawz;

    invoke-direct {v2, p0, v1}, Lawz;-><init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;Landroid/widget/ArrayAdapter;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 1684
    :cond_3
    invoke-super {p0, p1}, Lakn;->a(Landroid/view/MenuItem;)Z

    move-result v0

    goto/16 :goto_0
.end method

.method public a(Lyj;Z)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2790
    invoke-direct {p0, p1, v0, v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->a(Lyj;ZZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2792
    if-eqz p2, :cond_0

    .line 2793
    const-string v0, "resetToZeroState"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Ljava/lang/String;)V

    .line 2795
    :cond_0
    const/4 v0, 0x1

    .line 2798
    :cond_1
    return v0
.end method

.method public a_(Lyj;)V
    .locals 2

    .prologue
    .line 2515
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-virtual {p1, v0}, Lyj;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2518
    invoke-static {p1}, Lbkb;->f(Lyj;)I

    move-result v0

    const/16 v1, 0x66

    if-le v0, v1, :cond_0

    .line 2519
    invoke-static {p1}, Lbkb;->g(Lyj;)V

    .line 2521
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->a(Lyj;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2522
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->o:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v1, 0x619

    .line 2523
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2522
    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    .line 2526
    :cond_1
    return-void
.end method

.method public b(Lbkn;)V
    .locals 2

    .prologue
    .line 2840
    invoke-virtual {p1}, Lbkn;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2850
    const-string v0, "Babel"

    const-string v1, "Unsupported call action type for BabelHomeActivity!"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 2853
    :goto_0
    return-void

    .line 2842
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->B()V

    goto :goto_0

    .line 2846
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-virtual {p1, v0}, Lbkn;->a(Lyj;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->finish()V

    goto :goto_0

    .line 2840
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public b_(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2832
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-static {v0, p1}, Lapk;->a(Lyj;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 2556
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->o:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v1, 0x65c

    .line 2557
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2556
    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    .line 2558
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->A()V

    .line 2559
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 2530
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->o:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v1, 0x641

    .line 2531
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2530
    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    .line 2532
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-static {v0}, Lbbl;->e(Lyj;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 2533
    return-void
.end method

.method public finish()V
    .locals 2

    .prologue
    .line 1184
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lbbk;

    .line 1183
    invoke-static {v0, v1}, Lcyj;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    .line 1185
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbbk;

    .line 1186
    invoke-interface {v0}, Lbbk;->c()V

    goto :goto_0

    .line 1188
    :cond_0
    invoke-super {p0}, Lakn;->finish()V

    .line 1189
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    .line 2571
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->o:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v1, 0x643

    .line 2572
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2571
    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    .line 2573
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-static {v0}, Lbbl;->k(Lyj;)Landroid/content/Intent;

    move-result-object v0

    .line 2574
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 2575
    return-void
.end method

.method public i()V
    .locals 2

    .prologue
    .line 2579
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->o:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v1, 0x645

    .line 2580
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2579
    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    .line 2581
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-static {v0}, Lbbl;->m(Lyj;)Landroid/content/Intent;

    move-result-object v0

    .line 2582
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 2583
    return-void
.end method

.method public j()V
    .locals 2

    .prologue
    .line 2587
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->o:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v1, 0x438

    .line 2588
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2587
    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    .line 2589
    invoke-static {p0}, Lbbl;->a(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    .line 2590
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 2591
    return-void
.end method

.method public k()Lyj;
    .locals 1

    .prologue
    .line 2509
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    return-object v0
.end method

.method public n()V
    .locals 4

    .prologue
    .line 1594
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->L:Laxw;

    invoke-virtual {v0}, Laxw;->e()V

    .line 1596
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->g()Lkd;

    move-result-object v0

    .line 1597
    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-static {p0, v1}, Lf;->a(Landroid/content/Context;Lyj;)Z

    move-result v1

    .line 1598
    const-string v2, "phone_calls"

    invoke-direct {p0, v2}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->c(Ljava/lang/String;)Lkh;

    move-result-object v2

    .line 1599
    if-nez v2, :cond_1

    if-eqz v1, :cond_1

    .line 1601
    new-instance v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1602
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lh;->iu:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1603
    sget v2, Lcom/google/android/apps/hangouts/R$drawable;->bi:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1604
    invoke-virtual {v0}, Lkd;->c()Lkh;

    move-result-object v2

    .line 1605
    invoke-virtual {v2, v1}, Lkh;->a(Landroid/view/View;)Lkh;

    move-result-object v1

    sget v2, Lh;->iu:I

    .line 1606
    invoke-virtual {v1, v2}, Lkh;->a(I)Lkh;

    move-result-object v1

    const-string v2, "phone_calls"

    .line 1607
    invoke-virtual {v1, v2}, Lkh;->a(Ljava/lang/Object;)Lkh;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->R:Lki;

    .line 1608
    invoke-virtual {v1, v2}, Lkh;->a(Lki;)Lkh;

    move-result-object v1

    .line 1609
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lkd;->a(Lkh;Z)V

    .line 1610
    const-string v0, "Babel"

    const-string v1, "Added phone calls tab"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1616
    :cond_0
    :goto_0
    return-void

    .line 1611
    :cond_1
    if-nez v1, :cond_0

    if-eqz v2, :cond_0

    .line 1613
    invoke-virtual {v0, v2}, Lkd;->a(Lkh;)V

    .line 1614
    const-string v0, "Babel"

    const-string v1, "Removed phone calls tab"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public o_()V
    .locals 1

    .prologue
    .line 1979
    const-string v0, "people"

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->d(Ljava/lang/String;)Z

    .line 1980
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->u:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    if-eqz v0, :cond_0

    .line 1981
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->u:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->d()V

    .line 1983
    :cond_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 2595
    invoke-super {p0, p1, p2, p3}, Lakn;->onActivityResult(IILandroid/content/Intent;)V

    .line 2596
    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_2

    .line 2597
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->d()Lcom/google/android/apps/hangouts/phone/EsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2598
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->d()Lcom/google/android/apps/hangouts/phone/EsApplication;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->c(Z)V

    .line 2599
    invoke-static {}, Lbkb;->j()Lyj;

    move-result-object v0

    .line 2600
    if-nez v0, :cond_1

    .line 2601
    const-string v0, "Babel"

    const-string v1, "User signed out last account, exit babel."

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2602
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->finish()V

    .line 2617
    :cond_0
    :goto_0
    return-void

    .line 2606
    :cond_1
    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-virtual {v2}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2607
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->a(Lyj;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2608
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->o:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v1, 0x61a

    .line 2609
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2608
    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    goto :goto_0

    .line 2613
    :cond_2
    const/16 v0, 0x3e9

    if-ne p1, v0, :cond_0

    .line 2615
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->finish()V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 2743
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2744
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->c(Z)V

    .line 2774
    :cond_0
    :goto_0
    return-void

    .line 2748
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->C()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2749
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->D()V

    goto :goto_0

    .line 2756
    :cond_2
    const-string v0, "onBackPressed"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Ljava/lang/String;)V

    .line 2757
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->v:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->v:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2758
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->v:Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2762
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->w:Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->w:Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2763
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->w:Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/CallContactPickerFragment;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2769
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->u:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->u:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2773
    :cond_5
    invoke-super {p0}, Lakn;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 2778
    invoke-super {p0, p1}, Lakn;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2780
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->u_()V

    .line 2781
    return-void
.end method

.method public onConnected(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2809
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2810
    const-string v0, "Babel"

    const-string v1, "People client connected but home activity is finishing."

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2820
    :cond_0
    return-void

    .line 2815
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2817
    const/4 v0, 0x1

    invoke-static {v0}, Lbkb;->f(Z)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2818
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public onConnectionFailed(Lcft;)V
    .locals 0

    .prologue
    .line 2828
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 345
    :try_start_0
    const-string v3, "BabelHomeActivity.onCreate"

    invoke-static {v3}, Lbzq;->a(Ljava/lang/String;)V

    .line 348
    const-string v3, "onCreate"

    invoke-static {v3}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Ljava/lang/String;)V

    .line 349
    sget-boolean v3, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->s:Z

    if-eqz v3, :cond_0

    .line 350
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onCreate  savedInstanceState: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->f(Ljava/lang/String;)V

    .line 353
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->E:Landroid/os/Bundle;

    .line 354
    invoke-super {p0, p1}, Lakn;->onCreate(Landroid/os/Bundle;)V

    .line 356
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 357
    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string v5, "android.intent.action.MAIN"

    invoke-static {v3, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->z:Z

    .line 358
    const-string v3, "BabelHomeActivity.onCreate"

    invoke-static {v3, v4}, Lbxm;->a(Ljava/lang/String;Landroid/content/Intent;)V

    .line 360
    invoke-static {v4}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->f(Landroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->v()Z

    move-result v3

    if-nez v3, :cond_1

    .line 361
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->u()V

    .line 362
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->finish()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 545
    const-string v0, "/onCreate"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Ljava/lang/String;)V

    .line 546
    invoke-static {}, Lbzq;->a()V

    .line 547
    :goto_0
    return-void

    .line 368
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->isTaskRoot()Z

    move-result v3

    if-nez v3, :cond_2

    .line 369
    const-string v3, "Babel"

    const-string v5, "BabelHomeActivity was not the root task in onCreate"

    invoke-static {v3, v5}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    const-string v3, "android.intent.category.LAUNCHER"

    invoke-virtual {v4, v3}, Landroid/content/Intent;->hasCategory(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-boolean v3, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->z:Z

    if-eqz v3, :cond_2

    .line 371
    const-string v0, "Babel"

    const-string v1, "Finishing instead of re-launching from the launcher"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->finish()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 545
    const-string v0, "/onCreate"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Ljava/lang/String;)V

    .line 546
    invoke-static {}, Lbzq;->a()V

    goto :goto_0

    .line 377
    :cond_2
    :try_start_2
    invoke-static {}, Lbxm;->c()Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->y:Z

    .line 379
    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-static {v3, v5, v6}, Lf;->a(ZZZ)I

    move-result v3

    if-eqz v3, :cond_3

    .line 383
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->z:Z

    invoke-static {v0}, Lbbl;->a(Z)Landroid/content/Intent;

    move-result-object v0

    .line 384
    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 385
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 386
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->finish()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 545
    const-string v0, "/onCreate"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Ljava/lang/String;)V

    .line 546
    invoke-static {}, Lbzq;->a()V

    goto :goto_0

    .line 392
    :cond_3
    :try_start_3
    new-instance v3, Laxb;

    invoke-direct {v3, p0}, Laxb;-><init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)V

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Void;

    .line 399
    invoke-virtual {v3, v5}, Laxb;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 401
    invoke-static {v4}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Landroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 402
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->x()V

    .line 406
    :cond_4
    iget-boolean v3, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->z:Z

    if-eqz v3, :cond_5

    invoke-static {}, Lcom/google/android/apps/hangouts/fragments/SmsOobActivity;->a()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 407
    invoke-static {}, Lbbl;->c()Landroid/content/Intent;

    move-result-object v0

    .line 408
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 409
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->finish()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 545
    const-string v0, "/onCreate"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Ljava/lang/String;)V

    .line 546
    invoke-static {}, Lbzq;->a()V

    goto/16 :goto_0

    .line 417
    :cond_5
    :try_start_4
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->d()Lcom/google/android/apps/hangouts/phone/EsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/hangouts/phone/EsApplication;->p()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 418
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->d()Lcom/google/android/apps/hangouts/phone/EsApplication;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lcom/google/android/apps/hangouts/phone/EsApplication;->c(Z)V

    .line 419
    invoke-static {}, Lbkb;->j()Lyj;

    move-result-object v3

    .line 420
    if-nez v3, :cond_7

    .line 421
    const-string v0, "Babel"

    const-string v1, "User signed out last account, exit babel."

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->finish()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 545
    const-string v0, "/onCreate"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Ljava/lang/String;)V

    .line 546
    invoke-static {}, Lbzq;->a()V

    goto/16 :goto_0

    .line 427
    :cond_6
    if-eqz p1, :cond_9

    .line 428
    :try_start_5
    const-string v3, "key_account_name"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 432
    :goto_1
    const-string v5, "*"

    invoke-static {v3, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_a

    move v0, v2

    move-object v3, v1

    .line 459
    :cond_7
    :goto_2
    sget-boolean v5, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->s:Z

    if-eqz v5, :cond_8

    .line 460
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "onCreate account: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->f(Ljava/lang/String;)V

    .line 469
    :cond_8
    invoke-direct {p0, v3, v2, v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->a(Lyj;ZZ)Z

    move-result v0

    if-nez v0, :cond_d

    .line 470
    const-string v0, "Babel"

    const-string v1, "[BabelHomeActivity.onCreate] Account is not ready"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 545
    const-string v0, "/onCreate"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Ljava/lang/String;)V

    .line 546
    invoke-static {}, Lbzq;->a()V

    goto/16 :goto_0

    .line 430
    :cond_9
    :try_start_6
    const-string v3, "account_name"

    invoke-virtual {v4, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 435
    :cond_a
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_15

    .line 436
    invoke-static {v3}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v3

    .line 437
    if-eqz v3, :cond_b

    invoke-virtual {v3}, Lyj;->u()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 438
    invoke-static {}, Lbkb;->m()Z

    move-result v5

    if-nez v5, :cond_b

    move-object v3, v1

    .line 444
    :cond_b
    if-eqz v3, :cond_c

    move v2, v0

    .line 452
    :cond_c
    :goto_3
    if-nez v3, :cond_7

    .line 453
    const-string v3, "Babel"

    const-string v5, "account not found in intent, try getActiveAccount()"

    invoke-static {v3, v5}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 454
    invoke-static {}, Lbkb;->j()Lyj;

    move-result-object v3

    goto :goto_2

    .line 474
    :cond_d
    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v2, "com.google.android.apps.hangouts.phone.recentcalls"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 475
    invoke-static {}, Lbkb;->z()Z

    move-result v0

    if-nez v0, :cond_e

    .line 477
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->c()V

    .line 478
    const-string v0, "Babel"

    const-string v1, "[BabelHomeActivity.onCreate] Adding account from dialer intent."

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 545
    const-string v0, "/onCreate"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Ljava/lang/String;)V

    .line 546
    invoke-static {}, Lbzq;->a()V

    goto/16 :goto_0

    .line 480
    :cond_e
    :try_start_7
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-static {p0, v0}, Lf;->a(Landroid/content/Context;Lyj;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 481
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lh;->dm:I

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x104000a

    new-instance v3, Laxi;

    invoke-direct {v3, p0}, Laxi;-><init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 485
    :cond_f
    sget v0, Lf;->ei:I

    sget v2, Lg;->u:I

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->a(II)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->setContentView(Landroid/view/View;)V

    .line 488
    new-instance v0, Laxw;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e()Lae;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Laxw;-><init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;Lae;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->L:Laxw;

    .line 489
    sget v0, Lg;->ic:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->K:Landroid/support/v4/view/ViewPager;

    .line 490
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->K:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->L:Laxw;

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->a(Lgq;)V

    .line 491
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->K:Landroid/support/v4/view/ViewPager;

    new-instance v2, Laxj;

    invoke-direct {v2, p0}, Laxj;-><init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)V

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->a(Lhz;)V

    .line 500
    if-nez p1, :cond_14

    .line 501
    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v2, "com.google.android.apps.hangouts.phone.conversationlist"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 502
    const-string v0, "conversations"

    .line 511
    :goto_4
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->g()Lkd;

    move-result-object v1

    new-instance v2, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lh;->iq:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    sget v3, Lcom/google/android/apps/hangouts/R$drawable;->cd:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v1}, Lkd;->c()Lkh;

    move-result-object v3

    invoke-virtual {v3, v2}, Lkh;->a(Landroid/view/View;)Lkh;

    move-result-object v2

    sget v3, Lh;->iq:I

    invoke-virtual {v2, v3}, Lkh;->a(I)Lkh;

    move-result-object v2

    const-string v3, "people"

    invoke-virtual {v2, v3}, Lkh;->a(Ljava/lang/Object;)Lkh;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->R:Lki;

    invoke-virtual {v2, v3}, Lkh;->a(Lki;)Lkh;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lkd;->a(Lkh;Z)V

    new-instance v2, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lh;->br:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    sget v3, Lcom/google/android/apps/hangouts/R$drawable;->be:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v1}, Lkd;->c()Lkh;

    move-result-object v3

    invoke-virtual {v3, v2}, Lkh;->a(Landroid/view/View;)Lkh;

    move-result-object v2

    sget v3, Lh;->br:I

    invoke-virtual {v2, v3}, Lkh;->a(I)Lkh;

    move-result-object v2

    const-string v3, "conversations"

    invoke-virtual {v2, v3}, Lkh;->a(Ljava/lang/Object;)Lkh;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->R:Lki;

    invoke-virtual {v2, v3}, Lkh;->a(Lki;)Lkh;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lkd;->a(Lkh;Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->n()V

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_10

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v2, "last_selected_tab"

    const-string v3, "conversations"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_10
    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_11

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lkd;->d(I)Lkh;

    move-result-object v0

    invoke-virtual {v1, v0}, Lkd;->b(Lkh;)V

    :cond_11
    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Lkd;->c(I)V

    .line 514
    const-string v0, "Babel"

    const-string v1, "[BabelHomeActivity.onCreate] setContentView called"

    invoke-static {v0, v1}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 516
    sget v0, Lg;->bC:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->H:Landroid/support/v4/widget/DrawerLayout;

    .line 517
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e()Lae;

    move-result-object v0

    sget v1, Lg;->eJ:I

    .line 518
    invoke-virtual {v0, v1}, Lae;->a(I)Lt;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->I:Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;

    .line 520
    sget v0, Lg;->Z:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->J:Lcom/google/android/apps/hangouts/fragments/ButterBarContainer;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 545
    const-string v0, "/onCreate"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Ljava/lang/String;)V

    .line 546
    invoke-static {}, Lbzq;->a()V

    goto/16 :goto_0

    .line 503
    :cond_12
    :try_start_8
    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v2, "com.google.android.apps.hangouts.phone.recentcalls"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 504
    const-string v0, "use_dialer_activity"

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 505
    invoke-static {}, Lbbl;->n()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 506
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->finish()V

    .line 508
    :cond_13
    const-string v0, "phone_calls"
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_4

    .line 545
    :catchall_0
    move-exception v0

    const-string v1, "/onCreate"

    invoke-static {v1}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Ljava/lang/String;)V

    .line 546
    invoke-static {}, Lbzq;->a()V

    throw v0

    :cond_14
    move-object v0, v1

    goto/16 :goto_4

    :cond_15
    move-object v3, v1

    goto/16 :goto_3
.end method

.method public onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1987
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    .line 1988
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1989
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    .line 1990
    sget v3, Lf;->eR:I

    invoke-virtual {v2, v3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 1992
    sget v0, Lg;->aV:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 1994
    sget v3, Lh;->jv:I

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 1995
    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    sget v3, Lh;->jw:I

    .line 1996
    invoke-virtual {p0, v3}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Laxc;

    invoke-direct {v4, p0, v0}, Laxc;-><init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;Landroid/widget/EditText;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    sget v3, Lh;->ju:I

    .line 2008
    invoke-virtual {p0, v3}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Laxa;

    invoke-direct {v4, p0}, Laxa;-><init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2017
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 2018
    new-instance v2, Laxd;

    invoke-direct {v2, p0, v1}, Laxd;-><init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;Landroid/app/AlertDialog;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2037
    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    move-object v0, v1

    .line 2042
    :cond_0
    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 1510
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 1511
    sget v1, Lf;->gZ:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1512
    sget v0, Lg;->fY:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->A:Landroid/view/MenuItem;

    .line 1513
    invoke-super {p0, p1}, Lakn;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 966
    const-string v0, "onDestroy"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Ljava/lang/String;)V

    .line 967
    invoke-super {p0}, Lakn;->onDestroy()V

    .line 968
    const-string v0, "/onDestroy"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Ljava/lang/String;)V

    .line 969
    return-void
.end method

.method public onDisconnected()V
    .locals 0

    .prologue
    .line 2824
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 6

    .prologue
    .line 1212
    const-string v0, "onNewIntent"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Ljava/lang/String;)V

    .line 1214
    :try_start_0
    invoke-super {p0, p1}, Lakn;->onNewIntent(Landroid/content/Intent;)V

    .line 1215
    const-string v0, "BabelHomeActivity.onNewIntent"

    invoke-static {v0, p1}, Lbxm;->a(Ljava/lang/String;Landroid/content/Intent;)V

    .line 1217
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->F:Z

    if-nez v0, :cond_0

    .line 1226
    const-string v0, "Babel"

    const-string v1, "onNewIntent before mOnActivityAndFragmentCreateCalled!!!!!"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1324
    const-string v0, "/onNewIntent"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Ljava/lang/String;)V

    .line 1325
    :goto_0
    return-void

    .line 1230
    :cond_0
    :try_start_1
    invoke-static {p1}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1231
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->x()V

    .line 1234
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 1235
    const-string v0, "android.intent.action.MAIN"

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->z:Z

    .line 1236
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->d(Landroid/content/Intent;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_2

    .line 1324
    const-string v0, "/onNewIntent"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 1240
    :cond_2
    :try_start_2
    const-string v0, "conversation_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1242
    invoke-static {p1}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->f(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1243
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->v()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1244
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->g(Landroid/content/Intent;)Z

    .line 1245
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->x:Z

    .line 1253
    :cond_3
    const-string v0, "account_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1255
    sget-boolean v3, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->s:Z

    if-eqz v3, :cond_4

    .line 1256
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onNewIntent account: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->f(Ljava/lang/String;)V

    .line 1259
    :cond_4
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    .line 1260
    if-nez v0, :cond_5

    .line 1262
    iget-boolean v3, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->z:Z

    if-eqz v3, :cond_7

    .line 1265
    const-string v3, "Babel"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1266
    const-string v3, "Babel"

    const-string v4, "Intent account is null"

    invoke-static {v3, v4}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1274
    :cond_5
    :goto_1
    if-eqz v0, :cond_a

    .line 1275
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {p0, v0, v3, v4}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->a(Lyj;ZZ)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1276
    const-string v0, "Babel"

    const-string v1, "onNewIntent setAccount failed!!!!"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1324
    const-string v0, "/onNewIntent"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1247
    :cond_6
    :try_start_3
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->u()V

    .line 1248
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->finish()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1324
    const-string v0, "/onNewIntent"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1270
    :cond_7
    :try_start_4
    const-string v0, "Babel"

    const-string v3, "account not found in intent, try getActiveAccount()"

    invoke-static {v0, v3}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1271
    invoke-static {}, Lbkb;->j()Lyj;

    move-result-object v0

    goto :goto_1

    .line 1279
    :cond_8
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->c(Landroid/content/Intent;)V

    .line 1280
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 1281
    const-string v0, "conversation_type"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 1283
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lbbl;->a(Landroid/os/Bundle;)Lbdk;

    move-result-object v1

    .line 1284
    if-eqz v1, :cond_b

    .line 1285
    const-string v0, "invite_timestamp"

    const-wide/16 v4, 0x0

    .line 1286
    invoke-virtual {p1, v0, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    move-object v0, p0

    .line 1285
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->a(Lbdk;Ljava/lang/String;IJ)V

    .line 1321
    :cond_9
    :goto_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->x:Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1324
    :cond_a
    const-string v0, "/onNewIntent"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1288
    :cond_b
    :try_start_5
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->u:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->u:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    .line 1289
    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->c(Ljava/lang/String;)Lyh;

    move-result-object v0

    .line 1290
    :goto_3
    new-instance v1, Lahc;

    iget-object v4, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-direct {v1, v2, v4, v3}, Lahc;-><init>(Ljava/lang/String;Lyj;I)V

    .line 1295
    const/4 v2, 0x1

    iput-boolean v2, v1, Lahc;->b:Z

    .line 1296
    iput-object v0, v1, Lahc;->e:Lyh;

    .line 1298
    invoke-direct {p0, v1}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->b(Lahc;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 1324
    :catchall_0
    move-exception v0

    const-string v1, "/onNewIntent"

    invoke-static {v1}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Ljava/lang/String;)V

    throw v0

    .line 1289
    :cond_c
    const/4 v0, 0x0

    goto :goto_3

    .line 1300
    :cond_d
    :try_start_6
    const-string v0, "com.google.android.apps.hangouts.phone.conversationlist"

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1301
    const-string v0, "conversations"

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->d(Ljava/lang/String;)Z

    goto :goto_2

    .line 1302
    :cond_e
    const-string v0, "com.google.android.apps.hangouts.phone.recentcalls"

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1303
    const-string v0, "use_dialer_activity"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1304
    invoke-static {}, Lbbl;->n()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 1305
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->finish()V

    goto :goto_2

    .line 1307
    :cond_f
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->B()V

    .line 1308
    invoke-static {}, Lbkb;->z()Z

    move-result v0

    if-nez v0, :cond_9

    .line 1310
    const-string v0, "Babel"

    const-string v1, "[BabelHomeActivity.onNewIntent] Adding account from dialer intent."

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1311
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->c()V

    goto :goto_2

    .line 1314
    :cond_10
    const-string v0, "android.intent.action.INSERT"

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1315
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->o_()V

    goto :goto_2

    .line 1316
    :cond_11
    const-string v0, "com.google.android.apps.hangouts.phone.addgoogleaccount"

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 1317
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->c()V

    goto/16 :goto_2

    .line 1319
    :cond_12
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->z()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_2
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 1193
    const-string v0, "onPause"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Ljava/lang/String;)V

    .line 1196
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->y()Ljava/lang/String;

    move-result-object v0

    .line 1197
    if-eqz v0, :cond_0

    .line 1198
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1199
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "last_selected_tab"

    .line 1200
    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1201
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1204
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->N:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->D:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->N:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->N:Ljava/lang/Runnable;

    .line 1206
    :cond_1
    invoke-super {p0}, Lakn;->onPause()V

    .line 1207
    const-string v0, "/onPause"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Ljava/lang/String;)V

    .line 1208
    return-void
.end method

.method public onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2047
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 2048
    sget v0, Lg;->aV:I

    invoke-virtual {p2, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 2050
    const-string v1, "conversation_name"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2051
    const/4 v1, 0x0

    .line 2052
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    .line 2051
    invoke-virtual {v0, v1, v2}, Landroid/widget/EditText;->setSelection(II)V

    .line 2053
    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 2054
    const-string v1, "conversation_id"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2055
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTag(Ljava/lang/Object;)V

    .line 2057
    :cond_0
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1518
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->C()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1519
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 1545
    :goto_0
    return v0

    .line 1524
    :cond_0
    sget v1, Lg;->dq:I

    invoke-interface {p1, v1}, Landroid/view/Menu;->removeItem(I)V

    .line 1527
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->y()Ljava/lang/String;

    move-result-object v1

    .line 1529
    if-eqz v1, :cond_1

    .line 1530
    iget-object v2, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->A:Landroid/view/MenuItem;

    const-string v3, "conversations"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1532
    const-string v2, "phone_calls"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1533
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->d(Z)V

    .line 1539
    :cond_1
    :goto_1
    sget v0, Lg;->bd:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 1540
    if-eqz v0, :cond_2

    .line 1541
    invoke-static {}, Lf;->w()Z

    move-result v1

    .line 1542
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1545
    :cond_2
    invoke-super {p0, p1}, Lakn;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0

    .line 1535
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->g()Lkd;

    move-result-object v1

    invoke-virtual {v1, v0}, Lkd;->b(Z)V

    goto :goto_1
.end method

.method public onResume()V
    .locals 7

    .prologue
    .line 985
    const-string v0, "BabelHomeActivity.onResume"

    invoke-static {v0}, Lbzq;->a(Ljava/lang/String;)V

    .line 986
    const-string v0, "onResume"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Ljava/lang/String;)V

    .line 988
    :try_start_0
    invoke-super {p0}, Lakn;->onResume()V

    .line 990
    sget-boolean v0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->s:Z

    if-eqz v0, :cond_0

    .line 991
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onResume mAccount: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->f(Ljava/lang/String;)V

    .line 996
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    if-eqz v0, :cond_2

    .line 997
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-static {v0}, Lbkb;->l(Lyj;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 998
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    .line 999
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->z()V

    .line 1001
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-virtual {v0}, Lyj;->u()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1002
    invoke-static {}, Lbkb;->m()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1005
    invoke-static {}, Lbkb;->n()Lyj;

    move-result-object v0

    .line 1006
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lyj;->u()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1007
    invoke-static {v0}, Lbkb;->l(Lyj;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1012
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->a(Lyj;ZZ)Z

    .line 1015
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->z()V

    .line 1027
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    .line 1029
    :goto_1
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->a(Lyj;ZZ)Z

    move-result v0

    .line 1031
    invoke-static {}, Lbkb;->m()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-static {}, Lbkb;->n()Lyj;

    move-result-object v1

    invoke-static {v1}, Lbwf;->b(Lyj;)V

    invoke-virtual {v1}, Lyj;->u()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {v1}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->w()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {v1}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lh;->lq:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    invoke-virtual {v1}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->b(Ljava/lang/String;)V

    .line 1032
    :cond_3
    :goto_2
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->d(Z)V

    .line 1035
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->n()V

    .line 1038
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t()V

    .line 1040
    if-nez v0, :cond_4

    .line 1041
    const-string v0, "Babel"

    const-string v1, "[BabelHomeActivity.OnResume] Account is not ready."

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1044
    :cond_4
    new-instance v0, Laxo;

    invoke-direct {v0, p0}, Laxo;-><init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->N:Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->D:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->N:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1046
    const-string v0, "/onResume"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Ljava/lang/String;)V

    .line 1047
    invoke-static {}, Lbzq;->a()V

    .line 1048
    return-void

    .line 1023
    :cond_5
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1046
    :catchall_0
    move-exception v0

    const-string v1, "/onResume"

    invoke-static {v1}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Ljava/lang/String;)V

    .line 1047
    invoke-static {}, Lbzq;->a()V

    throw v0

    .line 1028
    :cond_6
    :try_start_2
    invoke-static {}, Lbkb;->j()Lyj;

    move-result-object v0

    goto/16 :goto_1

    .line 1031
    :cond_7
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->w()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lh;->lp:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->b(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 954
    invoke-super {p0, p1}, Lakn;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 956
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    if-nez v0, :cond_0

    .line 962
    :goto_0
    return-void

    .line 961
    :cond_0
    const-string v0, "key_account_name"

    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-virtual {v1}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onStart()V
    .locals 11

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const/4 v10, 0x1

    const/4 v6, 0x0

    .line 805
    const-string v0, "BabelHomeActivity.onStart"

    invoke-static {v0}, Lbzq;->a(Ljava/lang/String;)V

    .line 806
    const-string v0, "onStart"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Ljava/lang/String;)V

    .line 809
    invoke-super {p0}, Lakn;->onStart()V

    .line 810
    sget-boolean v0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->s:Z

    if-eqz v0, :cond_0

    .line 811
    const-string v0, "onStart"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->f(Ljava/lang/String;)V

    .line 815
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 822
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->F:Z

    if-nez v0, :cond_3

    .line 823
    iget-object v7, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->E:Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    sget-boolean v0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->s:Z

    if-eqz v0, :cond_1

    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "openInitialFragment savedInstanceState:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mStateSetByNewIntent:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->x:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->x:Z

    if-nez v0, :cond_2

    if-eqz v7, :cond_14

    const-string v0, "key_account_name"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-virtual {v1}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_14

    move-object v0, v6

    :goto_0
    if-nez v0, :cond_c

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->f(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->v()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->g(Landroid/content/Intent;)Z

    :cond_2
    :goto_1
    invoke-static {}, Lbkb;->e()V

    if-eqz v7, :cond_13

    invoke-direct {p0, v6}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->d(Landroid/content/Intent;)Z

    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->D:Landroid/os/Handler;

    new-instance v1, Laxk;

    invoke-direct {v1, p0, v7, v8}, Laxk;-><init>(Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;Landroid/os/Bundle;Landroid/content/Intent;)V

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 824
    iput-boolean v10, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->F:Z

    .line 825
    iput-object v6, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->E:Landroid/os/Bundle;

    .line 828
    :cond_3
    const-string v0, "/onStart"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Ljava/lang/String;)V

    .line 829
    invoke-static {}, Lbzq;->a()V

    .line 830
    return-void

    .line 823
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->finish()V

    goto :goto_1

    :cond_5
    const-string v1, "conversation_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    const-string v1, "Babel"

    const-string v2, "openInitialFragment resetToZeroState"

    invoke-static {v1, v2}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.action.INSERT"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->o_()V

    goto :goto_1

    :cond_6
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.google.android.apps.hangouts.phone.recentcalls"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "use_dialer_activity"

    invoke-virtual {v0, v1, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-static {}, Lbbl;->n()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->finish()V

    goto :goto_1

    :cond_7
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->B()V

    goto :goto_1

    :cond_8
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->z()V

    goto :goto_1

    :cond_9
    const-string v1, "conversation_type"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v1}, Lbbl;->a(Landroid/os/Bundle;)Lbdk;

    move-result-object v1

    if-eqz v1, :cond_a

    const-string v9, "invite_timestamp"

    invoke-virtual {v0, v9, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->a(Lbdk;Ljava/lang/String;IJ)V

    goto/16 :goto_1

    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->u:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->u:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->c(Ljava/lang/String;)Lyh;

    move-result-object v0

    :goto_3
    new-instance v1, Lahc;

    iget-object v4, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-direct {v1, v2, v4, v3}, Lahc;-><init>(Ljava/lang/String;Lyj;I)V

    iput-boolean v10, v1, Lahc;->b:Z

    iput-object v0, v1, Lahc;->e:Lyh;

    invoke-direct {p0, v1}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->b(Lahc;)V

    goto/16 :goto_1

    :cond_b
    move-object v0, v6

    goto :goto_3

    :cond_c
    const-string v1, "key_home_conv_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_f

    const-string v1, "key_home_conv_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "key_home_conv_type_id"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->u:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->u:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->c(Ljava/lang/String;)Lyh;

    move-result-object v0

    :goto_4
    sget-boolean v3, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->s:Z

    if-eqz v3, :cond_d

    const-string v3, "Babel"

    const-string v4, "openInitialFragment openConversation from savedInstanceState"

    invoke-static {v3, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_d
    new-instance v3, Lahc;

    iget-object v4, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-direct {v3, v1, v4, v2}, Lahc;-><init>(Ljava/lang/String;Lyj;I)V

    iput-boolean v10, v3, Lahc;->b:Z

    iput-object v0, v3, Lahc;->e:Lyh;

    invoke-direct {p0, v3}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->b(Lahc;)V

    goto/16 :goto_1

    :cond_e
    move-object v0, v6

    goto :goto_4

    :cond_f
    const-string v1, "key_home_invite_conv_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_11

    const-string v1, "key_home_inviter_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    if-eqz v1, :cond_11

    const-string v1, "key_home_invite_conv_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v1, "key_home_inviter_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lbdk;

    const-string v3, "key_home_conv_type_id"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    sget-boolean v0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->s:Z

    if-eqz v0, :cond_10

    const-string v0, "Babel"

    const-string v9, "openInitialFragment openInvite from savedInstanceState"

    invoke-static {v0, v9}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_10
    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->a(Lbdk;Ljava/lang/String;IJ)V

    goto/16 :goto_1

    :cond_11
    sget-boolean v0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->s:Z

    if-eqz v0, :cond_12

    const-string v0, "Babel"

    const-string v1, "openInitialFragment didn\'t open anything from saveInstanceState - going to zero state"

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_12
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->z()V

    goto/16 :goto_1

    :cond_13
    invoke-direct {p0, v8}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->d(Landroid/content/Intent;)Z

    goto/16 :goto_2

    :cond_14
    move-object v0, v7

    goto/16 :goto_0
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 973
    const-string v0, "onStop"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Ljava/lang/String;)V

    .line 974
    invoke-super {p0}, Lakn;->onStop()V

    .line 976
    const-string v0, "/onStop"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->e(Ljava/lang/String;)V

    .line 977
    return-void
.end method

.method public q_()V
    .locals 2

    .prologue
    .line 2537
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->o:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v1, 0x642

    .line 2538
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2537
    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    .line 2539
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    invoke-static {v0}, Lbbl;->d(Lyj;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 2540
    return-void
.end method

.method public r_()V
    .locals 3

    .prologue
    .line 2544
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->o:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v1, 0x644

    .line 2545
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2544
    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    .line 2546
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    if-eqz v0, :cond_0

    .line 2547
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->d()Lcom/google/android/apps/hangouts/phone/EsApplication;

    move-result-object v0

    .line 2548
    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->t:Lyj;

    const/4 v2, 0x0

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Landroid/app/Activity;Lyj;Ljava/lang/Runnable;)V

    .line 2552
    :goto_0
    return-void

    .line 2550
    :cond_0
    const-string v0, "Babel"

    const-string v1, "Unexpected null account in dnd_item"

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public s_()V
    .locals 2

    .prologue
    .line 2563
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->o:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v1, 0x646

    .line 2564
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2563
    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    .line 2565
    invoke-static {}, Lbbl;->e()Landroid/content/Intent;

    move-result-object v0

    .line 2566
    const/16 v1, 0x3e8

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/hangouts/phone/BabelHomeActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2567
    return-void
.end method
