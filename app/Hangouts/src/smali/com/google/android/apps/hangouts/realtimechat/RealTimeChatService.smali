.class public Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;
.super Landroid/app/IntentService;
.source "PG"


# static fields
.field static a:Lbnk;

.field private static final b:Z

.field private static c:Z

.field private static final d:I

.field private static volatile e:Lbop;

.field private static volatile f:Lboq;

.field private static final g:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static final h:Ljava/lang/Object;

.field private static i:Ljava/lang/String;

.field private static j:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final k:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lbiq;",
            ">;"
        }
    .end annotation
.end field

.field private static l:I

.field private static final m:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final n:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lbmu;",
            ">;"
        }
    .end annotation
.end field

.field private static final o:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Queue",
            "<",
            "Landroid/content/Intent;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final p:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lbor;",
            ">;"
        }
    .end annotation
.end field

.field private static final q:Ljava/lang/Object;

.field private static r:Landroid/os/PowerManager$WakeLock;

.field private static t:Lcxa;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcxa",
            "<",
            "Lboo;",
            ">;"
        }
    .end annotation
.end field

.field private static u:Z

.field private static final v:Ljava/lang/Object;


# instance fields
.field private s:Landroid/os/Handler;

.field private w:Lbqy;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 183
    sget-object v0, Lbys;->k:Lcyp;

    sput-boolean v1, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    .line 186
    sput-boolean v1, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c:Z

    .line 188
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    sput v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->d:I

    .line 554
    sput-object v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e:Lbop;

    .line 556
    sput-object v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f:Lboq;

    .line 557
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 562
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->h:Ljava/lang/Object;

    .line 566
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->k:Landroid/util/SparseArray;

    .line 579
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    sput-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->m:Ljava/util/Map;

    .line 583
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    sput-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->n:Ljava/util/Queue;

    .line 586
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    sput-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->o:Ljava/util/Map;

    .line 591
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    sput-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->p:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 597
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->q:Ljava/lang/Object;

    .line 604
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->v:Ljava/lang/Object;

    .line 627
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->s()V

    .line 630
    new-instance v0, Lbnn;

    invoke-direct {v0}, Lbnn;-><init>()V

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/Runnable;)V

    .line 1006
    sput-object v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 639
    const-string v0, "RealTimeChatService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 640
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 643
    invoke-direct {p0, p1}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 644
    return-void
.end method

.method public static a(Ljava/lang/String;ILbkn;)I
    .locals 2

    .prologue
    .line 3016
    const/16 v0, 0x99

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 3017
    const-string v1, "recent_call_type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3018
    const-string v1, "recent_call_action_info"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 3019
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;I)I
    .locals 2

    .prologue
    .line 2955
    const/16 v0, 0x66

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Ljava/lang/String;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2956
    const-string v1, "otr_status"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2957
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Ljava/lang/String;Lyj;JJ)I
    .locals 2

    .prologue
    .line 1944
    const/16 v0, 0x35

    invoke-static {p1, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 1946
    const-string v1, "timestamp"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1947
    const-string v1, "gcm_handle_timestamps"

    invoke-virtual {v0, v1, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1948
    const-string v1, "payload"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1949
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Ljava/util/List;Lyj;)I
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbiq;",
            ">;",
            "Lyj;",
            ")I"
        }
    .end annotation

    .prologue
    .line 1954
    const-string v1, ""

    .line 1955
    sget-object v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->k:Landroid/util/SparseArray;

    monitor-enter v2

    .line 1956
    :try_start_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    .line 1957
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    .line 1958
    sget v4, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->l:I

    add-int/lit8 v5, v4, 0x1

    sput v5, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->l:I

    .line 1959
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1960
    add-int/lit8 v5, v3, -0x1

    if-ge v0, v5, :cond_0

    .line 1961
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "|"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1963
    :cond_0
    sget-object v5, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->k:Landroid/util/SparseArray;

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1957
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1965
    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1967
    const/16 v0, 0x47

    invoke-static {p1, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 1968
    const-string v2, "simulated_event_msg_num"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1969
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0

    .line 1965
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public static a(Lyj;I)I
    .locals 3

    .prologue
    .line 1777
    const/16 v0, 0x8e

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 1778
    const-string v1, "hangout_type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1779
    const-string v1, "hangout_topic"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1780
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Lyj;IZ)I
    .locals 2

    .prologue
    .line 1201
    const/16 v0, 0x13

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 1202
    const-string v1, "setselfinfo_bit"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1203
    const-string v1, "setselfinfo_bit_value"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1204
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Lyj;Ldxv;Ldzg;)I
    .locals 3

    .prologue
    .line 1319
    const/16 v0, 0xb7

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->d(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "broadcast"

    .line 1320
    invoke-static {p1}, Ldxv;->toByteArray(Lepr;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "sync_metadata"

    .line 1321
    invoke-static {p2}, Ldzg;->toByteArray(Lepr;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    move-result-object v0

    .line 1322
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Lyj;Ldzk;)I
    .locals 3

    .prologue
    .line 2782
    const/16 v0, 0x44

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 2783
    const-string v1, "hangout_invite_receipt"

    .line 2784
    invoke-static {p1}, Ldzk;->toByteArray(Lepr;)[B

    move-result-object v2

    .line 2783
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 2785
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Lyj;Ljava/lang/String;J)I
    .locals 2

    .prologue
    .line 1640
    const/16 v0, 0x5b

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1641
    const-string v1, "timestamp"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1642
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Lyj;Ljava/lang/String;JZ)I
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1623
    new-array v0, v1, [Ljava/lang/String;

    aput-object p1, v0, v2

    new-array v1, v1, [J

    aput-wide p2, v1, v2

    invoke-static {p0, v0, v1, p4, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;[Ljava/lang/String;[JZZ)I

    move-result v0

    return v0
.end method

.method public static a(Lyj;Ljava/lang/String;Ldzj;)I
    .locals 3

    .prologue
    .line 2827
    const/16 v0, 0x4e

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 2828
    const-string v1, "hangout_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2829
    const-string v1, "call_log_data"

    invoke-static {p2}, Ldzj;->toByteArray(Lepr;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 2830
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Lyj;Ljava/lang/String;Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 1303
    const/16 v0, 0xb6

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->d(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "hangout_id"

    .line 1304
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "broadcast_id"

    .line 1305
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1306
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Lyj;Ljava/lang/String;Ljava/lang/String;I)I
    .locals 2

    .prologue
    .line 2095
    const/16 v0, 0x2f

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2096
    const-string v1, "message_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2097
    const-string v1, "error"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2098
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 1279
    new-instance v0, Ldxv;

    invoke-direct {v0}, Ldxv;-><init>()V

    .line 1280
    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Ldxv;->n:Ljava/lang/Integer;

    .line 1281
    const-string v1, "HANGOUT_ABUSE_REPORT_RECORDING"

    iput-object v1, v0, Ldxv;->d:Ljava/lang/String;

    .line 1282
    iput-object p1, v0, Ldxv;->b:Ljava/lang/String;

    .line 1283
    iput-object p2, v0, Ldxv;->g:Ljava/lang/String;

    .line 1285
    new-instance v1, Ldya;

    invoke-direct {v1}, Ldya;-><init>()V

    iput-object v1, v0, Ldxv;->j:Ldya;

    .line 1286
    iget-object v1, v0, Ldxv;->j:Ldya;

    iput-object p3, v1, Ldya;->d:Ljava/lang/String;

    .line 1288
    new-instance v1, Ldxw;

    invoke-direct {v1}, Ldxw;-><init>()V

    .line 1289
    iget-object v2, v0, Ldxv;->j:Ldya;

    iput-object v1, v2, Ldya;->g:Ldxw;

    .line 1290
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Ldxw;->b:Ljava/lang/Integer;

    .line 1291
    iput-object p3, v1, Ldxw;->c:Ljava/lang/String;

    .line 1292
    const/16 v1, 0xb5

    invoke-static {p0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->d(Lyj;I)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "broadcast"

    invoke-static {v0}, Ldxv;->toByteArray(Lepr;)[B

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I
    .locals 2

    .prologue
    .line 1853
    const/16 v0, 0x82

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1854
    const-string v1, "subject"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1855
    const-string v1, "uri"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1856
    const-string v1, "draft_attachment_count"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1858
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IILjava/lang/String;Ljava/lang/String;ZLcom/google/android/gms/maps/model/MarkerOptions;Lcom/google/android/gms/maps/model/CameraPosition;I)I
    .locals 20
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1817
    invoke-virtual/range {p0 .. p0}, Lyj;->a()V

    .line 1818
    const/16 v2, 0x1f

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v18

    .line 1819
    const-string v2, "message_text"

    move-object/from16 v0, v18

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1820
    const-string v2, "uri"

    move-object/from16 v0, v18

    move-object/from16 v1, p3

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1821
    const-string v2, "rotation"

    move-object/from16 v0, v18

    move/from16 v1, p4

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1822
    const-string v2, "picasa_photo_id"

    move-object/from16 v0, v18

    move-object/from16 v1, p5

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1823
    const-string v2, "width"

    move-object/from16 v0, v18

    move/from16 v1, p6

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1824
    const-string v2, "height"

    move-object/from16 v0, v18

    move/from16 v1, p7

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1825
    const-string v2, "content_type"

    move-object/from16 v0, v18

    move-object/from16 v1, p8

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1826
    const-string v2, "subject"

    move-object/from16 v0, v18

    move-object/from16 v1, p9

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1827
    const-string v2, "requires_mms"

    move-object/from16 v0, v18

    move/from16 v1, p10

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1828
    const-string v2, "marker_options"

    move-object/from16 v0, v18

    move-object/from16 v1, p11

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1829
    const-string v2, "camera_position"

    move-object/from16 v0, v18

    move-object/from16 v1, p12

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1830
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    .line 1831
    const-string v4, "timestamp"

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1832
    const-string v2, "otr_state"

    move-object/from16 v0, v18

    move/from16 v1, p13

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1834
    invoke-static {}, Lf;->w()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1835
    if-eqz p2, :cond_0

    const-string v2, "///"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "///"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1836
    const/4 v2, 0x3

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x3

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    const-string v2, "\\."

    invoke-virtual {v6, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    :try_start_0
    array-length v3, v2

    if-lez v3, :cond_1

    const/4 v3, 0x0

    aget-object v3, v2, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    :goto_0
    array-length v4, v2

    const/4 v5, 0x1

    if-le v4, v5, :cond_2

    const/4 v4, 0x1

    aget-object v2, v2, v4

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v16

    :goto_1
    new-instance v19, Ljava/lang/Thread;

    new-instance v2, Lboi;

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v7, p3

    move-object/from16 v8, p5

    move/from16 v9, p6

    move/from16 v10, p7

    move-object/from16 v11, p8

    move-object/from16 v12, p9

    move/from16 v13, p10

    move-object/from16 v14, p11

    move-object/from16 v15, p12

    invoke-direct/range {v2 .. v17}, Lboi;-><init>(ILyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;ZLcom/google/android/gms/maps/model/MarkerOptions;Lcom/google/android/gms/maps/model/CameraPosition;J)V

    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1841
    :cond_0
    :goto_2
    invoke-static/range {v18 .. v18}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v2

    return v2

    .line 1836
    :cond_1
    const/16 v3, 0xa

    goto :goto_0

    :cond_2
    const-wide/16 v16, 0x1f4

    goto :goto_1

    :catch_0
    move-exception v2

    goto :goto_2
.end method

.method public static a(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 2878
    const/16 v0, 0x5a

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 2879
    const-string v1, "chat_acl_key"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2880
    const-string v1, "chat_acl_circle_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2881
    const-string v1, "chat_acl_circle_name"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2882
    const-string v1, "chat_acl_level"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2883
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)I
    .locals 2

    .prologue
    .line 2892
    const/16 v0, 0x5c

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 2893
    const-string v1, "member_gaiaid"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2894
    const-string v1, "phone_number"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2895
    const-string v1, "user_name"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2896
    const-string v1, "blocked"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2897
    const-string v1, "retry_request"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2898
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Lyj;Ljava/lang/String;Lxm;)I
    .locals 2

    .prologue
    .line 2126
    const/16 v0, 0x20

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2127
    const-string v1, "audience"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2128
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Lyj;Ljava/lang/String;Z)I
    .locals 2

    .prologue
    .line 2110
    const/16 v0, 0x54

    .line 2111
    invoke-static {p0, v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2112
    const-string v1, "insert_error_message"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2113
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Lyj;Ljava/lang/String;ZZI)I
    .locals 2

    .prologue
    .line 2057
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v0, :cond_0

    .line 2058
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "RTCService.sendPresenceRequest: convId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", focus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    .line 2061
    :cond_0
    const/16 v0, 0x30

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2062
    const-string v1, "is_present"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2063
    const-string v1, "reciprocate"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2064
    const-string v1, "timeout_secs"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2065
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Lyj;Ljava/lang/String;ZZZ)I
    .locals 2

    .prologue
    .line 2675
    const/16 v0, 0x29

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2676
    const-string v1, "accept"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2677
    const-string v1, "block_inviter"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2678
    const-string v1, "report_inviter"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2679
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Lyj;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 1647
    const/16 v0, 0x90

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1648
    const-string v1, "event_ids"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 1649
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Lyj;Ljava/util/ArrayList;Ljava/lang/String;Z)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lyj;",
            "Ljava/util/ArrayList",
            "<",
            "Lbcn;",
            ">;",
            "Ljava/lang/String;",
            "Z)I"
        }
    .end annotation

    .prologue
    .line 2218
    const/16 v0, 0x3b

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 2219
    const-string v1, "batch_gebi_tag"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2220
    const-string v1, "from_contact_lookup"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2221
    const-string v1, "com.google.android.apps.hangouts.EntityLookupSpecs"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2222
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Lyj;Ljava/util/ArrayList;Z)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lyj;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;Z)I"
        }
    .end annotation

    .prologue
    .line 2734
    const/16 v0, 0x39

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 2735
    const-string v1, "gaia_id_list"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2736
    const-string v1, "include_rich_status"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2737
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Lyj;Lxm;Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 1757
    const/16 v0, 0xb9

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 1758
    const-string v1, "audience"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1759
    const-string v1, "original_conversation_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1760
    const-string v1, "omit_conversation_lookup"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1761
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Lyj;Lxm;Z)I
    .locals 1

    .prologue
    .line 1697
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Lxm;ZZ)I

    move-result v0

    return v0
.end method

.method public static a(Lyj;Lxm;ZZ)I
    .locals 1

    .prologue
    .line 1712
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Lxm;ZZI)I

    move-result v0

    return v0
.end method

.method public static a(Lyj;Lxm;ZZI)I
    .locals 3

    .prologue
    .line 1733
    const/16 v0, 0x1e

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 1734
    const-string v1, "audience"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1735
    const-string v1, "omit_conversation_lookup"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1736
    const-string v1, "force_group"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1737
    const-string v1, "transport_type"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1738
    if-eqz p3, :cond_0

    .line 1739
    const-string v1, "conversation_hangout"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1741
    :cond_0
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Lyj;[BZ)I
    .locals 2

    .prologue
    .line 2916
    const/16 v0, 0xb0

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 2917
    const-string v1, "pdu"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 2918
    const-string v1, "is_sms_read"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2919
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Lyj;[Ljava/lang/String;[JZZ)I
    .locals 2

    .prologue
    .line 1630
    const/16 v0, 0x48

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 1631
    const-string v1, "conversationids"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 1632
    const-string v1, "timestamps"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 1633
    const-string v1, "archive"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1634
    const-string v1, "perform_locally"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1635
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static a(Lyt;Ljava/lang/String;J)I
    .locals 2

    .prologue
    .line 6243
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v0

    const/16 v1, 0x6f

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 6244
    const-string v1, "conversation_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6245
    const-string v1, "extra_pending_conversation_operations"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 6246
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method private static a(Ljava/lang/String;I)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 659
    invoke-static {p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(I)Landroid/content/Intent;

    move-result-object v0

    .line 660
    const-string v1, "account_name"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 661
    return-object v0
.end method

.method private static a(Ljava/lang/String;ILjava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 666
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 667
    const-string v1, "op"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 668
    const-string v1, "account_name"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 669
    const-string v1, "conversation_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 670
    return-object v0
.end method

.method private static a(Lyj;ILjava/lang/String;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 679
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Ljava/lang/String;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 4884
    sget-boolean v1, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c:Z

    if-eqz v1, :cond_0

    if-nez p3, :cond_0

    .line 4887
    const-string v1, "Babel"

    const-string v2, "executeOperation called with no operation"

    invoke-static {v1, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 4903
    :goto_0
    return-object v0

    .line 4890
    :cond_0
    invoke-virtual {p3}, Lbnj;->a()V

    .line 4891
    new-instance v2, Lbos;

    .line 4892
    invoke-virtual {p3}, Lbnj;->b()I

    move-result v1

    invoke-direct {v2, v5, v1, v0}, Lbos;-><init>(IILbfz;)V

    .line 4893
    invoke-virtual {p3}, Lbnj;->c()Ljava/lang/Object;

    move-result-object v1

    .line 4895
    invoke-direct {p0, p2, v2, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Landroid/content/Intent;Lbos;Ljava/lang/Object;)V

    .line 4897
    const-string v0, "rid"

    invoke-virtual {p2, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p3, p1, v0}, Lbnj;->a(Lbki;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4900
    :goto_1
    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->n:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmu;

    if-eqz v0, :cond_2

    iget-object v2, v0, Lbmu;->b:Lyj;

    invoke-static {v2}, Lbkb;->o(Lyj;)Lbki;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Account is not valid. Skip parasite operation:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lbmu;->b:Lyj;

    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Lbmu;->a(I)V

    invoke-virtual {v0}, Lbmu;->a()V

    invoke-virtual {v0, v2, v5}, Lbmu;->a(Lbki;I)Z

    goto :goto_1

    :cond_2
    invoke-static {}, Lbpf;->k()V

    :cond_3
    move-object v0, v1

    .line 4903
    goto :goto_0
.end method

.method public static a(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 5871
    packed-switch p0, :pswitch_data_0

    .line 6139
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "(unknown opcode "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 5873
    :pswitch_1
    const-string v0, "OP_REGISTER_ACCOUNT"

    goto :goto_0

    .line 5875
    :pswitch_2
    const-string v0, "OP_UNREGISTER_ACCOUNT"

    goto :goto_0

    .line 5877
    :pswitch_3
    const-string v0, "OP_ACCOUNT_READY"

    goto :goto_0

    .line 5879
    :pswitch_4
    const-string v0, "OP_ACCOUNT_REMOVED"

    goto :goto_0

    .line 5881
    :pswitch_5
    const-string v0, "OP_CLEANUP_INVALID_ACCOUNTS"

    goto :goto_0

    .line 5883
    :pswitch_6
    const-string v0, "OP_GET_SELF_INFO"

    goto :goto_0

    .line 5885
    :pswitch_7
    const-string v0, "OP_SET_SELF_INFO_BIT"

    goto :goto_0

    .line 5887
    :pswitch_8
    const-string v0, "OP_START_CONVERSATION"

    goto :goto_0

    .line 5889
    :pswitch_9
    const-string v0, "OP_SEND_MESSAGE"

    goto :goto_0

    .line 5891
    :pswitch_a
    const-string v0, "OP_INVITE_PARTICIPANTS"

    goto :goto_0

    .line 5893
    :pswitch_b
    const-string v0, "OP_LEAVE_CONVERSATION"

    goto :goto_0

    .line 5895
    :pswitch_c
    const-string v0, "OP_UPDATE_CONVERSATION_WATERMARK"

    goto :goto_0

    .line 5897
    :pswitch_d
    const-string v0, "OP_REMOVE_MESSAGE"

    goto :goto_0

    .line 5899
    :pswitch_e
    const-string v0, "OP_SET_CONVERSATION_NAME"

    goto :goto_0

    .line 5901
    :pswitch_f
    const-string v0, "OP_SET_CONVERSATION_NOTIFICATION_LEVEL"

    goto :goto_0

    .line 5903
    :pswitch_10
    const-string v0, "OP_RECEIVE_RESPONSE"

    goto :goto_0

    .line 5905
    :pswitch_11
    const-string v0, "OP_RESET_NOTIFICATIONS"

    goto :goto_0

    .line 5907
    :pswitch_12
    const-string v0, "OP_REPLY_TO_INVITATION"

    goto :goto_0

    .line 5909
    :pswitch_13
    const-string v0, "OP_REQUEST_MORE_EVENTS"

    goto :goto_0

    .line 5911
    :pswitch_14
    const-string v0, "OP_RETRY_SEND_MESSAGE"

    goto :goto_0

    .line 5913
    :pswitch_15
    const-string v0, "OP_SET_MESSAGE_FAILED"

    goto :goto_0

    .line 5915
    :pswitch_16
    const-string v0, "OP_SEND_PRESENCE_REQUEST"

    goto :goto_0

    .line 5917
    :pswitch_17
    const-string v0, "OP_SEND_TYPING_REQUEST"

    goto :goto_0

    .line 5919
    :pswitch_18
    const-string v0, "OP_SEND_TILE_EVENT"

    goto :goto_0

    .line 5921
    :pswitch_19
    const-string v0, "OP_REQUEST_SUGGESTED_CONTACTS"

    goto :goto_0

    .line 5923
    :pswitch_1a
    const-string v0, "OP_RECEIVE_SERVER_UPDATE"

    goto :goto_0

    .line 5925
    :pswitch_1b
    const-string v0, "OP_REQUEST_MORE_CONVERSATIONS"

    goto :goto_0

    .line 5927
    :pswitch_1c
    const-string v0, "OP_REQUEST_WARM_SYNC"

    goto :goto_0

    .line 5929
    :pswitch_1d
    const-string v0, "OP_SYNC_BASELINE_SUGGESTED_CONTACTS"

    goto :goto_0

    .line 5931
    :pswitch_1e
    const-string v0, "OP_QUERY_AVAILABILITY"

    goto :goto_0

    .line 5933
    :pswitch_1f
    const-string v0, "OP_REQUEST_SEARCH_CONTACTS"

    goto :goto_0

    .line 5935
    :pswitch_20
    const-string v0, "OP_REQUEST_GET_CONTACT_BY_ID"

    goto :goto_0

    .line 5937
    :pswitch_21
    const-string v0, "OP_SET_ACTIVE_CLIENT"

    goto :goto_0

    .line 5939
    :pswitch_22
    const-string v0, "OP_MODIFY_OTR_STATUS"

    goto :goto_0

    .line 5941
    :pswitch_23
    const-string v0, "OP_REQUEST_CONVERSATION_META_DATA"

    goto :goto_0

    .line 5943
    :pswitch_24
    const-string v0, "OP_SET_DND_SETTING"

    goto :goto_0

    .line 5945
    :pswitch_25
    const-string v0, "OP_SET_HANGOUT_NOTIFICATION_STATUS"

    goto :goto_0

    .line 5947
    :pswitch_26
    const-string v0, "OP_GET_PROFILE"

    goto :goto_0

    .line 5949
    :pswitch_27
    const-string v0, "OP_RECEIVE_SIMULATED_EVENT"

    goto :goto_0

    .line 5951
    :pswitch_28
    const-string v0, "OP_ARCHIVE_CONVERSATIONS"

    goto :goto_0

    .line 5953
    :pswitch_29
    const-string v0, "OP_REQUEST_HANGOUT_INFO"

    goto :goto_0

    .line 5955
    :pswitch_2a
    const-string v0, "OP_LOCALE_CHANGED"

    goto :goto_0

    .line 5957
    :pswitch_2b
    const-string v0, "OP_EXPIRE_LAST_MESSAGE"

    goto/16 :goto_0

    .line 5959
    :pswitch_2c
    const-string v0, "OP_REPORT_CALL_PERF_STATS"

    goto/16 :goto_0

    .line 5961
    :pswitch_2d
    const-string v0, "OP_REQUEST_HANGOUT_PARTICIPANTS"

    goto/16 :goto_0

    .line 5963
    :pswitch_2e
    const-string v0, "OP_DELETE_MESSAGE"

    goto/16 :goto_0

    .line 5965
    :pswitch_2f
    const-string v0, "OP_UPDATE_CONVERSATION_SCROLL_TIME"

    goto/16 :goto_0

    .line 5967
    :pswitch_30
    const-string v0, "OP_UPDATE_MESSAGE_SCROLL_TIME"

    goto/16 :goto_0

    .line 5969
    :pswitch_31
    const-string v0, "OP_RETRY_CREATE_CONVERSATION"

    goto/16 :goto_0

    .line 5971
    :pswitch_32
    const-string v0, "OP_SET_CONVERSATION_CREATE_FAILED"

    goto/16 :goto_0

    .line 5973
    :pswitch_33
    const-string v0, "OP_START_PHONE_VERIFICATION"

    goto/16 :goto_0

    .line 5975
    :pswitch_34
    const-string v0, "OP_FINISH_PHONE_VERIFICATION"

    goto/16 :goto_0

    .line 5977
    :pswitch_35
    const-string v0, "OP_GET_CHAT_ACL_SETTINGS"

    goto/16 :goto_0

    .line 5979
    :pswitch_36
    const-string v0, "OP_SET_CHAT_ACL_SETTING"

    goto/16 :goto_0

    .line 5981
    :pswitch_37
    const-string v0, "OP_DELETE_CONVERSATION"

    goto/16 :goto_0

    .line 5983
    :pswitch_38
    const-string v0, "OP_SET_USER_BLOCK"

    goto/16 :goto_0

    .line 5985
    :pswitch_39
    const-string v0, "OP_UPDATE_NOTIFICATIONS"

    goto/16 :goto_0

    .line 5987
    :pswitch_3a
    const-string v0, "OP_CLEANUP_DB"

    goto/16 :goto_0

    .line 5989
    :pswitch_3b
    const-string v0, "OP_SET_LAST_INVITE_SEEN_TIMESTAMP"

    goto/16 :goto_0

    .line 5991
    :pswitch_3c
    const-string v0, "OP_LOAD_BLOCKED_PEOPLE"

    goto/16 :goto_0

    .line 5993
    :pswitch_3d
    const-string v0, "OP_CLEAR_ALERTED_MESSAGES"

    goto/16 :goto_0

    .line 5995
    :pswitch_3e
    const-string v0, "OP_REFRESH_PARTICIPANTS_INFO"

    goto/16 :goto_0

    .line 5997
    :pswitch_3f
    const-string v0, "OP_SEND_OFFNETWORK_INVITATION"

    goto/16 :goto_0

    .line 5999
    :pswitch_40
    const-string v0, "OP_HANDLE_PACKAGE_REPLACED"

    goto/16 :goto_0

    .line 6001
    :pswitch_41
    const-string v0, "OP_REVERT_CONVERSATION_NAME"

    goto/16 :goto_0

    .line 6003
    :pswitch_42
    const-string v0, "OP_REVERT_OTR_STATUS"

    goto/16 :goto_0

    .line 6005
    :pswitch_43
    const-string v0, "OP_SET_CONVERSATION_INVITE_FAILURE"

    goto/16 :goto_0

    .line 6007
    :pswitch_44
    const-string v0, "OP_UNREGISTER_REMOVED_ACCOUNTS"

    goto/16 :goto_0

    .line 6009
    :pswitch_45
    const-string v0, "OP_TIMEOUT_PHONE_VERIFICATION"

    goto/16 :goto_0

    .line 6011
    :pswitch_46
    const-string v0, "OP_RETRY_GCM_REGISTRATION"

    goto/16 :goto_0

    .line 6013
    :pswitch_47
    const-string v0, "OP_LOGOUT_TALK"

    goto/16 :goto_0

    .line 6015
    :pswitch_48
    const-string v0, "OP_RECEIVE_SMS_MESSAGE"

    goto/16 :goto_0

    .line 6017
    :pswitch_49
    const-string v0, "OP_SEND_PENDING_CONVERSATION_OPERATIONS"

    goto/16 :goto_0

    .line 6019
    :pswitch_4a
    const-string v0, "OP_RECEIVE_MMS_MESSAGE"

    goto/16 :goto_0

    .line 6021
    :pswitch_4b
    const-string v0, "OP_COMPLETE_CANCEL_SEND_MESSAGE"

    goto/16 :goto_0

    .line 6023
    :pswitch_4c
    const-string v0, "OP_PATCH_AFTER_REQUEST_WRITER_UPGRADE"

    goto/16 :goto_0

    .line 6025
    :pswitch_4d
    const-string v0, "OP_RECEIVE_MMS_WAP_PUSH"

    goto/16 :goto_0

    .line 6027
    :pswitch_4e
    const-string v0, "OP_RETRIEVE_MMS_MESSAGE"

    goto/16 :goto_0

    .line 6029
    :pswitch_4f
    const-string v0, "OP_RESTART_PURGED_CONVERSATION"

    goto/16 :goto_0

    .line 6031
    :pswitch_50
    const-string v0, "OP_SET_IN_CALL_SETTING"

    goto/16 :goto_0

    .line 6033
    :pswitch_51
    const-string v0, "OP_SET_MOOD_SETTING"

    goto/16 :goto_0

    .line 6035
    :pswitch_52
    const-string v0, "OP_DISMISS_SUGGESTED_CONTACT"

    goto/16 :goto_0

    .line 6037
    :pswitch_53
    const-string v0, "OP_SET_RICH_PRESENCE_ENABLED_STATE"

    goto/16 :goto_0

    .line 6039
    :pswitch_54
    const-string v0, "OP_UPDATE_INCALL_STATE"

    goto/16 :goto_0

    .line 6041
    :pswitch_55
    const-string v0, "OP_INSERT_PARTICIPANT_ENTITY"

    goto/16 :goto_0

    .line 6043
    :pswitch_56
    const-string v0, "OP_SET_BULK_RICH_PRESENCE_ENABLED_STATE"

    goto/16 :goto_0

    .line 6045
    :pswitch_57
    const-string v0, "OP_RECEIVE_SMS_DELIVERY_REPORT"

    goto/16 :goto_0

    .line 6047
    :pswitch_58
    const-string v0, "OP_FORWARD_MMS_MESSAGE"

    goto/16 :goto_0

    .line 6049
    :pswitch_59
    const-string v0, "OP_HANDLE_STORAGE_LOW_SMS"

    goto/16 :goto_0

    .line 6051
    :pswitch_5a
    const-string v0, "OP_HANDLE_STORAGE_OK_SMS"

    goto/16 :goto_0

    .line 6053
    :pswitch_5b
    const-string v0, "OP_FREE_SMS_STORAGE"

    goto/16 :goto_0

    .line 6055
    :pswitch_5c
    const-string v0, "OP_UPLOAD_ANALYTICS"

    goto/16 :goto_0

    .line 6057
    :pswitch_5d
    const-string v0, "OP_GET_VIDEO_DATA"

    goto/16 :goto_0

    .line 6059
    :pswitch_5e
    const-string v0, "OP_REMOVE_CONVERSATION_IF_EMPTY"

    goto/16 :goto_0

    .line 6061
    :pswitch_5f
    const-string v0, "OP_SYNC_SMS_MESSAGES"

    goto/16 :goto_0

    .line 6063
    :pswitch_60
    const-string v0, "OP_TRIGGER_DEFERRED_NOTIFICATION"

    goto/16 :goto_0

    .line 6065
    :pswitch_61
    const-string v0, "OP_REVIVE_MMS_NOTIFICATION"

    goto/16 :goto_0

    .line 6067
    :pswitch_62
    const-string v0, "OP_RENEW_ACCOUNT_REGISTRATION"

    goto/16 :goto_0

    .line 6069
    :pswitch_63
    const-string v0, "OP_CREATE_HANGOUT_ID"

    goto/16 :goto_0

    .line 6071
    :pswitch_64
    const-string v0, "OP_LEAVE_CONTINGENCY_FAILED"

    goto/16 :goto_0

    .line 6073
    :pswitch_65
    const-string v0, "OP_DELETE_CONVERSATION_FAILED"

    goto/16 :goto_0

    .line 6075
    :pswitch_66
    const-string v0, "OP_UPLOAD_VIDEO_CALL_LOGS"

    goto/16 :goto_0

    .line 6077
    :pswitch_67
    const-string v0, "OP_RECEIVE_SMSMMS_FROM_DUMP_FILE"

    goto/16 :goto_0

    .line 6079
    :pswitch_68
    const-string v0, "OP_SEND_EASTER_EGG"

    goto/16 :goto_0

    .line 6081
    :pswitch_69
    const-string v0, "OP_GET_VOICE_ACCOUNT_INFO"

    goto/16 :goto_0

    .line 6083
    :pswitch_6a
    const-string v0, "OP_CLEANUP_EVENT_SUGGESTIONS"

    goto/16 :goto_0

    .line 6085
    :pswitch_6b
    const-string v0, "OP_ENABLE_VOICE_CALLING"

    goto/16 :goto_0

    .line 6087
    :pswitch_6c
    const-string v0, "OP_GET_CALL_RATE"

    goto/16 :goto_0

    .line 6089
    :pswitch_6d
    const-string v0, "OP_SEND_STRESS_TEST_MESSAGE"

    goto/16 :goto_0

    .line 6091
    :pswitch_6e
    const-string v0, "OP_ADD_RECENT_PSTN_CALL"

    goto/16 :goto_0

    .line 6093
    :pswitch_6f
    const-string v0, "OP_GET_PHONE_LIST"

    goto/16 :goto_0

    .line 6095
    :pswitch_70
    const-string v0, "OP_ANALYTICS_UPLOAD_FAILED"

    goto/16 :goto_0

    .line 6097
    :pswitch_71
    const-string v0, "OP_CLEAR_CONTINUATION_TOKEN"

    goto/16 :goto_0

    .line 6099
    :pswitch_72
    const-string v0, "OP_POLL_PARASITE_OPERATIONS"

    goto/16 :goto_0

    .line 6101
    :pswitch_73
    const-string v0, "OP_ADD_BROADCAST"

    goto/16 :goto_0

    .line 6103
    :pswitch_74
    const-string v0, "OP_GET_BROADCAST"

    goto/16 :goto_0

    .line 6105
    :pswitch_75
    const-string v0, "OP_MODIFY_BROADCAST"

    goto/16 :goto_0

    .line 6107
    :pswitch_76
    const-string v0, "OP_REMOVE_BROADCAST"

    goto/16 :goto_0

    .line 6109
    :pswitch_77
    const-string v0, "OP_WARN_NO_SIM_FOR_SMS"

    goto/16 :goto_0

    .line 6111
    :pswitch_78
    const-string v0, "OP_MAYBE_REFRESH_CONTACTS_DATA"

    goto/16 :goto_0

    .line 6113
    :pswitch_79
    const-string v0, "OP_REFRESH_SMS_PARTICIPANTS"

    goto/16 :goto_0

    .line 6115
    :pswitch_7a
    const-string v0, "OP_TEST_WATCHDOG"

    goto/16 :goto_0

    .line 6117
    :pswitch_7b
    const-string v0, "OP_MERGE_CONVERSATIONS"

    goto/16 :goto_0

    .line 6119
    :pswitch_7c
    const-string v0, "OP_UNMERGE_CONVERSATIONS"

    goto/16 :goto_0

    .line 6121
    :pswitch_7d
    const-string v0, "OP_REQUEST_FIFE_URLS"

    goto/16 :goto_0

    .line 6123
    :pswitch_7e
    const-string v0, "OP_MERGE_ALL_CONVERSATIONS"

    goto/16 :goto_0

    .line 6125
    :pswitch_7f
    const-string v0, "OP_UNMERGE_ALL_CONVERSATIONS"

    goto/16 :goto_0

    .line 6127
    :pswitch_80
    const-string v0, "OP_UNDISMISS_SUGGESTED_CONTACT"

    goto/16 :goto_0

    .line 6129
    :pswitch_81
    const-string v0, "OP_UPDATE_CONVERSATION_CALL_MEDIA_TYPE"

    goto/16 :goto_0

    .line 6131
    :pswitch_82
    const-string v0, "OP_SAVE_SMS_AND_NOTIFY_IF_UNREAD"

    goto/16 :goto_0

    .line 6133
    :pswitch_83
    const-string v0, "OP_GET_AUDIO_DATA"

    goto/16 :goto_0

    .line 6135
    :pswitch_84
    const-string v0, "OP_FORK_CONVERSATION"

    goto/16 :goto_0

    .line 6137
    :pswitch_85
    const-string v0, "OP_TICKLE_GCM"

    goto/16 :goto_0

    .line 5871
    nop

    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_0
        :pswitch_14
        :pswitch_0
        :pswitch_0
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_0
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_0
        :pswitch_22
        :pswitch_0
        :pswitch_0
        :pswitch_23
        :pswitch_24
        :pswitch_0
        :pswitch_25
        :pswitch_26
        :pswitch_0
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_0
        :pswitch_2b
        :pswitch_0
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_0
        :pswitch_0
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_38
        :pswitch_39
        :pswitch_3a
        :pswitch_3b
        :pswitch_3c
        :pswitch_3d
        :pswitch_3e
        :pswitch_3f
        :pswitch_40
        :pswitch_41
        :pswitch_42
        :pswitch_43
        :pswitch_44
        :pswitch_45
        :pswitch_46
        :pswitch_47
        :pswitch_0
        :pswitch_48
        :pswitch_0
        :pswitch_49
        :pswitch_4a
        :pswitch_4b
        :pswitch_4c
        :pswitch_4d
        :pswitch_4e
        :pswitch_4f
        :pswitch_50
        :pswitch_51
        :pswitch_52
        :pswitch_53
        :pswitch_0
        :pswitch_54
        :pswitch_55
        :pswitch_56
        :pswitch_0
        :pswitch_0
        :pswitch_57
        :pswitch_0
        :pswitch_58
        :pswitch_59
        :pswitch_5a
        :pswitch_5b
        :pswitch_5c
        :pswitch_5d
        :pswitch_5e
        :pswitch_5f
        :pswitch_0
        :pswitch_60
        :pswitch_61
        :pswitch_62
        :pswitch_63
        :pswitch_64
        :pswitch_65
        :pswitch_66
        :pswitch_67
        :pswitch_68
        :pswitch_69
        :pswitch_6a
        :pswitch_6b
        :pswitch_6c
        :pswitch_6d
        :pswitch_6e
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6f
        :pswitch_0
        :pswitch_70
        :pswitch_71
        :pswitch_0
        :pswitch_72
        :pswitch_77
        :pswitch_0
        :pswitch_78
        :pswitch_0
        :pswitch_79
        :pswitch_0
        :pswitch_7a
        :pswitch_7b
        :pswitch_7c
        :pswitch_7d
        :pswitch_7e
        :pswitch_7f
        :pswitch_80
        :pswitch_82
        :pswitch_0
        :pswitch_0
        :pswitch_83
        :pswitch_81
        :pswitch_73
        :pswitch_74
        :pswitch_75
        :pswitch_76
        :pswitch_84
        :pswitch_85
    .end packed-switch
.end method

.method public static a(Landroid/content/Intent;Lyj;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 6264
    const-string v0, "op"

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 6265
    sparse-switch v0, :sswitch_data_0

    .line 6282
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(I)Ljava/lang/String;

    move-result-object v1

    :cond_0
    :goto_0
    return-object v1

    .line 6267
    :sswitch_0
    const-string v0, "server_response"

    .line 6268
    invoke-virtual {p0, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a([B)Ljava/io/Serializable;

    move-result-object v0

    .line 6269
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ServerResponse: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 6273
    :sswitch_1
    const-string v0, "payload"

    .line 6274
    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 6273
    invoke-static {v0, p1}, Lbiq;->a(Ljava/lang/String;Lyj;)Ljava/util/List;

    move-result-object v1

    .line 6275
    const-string v0, "ServerUpdate: "

    .line 6276
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbiq;

    .line 6277
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 6278
    goto :goto_1

    .line 6265
    nop

    :sswitch_data_0
    .sparse-switch
        0x27 -> :sswitch_0
        0x35 -> :sswitch_1
    .end sparse-switch
.end method

.method private a(Lyj;Lbki;Lbfz;)Ljava/util/List;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lyj;",
            "Lbki;",
            "Lbfz;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbos;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4585
    new-instance v13, Lbnl;

    invoke-direct {v13}, Lbnl;-><init>()V

    .line 4586
    new-instance v14, Ljava/util/LinkedList;

    invoke-direct {v14}, Ljava/util/LinkedList;-><init>()V

    .line 4593
    sget-boolean v1, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v1, :cond_0

    .line 4594
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "processResponse: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4596
    :cond_0
    const-string v1, "Babel"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "processResponse "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p3 .. p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for acct "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lyj;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    move-object/from16 v0, p3

    instance-of v1, v0, Lbhn;

    if-eqz v1, :cond_4

    move-object/from16 v1, p3

    check-cast v1, Lbhn;

    invoke-virtual {v1}, Lbhn;->i()I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_2

    invoke-virtual {v1}, Lbhn;->i()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    :cond_2
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    :try_start_0
    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    invoke-virtual {v1}, Lbhn;->j()Ljava/lang/String;

    new-instance v4, Lyt;

    move-object/from16 v0, p1

    invoke-direct {v4, v0}, Lyt;-><init>(Lyj;)V

    const-string v5, "upgrade_value"

    invoke-virtual {v1}, Lbhn;->i()I

    move-result v6

    int-to-long v6, v6

    invoke-virtual {v4, v5, v6, v7}, Lyt;->h(Ljava/lang/String;J)V

    const-string v5, "upgrade_url"

    invoke-virtual {v1}, Lbhn;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lyt;->j(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "upgrade_version"

    iget v2, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    int-to-long v6, v2

    invoke-virtual {v4, v5, v6, v7}, Lyt;->h(Ljava/lang/String;J)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_0
    invoke-virtual {v1}, Lbhn;->f()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_b

    invoke-virtual {v1}, Lbhn;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lbkb;->a(Lyj;Ljava/lang/String;)V

    :cond_4
    :goto_1
    move-object/from16 v0, p3

    instance-of v1, v0, Lbha;

    if-eqz v1, :cond_e

    move-object/from16 v1, p3

    check-cast v1, Lbha;

    invoke-virtual {v1}, Lbha;->f()Lbdh;

    move-result-object v2

    const-string v3, "Babel"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "processResponse for GetSelfInfo, participantId="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v2, Lbdh;->b:Lbdk;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v1}, Lbha;->g()Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v1}, Lbha;->h()Lyk;

    move-result-object v4

    invoke-virtual {v1}, Lbha;->m()[Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v2, v3, v4, v5}, Lbkb;->a(Lyj;Lbdh;Ljava/util/Map;Lyk;[Ljava/lang/String;)V

    invoke-virtual {v1}, Lbha;->i()Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->d()Lcom/google/android/apps/hangouts/phone/EsApplication;

    move-result-object v3

    invoke-virtual {v1}, Lbha;->j()J

    move-result-wide v4

    move-object/from16 v0, p1

    invoke-virtual {v3, v0, v4, v5}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Lyj;J)V

    invoke-virtual {v1}, Lbha;->j()J

    move-result-wide v3

    move-object/from16 v0, p1

    invoke-direct {p0, v0, v3, v4}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Lyj;J)V

    :goto_2
    invoke-virtual {v1}, Lbha;->k()Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-virtual {v1}, Lbha;->k()Ljava/util/List;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ljava/util/List;)V

    :cond_6
    invoke-virtual {v1}, Lbha;->l()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->w(Lyj;Ljava/lang/String;)V

    const/16 v1, 0x7c

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v1

    const-string v3, "participant_entity"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Landroid/content/Intent;)V

    .line 4598
    :cond_7
    :goto_3
    invoke-virtual {v13}, Lbnl;->e()I

    move-result v1

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lbne;->a(Lyj;I)V

    .line 4608
    invoke-virtual {v13}, Lbnl;->b()Ljava/util/List;

    move-result-object v1

    .line 4609
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_9

    .line 4610
    const-string v2, "Babel"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 4611
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "processResponse: sending "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " requests from processing "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 4612
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 4611
    invoke-static {v2, v3}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 4614
    :cond_8
    const/4 v2, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Lbki;->a(Ljava/util/Collection;I)V

    .line 4616
    :cond_9
    return-object v14

    .line 4596
    :catch_0
    move-exception v2

    const-string v4, "Babel"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "failed to find current version for "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3, v2}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_a
    const-string v1, "Babel"

    const-string v2, "Full jid should not be empty"

    invoke-static {v1, v2}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_b
    invoke-virtual {v1}, Lbhn;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_c

    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Account unregistered: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lyj;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_c
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Removed account "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " unregistered using account:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lyj;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_d
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->d()Lcom/google/android/apps/hangouts/phone/EsApplication;

    move-result-object v3

    const-wide/16 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v3, v0, v4, v5}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Lyj;J)V

    const-wide/16 v3, 0x0

    move-object/from16 v0, p1

    invoke-direct {p0, v0, v3, v4}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Lyj;J)V

    goto/16 :goto_2

    :cond_e
    move-object/from16 v0, p3

    instance-of v1, v0, Lbia;

    if-eqz v1, :cond_10

    invoke-static/range {p1 .. p1}, Lbpx;->a(Lyj;)Lbpx;

    move-result-object v2

    move-object/from16 v1, p3

    check-cast v1, Lbia;

    iget-boolean v1, v1, Lbia;->g:Z

    if-eqz v1, :cond_15

    const-string v1, "Babel"

    const-string v3, "Client is invalid. Force retry GcmRegistration"

    invoke-static {v1, v3}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lbld;->c()Lbld;

    move-result-object v1

    invoke-virtual {v1}, Lbld;->i()V

    :cond_f
    :goto_4
    const/4 v1, 0x3

    invoke-virtual {v2, v1}, Lbpx;->a(I)V

    :cond_10
    move-object/from16 v0, p3

    instance-of v1, v0, Lbhb;

    if-eqz v1, :cond_12

    move-object/from16 v1, p3

    check-cast v1, Lbhb;

    invoke-static/range {p1 .. p1}, Lbqh;->a(Lyj;)Lbqh;

    move-result-object v2

    sget-boolean v3, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v3, :cond_11

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SyncBaselineSuggestedContactsOperation successful: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lyj;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    :cond_11
    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lbqh;->a(I)V

    invoke-virtual {v1}, Lbhb;->f()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v2, v3}, Lyn;->a(Lyj;Ljava/util/List;Z)V

    invoke-virtual {v1}, Lbhb;->g()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v1, v2}, Lyn;->a(Lyj;Ljava/util/List;Z)V

    :cond_12
    move-object/from16 v0, p3

    instance-of v1, v0, Lbgs;

    if-eqz v1, :cond_14

    move-object/from16 v1, p3

    check-cast v1, Lbgs;

    invoke-static/range {p1 .. p1}, Lbow;->a(Lyj;)Lbow;

    move-result-object v2

    invoke-virtual {v1}, Lbgs;->i()Z

    move-result v1

    if-eqz v1, :cond_14

    invoke-virtual {v2}, Lbow;->d()I

    move-result v1

    const/4 v3, 0x2

    if-ne v1, v3, :cond_14

    sget-boolean v1, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v1, :cond_13

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "RefreshParticipantsOperationOperation successful: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lyj;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    :cond_13
    const/4 v1, 0x3

    invoke-virtual {v2, v1}, Lbow;->a(I)V

    :cond_14
    move-object/from16 v0, p3

    instance-of v1, v0, Lbic;

    if-eqz v1, :cond_16

    move-object/from16 v1, p3

    check-cast v1, Lbic;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->s:Landroid/os/Handler;

    new-instance v3, Lboc;

    move-object/from16 v0, p1

    invoke-direct {v3, p0, v0, v1}, Lboc;-><init>(Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;Lyj;Lbic;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_3

    :cond_15
    sget-boolean v1, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v1, :cond_f

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "SetActiveClientOperation successful: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lyj;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_16
    move-object/from16 v0, p3

    instance-of v1, v0, Lbgy;

    if-eqz v1, :cond_17

    move-object/from16 v1, p3

    check-cast v1, Lbgy;

    move-object/from16 v0, p1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Lbgy;)V

    goto/16 :goto_3

    :cond_17
    move-object/from16 v0, p3

    instance-of v1, v0, Lbgw;

    if-eqz v1, :cond_19

    move-object/from16 v1, p3

    check-cast v1, Lbgw;

    invoke-virtual {v1}, Lbgw;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lbgw;->g()Ljava/util/List;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {v1}, Lbgw;->g()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_18

    invoke-virtual {v1}, Lbgw;->g()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_19

    :cond_18
    new-instance v2, Lyt;

    move-object/from16 v0, p1

    invoke-direct {v2, v0}, Lyt;-><init>(Lyj;)V

    const/4 v3, 0x0

    invoke-virtual {v1}, Lbgw;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lyt;->Q(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v1}, Lbgw;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v4, v5, v1}, Lyt;->a(IJLjava/lang/String;)V

    :cond_19
    move-object/from16 v0, p3

    instance-of v1, v0, Lbgu;

    if-eqz v1, :cond_1a

    move-object/from16 v1, p3

    check-cast v1, Lbgu;

    invoke-virtual {v1}, Lbgu;->f()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_22

    const-string v1, "hangoutId is empty"

    invoke-static {v1}, Lcwz;->a(Ljava/lang/String;)V

    :cond_1a
    :goto_5
    move-object/from16 v0, p3

    instance-of v1, v0, Lbgd;

    if-nez v1, :cond_1b

    move-object/from16 v0, p3

    instance-of v1, v0, Lbhw;

    if-nez v1, :cond_1b

    new-instance v1, Lbos;

    invoke-virtual/range {p3 .. p3}, Lbfz;->a()I

    move-result v2

    const/4 v3, 0x1

    move-object/from16 v0, p3

    invoke-direct {v1, v2, v3, v0}, Lbos;-><init>(IILbfz;)V

    invoke-interface {v14, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1b
    move-object/from16 v0, p3

    instance-of v1, v0, Lbge;

    if-eqz v1, :cond_1c

    move-object/from16 v1, p3

    check-cast v1, Lbge;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->s:Landroid/os/Handler;

    new-instance v3, Lboe;

    invoke-direct {v3, p0, v1}, Lboe;-><init>(Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;Lbge;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_1c
    move-object/from16 v0, p3

    instance-of v1, v0, Lbgx;

    if-eqz v1, :cond_1d

    move-object/from16 v1, p3

    check-cast v1, Lbgx;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->s:Landroid/os/Handler;

    new-instance v3, Lbod;

    invoke-direct {v3, p0, v1}, Lbod;-><init>(Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;Lbgx;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_1d
    move-object/from16 v0, p3

    instance-of v1, v0, Lbgt;

    if-eqz v1, :cond_1e

    move-object/from16 v1, p3

    check-cast v1, Lbgt;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->s:Landroid/os/Handler;

    new-instance v3, Lbof;

    invoke-direct {v3, p0, v1}, Lbof;-><init>(Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;Lbgt;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_1e
    const-wide/16 v1, 0x0

    sget-boolean v3, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v3, :cond_1f

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    :cond_1f
    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Lbnl;->a(Lyj;)V

    new-instance v5, Lyt;

    move-object/from16 v0, p1

    invoke-direct {v5, v0}, Lyt;-><init>(Lyj;)V

    move-object/from16 v0, p3

    invoke-static {v5, v0, v13}, Lyp;->a(Lyt;Lbfz;Lbnl;)V

    invoke-virtual {v13}, Lbnl;->c()V

    sget-boolean v3, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v3, :cond_20

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    sub-long v6, v3, v1

    const-wide/16 v8, 0x3e8

    cmp-long v6, v6, v8

    if-lez v6, :cond_20

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "processResponse "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p3 .. p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " took "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sub-long v1, v3, v1

    invoke-virtual {v6, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    :cond_20
    move-object/from16 v0, p3

    instance-of v1, v0, Lbhw;

    if-eqz v1, :cond_7

    move-object/from16 v1, p3

    check-cast v1, Lbhw;

    invoke-virtual {v1}, Lbhw;->m()[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    if-lez v2, :cond_7

    invoke-virtual {v1}, Lbhw;->n()Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v2, "Babel"

    const-string v3, "initiate full res upload"

    invoke-static {v2, v3}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lbhw;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lbhw;->h()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v3, v4}, Lyt;->f(Ljava/lang/String;Ljava/lang/String;)Lzg;

    move-result-object v7

    if-eqz v7, :cond_21

    iget-object v1, v7, Lzg;->j:Ljava/lang/String;

    if-nez v1, :cond_23

    :cond_21
    const-string v1, "Babel"

    const-string v2, "empty image url, can\'t upload"

    invoke-static {v1, v2}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_22
    new-instance v2, Lbdv;

    invoke-virtual {v1}, Lbgu;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lbgu;->g()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lbdv;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v13, v2}, Lbnl;->a(Lbea;)V

    goto/16 :goto_5

    :cond_23
    const/4 v2, 0x0

    :try_start_1
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v5, v7, Lzg;->j:Ljava/lang/String;

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const-string v6, "r"

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    :goto_6
    const-wide/16 v11, 0x0

    if-eqz v1, :cond_24

    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->getStatSize()J

    move-result-wide v11

    :goto_7
    new-instance v1, Lbcp;

    invoke-virtual/range {p1 .. p1}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v5, v7, Lzg;->j:Ljava/lang/String;

    iget-object v6, v7, Lzg;->n:Ljava/lang/String;

    iget-object v7, v7, Lzg;->m:Ljava/lang/String;

    const-string v8, "image/jpeg"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    invoke-direct/range {v1 .. v12}, Lbcp;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V

    invoke-static/range {p1 .. p1}, Lbkb;->o(Lyj;)Lbki;

    move-result-object v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v1

    invoke-virtual {v2, v3, v1}, Lbki;->a(Ljava/util/Collection;I)V

    goto/16 :goto_3

    :catch_1
    move-exception v1

    const-string v5, "Babel"

    const-string v6, "error opening image"

    invoke-static {v5, v6, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v2

    goto :goto_6

    :cond_24
    const-string v1, "Babel"

    const-string v2, "could not open fd"

    invoke-static {v1, v2}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7
.end method

.method public static a()V
    .locals 3

    .prologue
    .line 1378
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1379
    const-string v1, "op"

    const/16 v2, 0x11

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1380
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Landroid/content/Intent;)V

    .line 1381
    return-void
.end method

.method public static a(IJ)V
    .locals 2

    .prologue
    .line 6315
    invoke-static {}, Lbkb;->n()Lyj;

    move-result-object v0

    .line 6316
    if-nez v0, :cond_0

    .line 6324
    :goto_0
    return-void

    .line 6320
    :cond_0
    const/16 v1, 0x85

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 6321
    const-string v1, "free_sms_storage_action_index"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 6322
    const-string v1, "free_sms_storage_duration"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 6323
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    goto :goto_0
.end method

.method public static a(ILyj;Lbos;)V
    .locals 2

    .prologue
    .line 5423
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 5424
    new-instance v1, Lbns;

    invoke-direct {v1, p0, p1, p2}, Lbns;-><init>(ILyj;Lbos;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 5432
    return-void
.end method

.method public static a(J)V
    .locals 5

    .prologue
    .line 2371
    const-string v0, "Babel"

    const-string v1, "Scheduling phone verification timeout alarm"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2373
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "alarm"

    .line 2374
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 2375
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->u()Landroid/app/PendingIntent;

    move-result-object v1

    .line 2376
    const/4 v2, 0x2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    add-long/2addr v3, p0

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 2378
    return-void
.end method

.method public static a(JJ)V
    .locals 7

    .prologue
    .line 2255
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "alarm"

    .line 2256
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 2257
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->t()Landroid/app/PendingIntent;

    move-result-object v6

    .line 2258
    const/4 v1, 0x2

    move-wide v2, p0

    move-wide v4, p2

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V

    .line 2260
    return-void
.end method

.method public static a(Landroid/net/Uri;JZ)V
    .locals 4

    .prologue
    .line 6205
    invoke-static {}, Lbkb;->m()Z

    move-result v0

    if-nez v0, :cond_0

    .line 6215
    :goto_0
    return-void

    .line 6209
    :cond_0
    invoke-static {}, Lbkb;->n()Lyj;

    move-result-object v0

    const/16 v1, 0x70

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 6210
    const-string v1, "uri"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 6211
    const-string v1, "thread_id"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 6212
    const-string v1, "notification_row_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 6213
    const-string v1, "mms_auto_retrieve"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 6214
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    goto :goto_0
.end method

.method public static a(Lbjr;)V
    .locals 2

    .prologue
    .line 5538
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 5539
    new-instance v1, Lbnz;

    invoke-direct {v1, p0}, Lbnz;-><init>(Lbjr;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 5547
    return-void
.end method

.method private a(Lbki;Landroid/content/Intent;Lbmu;Lyj;)V
    .locals 2

    .prologue
    .line 4555
    invoke-virtual {p3}, Lbmu;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4556
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v0, :cond_0

    .line 4557
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is executed directly: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p4}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    .line 4559
    :cond_0
    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->n:Ljava/util/Queue;

    invoke-interface {v0, p3}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 4560
    const/4 v0, 0x2

    invoke-virtual {p3, v0}, Lbmu;->a(I)V

    .line 4561
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    .line 4573
    :cond_1
    :goto_0
    return-void

    .line 4562
    :cond_2
    invoke-virtual {p3}, Lbmu;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 4563
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v0, :cond_3

    .line 4564
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is queued: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p4}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    .line 4566
    :cond_3
    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->n:Ljava/util/Queue;

    invoke-interface {v0, p3}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 4567
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Lbmu;->a(I)V

    goto :goto_0

    .line 4569
    :cond_4
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v0, :cond_1

    .line 4570
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is ignored: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p4}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Lbop;)V
    .locals 0

    .prologue
    .line 1044
    sput-object p0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e:Lbop;

    .line 1045
    return-void
.end method

.method public static a(Lboq;)V
    .locals 0

    .prologue
    .line 1053
    sput-object p0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f:Lboq;

    .line 1054
    return-void
.end method

.method public static a(Lbor;)V
    .locals 1

    .prologue
    .line 1024
    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->p:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1025
    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->p:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 1027
    :cond_0
    return-void
.end method

.method public static a(Ljava/io/PrintWriter;)V
    .locals 12

    .prologue
    const/4 v0, 0x0

    .line 6415
    sget-object v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->v:Ljava/lang/Object;

    monitor-enter v2

    .line 6416
    :try_start_0
    sget-object v1, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->t:Lcxa;

    if-nez v1, :cond_0

    .line 6417
    monitor-exit v2

    .line 6430
    :goto_0
    return-void

    .line 6419
    :cond_0
    const-string v1, "enqueueTime          s-q   e-s   e-q opcode"

    invoke-virtual {p0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 6420
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v1, "MM-dd HH:mm:ss.SSS"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-direct {v3, v1, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    move v1, v0

    .line 6421
    :goto_1
    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->t:Lcxa;

    invoke-virtual {v0}, Lcxa;->a()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 6422
    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->t:Lcxa;

    invoke-virtual {v0, v1}, Lcxa;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lboo;

    .line 6423
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    const-string v5, "%s %5d %5d %5d %s"

    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-wide v8, v0, Lboo;->b:J

    .line 6424
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-wide v8, v0, Lboo;->d:J

    iget-wide v10, v0, Lboo;->c:J

    sub-long/2addr v8, v10

    const-wide/32 v10, 0xf4240

    div-long/2addr v8, v10

    .line 6425
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    iget-wide v8, v0, Lboo;->e:J

    iget-wide v10, v0, Lboo;->d:J

    sub-long/2addr v8, v10

    const-wide/32 v10, 0xf4240

    div-long/2addr v8, v10

    .line 6426
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    iget-wide v8, v0, Lboo;->e:J

    iget-wide v10, v0, Lboo;->c:J

    sub-long/2addr v8, v10

    const-wide/32 v10, 0xf4240

    div-long/2addr v8, v10

    .line 6427
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x4

    iget v0, v0, Lboo;->a:I

    .line 6428
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v7

    .line 6423
    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 6421
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 6430
    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public static a(Ljava/lang/String;JZI)V
    .locals 2

    .prologue
    .line 6224
    const/16 v0, 0x8c

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 6225
    const-string v1, "notification_row_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 6226
    const-string v1, "mms_auto_retrieve"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 6227
    const-string v1, "error"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 6228
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    .line 6229
    return-void
.end method

.method public static a(Ljava/lang/String;Lbdk;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 5522
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 5523
    new-instance v1, Lbnx;

    invoke-direct {v1, p0, p1, p2, p3}, Lbnx;-><init>(Ljava/lang/String;Lbdk;Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 5532
    return-void
.end method

.method public static a(Ljava/lang/String;Lbdk;Z)V
    .locals 2

    .prologue
    .line 5483
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 5484
    new-instance v1, Lbnv;

    invoke-direct {v1, p0, p1, p2}, Lbnv;-><init>(Ljava/lang/String;Lbdk;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 5492
    return-void
.end method

.method public static a(Ljava/lang/String;Lbpp;Lbfz;)V
    .locals 4

    .prologue
    .line 1408
    const/16 v0, 0x27

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 1412
    const-string v1, "server_response"

    invoke-static {p2}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a(Lbfz;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 1413
    const-string v1, "server_request"

    .line 1414
    invoke-virtual {p1}, Lbpp;->a()Lbea;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->a(Lbea;)[B

    move-result-object v2

    .line 1413
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 1415
    sget-boolean v1, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v1, :cond_0

    .line 1416
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "handleServerResponses: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1419
    :cond_0
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Landroid/content/Intent;)V

    .line 1420
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1215
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1216
    invoke-virtual {v0, p0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1217
    const-string v1, "op"

    const/16 v2, 0x64

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1218
    const-string v1, "package"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1219
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Landroid/content/Intent;)V

    .line 1220
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2945
    const/16 v0, 0x65

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Ljava/lang/String;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2946
    const-string v1, "conversation_name"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2947
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Landroid/content/Intent;)V

    .line 2948
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;[BJZ)V
    .locals 2

    .prologue
    .line 6188
    const/16 v0, 0x74

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 6189
    const-string v1, "mms_content_location"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6190
    const-string v1, "mms_transaction_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 6191
    const-string v1, "notification_row_id"

    invoke-virtual {v0, v1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 6192
    const-string v1, "mms_auto_retrieve"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 6193
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    .line 6194
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lbdk;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 5501
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 5502
    new-instance v1, Lbnw;

    invoke-direct {v1, p0, p1}, Lbnw;-><init>(Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 5510
    return-void
.end method

.method public static a(Ljava/lang/String;Lyj;)V
    .locals 2

    .prologue
    .line 5442
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 5443
    new-instance v1, Lbnt;

    invoke-direct {v1, p1, p0}, Lbnt;-><init>(Lyj;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 5451
    return-void
.end method

.method public static a(Ljava/lang/String;Lyj;Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 5462
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 5463
    new-instance v1, Lbnu;

    invoke-direct {v1, p1, p0, p2}, Lbnu;-><init>(Lyj;Ljava/lang/String;Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 5472
    return-void
.end method

.method private static a(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1496
    invoke-static {v2}, Lbkb;->f(Z)Ljava/util/ArrayList;

    move-result-object v0

    .line 1499
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 1501
    const-string v0, "Babel"

    const-string v1, "All accounts removed, unregistering gcm"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1502
    invoke-static {}, Lbld;->c()Lbld;

    move-result-object v0

    invoke-virtual {v0}, Lbld;->g()V

    .line 1540
    :goto_0
    return-void

    .line 1506
    :cond_0
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1507
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v1

    .line 1508
    if-eqz v1, :cond_1

    invoke-static {v1}, Lbkb;->f(Lyj;)I

    move-result v2

    const/16 v3, 0x66

    if-ne v2, v3, :cond_1

    .line 1509
    invoke-virtual {v1}, Lyj;->u()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1510
    :cond_1
    invoke-static {}, Lbkb;->i()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1512
    const-string v0, "Babel"

    const-string v1, "All accounts are logged off or have sms only account. Unregistering gcm."

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1514
    invoke-static {}, Lbld;->c()Lbld;

    move-result-object v0

    invoke-virtual {v0}, Lbld;->g()V

    goto :goto_0

    .line 1516
    :cond_2
    const-string v0, "Babel"

    const-string v1, "Some account is in signing process. Skip unregistering gcm"

    invoke-static {v0, v1}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1521
    :cond_3
    const-string v1, "Babel"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1522
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Use account "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to unregistered removed accounts"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1526
    :cond_4
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1527
    invoke-static {v1}, Lbdk;->b(Ljava/lang/String;)Lbdk;

    move-result-object v3

    .line 1528
    invoke-static {v3}, Lbkb;->a(Lbdk;)Lyj;

    move-result-object v3

    .line 1529
    if-eqz v3, :cond_5

    .line 1530
    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Removing a valid account by mistake:accountName="

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1531
    invoke-virtual {v3}, Lyj;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", accountGaia="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1530
    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1536
    :cond_6
    const/16 v1, 0x68

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 1537
    const-string v1, "account_gaiaids"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 1539
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public static a(Lyj;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1140
    const/16 v0, 0xf

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Landroid/content/Intent;)V

    .line 1144
    invoke-static {p0, v1, v1, v1, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ZZIZ)V

    .line 1149
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;Z)I

    .line 1152
    invoke-static {p0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->l(Lyj;)V

    .line 1155
    invoke-static {p0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->m(Lyj;)V

    .line 1159
    invoke-static {p0}, Laai;->a(Lyj;)V

    .line 1162
    invoke-static {}, Lbmz;->a()Lbmz;

    move-result-object v0

    invoke-virtual {v0}, Lbmz;->f()V

    .line 1163
    return-void
.end method

.method public static a(Lyj;J)V
    .locals 2

    .prologue
    .line 1595
    const/16 v0, 0x42

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 1596
    const-string v1, "dnd_expiration"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1597
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Landroid/content/Intent;)V

    .line 1598
    return-void
.end method

.method public static a(Lyj;JIZ)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1665
    new-instance v0, Lboh;

    move-object v1, p0

    move v2, p3

    move v3, p4

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Lboh;-><init>(Lyj;IZJ)V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v6, v1, v2

    const/4 v2, 0x1

    aput-object v6, v1, v2

    .line 1685
    invoke-virtual {v0, v1}, Lboh;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1686
    return-void
.end method

.method public static a(Lyj;Lbea;Lbph;)V
    .locals 2

    .prologue
    .line 5294
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 5295
    new-instance v1, Lbnq;

    invoke-direct {v1, p1, p0, p2}, Lbnq;-><init>(Lbea;Lyj;Lbph;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 5305
    return-void
.end method

.method private a(Lyj;Lbki;Lbiq;JJJ)V
    .locals 9

    .prologue
    .line 4622
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4623
    const/4 v0, 0x0

    .line 4624
    instance-of v1, p3, Lbis;

    if-eqz v1, :cond_0

    move-object v0, p3

    .line 4625
    check-cast v0, Lbis;

    iget-object v0, v0, Lbis;->c:Ljava/lang/String;

    .line 4627
    :cond_0
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "processServerUpdate: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", account: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 4628
    invoke-virtual {p1}, Lyj;->c()Lbdk;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz v0, :cond_4

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "update convId "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 4627
    invoke-static {v1, v0}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 4632
    :cond_1
    instance-of v0, p3, Lbir;

    if-eqz v0, :cond_5

    .line 4636
    check-cast p3, Lbir;

    .line 4638
    iget v0, p3, Lbir;->b:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    .line 4639
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v0, :cond_2

    .line 4640
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Clearing active client time stamp for account: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    .line 4642
    :cond_2
    invoke-static {p1}, Lbpx;->a(Lyj;)Lbpx;

    move-result-object v0

    .line 4643
    invoke-virtual {v0}, Lbpx;->h()V

    .line 4786
    :cond_3
    :goto_1
    return-void

    .line 4628
    :cond_4
    const-string v0, ""

    goto :goto_0

    .line 4647
    :cond_5
    instance-of v0, p3, Lbjl;

    if-eqz v0, :cond_6

    .line 4654
    check-cast p3, Lbjl;

    .line 4655
    iget v0, p3, Lbjl;->c:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 4656
    new-instance v0, Lyt;

    invoke-direct {v0, p1}, Lyt;-><init>(Lyj;)V

    .line 4657
    iget-object v1, p3, Lbjl;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lyt;->r(Ljava/lang/String;)V

    goto :goto_1

    .line 4661
    :cond_6
    instance-of v0, p3, Lbjn;

    if-eqz v0, :cond_8

    .line 4662
    check-cast p3, Lbjn;

    .line 4663
    iget-boolean v0, p3, Lbjn;->c:Z

    if-eqz v0, :cond_7

    .line 4664
    iget-wide v0, p3, Lbjn;->b:J

    .line 4665
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->d()Lcom/google/android/apps/hangouts/phone/EsApplication;

    move-result-object v2

    invoke-virtual {v2, p1, v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Lyj;J)V

    .line 4666
    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Lyj;J)V

    .line 4667
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->d()Lcom/google/android/apps/hangouts/phone/EsApplication;

    move-result-object v2

    invoke-virtual {v2, p1, v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Lyj;J)V

    .line 4669
    :cond_7
    iget-object v0, p3, Lbjn;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 4670
    iget-object v0, p3, Lbjn;->d:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->w(Lyj;Ljava/lang/String;)V

    goto :goto_1

    .line 4674
    :cond_8
    instance-of v0, p3, Lbjq;

    if-eqz v0, :cond_9

    .line 4675
    check-cast p3, Lbjq;

    .line 4678
    new-instance v0, Lbnl;

    invoke-direct {v0}, Lbnl;-><init>()V

    .line 4679
    new-instance v0, Lyt;

    invoke-direct {v0, p1}, Lyt;-><init>(Lyj;)V

    .line 4681
    new-instance v1, Lbka;

    invoke-direct {v1, p3}, Lbka;-><init>(Lbjq;)V

    .line 4682
    invoke-virtual {v1, v0}, Lbka;->b(Lyt;)V

    goto :goto_1

    .line 4685
    :cond_9
    instance-of v0, p3, Lbit;

    if-eqz v0, :cond_a

    .line 4686
    check-cast p3, Lbit;

    .line 4688
    new-instance v0, Lbnl;

    invoke-direct {v0}, Lbnl;-><init>()V

    .line 4689
    new-instance v0, Lyt;

    invoke-direct {v0, p1}, Lyt;-><init>(Lyj;)V

    .line 4691
    new-instance v1, Lbkm;

    invoke-direct {v1, p3}, Lbkm;-><init>(Lbit;)V

    .line 4692
    invoke-virtual {v1, v0}, Lbkm;->a(Lyt;)V

    goto :goto_1

    .line 4695
    :cond_a
    instance-of v0, p3, Lbjb;

    if-eqz v0, :cond_b

    .line 4701
    check-cast p3, Lbjb;

    iget-object v0, p3, Lbjb;->d:Ljava/lang/String;

    .line 4702
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ContactsNotification.selfFanoutId == "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    .line 4703
    invoke-static {v0}, Lbea;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 4705
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;Z)I

    goto/16 :goto_1

    .line 4709
    :cond_b
    instance-of v0, p3, Lbje;

    if-eqz v0, :cond_c

    .line 4710
    check-cast p3, Lbje;

    .line 4713
    new-instance v0, Lbnl;

    invoke-direct {v0}, Lbnl;-><init>()V

    .line 4714
    new-instance v0, Lyt;

    invoke-direct {v0, p1}, Lyt;-><init>(Lyj;)V

    .line 4716
    new-instance v1, Lbku;

    invoke-direct {v1, p3}, Lbku;-><init>(Lbje;)V

    .line 4717
    invoke-virtual {v1, v0}, Lbku;->a(Lyt;)V

    goto/16 :goto_1

    .line 4720
    :cond_c
    instance-of v0, p3, Lbjj;

    if-eqz v0, :cond_d

    .line 4721
    check-cast p3, Lbjj;

    .line 4723
    new-instance v0, Lbnl;

    invoke-direct {v0}, Lbnl;-><init>()V

    .line 4724
    new-instance v0, Lyt;

    invoke-direct {v0, p1}, Lyt;-><init>(Lyj;)V

    .line 4726
    new-instance v1, Lbqp;

    invoke-direct {v1, p3}, Lbqp;-><init>(Lbjj;)V

    .line 4727
    invoke-virtual {v1, v0}, Lbqp;->a(Lyt;)V

    goto/16 :goto_1

    .line 4730
    :cond_d
    instance-of v0, p3, Lbjm;

    if-eqz v0, :cond_e

    .line 4731
    check-cast p3, Lbjm;

    .line 4734
    iget-object v0, p3, Lbjm;->b:Ljava/util/List;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ljava/util/List;)V

    goto/16 :goto_1

    .line 4738
    :cond_e
    instance-of v0, p3, Lbjf;

    if-eqz v0, :cond_f

    .line 4739
    check-cast p3, Lbjf;

    .line 4741
    iget-object v0, p0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->s:Landroid/os/Handler;

    new-instance v1, Lbnp;

    invoke-direct {v1, p0, p1, p3}, Lbnp;-><init>(Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;Lyj;Lbjf;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_1

    .line 4751
    :cond_f
    new-instance v2, Lbnl;

    invoke-direct {v2}, Lbnl;-><init>()V

    .line 4755
    invoke-virtual {v2, p1}, Lbnl;->a(Lyj;)V

    .line 4756
    invoke-virtual {v2}, Lbnl;->a()V

    .line 4758
    new-instance v0, Lyt;

    invoke-direct {v0, p1}, Lyt;-><init>(Lyj;)V

    move-object v1, p3

    move-wide v3, p4

    move-wide v5, p6

    move-wide/from16 v7, p8

    invoke-static/range {v0 .. v8}, Lyp;->a(Lyt;Lbiq;Lbnl;JJJ)V

    .line 4763
    invoke-virtual {v2}, Lbnl;->c()V

    .line 4766
    invoke-virtual {v2}, Lbnl;->b()Ljava/util/List;

    move-result-object v0

    .line 4767
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_11

    .line 4768
    const-string v1, "Babel"

    const/4 v3, 0x3

    invoke-static {v1, v3}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 4769
    const-string v1, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "processServerUpdate: sending "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " requests from processing "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 4770
    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 4769
    invoke-static {v1, v3}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 4772
    :cond_10
    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Lbki;->a(Ljava/util/Collection;I)V

    .line 4775
    :cond_11
    invoke-virtual {v2}, Lbnl;->e()I

    move-result v0

    .line 4776
    const-string v1, "Babel"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 4777
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "update should trigger notification? "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 4780
    :cond_12
    const/4 v1, 0x1

    if-ne v0, v1, :cond_13

    .line 4781
    const-string v0, "Babel"

    const-string v1, "Scheduling future notification after late push"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.apps.hangouts.DEFERRED_NOTIFICATION"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "op"

    const/16 v3, 0x8b

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "account_name"

    invoke-virtual {p1}, Lyj;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v2, 0x1

    const/16 v3, 0x6e

    const/4 v4, 0x0

    invoke-static {p1, v2, v3, v4}, Lbzb;->a(Lyj;IILjava/lang/String;)I

    move-result v2

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v3

    const/high16 v4, 0x10000000

    invoke-static {v3, v2, v1, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    const-string v5, "babel_latenotifdly"

    const-wide/16 v6, 0xbb8

    invoke-static {v5, v6, v7}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;J)J

    move-result-wide v5

    add-long/2addr v3, v5

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    goto/16 :goto_1

    .line 4783
    :cond_13
    invoke-static {p1, v0}, Lbne;->a(Lyj;I)V

    goto/16 :goto_1
.end method

.method public static a(Lyj;Lbph;)V
    .locals 2

    .prologue
    .line 5384
    if-eqz p1, :cond_1

    .line 5385
    invoke-virtual {p1}, Lbph;->d()Lbxl;

    move-result-object v0

    .line 5388
    :goto_0
    if-eqz v0, :cond_0

    .line 5392
    sget-object v1, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e:Lbop;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lbxl;->a:Landroid/content/Intent;

    if-eqz v1, :cond_2

    .line 5393
    sget-object v1, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e:Lbop;

    invoke-interface {v1, p0, v0}, Lbop;->a(Lyj;Lbxl;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5399
    :cond_0
    :goto_1
    return-void

    .line 5385
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 5397
    :cond_2
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lf;->a(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lbnr;

    invoke-direct {v0, p0}, Lbnr;-><init>(Lyj;)V

    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;->executeOnThreadPool(Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method public static a(Lyj;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1174
    const/16 v0, 0x4c

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Landroid/content/Intent;)V

    .line 1175
    return-void
.end method

.method public static a(Lyj;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 1581
    if-nez p0, :cond_0

    .line 1582
    const-string v0, "Babel"

    const-string v1, "resetNotifications called with an empty account, ignored"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1592
    :goto_0
    return-void

    .line 1586
    :cond_0
    const/16 v0, 0x28

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 1587
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1588
    const-string v1, "conversation_id_set"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1590
    :cond_1
    const-string v1, "notifications_target"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1591
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static a(Lyj;Ljava/lang/String;II)V
    .locals 8

    .prologue
    .line 2303
    if-lt p2, p3, :cond_0

    .line 2321
    :goto_0
    return-void

    .line 2307
    :cond_0
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v0, :cond_1

    .line 2308
    const-string v0, "Babel_Stress"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Scheduling stress message:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2311
    :cond_1
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "alarm"

    .line 2312
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 2313
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.apps.hangouts.MESSAGE_STRESS_TEST"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "op"

    const/16 v3, 0x98

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "account_name"

    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "conversation_id"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "stress_current_message_id"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "stress_max_message_id"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v2

    const/16 v3, 0x70

    invoke-static {v3}, Lbzb;->a(I)I

    move-result v3

    const/high16 v4, 0x10000000

    invoke-static {v2, v3, v1, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 2315
    const-string v2, "babel_stress_message_delay"

    const/16 v3, 0xa

    invoke-static {v2, v3}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v2

    .line 2318
    new-instance v3, Ljava/util/Random;

    invoke-direct {v3}, Ljava/util/Random;-><init>()V

    mul-int/lit16 v2, v2, 0x3e8

    invoke-virtual {v3, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    .line 2319
    const/4 v3, 0x2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    int-to-long v6, v2

    add-long/2addr v4, v6

    invoke-virtual {v0, v3, v4, v5, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method private a(Lyj;Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lyj;",
            "Ljava/util/List",
            "<",
            "Lbzo",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 4790
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    .line 4791
    invoke-virtual {p1}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lf;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v10}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 4793
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 4795
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzo;

    .line 4796
    iget-object v1, v0, Lbzo;->a:Ljava/io/Serializable;

    check-cast v1, Ljava/lang/Integer;

    invoke-static {v1, v10}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v6

    .line 4797
    iget-object v0, v0, Lbzo;->b:Ljava/io/Serializable;

    check-cast v0, Ljava/lang/Boolean;

    invoke-static {v0, v10}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v4

    .line 4799
    const-string v3, ""

    .line 4800
    packed-switch v6, :pswitch_data_0

    .line 4819
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v0, :cond_1

    .line 4820
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "RichPresenceEnabledStateNotification received for unhandled type: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    .line 4828
    :cond_1
    :goto_1
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4829
    iget-object v9, p0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->s:Landroid/os/Handler;

    new-instance v0, Lbol;

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, Lbol;-><init>(Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;Landroid/content/SharedPreferences;Ljava/lang/String;ZLyj;I)V

    invoke-virtual {v9, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 4802
    :pswitch_0
    sget v0, Lh;->kF:I

    .line 4803
    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 4806
    :pswitch_1
    sget v0, Lh;->kG:I

    .line 4807
    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 4813
    :pswitch_2
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v0, :cond_1

    .line 4814
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "RichPresenceEnabledStateNotification received for mood; nothingto do. enabled is set to "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    goto :goto_1

    .line 4833
    :cond_2
    return-void

    .line 4800
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Lyj;Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lyj;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1431
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v0, :cond_0

    .line 1432
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setFocusedConversation "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " account:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1433
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1432
    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1436
    :cond_0
    sget-object v1, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 1437
    :try_start_0
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->i:Ljava/lang/String;

    .line 1438
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, p1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j:Ljava/util/Set;

    .line 1439
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Lyj;Z)V
    .locals 2

    .prologue
    .line 1127
    const/16 v0, 0xd

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 1128
    const-string v1, "retry_request"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1129
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Landroid/content/Intent;)V

    .line 1130
    return-void
.end method

.method public static a(Lyj;ZZIZ)V
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2440
    invoke-static {p0}, Lbpf;->a(Lyj;)Lbpf;

    move-result-object v4

    .line 2441
    invoke-virtual {v4}, Lbpf;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2443
    if-eqz p2, :cond_2

    .line 2448
    invoke-virtual {v4}, Lbpf;->m()Z

    move-result v0

    move v1, v0

    .line 2450
    :goto_0
    invoke-virtual {v4, p3}, Lbpf;->b(I)J

    move-result-wide v4

    .line 2453
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v6, "alarm"

    .line 2454
    invoke-virtual {v0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 2456
    new-instance v6, Landroid/content/Intent;

    const-string v7, "com.google.android.apps.hangouts.SYNC"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2457
    const-string v7, "op"

    const/16 v8, 0x37

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2458
    const-string v7, "account_name"

    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2461
    const-string v7, "no_missed_events_expected"

    if-eqz p2, :cond_0

    if-nez v1, :cond_0

    move v2, v3

    :cond_0
    invoke-virtual {v6, v7, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2463
    const-string v1, "suppress_notifications"

    invoke-virtual {v6, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2464
    const-string v1, "from_background_polling"

    invoke-virtual {v6, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2465
    const/16 v1, 0x6d

    const/4 v2, 0x0

    invoke-static {p0, v3, v1, v2}, Lbzb;->a(Lyj;IILjava/lang/String;)I

    move-result v1

    .line 2468
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v2

    const/high16 v3, 0x10000000

    invoke-static {v2, v1, v6, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 2472
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v4, v5, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 2474
    :cond_1
    return-void

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public static a(Lyj;[J)V
    .locals 2

    .prologue
    .line 2006
    const/16 v0, 0x50

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 2007
    const-string v1, "message_row_ids"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 2008
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    .line 2009
    return-void
.end method

.method public static a(Lyj;[Ljava/lang/Long;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 1981
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    .line 1982
    const/16 v3, 0x2c

    invoke-static {p0, v3, p2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 1983
    const-string v4, "message_row_id"

    invoke-static {v2}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v5

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1984
    const-string v2, "timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    invoke-virtual {v3, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1985
    invoke-static {v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    .line 1981
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1987
    :cond_0
    invoke-static {p0, p2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->h(Lyj;Ljava/lang/String;)V

    .line 1988
    return-void
.end method

.method public static a(Lyj;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2027
    const/16 v0, 0xbb

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 2028
    const-string v1, "recent_call_row_ids"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 2029
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    .line 2030
    return-void
.end method

.method public static a(Lyt;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 5725
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "RealTimeChatService.notifyConversationLocated "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ==> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    .line 5727
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Lyj;Ljava/lang/String;Ljava/lang/String;)V

    .line 5737
    invoke-virtual {p0, p1, p1}, Lyt;->h(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    .line 5738
    if-eqz v0, :cond_0

    .line 5739
    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lyt;->f(J)V

    .line 5741
    :cond_0
    invoke-virtual {p0, p1, p2}, Lyt;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 5743
    new-instance v0, Lbnl;

    invoke-direct {v0}, Lbnl;-><init>()V

    .line 5744
    invoke-static {v0, p0, p2}, Lyp;->a(Lbnl;Lyt;Ljava/lang/String;)V

    .line 5745
    invoke-virtual {p0, p1}, Lyt;->i(Ljava/lang/String;)V

    .line 5748
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v1

    .line 5747
    invoke-static {v1}, Lbkb;->o(Lyj;)Lbki;

    move-result-object v1

    .line 5749
    invoke-virtual {v0}, Lbnl;->b()Ljava/util/List;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lbki;->a(Ljava/util/Collection;I)V

    .line 5750
    return-void
.end method

.method public static a(Lyt;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 5310
    if-eqz p2, :cond_0

    move v0, v1

    .line 5312
    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 5314
    invoke-virtual {p0}, Lyt;->a()V

    .line 5316
    if-eqz v0, :cond_1

    .line 5317
    :try_start_0
    array-length v5, p2

    move v3, v2

    :goto_1
    if-ge v3, v5, :cond_2

    aget-object v6, p2, v3

    .line 5319
    const/4 v7, 0x4

    const/4 v8, 0x0

    invoke-virtual {p0, p1, v6, v7, v8}, Lyt;->a(Ljava/lang/String;Ljava/lang/String;II)V

    .line 5321
    const-string v7, "-"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 5322
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 5317
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_0
    move v0, v2

    .line 5310
    goto :goto_0

    .line 5326
    :cond_1
    sget-wide v5, Lyp;->h:J

    invoke-virtual {p0, p1, v5, v6}, Lyt;->m(Ljava/lang/String;J)V

    .line 5328
    :cond_2
    invoke-virtual {p0, p1}, Lyt;->e(Ljava/lang/String;)Lyv;

    move-result-object v5

    .line 5329
    invoke-virtual {p0}, Lyt;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5331
    invoke-virtual {p0}, Lyt;->c()V

    .line 5333
    invoke-static {p0}, Lyp;->d(Lyt;)V

    .line 5335
    if-nez v5, :cond_3

    .line 5374
    :goto_2
    return-void

    .line 5331
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lyt;->c()V

    throw v0

    .line 5339
    :cond_3
    iget-object v3, v5, Lyv;->d:Ljava/lang/String;

    .line 5340
    if-nez v3, :cond_4

    .line 5341
    iget-object v3, v5, Lyv;->o:Ljava/lang/String;

    .line 5344
    :cond_4
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v6

    .line 5345
    new-instance v7, Lbk;

    invoke-direct {v7, v6}, Lbk;-><init>(Landroid/content/Context;)V

    .line 5348
    invoke-virtual {v7, v1}, Lbk;->a(Z)Lbk;

    .line 5349
    if-eqz v0, :cond_5

    sget v0, Lh;->bB:I

    .line 5351
    :goto_3
    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lbk;->a(Ljava/lang/CharSequence;)Lbk;

    .line 5352
    invoke-virtual {v7, v3}, Lbk;->b(Ljava/lang/CharSequence;)Lbk;

    .line 5353
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {v7, v0, v1}, Lbk;->a(J)Lbk;

    .line 5354
    sget v0, Lcom/google/android/apps/hangouts/R$drawable;->cI:I

    invoke-virtual {v7, v0}, Lbk;->a(I)Lbk;

    .line 5357
    invoke-virtual {p0}, Lyt;->f()Lyj;

    move-result-object v0

    iget v1, v5, Lyv;->b:I

    .line 5356
    invoke-static {v0, p1, v1}, Lbbl;->a(Lyj;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 5360
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 5362
    const/high16 v1, 0x10000000

    invoke-static {v6, v2, v0, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v7, v0}, Lbk;->a(Landroid/app/PendingIntent;)Lbk;

    .line 5368
    const-string v0, "notification"

    .line 5369
    invoke-virtual {v6, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 5370
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "failed_to_delete:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 5371
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xb

    .line 5373
    invoke-virtual {v7}, Lbk;->e()Landroid/app/Notification;

    move-result-object v3

    .line 5370
    invoke-virtual {v0, v1, v2, v3}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    goto :goto_2

    .line 5349
    :cond_5
    sget v0, Lh;->bA:I

    goto :goto_3
.end method

.method public static a(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1904
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v0, :cond_0

    .line 1905
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "requestWarmSyncForAllAccounts suppressNotifications: false fromBackgroundPolling: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    .line 1909
    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Lbkb;->f(Z)Ljava/util/ArrayList;

    move-result-object v0

    .line 1910
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1911
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    .line 1913
    if-eqz v0, :cond_1

    .line 1914
    invoke-static {v0, v2, v2, v2, p0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ZZIZ)V

    goto :goto_0

    .line 1919
    :cond_2
    return-void
.end method

.method public static a([B)V
    .locals 2

    .prologue
    .line 6166
    invoke-static {}, Lbkb;->m()Z

    move-result v0

    if-nez v0, :cond_0

    .line 6175
    :goto_0
    return-void

    .line 6171
    :cond_0
    invoke-static {}, Lbkb;->n()Lyj;

    move-result-object v0

    const/16 v1, 0x73

    .line 6170
    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 6173
    const-string v1, "mms_wap_push_data"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 6174
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    goto :goto_0
.end method

.method public static b(Lyj;I)I
    .locals 2

    .prologue
    .line 2701
    const/16 v0, 0x36

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 2702
    const-string v1, "conversation_sync_filter"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2703
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static b(Lyj;IZ)I
    .locals 2

    .prologue
    .line 2748
    const/16 v0, 0x79

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 2749
    const-string v1, "extra_rich_presence_type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2750
    const-string v1, "extra_rich_presence_type_enabled"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2751
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static b(Lyj;Ljava/lang/String;I)I
    .locals 2

    .prologue
    .line 2078
    const/16 v0, 0x31

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2079
    const-string v1, "typing_status"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2080
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static b(Lyj;Ljava/lang/String;J)I
    .locals 4

    .prologue
    .line 2196
    const/16 v0, 0x52

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2197
    const-string v1, "scroll_timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2198
    const-string v1, "scroll_to_item_timestamp"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2199
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static b(Lyj;Ljava/lang/String;Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 1346
    const/16 v0, 0xb8

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->d(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "hangout_id"

    .line 1347
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "broadcast_id"

    .line 1348
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1349
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static b(Lyj;Z)I
    .locals 2

    .prologue
    .line 1184
    const/16 v0, 0x12

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 1185
    const-string v1, "retry_request"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1186
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method private static b(I)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 650
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 651
    const-string v1, "op"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 652
    return-object v0
.end method

.method public static b()V
    .locals 3

    .prologue
    .line 1387
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1388
    const-string v1, "op"

    const/16 v2, 0x4a

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1389
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Landroid/content/Intent;)V

    .line 1390
    return-void
.end method

.method public static b(JJ)V
    .locals 7

    .prologue
    .line 2342
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "alarm"

    .line 2343
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 2344
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.apps.hangouts.RENEW_ACCOUNT_REGISTRATION"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "op"

    const/16 v3, 0x8d

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v2

    const/16 v3, 0x6f

    invoke-static {v3}, Lbzb;->a(I)I

    move-result v3

    const/high16 v4, 0x10000000

    invoke-static {v2, v3, v1, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 2347
    const/4 v1, 0x2

    move-wide v2, p0

    move-wide v4, p2

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V

    .line 2349
    return-void
.end method

.method public static b(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 6149
    invoke-static {}, Lbkb;->m()Z

    move-result v0

    if-nez v0, :cond_0

    .line 6158
    :goto_0
    return-void

    .line 6153
    :cond_0
    invoke-static {}, Lbkb;->n()Lyj;

    move-result-object v0

    .line 6154
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;

    invoke-virtual {p0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 6155
    const-string v1, "op"

    const/16 v2, 0x6d

    invoke-virtual {p0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 6156
    const-string v1, "account_name"

    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6157
    invoke-static {p0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    goto :goto_0
.end method

.method private b(Landroid/content/Intent;Lbos;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 5253
    iget-object v0, p0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->s:Landroid/os/Handler;

    new-instance v1, Lbon;

    invoke-direct {v1, p0, p1, p2, p3}, Lbon;-><init>(Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;Landroid/content/Intent;Lbos;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 5262
    return-void
.end method

.method public static b(Lbor;)V
    .locals 1

    .prologue
    .line 1035
    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->p:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 1036
    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2965
    const/16 v0, 0x67

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Ljava/lang/String;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Landroid/content/Intent;)V

    .line 2967
    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2974
    const/16 v0, 0x93

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Ljava/lang/String;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2975
    const-string v1, "message_text"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2976
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Landroid/content/Intent;)V

    .line 2977
    return-void
.end method

.method public static b(Lyj;)V
    .locals 1

    .prologue
    .line 1166
    const/16 v0, 0x10

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Landroid/content/Intent;)V

    .line 1169
    invoke-static {}, Lbmz;->a()Lbmz;

    move-result-object v0

    invoke-virtual {v0}, Lbmz;->f()V

    .line 1170
    return-void
.end method

.method public static b(Lyj;J)V
    .locals 2

    .prologue
    .line 1615
    const/16 v0, 0x5f

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 1616
    const-string v1, "last_seen_invite_timestamp"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1617
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Landroid/content/Intent;)V

    .line 1618
    return-void
.end method

.method public static b(Lyj;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1208
    const/16 v0, 0x45

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 1209
    const-string v1, "member_gaiaid"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1211
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Landroid/content/Intent;)V

    .line 1212
    return-void
.end method

.method public static b(Lyj;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2563
    const/16 v0, 0xaa

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 2564
    const-string v1, "conversationids"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 2566
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    .line 2567
    return-void
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 2998
    const/16 v0, 0x97

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 2999
    const-string v1, "phone_number"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3000
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static c(Lyj;J)I
    .locals 4

    .prologue
    .line 2178
    const/16 v0, 0x51

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 2179
    const-string v1, "scroll_timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2180
    const-string v1, "scroll_to_item_timestamp"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2181
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static c(Lyj;Ljava/lang/String;I)I
    .locals 2

    .prologue
    .line 2639
    const/16 v0, 0x26

    .line 2640
    invoke-static {p0, v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2641
    const-string v1, "notification_level"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2642
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static c(Lyj;Ljava/lang/String;J)I
    .locals 2

    .prologue
    .line 2231
    const/16 v0, 0x95

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2232
    const-string v1, "timestamp"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2233
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static c(Lyj;Z)I
    .locals 2

    .prologue
    .line 2527
    invoke-static {p0}, Lbqh;->a(Lyj;)Lbqh;

    move-result-object v0

    .line 2528
    if-nez p1, :cond_1

    invoke-virtual {v0}, Lbqh;->e()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lbqh;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2529
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v0, :cond_0

    .line 2530
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SyncBaselineSuggestedContactsOperation is idle: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2531
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2530
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    .line 2533
    :cond_0
    const/4 v0, -0x1

    .line 2538
    :goto_0
    return v0

    .line 2536
    :cond_1
    const/16 v0, 0x38

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 2537
    const-string v1, "force_execution"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2538
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    goto :goto_0
.end method

.method private static c(Lyj;I)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 674
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1057
    sget-object v1, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->m:Ljava/util/Map;

    monitor-enter v1

    .line 1058
    :try_start_0
    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->m:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 1059
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static c()V
    .locals 3

    .prologue
    .line 1396
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1397
    const-string v1, "op"

    const/16 v2, 0x7b

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1398
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Landroid/content/Intent;)V

    .line 1399
    return-void
.end method

.method public static c(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 6250
    invoke-static {}, Lbkb;->n()Lyj;

    move-result-object v0

    .line 6251
    if-nez v0, :cond_0

    .line 6258
    :goto_0
    return-void

    .line 6254
    :cond_0
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;

    invoke-virtual {p0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 6255
    const-string v1, "op"

    const/16 v2, 0x80

    invoke-virtual {p0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 6256
    const-string v1, "account_name"

    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6257
    invoke-static {p0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    goto :goto_0
.end method

.method public static c(Lyj;)V
    .locals 1

    .prologue
    .line 1476
    const/16 v0, 0xe

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Landroid/content/Intent;)V

    .line 1477
    return-void
.end method

.method public static c(Lyj;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1361
    const/16 v0, 0x55

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 1362
    const-string v1, "phone_number"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1363
    const-string v1, "sms_prefix"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1365
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Landroid/content/Intent;)V

    .line 1366
    return-void
.end method

.method public static c(Lyj;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2570
    const/16 v0, 0xab

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 2571
    const-string v1, "conversationids"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 2573
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    .line 2574
    return-void
.end method

.method public static c(Lyj;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 1455
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v0, :cond_0

    .line 1456
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isFocusedConversation "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j:Ljava/util/Set;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ?==? "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1458
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1456
    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1460
    :cond_0
    sget-object v1, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 1461
    :try_start_0
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->i:Ljava/lang/String;

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j:Ljava/util/Set;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j:Ljava/util/Set;

    .line 1463
    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1464
    const/4 v0, 0x1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1466
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 1467
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static d(Lyj;Ljava/lang/String;I)I
    .locals 2

    .prologue
    .line 2769
    const/16 v0, 0x3e

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2770
    const-string v1, "otr_status"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2771
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static d(Lyj;Z)I
    .locals 2

    .prologue
    .line 2756
    const/16 v0, 0x7d

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 2757
    const-string v1, "extra_rich_presence_type_enabled"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2758
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method private static d(Lyj;I)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 1223
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "account_name"

    .line 1224
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    .line 1225
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static d()V
    .locals 3

    .prologue
    .line 1443
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v0, :cond_0

    .line 1444
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unsetFocusedConversation from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j:Ljava/util/Set;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " account:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1447
    :cond_0
    sget-object v1, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 1448
    const/4 v0, 0x0

    :try_start_0
    sput-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->i:Ljava/lang/String;

    .line 1449
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j:Ljava/util/Set;

    .line 1450
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic d(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 180
    invoke-static {p0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Landroid/content/Intent;)V

    return-void
.end method

.method public static d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1485
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1486
    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1487
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Ljava/util/ArrayList;)V

    .line 1488
    return-void
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 6372
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 6373
    new-instance v1, Lbog;

    invoke-direct {v1, p0, p1}, Lbog;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 6381
    return-void
.end method

.method public static d(Lyj;)V
    .locals 2

    .prologue
    .line 1572
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ljava/lang/String;I)V

    .line 1573
    return-void
.end method

.method public static d(Lyj;J)V
    .locals 2

    .prologue
    .line 6365
    const/16 v0, 0xa9

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 6366
    const-string v1, "extra_duration"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 6367
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    .line 6368
    return-void
.end method

.method public static d(Lyj;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1609
    const/16 v0, 0x77

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 1610
    const-string v1, "mood_setting"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1611
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Landroid/content/Intent;)V

    .line 1612
    return-void
.end method

.method public static d(Lyj;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1370
    const/16 v0, 0x56

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 1371
    const-string v1, "phone_number"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1372
    const-string v1, "verification_code"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1374
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Landroid/content/Intent;)V

    .line 1375
    return-void
.end method

.method public static e(Lyj;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 1653
    const/16 v0, 0x8f

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static e(Lyj;Ljava/lang/String;Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 2624
    const/16 v0, 0x25

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2625
    const-string v1, "conversation_name"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2626
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static e()V
    .locals 4

    .prologue
    .line 1922
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v0, :cond_0

    .line 1923
    const-string v0, "requestPatchAfterRequestWriterUpgradeForAllAccounts"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    .line 1927
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Lbkb;->f(Z)Ljava/util/ArrayList;

    move-result-object v0

    .line 1928
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1929
    if-eqz v0, :cond_1

    .line 1930
    sget-boolean v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "requestPatchAfterRequestWriterUpgrade "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    :cond_2
    const/16 v2, 0x72

    invoke-static {v0, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    goto :goto_0

    .line 1933
    :cond_3
    return-void
.end method

.method private static e(Landroid/content/Intent;)V
    .locals 5

    .prologue
    .line 1092
    const-string v0, "rqtms"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1093
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->u:Z

    if-eqz v0, :cond_1

    .line 1094
    :cond_0
    const-string v0, "rqtns"

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1096
    :cond_1
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    .line 1097
    sget-object v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->q:Ljava/lang/Object;

    monitor-enter v2

    .line 1098
    :try_start_0
    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->r:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_3

    .line 1099
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v0, :cond_2

    .line 1100
    const-string v0, "initializing wakelock"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    .line 1102
    :cond_2
    const-string v0, "power"

    .line 1103
    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 1104
    const/4 v3, 0x1

    const-string v4, "hangouts_rtcs"

    invoke-virtual {v0, v3, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->r:Landroid/os/PowerManager$WakeLock;

    .line 1106
    :cond_3
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1107
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v0, :cond_4

    .line 1108
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "acquiring wakelock for opcode "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "op"

    const/4 v3, 0x0

    .line 1109
    invoke-virtual {p0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1108
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    .line 1111
    :cond_4
    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->r:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 1112
    const-class v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;

    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1113
    const-string v0, "pid"

    sget v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->d:I

    invoke-virtual {p0, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1114
    invoke-virtual {v1, p0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_5

    .line 1115
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RTCS failed to start service for intent "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116
    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->r:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 1118
    :cond_5
    return-void

    .line 1106
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public static e(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2033
    const-string v0, "\\|"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 2035
    array-length v1, v0

    const/4 v2, 0x4

    if-eq v1, v2, :cond_0

    .line 2036
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onRequestDiscarded invalid token: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 2043
    :goto_0
    return-void

    .line 2039
    :cond_0
    const/4 v1, 0x2

    aget-object v1, v0, v1

    const/16 v2, 0x71

    const/4 v3, 0x3

    aget-object v3, v0, v3

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Ljava/lang/String;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2041
    const-string v2, "message_row_id"

    const/4 v3, 0x1

    aget-object v0, v0, v3

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2042
    invoke-static {v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    goto :goto_0
.end method

.method public static e(Lyj;)V
    .locals 1

    .prologue
    .line 2016
    const/16 v0, 0xbc

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 2017
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    .line 2018
    return-void
.end method

.method private e(Lyj;J)V
    .locals 2

    .prologue
    .line 5265
    iget-object v0, p0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->s:Landroid/os/Handler;

    new-instance v1, Lbno;

    invoke-direct {v1, p0, p1, p2, p3}, Lbno;-><init>(Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;Lyj;J)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 5273
    return-void
.end method

.method private static f(Landroid/content/Intent;)I
    .locals 4

    .prologue
    .line 3059
    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    .line 3060
    const-string v1, "rid"

    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3061
    invoke-static {p0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Landroid/content/Intent;)V

    .line 3063
    sget-boolean v1, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v1, :cond_0

    .line 3064
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "start command request "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " opCode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "op"

    const/4 v3, 0x0

    .line 3065
    invoke-virtual {p0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 3064
    invoke-static {v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    .line 3067
    :cond_0
    return v0
.end method

.method public static f(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 2983
    const/16 v0, 0x94

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static f(Lyj;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 1786
    const/16 v0, 0x75

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static f(Lyj;Ljava/lang/String;Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 2647
    const/16 v0, 0xb1

    .line 2648
    invoke-static {p0, v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ringtone_uri"

    .line 2649
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2650
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static f()V
    .locals 2

    .prologue
    .line 2263
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v0, :cond_0

    .line 2264
    const-string v0, "cancelAllDBCleanupAlarms"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    .line 2266
    :cond_0
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    .line 2267
    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 2269
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->t()Landroid/app/PendingIntent;

    move-result-object v1

    .line 2270
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 2271
    return-void
.end method

.method public static f(Lyj;)V
    .locals 1

    .prologue
    .line 2849
    const/16 v0, 0x59

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Landroid/content/Intent;)V

    .line 2850
    return-void
.end method

.method public static g(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 2990
    const/16 v0, 0x96

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static g(Lyj;)I
    .locals 1

    .prologue
    .line 2926
    const/16 v0, 0x60

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static g(Lyj;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 1798
    const/16 v0, 0x21

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static g(Lyj;Ljava/lang/String;Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 2655
    const/16 v0, 0xb2

    .line 2656
    invoke-static {p0, v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ringtone_uri"

    .line 2657
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2658
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static g()V
    .locals 2

    .prologue
    .line 2384
    const-string v0, "Babel"

    const-string v1, "Canceling phone verification timeout alarm"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2386
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "alarm"

    .line 2387
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 2388
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->u()Landroid/app/PendingIntent;

    move-result-object v1

    .line 2389
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 2390
    return-void
.end method

.method private g(Landroid/content/Intent;)V
    .locals 22

    .prologue
    .line 3076
    const-string v2, "op"

    const/4 v3, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 3077
    const-string v3, "account_name"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 3078
    invoke-static {v5}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v3

    .line 3079
    const-string v4, "rid"

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .line 3081
    sget-boolean v4, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v4, :cond_0

    .line 3082
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "processIntent opCode "

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, " account "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, " requestId "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    .line 3086
    :cond_0
    sget-object v4, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f:Lboq;

    if-eqz v4, :cond_1

    .line 3087
    sget-object v4, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f:Lboq;

    move-object/from16 v0, p1

    invoke-interface {v4, v0}, Lboq;->a(Landroid/content/Intent;)V

    .line 3090
    :cond_1
    if-eqz v3, :cond_3

    .line 3092
    invoke-static {v3}, Lbkb;->o(Lyj;)Lbki;

    move-result-object v4

    .line 3100
    sparse-switch v2, :sswitch_data_0

    .line 3616
    packed-switch v2, :pswitch_data_0

    .line 4551
    :cond_2
    :goto_0
    :pswitch_0
    return-void

    .line 3094
    :cond_3
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Skipping intent for invalid account: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 3095
    invoke-static {v5}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 3094
    invoke-static {v2, v3}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 3104
    :sswitch_0
    const-string v2, "conversation_id_set"

    .line 3105
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3106
    invoke-static {v2}, Lbxz;->a(Ljava/lang/String;)Lbxz;

    move-result-object v2

    .line 3108
    const-string v4, "notifications_target"

    const/4 v5, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 3110
    and-int/lit8 v5, v4, 0x1

    if-eqz v5, :cond_4

    .line 3111
    new-instance v5, Lyt;

    invoke-direct {v5, v3}, Lyt;-><init>(Lyj;)V

    invoke-static {v5, v2}, Lyp;->a(Lyt;Lbxz;)V

    .line 3114
    :cond_4
    and-int/lit8 v5, v4, 0x2

    if-eqz v5, :cond_5

    .line 3115
    new-instance v5, Lyt;

    invoke-direct {v5, v3}, Lyt;-><init>(Lyj;)V

    invoke-static {v5, v2}, Lyp;->b(Lyt;Lbxz;)V

    .line 3118
    :cond_5
    and-int/lit8 v4, v4, 0x4

    if-eqz v4, :cond_2

    .line 3119
    new-instance v4, Lyt;

    invoke-direct {v4, v3}, Lyt;-><init>(Lyj;)V

    invoke-static {v4, v2}, Lyp;->c(Lyt;Lbxz;)V

    goto :goto_0

    .line 3125
    :sswitch_1
    invoke-static {v3}, Lbne;->c(Lyj;)V

    .line 3126
    invoke-static {v5}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->i(Ljava/lang/String;)V

    .line 3130
    invoke-static {}, Lf;->n()V

    .line 3131
    invoke-virtual {v3}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v2, v5, v6}, Lf;->a(Ljava/lang/String;II)V

    .line 3133
    invoke-static {}, Lbld;->c()Lbld;

    move-result-object v2

    invoke-virtual {v2}, Lbld;->d()Z

    move-result v2

    if-nez v2, :cond_6

    .line 3137
    const-string v2, "Babel"

    const-string v3, "GCM registration not done. Skip unregistering account"

    invoke-static {v2, v3}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 3141
    :cond_6
    sget-object v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 3142
    invoke-static {v3}, Lbnk;->d(Lyj;)Lbqm;

    move-result-object v2

    .line 3143
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3148
    :sswitch_2
    invoke-static {}, Lbld;->c()Lbld;

    move-result-object v2

    invoke-virtual {v2}, Lbld;->d()Z

    move-result v2

    if-nez v2, :cond_7

    .line 3149
    const-string v2, "Babel"

    const-string v3, "GCM registration not done before unregistering account"

    invoke-static {v2, v3}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3153
    :cond_7
    const-string v2, "account_gaiaids"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 3155
    sget-object v5, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 3156
    invoke-static {v3, v2}, Lbnk;->a(Lyj;Ljava/util/ArrayList;)Lbqn;

    move-result-object v2

    .line 3158
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3163
    :sswitch_3
    sget-boolean v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v2, :cond_8

    .line 3164
    const-string v2, "OP_REGISTER_ACCOUNT"

    invoke-static {v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    .line 3167
    :cond_8
    invoke-static {}, Lbld;->c()Lbld;

    move-result-object v2

    invoke-virtual {v2}, Lbld;->d()Z

    move-result v2

    if-nez v2, :cond_9

    .line 3168
    const-string v2, "Babel"

    const-string v3, "GCM registration not done before registering account"

    invoke-static {v2, v3}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3172
    :cond_9
    const-string v2, "retry_request"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 3173
    sget-object v5, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 3174
    invoke-static {v3, v2}, Lbnk;->b(Lyj;Z)Lbox;

    move-result-object v2

    .line 3175
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3180
    :sswitch_4
    sget-boolean v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v2, :cond_a

    .line 3181
    const-string v2, "OP_GET_SELF_INFO"

    invoke-static {v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    .line 3184
    :cond_a
    const-string v2, "retry_request"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 3185
    sget-object v5, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 3186
    invoke-static {v3, v2}, Lbnk;->a(Lyj;Z)Lblo;

    move-result-object v2

    .line 3187
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3192
    :sswitch_5
    sget-boolean v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v2, :cond_b

    .line 3193
    const-string v2, "OP_SET_SELF_INFO_BIT"

    invoke-static {v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    .line 3196
    :cond_b
    const-string v2, "setselfinfo_bit"

    const/4 v5, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 3197
    const/4 v5, -0x1

    if-ne v2, v5, :cond_c

    .line 3198
    const-string v2, "Babel"

    const-string v3, "OP_SET_SELF_INFO_BIT -- no EXTRA_SETSELFINO_BIT specified"

    invoke-static {v2, v3}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3201
    :cond_c
    const-string v5, "setselfinfo_bit_value"

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 3202
    sget-boolean v6, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v6, :cond_d

    .line 3203
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "OP_SET_SELF_INFO_BIT whichBit: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " value: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    .line 3206
    :cond_d
    sget-object v6, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 3207
    invoke-static {v3, v2, v5}, Lbnk;->a(Lyj;IZ)Lbqf;

    move-result-object v2

    .line 3208
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3213
    :sswitch_6
    invoke-static {}, Lbya;->b()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 3214
    new-instance v2, Lbyc;

    invoke-direct {v2}, Lbyc;-><init>()V

    const-string v4, "rtcs_handle_deferred_notification"

    .line 3215
    invoke-virtual {v2, v4}, Lbyc;->a(Ljava/lang/String;)Lbyc;

    move-result-object v2

    .line 3216
    invoke-virtual {v2, v3}, Lbyc;->a(Lyj;)Lbyc;

    move-result-object v2

    .line 3217
    invoke-virtual {v2}, Lbyc;->b()V

    .line 3219
    :cond_e
    sget-boolean v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v2, :cond_f

    .line 3220
    const-string v2, "handle OP_TRIGGER_DEFERRED_NOTIFICATION"

    invoke-static {v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    .line 3222
    :cond_f
    const/4 v2, 0x4

    invoke-static {v3, v2}, Lbne;->a(Lyj;I)V

    goto/16 :goto_0

    .line 3229
    :sswitch_7
    invoke-static {}, Lbya;->b()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 3230
    new-instance v2, Lbyc;

    invoke-direct {v2}, Lbyc;-><init>()V

    const-string v5, "rtcs_handle_warm_sync"

    .line 3231
    invoke-virtual {v2, v5}, Lbyc;->a(Ljava/lang/String;)Lbyc;

    move-result-object v2

    .line 3232
    invoke-virtual {v2, v3}, Lbyc;->a(Lyj;)Lbyc;

    move-result-object v2

    .line 3233
    invoke-virtual {v2}, Lbyc;->b()V

    .line 3235
    :cond_10
    invoke-static {v3}, Lbpf;->a(Lyj;)Lbpf;

    move-result-object v2

    .line 3237
    sget-boolean v5, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v5, :cond_11

    .line 3238
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "RequestWarmSyncOperation is executed directly: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 3239
    invoke-virtual {v3}, Lyj;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 3238
    invoke-static {v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    .line 3241
    :cond_11
    invoke-virtual {v2}, Lbpf;->n()V

    .line 3242
    const-string v3, "suppress_notifications"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 3244
    const-string v5, "no_missed_events_expected"

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 3246
    const-string v6, "from_background_polling"

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    .line 3248
    invoke-virtual {v2, v5}, Lbpf;->a(Z)V

    .line 3249
    invoke-virtual {v2, v3}, Lbpf;->b(Z)V

    .line 3250
    invoke-virtual {v2, v6}, Lbpf;->c(Z)V

    .line 3251
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lbpf;->a(I)V

    .line 3252
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3257
    :sswitch_8
    const-string v2, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3258
    new-instance v4, Lyt;

    invoke-direct {v4, v3}, Lyt;-><init>(Lyj;)V

    .line 3259
    invoke-virtual {v4, v2}, Lyt;->k(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3264
    :sswitch_9
    const-string v2, "last_seen_invite_timestamp"

    const-wide/16 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 3265
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->d()Lcom/google/android/apps/hangouts/phone/EsApplication;

    move-result-object v2

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/apps/hangouts/phone/EsApplication;->b(Lyj;J)V

    goto/16 :goto_0

    .line 3270
    :sswitch_a
    const-string v2, "dnd_expiration"

    const-wide/16 v5, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v5

    .line 3271
    sget-object v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 3272
    invoke-static {v3, v5, v6}, Lbnk;->b(Lyj;J)Lbpz;

    move-result-object v2

    .line 3273
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    .line 3274
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->d()Lcom/google/android/apps/hangouts/phone/EsApplication;

    move-result-object v2

    invoke-virtual {v2, v3, v5, v6}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Lyj;J)V

    .line 3275
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5, v6}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Lyj;J)V

    .line 3276
    const/4 v2, 0x1

    const/4 v4, 0x7

    invoke-static {v3, v2, v4}, Lbne;->a(Lyj;ZI)V

    goto/16 :goto_0

    .line 3282
    :sswitch_b
    const-string v2, "archive"

    const/4 v5, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 3283
    const-string v2, "perform_locally"

    const/4 v6, 0x0

    .line 3284
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    .line 3285
    const-string v2, "conversationids"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 3287
    const-string v2, "timestamps"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v8

    .line 3288
    array-length v2, v7

    array-length v9, v8

    if-eq v2, v9, :cond_12

    .line 3289
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Must have same number of conversation ids and timestamps to archive"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 3294
    :cond_12
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 3297
    const/4 v2, 0x0

    :goto_1
    array-length v10, v7

    if-ge v2, v10, :cond_13

    .line 3298
    new-instance v10, Lbjz;

    aget-object v11, v7, v2

    aget-wide v12, v8, v2

    invoke-direct {v10, v11, v12, v13}, Lbjz;-><init>(Ljava/lang/String;J)V

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3297
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 3303
    :cond_13
    sget-object v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 3304
    invoke-static {v3, v9, v5, v6}, Lbnk;->a(Lyj;Ljava/util/List;ZZ)Lbjy;

    move-result-object v2

    .line 3306
    const-string v5, "rid"

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    invoke-virtual {v2, v5}, Lbjy;->a(I)V

    .line 3307
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    .line 3308
    const/4 v2, 0x1

    const/4 v4, 0x7

    invoke-static {v3, v2, v4}, Lbne;->a(Lyj;ZI)V

    .line 3315
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v2

    .line 3316
    invoke-virtual {v3}, Lyj;->b()Ljava/lang/String;

    move-result-object v3

    .line 3315
    invoke-static {v2, v3}, Lcom/google/android/apps/hangouts/widget/BabelWidgetProvider;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3320
    :sswitch_c
    const-string v2, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3321
    const-string v5, "timestamp"

    const-wide/16 v6, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v5

    .line 3322
    sget-object v7, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 3323
    invoke-static {v3, v2, v5, v6}, Lbnk;->a(Lyj;Ljava/lang/String;J)Lbkt;

    move-result-object v2

    .line 3324
    const-string v5, "rid"

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    invoke-virtual {v2, v5}, Lbkt;->a(I)V

    .line 3325
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    .line 3326
    const/4 v2, 0x1

    const/4 v4, 0x7

    invoke-static {v3, v2, v4}, Lbne;->a(Lyj;ZI)V

    goto/16 :goto_0

    .line 3332
    :sswitch_d
    const-string v2, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3333
    const-string v4, "event_ids"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 3335
    new-instance v5, Lyt;

    invoke-direct {v5, v3}, Lyt;-><init>(Lyj;)V

    .line 3336
    invoke-static {v5, v2, v4}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyt;Ljava/lang/String;[Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3340
    :sswitch_e
    const-string v2, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3341
    new-instance v5, Lbnl;

    invoke-direct {v5}, Lbnl;-><init>()V

    .line 3342
    invoke-static {v3, v5, v2}, Lyp;->a(Lyj;Lbnl;Ljava/lang/String;)V

    .line 3344
    invoke-virtual {v5}, Lbnl;->b()Ljava/util/List;

    move-result-object v2

    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->m()I

    move-result v3

    invoke-virtual {v4, v2, v3}, Lbki;->a(Ljava/util/Collection;I)V

    goto/16 :goto_0

    .line 3348
    :sswitch_f
    const-string v2, "notification_type"

    const/4 v4, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 3350
    const-string v4, "is_sms"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 3351
    const/4 v4, 0x1

    invoke-static {v3, v4, v2}, Lbne;->a(Lyj;ZI)V

    goto/16 :goto_0

    .line 3361
    :sswitch_10
    new-instance v2, Lyt;

    invoke-direct {v2, v3}, Lyt;-><init>(Lyj;)V

    .line 3363
    const-string v3, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 3364
    const-string v4, "extra_pending_conversation_operations"

    const-wide/16 v5, 0x0

    .line 3365
    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 3367
    invoke-static {v2, v3, v4, v5}, Lyp;->b(Lyt;Ljava/lang/String;J)V

    goto/16 :goto_0

    .line 3375
    :sswitch_11
    sget-object v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 3376
    invoke-static {v3}, Lbnk;->a(Lyj;)Lbla;

    move-result-object v2

    .line 3377
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3382
    :sswitch_12
    const-string v2, "in_call_expiration_seconds"

    const/4 v5, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 3383
    const-string v5, "in_call_currently"

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 3385
    sget-object v6, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 3386
    invoke-static {v3, v5, v2}, Lbnk;->a(Lyj;ZI)Lbqb;

    move-result-object v2

    .line 3387
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3392
    :sswitch_13
    const-string v2, "mood_setting"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3395
    sget-object v5, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 3396
    invoke-static {v3, v2}, Lbnk;->k(Lyj;Ljava/lang/String;)Lbqd;

    move-result-object v5

    .line 3397
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v5}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    .line 3400
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->w(Lyj;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3405
    :sswitch_14
    const-string v2, "gaia_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3407
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_14

    .line 3408
    sget-object v5, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 3410
    invoke-static {v3, v2}, Lbnk;->a(Lyj;Ljava/lang/String;)Lbkw;

    move-result-object v2

    .line 3412
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3414
    :cond_14
    const-string v2, "Babel"

    const-string v3, "Dismiss suggested contacts operation scheduled without gaiaid"

    invoke-static {v2, v3}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3420
    :sswitch_15
    const-string v2, "member_gaiaid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3422
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_15

    .line 3423
    sget-object v5, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 3425
    invoke-static {v3, v2}, Lbnk;->b(Lyj;Ljava/lang/String;)Lbql;

    move-result-object v2

    .line 3427
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3429
    :cond_15
    const-string v2, "Babel"

    const-string v3, "Undismiss suggested contacts operation scheduled without gaiaid"

    invoke-static {v2, v3}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3436
    :sswitch_16
    const-string v2, "pdu"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v2

    .line 3437
    const-string v4, "is_sms_read"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 3438
    const/4 v5, 0x1

    new-array v5, v5, [Landroid/telephony/SmsMessage;

    const/4 v6, 0x0

    invoke-static {v2}, Landroid/telephony/SmsMessage;->createFromPdu([B)Landroid/telephony/SmsMessage;

    move-result-object v2

    aput-object v2, v5, v6

    .line 3439
    const/4 v2, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-static {v5, v3, v2, v6}, Lyp;->a([Landroid/telephony/SmsMessage;Lyj;ILjava/lang/Boolean;)V

    .line 3441
    if-nez v4, :cond_2

    .line 3442
    const-wide/16 v4, 0x0

    const/4 v2, 0x7

    const/4 v6, 0x1

    invoke-static {v3, v4, v5, v2, v6}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;JIZ)V

    goto/16 :goto_0

    .line 3449
    :sswitch_17
    const-string v2, "extra_rich_presence_type"

    const/4 v5, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 3450
    if-ltz v2, :cond_16

    const-string v5, "extra_rich_presence_type_enabled"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_18

    .line 3451
    :cond_16
    sget-boolean v3, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v3, :cond_2

    .line 3452
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SetRichPresenceEnabledState failed."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-gez v2, :cond_17

    const-string v2, " Invalid rich presence type."

    :goto_2
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_17
    const-string v2, " Unknown if enabled."

    goto :goto_2

    .line 3458
    :cond_18
    const-string v5, "extra_rich_presence_type_enabled"

    const/4 v6, 0x0

    .line 3459
    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 3461
    new-instance v6, Ljava/util/ArrayList;

    const/4 v7, 0x1

    invoke-direct {v6, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 3463
    new-instance v7, Lbzo;

    .line 3464
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-direct {v7, v2, v5}, Lbzo;-><init>(Ljava/io/Serializable;Ljava/io/Serializable;)V

    .line 3463
    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3466
    sget-object v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 3468
    invoke-static {v3, v6}, Lbnk;->a(Lyj;Ljava/util/List;)Lbqe;

    move-result-object v2

    .line 3471
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3476
    :sswitch_18
    const-string v2, "extra_rich_presence_type_enabled"

    const/4 v5, 0x0

    .line 3477
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 3479
    new-instance v5, Ljava/util/ArrayList;

    const/4 v6, 0x2

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 3482
    new-instance v6, Lbzo;

    const/4 v7, 0x2

    .line 3483
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lbzo;-><init>(Ljava/io/Serializable;Ljava/io/Serializable;)V

    .line 3482
    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3484
    new-instance v6, Lbzo;

    const/4 v7, 0x1

    .line 3485
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v6, v7, v2}, Lbzo;-><init>(Ljava/io/Serializable;Ljava/io/Serializable;)V

    .line 3484
    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3487
    sget-object v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 3488
    invoke-static {v3, v5}, Lbnk;->a(Lyj;Ljava/util/List;)Lbqe;

    move-result-object v2

    .line 3491
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3496
    :sswitch_19
    const-string v2, "participant_entity"

    .line 3497
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lbdh;

    .line 3498
    new-instance v4, Lyt;

    invoke-direct {v4, v3}, Lyt;-><init>(Lyj;)V

    .line 3499
    const/4 v3, 0x1

    invoke-virtual {v4, v2, v3}, Lyt;->a(Lbdh;Z)Z

    goto/16 :goto_0

    .line 3504
    :sswitch_1a
    new-instance v2, Lyt;

    invoke-direct {v2, v3}, Lyt;-><init>(Lyj;)V

    .line 3505
    const-string v3, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 3509
    invoke-virtual {v2}, Lyt;->e()Lzr;

    move-result-object v4

    const-string v5, "SELECT count(*) from messages WHERE conversation_id=? LIMIT 1"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    invoke-virtual {v4, v5, v6}, Lzr;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 3517
    :try_start_0
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_19

    .line 3518
    const/4 v5, 0x0

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 3519
    if-nez v5, :cond_19

    .line 3520
    invoke-virtual {v2, v3}, Lyt;->B(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3524
    :cond_19
    if-eqz v4, :cond_2

    .line 3525
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 3524
    :catchall_0
    move-exception v2

    if-eqz v4, :cond_1a

    .line 3525
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    :cond_1a
    throw v2

    .line 3532
    :sswitch_1b
    const-string v2, "recent_call_action_info"

    .line 3533
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lbkn;

    .line 3534
    const-string v4, "recent_call_type"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 3536
    invoke-static {v3, v4, v2}, Lf;->a(Lyj;ILbkn;)V

    goto/16 :goto_0

    .line 3541
    :sswitch_1c
    invoke-static {v3}, Lf;->a(Lyj;)V

    goto/16 :goto_0

    .line 3546
    :sswitch_1d
    const-string v2, "recent_call_row_ids"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 3547
    invoke-static {v3, v2}, Lf;->a(Lyj;[Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3552
    :sswitch_1e
    new-instance v2, Lyt;

    invoke-direct {v2, v3}, Lyt;-><init>(Lyj;)V

    .line 3553
    const-string v3, "request_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 3554
    invoke-virtual {v2, v3}, Lyt;->z(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3559
    :sswitch_1f
    new-instance v2, Lyt;

    invoke-direct {v2, v3}, Lyt;-><init>(Lyj;)V

    .line 3560
    const-string v3, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 3561
    invoke-virtual {v2, v3}, Lyt;->h(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3566
    :sswitch_20
    sget-boolean v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v2, :cond_1b

    .line 3567
    const-string v2, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "pollParasiteOperations for account "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 3568
    invoke-virtual {v3}, Lyj;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 3567
    invoke-static {v2, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3570
    :cond_1b
    invoke-static {v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Lyj;)V

    .line 3571
    invoke-static {v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->k(Lyj;)V

    .line 3572
    const/4 v2, 0x0

    invoke-static {v3, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;Z)I

    .line 3573
    invoke-static {v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->n(Lyj;)V

    .line 3574
    invoke-static {v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->l(Lyj;)V

    .line 3575
    invoke-static {v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->m(Lyj;)V

    .line 3578
    const/4 v2, 0x1

    invoke-static {v2}, Lbkb;->f(Z)Ljava/util/ArrayList;

    move-result-object v2

    .line 3579
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1c
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 3581
    invoke-static {v2}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v2

    .line 3583
    invoke-static {v2}, Lbpf;->a(Lyj;)Lbpf;

    move-result-object v4

    .line 3585
    invoke-virtual {v4}, Lbpf;->f()Z

    move-result v4

    if-eqz v4, :cond_1c

    .line 3589
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x2

    const/4 v7, 0x0

    invoke-static {v2, v4, v5, v6, v7}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ZZIZ)V

    goto :goto_3

    .line 3600
    :sswitch_21
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->s:Landroid/os/Handler;

    new-instance v3, Lboj;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lboj;-><init>(Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 3618
    :pswitch_1
    :try_start_1
    const-string v2, "server_response"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v2

    .line 3619
    invoke-static {v2}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->c([B)Lbfz;

    move-result-object v2

    .line 3621
    if-nez v2, :cond_1d

    .line 3622
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "no server response handling OP_RECEIVE_RESPONSE"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 4542
    :catch_0
    move-exception v2

    .line 4543
    const-string v3, "Babel"

    const-string v4, "Exception in processIntent"

    invoke-static {v3, v4, v2}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 4544
    new-instance v3, Lbos;

    const-string v4, "rid"

    const/4 v5, 0x0

    .line 4545
    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-direct {v3, v4, v5, v6}, Lbos;-><init>(IILbfz;)V

    const/4 v4, 0x0

    .line 4544
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v4}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Landroid/content/Intent;Lbos;Ljava/lang/Object;)V

    .line 4547
    instance-of v3, v2, Ljava/lang/RuntimeException;

    if-eqz v3, :cond_2

    .line 4548
    check-cast v2, Ljava/lang/RuntimeException;

    throw v2

    .line 3625
    :cond_1d
    if-eqz v2, :cond_2

    .line 3626
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    const-wide/16 v9, 0x3e8

    mul-long/2addr v7, v9

    invoke-virtual {v2, v7, v8}, Lbfz;->c(J)V

    .line 3627
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Lbki;Lbfz;)Ljava/util/List;

    move-result-object v2

    .line 3629
    new-instance v3, Lbos;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct {v3, v6, v4, v5}, Lbos;-><init>(IILbfz;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Landroid/content/Intent;Lbos;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 3636
    :pswitch_2
    const-string v2, "simulated_event_msg_num"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3638
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 3639
    sget-object v6, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->k:Landroid/util/SparseArray;

    monitor-enter v6
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 3640
    :try_start_3
    const-string v7, "\\|"

    invoke-virtual {v2, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    array-length v8, v7

    const/4 v2, 0x0

    :goto_4
    if-ge v2, v8, :cond_1e

    aget-object v9, v7, v2

    .line 3641
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    .line 3642
    sget-object v10, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->k:Landroid/util/SparseArray;

    const/4 v11, 0x0

    invoke-virtual {v10, v9, v11}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3643
    sget-object v10, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->k:Landroid/util/SparseArray;

    invoke-virtual {v10, v9}, Landroid/util/SparseArray;->remove(I)V

    .line 3640
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 3645
    :cond_1e
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 3647
    :try_start_4
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_5
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lbiq;

    .line 3648
    const-wide/16 v6, 0x0

    const-wide/16 v8, 0x0

    const-wide/16 v10, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v11}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Lbki;Lbiq;JJJ)V

    goto :goto_5

    .line 3645
    :catchall_1
    move-exception v2

    monitor-exit v6

    throw v2

    .line 3654
    :pswitch_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    const-wide/16 v7, 0x3e8

    mul-long v10, v5, v7

    .line 3656
    const-string v2, "payload"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3659
    invoke-static {}, Lbya;->b()Z

    move-result v5

    if-eqz v5, :cond_1f

    .line 3660
    new-instance v5, Lbyc;

    invoke-direct {v5}, Lbyc;-><init>()V

    const-string v6, "recv_server_update"

    .line 3661
    invoke-virtual {v5, v6}, Lbyc;->a(Ljava/lang/String;)Lbyc;

    move-result-object v5

    .line 3662
    invoke-virtual {v5, v3}, Lbyc;->a(Lyj;)Lbyc;

    move-result-object v5

    .line 3663
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v5, v6}, Lbyc;->c(I)Lbyc;

    move-result-object v5

    .line 3664
    invoke-virtual {v5}, Lbyc;->b()V

    .line 3668
    :cond_1f
    invoke-static {v2, v3}, Lbiq;->a(Ljava/lang/String;Lyj;)Ljava/util/List;

    move-result-object v2

    .line 3670
    invoke-static {}, Lbya;->b()Z

    move-result v5

    if-eqz v5, :cond_20

    .line 3671
    invoke-static {}, Lbya;->c()V

    .line 3674
    :cond_20
    if-eqz v2, :cond_22

    .line 3675
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_6
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lbiq;

    .line 3676
    instance-of v2, v5, Lbiv;

    if-eqz v2, :cond_21

    .line 3677
    move-object v0, v5

    check-cast v0, Lbiv;

    move-object v2, v0

    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Lbiv;->a(I)V

    .line 3680
    :cond_21
    const-string v2, "timestamp"

    const-wide/16 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    .line 3681
    const-string v2, "gcm_handle_timestamps"

    const-wide/16 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v8

    move-object/from16 v2, p0

    .line 3682
    invoke-direct/range {v2 .. v11}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Lbki;Lbiq;JJJ)V

    goto :goto_6

    .line 3685
    :cond_22
    const-string v2, "Babel"

    const-string v3, "could not parse ServerUpdate"

    invoke-static {v2, v3}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3691
    :pswitch_4
    const-string v2, "phone_number"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3692
    const-string v5, "sms_prefix"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 3693
    sget-object v6, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 3694
    invoke-static {v3, v2, v5}, Lbnk;->e(Lyj;Ljava/lang/String;Ljava/lang/String;)Lbqg;

    move-result-object v2

    .line 3695
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3700
    :pswitch_5
    const-string v2, "broadcast"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v2

    .line 3701
    sget-object v5, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 3702
    invoke-static {v3, v2}, Lbnk;->b(Lyj;[B)Lbjx;

    move-result-object v2

    .line 3703
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3708
    :pswitch_6
    const-string v2, "hangout_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3709
    const-string v5, "broadcast_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 3710
    sget-object v6, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 3711
    invoke-static {v3, v5, v2}, Lbnk;->f(Lyj;Ljava/lang/String;Ljava/lang/String;)Lblf;

    move-result-object v2

    .line 3712
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3717
    :pswitch_7
    const-string v2, "broadcast"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v2

    .line 3718
    const-string v5, "sync_metadata"

    .line 3719
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v5

    .line 3720
    sget-object v6, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 3721
    invoke-static {v3, v2, v5}, Lbnk;->a(Lyj;[B[B)Lbmp;

    move-result-object v2

    .line 3723
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3728
    :pswitch_8
    const-string v2, "broadcast_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3729
    const-string v5, "hangout_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 3730
    sget-object v6, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 3731
    invoke-static {v3, v2, v5}, Lbnk;->g(Lyj;Ljava/lang/String;Ljava/lang/String;)Lboy;

    move-result-object v2

    .line 3732
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3737
    :pswitch_9
    const-string v2, "phone_number"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3738
    const-string v5, "verification_code"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 3740
    sget-object v6, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 3741
    invoke-static {v3, v2, v5}, Lbnk;->b(Lyj;Ljava/lang/String;Ljava/lang/String;)Lbkz;

    move-result-object v2

    .line 3743
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3749
    :pswitch_a
    const-string v2, "audience"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v6

    check-cast v6, Lxm;

    .line 3750
    sget-object v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    const-string v2, "omit_conversation_lookup"

    const/4 v5, 0x0

    .line 3753
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    const-string v2, "conversation_hangout"

    const/4 v5, 0x0

    .line 3754
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    const-string v2, "force_group"

    const/4 v5, 0x0

    .line 3755
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    const-string v2, "transport_type"

    const/4 v5, 0x0

    .line 3756
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    move-object v5, v3

    .line 3751
    invoke-static/range {v5 .. v10}, Lbnk;->a(Lyj;Lxm;ZZZI)Lbnj;

    move-result-object v2

    .line 3758
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3763
    :pswitch_b
    sget-object v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    const-string v2, "conversation_id"

    .line 3765
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3764
    invoke-static {v3, v2}, Lbnk;->h(Lyj;Ljava/lang/String;)Lbot;

    move-result-object v2

    .line 3766
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3771
    :pswitch_c
    const-string v2, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 3773
    const-string v2, "message_text"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 3774
    const-string v2, "uri"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 3775
    const-string v2, "picasa_photo_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 3776
    const-string v2, "rotation"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    .line 3777
    const-string v2, "width"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v12

    .line 3778
    const-string v2, "height"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v13

    .line 3779
    const-string v2, "content_type"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 3780
    const-string v2, "subject"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 3781
    const-string v2, "requires_mms"

    const/4 v5, 0x0

    .line 3782
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v16

    .line 3783
    const-string v2, "marker_options"

    .line 3784
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v17

    check-cast v17, Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 3785
    const-string v2, "camera_position"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v18

    check-cast v18, Lcom/google/android/gms/maps/model/CameraPosition;

    .line 3787
    const-string v2, "timestamp"

    const-wide/16 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v19

    .line 3788
    const-string v2, "otr_state"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v21

    .line 3791
    new-instance v5, Lbps;

    move-object v6, v3

    invoke-direct/range {v5 .. v21}, Lbps;-><init>(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IILjava/lang/String;Ljava/lang/String;ZLcom/google/android/gms/maps/model/MarkerOptions;Lcom/google/android/gms/maps/model/CameraPosition;JI)V

    .line 3796
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v5}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3801
    :pswitch_d
    const-string v2, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 3803
    const-string v2, "stress_current_message_id"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v17

    .line 3805
    const-string v2, "stress_max_message_id"

    const/4 v5, 0x0

    .line 3806
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v18

    .line 3808
    sget-boolean v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v2, :cond_23

    .line 3809
    const-string v2, "Babel_Stress"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Sending stress message from RealTimeChatService:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3813
    :cond_23
    invoke-static/range {v17 .. v17}, Lf;->e(I)Ljava/lang/String;

    move-result-object v5

    .line 3815
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v6, Lbqz;

    .line 3814
    invoke-static {v2, v6}, Lcyj;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbqz;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    .line 3816
    invoke-interface/range {v2 .. v16}, Lbqz;->a(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IILjava/lang/String;Ljava/lang/String;ZLcom/google/android/gms/maps/model/MarkerOptions;Lcom/google/android/gms/maps/model/CameraPosition;I)V

    .line 3819
    add-int/lit8 v2, v17, 0x1

    .line 3820
    move/from16 v0, v18

    invoke-static {v3, v4, v2, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ljava/lang/String;II)V

    goto/16 :goto_0

    .line 3825
    :pswitch_e
    const-string v2, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 3827
    const-string v2, "uri"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 3828
    const-string v2, "subject"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 3829
    const-string v2, "draft_attachment_count"

    const/4 v5, 0x0

    .line 3830
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    .line 3831
    new-instance v5, Lbps;

    move-object v6, v3

    invoke-direct/range {v5 .. v10}, Lbps;-><init>(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 3833
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v5}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3838
    :pswitch_f
    const-string v2, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3839
    const-string v5, "message_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 3840
    const-string v6, "error"

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .line 3841
    const-string v7, "Babel"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "OP_SET_MESSAGE_FAILED: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3843
    sget-object v7, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 3844
    invoke-static {v3, v2, v5, v6}, Lbnk;->a(Lyj;Ljava/lang/String;Ljava/lang/String;I)Lbqc;

    move-result-object v2

    .line 3846
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3851
    :pswitch_10
    const-string v2, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 3852
    const-string v2, "insert_error_message"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 3855
    new-instance v2, Lyt;

    invoke-direct {v2, v3}, Lyt;-><init>(Lyj;)V

    .line 3856
    invoke-virtual {v2, v4}, Lyt;->E(Ljava/lang/String;)V

    .line 3862
    invoke-virtual {v2, v4}, Lyt;->Y(Ljava/lang/String;)J

    move-result-wide v6

    .line 3863
    invoke-static {v2, v4, v6, v7}, Lyp;->a(Lyt;Ljava/lang/String;J)V

    .line 3865
    const/4 v3, 0x4

    invoke-virtual {v2, v4, v3}, Lyt;->e(Ljava/lang/String;I)V

    .line 3868
    if-eqz v5, :cond_2

    .line 3870
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    const-wide/16 v7, 0x3e8

    mul-long v6, v5, v7

    .line 3871
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 3874
    const-wide/16 v8, -0x1

    move-object v5, v4

    invoke-static/range {v2 .. v9}, Lyp;->a(Lyt;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;JJ)V

    goto/16 :goto_0

    .line 3881
    :pswitch_11
    const-string v2, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 3882
    const-string v2, "message_row_id"

    const-wide/16 v5, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v8

    .line 3883
    const-string v2, "timestamp"

    const-wide/16 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v10

    .line 3884
    new-instance v5, Lbps;

    move-object v6, v3

    invoke-direct/range {v5 .. v11}, Lbps;-><init>(Lyj;Ljava/lang/String;JJ)V

    .line 3886
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v5}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3891
    :pswitch_12
    const-string v2, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 3892
    invoke-static {v3, v6}, Lf;->a(Lyj;Ljava/lang/String;)Lxm;

    move-result-object v7

    .line 3895
    const/4 v5, 0x1

    .line 3896
    invoke-virtual {v3}, Lyj;->c()Lbdk;

    move-result-object v2

    iget-object v8, v2, Lbdk;->a:Ljava/lang/String;

    .line 3897
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 3898
    invoke-virtual {v7}, Lxm;->b()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_7
    if-ltz v2, :cond_25

    .line 3899
    invoke-virtual {v7, v2}, Lxm;->a(I)Lxs;

    move-result-object v10

    invoke-virtual {v10}, Lxs;->h()Lbdh;

    move-result-object v10

    .line 3901
    invoke-virtual {v10}, Lbdh;->a()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_24

    .line 3902
    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3898
    :cond_24
    add-int/lit8 v2, v2, -0x1

    goto :goto_7

    .line 3905
    :cond_25
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v2

    const/4 v8, 0x1

    if-le v2, v8, :cond_26

    .line 3906
    const/4 v5, 0x2

    .line 3909
    :cond_26
    invoke-virtual {v7}, Lxm;->d()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_8
    if-ltz v2, :cond_27

    .line 3910
    invoke-virtual {v7, v2}, Lxm;->b(I)Lxo;

    move-result-object v5

    invoke-virtual {v5}, Lxo;->b()Ljava/lang/String;

    move-result-object v5

    .line 3911
    invoke-virtual {v7, v2}, Lxm;->b(I)Lxo;

    move-result-object v8

    invoke-virtual {v8}, Lxo;->d()Ljava/lang/String;

    move-result-object v8

    .line 3910
    invoke-static {v5, v8}, Lbdh;->a(Ljava/lang/String;Ljava/lang/String;)Lbdh;

    move-result-object v5

    invoke-interface {v9, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3912
    const/4 v5, 0x2

    .line 3909
    add-int/lit8 v2, v2, -0x1

    goto :goto_8

    .line 3915
    :cond_27
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 3917
    new-instance v7, Lbek;

    new-instance v8, Lbee;

    const/4 v10, 0x0

    invoke-direct {v8, v6, v5, v10, v9}, Lbee;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/util/List;)V

    invoke-direct {v7, v8}, Lbek;-><init>(Lbee;)V

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3920
    new-instance v5, Lbok;

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v3, v2}, Lbok;-><init>(Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;Lyj;Ljava/util/List;)V

    .line 3927
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v5}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3932
    :pswitch_13
    const-string v2, "message_row_ids"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v2

    .line 3933
    sget-object v5, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 3934
    invoke-static {v3, v2}, Lbnk;->a(Lyj;[J)Lbkv;

    move-result-object v2

    .line 3935
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3940
    :pswitch_14
    const-string v2, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3941
    const-string v4, "message_row_id"

    const-wide/16 v5, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 3942
    new-instance v6, Lyt;

    invoke-direct {v6, v3}, Lyt;-><init>(Lyj;)V

    .line 3945
    invoke-static {v6, v4, v5}, Lyp;->a(Lyt;J)V

    .line 3946
    invoke-static {v6, v2}, Lyp;->d(Lyt;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3952
    :pswitch_15
    const-string v2, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 3954
    const-string v2, "audience"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lxm;

    .line 3956
    sget-object v6, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 3957
    invoke-static {v3, v5, v2}, Lbnk;->a(Lyj;Ljava/lang/String;Lxm;)Lbmb;

    move-result-object v2

    .line 3958
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3963
    :pswitch_16
    const-string v2, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3965
    sget-object v5, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 3966
    invoke-static {v3, v2}, Lbnk;->g(Lyj;Ljava/lang/String;)Lbmc;

    move-result-object v2

    .line 3967
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3972
    :pswitch_17
    const-string v2, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3974
    sget-object v5, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 3975
    invoke-static {v3, v2}, Lbnk;->m(Lyj;Ljava/lang/String;)Lbqq;

    move-result-object v2

    .line 3976
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3981
    :pswitch_18
    const-string v2, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3982
    const-string v4, "call_media_type"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 3984
    new-instance v5, Lyt;

    invoke-direct {v5, v3}, Lyt;-><init>(Lyj;)V

    .line 3986
    invoke-virtual {v5, v2}, Lyt;->Q(Ljava/lang/String;)J

    move-result-wide v6

    .line 3985
    invoke-virtual {v5, v4, v6, v7, v2}, Lyt;->a(IJLjava/lang/String;)V

    goto/16 :goto_0

    .line 3991
    :pswitch_19
    const-string v2, "scroll_timestamp"

    const-wide/16 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 3992
    const-string v2, "scroll_to_item_timestamp"

    const-wide/16 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    .line 3995
    new-instance v2, Lyt;

    invoke-direct {v2, v3}, Lyt;-><init>(Lyj;)V

    .line 3996
    invoke-virtual {v2, v4, v5, v6, v7}, Lyt;->a(JJ)V

    goto/16 :goto_0

    .line 4002
    :pswitch_1a
    const-string v2, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 4003
    const-string v2, "scroll_timestamp"

    const-wide/16 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 4004
    const-string v2, "scroll_to_item_timestamp"

    const-wide/16 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    .line 4007
    new-instance v2, Lyt;

    invoke-direct {v2, v3}, Lyt;-><init>(Lyj;)V

    move-object v3, v8

    .line 4008
    invoke-virtual/range {v2 .. v7}, Lyt;->b(Ljava/lang/String;JJ)V

    goto/16 :goto_0

    .line 4014
    :pswitch_1b
    const-string v2, "hangout_invite_receipt"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v2

    .line 4016
    sget-object v5, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 4017
    invoke-static {v3, v2}, Lbnk;->a(Lyj;[B)Lbqa;

    move-result-object v2

    .line 4019
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4024
    :pswitch_1c
    const-string v2, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 4025
    const-string v2, "message_row_id"

    const-wide/16 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v5

    .line 4026
    sget-object v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 4027
    invoke-static {v3, v5, v6}, Lbnk;->a(Lyj;J)Lboz;

    move-result-object v2

    .line 4028
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4033
    :pswitch_1d
    const-string v2, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4035
    const-string v5, "conversation_name"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 4036
    sget-object v6, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 4037
    invoke-static {v3, v2}, Lbnk;->l(Lyj;Ljava/lang/String;)Lbqo;

    move-result-object v2

    .line 4038
    invoke-virtual {v2, v5}, Lbqo;->a(Ljava/lang/String;)V

    .line 4039
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4044
    :pswitch_1e
    const-string v2, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4045
    const-string v5, "notification_level"

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 4046
    new-instance v6, Lyt;

    invoke-direct {v6, v3}, Lyt;-><init>(Lyj;)V

    .line 4048
    invoke-virtual {v6, v2}, Lyt;->ag(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_9
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 4049
    sget-object v7, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 4050
    invoke-static {v3, v2}, Lbnk;->l(Lyj;Ljava/lang/String;)Lbqo;

    move-result-object v2

    .line 4051
    invoke-virtual {v2, v5}, Lbqo;->a(I)V

    .line 4052
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto :goto_9

    .line 4058
    :pswitch_1f
    const-string v2, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4059
    const-string v5, "ringtone_uri"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 4060
    sget-object v6, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 4061
    invoke-static {v3, v2}, Lbnk;->l(Lyj;Ljava/lang/String;)Lbqo;

    move-result-object v2

    .line 4062
    invoke-virtual {v2, v5}, Lbqo;->b(Ljava/lang/String;)V

    .line 4063
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4068
    :pswitch_20
    const-string v2, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4069
    const-string v5, "ringtone_uri"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 4070
    sget-object v6, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 4071
    invoke-static {v3, v2}, Lbnk;->l(Lyj;Ljava/lang/String;)Lbqo;

    move-result-object v2

    .line 4072
    invoke-virtual {v2, v5}, Lbqo;->c(Ljava/lang/String;)V

    .line 4073
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4078
    :pswitch_21
    const-string v2, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4080
    const-string v5, "accept"

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 4081
    const-string v6, "report_inviter"

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    .line 4082
    const-string v7, "block_inviter"

    const/4 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    .line 4083
    sget-object v8, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 4084
    invoke-static {v3, v2, v5, v6, v7}, Lbnk;->a(Lyj;Ljava/lang/String;ZZZ)Lbpa;

    move-result-object v2

    .line 4086
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4091
    :pswitch_22
    const-string v2, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4093
    sget-object v5, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 4094
    invoke-static {v3, v2}, Lbnk;->i(Lyj;Ljava/lang/String;)Lbpe;

    move-result-object v2

    .line 4095
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4100
    :pswitch_23
    const-string v2, "conversation_sync_filter"

    const/4 v5, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 4102
    sget-object v5, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 4103
    invoke-static {v3, v2}, Lbnk;->a(Lyj;I)Lbpd;

    move-result-object v2

    .line 4104
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4109
    :pswitch_24
    const-string v2, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4111
    new-instance v5, Lyt;

    invoke-direct {v5, v3}, Lyt;-><init>(Lyj;)V

    .line 4112
    invoke-virtual {v5, v2}, Lyt;->s(Ljava/lang/String;)I

    move-result v6

    .line 4115
    const/4 v7, 0x2

    if-eq v6, v7, :cond_28

    const-wide/16 v5, 0x0

    .line 4117
    :goto_a
    sget-object v7, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 4118
    invoke-static {v3, v2, v5, v6}, Lbnk;->b(Lyj;Ljava/lang/String;J)Lbpc;

    move-result-object v2

    .line 4120
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4116
    :cond_28
    invoke-virtual {v5, v2}, Lyt;->S(Ljava/lang/String;)J

    move-result-wide v5

    goto :goto_a

    .line 4125
    :pswitch_25
    const-string v2, "gaia_id_list"

    .line 4126
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 4127
    const-string v5, "include_rich_status"

    const/4 v6, 0x0

    .line 4128
    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 4129
    sget-object v6, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 4130
    invoke-static {v3, v2, v5}, Lbnk;->a(Lyj;Ljava/util/List;Z)Lbnd;

    move-result-object v2

    .line 4132
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4137
    :pswitch_26
    const-string v2, "otr_status"

    const/4 v5, 0x1

    .line 4138
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 4139
    const-string v5, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 4141
    sget-object v6, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 4142
    invoke-static {v3, v5, v2}, Lbnk;->a(Lyj;Ljava/lang/String;I)Lbmq;

    move-result-object v2

    .line 4143
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4148
    :pswitch_27
    const-string v2, "member_gaiaid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 4149
    const-string v2, "phone_number"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 4150
    const-string v2, "user_name"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 4151
    const-string v2, "blocked"

    const/4 v5, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    .line 4152
    const-string v2, "retry_request"

    const/4 v5, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    .line 4153
    sget-object v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    move-object v5, v3

    .line 4154
    invoke-static/range {v5 .. v10}, Lbnk;->a(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lbkl;

    move-result-object v2

    .line 4156
    const-string v3, "rid"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v2, v3}, Lbkl;->a(I)V

    .line 4157
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4162
    :pswitch_28
    sget-object v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 4163
    invoke-static {v3}, Lbnk;->c(Lyj;)Lbmd;

    move-result-object v2

    .line 4164
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4169
    :pswitch_29
    new-instance v2, Lyt;

    invoke-direct {v2, v3}, Lyt;-><init>(Lyj;)V

    .line 4170
    const-string v3, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 4171
    invoke-virtual {v2, v3}, Lyt;->V(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4176
    :pswitch_2a
    invoke-static {v3}, Lbpx;->a(Lyj;)Lbpx;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2, v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbmu;Lyj;)V

    goto/16 :goto_0

    .line 4182
    :pswitch_2b
    invoke-static {v3}, Lbqj;->a(Lyj;)Lbqj;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2, v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbmu;Lyj;)V

    goto/16 :goto_0

    .line 4187
    :pswitch_2c
    const-string v2, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4189
    const-string v5, "is_present"

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 4190
    const-string v6, "timeout_secs"

    const/4 v7, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .line 4191
    sget-object v7, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 4192
    invoke-static {v3, v2, v5, v6}, Lbnk;->a(Lyj;Ljava/lang/String;ZI)Lbpu;

    move-result-object v2

    .line 4194
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4199
    :pswitch_2d
    const-string v2, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4200
    const-string v5, "typing_status"

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 4201
    sget-object v6, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 4202
    invoke-static {v3, v2, v5}, Lbnk;->b(Lyj;Ljava/lang/String;I)Lbpw;

    move-result-object v2

    .line 4203
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4208
    :pswitch_2e
    const-string v2, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 4209
    const-string v2, "tile_type"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 4210
    const-string v2, "tile_event_version"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    .line 4211
    const-string v2, "tile_event_type"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 4214
    const-string v2, "tile_event_data"

    .line 4215
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v10

    check-cast v10, Ljava/util/Map;

    .line 4216
    sget-object v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    move-object v5, v3

    .line 4217
    invoke-static/range {v5 .. v10}, Lbnk;->a(Lyj;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/util/Map;)Lbpv;

    move-result-object v2

    .line 4219
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4223
    :pswitch_2f
    const-string v2, "query"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4224
    sget-object v5, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 4225
    invoke-static {v3, v2}, Lbnk;->j(Lyj;Ljava/lang/String;)Lbpr;

    move-result-object v2

    .line 4226
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4230
    :pswitch_30
    const-string v2, "com.google.android.apps.hangouts.EntityLookupSpecs"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 4232
    const-string v5, "batch_gebi_tag"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 4233
    const-string v6, "from_contact_lookup"

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    .line 4235
    sget-object v7, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 4236
    invoke-static {v3, v2, v5, v6}, Lbnk;->a(Lyj;Ljava/util/List;Ljava/lang/String;Z)Lbli;

    move-result-object v2

    .line 4238
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4242
    :pswitch_31
    invoke-static {v3}, Lbow;->a(Lyj;)Lbow;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2, v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbmu;Lyj;)V

    goto/16 :goto_0

    .line 4247
    :pswitch_32
    const-string v2, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4248
    new-instance v4, Lyt;

    invoke-direct {v4, v3}, Lyt;-><init>(Lyj;)V

    .line 4249
    invoke-virtual {v4}, Lyt;->a()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 4251
    :try_start_5
    invoke-static {v4, v2}, Lyt;->b(Lyt;Ljava/lang/String;)I

    .line 4252
    invoke-virtual {v4}, Lyt;->b()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 4254
    :try_start_6
    invoke-virtual {v4}, Lyt;->c()V

    goto/16 :goto_0

    :catchall_2
    move-exception v2

    invoke-virtual {v4}, Lyt;->c()V

    throw v2

    .line 4256
    :pswitch_33
    const-string v2, "conversationids"

    .line 4260
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 4261
    new-instance v4, Lyt;

    invoke-direct {v4, v3}, Lyt;-><init>(Lyj;)V

    .line 4262
    invoke-virtual {v4, v2}, Lyt;->b([Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4266
    :pswitch_34
    const-string v2, "conversationids"

    .line 4267
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 4268
    new-instance v4, Lyt;

    invoke-direct {v4, v3}, Lyt;-><init>(Lyj;)V

    .line 4269
    invoke-virtual {v4, v2}, Lyt;->a([Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4273
    :pswitch_35
    invoke-static {v3}, Lbqw;->a(Lyj;)Lbqw;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2, v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbmu;Lyj;)V

    goto/16 :goto_0

    .line 4278
    :pswitch_36
    invoke-static {v3}, Lbqi;->a(Lyj;)Lbqi;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2, v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbmu;Lyj;)V

    goto/16 :goto_0

    .line 4284
    :pswitch_37
    invoke-static {v3}, Lbqh;->a(Lyj;)Lbqh;

    move-result-object v2

    .line 4286
    const-string v5, "force_execution"

    const/4 v6, 0x0

    .line 4287
    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 4288
    invoke-virtual {v2}, Lbqh;->g()Z

    move-result v6

    if-eqz v6, :cond_29

    .line 4289
    sget-boolean v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v2, :cond_2

    .line 4290
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "SyncBaselineSuggestedContactsOperation already executing, skip: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 4291
    invoke-virtual {v3}, Lyj;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 4290
    invoke-static {v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4296
    :cond_29
    if-nez v5, :cond_2a

    invoke-virtual {v2}, Lbqh;->f()Z

    move-result v5

    if-eqz v5, :cond_2c

    .line 4297
    :cond_2a
    sget-boolean v5, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v5, :cond_2b

    .line 4298
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "SyncBaselineSuggestedContactsOperation is executed directly: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 4299
    invoke-virtual {v3}, Lyj;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 4298
    invoke-static {v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    .line 4301
    :cond_2b
    sget-object v3, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->n:Ljava/util/Queue;

    invoke-interface {v3, v2}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 4302
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lbqh;->a(I)V

    .line 4303
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4304
    :cond_2c
    invoke-virtual {v2}, Lbqh;->e()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 4305
    sget-boolean v4, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v4, :cond_2d

    .line 4306
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SyncBaselineSuggestedContactsOperation is queued: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 4307
    invoke-virtual {v3}, Lyj;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 4306
    invoke-static {v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    .line 4309
    :cond_2d
    sget-object v3, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->n:Ljava/util/Queue;

    invoke-interface {v3, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 4310
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lbqh;->a(I)V

    goto/16 :goto_0

    .line 4315
    :pswitch_38
    const-string v2, "member_gaiaid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4317
    sget-object v5, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 4318
    invoke-static {v3, v2}, Lbnk;->e(Lyj;Ljava/lang/String;)Lbln;

    move-result-object v2

    .line 4320
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4324
    :pswitch_39
    const-string v2, "hangout_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4325
    sget-object v5, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 4326
    invoke-static {v3, v2}, Lbnk;->d(Lyj;Ljava/lang/String;)Lbll;

    move-result-object v2

    .line 4327
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4331
    :pswitch_3a
    const-string v2, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4332
    sget-object v5, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 4333
    invoke-static {v3, v2}, Lbnk;->c(Lyj;Ljava/lang/String;)Lblk;

    move-result-object v2

    .line 4334
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4338
    :pswitch_3b
    const-string v2, "hangout_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 4339
    const-string v2, "call_log_data"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, [B

    .line 4340
    new-instance v6, Ldzj;

    invoke-direct {v6}, Ldzj;-><init>()V

    invoke-static {v6, v2}, Lepr;->mergeFrom(Lepr;[B)Lepr;

    move-result-object v2

    check-cast v2, Ldzj;

    .line 4341
    sget-object v6, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 4342
    invoke-static {v3, v5, v2}, Lbnk;->a(Lyj;Ljava/lang/String;Ldzj;)Lbpb;

    move-result-object v2

    .line 4343
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4347
    :pswitch_3c
    const-string v2, "compressed_log_file"

    .line 4348
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4349
    new-instance v5, Lbqx;

    invoke-direct {v5, v3, v2}, Lbqx;-><init>(Lyj;Ljava/lang/String;)V

    .line 4351
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v5}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4355
    :pswitch_3d
    sget-object v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 4356
    invoke-static {v3}, Lbnk;->b(Lyj;)Lblh;

    move-result-object v2

    .line 4357
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4361
    :pswitch_3e
    const-string v2, "chat_acl_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4362
    const-string v5, "chat_acl_circle_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 4363
    const-string v6, "chat_acl_circle_name"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 4364
    const-string v7, "chat_acl_level"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 4365
    sget-object v8, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 4366
    invoke-static {v3, v2, v5, v6, v7}, Lbnk;->a(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lbpy;

    move-result-object v2

    .line 4368
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4372
    :pswitch_3f
    const-string v2, "picasa_photo_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4373
    const-string v5, "gaia_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 4374
    sget-object v6, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 4375
    invoke-static {v3, v5, v2}, Lbnk;->c(Lyj;Ljava/lang/String;Ljava/lang/String;)Lblp;

    move-result-object v2

    .line 4376
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4380
    :pswitch_40
    const-string v2, "picasa_photo_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4381
    sget-object v5, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 4382
    invoke-static {v3, v2}, Lbnk;->f(Lyj;Ljava/lang/String;)Lble;

    move-result-object v2

    .line 4383
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4387
    :pswitch_41
    const-string v2, "email_address"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4388
    const-string v5, "phone_number"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 4389
    sget-object v6, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 4390
    invoke-static {v3, v2, v5}, Lbnk;->d(Lyj;Ljava/lang/String;Ljava/lang/String;)Lbpt;

    move-result-object v2

    .line 4391
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4395
    :pswitch_42
    const-string v2, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4397
    const-string v4, "conversation_name"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 4398
    invoke-static {v3, v2, v4}, Lyp;->a(Lyj;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4403
    :pswitch_43
    const-string v2, "otr_status"

    const/4 v4, 0x1

    .line 4404
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 4405
    const-string v4, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 4406
    invoke-static {v3, v4, v2}, Lyp;->a(Lyj;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 4411
    :pswitch_44
    const-string v2, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4412
    const/4 v4, 0x2

    invoke-static {v3, v2, v4}, Lyp;->b(Lyj;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 4417
    :pswitch_45
    move-object/from16 v0, p1

    invoke-static {v3, v0}, Lyp;->a(Lyj;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 4422
    :pswitch_46
    invoke-virtual {v3}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "mms_wap_push_data"

    .line 4423
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v3

    .line 4422
    invoke-static {v2, v3}, Lbvx;->a(Ljava/lang/String;[B)V

    goto/16 :goto_0

    .line 4427
    :pswitch_47
    const-string v2, "mms_content_location"

    .line 4428
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 4429
    const-string v2, "mms_transaction_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v7

    .line 4432
    const-string v2, "notification_row_id"

    const-wide/16 v8, -0x1

    .line 4433
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v8

    .line 4434
    const-string v2, "mms_auto_retrieve"

    const/4 v5, 0x0

    .line 4435
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    .line 4436
    sget-object v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    move-object v5, v3

    .line 4437
    invoke-static/range {v5 .. v10}, Lbnk;->a(Lyj;Ljava/lang/String;[BJZ)Lbpq;

    move-result-object v2

    .line 4439
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4443
    :pswitch_48
    const-string v2, "mms_auto_retrieve"

    const/4 v4, 0x0

    .line 4444
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 4445
    move-object/from16 v0, p1

    invoke-static {v3, v0, v2}, Lyp;->a(Lyj;Landroid/content/Intent;Z)V

    goto/16 :goto_0

    .line 4449
    :pswitch_49
    move-object/from16 v0, p1

    invoke-static {v3, v0}, Lyp;->b(Lyj;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 4453
    :pswitch_4a
    invoke-static {}, Lf;->q()V

    goto/16 :goto_0

    .line 4457
    :pswitch_4b
    invoke-static {}, Lf;->r()V

    goto/16 :goto_0

    .line 4461
    :pswitch_4c
    const-string v2, "free_sms_storage_action_index"

    const/4 v3, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 4463
    const-string v3, "free_sms_storage_duration"

    const-wide/16 v4, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    .line 4465
    invoke-static {v2, v3, v4}, Lbwb;->a(IJ)V

    goto/16 :goto_0

    .line 4469
    :pswitch_4d
    const-string v2, "notification_row_id"

    const-wide/16 v4, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 4471
    const-string v2, "mms_auto_retrieve"

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 4473
    const-string v6, "error"

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .line 4474
    invoke-static {v3, v4, v5, v2, v6}, Lyp;->a(Lyj;JZI)V

    goto/16 :goto_0

    .line 4479
    :pswitch_4e
    const-string v2, "hangout_type"

    const/4 v5, 0x1

    .line 4480
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 4481
    const-string v5, "hangout_topic"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 4482
    sget-object v6, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 4483
    invoke-static {v3, v2, v5}, Lbnk;->a(Lyj;ILjava/lang/String;)Lbks;

    move-result-object v2

    .line 4484
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4488
    :pswitch_4f
    const-string v2, "mms_dump_file"

    .line 4489
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4488
    invoke-static {v3, v2}, Lyp;->b(Lyj;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4493
    :pswitch_50
    const-string v2, "message_text"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4494
    const-string v5, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 4495
    if-eqz v5, :cond_2

    if-eqz v2, :cond_2

    .line 4496
    sget-object v6, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 4497
    invoke-static {v3, v5, v2}, Lbnk;->a(Lyj;Ljava/lang/String;Ljava/lang/String;)Lbkx;

    move-result-object v2

    .line 4498
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4503
    :pswitch_51
    new-instance v2, Lblq;

    invoke-direct {v2, v3}, Lblq;-><init>(Lyj;)V

    .line 4505
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4509
    :pswitch_52
    new-instance v2, Lbky;

    invoke-direct {v2, v3}, Lbky;-><init>(Lyj;)V

    .line 4510
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4514
    :pswitch_53
    const-string v2, "phone_number"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4515
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    invoke-static {v5}, Lcwz;->b(Z)V

    .line 4516
    new-instance v5, Lblg;

    invoke-direct {v5, v3, v2}, Lblg;-><init>(Lyj;Ljava/lang/String;)V

    .line 4517
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v5}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4521
    :pswitch_54
    new-instance v2, Lblm;

    invoke-direct {v2, v3}, Lblm;-><init>(Lyj;)V

    .line 4522
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4526
    :pswitch_55
    const-string v2, "extra_duration"

    const-wide/16 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    move-result-wide v2

    .line 4528
    :try_start_7
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0

    goto/16 :goto_0

    .line 4532
    :catch_1
    move-exception v2

    goto/16 :goto_0

    .line 4535
    :pswitch_56
    :try_start_8
    const-string v2, "image_urls"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 4536
    sget-object v5, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 4537
    invoke-static {v3, v2}, Lbnk;->a(Lyj;[Ljava/lang/String;)Lblj;

    move-result-object v2

    .line 4538
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbki;Landroid/content/Intent;Lbnj;)Ljava/lang/Object;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0

    goto/16 :goto_0

    .line 3100
    nop

    :sswitch_data_0
    .sparse-switch
        0xd -> :sswitch_3
        0xe -> :sswitch_1
        0x12 -> :sswitch_4
        0x13 -> :sswitch_5
        0x28 -> :sswitch_0
        0x37 -> :sswitch_7
        0x42 -> :sswitch_a
        0x48 -> :sswitch_b
        0x4c -> :sswitch_8
        0x5b -> :sswitch_c
        0x5d -> :sswitch_f
        0x5f -> :sswitch_9
        0x68 -> :sswitch_2
        0x6f -> :sswitch_10
        0x72 -> :sswitch_11
        0x76 -> :sswitch_12
        0x77 -> :sswitch_13
        0x78 -> :sswitch_14
        0x79 -> :sswitch_17
        0x7c -> :sswitch_19
        0x7d -> :sswitch_18
        0x88 -> :sswitch_1a
        0x8b -> :sswitch_6
        0x8f -> :sswitch_e
        0x90 -> :sswitch_d
        0x99 -> :sswitch_1b
        0x9f -> :sswitch_1e
        0xa0 -> :sswitch_1f
        0xa2 -> :sswitch_20
        0xa3 -> :sswitch_21
        0xaf -> :sswitch_15
        0xb0 -> :sswitch_16
        0xbb -> :sswitch_1d
        0xbc -> :sswitch_1c
    .end sparse-switch

    .line 3616
    :pswitch_data_0
    .packed-switch 0x1e
        :pswitch_a
        :pswitch_c
        :pswitch_15
        :pswitch_16
        :pswitch_0
        :pswitch_17
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1
        :pswitch_0
        :pswitch_21
        :pswitch_22
        :pswitch_0
        :pswitch_11
        :pswitch_0
        :pswitch_0
        :pswitch_f
        :pswitch_2c
        :pswitch_2d
        :pswitch_0
        :pswitch_2e
        :pswitch_0
        :pswitch_3
        :pswitch_23
        :pswitch_0
        :pswitch_37
        :pswitch_25
        :pswitch_2f
        :pswitch_30
        :pswitch_2a
        :pswitch_0
        :pswitch_26
        :pswitch_0
        :pswitch_0
        :pswitch_24
        :pswitch_0
        :pswitch_0
        :pswitch_1b
        :pswitch_38
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_39
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3b
        :pswitch_3a
        :pswitch_13
        :pswitch_19
        :pswitch_1a
        :pswitch_12
        :pswitch_10
        :pswitch_4
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_3d
        :pswitch_3e
        :pswitch_0
        :pswitch_27
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_28
        :pswitch_29
        :pswitch_31
        :pswitch_41
        :pswitch_0
        :pswitch_42
        :pswitch_43
        :pswitch_44
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_45
        :pswitch_0
        :pswitch_0
        :pswitch_48
        :pswitch_14
        :pswitch_0
        :pswitch_46
        :pswitch_47
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_49
        :pswitch_0
        :pswitch_e
        :pswitch_4a
        :pswitch_4b
        :pswitch_4c
        :pswitch_35
        :pswitch_3f
        :pswitch_0
        :pswitch_36
        :pswitch_0
        :pswitch_0
        :pswitch_4d
        :pswitch_0
        :pswitch_4e
        :pswitch_0
        :pswitch_0
        :pswitch_3c
        :pswitch_4f
        :pswitch_50
        :pswitch_51
        :pswitch_0
        :pswitch_52
        :pswitch_53
        :pswitch_d
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_54
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_32
        :pswitch_0
        :pswitch_55
        :pswitch_33
        :pswitch_34
        :pswitch_56
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1f
        :pswitch_20
        :pswitch_40
        :pswitch_18
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_a
        :pswitch_2b
    .end packed-switch
.end method

.method public static h()I
    .locals 1

    .prologue
    .line 2408
    const/16 v0, 0x6b

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(I)Landroid/content/Intent;

    move-result-object v0

    .line 2409
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static h(Lyj;Ljava/lang/String;Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 2857
    const/16 v0, 0x87

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 2858
    const-string v1, "picasa_photo_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2859
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2860
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static h(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 6334
    invoke-static {}, Lbkb;->n()Lyj;

    move-result-object v0

    .line 6335
    if-nez v0, :cond_0

    .line 6342
    :goto_0
    return-void

    .line 6339
    :cond_0
    const/16 v1, 0x92

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 6340
    const-string v1, "mms_dump_file"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6341
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    goto :goto_0
.end method

.method public static h(Lyj;)V
    .locals 1

    .prologue
    .line 3048
    const/16 v0, 0xa2

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 3049
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Landroid/content/Intent;)V

    .line 3050
    return-void
.end method

.method public static h(Lyj;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1992
    invoke-static {p1}, Lyt;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1993
    invoke-static {p1}, Lyt;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1994
    const/16 v0, 0x53

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    .line 1996
    :cond_0
    return-void
.end method

.method public static i(Lyj;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 2153
    const/16 v0, 0x23

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static i(Lyj;Ljava/lang/String;Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 2934
    const/16 v0, 0x63

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 2935
    const-string v1, "email_address"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2936
    const-string v1, "phone_number"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2937
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static i()V
    .locals 5

    .prologue
    const/16 v4, 0x6a

    .line 2505
    const-string v0, "Babel"

    const-string v1, "Canceling gcm registration retry alarm"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2507
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "alarm"

    .line 2508
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 2509
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.google.android.apps.hangouts.RETRY_GCM_REGISTRATION"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "op"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {v4}, Lbzb;->a(I)I

    move-result v3

    const/high16 v4, 0x10000000

    invoke-static {v1, v3, v2, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 2510
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 2511
    return-void
.end method

.method private static i(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 4930
    invoke-static {p0}, Lbpx;->a(Ljava/lang/String;)Lbpx;

    move-result-object v0

    .line 4931
    if-eqz v0, :cond_0

    .line 4932
    sget-object v1, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->n:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 4936
    :cond_0
    invoke-static {p0}, Lbqh;->a(Ljava/lang/String;)Lbqh;

    move-result-object v0

    .line 4937
    if-eqz v0, :cond_1

    .line 4938
    sget-object v1, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->n:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 4941
    :cond_1
    invoke-static {p0}, Lbow;->a(Ljava/lang/String;)Lbow;

    move-result-object v0

    .line 4943
    if-eqz v0, :cond_2

    .line 4944
    sget-object v1, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->n:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 4946
    :cond_2
    return-void
.end method

.method public static i(Lyj;)V
    .locals 1

    .prologue
    .line 6351
    const/16 v0, 0xa3

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 6352
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    .line 6353
    return-void
.end method

.method public static j()V
    .locals 1

    .prologue
    .line 2577
    const/16 v0, 0xad

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(I)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    .line 2578
    return-void
.end method

.method private static j(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 6232
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[RealTimeChatService] "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 6233
    return-void
.end method

.method private static j(Lyj;)V
    .locals 2

    .prologue
    .line 1546
    invoke-static {p0}, Lbpx;->a(Lyj;)Lbpx;

    move-result-object v0

    .line 1547
    invoke-virtual {v0}, Lbpx;->e()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lbpx;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1548
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v0, :cond_0

    .line 1549
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SetActiveClientOperation is idle: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    .line 1555
    :cond_0
    :goto_0
    return-void

    .line 1554
    :cond_1
    const/16 v0, 0x3c

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static j(Lyj;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2161
    const/16 v0, 0xb4

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "call_media_type"

    const/4 v2, 0x0

    .line 2163
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 2164
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    .line 2165
    return-void
.end method

.method public static j(Lyj;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 5557
    sget-object v1, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 5558
    :try_start_0
    invoke-static {p0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5559
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->i:Ljava/lang/String;

    .line 5560
    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5561
    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 5564
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5565
    sget-object v1, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->m:Ljava/util/Map;

    monitor-enter v1

    .line 5566
    :try_start_1
    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->m:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5567
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 5568
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 5569
    new-instance v1, Lboa;

    invoke-direct {v1, p1, p2}, Lboa;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 5577
    return-void

    .line 5564
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 5567
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static k(Lyj;Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 2207
    const/16 v0, 0x3a

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 2208
    const-string v1, "query"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2209
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static k()V
    .locals 1

    .prologue
    .line 2581
    const/16 v0, 0xae

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(I)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    .line 2582
    return-void
.end method

.method private static k(Lyj;)V
    .locals 2

    .prologue
    .line 1558
    invoke-static {p0}, Lbqj;->a(Lyj;)Lbqj;

    move-result-object v0

    .line 1559
    invoke-virtual {v0}, Lbqj;->e()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lbqj;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1560
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v0, :cond_0

    .line 1561
    const-string v0, "TickleGcmOperation skipped"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    .line 1566
    :cond_0
    :goto_0
    return-void

    .line 1565
    :cond_1
    const/16 v0, 0xba

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static l()I
    .locals 1

    .prologue
    .line 3007
    const/16 v0, 0xa5

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(I)Landroid/content/Intent;

    move-result-object v0

    .line 3008
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static l(Lyj;Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 2609
    const/16 v0, 0x78

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 2610
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2611
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method private static l(Lyj;)V
    .locals 2

    .prologue
    .line 2543
    invoke-static {p0}, Lbow;->a(Lyj;)Lbow;

    move-result-object v0

    .line 2544
    invoke-virtual {v0}, Lbow;->e()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lbow;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2545
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v0, :cond_0

    .line 2546
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "RefreshParticipantsOperation is idle: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2547
    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2546
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    .line 2553
    :cond_0
    :goto_0
    return-void

    .line 2552
    :cond_1
    const/16 v0, 0x62

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    goto :goto_0
.end method

.method public static m()I
    .locals 1

    .prologue
    .line 5711
    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    return v0
.end method

.method public static m(Lyj;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 2691
    const/16 v0, 0x2a

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method private static m(Lyj;)V
    .locals 2

    .prologue
    .line 2585
    invoke-static {p0}, Lbqw;->a(Lyj;)Lbqw;

    move-result-object v0

    .line 2586
    invoke-virtual {v0}, Lbqw;->e()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lbqw;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2587
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v0, :cond_0

    .line 2588
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UploadAnalyticsOperation is idle: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    .line 2594
    :cond_0
    :goto_0
    return-void

    .line 2593
    :cond_1
    const/16 v0, 0x86

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    goto :goto_0
.end method

.method public static n(Lyj;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 2714
    invoke-static {p1}, Lyt;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2715
    const/4 v0, 0x0

    .line 2717
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x41

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    goto :goto_0
.end method

.method public static n()V
    .locals 2

    .prologue
    .line 6295
    invoke-static {}, Lbkb;->n()Lyj;

    move-result-object v0

    .line 6296
    if-nez v0, :cond_0

    .line 6302
    :goto_0
    return-void

    .line 6300
    :cond_0
    const/16 v1, 0x83

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 6301
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    goto :goto_0
.end method

.method private static n(Lyj;)V
    .locals 2

    .prologue
    .line 2598
    invoke-static {p0}, Lbqi;->a(Lyj;)Lbqi;

    move-result-object v0

    .line 2599
    invoke-virtual {v0}, Lbqi;->e()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lbqi;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2600
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v0, :cond_0

    .line 2601
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SyncSmsDeletedMessagesOperation is idle: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    .line 2606
    :cond_0
    :goto_0
    return-void

    .line 2605
    :cond_1
    const/16 v0, 0x89

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    goto :goto_0
.end method

.method public static o(Lyj;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 2721
    const/16 v0, 0x61

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static o()V
    .locals 2

    .prologue
    .line 6305
    invoke-static {}, Lbkb;->n()Lyj;

    move-result-object v0

    .line 6306
    if-nez v0, :cond_0

    .line 6312
    :goto_0
    return-void

    .line 6310
    :cond_0
    const/16 v1, 0x84

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 6311
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    goto :goto_0
.end method

.method public static p(Lyj;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 2817
    const/16 v0, 0x4f

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static synthetic p()V
    .locals 0

    .prologue
    .line 180
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->s()V

    return-void
.end method

.method public static q(Lyj;Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 2840
    const/16 v0, 0x91

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 2841
    const-string v1, "compressed_log_file"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2842
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method static synthetic q()Z
    .locals 1

    .prologue
    .line 180
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    return v0
.end method

.method public static r(Lyj;Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 2867
    const/16 v0, 0xb3

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 2868
    const-string v1, "picasa_photo_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2869
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method public static synthetic r()Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    .prologue
    .line 180
    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->p:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method public static s(Lyj;Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 2906
    const/16 v0, 0xaf

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 2907
    const-string v1, "member_gaiaid"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2908
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method private static s()V
    .locals 4

    .prologue
    .line 611
    const-string v0, "babel_log_dump"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 614
    sget-object v1, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->v:Ljava/lang/Object;

    monitor-enter v1

    .line 615
    if-eqz v0, :cond_1

    :try_start_0
    sget-object v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->t:Lcxa;

    if-nez v2, :cond_1

    .line 616
    new-instance v2, Lcxa;

    const/16 v3, 0x64

    invoke-direct {v2, v3}, Lcxa;-><init>(I)V

    sput-object v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->t:Lcxa;

    .line 620
    :cond_0
    :goto_0
    sput-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->u:Z

    .line 621
    monitor-exit v1

    return-void

    .line 617
    :cond_1
    if-nez v0, :cond_0

    sget-object v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->t:Lcxa;

    if-eqz v2, :cond_0

    .line 618
    const/4 v2, 0x0

    sput-object v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->t:Lcxa;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 621
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static t(Lyj;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 3039
    const/16 v0, 0xa0

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 3040
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    move-result v0

    return v0
.end method

.method private static t()Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 2237
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.hangouts.CLEANUP_DB"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2238
    const-string v1, "op"

    const/16 v2, 0x5e

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2240
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0x66

    .line 2241
    invoke-static {v2}, Lbzb;->a(I)I

    move-result v2

    const/high16 v3, 0x10000000

    .line 2240
    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private static u()Landroid/app/PendingIntent;
    .locals 4

    .prologue
    const/16 v2, 0x69

    .line 2355
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.hangouts.TIMEOUT_PHONE_VERIFICATION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2356
    const-string v1, "op"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2358
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    .line 2359
    invoke-static {v2}, Lbzb;->a(I)I

    move-result v2

    const/high16 v3, 0x10000000

    .line 2358
    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public static u(Lyj;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 6328
    const/16 v0, 0x88

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 6329
    const-string v1, "conversation_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6330
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    .line 6331
    return-void
.end method

.method private static v()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 5753
    new-instance v0, Lbwv;

    invoke-direct {v0}, Lbwv;-><init>()V

    .line 5754
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v3

    .line 5756
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v4}, Lbwv;->a(Landroid/content/ContentResolver;)Ljava/util/List;

    move-result-object v4

    .line 5757
    invoke-virtual {v0, v3, v4}, Lbwv;->a(Landroid/content/Context;Ljava/util/List;)V

    .line 5760
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lbwv;->a(Landroid/content/ContentResolver;)Ljava/util/List;

    move-result-object v0

    .line 5762
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwx;

    .line 5763
    iget-boolean v4, v0, Lbwx;->c:Z

    if-eqz v4, :cond_0

    .line 5765
    const-string v3, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Talk Account id "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v5, v0, Lbwx;->a:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v0, v0, Lbwx;->b:Ljava/lang/String;

    .line 5767
    invoke-static {v0}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ") still signed in"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5765
    invoke-static {v3, v0}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 5773
    :goto_0
    const-string v3, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Talk signout success "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 5774
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->d()Lcom/google/android/apps/hangouts/phone/EsApplication;

    move-result-object v3

    .line 5775
    if-eqz v3, :cond_1

    .line 5776
    if-nez v0, :cond_2

    :goto_1
    invoke-virtual {v3, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->b(Z)V

    .line 5778
    :cond_1
    return-void

    :cond_2
    move v2, v1

    .line 5776
    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method public static v(Lyj;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 6345
    const/16 v0, 0x9f

    invoke-static {p0, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v0

    .line 6346
    const-string v1, "request_key"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6347
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->f(Landroid/content/Intent;)I

    .line 6348
    return-void
.end method

.method private static w()V
    .locals 14

    .prologue
    const/16 v13, 0x14

    const/4 v12, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 5783
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v6

    .line 5785
    const-string v0, "babel_richstatus"

    invoke-static {v0, v3}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 5788
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v0, :cond_0

    .line 5789
    const-string v0, "Babel"

    const-string v1, "Rich Status Disabled; skipping in call state."

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5868
    :cond_0
    :goto_0
    return-void

    .line 5795
    :cond_1
    const-string v0, "phone"

    invoke-virtual {v6, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 5797
    if-nez v0, :cond_2

    .line 5798
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v0, :cond_0

    .line 5799
    const-string v0, "Babel"

    const-string v1, "No TelephonyManager; skipping in call state."

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 5805
    :cond_2
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    if-ne v0, v12, :cond_3

    move v2, v3

    .line 5808
    :goto_1
    const-string v0, "alarm"

    invoke-virtual {v6, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 5810
    new-instance v1, Landroid/content/Intent;

    const-string v5, "com.google.android.apps.hangouts.UPDATE_IN_CALL_STATE"

    invoke-direct {v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 5811
    const-string v5, "op"

    const/16 v7, 0x7b

    invoke-virtual {v1, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 5813
    const/16 v5, 0x6c

    .line 5814
    invoke-static {v5}, Lbzb;->a(I)I

    move-result v5

    const/high16 v7, 0x10000000

    .line 5813
    invoke-static {v6, v5, v1, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v7

    .line 5820
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 5821
    sget v5, Lh;->kG:I

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 5829
    const-string v1, "babel_richstatus_in_call_refresh_interval"

    const/16 v5, 0xf0

    .line 5831
    invoke-static {v1, v5}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v1

    .line 5829
    invoke-static {v13, v1}, Ljava/lang/Math;->max(II)I

    move-result v9

    .line 5835
    invoke-static {v4}, Lbkb;->g(Z)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move v5, v4

    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 5836
    invoke-static {v1}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v1

    .line 5838
    if-eqz v1, :cond_7

    .line 5841
    invoke-virtual {v1}, Lyj;->b()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lf;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 5840
    invoke-virtual {v6, v11, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v11

    .line 5843
    invoke-interface {v11, v8, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 5847
    const/16 v11, 0x76

    invoke-static {v1, v11}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;I)Landroid/content/Intent;

    move-result-object v1

    const-string v11, "in_call_expiration_seconds"

    invoke-virtual {v1, v11, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v11, "in_call_currently"

    invoke-virtual {v1, v11, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Landroid/content/Intent;)V

    .line 5849
    if-eqz v2, :cond_7

    move v1, v3

    :goto_3
    move v5, v1

    .line 5854
    goto :goto_2

    :cond_3
    move v2, v4

    .line 5805
    goto/16 :goto_1

    .line 5856
    :cond_4
    if-eqz v5, :cond_6

    .line 5858
    if-le v9, v13, :cond_5

    :goto_4
    invoke-static {v3}, Lcwz;->a(Z)V

    .line 5860
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    add-int/lit8 v2, v9, -0x14

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v1

    .line 5864
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    add-long/2addr v1, v3

    .line 5863
    invoke-virtual {v0, v12, v1, v2, v7}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    goto/16 :goto_0

    :cond_5
    move v3, v4

    .line 5858
    goto :goto_4

    .line 5866
    :cond_6
    invoke-virtual {v0, v7}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    goto/16 :goto_0

    :cond_7
    move v1, v5

    goto :goto_3
.end method

.method private w(Lyj;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 4856
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    .line 4858
    invoke-virtual {p1}, Lyj;->b()Ljava/lang/String;

    move-result-object v1

    .line 4857
    invoke-static {v1}, Lf;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 4859
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 4860
    sget v2, Lh;->kI:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 4862
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 4865
    iget-object v2, p0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->s:Landroid/os/Handler;

    new-instance v3, Lbom;

    invoke-direct {v3, p0, v0, v1, p2}, Lbom;-><init>(Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 4872
    :cond_0
    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Intent;J)V
    .locals 14

    .prologue
    .line 733
    if-nez p1, :cond_1

    .line 735
    const-string v0, "Babel"

    const-string v1, "RTCS onHandleIntent called with null intent"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 894
    :cond_0
    :goto_0
    return-void

    .line 738
    :cond_1
    const-string v0, "op"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 739
    const-string v0, "account_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 740
    const-string v0, "rqtms"

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 742
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->u:Z

    if-eqz v0, :cond_6

    .line 743
    :cond_2
    const-string v0, "rqtns"

    const-wide/16 v4, 0x0

    invoke-virtual {p1, v0, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 748
    :goto_1
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v0, :cond_3

    .line 749
    sub-long v6, p2, v4

    const-wide/32 v8, 0xf4240

    div-long/2addr v6, v8

    .line 750
    const-string v0, "Babel"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "RTCIntent: start processing intent:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " account:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " delay: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ms"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 755
    :cond_3
    sget v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->d:I

    const-string v6, "pid"

    const/4 v7, -0x1

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    if-ne v0, v6, :cond_7

    const/4 v0, 0x1

    move v6, v0

    .line 756
    :goto_2
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v0, :cond_4

    .line 757
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, "onHandleIntent "

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, " "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, " opcode: "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 758
    invoke-static {v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, " respectWakeLock "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 757
    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    .line 761
    :cond_4
    if-eqz v6, :cond_9

    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->r:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->r:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_9

    .line 762
    :cond_5
    const-string v2, "Babel"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "RTCS.onHandleIntent called "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " opcode: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 763
    invoke-static {v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " sWakeLock: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->r:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isHeld: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->r:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_8

    const-string v0, "(null)"

    .line 765
    :goto_3
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 762
    invoke-static {v2, v0}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 766
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 745
    :cond_6
    const-wide/16 v4, 0x0

    goto/16 :goto_1

    .line 755
    :cond_7
    const/4 v0, 0x0

    move v6, v0

    goto/16 :goto_2

    .line 763
    :cond_8
    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->r:Landroid/os/PowerManager$WakeLock;

    .line 765
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_3

    .line 773
    :cond_9
    :try_start_0
    invoke-static {}, Lyp;->a()V

    .line 774
    sparse-switch v1, :sswitch_data_0

    .line 840
    invoke-static {v10}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v7

    .line 841
    if-nez v7, :cond_13

    .line 842
    const-string v0, "Babel"

    const-string v7, "skipping intent for invalid account"

    invoke-static {v0, v7}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 859
    :cond_a
    :goto_4
    invoke-static {}, Lbya;->b()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 861
    invoke-static {}, Lbya;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 864
    :cond_b
    invoke-static {}, Lyp;->b()V

    .line 865
    if-eqz v6, :cond_d

    .line 866
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v0, :cond_c

    .line 867
    const-string v0, "releasing wakelock"

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    .line 870
    :cond_c
    :try_start_1
    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->r:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    .line 881
    :cond_d
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->u:Z

    if-nez v0, :cond_e

    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v0, :cond_0

    .line 882
    :cond_e
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    .line 883
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->u:Z

    if-eqz v0, :cond_10

    .line 884
    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->t:Lcxa;

    if-eqz v0, :cond_10

    new-instance v0, Lboo;

    move-wide/from16 v6, p2

    invoke-direct/range {v0 .. v9}, Lboo;-><init>(IJJJJ)V

    sget-object v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->v:Ljava/lang/Object;

    monitor-enter v2

    :try_start_2
    sget-object v3, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->t:Lcxa;

    if-eqz v3, :cond_f

    sget-object v3, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->t:Lcxa;

    invoke-virtual {v3, v0}, Lcxa;->a(Ljava/lang/Object;)V

    :cond_f
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 887
    :cond_10
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v0, :cond_0

    .line 888
    sub-long v2, v8, p2

    const-wide/32 v4, 0xf4240

    div-long/2addr v2, v4

    .line 889
    const-string v0, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "RTCIntent: finish processing intent:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " account:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " time: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 777
    :sswitch_0
    :try_start_3
    const-string v0, "Babel"

    const-string v7, "clean up invalid accounts."

    invoke-static {v0, v7}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lym;->d()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_a

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Ljava/util/ArrayList;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_4

    .line 864
    :catchall_0
    move-exception v0

    invoke-static {}, Lyp;->b()V

    .line 865
    if-eqz v6, :cond_12

    .line 866
    sget-boolean v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v2, :cond_11

    .line 867
    const-string v2, "releasing wakelock"

    invoke-static {v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    .line 870
    :cond_11
    :try_start_4
    sget-object v2, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->r:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_1

    .line 877
    :cond_12
    throw v0

    .line 780
    :sswitch_1
    :try_start_5
    const-string v0, "Babel"

    const-string v7, "locale changed."

    invoke-static {v0, v7}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 781
    invoke-static {}, Lcom/google/android/apps/hangouts/content/EsProvider;->a()V

    goto/16 :goto_4

    .line 784
    :sswitch_2
    const-string v0, "Babel"

    const-string v7, "clean up database."

    invoke-static {v0, v7}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 785
    invoke-static {}, Lbrc;->c()Lbrc;

    move-result-object v0

    invoke-virtual {v0}, Lbrc;->e()V

    goto/16 :goto_4

    .line 788
    :sswitch_3
    const-string v0, "Babel"

    const-string v7, "renew accounts."

    invoke-static {v0, v7}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 789
    invoke-static {}, Lbkb;->r()V

    goto/16 :goto_4

    .line 793
    :sswitch_4
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v7, "package"

    .line 794
    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 792
    invoke-static {v0, v7}, Lcom/google/android/apps/hangouts/phone/PackageReplacedReceiver;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 797
    :sswitch_5
    invoke-static {}, Lbmz;->a()Lbmz;

    move-result-object v0

    invoke-virtual {v0}, Lbmz;->g()V

    goto/16 :goto_4

    .line 800
    :sswitch_6
    invoke-static {}, Lbld;->c()Lbld;

    move-result-object v0

    invoke-virtual {v0}, Lbld;->i()V

    goto/16 :goto_4

    .line 803
    :sswitch_7
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->v()V

    goto/16 :goto_4

    .line 806
    :sswitch_8
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->w()V

    goto/16 :goto_4

    .line 811
    :sswitch_9
    invoke-virtual {p0, v10}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 814
    :sswitch_a
    invoke-virtual {p0, v10}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Ljava/lang/String;)V

    .line 815
    invoke-static {v10}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->i(Ljava/lang/String;)V

    .line 816
    invoke-static {v10}, Lbrf;->a(Ljava/lang/String;)V

    .line 817
    invoke-static {v10}, Lbrk;->a(Ljava/lang/String;)V

    .line 818
    invoke-static {v10}, Lbsx;->a(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 821
    :sswitch_b
    const-string v0, "conversation_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 822
    const-string v7, "timestamp"

    const-wide/16 v8, 0x0

    .line 823
    invoke-virtual {p1, v7, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v7

    .line 824
    new-instance v9, Lyt;

    invoke-static {v10}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v11

    invoke-direct {v9, v11}, Lyt;-><init>(Lyj;)V

    invoke-virtual {v9, v0, v7, v8}, Lyt;->f(Ljava/lang/String;J)I

    move-result v9

    sget-boolean v11, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v11, :cond_a

    const-string v11, "Babel"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Deleted "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v12, " played event suggestions for conversation "

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v9, " on account "

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v9, " where the last played event was at timestamp "

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, "."

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v11, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 828
    :sswitch_c
    invoke-static {}, Lbsc;->e()V

    goto/16 :goto_4

    .line 831
    :sswitch_d
    invoke-static {}, Lyt;->x()V

    goto/16 :goto_4

    .line 834
    :sswitch_e
    invoke-static {}, Lyt;->y()V

    goto/16 :goto_4

    .line 844
    :cond_13
    invoke-static {v7}, Lbkb;->f(Lyj;)I

    move-result v0

    const/16 v8, 0x66

    if-eq v0, v8, :cond_14

    .line 846
    invoke-virtual {p0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 847
    :cond_14
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->g(Landroid/content/Intent;)V

    goto/16 :goto_4

    .line 849
    :cond_15
    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->o:Ljava/util/Map;

    invoke-virtual {v7}, Lyj;->b()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v0, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Queue;

    .line 850
    if-nez v0, :cond_16

    .line 851
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    .line 852
    sget-object v8, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->o:Ljava/util/Map;

    invoke-virtual {v7}, Lyj;->b()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v8, v7, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 854
    :cond_16
    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_4

    .line 871
    :catch_0
    move-exception v0

    move-object v2, v0

    .line 872
    const-string v3, "Babel"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "RTCS.onHandleIntent exit crash "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 873
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " opcode: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 874
    invoke-static {v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " sWakeLock: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->r:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isHeld: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->r:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_17

    const-string v0, "(null)"

    .line 876
    :goto_5
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 872
    invoke-static {v3, v0}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 877
    throw v2

    .line 874
    :cond_17
    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->r:Landroid/os/PowerManager$WakeLock;

    .line 876
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_5

    .line 871
    :catch_1
    move-exception v0

    move-object v2, v0

    .line 872
    const-string v3, "Babel"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "RTCS.onHandleIntent exit crash "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 873
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " opcode: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 874
    invoke-static {v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " sWakeLock: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->r:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isHeld: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->r:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_18

    const-string v0, "(null)"

    .line 876
    :goto_6
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 872
    invoke-static {v3, v0}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 877
    throw v2

    .line 874
    :cond_18
    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->r:Landroid/os/PowerManager$WakeLock;

    .line 876
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_6

    .line 884
    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0

    .line 774
    nop

    :sswitch_data_0
    .sparse-switch
        0xf -> :sswitch_9
        0x10 -> :sswitch_a
        0x11 -> :sswitch_0
        0x4a -> :sswitch_1
        0x5e -> :sswitch_2
        0x64 -> :sswitch_4
        0x69 -> :sswitch_5
        0x6a -> :sswitch_6
        0x6b -> :sswitch_7
        0x7b -> :sswitch_8
        0x8d -> :sswitch_3
        0x95 -> :sswitch_b
        0xa5 -> :sswitch_c
        0xad -> :sswitch_d
        0xae -> :sswitch_e
    .end sparse-switch
.end method

.method public a(Landroid/content/Intent;Lbos;Ljava/lang/Object;)V
    .locals 7

    .prologue
    const/4 v6, -0x1

    .line 5653
    const-string v0, "op"

    invoke-virtual {p1, v0, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 5654
    const-string v1, "rid"

    invoke-virtual {p1, v1, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 5655
    const-string v2, "account_name"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 5656
    invoke-static {v2}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v3

    .line 5657
    if-nez v3, :cond_1

    .line 5658
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "[onIntentProcessed] Skipping intent for invalid account: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 5659
    invoke-static {v2}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 5658
    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 5702
    :cond_0
    :goto_0
    return-void

    .line 5663
    :cond_1
    sget-boolean v4, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-eqz v4, :cond_2

    .line 5664
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[onIntentProcessed] opCode="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", requestId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", account="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->j(Ljava/lang/String;)V

    .line 5669
    :cond_2
    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 5686
    :sswitch_0
    check-cast p3, Lbkr;

    .line 5688
    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->p:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbor;

    .line 5689
    invoke-virtual {v0, v1, p3, p2}, Lbor;->a(ILbkr;Lbos;)V

    goto :goto_1

    .line 5671
    :sswitch_1
    if-eqz p3, :cond_0

    .line 5674
    check-cast p3, Ljava/util/List;

    .line 5676
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbos;

    .line 5677
    invoke-virtual {v0}, Lbos;->a()I

    move-result v4

    .line 5678
    sget-object v1, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->p:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbor;

    .line 5679
    invoke-virtual {v1, v4, v3, v0}, Lbor;->a(ILyj;Lbos;)V

    goto :goto_2

    .line 5695
    :sswitch_2
    check-cast p3, Lbkr;

    .line 5697
    const-string v0, "original_conversation_id"

    invoke-virtual {p1, v0, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 5698
    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->p:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbor;

    .line 5699
    invoke-virtual {v0, v3, p3, v1}, Lbor;->a(Lyj;Lbkr;I)V

    goto :goto_3

    .line 5669
    :sswitch_data_0
    .sparse-switch
        0x1e -> :sswitch_0
        0x27 -> :sswitch_1
        0xb9 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 980
    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->o:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Queue;

    .line 981
    if-nez v0, :cond_0

    .line 990
    :goto_0
    return-void

    .line 986
    :cond_0
    :goto_1
    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    if-eqz v1, :cond_1

    .line 987
    invoke-static {v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->e(Landroid/content/Intent;)V

    goto :goto_1

    .line 989
    :cond_1
    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->o:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a(Lyj;Lbgy;)V
    .locals 2

    .prologue
    .line 5584
    iget-object v0, p0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->s:Landroid/os/Handler;

    new-instance v1, Lbob;

    invoke-direct {v1, p0, p1, p2}, Lbob;-><init>(Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;Lyj;Lbgy;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 5592
    return-void
.end method

.method protected a(Landroid/content/Intent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 956
    const-string v1, "op"

    const/4 v2, -0x1

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 957
    sparse-switch v1, :sswitch_data_0

    .line 973
    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    :sswitch_0
    return v0

    .line 963
    :sswitch_1
    const-string v1, "server_response"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v1

    .line 964
    invoke-static {v1}, Lcom/google/android/apps/hangouts/realtimechat/RequestWriter;->c([B)Lbfz;

    move-result-object v1

    .line 966
    instance-of v2, v1, Lbhn;

    if-nez v2, :cond_1

    instance-of v1, v1, Lbha;

    if-eqz v1, :cond_0

    goto :goto_0

    .line 957
    :sswitch_data_0
    .sparse-switch
        0xd -> :sswitch_0
        0xe -> :sswitch_0
        0x12 -> :sswitch_0
        0x27 -> :sswitch_1
    .end sparse-switch
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 996
    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->o:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Queue;

    .line 997
    if-nez v0, :cond_0

    .line 1003
    :goto_0
    return-void

    .line 1001
    :cond_0
    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 1002
    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->o:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 1078
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 9

    .prologue
    const-wide/16 v7, 0x0

    .line 901
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 902
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->s:Landroid/os/Handler;

    .line 903
    sget-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    if-nez v0, :cond_0

    new-instance v0, Lbnk;

    invoke-direct {v0}, Lbnk;-><init>()V

    sput-object v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a:Lbnk;

    .line 904
    :cond_0
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/DebugActivity;->a()V

    .line 906
    const-string v0, "babel_rtcs_watchdog_warning"

    invoke-static {v0, v7, v8}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;J)J

    move-result-wide v3

    .line 909
    const-string v0, "babel_rtcs_watchdog_error"

    invoke-static {v0, v7, v8}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;J)J

    move-result-wide v5

    .line 913
    cmp-long v0, v3, v7

    if-gtz v0, :cond_1

    cmp-long v0, v5, v7

    if-lez v0, :cond_2

    .line 914
    :cond_1
    new-instance v0, Lbny;

    const-string v2, "RTCS-watchdog"

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lbny;-><init>(Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;Ljava/lang/String;JJ)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->w:Lbqy;

    .line 922
    :cond_2
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 926
    invoke-super {p0}, Landroid/app/IntentService;->onDestroy()V

    .line 927
    iget-object v0, p0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->w:Lbqy;

    if-eqz v0, :cond_0

    .line 928
    iget-object v0, p0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->w:Lbqy;

    invoke-virtual {v0}, Lbqy;->b()V

    .line 930
    :cond_0
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 718
    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->u:Z

    if-eqz v0, :cond_3

    .line 719
    :cond_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    .line 723
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->w:Lbqy;

    if-eqz v2, :cond_1

    .line 724
    iget-object v2, p0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->w:Lbqy;

    invoke-virtual {v2, p1}, Lbqy;->b(Ljava/lang/Object;)V

    .line 726
    :cond_1
    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Landroid/content/Intent;J)V

    .line 727
    iget-object v0, p0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->w:Lbqy;

    if-eqz v0, :cond_2

    .line 728
    iget-object v0, p0, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->w:Lbqy;

    invoke-virtual {v0}, Lbqy;->a()V

    .line 730
    :cond_2
    return-void

    .line 721
    :cond_3
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4

    .prologue
    .line 703
    and-int/lit8 v0, p2, 0x1

    if-eqz v0, :cond_0

    .line 704
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RTCS.onStartCommand called for redelivery / retry "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 705
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " opcode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "op"

    const/4 v3, 0x0

    .line 706
    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " flags "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " startId "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 704
    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 710
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/IntentService;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    return v0
.end method
