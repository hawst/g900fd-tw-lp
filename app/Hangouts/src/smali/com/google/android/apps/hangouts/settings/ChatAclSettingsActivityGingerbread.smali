.class public Lcom/google/android/apps/hangouts/settings/ChatAclSettingsActivityGingerbread;
.super Lbux;
.source "PG"


# instance fields
.field private a:Lbue;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lbux;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 25
    invoke-super {p0, p1}, Lbux;->onCreate(Landroid/os/Bundle;)V

    .line 27
    new-instance v0, Lbue;

    invoke-direct {v0, p0}, Lbue;-><init>(Lbtz;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/settings/ChatAclSettingsActivityGingerbread;->a:Lbue;

    .line 28
    iget-object v0, p0, Lcom/google/android/apps/hangouts/settings/ChatAclSettingsActivityGingerbread;->a:Lbue;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/settings/ChatAclSettingsActivityGingerbread;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "account_name"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbue;->a(Ljava/lang/String;)V

    .line 29
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 45
    invoke-super {p0}, Lbux;->onDestroy()V

    .line 46
    iget-object v0, p0, Lcom/google/android/apps/hangouts/settings/ChatAclSettingsActivityGingerbread;->a:Lbue;

    invoke-virtual {v0}, Lbue;->b()V

    .line 47
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 36
    invoke-super {p0}, Lbux;->onResume()V

    .line 37
    iget-object v0, p0, Lcom/google/android/apps/hangouts/settings/ChatAclSettingsActivityGingerbread;->a:Lbue;

    invoke-virtual {v0}, Lbue;->a()V

    .line 38
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 51
    invoke-super {p0}, Lbux;->onStop()V

    .line 52
    iget-object v0, p0, Lcom/google/android/apps/hangouts/settings/ChatAclSettingsActivityGingerbread;->a:Lbue;

    invoke-virtual {v0}, Lbue;->c()V

    .line 53
    return-void
.end method
