.class public Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;
.super Landroid/preference/RingtonePreference;
.source "PG"


# static fields
.field private static final a:Landroid/net/Uri;

.field private static final b:Landroid/net/Uri;


# instance fields
.field private c:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    sget v0, Lf;->hO:I

    .line 40
    invoke-static {v0}, Lf;->g(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;->a:Landroid/net/Uri;

    .line 41
    sget v0, Lf;->hN:I

    .line 42
    invoke-static {v0}, Lf;->g(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;->b:Landroid/net/Uri;

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1}, Landroid/preference/RingtonePreference;-><init>(Landroid/content/Context;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Landroid/preference/RingtonePreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, Landroid/preference/RingtonePreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    return-void
.end method

.method public static a()V
    .locals 5

    .prologue
    .line 64
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "babel_ringtones"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 65
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 68
    sget v1, Lf;->hO:I

    sget v2, Lh;->ar:I

    const-string v3, ".ogg"

    const/4 v4, 0x2

    invoke-static {v1, v2, v3, v4}, Lf;->a(IILjava/lang/String;I)Landroid/net/Uri;

    move-result-object v1

    .line 71
    if-eqz v1, :cond_0

    .line 72
    const-string v2, "message_ringtone"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 76
    :cond_0
    sget v1, Lf;->hN:I

    sget v2, Lh;->er:I

    const-string v3, ".ogg"

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lf;->a(IILjava/lang/String;I)Landroid/net/Uri;

    move-result-object v1

    .line 79
    if-eqz v1, :cond_1

    .line 80
    const-string v2, "incoming_call_ringtone"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 83
    :cond_1
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 84
    return-void
.end method

.method public static a(I)Z
    .locals 1

    .prologue
    .line 114
    sget v0, Lf;->hO:I

    if-eq p0, v0, :cond_0

    sget v0, Lf;->hN:I

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(I)Landroid/net/Uri;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 128
    invoke-static {p0}, Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;->a(I)Z

    move-result v0

    invoke-static {v0}, Lcwz;->a(Z)V

    .line 129
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v2, "babel_ringtones"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 132
    sget v0, Lf;->hO:I

    if-ne p0, v0, :cond_0

    .line 133
    const-string v0, "message_ringtone"

    .line 140
    :goto_0
    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 141
    if-nez v0, :cond_2

    :goto_1
    return-object v1

    .line 134
    :cond_0
    sget v0, Lf;->hN:I

    if-ne p0, v0, :cond_1

    .line 135
    const-string v0, "incoming_call_ringtone"

    goto :goto_0

    .line 138
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "Unknown resId: "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0

    .line 141
    :cond_2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_1
.end method

.method public static b(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 160
    sget-object v0, Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;->a:Landroid/net/Uri;

    invoke-virtual {v0, p0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 161
    sget v0, Lf;->hO:I

    invoke-static {v0}, Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;->b(I)Landroid/net/Uri;

    move-result-object p0

    .line 165
    :cond_0
    :goto_0
    return-object p0

    .line 162
    :cond_1
    sget-object v0, Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;->b:Landroid/net/Uri;

    invoke-virtual {v0, p0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    sget v0, Lf;->hN:I

    invoke-static {v0}, Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;->b(I)Landroid/net/Uri;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 91
    iput-object p1, p0, Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;->c:Landroid/net/Uri;

    .line 95
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 96
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 98
    sget-object v0, Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;->a:Landroid/net/Uri;

    invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    sget v0, Lh;->ar:I

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 110
    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 111
    return-void

    .line 100
    :cond_0
    sget-object v0, Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;->b:Landroid/net/Uri;

    invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 101
    sget v0, Lh;->er:I

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 103
    :cond_1
    const/4 v0, 0x0

    .line 104
    if-eqz p1, :cond_2

    .line 105
    invoke-static {v1, p1}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v0

    .line 107
    :cond_2
    if-nez v0, :cond_3

    sget v0, Lh;->li:I

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 108
    :cond_3
    invoke-virtual {v0, v1}, Landroid/media/Ringtone;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 87
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;->a(Landroid/net/Uri;)V

    .line 88
    return-void

    .line 87
    :cond_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public b()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;->c:Landroid/net/Uri;

    return-object v0
.end method

.method protected onRestoreRingtone()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 150
    sget-object v0, Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;->a:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;->c:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    sget v0, Lf;->hO:I

    invoke-static {v0}, Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;->b(I)Landroid/net/Uri;

    move-result-object v0

    .line 155
    :goto_0
    return-object v0

    .line 152
    :cond_0
    sget-object v0, Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;->b:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;->c:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 153
    sget v0, Lf;->hN:I

    invoke-static {v0}, Lcom/google/android/apps/hangouts/settings/BabelRingtonePreference;->b(I)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 155
    :cond_1
    invoke-super {p0}, Landroid/preference/RingtonePreference;->onRestoreRingtone()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method
