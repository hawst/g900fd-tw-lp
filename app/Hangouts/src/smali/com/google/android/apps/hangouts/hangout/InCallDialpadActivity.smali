.class public Lcom/google/android/apps/hangouts/hangout/InCallDialpadActivity;
.super Ly;
.source "PG"


# instance fields
.field private final n:Lapk;

.field private o:Laqk;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ly;-><init>()V

    .line 23
    invoke-static {}, Lapk;->b()Lapk;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/InCallDialpadActivity;->n:Lapk;

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 28
    invoke-super {p0, p1}, Ly;->onCreate(Landroid/os/Bundle;)V

    .line 29
    sget v0, Lf;->fE:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/hangout/InCallDialpadActivity;->setContentView(I)V

    .line 30
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 34
    invoke-super {p0}, Ly;->onStart()V

    .line 35
    new-instance v0, Laqk;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Laqk;-><init>(Lcom/google/android/apps/hangouts/hangout/InCallDialpadActivity;B)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/InCallDialpadActivity;->o:Laqk;

    .line 36
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/InCallDialpadActivity;->n:Lapk;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/InCallDialpadActivity;->o:Laqk;

    invoke-virtual {v0, v1}, Lapk;->a(Lapo;)V

    .line 37
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 41
    invoke-super {p0}, Ly;->onStop()V

    .line 42
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/InCallDialpadActivity;->n:Lapk;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/InCallDialpadActivity;->o:Laqk;

    invoke-virtual {v0, v1}, Lapk;->b(Lapo;)V

    .line 43
    return-void
.end method
