.class public Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;
.super Lakl;
.source "PG"

# interfaces
.implements Law;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lakl;",
        "Law",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private final Y:Lbor;

.field private a:Lyj;

.field private b:Landroid/widget/ListView;

.field private c:Lakt;

.field private d:Z

.field private e:I

.field private f:Landroid/os/Handler;

.field private g:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private h:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lcco;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Lakl;-><init>()V

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->d:Z

    .line 55
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->e:I

    .line 69
    new-instance v0, Lakr;

    invoke-direct {v0, p0}, Lakr;-><init>(Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->i:Lcco;

    .line 85
    new-instance v0, Laks;

    invoke-direct {v0, p0}, Laks;-><init>(Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->Y:Lbor;

    .line 221
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;)Landroid/util/SparseArray;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->h:Landroid/util/SparseArray;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;I)V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->g:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->h:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->h:Landroid/util/SparseArray;

    invoke-static {v0}, Lf;->a(Landroid/util/SparseArray;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->c()V

    :cond_0
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->d(Landroid/view/View;)V

    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 145
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->d:Z

    if-nez v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->Y:Lbor;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbor;)V

    .line 147
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->d:Z

    .line 149
    :cond_0
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->b()V

    return-void
.end method

.method public static synthetic c(Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;)Lyj;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->a:Lyj;

    return-object v0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 152
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->d:Z

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->Y:Lbor;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lbor;)V

    .line 154
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->d:Z

    .line 156
    :cond_0
    return-void
.end method

.method public static synthetic d(Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;)Landroid/util/SparseArray;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->g:Landroid/util/SparseArray;

    return-object v0
.end method

.method private d(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 327
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 328
    invoke-virtual {p0, p1}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->e(Landroid/view/View;)V

    .line 338
    :goto_0
    return-void

    .line 329
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 330
    invoke-virtual {p0, p1}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->c(Landroid/view/View;)V

    .line 331
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lh;->fA:I

    .line 332
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 333
    const/4 v1, 0x0

    invoke-static {p1, v1, v0}, Lf;->a(Landroid/view/View;Landroid/view/accessibility/AccessibilityManager;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 336
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->b(Landroid/view/View;)V

    goto :goto_0
.end method

.method public static synthetic e(Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;)Lakt;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->c:Lakt;

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;)I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->e:I

    return v0
.end method

.method public static synthetic g(Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;)V
    .locals 1

    .prologue
    .line 46
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->e:I

    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->c()V

    return-void
.end method

.method public static synthetic h(Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;)Lcco;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->i:Lcco;

    return-object v0
.end method


# virtual methods
.method protected a(Landroid/view/View;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 345
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 346
    const v0, 0x1020004

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 347
    sget v0, Lg;->eb:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 348
    sget v0, Lg;->eg:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 350
    :cond_0
    return-void
.end method

.method public a(Ldg;Landroid/database/Cursor;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 390
    invoke-virtual {p1}, Ldg;->l()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 396
    :goto_0
    return-void

    .line 392
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->c:Lakt;

    invoke-virtual {v0, p2}, Lakt;->a(Landroid/database/Cursor;)V

    .line 393
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->d(Landroid/view/View;)V

    goto :goto_0

    .line 390
    nop

    :pswitch_data_0
    .packed-switch 0x403
        :pswitch_0
    .end packed-switch
.end method

.method protected a()Z
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->c:Lakt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->c:Lakt;

    invoke-virtual {v0}, Lakt;->a()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->e:I

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 359
    invoke-super {p0, p1}, Lakl;->b(Landroid/view/View;)V

    .line 360
    sget v0, Lg;->eb:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 361
    sget v0, Lg;->eg:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 362
    return-void
.end method

.method protected isEmpty()Z
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->c:Lakt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->c:Lakt;

    invoke-virtual {v0}, Lakt;->a()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->c:Lakt;

    invoke-virtual {v0}, Lakt;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 214
    invoke-super {p0, p1}, Lakl;->onAttach(Landroid/app/Activity;)V

    .line 216
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 217
    const-string v1, "account_name"

    .line 218
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 217
    invoke-static {v0}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->a:Lyj;

    .line 219
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 182
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->f:Landroid/os/Handler;

    .line 183
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->g:Landroid/util/SparseArray;

    .line 184
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->h:Landroid/util/SparseArray;

    .line 185
    invoke-super {p0, p1}, Lakl;->onCreate(Landroid/os/Bundle;)V

    .line 186
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Ldg;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 373
    packed-switch p1, :pswitch_data_0

    move-object v0, v5

    .line 385
    :goto_0
    return-object v0

    .line 375
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->a:Lyj;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/content/EsProvider;->d(Lyj;)Landroid/net/Uri;

    move-result-object v3

    .line 376
    new-instance v0, Lbaj;

    .line 377
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->getActivity()Ly;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->a:Lyj;

    sget-object v4, Laku;->a:[Ljava/lang/String;

    const-string v7, "name ASC"

    move-object v6, v5

    invoke-direct/range {v0 .. v7}, Lbaj;-><init>(Landroid/content/Context;Lyj;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 373
    nop

    :pswitch_data_0
    .packed-switch 0x403
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 301
    sget v0, Lf;->fB:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 302
    sget v0, Lg;->eg:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->b:Landroid/widget/ListView;

    .line 303
    new-instance v0, Lakt;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->getActivity()Ly;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Lakt;-><init>(Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->c:Lakt;

    .line 304
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->b:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->c:Lakt;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 306
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->getLoaderManager()Lav;

    move-result-object v0

    const/16 v2, 0x403

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0, v2, v3, p0}, Lav;->a(ILandroid/os/Bundle;Law;)Ldg;

    move-result-object v0

    invoke-virtual {v0}, Ldg;->p()V

    .line 307
    return-object v1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 190
    invoke-super {p0}, Lakl;->onDestroy()V

    .line 191
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->c()V

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->b:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 193
    iput-object v1, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->g:Landroid/util/SparseArray;

    .line 194
    iput-object v1, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->h:Landroid/util/SparseArray;

    .line 195
    return-void
.end method

.method public synthetic onLoadFinished(Ldg;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 46
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->a(Ldg;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Ldg;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 400
    invoke-virtual {p1}, Ldg;->l()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 405
    :goto_0
    return-void

    .line 402
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->c:Lakt;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lakt;->a(Landroid/database/Cursor;)V

    goto :goto_0

    .line 400
    nop

    :pswitch_data_0
    .packed-switch 0x403
        :pswitch_0
    .end packed-switch
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 312
    invoke-super {p0, p1}, Lakl;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 313
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 199
    invoke-super {p0}, Lakl;->onStart()V

    .line 200
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->b()V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->a:Lyj;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;Z)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->e:I

    .line 201
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->d(Landroid/view/View;)V

    .line 202
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 206
    invoke-super {p0}, Lakl;->onStop()V

    .line 207
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->c()V

    .line 208
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->h:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 209
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/HiddenContactsFragment;->g:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 210
    return-void
.end method
