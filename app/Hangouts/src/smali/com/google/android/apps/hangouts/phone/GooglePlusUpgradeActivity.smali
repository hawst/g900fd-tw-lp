.class public Lcom/google/android/apps/hangouts/phone/GooglePlusUpgradeActivity;
.super Ly;
.source "PG"


# static fields
.field public static final n:[Ljava/lang/String;


# instance fields
.field private o:Landroid/widget/Button;

.field private p:Landroid/widget/Button;

.field private q:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 36
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "service_esmobile"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "service_googleme"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/hangouts/phone/GooglePlusUpgradeActivity;->n:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ly;-><init>()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/phone/GooglePlusUpgradeActivity;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/GooglePlusUpgradeActivity;->o:Landroid/widget/Button;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/phone/GooglePlusUpgradeActivity;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/GooglePlusUpgradeActivity;->h()V

    return-void
.end method

.method public static synthetic c(Lcom/google/android/apps/hangouts/phone/GooglePlusUpgradeActivity;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/GooglePlusUpgradeActivity;->p:Landroid/widget/Button;

    return-object v0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/GooglePlusUpgradeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/GooglePlusUpgradeActivity;->q:Ljava/lang/String;

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/GooglePlusUpgradeActivity;->q:Ljava/lang/String;

    invoke-static {v0}, Lciq;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 111
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/hangouts/phone/GooglePlusUpgradeActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 112
    return-void
.end method


# virtual methods
.method protected a()I
    .locals 1

    .prologue
    .line 74
    sget v0, Lf;->fk:I

    return v0
.end method

.method protected g()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v3, -0x1

    .line 83
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/GooglePlusUpgradeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "g_plus_upgrade_type"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 88
    const-string v2, "g_plus_upgrade_photo"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 89
    sget v2, Lcom/google/android/apps/hangouts/R$drawable;->ch:I

    .line 90
    sget v1, Lh;->nI:I

    .line 91
    sget v0, Lh;->bw:I

    move v4, v2

    move v2, v1

    move v1, v0

    .line 97
    :goto_0
    if-eq v2, v3, :cond_0

    .line 98
    sget v0, Lg;->hX:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/GooglePlusUpgradeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 99
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 101
    sget v0, Lg;->hY:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/GooglePlusUpgradeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 102
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/GooglePlusUpgradeActivity;->p:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 106
    :cond_0
    return-void

    .line 92
    :cond_1
    const-string v2, "g_plus_upgrade_hangout"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 93
    sget v2, Lcom/google/android/apps/hangouts/R$drawable;->ci:I

    .line 94
    sget v1, Lh;->nH:I

    .line 95
    sget v0, Lh;->bv:I

    move v4, v2

    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_2
    move v1, v0

    move v2, v3

    move v4, v0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 116
    if-ne p1, v5, :cond_1

    .line 117
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 118
    if-ne p2, v6, :cond_2

    .line 119
    sget v1, Lh;->cY:I

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/phone/GooglePlusUpgradeActivity;->q:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/hangouts/phone/GooglePlusUpgradeActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 120
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 123
    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/GooglePlusUpgradeActivity;->q:Ljava/lang/String;

    invoke-static {v1}, Lbkb;->b(Ljava/lang/String;)Lyj;

    move-result-object v1

    .line 124
    if-eqz v1, :cond_0

    .line 125
    invoke-virtual {v1}, Lyj;->E()V

    .line 127
    :cond_0
    invoke-virtual {p0, v6, v0}, Lcom/google/android/apps/hangouts/phone/GooglePlusUpgradeActivity;->setResult(ILandroid/content/Intent;)V

    .line 133
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/GooglePlusUpgradeActivity;->finish()V

    .line 135
    :cond_1
    return-void

    .line 129
    :cond_2
    sget v1, Lh;->cX:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/hangouts/phone/GooglePlusUpgradeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 130
    invoke-virtual {p0, v4, v0}, Lcom/google/android/apps/hangouts/phone/GooglePlusUpgradeActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 41
    invoke-super {p0, p1}, Ly;->onCreate(Landroid/os/Bundle;)V

    .line 44
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/GooglePlusUpgradeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "g_plus_upgrade_now"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/phone/GooglePlusUpgradeActivity;->h()V

    .line 71
    :goto_0
    return-void

    .line 48
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/GooglePlusUpgradeActivity;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/GooglePlusUpgradeActivity;->setContentView(I)V

    .line 50
    sget v0, Lg;->dW:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/GooglePlusUpgradeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/GooglePlusUpgradeActivity;->o:Landroid/widget/Button;

    .line 51
    sget v0, Lg;->bf:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/GooglePlusUpgradeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/phone/GooglePlusUpgradeActivity;->p:Landroid/widget/Button;

    .line 53
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/GooglePlusUpgradeActivity;->g()V

    .line 55
    new-instance v0, Lbbb;

    invoke-direct {v0, p0}, Lbbb;-><init>(Lcom/google/android/apps/hangouts/phone/GooglePlusUpgradeActivity;)V

    .line 68
    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/GooglePlusUpgradeActivity;->o:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    iget-object v1, p0, Lcom/google/android/apps/hangouts/phone/GooglePlusUpgradeActivity;->p:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method
