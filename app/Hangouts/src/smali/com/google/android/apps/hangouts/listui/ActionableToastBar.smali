.class public Lcom/google/android/apps/hangouts/listui/ActionableToastBar;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field private a:Z

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Laup;

.field private e:Landroid/view/View;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 40
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->a:Z

    .line 41
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    sget v3, Lf;->iv:I

    invoke-virtual {v2, v3, p0, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 43
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->h:Z

    .line 44
    return-void

    :cond_0
    move v0, v1

    .line 43
    goto :goto_0
.end method

.method private a()Landroid/animation/Animator;
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 135
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lf;->ij:I

    invoke-static {v0, v1}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->b:Ljava/lang/Object;

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->b:Ljava/lang/Object;

    check-cast v0, Landroid/animation/Animator;

    new-instance v1, Laun;

    invoke-direct {v1, p0}, Laun;-><init>(Lcom/google/android/apps/hangouts/listui/ActionableToastBar;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->b:Ljava/lang/Object;

    check-cast v0, Landroid/animation/Animator;

    invoke-virtual {v0, p0}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    .line 160
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->b:Ljava/lang/Object;

    check-cast v0, Landroid/animation/Animator;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/listui/ActionableToastBar;)Laup;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->d:Laup;

    return-object v0
.end method

.method private a(ZZ)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 110
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->a:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->h:Z

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->a()Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/Animator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    if-nez v0, :cond_2

    .line 111
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->a:Z

    .line 112
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    if-eqz p1, :cond_4

    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->h:Z

    if-eqz v0, :cond_4

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->c:Ljava/lang/Object;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lf;->ik:I

    invoke-static {v0, v1}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->c:Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->c:Ljava/lang/Object;

    check-cast v0, Landroid/animation/Animator;

    new-instance v1, Lauo;

    invoke-direct {v1, p0}, Lauo;-><init>(Lcom/google/android/apps/hangouts/listui/ActionableToastBar;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->c:Ljava/lang/Object;

    check-cast v0, Landroid/animation/Animator;

    invoke-virtual {v0, p0}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->c:Ljava/lang/Object;

    check-cast v0, Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 123
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->d:Laup;

    if-eqz v0, :cond_2

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->d:Laup;

    invoke-interface {v0, p2}, Laup;->a(Z)V

    .line 127
    :cond_2
    return-void

    .line 110
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 118
    :cond_4
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->setAlpha(F)V

    .line 119
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->setVisibility(I)V

    goto :goto_1
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/listui/ActionableToastBar;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 19
    invoke-direct {p0, v0, v0}, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->a(ZZ)V

    return-void
.end method


# virtual methods
.method public a(Laup;Ljava/lang/CharSequence;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 69
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->a:Z

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->d:Laup;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->d:Laup;

    invoke-interface {v0, v2}, Laup;->a(Z)V

    .line 77
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->d:Laup;

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->e:Landroid/view/View;

    new-instance v1, Laum;

    invoke-direct {v1, p0}, Laum;-><init>(Lcom/google/android/apps/hangouts/listui/ActionableToastBar;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(I)V

    .line 92
    iput-boolean v2, p0, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->a:Z

    .line 93
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->h:Z

    if-eqz v0, :cond_1

    .line 94
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->a()Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 98
    :goto_0
    return-void

    .line 96
    :cond_1
    invoke-virtual {p0, v2}, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->a(ZZ)V

    .line 105
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 48
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 50
    sget v0, Lf;->ip:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->f:Landroid/widget/TextView;

    .line 51
    sget v0, Lf;->in:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->e:Landroid/view/View;

    .line 52
    sget v0, Lf;->io:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/listui/ActionableToastBar;->g:Landroid/widget/TextView;

    .line 53
    return-void
.end method
