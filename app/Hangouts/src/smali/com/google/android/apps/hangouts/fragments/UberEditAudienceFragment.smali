.class public Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;
.super Lakl;
.source "PG"


# instance fields
.field private Y:Z

.field private Z:Z

.field private a:Landroid/app/Activity;

.field private aa:Laod;

.field private ab:Landroid/animation/ValueAnimator;

.field private ac:I

.field private final ad:Landroid/view/View$OnClickListener;

.field private final ae:Lbor;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Labj;

.field private e:Labk;

.field private f:Z

.field private g:I

.field private h:Z

.field private i:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Lakl;-><init>()V

    .line 396
    new-instance v0, Lanx;

    invoke-direct {v0, p0}, Lanx;-><init>(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->ad:Landroid/view/View$OnClickListener;

    .line 527
    new-instance v0, Lanz;

    invoke-direct {v0, p0}, Lanz;-><init>(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->ae:Lbor;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;)Landroid/animation/ValueAnimator;
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->ab:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method private a(I)V
    .locals 4

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->e:Labk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->e:Labk;

    iget-object v0, v0, Labk;->h:Landroid/widget/AbsListView;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->e:Labk;

    iget-object v0, v0, Labk;->h:Landroid/widget/AbsListView;

    .line 116
    invoke-virtual {v0}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    .line 117
    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    .line 118
    invoke-virtual {v0}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    .line 115
    invoke-virtual {v0, v1, v2, v3, p1}, Landroid/view/View;->setPadding(IIII)V

    .line 121
    :cond_0
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;I)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->a(I)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 53
    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->f:Z

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->a(ZLjava/lang/Runnable;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;Lxm;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->a(Lxm;)V

    return-void
.end method

.method private a(Lxm;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 474
    const-string v0, "Babel"

    const-string v2, "createConversation"

    invoke-static {v0, v2}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    invoke-virtual {p1}, Lxm;->a()Ljava/util/List;

    move-result-object v0

    .line 478
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->c()Lyj;

    move-result-object v2

    .line 479
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxs;

    .line 480
    invoke-virtual {v0}, Lxs;->b()Lbcx;

    move-result-object v0

    iget-object v0, v0, Lbcx;->d:Ljava/lang/String;

    .line 481
    if-eqz v0, :cond_0

    .line 482
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isEmergencyNumber(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    move-object v0, v1

    .line 493
    :goto_0
    invoke-static {v0}, Lbkb;->c(Lyj;)Lyj;

    move-result-object v2

    .line 497
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->c()Lyj;

    move-result-object v0

    invoke-virtual {v2, v0}, Lyj;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 498
    sget v0, Lh;->kV:I

    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {v2}, Lyj;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v4

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 499
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->getActivity()Ly;

    move-result-object v1

    invoke-static {v1, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 502
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->ae:Lbor;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbor;)V

    .line 504
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->g:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    move v0, v3

    .line 503
    :goto_1
    invoke-static {v2, p1, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Lxm;Z)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->ac:I

    .line 506
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->d:Labj;

    invoke-static {}, Lxm;->newBuilder()Lxn;

    move-result-object v1

    invoke-virtual {v1}, Lxn;->a()Lxm;

    move-result-object v1

    invoke-virtual {v0, v1}, Labj;->a(Lxm;)V

    .line 507
    invoke-static {}, Lavy;->e()V

    .line 508
    return-void

    .line 488
    :cond_3
    sget-object v5, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    invoke-virtual {v5, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_5

    move-object v0, v1

    .line 491
    goto :goto_0

    :cond_4
    move v0, v4

    .line 504
    goto :goto_1

    :cond_5
    move-object v0, v2

    goto :goto_0
.end method

.method private a(ZLjava/lang/Runnable;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 133
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->f:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->d:Labj;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->d:Labj;

    .line 134
    invoke-virtual {v0}, Labj;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 136
    :goto_0
    iget v3, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->g:I

    if-ne v3, v1, :cond_2

    if-eqz v0, :cond_2

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->d:Labj;

    invoke-virtual {v0}, Labj;->d()Lxm;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->a(Lxm;)V

    .line 157
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 134
    goto :goto_0

    .line 141
    :cond_2
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v1, v3, :cond_3

    .line 142
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->a(ZLjava/lang/Runnable;Z)V

    .line 155
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->c:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 156
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_1

    .line 146
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->getView()Landroid/view/View;

    move-result-object v1

    sget v3, Lg;->bF:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 147
    if-eqz v0, :cond_5

    move v1, v2

    :goto_2
    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 148
    if-eqz v0, :cond_4

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    :cond_4
    invoke-direct {p0, v2}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->a(I)V

    .line 149
    if-eqz p2, :cond_0

    .line 150
    invoke-interface {p2}, Ljava/lang/Runnable;->run()V

    goto :goto_1

    .line 147
    :cond_5
    const/16 v1, 0x8

    goto :goto_2
.end method

.method private a(ZLjava/lang/Runnable;Z)V
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 168
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v2, Lg;->bF:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 169
    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 170
    if-nez v0, :cond_1

    .line 236
    :cond_0
    :goto_0
    return-void

    .line 174
    :cond_1
    if-eqz p3, :cond_3

    move v2, v1

    .line 175
    :goto_1
    if-eqz p1, :cond_5

    .line 176
    iget-object v4, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->ab:Landroid/animation/ValueAnimator;

    if-eqz v4, :cond_2

    .line 177
    iget-object v4, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->ab:Landroid/animation/ValueAnimator;

    invoke-virtual {v4}, Landroid/animation/ValueAnimator;->cancel()V

    .line 178
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->ab:Landroid/animation/ValueAnimator;

    .line 181
    :cond_2
    invoke-virtual {v3}, Landroid/view/View;->getTranslationY()F

    move-result v4

    float-to-int v4, v4

    .line 182
    if-eq v2, v4, :cond_4

    .line 183
    const-string v5, "translationY"

    const/4 v6, 0x2

    new-array v6, v6, [F

    int-to-float v4, v4

    aput v4, v6, v1

    int-to-float v2, v2

    aput v2, v6, v7

    .line 185
    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    .line 187
    new-array v4, v7, [Landroid/animation/PropertyValuesHolder;

    aput-object v2, v4, v1

    invoke-static {v3, v4}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 188
    const-wide/16 v2, 0xc8

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 189
    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 190
    iput-object v1, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->ab:Landroid/animation/ValueAnimator;

    .line 191
    new-instance v2, Lanw;

    invoke-direct {v2, p0, p3, v0, p2}, Lanw;-><init>(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;ZILjava/lang/Runnable;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 223
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    :cond_3
    move v2, v0

    .line 174
    goto :goto_1

    .line 225
    :cond_4
    if-eqz p2, :cond_0

    .line 226
    invoke-interface {p2}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 230
    :cond_5
    int-to-float v2, v2

    invoke-virtual {v3, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 231
    if-eqz p3, :cond_6

    :goto_2
    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->a(I)V

    .line 232
    if-eqz p2, :cond_0

    .line 233
    invoke-interface {p2}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    :cond_6
    move v0, v1

    .line 231
    goto :goto_2
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 53
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->aa:Laod;

    invoke-interface {v0}, Laod;->c()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "audience"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lxm;

    if-eqz v0, :cond_4

    if-eqz p3, :cond_1

    invoke-virtual {v0}, Lxm;->d()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_0
    if-ltz v2, :cond_4

    invoke-virtual {v0, v2}, Lxm;->b(I)Lxo;

    move-result-object v3

    invoke-virtual {v3}, Lxo;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lxm;->b()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_2
    if-ltz v2, :cond_4

    invoke-virtual {v0, v2}, Lxm;->a(I)Lxs;

    move-result-object v3

    invoke-virtual {v3}, Lxs;->b()Lbcx;

    move-result-object v3

    if-eqz p1, :cond_2

    iget-object v3, v3, Lbcx;->a:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    move v0, v1

    goto :goto_1

    :cond_2
    if-eqz p2, :cond_3

    iget-object v3, v3, Lbcx;->d:Ljava/lang/String;

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    move v0, v1

    goto :goto_1

    :cond_3
    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;Z)Z
    .locals 0

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->h:Z

    return p1
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;)Laod;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->aa:Laod;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;)Labk;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->e:Labk;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;)I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->g:I

    return v0
.end method

.method public static synthetic e(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->aa:Laod;

    invoke-interface {v0}, Laod;->c()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "conversation_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private e()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x4

    const/4 v2, -0x1

    const/4 v4, 0x0

    .line 586
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->i:Z

    .line 587
    iput v2, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->ac:I

    .line 589
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->aa:Laod;

    invoke-interface {v0}, Laod;->c()Landroid/os/Bundle;

    move-result-object v0

    .line 590
    const-string v1, " edit_audience_mode"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->g:I

    .line 591
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->g:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcwz;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 593
    iget v1, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->g:I

    if-ne v1, v5, :cond_0

    .line 594
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->getView()Landroid/view/View;

    move-result-object v1

    sget v2, Lg;->bF:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 595
    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    new-instance v3, Laoa;

    invoke-direct {v3, p0, v1}, Laoa;-><init>(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;Landroid/view/View;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 604
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->e:Labk;

    new-instance v2, Laob;

    invoke-direct {v2, p0}, Laob;-><init>(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;)V

    invoke-virtual {v1, v2}, Labk;->a(Laoe;)V

    .line 621
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->d:Labj;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->aa:Laod;

    invoke-virtual {v1, v2}, Labj;->a(Laod;)V

    .line 622
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->d:Labj;

    new-instance v2, Laoc;

    invoke-direct {v2, p0}, Laoc;-><init>(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;)V

    invoke-virtual {v1, v2}, Labj;->a(Laoe;)V

    .line 643
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->e:Labk;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->aa:Laod;

    invoke-interface {v2}, Laod;->d()I

    move-result v2

    invoke-virtual {v1, v2}, Labk;->a(I)V

    .line 645
    iget v1, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->g:I

    packed-switch v1, :pswitch_data_0

    .line 668
    :goto_0
    iput-boolean v4, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->f:Z

    .line 669
    const/4 v0, 0x0

    invoke-direct {p0, v4, v0}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->a(ZLjava/lang/Runnable;)V

    .line 670
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lg;->bF:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->g:I

    if-eq v1, v5, :cond_1

    iget v1, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->g:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->g:I

    const/4 v2, 0x5

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->g:I

    const/4 v2, 0x6

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->g:I

    if-ne v1, v6, :cond_4

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->b:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->ad:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->c:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->ad:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->g:I

    if-ne v0, v5, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lg;->bG:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 671
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 672
    return-void

    .line 652
    :pswitch_0
    invoke-static {}, Lxm;->newBuilder()Lxn;

    move-result-object v0

    invoke-virtual {v0}, Lxn;->a()Lxm;

    move-result-object v0

    .line 653
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->d:Labj;

    invoke-virtual {v1, v0}, Labj;->a(Lxm;)V

    .line 654
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->e:Labk;

    invoke-virtual {v1, v0}, Labk;->a(Lxm;)V

    goto :goto_0

    .line 659
    :pswitch_1
    const-string v1, "audience"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lxm;

    .line 660
    if-eqz v0, :cond_3

    .line 661
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->d:Labj;

    invoke-virtual {v1, v0}, Labj;->a(Lxm;)V

    goto :goto_0

    .line 663
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->d:Labj;

    invoke-static {}, Lxm;->newBuilder()Lxn;

    move-result-object v1

    invoke-virtual {v1}, Lxn;->a()Lxm;

    move-result-object v1

    invoke-virtual {v0, v1}, Labj;->a(Lxm;)V

    goto/16 :goto_0

    .line 670
    :cond_4
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 645
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static synthetic f(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;)I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->ac:I

    return v0
.end method

.method public static synthetic g(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;)Labj;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->d:Labj;

    return-object v0
.end method

.method public static synthetic h(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;)Lbor;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->ae:Lbor;

    return-object v0
.end method

.method public static synthetic i(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, v1, v0, v1}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->a(ZLjava/lang/Runnable;Z)V

    return-void
.end method

.method public static synthetic j(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;)Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->h:Z

    return v0
.end method

.method public static synthetic k(Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;)V
    .locals 2

    .prologue
    .line 53
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->a(ZLjava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public a(Laod;)V
    .locals 0

    .prologue
    .line 318
    iput-object p1, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->aa:Laod;

    .line 319
    return-void
.end method

.method public a()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 240
    iget v1, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->g:I

    packed-switch v1, :pswitch_data_0

    .line 252
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->aa:Laod;

    if-eqz v1, :cond_0

    .line 253
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->aa:Laod;

    invoke-interface {v0}, Laod;->a()V

    .line 254
    const/4 v0, 0x1

    .line 256
    :cond_0
    :pswitch_0
    return v0

    .line 240
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public b()V
    .locals 1

    .prologue
    .line 322
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 323
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->d:Labj;

    invoke-virtual {v0}, Labj;->a()V

    .line 327
    :goto_0
    return-void

    .line 325
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->Z:Z

    goto :goto_0
.end method

.method public c()Lyj;
    .locals 1

    .prologue
    .line 511
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->aa:Laod;

    if-nez v0, :cond_0

    .line 512
    const/4 v0, 0x0

    .line 515
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->aa:Laod;

    invoke-interface {v0}, Laod;->k()Lyj;

    move-result-object v0

    goto :goto_0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 676
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->i:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->Y:Z

    if-eqz v0, :cond_0

    .line 677
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->e()V

    .line 679
    :cond_0
    return-void
.end method

.method protected isEmpty()Z
    .locals 1

    .prologue
    .line 524
    const/4 v0, 0x0

    return v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 279
    invoke-super {p0, p1}, Lakl;->onAttach(Landroid/app/Activity;)V

    .line 280
    iput-object p1, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->a:Landroid/app/Activity;

    .line 281
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 270
    invoke-super {p0, p1}, Lakl;->onCreate(Landroid/os/Bundle;)V

    .line 271
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->setHasOptionsMenu(Z)V

    .line 272
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 286
    sget v0, Lf;->gC:I

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 288
    sget v0, Lg;->eQ:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->b:Landroid/view/View;

    .line 289
    sget v0, Lg;->ah:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->c:Landroid/view/View;

    .line 291
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->getChildFragmentManager()Lae;

    move-result-object v0

    const-class v3, Labj;

    .line 292
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    .line 291
    invoke-virtual {v0, v3}, Lae;->a(Ljava/lang/String;)Lt;

    move-result-object v0

    check-cast v0, Labj;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->d:Labj;

    .line 293
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->getChildFragmentManager()Lae;

    move-result-object v0

    const-class v3, Labk;

    .line 294
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    .line 293
    invoke-virtual {v0, v3}, Lae;->a(Ljava/lang/String;)Lt;

    move-result-object v0

    check-cast v0, Labk;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->e:Labk;

    .line 297
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->d:Labj;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->e:Labk;

    if-nez v3, :cond_2

    move v3, v1

    :goto_1
    if-ne v0, v3, :cond_3

    :goto_2
    invoke-static {v1}, Lcwz;->a(Z)V

    .line 298
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->d:Labj;

    if-nez v0, :cond_0

    .line 299
    new-instance v0, Labj;

    invoke-direct {v0}, Labj;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->d:Labj;

    .line 300
    new-instance v0, Labk;

    invoke-direct {v0}, Labk;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->e:Labk;

    .line 301
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->getChildFragmentManager()Lae;

    move-result-object v0

    invoke-virtual {v0}, Lae;->a()Lao;

    move-result-object v0

    sget v1, Lg;->x:I

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->d:Labj;

    const-class v3, Labj;

    .line 303
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    .line 302
    invoke-virtual {v0, v1, v2, v3}, Lao;->a(ILt;Ljava/lang/String;)Lao;

    move-result-object v0

    sget v1, Lg;->y:I

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->e:Labk;

    const-class v3, Labk;

    .line 305
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    .line 304
    invoke-virtual {v0, v1, v2, v3}, Lao;->a(ILt;Ljava/lang/String;)Lao;

    move-result-object v0

    .line 306
    invoke-virtual {v0}, Lao;->b()I

    .line 309
    :cond_0
    return-object v4

    :cond_1
    move v0, v2

    .line 297
    goto :goto_0

    :cond_2
    move v3, v2

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 314
    invoke-super {p0}, Lakl;->onDestroyView()V

    .line 315
    return-void
.end method

.method public onHiddenChanged(Z)V
    .locals 2

    .prologue
    .line 570
    invoke-super {p0, p1}, Lakl;->onHiddenChanged(Z)V

    .line 573
    if-eqz p1, :cond_0

    .line 574
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->e:Labk;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Labk;->a(Lxm;)V

    .line 576
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 690
    invoke-super {p0}, Lakl;->onResume()V

    .line 691
    const-string v0, "Babel"

    const-string v1, "Resuming UberEditAudienceFragment"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 692
    iput-boolean v2, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->f:Z

    .line 693
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->a(ZLjava/lang/Runnable;)V

    .line 694
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->Z:Z

    if-eqz v0, :cond_0

    .line 695
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->d:Labj;

    invoke-virtual {v0}, Labj;->a()V

    .line 696
    iput-boolean v2, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->Z:Z

    .line 698
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 580
    invoke-super {p0}, Lakl;->onStart()V

    .line 581
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->Y:Z

    .line 582
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->e()V

    .line 583
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 683
    invoke-super {p0}, Lakl;->onStop()V

    .line 684
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->Y:Z

    .line 685
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/UberEditAudienceFragment;->ae:Lbor;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lbor;)V

    .line 686
    return-void
.end method
