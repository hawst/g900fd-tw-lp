.class public Lcom/google/android/apps/hangouts/phone/ArchivedConversationListActivity;
.super Layq;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Layq;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;ZII)V
    .locals 9

    .prologue
    const/4 v6, 0x1

    .line 30
    new-instance v0, Lbbw;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/phone/ArchivedConversationListActivity;->r:Lyj;

    move-object v1, p0

    move-object v3, p1

    move v4, p2

    move v5, p3

    move v7, p4

    move v8, v6

    invoke-direct/range {v0 .. v8}, Lbbw;-><init>(Landroid/app/Activity;Lyj;Ljava/lang/String;ZIZIZ)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lbbw;->executeOnThreadPool([Ljava/lang/Object;)Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;

    .line 31
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 14
    invoke-super {p0, p1}, Layq;->onCreate(Landroid/os/Bundle;)V

    .line 15
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/ArchivedConversationListActivity;->g()Lkd;

    move-result-object v0

    sget v1, Lh;->s:I

    invoke-virtual {v0, v1}, Lkd;->b(I)V

    .line 16
    iget-object v0, p0, Lcom/google/android/apps/hangouts/phone/ArchivedConversationListActivity;->s:Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationListFragment;->a(I)V

    .line 17
    return-void
.end method
