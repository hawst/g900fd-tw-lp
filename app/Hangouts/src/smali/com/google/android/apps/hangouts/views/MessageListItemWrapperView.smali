.class public Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Lcdw;


# static fields
.field private static final a:Z

.field private static b:I

.field private static c:I

.field private static d:I

.field private static y:Z


# instance fields
.field private e:Lcdg;

.field private f:Lcdh;

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:Z

.field private p:I

.field private q:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

.field private r:Landroid/widget/FrameLayout;

.field private s:Ljava/lang/String;

.field private t:J

.field private u:Z

.field private v:Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;

.field private w:Z

.field private x:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 38
    sget-object v0, Lbys;->s:Lcyp;

    sput-boolean v2, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->a:Z

    .line 40
    sput v1, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->b:I

    .line 41
    sput v1, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->c:I

    .line 152
    sput-boolean v2, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->y:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 164
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 111
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->l:I

    .line 146
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->w:Z

    .line 147
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->x:Z

    .line 155
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->z:Z

    .line 166
    invoke-virtual {p0, v1}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->a(I)V

    .line 169
    invoke-virtual {p0, v1}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->setClipToPadding(Z)V

    .line 171
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lf;->dV:I

    .line 172
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->d:I

    .line 173
    return-void
.end method

.method static synthetic a(Landroid/content/res/Resources;)I
    .locals 1

    .prologue
    .line 37
    invoke-static {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->c(Landroid/content/res/Resources;)I

    move-result v0

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;)I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->l:I

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;I)I
    .locals 0

    .prologue
    .line 37
    iput p1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->k:I

    return p1
.end method

.method private static a(JJLaah;Laat;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 777
    invoke-interface {p5, p0, p1, p2, p3}, Laat;->a(JJ)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjr;

    .line 778
    iget-object v0, v0, Lbjr;->d:Lbdk;

    .line 779
    invoke-interface {p4, v0}, Laah;->b(Lbdk;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 781
    invoke-interface {p4, v0}, Laah;->a(Lbdk;)Lbdh;

    move-result-object v3

    .line 785
    if-nez v3, :cond_1

    move v0, v1

    .line 795
    :goto_0
    return v0

    .line 791
    :cond_1
    if-eqz v3, :cond_0

    invoke-interface {p4, v0}, Laah;->c(Lbdk;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 792
    goto :goto_0

    .line 795
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/util/List;Ljava/util/List;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 800
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 810
    :cond_0
    :goto_0
    return v2

    :cond_1
    move v1, v2

    .line 804
    :goto_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 805
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    iget-object v3, v0, Lbdh;->b:Lbdk;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    iget-object v0, v0, Lbdh;->b:Lbdk;

    invoke-virtual {v3, v0}, Lbdk;->a(Lbdk;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 804
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 810
    :cond_2
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private static b(Landroid/content/res/Resources;)I
    .locals 2

    .prologue
    .line 180
    sget v0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->c:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 181
    sget v0, Lf;->dm:I

    .line 182
    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->c:I

    .line 184
    :cond_0
    sget v0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->c:I

    return v0
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;I)I
    .locals 0

    .prologue
    .line 37
    iput p1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->l:I

    return p1
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->s:Ljava/lang/String;

    return-object v0
.end method

.method private static c(Landroid/content/res/Resources;)I
    .locals 2

    .prologue
    .line 188
    sget v0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 189
    sget v0, Lf;->dt:I

    .line 190
    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->b:I

    .line 192
    :cond_0
    sget v0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->b:I

    return v0
.end method

.method public static synthetic c(Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;)Lcdh;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->f:Lcdh;

    return-object v0
.end method

.method public static c(Z)V
    .locals 0

    .prologue
    .line 297
    sput-boolean p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->y:Z

    .line 298
    return-void
.end method

.method public static synthetic d(Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;)Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->v:Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;

    return-object v0
.end method

.method private d(Z)V
    .locals 1

    .prologue
    .line 690
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->j()Z

    move-result v0

    .line 691
    if-nez p1, :cond_0

    if-eqz v0, :cond_1

    .line 692
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->k:I

    .line 697
    :goto_0
    return-void

    .line 695
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->c(Landroid/content/res/Resources;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->k:I

    goto :goto_0
.end method

.method static synthetic h()I
    .locals 1

    .prologue
    .line 37
    sget v0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->d:I

    return v0
.end method

.method private i()V
    .locals 4

    .prologue
    .line 301
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->getPaddingLeft()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->m:I

    .line 303
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->getPaddingRight()I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->n:I

    .line 301
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->setPadding(IIII)V

    .line 305
    return-void
.end method

.method private j()Z
    .locals 1

    .prologue
    .line 703
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->u:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->y:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(F)I
    .locals 5

    .prologue
    .line 264
    sget-boolean v0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->a:Z

    if-eqz v0, :cond_0

    .line 265
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setRevealAnimationPercentage "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->b(Landroid/content/res/Resources;)I

    move-result v0

    .line 273
    iget v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->g:I

    .line 274
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    add-int/2addr v1, v0

    iget v2, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->k:I

    sub-int/2addr v1, v2

    .line 278
    iget v2, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->j:I

    .line 280
    iget v3, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->h:I

    iget v4, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->h:I

    sub-int/2addr v1, v4

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    add-int/2addr v1, v3

    iput v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->j:I

    .line 283
    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, p1

    iget v3, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->i:I

    int-to-float v3, v3

    mul-float/2addr v1, v3

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->m:I

    .line 285
    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->n:I

    .line 287
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->i()V

    .line 289
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->e:Lcdg;

    instance-of v0, v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;

    if-eqz v0, :cond_1

    .line 290
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->e:Lcdg;

    check-cast v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;

    .line 291
    invoke-virtual {v0, p1}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->a(F)V

    .line 293
    :cond_1
    iget v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->j:I

    sub-int/2addr v0, v2

    return v0
.end method

.method public a()V
    .locals 0

    .prologue
    .line 714
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 527
    iput p1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->p:I

    .line 528
    return-void
.end method

.method public a(Landroid/database/Cursor;Laah;ILaat;)V
    .locals 13

    .prologue
    .line 313
    const/4 v1, 0x0

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    .line 314
    iget-wide v3, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->t:J

    cmp-long v3, v3, v1

    if-eqz v3, :cond_0

    .line 316
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->e()V

    .line 318
    :cond_0
    iput-wide v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->t:J

    .line 320
    const/4 v1, 0x1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->s:Ljava/lang/String;

    .line 321
    invoke-interface {p1}, Landroid/database/Cursor;->isLast()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->u:Z

    .line 322
    invoke-interface {p1}, Landroid/database/Cursor;->isFirst()Z

    move-result v7

    .line 325
    iget-boolean v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->u:Z

    if-eqz v1, :cond_8

    .line 326
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->v:Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->a(Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;)V

    .line 335
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->q:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    move/from16 v0, p3

    invoke-virtual {v1, v0}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->c(I)V

    .line 337
    const/4 v1, 0x6

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    .line 338
    const-wide v3, 0x7fffffffffffffffL

    .line 339
    iget-boolean v5, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->u:Z

    if-nez v5, :cond_2

    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 340
    const/4 v3, 0x6

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    .line 341
    invoke-interface {p1}, Landroid/database/Cursor;->moveToPrevious()Z

    .line 343
    :cond_2
    const/4 v5, 0x0

    .line 345
    iget-boolean v6, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->z:Z

    if-nez v6, :cond_13

    move-object v5, p2

    move-object/from16 v6, p4

    .line 346
    invoke-static/range {v1 .. v6}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->a(JJLaah;Laat;)Z

    move-result v5

    .line 349
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 350
    if-eqz v5, :cond_9

    .line 352
    move-object/from16 v0, p4

    invoke-interface {v0, v1, v2, v3, v4}, Laat;->a(JJ)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbjr;

    .line 354
    sget-boolean v8, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->a:Z

    if-eqz v8, :cond_4

    .line 355
    const-string v8, "Babel"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "[MessageListItem#bind] Associated watermark found for messageId "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->s:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " with timestamp "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    const-string v8, "Babel"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "  gaiaId: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, v3, Lbjr;->d:Lbdk;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "  timestamp: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-wide v10, v3, Lbjr;->e:J

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    :cond_4
    iget-object v3, v3, Lbjr;->d:Lbdk;

    .line 366
    invoke-interface {p2, v3}, Laah;->b(Lbdk;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 367
    invoke-interface {p2, v3}, Laah;->a(Lbdk;)Lbdh;

    move-result-object v8

    .line 370
    invoke-interface {p2, v3}, Laah;->c(Lbdk;)Z

    move-result v9

    .line 371
    sget-boolean v10, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->a:Z

    if-eqz v10, :cond_7

    .line 372
    const-string v10, "Babel"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Have watermark for "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " on message "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->s:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    if-eqz v9, :cond_5

    .line 375
    const-string v10, "Babel"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "  "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " is focused; Hide watermark."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    :cond_5
    if-nez v8, :cond_6

    .line 379
    const-string v10, "Babel"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "  "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " not in participant map; Hide watermark."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    :cond_6
    iget-boolean v10, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->u:Z

    if-eqz v10, :cond_7

    .line 383
    const-string v10, "Babel"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "  "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v11, " is on last message; Hide watermark."

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v10, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    :cond_7
    if-eqz v8, :cond_3

    if-nez v9, :cond_3

    iget-boolean v3, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->u:Z

    if-nez v3, :cond_3

    .line 391
    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 330
    :cond_8
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->v:Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->b()Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;

    move-result-object v1

    if-ne v1, p0, :cond_1

    .line 331
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->v:Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->a(Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;)V

    goto/16 :goto_0

    .line 396
    :cond_9
    iget-object v3, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->f:Lcdh;

    iget-object v4, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->s:Ljava/lang/String;

    .line 397
    invoke-virtual {v3, v4}, Lcdh;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 398
    iget-object v4, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->f:Lcdh;

    iget-object v8, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->s:Ljava/lang/String;

    invoke-virtual {v4, v8, v6}, Lcdh;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 401
    if-eqz v3, :cond_e

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_e

    .line 403
    iget-object v4, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->q:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    const/4 v8, 0x0

    invoke-virtual {v4, v3, v8}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->b(Ljava/util/List;Z)V

    .line 407
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_d

    .line 408
    invoke-static {v6, v3}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->a(Ljava/util/List;Ljava/util/List;)Z

    move-result v4

    if-nez v4, :cond_f

    .line 410
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 411
    invoke-interface {v4, v6}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 413
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 415
    invoke-interface {v8, v3}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 417
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_a

    .line 418
    iget-object v3, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->q:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    const/4 v6, 0x1

    invoke-virtual {v3, v8, v6}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->b(Ljava/util/List;Z)V

    .line 421
    :cond_a
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_b

    .line 422
    iget-object v3, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->q:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    const/4 v6, 0x1

    invoke-virtual {v3, v4, v6}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->a(Ljava/util/List;Z)V

    :cond_b
    move v9, v5

    .line 444
    :goto_2
    const/4 v3, 0x0

    iput v3, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->n:I

    .line 445
    const/4 v3, 0x0

    iput v3, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->m:I

    .line 447
    iget v3, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->p:I

    packed-switch v3, :pswitch_data_0

    .line 506
    :cond_c
    :goto_3
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->i()V

    .line 508
    if-nez v9, :cond_12

    const/4 v1, 0x1

    :goto_4
    invoke-virtual {p0, v1}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->a(Z)V

    .line 509
    return-void

    .line 430
    :cond_d
    iget-object v4, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->q:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    const/4 v6, 0x0

    invoke-virtual {v4, v3, v6}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->a(Ljava/util/List;Z)V

    move v9, v5

    goto :goto_2

    .line 435
    :cond_e
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_f

    .line 438
    iget-object v3, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->q:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    const/4 v4, 0x0

    invoke-virtual {v3, v6, v4}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->b(Ljava/util/List;Z)V

    :cond_f
    move v9, v5

    goto :goto_2

    .line 449
    :pswitch_0
    iget-boolean v3, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->w:Z

    if-nez v3, :cond_c

    .line 450
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->w:Z

    .line 451
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->b(Landroid/content/res/Resources;)I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->i:I

    .line 453
    const-wide/16 v3, 0x0

    .line 455
    if-nez v7, :cond_10

    invoke-interface {p1}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v5

    if-eqz v5, :cond_10

    .line 456
    const/4 v3, 0x6

    .line 457
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    .line 458
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    :cond_10
    move-wide v5, v1

    move-object v7, p2

    move-object/from16 v8, p4

    .line 461
    invoke-static/range {v3 .. v8}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->a(JJLaah;Laat;)Z

    move-result v1

    .line 465
    if-nez v1, :cond_11

    sget-boolean v1, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->y:Z

    if-eqz v1, :cond_11

    .line 474
    iget v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->i:I

    .line 475
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->c(Landroid/content/res/Resources;)I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->i:I

    .line 478
    :cond_11
    iget v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->i:I

    iput v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->m:I

    .line 479
    iget v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->i:I

    iput v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->h:I

    .line 480
    iget v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->h:I

    iput v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->j:I

    .line 482
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->e:Lcdg;

    invoke-interface {v1}, Lcdg;->b()Landroid/view/View;

    move-result-object v1

    .line 483
    const/4 v2, 0x0

    .line 484
    invoke-virtual {v1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    .line 483
    invoke-static {p0, v2, v1}, Lf;->a(Landroid/view/View;Landroid/view/accessibility/AccessibilityManager;Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 489
    :pswitch_1
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->j:I

    goto :goto_3

    .line 496
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->b(Landroid/content/res/Resources;)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->n:I

    .line 502
    :pswitch_3
    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->j:I

    goto/16 :goto_3

    .line 508
    :cond_12
    const/4 v1, 0x0

    goto/16 :goto_4

    :cond_13
    move v9, v5

    goto/16 :goto_2

    .line 447
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public a(Lcdg;)V
    .locals 2

    .prologue
    .line 246
    iput-object p1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->e:Lcdg;

    .line 247
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->r:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 248
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->r:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->e:Lcdg;

    invoke-interface {v1}, Lcdg;->b()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 249
    return-void
.end method

.method public a(Lcdh;)V
    .locals 0

    .prologue
    .line 535
    iput-object p1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->f:Lcdh;

    .line 536
    return-void
.end method

.method public a(Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;)V
    .locals 0

    .prologue
    .line 531
    iput-object p1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->v:Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;

    .line 532
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 708
    iput-boolean p1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->x:Z

    .line 709
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->g()V

    .line 710
    return-void
.end method

.method public b()Lcdg;
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->e:Lcdg;

    return-object v0
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 176
    iput-boolean p1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->z:Z

    .line 177
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 256
    const-string v0, "Babel"

    const-string v1, "onNewMessageAnimationEnded"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->p:I

    .line 258
    return-void
.end method

.method public d()J
    .locals 2

    .prologue
    .line 308
    iget-wide v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->t:J

    return-wide v0
.end method

.method public e()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 513
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 514
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->setTranslationX(F)V

    .line 516
    :cond_0
    iput-boolean v2, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->o:Z

    .line 517
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->q:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->e()V

    .line 518
    iput-boolean v2, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->w:Z

    .line 519
    return-void
.end method

.method public f()J
    .locals 2

    .prologue
    .line 539
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->e:Lcdg;

    invoke-interface {v0}, Lcdg;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public g()V
    .locals 7

    .prologue
    const/4 v4, 0x3

    const/4 v6, 0x4

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 717
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->f:Lcdh;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->s:Ljava/lang/String;

    .line 718
    invoke-virtual {v2, v3}, Lcdh;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->f:Lcdh;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->s:Ljava/lang/String;

    .line 719
    invoke-virtual {v2, v3}, Lcdh;->a(Ljava/lang/String;)I

    move-result v2

    move v5, v2

    .line 722
    :goto_0
    if-ne v5, v6, :cond_1

    .line 772
    :goto_1
    return-void

    :cond_0
    move v5, v1

    .line 719
    goto :goto_0

    .line 730
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->j()Z

    move-result v2

    if-eqz v2, :cond_2

    move v3, v4

    .line 738
    :goto_2
    if-ne v5, v3, :cond_5

    .line 742
    if-eq v3, v0, :cond_4

    :goto_3
    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->d(Z)V

    goto :goto_1

    .line 732
    :cond_2
    iget-boolean v2, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->x:Z

    if-eqz v2, :cond_3

    move v3, v0

    .line 733
    goto :goto_2

    .line 735
    :cond_3
    const/4 v2, 0x2

    move v3, v2

    goto :goto_2

    :cond_4
    move v0, v1

    .line 742
    goto :goto_3

    .line 749
    :cond_5
    invoke-static {v3}, Lcdh;->a(I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 750
    invoke-static {v5}, Lcdh;->a(I)Z

    move-result v2

    if-eqz v2, :cond_6

    move v2, v0

    .line 752
    :goto_4
    if-eqz v5, :cond_7

    if-eq v5, v4, :cond_7

    if-eq v5, v6, :cond_7

    if-nez v2, :cond_7

    .line 764
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->f:Lcdh;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->s:Ljava/lang/String;

    invoke-virtual {v0, v1, v6}, Lcdh;->a(Ljava/lang/String;I)V

    .line 766
    new-instance v0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView$WatermarkGalleryStateTransition;

    invoke-direct {v0, p0, p0, v3}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView$WatermarkGalleryStateTransition;-><init>(Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;Landroid/view/View;I)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    :cond_6
    move v2, v1

    .line 750
    goto :goto_4

    .line 768
    :cond_7
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->f:Lcdh;

    iget-object v4, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->s:Ljava/lang/String;

    invoke-virtual {v2, v4, v3}, Lcdh;->a(Ljava/lang/String;I)V

    .line 769
    if-eq v3, v0, :cond_8

    :goto_5
    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->d(Z)V

    goto :goto_1

    :cond_8
    move v0, v1

    goto :goto_5
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 240
    sget v0, Lg;->eA:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->r:Landroid/widget/FrameLayout;

    .line 241
    sget v0, Lg;->if:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->q:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    .line 242
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->q:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->a(Lcdw;)V

    .line 243
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 197
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 198
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->g:I

    .line 202
    iget v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->p:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 205
    iget v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->p:I

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->v:Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->o:Z

    if-nez v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->v:Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->b(Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;)V

    .line 208
    iput-boolean v2, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->o:Z

    .line 213
    :cond_0
    iget v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->j:I

    if-ltz v0, :cond_1

    .line 216
    iget v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->j:I

    .line 235
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->setMeasuredDimension(II)V

    .line 236
    return-void

    .line 217
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->z:Z

    if-eqz v0, :cond_2

    .line 219
    iget v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->g:I

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->c(Landroid/content/res/Resources;)I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0

    .line 222
    :cond_2
    iget v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->l:I

    if-ltz v0, :cond_3

    .line 223
    iget v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->l:I

    .line 232
    :goto_1
    iget v1, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->g:I

    sub-int v0, v1, v0

    goto :goto_0

    .line 226
    :cond_3
    iget v0, p0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->k:I

    goto :goto_1
.end method
