.class public Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;
.super Landroid/widget/TextView;
.source "PG"


# instance fields
.field private final a:Lapk;

.field private final b:Landroid/os/Handler;

.field private c:Ljava/lang/StringBuilder;

.field private final d:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 102
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    invoke-static {}, Lapk;->b()Lapk;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;->a:Lapk;

    .line 24
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;->b:Landroid/os/Handler;

    .line 30
    new-instance v0, Lapd;

    invoke-direct {v0, p0}, Lapd;-><init>(Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;->d:Ljava/lang/Runnable;

    .line 103
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;->d:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 104
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;)Lapk;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;->a:Lapk;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;
    .locals 0

    .prologue
    .line 22
    iput-object p1, p0, Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;->c:Ljava/lang/StringBuilder;

    return-object p1
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;)Ljava/lang/StringBuilder;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;->c:Ljava/lang/StringBuilder;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;->d:Ljava/lang/Runnable;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/apps/hangouts/hangout/DebugOverlayTextView;->b:Landroid/os/Handler;

    return-object v0
.end method
