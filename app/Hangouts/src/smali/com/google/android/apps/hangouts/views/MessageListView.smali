.class public Lcom/google/android/apps/hangouts/views/MessageListView;
.super Landroid/widget/ListView;
.source "PG"


# instance fields
.field a:Lcdj;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    return-void
.end method


# virtual methods
.method public a(II)V
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListView;->a:Lcdj;

    if-nez v0, :cond_0

    .line 29
    new-instance v0, Lcdj;

    invoke-direct {v0, p0}, Lcdj;-><init>(Lcom/google/android/apps/hangouts/views/MessageListView;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListView;->a:Lcdj;

    .line 32
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListView;->a:Lcdj;

    iget v0, v0, Lcdj;->a:I

    if-lt p1, v0, :cond_1

    .line 33
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListView;->a:Lcdj;

    iput p1, v0, Lcdj;->a:I

    .line 34
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListView;->a:Lcdj;

    iput p2, v0, Lcdj;->b:I

    .line 36
    :cond_1
    return-void
.end method

.method public onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListView;->a:Lcdj;

    if-eqz v0, :cond_1

    .line 45
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListView;->getLastVisiblePosition()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/MessageListView;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x3

    if-le v0, v1, :cond_0

    .line 46
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListView;->a:Lcdj;

    iget v0, v0, Lcdj;->a:I

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListView;->a:Lcdj;

    iget v1, v1, Lcdj;->b:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/hangouts/views/MessageListView;->setSelectionFromTop(II)V

    .line 49
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListView;->a:Lcdj;

    .line 51
    :cond_1
    invoke-super/range {p0 .. p5}, Landroid/widget/ListView;->onLayout(ZIIII)V

    .line 52
    return-void
.end method
