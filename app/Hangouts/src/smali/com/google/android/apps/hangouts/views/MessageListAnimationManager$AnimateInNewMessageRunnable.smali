.class public Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Comparable;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;",
        ">;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field public a:Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;

.field final synthetic b:Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;

.field private c:I

.field private d:Lcom/google/android/apps/hangouts/views/MessageListView;

.field private e:Lwr;

.field private f:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;Landroid/widget/AbsListView;)V
    .locals 1

    .prologue
    .line 86
    iput-object p1, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->b:Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    iput-object p2, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->a:Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;

    .line 88
    check-cast p3, Lcom/google/android/apps/hangouts/views/MessageListView;

    iput-object p3, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->d:Lcom/google/android/apps/hangouts/views/MessageListView;

    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->f:Z

    .line 90
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->b:Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->a(Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;)Lccw;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->b:Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->a(Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;)Lccw;

    move-result-object v0

    invoke-interface {v0}, Lccw;->b()V

    .line 137
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->b:Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->b(Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;)Z

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->a:Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->c()V

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->b:Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->c(Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;)Lccx;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 140
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->b:Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->c(Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;)Lccx;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->a:Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;

    invoke-virtual {v0, v1}, Lccx;->a(Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;)V

    .line 142
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->b:Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->d(Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;)V

    .line 143
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->a()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;)I
    .locals 4

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->a:Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->f()J

    move-result-wide v0

    iget-object v2, p1, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->a:Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;

    invoke-virtual {v2}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->f()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 77
    check-cast p1, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->a(Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;)I

    move-result v0

    return v0
.end method

.method public run()V
    .locals 3

    .prologue
    .line 94
    const-string v0, "percentage"

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-static {p0, v0, v1}, Lwr;->a(Ljava/lang/Object;Ljava/lang/String;[F)Lwr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->e:Lwr;

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->e:Lwr;

    invoke-static {}, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->c()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lwr;->c(J)Lwr;

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->e:Lwr;

    new-instance v1, Lwp;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lwp;-><init>(B)V

    invoke-virtual {v0, v1}, Lwr;->a(Lwt;)V

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->e:Lwr;

    new-instance v1, Lccv;

    invoke-direct {v1, p0}, Lccv;-><init>(Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;)V

    invoke-virtual {v0, v1}, Lwr;->a(Lwl;)V

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->b:Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->a(Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;)Lccw;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->b:Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->a(Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;)Lccw;

    move-result-object v0

    invoke-interface {v0}, Lccw;->a()V

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->a:Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->d:Lcom/google/android/apps/hangouts/views/MessageListView;

    if-ne v0, v1, :cond_1

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->d:Lcom/google/android/apps/hangouts/views/MessageListView;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->a:Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/MessageListView;->getPositionForView(Landroid/view/View;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->c:I

    .line 127
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->e:Lwr;

    invoke-virtual {v0}, Lwr;->b()V

    .line 131
    :goto_0
    return-void

    .line 129
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->a()V

    goto :goto_0

    .line 94
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public setPercentage(F)V
    .locals 4

    .prologue
    .line 146
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->f:Z

    if-eqz v0, :cond_1

    .line 165
    :cond_0
    :goto_0
    return-void

    .line 152
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->a:Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_2

    .line 154
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->f:Z

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->e:Lwr;

    invoke-virtual {v0}, Lwr;->c()V

    .line 157
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->a:Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->a(F)I

    move-result v0

    .line 158
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->d:Lcom/google/android/apps/hangouts/views/MessageListView;

    iget v2, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->c:I

    iget-object v3, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->a:Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;

    .line 159
    invoke-virtual {v3}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->getTop()I

    move-result v3

    sub-int v0, v3, v0

    .line 158
    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/hangouts/views/MessageListView;->a(II)V

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->a:Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->requestLayout()V

    .line 162
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->f:Z

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager$AnimateInNewMessageRunnable;->a:Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->a(F)I

    goto :goto_0
.end method
