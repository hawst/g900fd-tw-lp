.class public Lcom/google/android/apps/hangouts/phone/ResourceImagePreference;
.super Landroid/preference/Preference;
.source "PG"


# instance fields
.field private a:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/hangouts/phone/ResourceImagePreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/hangouts/phone/ResourceImagePreference;->a:I

    .line 30
    if-eqz p2, :cond_0

    const/4 v0, 0x0

    const-string v1, "mood"

    const/4 v2, 0x0

    invoke-interface {p2, v0, v1, v2}, Landroid/util/AttributeSet;->getAttributeBooleanValue(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 31
    sget v0, Lf;->go:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/ResourceImagePreference;->setLayoutResource(I)V

    .line 35
    :goto_0
    return-void

    .line 33
    :cond_0
    sget v0, Lf;->gn:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/ResourceImagePreference;->setLayoutResource(I)V

    goto :goto_0
.end method


# virtual methods
.method public a(I)V
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/google/android/apps/hangouts/phone/ResourceImagePreference;->a:I

    if-eq p1, v0, :cond_0

    .line 50
    iput p1, p0, Lcom/google/android/apps/hangouts/phone/ResourceImagePreference;->a:I

    .line 51
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/ResourceImagePreference;->notifyChanged()V

    .line 53
    :cond_0
    return-void
.end method

.method public onBindView(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 39
    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    .line 40
    sget v0, Lg;->gk:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 43
    if-eqz v0, :cond_0

    iget v1, p0, Lcom/google/android/apps/hangouts/phone/ResourceImagePreference;->a:I

    if-ltz v1, :cond_0

    .line 44
    iget v1, p0, Lcom/google/android/apps/hangouts/phone/ResourceImagePreference;->a:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 46
    :cond_0
    return-void
.end method
