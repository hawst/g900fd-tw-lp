.class public Lcom/google/android/apps/hangouts/fragments/SmsOobActivity;
.super Landroid/app/Activity;
.source "PG"


# static fields
.field private static a:Ljava/lang/Boolean;


# instance fields
.field private b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/hangouts/fragments/SmsOobActivity;->a:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 184
    invoke-static {}, Lbkb;->j()Lyj;

    move-result-object v0

    .line 185
    if-eqz v0, :cond_0

    invoke-static {}, Lbkb;->p()Lyj;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 186
    invoke-static {v0}, Lbkb;->d(Lyj;)V

    .line 188
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_1

    .line 190
    invoke-static {}, Lbbl;->d()Landroid/content/Intent;

    move-result-object v0

    .line 191
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 196
    :goto_0
    return-void

    .line 194
    :cond_1
    const/4 v0, 0x1

    invoke-static {v0}, Lbkb;->c(Z)V

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/SmsOobActivity;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/SmsOobActivity;->c()V

    return-void
.end method

.method public static a()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 141
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_1

    .line 148
    :cond_0
    :goto_0
    return v0

    .line 145
    :cond_1
    invoke-static {}, Lbkb;->m()Z

    move-result v1

    if-nez v1, :cond_0

    .line 146
    invoke-static {}, Lbzd;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 147
    invoke-static {}, Lbzd;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 148
    sget-object v1, Lcom/google/android/apps/hangouts/fragments/SmsOobActivity;->a:Ljava/lang/Boolean;

    if-nez v1, :cond_2

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string v3, "smsmms"

    invoke-virtual {v1, v3, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    sget v3, Lh;->lS:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/hangouts/fragments/SmsOobActivity;->a:Ljava/lang/Boolean;

    :cond_2
    sget-object v1, Lcom/google/android/apps/hangouts/fragments/SmsOobActivity;->a:Ljava/lang/Boolean;

    invoke-static {v1, v0}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {}, Lbkb;->z()Z

    move-result v1

    if-nez v1, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 165
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/fragments/SmsOobActivity;->a:Ljava/lang/Boolean;

    .line 166
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    .line 167
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 168
    const-string v2, "smsmms"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 170
    sget v2, Lh;->lS:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 171
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 172
    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 173
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 174
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/fragments/SmsOobActivity;)Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/SmsOobActivity;->b:Z

    return v0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 43
    invoke-static {}, Lbkb;->j()Lyj;

    move-result-object v0

    invoke-static {v0}, Lbbl;->b(Lyj;)Landroid/content/Intent;

    move-result-object v0

    .line 44
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/SmsOobActivity;->startActivity(Landroid/content/Intent;)V

    .line 45
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/SmsOobActivity;->finish()V

    .line 46
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 50
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 51
    sget v0, Lf;->gv:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/SmsOobActivity;->setContentView(I)V

    .line 54
    sget v0, Lg;->eW:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/SmsOobActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 55
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/SmsOobActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "android_sms"

    sget v3, Lh;->ib:I

    invoke-static {v0, p0, v1, v2, v3}, Lf;->a(Landroid/widget/TextView;Landroid/app/Activity;Landroid/content/res/Resources;Ljava/lang/String;I)V

    .line 60
    const-string v0, "notification"

    .line 61
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/SmsOobActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 62
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 65
    sget v0, Lg;->gW:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/SmsOobActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 66
    new-instance v1, Lanm;

    invoke-direct {v1, p0}, Lanm;-><init>(Lcom/google/android/apps/hangouts/fragments/SmsOobActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    sget v0, Lg;->gV:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/SmsOobActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 86
    new-instance v1, Lann;

    invoke-direct {v1, p0}, Lann;-><init>(Lcom/google/android/apps/hangouts/fragments/SmsOobActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/SmsOobActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "from_sms_promo_notification"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/SmsOobActivity;->b:Z

    .line 114
    invoke-static {}, Lbkb;->j()Lyj;

    move-result-object v0

    .line 115
    if-eqz v0, :cond_0

    .line 116
    iget-boolean v1, p0, Lcom/google/android/apps/hangouts/fragments/SmsOobActivity;->b:Z

    if-eqz v1, :cond_1

    .line 117
    invoke-static {v0}, Lbsx;->a(Lyj;)Lbsx;

    move-result-object v0

    const-string v1, "shown_sms_promo_screen_notify_count_since_last_upload"

    invoke-virtual {v0, v1}, Lbsx;->d(Ljava/lang/String;)V

    .line 124
    :cond_0
    :goto_0
    return-void

    .line 120
    :cond_1
    invoke-static {v0}, Lbsx;->a(Lyj;)Lbsx;

    move-result-object v0

    const-string v1, "shown_sms_promo_screen_launch_count_since_last_upload"

    invoke-virtual {v0, v1}, Lbsx;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 128
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 130
    invoke-static {}, Lbkb;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/SmsOobActivity;->c()V

    .line 133
    :cond_0
    return-void
.end method
