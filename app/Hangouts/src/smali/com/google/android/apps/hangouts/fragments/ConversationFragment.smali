.class public Lcom/google/android/apps/hangouts/fragments/ConversationFragment;
.super Lakk;
.source "PG"

# interfaces
.implements Laah;
.implements Laax;
.implements Labf;
.implements Labo;
.implements Labw;
.implements Ladq;
.implements Lalj;
.implements Lamc;
.implements Law;
.implements Lbzl;
.implements Lccu;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lakk",
        "<",
        "Landroid/widget/ListView;",
        "Laad;",
        ">;",
        "Laah;",
        "Laax;",
        "Labf;",
        "Labo;",
        "Labw;",
        "Ladq;",
        "Lalj;",
        "Lamc;",
        "Law",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lbzl;",
        "Lccu;"
    }
.end annotation


# static fields
.field private static Y:Z

.field public static final a:Ljava/util/Random;

.field private static aa:Z

.field private static ab:J

.field public static final b:Ljava/util/concurrent/Executor;

.field private static cC:Ljava/lang/String;

.field private static final cy:[I

.field private static final i:Z


# instance fields
.field private Z:Ljava/lang/Runnable;

.field private aA:Ljava/lang/String;

.field private aB:Ljava/lang/String;

.field private aC:Z

.field private aD:Z

.field private aE:Ljava/lang/String;

.field private aF:Lbme;

.field private aG:Z

.field private aH:Z

.field private final aI:Les;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Les",
            "<",
            "Ljava/lang/String;",
            "Lahf;",
            ">;"
        }
    .end annotation
.end field

.field private aJ:Z

.field private aK:Z

.field private aL:Z

.field private aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

.field private aN:Landroid/view/View;

.field private aO:Landroid/view/View;

.field private aP:Landroid/widget/EditText;

.field private aQ:Landroid/view/View;

.field private aR:Landroid/widget/Button;

.field private aS:Landroid/widget/Button;

.field private aT:Landroid/view/View;

.field private aU:Landroid/widget/ImageView;

.field private aV:Landroid/widget/ImageView;

.field private aW:Landroid/widget/ImageView;

.field private aX:Lccm;

.field private aY:Landroid/view/View;

.field private aZ:Landroid/view/View;

.field private ac:I

.field private ad:Lajk;

.field private ae:I

.field private af:I

.field private ag:Z

.field private ah:Lbwt;

.field private ai:Ljava/lang/String;

.field private aj:I

.field private ak:Z

.field private al:Landroid/app/AlertDialog;

.field private am:Lyj;

.field private an:I

.field private ao:I

.field private ap:I

.field private final aq:Lbor;

.field private ar:Z

.field private final as:Lahv;

.field private final at:Lahu;

.field private final au:Lahy;

.field private av:Landroid/net/Uri;

.field private aw:Z

.field private ax:Ljava/lang/String;

.field private ay:Ljava/lang/String;

.field private az:Ljava/lang/String;

.field private bA:Ljava/lang/String;

.field private bB:Ljava/lang/String;

.field private bC:Z

.field private bD:I

.field private bE:Z

.field private bF:Ljava/lang/String;

.field private bG:I

.field private bH:Ljava/lang/String;

.field private bI:I

.field private bJ:I

.field private bK:Ljava/lang/String;

.field private bL:Lcom/google/android/gms/maps/model/MarkerOptions;

.field private bM:Lcom/google/android/gms/maps/model/CameraPosition;

.field private bN:I

.field private bO:Z

.field private final bP:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Lbsv;",
            ">;>;"
        }
    .end annotation
.end field

.field private bQ:Z

.field private final bR:Ljava/lang/Runnable;

.field private final bS:[Z

.field private bT:Ljava/lang/Runnable;

.field private final bU:Lccw;

.field private bV:I

.field private bW:I

.field private final bX:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lahr;",
            ">;"
        }
    .end annotation
.end field

.field private bY:Lcom/google/android/apps/hangouts/views/AudienceView;

.field private bZ:Z

.field private ba:Lcom/google/android/apps/hangouts/views/SquareMapView;

.field private bb:Landroid/view/View;

.field private bc:Lzx;

.field private bd:Landroid/view/View;

.field private be:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

.field private final bf:Lbyz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbyz",
            "<",
            "Lahz;",
            ">;"
        }
    .end annotation
.end field

.field private final bg:Landroid/os/Handler;

.field private bh:Lcom/google/android/apps/hangouts/views/EasterEggView;

.field private bi:Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;

.field private bj:J

.field private bk:J

.field private final bl:Lyc;

.field private final bm:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lbdh;",
            ">;>;"
        }
    .end annotation
.end field

.field private bn:Lbza;

.field private bo:I

.field private bp:Ljava/lang/String;

.field private bq:J

.field private br:I

.field private bs:Lbdh;

.field private bt:Landroid/widget/EditText;

.field private bu:J

.field private bv:J

.field private bw:Landroid/database/Cursor;

.field private bx:Lzx;

.field private by:Lzx;

.field private bz:Ljava/lang/String;

.field public c:Lahp;

.field private final cA:Ljava/lang/Runnable;

.field private final cB:Landroid/text/TextWatcher;

.field private ca:Lajg;

.field private cb:Z

.field private cc:Lahs;

.field private final cd:[Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private ce:Z

.field private cf:Lahm;

.field private cg:Landroid/view/Menu;

.field private ch:Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;

.field private final ci:Ljava/lang/Runnable;

.field private cj:Z

.field private ck:Z

.field private cl:Z

.field private cm:Landroid/widget/Chronometer;

.field private cn:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

.field private co:Landroid/widget/TextView;

.field private cp:Landroid/widget/TextView;

.field private cq:Z

.field private cr:[Lalw;

.field private cs:I

.field private ct:Lbaj;

.field private cu:Ls;

.field private cv:Ladt;

.field private cw:Laha;

.field private final cx:Ljava/lang/Runnable;

.field private final cz:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/hangouts/views/MessageListItemView;",
            ">;"
        }
    .end annotation
.end field

.field public e:Landroid/view/View$OnClickListener;

.field public f:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 247
    sget-object v0, Lbys;->d:Lcyp;

    sput-boolean v1, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i:Z

    .line 250
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a:Ljava/util/Random;

    .line 271
    sput-boolean v1, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->Y:Z

    .line 863
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b:Ljava/util/concurrent/Executor;

    .line 3445
    invoke-static {}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->av()V

    .line 3447
    new-instance v0, Laff;

    invoke-direct {v0}, Laff;-><init>()V

    invoke-static {v0}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/Runnable;)V

    .line 3714
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cy:[I

    .line 7978
    const-string v0, "(select merge_key from merge_keys where merge_keys.conversation_id=conversations.conversation_id)  IN (SELECT merge_key FROM merge_keys WHERE conversation_id=?)"

    sput-object v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cC:Ljava/lang/String;

    return-void

    .line 3714
    :array_0
    .array-data 4
        0x2
        0x0
        0x1
        0x3
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 234
    invoke-direct {p0}, Lakk;-><init>()V

    .line 290
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ae:I

    .line 295
    iput v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->af:I

    .line 298
    new-instance v0, Lbwt;

    invoke-direct {v0}, Lbwt;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ah:Lbwt;

    .line 536
    new-instance v0, Lahw;

    invoke-direct {v0, p0, v1}, Lahw;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aq:Lbor;

    .line 545
    new-instance v0, Lahv;

    invoke-direct {v0, p0, v1}, Lahv;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->as:Lahv;

    .line 547
    new-instance v0, Lahu;

    invoke-direct {v0, p0, v1}, Lahu;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->at:Lahu;

    .line 549
    new-instance v0, Lahy;

    invoke-direct {v0, p0, v1}, Lahy;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->au:Lahy;

    .line 670
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aI:Les;

    .line 696
    new-instance v0, Lbyz;

    invoke-direct {v0}, Lbyz;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bf:Lbyz;

    .line 698
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bg:Landroid/os/Handler;

    .line 705
    new-instance v0, Lyc;

    invoke-direct {v0}, Lyc;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bl:Lyc;

    .line 707
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bm:Ljava/util/Map;

    .line 766
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bP:Landroid/util/SparseArray;

    .line 823
    new-instance v0, Lafi;

    invoke-direct {v0, p0}, Lafi;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bR:Ljava/lang/Runnable;

    .line 841
    new-array v0, v2, [Z

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bS:[Z

    .line 845
    new-instance v0, Lahb;

    invoke-direct {v0, p0, v1}, Lahb;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bU:Lccw;

    .line 848
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bV:I

    .line 849
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bW:I

    .line 853
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bX:Ljava/util/List;

    .line 860
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cb:Z

    .line 895
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->d:Ljava/util/List;

    .line 899
    new-array v0, v2, [Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cd:[Ljava/util/ArrayList;

    .line 904
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ce:Z

    .line 991
    new-instance v0, Lafu;

    invoke-direct {v0, p0}, Lafu;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ci:Ljava/lang/Runnable;

    .line 1003
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ck:Z

    .line 2732
    new-instance v0, Laez;

    invoke-direct {v0, p0}, Laez;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->e:Landroid/view/View$OnClickListener;

    .line 3605
    new-instance v0, Lafj;

    invoke-direct {v0, p0}, Lafj;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cx:Ljava/lang/Runnable;

    .line 3767
    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cz:Ljava/util/Map;

    .line 3868
    new-instance v0, Lafk;

    invoke-direct {v0, p0}, Lafk;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->f:Landroid/view/View$OnClickListener;

    .line 4165
    new-instance v0, Lafs;

    invoke-direct {v0, p0}, Lafs;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cA:Ljava/lang/Runnable;

    .line 7405
    new-instance v0, Lago;

    invoke-direct {v0, p0}, Lago;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cB:Landroid/text/TextWatcher;

    .line 8016
    return-void
.end method

.method public static synthetic A(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->co:Landroid/widget/TextView;

    return-object v0
.end method

.method public static synthetic B(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/apps/hangouts/views/EasterEggView;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bh:Lcom/google/android/apps/hangouts/views/EasterEggView;

    return-object v0
.end method

.method public static synthetic C(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->az:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic D(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bT:Ljava/lang/Runnable;

    return-object v0
.end method

.method public static synthetic E(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 234
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bT:Ljava/lang/Runnable;

    return-object v0
.end method

.method public static synthetic F(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/apps/hangouts/views/ComposeMessageView;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    return-object v0
.end method

.method public static synthetic G(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bF:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic H(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Laha;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cw:Laha;

    return-object v0
.end method

.method public static synthetic I(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V
    .locals 0

    .prologue
    .line 234
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aw()V

    return-void
.end method

.method public static synthetic J(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Z
    .locals 1

    .prologue
    .line 234
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bO:Z

    return v0
.end method

.method public static synthetic K(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->Z:Ljava/lang/Runnable;

    return-object v0
.end method

.method public static synthetic L(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aE:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic M(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bK:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic N(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Z
    .locals 1

    .prologue
    .line 234
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aK()Z

    move-result v0

    return v0
.end method

.method public static synthetic O(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Z
    .locals 1

    .prologue
    .line 234
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aJ:Z

    return v0
.end method

.method public static synthetic P(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)I
    .locals 1

    .prologue
    .line 234
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cs:I

    return v0
.end method

.method public static synthetic Q(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)J
    .locals 2

    .prologue
    .line 234
    iget-wide v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bj:J

    return-wide v0
.end method

.method public static synthetic R(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V
    .locals 0

    .prologue
    .line 234
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->az()V

    return-void
.end method

.method public static synthetic S(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)J
    .locals 2

    .prologue
    .line 234
    iget-wide v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bk:J

    return-wide v0
.end method

.method public static synthetic T(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Z
    .locals 1

    .prologue
    .line 234
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aG:Z

    return v0
.end method

.method public static synthetic U(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V
    .locals 0

    .prologue
    .line 234
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ak()V

    return-void
.end method

.method public static synthetic V(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ai:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic W(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lbwt;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ah:Lbwt;

    return-object v0
.end method

.method public static synthetic X(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)I
    .locals 1

    .prologue
    .line 234
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ac:I

    return v0
.end method

.method public static synthetic Y(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/apps/hangouts/views/SquareMapView;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ba:Lcom/google/android/apps/hangouts/views/SquareMapView;

    return-object v0
.end method

.method public static synthetic Z(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ba:Lcom/google/android/apps/hangouts/views/SquareMapView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ba:Lcom/google/android/apps/hangouts/views/SquareMapView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/SquareMapView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ba:Lcom/google/android/apps/hangouts/views/SquareMapView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/SquareMapView;->a()Lcox;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a:Ljava/util/Random;

    invoke-virtual {v1}, Ljava/util/Random;->nextLong()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Math;->abs(J)J

    move-result-wide v1

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    invoke-static {v3, v1, v2}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Lyj;J)Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lage;

    invoke-direct {v2, p0, v1}, Lage;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Landroid/net/Uri;)V

    invoke-virtual {v0, v2}, Lcox;->a(Lcpd;)V

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Lajk;)Lajk;
    .locals 0

    .prologue
    .line 234
    iput-object p1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ad:Lajk;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Lbza;)Lbza;
    .locals 0

    .prologue
    .line 234
    iput-object p1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bn:Lbza;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Lccm;)Lccm;
    .locals 0

    .prologue
    .line 234
    iput-object p1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aX:Lccm;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ch:Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Lcom/google/android/gms/maps/model/CameraPosition;)Lcom/google/android/gms/maps/model/CameraPosition;
    .locals 0

    .prologue
    .line 234
    iput-object p1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bM:Lcom/google/android/gms/maps/model/CameraPosition;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/MarkerOptions;
    .locals 0

    .prologue
    .line 234
    iput-object p1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bL:Lcom/google/android/gms/maps/model/MarkerOptions;

    return-object p1
.end method

.method private static a([Landroid/text/style/URLSpan;)Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Landroid/text/style/URLSpan;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1775
    array-length v1, p0

    .line 1776
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 1778
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 1779
    aget-object v3, p0, v0

    invoke-virtual {v3}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1780
    aget-object v3, p0, v0

    invoke-virtual {v3}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1778
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1783
    :cond_1
    return-object v2
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Ls;)Ls;
    .locals 0

    .prologue
    .line 234
    iput-object p1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cu:Ls;

    return-object p1
.end method

.method private a(Lyh;)Lyh;
    .locals 4

    .prologue
    .line 2524
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    .line 2525
    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/content/DraftService;->a(Ljava/lang/String;Lyj;)Lyh;

    move-result-object v0

    .line 2526
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lyh;->a()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2527
    sget-boolean v1, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i:Z

    if-eqz v1, :cond_0

    .line 2528
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DraftService.getDraft pendingDraft: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move-object p1, v0

    .line 2536
    :cond_1
    :goto_0
    return-object p1

    .line 2532
    :cond_2
    sget-boolean v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i:Z

    if-eqz v0, :cond_1

    .line 2533
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DraftService.getDraft returning original draft: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Laea;)V
    .locals 4

    .prologue
    .line 4745
    new-instance v0, Lahp;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lahp;-><init>(B)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->c:Lahp;

    .line 4748
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->c:Lahp;

    invoke-virtual {p1}, Laea;->a()J

    move-result-wide v1

    iput-wide v1, v0, Lahp;->a:J

    .line 4751
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bm:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 4752
    if-eqz v0, :cond_1

    .line 4753
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    .line 4754
    iget-object v0, v0, Lbdh;->b:Lbdk;

    .line 4755
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Lbdk;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 4756
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->c:Lahp;

    iput-object v0, v1, Lahp;->b:Lbdk;

    .line 4762
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->c:Lahp;

    iget-object v0, v0, Lahp;->b:Lbdk;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->c:Lahp;

    iget-object v0, v0, Lahp;->b:Lbdk;

    iget-object v0, v0, Lbdk;->a:Ljava/lang/String;

    .line 4763
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4767
    invoke-virtual {p1}, Laea;->j()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laeh;

    .line 4768
    iget-object v0, v0, Laeh;->a:Ljava/lang/String;

    .line 4769
    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->c:Lahp;

    iget-object v2, v2, Lahp;->b:Lbdk;

    iget-object v2, v2, Lbdk;->b:Ljava/lang/String;

    .line 4770
    invoke-static {v0}, Lbzd;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 4771
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->c:Lahp;

    iput-object v0, v1, Lahp;->d:Ljava/lang/String;

    .line 4776
    :cond_3
    return-void
.end method

.method private a(Lahg;)V
    .locals 3

    .prologue
    .line 8025
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ca:Lajg;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 8035
    invoke-interface {p1}, Lahg;->a()V

    .line 8036
    :goto_0
    return-void

    .line 8028
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->T()[Lajk;

    move-result-object v1

    .line 8029
    const/4 v0, 0x0

    :goto_1
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 8030
    aget-object v2, v1, v0

    invoke-interface {p1, v2}, Lahg;->a(Lajk;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-eqz v2, :cond_1

    .line 8031
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 8035
    :cond_1
    invoke-interface {p1}, Lahg;->a()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {p1}, Lahg;->a()V

    throw v0
.end method

.method private a(Lahn;)V
    .locals 3

    .prologue
    .line 8008
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aI:Les;

    invoke-virtual {v0}, Les;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 8009
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahf;

    invoke-interface {p1, v1, v0}, Lahn;->a(Ljava/lang/String;Lahf;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 8012
    :catchall_0
    move-exception v0

    throw v0

    :cond_0
    return-void
.end method

.method private a(Lajk;I)V
    .locals 3

    .prologue
    .line 4779
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ac:I

    if-eq v0, p2, :cond_1

    .line 4781
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->R()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 4782
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ch:Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;

    .line 4785
    :cond_0
    const/4 v1, 0x0

    move v2, v1

    move-object v1, v0

    move v0, v2

    .line 4784
    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->a(Z)V

    .line 4789
    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ad:Lajk;

    .line 4790
    iput p2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ac:I

    .line 4791
    if-eqz p1, :cond_2

    .line 4792
    iget-object v0, p1, Lajk;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    .line 4793
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ca:Lajg;

    if-eqz v0, :cond_2

    .line 4794
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ca:Lajg;

    iget-object v1, p1, Lajk;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lajg;->a(Ljava/lang/String;)V

    .line 4798
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    if-eqz v0, :cond_3

    .line 4799
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a()V

    .line 4800
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->T()[Lajk;

    move-result-object v0

    .line 4801
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->l()V

    .line 4802
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a([Lajk;)V

    .line 4804
    :cond_3
    return-void

    .line 4784
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ch:Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->be:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    .line 4785
    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->d()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    move v2, v1

    move-object v1, v0

    move v0, v2

    goto :goto_0
.end method

.method private a(Landroid/database/Cursor;Lyj;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 4425
    sget-boolean v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i:Z

    if-eqz v0, :cond_0

    .line 4426
    const-string v1, "Babel"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "onConversationParticipantsLoaderFinished() mConversationId="

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ": this="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bA:Ljava/lang/String;

    if-eqz v0, :cond_8

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ", convName="

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bA:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 4432
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->d()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 4426
    invoke-static {v1, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4436
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bm:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 4437
    const/4 v1, 0x0

    .line 4438
    iget-boolean v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aC:Z

    .line 4439
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 4441
    :goto_1
    const/16 v0, 0xd

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 4443
    if-eqz v0, :cond_12

    .line 4448
    invoke-static {p2, p1}, Lyc;->a(Lyj;Landroid/database/Cursor;)Lbdh;

    move-result-object v5

    .line 4449
    const/16 v0, 0xc

    .line 4450
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 4453
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an:I

    if-ne v0, v2, :cond_1

    if-eqz v5, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    if-eqz v0, :cond_1

    iget-object v0, v5, Lbdh;->b:Lbdk;

    iget-object v0, v0, Lbdk;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, v5, Lbdh;->b:Lbdk;

    iget-object v7, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    invoke-virtual {v7}, Lyj;->c()Lbdk;

    move-result-object v7

    invoke-virtual {v0, v7}, Lbdk;->a(Lbdk;)Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, v5, Lbdh;->a:I

    if-ne v0, v2, :cond_1

    new-instance v0, Lafy;

    invoke-direct {v0, p0, v5, v6}, Lafy;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Lbdh;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lahn;)V

    .line 4455
    :cond_1
    sget-boolean v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i:Z

    if-eqz v0, :cond_2

    .line 4456
    const-string v0, "Babel"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Participant: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lbdh;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " Conversation Id: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4460
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bm:Ljava/util/Map;

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 4461
    if-nez v0, :cond_3

    .line 4462
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4463
    iget-object v7, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bm:Ljava/util/Map;

    invoke-interface {v7, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4465
    :cond_3
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4467
    iget-object v0, v5, Lbdh;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isEmergencyNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 4468
    iget-object v0, v5, Lbdh;->c:Ljava/lang/String;

    .line 4469
    iput-boolean v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aC:Z

    .line 4471
    :goto_2
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_13

    .line 4476
    :goto_3
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    if-eqz v1, :cond_4

    iget-boolean v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aC:Z

    if-eq v4, v1, :cond_4

    .line 4478
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a()V

    .line 4484
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aC:Z

    if-eqz v1, :cond_6

    iget-boolean v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aD:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    .line 4485
    invoke-virtual {v1}, Lyj;->x()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-static {}, Lbzd;->c()Z

    move-result v1

    if-nez v1, :cond_6

    .line 4486
    :cond_5
    iput-boolean v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aD:Z

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v4

    invoke-direct {v1, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v4, Lh;->gI:I

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v5

    sget v6, Lh;->gG:I

    invoke-virtual {v5, v6}, Ly;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v5

    sget v6, Lh;->gH:I

    new-array v7, v2, [Ljava/lang/Object;

    aput-object v0, v7, v3

    invoke-virtual {v5, v6, v7}, Ly;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    new-instance v4, Lafv;

    invoke-direct {v4, p0}, Lafv;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    invoke-virtual {v0, v1, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Laft;

    invoke-direct {v1, p0}, Laft;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 4490
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bl:Lyc;

    sget v1, Lyd;->a:I

    invoke-virtual {v0, p1, v1}, Lyc;->a(Landroid/database/Cursor;I)I

    .line 4491
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aB()V

    .line 4493
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ak()V

    .line 4494
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a()V

    .line 4498
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bX:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_7
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahr;

    .line 4499
    iget-object v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bl:Lyc;

    iget-object v5, v0, Lahr;->b:Lbdk;

    invoke-virtual {v4, v5}, Lyc;->a(Lbdk;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 4500
    iget-object v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aq:Lbor;

    iget-object v5, v0, Lahr;->a:Ljava/lang/String;

    iget-object v6, v0, Lahr;->b:Lbdk;

    iget-boolean v0, v0, Lahr;->c:Z

    invoke-virtual {v4, v5, v6, v0}, Lbor;->a(Ljava/lang/String;Lbdk;Z)V

    goto :goto_4

    .line 4426
    :cond_8
    const-string v0, ""

    goto/16 :goto_0

    .line 4506
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bX:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 4508
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->O()Z

    move-result v0

    if-nez v0, :cond_a

    .line 4509
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aD()V

    .line 4510
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aE()V

    .line 4513
    :cond_a
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ah:Lbwt;

    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an:I

    if-eq v0, v2, :cond_c

    move v0, v2

    :goto_5
    invoke-virtual {v1, v0}, Lbwt;->a(Z)V

    .line 4518
    iput-boolean v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ce:Z

    .line 4519
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bl:Lyc;

    invoke-virtual {v0}, Lyc;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_b
    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    .line 4520
    invoke-static {}, Lbvx;->f()Lbdh;

    move-result-object v4

    invoke-virtual {v0, v4}, Lbdh;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 4521
    iput-boolean v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ce:Z

    goto :goto_6

    :cond_c
    move v0, v3

    .line 4513
    goto :goto_5

    .line 4524
    :cond_d
    invoke-virtual {v0}, Lbdh;->c()Ljava/lang/String;

    move-result-object v0

    .line 4525
    if-eqz v0, :cond_b

    invoke-static {v0}, Laea;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 4526
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ah:Lbwt;

    invoke-virtual {v0}, Lbwt;->e()V

    .line 4530
    :cond_e
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ce:Z

    if-eqz v0, :cond_f

    .line 4533
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->m()V

    .line 4535
    :cond_f
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a()V

    .line 4537
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bZ:Z

    if-eqz v0, :cond_11

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->d()I

    move-result v0

    invoke-static {v0}, Lbvx;->a(I)Z

    move-result v0

    if-nez v0, :cond_10

    .line 4538
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->R()Z

    move-result v0

    if-nez v0, :cond_11

    .line 4539
    :cond_10
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bY:Lcom/google/android/apps/hangouts/views/AudienceView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/hangouts/views/AudienceView;->setVisibility(I)V

    .line 4540
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bY:Lcom/google/android/apps/hangouts/views/AudienceView;

    .line 4542
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax()Ljava/util/Collection;

    move-result-object v1

    .line 4541
    invoke-static {v1}, Lf;->a(Ljava/util/Collection;)Lxm;

    move-result-object v1

    .line 4540
    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/AudienceView;->a(Lxm;)V

    .line 4544
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bY:Lcom/google/android/apps/hangouts/views/AudienceView;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cA:Ljava/lang/Runnable;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/hangouts/views/AudienceView;->a(ZLjava/lang/Runnable;)V

    .line 4549
    :goto_7
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->h:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    .line 4550
    return-void

    .line 4546
    :cond_11
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aA()V

    goto :goto_7

    :cond_12
    move-object v0, v1

    goto/16 :goto_2

    :cond_13
    move-object v1, v0

    goto/16 :goto_1

    :cond_14
    move-object v0, v1

    goto/16 :goto_3
.end method

.method public static synthetic a(Landroid/widget/RelativeLayout$LayoutParams;)V
    .locals 1

    .prologue
    .line 234
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    const/16 v0, 0xb

    invoke-virtual {p0, v0}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    return-void
.end method

.method private a(Lbdn;)V
    .locals 12

    .prologue
    const-wide/32 v10, 0x5265c00

    const-wide/32 v8, 0x36ee80

    const/4 v0, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2273
    if-nez p1, :cond_0

    .line 2285
    :goto_0
    return-void

    .line 2279
    :cond_0
    iget-wide v1, p1, Lbdn;->i:J

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v1, v4, v1

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const-wide/32 v4, 0xdbba0

    cmp-long v4, v1, v4

    if-gez v4, :cond_2

    sget v1, Lh;->cn:I

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 2280
    :goto_1
    if-eqz v1, :cond_1

    .line 2281
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lh;->fU:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2284
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cf:Lahm;

    invoke-interface {v1, v0}, Lahm;->c_(Ljava/lang/String;)V

    goto :goto_0

    .line 2279
    :cond_2
    cmp-long v4, v1, v8

    if-gez v4, :cond_3

    sget v1, Lh;->co:I

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_1

    :cond_3
    const-wide/32 v4, 0x2255100

    cmp-long v4, v1, v4

    if-gez v4, :cond_4

    div-long/2addr v1, v8

    long-to-int v1, v1

    sget v2, Lf;->hi:I

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v3, v2, v1, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_4
    cmp-long v4, v1, v10

    if-gez v4, :cond_5

    sget v1, Lh;->cp:I

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_1

    :cond_5
    const-wide/32 v4, 0xa4cb800

    cmp-long v4, v1, v4

    if-gez v4, :cond_6

    sget v1, Lh;->cq:I

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_1

    :cond_6
    const-wide/32 v4, 0x240c8400

    cmp-long v4, v1, v4

    if-gez v4, :cond_7

    div-long/2addr v1, v10

    long-to-int v1, v1

    sget v2, Lf;->hh:I

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v3, v2, v1, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_7
    const-wide v4, 0xb43e9400L

    cmp-long v4, v1, v4

    if-gez v4, :cond_8

    const-wide/32 v4, 0x240c8400

    div-long/2addr v1, v4

    long-to-int v1, v1

    sget v2, Lf;->hk:I

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v3, v2, v1, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    :cond_8
    const-wide v4, 0x9a7ec800L

    div-long/2addr v1, v4

    long-to-int v1, v1

    const/4 v2, 0x4

    if-ge v1, v2, :cond_9

    sget v2, Lf;->hj:I

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v3, v2, v1, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    :cond_9
    move-object v1, v0

    goto/16 :goto_1
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;I)V
    .locals 4

    .prologue
    .line 234
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aJ:Z

    if-eqz v0, :cond_0

    const-wide/16 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g:Lbai;

    check-cast v0, Laad;

    invoke-virtual {v0}, Laad;->a()Landroid/database/Cursor;

    move-result-object v0

    if-lez p1, :cond_1

    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_1

    add-int/lit8 v3, p1, -0x1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v1, 0x6

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    :goto_0
    new-instance v2, Laew;

    invoke-direct {v2, p0, v0, v1}, Laew;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;J)V

    invoke-direct {p0, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lahn;)V

    if-nez p1, :cond_0

    iget v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cs:I

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g:Lbai;

    check-cast v0, Laad;

    invoke-virtual {v0}, Laad;->a()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne v1, v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cs:I

    const-string v1, "babel_conversation_messages_limit"

    const/16 v2, 0x1f4

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cs:I

    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aq()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->av:Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ct:Lbaj;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->av:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lbaj;->a(Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ct:Lbaj;

    invoke-virtual {v0}, Lbaj;->q()V

    :cond_0
    return-void

    :cond_1
    move-wide v0, v1

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Laea;)V
    .locals 0

    .prologue
    .line 234
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Laea;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Lahc;)V
    .locals 0

    .prologue
    .line 234
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Lahc;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Lahn;)V
    .locals 0

    .prologue
    .line 234
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lahn;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Lahp;)V
    .locals 4

    .prologue
    .line 234
    if-eqz p1, :cond_0

    iget-wide v0, p1, Lahp;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p1, Lahp;->b:Lbdk;

    if-eqz v0, :cond_0

    iget-boolean v0, p1, Lahp;->e:Z

    if-eqz v0, :cond_1

    iget-object v0, p1, Lahp;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    new-instance v0, Lagp;

    invoke-direct {v0, p0}, Lagp;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    const/4 v1, 0x1

    new-array v1, v1, [Lahp;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lagp;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Landroid/content/Context;Ljava/lang/String;IZ)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 234
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lf;->du:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    new-instance v1, Lzx;

    new-instance v2, Lbyq;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    invoke-direct {v2, p2, v3}, Lbyq;-><init>(Ljava/lang/String;Lyj;)V

    invoke-virtual {v2, v0}, Lbyq;->a(I)Lbyq;

    move-result-object v0

    invoke-virtual {v0, v4}, Lbyq;->a(Z)Lbyq;

    move-result-object v0

    invoke-virtual {v0, p4}, Lbyq;->c(Z)Lbyq;

    move-result-object v0

    invoke-virtual {v0, p3}, Lbyq;->b(I)Lbyq;

    move-result-object v0

    invoke-virtual {v0, v4}, Lbyq;->d(Z)Lbyq;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->at:Lahu;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-direct {v1, v0, v2, v3, v4}, Lzx;-><init>(Lbyq;Laac;ZLjava/lang/Object;)V

    iput-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bc:Lzx;

    invoke-static {}, Lbsn;->c()Lbsn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bc:Lzx;

    invoke-virtual {v0, v1}, Lbsn;->c(Lbrv;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 234
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Ljava/lang/Runnable;Z)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 234
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Ljava/lang/String;JLjava/lang/String;ILyh;)V
    .locals 10

    .prologue
    .line 234
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "selectPhoto "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cc:Lahs;

    if-eqz v0, :cond_1

    const-string v0, "Babel"

    const-string v1, "A new PersistPhotoTask is being created though the old one hasn\'t finished yet!"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cc:Lahs;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lahs;->cancel(Z)Z

    :cond_1
    new-instance v0, Lahs;

    iget-object v6, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v9

    move-object v1, p0

    move-wide v3, p2

    move-object v5, p4

    move v7, p5

    move-object/from16 v8, p6

    invoke-direct/range {v0 .. v9}, Lahs;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Landroid/net/Uri;JLjava/lang/String;Lyj;ILyh;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cc:Lahs;

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cc:Lahs;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lahs;->executeOnThreadPool([Ljava/lang/Object;)Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;

    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aF()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Lyh;)V
    .locals 0

    .prologue
    .line 234
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->c(Lyh;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Z)V
    .locals 0

    .prologue
    .line 234
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Z)V

    return-void
.end method

.method private a(Lcom/google/android/apps/hangouts/views/MessageListItemView;)V
    .locals 10

    .prologue
    .line 6978
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    long-to-int v7, v0

    .line 6979
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 6980
    invoke-virtual {p1}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->l()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lbvy;

    .line 6981
    iget-object v0, v3, Lbvy;->b:Ljava/lang/String;

    invoke-static {v0}, Lf;->e(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6982
    new-instance v1, Lbyq;

    iget-object v0, v3, Lbvy;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    invoke-direct {v1, v0, v2}, Lbyq;-><init>(Ljava/lang/String;Lyj;)V

    .line 6983
    invoke-virtual {v1}, Lbyq;->b()Lbyq;

    .line 6984
    new-instance v0, Lzx;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->au:Lahy;

    iget-object v3, v3, Lbvy;->b:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 6987
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lzx;-><init>(Lbyq;Laac;Ljava/lang/String;ZZLjava/lang/Object;)V

    .line 6988
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 6991
    :cond_1
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 6993
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bP:Landroid/util/SparseArray;

    invoke-virtual {v0, v7, v8}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 6994
    invoke-static {}, Lbsn;->c()Lbsn;

    move-result-object v1

    .line 6995
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsv;

    .line 6996
    invoke-virtual {v1, v0}, Lbsn;->c(Lbrv;)V

    goto :goto_1

    .line 6999
    :cond_2
    return-void
.end method

.method private a(Ljava/lang/Runnable;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 911
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bS:[Z

    aget-boolean v0, v0, v2

    if-eqz v0, :cond_1

    .line 912
    if-eqz p2, :cond_0

    .line 913
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bg:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 923
    :goto_0
    return-void

    .line 915
    :cond_0
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 918
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cd:[Ljava/util/ArrayList;

    aget-object v0, v0, v2

    if-nez v0, :cond_2

    .line 919
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cd:[Ljava/util/ArrayList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    aput-object v1, v0, v2

    .line 921
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cd:[Ljava/util/ArrayList;

    aget-object v0, v0, v2

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 7123
    if-nez p1, :cond_1

    if-nez p2, :cond_1

    .line 7138
    :cond_0
    :goto_0
    return-void

    .line 7127
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 7131
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cf:Lahm;

    invoke-interface {v0, p1}, Lahm;->b(Ljava/lang/String;)V

    .line 7134
    const-string v0, "babel_enable_last_seen"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7136
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an:I

    if-ne v0, v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    if-ne v1, v2, :cond_0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    iget-object v0, v0, Lbdh;->b:Lbdk;

    iget-object v0, v0, Lbdk;->a:Ljava/lang/String;

    invoke-static {}, Lbzh;->b()Lbzh;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    invoke-virtual {v1, v0, v2}, Lbzh;->a(Ljava/lang/String;Lyj;)Lbdn;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lbdn;)V

    goto :goto_0

    :cond_2
    move-object p1, p2

    .line 7127
    goto :goto_1
.end method

.method private a(ZZ)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2500
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cj:Z

    if-eqz v0, :cond_1

    .line 2521
    :cond_0
    :goto_0
    return-void

    .line 2503
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aQ:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 2504
    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aQ:Landroid/view/View;

    if-eqz p1, :cond_4

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2505
    if-eqz p1, :cond_2

    .line 2506
    if-eqz p2, :cond_5

    .line 2507
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aR:Landroid/widget/Button;

    sget v3, Lh;->cj:I

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(I)V

    .line 2513
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    if-eqz v0, :cond_3

    .line 2514
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    if-eqz p1, :cond_6

    :goto_3
    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->setVisibility(I)V

    .line 2516
    :cond_3
    if-eqz p1, :cond_7

    .line 2517
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aQ:Landroid/view/View;

    sget v1, Lg;->bX:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto :goto_0

    :cond_4
    move v0, v2

    .line 2504
    goto :goto_1

    .line 2509
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aR:Landroid/widget/Button;

    sget v3, Lh;->kC:I

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(I)V

    goto :goto_2

    :cond_6
    move v2, v1

    .line 2514
    goto :goto_3

    .line 2518
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    if-eqz v0, :cond_0

    .line 2519
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->requestFocus()Z

    goto :goto_0
.end method

.method public static synthetic aA(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bp:Ljava/lang/String;

    return-object v0
.end method

.method private aA()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 4589
    iput-boolean v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bZ:Z

    .line 4590
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bY:Lcom/google/android/apps/hangouts/views/AudienceView;

    if-eqz v0, :cond_0

    .line 4591
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bY:Lcom/google/android/apps/hangouts/views/AudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/AudienceView;->c()V

    .line 4592
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bY:Lcom/google/android/apps/hangouts/views/AudienceView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/AudienceView;->setVisibility(I)V

    .line 4593
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bY:Lcom/google/android/apps/hangouts/views/AudienceView;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/hangouts/views/AudienceView;->a(ZLjava/lang/Runnable;)V

    .line 4595
    :cond_0
    return-void
.end method

.method private aB()V
    .locals 4

    .prologue
    .line 4601
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->br:I

    .line 4602
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bs:Lbdh;

    .line 4606
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bl:Lyc;

    invoke-virtual {v0}, Lyc;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    .line 4607
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v3, v0, Lbdh;->i:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 4608
    iget v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->br:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->br:I

    .line 4609
    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bs:Lbdh;

    goto :goto_0

    .line 4612
    :cond_1
    return-void
.end method

.method private aC()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 5107
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aI:Les;

    invoke-virtual {v0}, Les;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_0
    :pswitch_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahf;

    .line 5108
    iget v0, v0, Lahf;->b:I

    packed-switch v0, :pswitch_data_0

    move v0, v1

    :goto_1
    move v1, v0

    .line 5117
    goto :goto_0

    .line 5114
    :pswitch_1
    add-int/lit8 v0, v1, 0x1

    goto :goto_1

    .line 5118
    :cond_0
    const/4 v0, 0x1

    if-le v1, v0, :cond_1

    .line 5119
    new-instance v0, Lafx;

    invoke-direct {v0, p0}, Lafx;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    new-array v1, v2, [Ljava/lang/Void;

    .line 5169
    invoke-virtual {v0, v1}, Lafx;->executeOnThreadPool([Ljava/lang/Object;)Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;

    .line 5172
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->j(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-static {v1}, Lyt;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->j(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    :cond_2
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "didn\'t find conversation "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " in merged conversation; serverId is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aI:Les;

    invoke-virtual {v0}, Les;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "merged id "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lbys;->g(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "didn\'t find conversation id in merged set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 5173
    :cond_4
    return-void

    .line 5108
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private aD()V
    .locals 10

    .prologue
    const-wide/16 v5, 0x0

    .line 5415
    iget-wide v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bk:J

    cmp-long v0, v0, v5

    if-lez v0, :cond_4

    .line 5416
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    .line 5417
    invoke-static {v0}, Laas;->a(Ljava/lang/String;)Laat;

    move-result-object v0

    .line 5418
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 5420
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 5422
    const-wide v3, 0x7fffffffffffffffL

    invoke-interface {v0, v5, v6, v3, v4}, Laat;->a(JJ)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjr;

    .line 5423
    iget-object v4, v0, Lbjr;->d:Lbdk;

    .line 5424
    iget-object v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bl:Lyc;

    .line 5425
    invoke-virtual {v5, v4}, Lyc;->b(Lbdk;)Lbdh;

    move-result-object v5

    .line 5426
    if-eqz v5, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bl:Lyc;

    invoke-virtual {v6, v4}, Lyc;->c(Lbdk;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 5427
    iget-wide v6, v0, Lbjr;->e:J

    iget-wide v8, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bk:J

    cmp-long v0, v6, v8

    if-ltz v0, :cond_1

    .line 5431
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 5433
    :cond_1
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 5437
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 5444
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->be:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->a(Ljava/util/List;Z)V

    .line 5447
    :cond_3
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 5448
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->be:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->b(Ljava/util/List;Z)V

    .line 5451
    :cond_4
    return-void
.end method

.method private aE()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 5454
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->O()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ak:Z

    if-nez v0, :cond_0

    .line 5456
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an:I

    if-ne v0, v3, :cond_0

    .line 5457
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->Q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5458
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->K()Lbdh;

    move-result-object v0

    .line 5459
    if-eqz v0, :cond_0

    iget-object v1, v0, Lbdh;->b:Lbdk;

    iget-object v1, v1, Lbdk;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 5467
    :cond_0
    :goto_0
    return-void

    .line 5462
    :cond_1
    invoke-static {}, Lbzh;->b()Lbzh;

    move-result-object v1

    iget-object v0, v0, Lbdh;->b:Lbdk;

    iget-object v0, v0, Lbdk;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    invoke-virtual {v1, v0, v2}, Lbzh;->b(Ljava/lang/String;Lyj;)V

    .line 5464
    iput-boolean v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ak:Z

    goto :goto_0
.end method

.method private aF()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    .line 5772
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bY:Lcom/google/android/apps/hangouts/views/AudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/AudienceView;->getVisibility()I

    move-result v0

    if-ne v0, v4, :cond_0

    .line 5782
    :goto_0
    return-void

    .line 5776
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 5777
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bY:Lcom/google/android/apps/hangouts/views/AudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/AudienceView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    new-instance v1, Landroid/animation/LayoutTransition;

    invoke-direct {v1}, Landroid/animation/LayoutTransition;-><init>()V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v2, v3}, Landroid/animation/LayoutTransition;->setDuration(J)V

    new-instance v2, Lagc;

    invoke-direct {v2, p0, v0}, Lagc;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Landroid/view/ViewGroup;)V

    invoke-virtual {v1, v2}, Landroid/animation/LayoutTransition;->addTransitionListener(Landroid/animation/LayoutTransition$TransitionListener;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 5780
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bY:Lcom/google/android/apps/hangouts/views/AudienceView;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/hangouts/views/AudienceView;->setVisibility(I)V

    .line 5781
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bZ:Z

    goto :goto_0
.end method

.method private aG()V
    .locals 2

    .prologue
    .line 6217
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cc:Lahs;

    if-eqz v0, :cond_0

    .line 6218
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cc:Lahs;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lahs;->cancel(Z)Z

    .line 6219
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cc:Lahs;

    .line 6221
    :cond_0
    return-void
.end method

.method private aH()Z
    .locals 2

    .prologue
    .line 6499
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aI:Les;

    invoke-virtual {v0}, Les;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahf;

    .line 6500
    iget-boolean v0, v0, Lahf;->d:Z

    if-nez v0, :cond_0

    .line 6501
    const/4 v0, 0x1

    .line 6504
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aI()V
    .locals 2

    .prologue
    .line 6511
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aJ:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aH()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6512
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aN:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 6516
    :goto_0
    return-void

    .line 6514
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aN:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private aJ()V
    .locals 2

    .prologue
    .line 6603
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->g(Lyj;Ljava/lang/String;)I

    .line 6604
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cf:Lahm;

    if-eqz v0, :cond_0

    .line 6605
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cf:Lahm;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-interface {v0}, Lahm;->a()V

    .line 6607
    :cond_0
    return-void
.end method

.method private aK()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 6845
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aQ:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 6846
    invoke-direct {p0, v0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(ZZ)V

    .line 6847
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cz:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 6848
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g:Lbai;

    check-cast v0, Laad;

    invoke-virtual {v0}, Laad;->notifyDataSetChanged()V

    .line 6849
    const/4 v0, 0x1

    .line 6851
    :cond_0
    return v0
.end method

.method private aL()V
    .locals 3

    .prologue
    .line 6909
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->be:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    if-nez v0, :cond_1

    .line 6915
    :cond_0
    return-void

    .line 6912
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bl:Lyc;

    invoke-virtual {v0}, Lyc;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    .line 6913
    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->be:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->a(Lbdh;)V

    goto :goto_0
.end method

.method private aM()V
    .locals 2

    .prologue
    .line 7037
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bA:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bB:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 7038
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bA:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bB:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 7040
    :cond_1
    return-void
.end method

.method public static synthetic aa(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)I
    .locals 1

    .prologue
    .line 234
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bG:I

    return v0
.end method

.method public static synthetic aa()Z
    .locals 1

    .prologue
    .line 234
    sget-boolean v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i:Z

    return v0
.end method

.method public static synthetic ab(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bH:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic ab()Z
    .locals 1

    .prologue
    .line 234
    sget-boolean v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aa:Z

    return v0
.end method

.method public static synthetic ac(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)I
    .locals 1

    .prologue
    .line 234
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bI:I

    return v0
.end method

.method public static synthetic ac()J
    .locals 2

    .prologue
    .line 234
    sget-wide v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ab:J

    return-wide v0
.end method

.method public static synthetic ad(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)I
    .locals 1

    .prologue
    .line 234
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bJ:I

    return v0
.end method

.method public static synthetic ad()V
    .locals 0

    .prologue
    .line 234
    invoke-static {}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->av()V

    return-void
.end method

.method public static synthetic ae(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)I
    .locals 1

    .prologue
    .line 234
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aj:I

    return v0
.end method

.method public static synthetic af(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/gms/maps/model/MarkerOptions;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bL:Lcom/google/android/gms/maps/model/MarkerOptions;

    return-object v0
.end method

.method public static synthetic ag(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/gms/maps/model/CameraPosition;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bM:Lcom/google/android/gms/maps/model/CameraPosition;

    return-object v0
.end method

.method public static synthetic ah(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 234
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ai:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic ai(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Z
    .locals 1

    .prologue
    .line 234
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aH:Z

    return v0
.end method

.method private aj()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1086
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->al:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 1087
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->al:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 1088
    iput-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->al:Landroid/app/AlertDialog;

    .line 1091
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cu:Ls;

    if-eqz v0, :cond_1

    .line 1092
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cu:Ls;

    invoke-virtual {v0}, Ls;->a()V

    .line 1093
    iput-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cu:Ls;

    .line 1095
    :cond_1
    return-void
.end method

.method public static synthetic aj(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V
    .locals 0

    .prologue
    .line 234
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aF()V

    return-void
.end method

.method private ak()V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1099
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->O()Z

    move-result v2

    if-nez v2, :cond_0

    iget v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ap:I

    if-ne v2, v0, :cond_2

    .line 1101
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cr:[Lalw;

    array-length v5, v4

    move v3, v1

    move v2, v1

    :goto_1
    if-ge v3, v5, :cond_4

    aget-object v6, v4, v3

    .line 1102
    if-nez v2, :cond_1

    if-eqz v0, :cond_3

    .line 1103
    :cond_1
    invoke-virtual {v6, v1}, Lalw;->a(Z)V

    .line 1101
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 1099
    goto :goto_0

    .line 1105
    :cond_3
    invoke-virtual {v6}, Lalw;->d()Z

    move-result v2

    .line 1106
    invoke-virtual {v6, v2}, Lalw;->a(Z)V

    goto :goto_2

    .line 1109
    :cond_4
    return-void
.end method

.method public static synthetic ak(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V
    .locals 2

    .prologue
    .line 234
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->isDetached()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ba:Lcom/google/android/apps/hangouts/views/SquareMapView;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lg;->ay:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/SquareMapView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ba:Lcom/google/android/apps/hangouts/views/SquareMapView;

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/ShareLocationActivity;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ba:Lcom/google/android/apps/hangouts/views/SquareMapView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/SquareMapView;->b()V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ba:Lcom/google/android/apps/hangouts/views/SquareMapView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/SquareMapView;->c()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ba:Lcom/google/android/apps/hangouts/views/SquareMapView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/SquareMapView;->setVisibility(I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ba:Lcom/google/android/apps/hangouts/views/SquareMapView;

    goto :goto_0
.end method

.method public static synthetic al(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aT:Landroid/view/View;

    return-object v0
.end method

.method private al()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1994
    iget v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an:I

    if-eq v2, v1, :cond_1

    .line 1995
    const-string v1, "Babel"

    const-string v2, "hasParticipantPhoneNumber: Not 1:1 conversation type"

    invoke-static {v1, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2004
    :cond_0
    :goto_0
    return v0

    .line 1999
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->Q()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->R()Z

    move-result v2

    if-nez v2, :cond_2

    .line 2000
    const-string v1, "Babel"

    const-string v2, "hasParticipantPhoneNumber: Not Babel or SMS transport"

    invoke-static {v1, v2}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2004
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ca:Lajg;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ca:Lajg;

    invoke-virtual {v2}, Lajg;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public static synthetic am(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aU:Landroid/widget/ImageView;

    return-object v0
.end method

.method private am()Z
    .locals 2

    .prologue
    .line 2018
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    invoke-static {v0, v1}, Lf;->a(Landroid/content/Context;Lyj;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic an(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aZ:Landroid/view/View;

    return-object v0
.end method

.method private an()Z
    .locals 1

    .prologue
    .line 2022
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lbzd;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ao()Ljava/lang/Boolean;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 2036
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 2038
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->t()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2039
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 2094
    :cond_0
    :goto_0
    return-object v0

    .line 2042
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->s()Z

    .line 2043
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->T()[Lajk;

    move-result-object v4

    .line 2044
    if-eqz v4, :cond_2

    .line 2047
    array-length v5, v4

    move v2, v3

    :goto_1
    if-ge v2, v5, :cond_2

    aget-object v6, v4, v2

    .line 2048
    iget-object v7, v6, Lajk;->b:Lbdh;

    if-eqz v7, :cond_4

    .line 2049
    iget-object v7, v6, Lajk;->b:Lbdh;

    invoke-virtual {v7}, Lbdh;->a()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_3

    .line 2050
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 2062
    :cond_2
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->R()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2063
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    .line 2053
    :cond_3
    iget-object v7, v6, Lajk;->a:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-static {v7, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_4

    iget-object v6, v6, Lajk;->b:Lbdh;

    .line 2054
    invoke-virtual {v6}, Lbdh;->a()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_4

    .line 2055
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_2

    .line 2047
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2064
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->Q()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2065
    invoke-static {}, Lape;->c()Ljava/lang/Boolean;

    move-result-object v0

    .line 2066
    if-nez v0, :cond_6

    .line 2068
    new-instance v0, Lagw;

    invoke-direct {v0, p0}, Lagw;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    .line 2077
    invoke-static {v0, v0}, Lape;->a(Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    move-object v0, v1

    .line 2079
    goto :goto_0

    .line 2080
    :cond_6
    invoke-static {v0, v3}, Lf;->a(Ljava/lang/Boolean;Z)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 2081
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    .line 2083
    :cond_7
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    .line 2089
    :cond_8
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-ne v0, v2, :cond_0

    move-object v0, v1

    .line 2090
    goto :goto_0
.end method

.method public static synthetic ao(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 234
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic ap(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aV:Landroid/widget/ImageView;

    return-object v0
.end method

.method private ap()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2296
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->T()[Lajk;

    move-result-object v0

    .line 2298
    if-eqz v0, :cond_0

    array-length v1, v0

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 2299
    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 2301
    iget-object v1, v0, Lajk;->h:Laea;

    if-nez v1, :cond_0

    iget-object v1, v0, Lajk;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2302
    iget-object v0, v0, Lajk;->c:Ljava/lang/String;

    .line 2305
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aq()Landroid/net/Uri;
    .locals 3

    .prologue
    .line 2541
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->c(Lyj;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2542
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "limit"

    iget v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cs:I

    .line 2543
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    .line 2542
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 2543
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 2544
    return-object v0
.end method

.method public static synthetic aq(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Z
    .locals 1

    .prologue
    .line 234
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bE:Z

    return v0
.end method

.method public static synthetic ar(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lahs;
    .locals 1

    .prologue
    .line 234
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cc:Lahs;

    return-object v0
.end method

.method private ar()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 3175
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v0

    if-nez v0, :cond_0

    .line 3198
    :goto_0
    return-void

    .line 3180
    :cond_0
    sget-boolean v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i:Z

    if-eqz v0, :cond_1

    .line 3181
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "restartMessageLoader mConversationId: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3183
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getLoaderManager()Lav;

    move-result-object v0

    invoke-virtual {v0, v3}, Lav;->b(I)Ldg;

    move-result-object v0

    .line 3184
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    if-nez v1, :cond_3

    .line 3185
    if-eqz v0, :cond_2

    .line 3186
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getLoaderManager()Lav;

    move-result-object v0

    invoke-virtual {v0, v3}, Lav;->a(I)V

    .line 3188
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->c(Landroid/view/View;)V

    goto :goto_0

    .line 3193
    :cond_3
    if-eqz v0, :cond_4

    .line 3194
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getLoaderManager()Lav;

    move-result-object v0

    invoke-virtual {v0, v3}, Lav;->a(I)V

    .line 3196
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getLoaderManager()Lav;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v1, p0}, Lav;->a(ILandroid/os/Bundle;Law;)Ldg;

    goto :goto_0
.end method

.method public static synthetic as(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lzx;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bx:Lzx;

    return-object v0
.end method

.method private as()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3201
    sget-boolean v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i:Z

    if-eqz v0, :cond_0

    .line 3202
    const-string v0, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "restartOtherLoaders mConversationId: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3205
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 3206
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->c(Landroid/view/View;)V

    .line 3208
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v0

    if-nez v0, :cond_3

    .line 3233
    :cond_2
    return-void

    .line 3211
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getLoaderManager()Lav;

    move-result-object v3

    .line 3212
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    if-eqz v0, :cond_5

    move v0, v1

    .line 3213
    :goto_0
    sget-object v4, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cy:[I

    array-length v4, v4

    if-ge v2, v4, :cond_2

    .line 3214
    sget-object v4, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cy:[I

    aget v4, v4, v2

    .line 3216
    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    .line 3217
    invoke-virtual {v3, v4}, Lav;->a(I)V

    .line 3225
    if-eq v4, v1, :cond_4

    .line 3226
    if-eqz v0, :cond_4

    .line 3230
    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5, p0}, Lav;->a(ILandroid/os/Bundle;Law;)Ldg;

    .line 3213
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_5
    move v0, v2

    .line 3212
    goto :goto_0
.end method

.method public static synthetic at(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lzx;
    .locals 1

    .prologue
    .line 234
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bx:Lzx;

    return-object v0
.end method

.method private at()V
    .locals 4

    .prologue
    .line 3302
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->j()V

    .line 3305
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bg:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ci:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 3307
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bg:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ci:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7918

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 3309
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 3310
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->c(Landroid/view/View;)V

    .line 3316
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->G()Z

    .line 3317
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->g()V

    goto :goto_1

    .line 3311
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g:Lbai;

    check-cast v0, Laad;

    invoke-virtual {v0}, Laad;->a()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_0

    .line 3313
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->e(Landroid/view/View;)V

    goto :goto_0

    .line 3318
    :cond_2
    return-void
.end method

.method public static synthetic au(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aW:Landroid/widget/ImageView;

    return-object v0
.end method

.method private au()V
    .locals 2

    .prologue
    .line 3338
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aI:Les;

    if-nez v0, :cond_1

    .line 3345
    :cond_0
    :goto_0
    return-void

    .line 3341
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->F()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3344
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aI:Les;

    invoke-virtual {v1}, Les;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ljava/util/Set;)V

    goto :goto_0
.end method

.method public static synthetic av(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lzx;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bc:Lzx;

    return-object v0
.end method

.method private static av()V
    .locals 3

    .prologue
    .line 3436
    const-string v0, "babel_poll_conversation_data"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aa:Z

    .line 3439
    const-string v0, "babel_poll_conversation_data_frequency_ms"

    const-wide/32 v1, 0xea60

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ab:J

    .line 3442
    return-void
.end method

.method public static synthetic aw(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lzx;
    .locals 1

    .prologue
    .line 234
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bc:Lzx;

    return-object v0
.end method

.method private aw()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 3456
    new-instance v1, Lafg;

    invoke-direct {v1, p0}, Lafg;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lahn;)V

    .line 3484
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ah:Lbwt;

    if-eqz v1, :cond_0

    .line 3488
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ah:Lbwt;

    iget v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an:I

    if-eq v2, v0, :cond_1

    :goto_0
    invoke-virtual {v1, v0}, Lbwt;->a(Z)V

    .line 3490
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a()V

    .line 3492
    :cond_0
    return-void

    .line 3488
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic ax(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lccm;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aX:Lccm;

    return-object v0
.end method

.method private ax()Ljava/util/Collection;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lbdh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3509
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bm:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 3510
    if-nez v0, :cond_0

    .line 3511
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 3521
    :goto_0
    return-object v0

    .line 3514
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 3515
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    .line 3516
    iget-object v3, v0, Lbdh;->b:Lbdk;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Lbdk;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 3517
    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 3521
    goto :goto_0
.end method

.method private ay()V
    .locals 4

    .prologue
    .line 3568
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->F()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3603
    :goto_0
    return-void

    .line 3573
    :cond_0
    new-instance v0, Lafh;

    invoke-direct {v0, p0}, Lafh;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lahn;)V

    .line 3599
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bg:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cx:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 3600
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bg:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cx:Ljava/lang/Runnable;

    const-string v2, "babel_focusrenewperiodmillis"

    const v3, 0x41eb0

    invoke-static {v2, v3}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public static synthetic ay(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/16 v1, 0x8

    .line 234
    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ba:Lcom/google/android/apps/hangouts/views/SquareMapView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ba:Lcom/google/android/apps/hangouts/views/SquareMapView;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/hangouts/views/SquareMapView;->setVisibility(I)V

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aV:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aY:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aT:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aU:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bK:Ljava/lang/String;

    invoke-static {v2}, Lf;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aZ:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aZ:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static synthetic az(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Landroid/util/SparseArray;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bP:Landroid/util/SparseArray;

    return-object v0
.end method

.method private az()V
    .locals 3

    .prologue
    .line 4135
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ah:Lbwt;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lbwt;->a(Ljava/lang/String;Z)V

    .line 4136
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Z)V

    .line 4137
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a()V

    .line 4139
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;I)I
    .locals 0

    .prologue
    .line 234
    iput p1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bD:I

    return p1
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lyj;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    return-object v0
.end method

.method private b(Lahc;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 2552
    iget-object v0, p1, Lahc;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2553
    invoke-virtual {p0, v7}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Z)V

    .line 2557
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g:Lbai;

    if-eqz v0, :cond_1

    .line 2558
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g:Lbai;

    check-cast v0, Laad;

    invoke-virtual {v0}, Laad;->d_()V

    .line 2562
    :cond_1
    iput-object v6, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bA:Ljava/lang/String;

    .line 2563
    iput-object v6, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bB:Ljava/lang/String;

    .line 2565
    iget-wide v2, p1, Lahc;->i:J

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-lez v0, :cond_b

    .line 2566
    iget-wide v2, p1, Lahc;->i:J

    iput-wide v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bu:J

    .line 2574
    :goto_0
    iget-object v0, p1, Lahc;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 2575
    iget-object v0, p1, Lahc;->a:Ljava/lang/String;

    const-string v2, "\\|"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    .line 2579
    :goto_1
    iget-boolean v0, p1, Lahc;->b:Z

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aw:Z

    .line 2580
    iput-object v6, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->az:Ljava/lang/String;

    .line 2582
    iget-object v0, p1, Lahc;->c:Lyj;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    .line 2583
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bl:Lyc;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lyc;->d(Lyj;Ljava/lang/String;)V

    .line 2584
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bm:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2586
    iget-object v0, p1, Lahc;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget v0, p1, Lahc;->d:I

    if-nez v0, :cond_2

    .line 2588
    const-string v0, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "opening conversation with unknown type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lahc;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 2590
    :cond_2
    iget v0, p1, Lahc;->d:I

    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an:I

    .line 2591
    iget v0, p1, Lahc;->m:I

    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ac:I

    .line 2594
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ca:Lajg;

    if-eqz v0, :cond_3

    .line 2595
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ca:Lajg;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lajg;->a(Ljava/lang/String;)V

    .line 2600
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->T()[Lajk;

    move-result-object v2

    .line 2601
    array-length v3, v2

    move v0, v1

    :goto_2
    if-ge v0, v3, :cond_4

    aget-object v4, v2, v0

    .line 2602
    iget-boolean v5, v4, Lajk;->j:Z

    if-eqz v5, :cond_d

    .line 2605
    invoke-virtual {v4}, Lajk;->a()I

    move-result v0

    invoke-direct {p0, v4, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lajk;I)V

    .line 2610
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    if-eqz v0, :cond_e

    .line 2611
    const-string v0, "babel_conversation_messages_limit"

    const/16 v2, 0x1f4

    invoke-static {v0, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cs:I

    .line 2614
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aq()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->av:Landroid/net/Uri;

    .line 2618
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    if-eqz v0, :cond_5

    .line 2619
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->n()V

    .line 2620
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    iget-object v2, p1, Lahc;->c:Lyj;

    invoke-virtual {v0, p0, v2}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a(Lt;Lyj;)V

    .line 2623
    :cond_5
    invoke-direct {p0, v1, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(ZZ)V

    .line 2625
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->h:Landroid/widget/AbsListView;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g:Lbai;

    if-eqz v0, :cond_6

    .line 2626
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g:Lbai;

    check-cast v0, Laad;

    iget v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an:I

    invoke-virtual {v0, v1}, Laad;->a(I)V

    .line 2629
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->be:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    if-eqz v0, :cond_7

    .line 2630
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->be:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an:I

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->a(Ljava/lang/String;ILyj;)V

    .line 2634
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2638
    iget-object v0, p1, Lahc;->g:Ljava/lang/String;

    iget-object v1, p1, Lahc;->f:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2641
    :cond_8
    iget-object v0, p1, Lahc;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bA:Ljava/lang/String;

    .line 2642
    iget-object v0, p1, Lahc;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bB:Ljava/lang/String;

    .line 2643
    iget-object v0, p1, Lahc;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aA:Ljava/lang/String;

    .line 2644
    iget-object v0, p1, Lahc;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aB:Ljava/lang/String;

    .line 2645
    iget-boolean v0, p1, Lahc;->h:Z

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bZ:Z

    .line 2647
    const-string v0, "Babel"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2648
    const-string v1, "Babel"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "setConversationParams done with: mConversationId="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", mMessageUri="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->av:Landroid/net/Uri;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", mAccount="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    if-nez v0, :cond_f

    const-string v0, "null"

    .line 2651
    :goto_4
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2648
    invoke-static {v1, v0}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2654
    :cond_9
    iget-object v0, p1, Lahc;->e:Lyh;

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lyh;)Lyh;

    move-result-object v0

    .line 2655
    if-eqz v0, :cond_a

    .line 2657
    iput-boolean v7, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ck:Z

    .line 2659
    :cond_a
    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->d(Lyh;)V

    .line 2661
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aj()V

    .line 2662
    return-void

    .line 2571
    :cond_b
    iput-boolean v7, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aL:Z

    goto/16 :goto_0

    .line 2577
    :cond_c
    iput-object v6, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    goto/16 :goto_1

    .line 2601
    :cond_d
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_2

    .line 2616
    :cond_e
    iput-object v6, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->av:Landroid/net/Uri;

    goto/16 :goto_3

    .line 2648
    :cond_f
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    .line 2651
    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbys;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_4
.end method

.method private b(Landroid/database/Cursor;Lyj;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 4615
    iget-wide v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bu:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 4618
    iput-object p1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bw:Landroid/database/Cursor;

    .line 4672
    :goto_0
    return-void

    .line 4623
    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToLast()Z

    .line 4625
    invoke-interface {p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_3

    .line 4626
    const/4 v0, 0x2

    .line 4627
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    .line 4628
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/16 v3, 0x11

    if-ne v0, v3, :cond_2

    iget-wide v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bu:J

    cmp-long v0, v1, v3

    if-lez v0, :cond_2

    iget-wide v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bv:J

    cmp-long v0, v1, v3

    if-lez v0, :cond_2

    .line 4632
    new-instance v0, Lbyq;

    const/4 v3, 0x4

    .line 4633
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3, v8}, Lbyq;-><init>(Ljava/lang/String;Lyj;)V

    .line 4635
    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bx:Lzx;

    if-eqz v3, :cond_1

    .line 4636
    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bx:Lzx;

    invoke-virtual {v3}, Lzx;->b()V

    .line 4639
    :cond_1
    iput-object v8, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->by:Lzx;

    .line 4640
    iput-object v8, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bz:Ljava/lang/String;

    .line 4642
    const/4 v3, 0x5

    .line 4643
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 4645
    new-instance v4, Lzx;

    iget-object v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->as:Lahv;

    new-instance v6, Ljava/lang/Integer;

    invoke-direct {v6, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-direct {v4, v0, v5, v7, v6}, Lzx;-><init>(Lbyq;Laac;ZLjava/lang/Object;)V

    .line 4652
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g:Lbai;

    check-cast v0, Laad;

    invoke-virtual {v0}, Laad;->a()Landroid/database/Cursor;

    move-result-object v0

    .line 4653
    if-eqz v0, :cond_4

    invoke-interface {v0}, Landroid/database/Cursor;->moveToLast()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 4654
    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 4655
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 4656
    iput-object v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bx:Lzx;

    .line 4657
    invoke-static {}, Lbsn;->c()Lbsn;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bx:Lzx;

    invoke-virtual {v0, v3}, Lbsn;->a(Lbrv;)Z

    .line 4665
    :cond_2
    :goto_1
    iput-wide v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bv:J

    .line 4668
    :cond_3
    iput-object v8, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bw:Landroid/database/Cursor;

    .line 4670
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    iget-wide v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bv:J

    invoke-static {p2, v0, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;Ljava/lang/String;J)I

    goto :goto_0

    .line 4659
    :cond_4
    iput-object v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->by:Lzx;

    .line 4661
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bz:Ljava/lang/String;

    goto :goto_1
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 234
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->e(Ljava/lang/String;)V

    return-void
.end method

.method private b(Ldg;Landroid/database/Cursor;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x6

    const/4 v8, 0x4

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 5267
    sget-boolean v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i:Z

    if-eqz v0, :cond_0

    .line 5268
    const-string v1, "Babel"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "onMessagesLoaderFinished MESSAGES_LOADER "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " mAdapter.getCount(): "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g:Lbai;

    check-cast v0, Laad;

    .line 5269
    invoke-virtual {v0}, Laad;->getCount()I

    move-result v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " new cursor data count: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 5270
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5268
    invoke-static {v1, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5273
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ch:Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;->a()Lccw;

    move-result-object v0

    invoke-interface {v0}, Lccw;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5274
    sget-boolean v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i:Z

    if-eqz v0, :cond_1

    .line 5275
    const-string v0, "Babel"

    const-string v1, "  ignore onMessagesLoaderFinished() MESSAGES_LOADER, UI updateblocking is on."

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5409
    :cond_1
    :goto_0
    return-void

    .line 5282
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g:Lbai;

    check-cast v0, Laad;

    invoke-virtual {v0}, Laad;->getCount()I

    move-result v4

    .line 5283
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->h:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v5

    .line 5284
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v6

    .line 5285
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->h:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 5286
    if-eqz v0, :cond_e

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    move v1, v0

    .line 5288
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g:Lbai;

    check-cast v0, Laad;

    invoke-virtual {v0, p2}, Laad;->d(Landroid/database/Cursor;)Z

    .line 5293
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->by:Lzx;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bz:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 5294
    invoke-interface {p2}, Landroid/database/Cursor;->moveToLast()Z

    .line 5295
    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v7, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bz:Ljava/lang/String;

    .line 5296
    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 5297
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->by:Lzx;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bx:Lzx;

    .line 5298
    invoke-static {}, Lbsn;->c()Lbsn;

    move-result-object v0

    iget-object v7, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bx:Lzx;

    invoke-virtual {v0, v7}, Lbsn;->a(Lbrv;)Z

    .line 5303
    :cond_3
    iput-object v10, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->by:Lzx;

    .line 5304
    iput-object v10, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bz:Ljava/lang/String;

    .line 5310
    add-int/lit8 v0, v4, 0x1

    if-le v6, v0, :cond_4

    .line 5312
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aH()Z

    move-result v0

    if-eqz v0, :cond_f

    move v0, v3

    .line 5315
    :goto_2
    add-int/2addr v5, v0

    add-int/lit8 v5, v5, -0x1

    sub-int v4, v6, v4

    add-int/2addr v4, v5

    .line 5318
    add-int/lit8 v5, v6, -0x1

    .line 5319
    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 5318
    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 5320
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->h:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0, v4, v1}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 5324
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    if-nez v0, :cond_10

    .line 5325
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->c(Landroid/view/View;)V

    .line 5330
    :goto_3
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_11

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 5331
    invoke-interface {p2, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bj:J

    .line 5336
    :goto_4
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_6

    invoke-interface {p2}, Landroid/database/Cursor;->moveToLast()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 5337
    iget-wide v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bk:J

    .line 5338
    invoke-interface {p2, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bk:J

    .line 5339
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->F()Z

    move-result v4

    if-eqz v4, :cond_5

    iget-wide v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bk:J

    cmp-long v0, v4, v0

    if-lez v0, :cond_5

    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bO:Z

    if-nez v0, :cond_5

    .line 5340
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 5343
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->h(Ljava/lang/String;)V

    .line 5346
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->O()Z

    move-result v0

    if-nez v0, :cond_6

    .line 5348
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aD()V

    .line 5351
    :cond_6
    iput-boolean v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aJ:Z

    .line 5353
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aI()V

    .line 5366
    invoke-interface {p2}, Landroid/database/Cursor;->moveToLast()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 5367
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cz:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    .line 5369
    :cond_7
    invoke-interface {p2, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 5370
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cz:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 5371
    const/4 v1, 0x7

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 5372
    if-ne v1, v8, :cond_12

    .line 5373
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cz:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 5377
    :goto_5
    add-int/lit8 v0, v0, -0x1

    .line 5379
    :cond_8
    invoke-interface {p2, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 5380
    iget-boolean v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aG:Z

    if-nez v4, :cond_9

    if-eqz v1, :cond_9

    iget-object v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    .line 5381
    invoke-virtual {v4}, Lyj;->c()Lbdk;

    move-result-object v4

    iget-object v4, v4, Lbdk;->a:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 5382
    iput-boolean v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aG:Z

    .line 5384
    :cond_9
    invoke-interface {p2}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v1

    if-eqz v1, :cond_a

    iget-boolean v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aG:Z

    if-eqz v1, :cond_7

    if-gtz v0, :cond_7

    .line 5387
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cz:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_b

    .line 5388
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aK()Z

    .line 5390
    :cond_b
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ak()V

    .line 5396
    invoke-interface {p2}, Landroid/database/Cursor;->moveToLast()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 5398
    :cond_c
    const/16 v0, 0x22

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 5399
    invoke-static {v0}, Lf;->c(I)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 5400
    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->af:I

    .line 5405
    :cond_d
    :goto_6
    iput-boolean v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ag:Z

    .line 5408
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a()V

    goto/16 :goto_0

    :cond_e
    move v1, v2

    .line 5286
    goto/16 :goto_1

    :cond_f
    move v0, v2

    .line 5312
    goto/16 :goto_2

    .line 5327
    :cond_10
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Landroid/view/View;)V

    goto/16 :goto_3

    .line 5333
    :cond_11
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v4, 0x3e8

    mul-long/2addr v0, v4

    iput-wide v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bj:J

    goto/16 :goto_4

    .line 5375
    :cond_12
    iget-object v6, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cz:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v6, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    .line 5403
    :cond_13
    invoke-interface {p2}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v0

    if-nez v0, :cond_c

    goto :goto_6
.end method

.method private b(Lyh;)V
    .locals 2

    .prologue
    .line 2681
    iget-object v0, p1, Lyh;->g:Ljava/lang/String;

    invoke-static {v0}, Lf;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2698
    :goto_0
    return-void

    .line 2684
    :cond_0
    new-instance v0, Laey;

    invoke-direct {v0, p0, p1}, Laey;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Lyh;)V

    .line 2697
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Ljava/lang/Runnable;Z)V

    goto :goto_0
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 7391
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aO:Landroid/view/View;

    if-nez v0, :cond_0

    .line 7403
    :goto_0
    return-void

    .line 7394
    :cond_0
    if-eqz p1, :cond_1

    .line 7395
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aP:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ah:Lbwt;

    invoke-virtual {v1}, Lbwt;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 7396
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aP:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cB:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 7397
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aP:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 7402
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aO:Landroid/view/View;

    if-eqz p1, :cond_2

    const/4 v0, 0x0

    :goto_2
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 7399
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aP:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cB:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 7400
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->c()V

    goto :goto_1

    .line 7402
    :cond_2
    const/16 v0, 0x8

    goto :goto_2
.end method

.method private static b(II)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 3759
    const/4 v2, 0x2

    if-eq p0, v2, :cond_0

    if-ne p0, v0, :cond_1

    :cond_0
    move v2, v0

    .line 3762
    :goto_0
    if-eqz v2, :cond_2

    invoke-static {p1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->c(I)Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_1
    return v0

    :cond_1
    move v2, v1

    .line 3759
    goto :goto_0

    :cond_2
    move v0, v1

    .line 3762
    goto :goto_1
.end method

.method public static synthetic c(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;I)I
    .locals 0

    .prologue
    .line 234
    iput p1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bG:I

    return p1
.end method

.method private c(Lahc;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2857
    const-string v0, "Babel"

    const-string v1, "Calling setConversationParams from initialize()"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2858
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ah:Lbwt;

    invoke-virtual {v0}, Lbwt;->a()V

    .line 2859
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Lahc;)V

    .line 2860
    iput-boolean v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aD:Z

    .line 2861
    iput-boolean v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aC:Z

    .line 2862
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->B()V

    .line 2863
    return-void
.end method

.method private c(Landroid/database/Cursor;Lyj;)V
    .locals 13

    .prologue
    .line 4813
    const/4 v1, 0x0

    .line 4814
    const/4 v0, 0x0

    .line 4815
    const/4 v3, 0x0

    .line 4817
    iget v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bo:I

    .line 4818
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bp:Ljava/lang/String;

    .line 4825
    new-instance v6, Les;

    invoke-direct {v6}, Les;-><init>()V

    .line 4827
    if-eqz p1, :cond_14

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_14

    .line 4829
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    if-nez v2, :cond_16

    .line 4830
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 4831
    const/4 v0, 0x4

    .line 4832
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v1

    move-object v1, v0

    .line 4835
    :goto_1
    const/4 v0, 0x0

    .line 4836
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 4837
    const/16 v0, 0x11

    .line 4838
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 4840
    if-nez v3, :cond_0

    const/4 v0, 0x1

    if-ne v8, v0, :cond_0

    .line 4842
    const/4 v0, 0x1

    .line 4843
    const/4 v3, 0x3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bA:Ljava/lang/String;

    .line 4844
    const/4 v3, 0x4

    .line 4845
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bB:Ljava/lang/String;

    move v3, v0

    .line 4848
    :cond_0
    const/16 v0, 0x12

    .line 4849
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 4848
    invoke-static {v0}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v9

    .line 4850
    iget-wide v11, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bu:J

    cmp-long v0, v9, v11

    if-lez v0, :cond_1

    .line 4851
    iput-wide v9, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bu:J

    .line 4856
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aI:Les;

    invoke-virtual {v0, v7}, Les;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahf;

    .line 4857
    if-nez v0, :cond_2

    .line 4858
    new-instance v0, Lahf;

    invoke-direct {v0}, Lahf;-><init>()V

    .line 4860
    :cond_2
    invoke-virtual {v6, v7, v0}, Les;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4862
    iput-object v7, v0, Lahf;->a:Ljava/lang/String;

    .line 4863
    const/16 v4, 0x13

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    iput-wide v9, v0, Lahf;->c:J

    .line 4865
    const/4 v4, 0x6

    .line 4866
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-eqz v4, :cond_b

    const/4 v4, 0x1

    :goto_2
    iput-boolean v4, v0, Lahf;->d:Z

    .line 4868
    const/16 v4, 0x16

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lahf;->f:Ljava/lang/String;

    .line 4870
    iput v8, v0, Lahf;->b:I

    .line 4871
    const/16 v4, 0x14

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    iput-wide v9, v0, Lahf;->g:J

    .line 4873
    const/16 v4, 0x17

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    iput-wide v9, v0, Lahf;->h:J

    .line 4876
    iget-object v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-static {v7, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 4877
    iget-object v4, v0, Lahf;->f:Ljava/lang/String;

    iput-object v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->az:Ljava/lang/String;

    .line 4880
    :cond_3
    const/4 v4, 0x2

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 4881
    iget v9, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an:I

    if-eqz v9, :cond_4

    iget v9, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an:I

    if-eq v4, v9, :cond_4

    .line 4883
    const-string v9, "Babel"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "loaded conversation type "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " overriding "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " for conversation "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " (this is weird)"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 4888
    :cond_4
    const/4 v9, 0x1

    if-ne v8, v9, :cond_5

    .line 4889
    invoke-virtual {p2}, Lyj;->s()Z

    move-result v8

    if-eqz v8, :cond_c

    .line 4894
    const/4 v8, 0x0

    iput v8, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bo:I

    .line 4899
    :goto_3
    iput-object v7, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bp:Ljava/lang/String;

    .line 4900
    const/16 v7, 0xa

    .line 4901
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    iput-wide v7, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bq:J

    .line 4902
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ak()V

    .line 4905
    :cond_5
    iget-object v7, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    iget-object v0, v0, Lahf;->a:Ljava/lang/String;

    invoke-static {v7, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 4906
    iput v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an:I

    .line 4907
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an:I

    const/4 v4, 0x1

    if-eq v0, v4, :cond_6

    .line 4908
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a([Lajk;)V

    .line 4911
    :cond_6
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ad:Lajk;

    .line 4912
    new-instance v0, Lafw;

    invoke-direct {v0, p0}, Lafw;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lahg;)V

    .line 4932
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ad:Lajk;

    const/16 v4, 0x11

    .line 4933
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 4932
    invoke-direct {p0, v0, v4}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lajk;I)V

    .line 4935
    const/16 v0, 0x18

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aA:Ljava/lang/String;

    .line 4936
    const/16 v0, 0x19

    .line 4937
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aB:Ljava/lang/String;

    .line 4938
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ao:I

    .line 4939
    const/16 v0, 0x15

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ap:I

    .line 4945
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aK:Z

    .line 4947
    sget-boolean v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i:Z

    if-eqz v0, :cond_7

    .line 4948
    const-string v4, "Babel"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, "onConversationsLoaderFinished(CONVERSATION_LOADER) mConversationId="

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, ": this="

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bA:Ljava/lang/String;

    if-eqz v0, :cond_d

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v8, ", convName="

    invoke-direct {v0, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bA:Ljava/lang/String;

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_4
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, " "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 4954
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->d()I

    move-result v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 4948
    invoke-static {v4, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4958
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->u_()V

    .line 4960
    const/4 v0, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bV:I

    .line 4961
    const/16 v0, 0x8

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bW:I

    .line 4962
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    iget v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bV:I

    invoke-virtual {v0, v4}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->b(I)V

    .line 4963
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    iget v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bW:I

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->k()V

    .line 4964
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->be:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    iget v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bV:I

    invoke-virtual {v0, v4}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->b(I)V

    .line 4965
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->be:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    iget v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an:I

    invoke-virtual {v0, v4}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->c(I)V

    .line 4966
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aE()V

    .line 4968
    const/16 v0, 0xb

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/4 v4, 0x2

    if-ne v0, v4, :cond_e

    const/4 v0, 0x1

    :goto_5
    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cl:Z

    .line 4972
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ck:Z

    if-nez v0, :cond_9

    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cq:Z

    if-nez v0, :cond_9

    .line 4973
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ck:Z

    .line 4974
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ah:Lbwt;

    .line 4975
    invoke-virtual {v0}, Lbwt;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 4976
    const/4 v0, 0x0

    .line 4977
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 4978
    new-instance v0, Lyh;

    invoke-direct {v0, p1}, Lyh;-><init>(Landroid/database/Cursor;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lyh;)Lyh;

    move-result-object v0

    .line 4980
    :cond_8
    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->d(Lyh;)V

    .line 4984
    :cond_9
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_17

    .line 4985
    if-nez v3, :cond_a

    .line 4986
    iput-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bA:Ljava/lang/String;

    .line 4987
    iput-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bB:Ljava/lang/String;

    .line 4989
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aI:Les;

    invoke-virtual {v0}, Les;->size()I

    move-result v0

    if-nez v0, :cond_10

    .line 4991
    const-wide/16 v0, 0x0

    .line 4993
    invoke-virtual {v6}, Les;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-wide v1, v0

    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahf;

    .line 4994
    iget-wide v7, v0, Lahf;->h:J

    invoke-static {v1, v2, v7, v8}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    move-wide v1, v0

    .line 4996
    goto :goto_6

    .line 4866
    :cond_b
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 4896
    :cond_c
    const/16 v8, 0x9

    invoke-interface {p1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    iput v8, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bo:I

    goto/16 :goto_3

    .line 4948
    :cond_d
    const-string v0, ""

    goto/16 :goto_4

    .line 4968
    :cond_e
    const/4 v0, 0x0

    goto :goto_5

    .line 4997
    :cond_f
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g:Lbai;

    check-cast v0, Laad;

    invoke-virtual {v0, v1, v2}, Laad;->a(J)V

    .line 4999
    :cond_10
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aI:Les;

    invoke-virtual {v0}, Les;->clear()V

    .line 5000
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aI:Les;

    invoke-virtual {v0, v6}, Les;->a(Lfe;)V

    .line 5005
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aC()V

    .line 5018
    :cond_11
    :goto_7
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->au()V

    .line 5023
    if-nez v5, :cond_12

    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bo:I

    if-eqz v0, :cond_12

    .line 5025
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bp:Ljava/lang/String;

    invoke-static {p2, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->p(Lyj;Ljava/lang/String;)I

    .line 5030
    :cond_12
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aI()V

    .line 5037
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aL:Z

    if-eqz v0, :cond_13

    .line 5038
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aL:Z

    .line 5039
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aw()V

    .line 5043
    :cond_13
    if-eqz p2, :cond_15

    .line 5044
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    iget-wide v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bv:J

    invoke-static {p2, v0, v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;Ljava/lang/String;J)I

    .line 5052
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ay()V

    .line 5053
    :goto_8
    return-void

    .line 5009
    :cond_14
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 5010
    if-nez v0, :cond_11

    .line 5014
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cf:Lahm;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-interface {v0}, Lahm;->a()V

    goto :goto_7

    .line 5048
    :cond_15
    const-string v0, "Babel"

    const-string v1, "Null account in onConversationsLoaderFinished"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_8

    :cond_16
    move-object v2, v1

    move-object v1, v0

    goto/16 :goto_1

    :cond_17
    move-object v0, v1

    move-object v1, v2

    goto/16 :goto_0
.end method

.method public static synthetic c(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 234
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->h(Ljava/lang/String;)V

    return-void
.end method

.method private c(Lyh;)V
    .locals 6

    .prologue
    .line 3613
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    iput-object v0, p1, Lyh;->a:Ljava/lang/String;

    .line 3614
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->T()[Lajk;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 3615
    iget-object v4, v3, Lajk;->a:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 3616
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    iget-object v3, v3, Lajk;->a:Ljava/lang/String;

    invoke-static {v4, v5, v3, p1}, Lcom/google/android/apps/hangouts/content/DraftService;->a(Landroid/content/Context;Lyj;Ljava/lang/String;Lyh;)V

    .line 3614
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3619
    :cond_1
    return-void
.end method

.method public static c(I)Z
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 6893
    packed-switch p0, :pswitch_data_0

    .line 6901
    const/4 v0, 0x0

    :pswitch_0
    return v0

    .line 6893
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static synthetic c(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Z
    .locals 1

    .prologue
    .line 234
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bQ:Z

    return v0
.end method

.method public static synthetic d(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;I)I
    .locals 0

    .prologue
    .line 234
    iput p1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bI:I

    return p1
.end method

.method public static synthetic d(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 234
    iput-object p1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aE:Ljava/lang/String;

    return-object p1
.end method

.method public static synthetic d(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V
    .locals 10

    .prologue
    .line 234
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bf:Lbyz;

    invoke-virtual {v0}, Lbyz;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lahz;

    iget-wide v6, v1, Lahz;->b:J

    sub-long v6, v2, v6

    const-wide/16 v8, 0x7530

    cmp-long v1, v6, v8

    if-lez v1, :cond_0

    const-string v1, "Babel"

    const/4 v6, 0x3

    invoke-static {v1, v6}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v6, "Babel"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v1, "Typing status expired for "

    invoke-direct {v7, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lahz;

    iget-object v1, v1, Lahz;->a:Ljava/lang/String;

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdk;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bf:Lbyz;

    invoke-virtual {v2, v0}, Lbyz;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lbdk;)Lbdh;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->be:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    const/4 v3, 0x4

    const/4 v4, 0x1

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->a(Lbdh;IZ)V

    goto :goto_1

    :cond_4
    return-void
.end method

.method public static synthetic d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 234
    invoke-static {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i(Ljava/lang/String;)V

    return-void
.end method

.method private d(Lyh;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v0, 0x0

    .line 5059
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lyh;->a()Z

    move-result v1

    if-nez v1, :cond_3

    .line 5060
    iget-object v1, p1, Lyh;->b:Ljava/lang/String;

    .line 5061
    iget-object v0, p1, Lyh;->c:Ljava/lang/String;

    .line 5062
    iget-object v2, p1, Lyh;->g:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bK:Ljava/lang/String;

    .line 5063
    iget-object v2, p1, Lyh;->d:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bF:Ljava/lang/String;

    .line 5064
    iget v2, p1, Lyh;->e:I

    iput v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bG:I

    .line 5065
    iget-object v2, p1, Lyh;->f:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bH:Ljava/lang/String;

    .line 5068
    iget-boolean v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aw:Z

    if-nez v2, :cond_0

    iget-object v2, p1, Lyh;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 5069
    iget-object v2, p1, Lyh;->a:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    .line 5070
    iput-boolean v6, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aw:Z

    .line 5074
    :cond_0
    iget-object v2, p1, Lyh;->d:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 5075
    sget-boolean v2, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i:Z

    if-eqz v2, :cond_1

    .line 5076
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mPendingAttachmentUrl: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lyh;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 5078
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Lyh;)V

    .line 5089
    :cond_2
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ah:Lbwt;

    invoke-virtual {v2, v0, v5}, Lbwt;->a(Ljava/lang/String;Z)V

    .line 5090
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ah:Lbwt;

    invoke-virtual {v0}, Lbwt;->d()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Z)V

    .line 5092
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-virtual {v0, v1, v6}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a(Ljava/lang/String;Z)V

    .line 5093
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a()V

    .line 5094
    return-void

    .line 5083
    :cond_3
    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bK:Ljava/lang/String;

    .line 5084
    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bF:Ljava/lang/String;

    .line 5085
    iput v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bG:I

    .line 5086
    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bH:Ljava/lang/String;

    move-object v1, v0

    goto :goto_0
.end method

.method public static synthetic e(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;I)I
    .locals 0

    .prologue
    .line 234
    iput p1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bJ:I

    return p1
.end method

.method public static synthetic e(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bi:Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 234
    iput-object p1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bK:Ljava/lang/String;

    return-object p1
.end method

.method private e(I)V
    .locals 3

    .prologue
    .line 927
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cd:[Ljava/util/ArrayList;

    aget-object v1, v0, p1

    .line 928
    if-nez v1, :cond_0

    .line 935
    :goto_0
    return-void

    .line 931
    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 932
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_1

    .line 934
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    goto :goto_0
.end method

.method private e(Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 2191
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cf:Lahm;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2193
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->K()Lbdh;

    move-result-object v2

    .line 2194
    if-eqz v2, :cond_0

    .line 2195
    new-instance v0, Lbkn;

    const/4 v1, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v5, v2, Lbdh;->e:Ljava/lang/String;

    iget-object v6, v2, Lbdh;->h:Ljava/lang/String;

    iget-object v7, v2, Lbdh;->h:Ljava/lang/String;

    const/16 v8, 0x3f

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lbkn;-><init>(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 2199
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cf:Lahm;

    invoke-interface {v1, v0}, Lahm;->a(Lbkn;)V

    .line 2202
    :cond_0
    return-void
.end method

.method public static synthetic f(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)I
    .locals 1

    .prologue
    .line 234
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ae:I

    return v0
.end method

.method public static synthetic f(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 234
    iput-object p1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bF:Ljava/lang/String;

    return-object p1
.end method

.method private f(I)V
    .locals 3

    .prologue
    .line 6475
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bW:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 6476
    const-string v0, "Babel"

    const-string v1, "toggleHistoryNotAllowed"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    invoke-virtual {v0}, Lyj;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lh;->kk:I

    :goto_0
    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 6480
    :goto_1
    return-void

    .line 6476
    :cond_0
    sget v0, Lh;->kj:I

    goto :goto_0

    .line 6478
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->d(Lyj;Ljava/lang/String;I)I

    goto :goto_1
.end method

.method public static synthetic f(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;I)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 234
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aK()Z

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g:Lbai;

    check-cast v0, Laad;

    invoke-virtual {v0}, Laad;->a()Landroid/database/Cursor;

    move-result-object v3

    invoke-interface {v3, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    const/16 v0, 0x17

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lf;->f(Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    :cond_0
    :goto_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "http://"

    invoke-virtual {v1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "https://"

    invoke-virtual {v1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    invoke-interface {v3, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    const-string v6, "UTF-8"

    invoke-static {v2, v6}, Lbvx;->a(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v2

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Ljava/lang/String;Ljava/lang/String;[BJZ)V

    :cond_2
    return-void

    :cond_3
    move-object v1, v2

    goto :goto_0

    :cond_4
    move-object v1, v2

    goto :goto_1
.end method

.method public static synthetic g(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lajk;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ad:Lajk;

    return-object v0
.end method

.method public static synthetic g(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 234
    iput-object p1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bH:Ljava/lang/String;

    return-object p1
.end method

.method public static synthetic g(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 234
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aK()Z

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g:Lbai;

    check-cast v0, Laad;

    invoke-virtual {v0}, Laad;->a()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    const/4 v2, 0x1

    new-array v2, v2, [J

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    aput-wide v3, v2, v5

    invoke-static {v1, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;[J)V

    return-void
.end method

.method private g(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2205
    invoke-static {p1}, Lbbl;->j(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2206
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->startActivity(Landroid/content/Intent;)V

    .line 2207
    return-void
.end method

.method private h(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 3501
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aK:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bu:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 3502
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    invoke-static {v0, p1}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->i(Lyj;Ljava/lang/String;)I

    .line 3506
    :goto_0
    return-void

    .line 3504
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aL:Z

    goto :goto_0
.end method

.method public static synthetic h(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Z
    .locals 1

    .prologue
    .line 234
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ar:Z

    return v0
.end method

.method public static synthetic i(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Les;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aI:Les;

    return-object v0
.end method

.method private static i(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 4187
    if-eqz p0, :cond_0

    .line 4188
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 4189
    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/hangouts/content/EsProvider;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4190
    new-instance v1, Lahk;

    invoke-direct {v1, v0}, Lahk;-><init>(Landroid/net/Uri;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Lahk;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 4193
    :cond_0
    return-void
.end method

.method public static synthetic j(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lahm;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cf:Lahm;

    return-object v0
.end method

.method private j(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 5201
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aI:Les;

    invoke-virtual {v0}, Les;->size()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aI:Les;

    .line 5202
    invoke-virtual {v0}, Les;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic k(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic l(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lyc;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bl:Lyc;

    return-object v0
.end method

.method public static synthetic m(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/util/List;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bX:Ljava/util/List;

    return-object v0
.end method

.method public static synthetic n(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->be:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    return-object v0
.end method

.method public static synthetic o(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lbyz;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bf:Lbyz;

    return-object v0
.end method

.method public static synthetic p(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ci:Ljava/lang/Runnable;

    return-object v0
.end method

.method public static synthetic q(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bg:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic r(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)I
    .locals 1

    .prologue
    .line 234
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an:I

    return v0
.end method

.method public static synthetic s(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V
    .locals 0

    .prologue
    .line 234
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ar()V

    return-void
.end method

.method public static synthetic t(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V
    .locals 0

    .prologue
    .line 234
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->as()V

    return-void
.end method

.method public static synthetic u(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V
    .locals 0

    .prologue
    .line 234
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ay()V

    return-void
.end method

.method public static synthetic v(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V
    .locals 0

    .prologue
    .line 234
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aC()V

    return-void
.end method

.method public static synthetic w(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V
    .locals 0

    .prologue
    .line 234
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aD()V

    return-void
.end method

.method public static synthetic x(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lbza;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bn:Lbza;

    return-object v0
.end method

.method public static synthetic y(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cn:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    return-object v0
.end method

.method public static synthetic z(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cp:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method A()Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2717
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2718
    const-string v2, "share_intent"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 2719
    if-nez v0, :cond_1

    move-object v0, v1

    .line 2729
    :cond_0
    :goto_0
    return-object v0

    .line 2722
    :cond_1
    const-string v2, "conversation_id"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2723
    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    move-object v0, v1

    .line 2724
    goto :goto_0

    .line 2726
    :cond_2
    iget-boolean v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bE:Z

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 2727
    goto :goto_0
.end method

.method public B()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2767
    sget-boolean v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i:Z

    if-eqz v0, :cond_0

    .line 2768
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleOptionalShareIntent mComposeMessageView: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2771
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    .line 2773
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2774
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->A()Landroid/content/Intent;

    move-result-object v1

    .line 2775
    const-string v0, "handleOptionalShareIntent"

    invoke-static {v0, v1}, Lbxm;->a(Ljava/lang/String;Landroid/content/Intent;)V

    .line 2776
    if-eqz v1, :cond_2

    .line 2783
    iput-boolean v8, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bE:Z

    .line 2785
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2786
    const-string v2, "android.intent.extra.TEXT"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2787
    iput-boolean v8, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cq:Z

    .line 2788
    const-string v2, "Babel"

    const-string v3, "handleOptionalShareIntent attaching TEXT"

    invoke-static {v2, v3}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2789
    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    const-string v3, "android.intent.extra.TEXT"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v7}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a(Ljava/lang/String;Z)V

    .line 2791
    :cond_1
    invoke-virtual {v1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v2

    .line 2792
    invoke-static {v2}, Lf;->d(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2793
    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 2794
    if-nez v0, :cond_3

    .line 2853
    :cond_2
    :goto_0
    return-void

    .line 2797
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    const/4 v3, 0x6

    invoke-virtual {v1, v3}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a(I)V

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aT:Landroid/view/View;

    sget v3, Lg;->v:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget v3, Lh;->nK:I

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aT:Landroid/view/View;

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aT:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aY:Landroid/view/View;

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aU:Landroid/widget/ImageView;

    sget v3, Lcom/google/android/apps/hangouts/R$drawable;->cr:I

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bF:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bK:Ljava/lang/String;

    goto :goto_0

    .line 2799
    :cond_4
    const-string v2, "requires_mms"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2800
    const-string v2, "uri"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ai:Ljava/lang/String;

    .line 2801
    iput v7, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aj:I

    .line 2802
    const-string v2, "draft_attachment_url"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2803
    const-string v3, "draft_subject"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2804
    iget-object v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ah:Lbwt;

    invoke-virtual {v4, v3, v8}, Lbwt;->a(Ljava/lang/String;Z)V

    .line 2805
    iget-object v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ah:Lbwt;

    invoke-virtual {v4}, Lbwt;->d()Z

    move-result v4

    invoke-direct {p0, v4}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Z)V

    .line 2806
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 2807
    iget-object v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ah:Lbwt;

    invoke-virtual {v4, v8, v8}, Lbwt;->a(ZZ)V

    .line 2808
    iput v8, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aj:I

    .line 2809
    new-instance v4, Lyh;

    invoke-direct {v4}, Lyh;-><init>()V

    .line 2810
    iput-object v2, v4, Lyh;->d:Ljava/lang/String;

    .line 2811
    const-string v2, "draft_content_type"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lyh;->g:Ljava/lang/String;

    .line 2812
    iput-object v3, v4, Lyh;->c:Ljava/lang/String;

    .line 2813
    invoke-direct {p0, v4}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Lyh;)V

    .line 2843
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bt:Landroid/widget/EditText;

    invoke-virtual {v0, v7}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 2845
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a(I)V

    .line 2847
    :cond_5
    const-string v0, "Babel"

    const-string v2, "handleOptionalShareIntent possibly attaching PHOTO"

    invoke-static {v0, v2}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2849
    const/4 v0, -0x1

    invoke-virtual {p0, v8, v0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->onActivityResult(IILandroid/content/Intent;)V

    goto/16 :goto_0

    .line 2815
    :cond_6
    const-string v2, "draft_attachment_count"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aj:I

    .line 2816
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aj:I

    if-lez v0, :cond_7

    .line 2817
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ah:Lbwt;

    invoke-virtual {v0, v8, v8}, Lbwt;->a(ZZ)V

    .line 2818
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aT:Landroid/view/View;

    sget v2, Lg;->v:I

    .line 2819
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2820
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lf;->hr:I

    iget v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aj:I

    new-array v5, v8, [Ljava/lang/Object;

    iget v6, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aj:I

    .line 2823
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    .line 2821
    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2820
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2824
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aT:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 2825
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aY:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 2826
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aZ:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2827
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aU:Landroid/widget/ImageView;

    sget v2, Lcom/google/android/apps/hangouts/R$drawable;->aK:I

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 2832
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aT:Landroid/view/View;

    sget v2, Lg;->v:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v2, Lh;->cT:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aT:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aT:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aY:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aU:Landroid/widget/ImageView;

    sget v2, Lcom/google/android/apps/hangouts/R$drawable;->aK:I

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1
.end method

.method public C()V
    .locals 2

    .prologue
    .line 2866
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    if-eqz v0, :cond_0

    .line 2867
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->setVisibility(I)V

    .line 2870
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cj:Z

    .line 2871
    return-void
.end method

.method public D()Z
    .locals 1

    .prologue
    .line 2879
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cj:Z

    invoke-static {v0}, Lcwz;->b(Z)V

    .line 2880
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->d()Z

    move-result v0

    return v0
.end method

.method public E()V
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    const/16 v7, 0x8

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 2885
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g:Lbai;

    check-cast v0, Laad;

    invoke-virtual {v0, v5}, Laad;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 2886
    const-string v0, "Babel"

    const-string v2, "stopLoaders"

    invoke-static {v0, v2}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    :goto_0
    sget-object v2, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cy:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getLoaderManager()Lav;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cy:[I

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Lav;->a(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2887
    :cond_0
    invoke-virtual {p0, v6}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Z)V

    .line 2889
    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cd:[Ljava/util/ArrayList;

    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2891
    :cond_2
    iput-object v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    .line 2892
    iput-object v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aE:Ljava/lang/String;

    .line 2894
    iput-wide v8, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bu:J

    .line 2895
    iput-wide v8, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bv:J

    .line 2896
    iput-object v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bw:Landroid/database/Cursor;

    .line 2898
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aG()V

    .line 2900
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bx:Lzx;

    if-eqz v0, :cond_3

    .line 2901
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bx:Lzx;

    invoke-virtual {v0}, Lzx;->b()V

    .line 2902
    iput-object v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bx:Lzx;

    .line 2905
    :cond_3
    iput-object v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->by:Lzx;

    .line 2906
    iput-object v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bz:Ljava/lang/String;

    .line 2908
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aW:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aW:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2911
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-virtual {v0, v5, v1}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a(Ljava/lang/String;Z)V

    .line 2912
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ck:Z

    .line 2914
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bg:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bR:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2915
    iput-boolean v6, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bQ:Z

    move v0, v1

    .line 2916
    :goto_2
    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bS:[Z

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 2917
    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bS:[Z

    aput-boolean v1, v2, v0

    .line 2916
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2920
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g:Lbai;

    check-cast v0, Laad;

    invoke-virtual {v0}, Laad;->e()V

    .line 2922
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aJ:Z

    .line 2923
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aK:Z

    .line 2924
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aL:Z

    .line 2925
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bl:Lyc;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lyc;->d(Lyj;Ljava/lang/String;)V

    .line 2926
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bm:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2927
    iput v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->br:I

    .line 2928
    iput-object v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bs:Lbdh;

    .line 2929
    iput-object v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ai:Ljava/lang/String;

    .line 2930
    iput v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aj:I

    .line 2931
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aG:Z

    .line 2932
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aH:Z

    .line 2934
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ah:Lbwt;

    invoke-virtual {v0}, Lbwt;->b()V

    .line 2936
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ak:Z

    .line 2937
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->be:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->e()V

    .line 2940
    invoke-static {v1}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->c(Z)V

    .line 2942
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aT:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 2944
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bF:Ljava/lang/String;

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bL:Lcom/google/android/gms/maps/model/MarkerOptions;

    if-eqz v0, :cond_6

    .line 2945
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->L()V

    .line 2947
    :cond_6
    invoke-direct {p0, v5, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lajk;I)V

    .line 2949
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bf:Lbyz;

    invoke-virtual {v0}, Lbyz;->clear()V

    .line 2951
    iput v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bo:I

    .line 2952
    iput-object v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bp:Ljava/lang/String;

    .line 2953
    iput-object v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bn:Lbza;

    .line 2954
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cn:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->e()V

    .line 2955
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cn:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    invoke-virtual {v0, v2, v1, v3}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->a(Ljava/lang/String;ILyj;)V

    .line 2957
    const/16 v0, 0x1e

    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ao:I

    .line 2958
    iput v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ap:I

    .line 2960
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bh:Lcom/google/android/apps/hangouts/views/EasterEggView;

    if-eqz v0, :cond_7

    .line 2961
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bh:Lcom/google/android/apps/hangouts/views/EasterEggView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/EasterEggView;->a()V

    .line 2964
    :cond_7
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aA()V

    .line 2965
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ak()V

    .line 2968
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->u_()V

    .line 2969
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cb:Z

    .line 2970
    return-void
.end method

.method public F()Z
    .locals 1

    .prologue
    .line 3330
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bC:Z

    return v0
.end method

.method public G()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 3352
    sget-boolean v1, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i:Z

    if-eqz v1, :cond_0

    .line 3353
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "FOCUS: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " isFocused(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->F()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3355
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->F()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 3432
    :cond_1
    :goto_0
    return v0

    .line 3358
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 3359
    const/4 v0, 0x0

    goto :goto_0

    .line 3362
    :cond_3
    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bC:Z

    .line 3364
    const/4 v1, 0x3

    iput v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bD:I

    .line 3365
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    if-eqz v1, :cond_4

    .line 3366
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->c()V

    .line 3369
    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->au()V

    .line 3372
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->O()Z

    move-result v1

    if-nez v1, :cond_5

    .line 3375
    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bO:Z

    .line 3376
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bg:Landroid/os/Handler;

    new-instance v2, Lafc;

    invoke-direct {v2, p0}, Lafc;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    const-wide/16 v3, 0xc8

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 3415
    :cond_5
    new-instance v1, Lafe;

    invoke-direct {v1, p0}, Lafe;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    iput-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->Z:Ljava/lang/Runnable;

    .line 3426
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bg:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->Z:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 3428
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bw:Landroid/database/Cursor;

    if-eqz v1, :cond_1

    .line 3429
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bw:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Landroid/database/Cursor;Lyj;)V

    goto :goto_0
.end method

.method public H()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 3622
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bt:Landroid/widget/EditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 3623
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    const-string v1, ""

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a(Ljava/lang/String;Z)V

    .line 3624
    iput-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bF:Ljava/lang/String;

    .line 3625
    iput-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bH:Ljava/lang/String;

    .line 3626
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ah:Lbwt;

    if-eqz v0, :cond_0

    .line 3627
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ah:Lbwt;

    invoke-virtual {v0}, Lbwt;->b()V

    .line 3629
    :cond_0
    new-instance v0, Lyh;

    invoke-direct {v0}, Lyh;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->c(Lyh;)V

    .line 3630
    return-void
.end method

.method public I()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3636
    const-string v0, "Babel"

    const-string v3, "messageLoadFailed"

    invoke-static {v0, v3}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3639
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->h:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    if-gtz v0, :cond_1

    .line 3640
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v0, v3, :cond_0

    .line 3641
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->h:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->smoothScrollToPositionFromTop(II)V

    :cond_0
    move v0, v1

    .line 3649
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v1

    sget v2, Lh;->jy:I

    invoke-static {v1, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 3650
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 3651
    return-void

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method public J()V
    .locals 1

    .prologue
    .line 4178
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bF:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i(Ljava/lang/String;)V

    .line 4179
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->L()V

    .line 4180
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aG()V

    .line 4181
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bN:I

    .line 4182
    return-void
.end method

.method public K()Lbdh;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 4402
    iget v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 4404
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bl:Lyc;

    invoke-virtual {v1}, Lyc;->b()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    .line 4405
    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bl:Lyc;

    iget-object v4, v0, Lbdh;->b:Lbdk;

    invoke-virtual {v3, v4}, Lyc;->c(Lbdk;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 4406
    iget-object v3, v0, Lbdh;->b:Lbdk;

    iget-object v3, v3, Lbdk;->a:Ljava/lang/String;

    if-nez v3, :cond_2

    .line 4409
    if-nez v1, :cond_0

    move-object v1, v0

    .line 4410
    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 4421
    :cond_2
    return-object v0
.end method

.method public L()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 6426
    const-string v0, "Babel"

    const-string v1, "cancelPendingAttachment"

    invoke-static {v0, v1}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 6427
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    if-eqz v0, :cond_0

    .line 6428
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a(I)V

    .line 6430
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aT:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 6431
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aT:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 6433
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aU:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 6434
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aU:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 6436
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ba:Lcom/google/android/apps/hangouts/views/SquareMapView;

    if-eqz v0, :cond_3

    .line 6437
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ba:Lcom/google/android/apps/hangouts/views/SquareMapView;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/hangouts/views/SquareMapView;->setVisibility(I)V

    .line 6439
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aX:Lccm;

    if-eqz v0, :cond_4

    .line 6440
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aX:Lccm;

    invoke-virtual {v0}, Lccm;->c()V

    .line 6442
    :cond_4
    iput-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bF:Ljava/lang/String;

    .line 6443
    iput v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bG:I

    .line 6444
    iput-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bH:Ljava/lang/String;

    .line 6445
    iput-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bK:Ljava/lang/String;

    .line 6446
    iput-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bL:Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 6447
    iput-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bM:Lcom/google/android/gms/maps/model/CameraPosition;

    .line 6448
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bc:Lzx;

    if-eqz v0, :cond_5

    .line 6449
    invoke-static {}, Lbsn;->c()Lbsn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bc:Lzx;

    invoke-virtual {v0, v1}, Lbsn;->b(Lbrv;)V

    .line 6450
    iput-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bc:Lzx;

    .line 6452
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ai:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 6454
    iput-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ai:Ljava/lang/String;

    .line 6455
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->H()V

    .line 6456
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->az()V

    .line 6458
    :cond_6
    return-void
.end method

.method public M()V
    .locals 6

    .prologue
    .line 6613
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    move v4, v0

    .line 6614
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v2

    .line 6617
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->R()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 6618
    if-eqz v4, :cond_1

    sget v0, Lh;->jx:I

    :goto_1
    move v1, v0

    .line 6627
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    .line 6629
    invoke-virtual {v2, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    .line 6631
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax()Ljava/util/Collection;

    move-result-object v3

    if-eqz v4, :cond_5

    const/4 v4, 0x4

    .line 6633
    :goto_3
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->d()I

    move-result v5

    .line 6627
    invoke-static/range {v0 .. v5}, Lbbl;->a(Lyj;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;II)Landroid/content/Intent;

    move-result-object v0

    .line 6635
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->startActivity(Landroid/content/Intent;)V

    .line 6636
    return-void

    .line 6613
    :cond_0
    const/4 v0, 0x0

    move v4, v0

    goto :goto_0

    .line 6619
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->d()I

    move-result v0

    invoke-static {v0}, Lbvx;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    sget v0, Lh;->hr:I

    goto :goto_1

    :cond_2
    sget v0, Lh;->hs:I

    goto :goto_1

    .line 6623
    :cond_3
    if-eqz v4, :cond_4

    sget v0, Lh;->jx:I

    :goto_4
    move v1, v0

    goto :goto_2

    :cond_4
    sget v0, Lh;->hq:I

    goto :goto_4

    .line 6631
    :cond_5
    const/4 v4, 0x3

    goto :goto_3
.end method

.method public N()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6696
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    return-object v0
.end method

.method public O()Z
    .locals 2

    .prologue
    .line 6776
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getId()I

    move-result v0

    sget v1, Lg;->dK:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public P()I
    .locals 1

    .prologue
    .line 6922
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->af:I

    return v0
.end method

.method public Q()Z
    .locals 1

    .prologue
    .line 6931
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ac:I

    invoke-static {v0}, Lf;->d(I)Z

    move-result v0

    return v0
.end method

.method public R()Z
    .locals 1

    .prologue
    .line 6935
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ac:I

    invoke-static {v0}, Lf;->c(I)Z

    move-result v0

    return v0
.end method

.method public S()Z
    .locals 1

    .prologue
    .line 6939
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->d()I

    move-result v0

    invoke-static {v0}, Lf;->b(I)Z

    move-result v0

    return v0
.end method

.method public T()[Lajk;
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 7027
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ca:Lajg;

    if-eqz v0, :cond_2

    .line 7028
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ca:Lajg;

    invoke-virtual {v0}, Lajg;->a()[Lajk;

    move-result-object v0

    .line 7030
    :goto_0
    if-eqz v0, :cond_0

    array-length v1, v0

    if-nez v1, :cond_1

    .line 7031
    :cond_0
    new-instance v7, Lyv;

    invoke-direct {v7}, Lyv;-><init>()V

    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ac:I

    iput v0, v7, Lyv;->c:I

    const/4 v0, 0x1

    new-array v10, v0, [Lajk;

    const/4 v11, 0x0

    new-instance v0, Lajk;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    iget v9, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ac:I

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v8, v2

    invoke-direct/range {v0 .. v9}, Lajk;-><init>(Ljava/lang/String;Lbdh;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lyv;Laea;I)V

    aput-object v0, v10, v11

    move-object v0, v10

    .line 7033
    :cond_1
    return-object v0

    :cond_2
    move-object v0, v2

    goto :goto_0
.end method

.method public U()V
    .locals 2

    .prologue
    .line 7107
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bd:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 7108
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->d()Lcom/google/android/apps/hangouts/phone/EsApplication;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->c(I)V

    .line 7110
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bd:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 7111
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bd:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 7112
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bd:Landroid/view/View;

    .line 7114
    :cond_0
    return-void
.end method

.method public V()I
    .locals 5

    .prologue
    const/4 v1, 0x3

    .line 7149
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    invoke-virtual {v0}, Lyj;->x()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 7150
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an:I

    const/4 v2, 0x1

    if-eq v0, v2, :cond_0

    move v0, v1

    .line 7167
    :goto_0
    return v0

    .line 7156
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bl:Lyc;

    invoke-virtual {v0}, Lyc;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdh;

    .line 7157
    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bl:Lyc;

    iget-object v4, v0, Lbdh;->b:Lbdk;

    invoke-virtual {v3, v4}, Lyc;->c(Lbdk;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 7158
    invoke-virtual {v0}, Lbdh;->e()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v0}, Lbdh;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_2
    move v0, v1

    .line 7161
    goto :goto_0

    .line 7167
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->P()I

    move-result v1

    invoke-virtual {v0, v1}, Lyj;->a(I)I

    move-result v0

    goto :goto_0
.end method

.method public W()I
    .locals 1

    .prologue
    .line 7332
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an:I

    return v0
.end method

.method public X()Z
    .locals 1

    .prologue
    .line 7426
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bZ:Z

    return v0
.end method

.method public Y()Lyh;
    .locals 7

    .prologue
    .line 7430
    new-instance v0, Lyh;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    .line 7431
    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->g()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ah:Lbwt;

    .line 7432
    invoke-virtual {v2}, Lbwt;->c()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bF:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bG:I

    iget-object v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bH:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bK:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lyh;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 7434
    return-object v0
.end method

.method public Z()I
    .locals 1

    .prologue
    .line 8040
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bN:I

    return v0
.end method

.method public a(Lbdk;)Lbdh;
    .locals 1

    .prologue
    .line 6757
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bl:Lyc;

    invoke-virtual {v0, p1}, Lyc;->b(Lbdk;)Lbdh;

    move-result-object v0

    return-object v0
.end method

.method public a()Lyj;
    .locals 1

    .prologue
    .line 6767
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    return-object v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 788
    iput p1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->br:I

    .line 789
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ak()V

    .line 790
    return-void
.end method

.method public a(II)V
    .locals 2

    .prologue
    .line 6643
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bo:I

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 6644
    :goto_0
    if-nez p2, :cond_0

    iget p2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bo:I

    .line 6647
    :cond_0
    new-instance v1, Lagh;

    invoke-direct {v1, p0, v0, p2, p1}, Lagh;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;ZII)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lahg;)V

    .line 6693
    return-void

    .line 6643
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lahc;)V
    .locals 1

    .prologue
    .line 3114
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->E()V

    .line 3115
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lahc;Z)V

    .line 3116
    return-void
.end method

.method public a(Lahc;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 3124
    sget-boolean v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i:Z

    if-eqz v0, :cond_0

    .line 3125
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "initializeAndStart conversationId: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3128
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aI:Les;

    invoke-virtual {v0}, Les;->clear()V

    .line 3129
    iput-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aE:Ljava/lang/String;

    .line 3130
    iput-boolean v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ag:Z

    .line 3131
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ae:I

    .line 3133
    iput-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ca:Lajg;

    .line 3134
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a([Lajk;)V

    .line 3137
    if-nez p2, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 3138
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->c(Lahc;)V

    .line 3141
    :cond_2
    iput-boolean v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bQ:Z

    .line 3143
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ar()V

    .line 3144
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->at()V

    .line 3145
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->as()V

    .line 3147
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bQ:Z

    if-eqz v0, :cond_3

    .line 3148
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bg:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bR:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 3153
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Landroid/view/View;)V

    .line 3155
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->B()V

    .line 3156
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->u_()V

    .line 3157
    return-void
.end method

.method public a(Lahm;)V
    .locals 0

    .prologue
    .line 984
    iput-object p1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cf:Lahm;

    .line 985
    return-void
.end method

.method public a(Lajk;)V
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v4, 0x0

    const/4 v11, 0x0

    .line 4675
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ad:Lajk;

    if-nez v0, :cond_1

    .line 4676
    invoke-virtual {p1}, Lajk;->a()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lajk;I)V

    .line 4742
    :cond_0
    :goto_0
    return-void

    .line 4680
    :cond_1
    iget-object v0, p1, Lajk;->g:Lyv;

    if-nez v0, :cond_4

    .line 4681
    new-instance v10, Ljava/util/HashSet;

    invoke-direct {v10}, Ljava/util/HashSet;-><init>()V

    .line 4683
    iget-object v0, p1, Lajk;->b:Lbdh;

    .line 4684
    invoke-virtual {v0}, Lbdh;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lajk;->b:Lbdh;

    .line 4685
    invoke-virtual {v1}, Lbdh;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lajk;->b:Lbdh;

    iget-object v2, v2, Lbdh;->d:Ljava/lang/String;

    iget-object v3, p1, Lajk;->c:Ljava/lang/String;

    iget-object v6, p1, Lajk;->d:Ljava/lang/String;

    .line 4693
    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    move-object v5, v4

    move-object v7, v4

    move-object v8, v4

    .line 4683
    invoke-static/range {v0 .. v9}, Lbdh;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)Lbdh;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 4695
    invoke-static {v10}, Lf;->a(Ljava/util/Collection;)Lxm;

    move-result-object v0

    .line 4700
    iget-object v1, p1, Lajk;->b:Lbdh;

    invoke-virtual {v1}, Lbdh;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 4701
    invoke-virtual {v0}, Lxm;->b()I

    move-result v1

    if-ne v1, v12, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ad:Lajk;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ad:Lajk;

    iget-object v1, v1, Lajk;->b:Lbdh;

    .line 4703
    invoke-virtual {v1}, Lbdh;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 4705
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ad:Lajk;

    iget-object v1, v1, Lajk;->b:Lbdh;

    .line 4706
    invoke-virtual {v1}, Lbdh;->b()Ljava/lang/String;

    move-result-object v1

    .line 4705
    invoke-static {v1}, Lbcn;->b(Ljava/lang/String;)Lbcn;

    move-result-object v1

    .line 4707
    invoke-virtual {v0, v11}, Lxm;->a(I)Lxs;

    move-result-object v2

    .line 4708
    invoke-virtual {v2}, Lxs;->b()Lbcx;

    move-result-object v2

    invoke-virtual {v2, v1}, Lbcx;->a(Lbcn;)V

    .line 4712
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a()Lyj;

    move-result-object v1

    .line 4717
    invoke-virtual {p1}, Lajk;->a()I

    move-result v2

    .line 4711
    invoke-static {v1, v0, v11, v11, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Lxm;ZZI)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ae:I

    .line 4718
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->N()Ljava/lang/String;

    move-result-object v0

    .line 4719
    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->S()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 4720
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    invoke-static {v1, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->u(Lyj;Ljava/lang/String;)V

    .line 4739
    :cond_3
    :goto_1
    iget-object v0, p1, Lajk;->h:Laea;

    if-eqz v0, :cond_0

    .line 4740
    iget-object v0, p1, Lajk;->h:Laea;

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Laea;)V

    goto/16 :goto_0

    .line 4725
    :cond_4
    invoke-virtual {p0, v11}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Z)V

    .line 4729
    iget-object v0, p1, Lajk;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    .line 4730
    iput-boolean v12, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aw:Z

    .line 4732
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aC()V

    .line 4734
    invoke-virtual {p1}, Lajk;->a()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lajk;I)V

    .line 4736
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->G()Z

    goto :goto_1
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 6550
    const-string v0, "conversation_error"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6551
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aJ()V

    .line 6582
    :cond_0
    :goto_0
    return-void

    .line 6552
    :cond_1
    const-string v0, "delete_conversation"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 6553
    invoke-virtual {p0, p1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->d(Landroid/os/Bundle;)Lyj;

    move-result-object v0

    .line 6554
    if-eqz v0, :cond_2

    .line 6555
    new-instance v1, Lagf;

    invoke-direct {v1, p0, v0}, Lagf;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Lyj;)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lahn;)V

    .line 6571
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bg:Landroid/os/Handler;

    new-instance v1, Lagg;

    invoke-direct {v1, p0}, Lagg;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 6577
    :cond_3
    const-string v0, "leave_conversation"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 6578
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aJ()V

    goto :goto_0

    .line 6579
    :cond_4
    const-string v0, "delete_message"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6580
    const-string v0, "row_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    const/4 v3, 0x1

    new-array v3, v3, [J

    const/4 v4, 0x0

    aput-wide v0, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;[J)V

    goto :goto_0
.end method

.method public a(Lccp;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 3753
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aK()Z

    .line 3754
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cf:Lahm;

    invoke-interface {v0, p1, p2, p3}, Lahm;->a(Lccp;Ljava/lang/String;Ljava/lang/String;)V

    .line 3755
    return-void
.end method

.method public a(Ldg;Landroid/database/Cursor;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/16 v7, 0x22

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 4286
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    if-nez v0, :cond_1

    .line 4287
    const-string v0, "Babel"

    const-string v1, "onLoadFinished called but fragment account is now null"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 4396
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p1

    .line 4290
    check-cast v0, Lbaj;

    invoke-virtual {v0}, Lbaj;->A()Lyj;

    move-result-object v0

    .line 4291
    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    invoke-virtual {v4}, Lyj;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 4292
    const-string v0, "Babel"

    const-string v1, "onLoadFinished called for non-fragment account"

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 4296
    :cond_2
    sget-boolean v2, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i:Z

    if-eqz v2, :cond_3

    .line 4297
    const-string v2, "Babel"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "onLoadFinished "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "; data count is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4299
    :cond_3
    invoke-virtual {p1}, Ldg;->l()I

    move-result v2

    .line 4300
    packed-switch v2, :pswitch_data_0

    .line 4322
    :cond_4
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ar:Z

    if-nez v0, :cond_5

    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an:I

    if-ne v0, v3, :cond_5

    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ac:I

    invoke-static {v0}, Lf;->c(I)Z

    move-result v0

    if-nez v0, :cond_c

    .line 4324
    :cond_5
    :goto_2
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM()V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    if-nez v0, :cond_d

    sget-boolean v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i:Z

    if-eqz v0, :cond_6

    const-string v0, "Babel"

    const-string v4, "tryRunConversationVariantsEngine -- NO ACCOUNT"

    invoke-static {v0, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ca:Lajg;

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ca:Lajg;

    iget-object v4, v4, Lajg;->b:Ljava/lang/String;

    invoke-static {v0, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1a

    sget-boolean v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i:Z

    if-eqz v0, :cond_7

    const-string v0, "Babel"

    const-string v4, "tryRunConversationVariantsEngine -- ID MATCHES"

    invoke-static {v0, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4326
    :cond_7
    :goto_4
    invoke-direct {p0, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->e(I)V

    .line 4328
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v0

    .line 4329
    if-eqz v0, :cond_8

    .line 4330
    invoke-virtual {v0}, Ly;->u_()V

    .line 4333
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bS:[Z

    aput-boolean v3, v0, v2

    .line 4334
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bQ:Z

    if-eqz v0, :cond_0

    move v0, v1

    .line 4336
    :goto_5
    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bS:[Z

    array-length v2, v2

    if-ge v0, v2, :cond_21

    .line 4337
    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bS:[Z

    aget-boolean v2, v2, v0

    if-nez v2, :cond_1b

    move v0, v1

    .line 4344
    :goto_6
    if-eqz v0, :cond_0

    .line 4345
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bg:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bR:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 4348
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    if-nez v0, :cond_1c

    .line 4349
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->c(Landroid/view/View;)V

    .line 4358
    :goto_7
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an:I

    if-ne v0, v3, :cond_a

    .line 4359
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->d()Lcom/google/android/apps/hangouts/phone/EsApplication;

    move-result-object v0

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->b(I)Z

    move-result v0

    if-nez v0, :cond_a

    .line 4361
    const-string v0, "babel_max_outgoing_message_search_rows"

    const/16 v2, 0x32

    invoke-static {v0, v2}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v2

    .line 4364
    if-lez v2, :cond_a

    .line 4365
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g:Lbai;

    check-cast v0, Laad;

    invoke-virtual {v0}, Laad;->a()Landroid/database/Cursor;

    move-result-object v4

    .line 4366
    if-eqz v4, :cond_a

    invoke-interface {v4}, Landroid/database/Cursor;->moveToLast()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 4367
    invoke-interface {v4, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    move v0, v2

    .line 4371
    :cond_9
    const/16 v2, 0x8

    invoke-interface {v4, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 4372
    if-ne v2, v3, :cond_1d

    .line 4373
    invoke-interface {v4, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    move v2, v0

    .line 4381
    :goto_8
    if-nez v5, :cond_1f

    move v0, v3

    :goto_9
    if-nez v0, :cond_a

    .line 4382
    if-nez v2, :cond_20

    move v0, v3

    :goto_a
    if-nez v0, :cond_a

    .line 4384
    invoke-static {v5}, Lf;->c(I)Z

    move-result v0

    .line 4385
    invoke-static {v2}, Lf;->c(I)Z

    move-result v2

    if-eq v0, v2, :cond_a

    .line 4386
    iput-boolean v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aH:Z

    .line 4387
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ak()V

    .line 4393
    :cond_a
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bQ:Z

    goto/16 :goto_0

    .line 4302
    :pswitch_0
    new-instance v0, Lahx;

    invoke-direct {v0, p2}, Lahx;-><init>(Landroid/database/Cursor;)V

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Ldg;Landroid/database/Cursor;)V

    goto/16 :goto_1

    .line 4305
    :pswitch_1
    invoke-direct {p0, p2, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->c(Landroid/database/Cursor;Lyj;)V

    .line 4306
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aI:Les;

    invoke-virtual {v0}, Les;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 4307
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getLoaderManager()Lav;

    move-result-object v0

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4, p0}, Lav;->a(ILandroid/os/Bundle;Law;)Ldg;

    goto/16 :goto_1

    .line 4311
    :pswitch_2
    invoke-direct {p0, p2, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Landroid/database/Cursor;Lyj;)V

    goto/16 :goto_1

    .line 4314
    :pswitch_3
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->F()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 4315
    invoke-direct {p0, p2, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Landroid/database/Cursor;Lyj;)V

    goto/16 :goto_1

    .line 4317
    :cond_b
    iput-object p2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bw:Landroid/database/Cursor;

    goto/16 :goto_1

    .line 4322
    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ag:Z

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->K()Lbdh;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lbdh;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->V()I

    move-result v4

    iget v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ac:I

    if-eq v5, v4, :cond_5

    invoke-static {v0}, Lbcx;->a(Ljava/lang/String;)Lbcx;

    move-result-object v5

    invoke-static {v0}, Lbcn;->b(Ljava/lang/String;)Lbcn;

    move-result-object v0

    invoke-virtual {v5, v0}, Lbcx;->a(Lbcn;)V

    invoke-static {}, Lxs;->newBuilder()Lxt;

    move-result-object v0

    invoke-virtual {v0, v5}, Lxt;->a(Lbcx;)V

    invoke-static {}, Lxm;->newBuilder()Lxn;

    move-result-object v5

    invoke-virtual {v0}, Lxt;->a()Lxs;

    move-result-object v0

    invoke-virtual {v5, v0}, Lxn;->a(Lxs;)Lxn;

    move-result-object v0

    invoke-virtual {v0}, Lxn;->a()Lxm;

    move-result-object v0

    iput-boolean v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ar:Z

    iget-object v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    invoke-static {v5, v0, v1, v1, v4}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Lxm;ZZI)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ae:I

    goto/16 :goto_2

    .line 4324
    :cond_d
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->O()Z

    move-result v0

    if-eqz v0, :cond_f

    sget-boolean v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i:Z

    if-eqz v0, :cond_e

    const-string v0, "Babel"

    const-string v4, "tryRunConversationVariantsEngine -- INVITE"

    invoke-static {v0, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_e
    move v0, v1

    goto/16 :goto_3

    :cond_f
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->K()Lbdh;

    move-result-object v0

    if-nez v0, :cond_11

    sget-boolean v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i:Z

    if-eqz v0, :cond_10

    const-string v0, "Babel"

    const-string v4, "tryRunConversationVariantsEngine -- NO ROOT"

    invoke-static {v0, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_10
    move v0, v1

    goto/16 :goto_3

    :cond_11
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aI:Les;

    invoke-virtual {v0}, Les;->size()I

    move-result v0

    if-nez v0, :cond_13

    sget-boolean v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i:Z

    if-eqz v0, :cond_12

    const-string v0, "Babel"

    const-string v4, "tryRunConversationVariantsEngine -- NO MERGED IDS"

    invoke-static {v0, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_12
    move v0, v1

    goto/16 :goto_3

    :cond_13
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->az:Ljava/lang/String;

    if-nez v0, :cond_15

    sget-boolean v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i:Z

    if-eqz v0, :cond_14

    const-string v0, "Babel"

    const-string v4, "tryRunConversationVariantsEngine -- NO MERGE KEY"

    invoke-static {v0, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_14
    move v0, v1

    goto/16 :goto_3

    :cond_15
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an:I

    if-eq v0, v3, :cond_17

    sget-boolean v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i:Z

    if-eqz v0, :cond_16

    const-string v0, "Babel"

    const-string v4, "tryRunConversationVariantsEngine -- NOT STICK_ONE_ONE"

    invoke-static {v0, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_16
    move v0, v1

    goto/16 :goto_3

    :cond_17
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ag:Z

    if-nez v0, :cond_19

    sget-boolean v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i:Z

    if-eqz v0, :cond_18

    const-string v0, "Babel"

    const-string v4, "tryRunConversationVariantsEngine -- NOT SMS SENT OR RECEIVED CALCULATED"

    invoke-static {v0, v4}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_18
    move v0, v1

    goto/16 :goto_3

    :cond_19
    move v0, v3

    goto/16 :goto_3

    :cond_1a
    new-instance v0, Lajg;

    iget-object v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aw:Z

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->K()Lbdh;

    move-result-object v6

    invoke-direct {v0, p0, v4, v5, v6}, Lajg;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Ljava/lang/String;ZLbdh;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ca:Lajg;

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ca:Lajg;

    iget-object v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->az:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aI:Les;

    invoke-virtual {v5}, Les;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lajg;->a(Ljava/lang/String;Ljava/util/Collection;)V

    goto/16 :goto_4

    .line 4336
    :cond_1b
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_5

    .line 4351
    :cond_1c
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Landroid/view/View;)V

    goto/16 :goto_7

    .line 4377
    :cond_1d
    add-int/lit8 v0, v0, -0x1

    .line 4378
    invoke-interface {v4}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v2

    if-eqz v2, :cond_1e

    if-gtz v0, :cond_9

    :cond_1e
    move v2, v1

    goto/16 :goto_8

    :cond_1f
    move v0, v1

    .line 4381
    goto/16 :goto_9

    :cond_20
    move v0, v1

    .line 4382
    goto/16 :goto_a

    :cond_21
    move v0, v3

    goto/16 :goto_6

    .line 4300
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2701
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a(Ljava/lang/CharSequence;)V

    .line 2702
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 6586
    return-void
.end method

.method public a(Ljava/lang/String;Lbdn;)V
    .locals 5

    .prologue
    .line 1498
    invoke-static {p1}, Lbdk;->b(Ljava/lang/String;)Lbdk;

    move-result-object v0

    .line 1499
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->Q()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    .line 1500
    invoke-virtual {v1}, Lyj;->c()Lbdk;

    move-result-object v1

    invoke-virtual {v1, v0}, Lbdk;->a(Lbdk;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->O()Z

    move-result v1

    if-nez v1, :cond_1

    iget v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 1502
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lbdk;)Lbdh;

    move-result-object v1

    .line 1503
    if-eqz v1, :cond_1

    .line 1504
    const-string v2, "Babel"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lbys;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1505
    const-string v2, "Babel"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Presence update for Participant: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v1, Lbdh;->e:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " Presence: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1506
    invoke-virtual {p2}, Lbdn;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1505
    invoke-static {v2, v1}, Lbys;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1510
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->be:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    invoke-virtual {v1, v0, p2}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->a(Lbdk;Lbdn;)V

    .line 1513
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g:Lbai;

    check-cast v0, Laad;

    invoke-virtual {v0}, Laad;->notifyDataSetChanged()V

    .line 1516
    const-string v0, "babel_enable_last_seen"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1518
    invoke-direct {p0, p2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lbdn;)V

    .line 1522
    :cond_1
    return-void
.end method

.method public a(Lyj;Landroid/widget/ArrayAdapter;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lyj;",
            "Landroid/widget/ArrayAdapter",
            "<",
            "Laxs;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 7785
    new-instance v0, Lahh;

    const-string v1, "Convert to invite (high)"

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-direct {v0, v1, v2, p1}, Lahh;-><init>(Ljava/lang/String;Ljava/lang/String;Lyj;)V

    invoke-virtual {p2, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 7788
    new-instance v0, Lahh;

    const-string v1, "Convert to invite (low)"

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-direct {v0, v1, v2, p1}, Lahh;-><init>(Ljava/lang/String;Ljava/lang/String;Lyj;)V

    invoke-virtual {p2, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 7791
    new-instance v0, Lagq;

    const-string v1, "Remove conversation"

    invoke-direct {v0, p0, v1}, Lagq;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 7807
    new-instance v0, Lagr;

    const-string v1, "Inspect conversation"

    invoke-direct {v0, p0, v1}, Lagr;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 7817
    new-instance v0, Lags;

    const-string v1, "Email conversation"

    invoke-direct {v0, p0, v1}, Lags;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 7832
    new-instance v0, Lagu;

    const-string v1, "Show SMS target"

    invoke-direct {v0, p0, v1}, Lagu;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 7856
    new-instance v0, Lagv;

    const-string v1, "Start Stress Test"

    invoke-direct {v0, p0, v1}, Lagv;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 7867
    new-instance v0, Lahi;

    const-string v1, "Debug contact info"

    invoke-direct {v0, p0, v1}, Lahi;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 7868
    return-void
.end method

.method public a(Z)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 3525
    sget-boolean v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i:Z

    if-eqz v0, :cond_0

    .line 3526
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UN FOCUS: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " isFocused(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->F()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3528
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->F()Z

    move-result v0

    if-nez v0, :cond_1

    .line 3565
    :goto_0
    return-void

    .line 3532
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->Z:Ljava/lang/Runnable;

    if-eqz v0, :cond_2

    .line 3533
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bg:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->Z:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 3534
    iput-object v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->Z:Ljava/lang/Runnable;

    .line 3537
    :cond_2
    iput-boolean v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bC:Z

    .line 3539
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aL()V

    .line 3541
    iput-object v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aE:Ljava/lang/String;

    .line 3543
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    if-eqz v0, :cond_3

    if-eqz p1, :cond_3

    .line 3544
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->e()V

    .line 3547
    :cond_3
    invoke-static {}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->d()V

    .line 3548
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-static {v0}, Lyt;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 3549
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bD:I

    if-eq v0, v4, :cond_4

    .line 3550
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-static {v0, v1, v4}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lyj;Ljava/lang/String;I)I

    .line 3553
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    const/4 v2, -0x1

    invoke-static {v0, v1, v3, v3, v2}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;Ljava/lang/String;ZZI)I

    .line 3556
    :cond_5
    new-instance v0, Lyh;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    .line 3557
    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->g()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ah:Lbwt;

    .line 3558
    invoke-virtual {v2}, Lbwt;->c()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bF:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bG:I

    iget-object v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bH:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bK:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lyh;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 3560
    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->c(Lyh;)V

    .line 3564
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bg:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cx:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 2183
    if-eqz p1, :cond_0

    .line 2184
    invoke-direct {p0, p2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->e(Ljava/lang/String;)V

    .line 2188
    :goto_0
    return-void

    .line 2186
    :cond_0
    invoke-direct {p0, p2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a([Lajk;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 7043
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aw:Z

    .line 7044
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->c:Lahp;

    .line 7045
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM()V

    .line 7047
    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a([Lajk;)V

    .line 7048
    array-length v3, p1

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, p1, v2

    .line 7049
    iget-boolean v5, v4, Lajk;->j:Z

    if-eqz v5, :cond_5

    .line 7050
    invoke-virtual {p0, v4}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lajk;)V

    .line 7057
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bd:Landroid/view/View;

    if-nez v2, :cond_3

    array-length v2, p1

    if-le v2, v1, :cond_3

    .line 7058
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->d()Lcom/google/android/apps/hangouts/phone/EsApplication;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/google/android/apps/hangouts/phone/EsApplication;->b(I)Z

    move-result v2

    if-nez v2, :cond_3

    .line 7063
    array-length v3, p1

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_1

    aget-object v4, p1, v2

    .line 7064
    iget-object v5, v4, Lajk;->b:Lbdh;

    if-eqz v5, :cond_6

    iget-object v4, v4, Lajk;->b:Lbdh;

    .line 7065
    invoke-virtual {v4}, Lbdh;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    move v0, v1

    .line 7070
    :cond_1
    if-eqz v0, :cond_3

    .line 7071
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v2, Lg;->ef:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 7072
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    sget v3, Lf;->gz:I

    .line 7073
    invoke-virtual {v2, v3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    sget v2, Lg;->hO:I

    .line 7074
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bd:Landroid/view/View;

    .line 7076
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bd:Landroid/view/View;

    sget v2, Lg;->hP:I

    .line 7077
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 7079
    if-eqz v0, :cond_2

    .line 7080
    sget v2, Lh;->nr:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 7084
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    .line 7083
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 7086
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bd:Landroid/view/View;

    new-instance v2, Lagl;

    invoke-direct {v2, p0}, Lagl;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 7099
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v0

    .line 7100
    if-eqz v0, :cond_4

    .line 7101
    invoke-virtual {v0}, Ly;->u_()V

    .line 7103
    :cond_4
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cb:Z

    .line 7104
    return-void

    .line 7048
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 7063
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public a(J)Z
    .locals 2

    .prologue
    .line 3865
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cz:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b(I)V
    .locals 18

    .prologue
    .line 3777
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v2

    .line 3778
    if-nez v2, :cond_0

    .line 3862
    :goto_0
    return-void

    .line 3781
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g:Lbai;

    check-cast v2, Laad;

    invoke-virtual {v2}, Laad;->a()Landroid/database/Cursor;

    move-result-object v10

    .line 3782
    move/from16 v0, p1

    invoke-interface {v10, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 3783
    const/4 v2, 0x0

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    .line 3784
    const/4 v2, 0x2

    const/16 v5, 0x1f

    .line 3786
    invoke-interface {v10, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    if-ne v2, v5, :cond_2

    const/4 v2, 0x1

    .line 3787
    :goto_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cz:Ljava/util/Map;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 3788
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cz:Ljava/util/Map;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v5, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3855
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cz:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    if-lez v3, :cond_7

    const/4 v3, 0x1

    :goto_2
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(ZZ)V

    .line 3856
    if-eqz v2, :cond_8

    .line 3857
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aR:Landroid/widget/Button;

    new-instance v3, Lagm;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v3, v0, v1}, Lagm;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;I)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aS:Landroid/widget/Button;

    new-instance v3, Lagn;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v3, v0, v1}, Lagn;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;I)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3861
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g:Lbai;

    check-cast v2, Laad;

    invoke-virtual {v2}, Laad;->notifyDataSetChanged()V

    goto :goto_0

    .line 3786
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 3790
    :cond_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cz:Ljava/util/Map;

    .line 3791
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    if-eqz v2, :cond_4

    const/4 v3, 0x3

    .line 3794
    :goto_4
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 3790
    invoke-interface {v5, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3800
    if-nez v2, :cond_5

    const/4 v3, 0x1

    .line 3805
    :goto_5
    if-eqz v3, :cond_1

    .line 3806
    const-string v3, "babel_select_adjacent_maximum_timedelta"

    const/16 v4, 0x3c

    invoke-static {v3, v4}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v3

    const v4, 0xf4240

    mul-int/2addr v3, v4

    int-to-long v11, v3

    .line 3809
    const-string v3, "babel_select_adjacent_maximum_count"

    const/16 v4, 0xa

    invoke-static {v3, v4}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;I)I

    move-result v13

    .line 3812
    move/from16 v0, p1

    invoke-interface {v10, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 3813
    const/4 v3, 0x6

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 3815
    const/4 v3, 0x0

    move-wide v4, v6

    .line 3816
    :goto_6
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 3817
    const/16 v8, 0x8

    invoke-interface {v10, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 3818
    const/4 v8, 0x7

    invoke-interface {v10, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 3819
    const/4 v8, 0x6

    invoke-interface {v10, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 3820
    const/16 v16, 0x1f

    move/from16 v0, v16

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 3822
    invoke-static {v14, v15}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(II)Z

    move-result v14

    if-eqz v14, :cond_6

    add-long/2addr v4, v11

    cmp-long v4, v8, v4

    if-gtz v4, :cond_6

    add-int/lit8 v3, v3, 0x1

    if-gt v3, v13, :cond_6

    const/4 v4, 0x2

    move/from16 v0, v16

    if-eq v0, v4, :cond_6

    .line 3826
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cz:Ljava/util/Map;

    const/4 v5, 0x0

    invoke-interface {v10, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    .line 3829
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    .line 3828
    invoke-interface {v4, v5, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-wide v4, v8

    .line 3831
    goto :goto_6

    .line 3791
    :cond_4
    const/4 v3, 0x7

    .line 3796
    invoke-interface {v10, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    goto :goto_4

    .line 3800
    :cond_5
    const/4 v3, 0x0

    goto :goto_5

    .line 3833
    :cond_6
    move/from16 v0, p1

    invoke-interface {v10, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 3835
    const/4 v3, 0x0

    .line 3836
    :goto_7
    invoke-interface {v10}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 3837
    const/16 v4, 0x8

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 3838
    const/4 v4, 0x7

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 3839
    const/4 v4, 0x6

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 3840
    const/16 v14, 0x1f

    invoke-interface {v10, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 3842
    invoke-static {v8, v9}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(II)Z

    move-result v8

    if-eqz v8, :cond_1

    sub-long/2addr v6, v11

    cmp-long v6, v4, v6

    if-ltz v6, :cond_1

    add-int/lit8 v3, v3, 0x1

    if-gt v3, v13, :cond_1

    const/4 v6, 0x2

    if-eq v14, v6, :cond_1

    .line 3846
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cz:Ljava/util/Map;

    const/4 v7, 0x0

    invoke-interface {v10, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 3849
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    .line 3848
    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-wide v6, v4

    .line 3851
    goto :goto_7

    .line 3855
    :cond_7
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 3859
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cz:Ljava/util/Map;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_8
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    const/4 v8, 0x0

    invoke-static {v3, v8}, Lf;->a(Ljava/lang/Integer;I)I

    move-result v3

    const/4 v8, 0x3

    if-ne v3, v8, :cond_9

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_9
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_8

    :cond_9
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_9

    :cond_a
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v7, v2, [Ljava/lang/Long;

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    invoke-static {v7}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v4, v2, [J

    const/4 v2, 0x0

    move v3, v2

    :goto_a
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_b

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-static {v2}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v8

    aput-wide v8, v4, v3

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_a

    :cond_b
    invoke-static {v4}, Ljava/util/Arrays;->sort([J)V

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/Long;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aR:Landroid/widget/Button;

    new-instance v5, Lagj;

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v7, v2}, Lagj;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;[Ljava/lang/Long;Landroid/app/Activity;)V

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aS:Landroid/widget/Button;

    new-instance v3, Lagk;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4}, Lagk;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;[J)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_3
.end method

.method public b(J)V
    .locals 4

    .prologue
    .line 6871
    const-wide/16 v0, 0x3e8

    div-long v0, p1, v0

    .line 6877
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v0, v2, v0

    .line 6878
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 6879
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cm:Landroid/widget/Chronometer;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Chronometer;->setVisibility(I)V

    .line 6890
    :goto_0
    return-void

    .line 6883
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cm:Landroid/widget/Chronometer;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/Chronometer;->setVisibility(I)V

    .line 6887
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long v0, v2, v0

    .line 6888
    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cm:Landroid/widget/Chronometer;

    invoke-virtual {v2, v0, v1}, Landroid/widget/Chronometer;->setBase(J)V

    .line 6889
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cm:Landroid/widget/Chronometer;

    invoke-virtual {v0}, Landroid/widget/Chronometer;->start()V

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 6594
    const-string v0, "conversation_error"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6595
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aJ()V

    .line 6597
    :cond_0
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 820
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aJ:Z

    return v0
.end method

.method public b(Lbdk;)Z
    .locals 1

    .prologue
    .line 6762
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    invoke-virtual {v0}, Lyj;->c()Lbdk;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbdk;->a(Lbdk;)Z

    move-result v0

    return v0
.end method

.method public c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 7989
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7990
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "@"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a(Ljava/lang/CharSequence;)V

    .line 7992
    :cond_0
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 815
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aG:Z

    return v0
.end method

.method public c(Lbdk;)Z
    .locals 1

    .prologue
    .line 6772
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->be:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->a(Lbdk;)Z

    move-result v0

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 6927
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ac:I

    return v0
.end method

.method public d(Lbdk;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 7879
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bl:Lyc;

    invoke-virtual {v0, p1}, Lyc;->d(Lbdk;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d(I)V
    .locals 0

    .prologue
    .line 8044
    iput p1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bN:I

    .line 8045
    return-void
.end method

.method public e(Lbdk;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 7883
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bl:Lyc;

    invoke-virtual {v0, p1}, Lyc;->b(Lbdk;)Lbdh;

    move-result-object v1

    .line 7884
    if-eqz v1, :cond_1

    .line 7885
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an:I

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lbdh;->a(Z)Ljava/lang/String;

    move-result-object v0

    .line 7888
    :goto_1
    return-object v0

    .line 7885
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 7888
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 804
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aH:Z

    return v0
.end method

.method public e_()V
    .locals 1

    .prologue
    .line 794
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bs:Lbdh;

    .line 795
    return-void
.end method

.method public f(Lbdk;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 7892
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bl:Lyc;

    invoke-virtual {v0, p1}, Lyc;->e(Lbdk;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 809
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aH:Z

    .line 810
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ak()V

    .line 811
    return-void
.end method

.method public f_()V
    .locals 0

    .prologue
    .line 1494
    return-void
.end method

.method public g()Lbdh;
    .locals 1

    .prologue
    .line 783
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bs:Lbdh;

    return-object v0
.end method

.method public g(Lbdk;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 7900
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bl:Lyc;

    invoke-virtual {v0, p1}, Lyc;->b(Lbdk;)Lbdh;

    move-result-object v0

    .line 7901
    if-eqz v0, :cond_0

    .line 7902
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbdh;->a(Z)Ljava/lang/String;

    move-result-object v0

    .line 7904
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 778
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->br:I

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 4174
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-super {p0}, Lakk;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()V
    .locals 2

    .prologue
    .line 2178
    const/16 v0, 0x3f

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(II)V

    .line 2179
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 4197
    invoke-super {p0, p1}, Lakk;->onActivityCreated(Landroid/os/Bundle;)V

    .line 4198
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->be:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    new-instance v1, Lahq;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lahq;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;B)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->a(Lcdw;)V

    .line 4199
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->B()V

    .line 4200
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1830
    packed-switch p1, :pswitch_data_0

    .line 1839
    new-instance v0, Lagt;

    invoke-direct {v0, p0, p1, p2, p3}, Lagt;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;IILandroid/content/Intent;)V

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Ljava/lang/Runnable;Z)V

    .line 1852
    :cond_0
    :goto_0
    return-void

    .line 1832
    :pswitch_0
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 1833
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cf:Lahm;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-interface {v0}, Lahm;->a()V

    goto :goto_0

    .line 1830
    :pswitch_data_0
    .packed-switch 0x7d0
        :pswitch_0
    .end packed-switch
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    const/4 v7, 0x1

    .line 1790
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    .line 1792
    sget v0, Lg;->aW:I

    if-eq v2, v0, :cond_0

    sget v0, Lg;->bn:I

    if-eq v2, v0, :cond_0

    sget v0, Lg;->cn:I

    if-eq v2, v0, :cond_0

    sget v0, Lg;->bi:I

    if-eq v2, v0, :cond_0

    sget v0, Lg;->gw:I

    if-ne v2, v0, :cond_2

    .line 1795
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v0

    .line 1796
    instance-of v3, v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    if-eqz v3, :cond_2

    .line 1797
    check-cast v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 1798
    iget-object v0, v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;->targetView:Landroid/view/View;

    check-cast v0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;

    .line 1800
    if-eqz v0, :cond_2

    .line 1801
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->b()Lcdg;

    move-result-object v3

    instance-of v3, v3, Lcom/google/android/apps/hangouts/views/MessageListItemView;

    if-eqz v3, :cond_2

    .line 1803
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->b()Lcdg;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/google/android/apps/hangouts/views/MessageListItemView;

    .line 1805
    sget v0, Lg;->aW:I

    if-ne v2, v0, :cond_3

    .line 1807
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v0

    const-string v1, "clipboard"

    .line 1808
    invoke-virtual {v0, v1}, Ly;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/ClipboardManager;

    .line 1809
    invoke-virtual {v5}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->d()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/ClipboardManager;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_0
    move v1, v7

    .line 1825
    :cond_2
    return v1

    .line 1810
    :cond_3
    sget v0, Lg;->bn:I

    if-ne v2, v0, :cond_4

    .line 1811
    invoke-virtual {v5}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->u()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lh;->gC:I

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->al:Landroid/app/AlertDialog;

    goto :goto_0

    .line 1812
    :cond_4
    sget v0, Lg;->cn:I

    if-ne v2, v0, :cond_9

    .line 1813
    invoke-virtual {v5}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->k()Lbvy;

    move-result-object v8

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v5}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->v()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbvx;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget v2, Lh;->cS:I

    new-array v3, v7, [Ljava/lang/Object;

    if-nez v0, :cond_5

    const-string v0, ""

    :cond_5
    aput-object v0, v3, v1

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->o()I

    move-result v2

    if-ne v2, v7, :cond_6

    move v1, v7

    :cond_6
    invoke-virtual {v5}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->q()Ljava/lang/String;

    move-result-object v2

    if-eqz v8, :cond_8

    iget-object v3, v8, Lbvy;->a:Ljava/lang/String;

    :goto_1
    if-eqz v8, :cond_7

    iget-object v4, v8, Lbvy;->b:Ljava/lang/String;

    :cond_7
    invoke-virtual {v5}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->n()I

    move-result v5

    invoke-static/range {v0 .. v6}, Lbbl;->a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_8
    move-object v3, v4

    goto :goto_1

    .line 1814
    :cond_9
    sget v0, Lg;->bi:I

    if-ne v2, v0, :cond_a

    .line 1815
    invoke-virtual {v5}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->w()J

    move-result-wide v2

    sget v0, Lh;->jR:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget v5, Lh;->jh:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget v6, Lh;->ab:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v0, v5, v6}, Labe;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Labe;

    move-result-object v0

    invoke-virtual {v0}, Labe;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "row_id"

    invoke-virtual {v4, v5, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    invoke-virtual {v0, v4}, Labe;->setArguments(Landroid/os/Bundle;)V

    invoke-virtual {v0, p0, v1}, Labe;->setTargetFragment(Lt;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getFragmentManager()Lae;

    move-result-object v1

    const-string v2, "delete_message"

    invoke-virtual {v0, v1, v2}, Labe;->a(Lae;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1816
    :cond_a
    sget v0, Lg;->gw:I

    if-ne v2, v0, :cond_1

    .line 1817
    invoke-direct {p0, v5}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lcom/google/android/apps/hangouts/views/MessageListItemView;)V

    goto/16 :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1561
    invoke-super {p0, p1}, Lakk;->onCreate(Landroid/os/Bundle;)V

    .line 1563
    sget-boolean v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->Y:Z

    if-nez v0, :cond_0

    .line 1564
    sput-boolean v2, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->Y:Z

    .line 1567
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bn:Lbza;

    .line 1568
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aJ:Z

    .line 1571
    invoke-static {v1}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->c(Z)V

    .line 1572
    invoke-virtual {p0, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->setHasOptionsMenu(Z)V

    .line 1573
    invoke-static {}, Lbzh;->b()Lbzh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbzh;->a(Lbzl;)V

    .line 1574
    return-void
.end method

.method public onCreateAnimation(IZI)Landroid/view/animation/Animation;
    .locals 2

    .prologue
    .line 1619
    if-eqz p3, :cond_1

    .line 1624
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v0

    invoke-static {v0, p3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1625
    if-eqz v0, :cond_0

    .line 1626
    new-instance v1, Lagi;

    invoke-direct {v1, p0, p2}, Lagi;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Z)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1646
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1661
    invoke-super {p0, p1, p2, p3}, Lakk;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 1662
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->h:Landroid/widget/AbsListView;

    if-ne p2, v0, :cond_8

    .line 1666
    instance-of v0, p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    if-eqz v0, :cond_c

    move-object v0, p3

    .line 1667
    check-cast v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 1668
    iget-object v0, v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;->targetView:Landroid/view/View;

    check-cast v0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;

    .line 1670
    if-eqz v0, :cond_c

    .line 1671
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->b()Lcdg;

    move-result-object v1

    instance-of v1, v1, Lcom/google/android/apps/hangouts/views/MessageListItemView;

    if-eqz v1, :cond_c

    .line 1673
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->b()Lcdg;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;

    .line 1674
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->o()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    move v1, v3

    .line 1680
    :goto_0
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->p()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_b

    move v4, v2

    .line 1684
    :goto_1
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->n()I

    move-result v0

    .line 1689
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v5

    invoke-virtual {v5}, Ly;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v5

    .line 1690
    sget v6, Lf;->ha:I

    invoke-virtual {v5, v6, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1692
    if-nez v1, :cond_2

    .line 1695
    if-eqz v4, :cond_0

    if-lez v0, :cond_1

    .line 1696
    :cond_0
    sget v0, Lg;->cn:I

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 1697
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1700
    :cond_1
    sget v0, Lg;->bi:I

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 1701
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1704
    :cond_2
    instance-of v0, p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    if-eqz v0, :cond_7

    .line 1705
    check-cast p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 1706
    iget-object v0, p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;->targetView:Landroid/view/View;

    check-cast v0, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;

    .line 1708
    if-eqz v0, :cond_7

    .line 1709
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->b()Lcdg;

    move-result-object v1

    instance-of v1, v1, Lcom/google/android/apps/hangouts/views/MessageListItemView;

    if-eqz v1, :cond_7

    .line 1711
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/MessageListItemWrapperView;->b()Lcdg;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;

    .line 1712
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->d()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1713
    sget v1, Lg;->aW:I

    invoke-interface {p1, v1}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1714
    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1716
    :cond_3
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->m()Z

    move-result v1

    if-nez v1, :cond_4

    .line 1717
    sget v1, Lg;->gw:I

    .line 1718
    invoke-interface {p1, v1}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1719
    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1724
    :cond_4
    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->d()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    const/16 v0, 0xf

    invoke-static {v1, v0}, Landroid/text/util/Linkify;->addLinks(Landroid/text/Spannable;I)Z

    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result v0

    const-class v4, Landroid/text/style/URLSpan;

    invoke-virtual {v1, v3, v0, v4}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a([Landroid/text/style/URLSpan;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_5
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v1, 0x0

    const-string v4, ":"

    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-ltz v4, :cond_a

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    :goto_4
    const-string v4, "mailto"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    move v0, v2

    move-object v4, v1

    :goto_5
    if-eqz v0, :cond_5

    invoke-static {v1}, Lbbl;->i(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    sget v1, Lh;->gd:I

    new-array v6, v2, [Ljava/lang/Object;

    aput-object v4, v6, v3

    invoke-virtual {p0, v1, v6}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/16 v4, 0x1b

    invoke-interface {p1, v3, v4, v3, v1}, Landroid/view/ContextMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_3

    :pswitch_0
    move v1, v2

    .line 1677
    goto/16 :goto_0

    .line 1724
    :cond_6
    const-string v4, "tel"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-static {v1}, Lbzd;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    move v0, v2

    goto :goto_5

    .line 1728
    :cond_7
    sget v0, Lh;->gn:I

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->setHeaderTitle(I)Landroid/view/ContextMenu;

    .line 1730
    :cond_8
    return-void

    :cond_9
    move v0, v3

    move-object v4, v1

    goto :goto_5

    :cond_a
    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_4

    :cond_b
    move v4, v3

    goto/16 :goto_1

    :cond_c
    move v0, v3

    move v4, v3

    move v1, v3

    goto/16 :goto_2

    .line 1674
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Ldg;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v11, 0x0

    const/4 v7, 0x0

    const/4 v10, 0x1

    .line 4225
    sget-boolean v0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i:Z

    if-eqz v0, :cond_0

    .line 4226
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onCreateLoader "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " conversationId "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4229
    :cond_0
    if-ne p1, v6, :cond_4

    .line 4230
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->O()Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_3

    .line 4231
    :cond_1
    new-instance v0, Lbaj;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->av:Landroid/net/Uri;

    sget-object v4, Laho;->a:[Ljava/lang/String;

    const-string v5, "type!=?"

    sget-object v6, Laho;->b:[Ljava/lang/String;

    const-string v7, "timestamp desc"

    invoke-direct/range {v0 .. v7}, Lbaj;-><init>(Landroid/content/Context;Lyj;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ct:Lbaj;

    .line 4278
    :goto_0
    sget-boolean v1, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->i:Z

    if-eqz v1, :cond_2

    .line 4279
    const-string v1, "Babel"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onCreateLoader: id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", loader="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 4281
    :cond_2
    return-object v0

    .line 4239
    :cond_3
    new-instance v0, Lbaj;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->av:Landroid/net/Uri;

    sget-object v4, Laho;->a:[Ljava/lang/String;

    const-string v5, "type=?"

    sget-object v6, Laho;->c:[Ljava/lang/String;

    const-string v7, "timestamp desc"

    invoke-direct/range {v0 .. v7}, Lbaj;-><init>(Landroid/content/Context;Lyj;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ct:Lbaj;

    goto :goto_0

    .line 4247
    :cond_4
    if-nez p1, :cond_5

    .line 4248
    new-instance v0, Lbaj;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    sget-object v3, Lcom/google/android/apps/hangouts/content/EsProvider;->e:Landroid/net/Uri;

    iget-object v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    .line 4250
    invoke-static {v3, v4}, Lcom/google/android/apps/hangouts/content/EsProvider;->a(Landroid/net/Uri;Lyj;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lahe;->a:[Ljava/lang/String;

    sget-object v5, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cC:Ljava/lang/String;

    new-array v6, v10, [Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    aput-object v8, v6, v11

    invoke-direct/range {v0 .. v7}, Lbaj;-><init>(Landroid/content/Context;Lyj;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 4254
    :cond_5
    if-ne p1, v10, :cond_6

    .line 4255
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aI:Les;

    .line 4256
    invoke-virtual {v0}, Les;->keySet()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aI:Les;

    .line 4257
    invoke-virtual {v1}, Les;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    .line 4256
    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 4255
    invoke-static {v0}, Lf;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 4259
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bl:Lyc;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    invoke-virtual {v1, v2, v0}, Lyc;->d(Lyj;Ljava/lang/String;)V

    .line 4260
    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ay:Ljava/lang/String;

    .line 4261
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bm:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 4263
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bl:Lyc;

    sget v1, Lyd;->b:I

    invoke-virtual {v0, v1}, Lyc;->a(I)Ldg;

    move-result-object v0

    goto/16 :goto_0

    .line 4264
    :cond_6
    const/4 v0, 0x3

    if-ne p1, v0, :cond_7

    .line 4265
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/content/EsProvider;->e(Lyj;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 4266
    new-instance v0, Lbaj;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    sget-object v4, Lahl;->a:[Ljava/lang/String;

    const-string v5, "timestamp>? AND type=?"

    new-array v6, v6, [Ljava/lang/String;

    iget-wide v8, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bu:J

    .line 4272
    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v11

    const/16 v8, 0x11

    .line 4273
    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v10

    invoke-direct/range {v0 .. v7}, Lbaj;-><init>(Landroid/content/Context;Lyj;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    move-object v0, v7

    goto/16 :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 1651
    iput-object p1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cg:Landroid/view/Menu;

    .line 1652
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->O()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1656
    :goto_0
    return-void

    .line 1655
    :cond_0
    sget v0, Lf;->gR:I

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/16 v10, 0xb

    const/16 v9, 0x8

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 3881
    sget v0, Lf;->eF:I

    invoke-virtual {p1, v0, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 3883
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lbme;

    invoke-static {v0, v1}, Lcyj;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbme;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aF:Lbme;

    .line 3885
    new-instance v0, Ladt;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v1

    invoke-direct {v0, v1}, Ladt;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cv:Ladt;

    .line 3886
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cv:Ladt;

    new-instance v1, Lafl;

    invoke-direct {v1, p0}, Lafl;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    invoke-virtual {v0, v1}, Ladt;->a(Ladv;)V

    .line 3897
    sget v0, Lg;->av:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    .line 3898
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ah:Lbwt;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a(Lbwt;)V

    .line 3900
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    sget v1, Lg;->eE:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bt:Landroid/widget/EditText;

    .line 3901
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->b()Lcom/google/android/apps/hangouts/views/TransportSpinner;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/google/android/apps/hangouts/views/TransportSpinner;->a(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    new-instance v1, Lafz;

    invoke-direct {v1, p0}, Lafz;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->a(Lcbl;)V

    .line 3902
    sget v0, Lg;->gj:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aQ:Landroid/view/View;

    .line 3903
    sget v0, Lg;->gh:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aR:Landroid/widget/Button;

    .line 3904
    sget v0, Lg;->bg:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aS:Landroid/widget/Button;

    .line 3905
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aQ:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 3907
    if-eqz p3, :cond_0

    .line 3908
    const-string v0, "conversation_params"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lahc;

    .line 3909
    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->b(Lahc;)V

    .line 3911
    const-string v0, "pending_attachment_content_type"

    .line 3912
    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bK:Ljava/lang/String;

    .line 3913
    const-string v0, "pending_attachment_sent_request_id"

    invoke-virtual {p3, v0, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bN:I

    .line 3915
    const-string v0, "camera_position"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/CameraPosition;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bM:Lcom/google/android/gms/maps/model/CameraPosition;

    .line 3917
    const-string v0, "marker_options"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/MarkerOptions;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bL:Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 3919
    const-string v0, "pending_photo_height"

    invoke-virtual {p3, v0, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bJ:I

    .line 3921
    const-string v0, "pending_photo_width"

    invoke-virtual {p3, v0, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bI:I

    .line 3923
    const-string v0, "share_intent_processed"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bE:Z

    .line 3926
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cj:Z

    if-eqz v0, :cond_1

    .line 3927
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-virtual {v0, v9}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->setVisibility(I)V

    .line 3930
    :cond_1
    sget v0, Lg;->fe:I

    .line 3931
    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->be:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    .line 3932
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->be:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->f()V

    .line 3934
    const v0, 0x102000a

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->h:Landroid/widget/AbsListView;

    .line 3936
    new-instance v0, Lalh;

    invoke-direct {v0, v6, p0}, Lalh;-><init>(Landroid/view/View;Lalj;)V

    .line 3937
    new-instance v1, Labu;

    invoke-direct {v1, v6, p0}, Labu;-><init>(Landroid/view/View;Labw;)V

    .line 3938
    new-instance v2, Lama;

    invoke-direct {v2, v6, p0}, Lama;-><init>(Landroid/view/View;Lamc;)V

    .line 3939
    new-instance v3, Labm;

    invoke-direct {v3, v6, p0}, Labm;-><init>(Landroid/view/View;Labo;)V

    .line 3940
    new-instance v4, Laaw;

    invoke-direct {v4, v6, p0}, Laaw;-><init>(Landroid/view/View;Laax;)V

    .line 3943
    const/4 v5, 0x5

    new-array v5, v5, [Lalw;

    aput-object v1, v5, v7

    aput-object v2, v5, v8

    const/4 v1, 0x2

    aput-object v0, v5, v1

    const/4 v0, 0x3

    aput-object v4, v5, v0

    const/4 v0, 0x4

    aput-object v3, v5, v0

    iput-object v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cr:[Lalw;

    .line 3946
    sget v0, Lg;->aw:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aT:Landroid/view/View;

    .line 3947
    sget v0, Lg;->ax:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aU:Landroid/widget/ImageView;

    .line 3948
    sget v0, Lg;->dA:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aV:Landroid/widget/ImageView;

    .line 3949
    sget v0, Lg;->he:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aW:Landroid/widget/ImageView;

    .line 3950
    sget v0, Lg;->fu:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aZ:Landroid/view/View;

    .line 3952
    sget v0, Lg;->ak:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bb:Landroid/view/View;

    .line 3953
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bb:Landroid/view/View;

    new-instance v1, Lafm;

    invoke-direct {v1, p0}, Lafm;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3960
    sget v0, Lg;->an:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Chronometer;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cm:Landroid/widget/Chronometer;

    .line 3961
    sget v0, Lg;->dg:I

    .line 3962
    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cn:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    .line 3963
    sget v0, Lg;->eU:I

    .line 3964
    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->co:Landroid/widget/TextView;

    .line 3965
    sget v0, Lg;->eT:I

    .line 3966
    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cp:Landroid/widget/TextView;

    .line 3968
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v10, :cond_2

    .line 3970
    sget v0, Lg;->eM:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    new-instance v1, Landroid/animation/LayoutTransition;

    invoke-direct {v1}, Landroid/animation/LayoutTransition;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    sget v0, Lg;->gi:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    new-instance v1, Landroid/animation/LayoutTransition;

    invoke-direct {v1}, Landroid/animation/LayoutTransition;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 3973
    :cond_2
    sget v0, Lf;->fO:I

    invoke-virtual {p1, v0, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 3974
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->h:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 3975
    sget v0, Lg;->ee:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aN:Landroid/view/View;

    .line 3976
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aI()V

    .line 3977
    new-instance v0, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->h:Landroid/widget/AbsListView;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bU:Lccw;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bg:Landroid/os/Handler;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;-><init>(Landroid/widget/AbsListView;Lccw;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ch:Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;

    .line 3979
    new-instance v0, Laad;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->h:Landroid/widget/AbsListView;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ch:Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;

    iget v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an:I

    .line 3980
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->O()Z

    move-result v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Laad;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Landroid/widget/AbsListView;Lcom/google/android/apps/hangouts/views/MessageListAnimationManager;IZ)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g:Lbai;

    .line 3981
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g:Lbai;

    check-cast v0, Laad;

    new-instance v1, Lafn;

    invoke-direct {v1, p0}, Lafn;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    invoke-virtual {v0, v1}, Laad;->a(Laaf;)V

    .line 3998
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->h:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g:Lbai;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 3999
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v10, :cond_4

    const-string v0, "babel_force_gb_copy_paste_textview"

    invoke-static {v0, v8}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_4

    .line 4002
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->h:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0, v7}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 4008
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->h:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    new-instance v1, Lafp;

    invoke-direct {v1, p0}, Lafp;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 4083
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->h:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    new-instance v1, Lafq;

    invoke-direct {v1, p0}, Lafq;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    .line 4091
    sget v0, Lg;->z:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/AudienceView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bY:Lcom/google/android/apps/hangouts/views/AudienceView;

    .line 4092
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bY:Lcom/google/android/apps/hangouts/views/AudienceView;

    invoke-virtual {v0, v8}, Lcom/google/android/apps/hangouts/views/AudienceView;->a(Z)V

    .line 4094
    sget v0, Lg;->hg:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aO:Landroid/view/View;

    .line 4095
    sget v0, Lg;->hf:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aP:Landroid/widget/EditText;

    .line 4096
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aP:Landroid/widget/EditText;

    new-array v1, v8, [Landroid/text/InputFilter;

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    .line 4097
    invoke-static {}, Lbvm;->a()Lsm;

    move-result-object v3

    invoke-virtual {v3}, Lsm;->t()I

    move-result v3

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v1, v7

    .line 4096
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 4099
    sget v0, Lg;->bj:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 4100
    new-instance v1, Lafr;

    invoke-direct {v1, p0}, Lafr;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 4107
    sget v0, Lg;->co:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aY:Landroid/view/View;

    .line 4110
    sget v0, Lg;->bE:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/EasterEggView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bh:Lcom/google/android/apps/hangouts/views/EasterEggView;

    .line 4112
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "babel_easter_eggs"

    invoke-static {v0, v1, v8}, Lcww;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_3

    .line 4115
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bh:Lcom/google/android/apps/hangouts/views/EasterEggView;

    invoke-virtual {v0, v9}, Lcom/google/android/apps/hangouts/views/EasterEggView;->setVisibility(I)V

    .line 4116
    iput-object v11, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bh:Lcom/google/android/apps/hangouts/views/EasterEggView;

    .line 4119
    :cond_3
    sget v0, Lg;->A:I

    .line 4120
    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bi:Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;

    .line 4122
    return-object v6

    .line 4005
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->h:Landroid/widget/AbsListView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->registerForContextMenu(Landroid/view/View;)V

    goto/16 :goto_0
.end method

.method public bridge synthetic onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;
    .locals 1

    .prologue
    .line 234
    invoke-super {p0, p1, p2, p3, p4}, Lakk;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 4204
    invoke-super {p0}, Lakk;->onDestroyView()V

    .line 4207
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    if-eqz v0, :cond_0

    .line 4208
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->f()V

    .line 4211
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ba:Lcom/google/android/apps/hangouts/views/SquareMapView;

    if-eqz v0, :cond_1

    .line 4212
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ba:Lcom/google/android/apps/hangouts/views/SquareMapView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/SquareMapView;->e()V

    .line 4215
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cv:Ladt;

    if-eqz v0, :cond_2

    .line 4216
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cv:Ladt;

    invoke-virtual {v0}, Ladt;->b()V

    .line 4219
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->be:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->a(Lcdw;)V

    .line 4220
    invoke-static {}, Lbzh;->b()Lbzh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbzh;->b(Lbzl;)V

    .line 4221
    return-void
.end method

.method public bridge synthetic onDetach()V
    .locals 0

    .prologue
    .line 234
    invoke-super {p0}, Lakk;->onDetach()V

    return-void
.end method

.method public synthetic onLoadFinished(Ldg;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 234
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Ldg;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Ldg;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldg",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 6520
    return-void
.end method

.method public onLowMemory()V
    .locals 1

    .prologue
    .line 3294
    invoke-super {p0}, Lakk;->onLowMemory()V

    .line 3296
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ba:Lcom/google/android/apps/hangouts/views/SquareMapView;

    if-eqz v0, :cond_0

    .line 3297
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ba:Lcom/google/android/apps/hangouts/views/SquareMapView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/SquareMapView;->f()V

    .line 3299
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 15

    .prologue
    .line 2310
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->O()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2311
    const/4 v0, 0x0

    .line 2437
    :goto_0
    return v0

    .line 2313
    :cond_0
    const/4 v14, 0x1

    .line 2314
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_1

    .line 2315
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v0

    invoke-static {v0}, Laz;->a(Landroid/app/Activity;)V

    move v0, v14

    goto :goto_0

    .line 2316
    :cond_1
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lg;->gO:I

    if-ne v0, v1, :cond_4

    .line 2317
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    if-eqz v0, :cond_19

    .line 2318
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aI:Les;

    invoke-virtual {v0}, Les;->keySet()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aI:Les;

    .line 2319
    invoke-virtual {v1}, Les;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    .line 2318
    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [Ljava/lang/String;

    .line 2322
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 2323
    const/4 v12, 0x0

    .line 2324
    const/4 v13, 0x0

    .line 2329
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ay:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bA:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an:I

    .line 2332
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->d()I

    move-result v4

    iget v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ao:I

    iget-wide v6, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bk:J

    iget-boolean v8, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ce:Z

    iget-object v9, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aA:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aB:Ljava/lang/String;

    .line 2330
    invoke-static/range {v0 .. v13}, Lbbl;->a(Lyj;Ljava/lang/String;Ljava/lang/String;IIIJZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2344
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v0

    invoke-virtual {v0}, Ly;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "share_intent"

    .line 2345
    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 2346
    if-eqz v0, :cond_2

    .line 2347
    const-string v2, "conversation_id"

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2348
    const-string v2, "share_intent"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2350
    :cond_2
    const/16 v0, 0x7d0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->startActivityForResult(Landroid/content/Intent;I)V

    move v0, v14

    .line 2351
    goto :goto_0

    .line 2326
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ad:Lajk;

    iget-object v0, v0, Lajk;->b:Lbdh;

    invoke-virtual {v0}, Lbdh;->b()Ljava/lang/String;

    move-result-object v12

    .line 2327
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ad:Lajk;

    iget-object v0, v0, Lajk;->b:Lbdh;

    invoke-virtual {v0}, Lbdh;->a()Ljava/lang/String;

    move-result-object v13

    goto :goto_1

    .line 2352
    :cond_4
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lg;->n:I

    if-ne v0, v1, :cond_5

    .line 2353
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ap()Ljava/lang/String;

    move-result-object v0

    .line 2356
    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 2357
    invoke-static {v0}, Lbbl;->i(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2358
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v1

    const/16 v2, 0x66

    invoke-virtual {v1, v0, v2}, Ly;->startActivityForResult(Landroid/content/Intent;I)V

    move v0, v14

    .line 2359
    goto/16 :goto_0

    :cond_5
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lg;->fN:I

    if-ne v0, v1, :cond_6

    .line 2360
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->M()V

    move v0, v14

    goto/16 :goto_0

    .line 2361
    :cond_6
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lg;->fV:I

    if-ne v0, v1, :cond_7

    .line 2362
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->f(I)V

    move v0, v14

    goto/16 :goto_0

    .line 2363
    :cond_7
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lg;->fW:I

    if-ne v0, v1, :cond_8

    .line 2364
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->f(I)V

    move v0, v14

    goto/16 :goto_0

    .line 2365
    :cond_8
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lg;->hd:I

    if-ne v0, v1, :cond_9

    .line 2366
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aF:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v1, 0x5ff

    .line 2367
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2366
    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    .line 2368
    const/16 v0, 0x3f

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(II)V

    move v0, v14

    goto/16 :goto_0

    .line 2369
    :cond_9
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lg;->fJ:I

    if-ne v0, v1, :cond_e

    .line 2370
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aF:Lbme;

    invoke-interface {v0}, Lbme;->a()Laux;

    move-result-object v0

    const/16 v1, 0x5fe

    .line 2371
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2370
    invoke-virtual {v0, v1}, Laux;->a(Ljava/lang/Integer;)V

    .line 2372
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am()Z

    move-result v2

    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cv:Ladt;

    invoke-virtual {v0}, Ladt;->a()Z

    move-result v0

    if-eqz v0, :cond_b

    const/4 v4, 0x1

    :goto_2
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aj()V

    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_c

    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->al()Z

    move-result v0

    if-eqz v0, :cond_c

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ao()Ljava/lang/Boolean;

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-ne v0, v1, :cond_a

    new-instance v0, Laed;

    invoke-direct {v0}, Laed;-><init>()V

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_a
    new-instance v0, Lagx;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lagx;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;ZLjava/util/ArrayList;ZLjava/util/ArrayList;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lahg;)V

    move v0, v14

    goto/16 :goto_0

    :cond_b
    const/4 v4, 0x0

    goto :goto_2

    :cond_c
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ao()Ljava/lang/Boolean;

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-ne v0, v1, :cond_d

    const/16 v0, 0x3f

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(II)V

    move v0, v14

    goto/16 :goto_0

    :cond_d
    const-string v0, "Babel"

    const-string v1, "can\'t place a call or Hangout in this conversation!"

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v14

    goto/16 :goto_0

    .line 2373
    :cond_e
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lg;->fS:I

    if-eq v0, v1, :cond_f

    .line 2374
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lg;->fI:I

    if-ne v0, v1, :cond_12

    .line 2375
    :cond_f
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    if-eqz v0, :cond_19

    .line 2376
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2377
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2378
    new-instance v1, Lagy;

    invoke-direct {v1, p0, v0, v3}, Lagy;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lahn;)V

    .line 2392
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    .line 2391
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 2393
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v4, v1, [J

    .line 2394
    const/4 v1, 0x0

    move v2, v1

    :goto_3
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v2, v1, :cond_10

    .line 2395
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-static {v1}, Lf;->a(Ljava/lang/Long;)J

    move-result-wide v5

    aput-wide v5, v4, v2

    .line 2394
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    .line 2398
    :cond_10
    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    .line 2402
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v3, Lg;->fI:I

    if-ne v1, v3, :cond_11

    const/4 v1, 0x1

    :goto_4
    const/4 v3, 0x0

    .line 2398
    invoke-static {v2, v0, v4, v1, v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lyj;[Ljava/lang/String;[JZZ)I

    .line 2409
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bg:Landroid/os/Handler;

    new-instance v1, Lagz;

    invoke-direct {v1, p0}, Lagz;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    move v0, v14

    .line 2415
    goto/16 :goto_0

    .line 2402
    :cond_11
    const/4 v1, 0x0

    goto :goto_4

    .line 2416
    :cond_12
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lg;->fK:I

    if-ne v0, v1, :cond_14

    .line 2417
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    if-eqz v0, :cond_13

    .line 2418
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    invoke-virtual {v0}, Lyj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->f(Ljava/lang/String;)V

    move v0, v14

    goto/16 :goto_0

    .line 2422
    :cond_13
    const-string v0, "Babel"

    const-string v1, "ConversationFragment delete failed because of null account."

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v14

    goto/16 :goto_0

    .line 2424
    :cond_14
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lg;->fO:I

    if-ne v0, v1, :cond_16

    .line 2425
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    if-eqz v0, :cond_15

    .line 2426
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->x()V

    move v0, v14

    goto/16 :goto_0

    .line 2428
    :cond_15
    const-string v0, "Babel"

    const-string v1, "ConversationFragment leave failed because of null account."

    invoke-static {v0, v1}, Lbys;->f(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v14

    goto/16 :goto_0

    .line 2430
    :cond_16
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lg;->gc:I

    if-ne v0, v1, :cond_17

    .line 2431
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aI:Les;

    invoke-virtual {v0}, Les;->keySet()Ljava/util/Set;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aI:Les;

    invoke-virtual {v2}, Les;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;[Ljava/lang/String;)V

    move v0, v14

    goto/16 :goto_0

    .line 2432
    :cond_17
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lg;->bd:I

    if-ne v0, v1, :cond_18

    .line 2433
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getActivity()Ly;

    move-result-object v0

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v2, Landroid/widget/ArrayAdapter;

    const v3, 0x1090003

    invoke-direct {v2, v0, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lyj;Landroid/widget/ArrayAdapter;)V

    new-instance v0, Laex;

    invoke-direct {v0, p0, v2}, Laex;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;Landroid/widget/ArrayAdapter;)V

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    move v0, v14

    goto/16 :goto_0

    .line 2435
    :cond_18
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_19
    move v0, v14

    goto/16 :goto_0
.end method

.method public onPause()V
    .locals 5

    .prologue
    .line 3661
    invoke-super {p0}, Lakk;->onPause()V

    .line 3664
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ba:Lcom/google/android/apps/hangouts/views/SquareMapView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ba:Lcom/google/android/apps/hangouts/views/SquareMapView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/SquareMapView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 3665
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ba:Lcom/google/android/apps/hangouts/views/SquareMapView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/SquareMapView;->d()V

    .line 3668
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bg:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ci:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bg:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bR:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bf:Lbyz;

    invoke-virtual {v0}, Lbyz;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdk;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Lbdk;)Lbdh;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->be:Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;

    const/4 v3, 0x4

    const/4 v4, 0x1

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/apps/hangouts/views/ParticipantsGalleryView;->a(Lbdh;IZ)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g:Lbai;

    check-cast v0, Laad;

    invoke-virtual {v0}, Laad;->c()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->a(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aI()V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/hangouts/views/MessageListItemView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/MessageListItemView;->h()V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cw:Laha;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cw:Laha;

    invoke-virtual {v0}, Laha;->c()V

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bh:Lcom/google/android/apps/hangouts/views/EasterEggView;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bh:Lcom/google/android/apps/hangouts/views/EasterEggView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/EasterEggView;->a()V

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bi:Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->d()V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bi:Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->e()V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bi:Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->b()V

    .line 3670
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aj()V

    .line 3671
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1856
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->R()Z

    move-result v3

    .line 1858
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->O()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1859
    const-string v0, "Babel"

    const-string v1, "ConversationFragment: bail out onPrepareOptionsMenu"

    invoke-static {v0, v1}, Lbys;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1988
    :cond_0
    :goto_0
    return-void

    .line 1862
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    .line 1863
    :goto_1
    if-nez v0, :cond_3

    .line 1864
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    if-nez v0, :cond_3

    .line 1865
    const-string v0, "Babel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "no account, conv id is: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbys;->h(Ljava/lang/String;Ljava/lang/String;)V

    .line 1866
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No account"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v0, v2

    .line 1862
    goto :goto_1

    .line 1872
    :cond_3
    sget v0, Lg;->n:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    .line 1873
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ap()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_13

    move v0, v1

    :goto_2
    invoke-interface {v4, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1875
    sget v0, Lg;->fP:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 1876
    if-eqz v0, :cond_4

    .line 1877
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1879
    :cond_4
    sget v0, Lg;->fT:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 1880
    if-eqz v0, :cond_5

    .line 1881
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1884
    :cond_5
    sget v0, Lg;->fV:I

    .line 1885
    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    .line 1886
    sget v0, Lg;->fW:I

    .line 1887
    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    .line 1888
    sget v0, Lg;->fN:I

    .line 1889
    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    .line 1891
    if-eqz v6, :cond_7

    .line 1892
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    .line 1893
    invoke-virtual {v0}, Lyj;->s()Z

    move-result v0

    if-eqz v0, :cond_14

    move v0, v1

    .line 1894
    :goto_3
    iget-boolean v7, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aK:Z

    if-eqz v7, :cond_15

    if-nez v0, :cond_15

    if-eqz v3, :cond_6

    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an:I

    if-eq v0, v12, :cond_15

    .line 1898
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->d()I

    move-result v0

    invoke-static {v0}, Lbvx;->a(I)Z

    move-result v0

    if-eqz v0, :cond_15

    :cond_6
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ce:Z

    if-nez v0, :cond_15

    move v0, v1

    .line 1900
    :goto_4
    invoke-interface {v6, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1901
    invoke-interface {v6, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1902
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an:I

    if-ne v0, v12, :cond_16

    sget v0, Lh;->jr:I

    :goto_5
    invoke-interface {v6, v0}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 1909
    :cond_7
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bV:I

    if-ne v0, v12, :cond_18

    if-nez v3, :cond_18

    move v0, v1

    .line 1912
    :goto_6
    iget v6, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bV:I

    if-ne v6, v1, :cond_19

    if-nez v3, :cond_19

    move v3, v1

    .line 1916
    :goto_7
    invoke-interface {v4, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1917
    invoke-interface {v5, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1919
    sget v0, Lg;->fI:I

    .line 1920
    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 1921
    sget v0, Lg;->fS:I

    .line 1922
    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    .line 1923
    sget v0, Lg;->fK:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    .line 1924
    sget v0, Lg;->fO:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    .line 1925
    sget v0, Lg;->gc:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    .line 1927
    if-eqz v3, :cond_8

    .line 1928
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cl:Z

    if-nez v0, :cond_1a

    move v0, v1

    :goto_8
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1930
    :cond_8
    if-eqz v4, :cond_9

    .line 1931
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cl:Z

    invoke-interface {v4, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1933
    :cond_9
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an:I

    if-ne v0, v12, :cond_1b

    .line 1934
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->S()Z

    move-result v0

    if-nez v0, :cond_1b

    move v3, v1

    .line 1935
    :goto_9
    if-eqz v5, :cond_a

    .line 1936
    if-nez v3, :cond_1c

    move v0, v1

    :goto_a
    invoke-interface {v5, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1938
    :cond_a
    if-eqz v6, :cond_b

    .line 1939
    invoke-interface {v6, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1942
    :cond_b
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->T()[Lajk;

    move-result-object v0

    .line 1943
    if-eqz v0, :cond_1d

    array-length v0, v0

    if-le v0, v1, :cond_1d

    move v0, v1

    .line 1945
    :goto_b
    if-eqz v7, :cond_c

    .line 1946
    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1949
    :cond_c
    sget v0, Lg;->hd:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 1950
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ao()Ljava/lang/Boolean;

    move-result-object v4

    .line 1951
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-ne v4, v0, :cond_1e

    move v0, v1

    :goto_c
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1952
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    if-eq v4, v0, :cond_1f

    move v0, v1

    :goto_d
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1954
    sget v0, Lg;->fJ:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    .line 1955
    if-eqz v5, :cond_f

    .line 1956
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-ne v4, v0, :cond_20

    move v0, v1

    .line 1957
    :goto_e
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am()Z

    move-result v4

    .line 1958
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an()Z

    move-result v6

    .line 1959
    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cv:Ladt;

    invoke-virtual {v3}, Ladt;->a()Z

    move-result v7

    .line 1960
    if-nez v4, :cond_d

    if-eqz v6, :cond_21

    if-eqz v7, :cond_21

    :cond_d
    move v3, v1

    .line 1962
    :goto_f
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->al()Z

    move-result v8

    .line 1963
    if-eqz v8, :cond_22

    if-eqz v3, :cond_22

    move v3, v1

    .line 1965
    :goto_10
    const-string v9, "Babel"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "hasHangoutsCallOption: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " supportsPhoneCallsOverVoip: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v10, " supportsPhoneCallsOverPstn: "

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " hasCellularService: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " hasParticipantPhoneNumber: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v9, v4}, Lbys;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1970
    if-nez v0, :cond_e

    if-eqz v3, :cond_23

    :cond_e
    move v0, v1

    :goto_11
    invoke-interface {v5, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1976
    :cond_f
    sget v0, Lg;->gO:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 1977
    if-eqz v0, :cond_12

    .line 1978
    iget-boolean v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cb:Z

    if-nez v3, :cond_10

    iget v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an:I

    if-ne v3, v12, :cond_11

    :cond_10
    move v2, v1

    .line 1980
    :cond_11
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1983
    :cond_12
    sget v0, Lg;->bd:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 1984
    if-eqz v0, :cond_0

    .line 1985
    invoke-static {}, Lf;->w()Z

    move-result v1

    .line 1986
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    :cond_13
    move v0, v2

    .line 1873
    goto/16 :goto_2

    :cond_14
    move v0, v2

    .line 1893
    goto/16 :goto_3

    :cond_15
    move v0, v2

    .line 1898
    goto/16 :goto_4

    .line 1902
    :cond_16
    if-eqz v3, :cond_17

    sget v0, Lh;->hr:I

    goto/16 :goto_5

    :cond_17
    sget v0, Lh;->jt:I

    goto/16 :goto_5

    :cond_18
    move v0, v2

    .line 1909
    goto/16 :goto_6

    :cond_19
    move v3, v2

    .line 1912
    goto/16 :goto_7

    :cond_1a
    move v0, v2

    .line 1928
    goto/16 :goto_8

    :cond_1b
    move v3, v2

    .line 1934
    goto/16 :goto_9

    :cond_1c
    move v0, v2

    .line 1936
    goto/16 :goto_a

    :cond_1d
    move v0, v2

    .line 1943
    goto/16 :goto_b

    :cond_1e
    move v0, v2

    .line 1951
    goto/16 :goto_c

    :cond_1f
    move v0, v2

    .line 1952
    goto/16 :goto_d

    :cond_20
    move v0, v2

    .line 1956
    goto/16 :goto_e

    :cond_21
    move v3, v2

    .line 1960
    goto/16 :goto_f

    :cond_22
    move v3, v2

    .line 1963
    goto/16 :goto_10

    :cond_23
    move v0, v2

    .line 1970
    goto :goto_11
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 3237
    invoke-super {p0}, Lakk;->onResume()V

    .line 3239
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ba:Lcom/google/android/apps/hangouts/views/SquareMapView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ba:Lcom/google/android/apps/hangouts/views/SquareMapView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/SquareMapView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 3240
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ba:Lcom/google/android/apps/hangouts/views/SquareMapView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/SquareMapView;->c()V

    .line 3243
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM()V

    .line 3249
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3250
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->at()V

    .line 3253
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bi:Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;

    new-instance v1, Lafa;

    invoke-direct {v1, p0}, Lafa;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->a(Lasd;)V

    .line 3265
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bi:Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;

    new-instance v1, Lafb;

    invoke-direct {v1, p0}, Lafb;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->a(Lasf;)V

    .line 3289
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bi:Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/hangout/ProximityCoverView;->a()V

    .line 3290
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 1578
    invoke-super {p0, p1}, Lakk;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1580
    new-instance v7, Lahc;

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    iget v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->an:I

    invoke-direct {v7, v0, v1, v2}, Lahc;-><init>(Ljava/lang/String;Lyj;I)V

    .line 1582
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aw:Z

    iput-boolean v0, v7, Lahc;->b:Z

    .line 1583
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bA:Ljava/lang/String;

    iput-object v0, v7, Lahc;->f:Ljava/lang/String;

    .line 1584
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bB:Ljava/lang/String;

    iput-object v0, v7, Lahc;->g:Ljava/lang/String;

    .line 1585
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bZ:Z

    iput-boolean v0, v7, Lahc;->h:Z

    .line 1586
    iget-wide v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bu:J

    iput-wide v0, v7, Lahc;->i:J

    .line 1587
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aA:Ljava/lang/String;

    iput-object v0, v7, Lahc;->j:Ljava/lang/String;

    .line 1588
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aB:Ljava/lang/String;

    iput-object v0, v7, Lahc;->k:Ljava/lang/String;

    .line 1589
    new-instance v0, Lyh;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    .line 1590
    invoke-virtual {v1}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->g()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ah:Lbwt;

    invoke-virtual {v2}, Lbwt;->c()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bF:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bG:I

    iget-object v5, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bH:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bK:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lyh;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    iput-object v0, v7, Lahc;->e:Lyh;

    .line 1593
    const-string v0, "conversation_params"

    invoke-virtual {p1, v0, v7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1594
    const-string v0, "pending_attachment_content_type"

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bK:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1595
    const-string v0, "pending_attachment_sent_request_id"

    iget v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bN:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1596
    const-string v0, "pending_photo_height"

    iget v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bJ:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1597
    const-string v0, "pending_photo_width"

    iget v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bI:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1598
    const-string v0, "share_intent_processed"

    iget-boolean v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bE:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1600
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bM:Lcom/google/android/gms/maps/model/CameraPosition;

    if-eqz v0, :cond_0

    .line 1601
    const-string v0, "camera_position"

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bM:Lcom/google/android/gms/maps/model/CameraPosition;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1603
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bL:Lcom/google/android/gms/maps/model/MarkerOptions;

    if-eqz v0, :cond_1

    .line 1604
    const-string v0, "marker_options"

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bL:Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1607
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ba:Lcom/google/android/apps/hangouts/views/SquareMapView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ba:Lcom/google/android/apps/hangouts/views/SquareMapView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/SquareMapView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 1610
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1611
    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ba:Lcom/google/android/apps/hangouts/views/SquareMapView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/hangouts/views/SquareMapView;->a(Landroid/os/Bundle;)V

    .line 1612
    const-string v1, "map_state"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1614
    :cond_2
    return-void
.end method

.method public bridge synthetic onScroll(Landroid/widget/AbsListView;III)V
    .locals 0

    .prologue
    .line 234
    invoke-super {p0, p1, p2, p3, p4}, Lakk;->onScroll(Landroid/widget/AbsListView;III)V

    return-void
.end method

.method public bridge synthetic onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 234
    invoke-super {p0, p1, p2}, Lakk;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 3322
    invoke-super {p0}, Lakk;->onStart()V

    .line 3323
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aq:Lbor;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->a(Lbor;)V

    .line 3326
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->g:Lbai;

    check-cast v0, Laad;

    invoke-virtual {v0}, Laad;->f()V

    .line 3327
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 3731
    invoke-super {p0}, Lakk;->onStop()V

    .line 3732
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aq:Lbor;

    invoke-static {v0}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->b(Lbor;)V

    .line 3733
    return-void
.end method

.method public q()J
    .locals 2

    .prologue
    .line 799
    iget-wide v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bq:J

    return-wide v0
.end method

.method public r()Laha;
    .locals 2

    .prologue
    .line 1059
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cw:Laha;

    if-nez v0, :cond_0

    .line 1060
    new-instance v0, Laha;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Laha;-><init>(Lcom/google/android/apps/hangouts/fragments/ConversationFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cw:Laha;

    .line 1062
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->cw:Laha;

    return-object v0
.end method

.method public s()Z
    .locals 1

    .prologue
    .line 1112
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ax:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public t()Z
    .locals 1

    .prologue
    .line 2027
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    invoke-virtual {v0}, Lyj;->aj()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public u()V
    .locals 4

    .prologue
    .line 2211
    const/16 v0, 0x1e

    iput v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ao:I

    .line 2216
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aI:Les;

    invoke-virtual {v0}, Les;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2217
    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->am:Lyj;

    iget v3, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ao:I

    invoke-static {v2, v0, v3}, Lcom/google/android/apps/hangouts/realtimechat/RealTimeChatService;->c(Lyj;Ljava/lang/String;I)I

    goto :goto_0

    .line 2220
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ak()V

    .line 2221
    return-void
.end method

.method public v()Z
    .locals 2

    .prologue
    .line 2225
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->ao:I

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public w()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2241
    iget v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->bo:I

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-string v3, "connectivity"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method public x()V
    .locals 4

    .prologue
    .line 2460
    sget v0, Lh;->jK:I

    .line 2461
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget v1, Lh;->jJ:I

    .line 2462
    invoke-virtual {p0, v1}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lh;->jI:I

    .line 2463
    invoke-virtual {p0, v2}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v3, Lh;->kt:I

    .line 2464
    invoke-virtual {p0, v3}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2460
    invoke-static {v0, v1, v2, v3}, Labe;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Labe;

    move-result-object v0

    .line 2465
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Labe;->setTargetFragment(Lt;I)V

    .line 2466
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->getFragmentManager()Lae;

    move-result-object v1

    const-string v2, "leave_conversation"

    invoke-virtual {v0, v1, v2}, Labe;->a(Lae;Ljava/lang/String;)V

    .line 2467
    return-void
.end method

.method public y()V
    .locals 1

    .prologue
    .line 2705
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->h()V

    .line 2706
    return-void
.end method

.method public z()V
    .locals 1

    .prologue
    .line 2709
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/ConversationFragment;->aM:Lcom/google/android/apps/hangouts/views/ComposeMessageView;

    invoke-virtual {v0}, Lcom/google/android/apps/hangouts/views/ComposeMessageView;->i()V

    .line 2710
    return-void
.end method
