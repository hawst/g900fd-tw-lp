.class public final Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;
.super Las;
.source "PG"

# interfaces
.implements Lcgn;


# instance fields
.field public Y:Lalt;

.field public Z:Lcwo;

.field private final aa:Lbme;

.field private ab:Landroid/support/v4/widget/DrawerLayout;

.field private ac:Lalv;

.field private ad:Le;

.field private ae:Lcwk;

.field private af:Lcgl;

.field private ag:Lcwk;

.field private ah:Ljava/lang/Runnable;

.field private ai:Z

.field public i:Lcwo;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0}, Las;-><init>()V

    .line 66
    invoke-static {}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lbme;

    .line 65
    invoke-static {v0, v1}, Lcyj;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbme;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->aa:Lbme;

    .line 495
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Lalv;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ac:Lalv;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;Z)Z
    .locals 0

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ai:Z

    return p1
.end method

.method private b()I
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->i:Lcwo;

    invoke-virtual {v0}, Lcwo;->getCount()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->Z:Lcwo;

    invoke-virtual {v1}, Lcwo;->getCount()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static synthetic b(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Lalt;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->Y:Lalt;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Lcwo;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->i:Lcwo;

    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 230
    const-string v0, "babel_enable_call_me_maybe"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/hangouts/phone/EsApplication;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 233
    new-instance v1, Lcuw;

    invoke-direct {v1}, Lcuw;-><init>()V

    .line 234
    invoke-virtual {v1, v0}, Lcuw;->a(Z)Lcuw;

    move-result-object v0

    .line 235
    sget-object v1, Lcve;->e:Lcut;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->af:Lcgl;

    invoke-virtual {v1, v2, v0}, Lcut;->a(Lcgl;Lcuw;)Lcgo;

    move-result-object v0

    new-instance v1, Lalo;

    invoke-direct {v1, p0}, Lalo;-><init>(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)V

    invoke-interface {v0, v1}, Lcgo;->a(Lcgr;)V

    .line 243
    return-void
.end method

.method public static synthetic d(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Lcwo;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->Z:Lcwo;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ai:Z

    return v0
.end method

.method public static synthetic f(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->c()V

    return-void
.end method

.method public static synthetic g(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Lbme;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->aa:Lbme;

    return-object v0
.end method

.method public static synthetic h(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ah:Ljava/lang/Runnable;

    return-object v0
.end method

.method public static synthetic i(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ah:Ljava/lang/Runnable;

    return-object v0
.end method

.method public static synthetic j(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)Le;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ad:Le;

    return-object v0
.end method

.method public static synthetic k(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)I
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->b()I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .prologue
    .line 148
    return-void
.end method

.method public final a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 153
    invoke-super/range {p0 .. p5}, Las;->a(Landroid/widget/ListView;Landroid/view/View;IJ)V

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ah:Ljava/lang/Runnable;

    invoke-static {v0}, Lcwz;->a(Ljava/lang/Object;)V

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ab:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->i(Landroid/view/View;)V

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->i:Lcwo;

    invoke-virtual {v0}, Lcwo;->getCount()I

    move-result v0

    if-ge p3, v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->Z:Lcwo;

    invoke-virtual {v0, v2}, Lcwo;->a(Lcwc;)V

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->i:Lcwo;

    invoke-virtual {v0, p3}, Lcwo;->b(I)V

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->i:Lcwo;

    invoke-virtual {v0, p3}, Lcwo;->a(I)Lcwc;

    move-result-object v0

    .line 161
    new-instance v1, Lalk;

    invoke-direct {v1, p0, v0}, Lalk;-><init>(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;Lcwc;)V

    iput-object v1, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ah:Ljava/lang/Runnable;

    .line 227
    :goto_0
    return-void

    .line 168
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->b()I

    move-result v0

    if-ge p3, v0, :cond_1

    .line 169
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->i:Lcwo;

    invoke-virtual {v0, v2}, Lcwo;->a(Lcwc;)V

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->Z:Lcwo;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->i:Lcwo;

    invoke-virtual {v1}, Lcwo;->getCount()I

    move-result v1

    sub-int v1, p3, v1

    invoke-virtual {v0, v1}, Lcwo;->b(I)V

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->Z:Lcwo;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->i:Lcwo;

    invoke-virtual {v1}, Lcwo;->getCount()I

    move-result v1

    sub-int v1, p3, v1

    invoke-virtual {v0, v1}, Lcwo;->a(I)Lcwc;

    move-result-object v0

    .line 173
    new-instance v1, Lall;

    invoke-direct {v1, p0, v0}, Lall;-><init>(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;Lcwc;)V

    iput-object v1, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ah:Ljava/lang/Runnable;

    goto :goto_0

    .line 182
    :cond_1
    long-to-int v0, p4

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 198
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ac:Lalv;

    invoke-interface {v0}, Lalv;->d()V

    goto :goto_0

    .line 184
    :pswitch_1
    new-instance v0, Lalm;

    invoke-direct {v0, p0}, Lalm;-><init>(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ah:Ljava/lang/Runnable;

    goto :goto_0

    .line 195
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ac:Lalv;

    invoke-interface {v0}, Lalv;->c()V

    goto :goto_0

    .line 201
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ac:Lalv;

    invoke-interface {v0}, Lalv;->q_()V

    goto :goto_0

    .line 204
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ac:Lalv;

    invoke-interface {v0}, Lalv;->h()V

    goto :goto_0

    .line 207
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ac:Lalv;

    invoke-interface {v0}, Lalv;->r_()V

    goto :goto_0

    .line 210
    :pswitch_6
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ac:Lalv;

    invoke-interface {v0}, Lalv;->i()V

    goto :goto_0

    .line 213
    :pswitch_7
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ac:Lalv;

    invoke-interface {v0}, Lalv;->s_()V

    goto :goto_0

    .line 218
    :pswitch_8
    new-instance v0, Laln;

    invoke-direct {v0, p0}, Laln;-><init>(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ah:Ljava/lang/Runnable;

    goto :goto_0

    .line 182
    nop

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_7
        :pswitch_6
        :pswitch_8
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->c()V

    .line 145
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 121
    invoke-super {p0, p1}, Las;->onActivityCreated(Landroid/os/Bundle;)V

    .line 123
    new-instance v0, Lcvg;

    invoke-direct {v0}, Lcvg;-><init>()V

    .line 124
    invoke-virtual {v0}, Lcvg;->a()Lcvg;

    move-result-object v0

    invoke-virtual {v0}, Lcvg;->b()Lcvf;

    move-result-object v0

    .line 125
    new-instance v1, Lcgm;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->getActivity()Ly;

    move-result-object v2

    invoke-direct {v1, v2}, Lcgm;-><init>(Landroid/content/Context;)V

    sget-object v2, Lcve;->c:Lcgd;

    invoke-virtual {v1, v2, v0}, Lcgm;->a(Lcgd;Lft;)Lcgm;

    move-result-object v0

    .line 126
    invoke-virtual {v0, p0}, Lcgm;->a(Lcgn;)Lcgm;

    move-result-object v0

    invoke-virtual {v0}, Lcgm;->a()Lcgl;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->af:Lcgl;

    .line 128
    new-instance v0, Lcwk;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->getActivity()Ly;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->af:Lcgl;

    invoke-direct {v0, v1, v2}, Lcwk;-><init>(Landroid/content/Context;Lcgl;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ae:Lcwk;

    .line 129
    new-instance v0, Lcwk;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->getActivity()Ly;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->af:Lcgl;

    invoke-direct {v0, v1, v2}, Lcwk;-><init>(Landroid/content/Context;Lcgl;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ag:Lcwk;

    .line 130
    new-instance v0, Lcwo;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->getActivity()Ly;

    move-result-object v1

    sget v2, Lf;->dY:I

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ae:Lcwk;

    new-instance v4, Lcwr;

    invoke-direct {v4, v6}, Lcwr;-><init>(B)V

    new-instance v5, Lalr;

    invoke-direct {v5, p0, v6}, Lalr;-><init>(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;B)V

    invoke-direct/range {v0 .. v5}, Lcwo;-><init>(Landroid/content/Context;ILcwk;Lcwr;Lcwp;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->i:Lcwo;

    .line 132
    new-instance v0, Lcwo;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->getActivity()Ly;

    move-result-object v1

    sget v2, Lf;->dY:I

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ag:Lcwk;

    new-instance v4, Lcwr;

    invoke-direct {v4, v6}, Lcwr;-><init>(B)V

    new-instance v5, Lalr;

    invoke-direct {v5, p0, v6}, Lalr;-><init>(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;B)V

    invoke-direct/range {v0 .. v5}, Lcwo;-><init>(Landroid/content/Context;ILcwk;Lcwr;Lcwp;)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->Z:Lcwo;

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->Z:Lcwo;

    invoke-virtual {v0}, Lcwo;->a()V

    .line 137
    new-instance v0, Lalt;

    invoke-direct {v0, p0, v6}, Lalt;-><init>(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->Y:Lalt;

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->Y:Lalt;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->a(Landroid/widget/ListAdapter;)V

    .line 139
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->a()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 140
    return-void
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 4

    .prologue
    .line 408
    invoke-super {p0, p1}, Las;->onAttach(Landroid/app/Activity;)V

    .line 410
    :try_start_0
    move-object v0, p1

    check-cast v0, Lalv;

    move-object v1, v0

    iput-object v1, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ac:Lalv;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 414
    return-void

    .line 412
    :catch_0
    move-exception v1

    new-instance v1, Ljava/lang/ClassCastException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 413
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " must implement NavigationDrawerListener."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 419
    invoke-super {p0, p1}, Las;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 420
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ad:Le;

    invoke-virtual {v0}, Le;->b()V

    .line 421
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 110
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->getActivity()Ly;

    move-result-object v0

    check-cast v0, Lkj;

    invoke-virtual {v0}, Lkj;->g()Lkd;

    move-result-object v0

    .line 111
    invoke-virtual {v0, v1}, Lkd;->a(Z)V

    .line 112
    invoke-virtual {v0, v1}, Lkd;->c(Z)V

    .line 113
    invoke-virtual {p0, v1}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->setHasOptionsMenu(Z)V

    .line 114
    invoke-super {p0, p1, p2, p3}, Las;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 115
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 116
    return-object v0
.end method

.method public final onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 375
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ae:Lcwk;

    invoke-virtual {v0}, Lcwk;->a()V

    .line 376
    iput-object v1, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ae:Lcwk;

    .line 377
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->i:Lcwo;

    invoke-virtual {v0, v1}, Lcwo;->a(Ljava/lang/Iterable;)V

    .line 378
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ag:Lcwk;

    invoke-virtual {v0}, Lcwk;->a()V

    .line 379
    iput-object v1, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ag:Lcwk;

    .line 380
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->Z:Lcwo;

    invoke-virtual {v0, v1}, Lcwo;->a(Ljava/lang/Iterable;)V

    .line 382
    invoke-super {p0}, Las;->onDestroy()V

    .line 383
    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 425
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ad:Le;

    invoke-virtual {v0, p1}, Le;->a(Landroid/view/MenuItem;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Las;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onPause()V
    .locals 2

    .prologue
    .line 402
    invoke-super {p0}, Las;->onPause()V

    .line 403
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ab:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->i(Landroid/view/View;)V

    .line 404
    return-void
.end method

.method public final onResume()V
    .locals 2

    .prologue
    .line 387
    invoke-super {p0}, Las;->onResume()V

    .line 389
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->c()V

    .line 392
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ab:Landroid/support/v4/widget/DrawerLayout;

    new-instance v1, Lalq;

    invoke-direct {v1, p0}, Lalq;-><init>(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->post(Ljava/lang/Runnable;)Z

    .line 398
    return-void
.end method

.method public final onStart()V
    .locals 7

    .prologue
    .line 297
    invoke-super {p0}, Las;->onStart()V

    .line 299
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->getActivity()Ly;

    move-result-object v0

    sget v1, Lg;->bC:I

    invoke-virtual {v0, v1}, Ly;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ab:Landroid/support/v4/widget/DrawerLayout;

    .line 300
    new-instance v0, Lalp;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->getActivity()Ly;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ab:Landroid/support/v4/widget/DrawerLayout;

    sget v4, Lcom/google/android/apps/hangouts/R$drawable;->bQ:I

    sget v5, Lh;->ho:I

    sget v6, Lh;->hn:I

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lalp;-><init>(Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;III)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ad:Le;

    .line 348
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ab:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ad:Le;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->a(Ljk;)V

    .line 350
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->af:Lcgl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->af:Lcgl;

    invoke-interface {v0}, Lcgl;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->af:Lcgl;

    invoke-interface {v0}, Lcgl;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 351
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->af:Lcgl;

    invoke-interface {v0}, Lcgl;->a()V

    .line 352
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->c()V

    .line 355
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->getActivity()Ly;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "navigation_drawer_shown"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "navigation_drawer_shown"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->ab:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->h(Landroid/view/View;)V

    .line 356
    :cond_1
    return-void
.end method

.method public final onStop()V
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->af:Lcgl;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->af:Lcgl;

    invoke-interface {v0}, Lcgl;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->af:Lcgl;

    invoke-interface {v0}, Lcgl;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 290
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/fragments/NavigationDrawerFragment;->af:Lcgl;

    invoke-interface {v0}, Lcgl;->b()V

    .line 292
    :cond_1
    invoke-super {p0}, Las;->onStop()V

    .line 293
    return-void
.end method
