.class public Lcom/google/android/apps/hangouts/phone/AccountSelectionActivity;
.super Lkj;
.source "PG"

# interfaces
.implements Labd;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lkj;-><init>()V

    return-void
.end method


# virtual methods
.method public a_(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/AccountSelectionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "intent"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 30
    const-string v1, "account_name"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 31
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/phone/AccountSelectionActivity;->startActivity(Landroid/content/Intent;)V

    .line 32
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/AccountSelectionActivity;->finish()V

    .line 33
    return-void
.end method

.method public n_()V
    .locals 0

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/AccountSelectionActivity;->finish()V

    .line 37
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 19
    invoke-super {p0, p1}, Lkj;->onCreate(Landroid/os/Bundle;)V

    .line 20
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/phone/AccountSelectionActivity;->e()Lae;

    move-result-object v1

    invoke-virtual {v1}, Lae;->a()Lao;

    move-result-object v8

    .line 21
    sget v1, Lh;->g:I

    sget v2, Lh;->f:I

    const/4 v3, -0x1

    move v4, v0

    move v5, v0

    move v6, v0

    move v7, v0

    invoke-static/range {v0 .. v7}, Laay;->a(ZIIIZZZZ)Laay;

    move-result-object v0

    .line 24
    const/4 v1, 0x0

    invoke-virtual {v8, v0, v1}, Lao;->a(Lt;Ljava/lang/String;)Lao;

    .line 25
    invoke-virtual {v8}, Lao;->b()I

    .line 26
    return-void
.end method
