.class public Lcom/google/android/apps/hangouts/views/AvatarView;
.super Landroid/view/View;
.source "PG"

# interfaces
.implements Laac;
.implements Lbri;
.implements Lbrj;


# static fields
.field public static final a:Z


# instance fields
.field private final b:I

.field private c:Ljava/lang/String;

.field private d:Lyb;

.field private e:Lzx;

.field private f:Landroid/graphics/Bitmap;

.field private g:Lbzn;

.field private h:Landroid/graphics/Bitmap;

.field private final i:Landroid/graphics/Rect;

.field private final j:Landroid/graphics/Rect;

.field private k:I

.field private l:I

.field private m:Z

.field private final n:Landroid/graphics/Paint;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lbys;->s:Lcyp;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/hangouts/views/AvatarView;->a:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/hangouts/views/AvatarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/hangouts/views/AvatarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 87
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 59
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->i:Landroid/graphics/Rect;

    .line 60
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->j:Landroid/graphics/Rect;

    .line 62
    iput v2, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->k:I

    .line 63
    iput v1, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->l:I

    .line 64
    iput-boolean v1, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->m:Z

    .line 89
    if-eqz p2, :cond_9

    .line 90
    sget-object v0, Lwj;->j:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 92
    invoke-virtual {v4, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 93
    if-nez v0, :cond_0

    .line 94
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Missing \'size\' attribute"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 97
    :cond_0
    const-string v5, "tiny"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    move v0, v1

    :goto_0
    iput v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->b:I

    .line 99
    invoke-virtual {v4, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 100
    if-nez v0, :cond_6

    move-object v0, p0

    .line 103
    :goto_1
    iput v1, v0, Lcom/google/android/apps/hangouts/views/AvatarView;->k:I

    .line 106
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    .line 111
    :goto_2
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->n:Landroid/graphics/Paint;

    .line 112
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/AvatarView;->b()V

    .line 113
    return-void

    .line 97
    :cond_1
    const-string v5, "small"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    const-string v5, "medium"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v0, v3

    goto :goto_0

    :cond_3
    const-string v5, "large"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v0, 0x3

    goto :goto_0

    :cond_4
    const-string v5, "xlarge"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    const/4 v0, 0x4

    goto :goto_0

    :cond_5
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid avatar size: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 103
    :cond_6
    const-string v5, "square"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    move v1, v2

    move-object v0, p0

    goto :goto_1

    :cond_7
    const-string v2, "round"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    move-object v0, p0

    goto :goto_1

    :cond_8
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid avatar shape: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 108
    :cond_9
    iput v3, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->b:I

    .line 109
    iput v1, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->k:I

    goto :goto_2
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 116
    const/4 v0, 0x0

    .line 118
    iget v2, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->l:I

    if-nez v2, :cond_3

    .line 119
    iget v2, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->b:I

    packed-switch v2, :pswitch_data_0

    .line 172
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->f:Landroid/graphics/Bitmap;

    if-eq v1, v0, :cond_0

    .line 173
    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->f:Landroid/graphics/Bitmap;

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->g:Lbzn;

    if-nez v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->f:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/AvatarView;->a(Landroid/graphics/Bitmap;)V

    .line 179
    :cond_0
    return-void

    .line 123
    :pswitch_0
    iget v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->k:I

    if-ne v0, v1, :cond_1

    .line 124
    invoke-static {}, Lyn;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 126
    :cond_1
    invoke-static {}, Lyn;->c()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 132
    :pswitch_1
    iget v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->k:I

    if-ne v0, v1, :cond_2

    .line 133
    invoke-static {}, Lyn;->i()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 135
    :cond_2
    invoke-static {}, Lyn;->h()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 139
    :cond_3
    iget v2, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->l:I

    if-ne v2, v1, :cond_4

    .line 140
    invoke-static {}, Lyn;->p()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 141
    :cond_4
    iget v2, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->l:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_7

    .line 142
    iget v2, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->b:I

    packed-switch v2, :pswitch_data_1

    goto :goto_0

    .line 146
    :pswitch_2
    iget v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->k:I

    if-ne v0, v1, :cond_5

    .line 147
    invoke-static {}, Lyn;->m()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 149
    :cond_5
    invoke-static {}, Lyn;->l()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 155
    :pswitch_3
    iget v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->k:I

    if-ne v0, v1, :cond_6

    .line 156
    invoke-static {}, Lyn;->o()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 158
    :cond_6
    invoke-static {}, Lyn;->n()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 163
    :cond_7
    iget v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->l:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_8

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 164
    iget v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->k:I

    if-ne v0, v1, :cond_9

    .line 165
    invoke-static {}, Lyn;->k()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 163
    :cond_8
    const/4 v0, 0x0

    goto :goto_1

    .line 167
    :cond_9
    invoke-static {}, Lyn;->j()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 119
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 142
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private c()V
    .locals 10

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    .line 345
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/AvatarView;->getMeasuredHeight()I

    move-result v0

    .line 346
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/AvatarView;->getMeasuredWidth()I

    move-result v1

    .line 350
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 367
    :cond_0
    :goto_0
    return-void

    .line 354
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 355
    iget-object v3, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    .line 357
    if-le v1, v0, :cond_2

    .line 358
    int-to-float v4, v2

    int-to-float v5, v0

    int-to-float v6, v1

    div-float/2addr v5, v6

    sub-float v5, v8, v5

    mul-float/2addr v4, v5

    div-float/2addr v4, v9

    float-to-int v4, v4

    .line 359
    sub-int/2addr v2, v4

    .line 360
    iget-object v5, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->j:Landroid/graphics/Rect;

    invoke-virtual {v5, v7, v4, v3, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 366
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->i:Landroid/graphics/Rect;

    invoke-virtual {v2, v7, v7, v1, v0}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0

    .line 362
    :cond_2
    int-to-float v4, v3

    int-to-float v5, v1

    int-to-float v6, v0

    div-float/2addr v5, v6

    sub-float v5, v8, v5

    mul-float/2addr v4, v5

    div-float/2addr v4, v9

    float-to-int v4, v4

    .line 363
    sub-int/2addr v3, v4

    .line 364
    iget-object v5, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->j:Landroid/graphics/Rect;

    invoke-virtual {v5, v4, v7, v3, v2}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_1
.end method

.method private d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 438
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->e:Lzx;

    if-eqz v0, :cond_0

    .line 439
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->e:Lzx;

    invoke-virtual {v0}, Lzx;->b()V

    .line 440
    iput-object v1, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->e:Lzx;

    .line 442
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->d:Lyb;

    if-eqz v0, :cond_1

    .line 443
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->d:Lyb;

    invoke-virtual {v0}, Lyb;->b()V

    .line 444
    iput-object v1, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->d:Lyb;

    .line 446
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->f:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/AvatarView;->a(Landroid/graphics/Bitmap;)V

    .line 447
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->g:Lbzn;

    if-eqz v0, :cond_2

    .line 448
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->g:Lbzn;

    invoke-virtual {v0}, Lbzn;->b()V

    .line 449
    iput-object v1, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->g:Lbzn;

    .line 451
    :cond_2
    iput-object v1, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->c:Ljava/lang/String;

    .line 452
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->m:Z

    .line 453
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 262
    iget-boolean v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->m:Z

    if-eqz v0, :cond_0

    .line 263
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->m:Z

    .line 264
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->e:Lzx;

    if-eqz v0, :cond_0

    invoke-static {}, Lbsn;->c()Lbsn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->e:Lzx;

    invoke-virtual {v0, v1}, Lbsn;->a(Lbrv;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->e:Lzx;

    .line 269
    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 182
    iput p1, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->l:I

    .line 183
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/AvatarView;->b()V

    .line 184
    return-void
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->h:Landroid/graphics/Bitmap;

    if-eq v0, p1, :cond_0

    .line 289
    iput-object p1, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->h:Landroid/graphics/Bitmap;

    .line 290
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/AvatarView;->c()V

    .line 291
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/AvatarView;->invalidate()V

    .line 293
    :cond_0
    return-void
.end method

.method public a(Lbdk;Lyj;)V
    .locals 1

    .prologue
    .line 276
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/AvatarView;->d()V

    .line 277
    invoke-static {p1, p2, p0}, Lbrf;->a(Lbdk;Lyj;Lbri;)Lyb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->d:Lyb;

    .line 278
    return-void
.end method

.method public a(Lbzn;Lbyd;ZLzx;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 312
    invoke-static {p2}, Lcwz;->a(Ljava/lang/Object;)V

    .line 313
    sget-boolean v0, Lcom/google/android/apps/hangouts/views/AvatarView;->a:Z

    if-eqz v0, :cond_0

    .line 314
    const-string v2, "Babel_medialoader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "AvatarView: setImageBitmap "

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p1, :cond_2

    move-object v0, v1

    .line 315
    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "gifImage="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez p2, :cond_3

    move-object v0, v1

    .line 316
    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " success="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " loadedFromCache="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 314
    invoke-static {v2, v0}, Lbys;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->e:Lzx;

    if-eq v0, p4, :cond_4

    .line 322
    if-eqz p1, :cond_1

    .line 323
    invoke-virtual {p1}, Lbzn;->b()V

    .line 338
    :cond_1
    :goto_2
    return-void

    .line 315
    :cond_2
    invoke-virtual {p1}, Lbzn;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 316
    :cond_3
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 329
    :cond_4
    iput-object v1, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->e:Lzx;

    .line 331
    if-eqz p3, :cond_1

    if-eqz p1, :cond_1

    .line 334
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->g:Lbzn;

    invoke-static {v0}, Lcwz;->a(Ljava/lang/Object;)V

    .line 335
    iput-object p1, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->g:Lbzn;

    .line 336
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->g:Lbzn;

    invoke-virtual {v0}, Lbzn;->e()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/AvatarView;->a(Landroid/graphics/Bitmap;)V

    goto :goto_2
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;ILaan;Lyj;)V
    .locals 6

    .prologue
    .line 426
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/hangouts/views/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;Lyb;Ljava/lang/String;Lyj;)V

    .line 427
    const/4 v0, 0x1

    if-ne p3, v0, :cond_0

    .line 428
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/views/AvatarView;->a(I)V

    .line 430
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lyb;Ljava/lang/String;Lyj;)V
    .locals 1

    .prologue
    .line 419
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->d:Lyb;

    .line 420
    invoke-virtual {p0, p2, p5}, Lcom/google/android/apps/hangouts/views/AvatarView;->a(Ljava/lang/String;Lyj;)V

    .line 421
    return-void
.end method

.method public a(Ljava/lang/String;Lyj;)V
    .locals 1

    .prologue
    .line 220
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/apps/hangouts/views/AvatarView;->a(Ljava/lang/String;Lyj;Z)V

    .line 221
    return-void
.end method

.method public a(Ljava/lang/String;Lyj;Z)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 232
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 233
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/AvatarView;->d()V

    .line 256
    :cond_0
    :goto_0
    return-void

    .line 236
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->c:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 240
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/AvatarView;->d()V

    .line 241
    iput-object p1, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->c:Ljava/lang/String;

    .line 242
    iput-boolean p3, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->m:Z

    .line 243
    new-instance v3, Lzx;

    new-instance v4, Lbyq;

    invoke-direct {v4, p1, p2}, Lbyq;-><init>(Ljava/lang/String;Lyj;)V

    .line 244
    iget v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->b:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "Invalid avatar size"

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    move v0, v1

    :goto_1
    invoke-virtual {v4, v0}, Lbyq;->a(I)Lbyq;

    move-result-object v0

    .line 245
    invoke-virtual {v0, v2}, Lbyq;->d(Z)Lbyq;

    move-result-object v0

    iget v4, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->k:I

    if-nez v4, :cond_2

    move v1, v2

    :cond_2
    invoke-virtual {v0, v1}, Lbyq;->b(Z)Lbyq;

    move-result-object v0

    invoke-direct {v3, v0, p0, v2, v5}, Lzx;-><init>(Lbyq;Laac;ZLjava/lang/Object;)V

    iput-object v3, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->e:Lzx;

    .line 249
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->e:Lzx;

    if-eqz v0, :cond_0

    .line 250
    invoke-static {}, Lbsn;->c()Lbsn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->e:Lzx;

    invoke-virtual {v0, v1, p3}, Lbsn;->a(Lbrv;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252
    iput-object v5, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->e:Lzx;

    goto :goto_0

    .line 244
    :pswitch_0
    invoke-static {}, Lyn;->b()I

    move-result v0

    goto :goto_1

    :pswitch_1
    invoke-static {}, Lyn;->f()I

    move-result v0

    goto :goto_1

    :pswitch_2
    invoke-static {}, Lyn;->g()I

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Ljava/lang/String;ZLyj;)V
    .locals 1

    .prologue
    .line 282
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/AvatarView;->d()V

    .line 283
    invoke-static {p1, p2, p3, p0}, Lbrf;->a(Ljava/lang/String;ZLyj;Lbrj;)Lyb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->d:Lyb;

    .line 285
    return-void
.end method

.method public a(Lyb;)V
    .locals 1

    .prologue
    .line 434
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->d:Lyb;

    .line 435
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 208
    if-eqz p1, :cond_0

    .line 209
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->n:Landroid/graphics/Paint;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 213
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/views/AvatarView;->invalidate()V

    .line 214
    return-void

    .line 211
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->n:Landroid/graphics/Paint;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 460
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/AvatarView;->d()V

    .line 461
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 462
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 371
    sget-boolean v0, Lcom/google/android/apps/hangouts/views/AvatarView;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 373
    invoke-static {}, Lbyr;->a()Lbxs;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->h:Landroid/graphics/Bitmap;

    iget-object v0, v0, Lbxs;->b:Lbxu;

    iget-object v0, v0, Lbxu;->d:Lex;

    invoke-virtual {v0, v1}, Lex;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Exception;

    .line 374
    const-string v1, "Babel"

    const-string v2, "Attempting to draw with a recycled bitmap"

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v1, v2, v3}, Lbys;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 377
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->h:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->j:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->i:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/hangouts/views/AvatarView;->n:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 378
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 300
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 301
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 300
    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/hangouts/views/AvatarView;->setMeasuredDimension(II)V

    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/hangouts/views/AvatarView;->c()V

    .line 303
    return-void
.end method
