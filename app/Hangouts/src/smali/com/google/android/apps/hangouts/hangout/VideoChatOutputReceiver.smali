.class public Lcom/google/android/apps/hangouts/hangout/VideoChatOutputReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9

    .prologue
    const/high16 v8, 0x8000000

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 27
    invoke-static {}, Lapk;->b()Lapk;

    move-result-object v0

    invoke-virtual {v0}, Lapk;->c()Lapx;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 28
    invoke-static {}, Lapk;->b()Lapk;

    move-result-object v0

    invoke-virtual {v0}, Lapk;->c()Lapx;

    move-result-object v0

    invoke-virtual {v0}, Lapx;->i()Z

    move-result v0

    if-nez v0, :cond_1

    .line 35
    :cond_0
    :goto_0
    return-void

    .line 31
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 32
    const-string v3, "com.google.android.libraries.hangouts.video.ACTION_ONGOING_NOTIFICATION_REQUEST"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    invoke-static {}, Lapk;->b()Lapk;

    move-result-object v0

    invoke-virtual {v0}, Lapk;->c()Lapx;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lapx;->J()I

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_1
    invoke-static {}, Lapk;->b()Lapk;

    move-result-object v3

    invoke-virtual {v3}, Lapk;->c()Lapx;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lapx;->K()Z

    move-result v3

    if-eqz v3, :cond_4

    :goto_2
    invoke-static {}, Lapk;->b()Lapk;

    move-result-object v2

    invoke-virtual {v2}, Lapk;->d()Lcom/google/android/libraries/hangouts/video/CallState;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/CallState;->getStartTime()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-eqz v3, :cond_5

    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/CallState;->getStartTime()J

    move-result-wide v2

    :goto_3
    const-string v4, "com.google.android.libraries.hangouts.video.sessionid"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const/4 v6, 0x4

    invoke-static {v6}, Lbzb;->a(I)I

    move-result v6

    invoke-static {v4}, Lbbl;->e(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v7

    invoke-static {p1, v6, v7, v8}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    const/16 v7, 0x65

    invoke-static {v7}, Lbzb;->a(I)I

    move-result v7

    invoke-static {v4}, Lbbl;->f(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    invoke-static {p1, v7, v4, v8}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    new-instance v7, Lbk;

    invoke-direct {v7, p1}, Lbk;-><init>(Landroid/content/Context;)V

    invoke-virtual {v7, v2, v3}, Lbk;->a(J)Lbk;

    move-result-object v3

    if-nez v0, :cond_2

    if-eqz v1, :cond_6

    :cond_2
    sget v2, Lcom/google/android/apps/hangouts/R$drawable;->cO:I

    :goto_4
    invoke-virtual {v3, v2}, Lbk;->a(I)Lbk;

    move-result-object v2

    if-eqz v0, :cond_7

    sget v1, Lh;->oC:I

    :goto_5
    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lbk;->a(Ljava/lang/CharSequence;)Lbk;

    move-result-object v2

    if-eqz v0, :cond_9

    sget v1, Lh;->oB:I

    :goto_6
    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lbk;->b(Ljava/lang/CharSequence;)Lbk;

    move-result-object v1

    invoke-virtual {v1}, Lbk;->a()Lbk;

    move-result-object v1

    invoke-virtual {v1, v6}, Lbk;->a(Landroid/app/PendingIntent;)Lbk;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lbk;->c(I)Lbk;

    move-result-object v1

    sget v2, Lcom/google/android/apps/hangouts/R$drawable;->bu:I

    if-eqz v0, :cond_a

    sget v0, Lh;->oA:I

    :goto_7
    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0, v4}, Lbk;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Lbk;

    move-result-object v0

    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;

    move-result-object v1

    invoke-virtual {v0}, Lbk;->e()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->respondToOngoingNotificationRequest(Landroid/app/Notification;)V

    goto/16 :goto_0

    :cond_3
    move v0, v2

    goto/16 :goto_1

    :cond_4
    move v1, v2

    goto/16 :goto_2

    :cond_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    goto/16 :goto_3

    :cond_6
    sget v2, Lcom/google/android/apps/hangouts/R$drawable;->cK:I

    goto :goto_4

    :cond_7
    if-eqz v1, :cond_8

    sget v1, Lh;->oR:I

    goto :goto_5

    :cond_8
    sget v1, Lh;->ev:I

    goto :goto_5

    :cond_9
    sget v1, Lh;->eu:I

    goto :goto_6

    :cond_a
    sget v0, Lh;->dJ:I

    goto :goto_7
.end method
