.class public final Lcom/google/android/libraries/hangouts/video/ApiaryApiInfo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x2ba6046fa3ebca2aL


# instance fields
.field private final mApiKey:Ljava/lang/String;

.field private final mCertificate:Ljava/lang/String;

.field private final mClientId:Ljava/lang/String;

.field private final mPackageName:Ljava/lang/String;

.field private final mSdkVersion:Ljava/lang/String;

.field private final mSourceInfo:Lcom/google/android/libraries/hangouts/video/ApiaryApiInfo;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 32
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/libraries/hangouts/video/ApiaryApiInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/libraries/hangouts/video/ApiaryApiInfo;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/libraries/hangouts/video/ApiaryApiInfo;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/ApiaryApiInfo;->mApiKey:Ljava/lang/String;

    .line 23
    iput-object p2, p0, Lcom/google/android/libraries/hangouts/video/ApiaryApiInfo;->mClientId:Ljava/lang/String;

    .line 24
    iput-object p3, p0, Lcom/google/android/libraries/hangouts/video/ApiaryApiInfo;->mPackageName:Ljava/lang/String;

    .line 25
    iput-object p4, p0, Lcom/google/android/libraries/hangouts/video/ApiaryApiInfo;->mCertificate:Ljava/lang/String;

    .line 26
    iput-object p6, p0, Lcom/google/android/libraries/hangouts/video/ApiaryApiInfo;->mSourceInfo:Lcom/google/android/libraries/hangouts/video/ApiaryApiInfo;

    .line 27
    iput-object p5, p0, Lcom/google/android/libraries/hangouts/video/ApiaryApiInfo;->mSdkVersion:Ljava/lang/String;

    .line 28
    return-void
.end method


# virtual methods
.method public getApiKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ApiaryApiInfo;->mApiKey:Ljava/lang/String;

    return-object v0
.end method

.method public getCertificate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ApiaryApiInfo;->mCertificate:Ljava/lang/String;

    return-object v0
.end method

.method public getClientId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ApiaryApiInfo;->mClientId:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ApiaryApiInfo;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getSdkVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ApiaryApiInfo;->mSdkVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getSourceInfo()Lcom/google/android/libraries/hangouts/video/ApiaryApiInfo;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ApiaryApiInfo;->mSourceInfo:Lcom/google/android/libraries/hangouts/video/ApiaryApiInfo;

    return-object v0
.end method
