.class public abstract Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/hangouts/video/CallStateListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getListeners()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/hangouts/video/CallStateListener;",
            ">;"
        }
    .end annotation
.end method

.method public onAudioUpdated(Lcom/google/android/libraries/hangouts/video/AudioDeviceState;Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/hangouts/video/AudioDeviceState;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/libraries/hangouts/video/AudioDevice;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;->getListeners()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStateListener;

    .line 49
    invoke-interface {v0, p1, p2}, Lcom/google/android/libraries/hangouts/video/CallStateListener;->onAudioUpdated(Lcom/google/android/libraries/hangouts/video/AudioDeviceState;Ljava/util/Set;)V

    goto :goto_0

    .line 51
    :cond_0
    return-void
.end method

.method public onAuthUserActionRequired(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;->getListeners()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStateListener;

    .line 34
    invoke-interface {v0, p1}, Lcom/google/android/libraries/hangouts/video/CallStateListener;->onAuthUserActionRequired(Landroid/content/Intent;)V

    goto :goto_0

    .line 36
    :cond_0
    return-void
.end method

.method public onBoundToService()V
    .locals 2

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;->getListeners()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStateListener;

    .line 27
    invoke-interface {v0}, Lcom/google/android/libraries/hangouts/video/CallStateListener;->onBoundToService()V

    goto :goto_0

    .line 29
    :cond_0
    return-void
.end method

.method public onBroadcastProducerChange(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;->getListeners()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStateListener;

    .line 139
    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/libraries/hangouts/video/CallStateListener;->onBroadcastProducerChange(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 141
    :cond_0
    return-void
.end method

.method public onBroadcastSessionStateChange(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;->getListeners()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStateListener;

    .line 147
    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/libraries/hangouts/video/CallStateListener;->onBroadcastSessionStateChange(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 149
    :cond_0
    return-void
.end method

.method public onBroadcastStreamStateChange(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;->getListeners()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStateListener;

    .line 124
    invoke-interface {v0, p1, p2}, Lcom/google/android/libraries/hangouts/video/CallStateListener;->onBroadcastStreamStateChange(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 126
    :cond_0
    return-void
.end method

.method public onBroadcastStreamViewerCountChange(I)V
    .locals 2

    .prologue
    .line 130
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;->getListeners()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStateListener;

    .line 131
    invoke-interface {v0, p1}, Lcom/google/android/libraries/hangouts/video/CallStateListener;->onBroadcastStreamViewerCountChange(I)V

    goto :goto_0

    .line 133
    :cond_0
    return-void
.end method

.method public onCallEnded(Lcom/google/android/libraries/hangouts/video/CallState;)V
    .locals 2

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;->getListeners()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStateListener;

    .line 73
    invoke-interface {v0, p1}, Lcom/google/android/libraries/hangouts/video/CallStateListener;->onCallEnded(Lcom/google/android/libraries/hangouts/video/CallState;)V

    goto :goto_0

    .line 75
    :cond_0
    return-void
.end method

.method public onCommonNotificationReceived(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;->getListeners()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStateListener;

    .line 109
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/libraries/hangouts/video/CallStateListener;->onCommonNotificationReceived(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 112
    :cond_0
    return-void
.end method

.method public onCommonNotificationRetracted(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;->getListeners()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStateListener;

    .line 117
    invoke-interface {v0, p1}, Lcom/google/android/libraries/hangouts/video/CallStateListener;->onCommonNotificationRetracted(Ljava/lang/String;)V

    goto :goto_0

    .line 119
    :cond_0
    return-void
.end method

.method public onConversationIdChanged(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;->getListeners()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStateListener;

    .line 101
    invoke-interface {v0, p1}, Lcom/google/android/libraries/hangouts/video/CallStateListener;->onConversationIdChanged(Ljava/lang/String;)V

    goto :goto_0

    .line 103
    :cond_0
    return-void
.end method

.method public onEndpointEvent(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/EndpointEvent;)V
    .locals 2

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;->getListeners()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStateListener;

    .line 80
    invoke-interface {v0, p1, p2}, Lcom/google/android/libraries/hangouts/video/CallStateListener;->onEndpointEvent(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/EndpointEvent;)V

    goto :goto_0

    .line 82
    :cond_0
    return-void
.end method

.method public onHangoutIdResolved(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;ZZ)V
    .locals 10

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;->getListeners()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStateListener;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    .line 58
    invoke-interface/range {v0 .. v8}, Lcom/google/android/libraries/hangouts/video/CallStateListener;->onHangoutIdResolved(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;ZZ)V

    goto :goto_0

    .line 61
    :cond_0
    return-void
.end method

.method public onLoudestSpeakerUpdate(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;->getListeners()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStateListener;

    .line 87
    invoke-interface {v0, p1, p2}, Lcom/google/android/libraries/hangouts/video/CallStateListener;->onLoudestSpeakerUpdate(Ljava/lang/String;I)V

    goto :goto_0

    .line 89
    :cond_0
    return-void
.end method

.method public onMediaStarted(Lcom/google/android/libraries/hangouts/video/CallState;)V
    .locals 2

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;->getListeners()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStateListener;

    .line 66
    invoke-interface {v0, p1}, Lcom/google/android/libraries/hangouts/video/CallStateListener;->onMediaStarted(Lcom/google/android/libraries/hangouts/video/CallState;)V

    goto :goto_0

    .line 68
    :cond_0
    return-void
.end method

.method public onPresenterUpdate(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V
    .locals 2

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;->getListeners()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStateListener;

    .line 94
    invoke-interface {v0, p1, p2}, Lcom/google/android/libraries/hangouts/video/CallStateListener;->onPresenterUpdate(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V

    goto :goto_0

    .line 96
    :cond_0
    return-void
.end method

.method public onSignedIn()V
    .locals 2

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;->getListeners()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStateListener;

    .line 41
    invoke-interface {v0}, Lcom/google/android/libraries/hangouts/video/CallStateListener;->onSignedIn()V

    goto :goto_0

    .line 43
    :cond_0
    return-void
.end method
