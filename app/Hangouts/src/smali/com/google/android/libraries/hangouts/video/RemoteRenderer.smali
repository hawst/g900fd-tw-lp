.class public Lcom/google/android/libraries/hangouts/video/RemoteRenderer;
.super Lcom/google/android/libraries/hangouts/video/Renderer;
.source "PG"


# static fields
.field private static final TAG:Ljava/lang/String; = "vclib"


# instance fields
.field private final mCallback:Lcom/google/android/libraries/hangouts/video/Renderer$RendererThreadCallback;

.field private mOutputTextureName:I


# direct methods
.method constructor <init>(Lcom/google/android/libraries/hangouts/video/RendererManager;Lcom/google/android/libraries/hangouts/video/Renderer$RendererThreadCallback;)V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x3

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/libraries/hangouts/video/RemoteRenderer;-><init>(Lcom/google/android/libraries/hangouts/video/RendererManager;Lcom/google/android/libraries/hangouts/video/Renderer$RendererThreadCallback;I)V

    .line 32
    return-void
.end method

.method constructor <init>(Lcom/google/android/libraries/hangouts/video/RendererManager;Lcom/google/android/libraries/hangouts/video/Renderer$RendererThreadCallback;I)V
    .locals 3

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/Renderer;-><init>()V

    .line 36
    const/4 v0, 0x3

    if-eq p3, v0, :cond_0

    const/4 v0, 0x5

    if-ne p3, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 38
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/RemoteRenderer;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    .line 39
    iput-object p2, p0, Lcom/google/android/libraries/hangouts/video/RemoteRenderer;->mCallback:Lcom/google/android/libraries/hangouts/video/Renderer$RendererThreadCallback;

    .line 40
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/RemoteRenderer;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    invoke-virtual {v0, p3}, Lcom/google/android/libraries/hangouts/video/RendererManager;->instantiateRenderer(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/RemoteRenderer;->mRendererID:I

    .line 41
    const-string v0, "vclib"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "construct "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/libraries/hangouts/video/RemoteRenderer;->mRendererID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/RemoteRenderer;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/hangouts/video/RendererManager;->registerRendererForStats(Lcom/google/android/libraries/hangouts/video/Renderer;)V

    .line 43
    return-void

    .line 36
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public drawTexture(Lcom/google/android/libraries/hangouts/video/Renderer$DrawInputParams;Lcom/google/android/libraries/hangouts/video/Renderer$DrawOutputParams;)Z
    .locals 4

    .prologue
    move-object v0, p2

    .line 68
    check-cast v0, Lcom/google/android/libraries/hangouts/video/RemoteRenderer$RendererFrameOutputData;

    .line 69
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/RemoteRenderer;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v2, p0, Lcom/google/android/libraries/hangouts/video/RemoteRenderer;->mRendererID:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, p2}, Lcom/google/android/libraries/hangouts/video/RendererManager;->renderFrame(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 70
    iget-boolean v0, v0, Lcom/google/android/libraries/hangouts/video/RemoteRenderer$RendererFrameOutputData;->updatedTexture:Z

    return v0
.end method

.method public getOutputTextureName()I
    .locals 2

    .prologue
    .line 55
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/RemoteRenderer;->mOutputTextureName:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 56
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/RemoteRenderer;->mOutputTextureName:I

    return v0
.end method

.method public initializeGLContext()V
    .locals 3

    .prologue
    .line 47
    const-string v0, "vclib"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "initializeGLContext "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/libraries/hangouts/video/RemoteRenderer;->mRendererID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/RemoteRenderer;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/RemoteRenderer;->mRendererID:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/RendererManager;->initializeGLContext(I)Z

    .line 49
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/RemoteRenderer;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/RemoteRenderer;->mRendererID:I

    const-string v2, "sub_outtex"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/hangouts/video/RendererManager;->getIntParam(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/RemoteRenderer;->mOutputTextureName:I

    .line 51
    return-void
.end method

.method public release()V
    .locals 3

    .prologue
    .line 61
    const-string v0, "vclib"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "release "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/libraries/hangouts/video/RemoteRenderer;->mRendererID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/RemoteRenderer;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/hangouts/video/RendererManager;->unregisterRendererForStats(Lcom/google/android/libraries/hangouts/video/Renderer;)V

    .line 63
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/RemoteRenderer;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/RemoteRenderer;->mRendererID:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/RendererManager;->releaseRenderer(I)V

    .line 64
    return-void
.end method
