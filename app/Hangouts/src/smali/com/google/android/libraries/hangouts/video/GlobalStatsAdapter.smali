.class public Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final mBatteryStateReceiver:Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter$BatteryStateReceiver;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter;->mContext:Landroid/content/Context;

    .line 26
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 27
    new-instance v1, Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter$BatteryStateReceiver;

    invoke-direct {v1, p0}, Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter$BatteryStateReceiver;-><init>(Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter;)V

    iput-object v1, p0, Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter;->mBatteryStateReceiver:Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter$BatteryStateReceiver;

    .line 28
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter;->mBatteryStateReceiver:Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter$BatteryStateReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 29
    return-void
.end method


# virtual methods
.method release()V
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter;->mBatteryStateReceiver:Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter$BatteryStateReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 33
    return-void
.end method

.method public updateStats(Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;)V
    .locals 1

    .prologue
    .line 36
    invoke-static {}, Lcxl;->c()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;->setOnlineCpuCores(I)V

    .line 37
    invoke-static {}, Lcxl;->e()I

    move-result v0

    div-int/lit16 v0, v0, 0x3e8

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;->setCurrentCpuSpeedMHz(I)V

    .line 38
    invoke-static {}, Lcxl;->f()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;->setUtilizationPerCpu(I)V

    .line 39
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter;->mBatteryStateReceiver:Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter$BatteryStateReceiver;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter$BatteryStateReceiver;->getIsOnBattery()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;->setIsOnBattery(Z)V

    .line 40
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter;->mBatteryStateReceiver:Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter$BatteryStateReceiver;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter$BatteryStateReceiver;->getBatteryLevel()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;->setBatteryLevel(I)V

    .line 41
    return-void
.end method
