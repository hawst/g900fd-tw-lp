.class Lcom/google/android/libraries/hangouts/video/CallManager$InitializeMobileHipriAndConnectTask;
.super Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/hangouts/video/SafeAsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/libraries/hangouts/video/CallManager;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/hangouts/video/CallManager;)V
    .locals 0

    .prologue
    .line 630
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CallManager$InitializeMobileHipriAndConnectTask;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackgroundTimed([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 633
    const-string v0, "talk.google.com"

    # invokes: Lcom/google/android/libraries/hangouts/video/CallManager;->lookupHost(Ljava/lang/String;)I
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallManager;->access$300(Ljava/lang/String;)I

    move-result v0

    .line 634
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 635
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 637
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager$InitializeMobileHipriAndConnectTask;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallManager;->mConnectivityManager:Landroid/net/ConnectivityManager;
    invoke-static {v1}, Lcom/google/android/libraries/hangouts/video/CallManager;->access$400(Lcom/google/android/libraries/hangouts/video/CallManager;)Landroid/net/ConnectivityManager;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, v2, v0}, Landroid/net/ConnectivityManager;->requestRouteToHost(II)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method protected bridge synthetic doInBackgroundTimed([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 630
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/hangouts/video/CallManager$InitializeMobileHipriAndConnectTask;->doInBackgroundTimed([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    .line 643
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 644
    const-string v0, "vclib"

    const-string v1, "requestRouteToHost failed"

    invoke-static {v0, v1}, Lcxc;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 645
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager$InitializeMobileHipriAndConnectTask;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    const/16 v1, 0x3eb

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/hangouts/video/CallManager;->handleCallEnd(ILjava/lang/String;)V

    .line 647
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 630
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/hangouts/video/CallManager$InitializeMobileHipriAndConnectTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
