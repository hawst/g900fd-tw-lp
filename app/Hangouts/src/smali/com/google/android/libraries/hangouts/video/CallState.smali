.class public final Lcom/google/android/libraries/hangouts/video/CallState;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final SESSION_ERROR_NETWORK:I = 0x3

.field static final SESSION_ERROR_NONE:I = 0x0

.field public static final STATE_DEINIT:I = 0xf

.field static final STATE_INIT:I = 0x0

.field public static final STATE_INPROGRESS:I = 0xe

.field private static final STATE_INVALID:I = -0x1

.field public static final STATE_RECEIVEDACCEPT:I = 0x6

.field public static final STATE_RECEIVEDINITIATE:I = 0x2

.field static final STATE_RECEIVEDMODIFY:I = 0x8

.field public static final STATE_RECEIVEDPRACCEPT:I = 0x5

.field public static final STATE_RECEIVEDREJECT:I = 0xa

.field public static final STATE_RECEIVEDTERMINATE:I = 0xd

.field public static final STATE_SENTACCEPT:I = 0x4

.field public static final STATE_SENTINITIATE:I = 0x1

.field static final STATE_SENTMODIFY:I = 0x7

.field public static final STATE_SENTPRACCEPT:I = 0x3

.field static final STATE_SENTREDIRECT:I = 0xb

.field public static final STATE_SENTREJECT:I = 0x9

.field public static final STATE_SENTTERMINATE:I = 0xc

.field private static final WINDOW_FOR_PRE_EXISTING_ENDPOINTS_MILLIS:I = 0x3e8


# instance fields
.field private final mCallOptions:Lcom/google/android/libraries/hangouts/video/CallOptions;

.field final mCallStatistics:Lcom/google/android/libraries/hangouts/video/CallStatistics;

.field private mConnectivityCheck:Ldon;

.field private mEndCause:I

.field private mErrorMessage:Ljava/lang/String;

.field mHangoutRequest:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

.field private mIsPendingSignIn:Z

.field mIsSecure:Z

.field mIsVideo:Z

.field mLastPresenter:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

.field private mMediaErrorCode:I

.field private mMediaEverConnected:Z

.field private mMediaNetworkType:I

.field mMediaState:I

.field private mOriginalHangoutRequest:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

.field mPresentAgent:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

.field mPresenter:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

.field private final mRemoteEndpoints:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;",
            ">;"
        }
    .end annotation
.end field

.field mRemoteFullJid:Ljava/lang/String;

.field mSelf:Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

.field final mSessionId:Ljava/lang/String;

.field private mStartTime:J

.field mTag:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/hangouts/video/CallOptions;)V
    .locals 26

    .prologue
    .line 111
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/libraries/hangouts/video/CallOptions;->getSessionId()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, -0x1

    const/4 v11, 0x0

    const/4 v12, 0x0

    const-wide/16 v13, 0x0

    const/16 v15, 0x8

    new-instance v16, Ljava/util/HashMap;

    invoke-direct/range {v16 .. v16}, Ljava/util/HashMap;-><init>()V

    const/16 v17, 0x0

    new-instance v18, Lcom/google/android/libraries/hangouts/video/CallStatistics;

    .line 126
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/libraries/hangouts/video/CallOptions;->getSessionId()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v18

    invoke-direct {v0, v1}, Lcom/google/android/libraries/hangouts/video/CallStatistics;-><init>(Ljava/lang/String;)V

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    move-object/from16 v1, p0

    move-object/from16 v3, p1

    .line 110
    invoke-direct/range {v1 .. v25}, Lcom/google/android/libraries/hangouts/video/CallState;-><init>(Ljava/lang/String;Lcom/google/android/libraries/hangouts/video/CallOptions;ZLjava/lang/String;Lcom/google/android/libraries/hangouts/video/HangoutRequest;Lcom/google/android/libraries/hangouts/video/HangoutRequest;ZZIZLjava/lang/Object;JILjava/util/Map;Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;Lcom/google/android/libraries/hangouts/video/CallStatistics;IILjava/lang/String;Ldon;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V

    .line 134
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lcom/google/android/libraries/hangouts/video/CallOptions;ZLjava/lang/String;Lcom/google/android/libraries/hangouts/video/HangoutRequest;Lcom/google/android/libraries/hangouts/video/HangoutRequest;ZZIZLjava/lang/Object;JILjava/util/Map;Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;Lcom/google/android/libraries/hangouts/video/CallStatistics;IILjava/lang/String;Ldon;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/libraries/hangouts/video/CallOptions;",
            "Z",
            "Ljava/lang/String;",
            "Lcom/google/android/libraries/hangouts/video/HangoutRequest;",
            "Lcom/google/android/libraries/hangouts/video/HangoutRequest;",
            "ZZIZ",
            "Ljava/lang/Object;",
            "JI",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;",
            ">;",
            "Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;",
            "Lcom/google/android/libraries/hangouts/video/CallStatistics;",
            "II",
            "Ljava/lang/String;",
            "Ldon;",
            "Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;",
            "Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;",
            "Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;",
            ")V"
        }
    .end annotation

    .prologue
    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mSessionId:Ljava/lang/String;

    .line 149
    iput-object p2, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mCallOptions:Lcom/google/android/libraries/hangouts/video/CallOptions;

    .line 150
    iput-boolean p3, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mIsPendingSignIn:Z

    .line 151
    invoke-virtual {p0, p4}, Lcom/google/android/libraries/hangouts/video/CallState;->setRemoteJid(Ljava/lang/String;)V

    .line 152
    iput-object p5, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mOriginalHangoutRequest:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    .line 153
    iput-object p6, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mHangoutRequest:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    .line 154
    iput-boolean p7, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mIsVideo:Z

    .line 155
    iput-boolean p8, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mIsSecure:Z

    .line 156
    iput p9, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mMediaState:I

    .line 157
    iput-boolean p10, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mMediaEverConnected:Z

    .line 158
    iput-object p11, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mTag:Ljava/lang/Object;

    .line 159
    iput-wide p12, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mStartTime:J

    .line 160
    iput p14, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mMediaNetworkType:I

    .line 161
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mRemoteEndpoints:Ljava/util/Map;

    .line 162
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mSelf:Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    .line 163
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mCallStatistics:Lcom/google/android/libraries/hangouts/video/CallStatistics;

    .line 164
    move/from16 v0, p18

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mMediaErrorCode:I

    .line 165
    move/from16 v0, p19

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mEndCause:I

    .line 166
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mErrorMessage:Ljava/lang/String;

    .line 167
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mConnectivityCheck:Ldon;

    .line 168
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mPresenter:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 169
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mPresentAgent:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 170
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mLastPresenter:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 171
    return-void
.end method

.method static getMediaStateName(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 433
    packed-switch p0, :pswitch_data_0

    .line 463
    :pswitch_0
    const-string v0, "Unknown type"

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    .line 464
    const-string v0, "Unknown state"

    :goto_0
    return-object v0

    .line 435
    :pswitch_1
    const-string v0, "STATE_INIT"

    goto :goto_0

    .line 437
    :pswitch_2
    const-string v0, "STATE_SENTINITIATE"

    goto :goto_0

    .line 439
    :pswitch_3
    const-string v0, "STATE_RECEIVEDINITIATE"

    goto :goto_0

    .line 441
    :pswitch_4
    const-string v0, "STATE_SENTACCEPT"

    goto :goto_0

    .line 443
    :pswitch_5
    const-string v0, "STATE_RECEIVEDACCEPT"

    goto :goto_0

    .line 445
    :pswitch_6
    const-string v0, "STATE_SENTMODIFY"

    goto :goto_0

    .line 447
    :pswitch_7
    const-string v0, "STATE_RECEIVEDMODIFY"

    goto :goto_0

    .line 449
    :pswitch_8
    const-string v0, "STATE_SENTREJECT"

    goto :goto_0

    .line 451
    :pswitch_9
    const-string v0, "STATE_RECEIVEDREJECT"

    goto :goto_0

    .line 453
    :pswitch_a
    const-string v0, "STATE_SENTREDIRECT"

    goto :goto_0

    .line 455
    :pswitch_b
    const-string v0, "STATE_SENTTERMINATE"

    goto :goto_0

    .line 457
    :pswitch_c
    const-string v0, "STATE_RECEIVEDTERMINATE"

    goto :goto_0

    .line 459
    :pswitch_d
    const-string v0, "STATE_INPROGRESS"

    goto :goto_0

    .line 461
    :pswitch_e
    const-string v0, "STATE_DEINIT"

    goto :goto_0

    .line 433
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method


# virtual methods
.method addRemoteEndpoint(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    .line 319
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isSelfEndpoint()Z

    move-result v1

    invoke-static {v1}, Lcwz;->b(Z)V

    .line 320
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mRemoteEndpoints:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getEndpointMucJid()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 321
    iget-wide v1, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mStartTime:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_1

    .line 329
    :cond_0
    :goto_0
    return v0

    .line 325
    :cond_1
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    .line 326
    iget-wide v3, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mStartTime:J

    const-wide/16 v5, 0x3e8

    add-long/2addr v3, v5

    cmp-long v1, v1, v3

    if-ltz v1, :cond_0

    const/4 v0, 0x0

    .line 329
    goto :goto_0
.end method

.method public getCallOptions()Lcom/google/android/libraries/hangouts/video/CallOptions;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mCallOptions:Lcom/google/android/libraries/hangouts/video/CallOptions;

    return-object v0
.end method

.method public getCallStatistics()Lcom/google/android/libraries/hangouts/video/CallStatistics;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mCallStatistics:Lcom/google/android/libraries/hangouts/video/CallStatistics;

    return-object v0
.end method

.method public getEndCause()I
    .locals 2

    .prologue
    .line 343
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mEndCause:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 344
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mEndCause:I

    return v0
.end method

.method public getEndpoint(Ljava/lang/String;)Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mSelf:Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mSelf:Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;->getEndpointMucJid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 238
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mSelf:Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    .line 240
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/hangouts/video/CallState;->getRemoteEndpoint(Ljava/lang/String;)Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    move-result-object v0

    goto :goto_0
.end method

.method public getErrorMessage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 349
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mEndCause:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 350
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mErrorMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getHangoutRequest()Lcom/google/android/libraries/hangouts/video/HangoutRequest;
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mHangoutRequest:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    return-object v0
.end method

.method public getLastPresenter()Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;
    .locals 1

    .prologue
    .line 390
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mLastPresenter:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    return-object v0
.end method

.method public getLogData(Landroid/content/Context;Ljava/lang/String;IILcom/google/android/libraries/hangouts/video/LocalState;)Ldzj;
    .locals 11

    .prologue
    .line 403
    if-nez p5, :cond_1

    .line 405
    const/4 v0, 0x0

    .line 421
    :cond_0
    :goto_0
    return-object v0

    .line 407
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mCallStatistics:Lcom/google/android/libraries/hangouts/video/CallStatistics;

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mOriginalHangoutRequest:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    .line 413
    invoke-virtual/range {p5 .. p5}, Lcom/google/android/libraries/hangouts/video/LocalState;->getJidResource()Ljava/lang/String;

    move-result-object v6

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mSelf:Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    if-nez v1, :cond_2

    const/4 v7, 0x0

    .line 415
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/CallState;->wasMediaInitiated()Z

    move-result v8

    iget v9, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mEndCause:I

    iget v10, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mMediaErrorCode:I

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    move v5, p4

    .line 407
    invoke-virtual/range {v0 .. v10}, Lcom/google/android/libraries/hangouts/video/CallStatistics;->getLogData(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/libraries/hangouts/video/HangoutRequest;IILjava/lang/String;Ljava/lang/String;ZII)Ldzj;

    move-result-object v0

    .line 418
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mConnectivityCheck:Ldon;

    if-eqz v1, :cond_0

    .line 419
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mConnectivityCheck:Ldon;

    iput-object v1, v0, Ldzj;->h:Ldon;

    goto :goto_0

    .line 413
    :cond_2
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mSelf:Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    .line 414
    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;->getEndpointMucJid()Ljava/lang/String;

    move-result-object v7

    goto :goto_1
.end method

.method public getMappedEndCause()I
    .locals 1

    .prologue
    .line 425
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mEndCause:I

    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallStatistics;->getMappedEndCause(I)I

    move-result v0

    return v0
.end method

.method getMediaNetworkType()I
    .locals 1

    .prologue
    .line 277
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mMediaNetworkType:I

    return v0
.end method

.method public getMediaState()I
    .locals 1

    .prologue
    .line 218
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mMediaState:I

    return v0
.end method

.method public getPresentAgent()Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;
    .locals 1

    .prologue
    .line 386
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mPresentAgent:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    return-object v0
.end method

.method public getPresenter()Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;
    .locals 1

    .prologue
    .line 382
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mPresenter:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    return-object v0
.end method

.method public getReadableMediaNetworkType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 284
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mMediaNetworkType:I

    packed-switch v0, :pswitch_data_0

    .line 306
    const-string v0, "unk"

    :goto_0
    return-object v0

    .line 286
    :pswitch_0
    const-string v0, "mobile"

    goto :goto_0

    .line 288
    :pswitch_1
    const-string v0, "wifi"

    goto :goto_0

    .line 290
    :pswitch_2
    const-string v0, "mms"

    goto :goto_0

    .line 292
    :pswitch_3
    const-string v0, "supl"

    goto :goto_0

    .line 294
    :pswitch_4
    const-string v0, "dun"

    goto :goto_0

    .line 296
    :pswitch_5
    const-string v0, "hipri"

    goto :goto_0

    .line 298
    :pswitch_6
    const-string v0, "wimax"

    goto :goto_0

    .line 300
    :pswitch_7
    const-string v0, "bt"

    goto :goto_0

    .line 302
    :pswitch_8
    const-string v0, "dummy"

    goto :goto_0

    .line 304
    :pswitch_9
    const-string v0, "eth"

    goto :goto_0

    .line 284
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method getRemoteBareJid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mRemoteFullJid:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mRemoteFullJid:Ljava/lang/String;

    invoke-static {v0}, Lf;->q(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getRemoteEndpoint(Ljava/lang/String;)Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mRemoteEndpoints:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    return-object v0
.end method

.method public getRemoteEndpoints()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;",
            ">;"
        }
    .end annotation

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mRemoteEndpoints:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public getRemoteFullJid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mRemoteFullJid:Ljava/lang/String;

    return-object v0
.end method

.method public getSelf()Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mSelf:Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    return-object v0
.end method

.method public getSessionId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mSessionId:Ljava/lang/String;

    return-object v0
.end method

.method public getStartTime()J
    .locals 2

    .prologue
    .line 429
    iget-wide v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mStartTime:J

    return-wide v0
.end method

.method initializeHangoutRequest(Lcom/google/android/libraries/hangouts/video/HangoutRequest;)V
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mOriginalHangoutRequest:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-static {v0}, Lcwz;->a(Ljava/lang/Object;)V

    .line 175
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mOriginalHangoutRequest:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    .line 176
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mHangoutRequest:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    .line 177
    return-void
.end method

.method public isInProgress()Z
    .locals 2

    .prologue
    .line 206
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mMediaState:I

    const/16 v1, 0xe

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInitiatingMedia()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 188
    iget v1, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mMediaState:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMediaConnected()Z
    .locals 2

    .prologue
    .line 196
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mMediaState:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mMediaState:I

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mMediaState:I

    const/16 v1, 0xe

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPendingSignIn()Z
    .locals 1

    .prologue
    .line 180
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mIsPendingSignIn:Z

    return v0
.end method

.method removeRemoteEndpoint(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V
    .locals 2

    .prologue
    .line 333
    invoke-static {p1}, Lcwz;->b(Ljava/lang/Object;)V

    .line 334
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mSelf:Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    invoke-static {p1, v0}, Lcwz;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 335
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mRemoteEndpoints:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getEndpointMucJid()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 336
    return-void
.end method

.method public setConnectivityCheck(Ldon;)V
    .locals 0

    .prologue
    .line 371
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mConnectivityCheck:Ldon;

    .line 372
    return-void
.end method

.method setEndCauseIfUnset(I)V
    .locals 1

    .prologue
    .line 354
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/libraries/hangouts/video/CallState;->setEndCauseIfUnset(ILjava/lang/String;)V

    .line 355
    return-void
.end method

.method setEndCauseIfUnset(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 358
    const/4 v0, 0x1

    const/16 v1, 0x3f7

    invoke-static {p1, v0, v1}, Lcwz;->a(III)V

    .line 360
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mEndCause:I

    if-nez v0, :cond_0

    .line 361
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mEndCause:I

    .line 362
    iput-object p2, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mErrorMessage:Ljava/lang/String;

    .line 364
    :cond_0
    return-void
.end method

.method setMediaError(I)V
    .locals 0

    .prologue
    .line 339
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mMediaErrorCode:I

    .line 340
    return-void
.end method

.method setMediaNetworkType(I)V
    .locals 0

    .prologue
    .line 312
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mMediaNetworkType:I

    .line 313
    return-void
.end method

.method setMediaState(I)V
    .locals 2

    .prologue
    .line 222
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mMediaState:I

    .line 223
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mMediaState:I

    const/16 v1, 0xe

    if-ne v0, v1, :cond_0

    .line 224
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mMediaEverConnected:Z

    .line 226
    :cond_0
    return-void
.end method

.method setRemoteJid(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mRemoteFullJid:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mRemoteFullJid:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 258
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mRemoteFullJid:Ljava/lang/String;

    .line 259
    return-void

    .line 257
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setSelf(Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;)V
    .locals 2

    .prologue
    .line 271
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;->isSelfEndpoint()Z

    move-result v0

    invoke-static {v0}, Lcwz;->a(Z)V

    .line 272
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mSelf:Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    .line 273
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mStartTime:J

    .line 274
    return-void
.end method

.method public setSignedIn()V
    .locals 1

    .prologue
    .line 184
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mIsPendingSignIn:Z

    .line 185
    return-void
.end method

.method public shouldManagePlatformInteraction()Z
    .locals 1

    .prologue
    .line 367
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mCallOptions:Lcom/google/android/libraries/hangouts/video/CallOptions;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallOptions;->shouldManagePlatformInteraction()Z

    move-result v0

    return v0
.end method

.method updatePresentToAllEndpoints(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mPresenter:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mLastPresenter:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 377
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mPresenter:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 378
    iput-object p2, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mPresentAgent:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 379
    return-void
.end method

.method public wasMediaEverConnected()Z
    .locals 1

    .prologue
    .line 202
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mMediaEverConnected:Z

    return v0
.end method

.method public wasMediaInitiated()Z
    .locals 2

    .prologue
    .line 192
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CallState;->mMediaState:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
