.class Lcom/google/android/libraries/hangouts/video/SelfRenderer$CameraCallback;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/hangouts/video/CameraManager$CameraManagerCallback;


# instance fields
.field final synthetic this$0:Lcom/google/android/libraries/hangouts/video/SelfRenderer;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/hangouts/video/SelfRenderer;)V
    .locals 0

    .prologue
    .line 209
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer$CameraCallback;->this$0:Lcom/google/android/libraries/hangouts/video/SelfRenderer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDesiredCaptureVideoSpec()Lcom/google/android/libraries/hangouts/video/VideoSpecification;
    .locals 1

    .prologue
    .line 213
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->getOutgoingNoEffectsVideoSpec()Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v0

    return-object v0
.end method

.method public onCameraOpen(Lcom/google/android/libraries/hangouts/video/Size;IZ)V
    .locals 3

    .prologue
    .line 238
    invoke-static {}, Lcxc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 239
    const-string v0, "vclib"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onCameraOpen "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " orientation "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " flip "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " deviceOrientation "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer$CameraCallback;->this$0:Lcom/google/android/libraries/hangouts/video/SelfRenderer;

    iget v2, v2, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mDeviceOrientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " cameraOrientation "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer$CameraCallback;->this$0:Lcom/google/android/libraries/hangouts/video/SelfRenderer;

    iget v2, v2, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mCameraRotation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    :cond_0
    invoke-static {}, Lcwz;->g()V

    .line 248
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer$CameraCallback;->this$0:Lcom/google/android/libraries/hangouts/video/SelfRenderer;

    iget-object v0, v0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mCallback:Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;

    new-instance v1, Lcom/google/android/libraries/hangouts/video/SelfRenderer$CameraCallback$2;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/libraries/hangouts/video/SelfRenderer$CameraCallback$2;-><init>(Lcom/google/android/libraries/hangouts/video/SelfRenderer$CameraCallback;Lcom/google/android/libraries/hangouts/video/Size;IZ)V

    invoke-interface {v0, v1}, Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;->queueEventForGLThread(Ljava/lang/Runnable;)V

    .line 254
    return-void
.end method

.method public onCameraOpenError(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 259
    invoke-static {}, Lcwz;->j()V

    .line 260
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer$CameraCallback;->this$0:Lcom/google/android/libraries/hangouts/video/SelfRenderer;

    iget-object v0, v0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mCallback:Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;

    new-instance v1, Lcom/google/android/libraries/hangouts/video/SelfRenderer$CameraCallback$3;

    invoke-direct {v1, p0, p1}, Lcom/google/android/libraries/hangouts/video/SelfRenderer$CameraCallback$3;-><init>(Lcom/google/android/libraries/hangouts/video/SelfRenderer$CameraCallback;Ljava/lang/Exception;)V

    invoke-interface {v0, v1}, Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;->queueEventForGLThread(Ljava/lang/Runnable;)V

    .line 266
    return-void
.end method

.method public onFrameOutputSet(Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;)V
    .locals 3

    .prologue
    .line 219
    invoke-static {}, Lcxc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220
    const-string v0, "vclib"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onFrameOutputSet "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;->size:Lcom/google/android/libraries/hangouts/video/Size;

    iget v2, v2, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;->size:Lcom/google/android/libraries/hangouts/video/Size;

    iget v2, v2, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    :cond_0
    invoke-static {}, Lcwz;->f()V

    .line 225
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer$CameraCallback;->this$0:Lcom/google/android/libraries/hangouts/video/SelfRenderer;

    iget-object v0, v0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mCallback:Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;

    new-instance v1, Lcom/google/android/libraries/hangouts/video/SelfRenderer$CameraCallback$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/libraries/hangouts/video/SelfRenderer$CameraCallback$1;-><init>(Lcom/google/android/libraries/hangouts/video/SelfRenderer$CameraCallback;Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;)V

    invoke-interface {v0, v1}, Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;->queueEventForGLThread(Ljava/lang/Runnable;)V

    .line 232
    return-void
.end method
