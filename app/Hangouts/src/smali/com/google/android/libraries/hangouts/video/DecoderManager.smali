.class public final Lcom/google/android/libraries/hangouts/video/DecoderManager;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final SW_DECODER_PREFIXES:[Ljava/lang/String;

.field private static final VP8_MIME_TYPE:Ljava/lang/String; = "video/x-vnd.on2.vp8"


# instance fields
.field private mNativeContext:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 28
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "OMX.google."

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "OMX.SEC."

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/libraries/hangouts/video/DecoderManager;->SW_DECODER_PREFIXES:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/DecoderManager;->nativeInit()V

    .line 39
    return-void
.end method

.method static native frameRenderedExternally(IJII)Z
.end method

.method static native getNextEncodedFrame(IZLjava/nio/ByteBuffer;Ljava/lang/Object;)Z
.end method

.method private static getVp8CodecInfo()Landroid/media/MediaCodecInfo;
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 77
    invoke-static {}, Landroid/media/MediaCodecList;->getCodecCount()I

    move-result v4

    move v3, v1

    .line 78
    :goto_0
    if-ge v3, v4, :cond_2

    .line 79
    invoke-static {v3}, Landroid/media/MediaCodecList;->getCodecInfoAt(I)Landroid/media/MediaCodecInfo;

    move-result-object v2

    .line 81
    invoke-virtual {v2}, Landroid/media/MediaCodecInfo;->isEncoder()Z

    move-result v0

    if-nez v0, :cond_1

    .line 82
    invoke-virtual {v2}, Landroid/media/MediaCodecInfo;->getSupportedTypes()[Ljava/lang/String;

    move-result-object v5

    move v0, v1

    .line 85
    :goto_1
    array-length v6, v5

    if-ge v0, v6, :cond_1

    .line 86
    aget-object v6, v5, v0

    const-string v7, "video/x-vnd.on2.vp8"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    move-object v0, v2

    .line 91
    :goto_2
    return-object v0

    .line 85
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 78
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 91
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private final native nativeInit()V
.end method

.method private final native nativeRelease()V
.end method

.method public static supportsHardwareAcceleration()Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 54
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-ge v1, v2, :cond_1

    .line 66
    :cond_0
    :goto_0
    return v0

    .line 57
    :cond_1
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/DecoderManager;->getVp8CodecInfo()Landroid/media/MediaCodecInfo;

    move-result-object v2

    .line 58
    if-eqz v2, :cond_0

    .line 61
    sget-object v3, Lcom/google/android/libraries/hangouts/video/DecoderManager;->SW_DECODER_PREFIXES:[Ljava/lang/String;

    array-length v4, v3

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_2

    aget-object v5, v3, v1

    .line 62
    invoke-virtual {v2}, Landroid/media/MediaCodecInfo;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 61
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 66
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public createDecoder()Lcom/google/android/libraries/hangouts/video/Decoder;
    .locals 1

    .prologue
    .line 101
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/DecoderManager;->supportsHardwareAcceleration()Z

    move-result v0

    if-nez v0, :cond_0

    .line 102
    const/4 v0, 0x0

    .line 106
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/libraries/hangouts/video/Decoder;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/hangouts/video/Decoder;-><init>(Lcom/google/android/libraries/hangouts/video/DecoderManager;)V

    goto :goto_0
.end method

.method public release()V
    .locals 0

    .prologue
    .line 114
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/DecoderManager;->nativeRelease()V

    .line 115
    return-void
.end method
