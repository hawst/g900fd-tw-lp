.class Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;
.super Ljava/lang/Thread;
.source "PG"


# instance fields
.field final synthetic this$0:Lcom/google/android/libraries/hangouts/video/GlManager;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/hangouts/video/GlManager;)V
    .locals 1

    .prologue
    .line 197
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    .line 200
    const-string v0, "GLThread.newarch"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 201
    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 205
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->glHelper:Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$100(Lcom/google/android/libraries/hangouts/video/GlManager;)Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->init()V

    .line 209
    new-array v2, v8, [I

    .line 210
    invoke-static {v8, v2, v7}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    .line 211
    new-instance v0, Landroid/graphics/SurfaceTexture;

    aget v1, v2, v7

    invoke-direct {v0, v1}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    .line 212
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->glHelper:Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;
    invoke-static {v1}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$100(Lcom/google/android/libraries/hangouts/video/GlManager;)Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->egl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    .line 213
    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->glHelper:Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;
    invoke-static {v3}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$100(Lcom/google/android/libraries/hangouts/video/GlManager;)Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->eglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->glHelper:Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;
    invoke-static {v4}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$100(Lcom/google/android/libraries/hangouts/video/GlManager;)Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->eglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    const/4 v5, 0x0

    .line 212
    invoke-interface {v1, v3, v4, v0, v5}, Ljavax/microedition/khronos/egl/EGL10;->eglCreateWindowSurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljava/lang/Object;[I)Ljavax/microedition/khronos/egl/EGLSurface;

    move-result-object v3

    .line 218
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->glHelper:Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$100(Lcom/google/android/libraries/hangouts/video/GlManager;)Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->egl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->glHelper:Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;
    invoke-static {v1}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$100(Lcom/google/android/libraries/hangouts/video/GlManager;)Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->eglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    .line 219
    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->glHelper:Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;
    invoke-static {v4}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$100(Lcom/google/android/libraries/hangouts/video/GlManager;)Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->eglContext:Ljavax/microedition/khronos/egl/EGLContext;

    .line 218
    invoke-interface {v0, v1, v3, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 220
    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->eventQueue:Ljava/util/Queue;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$200(Lcom/google/android/libraries/hangouts/video/GlManager;)Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 221
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 261
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->videoSources:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$300(Lcom/google/android/libraries/hangouts/video/GlManager;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 262
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->glHelper:Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$100(Lcom/google/android/libraries/hangouts/video/GlManager;)Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->egl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->glHelper:Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;
    invoke-static {v1}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$100(Lcom/google/android/libraries/hangouts/video/GlManager;)Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->eglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface {v0, v1, v3}, Ljavax/microedition/khronos/egl/EGL10;->eglDestroySurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z

    .line 263
    invoke-static {v8, v2, v7}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    .line 264
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->outputRenderers:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$400(Lcom/google/android/libraries/hangouts/video/GlManager;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;

    .line 265
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->release()V

    goto :goto_1

    .line 227
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->videoSources:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$300(Lcom/google/android/libraries/hangouts/video/GlManager;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/VideoSource;

    .line 228
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/VideoSource;->processFrame()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 261
    :catchall_0
    move-exception v0

    move-object v1, v0

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->videoSources:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$300(Lcom/google/android/libraries/hangouts/video/GlManager;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 262
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->glHelper:Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$100(Lcom/google/android/libraries/hangouts/video/GlManager;)Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->egl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->glHelper:Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;
    invoke-static {v4}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$100(Lcom/google/android/libraries/hangouts/video/GlManager;)Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->eglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface {v0, v4, v3}, Ljavax/microedition/khronos/egl/EGL10;->eglDestroySurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z

    .line 263
    invoke-static {v8, v2, v7}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    .line 264
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->outputRenderers:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$400(Lcom/google/android/libraries/hangouts/video/GlManager;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;

    .line 265
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->release()V

    goto :goto_3

    .line 234
    :cond_2
    :try_start_2
    const-string v0, "eglMakeCurrent failed"

    invoke-static {v0}, Lf;->s(Ljava/lang/String;)V

    .line 238
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->outputRenderers:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$400(Lcom/google/android/libraries/hangouts/video/GlManager;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    :sswitch_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;

    .line 239
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->initIfNeeded()I

    move-result v1

    .line 240
    if-nez v1, :cond_4

    .line 241
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->render()I

    move-result v1

    .line 243
    :cond_4
    sparse-switch v1, :sswitch_data_0

    .line 250
    const-string v0, "vclib"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "GL error: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcxc;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 247
    :sswitch_1
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->release()V

    goto :goto_4

    .line 254
    :cond_5
    monitor-enter p0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 255
    const-wide/16 v0, 0x5

    :try_start_3
    invoke-virtual {p0, v0, v1}, Ljava/lang/Object;->wait(J)V

    .line 256
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 257
    :try_start_4
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 261
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->videoSources:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$300(Lcom/google/android/libraries/hangouts/video/GlManager;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 262
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->glHelper:Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$100(Lcom/google/android/libraries/hangouts/video/GlManager;)Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->egl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->glHelper:Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;
    invoke-static {v1}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$100(Lcom/google/android/libraries/hangouts/video/GlManager;)Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->eglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface {v0, v1, v3}, Ljavax/microedition/khronos/egl/EGL10;->eglDestroySurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z

    .line 263
    invoke-static {v8, v2, v7}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    .line 264
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->outputRenderers:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$400(Lcom/google/android/libraries/hangouts/video/GlManager;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;

    .line 265
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->release()V

    goto :goto_5

    .line 256
    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit p0

    throw v0
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 267
    :cond_6
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->outputRenderers:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$400(Lcom/google/android/libraries/hangouts/video/GlManager;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 268
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->glHelper:Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$100(Lcom/google/android/libraries/hangouts/video/GlManager;)Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->release()V

    .line 269
    :goto_6
    return-void

    .line 267
    :cond_7
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->outputRenderers:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$400(Lcom/google/android/libraries/hangouts/video/GlManager;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 268
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->glHelper:Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$100(Lcom/google/android/libraries/hangouts/video/GlManager;)Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->release()V

    goto :goto_6

    .line 267
    :cond_8
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->outputRenderers:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$400(Lcom/google/android/libraries/hangouts/video/GlManager;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 268
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;->this$0:Lcom/google/android/libraries/hangouts/video/GlManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/GlManager;->glHelper:Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/GlManager;->access$100(Lcom/google/android/libraries/hangouts/video/GlManager;)Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;->release()V

    throw v1

    .line 243
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x300d -> :sswitch_1
    .end sparse-switch
.end method
