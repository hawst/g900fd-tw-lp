.class public final Lcom/google/android/libraries/hangouts/video/RendererManager;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final IMAGE_STABILIZATION_RENDERER_LEVEL_KEY:Ljava/lang/String; = "is_level"

.field static final IMAGE_STABILIZATION_RENDERER_PULSE_RESET_KEY:Ljava/lang/String; = "is_reset"

.field static final RENDERER_FLIP_INPUT_KEY:Ljava/lang/String; = "sub_flipinput"

.field static final RENDERER_INPUT_CLIP_DIMENSIONS_KEY:Ljava/lang/String; = "sub_inclip"

.field static final RENDERER_INPUT_DIMENSIONS_KEY:Ljava/lang/String; = "sub_indims"

.field static final RENDERER_INPUT_TEXTURE_KEY:Ljava/lang/String; = "sub_intex"

.field static final RENDERER_INPUT_TEXTURE_TYPE_KEY:Ljava/lang/String; = "sub_intextype"

.field static final RENDERER_IS_SCREENCAST_KEY:Ljava/lang/String; = "sub_screencast"

.field static final RENDERER_OUTPUT_DIMENSIONS_KEY:Ljava/lang/String; = "sub_outdims"

.field static final RENDERER_OUTPUT_FBO_KEY:Ljava/lang/String; = "sub_outfbo"

.field static final RENDERER_OUTPUT_GL_FORMAT_KEY:Ljava/lang/String; = "sub_outformat"

.field static final RENDERER_OUTPUT_TEXTURE_KEY:Ljava/lang/String; = "sub_outtex"

.field static final RENDERER_RENDER_CALLS_KEY:Ljava/lang/String; = "sub_renderedframes"

.field static final RENDERER_TYPE_CAMERA:I = 0x1

.field static final RENDERER_TYPE_ENCODE_GB:I = 0x4

.field static final RENDERER_TYPE_FAKE_REMOTE:I = 0x5

.field static final RENDERER_TYPE_IMAGE_STABILIZATION:I = 0x2

.field static final RENDERER_TYPE_MAX:I = 0x5

.field static final RENDERER_TYPE_REMOTE:I = 0x3

.field static final RENDERER_TYPE_SELF:I = 0x0

.field static final SELF_RENDERER_CAMERA_ROTATION_KEY:Ljava/lang/String; = "c_rotation"

.field private static sRendererStatsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/hangouts/video/Renderer;",
            ">;"
        }
    .end annotation
.end field

.field private static final sRendererStatsListLock:Ljava/lang/Object;


# instance fields
.field private mNativeContext:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/Libjingle;->load()V

    .line 63
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/libraries/hangouts/video/RendererManager;->sRendererStatsListLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/RendererManager;->nativeInit()V

    .line 67
    sget-object v1, Lcom/google/android/libraries/hangouts/video/RendererManager;->sRendererStatsListLock:Ljava/lang/Object;

    monitor-enter v1

    .line 73
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/libraries/hangouts/video/RendererManager;->sRendererStatsList:Ljava/util/List;

    .line 74
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static dump(Ljava/io/PrintWriter;)V
    .locals 3

    .prologue
    .line 98
    sget-object v1, Lcom/google/android/libraries/hangouts/video/RendererManager;->sRendererStatsListLock:Ljava/lang/Object;

    monitor-enter v1

    .line 99
    :try_start_0
    sget-object v0, Lcom/google/android/libraries/hangouts/video/RendererManager;->sRendererStatsList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 100
    sget-object v0, Lcom/google/android/libraries/hangouts/video/RendererManager;->sRendererStatsList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/Renderer;

    .line 101
    invoke-virtual {v0, p0}, Lcom/google/android/libraries/hangouts/video/Renderer;->dump(Ljava/io/PrintWriter;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 104
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private final native nativeInit()V
.end method

.method private final native nativeRelease()V
.end method


# virtual methods
.method public createAcceleratedRemoteRenderer(Lcom/google/android/libraries/hangouts/video/Decoder;)Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;
    .locals 1

    .prologue
    .line 159
    invoke-static {p1}, Lcwz;->b(Ljava/lang/Object;)V

    .line 160
    new-instance v0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;

    invoke-direct {v0, p0, p1}, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;-><init>(Lcom/google/android/libraries/hangouts/video/RendererManager;Lcom/google/android/libraries/hangouts/video/Decoder;)V

    return-object v0
.end method

.method public createCustomInputRenderer(Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;)Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;
    .locals 1

    .prologue
    .line 203
    new-instance v0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;

    invoke-direct {v0, p0, p1}, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;-><init>(Lcom/google/android/libraries/hangouts/video/RendererManager;Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;)V

    return-object v0
.end method

.method public createFakeRemoteRenderer(Lcom/google/android/libraries/hangouts/video/Renderer$RendererThreadCallback;)Lcom/google/android/libraries/hangouts/video/FakeRemoteRenderer;
    .locals 1

    .prologue
    .line 171
    new-instance v0, Lcom/google/android/libraries/hangouts/video/FakeRemoteRenderer;

    invoke-direct {v0, p0, p1}, Lcom/google/android/libraries/hangouts/video/FakeRemoteRenderer;-><init>(Lcom/google/android/libraries/hangouts/video/RendererManager;Lcom/google/android/libraries/hangouts/video/Renderer$RendererThreadCallback;)V

    return-object v0
.end method

.method public createRemoteRenderer(Lcom/google/android/libraries/hangouts/video/Renderer$RendererThreadCallback;)Lcom/google/android/libraries/hangouts/video/RemoteRenderer;
    .locals 1

    .prologue
    .line 148
    new-instance v0, Lcom/google/android/libraries/hangouts/video/RemoteRenderer;

    invoke-direct {v0, p0, p1}, Lcom/google/android/libraries/hangouts/video/RemoteRenderer;-><init>(Lcom/google/android/libraries/hangouts/video/RendererManager;Lcom/google/android/libraries/hangouts/video/Renderer$RendererThreadCallback;)V

    return-object v0
.end method

.method public createSelfRenderer(Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;Lcom/google/android/libraries/hangouts/video/CameraSpecification;)Lcom/google/android/libraries/hangouts/video/SelfRendererICS;
    .locals 1

    .prologue
    .line 186
    new-instance v0, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;-><init>(Lcom/google/android/libraries/hangouts/video/RendererManager;Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;Lcom/google/android/libraries/hangouts/video/CameraSpecification;)V

    return-object v0
.end method

.method public createSelfRendererGB(Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;Lcom/google/android/libraries/hangouts/video/CameraSpecification;)Lcom/google/android/libraries/hangouts/video/SelfRendererGB;
    .locals 1

    .prologue
    .line 191
    new-instance v0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;-><init>(Lcom/google/android/libraries/hangouts/video/RendererManager;Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;Lcom/google/android/libraries/hangouts/video/CameraSpecification;)V

    return-object v0
.end method

.method final native getIntParam(ILjava/lang/String;)I
.end method

.method public getNativeContext()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/RendererManager;->mNativeContext:I

    return v0
.end method

.method final native initializeGLContext(I)Z
.end method

.method instantiateRenderer(I)I
    .locals 2

    .prologue
    .line 207
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-static {p1, v0, v1}, Lcwz;->a(III)V

    .line 208
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/hangouts/video/RendererManager;->nativeInstantiateRenderer(I)I

    move-result v0

    return v0
.end method

.method final native nativeInstantiateRenderer(I)I
.end method

.method registerRendererForStats(Lcom/google/android/libraries/hangouts/video/Renderer;)V
    .locals 2

    .prologue
    .line 114
    sget-object v1, Lcom/google/android/libraries/hangouts/video/RendererManager;->sRendererStatsListLock:Ljava/lang/Object;

    monitor-enter v1

    .line 117
    :try_start_0
    sget-object v0, Lcom/google/android/libraries/hangouts/video/RendererManager;->sRendererStatsList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 118
    sget-object v0, Lcom/google/android/libraries/hangouts/video/RendererManager;->sRendererStatsList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 120
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public release()V
    .locals 2

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/RendererManager;->nativeRelease()V

    .line 83
    sget-object v1, Lcom/google/android/libraries/hangouts/video/RendererManager;->sRendererStatsListLock:Ljava/lang/Object;

    monitor-enter v1

    .line 84
    const/4 v0, 0x0

    :try_start_0
    sput-object v0, Lcom/google/android/libraries/hangouts/video/RendererManager;->sRendererStatsList:Ljava/util/List;

    .line 85
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final native releaseRenderer(I)V
.end method

.method final native renderFrame(ILjava/lang/Object;Ljava/lang/Object;)V
.end method

.method final native setIntParam(ILjava/lang/String;I)V
.end method

.method unregisterRendererForStats(Lcom/google/android/libraries/hangouts/video/Renderer;)V
    .locals 2

    .prologue
    .line 130
    sget-object v1, Lcom/google/android/libraries/hangouts/video/RendererManager;->sRendererStatsListLock:Ljava/lang/Object;

    monitor-enter v1

    .line 134
    :try_start_0
    sget-object v0, Lcom/google/android/libraries/hangouts/video/RendererManager;->sRendererStatsList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 135
    sget-object v0, Lcom/google/android/libraries/hangouts/video/RendererManager;->sRendererStatsList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 137
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
