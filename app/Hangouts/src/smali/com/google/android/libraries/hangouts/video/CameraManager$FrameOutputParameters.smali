.class Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public frameRate:I

.field public size:Lcom/google/android/libraries/hangouts/video/Size;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;)V
    .locals 2

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    new-instance v0, Lcom/google/android/libraries/hangouts/video/Size;

    iget-object v1, p1, Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;->size:Lcom/google/android/libraries/hangouts/video/Size;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/hangouts/video/Size;-><init>(Lcom/google/android/libraries/hangouts/video/Size;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;->size:Lcom/google/android/libraries/hangouts/video/Size;

    .line 100
    iget v0, p1, Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;->frameRate:I

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;->frameRate:I

    .line 101
    return-void
.end method

.method public constructor <init>(Lcom/google/android/libraries/hangouts/video/Size;I)V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;->size:Lcom/google/android/libraries/hangouts/video/Size;

    .line 95
    iput p2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;->frameRate:I

    .line 96
    return-void
.end method
