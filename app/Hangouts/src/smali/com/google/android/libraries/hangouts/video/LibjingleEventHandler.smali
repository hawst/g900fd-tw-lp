.class final Lcom/google/android/libraries/hangouts/video/LibjingleEventHandler;
.super Landroid/os/Handler;
.source "PG"


# static fields
.field private static final APIARY_REQUEST_OP:I = 0xa

.field private static final AUDIO_LEVELS_OP:I = 0x7

.field private static final BROADCAST_PRODUCER_CHANGE_SUB_OP:I = 0x5

.field private static final BROADCAST_SESSION_STATE_CHANGE_SUB_OP:I = 0x6

.field private static final BROADCAST_STREAM_STATE_CHANGE_SUB_OP:I = 0x3

.field private static final BROADCAST_STREAM_VIEWER_COUNT_CHANGE_SUB_OP:I = 0x4

.field private static final CALL_ERROR_OP:I = 0x2

.field private static final CHECK_CONNECTIVITY_DONE_OP:I = 0x9

.field private static final COMMON_NOTIFICATION_RECEIVED_SUB_OP:I = 0x1

.field private static final COMMON_NOTIFICATION_RETRACTED_SUB_OP:I = 0x2

.field private static final CONVERSATION_ID_CHANGED_SUB_OP:I = 0x0

.field private static final ENDPOINT_EVENT_OP:I = 0x5

.field private static final HANGOUT_ID_RESOLVED_OP:I = 0x1

.field private static final HANGOUT_ID_RESOLVED_OP_FLAG_ABUSE_RECORDABLE:I = 0x8

.field private static final HANGOUT_ID_RESOLVED_OP_FLAG_BROADCAST:I = 0x2

.field private static final HANGOUT_ID_RESOLVED_OP_FLAG_LITE:I = 0x1

.field private static final HANGOUT_ID_RESOLVED_OP_FLAG_RECORDABLE:I = 0x4

.field private static final MEDIA_SOURCES_OP:I = 0x6

.field private static final MEDIA_STATE_CHANGED_OP:I = 0x3

.field private static final OVERLOADED_OP:I = 0x8

.field private static final SIGNIN_STATE_CHANGED_OP:I = 0x0

.field private static final STATS_OP:I = 0x4

.field private static final TAG:Ljava/lang/String; = "vclib"


# instance fields
.field private mCallback:Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v1, 0xa

    const/16 v3, 0x8

    const/4 v2, 0x6

    .line 59
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 61
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 63
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 65
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 66
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 74
    return-void
.end method

.method public constructor <init>(Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 78
    return-void
.end method

.method static getEndpointEventTypeName(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 218
    packed-switch p0, :pswitch_data_0

    .line 234
    const-string v0, "Unknown type"

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    .line 235
    const-string v0, "Unknown type"

    :goto_0
    return-object v0

    .line 220
    :pswitch_0
    const-string v0, "CHATROOM_ENTERED"

    goto :goto_0

    .line 222
    :pswitch_1
    const-string v0, "ENDPOINT_ENTERED"

    goto :goto_0

    .line 224
    :pswitch_2
    const-string v0, "ENDPOINT_EXITED"

    goto :goto_0

    .line 226
    :pswitch_3
    const-string v0, "ENDPOINT_CHANGED"

    goto :goto_0

    .line 228
    :pswitch_4
    const-string v0, "ENDPOINT_AUDIO_MUTE_STATE_CHANGED"

    goto :goto_0

    .line 230
    :pswitch_5
    const-string v0, "ENDPOINT_VIDEO_MUTE_STATE_CHANGED"

    goto :goto_0

    .line 232
    :pswitch_6
    const-string v0, "ENDPOINT_MEDIA_BLOCKED"

    goto :goto_0

    .line 218
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private log(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 240
    const-string v0, "vclib"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[LibjingleEventHandler] "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcxc;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 86
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/LibjingleEventHandler;->mCallback:Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;

    if-nez v2, :cond_0

    .line 213
    :goto_0
    return-void

    .line 91
    :cond_0
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v6

    .line 92
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 212
    const-string v0, "vclib"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown message type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcxc;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 98
    :pswitch_0
    iget v2, p1, Landroid/os/Message;->arg1:I

    if-ne v2, v0, :cond_1

    .line 99
    :goto_1
    const-string v1, "str1"

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 100
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/LibjingleEventHandler;->mCallback:Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;

    invoke-interface {v2, v1, v0}, Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;->handleSignedInStateUpdate(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 98
    goto :goto_1

    .line 104
    :pswitch_1
    const-string v2, "str1"

    invoke-virtual {v6, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 105
    const-string v2, "str2"

    invoke-virtual {v6, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 106
    const-string v4, "str3"

    invoke-virtual {v6, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 107
    const-string v4, "str4"

    invoke-virtual {v6, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 108
    iget v4, p1, Landroid/os/Message;->arg1:I

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_2

    move v4, v0

    .line 109
    :goto_2
    iget v5, p1, Landroid/os/Message;->arg1:I

    and-int/lit8 v5, v5, 0x2

    if-eqz v5, :cond_3

    move v5, v0

    .line 110
    :goto_3
    iget v7, p1, Landroid/os/Message;->arg1:I

    and-int/lit8 v7, v7, 0x4

    if-eqz v7, :cond_4

    move v7, v0

    .line 111
    :goto_4
    iget v8, p1, Landroid/os/Message;->arg1:I

    and-int/lit8 v8, v8, 0x8

    if-eqz v8, :cond_5

    move v8, v0

    .line 113
    :goto_5
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/LibjingleEventHandler;->mCallback:Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;

    .line 114
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    move-object v1, v3

    .line 116
    :goto_6
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 113
    :goto_7
    invoke-interface/range {v0 .. v8}, Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;->handleHangoutIdResolved(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;ZZ)V

    goto :goto_0

    :cond_2
    move v4, v1

    .line 108
    goto :goto_2

    :cond_3
    move v5, v1

    .line 109
    goto :goto_3

    :cond_4
    move v7, v1

    .line 110
    goto :goto_4

    :cond_5
    move v8, v1

    .line 111
    goto :goto_5

    :cond_6
    move-object v1, v9

    .line 114
    goto :goto_6

    :cond_7
    move-object v3, v10

    .line 116
    goto :goto_7

    .line 123
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/LibjingleEventHandler;->mCallback:Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;

    iget v1, p1, Landroid/os/Message;->arg1:I

    const-string v2, "str1"

    invoke-virtual {v6, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;->handleCallEnd(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 129
    :pswitch_3
    iget v2, p1, Landroid/os/Message;->arg1:I

    .line 130
    iget v3, p1, Landroid/os/Message;->arg2:I

    .line 131
    const-string v0, "str1"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 132
    const-string v0, "str2"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 133
    const-string v0, "str3"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 134
    invoke-static {}, Lcxc;->c()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 135
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, "handleMessage(MEDIA_STATE_CHANGED): for sessionId: "

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, " new state="

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 136
    invoke-static {v2}, Lcom/google/android/libraries/hangouts/video/CallState;->getMediaStateName(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 135
    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/LibjingleEventHandler;->log(Ljava/lang/String;)V

    .line 138
    :cond_8
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/LibjingleEventHandler;->mCallback:Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;

    invoke-interface/range {v0 .. v5}, Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;->handleMediaStateChanged(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 142
    :pswitch_4
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/LibjingleEventHandler;->mCallback:Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/libraries/hangouts/video/Stats;

    invoke-interface {v1, v0}, Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;->handleStatsUpdate(Lcom/google/android/libraries/hangouts/video/Stats;)V

    goto/16 :goto_0

    .line 146
    :pswitch_5
    const-string v0, "str1"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 147
    iget v3, p1, Landroid/os/Message;->arg1:I

    .line 148
    iget v4, p1, Landroid/os/Message;->arg2:I

    .line 149
    const-string v0, "str2"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 150
    const-string v0, "str3"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 151
    const-string v0, "str4"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 152
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/LibjingleEventHandler;->mCallback:Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;

    invoke-interface/range {v0 .. v6}, Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;->handleEndpointEvent(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 158
    :pswitch_6
    const-string v0, "str1"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 159
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/LibjingleEventHandler;->mCallback:Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/libraries/hangouts/video/MediaSources;

    invoke-interface {v2, v1, v0}, Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;->handleMediaSourcesUpdate(Ljava/lang/String;Lcom/google/android/libraries/hangouts/video/MediaSources;)V

    goto/16 :goto_0

    .line 163
    :pswitch_7
    const-string v0, "str1"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 164
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/LibjingleEventHandler;->mCallback:Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v1, v0, v2}, Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;->handleLoudestSpeakerUpdate(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 168
    :pswitch_8
    iget v1, p1, Landroid/os/Message;->arg1:I

    .line 169
    iget v2, p1, Landroid/os/Message;->arg2:I

    .line 170
    const-string v0, "str1"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 171
    const-string v4, "str2"

    invoke-virtual {v6, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 172
    const-string v5, "str3"

    invoke-virtual {v6, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 173
    packed-switch v1, :pswitch_data_1

    goto/16 :goto_0

    .line 175
    :pswitch_9
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/LibjingleEventHandler;->mCallback:Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;

    .line 176
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 175
    :goto_8
    invoke-interface {v1, v3}, Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;->handleConversationIdChanged(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    move-object v3, v0

    .line 176
    goto :goto_8

    .line 179
    :pswitch_a
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/LibjingleEventHandler;->mCallback:Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;

    invoke-interface {v1, v0, v2, v4, v5}, Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;->handleCommonNotificationReceived(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 182
    :pswitch_b
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/LibjingleEventHandler;->mCallback:Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;

    invoke-interface {v1, v0}, Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;->handleCommonNotificationRetracted(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 185
    :pswitch_c
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/LibjingleEventHandler;->mCallback:Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;

    invoke-interface {v1, v0, v4}, Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;->handleBroadcastStreamStateChange(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 188
    :pswitch_d
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/LibjingleEventHandler;->mCallback:Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;

    invoke-interface {v0, v2}, Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;->handleBroadcastStreamViewerCountChange(I)V

    goto/16 :goto_0

    .line 191
    :pswitch_e
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/LibjingleEventHandler;->mCallback:Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;

    invoke-interface {v1, v0, v4, v5}, Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;->handleBroadcastProducerChange(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 194
    :pswitch_f
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/LibjingleEventHandler;->mCallback:Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;

    invoke-interface {v1, v0, v4, v5}, Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;->handleBroadcastSessionStateChange(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 201
    :pswitch_10
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/LibjingleEventHandler;->mCallback:Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;

    invoke-interface {v1, v0}, Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;->handleCheckConnectivity([Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;)V

    goto/16 :goto_0

    .line 205
    :pswitch_11
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/LibjingleEventHandler;->mCallback:Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;

    const-string v1, "str1"

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    const-string v3, "str2"

    .line 206
    invoke-virtual {v6, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, [B

    iget v5, p1, Landroid/os/Message;->arg1:I

    .line 205
    invoke-interface/range {v0 .. v5}, Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;->makeApiaryRequest(JLjava/lang/String;[BI)V

    goto/16 :goto_0

    .line 92
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_10
        :pswitch_11
    .end packed-switch

    .line 173
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method public setCallback(Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/LibjingleEventHandler;->mCallback:Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;

    .line 82
    return-void
.end method
