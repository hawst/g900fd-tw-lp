.class public Lcom/google/android/libraries/hangouts/video/SurfaceInfo;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final height:I

.field private final surface:Landroid/view/Surface;

.field private final width:I


# direct methods
.method public constructor <init>(Landroid/view/Surface;II)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/SurfaceInfo;->surface:Landroid/view/Surface;

    .line 15
    iput p2, p0, Lcom/google/android/libraries/hangouts/video/SurfaceInfo;->width:I

    .line 16
    iput p3, p0, Lcom/google/android/libraries/hangouts/video/SurfaceInfo;->height:I

    .line 17
    return-void
.end method


# virtual methods
.method public getHeight()I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/SurfaceInfo;->height:I

    return v0
.end method

.method public getSurface()Landroid/view/Surface;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SurfaceInfo;->surface:Landroid/view/Surface;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/SurfaceInfo;->width:I

    return v0
.end method
