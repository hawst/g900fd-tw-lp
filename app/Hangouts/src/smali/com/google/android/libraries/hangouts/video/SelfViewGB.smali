.class public final Lcom/google/android/libraries/hangouts/video/SelfViewGB;
.super Landroid/view/SurfaceView;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x9
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "vclib"


# instance fields
.field private mCameraInterface:Lcom/google/android/libraries/hangouts/video/CameraInterface;

.field private mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;

.field private mDeviceOrientation:I

.field private final mSurfaceHolderCallback:Landroid/view/SurfaceHolder$Callback;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/hangouts/video/SelfViewGB;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 38
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/libraries/hangouts/video/SelfViewGB;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 66
    new-instance v0, Lcom/google/android/libraries/hangouts/video/SelfViewGB$1;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/hangouts/video/SelfViewGB$1;-><init>(Lcom/google/android/libraries/hangouts/video/SelfViewGB;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfViewGB;->mSurfaceHolderCallback:Landroid/view/SurfaceHolder$Callback;

    .line 44
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/SelfViewGB;->init()V

    .line 45
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/libraries/hangouts/video/SelfViewGB;)Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfViewGB;->mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/libraries/hangouts/video/SelfViewGB;)I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/SelfViewGB;->mDeviceOrientation:I

    return v0
.end method

.method private init()V
    .locals 2

    .prologue
    .line 56
    const-string v0, "vclib"

    const-string v1, "init"

    invoke-static {v0, v1}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/CameraInterface;->getInstance()Lcom/google/android/libraries/hangouts/video/CameraInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfViewGB;->mCameraInterface:Lcom/google/android/libraries/hangouts/video/CameraInterface;

    .line 58
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfViewGB;->mCameraInterface:Lcom/google/android/libraries/hangouts/video/CameraInterface;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CameraInterface;->getCameraManager()Lcom/google/android/libraries/hangouts/video/CameraManager;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfViewGB;->mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;

    .line 60
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/hangouts/video/SelfViewGB;->setZOrderMediaOverlay(Z)V

    .line 61
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/SelfViewGB;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    .line 62
    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    .line 63
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/SelfViewGB;->mSurfaceHolderCallback:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 64
    return-void
.end method


# virtual methods
.method public getCamera()Lcom/google/android/libraries/hangouts/video/CameraInterface;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfViewGB;->mCameraInterface:Lcom/google/android/libraries/hangouts/video/CameraInterface;

    return-object v0
.end method

.method public setDeviceOrientation(I)V
    .locals 0

    .prologue
    .line 48
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/SelfViewGB;->mDeviceOrientation:I

    .line 49
    return-void
.end method
