.class Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;
.super Landroid/os/Handler;
.source "PG"


# instance fields
.field final synthetic this$0:Lcom/google/android/libraries/hangouts/video/Decoder;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/hangouts/video/Decoder;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;->this$0:Lcom/google/android/libraries/hangouts/video/Decoder;

    .line 103
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 104
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0xa

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 109
    :try_start_0
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 165
    const-string v0, "vclib"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown message in DecodeHandler: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcxc;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    :cond_0
    :goto_0
    return-void

    .line 112
    :pswitch_0
    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v2, v0, :cond_3

    .line 115
    :goto_1
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;->removeMessages(I)V

    .line 118
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;->this$0:Lcom/google/android/libraries/hangouts/video/Decoder;

    # invokes: Lcom/google/android/libraries/hangouts/video/Decoder;->cleanup(Z)V
    invoke-static {v2, v0}, Lcom/google/android/libraries/hangouts/video/Decoder;->access$000(Lcom/google/android/libraries/hangouts/video/Decoder;Z)V

    .line 120
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;->this$0:Lcom/google/android/libraries/hangouts/video/Decoder;

    # getter for: Lcom/google/android/libraries/hangouts/video/Decoder;->mLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/google/android/libraries/hangouts/video/Decoder;->access$100(Lcom/google/android/libraries/hangouts/video/Decoder;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    :try_start_1
    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;->this$0:Lcom/google/android/libraries/hangouts/video/Decoder;

    # getter for: Lcom/google/android/libraries/hangouts/video/Decoder;->mUpdateGLTextureInfo:Z
    invoke-static {v3}, Lcom/google/android/libraries/hangouts/video/Decoder;->access$200(Lcom/google/android/libraries/hangouts/video/Decoder;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 123
    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;->this$0:Lcom/google/android/libraries/hangouts/video/Decoder;

    # getter for: Lcom/google/android/libraries/hangouts/video/Decoder;->mOldGLTextureId:I
    invoke-static {v3}, Lcom/google/android/libraries/hangouts/video/Decoder;->access$300(Lcom/google/android/libraries/hangouts/video/Decoder;)I

    move-result v3

    if-eqz v3, :cond_1

    .line 124
    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;->this$0:Lcom/google/android/libraries/hangouts/video/Decoder;

    # getter for: Lcom/google/android/libraries/hangouts/video/Decoder;->mTexturesToDelete:Ljava/util/List;
    invoke-static {v3}, Lcom/google/android/libraries/hangouts/video/Decoder;->access$400(Lcom/google/android/libraries/hangouts/video/Decoder;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;->this$0:Lcom/google/android/libraries/hangouts/video/Decoder;

    # getter for: Lcom/google/android/libraries/hangouts/video/Decoder;->mOldGLTextureId:I
    invoke-static {v4}, Lcom/google/android/libraries/hangouts/video/Decoder;->access$300(Lcom/google/android/libraries/hangouts/video/Decoder;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 125
    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;->this$0:Lcom/google/android/libraries/hangouts/video/Decoder;

    const/4 v4, 0x0

    # setter for: Lcom/google/android/libraries/hangouts/video/Decoder;->mOldGLTextureId:I
    invoke-static {v3, v4}, Lcom/google/android/libraries/hangouts/video/Decoder;->access$302(Lcom/google/android/libraries/hangouts/video/Decoder;I)I

    .line 127
    :cond_1
    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;->this$0:Lcom/google/android/libraries/hangouts/video/Decoder;

    const/4 v4, 0x0

    # setter for: Lcom/google/android/libraries/hangouts/video/Decoder;->mUpdateGLTextureInfo:Z
    invoke-static {v3, v4}, Lcom/google/android/libraries/hangouts/video/Decoder;->access$202(Lcom/google/android/libraries/hangouts/video/Decoder;Z)Z

    .line 129
    :cond_2
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 130
    if-eqz v0, :cond_0

    .line 131
    :try_start_2
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;->this$0:Lcom/google/android/libraries/hangouts/video/Decoder;

    # invokes: Lcom/google/android/libraries/hangouts/video/Decoder;->initializeMediaCodec()Z
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/Decoder;->access$500(Lcom/google/android/libraries/hangouts/video/Decoder;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 132
    const-string v0, "vclib"

    const-string v2, "Initialize failed, ignoring decode."

    invoke-static {v0, v2}, Lcxc;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 167
    :catch_0
    move-exception v0

    .line 168
    const-string v2, "vclib"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception in DecodeHandler: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcxc;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    const-string v2, "vclib"

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcxc;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0, v6, v7}, Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :cond_3
    move v0, v1

    .line 112
    goto :goto_1

    .line 129
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2

    throw v0

    .line 135
    :cond_4
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;->sendMessage(Landroid/os/Message;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0

    .line 141
    :pswitch_1
    const/4 v0, 0x0

    :try_start_4
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;->removeMessages(I)V

    .line 143
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 144
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;->this$0:Lcom/google/android/libraries/hangouts/video/Decoder;

    # invokes: Lcom/google/android/libraries/hangouts/video/Decoder;->processInput()V
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/Decoder;->access$600(Lcom/google/android/libraries/hangouts/video/Decoder;)V

    .line 145
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;->this$0:Lcom/google/android/libraries/hangouts/video/Decoder;

    # invokes: Lcom/google/android/libraries/hangouts/video/Decoder;->maybeRenderFrame()Z
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/Decoder;->access$700(Lcom/google/android/libraries/hangouts/video/Decoder;)Z

    .line 146
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;->this$0:Lcom/google/android/libraries/hangouts/video/Decoder;

    # getter for: Lcom/google/android/libraries/hangouts/video/Decoder;->mMediaCodecResetRequired:Z
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/Decoder;->access$800(Lcom/google/android/libraries/hangouts/video/Decoder;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 147
    const-string v0, "vclib"

    const-string v2, "The MediaCodec HW decoder requires a restart."

    invoke-static {v0, v2}, Lcxc;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;->sendMessage(Landroid/os/Message;)Z
    :try_end_4
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_0

    .line 155
    :catch_1
    move-exception v0

    .line 158
    :try_start_5
    const-string v2, "vclib"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Decoding failed: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcxc;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    const-string v2, "vclib"

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcxc;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    const-string v0, "vclib"

    const-string v2, "Attempting to reset decoder."

    invoke-static {v0, v2}, Lcxc;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;->sendMessage(Landroid/os/Message;)Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    goto/16 :goto_0

    .line 152
    :cond_5
    :try_start_6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    .line 153
    const-wide/16 v4, 0x0

    sub-long v2, v6, v2

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 154
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_6
    .catch Ljava/lang/IllegalStateException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    goto/16 :goto_0

    .line 109
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
