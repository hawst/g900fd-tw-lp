.class public abstract Lcom/google/android/libraries/hangouts/video/CallStateListener$AbstractCallStateListener;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/hangouts/video/CallStateListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioUpdated(Lcom/google/android/libraries/hangouts/video/AudioDeviceState;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/hangouts/video/AudioDeviceState;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/libraries/hangouts/video/AudioDevice;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 41
    return-void
.end method

.method public onAuthUserActionRequired(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 32
    return-void
.end method

.method public onBoundToService()V
    .locals 0

    .prologue
    .line 28
    return-void
.end method

.method public onBroadcastProducerChange(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 95
    return-void
.end method

.method public onBroadcastSessionStateChange(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 100
    return-void
.end method

.method public onBroadcastStreamStateChange(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 86
    return-void
.end method

.method public onBroadcastStreamViewerCountChange(I)V
    .locals 0

    .prologue
    .line 90
    return-void
.end method

.method public onCallEnded(Lcom/google/android/libraries/hangouts/video/CallState;)V
    .locals 0

    .prologue
    .line 56
    return-void
.end method

.method public onCommonNotificationReceived(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 78
    return-void
.end method

.method public onCommonNotificationRetracted(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 82
    return-void
.end method

.method public onConversationIdChanged(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 73
    return-void
.end method

.method public onEndpointEvent(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/EndpointEvent;)V
    .locals 0

    .prologue
    .line 60
    return-void
.end method

.method public onHangoutIdResolved(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;ZZ)V
    .locals 0

    .prologue
    .line 48
    return-void
.end method

.method public onLoudestSpeakerUpdate(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 64
    return-void
.end method

.method public onMediaStarted(Lcom/google/android/libraries/hangouts/video/CallState;)V
    .locals 0

    .prologue
    .line 52
    return-void
.end method

.method public onPresenterUpdate(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V
    .locals 0

    .prologue
    .line 69
    return-void
.end method

.method public onSignedIn()V
    .locals 0

    .prologue
    .line 36
    return-void
.end method
