.class public Lcom/google/android/libraries/hangouts/video/VideoChat;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static AGC_CONFIG_COMP_GAIN:Ljava/lang/String; = null

.field public static AGC_CONFIG_LIMITER_ENABLE:Ljava/lang/String; = null

.field public static AGC_CONFIG_TARGET_LEVEL:Ljava/lang/String; = null

.field public static AGC_MODE:Ljava/lang/String; = null

.field public static AGC_MODE_ADAPTIVE_ANALOG:Ljava/lang/String; = null

.field public static AGC_MODE_ADAPTIVE_DIGITAL:Ljava/lang/String; = null

.field public static AGC_MODE_DEFAULT:Ljava/lang/String; = null

.field public static AGC_MODE_FIXED_DIGITAL:Ljava/lang/String; = null

.field public static AUDIO_PLAYBACK_SAMPLING_RATE:Ljava/lang/String; = null

.field public static AUDIO_RECORDING_DEVICE:Ljava/lang/String; = null

.field public static AUDIO_RECORDING_SAMPLING_RATE:Ljava/lang/String; = null

.field public static BLOCK_INTERFACE_NAMES:Ljava/lang/String; = null

.field public static CALL_ENTER_STEP_TIMEOUT_MILLIS:Ljava/lang/String; = null

.field public static final CHECK_CONNECTIVITY_ALWAYS:I = 0x2

.field public static final CHECK_CONNECTIVITY_DISABLED:I = 0x0

.field public static final CHECK_CONNECTIVITY_ON_ERRORS:I = 0x1

.field public static DIAGNOSTIC_RAW_LOG_FILE_SIZE_BYTES:Ljava/lang/String; = null

.field public static EC_COMFORT_NOISE_GENERATION:Ljava/lang/String; = null

.field public static ENABLE_AUTO_GAIN_CONTROL:Ljava/lang/String; = null

.field public static ENABLE_ECHO_CANCELLATION:Ljava/lang/String; = null

.field public static ENABLE_NOISE_SUPPRESSION:Ljava/lang/String; = null

.field public static ENABLE_RX_AUTO_GAIN_CONTROL:Ljava/lang/String; = null

.field public static RX_AGC_CONFIG_COMP_GAIN:Ljava/lang/String; = null

.field public static RX_AGC_CONFIG_LIMITER_ENABLE:Ljava/lang/String; = null

.field public static RX_AGC_CONFIG_TARGET_LEVEL:Ljava/lang/String; = null

.field private static final TAG:Ljava/lang/String; = "vclib"

.field public static USE_DEFAULT_NETWORKS_ONLY:Ljava/lang/String;

.field public static USE_MESI:Ljava/lang/String;

.field public static VIDEO_ENCODE_CORES:Ljava/lang/String;

.field public static VIDEO_MAX_FRAMERATE:Ljava/lang/String;

.field public static VIDEO_MAX_HEIGHT:Ljava/lang/String;

.field public static VIDEO_MAX_WIDTH:Ljava/lang/String;

.field public static WRITE_DIAGNOSTIC_LOGS:Ljava/lang/String;

.field public static XMPP_HOSTNAME:Ljava/lang/String;

.field public static XMPP_PORT:Ljava/lang/String;

.field private static sInstance:Lcom/google/android/libraries/hangouts/video/VideoChat;


# instance fields
.field private final mCallStateListener:Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;

.field private final mContext:Landroid/content/Context;

.field private mIsListening:Z

.field private final mListeners:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/libraries/hangouts/video/CallStateListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mSoftConnection:Landroid/content/ServiceConnection;

.field private mSoftServiceBinder:Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    const-string v0, "USE_DEFAULT_NETWORKS_ONLY"

    sput-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->USE_DEFAULT_NETWORKS_ONLY:Ljava/lang/String;

    .line 65
    const-string v0, "BLOCK_INTERFACE_NAMES"

    sput-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->BLOCK_INTERFACE_NAMES:Ljava/lang/String;

    .line 66
    const-string v0, "ENABLE_ECHO_CANCELLATION"

    sput-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->ENABLE_ECHO_CANCELLATION:Ljava/lang/String;

    .line 67
    const-string v0, "ENABLE_AUTO_GAIN_CONTROL"

    sput-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->ENABLE_AUTO_GAIN_CONTROL:Ljava/lang/String;

    .line 68
    const-string v0, "ENABLE_NOISE_SUPPRESSION"

    sput-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->ENABLE_NOISE_SUPPRESSION:Ljava/lang/String;

    .line 69
    const-string v0, "EC_COMFORT_NOISE_GENERATION"

    sput-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->EC_COMFORT_NOISE_GENERATION:Ljava/lang/String;

    .line 70
    const-string v0, "AGC_MODE"

    sput-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->AGC_MODE:Ljava/lang/String;

    .line 71
    const-string v0, "AGC_CONFIG_TARGET_LEVEL"

    sput-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->AGC_CONFIG_TARGET_LEVEL:Ljava/lang/String;

    .line 72
    const-string v0, "AGC_CONFIG_COMP_GAIN"

    sput-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->AGC_CONFIG_COMP_GAIN:Ljava/lang/String;

    .line 73
    const-string v0, "AGC_CONFIG_LIMITER_ENABLE"

    sput-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->AGC_CONFIG_LIMITER_ENABLE:Ljava/lang/String;

    .line 74
    const-string v0, "ENABLE_RX_AUTO_GAIN_CONTROL"

    sput-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->ENABLE_RX_AUTO_GAIN_CONTROL:Ljava/lang/String;

    .line 75
    const-string v0, "RX_AGC_CONFIG_TARGET_LEVEL"

    sput-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->RX_AGC_CONFIG_TARGET_LEVEL:Ljava/lang/String;

    .line 76
    const-string v0, "RX_AGC_CONFIG_COMP_GAIN"

    sput-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->RX_AGC_CONFIG_COMP_GAIN:Ljava/lang/String;

    .line 77
    const-string v0, "RX_AGC_CONFIG_LIMITER_ENABLE"

    sput-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->RX_AGC_CONFIG_LIMITER_ENABLE:Ljava/lang/String;

    .line 78
    const-string v0, "AUDIO_RECORDING_SAMPLING_RATE"

    sput-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->AUDIO_RECORDING_SAMPLING_RATE:Ljava/lang/String;

    .line 79
    const-string v0, "AUDIO_PLAYBACK_SAMPLING_RATE"

    sput-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->AUDIO_PLAYBACK_SAMPLING_RATE:Ljava/lang/String;

    .line 80
    const-string v0, "AUDIO_RECORDING_DEVICE"

    sput-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->AUDIO_RECORDING_DEVICE:Ljava/lang/String;

    .line 82
    const-string v0, "VIDEO_MAX_WIDTH"

    sput-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->VIDEO_MAX_WIDTH:Ljava/lang/String;

    .line 83
    const-string v0, "VIDEO_MAX_HEIGHT"

    sput-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->VIDEO_MAX_HEIGHT:Ljava/lang/String;

    .line 84
    const-string v0, "VIDEO_MAX_FRAMERATE"

    sput-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->VIDEO_MAX_FRAMERATE:Ljava/lang/String;

    .line 85
    const-string v0, "VIDEO_ENCODE_CORES"

    sput-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->VIDEO_ENCODE_CORES:Ljava/lang/String;

    .line 87
    const-string v0, "XMPP_HOSTNAME"

    sput-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->XMPP_HOSTNAME:Ljava/lang/String;

    .line 88
    const-string v0, "XMPP_PORT"

    sput-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->XMPP_PORT:Ljava/lang/String;

    .line 90
    const-string v0, "DEFAULT"

    sput-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->AGC_MODE_DEFAULT:Ljava/lang/String;

    .line 91
    const-string v0, "ADAPTIVE_ANALOG"

    sput-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->AGC_MODE_ADAPTIVE_ANALOG:Ljava/lang/String;

    .line 92
    const-string v0, "ADAPTIVE_DIGITAL"

    sput-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->AGC_MODE_ADAPTIVE_DIGITAL:Ljava/lang/String;

    .line 93
    const-string v0, "FIXED_DIGITAL"

    sput-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->AGC_MODE_FIXED_DIGITAL:Ljava/lang/String;

    .line 95
    const-string v0, "CALL_ENTER_STEP_TIMEOUT_MILLIS"

    sput-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->CALL_ENTER_STEP_TIMEOUT_MILLIS:Ljava/lang/String;

    .line 96
    const-string v0, "WRITE_DIAGNOSTIC_LOGS"

    sput-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->WRITE_DIAGNOSTIC_LOGS:Ljava/lang/String;

    .line 97
    const-string v0, "DIAGNOSTIC_RAW_LOG_FILE_SIZE_BYTES"

    sput-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->DIAGNOSTIC_RAW_LOG_FILE_SIZE_BYTES:Ljava/lang/String;

    .line 99
    const-string v0, "USE_MESI"

    sput-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->USE_MESI:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mListeners:Ljava/util/LinkedList;

    .line 610
    new-instance v0, Lcom/google/android/libraries/hangouts/video/VideoChat$1;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/hangouts/video/VideoChat$1;-><init>(Lcom/google/android/libraries/hangouts/video/VideoChat;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mSoftConnection:Landroid/content/ServiceConnection;

    .line 653
    new-instance v0, Lcom/google/android/libraries/hangouts/video/VideoChat$2;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/hangouts/video/VideoChat$2;-><init>(Lcom/google/android/libraries/hangouts/video/VideoChat;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mCallStateListener:Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;

    .line 140
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mContext:Landroid/content/Context;

    .line 141
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->startListening()V

    .line 142
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/libraries/hangouts/video/VideoChat;Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/VideoChat;->onSoftVideoChatServiceBound(Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/libraries/hangouts/video/VideoChat;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->stopListening()V

    return-void
.end method

.method static synthetic access$202(Lcom/google/android/libraries/hangouts/video/VideoChat;Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;)Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mSoftServiceBinder:Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/libraries/hangouts/video/VideoChat;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->startListening()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/libraries/hangouts/video/VideoChat;)Ljava/util/LinkedList;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mListeners:Ljava/util/LinkedList;

    return-object v0
.end method

.method private bindVideoChatService()V
    .locals 4

    .prologue
    .line 645
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.talk.SOFT_BIND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 646
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mContext:Landroid/content/Context;

    const-class v2, Lcom/google/android/libraries/hangouts/video/VideoChatService;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 650
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mSoftConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 651
    return-void
.end method

.method private constructBaseIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 562
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mContext:Landroid/content/Context;

    const-class v2, Lcom/google/android/libraries/hangouts/video/VideoChatService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 563
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 564
    if-eqz p2, :cond_0

    .line 565
    const-string v1, "remote_jid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 567
    :cond_0
    if-eqz p3, :cond_1

    .line 568
    const-string v1, "account_name"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 571
    :cond_1
    return-object v0
.end method

.method public static getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;
    .locals 1

    .prologue
    .line 129
    sget-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->sInstance:Lcom/google/android/libraries/hangouts/video/VideoChat;

    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 130
    invoke-static {}, Lcwz;->a()V

    .line 131
    sget-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->sInstance:Lcom/google/android/libraries/hangouts/video/VideoChat;

    return-object v0
.end method

.method public static initialize(Landroid/content/Context;Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;)V
    .locals 1

    .prologue
    .line 111
    sget-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->sInstance:Lcom/google/android/libraries/hangouts/video/VideoChat;

    invoke-static {v0}, Lcwz;->a(Ljava/lang/Object;)V

    .line 112
    invoke-static {}, Lcwz;->a()V

    .line 113
    new-instance v0, Lcom/google/android/libraries/hangouts/video/VideoChat;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/hangouts/video/VideoChat;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->sInstance:Lcom/google/android/libraries/hangouts/video/VideoChat;

    .line 114
    invoke-static {p0, p1}, Lcom/google/android/libraries/hangouts/video/Registration;->register(Landroid/content/Context;Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;)V

    .line 115
    invoke-static {}, Lcxc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/Libjingle;->load()V

    .line 120
    :cond_0
    return-void
.end method

.method private inviteUsers(Z[Ljava/lang/String;[Ljava/lang/String;IZZLjava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 327
    const-string v0, "com.google.android.libraries.hangouts.video.ACTION_INVITE_USERS"

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/libraries/hangouts/video/VideoChat;->constructBaseIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 331
    const-string v1, "invite_conversation"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 333
    const-string v1, "invited_user_ids"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 334
    const-string v1, "invited_circle_ids"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 335
    const-string v1, "invite_type"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 336
    const-string v1, "ring_invitees"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 337
    const-string v1, "create_activity"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 338
    const-string v1, "hangout_start_context"

    invoke-virtual {v0, v1, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 339
    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->sendMessage(Landroid/content/Intent;)V

    .line 340
    return-void
.end method

.method private onSoftVideoChatServiceBound(Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;)V
    .locals 2

    .prologue
    .line 634
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mSoftServiceBinder:Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;

    .line 638
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mIsListening:Z

    if-eqz v0, :cond_0

    .line 639
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mSoftServiceBinder:Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mCallStateListener:Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;->addRemoteCallStateListener(Lcom/google/android/libraries/hangouts/video/CallStateListener;)V

    .line 640
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mCallStateListener:Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;->onBoundToService()V

    .line 642
    :cond_0
    return-void
.end method

.method private sendMessage(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 558
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 559
    return-void
.end method

.method private startListening()V
    .locals 2

    .prologue
    .line 581
    const-string v0, "vclib"

    const-string v1, "startListening"

    invoke-static {v0, v1}, Lcxc;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 582
    invoke-static {}, Lcwz;->a()V

    .line 583
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mIsListening:Z

    .line 584
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->bindVideoChatService()V

    .line 585
    return-void
.end method

.method private stopListening()V
    .locals 3

    .prologue
    .line 594
    const-string v0, "vclib"

    const-string v1, "stopListening"

    invoke-static {v0, v1}, Lcxc;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 595
    invoke-static {}, Lcwz;->a()V

    .line 596
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mIsListening:Z

    .line 597
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mSoftServiceBinder:Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;

    if-eqz v0, :cond_0

    .line 598
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mSoftServiceBinder:Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mCallStateListener:Lcom/google/android/libraries/hangouts/video/ForwardingCallStateListener;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;->removeRemoteCallStateListener(Lcom/google/android/libraries/hangouts/video/CallStateListener;)V

    .line 602
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mSoftConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 608
    :goto_0
    return-void

    .line 603
    :catch_0
    move-exception v0

    .line 606
    const-string v1, "vclib"

    const-string v2, "Problem unbinding service."

    invoke-static {v1, v2, v0}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public addListener(Lcom/google/android/libraries/hangouts/video/CallStateListener;)V
    .locals 1

    .prologue
    .line 154
    invoke-static {}, Lcwz;->a()V

    .line 155
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mListeners:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 156
    return-void
.end method

.method public addLogComment(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 549
    const-string v0, "com.google.android.libraries.hangouts.video.ACTION_ADD_LOG_COMMENT"

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/libraries/hangouts/video/VideoChat;->constructBaseIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 553
    const-string v1, "com.google.android.libraries.hangouts.video.log_comment"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 554
    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->sendMessage(Landroid/content/Intent;)V

    .line 555
    return-void
.end method

.method public bindRenderer(ILcom/google/android/libraries/hangouts/video/RemoteRenderer;)V
    .locals 3

    .prologue
    .line 479
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mSoftServiceBinder:Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;

    if-nez v0, :cond_0

    .line 480
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "VideoChatService is not bound"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 482
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mSoftServiceBinder:Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;

    iget-object v1, p2, Lcom/google/android/libraries/hangouts/video/RemoteRenderer;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    .line 484
    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/RendererManager;->getNativeContext()I

    move-result v1

    iget v2, p2, Lcom/google/android/libraries/hangouts/video/RemoteRenderer;->mRendererID:I

    .line 482
    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;->bindRenderer(III)V

    .line 486
    return-void
.end method

.method public blockMedia(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 434
    const-string v0, "com.google.android.libraries.hangouts.video.ACTION_BLOCK_MEDIA"

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/libraries/hangouts/video/VideoChat;->constructBaseIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 438
    const-string v1, "com.google.android.libraries.hangouts.video.media_blockee"

    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getEndpointMucJid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 439
    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->sendMessage(Landroid/content/Intent;)V

    .line 440
    return-void
.end method

.method getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getCurrentCall()Lcom/google/android/libraries/hangouts/video/CallState;
    .locals 1

    .prologue
    .line 175
    invoke-static {}, Lcwz;->a()V

    .line 176
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mSoftServiceBinder:Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mSoftServiceBinder:Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;->getCurrentCall()Lcom/google/android/libraries/hangouts/video/CallState;

    move-result-object v0

    .line 179
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLocalState()Lcom/google/android/libraries/hangouts/video/LocalState;
    .locals 1

    .prologue
    .line 203
    invoke-static {}, Lcwz;->a()V

    .line 204
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mSoftServiceBinder:Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;

    if-eqz v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mSoftServiceBinder:Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;->getLocalState()Lcom/google/android/libraries/hangouts/video/LocalState;

    move-result-object v0

    .line 207
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRecentPreviousCall()Lcom/google/android/libraries/hangouts/video/CallState;
    .locals 1

    .prologue
    .line 188
    invoke-static {}, Lcwz;->a()V

    .line 189
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mSoftServiceBinder:Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;

    if-eqz v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mSoftServiceBinder:Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;->getPreviousCall()Lcom/google/android/libraries/hangouts/video/CallState;

    move-result-object v0

    .line 192
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public handlePushNotification([B)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 298
    const-string v0, "com.google.android.libraries.hangouts.video.ACTION_PUSH_NOTIFICATION"

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/libraries/hangouts/video/VideoChat;->constructBaseIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 302
    const-string v1, "push_proto"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 303
    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->sendMessage(Landroid/content/Intent;)V

    .line 304
    return-void
.end method

.method public inviteConversationParticipants(IZ)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 307
    const/4 v1, 0x1

    new-array v2, v6, [Ljava/lang/String;

    new-array v3, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    move-object v0, p0

    move v4, p1

    move v5, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/libraries/hangouts/video/VideoChat;->inviteUsers(Z[Ljava/lang/String;[Ljava/lang/String;IZZLjava/lang/String;)V

    .line 308
    return-void
.end method

.method public invitePstn(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 354
    const-string v0, "com.google.android.libraries.hangouts.video.ACTION_INVITE_PSTN"

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/libraries/hangouts/video/VideoChat;->constructBaseIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 358
    const-string v1, "com.google.android.libraries.hangouts.video.pstn_jid"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 359
    const-string v1, "com.google.android.libraries.hangouts.video.phone_number"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 360
    const-string v1, "com.google.android.libraries.hangouts.video.keep_alive"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 361
    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->sendMessage(Landroid/content/Intent;)V

    .line 362
    return-void
.end method

.method public inviteUsers([Ljava/lang/String;[Ljava/lang/String;IZZLjava/lang/String;)V
    .locals 8

    .prologue
    .line 313
    const/4 v1, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/libraries/hangouts/video/VideoChat;->inviteUsers(Z[Ljava/lang/String;[Ljava/lang/String;IZZLjava/lang/String;)V

    .line 315
    return-void
.end method

.method public prepareCall(Lcom/google/android/libraries/hangouts/video/CallOptions;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 224
    const-string v0, "com.google.android.libraries.hangouts.video.ACTION_PREPARE_CALL"

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/libraries/hangouts/video/VideoChat;->constructBaseIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 228
    const-string v1, "call_options"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 229
    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->sendMessage(Landroid/content/Intent;)V

    .line 230
    return-void
.end method

.method public publishVideoMuteState(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 425
    const-string v0, "com.google.android.libraries.hangouts.video.ACTION_PUBLISH_VIDEO_MUTE_STATE"

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/libraries/hangouts/video/VideoChat;->constructBaseIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 429
    const-string v1, "com.google.android.libraries.hangouts.video.video_mute"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 430
    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->sendMessage(Landroid/content/Intent;)V

    .line 431
    return-void
.end method

.method public remoteMute(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 416
    const-string v0, "com.google.android.libraries.hangouts.video.ACTION_REMOTE_MUTE"

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/libraries/hangouts/video/VideoChat;->constructBaseIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 420
    const-string v1, "com.google.android.libraries.hangouts.video.remote_mutee"

    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getEndpointMucJid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 421
    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->sendMessage(Landroid/content/Intent;)V

    .line 422
    return-void
.end method

.method public removeListener(Lcom/google/android/libraries/hangouts/video/CallStateListener;)V
    .locals 1

    .prologue
    .line 164
    invoke-static {}, Lcwz;->a()V

    .line 165
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mListeners:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 166
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 167
    return-void
.end method

.method public requestVideoViews([Lcom/google/android/libraries/hangouts/video/VideoViewRequest;)V
    .locals 2

    .prologue
    .line 461
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mSoftServiceBinder:Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;

    if-nez v0, :cond_0

    .line 462
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "VideoChatService is not bound"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 464
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mSoftServiceBinder:Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;->requestVideoViews([Lcom/google/android/libraries/hangouts/video/VideoViewRequest;)V

    .line 465
    return-void
.end method

.method public respondToOngoingNotificationRequest(Landroid/app/Notification;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 540
    const-string v0, "com.google.android.libraries.hangouts.video.ACTION_ONGOING_NOTIFICATION_RESPONSE"

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/libraries/hangouts/video/VideoChat;->constructBaseIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 544
    const-string v1, "com.google.android.libraries.hangouts.video.ongoing_notif"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 545
    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->sendMessage(Landroid/content/Intent;)V

    .line 546
    return-void
.end method

.method public sendDtmf(CILjava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 372
    const-string v0, "vclib"

    const-string v1, "sendDtmf"

    invoke-static {v0, v1}, Lcxc;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    const-string v0, "com.google.android.libraries.hangouts.video.ACTION_SEND_DTMF"

    invoke-direct {p0, v0, v2, v2}, Lcom/google/android/libraries/hangouts/video/VideoChat;->constructBaseIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 377
    const-string v1, "com.google.android.libraries.hangouts.video.dtmf_code"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;C)Landroid/content/Intent;

    .line 378
    const-string v1, "com.google.android.libraries.hangouts.video.duration_millis"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 379
    const-string v1, "com.google.android.libraries.hangouts.video.remote_muc_jid"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 380
    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->sendMessage(Landroid/content/Intent;)V

    .line 381
    return-void
.end method

.method public setAudioDevice(Lcom/google/android/libraries/hangouts/video/AudioDevice;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 388
    const-string v0, "com.google.android.libraries.hangouts.video.ACTION_SET_AUDIO_DEVICE"

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/libraries/hangouts/video/VideoChat;->constructBaseIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 392
    const-string v1, "com.google.android.libraries.hangouts.video.audio_device"

    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/AudioDevice;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 393
    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->sendMessage(Landroid/content/Intent;)V

    .line 394
    return-void
.end method

.method public setCallTag(Ljava/lang/String;Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 507
    const-string v0, "com.google.android.libraries.hangouts.video.ACTION_SET_CALL_TAG"

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/libraries/hangouts/video/VideoChat;->constructBaseIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 511
    const-string v1, "com.google.android.libraries.hangouts.video.sessionid"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 512
    const-string v1, "com.google.android.libraries.hangouts.video.tag"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 513
    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->sendMessage(Landroid/content/Intent;)V

    .line 514
    return-void
.end method

.method public setMute(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 402
    const-string v0, "com.google.android.libraries.hangouts.video.ACTION_SET_MUTE"

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/libraries/hangouts/video/VideoChat;->constructBaseIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 406
    const-string v1, "com.google.android.libraries.hangouts.video.mute"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 407
    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->sendMessage(Landroid/content/Intent;)V

    .line 408
    return-void
.end method

.method public signIn(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 212
    const-string v0, "com.google.android.libraries.hangouts.video.ACTION_SIGN_IN"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/libraries/hangouts/video/VideoChat;->constructBaseIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 216
    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->sendMessage(Landroid/content/Intent;)V

    .line 217
    return-void
.end method

.method public startCall(Ljava/lang/String;Lcom/google/android/libraries/hangouts/video/HangoutRequest;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 244
    const-string v0, "com.google.android.libraries.hangouts.video.ACTION_START_CALL"

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/libraries/hangouts/video/VideoChat;->constructBaseIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 248
    const-string v1, "com.google.android.libraries.hangouts.video.sessionid"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 249
    const-string v1, "hangout_request"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 250
    const-string v1, "com.google.android.libraries.hangouts.video.gcm_registration"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 251
    const-string v1, "com.google.android.libraries.hangouts.video.android_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 252
    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->sendMessage(Landroid/content/Intent;)V

    .line 253
    return-void
.end method

.method public terminateCall(Ljava/lang/String;I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 263
    sparse-switch p2, :sswitch_data_0

    .line 286
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Illegal reason : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 276
    :sswitch_0
    const-string v0, "com.google.android.libraries.hangouts.video.ACTION_TERMINATE_CALL"

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/libraries/hangouts/video/VideoChat;->constructBaseIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 280
    const-string v1, "com.google.android.libraries.hangouts.video.sessionid"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 281
    const-string v1, "com.google.android.libraries.hangouts.video.terminate_reason"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 282
    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->sendMessage(Landroid/content/Intent;)V

    .line 283
    return-void

    .line 263
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x4 -> :sswitch_0
        0xb -> :sswitch_0
        0x3ec -> :sswitch_0
        0x3ed -> :sswitch_0
        0x3ee -> :sswitch_0
        0x3f2 -> :sswitch_0
        0x3f3 -> :sswitch_0
        0x3f4 -> :sswitch_0
        0x3f5 -> :sswitch_0
        0x3f6 -> :sswitch_0
        0x3f7 -> :sswitch_0
    .end sparse-switch
.end method

.method public unbindRenderer(I)V
    .locals 2

    .prologue
    .line 493
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mSoftServiceBinder:Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;

    if-nez v0, :cond_0

    .line 494
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "VideoChatService is not bound"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 496
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChat;->mSoftServiceBinder:Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;->unbindRenderer(I)V

    .line 497
    return-void
.end method

.method public updateJoinedNotification(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 522
    const-string v0, "com.google.android.libraries.hangouts.video.ACTION_ONGOING_NOTIFICATION_REQUEST"

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/libraries/hangouts/video/VideoChat;->constructBaseIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 526
    const-string v1, "com.google.android.libraries.hangouts.video.sessionid"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 527
    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->sendMessage(Landroid/content/Intent;)V

    .line 528
    return-void
.end method
