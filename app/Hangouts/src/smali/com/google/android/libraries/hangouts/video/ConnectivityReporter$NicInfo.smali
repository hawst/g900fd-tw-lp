.class public Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final externalIp:Ljava/lang/String;

.field private final http:Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;

.field private final https:Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;

.field private final localIp:Ljava/lang/String;

.field private final mediaServerIp:Ljava/lang/String;

.field private final proxyIp:Ljava/lang/String;

.field private final proxyType:I

.field private final ssltcp:Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;

.field private final stun:Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;

.field private final stunServerIp:Ljava/lang/String;

.field private final tcp:Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;

.field private final udp:Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->localIp:Ljava/lang/String;

    .line 64
    iput-object p2, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->externalIp:Ljava/lang/String;

    .line 65
    iput-object p3, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->stunServerIp:Ljava/lang/String;

    .line 66
    iput-object p4, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->mediaServerIp:Ljava/lang/String;

    .line 67
    iput-object p5, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->proxyIp:Ljava/lang/String;

    .line 68
    iput p6, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->proxyType:I

    .line 69
    iput-object p7, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->stun:Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;

    .line 70
    iput-object p8, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->http:Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;

    .line 71
    iput-object p9, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->https:Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;

    .line 72
    iput-object p10, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->udp:Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;

    .line 73
    iput-object p11, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->tcp:Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;

    .line 74
    iput-object p12, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->ssltcp:Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;

    .line 75
    return-void
.end method


# virtual methods
.method public toProto()Ldoo;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 95
    new-instance v1, Ldoo;

    invoke-direct {v1}, Ldoo;-><init>()V

    .line 97
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->localIp:Ljava/lang/String;

    iput-object v0, v1, Ldoo;->b:Ljava/lang/String;

    .line 98
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->externalIp:Ljava/lang/String;

    iput-object v0, v1, Ldoo;->c:Ljava/lang/String;

    .line 99
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->stunServerIp:Ljava/lang/String;

    iput-object v0, v1, Ldoo;->e:Ljava/lang/String;

    .line 100
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->mediaServerIp:Ljava/lang/String;

    iput-object v0, v1, Ldoo;->f:Ljava/lang/String;

    .line 101
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->proxyIp:Ljava/lang/String;

    iput-object v0, v1, Ldoo;->d:Ljava/lang/String;

    .line 102
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->proxyType:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoo;->g:Ljava/lang/Integer;

    .line 103
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->stun:Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;

    if-eqz v0, :cond_0

    .line 104
    new-array v0, v4, [Ldop;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->stun:Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;

    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;->toProto()Ldop;

    move-result-object v2

    aput-object v2, v0, v3

    # invokes: Lcom/google/android/libraries/hangouts/video/ConnectivityReporter;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter;->access$000([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldop;

    iput-object v0, v1, Ldoo;->i:[Ldop;

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->http:Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;

    if-eqz v0, :cond_1

    .line 108
    new-array v0, v4, [Ldop;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->http:Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;

    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;->toProto()Ldop;

    move-result-object v2

    aput-object v2, v0, v3

    # invokes: Lcom/google/android/libraries/hangouts/video/ConnectivityReporter;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter;->access$000([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldop;

    iput-object v0, v1, Ldoo;->j:[Ldop;

    .line 111
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->https:Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;

    if-eqz v0, :cond_2

    .line 112
    new-array v0, v4, [Ldop;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->https:Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;

    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;->toProto()Ldop;

    move-result-object v2

    aput-object v2, v0, v3

    # invokes: Lcom/google/android/libraries/hangouts/video/ConnectivityReporter;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter;->access$000([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldop;

    iput-object v0, v1, Ldoo;->k:[Ldop;

    .line 115
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->udp:Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;

    if-eqz v0, :cond_3

    .line 116
    new-array v0, v4, [Ldop;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->udp:Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;

    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;->toProto()Ldop;

    move-result-object v2

    aput-object v2, v0, v3

    # invokes: Lcom/google/android/libraries/hangouts/video/ConnectivityReporter;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter;->access$000([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldop;

    iput-object v0, v1, Ldoo;->l:[Ldop;

    .line 119
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->tcp:Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;

    if-eqz v0, :cond_4

    .line 120
    new-array v0, v4, [Ldop;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->tcp:Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;

    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;->toProto()Ldop;

    move-result-object v2

    aput-object v2, v0, v3

    # invokes: Lcom/google/android/libraries/hangouts/video/ConnectivityReporter;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter;->access$000([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldop;

    iput-object v0, v1, Ldoo;->m:[Ldop;

    .line 123
    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->ssltcp:Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;

    if-eqz v0, :cond_5

    .line 124
    new-array v0, v4, [Ldop;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->ssltcp:Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;

    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;->toProto()Ldop;

    move-result-object v2

    aput-object v2, v0, v3

    # invokes: Lcom/google/android/libraries/hangouts/video/ConnectivityReporter;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter;->access$000([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldop;

    iput-object v0, v1, Ldoo;->n:[Ldop;

    .line 127
    :cond_5
    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 79
    invoke-static {p0}, Lm;->a(Ljava/lang/Object;)Lebg;

    move-result-object v0

    const-string v1, "externalIp"

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->externalIp:Ljava/lang/String;

    .line 80
    invoke-virtual {v0, v1, v2}, Lebg;->a(Ljava/lang/String;Ljava/lang/Object;)Lebg;

    move-result-object v0

    const-string v1, "stunServerIp"

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->stunServerIp:Ljava/lang/String;

    .line 81
    invoke-virtual {v0, v1, v2}, Lebg;->a(Ljava/lang/String;Ljava/lang/Object;)Lebg;

    move-result-object v0

    const-string v1, "mediaServerIp"

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->mediaServerIp:Ljava/lang/String;

    .line 82
    invoke-virtual {v0, v1, v2}, Lebg;->a(Ljava/lang/String;Ljava/lang/Object;)Lebg;

    move-result-object v0

    const-string v1, "proxyIp"

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->proxyIp:Ljava/lang/String;

    .line 83
    invoke-virtual {v0, v1, v2}, Lebg;->a(Ljava/lang/String;Ljava/lang/Object;)Lebg;

    move-result-object v0

    const-string v1, "proxyType"

    iget v2, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->proxyType:I

    .line 84
    invoke-virtual {v0, v1, v2}, Lebg;->a(Ljava/lang/String;I)Lebg;

    move-result-object v0

    const-string v1, "stun"

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->stun:Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;

    .line 85
    invoke-virtual {v0, v1, v2}, Lebg;->a(Ljava/lang/String;Ljava/lang/Object;)Lebg;

    move-result-object v0

    const-string v1, "http"

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->http:Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;

    .line 86
    invoke-virtual {v0, v1, v2}, Lebg;->a(Ljava/lang/String;Ljava/lang/Object;)Lebg;

    move-result-object v0

    const-string v1, "https"

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->https:Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;

    .line 87
    invoke-virtual {v0, v1, v2}, Lebg;->a(Ljava/lang/String;Ljava/lang/Object;)Lebg;

    move-result-object v0

    const-string v1, "udp"

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->udp:Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;

    .line 88
    invoke-virtual {v0, v1, v2}, Lebg;->a(Ljava/lang/String;Ljava/lang/Object;)Lebg;

    move-result-object v0

    const-string v1, "tcp"

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->tcp:Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;

    .line 89
    invoke-virtual {v0, v1, v2}, Lebg;->a(Ljava/lang/String;Ljava/lang/Object;)Lebg;

    move-result-object v0

    const-string v1, "ssltcp"

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->ssltcp:Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;

    .line 90
    invoke-virtual {v0, v1, v2}, Lebg;->a(Ljava/lang/String;Ljava/lang/Object;)Lebg;

    move-result-object v0

    .line 91
    invoke-virtual {v0}, Lebg;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
