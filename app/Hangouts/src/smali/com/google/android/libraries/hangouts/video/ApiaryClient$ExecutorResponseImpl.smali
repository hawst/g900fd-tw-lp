.class Lcom/google/android/libraries/hangouts/video/ApiaryClient$ExecutorResponseImpl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/hangouts/video/ApiaryClient$ExecutorResponse;


# instance fields
.field private final mHttpResponse:Lcom/google/api/client/http/HttpResponse;


# direct methods
.method constructor <init>(Lcom/google/api/client/http/HttpResponse;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient$ExecutorResponseImpl;->mHttpResponse:Lcom/google/api/client/http/HttpResponse;

    .line 60
    return-void
.end method


# virtual methods
.method public getContent()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient$ExecutorResponseImpl;->mHttpResponse:Lcom/google/api/client/http/HttpResponse;

    invoke-virtual {v0}, Lcom/google/api/client/http/HttpResponse;->getContent()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public getFirstHeaderStringValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient$ExecutorResponseImpl;->mHttpResponse:Lcom/google/api/client/http/HttpResponse;

    invoke-virtual {v0}, Lcom/google/api/client/http/HttpResponse;->getHeaders()Lcom/google/api/client/http/HttpHeaders;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/api/client/http/HttpHeaders;->getFirstHeaderStringValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
