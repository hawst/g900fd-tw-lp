.class Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestTask;
.super Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/hangouts/video/SafeAsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "[B>;"
    }
.end annotation


# instance fields
.field private final mPath:Ljava/lang/String;

.field private final mRequest:[B

.field private final mRequestId:J

.field private final mTimeoutMillis:I

.field final synthetic this$0:Lcom/google/android/libraries/hangouts/video/ApiaryClient;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/hangouts/video/ApiaryClient;JLjava/lang/String;[BI)V
    .locals 2

    .prologue
    .line 95
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestTask;->this$0:Lcom/google/android/libraries/hangouts/video/ApiaryClient;

    .line 97
    sget-wide v0, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;->MAX_NETWORK_OPERATION_TIME_MILLIS:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;-><init>(J)V

    .line 98
    iput-wide p2, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestTask;->mRequestId:J

    .line 99
    iput-object p4, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestTask;->mPath:Ljava/lang/String;

    .line 100
    iput-object p5, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestTask;->mRequest:[B

    .line 101
    iput p6, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestTask;->mTimeoutMillis:I

    .line 102
    return-void
.end method

.method private handleProtoResponse(Lcom/google/android/libraries/hangouts/video/ApiaryClient$ExecutorResponse;)[B
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 138
    :try_start_0
    invoke-interface {p1}, Lcom/google/android/libraries/hangouts/video/ApiaryClient$ExecutorResponse;->getContent()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 140
    :try_start_1
    new-instance v3, Ljava/io/BufferedInputStream;

    invoke-direct {v3, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 141
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 143
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->read()I

    move-result v1

    .line 144
    :goto_0
    const/4 v5, -0x1

    if-eq v1, v5, :cond_0

    .line 145
    int-to-byte v1, v1

    .line 146
    invoke-virtual {v4, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 147
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->read()I

    move-result v1

    goto :goto_0

    .line 150
    :cond_0
    const-string v1, "X-Goog-Safety-Encoding"

    invoke-interface {p1, v1}, Lcom/google/android/libraries/hangouts/video/ApiaryClient$ExecutorResponse;->getFirstHeaderStringValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 151
    const-string v3, "base64"

    invoke-static {v1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 152
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    const/4 v3, 0x0

    invoke-static {v1, v3}, Landroid/util/Base64;->decode([BI)[B
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 160
    if-eqz v2, :cond_1

    .line 162
    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 166
    :cond_1
    :goto_1
    return-object v0

    .line 155
    :cond_2
    :try_start_3
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v0

    .line 160
    if-eqz v2, :cond_1

    .line 162
    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    goto :goto_1

    .line 156
    :catch_1
    move-exception v1

    move-object v2, v0

    .line 157
    :goto_2
    :try_start_5
    const-string v3, "vclib"

    const-string v4, "Error processing apiary response"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    invoke-static {v3, v4, v5}, Lcxc;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 158
    if-eqz v2, :cond_1

    .line 162
    :try_start_6
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_1

    :catch_2
    move-exception v1

    goto :goto_1

    .line 160
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_3
    if-eqz v2, :cond_3

    .line 162
    :try_start_7
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 166
    :cond_3
    :goto_4
    throw v0

    :catch_3
    move-exception v1

    goto :goto_1

    :catch_4
    move-exception v1

    goto :goto_4

    .line 160
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 156
    :catch_5
    move-exception v1

    goto :goto_2
.end method


# virtual methods
.method protected bridge synthetic doInBackgroundTimed([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 89
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestTask;->doInBackgroundTimed([Ljava/lang/Void;)[B

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackgroundTimed([Ljava/lang/Void;)[B
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v0, 0x0

    .line 106
    new-instance v1, Lczi;

    invoke-direct {v1}, Lczi;-><init>()V

    .line 107
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestTask;->this$0:Lcom/google/android/libraries/hangouts/video/ApiaryClient;

    # getter for: Lcom/google/android/libraries/hangouts/video/ApiaryClient;->mAuthToken:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/libraries/hangouts/video/ApiaryClient;->access$000(Lcom/google/android/libraries/hangouts/video/ApiaryClient;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lczi;->c(Ljava/lang/String;)Lczi;

    .line 109
    new-instance v1, Lczq;

    invoke-direct {v1}, Lczq;-><init>()V

    .line 110
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestTask;->this$0:Lcom/google/android/libraries/hangouts/video/ApiaryClient;

    .line 111
    # getter for: Lcom/google/android/libraries/hangouts/video/ApiaryClient;->mAuthToken:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/libraries/hangouts/video/ApiaryClient;->access$000(Lcom/google/android/libraries/hangouts/video/ApiaryClient;)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestTask;->mTimeoutMillis:I

    .line 110
    # invokes: Lcom/google/android/libraries/hangouts/video/ApiaryClient;->createHttpRequestInitializer(Lcom/google/android/libraries/hangouts/video/ApiaryApiInfo;Ljava/lang/String;Ljava/lang/Long;I)Lcom/google/api/client/http/HttpRequestInitializer;
    invoke-static {v0, v2, v0, v3}, Lcom/google/android/libraries/hangouts/video/ApiaryClient;->access$100(Lcom/google/android/libraries/hangouts/video/ApiaryApiInfo;Ljava/lang/String;Ljava/lang/Long;I)Lcom/google/api/client/http/HttpRequestInitializer;

    move-result-object v2

    .line 113
    invoke-virtual {v1, v2}, Lczq;->createRequestFactory(Lcom/google/api/client/http/HttpRequestInitializer;)Lcom/google/api/client/http/HttpRequestFactory;

    move-result-object v1

    .line 114
    new-instance v2, Lcom/google/android/libraries/hangouts/video/ApiaryClient$ApiaryHttpContent;

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestTask;->this$0:Lcom/google/android/libraries/hangouts/video/ApiaryClient;

    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestTask;->mRequest:[B

    invoke-direct {v2, v3, v4}, Lcom/google/android/libraries/hangouts/video/ApiaryClient$ApiaryHttpContent;-><init>(Lcom/google/android/libraries/hangouts/video/ApiaryClient;[B)V

    .line 118
    :try_start_0
    new-instance v3, Ljava/net/URL;

    new-instance v4, Ljava/net/URL;

    iget-object v5, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestTask;->this$0:Lcom/google/android/libraries/hangouts/video/ApiaryClient;

    # getter for: Lcom/google/android/libraries/hangouts/video/ApiaryClient;->mApiaryUri:Ljava/lang/String;
    invoke-static {v5}, Lcom/google/android/libraries/hangouts/video/ApiaryClient;->access$200(Lcom/google/android/libraries/hangouts/video/ApiaryClient;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestTask;->mPath:Ljava/lang/String;

    invoke-direct {v3, v4, v5}, Ljava/net/URL;-><init>(Ljava/net/URL;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 124
    new-instance v4, Lcom/google/api/client/http/GenericUrl;

    invoke-direct {v4, v3}, Lcom/google/api/client/http/GenericUrl;-><init>(Ljava/net/URL;)V

    .line 126
    :try_start_1
    invoke-virtual {v1, v4, v2}, Lcom/google/api/client/http/HttpRequestFactory;->buildPostRequest(Lcom/google/api/client/http/GenericUrl;Lcom/google/api/client/http/HttpContent;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v1

    .line 127
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestTask;->this$0:Lcom/google/android/libraries/hangouts/video/ApiaryClient;

    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/ApiaryClient;->getExecutor()Lcom/google/android/libraries/hangouts/video/ApiaryClient$HttpRequestExecutor;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/google/android/libraries/hangouts/video/ApiaryClient$HttpRequestExecutor;->execute(Lcom/google/api/client/http/HttpRequest;)Lcom/google/android/libraries/hangouts/video/ApiaryClient$ExecutorResponse;

    move-result-object v1

    .line 128
    invoke-direct {p0, v1}, Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestTask;->handleProtoResponse(Lcom/google/android/libraries/hangouts/video/ApiaryClient$ExecutorResponse;)[B
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 131
    :goto_0
    return-object v0

    .line 119
    :catch_0
    move-exception v1

    .line 120
    const-string v2, "vclib"

    const-string v3, "Error processing request url"

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v1, v4, v6

    invoke-static {v2, v3, v4}, Lcxc;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 129
    :catch_1
    move-exception v1

    .line 130
    const-string v2, "vclib"

    const-string v3, "Error making apiary request"

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v1, v4, v6

    invoke-static {v2, v3, v4}, Lcxc;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 89
    check-cast p1, [B

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestTask;->onPostExecute([B)V

    return-void
.end method

.method protected onPostExecute([B)V
    .locals 3

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestTask;->this$0:Lcom/google/android/libraries/hangouts/video/ApiaryClient;

    # getter for: Lcom/google/android/libraries/hangouts/video/ApiaryClient;->mListener:Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestListener;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/ApiaryClient;->access$300(Lcom/google/android/libraries/hangouts/video/ApiaryClient;)Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 174
    if-nez p1, :cond_1

    .line 175
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestTask;->this$0:Lcom/google/android/libraries/hangouts/video/ApiaryClient;

    # getter for: Lcom/google/android/libraries/hangouts/video/ApiaryClient;->mListener:Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestListener;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/ApiaryClient;->access$300(Lcom/google/android/libraries/hangouts/video/ApiaryClient;)Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestListener;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestTask;->mRequestId:J

    invoke-interface {v0, v1, v2}, Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestListener;->onRequestError(J)V

    .line 180
    :cond_0
    :goto_0
    return-void

    .line 177
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestTask;->this$0:Lcom/google/android/libraries/hangouts/video/ApiaryClient;

    # getter for: Lcom/google/android/libraries/hangouts/video/ApiaryClient;->mListener:Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestListener;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/ApiaryClient;->access$300(Lcom/google/android/libraries/hangouts/video/ApiaryClient;)Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestListener;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestTask;->mRequestId:J

    invoke-interface {v0, v1, v2, p1}, Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestListener;->onRequestCompleted(J[B)V

    goto :goto_0
.end method
