.class public Lcom/google/android/libraries/hangouts/video/Stats$BandwidthEstimationStats;
.super Lcom/google/android/libraries/hangouts/video/Stats;
.source "PG"


# instance fields
.field public final availableRecvBitrate:I

.field public final availableSendBitrate:I

.field public final retransmissionBitrate:I

.field public final transmissionBitrate:I


# direct methods
.method public constructor <init>(IIII)V
    .locals 0

    .prologue
    .line 430
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/Stats;-><init>()V

    .line 431
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/Stats$BandwidthEstimationStats;->availableSendBitrate:I

    .line 432
    iput p2, p0, Lcom/google/android/libraries/hangouts/video/Stats$BandwidthEstimationStats;->availableRecvBitrate:I

    .line 433
    iput p3, p0, Lcom/google/android/libraries/hangouts/video/Stats$BandwidthEstimationStats;->transmissionBitrate:I

    .line 434
    iput p4, p0, Lcom/google/android/libraries/hangouts/video/Stats$BandwidthEstimationStats;->retransmissionBitrate:I

    .line 435
    return-void
.end method

.method static printLegend(Ljava/io/PrintWriter;)V
    .locals 1

    .prologue
    .line 452
    const-string v0, "  BandwidthEstimation -- availSend, avilRecv, trans, retrans"

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 453
    return-void
.end method


# virtual methods
.method public addTo(Ldoe;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    .line 439
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/Stats;->createEmptyMediaProto(I)Ldoh;

    move-result-object v1

    .line 440
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->l:Ljava/lang/Integer;

    .line 441
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$BandwidthEstimationStats;->availableSendBitrate:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->A:Ljava/lang/Integer;

    .line 442
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$BandwidthEstimationStats;->availableRecvBitrate:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->B:Ljava/lang/Integer;

    .line 443
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$BandwidthEstimationStats;->transmissionBitrate:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->C:Ljava/lang/Integer;

    .line 444
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$BandwidthEstimationStats;->retransmissionBitrate:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->D:Ljava/lang/Integer;

    .line 446
    iget-object v0, p1, Ldoe;->c:[Ldoh;

    array-length v2, v0

    .line 447
    iget-object v0, p1, Ldoe;->c:[Ldoh;

    add-int/lit8 v3, v2, 0x1

    invoke-static {v0, v3}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldoh;

    iput-object v0, p1, Ldoe;->c:[Ldoh;

    .line 448
    iget-object v0, p1, Ldoe;->c:[Ldoh;

    aput-object v1, v0, v2

    .line 449
    return-void
.end method

.method public print(Ljava/io/PrintWriter;Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;)V
    .locals 2

    .prologue
    .line 457
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, " -- BandwidthEstimation -- "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$BandwidthEstimationStats;->availableSendBitrate:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$BandwidthEstimationStats;->availableRecvBitrate:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$BandwidthEstimationStats;->transmissionBitrate:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$BandwidthEstimationStats;->retransmissionBitrate:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 462
    return-void
.end method
