.class public Lcom/google/android/libraries/hangouts/video/ExitHistory;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final PREF_ERROR:Ljava/lang/String; = "HISTORY_ERROR"

.field private static final PREF_EXIT_REPORTED:Ljava/lang/String; = "HISTORY_EXIT_REPORTED"

.field private static final PREF_HAS_EVENT:Ljava/lang/String; = "HISTORY_HAS_EVENT"


# instance fields
.field private final mError:I

.field private final mExitReported:Z


# direct methods
.method private constructor <init>(IZ)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/ExitHistory;->mError:I

    .line 26
    iput-boolean p2, p0, Lcom/google/android/libraries/hangouts/video/ExitHistory;->mExitReported:Z

    .line 27
    return-void
.end method

.method private static findPrefs(Landroid/content/Context;Lcom/google/android/libraries/hangouts/video/HangoutRequest;)Landroid/content/SharedPreferences;
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 73
    const-class v1, Lcom/google/android/libraries/hangouts/video/ExitHistory;

    .line 74
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    .line 73
    invoke-virtual {p0, v1, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 75
    const-string v2, "HISTORY_HAS_EVENT"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_1

    .line 83
    :cond_0
    :goto_0
    return-object v0

    .line 79
    :cond_1
    invoke-static {v1}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->get(Landroid/content/SharedPreferences;)Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v2

    .line 80
    invoke-virtual {p1, v2}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 81
    goto :goto_0
.end method

.method public static getHistory(Landroid/content/Context;Lcom/google/android/libraries/hangouts/video/HangoutRequest;)Lcom/google/android/libraries/hangouts/video/ExitHistory;
    .locals 4

    .prologue
    .line 61
    invoke-static {p0, p1}, Lcom/google/android/libraries/hangouts/video/ExitHistory;->findPrefs(Landroid/content/Context;Lcom/google/android/libraries/hangouts/video/HangoutRequest;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 62
    if-eqz v0, :cond_0

    .line 63
    const-string v1, "HISTORY_ERROR"

    const/16 v2, 0x3ec

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 64
    const-string v2, "HISTORY_EXIT_REPORTED"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 65
    new-instance v0, Lcom/google/android/libraries/hangouts/video/ExitHistory;

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/hangouts/video/ExitHistory;-><init>(IZ)V

    .line 67
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static recordExit(Landroid/content/Context;Lcom/google/android/libraries/hangouts/video/HangoutRequest;IZ)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 39
    const/16 v0, 0x3f7

    invoke-static {p2, v2, v0}, Lcwz;->a(III)V

    .line 42
    const-class v0, Lcom/google/android/libraries/hangouts/video/ExitHistory;

    .line 43
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    .line 42
    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 43
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 44
    const-string v1, "HISTORY_HAS_EVENT"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 45
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->put(Landroid/content/SharedPreferences$Editor;)V

    .line 46
    const-string v1, "HISTORY_ERROR"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 47
    const-string v1, "HISTORY_EXIT_REPORTED"

    invoke-interface {v0, v1, p3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 48
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 49
    return-void
.end method

.method public static setExitReported(Landroid/content/Context;Lcom/google/android/libraries/hangouts/video/HangoutRequest;)V
    .locals 2

    .prologue
    .line 53
    const/16 v0, 0x3ec

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1}, Lcom/google/android/libraries/hangouts/video/ExitHistory;->recordExit(Landroid/content/Context;Lcom/google/android/libraries/hangouts/video/HangoutRequest;IZ)V

    .line 54
    return-void
.end method


# virtual methods
.method public exitReported()Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/ExitHistory;->mExitReported:Z

    return v0
.end method

.method public getEndCause()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/ExitHistory;->mError:I

    return v0
.end method
