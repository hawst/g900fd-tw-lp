.class final Lcom/google/android/libraries/hangouts/video/ApiaryClient$1;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/api/client/http/HttpRequestInitializer;


# instance fields
.field final synthetic val$authInitializer:Lczi;

.field final synthetic val$authTime:Ljava/lang/Long;

.field final synthetic val$authToken:Ljava/lang/String;

.field final synthetic val$timeout:I


# direct methods
.method constructor <init>(Ljava/lang/Long;Ljava/lang/String;Lczi;I)V
    .locals 0

    .prologue
    .line 229
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient$1;->val$authTime:Ljava/lang/Long;

    iput-object p2, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient$1;->val$authToken:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient$1;->val$authInitializer:Lczi;

    iput p4, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient$1;->val$timeout:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public initialize(Lcom/google/api/client/http/HttpRequest;)V
    .locals 5

    .prologue
    .line 252
    invoke-virtual {p1}, Lcom/google/api/client/http/HttpRequest;->getHeaders()Lcom/google/api/client/http/HttpHeaders;

    move-result-object v1

    const-string v2, "X-Auth-Time"

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient$1;->val$authTime:Ljava/lang/Long;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient$1;->val$authTime:Ljava/lang/Long;

    .line 253
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 252
    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/google/api/client/http/HttpHeaders;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient$1;->val$authToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 255
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient$1;->val$authInitializer:Lczi;

    invoke-virtual {v0, p1}, Lczi;->initialize(Lcom/google/api/client/http/HttpRequest;)V

    .line 258
    :cond_0
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient$1;->val$timeout:I

    invoke-virtual {p1, v0}, Lcom/google/api/client/http/HttpRequest;->setConnectTimeout(I)Lcom/google/api/client/http/HttpRequest;

    .line 259
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient$1;->val$timeout:I

    invoke-virtual {p1, v0}, Lcom/google/api/client/http/HttpRequest;->setReadTimeout(I)Lcom/google/api/client/http/HttpRequest;

    .line 260
    return-void

    .line 253
    :cond_1
    const-string v0, "none"

    goto :goto_0
.end method
