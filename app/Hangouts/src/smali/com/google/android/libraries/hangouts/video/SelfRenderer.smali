.class public abstract Lcom/google/android/libraries/hangouts/video/SelfRenderer;
.super Lcom/google/android/libraries/hangouts/video/Renderer;
.source "PG"


# static fields
.field private static final LANDSCAPE_ASPECT_RATIO:Lcom/google/android/libraries/hangouts/video/Size;

.field private static final PORTRAIT_ASPECT_RATIO:Lcom/google/android/libraries/hangouts/video/Size;

.field protected static final TAG:Ljava/lang/String; = "vclib"


# instance fields
.field protected final mCallback:Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;

.field protected mCameraCaptureFlip:Z

.field protected final mCameraInterface:Lcom/google/android/libraries/hangouts/video/CameraInterface;

.field protected mCameraOrientation:I

.field protected mCameraRotation:I

.field protected mCaptureSize:Lcom/google/android/libraries/hangouts/video/Size;

.field protected mDeviceOrientation:I

.field protected mEncodeRendererID:I

.field protected mFrameOutputParameters:Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;

.field protected mHaveInitializedCameraSettings:Z

.field protected mMaxFrameOutputParameters:Lcom/google/android/libraries/hangouts/video/Size;

.field protected mRotatedCameraBufferSize:Lcom/google/android/libraries/hangouts/video/Size;

.field protected mRotatedCaptureSize:Lcom/google/android/libraries/hangouts/video/Size;

.field protected mRotatedFrameOutputSize:Lcom/google/android/libraries/hangouts/video/Size;

.field protected mRotatedScaledCameraBufferSize:Lcom/google/android/libraries/hangouts/video/Size;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0x10

    .line 27
    new-instance v0, Lcom/google/android/libraries/hangouts/video/Size;

    const/16 v1, 0xa

    invoke-direct {v0, v2, v1}, Lcom/google/android/libraries/hangouts/video/Size;-><init>(II)V

    sput-object v0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->PORTRAIT_ASPECT_RATIO:Lcom/google/android/libraries/hangouts/video/Size;

    .line 28
    new-instance v0, Lcom/google/android/libraries/hangouts/video/Size;

    const/16 v1, 0x9

    invoke-direct {v0, v2, v1}, Lcom/google/android/libraries/hangouts/video/Size;-><init>(II)V

    sput-object v0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->LANDSCAPE_ASPECT_RATIO:Lcom/google/android/libraries/hangouts/video/Size;

    return-void
.end method

.method protected constructor <init>(Lcom/google/android/libraries/hangouts/video/RendererManager;Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;Lcom/google/android/libraries/hangouts/video/CameraSpecification;)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/Renderer;-><init>()V

    .line 34
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mEncodeRendererID:I

    .line 54
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    .line 55
    iput-object p2, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mCallback:Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;

    .line 56
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/CameraInterface;->getInstance()Lcom/google/android/libraries/hangouts/video/CameraInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mCameraInterface:Lcom/google/android/libraries/hangouts/video/CameraInterface;

    .line 59
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->getOutgoingNoEffectsVideoSpec()Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->getSize()Lcom/google/android/libraries/hangouts/video/Size;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mMaxFrameOutputParameters:Lcom/google/android/libraries/hangouts/video/Size;

    .line 60
    return-void
.end method

.method public static getDefaultAspectRatioSize(Z)Lcom/google/android/libraries/hangouts/video/Size;
    .locals 1

    .prologue
    .line 31
    if-eqz p0, :cond_0

    sget-object v0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->PORTRAIT_ASPECT_RATIO:Lcom/google/android/libraries/hangouts/video/Size;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->LANDSCAPE_ASPECT_RATIO:Lcom/google/android/libraries/hangouts/video/Size;

    goto :goto_0
.end method


# virtual methods
.method public abstract encodeFrame()V
.end method

.method protected abstract getCameraManager()Lcom/google/android/libraries/hangouts/video/CameraManager;
.end method

.method public getCurrentCameraId()I
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mCameraInterface:Lcom/google/android/libraries/hangouts/video/CameraInterface;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CameraInterface;->getCurrentCameraId()I

    move-result v0

    return v0
.end method

.method protected getFrameOutputParameters()Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;
    .locals 3

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mFrameOutputParameters:Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;

    if-nez v0, :cond_0

    .line 189
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->getCameraManager()Lcom/google/android/libraries/hangouts/video/CameraManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CameraManager;->getRequestedOutputParameters()Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 190
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->getCameraManager()Lcom/google/android/libraries/hangouts/video/CameraManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CameraManager;->getRequestedOutputParameters()Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mFrameOutputParameters:Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;

    .line 202
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mFrameOutputParameters:Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;

    return-object v0

    .line 194
    :cond_1
    invoke-static {}, Lcxc;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 195
    const-string v0, "vclib"

    const-string v1, "getFrameOutputParameters -- no frameOutputParams yet"

    invoke-static {v0, v1}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    :cond_2
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->getOutgoingWithEffectsVideoSpec()Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->getSize()Lcom/google/android/libraries/hangouts/video/Size;

    move-result-object v0

    .line 199
    new-instance v1, Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;

    const/4 v2, -0x1

    invoke-direct {v1, v0, v2}, Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;-><init>(Lcom/google/android/libraries/hangouts/video/Size;I)V

    iput-object v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mFrameOutputParameters:Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;

    goto :goto_0
.end method

.method public getRotatedFrameOutputSize()Lcom/google/android/libraries/hangouts/video/Size;
    .locals 2

    .prologue
    .line 177
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->getCurrentCameraId()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mRotatedFrameOutputSize:Lcom/google/android/libraries/hangouts/video/Size;

    if-nez v0, :cond_1

    .line 179
    :cond_0
    const/4 v0, 0x0

    .line 181
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/google/android/libraries/hangouts/video/Size;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mRotatedFrameOutputSize:Lcom/google/android/libraries/hangouts/video/Size;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/hangouts/video/Size;-><init>(Lcom/google/android/libraries/hangouts/video/Size;)V

    goto :goto_0
.end method

.method protected isPortrait()Z
    .locals 2

    .prologue
    .line 102
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mCameraRotation:I

    const/16 v1, 0x5a

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mCameraRotation:I

    const/16 v1, 0x10e

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract onCameraOpened(Lcom/google/android/libraries/hangouts/video/Size;IZ)V
.end method

.method protected onFrameSizesChanged()V
    .locals 4

    .prologue
    .line 115
    const-string v0, "vclib"

    const-string v1, "onFrameSizesChanged"

    invoke-static {v0, v1}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    invoke-static {}, Lcwz;->c()V

    .line 120
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->isPortrait()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->getDefaultAspectRatioSize(Z)Lcom/google/android/libraries/hangouts/video/Size;

    move-result-object v0

    .line 121
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->getFrameOutputParameters()Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;->size:Lcom/google/android/libraries/hangouts/video/Size;

    .line 120
    invoke-static {v0, v1}, Lcom/google/android/libraries/hangouts/video/Size;->scaleUpToFitInside(Lcom/google/android/libraries/hangouts/video/Size;Lcom/google/android/libraries/hangouts/video/Size;)Lcom/google/android/libraries/hangouts/video/Size;

    move-result-object v0

    .line 124
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mMaxFrameOutputParameters:Lcom/google/android/libraries/hangouts/video/Size;

    invoke-static {v0, v1}, Lcom/google/android/libraries/hangouts/video/Size;->scaleDownToFitInside(Lcom/google/android/libraries/hangouts/video/Size;Lcom/google/android/libraries/hangouts/video/Size;)Lcom/google/android/libraries/hangouts/video/Size;

    move-result-object v0

    .line 127
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->isPortrait()Z

    move-result v1

    if-nez v1, :cond_0

    .line 128
    new-instance v1, Lcom/google/android/libraries/hangouts/video/Size;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mCaptureSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v2, v2, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mCaptureSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v3, v3, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    invoke-direct {v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/Size;-><init>(II)V

    iput-object v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mRotatedCaptureSize:Lcom/google/android/libraries/hangouts/video/Size;

    .line 129
    new-instance v1, Lcom/google/android/libraries/hangouts/video/Size;

    iget v2, v0, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    iget v3, v0, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    invoke-direct {v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/Size;-><init>(II)V

    iput-object v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mRotatedFrameOutputSize:Lcom/google/android/libraries/hangouts/video/Size;

    .line 138
    :goto_0
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mRotatedFrameOutputSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mRotatedCaptureSize:Lcom/google/android/libraries/hangouts/video/Size;

    invoke-static {v1, v2}, Lcom/google/android/libraries/hangouts/video/Size;->scaleDownToFitInside(Lcom/google/android/libraries/hangouts/video/Size;Lcom/google/android/libraries/hangouts/video/Size;)Lcom/google/android/libraries/hangouts/video/Size;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mRotatedFrameOutputSize:Lcom/google/android/libraries/hangouts/video/Size;

    .line 142
    iget v1, v0, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    const/16 v2, 0x3c0

    if-lt v1, v2, :cond_1

    iget v1, v0, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    const/16 v2, 0x21c

    if-lt v1, v2, :cond_1

    .line 146
    new-instance v0, Lcom/google/android/libraries/hangouts/video/Size;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mRotatedFrameOutputSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v1, v1, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    and-int/lit8 v1, v1, -0x40

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mRotatedFrameOutputSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v2, v2, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    and-int/lit8 v2, v2, -0x2

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/hangouts/video/Size;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mRotatedFrameOutputSize:Lcom/google/android/libraries/hangouts/video/Size;

    .line 161
    :goto_1
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mRotatedCaptureSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v0, v0, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mRotatedCaptureSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v1, v1, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 163
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mRotatedFrameOutputSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v1, v1, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mRotatedFrameOutputSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v2, v2, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 166
    cmpg-float v0, v0, v1

    if-gez v0, :cond_3

    .line 168
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mRotatedFrameOutputSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v0, v0, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mRotatedCaptureSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v1, v1, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 173
    :goto_2
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mRotatedCaptureSize:Lcom/google/android/libraries/hangouts/video/Size;

    invoke-static {v1, v0}, Lcom/google/android/libraries/hangouts/video/Size;->newWithScale(Lcom/google/android/libraries/hangouts/video/Size;F)Lcom/google/android/libraries/hangouts/video/Size;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mRotatedScaledCameraBufferSize:Lcom/google/android/libraries/hangouts/video/Size;

    .line 174
    return-void

    .line 132
    :cond_0
    new-instance v1, Lcom/google/android/libraries/hangouts/video/Size;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mCaptureSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v2, v2, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mCaptureSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v3, v3, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    invoke-direct {v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/Size;-><init>(II)V

    iput-object v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mRotatedCaptureSize:Lcom/google/android/libraries/hangouts/video/Size;

    .line 133
    new-instance v1, Lcom/google/android/libraries/hangouts/video/Size;

    iget v2, v0, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    iget v3, v0, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    invoke-direct {v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/Size;-><init>(II)V

    iput-object v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mRotatedFrameOutputSize:Lcom/google/android/libraries/hangouts/video/Size;

    goto :goto_0

    .line 148
    :cond_1
    iget v1, v0, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    const/16 v2, 0x1e0

    if-lt v1, v2, :cond_2

    iget v0, v0, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    const/16 v1, 0x10e

    if-lt v0, v1, :cond_2

    .line 152
    new-instance v0, Lcom/google/android/libraries/hangouts/video/Size;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mRotatedFrameOutputSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v1, v1, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    and-int/lit8 v1, v1, -0x20

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mRotatedFrameOutputSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v2, v2, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    and-int/lit8 v2, v2, -0x2

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/hangouts/video/Size;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mRotatedFrameOutputSize:Lcom/google/android/libraries/hangouts/video/Size;

    goto :goto_1

    .line 157
    :cond_2
    new-instance v0, Lcom/google/android/libraries/hangouts/video/Size;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mRotatedFrameOutputSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v1, v1, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    and-int/lit8 v1, v1, -0x8

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mRotatedFrameOutputSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v2, v2, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    and-int/lit8 v2, v2, -0x2

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/hangouts/video/Size;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mRotatedFrameOutputSize:Lcom/google/android/libraries/hangouts/video/Size;

    goto :goto_1

    .line 171
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mRotatedFrameOutputSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v0, v0, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mRotatedCaptureSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v1, v1, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_2
.end method

.method protected recomputeCameraRotation()V
    .locals 3

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mCameraCaptureFlip:Z

    if-eqz v0, :cond_2

    .line 79
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mCameraOrientation:I

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mDeviceOrientation:I

    add-int/2addr v0, v1

    rem-int/lit16 v0, v0, 0x168

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mCameraRotation:I

    .line 80
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mCameraRotation:I

    rsub-int v0, v0, 0x168

    rem-int/lit16 v0, v0, 0x168

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mCameraRotation:I

    .line 92
    :cond_0
    :goto_0
    invoke-static {}, Lcxc;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 93
    const-string v0, "vclib"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ORIENT: mCameraOrientation: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mCameraOrientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mDeviceOrientation: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mDeviceOrientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mCameraRotation: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mCameraRotation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    :cond_1
    return-void

    .line 82
    :cond_2
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mCameraOrientation:I

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mDeviceOrientation:I

    sub-int/2addr v0, v1

    add-int/lit16 v0, v0, 0x168

    rem-int/lit16 v0, v0, 0x168

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mCameraRotation:I

    .line 87
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_0

    .line 88
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mCameraRotation:I

    rsub-int v0, v0, 0x168

    rem-int/lit16 v0, v0, 0x168

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mCameraRotation:I

    goto :goto_0
.end method

.method public setDeviceOrientation(I)V
    .locals 0

    .prologue
    .line 107
    invoke-static {}, Lcwz;->c()V

    .line 108
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mDeviceOrientation:I

    .line 109
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->recomputeCameraRotation()V

    .line 110
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->onFrameSizesChanged()V

    .line 111
    return-void
.end method

.method public suspendCamera()V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mCameraInterface:Lcom/google/android/libraries/hangouts/video/CameraInterface;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CameraInterface;->suspendCamera()V

    .line 73
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->getCameraManager()Lcom/google/android/libraries/hangouts/video/CameraManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/CameraManager;->setApplicationCallback(Lcom/google/android/libraries/hangouts/video/CameraManager$CameraManagerCallback;)V

    .line 74
    return-void
.end method

.method public useCamera(Lcom/google/android/libraries/hangouts/video/CameraSpecification;)V
    .locals 2

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->getCameraManager()Lcom/google/android/libraries/hangouts/video/CameraManager;

    move-result-object v0

    new-instance v1, Lcom/google/android/libraries/hangouts/video/SelfRenderer$CameraCallback;

    invoke-direct {v1, p0}, Lcom/google/android/libraries/hangouts/video/SelfRenderer$CameraCallback;-><init>(Lcom/google/android/libraries/hangouts/video/SelfRenderer;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/CameraManager;->setApplicationCallback(Lcom/google/android/libraries/hangouts/video/CameraManager$CameraManagerCallback;)V

    .line 68
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRenderer;->mCameraInterface:Lcom/google/android/libraries/hangouts/video/CameraInterface;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/CameraInterface;->useCamera(Lcom/google/android/libraries/hangouts/video/CameraSpecification;)V

    .line 69
    return-void
.end method
