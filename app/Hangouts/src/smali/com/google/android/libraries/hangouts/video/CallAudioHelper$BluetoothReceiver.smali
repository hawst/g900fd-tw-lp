.class Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;


# direct methods
.method private constructor <init>(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;Lcom/google/android/libraries/hangouts/video/CallAudioHelper$1;)V
    .locals 0

    .prologue
    .line 133
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;-><init>(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)V

    return-void
.end method

.method private onBluetoothDisconnected()V
    .locals 2

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$300(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->BLUETOOTH_ON:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    .line 215
    # getter for: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$300(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->BLUETOOTH_TURNING_ON:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    .line 216
    # getter for: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$300(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->BLUETOOTH_TURNING_OFF:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    if-ne v0, v1, :cond_1

    .line 217
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    # invokes: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->usePendingAudioDeviceState()V
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$1000(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)V

    .line 219
    :cond_1
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/16 v2, 0xa

    .line 136
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 137
    const-string v1, "android.bluetooth.headset.profile.action.AUDIO_STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 139
    const-string v0, "android.bluetooth.profile.extra.STATE"

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 141
    const/16 v1, 0xc

    if-ne v0, v1, :cond_1

    .line 142
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    const-string v1, "ACTION_AUDIO_STATE_CHANGED : STATE_AUDIO_CONNECTED"

    # invokes: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->log(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$100(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;Ljava/lang/String;)V

    .line 145
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->BLUETOOTH_ON:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    # setter for: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;
    invoke-static {v0, v1}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$302(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;Lcom/google/android/libraries/hangouts/video/AudioDeviceState;)Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    .line 146
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    # invokes: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->cancelBluetoothTimer()V
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$900(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)V

    .line 147
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    # invokes: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->reportUpdate()V
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$800(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)V

    .line 206
    :cond_0
    :goto_0
    return-void

    .line 148
    :cond_1
    if-ne v0, v2, :cond_0

    .line 149
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    const-string v1, "ACTION_AUDIO_STATE_CHANGED : STATE_AUDIO_DISCONNECTED"

    # invokes: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->log(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$100(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;Ljava/lang/String;)V

    .line 155
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;->isInitialStickyBroadcast()Z

    move-result v0

    if-nez v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    # invokes: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->cancelBluetoothTimer()V
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$900(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)V

    .line 158
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;->onBluetoothDisconnected()V

    .line 159
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    # invokes: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->reportUpdate()V
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$800(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)V

    goto :goto_0

    .line 163
    :cond_2
    const-string v1, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    const-string v0, "android.bluetooth.profile.extra.STATE"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 169
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "BluetoothReceiver.onReceive: got ACTION_CONNECTION_STATE_CHANGED, profileState="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", isInitialSticky="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 171
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;->isInitialStickyBroadcast()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 169
    # invokes: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->log(Ljava/lang/String;)V
    invoke-static {v0, v2}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$100(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;Ljava/lang/String;)V

    .line 173
    const-string v0, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 176
    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 190
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    const-string v1, "ACTION_CONNECTION_STATE_CHANGED : STATE_DISCONNECTED"

    # invokes: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->log(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$100(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;Ljava/lang/String;)V

    .line 194
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    # invokes: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->stopBluetoothSco()V
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$500(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)V

    .line 195
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;->onBluetoothDisconnected()V

    .line 196
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v0, v1}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$602(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;

    .line 199
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDevices:Ljava/util/Set;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$700(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDevice;->BLUETOOTH_HEADSET:Lcom/google/android/libraries/hangouts/video/AudioDevice;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDevices:Ljava/util/Set;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$700(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDevice;->BLUETOOTH_HEADSET:Lcom/google/android/libraries/hangouts/video/AudioDevice;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 201
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    # invokes: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->reportUpdate()V
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$800(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)V

    goto/16 :goto_0

    .line 178
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    const-string v2, "ACTION_CONNECTION_STATE_CHANGED : STATE_CONNECTED"

    # invokes: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->log(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$100(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;Ljava/lang/String;)V

    .line 180
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDevices:Ljava/util/Set;
    invoke-static {v1}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$700(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)Ljava/util/Set;

    move-result-object v1

    sget-object v2, Lcom/google/android/libraries/hangouts/video/AudioDevice;->BLUETOOTH_HEADSET:Lcom/google/android/libraries/hangouts/video/AudioDevice;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 181
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    # invokes: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->reportUpdate()V
    invoke-static {v1}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$800(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)V

    .line 183
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v1}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$600(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    if-nez v1, :cond_0

    .line 185
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    # setter for: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v1, v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$602(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;

    .line 186
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDevice;->BLUETOOTH_HEADSET:Lcom/google/android/libraries/hangouts/video/AudioDevice;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->setAudioDevice(Lcom/google/android/libraries/hangouts/video/AudioDevice;)V

    goto/16 :goto_0

    .line 176
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
