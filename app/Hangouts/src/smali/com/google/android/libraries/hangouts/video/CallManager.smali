.class final Lcom/google/android/libraries/hangouts/video/CallManager;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestListener;
.implements Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;


# static fields
.field private static final ACCOUNT_TYPE:Ljava/lang/String; = "com.google"

.field private static final CALL_TERMINATION_TIMEOUT_MILLIS:J = 0x3a98L

.field public static final HANGOUT_ACOUSTIC_ECHO_CANCELER_MODE:Ljava/lang/String; = "babel_hangout_aec_mode"

.field public static final HANGOUT_AUTOMATIC_GAIN_CONTROL_MODE:Ljava/lang/String; = "babel_hangout_agc_mode"

.field public static final HANGOUT_NOISE_SUPPRESSION_MODE:Ljava/lang/String; = "babel_hangout_ns_mode"

.field private static final TAG:Ljava/lang/String; = "vclib"

.field private static final XMPP_SERVER:Ljava/lang/String; = "talk.google.com"

.field private static sInstance:Lcom/google/android/libraries/hangouts/video/CallManager;

.field private static sInstanceLock:Ljava/lang/Object;


# instance fields
.field private mAndroidId:Ljava/lang/String;

.field private final mApiaryClient:Lcom/google/android/libraries/hangouts/video/ApiaryClient;

.field private mAuthToken:Ljava/lang/String;

.field private final mCallAudioHelper:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

.field private mCallBoundaryCallback:Lcom/google/android/libraries/hangouts/video/CallManager$CallBoundaryCallback;

.field private mCallEndSignalStrength:Ldog;

.field private final mCallStateListeners:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/libraries/hangouts/video/CallStateListener;",
            ">;"
        }
    .end annotation
.end field

.field private mCheckingConnectivity:Z

.field private final mConnectionMonitor:Lcom/google/android/libraries/hangouts/video/ConnectionMonitor;

.field private final mConnectivityManager:Landroid/net/ConnectivityManager;

.field private final mContext:Landroid/content/Context;

.field private mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

.field private final mEnsureCallTerminationRunnable:Ljava/lang/Runnable;

.field private mGcmRegistration:Ljava/lang/String;

.field private final mGlobalStatsAdapter:Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter;

.field private mInitialAudioUpdateBroadcast:Z

.field private final mLibjingle:Lcom/google/android/libraries/hangouts/video/Libjingle;

.field private final mLibjingleEventHandler:Lcom/google/android/libraries/hangouts/video/LibjingleEventHandler;

.field private final mLocalHandler:Landroid/os/Handler;

.field private final mLocalState:Lcom/google/android/libraries/hangouts/video/LocalState;

.field private mNetworkStateReceiver:Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;

.field private mPreviousCall:Lcom/google/android/libraries/hangouts/video/CallState;

.field private final mRegistrationComponents:Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;

.field private final mService:Lcom/google/android/libraries/hangouts/video/VideoChatService;

.field private mSigninTask:Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private final mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

.field private final mWifiManager:Landroid/net/wifi/WifiManager;

.field private mWifiStateReceiver:Lcom/google/android/libraries/hangouts/video/CallManager$WifiStateReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 243
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/libraries/hangouts/video/CallManager;->sInstanceLock:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/libraries/hangouts/video/VideoChatService;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/16 v1, 0x3e80

    .line 255
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 216
    new-instance v0, Lcom/google/android/libraries/hangouts/video/CallManager$1;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/hangouts/video/CallManager$1;-><init>(Lcom/google/android/libraries/hangouts/video/CallManager;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mEnsureCallTerminationRunnable:Ljava/lang/Runnable;

    .line 256
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mService:Lcom/google/android/libraries/hangouts/video/VideoChatService;

    .line 257
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mContext:Landroid/content/Context;

    .line 259
    new-instance v0, Lcom/google/android/libraries/hangouts/video/LibjingleEventHandler;

    invoke-direct {v0}, Lcom/google/android/libraries/hangouts/video/LibjingleEventHandler;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingleEventHandler:Lcom/google/android/libraries/hangouts/video/LibjingleEventHandler;

    .line 260
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalHandler:Landroid/os/Handler;

    .line 261
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingleEventHandler:Lcom/google/android/libraries/hangouts/video/LibjingleEventHandler;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/hangouts/video/LibjingleEventHandler;->setCallback(Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;)V

    .line 263
    new-instance v0, Lcom/google/android/libraries/hangouts/video/Libjingle;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingleEventHandler:Lcom/google/android/libraries/hangouts/video/LibjingleEventHandler;

    invoke-direct {v0, v2, v3}, Lcom/google/android/libraries/hangouts/video/Libjingle;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingle:Lcom/google/android/libraries/hangouts/video/Libjingle;

    .line 264
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/Registration;->getRegisteredComponents(Landroid/content/Context;)Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mRegistrationComponents:Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;

    .line 265
    new-instance v0, Lcom/google/android/libraries/hangouts/video/ApiaryClient;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mRegistrationComponents:Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;

    iget-object v2, v2, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->apiaryUri:Ljava/lang/String;

    invoke-direct {v0, v2}, Lcom/google/android/libraries/hangouts/video/ApiaryClient;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mApiaryClient:Lcom/google/android/libraries/hangouts/video/ApiaryClient;

    .line 268
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 269
    const-string v2, "babel_hangout_ns_mode"

    .line 270
    invoke-static {v0, v2}, Lcww;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "babel_hangout_aec_mode"

    .line 271
    invoke-static {v0, v3}, Lcww;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "babel_hangout_agc_mode"

    .line 272
    invoke-static {v0, v4}, Lcww;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 269
    invoke-static {v2, v3, v0}, Lorg/webrtc/voiceengine/AudioEffectsJB;->initialize(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingle:Lcom/google/android/libraries/hangouts/video/Libjingle;

    .line 274
    invoke-static {}, Lorg/webrtc/voiceengine/AudioEffectsJB;->shouldUseWebRTCNoiseSuppressor()Z

    move-result v2

    .line 273
    invoke-virtual {v0, v2}, Lcom/google/android/libraries/hangouts/video/Libjingle;->setUseWebRTCNoiseSuppressor(Z)V

    .line 275
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingle:Lcom/google/android/libraries/hangouts/video/Libjingle;

    .line 276
    invoke-static {}, Lorg/webrtc/voiceengine/AudioEffectsJB;->shouldUseWebRTCAcousticEchoCanceler()Z

    move-result v2

    .line 275
    invoke-virtual {v0, v2}, Lcom/google/android/libraries/hangouts/video/Libjingle;->setUseWebRTCAcousticEchoCanceler(Z)V

    .line 277
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingle:Lcom/google/android/libraries/hangouts/video/Libjingle;

    .line 278
    invoke-static {}, Lorg/webrtc/voiceengine/AudioEffectsJB;->shouldUseWebRTCAutomaticGainControl()Z

    move-result v2

    .line 277
    invoke-virtual {v0, v2}, Lcom/google/android/libraries/hangouts/video/Libjingle;->setUseWebRTCAutomaticGainControl(Z)V

    .line 281
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v0, v2, :cond_1

    .line 284
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mContext:Landroid/content/Context;

    const-string v2, "audio"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    const-string v2, "ec_supported"

    .line 285
    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 286
    if-eqz v0, :cond_1

    .line 287
    const-string v2, "="

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 288
    array-length v2, v0

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 289
    const/4 v2, 0x1

    aget-object v0, v0, v2

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 290
    const-string v2, "yes"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "true"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "1"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 295
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingle:Lcom/google/android/libraries/hangouts/video/Libjingle;

    invoke-virtual {v0, v6}, Lcom/google/android/libraries/hangouts/video/Libjingle;->setUseWebRTCAcousticEchoCanceler(Z)V

    .line 302
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v0, v2, :cond_3

    .line 303
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mContext:Landroid/content/Context;

    const-string v2, "audio"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 304
    const-string v2, "android.media.property.OUTPUT_SAMPLE_RATE"

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 305
    if-eqz v0, :cond_3

    .line 306
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 307
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 311
    :goto_0
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingle:Lcom/google/android/libraries/hangouts/video/Libjingle;

    invoke-virtual {v2, v1}, Lcom/google/android/libraries/hangouts/video/Libjingle;->setRecordingSampleRate(I)V

    .line 312
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingle:Lcom/google/android/libraries/hangouts/video/Libjingle;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/hangouts/video/Libjingle;->setPlayoutSampleRate(I)V

    .line 314
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_2

    .line 315
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingle:Lcom/google/android/libraries/hangouts/video/Libjingle;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/Libjingle;->setRecordingDevice(I)V

    .line 320
    :goto_1
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingle:Lcom/google/android/libraries/hangouts/video/Libjingle;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mRegistrationComponents:Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;

    iget-object v1, v1, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->jidResourcePrefix:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mRegistrationComponents:Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;

    iget-object v2, v2, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->clientVersion:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mRegistrationComponents:Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;

    iget-object v3, v3, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->overides:[[Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mRegistrationComponents:Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;

    iget-boolean v4, v4, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->useHardwareDecode:Z

    iget-object v5, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mRegistrationComponents:Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;

    iget-object v5, v5, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->rawLogDirectory:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/libraries/hangouts/video/Libjingle;->init(Ljava/lang/String;Ljava/lang/String;[[Ljava/lang/String;ZLjava/lang/String;)V

    .line 325
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCallStateListeners:Ljava/util/LinkedList;

    .line 326
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mContext:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    .line 328
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mContext:Landroid/content/Context;

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 330
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mWifiManager:Landroid/net/wifi/WifiManager;

    const/4 v1, 0x3

    const-string v2, "VideoChatWifiLock"

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/WifiManager;->createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    .line 333
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0, v6}, Landroid/net/wifi/WifiManager$WifiLock;->setReferenceCounted(Z)V

    .line 334
    new-instance v0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalHandler:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/libraries/hangouts/video/CallManager$2;

    invoke-direct {v3, p0}, Lcom/google/android/libraries/hangouts/video/CallManager$2;-><init>(Lcom/google/android/libraries/hangouts/video/CallManager;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;-><init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCallAudioHelper:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    .line 342
    new-instance v0, Lcom/google/android/libraries/hangouts/video/LocalState;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCallAudioHelper:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->getAudioDeviceState()Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCallAudioHelper:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    .line 343
    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->getAudioDevices()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/hangouts/video/LocalState;-><init>(Lcom/google/android/libraries/hangouts/video/AudioDeviceState;Ljava/util/Set;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalState:Lcom/google/android/libraries/hangouts/video/LocalState;

    .line 345
    new-instance v0, Lcom/google/android/libraries/hangouts/video/ConnectionMonitor;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/hangouts/video/ConnectionMonitor;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mConnectionMonitor:Lcom/google/android/libraries/hangouts/video/ConnectionMonitor;

    .line 346
    new-instance v0, Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mGlobalStatsAdapter:Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter;

    .line 347
    return-void

    .line 317
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingle:Lcom/google/android/libraries/hangouts/video/Libjingle;

    invoke-virtual {v0, v6}, Lcom/google/android/libraries/hangouts/video/Libjingle;->setRecordingDevice(I)V

    goto/16 :goto_1

    :cond_3
    move v0, v1

    goto/16 :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/libraries/hangouts/video/CallManager;)V
    .locals 0

    .prologue
    .line 139
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->onAudioUpdated()V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/libraries/hangouts/video/CallManager;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mAndroidId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/libraries/hangouts/video/CallManager;)Lcom/google/android/libraries/hangouts/video/Libjingle;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingle:Lcom/google/android/libraries/hangouts/video/Libjingle;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/libraries/hangouts/video/CallManager;)Lcom/google/android/libraries/hangouts/video/ApiaryClient;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mApiaryClient:Lcom/google/android/libraries/hangouts/video/ApiaryClient;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/libraries/hangouts/video/CallManager;)V
    .locals 0

    .prologue
    .line 139
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->reset()V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/libraries/hangouts/video/CallManager;)Ljava/util/List;
    .locals 1

    .prologue
    .line 139
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->getClonedListeners()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/libraries/hangouts/video/CallManager;)Lcom/google/android/libraries/hangouts/video/CallState;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/libraries/hangouts/video/CallManager;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/EndpointEvent;)V
    .locals 0

    .prologue
    .line 139
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/hangouts/video/CallManager;->broadcastEndpointEvent(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/EndpointEvent;)V

    return-void
.end method

.method static synthetic access$1700(Lcom/google/android/libraries/hangouts/video/CallManager;)Z
    .locals 1

    .prologue
    .line 139
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->isPreparingOrInCall()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1800(Lcom/google/android/libraries/hangouts/video/CallManager;)Lcom/google/android/libraries/hangouts/video/LocalState;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalState:Lcom/google/android/libraries/hangouts/video/LocalState;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/android/libraries/hangouts/video/CallManager;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/google/android/libraries/hangouts/video/CallManager;)Z
    .locals 1

    .prologue
    .line 139
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->shouldAttemptReconnect()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2200(Lcom/google/android/libraries/hangouts/video/CallManager;)V
    .locals 0

    .prologue
    .line 139
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->handleNetworkDisconnect()V

    return-void
.end method

.method static synthetic access$300(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 139
    invoke-static {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->lookupHost(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/libraries/hangouts/video/CallManager;)Landroid/net/ConnectivityManager;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/libraries/hangouts/video/CallManager;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/android/libraries/hangouts/video/CallManager;Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;)Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mSigninTask:Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;

    return-object p1
.end method

.method static synthetic access$700(Lcom/google/android/libraries/hangouts/video/CallManager;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mAuthToken:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/android/libraries/hangouts/video/CallManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mAuthToken:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$800(Lcom/google/android/libraries/hangouts/video/CallManager;)Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mRegistrationComponents:Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/libraries/hangouts/video/CallManager;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mGcmRegistration:Ljava/lang/String;

    return-object v0
.end method

.method private acquireWakeLocks(Landroid/net/NetworkInfo;)V
    .locals 3

    .prologue
    .line 562
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-static {v0}, Lcwz;->a(Ljava/lang/Object;)V

    .line 563
    invoke-static {}, Lcwz;->a()V

    .line 564
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mContext:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 565
    const v1, 0x20000001

    const-string v2, "vclib"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 567
    const-string v0, "vclib"

    const-string v1, "Acquiring WakeLock"

    invoke-static {v0, v1}, Lcxc;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 568
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 570
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 571
    const-string v0, "vclib"

    const-string v1, "Acquiring WiFi lock"

    invoke-static {v0, v1}, Lcxc;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 572
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    .line 574
    :cond_0
    return-void
.end method

.method private broadcastEndpointEvent(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/EndpointEvent;)V
    .locals 2

    .prologue
    .line 1095
    invoke-static {}, Lcwz;->a()V

    .line 1096
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->getClonedListeners()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStateListener;

    .line 1097
    invoke-interface {v0, p1, p2}, Lcom/google/android/libraries/hangouts/video/CallStateListener;->onEndpointEvent(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/EndpointEvent;)V

    goto :goto_0

    .line 1099
    :cond_0
    return-void
.end method

.method private endCallAndSignOut()V
    .locals 3

    .prologue
    .line 754
    invoke-static {}, Lcwz;->a()V

    .line 755
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalState:Lcom/google/android/libraries/hangouts/video/LocalState;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/LocalState;->getSigninState()I

    move-result v0

    if-nez v0, :cond_0

    .line 756
    const-string v0, "vclib"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ignoring disconnectAndSignout call for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalState:Lcom/google/android/libraries/hangouts/video/LocalState;

    .line 757
    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/LocalState;->getAccountName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", state="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalState:Lcom/google/android/libraries/hangouts/video/LocalState;

    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/LocalState;->getSigninState()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 756
    invoke-static {v0, v1}, Lcxc;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 763
    :goto_0
    return-void

    .line 760
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalState:Lcom/google/android/libraries/hangouts/video/LocalState;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/LocalState;->setSigninState(I)V

    .line 761
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalState:Lcom/google/android/libraries/hangouts/video/LocalState;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/LocalState;->setSignalingNetworkType(I)V

    .line 762
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingle:Lcom/google/android/libraries/hangouts/video/Libjingle;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalState:Lcom/google/android/libraries/hangouts/video/LocalState;

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/LocalState;->getAccountName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/Libjingle;->endCallAndSignOut(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getClonedListeners()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/hangouts/video/CallStateListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 938
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCallStateListeners:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 939
    return-object v0
.end method

.method private getEndpointMucJid(Lcom/google/android/libraries/hangouts/video/NamedSource;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1102
    invoke-static {}, Lcwz;->a()V

    .line 1105
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    if-eqz v0, :cond_0

    .line 1106
    const-string v0, "%s/%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v3}, Lcom/google/android/libraries/hangouts/video/CallState;->getRemoteBareJid()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p1, Lcom/google/android/libraries/hangouts/video/NamedSource;->nick:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1108
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getInstance(Lcom/google/android/libraries/hangouts/video/VideoChatService;)Lcom/google/android/libraries/hangouts/video/CallManager;
    .locals 2

    .prologue
    .line 246
    invoke-static {}, Lcwz;->a()V

    .line 247
    sget-object v1, Lcom/google/android/libraries/hangouts/video/CallManager;->sInstanceLock:Ljava/lang/Object;

    monitor-enter v1

    .line 248
    :try_start_0
    sget-object v0, Lcom/google/android/libraries/hangouts/video/CallManager;->sInstance:Lcom/google/android/libraries/hangouts/video/CallManager;

    if-nez v0, :cond_0

    .line 249
    new-instance v0, Lcom/google/android/libraries/hangouts/video/CallManager;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/hangouts/video/CallManager;-><init>(Lcom/google/android/libraries/hangouts/video/VideoChatService;)V

    sput-object v0, Lcom/google/android/libraries/hangouts/video/CallManager;->sInstance:Lcom/google/android/libraries/hangouts/video/CallManager;

    .line 251
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 252
    sget-object v0, Lcom/google/android/libraries/hangouts/video/CallManager;->sInstance:Lcom/google/android/libraries/hangouts/video/CallManager;

    return-object v0

    .line 251
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private handleMediaSourcesUpdate([Lcom/google/android/libraries/hangouts/video/NamedSource;I)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1129
    invoke-static {}, Lcwz;->a()V

    .line 1130
    array-length v3, p1

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v0, p1, v1

    .line 1131
    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/CallManager;->getEndpointMucJid(Lcom/google/android/libraries/hangouts/video/NamedSource;)Ljava/lang/String;

    move-result-object v4

    .line 1134
    if-eqz v4, :cond_0

    iget-object v5, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    iget-object v5, v5, Lcom/google/android/libraries/hangouts/video/CallState;->mSelf:Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    iget-object v5, v5, Lcom/google/android/libraries/hangouts/video/CallState;->mSelf:Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    .line 1135
    invoke-virtual {v5}, Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;->getEndpointMucJid()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1136
    :cond_0
    iget-object v5, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v5, v4}, Lcom/google/android/libraries/hangouts/video/CallState;->getRemoteEndpoint(Ljava/lang/String;)Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    move-result-object v5

    .line 1140
    if-nez v5, :cond_2

    .line 1143
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, "Got media update before endpoint enter event: "

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v4, v2, [Ljava/lang/Object;

    invoke-direct {p0, v0, v4}, Lcom/google/android/libraries/hangouts/video/CallManager;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1130
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1147
    :cond_2
    packed-switch p2, :pswitch_data_0

    .line 1161
    const-string v4, "Unexpected MediaSourceEvent type"

    invoke-static {v4}, Lcwz;->a(Ljava/lang/String;)V

    .line 1165
    :goto_1
    new-instance v4, Lcom/google/android/libraries/hangouts/video/endpoint/MediaSourceEvent;

    iget v0, v0, Lcom/google/android/libraries/hangouts/video/NamedSource;->ssrc:I

    invoke-direct {v4, p2, v0}, Lcom/google/android/libraries/hangouts/video/endpoint/MediaSourceEvent;-><init>(II)V

    .line 1166
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->getClonedListeners()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStateListener;

    .line 1167
    invoke-interface {v0, v5, v4}, Lcom/google/android/libraries/hangouts/video/CallStateListener;->onEndpointEvent(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/EndpointEvent;)V

    goto :goto_2

    .line 1149
    :pswitch_0
    iget v4, v0, Lcom/google/android/libraries/hangouts/video/NamedSource;->ssrc:I

    invoke-virtual {v5, v4}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->addAudioSsrc(I)V

    goto :goto_1

    .line 1152
    :pswitch_1
    iget v4, v0, Lcom/google/android/libraries/hangouts/video/NamedSource;->ssrc:I

    invoke-virtual {v5, v4}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->removeAudioSsrc(I)V

    goto :goto_1

    .line 1155
    :pswitch_2
    iget v4, v0, Lcom/google/android/libraries/hangouts/video/NamedSource;->ssrc:I

    invoke-virtual {v5, v4}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->addVideoSsrc(I)V

    goto :goto_1

    .line 1158
    :pswitch_3
    iget v4, v0, Lcom/google/android/libraries/hangouts/video/NamedSource;->ssrc:I

    invoke-virtual {v5, v4}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->removeVideoSsrc(I)V

    goto :goto_1

    .line 1170
    :cond_3
    return-void

    .line 1147
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private handleNetworkDisconnect()V
    .locals 2

    .prologue
    .line 1664
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    if-eqz v0, :cond_0

    .line 1665
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/CallState;->setMediaError(I)V

    .line 1666
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    iget-object v0, v0, Lcom/google/android/libraries/hangouts/video/CallState;->mSessionId:Ljava/lang/String;

    const/16 v1, 0x3eb

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/hangouts/video/CallManager;->terminateCallInternal(Ljava/lang/String;I)Z

    .line 1669
    :cond_0
    return-void
.end method

.method private initiateCallWithState()V
    .locals 3

    .prologue
    .line 553
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    invoke-static {v0}, Lcwz;->a(Z)V

    .line 554
    invoke-static {}, Lcwz;->a()V

    .line 556
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "initiateCall for "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    iget-object v1, v1, Lcom/google/android/libraries/hangouts/video/CallState;->mSessionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/hangouts/video/CallManager;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 558
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingle:Lcom/google/android/libraries/hangouts/video/Libjingle;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/CallState;->getCallOptions()Lcom/google/android/libraries/hangouts/video/CallOptions;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    iget-object v2, v2, Lcom/google/android/libraries/hangouts/video/CallState;->mHangoutRequest:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/hangouts/video/Libjingle;->initiateHangoutCall(Lcom/google/android/libraries/hangouts/video/CallOptions;Lcom/google/android/libraries/hangouts/video/HangoutRequest;)V

    .line 559
    return-void
.end method

.method private isCurrentCall(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 458
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    iget-object v0, v0, Lcom/google/android/libraries/hangouts/video/CallState;->mSessionId:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isInitialized()Z
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingle:Lcom/google/android/libraries/hangouts/video/Libjingle;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/Libjingle;->isInitialized()Z

    move-result v0

    return v0
.end method

.method private isMediaConnected()Z
    .locals 1

    .prologue
    .line 448
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->isMediaConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isPreparingOrInCall()Z
    .locals 1

    .prologue
    .line 444
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private varargs log(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1692
    invoke-static {}, Lcxc;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1693
    const-string v0, "vclib"

    invoke-static {p1, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcxc;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1695
    :cond_0
    return-void
.end method

.method private static lookupHost(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 653
    :try_start_0
    invoke-static {p0}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 659
    invoke-virtual {v0}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v0

    .line 660
    const/4 v1, 0x3

    aget-byte v1, v0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x18

    const/4 v2, 0x2

    aget-byte v2, v0, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x10

    or-int/2addr v1, v2

    const/4 v2, 0x1

    aget-byte v2, v0, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v1, v2

    const/4 v2, 0x0

    aget-byte v0, v0, v2

    and-int/lit16 v0, v0, 0xff

    or-int/2addr v0, v1

    .line 664
    :goto_0
    return v0

    .line 655
    :catch_0
    move-exception v0

    const/4 v0, -0x1

    goto :goto_0
.end method

.method private onAudioUpdated()V
    .locals 4

    .prologue
    .line 912
    invoke-static {}, Lcwz;->a()V

    .line 913
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalState:Lcom/google/android/libraries/hangouts/video/LocalState;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCallAudioHelper:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->getAudioDeviceState()Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCallAudioHelper:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    .line 914
    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->getAudioDevices()Ljava/util/Set;

    move-result-object v2

    .line 913
    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/hangouts/video/LocalState;->setAudioState(Lcom/google/android/libraries/hangouts/video/AudioDeviceState;Ljava/util/Set;)V

    .line 915
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->getClonedListeners()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStateListener;

    .line 916
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalState:Lcom/google/android/libraries/hangouts/video/LocalState;

    iget-object v2, v2, Lcom/google/android/libraries/hangouts/video/LocalState;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalState:Lcom/google/android/libraries/hangouts/video/LocalState;

    iget-object v3, v3, Lcom/google/android/libraries/hangouts/video/LocalState;->mAvailableAudioDevices:Ljava/util/Set;

    invoke-interface {v0, v2, v3}, Lcom/google/android/libraries/hangouts/video/CallStateListener;->onAudioUpdated(Lcom/google/android/libraries/hangouts/video/AudioDeviceState;Ljava/util/Set;)V

    goto :goto_0

    .line 919
    :cond_0
    return-void
.end method

.method private onCallConnecting()V
    .locals 1

    .prologue
    .line 1287
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    iget-object v0, v0, Lcom/google/android/libraries/hangouts/video/CallState;->mCallStatistics:Lcom/google/android/libraries/hangouts/video/CallStatistics;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallStatistics;->onCallAccepted()V

    .line 1288
    return-void
.end method

.method private releaseWakeLocks()V
    .locals 2

    .prologue
    .line 577
    invoke-static {}, Lcwz;->a()V

    .line 578
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    .line 579
    const-string v0, "vclib"

    const-string v1, "Releasing WakeLock"

    invoke-static {v0, v1}, Lcxc;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 580
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 581
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 584
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 585
    const-string v0, "vclib"

    const-string v1, "Releasing WiFi lock"

    invoke-static {v0, v1}, Lcxc;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 586
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mWifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    .line 590
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mService:Lcom/google/android/libraries/hangouts/video/VideoChatService;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->getMobileHipriManager()Lcom/google/android/libraries/hangouts/video/MobileHipriManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/MobileHipriManager;->stopUsingMobileHipri()V

    .line 591
    return-void
.end method

.method private reset()V
    .locals 2

    .prologue
    .line 1518
    invoke-static {}, Lcwz;->a()V

    .line 1519
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->endCallAndSignOut()V

    .line 1520
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->resetLocalStateIfNeeded()V

    .line 1521
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingleEventHandler:Lcom/google/android/libraries/hangouts/video/LibjingleEventHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/LibjingleEventHandler;->setCallback(Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;)V

    .line 1522
    return-void
.end method

.method private resetCall()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1497
    iput-object v3, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCallEndSignalStrength:Ldog;

    .line 1500
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCallBoundaryCallback:Lcom/google/android/libraries/hangouts/video/CallManager$CallBoundaryCallback;

    if-eqz v0, :cond_0

    .line 1501
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCallBoundaryCallback:Lcom/google/android/libraries/hangouts/video/CallManager$CallBoundaryCallback;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/hangouts/video/CallManager$CallBoundaryCallback;->onCallEnd(Lcom/google/android/libraries/hangouts/video/CallState;)V

    .line 1503
    :cond_0
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->getClonedListeners()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStateListener;

    .line 1504
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-interface {v0, v2}, Lcom/google/android/libraries/hangouts/video/CallStateListener;->onCallEnded(Lcom/google/android/libraries/hangouts/video/CallState;)V

    goto :goto_0

    .line 1506
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->shouldManagePlatformInteraction()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1507
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCallAudioHelper:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->deinitAudio()V

    .line 1509
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mEnsureCallTerminationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1510
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mPreviousCall:Lcom/google/android/libraries/hangouts/video/CallState;

    .line 1511
    iput-object v3, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    .line 1512
    return-void
.end method

.method private resetCallWithEndCause(I)V
    .locals 1

    .prologue
    .line 1492
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/CallState;->setEndCauseIfUnset(I)V

    .line 1493
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->resetCall()V

    .line 1494
    return-void
.end method

.method private resetLocalStateIfNeeded()V
    .locals 2

    .prologue
    .line 1525
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalState:Lcom/google/android/libraries/hangouts/video/LocalState;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/LocalState;->getSigninState()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1526
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mService:Lcom/google/android/libraries/hangouts/video/VideoChatService;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalState:Lcom/google/android/libraries/hangouts/video/LocalState;

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/LocalState;->getAccountName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->onConnectionClosed(Ljava/lang/String;)V

    .line 1527
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalState:Lcom/google/android/libraries/hangouts/video/LocalState;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/LocalState;->setSigninState(I)V

    .line 1529
    :cond_0
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->releaseWakeLocks()V

    .line 1530
    return-void
.end method

.method private setNetworkType(Landroid/net/NetworkInfo;)V
    .locals 3

    .prologue
    .line 595
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mService:Lcom/google/android/libraries/hangouts/video/VideoChatService;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->getMobileHipriManager()Lcom/google/android/libraries/hangouts/video/MobileHipriManager;

    move-result-object v0

    .line 596
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/MobileHipriManager;->startUsingMobileHipriIfOnMobileNetwork()Z

    move-result v1

    .line 597
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalState:Lcom/google/android/libraries/hangouts/video/LocalState;

    if-eqz v1, :cond_1

    const/4 v0, 0x5

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/libraries/hangouts/video/LocalState;->setSignalingNetworkType(I)V

    .line 601
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mNetworkStateReceiver:Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;

    if-eqz v0, :cond_0

    .line 602
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mNetworkStateReceiver:Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;

    new-instance v1, Lcom/google/android/libraries/hangouts/video/CallManager$3;

    invoke-direct {v1, p0}, Lcom/google/android/libraries/hangouts/video/CallManager$3;-><init>(Lcom/google/android/libraries/hangouts/video/CallManager;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;->setMobileHipriConnectedRunnable(Ljava/lang/Runnable;)V

    .line 614
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalState:Lcom/google/android/libraries/hangouts/video/LocalState;

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/LocalState;->getSignalingNetworkType()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/CallState;->setMediaNetworkType(I)V

    .line 615
    return-void

    .line 599
    :cond_1
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    goto :goto_0
.end method

.method private shouldAttemptReconnect()Z
    .locals 1

    .prologue
    .line 1660
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    iget-object v0, v0, Lcom/google/android/libraries/hangouts/video/CallState;->mHangoutRequest:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private shouldCheckConnectivity(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1310
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mRegistrationComponents:Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;

    iget v1, v1, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->checkConnectivity:I

    packed-switch v1, :pswitch_data_0

    .line 1318
    const-string v1, "Unexpected"

    invoke-static {v1}, Lcwz;->a(Ljava/lang/String;)V

    .line 1319
    :goto_0
    :pswitch_0
    return v0

    .line 1314
    :pswitch_1
    invoke-static {p1}, Lcom/google/android/libraries/hangouts/video/VideoChatConstants;->isConnectivityRelatedIssue(I)Z

    move-result v0

    goto :goto_0

    .line 1316
    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    .line 1310
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private terminateCallInternal(Ljava/lang/String;I)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 773
    invoke-static {}, Lcwz;->a()V

    .line 774
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/CallManager;->isCurrentCall(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 775
    invoke-static {}, Lcxc;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 776
    const-string v2, "vclib"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "terminateCallInternal sessionId:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " endCause:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcxc;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 779
    :cond_0
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v2, p2}, Lcom/google/android/libraries/hangouts/video/CallState;->setEndCauseIfUnset(I)V

    .line 780
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/CallState;->wasMediaInitiated()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    .line 781
    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/CallState;->getMediaState()I

    move-result v2

    const/16 v3, 0xf

    if-eq v2, v3, :cond_2

    .line 782
    const/16 v2, 0x3f7

    if-eq p2, v2, :cond_1

    move v0, v1

    .line 785
    :cond_1
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingle:Lcom/google/android/libraries/hangouts/video/Libjingle;

    invoke-virtual {v2, p1, v0}, Lcom/google/android/libraries/hangouts/video/Libjingle;->terminateCall(Ljava/lang/String;Z)V

    .line 790
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mEnsureCallTerminationRunnable:Ljava/lang/Runnable;

    const-wide/16 v3, 0x3a98

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 804
    :goto_0
    return v1

    .line 793
    :cond_2
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mSigninTask:Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;

    if-eqz v2, :cond_3

    .line 794
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mSigninTask:Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;

    invoke-virtual {v2, v0}, Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;->cancel(Z)Z

    .line 795
    iput-object v5, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mSigninTask:Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;

    .line 799
    :cond_3
    invoke-virtual {p0, p2, v5}, Lcom/google/android/libraries/hangouts/video/CallManager;->handleCallEnd(ILjava/lang/String;)V

    goto :goto_0

    .line 803
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t terminate call: no call that matches sessionId: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v0, [Ljava/lang/Object;

    invoke-direct {p0, v1, v2}, Lcom/google/android/libraries/hangouts/video/CallManager;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    move v1, v0

    .line 804
    goto :goto_0
.end method

.method private updateCallState(Ljava/lang/String;ILjava/lang/String;ZZ)V
    .locals 1

    .prologue
    .line 926
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    iget-object v0, v0, Lcom/google/android/libraries/hangouts/video/CallState;->mSessionId:Ljava/lang/String;

    invoke-static {v0, p1}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 927
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v0, p2}, Lcom/google/android/libraries/hangouts/video/CallState;->setMediaState(I)V

    .line 928
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v0, p3}, Lcom/google/android/libraries/hangouts/video/CallState;->setRemoteJid(Ljava/lang/String;)V

    .line 929
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    iput-boolean p4, v0, Lcom/google/android/libraries/hangouts/video/CallState;->mIsVideo:Z

    .line 930
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    iput-boolean p5, v0, Lcom/google/android/libraries/hangouts/video/CallState;->mIsSecure:Z

    .line 931
    return-void
.end method


# virtual methods
.method public addCallStateListener(Lcom/google/android/libraries/hangouts/video/CallStateListener;)V
    .locals 1

    .prologue
    .line 896
    invoke-static {}, Lcwz;->a()V

    .line 897
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCallStateListeners:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 898
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCallStateListeners:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 900
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mInitialAudioUpdateBroadcast:Z

    if-nez v0, :cond_1

    .line 901
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->onAudioUpdated()V

    .line 902
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mInitialAudioUpdateBroadcast:Z

    .line 904
    :cond_1
    return-void
.end method

.method public addLogComment(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1698
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingle:Lcom/google/android/libraries/hangouts/video/Libjingle;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/Libjingle;->addLogComment(Ljava/lang/String;)V

    .line 1699
    return-void
.end method

.method public bindRenderer(III)V
    .locals 1

    .prologue
    .line 376
    invoke-static {}, Lcwz;->a()V

    .line 377
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingle:Lcom/google/android/libraries/hangouts/video/Libjingle;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/libraries/hangouts/video/Libjingle;->bindRenderer(III)V

    .line 378
    return-void
.end method

.method public blockMedia(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 851
    invoke-static {}, Lcwz;->a()V

    .line 852
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingle:Lcom/google/android/libraries/hangouts/video/Libjingle;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/Libjingle;->blockMedia(Ljava/lang/String;)V

    .line 853
    return-void
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 2

    .prologue
    .line 1677
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    if-nez v0, :cond_0

    .line 1689
    :goto_0
    return-void

    .line 1681
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    iget-object v0, v0, Lcom/google/android/libraries/hangouts/video/CallState;->mCallStatistics:Lcom/google/android/libraries/hangouts/video/CallStatistics;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/CallStatistics;->dump(Ljava/io/PrintWriter;)V

    .line 1682
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->isPreparingOrInCall()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1683
    const-string v0, "Call info"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1684
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "  media state: "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->isMediaConnected()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "connected"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1685
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "    sessionId: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    iget-object v1, v1, Lcom/google/android/libraries/hangouts/video/CallState;->mSessionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1686
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "    remoteJid: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/CallState;->getRemoteFullJid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1688
    :cond_1
    invoke-static {p1}, Lcom/google/android/libraries/hangouts/video/RendererManager;->dump(Ljava/io/PrintWriter;)V

    goto :goto_0

    .line 1684
    :cond_2
    const-string v0, "-"

    goto :goto_1
.end method

.method public getCallTag(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 406
    invoke-static {}, Lcwz;->a()V

    .line 407
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/CallManager;->isCurrentCall(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 408
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    iget-object v0, v0, Lcom/google/android/libraries/hangouts/video/CallState;->mTag:Ljava/lang/Object;

    .line 410
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentCall()Lcom/google/android/libraries/hangouts/video/CallState;
    .locals 1

    .prologue
    .line 350
    invoke-static {}, Lcwz;->a()V

    .line 351
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    return-object v0
.end method

.method public getLocalState()Lcom/google/android/libraries/hangouts/video/LocalState;
    .locals 1

    .prologue
    .line 360
    invoke-static {}, Lcwz;->a()V

    .line 361
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalState:Lcom/google/android/libraries/hangouts/video/LocalState;

    return-object v0
.end method

.method public getPreviousCall()Lcom/google/android/libraries/hangouts/video/CallState;
    .locals 1

    .prologue
    .line 355
    invoke-static {}, Lcwz;->a()V

    .line 356
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mPreviousCall:Lcom/google/android/libraries/hangouts/video/CallState;

    return-object v0
.end method

.method public handleBroadcastProducerChange(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1273
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->getClonedListeners()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStateListener;

    .line 1274
    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/libraries/hangouts/video/CallStateListener;->onBroadcastProducerChange(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1276
    :cond_0
    return-void
.end method

.method public handleBroadcastSessionStateChange(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1281
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->getClonedListeners()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStateListener;

    .line 1282
    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/libraries/hangouts/video/CallStateListener;->onBroadcastSessionStateChange(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1284
    :cond_0
    return-void
.end method

.method public handleBroadcastStreamStateChange(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1258
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->getClonedListeners()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStateListener;

    .line 1259
    invoke-interface {v0, p1, p2}, Lcom/google/android/libraries/hangouts/video/CallStateListener;->onBroadcastStreamStateChange(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1261
    :cond_0
    return-void
.end method

.method public handleBroadcastStreamViewerCountChange(I)V
    .locals 2

    .prologue
    .line 1265
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->getClonedListeners()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStateListener;

    .line 1266
    invoke-interface {v0, p1}, Lcom/google/android/libraries/hangouts/video/CallStateListener;->onBroadcastStreamViewerCountChange(I)V

    goto :goto_0

    .line 1268
    :cond_0
    return-void
.end method

.method public handleCallEnd(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 1325
    invoke-static {}, Lcwz;->a()V

    .line 1326
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    if-nez v0, :cond_1

    .line 1329
    invoke-static {p1}, Lcom/google/android/libraries/hangouts/video/VideoChatConstants;->isNetworkError(I)Z

    move-result v0

    invoke-static {v0}, Lcwz;->a(Z)V

    .line 1342
    :cond_0
    :goto_0
    return-void

    .line 1330
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCheckingConnectivity:Z

    if-nez v0, :cond_0

    .line 1331
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/libraries/hangouts/video/CallState;->setEndCauseIfUnset(ILjava/lang/String;)V

    .line 1332
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->getEndCause()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/CallManager;->shouldCheckConnectivity(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1333
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCheckingConnectivity:Z

    .line 1334
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mConnectionMonitor:Lcom/google/android/libraries/hangouts/video/ConnectionMonitor;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    .line 1335
    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/CallState;->getMediaNetworkType()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/ConnectionMonitor;->getSignalStrength(I)Ldog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCallEndSignalStrength:Ldog;

    .line 1336
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingle:Lcom/google/android/libraries/hangouts/video/Libjingle;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/Libjingle;->initiateCheckConnectivity()V

    goto :goto_0

    .line 1338
    :cond_2
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->resetCall()V

    .line 1339
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->reset()V

    goto :goto_0
.end method

.method public handleCheckConnectivity([Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1476
    invoke-static {p1}, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter;->toLogEntry([Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;)Ldon;

    move-result-object v0

    .line 1477
    if-eqz v0, :cond_1

    .line 1478
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mConnectionMonitor:Lcom/google/android/libraries/hangouts/video/ConnectionMonitor;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    .line 1479
    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/CallState;->getMediaNetworkType()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/hangouts/video/ConnectionMonitor;->getNetworkType(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Ldon;->e:Ljava/lang/Integer;

    .line 1480
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCallEndSignalStrength:Ldog;

    iput-object v1, v0, Ldon;->f:Ldog;

    .line 1481
    const/4 v1, 0x2

    invoke-static {v1}, Lcxc;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1482
    const-string v1, "vclib"

    const-string v2, "Connectivity check completed: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ldon;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1484
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/hangouts/video/CallState;->setConnectivityCheck(Ldon;)V

    .line 1486
    :cond_1
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->resetCall()V

    .line 1487
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->reset()V

    .line 1488
    iput-boolean v5, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCheckingConnectivity:Z

    .line 1489
    return-void
.end method

.method public handleCommonNotificationReceived(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1243
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->getClonedListeners()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStateListener;

    .line 1244
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/libraries/hangouts/video/CallStateListener;->onCommonNotificationReceived(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1247
    :cond_0
    return-void
.end method

.method public handleCommonNotificationRetracted(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1251
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->getClonedListeners()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStateListener;

    .line 1252
    invoke-interface {v0, p1}, Lcom/google/android/libraries/hangouts/video/CallStateListener;->onCommonNotificationRetracted(Ljava/lang/String;)V

    goto :goto_0

    .line 1254
    :cond_0
    return-void
.end method

.method public handleConversationIdChanged(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1235
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->getClonedListeners()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStateListener;

    .line 1236
    invoke-interface {v0, p1}, Lcom/google/android/libraries/hangouts/video/CallStateListener;->onConversationIdChanged(Ljava/lang/String;)V

    goto :goto_0

    .line 1238
    :cond_0
    return-void
.end method

.method public handleEndpointEvent(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x0

    const/4 v7, 0x0

    const/4 v3, 0x1

    .line 960
    invoke-static {}, Lcxc;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 961
    const-string v1, "handleEndpointEvent: endpointMucJid=%s eventType=%s, args=%d, %s, %s"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v7

    .line 962
    invoke-static {p3}, Lcom/google/android/libraries/hangouts/video/LibjingleEventHandler;->getEndpointEventTypeName(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 963
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v5

    const/4 v4, 0x3

    aput-object p5, v2, v4

    const/4 v4, 0x4

    aput-object p6, v2, v4

    .line 961
    invoke-direct {p0, v1, v2}, Lcom/google/android/libraries/hangouts/video/CallManager;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 966
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    if-nez v1, :cond_2

    .line 967
    const-string v0, "Got endpoint event, but there\'s no current call. Ignore."

    new-array v1, v7, [Ljava/lang/Object;

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/hangouts/video/CallManager;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1092
    :cond_1
    :goto_0
    return-void

    .line 970
    :cond_2
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v1, p2}, Lcom/google/android/libraries/hangouts/video/CallState;->getEndpoint(Ljava/lang/String;)Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    move-result-object v2

    .line 972
    if-eqz p3, :cond_3

    if-ne p3, v3, :cond_c

    .line 974
    :cond_3
    if-nez v2, :cond_1

    .line 980
    invoke-static {p5}, Lf;->q(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_7

    :cond_4
    :goto_1
    if-eqz v0, :cond_8

    const-string v1, "pstn-conference.google.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "dev-pstn-conference.google.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "alpha-pstn-conference.google.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "beta-dev-pstn-conference.google.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_5
    move v0, v3

    :goto_2
    if-eqz v0, :cond_9

    .line 982
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 983
    new-instance v1, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;

    invoke-direct {v1, p2, p6, p4, p5}, Lcom/google/android/libraries/hangouts/video/endpoint/PstnEndpoint;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    move v5, v7

    .line 999
    :goto_3
    if-eqz v5, :cond_b

    .line 1000
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    move-object v0, v1

    check-cast v0, Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    invoke-virtual {v2, v0}, Lcom/google/android/libraries/hangouts/video/CallState;->setSelf(Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;)V

    move v0, v7

    .line 1004
    :goto_4
    new-instance v2, Lcom/google/android/libraries/hangouts/video/endpoint/EnterEvent;

    invoke-direct {v2, v0}, Lcom/google/android/libraries/hangouts/video/endpoint/EnterEvent;-><init>(Z)V

    move-object v8, v2

    move-object v2, v1

    move-object v1, v8

    .line 1091
    :cond_6
    :goto_5
    invoke-direct {p0, v2, v1}, Lcom/google/android/libraries/hangouts/video/CallManager;->broadcastEndpointEvent(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/EndpointEvent;)V

    goto :goto_0

    .line 980
    :cond_7
    const/16 v2, 0x40

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    if-ltz v2, :cond_4

    add-int/lit8 v0, v2, 0x1

    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_8
    move v0, v7

    goto :goto_2

    .line 986
    :cond_9
    if-nez p3, :cond_a

    move v5, v3

    .line 988
    :goto_6
    if-nez v5, :cond_16

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->getSelf()Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 991
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->getSelf()Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;->getObfuscatedGaiaId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    .line 994
    :goto_7
    new-instance v0, Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    move-object v1, p2

    move-object v2, p6

    move v3, p4

    move-object v4, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ZZ)V

    move-object v1, v0

    goto :goto_3

    :cond_a
    move v5, v7

    .line 986
    goto :goto_6

    .line 1002
    :cond_b
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/CallState;->addRemoteEndpoint(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)Z

    move-result v0

    goto :goto_4

    .line 1005
    :cond_c
    if-ne p3, v5, :cond_f

    .line 1006
    if-nez v2, :cond_d

    .line 1008
    const-string v0, "vclib"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Got an ENDPOINT_EXITED event for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", which doesn\'t exist in our endpoints"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcxc;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1014
    :cond_d
    if-eqz p5, :cond_e

    .line 1016
    :try_start_0
    invoke-static {p5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1023
    :cond_e
    :goto_8
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/hangouts/video/CallState;->removeRemoteEndpoint(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V

    .line 1024
    new-instance v1, Lcom/google/android/libraries/hangouts/video/endpoint/ExitEvent;

    invoke-direct {v1, v0}, Lcom/google/android/libraries/hangouts/video/endpoint/ExitEvent;-><init>(Ljava/lang/Integer;)V

    goto :goto_5

    .line 1018
    :catch_0
    move-exception v1

    const-string v1, "vclib"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "CallState - unexpected endpoint exit error string:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcxc;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_8

    .line 1026
    :cond_f
    if-eqz v2, :cond_1

    .line 1033
    packed-switch p3, :pswitch_data_0

    .line 1085
    const-string v1, "Unexpected event type"

    invoke-static {v1}, Lcwz;->a(Ljava/lang/String;)V

    move-object v1, v0

    .line 1086
    goto/16 :goto_5

    .line 1035
    :pswitch_0
    invoke-virtual {v2, p4}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->setConnectionStatus(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1038
    new-instance v1, Lcom/google/android/libraries/hangouts/video/endpoint/ConnectionStatusChangedEvent;

    invoke-direct {v1}, Lcom/google/android/libraries/hangouts/video/endpoint/ConnectionStatusChangedEvent;-><init>()V

    goto/16 :goto_5

    .line 1042
    :pswitch_1
    if-eqz p4, :cond_10

    move v7, v3

    .line 1043
    :cond_10
    if-nez p5, :cond_12

    .line 1046
    :goto_9
    invoke-static {v2, v0}, Lcwz;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1047
    new-instance v1, Lcom/google/android/libraries/hangouts/video/endpoint/AudioMuteEvent;

    invoke-direct {v1, v7, v0}, Lcom/google/android/libraries/hangouts/video/endpoint/AudioMuteEvent;-><init>(ZLcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V

    .line 1048
    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isSelfEndpoint()Z

    move-result v4

    if-eqz v4, :cond_13

    .line 1049
    if-eqz v0, :cond_6

    move-object v0, v1

    .line 1054
    check-cast v0, Lcom/google/android/libraries/hangouts/video/endpoint/AudioMuteEvent;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/endpoint/AudioMuteEvent;->isMuted()Z

    move-result v0

    invoke-static {v0}, Lcwz;->a(Z)V

    .line 1055
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->shouldManagePlatformInteraction()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1056
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCallAudioHelper:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->setMute(Z)V

    .line 1060
    :cond_11
    invoke-virtual {v2, v7}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->setAudioMuted(Z)V

    goto/16 :goto_5

    .line 1043
    :cond_12
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    .line 1044
    invoke-virtual {v0, p5}, Lcom/google/android/libraries/hangouts/video/CallState;->getEndpoint(Ljava/lang/String;)Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    move-result-object v0

    goto :goto_9

    .line 1063
    :cond_13
    invoke-virtual {v2, v7}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->setAudioMuted(Z)V

    goto/16 :goto_5

    .line 1068
    :pswitch_2
    if-eqz p4, :cond_14

    move v7, v3

    .line 1069
    :cond_14
    instance-of v0, v2, Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    invoke-static {v0}, Lcwz;->a(Z)V

    move-object v0, v2

    .line 1070
    check-cast v0, Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    .line 1071
    invoke-virtual {v0, v7}, Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;->setVideoMuted(Z)V

    .line 1072
    new-instance v1, Lcom/google/android/libraries/hangouts/video/endpoint/VideoMuteEvent;

    invoke-direct {v1, v7}, Lcom/google/android/libraries/hangouts/video/endpoint/VideoMuteEvent;-><init>(Z)V

    goto/16 :goto_5

    .line 1076
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v0, p5}, Lcom/google/android/libraries/hangouts/video/CallState;->getEndpoint(Ljava/lang/String;)Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    move-result-object v0

    .line 1077
    new-instance v1, Lcom/google/android/libraries/hangouts/video/endpoint/MediaBlockEvent;

    invoke-direct {v1, v0}, Lcom/google/android/libraries/hangouts/video/endpoint/MediaBlockEvent;-><init>(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V

    .line 1078
    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isSelfEndpoint()Z

    move-result v4

    if-eqz v4, :cond_15

    .line 1079
    invoke-virtual {v0, v3}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->setMediaBlocked(Z)V

    goto/16 :goto_5

    .line 1080
    :cond_15
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isSelfEndpoint()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1081
    invoke-virtual {v2, v3}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->setMediaBlocked(Z)V

    goto/16 :goto_5

    :cond_16
    move v6, v5

    goto/16 :goto_7

    .line 1033
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public handleHangoutIdResolved(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;ZZ)V
    .locals 10

    .prologue
    .line 1295
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    iget-object v0, v0, Lcom/google/android/libraries/hangouts/video/CallState;->mHangoutRequest:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getHangoutId()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1296
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    iget-object v1, v1, Lcom/google/android/libraries/hangouts/video/CallState;->mHangoutRequest:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->cloneWithHangoutId(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/libraries/hangouts/video/CallState;->mHangoutRequest:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    .line 1299
    :cond_0
    if-eqz p3, :cond_1

    .line 1300
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    iget-object v1, v1, Lcom/google/android/libraries/hangouts/video/CallState;->mHangoutRequest:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    invoke-virtual {v1, p3}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->cloneWithConversationId(Ljava/lang/String;)Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/libraries/hangouts/video/CallState;->mHangoutRequest:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    .line 1303
    :cond_1
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->getClonedListeners()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStateListener;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    .line 1304
    invoke-interface/range {v0 .. v8}, Lcom/google/android/libraries/hangouts/video/CallStateListener;->onHangoutIdResolved(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;ZZ)V

    goto :goto_0

    .line 1307
    :cond_2
    return-void
.end method

.method public handleLoudestSpeakerUpdate(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 1224
    invoke-static {}, Lcwz;->a()V

    .line 1225
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/CallManager;->isCurrentCall(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1227
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->getClonedListeners()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStateListener;

    .line 1228
    invoke-interface {v0, p1, p2}, Lcom/google/android/libraries/hangouts/video/CallStateListener;->onLoudestSpeakerUpdate(Ljava/lang/String;I)V

    goto :goto_0

    .line 1231
    :cond_0
    return-void
.end method

.method public handleMediaSourcesUpdate(Ljava/lang/String;Lcom/google/android/libraries/hangouts/video/MediaSources;)V
    .locals 2

    .prologue
    .line 1114
    invoke-static {}, Lcwz;->a()V

    .line 1115
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/CallManager;->isCurrentCall(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1126
    :goto_0
    return-void

    .line 1118
    :cond_0
    iget-object v0, p2, Lcom/google/android/libraries/hangouts/video/MediaSources;->audioAdded:[Lcom/google/android/libraries/hangouts/video/NamedSource;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/hangouts/video/CallManager;->handleMediaSourcesUpdate([Lcom/google/android/libraries/hangouts/video/NamedSource;I)V

    .line 1120
    iget-object v0, p2, Lcom/google/android/libraries/hangouts/video/MediaSources;->audioRemoved:[Lcom/google/android/libraries/hangouts/video/NamedSource;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/hangouts/video/CallManager;->handleMediaSourcesUpdate([Lcom/google/android/libraries/hangouts/video/NamedSource;I)V

    .line 1122
    iget-object v0, p2, Lcom/google/android/libraries/hangouts/video/MediaSources;->videoAdded:[Lcom/google/android/libraries/hangouts/video/NamedSource;

    const/4 v1, 0x2

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/hangouts/video/CallManager;->handleMediaSourcesUpdate([Lcom/google/android/libraries/hangouts/video/NamedSource;I)V

    .line 1124
    iget-object v0, p2, Lcom/google/android/libraries/hangouts/video/MediaSources;->videoRemoved:[Lcom/google/android/libraries/hangouts/video/NamedSource;

    const/4 v1, 0x3

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/hangouts/video/CallManager;->handleMediaSourcesUpdate([Lcom/google/android/libraries/hangouts/video/NamedSource;I)V

    goto :goto_0
.end method

.method public handleMediaStateChanged(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V
    .locals 12

    .prologue
    .line 1347
    invoke-static {}, Lcwz;->a()V

    .line 1349
    invoke-static/range {p4 .. p4}, Lf;->q(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1350
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingle:Lcom/google/android/libraries/hangouts/video/Libjingle;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/Libjingle;->isSecure(Ljava/lang/String;)Z

    move-result v5

    .line 1351
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingle:Lcom/google/android/libraries/hangouts/video/Libjingle;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/Libjingle;->isVideo(Ljava/lang/String;)Z

    move-result v4

    .line 1353
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/CallManager;->isCurrentCall(Ljava/lang/String;)Z

    move-result v0

    .line 1354
    if-nez v0, :cond_1

    .line 1355
    const-string v0, "vclib"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Received state change for unknown call: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcxc;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1472
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1359
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->getMediaState()I

    move-result v6

    .line 1361
    packed-switch p2, :pswitch_data_0

    :pswitch_1
    goto :goto_0

    .line 1366
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    iget-object v0, v0, Lcom/google/android/libraries/hangouts/video/CallState;->mCallStatistics:Lcom/google/android/libraries/hangouts/video/CallStatistics;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallStatistics;->initializeStats()V

    .line 1367
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCallBoundaryCallback:Lcom/google/android/libraries/hangouts/video/CallManager$CallBoundaryCallback;

    if-eqz v0, :cond_2

    .line 1368
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCallBoundaryCallback:Lcom/google/android/libraries/hangouts/video/CallManager$CallBoundaryCallback;

    invoke-interface {v0, p1, v4}, Lcom/google/android/libraries/hangouts/video/CallManager$CallBoundaryCallback;->onMediaInitiated(Ljava/lang/String;Z)V

    :cond_2
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object/from16 v3, p5

    .line 1370
    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/hangouts/video/CallManager;->updateCallState(Ljava/lang/String;ILjava/lang/String;ZZ)V

    goto :goto_0

    .line 1377
    :pswitch_3
    const-string v0, "This is not expected"

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_4
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object/from16 v3, p5

    .line 1381
    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/hangouts/video/CallManager;->updateCallState(Ljava/lang/String;ILjava/lang/String;ZZ)V

    goto :goto_0

    .line 1385
    :pswitch_5
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->onCallConnecting()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object/from16 v3, p5

    .line 1386
    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/hangouts/video/CallManager;->updateCallState(Ljava/lang/String;ILjava/lang/String;ZZ)V

    goto :goto_0

    :pswitch_6
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object/from16 v3, p5

    .line 1396
    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/hangouts/video/CallManager;->updateCallState(Ljava/lang/String;ILjava/lang/String;ZZ)V

    goto :goto_0

    .line 1400
    :pswitch_7
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    const/16 v1, 0x3ef

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/CallState;->setEndCauseIfUnset(I)V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object/from16 v3, p5

    .line 1402
    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/hangouts/video/CallManager;->updateCallState(Ljava/lang/String;ILjava/lang/String;ZZ)V

    goto :goto_0

    .line 1409
    :pswitch_8
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v0, p3}, Lcom/google/android/libraries/hangouts/video/CallState;->setMediaError(I)V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object/from16 v3, p5

    .line 1410
    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/hangouts/video/CallManager;->updateCallState(Ljava/lang/String;ILjava/lang/String;ZZ)V

    .line 1413
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    const/16 v1, 0x3ef

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/CallState;->setEndCauseIfUnset(I)V

    .line 1415
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->shouldManagePlatformInteraction()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1416
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCallAudioHelper:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->deinitAudio()V

    goto :goto_0

    .line 1421
    :pswitch_9
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v0, p3}, Lcom/google/android/libraries/hangouts/video/CallState;->setMediaError(I)V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object/from16 v3, p5

    .line 1422
    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/hangouts/video/CallManager;->updateCallState(Ljava/lang/String;ILjava/lang/String;ZZ)V

    .line 1423
    const/4 v0, 0x1

    if-ne v6, v0, :cond_4

    .line 1424
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    iget-object v0, v0, Lcom/google/android/libraries/hangouts/video/CallState;->mHangoutRequest:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    if-nez v0, :cond_3

    .line 1428
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    const/16 v1, 0x3ef

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/CallState;->setEndCauseIfUnset(I)V

    .line 1438
    :goto_1
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->shouldManagePlatformInteraction()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1439
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCallAudioHelper:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->deinitAudio()V

    goto/16 :goto_0

    .line 1431
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    const/16 v1, 0x3f0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/CallState;->setEndCauseIfUnset(I)V

    goto :goto_1

    .line 1435
    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    const/16 v1, 0x3f1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/CallState;->setEndCauseIfUnset(I)V

    goto :goto_1

    .line 1444
    :pswitch_a
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCallBoundaryCallback:Lcom/google/android/libraries/hangouts/video/CallManager$CallBoundaryCallback;

    if-eqz v0, :cond_5

    .line 1445
    iget-object v6, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCallBoundaryCallback:Lcom/google/android/libraries/hangouts/video/CallManager$CallBoundaryCallback;

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    iget-object v9, v0, Lcom/google/android/libraries/hangouts/video/CallState;->mHangoutRequest:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-object v7, p1

    move-object/from16 v8, p5

    move v11, v4

    invoke-interface/range {v6 .. v11}, Lcom/google/android/libraries/hangouts/video/CallManager$CallBoundaryCallback;->onMediaConnected(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/libraries/hangouts/video/HangoutRequest;Ljava/lang/String;Z)V

    :cond_5
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object/from16 v3, p5

    .line 1448
    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/hangouts/video/CallManager;->updateCallState(Ljava/lang/String;ILjava/lang/String;ZZ)V

    .line 1449
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->getClonedListeners()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStateListener;

    .line 1450
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-interface {v0, v2}, Lcom/google/android/libraries/hangouts/video/CallStateListener;->onMediaStarted(Lcom/google/android/libraries/hangouts/video/CallState;)V

    goto :goto_2

    .line 1455
    :pswitch_b
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "STATE_DEINIT sessionid: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/hangouts/video/CallManager;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1458
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->getRemoteFullJid()Ljava/lang/String;

    move-result-object v3

    .line 1460
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->shouldManagePlatformInteraction()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1461
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCallAudioHelper:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->deinitAudio()V

    :cond_6
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    .line 1464
    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/hangouts/video/CallManager;->updateCallState(Ljava/lang/String;ILjava/lang/String;ZZ)V

    .line 1469
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/hangouts/video/CallManager;->handleCallEnd(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 1361
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_1
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public handlePushNotification([B)V
    .locals 1

    .prologue
    .line 809
    invoke-static {}, Lcwz;->a()V

    .line 810
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingle:Lcom/google/android/libraries/hangouts/video/Libjingle;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/Libjingle;->handlePushNotification([B)V

    .line 811
    return-void
.end method

.method public handleSignedInStateUpdate(Ljava/lang/String;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1179
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "handleSignedInStateUpdate: "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", signedIn="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v3, v2, [Ljava/lang/Object;

    invoke-direct {p0, v0, v3}, Lcom/google/android/libraries/hangouts/video/CallManager;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1180
    invoke-static {}, Lcwz;->a()V

    .line 1181
    if-nez p1, :cond_1

    move v3, v1

    :goto_0
    if-nez p2, :cond_2

    move v0, v1

    :goto_1
    if-ne v3, v0, :cond_3

    move v0, v1

    :goto_2
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 1183
    if-eqz p2, :cond_4

    .line 1187
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalState:Lcom/google/android/libraries/hangouts/video/LocalState;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/LocalState;->setSigninState(I)V

    .line 1188
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalState:Lcom/google/android/libraries/hangouts/video/LocalState;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/LocalState;->setFullJid(Ljava/lang/String;)V

    .line 1189
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->isPendingSignIn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1190
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->setSignedIn()V

    .line 1191
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->initiateCallWithState()V

    .line 1193
    :cond_0
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->getClonedListeners()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStateListener;

    .line 1194
    invoke-interface {v0}, Lcom/google/android/libraries/hangouts/video/CallStateListener;->onSignedIn()V

    goto :goto_3

    :cond_1
    move v3, v2

    .line 1181
    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    .line 1197
    :cond_4
    invoke-static {p1}, Lcwz;->a(Ljava/lang/Object;)V

    .line 1199
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalState:Lcom/google/android/libraries/hangouts/video/LocalState;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/LocalState;->getSigninState()I

    move-result v0

    if-ne v0, v1, :cond_7

    .line 1203
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalState:Lcom/google/android/libraries/hangouts/video/LocalState;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/LocalState;->wasTokenInvalidated()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1206
    const/16 v0, 0x3e8

    invoke-virtual {p0, v0, v4}, Lcom/google/android/libraries/hangouts/video/CallManager;->handleCallEnd(ILjava/lang/String;)V

    .line 1220
    :cond_5
    :goto_4
    return-void

    .line 1209
    :cond_6
    const-string v0, "vclib"

    const-string v1, "Invalidating token."

    invoke-static {v0, v1}, Lcxc;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1210
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.google"

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mAuthToken:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 1211
    iput-object v4, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mAuthToken:Ljava/lang/String;

    .line 1212
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalState:Lcom/google/android/libraries/hangouts/video/LocalState;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/LocalState;->setTokenInvalidated()V

    .line 1213
    new-instance v0, Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalState:Lcom/google/android/libraries/hangouts/video/LocalState;

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/LocalState;->getAccountName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;-><init>(Lcom/google/android/libraries/hangouts/video/CallManager;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mSigninTask:Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;

    .line 1214
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mSigninTask:Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;->executeOnThreadPool([Ljava/lang/Object;)Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;

    goto :goto_4

    .line 1217
    :cond_7
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->resetLocalStateIfNeeded()V

    goto :goto_4
.end method

.method public handleStatsUpdate(Lcom/google/android/libraries/hangouts/video/Stats;)V
    .locals 3

    .prologue
    .line 944
    instance-of v0, p1, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 945
    check-cast v0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;

    .line 946
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/CallState;->getMediaNetworkType()I

    move-result v1

    .line 947
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mConnectionMonitor:Lcom/google/android/libraries/hangouts/video/ConnectionMonitor;

    .line 948
    invoke-virtual {v2, v1}, Lcom/google/android/libraries/hangouts/video/ConnectionMonitor;->getNetworkType(I)I

    move-result v2

    .line 947
    invoke-virtual {v0, v2}, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->setMediaNetworkType(I)V

    .line 949
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mConnectionMonitor:Lcom/google/android/libraries/hangouts/video/ConnectionMonitor;

    .line 950
    invoke-virtual {v2, v1}, Lcom/google/android/libraries/hangouts/video/ConnectionMonitor;->getSignalStrength(I)Ldog;

    move-result-object v1

    .line 949
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->setSignalStrength(Ldog;)V

    .line 954
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    iget-object v0, v0, Lcom/google/android/libraries/hangouts/video/CallState;->mCallStatistics:Lcom/google/android/libraries/hangouts/video/CallStatistics;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/CallStatistics;->update(Lcom/google/android/libraries/hangouts/video/Stats;)V

    .line 955
    return-void

    .line 951
    :cond_1
    instance-of v0, p1, Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;

    if-eqz v0, :cond_0

    .line 952
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mGlobalStatsAdapter:Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter;

    move-object v0, p1

    check-cast v0, Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter;->updateStats(Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;)V

    goto :goto_0
.end method

.method public invitePstn(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 823
    invoke-static {}, Lcwz;->a()V

    .line 824
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingle:Lcom/google/android/libraries/hangouts/video/Libjingle;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/libraries/hangouts/video/Libjingle;->invitePstn(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 825
    return-void
.end method

.method public inviteUsers(Z[Ljava/lang/String;[Ljava/lang/String;IZZLjava/lang/String;)V
    .locals 8

    .prologue
    .line 817
    invoke-static {}, Lcwz;->a()V

    .line 818
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingle:Lcom/google/android/libraries/hangouts/video/Libjingle;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move-object v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/libraries/hangouts/video/Libjingle;->inviteUsers(Z[Ljava/lang/String;[Ljava/lang/String;IZZLjava/lang/String;)V

    .line 820
    return-void
.end method

.method public makeApiaryRequest(JLjava/lang/String;[BI)V
    .locals 6

    .prologue
    .line 1174
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mApiaryClient:Lcom/google/android/libraries/hangouts/video/ApiaryClient;

    move-wide v1, p1

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/libraries/hangouts/video/ApiaryClient;->makeRequest(JLjava/lang/String;[BI)V

    .line 1175
    return-void
.end method

.method public onRequestCompleted(J[B)V
    .locals 1

    .prologue
    .line 1708
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingle:Lcom/google/android/libraries/hangouts/video/Libjingle;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/libraries/hangouts/video/Libjingle;->handleApiaryResponse(J[B)V

    .line 1709
    return-void
.end method

.method public onRequestError(J)V
    .locals 2

    .prologue
    .line 1703
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingle:Lcom/google/android/libraries/hangouts/video/Libjingle;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/libraries/hangouts/video/Libjingle;->handleApiaryResponse(J[B)V

    .line 1704
    return-void
.end method

.method prepareCall(Lcom/google/android/libraries/hangouts/video/CallOptions;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 474
    invoke-static {}, Lcwz;->a()V

    .line 475
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-static {v2}, Lcwz;->a(Ljava/lang/Object;)V

    .line 477
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->isInitialized()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/CallOptions;->getSessionId()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 478
    :cond_0
    invoke-direct {p0, v1}, Lcom/google/android/libraries/hangouts/video/CallManager;->resetCallWithEndCause(I)V

    .line 523
    :goto_0
    return v0

    .line 482
    :cond_1
    new-instance v2, Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-direct {v2, p1}, Lcom/google/android/libraries/hangouts/video/CallState;-><init>(Lcom/google/android/libraries/hangouts/video/CallOptions;)V

    iput-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    .line 483
    iput-object v5, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mPreviousCall:Lcom/google/android/libraries/hangouts/video/CallState;

    .line 485
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v3

    .line 486
    if-nez v3, :cond_2

    .line 487
    const/16 v1, 0x3e9

    invoke-direct {p0, v1}, Lcom/google/android/libraries/hangouts/video/CallManager;->resetCallWithEndCause(I)V

    goto :goto_0

    .line 491
    :cond_2
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v4}, Lf;->a(Landroid/content/ContentResolver;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-static {v2}, Lf;->e(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_3

    move v2, v1

    :goto_1
    if-eqz v2, :cond_4

    .line 493
    const/16 v1, 0x3ea

    invoke-direct {p0, v1}, Lcom/google/android/libraries/hangouts/video/CallManager;->resetCallWithEndCause(I)V

    goto :goto_0

    :cond_3
    move v2, v0

    .line 491
    goto :goto_1

    .line 497
    :cond_4
    invoke-direct {p0, v3}, Lcom/google/android/libraries/hangouts/video/CallManager;->acquireWakeLocks(Landroid/net/NetworkInfo;)V

    .line 499
    invoke-direct {p0, v3}, Lcom/google/android/libraries/hangouts/video/CallManager;->setNetworkType(Landroid/net/NetworkInfo;)V

    .line 501
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/CallOptions;->shouldManagePlatformInteraction()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 502
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCallAudioHelper:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/CallOptions;->preferSpeakerphone()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->initAudio(Z)V

    .line 506
    :cond_5
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCallBoundaryCallback:Lcom/google/android/libraries/hangouts/video/CallManager$CallBoundaryCallback;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-interface {v0, v2}, Lcom/google/android/libraries/hangouts/video/CallManager$CallBoundaryCallback;->onNewCallPrepared(Lcom/google/android/libraries/hangouts/video/CallState;)V

    .line 508
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->shouldManagePlatformInteraction()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mContext:Landroid/content/Context;

    .line 509
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lf;->a(Landroid/content/ContentResolver;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 510
    new-instance v0, Landroid/content/IntentFilter;

    const-string v2, "android.net.wifi.supplicant.CONNECTION_CHANGE"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 512
    const-string v2, "android.net.wifi.supplicant.STATE_CHANGE"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 513
    new-instance v2, Lcom/google/android/libraries/hangouts/video/CallManager$WifiStateReceiver;

    invoke-direct {v2, p0, v5}, Lcom/google/android/libraries/hangouts/video/CallManager$WifiStateReceiver;-><init>(Lcom/google/android/libraries/hangouts/video/CallManager;Lcom/google/android/libraries/hangouts/video/CallManager$1;)V

    iput-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mWifiStateReceiver:Lcom/google/android/libraries/hangouts/video/CallManager$WifiStateReceiver;

    .line 514
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mWifiStateReceiver:Lcom/google/android/libraries/hangouts/video/CallManager$WifiStateReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 517
    :cond_6
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->shouldManagePlatformInteraction()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 518
    new-instance v0, Landroid/content/IntentFilter;

    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 519
    new-instance v2, Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;

    invoke-direct {v2, p0, v5}, Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;-><init>(Lcom/google/android/libraries/hangouts/video/CallManager;Lcom/google/android/libraries/hangouts/video/CallManager$1;)V

    iput-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mNetworkStateReceiver:Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;

    .line 520
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mNetworkStateReceiver:Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_7
    move v0, v1

    .line 523
    goto/16 :goto_0
.end method

.method public publishVideoMuteState(Z)V
    .locals 1

    .prologue
    .line 846
    invoke-static {}, Lcwz;->a()V

    .line 847
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingle:Lcom/google/android/libraries/hangouts/video/Libjingle;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/Libjingle;->publishVideoMuteState(Z)V

    .line 848
    return-void
.end method

.method public release()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 420
    invoke-static {}, Lcwz;->a()V

    .line 422
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCallAudioHelper:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->deinitAudio()V

    .line 423
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mApiaryClient:Lcom/google/android/libraries/hangouts/video/ApiaryClient;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/hangouts/video/ApiaryClient;->setListener(Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestListener;)V

    .line 425
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingle:Lcom/google/android/libraries/hangouts/video/Libjingle;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/Libjingle;->release()V

    .line 426
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mWifiStateReceiver:Lcom/google/android/libraries/hangouts/video/CallManager$WifiStateReceiver;

    if-eqz v0, :cond_0

    .line 427
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mWifiStateReceiver:Lcom/google/android/libraries/hangouts/video/CallManager$WifiStateReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 428
    iput-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mWifiStateReceiver:Lcom/google/android/libraries/hangouts/video/CallManager$WifiStateReceiver;

    .line 430
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mNetworkStateReceiver:Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;

    if-eqz v0, :cond_1

    .line 431
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mNetworkStateReceiver:Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 432
    iput-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mNetworkStateReceiver:Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;

    .line 435
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mGlobalStatsAdapter:Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter;->release()V

    .line 436
    sput-object v2, Lcom/google/android/libraries/hangouts/video/CallManager;->sInstance:Lcom/google/android/libraries/hangouts/video/CallManager;

    .line 437
    return-void
.end method

.method public remoteMute(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 841
    invoke-static {}, Lcwz;->a()V

    .line 842
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingle:Lcom/google/android/libraries/hangouts/video/Libjingle;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/Libjingle;->remoteMute(Ljava/lang/String;)V

    .line 843
    return-void
.end method

.method public removeCallStateListener(Lcom/google/android/libraries/hangouts/video/CallStateListener;)V
    .locals 1

    .prologue
    .line 907
    invoke-static {}, Lcwz;->a()V

    .line 908
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCallStateListeners:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 909
    return-void
.end method

.method public requestVideoViews([Lcom/google/android/libraries/hangouts/video/VideoViewRequest;)V
    .locals 1

    .prologue
    .line 833
    invoke-static {}, Lcwz;->a()V

    .line 834
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 838
    :goto_0
    return-void

    .line 837
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingle:Lcom/google/android/libraries/hangouts/video/Libjingle;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/Libjingle;->requestVideoViews([Lcom/google/android/libraries/hangouts/video/VideoViewRequest;)V

    goto :goto_0
.end method

.method public sendDtmf(CILjava/lang/String;)V
    .locals 1

    .prologue
    .line 828
    invoke-static {}, Lcwz;->a()V

    .line 829
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingle:Lcom/google/android/libraries/hangouts/video/Libjingle;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/libraries/hangouts/video/Libjingle;->sendDtmf(CILjava/lang/String;)V

    .line 830
    return-void
.end method

.method public setAudioDevice(Lcom/google/android/libraries/hangouts/video/AudioDevice;)V
    .locals 1

    .prologue
    .line 890
    invoke-static {}, Lcwz;->a()V

    .line 891
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->shouldManagePlatformInteraction()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 892
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCallAudioHelper:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->setAudioDevice(Lcom/google/android/libraries/hangouts/video/AudioDevice;)V

    .line 893
    return-void

    .line 891
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setCallBoundaryCallback(Lcom/google/android/libraries/hangouts/video/CallManager$CallBoundaryCallback;)V
    .locals 0

    .prologue
    .line 371
    invoke-static {}, Lcwz;->a()V

    .line 372
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCallBoundaryCallback:Lcom/google/android/libraries/hangouts/video/CallManager$CallBoundaryCallback;

    .line 373
    return-void
.end method

.method public setCallTag(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 392
    invoke-static {}, Lcwz;->a()V

    .line 393
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/CallManager;->isCurrentCall(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 394
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    iput-object p2, v0, Lcom/google/android/libraries/hangouts/video/CallState;->mTag:Ljava/lang/Object;

    .line 396
    :cond_0
    return-void
.end method

.method public setMute(Z)V
    .locals 2

    .prologue
    .line 856
    invoke-static {}, Lcwz;->a()V

    .line 858
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->shouldManagePlatformInteraction()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 859
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCallAudioHelper:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->setMute(Z)V

    .line 864
    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    if-nez v0, :cond_2

    .line 866
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mPreviousCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 887
    :goto_1
    return-void

    .line 861
    :cond_1
    invoke-static {p1}, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->setMicrophoneMute(Z)V

    goto :goto_0

    .line 868
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    iget-object v0, v0, Lcom/google/android/libraries/hangouts/video/CallState;->mHangoutRequest:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->isMediaConnected()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 869
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingle:Lcom/google/android/libraries/hangouts/video/Libjingle;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/Libjingle;->publishAudioMuteState(Z)V

    .line 871
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    iget-object v0, v0, Lcom/google/android/libraries/hangouts/video/CallState;->mSelf:Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    if-nez v0, :cond_4

    .line 872
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Mute is allowed only after STATE_INPROGRESS"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 874
    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    iget-object v0, v0, Lcom/google/android/libraries/hangouts/video/CallState;->mSelf:Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;->setAudioMuted(Z)V

    .line 877
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/libraries/hangouts/video/CallManager$4;

    invoke-direct {v1, p0, p1}, Lcom/google/android/libraries/hangouts/video/CallManager$4;-><init>(Lcom/google/android/libraries/hangouts/video/CallManager;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1
.end method

.method signIn(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 618
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalState:Lcom/google/android/libraries/hangouts/video/LocalState;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/LocalState;->setAccountName(Ljava/lang/String;)V

    .line 619
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalState:Lcom/google/android/libraries/hangouts/video/LocalState;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/LocalState;->setSigninState(I)V

    .line 620
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mService:Lcom/google/android/libraries/hangouts/video/VideoChatService;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->onConnectionOpened(Ljava/lang/String;)V

    .line 622
    new-instance v0, Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;

    invoke-direct {v0, p0, p1}, Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;-><init>(Lcom/google/android/libraries/hangouts/video/CallManager;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mSigninTask:Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;

    .line 623
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mSigninTask:Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;->executeOnThreadPool([Ljava/lang/Object;)Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;

    .line 624
    return-void
.end method

.method startCall(Ljava/lang/String;Lcom/google/android/libraries/hangouts/video/HangoutRequest;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 528
    invoke-static {}, Lcwz;->a()V

    .line 529
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/CallManager;->isCurrentCall(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    iget-object v0, v0, Lcom/google/android/libraries/hangouts/video/CallState;->mHangoutRequest:Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    if-eqz v0, :cond_1

    .line 530
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "prepareCall should be called once for "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/hangouts/video/CallManager;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 550
    :goto_0
    return-void

    .line 534
    :cond_1
    invoke-virtual {p2}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->hasInformationForCall()Z

    move-result v0

    if-nez v0, :cond_2

    .line 535
    const-string v0, "vclib"

    const-string v1, "startCall lacks enough info to place a call."

    invoke-static {v0, v1}, Lcxc;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/CallManager;->resetCallWithEndCause(I)V

    goto :goto_0

    .line 539
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v0, p2}, Lcom/google/android/libraries/hangouts/video/CallState;->initializeHangoutRequest(Lcom/google/android/libraries/hangouts/video/HangoutRequest;)V

    .line 540
    iput-object p3, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mGcmRegistration:Ljava/lang/String;

    .line 541
    iput-object p4, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mAndroidId:Ljava/lang/String;

    .line 543
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalState:Lcom/google/android/libraries/hangouts/video/LocalState;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/LocalState;->getSigninState()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    .line 544
    const-string v0, "We\'re not yet signed in; signing in and postpoining initiation until done"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/hangouts/video/CallManager;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 545
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->getHangoutRequest()Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getAccountName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/hangouts/video/CallManager;->signIn(Ljava/lang/String;)V

    goto :goto_0

    .line 547
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallState;->setSignedIn()V

    .line 548
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->initiateCallWithState()V

    goto :goto_0
.end method

.method public terminateCall(Ljava/lang/String;I)Z
    .locals 1

    .prologue
    .line 769
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/hangouts/video/CallManager;->terminateCallInternal(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public unbindRenderer(I)V
    .locals 1

    .prologue
    .line 381
    invoke-static {}, Lcwz;->a()V

    .line 382
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingle:Lcom/google/android/libraries/hangouts/video/Libjingle;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/Libjingle;->unbindRenderer(I)V

    .line 383
    return-void
.end method
