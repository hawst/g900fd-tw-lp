.class public Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;
.super Lcom/google/android/libraries/hangouts/video/Stats;
.source "PG"


# instance fields
.field public final audioLevel:I

.field public final bytesSent:I

.field public final codecName:Ljava/lang/String;

.field public final echoDelayMedianMillis:I

.field public final echoDelayStdMillis:I

.field public final echoReturnLoss:I

.field public final echoReturnLossEnhancement:I

.field public final extSeqNum:I

.field public final fractionLost:F

.field public final jitterMillis:I

.field public final packetsLost:I

.field public final packetsSent:I

.field public final rttMillis:I

.field public final ssrc:I


# direct methods
.method public constructor <init>(ILjava/lang/String;IIIFIIIIIIII)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/Stats;-><init>()V

    .line 88
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->ssrc:I

    .line 89
    iput-object p2, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->codecName:Ljava/lang/String;

    .line 90
    iput p3, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->bytesSent:I

    .line 91
    iput p4, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->packetsSent:I

    .line 92
    iput p5, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->packetsLost:I

    .line 93
    iput p6, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->fractionLost:F

    .line 94
    iput p7, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->extSeqNum:I

    .line 95
    iput p8, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->rttMillis:I

    .line 96
    iput p9, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->jitterMillis:I

    .line 97
    iput p10, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->audioLevel:I

    .line 98
    iput p11, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->echoDelayMedianMillis:I

    .line 99
    iput p12, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->echoDelayStdMillis:I

    .line 100
    iput p13, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->echoReturnLoss:I

    .line 101
    iput p14, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->echoReturnLossEnhancement:I

    .line 102
    return-void
.end method

.method static printLegend(Ljava/io/PrintWriter;)V
    .locals 1

    .prologue
    .line 142
    const-string v0, "  VoiceSender -- ssrc, codec, sent, lost, seq, rtt, jitter, level, echo, echoRetLoss"

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 144
    return-void
.end method


# virtual methods
.method public addTo(Ldoe;)V
    .locals 6

    .prologue
    const/16 v5, -0x64

    const/4 v4, -0x1

    .line 106
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/Stats;->createEmptyMediaProto(I)Ldoh;

    move-result-object v1

    .line 107
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->l:Ljava/lang/Integer;

    .line 108
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->ssrc:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->m:Ljava/lang/Integer;

    .line 109
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->codecName:Ljava/lang/String;

    iput-object v0, v1, Ldoh;->G:Ljava/lang/String;

    .line 110
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->bytesSent:I

    int-to-long v2, v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v1, Ldoh;->h:Ljava/lang/Long;

    .line 111
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->packetsSent:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->i:Ljava/lang/Integer;

    .line 112
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->packetsLost:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->d:Ljava/lang/Integer;

    .line 113
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->fractionLost:F

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v0, v2

    float-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->c:Ljava/lang/Integer;

    .line 114
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->extSeqNum:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->e:Ljava/lang/Integer;

    .line 115
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->rttMillis:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->g:Ljava/lang/Integer;

    .line 116
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->jitterMillis:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->f:Ljava/lang/Integer;

    .line 119
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->audioLevel:I

    if-eq v0, v4, :cond_0

    .line 120
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->audioLevel:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->n:Ljava/lang/Integer;

    .line 122
    :cond_0
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->echoDelayMedianMillis:I

    if-eq v0, v4, :cond_1

    .line 123
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->echoDelayMedianMillis:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->I:Ljava/lang/Integer;

    .line 125
    :cond_1
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->echoDelayStdMillis:I

    if-eq v0, v4, :cond_2

    .line 126
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->echoDelayStdMillis:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->J:Ljava/lang/Integer;

    .line 130
    :cond_2
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->echoReturnLoss:I

    if-eq v0, v5, :cond_3

    .line 131
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->echoReturnLoss:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->K:Ljava/lang/Integer;

    .line 133
    :cond_3
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->echoReturnLossEnhancement:I

    if-eq v0, v5, :cond_4

    .line 134
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->echoReturnLossEnhancement:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->L:Ljava/lang/Integer;

    .line 136
    :cond_4
    iget-object v0, p1, Ldoe;->c:[Ldoh;

    array-length v2, v0

    .line 137
    iget-object v0, p1, Ldoe;->c:[Ldoh;

    add-int/lit8 v3, v2, 0x1

    invoke-static {v0, v3}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldoh;

    iput-object v0, p1, Ldoe;->c:[Ldoh;

    .line 138
    iget-object v0, p1, Ldoe;->c:[Ldoh;

    aput-object v1, v0, v2

    .line 139
    return-void
.end method

.method public print(Ljava/io/PrintWriter;Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;)V
    .locals 5

    .prologue
    .line 148
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, " -- VoiceSender -- "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->ssrc:I

    int-to-long v1, v1

    const-wide v3, 0xffffffffL

    and-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->codecName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->packetsSent:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->bytesSent:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "), "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->packetsLost:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->fractionLost:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "), "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->extSeqNum:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->rttMillis:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->jitterMillis:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->audioLevel:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->echoDelayMedianMillis:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->echoDelayStdMillis:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "), "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->echoReturnLoss:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->echoReturnLossEnhancement:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 161
    return-void
.end method
