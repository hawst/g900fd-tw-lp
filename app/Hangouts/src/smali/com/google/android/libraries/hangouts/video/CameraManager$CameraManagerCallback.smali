.class interface abstract Lcom/google/android/libraries/hangouts/video/CameraManager$CameraManagerCallback;
.super Ljava/lang/Object;
.source "PG"


# virtual methods
.method public abstract getDesiredCaptureVideoSpec()Lcom/google/android/libraries/hangouts/video/VideoSpecification;
.end method

.method public abstract onCameraOpen(Lcom/google/android/libraries/hangouts/video/Size;IZ)V
.end method

.method public abstract onCameraOpenError(Ljava/lang/Exception;)V
.end method

.method public abstract onFrameOutputSet(Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;)V
.end method
