.class Lcom/google/android/libraries/hangouts/video/VideoChatService$CallBoundaryCallback;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/hangouts/video/CallManager$CallBoundaryCallback;


# instance fields
.field private mSessionId:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/libraries/hangouts/video/VideoChatService;


# direct methods
.method private constructor <init>(Lcom/google/android/libraries/hangouts/video/VideoChatService;)V
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService$CallBoundaryCallback;->this$0:Lcom/google/android/libraries/hangouts/video/VideoChatService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/libraries/hangouts/video/VideoChatService;Lcom/google/android/libraries/hangouts/video/VideoChatService$1;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/VideoChatService$CallBoundaryCallback;-><init>(Lcom/google/android/libraries/hangouts/video/VideoChatService;)V

    return-void
.end method


# virtual methods
.method public onCallEnd(Lcom/google/android/libraries/hangouts/video/CallState;)V
    .locals 3

    .prologue
    .line 76
    const-string v0, "vclib"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onCallEnd: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/CallState;->getEndCause()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcxc;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService$CallBoundaryCallback;->mSessionId:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService$CallBoundaryCallback;->mSessionId:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/CallState;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService$CallBoundaryCallback;->this$0:Lcom/google/android/libraries/hangouts/video/VideoChatService;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->removeOngoingNotification()V

    .line 79
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService$CallBoundaryCallback;->this$0:Lcom/google/android/libraries/hangouts/video/VideoChatService;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService$CallBoundaryCallback;->mSessionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->removeKeepAliveRequest(Ljava/lang/String;)V

    .line 81
    :cond_0
    return-void
.end method

.method public onMediaConnected(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/libraries/hangouts/video/HangoutRequest;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 70
    return-void
.end method

.method public onMediaInitiated(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 65
    return-void
.end method

.method public onNewCallPrepared(Lcom/google/android/libraries/hangouts/video/CallState;)V
    .locals 2

    .prologue
    .line 52
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/CallState;->getSessionId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService$CallBoundaryCallback;->mSessionId:Ljava/lang/String;

    .line 55
    const-string v0, "vclib"

    const-string v1, "onNewCallStarting"

    invoke-static {v0, v1}, Lcxc;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService$CallBoundaryCallback;->this$0:Lcom/google/android/libraries/hangouts/video/VideoChatService;

    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/CallState;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->addKeepAliveRequest(Ljava/lang/String;)V

    .line 57
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService$CallBoundaryCallback;->this$0:Lcom/google/android/libraries/hangouts/video/VideoChatService;

    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/CallState;->getSessionId()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/google/android/libraries/hangouts/video/VideoChatService;->postOngoingNotification(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->access$000(Lcom/google/android/libraries/hangouts/video/VideoChatService;Ljava/lang/String;)V

    .line 58
    return-void
.end method
