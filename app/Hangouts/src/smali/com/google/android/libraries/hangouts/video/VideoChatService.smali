.class public final Lcom/google/android/libraries/hangouts/video/VideoChatService;
.super Landroid/app/Service;
.source "PG"


# static fields
.field public static final ACTION_SOFT_BIND:Ljava/lang/String; = "com.google.android.talk.SOFT_BIND"

.field private static final STOP_SERVICE_DELAY_MILLIS:I = 0xbb8

.field private static final STOP_SERVICE_TOKEN:I = 0x1

.field private static final TAG:Ljava/lang/String; = "vclib"

.field private static final VIDEOCHAT_SERVICE_STATUS:I = 0x1


# instance fields
.field private mCachedCallSession:Lcom/google/android/libraries/hangouts/video/CallSession;

.field private final mCachedCallSessionLock:Ljava/lang/Object;

.field private mCallManager:Lcom/google/android/libraries/hangouts/video/CallManager;

.field private final mKeepAliveRequests:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mMobileHipriManager:Lcom/google/android/libraries/hangouts/video/MobileHipriManager;

.field private mOutputReceiverComponent:Landroid/content/ComponentName;

.field private final mSoftBinder:Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;

.field private final mStopServiceHandler:Lcom/google/android/libraries/hangouts/video/VideoChatService$StopServiceHandler;

.field private mStopped:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 92
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mKeepAliveRequests:Ljava/util/HashSet;

    .line 101
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mCachedCallSessionLock:Ljava/lang/Object;

    .line 401
    new-instance v0, Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;-><init>(Lcom/google/android/libraries/hangouts/video/VideoChatService;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mSoftBinder:Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;

    .line 479
    new-instance v0, Lcom/google/android/libraries/hangouts/video/VideoChatService$StopServiceHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/libraries/hangouts/video/VideoChatService$StopServiceHandler;-><init>(Lcom/google/android/libraries/hangouts/video/VideoChatService;Lcom/google/android/libraries/hangouts/video/VideoChatService$1;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mStopServiceHandler:Lcom/google/android/libraries/hangouts/video/VideoChatService$StopServiceHandler;

    .line 484
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/libraries/hangouts/video/VideoChatService;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->postOngoingNotification(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/libraries/hangouts/video/VideoChatService;)Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mStopped:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/libraries/hangouts/video/VideoChatService;)Lcom/google/android/libraries/hangouts/video/CallManager;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mCallManager:Lcom/google/android/libraries/hangouts/video/CallManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/libraries/hangouts/video/VideoChatService;)Z
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->stopServiceIfSafe()Z

    move-result v0

    return v0
.end method

.method private getCallSession()Lcom/google/android/libraries/hangouts/video/CallSession;
    .locals 3

    .prologue
    .line 300
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mCachedCallSessionLock:Ljava/lang/Object;

    monitor-enter v1

    .line 301
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mCachedCallSession:Lcom/google/android/libraries/hangouts/video/CallSession;

    if-nez v0, :cond_0

    .line 302
    new-instance v0, Lcom/google/android/libraries/hangouts/video/CallSession;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mCallManager:Lcom/google/android/libraries/hangouts/video/CallManager;

    invoke-direct {v0, v2}, Lcom/google/android/libraries/hangouts/video/CallSession;-><init>(Lcom/google/android/libraries/hangouts/video/CallManager;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mCachedCallSession:Lcom/google/android/libraries/hangouts/video/CallSession;

    .line 304
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 306
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mCachedCallSession:Lcom/google/android/libraries/hangouts/video/CallSession;

    return-object v0

    .line 304
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private handleIncomingIntent(Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v6, -0x1

    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 141
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 142
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->getCallSession()Lcom/google/android/libraries/hangouts/video/CallSession;

    move-result-object v0

    .line 143
    const-string v1, "account_name"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 144
    const-string v1, "remote_jid"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 145
    const-string v1, "com.google.android.libraries.hangouts.video.sessionid"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 146
    if-nez v1, :cond_15

    .line 147
    const-string v1, ""

    move-object v2, v1

    .line 150
    :goto_0
    const-string v1, "com.google.android.libraries.hangouts.video.ACTION_PREPARE_CALL"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 151
    const-string v1, "call_options"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/hangouts/video/CallOptions;

    .line 153
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/CallSession;->prepareCall(Lcom/google/android/libraries/hangouts/video/CallOptions;)V

    .line 238
    :cond_0
    :goto_1
    return-void

    .line 154
    :cond_1
    const-string v1, "com.google.android.libraries.hangouts.video.ACTION_START_CALL"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 155
    const-string v1, "hangout_request"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    .line 157
    const-string v3, "com.google.android.libraries.hangouts.video.gcm_registration"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 158
    const-string v4, "com.google.android.libraries.hangouts.video.android_id"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 159
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    if-nez v1, :cond_3

    .line 160
    :cond_2
    const-string v0, "vclib"

    const-string v1, "Ignoring ACTION_TERMINATE_CALL for empty sessionId/hangoutReq"

    invoke-static {v0, v1}, Lcxc;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 162
    :cond_3
    invoke-virtual {v0, v2, v1, v3, v4}, Lcom/google/android/libraries/hangouts/video/CallSession;->startCall(Ljava/lang/String;Lcom/google/android/libraries/hangouts/video/HangoutRequest;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 164
    :cond_4
    const-string v1, "com.google.android.libraries.hangouts.video.ACTION_TERMINATE_CALL"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 165
    const-string v1, "com.google.android.libraries.hangouts.video.terminate_reason"

    const/16 v3, 0x3ec

    invoke-virtual {p1, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 167
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 168
    const-string v0, "vclib"

    const-string v1, "Ignoring ACTION_TERMINATE_CALL for empty sessionId"

    invoke-static {v0, v1}, Lcxc;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 170
    :cond_5
    invoke-virtual {v0, v2, v1}, Lcom/google/android/libraries/hangouts/video/CallSession;->terminateCall(Ljava/lang/String;I)Z

    goto :goto_1

    .line 172
    :cond_6
    const-string v1, "com.google.android.libraries.hangouts.video.ACTION_PUSH_NOTIFICATION"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 173
    const-string v1, "push_proto"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v1

    .line 174
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/CallSession;->handlePushNotification([B)V

    goto :goto_1

    .line 175
    :cond_7
    const-string v1, "com.google.android.libraries.hangouts.video.ACTION_INVITE_USERS"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 176
    const-string v1, "invite_type"

    invoke-virtual {p1, v1, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 178
    const-string v1, "invite_conversation"

    invoke-virtual {p1, v1, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 180
    const-string v2, "invited_user_ids"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 181
    const-string v3, "invited_circle_ids"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 182
    const-string v5, "ring_invitees"

    invoke-virtual {p1, v5, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 183
    const-string v6, "create_activity"

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    .line 185
    const-string v7, "hangout_start_context"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 187
    invoke-virtual/range {v0 .. v7}, Lcom/google/android/libraries/hangouts/video/CallSession;->inviteUsers(Z[Ljava/lang/String;[Ljava/lang/String;IZZLjava/lang/String;)V

    goto/16 :goto_1

    .line 189
    :cond_8
    const-string v1, "com.google.android.libraries.hangouts.video.ACTION_INVITE_PSTN"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 190
    const-string v1, "com.google.android.libraries.hangouts.video.pstn_jid"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 191
    const-string v2, "com.google.android.libraries.hangouts.video.phone_number"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 192
    const-string v3, "com.google.android.libraries.hangouts.video.keep_alive"

    invoke-virtual {p1, v3, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 193
    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/CallSession;->invitePstn(Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 194
    :cond_9
    const-string v1, "com.google.android.libraries.hangouts.video.ACTION_SEND_DTMF"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 195
    const-string v1, "com.google.android.libraries.hangouts.video.dtmf_code"

    invoke-virtual {p1, v1, v5}, Landroid/content/Intent;->getCharExtra(Ljava/lang/String;C)C

    move-result v1

    .line 196
    const-string v2, "com.google.android.libraries.hangouts.video.duration_millis"

    invoke-virtual {p1, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 197
    const-string v3, "com.google.android.libraries.hangouts.video.remote_muc_jid"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 198
    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/CallSession;->sendDtmf(CILjava/lang/String;)V

    goto/16 :goto_1

    .line 199
    :cond_a
    const-string v1, "com.google.android.libraries.hangouts.video.ACTION_SET_AUDIO_DEVICE"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 200
    const-string v1, "com.google.android.libraries.hangouts.video.audio_device"

    invoke-virtual {p1, v1, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 201
    if-eq v1, v6, :cond_0

    .line 202
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/AudioDevice;->values()[Lcom/google/android/libraries/hangouts/video/AudioDevice;

    move-result-object v2

    aget-object v1, v2, v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/CallSession;->setAudioDevice(Lcom/google/android/libraries/hangouts/video/AudioDevice;)V

    goto/16 :goto_1

    .line 204
    :cond_b
    const-string v1, "com.google.android.libraries.hangouts.video.ACTION_SET_MUTE"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 205
    const-string v1, "com.google.android.libraries.hangouts.video.mute"

    invoke-virtual {p1, v1, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/CallSession;->setMute(Z)V

    goto/16 :goto_1

    .line 206
    :cond_c
    const-string v1, "com.google.android.libraries.hangouts.video.ACTION_REMOTE_MUTE"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 207
    const-string v1, "com.google.android.libraries.hangouts.video.remote_mutee"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 208
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/CallSession;->remoteMute(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 209
    :cond_d
    const-string v1, "com.google.android.libraries.hangouts.video.ACTION_PUBLISH_VIDEO_MUTE_STATE"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 210
    const-string v1, "com.google.android.libraries.hangouts.video.video_mute"

    invoke-virtual {p1, v1, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 212
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/CallSession;->publishVideoMuteState(Z)V

    goto/16 :goto_1

    .line 213
    :cond_e
    const-string v1, "com.google.android.libraries.hangouts.video.ACTION_BLOCK_MEDIA"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 214
    const-string v1, "com.google.android.libraries.hangouts.video.media_blockee"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 215
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/CallSession;->blockMedia(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 216
    :cond_f
    const-string v1, "com.google.android.libraries.hangouts.video.ACTION_SET_CALL_TAG"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 217
    const-string v1, "com.google.android.libraries.hangouts.video.tag"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    .line 218
    if-eqz v1, :cond_0

    .line 219
    invoke-virtual {v0, v2, v1}, Lcom/google/android/libraries/hangouts/video/CallSession;->setCallTag(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto/16 :goto_1

    .line 221
    :cond_10
    const-string v1, "com.google.android.libraries.hangouts.video.ACTION_ONGOING_NOTIFICATION_RESPONSE"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 222
    const-string v0, "com.google.android.libraries.hangouts.video.ongoing_notif"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/Notification;

    .line 223
    if-eqz v0, :cond_0

    .line 224
    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->onReceiveOngoingNotification(Landroid/app/Notification;)V

    goto/16 :goto_1

    .line 226
    :cond_11
    const-string v1, "com.google.android.libraries.hangouts.video.ACTION_ADD_LOG_COMMENT"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 227
    const-string v1, "com.google.android.libraries.hangouts.video.log_comment"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 228
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/CallSession;->addLogComment(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 229
    :cond_12
    const-string v1, "com.google.android.libraries.hangouts.video.ACTION_ONGOING_NOTIFICATION_REQUEST"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 230
    invoke-direct {p0, v2}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->postOngoingNotification(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 231
    :cond_13
    const-string v1, "com.google.android.libraries.hangouts.video.ACTION_SIGN_IN"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 232
    if-eqz v4, :cond_0

    .line 233
    invoke-virtual {v0, v4}, Lcom/google/android/libraries/hangouts/video/CallSession;->signIn(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 236
    :cond_14
    const-string v0, "vclib"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown vc message action: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcxc;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_15
    move-object v2, v1

    goto/16 :goto_0
.end method

.method private initCallManager()Lcom/google/android/libraries/hangouts/video/CallManager;
    .locals 3

    .prologue
    .line 314
    invoke-static {p0}, Lcom/google/android/libraries/hangouts/video/CallManager;->getInstance(Lcom/google/android/libraries/hangouts/video/VideoChatService;)Lcom/google/android/libraries/hangouts/video/CallManager;

    move-result-object v0

    .line 315
    new-instance v1, Lcom/google/android/libraries/hangouts/video/VideoChatService$CallBoundaryCallback;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/libraries/hangouts/video/VideoChatService$CallBoundaryCallback;-><init>(Lcom/google/android/libraries/hangouts/video/VideoChatService;Lcom/google/android/libraries/hangouts/video/VideoChatService$1;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/CallManager;->setCallBoundaryCallback(Lcom/google/android/libraries/hangouts/video/CallManager$CallBoundaryCallback;)V

    .line 316
    return-object v0
.end method

.method private onReceiveOngoingNotification(Landroid/app/Notification;)V
    .locals 1

    .prologue
    .line 465
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mCallManager:Lcom/google/android/libraries/hangouts/video/CallManager;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallManager;->getCurrentCall()Lcom/google/android/libraries/hangouts/video/CallState;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 466
    iget v0, p1, Landroid/app/Notification;->flags:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p1, Landroid/app/Notification;->flags:I

    .line 467
    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->startForeground(ILandroid/app/Notification;)V

    .line 469
    :cond_0
    return-void
.end method

.method private postOngoingNotification(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 458
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.libraries.hangouts.video.ACTION_ONGOING_NOTIFICATION_REQUEST"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 459
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mOutputReceiverComponent:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 460
    const-string v1, "com.google.android.libraries.hangouts.video.sessionid"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 461
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->sendBroadcast(Landroid/content/Intent;)V

    .line 462
    return-void
.end method

.method private stopServiceIfSafe()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 418
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mKeepAliveRequests:Ljava/util/HashSet;

    monitor-enter v1

    .line 419
    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mStopped:Z

    if-nez v2, :cond_0

    .line 420
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->safeToStop()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 421
    const-string v2, "vclib"

    const-string v3, "Stopping VideoChatService..."

    invoke-static {v2, v3}, Lcxc;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->stopSelf()V

    .line 423
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mStopped:Z

    .line 424
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 428
    :goto_0
    return v0

    .line 427
    :cond_0
    monitor-exit v1

    .line 428
    const/4 v0, 0x0

    goto :goto_0

    .line 427
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method addKeepAliveRequest(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 250
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mKeepAliveRequests:Ljava/util/HashSet;

    monitor-enter v1

    .line 251
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mKeepAliveRequests:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcwz;->b(Z)V

    .line 252
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mKeepAliveRequests:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 253
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mKeepAliveRequests:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 254
    invoke-static {}, Lcxc;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 255
    const-string v0, "vclib"

    const-string v2, "add keep-alive for: %s (%d total)"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mKeepAliveRequests:Ljava/util/HashSet;

    .line 256
    invoke-virtual {v5}, Ljava/util/HashSet;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 255
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    .line 258
    :cond_1
    const-string v0, "vclib"

    const-string v2, "add keep-alive"

    invoke-static {v0, v2}, Lcxc;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 261
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mCallManager:Lcom/google/android/libraries/hangouts/video/CallManager;

    invoke-virtual {v0, p2}, Lcom/google/android/libraries/hangouts/video/CallManager;->dump(Ljava/io/PrintWriter;)V

    .line 247
    return-void
.end method

.method public getMobileHipriManager()Lcom/google/android/libraries/hangouts/video/MobileHipriManager;
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mMobileHipriManager:Lcom/google/android/libraries/hangouts/video/MobileHipriManager;

    return-object v0
.end method

.method public getNumKeepAliveRequests()I
    .locals 2

    .prologue
    .line 508
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mKeepAliveRequests:Ljava/util/HashSet;

    monitor-enter v1

    .line 509
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mKeepAliveRequests:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 510
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method getOutputReceiverComponent()Landroid/content/ComponentName;
    .locals 1

    .prologue
    .line 404
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mOutputReceiverComponent:Landroid/content/ComponentName;

    return-object v0
.end method

.method public isStopped()Z
    .locals 1

    .prologue
    .line 515
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mStopped:Z

    return v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    .prologue
    .line 290
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mStopped:Z

    .line 292
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.talk.SOFT_BIND"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 293
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mSoftBinder:Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;

    .line 295
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method onConnectionClosed(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 322
    const-string v0, "vclib"

    const-string v1, "onConnectionClosed"

    invoke-static {v0, v1}, Lcxc;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->removeKeepAliveRequest(Ljava/lang/String;)V

    .line 326
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mKeepAliveRequests:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 327
    return-void

    .line 326
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method onConnectionOpened(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mKeepAliveRequests:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 336
    const-string v0, "vclib"

    const-string v1, "onConnectionOpened"

    invoke-static {v0, v1}, Lcxc;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->addKeepAliveRequest(Ljava/lang/String;)V

    .line 338
    return-void
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 107
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 113
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->stopForeground(Z)V

    .line 115
    invoke-static {p0}, Lcom/google/android/libraries/hangouts/video/Registration;->getRegisteredComponents(Landroid/content/Context;)Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;

    move-result-object v0

    .line 116
    iget-object v1, v0, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->outgoingVideoChatReceiver:Landroid/content/ComponentName;

    if-eqz v1, :cond_0

    .line 117
    iget-object v0, v0, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->outgoingVideoChatReceiver:Landroid/content/ComponentName;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->setOutputReceiverComponent(Landroid/content/ComponentName;)V

    .line 123
    :goto_0
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->initCallManager()Lcom/google/android/libraries/hangouts/video/CallManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mCallManager:Lcom/google/android/libraries/hangouts/video/CallManager;

    .line 125
    new-instance v0, Lcom/google/android/libraries/hangouts/video/MobileHipriManager;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/hangouts/video/MobileHipriManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mMobileHipriManager:Lcom/google/android/libraries/hangouts/video/MobileHipriManager;

    .line 126
    return-void

    .line 120
    :cond_0
    const-string v0, "Registration.outgoingVideoChatReceiver not specified"

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 433
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 434
    const-string v0, "vclib"

    const-string v1, "VideoChatService#onDestroy"

    invoke-static {v0, v1}, Lcxc;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mCachedCallSession:Lcom/google/android/libraries/hangouts/video/CallSession;

    .line 436
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mCallManager:Lcom/google/android/libraries/hangouts/video/CallManager;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CallManager;->release()V

    .line 439
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mMobileHipriManager:Lcom/google/android/libraries/hangouts/video/MobileHipriManager;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/MobileHipriManager;->stopUsingMobileHipri()V

    .line 440
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1

    .prologue
    .line 131
    invoke-static {p1}, Lcwz;->b(Ljava/lang/Object;)V

    .line 133
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mStopped:Z

    .line 134
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->handleIncomingIntent(Landroid/content/Intent;)V

    .line 137
    const/4 v0, 0x2

    return v0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 409
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.talk.SOFT_BIND"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 413
    const/4 v0, 0x0

    return v0
.end method

.method postStopServiceIfSafe()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 499
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mStopServiceHandler:Lcom/google/android/libraries/hangouts/video/VideoChatService$StopServiceHandler;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/VideoChatService$StopServiceHandler;->removeMessages(I)V

    .line 500
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mStopServiceHandler:Lcom/google/android/libraries/hangouts/video/VideoChatService$StopServiceHandler;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/VideoChatService$StopServiceHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 501
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mStopServiceHandler:Lcom/google/android/libraries/hangouts/video/VideoChatService$StopServiceHandler;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/libraries/hangouts/video/VideoChatService$StopServiceHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 502
    return-void
.end method

.method public removeKeepAliveRequest(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 266
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mKeepAliveRequests:Ljava/util/HashSet;

    monitor-enter v1

    .line 272
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mKeepAliveRequests:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mKeepAliveRequests:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 274
    invoke-static {}, Lcxc;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 275
    const-string v0, "vclib"

    const-string v2, "remove keep-alive for: %s (%d remaining)"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mKeepAliveRequests:Ljava/util/HashSet;

    .line 276
    invoke-virtual {v5}, Ljava/util/HashSet;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 275
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    :cond_0
    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 282
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->stopServiceIfSafe()Z

    .line 283
    return-void

    .line 278
    :cond_1
    :try_start_1
    const-string v0, "vclib"

    const-string v2, "remove keep-alive"

    invoke-static {v0, v2}, Lcxc;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 281
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method removeOngoingNotification()V
    .locals 1

    .prologue
    .line 476
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/hangouts/video/VideoChatService;->stopForeground(Z)V

    .line 477
    return-void
.end method

.method public safeToStop()Z
    .locals 2

    .prologue
    .line 447
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mKeepAliveRequests:Ljava/util/HashSet;

    monitor-enter v1

    .line 448
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mKeepAliveRequests:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 449
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method setOutputReceiverComponent(Landroid/content/ComponentName;)V
    .locals 0

    .prologue
    .line 310
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/VideoChatService;->mOutputReceiverComponent:Landroid/content/ComponentName;

    .line 311
    return-void
.end method
