.class public Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final error:I

.field private final rtt:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;->rtt:I

    .line 25
    iput p2, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;->error:I

    .line 26
    return-void
.end method


# virtual methods
.method public toProto()Ldop;
    .locals 2

    .prologue
    .line 37
    new-instance v0, Ldop;

    invoke-direct {v0}, Ldop;-><init>()V

    .line 39
    iget v1, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;->rtt:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Ldop;->b:Ljava/lang/Integer;

    .line 40
    iget v1, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;->error:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Ldop;->c:Ljava/lang/Integer;

    .line 41
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 30
    invoke-static {p0}, Lm;->a(Ljava/lang/Object;)Lebg;

    move-result-object v0

    const-string v1, "rtt"

    iget v2, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;->rtt:I

    .line 31
    invoke-virtual {v0, v1, v2}, Lebg;->a(Ljava/lang/String;I)Lebg;

    move-result-object v0

    const-string v1, "error"

    iget v2, p0, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$ConnectInfo;->error:I

    .line 32
    invoke-virtual {v0, v1, v2}, Lebg;->a(Ljava/lang/String;I)Lebg;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Lebg;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
