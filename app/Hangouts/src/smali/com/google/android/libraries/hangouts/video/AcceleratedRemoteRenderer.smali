.class public Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;
.super Lcom/google/android/libraries/hangouts/video/Renderer;
.source "PG"


# static fields
.field private static final STATS_TO_KEEP:I = 0x78

.field private static final STATS_UPDATE_PERIOD_MS:I = 0x2710

.field private static final TAG:Ljava/lang/String; = "vclib"


# instance fields
.field private final mDecoder:Lcom/google/android/libraries/hangouts/video/Decoder;

.field private mFirstStatsUpdateTime:J

.field private mLastHeight:I

.field private mLastStatsUpdateTime:J

.field private mLastWidth:I

.field private mNumFramesRendered:J

.field private mNumRendererCalls:J

.field mStats:Lcxa;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcxa",
            "<",
            "Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer$Stats;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/android/libraries/hangouts/video/RendererManager;Lcom/google/android/libraries/hangouts/video/Decoder;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 50
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/Renderer;-><init>()V

    .line 35
    new-instance v0, Lcxa;

    const/16 v1, 0x78

    invoke-direct {v0, v1}, Lcxa;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;->mStats:Lcxa;

    .line 51
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    .line 53
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;->mRendererID:I

    .line 54
    iput-object p2, p0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;->mDecoder:Lcom/google/android/libraries/hangouts/video/Decoder;

    .line 55
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/hangouts/video/RendererManager;->registerRendererForStats(Lcom/google/android/libraries/hangouts/video/Renderer;)V

    .line 57
    iput-wide v2, p0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;->mNumFramesRendered:J

    .line 58
    iput-wide v2, p0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;->mNumRendererCalls:J

    .line 59
    return-void
.end method


# virtual methods
.method public drawTexture(Lcom/google/android/libraries/hangouts/video/Renderer$DrawInputParams;Lcom/google/android/libraries/hangouts/video/Renderer$DrawOutputParams;)Z
    .locals 9

    .prologue
    const-wide/16 v5, 0x1

    const/4 v1, 0x0

    .line 109
    check-cast p2, Lcom/google/android/libraries/hangouts/video/RemoteRenderer$RendererFrameOutputData;

    .line 111
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;->mDecoder:Lcom/google/android/libraries/hangouts/video/Decoder;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/Decoder;->drawTexture()Z

    move-result v2

    .line 112
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;->mDecoder:Lcom/google/android/libraries/hangouts/video/Decoder;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/Decoder;->getCurrentOutputFormat()Landroid/media/MediaFormat;

    move-result-object v0

    .line 113
    if-eqz v2, :cond_4

    if-eqz v0, :cond_4

    .line 114
    const-string v3, "width"

    invoke-virtual {v0, v3}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v3

    .line 115
    const-string v4, "height"

    invoke-virtual {v0, v4}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v4

    .line 116
    iput v3, p2, Lcom/google/android/libraries/hangouts/video/RemoteRenderer$RendererFrameOutputData;->frameWidth:I

    .line 117
    iput v4, p2, Lcom/google/android/libraries/hangouts/video/RemoteRenderer$RendererFrameOutputData;->frameHeight:I

    .line 118
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;->mLastWidth:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;->mLastWidth:I

    if-ne v3, v0, :cond_1

    :cond_0
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;->mLastHeight:I

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;->mLastHeight:I

    if-eq v4, v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p2, Lcom/google/android/libraries/hangouts/video/RemoteRenderer$RendererFrameOutputData;->frameSizeChanged:Z

    .line 121
    iput v3, p0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;->mLastWidth:I

    .line 122
    iput v4, p0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;->mLastHeight:I

    .line 123
    iget-wide v3, p0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;->mNumFramesRendered:J

    add-long/2addr v3, v5

    iput-wide v3, p0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;->mNumFramesRendered:J

    .line 127
    :goto_1
    iget-wide v3, p0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;->mNumRendererCalls:J

    add-long/2addr v3, v5

    iput-wide v3, p0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;->mNumRendererCalls:J

    .line 129
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 130
    iget-wide v5, p0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;->mLastStatsUpdateTime:J

    sub-long v5, v3, v5

    const-wide/16 v7, 0x2710

    cmp-long v0, v5, v7

    if-lez v0, :cond_2

    .line 131
    new-instance v0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer$Stats;

    const/4 v5, 0x0

    invoke-direct {v0, p0, v5}, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer$Stats;-><init>(Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer$1;)V

    .line 132
    iput-wide v3, v0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer$Stats;->time:J

    .line 134
    iput v1, v0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer$Stats;->dropped:I

    .line 135
    iput v1, v0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer$Stats;->pushed:I

    .line 136
    iget-wide v5, p0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;->mNumFramesRendered:J

    iput-wide v5, v0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer$Stats;->renderered:J

    .line 137
    iget-wide v5, p0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;->mNumRendererCalls:J

    iput-wide v5, v0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer$Stats;->rendererCalls:J

    .line 138
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;->mStats:Lcxa;

    monitor-enter v1

    .line 139
    :try_start_0
    iget-object v5, p0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;->mStats:Lcxa;

    invoke-virtual {v5, v0}, Lcxa;->a(Ljava/lang/Object;)V

    .line 140
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 141
    iput-wide v3, p0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;->mLastStatsUpdateTime:J

    .line 143
    :cond_2
    iput-boolean v2, p2, Lcom/google/android/libraries/hangouts/video/RemoteRenderer$RendererFrameOutputData;->updatedTexture:Z

    .line 144
    return v2

    :cond_3
    move v0, v1

    .line 118
    goto :goto_0

    .line 125
    :cond_4
    iput-boolean v1, p2, Lcom/google/android/libraries/hangouts/video/RemoteRenderer$RendererFrameOutputData;->frameSizeChanged:Z

    goto :goto_1

    .line 140
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 11

    .prologue
    .line 63
    const-string v0, "Accelerated Renderer -- dropped, pushed, new frames renderered, rendered"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 64
    new-instance v1, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer$Stats;

    const/4 v0, 0x0

    invoke-direct {v1, p0, v0}, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer$Stats;-><init>(Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer$1;)V

    .line 65
    iget-wide v2, p0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;->mFirstStatsUpdateTime:J

    iput-wide v2, v1, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer$Stats;->time:J

    .line 67
    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;->mStats:Lcxa;

    monitor-enter v3

    .line 68
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;->mStats:Lcxa;

    invoke-virtual {v0}, Lcxa;->a()I

    move-result v4

    .line 69
    const/4 v0, 0x0

    move-object v2, v1

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    .line 70
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;->mStats:Lcxa;

    invoke-virtual {v0, v1}, Lcxa;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer$Stats;

    .line 71
    new-instance v5, Ljava/util/Date;

    iget-wide v6, v0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer$Stats;->time:J

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    .line 72
    iget-wide v6, v0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer$Stats;->time:J

    iget-wide v8, v2, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer$Stats;->time:J

    sub-long/2addr v6, v8

    long-to-float v6, v6

    const/high16 v7, 0x447a0000    # 1000.0f

    div-float/2addr v6, v7

    .line 73
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " -- "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v7, v0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer$Stats;->dropped:I

    iget v8, v2, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer$Stats;->dropped:I

    sub-int/2addr v7, v8

    int-to-float v7, v7

    div-float/2addr v7, v6

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v7, v0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer$Stats;->pushed:I

    iget v8, v2, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer$Stats;->pushed:I

    sub-int/2addr v7, v8

    int-to-float v7, v7

    div-float/2addr v7, v6

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v7, v0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer$Stats;->renderered:J

    iget-wide v9, v2, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer$Stats;->renderered:J

    sub-long/2addr v7, v9

    long-to-float v7, v7

    div-float/2addr v7, v6

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v7, v0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer$Stats;->rendererCalls:J

    iget-wide v9, v2, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer$Stats;->rendererCalls:J

    sub-long/2addr v7, v9

    long-to-float v2, v7

    div-float/2addr v2, v6

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 69
    add-int/lit8 v1, v1, 0x1

    move-object v2, v0

    goto :goto_0

    .line 80
    :cond_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method

.method public getOutputTextureName()I
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;->mDecoder:Lcom/google/android/libraries/hangouts/video/Decoder;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/Decoder;->getOutputTextureName()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 92
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;->mDecoder:Lcom/google/android/libraries/hangouts/video/Decoder;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/Decoder;->getOutputTextureName()I

    move-result v0

    return v0
.end method

.method public initializeGLContext()V
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;->mDecoder:Lcom/google/android/libraries/hangouts/video/Decoder;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/Decoder;->initializeGLContext()V

    .line 86
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;->mFirstStatsUpdateTime:J

    iput-wide v0, p0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;->mLastStatsUpdateTime:J

    .line 87
    return-void
.end method

.method public isExternalTexture()Z
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x1

    return v0
.end method

.method public release()V
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;->mDecoder:Lcom/google/android/libraries/hangouts/video/Decoder;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/Decoder;->release()V

    .line 103
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/AcceleratedRemoteRenderer;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/hangouts/video/RendererManager;->unregisterRendererForStats(Lcom/google/android/libraries/hangouts/video/Renderer;)V

    .line 104
    return-void
.end method
