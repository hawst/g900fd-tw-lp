.class public Lcom/google/android/libraries/hangouts/video/ConnectionMonitor;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final TAG:Ljava/lang/String; = "vclib"

.field private static final UNAVAILABLE:I = -0x1


# instance fields
.field private final mConnectivityManager:Landroid/net/ConnectivityManager;

.field private final mContext:Landroid/content/Context;

.field private final mTelephonyManager:Landroid/telephony/TelephonyManager;

.field private final mWifiManager:Landroid/net/wifi/WifiManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const-string v0, "vclib"

    const-string v1, "ConnectionMonitor#constructor"

    invoke-static {v0, v1}, Lcxc;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/ConnectionMonitor;->mContext:Landroid/content/Context;

    .line 47
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ConnectionMonitor;->mContext:Landroid/content/Context;

    const-string v1, "connectivity"

    .line 48
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/ConnectionMonitor;->mConnectivityManager:Landroid/net/ConnectivityManager;

    .line 49
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ConnectionMonitor;->mContext:Landroid/content/Context;

    const-string v1, "wifi"

    .line 50
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/ConnectionMonitor;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 51
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ConnectionMonitor;->mContext:Landroid/content/Context;

    const-string v1, "phone"

    .line 52
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/ConnectionMonitor;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 53
    return-void
.end method

.method private getCellNetworkType(I)I
    .locals 1

    .prologue
    .line 191
    packed-switch p1, :pswitch_data_0

    .line 211
    const/4 v0, 0x5

    :goto_0
    return v0

    .line 197
    :pswitch_0
    const/4 v0, 0x6

    goto :goto_0

    .line 207
    :pswitch_1
    const/4 v0, 0x7

    goto :goto_0

    .line 209
    :pswitch_2
    const/16 v0, 0x8

    goto :goto_0

    .line 191
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private getRegisteredCellInfo()Landroid/telephony/CellInfo;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 164
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-ge v0, v2, :cond_0

    move-object v0, v1

    .line 175
    :goto_0
    return-object v0

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ConnectionMonitor;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getAllCellInfo()Ljava/util/List;

    move-result-object v0

    .line 168
    if-eqz v0, :cond_2

    .line 169
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/CellInfo;

    .line 170
    invoke-virtual {v0}, Landroid/telephony/CellInfo;->isRegistered()Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 175
    goto :goto_0
.end method

.method private getWifiSignalLevel()I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 104
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-ge v1, v2, :cond_0

    .line 118
    :goto_0
    return v0

    .line 107
    :cond_0
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/libraries/hangouts/video/ConnectionMonitor;->isNetworkConnected(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 108
    const-string v1, "vclib"

    const-string v2, "ConnectionMonitor wifi (not connected"

    invoke-static {v1, v2}, Lcxc;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 111
    :cond_1
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/ConnectionMonitor;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getRssi()I

    move-result v1

    .line 114
    const/16 v2, -0xc8

    if-ne v1, v2, :cond_2

    .line 115
    const-string v1, "vclib"

    const-string v2, "WifiManager.getConnectionInfo().getRssi() not working"

    invoke-static {v1, v2}, Lcxc;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 118
    :cond_2
    const/16 v0, 0x64

    invoke-static {v1, v0}, Landroid/net/wifi/WifiManager;->calculateSignalLevel(II)I

    move-result v0

    goto :goto_0
.end method

.method private isCellNetworkType(I)Z
    .locals 1

    .prologue
    .line 179
    if-eqz p1, :cond_0

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/4 v0, 0x5

    if-eq p1, v0, :cond_0

    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isNetworkConnected(I)Z
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ConnectionMonitor;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v0, p1}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 217
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateCellSignalStrength(Ldog;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, -0x1

    .line 122
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/ConnectionMonitor;->getRegisteredCellInfo()Landroid/telephony/CellInfo;

    move-result-object v0

    .line 126
    if-eqz v0, :cond_0

    invoke-direct {p0, v2}, Lcom/google/android/libraries/hangouts/video/ConnectionMonitor;->isNetworkConnected(I)Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    move-object v0, v1

    move v1, v3

    .line 151
    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p1, Ldog;->c:Ljava/lang/Integer;

    .line 153
    if-eqz v0, :cond_6

    .line 154
    invoke-virtual {v0}, Landroid/telephony/CellSignalStrength;->getLevel()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p1, Ldog;->d:Ljava/lang/Integer;

    .line 155
    invoke-virtual {v0}, Landroid/telephony/CellSignalStrength;->getAsuLevel()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Ldog;->e:Ljava/lang/Integer;

    .line 160
    :goto_1
    return-void

    .line 129
    :cond_1
    instance-of v4, v0, Landroid/telephony/CellInfoCdma;

    if-eqz v4, :cond_2

    .line 130
    const/4 v1, 0x1

    .line 131
    check-cast v0, Landroid/telephony/CellInfoCdma;

    .line 132
    invoke-virtual {v0}, Landroid/telephony/CellInfoCdma;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthCdma;

    move-result-object v0

    goto :goto_0

    .line 133
    :cond_2
    instance-of v4, v0, Landroid/telephony/CellInfoGsm;

    if-eqz v4, :cond_3

    .line 134
    const/4 v1, 0x2

    .line 135
    check-cast v0, Landroid/telephony/CellInfoGsm;

    .line 136
    invoke-virtual {v0}, Landroid/telephony/CellInfoGsm;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthGsm;

    move-result-object v0

    goto :goto_0

    .line 137
    :cond_3
    instance-of v4, v0, Landroid/telephony/CellInfoLte;

    if-eqz v4, :cond_4

    .line 138
    const/4 v1, 0x3

    .line 139
    check-cast v0, Landroid/telephony/CellInfoLte;

    .line 140
    invoke-virtual {v0}, Landroid/telephony/CellInfoLte;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthLte;

    move-result-object v0

    goto :goto_0

    .line 141
    :cond_4
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x12

    if-lt v4, v5, :cond_5

    instance-of v4, v0, Landroid/telephony/CellInfoWcdma;

    if-eqz v4, :cond_5

    .line 143
    const/4 v1, 0x4

    .line 144
    check-cast v0, Landroid/telephony/CellInfoWcdma;

    .line 145
    invoke-virtual {v0}, Landroid/telephony/CellInfoWcdma;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthWcdma;

    move-result-object v0

    goto :goto_0

    :cond_5
    move-object v0, v1

    move v1, v2

    .line 148
    goto :goto_0

    .line 157
    :cond_6
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Ldog;->d:Ljava/lang/Integer;

    .line 158
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Ldog;->e:Ljava/lang/Integer;

    goto :goto_1
.end method

.method private updateWifiSignalStrength(Ldog;)V
    .locals 1

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/ConnectionMonitor;->getWifiSignalLevel()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Ldog;->b:Ljava/lang/Integer;

    .line 101
    return-void
.end method


# virtual methods
.method public getNetworkType(I)I
    .locals 1

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/ConnectionMonitor;->isCellNetworkType(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ConnectionMonitor;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/ConnectionMonitor;->getCellNetworkType(I)I

    move-result v0

    .line 96
    :goto_0
    return v0

    .line 86
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 96
    :pswitch_0
    const/4 v0, -0x1

    goto :goto_0

    .line 88
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 90
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 92
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 94
    :pswitch_4
    const/4 v0, 0x4

    goto :goto_0

    .line 86
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getSignalStrength(I)Ldog;
    .locals 2

    .prologue
    .line 64
    new-instance v0, Ldog;

    invoke-direct {v0}, Ldog;-><init>()V

    .line 65
    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    .line 66
    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/ConnectionMonitor;->updateWifiSignalStrength(Ldog;)V

    .line 70
    :cond_0
    :goto_0
    return-object v0

    .line 67
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/ConnectionMonitor;->isCellNetworkType(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 68
    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/ConnectionMonitor;->updateCellSignalStrength(Ldog;)V

    goto :goto_0
.end method
