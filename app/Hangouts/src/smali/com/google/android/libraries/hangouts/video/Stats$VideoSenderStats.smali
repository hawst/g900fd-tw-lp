.class public Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;
.super Lcom/google/android/libraries/hangouts/video/Stats;
.source "PG"


# instance fields
.field public final bytesSent:I

.field public final codecName:Ljava/lang/String;

.field public final firsRcvd:I

.field public final fractionLost:F

.field public final frameHeight:I

.field public final frameRateInput:I

.field public final frameRateSent:I

.field public final frameWidth:I

.field public final nacksRcvd:I

.field public final nominalBitRate:I

.field public final packetsLost:I

.field public final packetsSent:I

.field public final preferredBitRate:I

.field public final rttMillis:I

.field public final ssrc:I


# direct methods
.method public constructor <init>(ILjava/lang/String;IIIFIIIIIIIII)V
    .locals 0

    .prologue
    .line 279
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/Stats;-><init>()V

    .line 280
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->ssrc:I

    .line 281
    iput-object p2, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->codecName:Ljava/lang/String;

    .line 282
    iput p3, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->bytesSent:I

    .line 283
    iput p4, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->packetsSent:I

    .line 284
    iput p5, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->packetsLost:I

    .line 285
    iput p6, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->fractionLost:F

    .line 286
    iput p7, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->firsRcvd:I

    .line 287
    iput p8, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->nacksRcvd:I

    .line 288
    iput p9, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->rttMillis:I

    .line 289
    iput p10, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->frameWidth:I

    .line 290
    iput p11, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->frameHeight:I

    .line 291
    iput p12, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->frameRateInput:I

    .line 292
    iput p13, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->frameRateSent:I

    .line 293
    iput p14, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->nominalBitRate:I

    .line 294
    iput p15, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->preferredBitRate:I

    .line 295
    return-void
.end method

.method static printLegend(Ljava/io/PrintWriter;)V
    .locals 1

    .prologue
    .line 324
    const-string v0, "  VideoSender -- ssrc, codec, sent, lost, rcvd, rtt, size, in, sent, rate"

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 326
    return-void
.end method


# virtual methods
.method public addTo(Ldoe;)V
    .locals 4

    .prologue
    .line 299
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/Stats;->createEmptyMediaProto(I)Ldoh;

    move-result-object v1

    .line 300
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->l:Ljava/lang/Integer;

    .line 301
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->ssrc:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->m:Ljava/lang/Integer;

    .line 302
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->codecName:Ljava/lang/String;

    iput-object v0, v1, Ldoh;->G:Ljava/lang/String;

    .line 303
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->bytesSent:I

    int-to-long v2, v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v1, Ldoh;->h:Ljava/lang/Long;

    .line 304
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->packetsSent:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->i:Ljava/lang/Integer;

    .line 305
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->packetsLost:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->d:Ljava/lang/Integer;

    .line 306
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->fractionLost:F

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v0, v2

    float-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->c:Ljava/lang/Integer;

    .line 307
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->firsRcvd:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->o:Ljava/lang/Integer;

    .line 308
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->nacksRcvd:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->p:Ljava/lang/Integer;

    .line 309
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->rttMillis:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->g:Ljava/lang/Integer;

    .line 310
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->frameWidth:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->P:Ljava/lang/Integer;

    .line 311
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->frameHeight:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->Q:Ljava/lang/Integer;

    .line 312
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->frameRateInput:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->q:Ljava/lang/Integer;

    .line 313
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->frameRateSent:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->r:Ljava/lang/Integer;

    .line 318
    iget-object v0, p1, Ldoe;->c:[Ldoh;

    array-length v2, v0

    .line 319
    iget-object v0, p1, Ldoe;->c:[Ldoh;

    add-int/lit8 v3, v2, 0x1

    invoke-static {v0, v3}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldoh;

    iput-object v0, p1, Ldoe;->c:[Ldoh;

    .line 320
    iget-object v0, p1, Ldoe;->c:[Ldoh;

    aput-object v1, v0, v2

    .line 321
    return-void
.end method

.method public print(Ljava/io/PrintWriter;Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;)V
    .locals 2

    .prologue
    .line 330
    # getter for: Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;->frameRateSendList:Ljava/util/ArrayList;
    invoke-static {p2}, Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;->access$100(Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;)Ljava/util/ArrayList;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->frameRateSent:I

    int-to-float v1, v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 331
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, " -- VideoSender -- "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->ssrc:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->codecName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->packetsSent:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->bytesSent:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "), "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->packetsLost:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->fractionLost:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "), "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->firsRcvd:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->nacksRcvd:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "), "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->rttMillis:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->frameWidth:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->frameHeight:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->frameRateInput:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->frameRateSent:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->nominalBitRate:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->preferredBitRate:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 342
    return-void
.end method
