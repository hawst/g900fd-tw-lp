.class Lcom/google/android/libraries/hangouts/video/CallManager$4;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

.field final synthetic val$isMuted:Z


# direct methods
.method constructor <init>(Lcom/google/android/libraries/hangouts/video/CallManager;Z)V
    .locals 0

    .prologue
    .line 877
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CallManager$4;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    iput-boolean p2, p0, Lcom/google/android/libraries/hangouts/video/CallManager$4;->val$isMuted:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 880
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager$4;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallManager;->access$1500(Lcom/google/android/libraries/hangouts/video/CallManager;)Lcom/google/android/libraries/hangouts/video/CallState;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 881
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager$4;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager$4;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    .line 882
    # getter for: Lcom/google/android/libraries/hangouts/video/CallManager;->mCurrentCall:Lcom/google/android/libraries/hangouts/video/CallState;
    invoke-static {v1}, Lcom/google/android/libraries/hangouts/video/CallManager;->access$1500(Lcom/google/android/libraries/hangouts/video/CallManager;)Lcom/google/android/libraries/hangouts/video/CallState;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/libraries/hangouts/video/CallState;->mSelf:Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;

    new-instance v2, Lcom/google/android/libraries/hangouts/video/endpoint/AudioMuteEvent;

    iget-boolean v3, p0, Lcom/google/android/libraries/hangouts/video/CallManager$4;->val$isMuted:Z

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/google/android/libraries/hangouts/video/endpoint/AudioMuteEvent;-><init>(ZLcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V

    .line 881
    # invokes: Lcom/google/android/libraries/hangouts/video/CallManager;->broadcastEndpointEvent(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/EndpointEvent;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/libraries/hangouts/video/CallManager;->access$1600(Lcom/google/android/libraries/hangouts/video/CallManager;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/EndpointEvent;)V

    .line 884
    :cond_0
    return-void
.end method
