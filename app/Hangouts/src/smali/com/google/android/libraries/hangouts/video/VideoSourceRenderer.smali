.class public Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final EXTERNAL_FRAGMENT_SHADER:Ljava/lang/String; = "#extension GL_OES_EGL_image_external : require\nuniform samplerExternalOES s_texture;precision mediump float;varying vec2 v_texCoord;void main() {  gl_FragColor = texture2D(s_texture, v_texCoord);}"

.field private static final FLOAT_SIZE_BYTES:I = 0x4

.field private static final FRAGMENT_SHADER:Ljava/lang/String; = "uniform sampler2D s_texture;precision mediump float;varying vec2 v_texCoord;void main() {  gl_FragColor = texture2D(s_texture, v_texCoord);}"

.field private static final FRAGMENT_SHADER_BODY:Ljava/lang/String; = "precision mediump float;varying vec2 v_texCoord;void main() {  gl_FragColor = texture2D(s_texture, v_texCoord);}"

.field private static final VERTEX_SHADER:Ljava/lang/String; = "attribute vec4 vPosition;attribute vec2 a_texCoord;varying vec2 v_texCoord;void main() {  gl_Position = vPosition;  v_texCoord = a_texCoord;}"


# instance fields
.field private final destinationFloatRect:Lcom/google/android/libraries/hangouts/video/FloatRect;

.field private final destinationVertices:[F

.field private glProgram:I

.field private lastInputHeight:I

.field private lastInputWidth:I

.field private final quadVertices:Ljava/nio/FloatBuffer;

.field private final surfaceInfo:Lcom/google/android/libraries/hangouts/video/SurfaceInfo;

.field private texCoordHandle:I

.field private texHandle:I

.field private final textureVertices:Ljava/nio/FloatBuffer;

.field private triangleVertsHandle:I

.field private final videoSource:Lcom/google/android/libraries/hangouts/video/VideoSource;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/hangouts/video/VideoSource;Lcom/google/android/libraries/hangouts/video/SurfaceInfo;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Lcom/google/android/libraries/hangouts/video/FloatRect;

    invoke-direct {v0}, Lcom/google/android/libraries/hangouts/video/FloatRect;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->destinationFloatRect:Lcom/google/android/libraries/hangouts/video/FloatRect;

    .line 37
    const/16 v0, 0x8

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->destinationVertices:[F

    .line 38
    iput v1, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->lastInputWidth:I

    .line 39
    iput v1, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->lastInputHeight:I

    .line 73
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->videoSource:Lcom/google/android/libraries/hangouts/video/VideoSource;

    .line 74
    iput-object p2, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->surfaceInfo:Lcom/google/android/libraries/hangouts/video/SurfaceInfo;

    .line 76
    const/16 v0, 0x20

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 77
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 78
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->textureVertices:Ljava/nio/FloatBuffer;

    .line 79
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->destinationVertices:[F

    array-length v0, v0

    shl-int/lit8 v0, v0, 0x2

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 80
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 81
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->quadVertices:Ljava/nio/FloatBuffer;

    .line 82
    return-void
.end method

.method private maybeInitGl()V
    .locals 2

    .prologue
    .line 85
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->glProgram:I

    if-nez v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->videoSource:Lcom/google/android/libraries/hangouts/video/VideoSource;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/VideoSource;->isExternalTexture()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 88
    const-string v0, "attribute vec4 vPosition;attribute vec2 a_texCoord;varying vec2 v_texCoord;void main() {  gl_Position = vPosition;  v_texCoord = a_texCoord;}"

    const-string v1, "#extension GL_OES_EGL_image_external : require\nuniform samplerExternalOES s_texture;precision mediump float;varying vec2 v_texCoord;void main() {  gl_FragColor = texture2D(s_texture, v_texCoord);}"

    invoke-static {v0, v1}, Lf;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->glProgram:I

    .line 89
    const-string v0, "failed to compile regular shaders"

    invoke-static {v0}, Lf;->s(Ljava/lang/String;)V

    .line 96
    :goto_0
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->glProgram:I

    const-string v1, "a_texCoord"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->texCoordHandle:I

    .line 97
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->glProgram:I

    const-string v1, "vPosition"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->triangleVertsHandle:I

    .line 98
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->glProgram:I

    const-string v1, "s_texture"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->texHandle:I

    .line 99
    const-string v0, "get..Location"

    invoke-static {v0}, Lf;->s(Ljava/lang/String;)V

    .line 101
    :cond_0
    return-void

    .line 91
    :cond_1
    const-string v0, "attribute vec4 vPosition;attribute vec2 a_texCoord;varying vec2 v_texCoord;void main() {  gl_Position = vPosition;  v_texCoord = a_texCoord;}"

    const-string v1, "uniform sampler2D s_texture;precision mediump float;varying vec2 v_texCoord;void main() {  gl_FragColor = texture2D(s_texture, v_texCoord);}"

    invoke-static {v0, v1}, Lf;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->glProgram:I

    .line 92
    const-string v0, "failed to compile OES shaders"

    invoke-static {v0}, Lf;->s(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private maybeUpdateOutputRectangle()V
    .locals 4

    .prologue
    .line 121
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->lastInputWidth:I

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->videoSource:Lcom/google/android/libraries/hangouts/video/VideoSource;

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/VideoSource;->getWidth()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->lastInputHeight:I

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->videoSource:Lcom/google/android/libraries/hangouts/video/VideoSource;

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/VideoSource;->getHeight()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 152
    :goto_0
    return-void

    .line 124
    :cond_0
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->updateSourceTextureCoordinates()V

    .line 127
    new-instance v0, Lcom/google/android/libraries/hangouts/video/Size;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->videoSource:Lcom/google/android/libraries/hangouts/video/VideoSource;

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/VideoSource;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->videoSource:Lcom/google/android/libraries/hangouts/video/VideoSource;

    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/VideoSource;->getHeight()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/hangouts/video/Size;-><init>(II)V

    .line 128
    new-instance v1, Lcom/google/android/libraries/hangouts/video/Size;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->surfaceInfo:Lcom/google/android/libraries/hangouts/video/SurfaceInfo;

    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/SurfaceInfo;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->surfaceInfo:Lcom/google/android/libraries/hangouts/video/SurfaceInfo;

    invoke-virtual {v3}, Lcom/google/android/libraries/hangouts/video/SurfaceInfo;->getHeight()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/Size;-><init>(II)V

    .line 130
    iget v2, v0, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    iget v3, v1, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    if-gt v2, v3, :cond_1

    iget v2, v0, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    iget v3, v1, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    if-le v2, v3, :cond_2

    .line 131
    :cond_1
    invoke-static {v0, v1}, Lcom/google/android/libraries/hangouts/video/Size;->scaleDownToFitInside(Lcom/google/android/libraries/hangouts/video/Size;Lcom/google/android/libraries/hangouts/video/Size;)Lcom/google/android/libraries/hangouts/video/Size;

    move-result-object v0

    .line 139
    :goto_1
    iget v1, v0, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->surfaceInfo:Lcom/google/android/libraries/hangouts/video/SurfaceInfo;

    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/SurfaceInfo;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 140
    iget v0, v0, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    int-to-float v0, v0

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->surfaceInfo:Lcom/google/android/libraries/hangouts/video/SurfaceInfo;

    invoke-virtual {v2}, Lcom/google/android/libraries/hangouts/video/SurfaceInfo;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v0, v2

    .line 142
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->destinationFloatRect:Lcom/google/android/libraries/hangouts/video/FloatRect;

    neg-float v3, v1

    iput v3, v2, Lcom/google/android/libraries/hangouts/video/FloatRect;->left:F

    .line 143
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->destinationFloatRect:Lcom/google/android/libraries/hangouts/video/FloatRect;

    iput v0, v2, Lcom/google/android/libraries/hangouts/video/FloatRect;->top:F

    .line 144
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->destinationFloatRect:Lcom/google/android/libraries/hangouts/video/FloatRect;

    iput v1, v2, Lcom/google/android/libraries/hangouts/video/FloatRect;->right:F

    .line 145
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->destinationFloatRect:Lcom/google/android/libraries/hangouts/video/FloatRect;

    neg-float v0, v0

    iput v0, v1, Lcom/google/android/libraries/hangouts/video/FloatRect;->bottom:F

    .line 147
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->destinationFloatRect:Lcom/google/android/libraries/hangouts/video/FloatRect;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->destinationVertices:[F

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->convertPointsToVertices(Lcom/google/android/libraries/hangouts/video/FloatRect;[F)V

    .line 148
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->quadVertices:Ljava/nio/FloatBuffer;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->destinationVertices:[F

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 150
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->videoSource:Lcom/google/android/libraries/hangouts/video/VideoSource;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/VideoSource;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->lastInputWidth:I

    .line 151
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->videoSource:Lcom/google/android/libraries/hangouts/video/VideoSource;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/VideoSource;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->lastInputHeight:I

    goto/16 :goto_0

    .line 133
    :cond_2
    invoke-static {v0, v1}, Lcom/google/android/libraries/hangouts/video/Size;->scaleUpToFitInside(Lcom/google/android/libraries/hangouts/video/Size;Lcom/google/android/libraries/hangouts/video/Size;)Lcom/google/android/libraries/hangouts/video/Size;

    move-result-object v0

    goto :goto_1
.end method

.method private updateSourceTextureCoordinates()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 161
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->videoSource:Lcom/google/android/libraries/hangouts/video/VideoSource;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/VideoSource;->shouldFlipTexture()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    move v3, v2

    .line 169
    :goto_0
    const/16 v4, 0x8

    new-array v4, v4, [F

    aput v3, v4, v6

    const/4 v5, 0x1

    aput v2, v4, v5

    const/4 v5, 0x2

    aput v0, v4, v5

    const/4 v5, 0x3

    aput v2, v4, v5

    const/4 v2, 0x4

    aput v0, v4, v2

    const/4 v0, 0x5

    aput v1, v4, v0

    const/4 v0, 0x6

    aput v3, v4, v0

    const/4 v0, 0x7

    aput v1, v4, v0

    .line 175
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->textureVertices:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v4}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 176
    return-void

    :cond_0
    move v0, v2

    move v3, v1

    .line 166
    goto :goto_0
.end method


# virtual methods
.method convertPointsToVertices(Lcom/google/android/libraries/hangouts/video/FloatRect;[F)V
    .locals 2

    .prologue
    .line 110
    const/4 v0, 0x0

    iget v1, p1, Lcom/google/android/libraries/hangouts/video/FloatRect;->left:F

    aput v1, p2, v0

    .line 111
    const/4 v0, 0x1

    iget v1, p1, Lcom/google/android/libraries/hangouts/video/FloatRect;->bottom:F

    aput v1, p2, v0

    .line 112
    const/4 v0, 0x2

    iget v1, p1, Lcom/google/android/libraries/hangouts/video/FloatRect;->right:F

    aput v1, p2, v0

    .line 113
    const/4 v0, 0x3

    iget v1, p1, Lcom/google/android/libraries/hangouts/video/FloatRect;->bottom:F

    aput v1, p2, v0

    .line 114
    const/4 v0, 0x4

    iget v1, p1, Lcom/google/android/libraries/hangouts/video/FloatRect;->right:F

    aput v1, p2, v0

    .line 115
    const/4 v0, 0x5

    iget v1, p1, Lcom/google/android/libraries/hangouts/video/FloatRect;->top:F

    aput v1, p2, v0

    .line 116
    const/4 v0, 0x6

    iget v1, p1, Lcom/google/android/libraries/hangouts/video/FloatRect;->left:F

    aput v1, p2, v0

    .line 117
    const/4 v0, 0x7

    iget v1, p1, Lcom/google/android/libraries/hangouts/video/FloatRect;->top:F

    aput v1, p2, v0

    .line 118
    return-void
.end method

.method public drawFrame()V
    .locals 7

    .prologue
    const/16 v6, 0x2601

    const/16 v2, 0x1406

    const/4 v1, 0x2

    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 184
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->maybeInitGl()V

    .line 185
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->glProgram:I

    if-nez v0, :cond_0

    .line 186
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "VideoSourceRenderer not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 188
    :cond_0
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->maybeUpdateOutputRectangle()V

    .line 190
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->glProgram:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glUseProgram(I)V

    .line 191
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->surfaceInfo:Lcom/google/android/libraries/hangouts/video/SurfaceInfo;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/SurfaceInfo;->getWidth()I

    move-result v0

    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->surfaceInfo:Lcom/google/android/libraries/hangouts/video/SurfaceInfo;

    invoke-virtual {v4}, Lcom/google/android/libraries/hangouts/video/SurfaceInfo;->getHeight()I

    move-result v4

    invoke-static {v3, v3, v0, v4}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 192
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {v5, v5, v5, v0}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    .line 193
    const/16 v0, 0x4000

    invoke-static {v0}, Landroid/opengl/GLES20;->glClear(I)V

    .line 194
    const/16 v0, 0xbe2

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 195
    const/16 v0, 0xb71

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 198
    const v0, 0x84c0

    invoke-static {v0}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 199
    const/16 v0, 0xde1

    .line 200
    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->videoSource:Lcom/google/android/libraries/hangouts/video/VideoSource;

    invoke-virtual {v4}, Lcom/google/android/libraries/hangouts/video/VideoSource;->isExternalTexture()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 201
    const v0, 0x8d65

    .line 203
    :cond_1
    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->videoSource:Lcom/google/android/libraries/hangouts/video/VideoSource;

    invoke-virtual {v4}, Lcom/google/android/libraries/hangouts/video/VideoSource;->getTextureName()I

    move-result v4

    invoke-static {v0, v4}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 204
    iget v4, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->texHandle:I

    invoke-static {}, Lf;->A()I

    move-result v5

    invoke-static {v4, v5}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 205
    const/16 v4, 0x2801

    invoke-static {v0, v4, v6}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 206
    const/16 v4, 0x2800

    invoke-static {v0, v4, v6}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 209
    const/16 v4, 0x2802

    const v5, 0x812f

    invoke-static {v0, v4, v5}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 210
    const/16 v4, 0x2803

    const v5, 0x812f

    invoke-static {v0, v4, v5}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 213
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->texCoordHandle:I

    iget-object v5, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->textureVertices:Ljava/nio/FloatBuffer;

    move v4, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 214
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->texCoordHandle:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 217
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->triangleVertsHandle:I

    iget-object v5, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->quadVertices:Ljava/nio/FloatBuffer;

    move v4, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 218
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->triangleVertsHandle:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 219
    const-string v0, "setup"

    invoke-static {v0}, Lf;->s(Ljava/lang/String;)V

    .line 222
    const/4 v0, 0x6

    const/4 v1, 0x4

    invoke-static {v0, v3, v1}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    .line 223
    const-string v0, "glDrawArrays"

    invoke-static {v0}, Lf;->s(Ljava/lang/String;)V

    .line 224
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->glProgram:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glDeleteProgram(I)V

    .line 106
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSourceRenderer;->glProgram:I

    .line 107
    return-void
.end method
