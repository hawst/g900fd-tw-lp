.class final Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;
.super Lcom/google/android/libraries/hangouts/video/CameraManager;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x9
.end annotation


# static fields
.field private static final DEBUG:Z = true

.field private static final MAX_OOM_COUNT:I = 0x1

.field private static final NUM_CAMERA_BUFFERS:I = 0x4

.field private static volatile sInstance:Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;

.field private static final sInstanceLock:Ljava/lang/Object;


# instance fields
.field private mDeviceOrientation:I

.field private mPreviewCallback:Landroid/hardware/Camera$PreviewCallback;

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->sInstanceLock:Ljava/lang/Object;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CameraManager;-><init>()V

    .line 33
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->mDeviceOrientation:I

    return-void
.end method

.method private static getGingerbreadInstance()Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;
    .locals 2

    .prologue
    .line 40
    sget-object v0, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->sInstance:Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;

    if-nez v0, :cond_1

    .line 41
    sget-object v1, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->sInstanceLock:Ljava/lang/Object;

    monitor-enter v1

    .line 42
    :try_start_0
    sget-object v0, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->sInstance:Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;

    if-nez v0, :cond_0

    .line 43
    new-instance v0, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;

    invoke-direct {v0}, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;-><init>()V

    sput-object v0, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->sInstance:Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;

    .line 45
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 47
    :cond_1
    sget-object v0, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->sInstance:Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;

    return-object v0

    .line 45
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getInstance()Lcom/google/android/libraries/hangouts/video/CameraManager;
    .locals 1

    .prologue
    .line 36
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->getGingerbreadInstance()Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;

    move-result-object v0

    return-object v0
.end method

.method private setRotationParams()V
    .locals 10

    .prologue
    .line 115
    invoke-static {}, Lcwz;->j()V

    .line 116
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->mCameraLock:Ljava/lang/Object;

    monitor-enter v2

    .line 117
    :try_start_0
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->mDeviceOrientation:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 118
    invoke-static {}, Lcxc;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    const-string v0, "vclib"

    const-string v1, "setRotationParams bailing; no device orientation"

    invoke-static {v0, v1}, Lcxc;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    :cond_0
    monitor-exit v2

    .line 192
    :goto_0
    return-void

    .line 123
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->getCurrentCameraSettings()Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    move-result-object v3

    .line 124
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->getCurrentCamera()Landroid/hardware/Camera;

    move-result-object v4

    .line 125
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->getCameraState()I

    move-result v5

    .line 127
    iget v6, v3, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->orientation:I

    .line 143
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->mDeviceOrientation:I

    .line 146
    iget-boolean v0, v3, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->cameraIsFrontFacing:Z

    if-eqz v0, :cond_5

    .line 147
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->mDeviceOrientation:I

    add-int/2addr v0, v6

    rsub-int v0, v0, 0x168

    add-int/lit16 v0, v0, 0x168

    rem-int/lit16 v0, v0, 0x168

    .line 149
    iget v1, p0, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->mDeviceOrientation:I

    add-int/2addr v1, v6

    rem-int/lit16 v1, v1, 0x168

    .line 155
    :goto_1
    invoke-static {}, Lcxc;->c()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 156
    const-string v7, "vclib"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "setRotation dev "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v9, p0, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->mDeviceOrientation:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " cameraOrientation "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " displayOrientation "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " encode "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, " isfront "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v3, v3, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->cameraIsFrontFacing:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v7, v1}, Lcxc;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    :cond_2
    if-eqz v4, :cond_4

    .line 167
    and-int/lit8 v1, v5, 0x4

    if-eqz v1, :cond_3

    .line 168
    const-string v1, "vclib"

    const-string v3, "setRotationParams calling stopPreview"

    invoke-static {v1, v3}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    invoke-virtual {v4}, Landroid/hardware/Camera;->stopPreview()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173
    :cond_3
    :try_start_1
    invoke-virtual {v4, v0}, Landroid/hardware/Camera;->setDisplayOrientation(I)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 180
    :goto_2
    and-int/lit8 v0, v5, 0x4

    if-eqz v0, :cond_4

    .line 181
    :try_start_2
    const-string v0, "vclib"

    const-string v1, "setRotationParams calling startPreview"

    invoke-static {v0, v1}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 183
    :try_start_3
    invoke-virtual {v4}, Landroid/hardware/Camera;->startPreview()V

    .line 185
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->restorePreviewCallback()Z
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 192
    :cond_4
    :goto_3
    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 151
    :cond_5
    :try_start_5
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->mDeviceOrientation:I

    sub-int v0, v6, v0

    add-int/lit16 v0, v0, 0x168

    rem-int/lit16 v1, v0, 0x168

    .line 152
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->mDeviceOrientation:I

    sub-int v0, v6, v0

    add-int/lit16 v0, v0, 0x168

    rem-int/lit16 v0, v0, 0x168

    goto :goto_1

    .line 174
    :catch_0
    move-exception v1

    .line 175
    const-string v3, "vclib"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "### CameraManager: set camera display orientation to "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, " failed!"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v6, Ljava/lang/RuntimeException;

    invoke-direct {v6}, Ljava/lang/RuntimeException;-><init>()V

    invoke-static {v3, v0, v6}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 177
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->mCallback:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraManagerCallback;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraManagerCallback;->onCameraOpenError(Ljava/lang/Exception;)V

    goto :goto_2

    .line 186
    :catch_1
    move-exception v0

    .line 187
    const-string v1, "vclib"

    const-string v3, "exception"

    invoke-static {v1, v3, v0}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 188
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->mCallback:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraManagerCallback;

    invoke-interface {v1, v0}, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraManagerCallback;->onCameraOpenError(Ljava/lang/Exception;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_3
.end method


# virtual methods
.method public arePreview3ALocksSupportedLocked(Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;)Z
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    return v0
.end method

.method public endCallback()V
    .locals 3

    .prologue
    .line 208
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->mCameraLock:Ljava/lang/Object;

    monitor-enter v1

    .line 209
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->getCameraState()I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    .line 210
    const-string v0, "vclib"

    const-string v2, "endCallback"

    invoke-static {v0, v2}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->getCurrentCamera()Landroid/hardware/Camera;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/hardware/Camera;->setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V

    .line 212
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->mPreviewCallback:Landroid/hardware/Camera$PreviewCallback;

    .line 214
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public getCurrentPreviewSize()Lcom/google/android/libraries/hangouts/video/Size;
    .locals 2

    .prologue
    .line 228
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->mCameraLock:Ljava/lang/Object;

    monitor-enter v1

    .line 229
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->getCurrentCameraSettings()Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->previewSize:Lcom/google/android/libraries/hangouts/video/Size;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 230
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected initializeCameraLocked(Landroid/hardware/Camera$Parameters;Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->setRotationParams()V

    .line 54
    return-void
.end method

.method protected restorePreviewCallback()Z
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x5

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 236
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->getCurrentCamera()Landroid/hardware/Camera;

    move-result-object v7

    .line 237
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->getCameraState()I

    move-result v2

    .line 238
    and-int/lit8 v3, v2, 0x4

    if-nez v3, :cond_0

    .line 239
    const-string v1, "vclib"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "restorePreviewCallback called with state "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcxc;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    :goto_0
    return v0

    .line 242
    :cond_0
    invoke-virtual {v7, v12}, Landroid/hardware/Camera;->setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V

    .line 243
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->getCurrentCameraSettings()Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->previewSize:Lcom/google/android/libraries/hangouts/video/Size;

    .line 244
    iget v3, v2, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    iget v2, v2, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    mul-int/2addr v2, v3

    const/16 v3, 0x11

    .line 245
    invoke-static {v3}, Landroid/graphics/ImageFormat;->getBitsPerPixel(I)I

    move-result v3

    mul-int/2addr v2, v3

    div-int/lit8 v8, v2, 0x8

    move v6, v1

    move v2, v1

    .line 248
    :goto_1
    const/4 v3, 0x4

    if-ge v6, v3, :cond_3

    move v3, v1

    move v4, v1

    move v5, v2

    .line 251
    :goto_2
    if-nez v4, :cond_2

    .line 258
    :try_start_0
    new-array v2, v8, [B

    invoke-virtual {v7, v2}, Landroid/hardware/Camera;->addCallbackBuffer([B)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move v4, v0

    .line 270
    goto :goto_2

    .line 260
    :catch_0
    move-exception v2

    .line 261
    const-string v9, "vclib"

    const-string v10, "Out of memory creating camera buffers "

    invoke-static {v11, v9, v10, v2}, Lcxc;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 262
    add-int/lit8 v2, v3, 0x1

    .line 263
    if-gt v2, v0, :cond_1

    .line 264
    sget-object v3, Lcxe;->a:Lcxe;

    invoke-virtual {v3}, Lcxe;->a()V

    move v3, v2

    goto :goto_2

    .line 268
    :cond_1
    const-string v3, "vclib"

    const-string v4, "Camera buffer init failed. Out of Memory."

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v11, v3, v4, v5}, Lcxc;->a(ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v3, v2

    move v4, v0

    move v5, v0

    .line 270
    goto :goto_2

    .line 248
    :cond_2
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    move v2, v5

    goto :goto_1

    .line 274
    :cond_3
    if-eqz v2, :cond_4

    .line 275
    invoke-virtual {v7, v12}, Landroid/hardware/Camera;->setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V

    move v0, v1

    .line 276
    goto :goto_0

    .line 280
    :cond_4
    const-string v1, "vclib"

    const-string v2, "restorePreviewCallback calling setPreviewCallbackWithBuffer"

    invoke-static {v1, v2}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->mPreviewCallback:Landroid/hardware/Camera$PreviewCallback;

    invoke-virtual {v7, v1}, Landroid/hardware/Camera;->setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V

    goto :goto_0
.end method

.method public returnBuffer([B)V
    .locals 4

    .prologue
    .line 218
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->mCameraLock:Ljava/lang/Object;

    monitor-enter v1

    .line 219
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->getCameraState()I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    .line 220
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->getCurrentCamera()Landroid/hardware/Camera;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->addCallbackBuffer([B)V

    .line 224
    :goto_0
    monitor-exit v1

    return-void

    .line 222
    :cond_0
    const-string v0, "vclib"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "returnBuffer with state "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->getCameraState()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcxc;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 224
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public setCallback(Landroid/hardware/Camera$PreviewCallback;)V
    .locals 3

    .prologue
    .line 197
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->mCameraLock:Ljava/lang/Object;

    monitor-enter v1

    .line 199
    :try_start_0
    const-string v0, "vclib"

    const-string v2, "setCallback"

    invoke-static {v0, v2}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->mPreviewCallback:Landroid/hardware/Camera$PreviewCallback;

    .line 203
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->restorePreviewCallback()Z

    .line 204
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public setPreview3ALocksLocked(ZLandroid/hardware/Camera;Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;)V
    .locals 2

    .prologue
    .line 60
    const-string v0, "vclib"

    const-string v1, "Cannot set 3A locks in Gingerbread"

    invoke-static {v0, v1}, Lcxc;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    return-void
.end method

.method public setPreviewSurfaceHolder(Landroid/view/SurfaceHolder;)V
    .locals 3

    .prologue
    .line 90
    invoke-static {}, Lcwz;->a()V

    .line 91
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 92
    const-string v0, "vclib"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setPreviewSurfaceHolder "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcxc;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    if-eqz v0, :cond_0

    .line 94
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->onNewPreviewSurfaceSet()V

    .line 98
    :goto_0
    return-void

    .line 96
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->onPreviewSurfaceCleared()V

    goto :goto_0
.end method

.method public setRotation(I)V
    .locals 3

    .prologue
    .line 106
    const-string v0, "vclib"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setRotation "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcxc;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->mDeviceOrientation:I

    if-eq p1, v0, :cond_0

    .line 108
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->mDeviceOrientation:I

    .line 109
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->setRotationParams()V

    .line 111
    :cond_0
    return-void
.end method

.method protected setSurfaceOnCameraLocked(Landroid/hardware/Camera;)Z
    .locals 4

    .prologue
    .line 72
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {p1, v0}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 78
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 73
    :catch_0
    move-exception v0

    .line 74
    const-string v1, "vclib"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setPreviewDisplay() failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcxc;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->mCallback:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraManagerCallback;

    invoke-interface {v1, v0}, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraManagerCallback;->onCameraOpenError(Ljava/lang/Exception;)V

    .line 76
    const/4 v0, 0x0

    goto :goto_0
.end method
