.class public Lcom/google/android/libraries/hangouts/video/VideoViewRequest;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final frameRate:I

.field public final height:I

.field public final rendererId:I

.field public final rendererManagerNativeContext:I

.field public final ssrc:I

.field public final width:I


# direct methods
.method public constructor <init>(ILcom/google/android/libraries/hangouts/video/Renderer;III)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/VideoViewRequest;->ssrc:I

    .line 33
    iput p3, p0, Lcom/google/android/libraries/hangouts/video/VideoViewRequest;->width:I

    .line 34
    iput p4, p0, Lcom/google/android/libraries/hangouts/video/VideoViewRequest;->height:I

    .line 35
    iput p5, p0, Lcom/google/android/libraries/hangouts/video/VideoViewRequest;->frameRate:I

    .line 36
    iget-object v0, p2, Lcom/google/android/libraries/hangouts/video/Renderer;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/RendererManager;->getNativeContext()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/VideoViewRequest;->rendererManagerNativeContext:I

    .line 37
    iget v0, p2, Lcom/google/android/libraries/hangouts/video/Renderer;->mRendererID:I

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/VideoViewRequest;->rendererId:I

    .line 38
    return-void
.end method
