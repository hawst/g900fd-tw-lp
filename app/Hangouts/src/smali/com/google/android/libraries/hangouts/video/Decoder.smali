.class public final Lcom/google/android/libraries/hangouts/video/Decoder;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# static fields
.field private static final DECODER_THREAD_NAME:Ljava/lang/String; = "DecoderHandlerThread"

.field private static final MAX_FAILED_GETNEXTINPUTBUFFER_COUNT:I = 0x64

.field private static final MSG_DECODE:I = 0x0

.field private static final MSG_RESET:I = 0x1

.field private static final MSG_STOP:I = 0x2

.field private static final NO_WAIT_TIMEOUT:I = 0x0

.field private static final POLL_PERIOD_MS:I = 0xa

.field private static final TAG:Ljava/lang/String; = "vclib"

.field private static final TIMESTAMP_TO_PRESENTATION_TIME_MULTIPLIER:I = 0xb

.field private static final VP8_MIME_TYPE:Ljava/lang/String; = "video/x-vnd.on2.vp8"

.field private static final VP8_SOFTWARE_DECODER_NAME:Ljava/lang/String; = "OMX.google.vp8.decoder"

.field private static sHardwareDecoderCount:I

.field private static final sHardwareDecoderCountLock:Ljava/lang/Object;

.field private static sMaxHardwareDecoderCount:I

.field private static sSupportsDynamicResolutionChanges:Z


# instance fields
.field private mCachedResolutionChangeFrame:Lcom/google/android/libraries/hangouts/video/Decoder$CachedEncodedFrame;

.field private mCurrentInputHeight:I

.field private mCurrentInputWidth:I

.field private mCurrentOutputFormat:Landroid/media/MediaFormat;

.field private final mDecodeHandler:Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;

.field private final mDecoderManager:Lcom/google/android/libraries/hangouts/video/DecoderManager;

.field private final mDecoderThread:Landroid/os/HandlerThread;

.field private mExpectedOutputFrameCount:I

.field private mFailedGetNextInputBufferCount:I

.field private mGLTextureId:I

.field private mHasRenderedFrames:Z

.field private mInputBuffers:[Ljava/nio/ByteBuffer;

.field private mIsHardwareDecoder:Z

.field private final mLock:Ljava/lang/Object;

.field private mMediaCodec:Landroid/media/MediaCodec;

.field private mMediaCodecResetRequired:Z

.field private mNextInputBufferIndex:I

.field private mOldGLTextureId:I

.field private mOldSurface:Landroid/view/Surface;

.field private mOldSurfaceTexture:Landroid/graphics/SurfaceTexture;

.field private mRequireKeyframe:Z

.field private mSsrc:I

.field private mSurface:Landroid/view/Surface;

.field private mSurfaceTexture:Landroid/graphics/SurfaceTexture;

.field private mTexturesToDelete:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mUpdateGLTextureInfo:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 46
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/libraries/hangouts/video/Decoder;->sSupportsDynamicResolutionChanges:Z

    .line 56
    const v0, 0x7fffffff

    sput v0, Lcom/google/android/libraries/hangouts/video/Decoder;->sMaxHardwareDecoderCount:I

    .line 57
    const-string v0, "manta"

    sget-object v1, Landroid/os/Build;->HARDWARE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    const/4 v0, 0x3

    sput v0, Lcom/google/android/libraries/hangouts/video/Decoder;->sMaxHardwareDecoderCount:I

    .line 210
    :cond_0
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/libraries/hangouts/video/Decoder;->sHardwareDecoderCountLock:Ljava/lang/Object;

    return-void
.end method

.method constructor <init>(Lcom/google/android/libraries/hangouts/video/DecoderManager;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 300
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 301
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mDecoderManager:Lcom/google/android/libraries/hangouts/video/DecoderManager;

    .line 302
    iput v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mSsrc:I

    .line 303
    iput v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mNextInputBufferIndex:I

    .line 304
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mLock:Ljava/lang/Object;

    .line 306
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mRequireKeyframe:Z

    .line 309
    iput v1, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mGLTextureId:I

    .line 310
    iput v1, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mOldGLTextureId:I

    .line 311
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mTexturesToDelete:Ljava/util/List;

    .line 313
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "DecoderHandlerThread"

    const/4 v2, -0x4

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mDecoderThread:Landroid/os/HandlerThread;

    .line 314
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mDecoderThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 315
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mDecoderThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    .line 316
    if-nez v0, :cond_0

    .line 317
    const-string v1, "vclib"

    const-string v2, "Unable to create decoder thread/looper."

    invoke-static {v1, v2}, Lcxc;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    const-string v1, "Unable to create decoder thread/looper."

    invoke-static {v1}, Lcwz;->a(Ljava/lang/String;)V

    .line 321
    :cond_0
    new-instance v1, Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;

    invoke-direct {v1, p0, v0}, Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;-><init>(Lcom/google/android/libraries/hangouts/video/Decoder;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mDecodeHandler:Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;

    .line 324
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/libraries/hangouts/video/Decoder;Z)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/Decoder;->cleanup(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/libraries/hangouts/video/Decoder;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/libraries/hangouts/video/Decoder;)Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mUpdateGLTextureInfo:Z

    return v0
.end method

.method static synthetic access$202(Lcom/google/android/libraries/hangouts/video/Decoder;Z)Z
    .locals 0

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mUpdateGLTextureInfo:Z

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/libraries/hangouts/video/Decoder;)I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mOldGLTextureId:I

    return v0
.end method

.method static synthetic access$302(Lcom/google/android/libraries/hangouts/video/Decoder;I)I
    .locals 0

    .prologue
    .line 41
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mOldGLTextureId:I

    return p1
.end method

.method static synthetic access$400(Lcom/google/android/libraries/hangouts/video/Decoder;)Ljava/util/List;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mTexturesToDelete:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/libraries/hangouts/video/Decoder;)Z
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/Decoder;->initializeMediaCodec()Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/libraries/hangouts/video/Decoder;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/Decoder;->processInput()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/libraries/hangouts/video/Decoder;)Z
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/Decoder;->maybeRenderFrame()Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/libraries/hangouts/video/Decoder;)Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mMediaCodecResetRequired:Z

    return v0
.end method

.method private cleanup(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 694
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/Decoder;->usedInputBuffer()V

    .line 695
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 696
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mHasRenderedFrames:Z

    .line 697
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 698
    iput v2, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mFailedGetNextInputBufferCount:I

    .line 699
    iput-boolean v2, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mMediaCodecResetRequired:Z

    .line 700
    iput v2, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mCurrentInputWidth:I

    .line 701
    iput v2, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mCurrentInputHeight:I

    .line 702
    iput-object v3, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mCurrentOutputFormat:Landroid/media/MediaFormat;

    .line 703
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/Decoder;->releaseMediaCodec()V

    .line 704
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mOldSurfaceTexture:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_0

    .line 705
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mOldSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->release()V

    .line 706
    iput-object v3, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mOldSurfaceTexture:Landroid/graphics/SurfaceTexture;

    .line 708
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mOldSurface:Landroid/view/Surface;

    if-eqz v0, :cond_1

    .line 709
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mOldSurface:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 710
    iput-object v3, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mOldSurface:Landroid/view/Surface;

    .line 712
    :cond_1
    if-nez p1, :cond_3

    .line 713
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_2

    .line 714
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->release()V

    .line 715
    iput-object v3, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    .line 717
    :cond_2
    iput v2, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mGLTextureId:I

    .line 719
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mRequireKeyframe:Z

    .line 720
    iput-object v3, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mInputBuffers:[Ljava/nio/ByteBuffer;

    .line 721
    return-void

    .line 697
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private createMediaCodec()V
    .locals 4

    .prologue
    .line 347
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mIsHardwareDecoder:Z

    .line 348
    sget-object v1, Lcom/google/android/libraries/hangouts/video/Decoder;->sHardwareDecoderCountLock:Ljava/lang/Object;

    monitor-enter v1

    .line 349
    :try_start_0
    sget v0, Lcom/google/android/libraries/hangouts/video/Decoder;->sHardwareDecoderCount:I

    sget v2, Lcom/google/android/libraries/hangouts/video/Decoder;->sMaxHardwareDecoderCount:I

    if-lt v0, v2, :cond_0

    .line 350
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mIsHardwareDecoder:Z

    .line 354
    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 357
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mIsHardwareDecoder:Z

    if-eqz v0, :cond_1

    .line 359
    :try_start_1
    const-string v0, "video/x-vnd.on2.vp8"

    invoke-static {v0}, Landroid/media/MediaCodec;->createDecoderByType(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mMediaCodec:Landroid/media/MediaCodec;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 375
    :goto_1
    return-void

    .line 352
    :cond_0
    :try_start_2
    sget v0, Lcom/google/android/libraries/hangouts/video/Decoder;->sHardwareDecoderCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/google/android/libraries/hangouts/video/Decoder;->sHardwareDecoderCount:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 354
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 360
    :catch_0
    move-exception v0

    .line 364
    const-string v1, "vclib"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MediaCodec.createDecoderByType failed, "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcxc;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 368
    :cond_1
    :try_start_3
    const-string v0, "vclib"

    const-string v1, "Creating a software vp8 decoder."

    invoke-static {v0, v1}, Lcxc;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    const-string v0, "OMX.google.vp8.decoder"

    invoke-static {v0}, Landroid/media/MediaCodec;->createByCodecName(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mMediaCodec:Landroid/media/MediaCodec;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 370
    :catch_1
    move-exception v0

    .line 372
    const-string v1, "vclib"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MediaCodec.createByCodecName failed, "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcxc;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private getNextInputBufferIndex()I
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 510
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mNextInputBufferIndex:I

    if-ne v0, v3, :cond_0

    .line 511
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mMediaCodec:Landroid/media/MediaCodec;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaCodec;->dequeueInputBuffer(J)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mNextInputBufferIndex:I

    .line 513
    :cond_0
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mNextInputBufferIndex:I

    if-ne v0, v3, :cond_1

    .line 514
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mFailedGetNextInputBufferCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mFailedGetNextInputBufferCount:I

    .line 518
    :goto_0
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mNextInputBufferIndex:I

    return v0

    .line 516
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mFailedGetNextInputBufferCount:I

    goto :goto_0
.end method

.method private initializeMediaCodec()Z
    .locals 6

    .prologue
    const/16 v3, 0x780

    const/16 v2, 0x280

    const/4 v0, 0x0

    .line 385
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/Decoder;->createMediaCodec()V

    .line 386
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mMediaCodec:Landroid/media/MediaCodec;

    if-nez v1, :cond_0

    .line 387
    const-string v1, "vclib"

    const-string v2, "Unable to create a decoder for VP8."

    invoke-static {v1, v2}, Lcxc;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    :goto_0
    return v0

    .line 393
    :cond_0
    const-string v1, "video/x-vnd.on2.vp8"

    invoke-static {v1, v2, v2}, Landroid/media/MediaFormat;->createVideoFormat(Ljava/lang/String;II)Landroid/media/MediaFormat;

    move-result-object v1

    .line 396
    const-string v2, "max-width"

    invoke-virtual {v1, v2, v3}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 397
    const-string v2, "max-height"

    invoke-virtual {v1, v2, v3}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 399
    :try_start_0
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mMediaCodec:Landroid/media/MediaCodec;

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mSurface:Landroid/view/Surface;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v1, v3, v4, v5}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    .line 400
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mMediaCodec:Landroid/media/MediaCodec;

    invoke-virtual {v1}, Landroid/media/MediaCodec;->start()V

    .line 402
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mMediaCodec:Landroid/media/MediaCodec;

    invoke-virtual {v1}, Landroid/media/MediaCodec;->getInputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mInputBuffers:[Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 411
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mInputBuffers:[Ljava/nio/ByteBuffer;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->isDirect()Z

    move-result v1

    if-nez v1, :cond_1

    .line 412
    const-string v1, "All MediaCodec objects should use direct input buffers."

    invoke-static {v1}, Lcwz;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 403
    :catch_0
    move-exception v1

    .line 407
    const-string v2, "vclib"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Initialization failed with exception: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcxc;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    const-string v2, "vclib"

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcxc;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 415
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private maybeRenderFrame()Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 628
    invoke-static {}, Lcwz;->h()V

    .line 629
    iget-boolean v2, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mMediaCodecResetRequired:Z

    if-eqz v2, :cond_1

    .line 668
    :cond_0
    :goto_0
    return v0

    .line 632
    :cond_1
    iget v2, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mExpectedOutputFrameCount:I

    if-eqz v2, :cond_0

    .line 636
    new-instance v2, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct {v2}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    .line 637
    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mMediaCodec:Landroid/media/MediaCodec;

    const-wide/16 v4, 0x0

    invoke-virtual {v3, v2, v4, v5}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v3

    .line 639
    if-ltz v3, :cond_2

    .line 640
    iget-wide v4, v2, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    const-wide/16 v6, 0xb

    div-long/2addr v4, v6

    .line 644
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mMediaCodec:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputFormat()Landroid/media/MediaFormat;

    move-result-object v0

    .line 645
    const-string v2, "width"

    invoke-virtual {v0, v2}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v2

    .line 646
    const-string v6, "height"

    invoke-virtual {v0, v6}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    .line 649
    iget-object v6, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mMediaCodec:Landroid/media/MediaCodec;

    invoke-virtual {v6, v3, v1}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 650
    iget v3, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mExpectedOutputFrameCount:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mExpectedOutputFrameCount:I

    .line 651
    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mDecoderManager:Lcom/google/android/libraries/hangouts/video/DecoderManager;

    iget v3, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mSsrc:I

    invoke-static {v3, v4, v5, v2, v0}, Lcom/google/android/libraries/hangouts/video/DecoderManager;->frameRenderedExternally(IJII)Z

    .line 652
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 653
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mHasRenderedFrames:Z

    .line 654
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v1

    .line 655
    goto :goto_0

    .line 654
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 656
    :cond_2
    const/4 v2, -0x2

    if-ne v3, v2, :cond_0

    .line 657
    const-string v2, "vclib"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "decoder (ssrc="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mSsrc:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") resolution changed. New format: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mMediaCodec:Landroid/media/MediaCodec;

    .line 658
    invoke-virtual {v4}, Landroid/media/MediaCodec;->getOutputFormat()Landroid/media/MediaFormat;

    move-result-object v4

    invoke-virtual {v4}, Landroid/media/MediaFormat;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 657
    invoke-static {v2, v3}, Lcxc;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 659
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mCurrentOutputFormat:Landroid/media/MediaFormat;

    if-eqz v2, :cond_3

    sget-boolean v2, Lcom/google/android/libraries/hangouts/video/Decoder;->sSupportsDynamicResolutionChanges:Z

    if-nez v2, :cond_3

    .line 660
    const-string v2, "vclib"

    const-string v3, "Missed a dynamic resolution change when handling incoming frames. Resetting hw decoder now."

    invoke-static {v2, v3}, Lcxc;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    iput-boolean v1, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mMediaCodecResetRequired:Z

    goto/16 :goto_0

    .line 665
    :cond_3
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 666
    :try_start_1
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mMediaCodec:Landroid/media/MediaCodec;

    invoke-virtual {v2}, Landroid/media/MediaCodec;->getOutputFormat()Landroid/media/MediaFormat;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mCurrentOutputFormat:Landroid/media/MediaFormat;

    .line 667
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private processInput()V
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 554
    invoke-static {}, Lcwz;->h()V

    .line 555
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/Decoder;->getNextInputBufferIndex()I

    move-result v1

    .line 556
    const/4 v0, -0x1

    if-ne v1, v0, :cond_1

    .line 559
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mFailedGetNextInputBufferCount:I

    const/16 v1, 0x64

    if-lt v0, v1, :cond_0

    .line 560
    iput-boolean v7, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mMediaCodecResetRequired:Z

    .line 620
    :cond_0
    :goto_0
    return-void

    .line 565
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mInputBuffers:[Ljava/nio/ByteBuffer;

    aget-object v2, v0, v1

    .line 567
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mCachedResolutionChangeFrame:Lcom/google/android/libraries/hangouts/video/Decoder$CachedEncodedFrame;

    if-eqz v0, :cond_4

    .line 569
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mRequireKeyframe:Z

    invoke-static {v0}, Lcwz;->a(Z)V

    .line 570
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget v3, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mCurrentInputWidth:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v3}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 571
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget v3, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mCurrentInputHeight:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v3}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 572
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mCachedResolutionChangeFrame:Lcom/google/android/libraries/hangouts/video/Decoder$CachedEncodedFrame;

    iget-object v0, v0, Lcom/google/android/libraries/hangouts/video/Decoder$CachedEncodedFrame;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 573
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mCachedResolutionChangeFrame:Lcom/google/android/libraries/hangouts/video/Decoder$CachedEncodedFrame;

    iget-object v0, v0, Lcom/google/android/libraries/hangouts/video/Decoder$CachedEncodedFrame;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 574
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mCachedResolutionChangeFrame:Lcom/google/android/libraries/hangouts/video/Decoder$CachedEncodedFrame;

    iget-object v0, v0, Lcom/google/android/libraries/hangouts/video/Decoder$CachedEncodedFrame;->outParams:Lcom/google/android/libraries/hangouts/video/Decoder$FrameDataOutputParams;

    .line 575
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mCachedResolutionChangeFrame:Lcom/google/android/libraries/hangouts/video/Decoder$CachedEncodedFrame;

    move-object v3, v0

    .line 604
    :goto_1
    iput-boolean v6, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mRequireKeyframe:Z

    .line 606
    iget v0, v3, Lcom/google/android/libraries/hangouts/video/Decoder$FrameDataOutputParams;->width:I

    if-eqz v0, :cond_2

    iget v0, v3, Lcom/google/android/libraries/hangouts/video/Decoder$FrameDataOutputParams;->height:I

    if-eqz v0, :cond_2

    .line 607
    iget v0, v3, Lcom/google/android/libraries/hangouts/video/Decoder$FrameDataOutputParams;->width:I

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mCurrentInputWidth:I

    .line 608
    iget v0, v3, Lcom/google/android/libraries/hangouts/video/Decoder$FrameDataOutputParams;->height:I

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mCurrentInputHeight:I

    .line 612
    :cond_2
    iget-boolean v0, v3, Lcom/google/android/libraries/hangouts/video/Decoder$FrameDataOutputParams;->isEndOfStream:Z

    if-eqz v0, :cond_3

    .line 613
    const/4 v6, 0x4

    .line 615
    :cond_3
    iget-wide v4, v3, Lcom/google/android/libraries/hangouts/video/Decoder$FrameDataOutputParams;->timeStamp:J

    const-wide/16 v7, 0xb

    mul-long/2addr v4, v7

    .line 616
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mMediaCodec:Landroid/media/MediaCodec;

    iget v2, v3, Lcom/google/android/libraries/hangouts/video/Decoder$FrameDataOutputParams;->offset:I

    iget v3, v3, Lcom/google/android/libraries/hangouts/video/Decoder$FrameDataOutputParams;->size:I

    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V

    .line 618
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mExpectedOutputFrameCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mExpectedOutputFrameCount:I

    .line 619
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/Decoder;->usedInputBuffer()V

    goto :goto_0

    .line 578
    :cond_4
    new-instance v0, Lcom/google/android/libraries/hangouts/video/Decoder$FrameDataOutputParams;

    invoke-direct {v0}, Lcom/google/android/libraries/hangouts/video/Decoder$FrameDataOutputParams;-><init>()V

    .line 579
    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mDecoderManager:Lcom/google/android/libraries/hangouts/video/DecoderManager;

    iget v3, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mSsrc:I

    iget-boolean v4, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mRequireKeyframe:Z

    invoke-static {v3, v4, v2, v0}, Lcom/google/android/libraries/hangouts/video/DecoderManager;->getNextEncodedFrame(IZLjava/nio/ByteBuffer;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 584
    iget v3, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mCurrentInputWidth:I

    if-eqz v3, :cond_6

    iget v3, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mCurrentInputHeight:I

    if-eqz v3, :cond_6

    iget v3, v0, Lcom/google/android/libraries/hangouts/video/Decoder$FrameDataOutputParams;->width:I

    if-eqz v3, :cond_6

    iget v3, v0, Lcom/google/android/libraries/hangouts/video/Decoder$FrameDataOutputParams;->height:I

    if-eqz v3, :cond_6

    iget v3, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mCurrentInputWidth:I

    iget v4, v0, Lcom/google/android/libraries/hangouts/video/Decoder$FrameDataOutputParams;->width:I

    if-ne v3, v4, :cond_5

    iget v3, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mCurrentInputHeight:I

    iget v4, v0, Lcom/google/android/libraries/hangouts/video/Decoder$FrameDataOutputParams;->height:I

    if-eq v3, v4, :cond_6

    .line 587
    :cond_5
    const-string v3, "vclib"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Dynamic resolution change detected ("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mCurrentInputWidth:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mCurrentInputHeight:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " -> "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v0, Lcom/google/android/libraries/hangouts/video/Decoder$FrameDataOutputParams;->width:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v0, Lcom/google/android/libraries/hangouts/video/Decoder$FrameDataOutputParams;->height:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcxc;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 590
    sget-boolean v3, Lcom/google/android/libraries/hangouts/video/Decoder;->sSupportsDynamicResolutionChanges:Z

    if-nez v3, :cond_6

    .line 591
    const-string v1, "vclib"

    const-string v3, "This decoder doesn\'t support dynamic changes. Restarting."

    invoke-static {v1, v3}, Lcxc;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    new-instance v1, Lcom/google/android/libraries/hangouts/video/Decoder$CachedEncodedFrame;

    invoke-direct {v1, p0}, Lcom/google/android/libraries/hangouts/video/Decoder$CachedEncodedFrame;-><init>(Lcom/google/android/libraries/hangouts/video/Decoder;)V

    iput-object v1, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mCachedResolutionChangeFrame:Lcom/google/android/libraries/hangouts/video/Decoder$CachedEncodedFrame;

    .line 593
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mCachedResolutionChangeFrame:Lcom/google/android/libraries/hangouts/video/Decoder$CachedEncodedFrame;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    iput-object v3, v1, Lcom/google/android/libraries/hangouts/video/Decoder$CachedEncodedFrame;->buffer:Ljava/nio/ByteBuffer;

    .line 594
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 595
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mCachedResolutionChangeFrame:Lcom/google/android/libraries/hangouts/video/Decoder$CachedEncodedFrame;

    iget-object v1, v1, Lcom/google/android/libraries/hangouts/video/Decoder$CachedEncodedFrame;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 596
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mCachedResolutionChangeFrame:Lcom/google/android/libraries/hangouts/video/Decoder$CachedEncodedFrame;

    iput-object v0, v1, Lcom/google/android/libraries/hangouts/video/Decoder$CachedEncodedFrame;->outParams:Lcom/google/android/libraries/hangouts/video/Decoder$FrameDataOutputParams;

    .line 597
    iput-boolean v7, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mMediaCodecResetRequired:Z

    goto/16 :goto_0

    :cond_6
    move-object v3, v0

    goto/16 :goto_1
.end method

.method private releaseMediaCodec()V
    .locals 2

    .prologue
    .line 330
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mMediaCodec:Landroid/media/MediaCodec;

    if-eqz v0, :cond_0

    .line 331
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mMediaCodec:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->stop()V

    .line 332
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mMediaCodec:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->release()V

    .line 333
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mMediaCodec:Landroid/media/MediaCodec;

    .line 334
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mIsHardwareDecoder:Z

    if-eqz v0, :cond_0

    .line 335
    sget-object v1, Lcom/google/android/libraries/hangouts/video/Decoder;->sHardwareDecoderCountLock:Ljava/lang/Object;

    monitor-enter v1

    .line 336
    :try_start_0
    sget v0, Lcom/google/android/libraries/hangouts/video/Decoder;->sHardwareDecoderCount:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/google/android/libraries/hangouts/video/Decoder;->sHardwareDecoderCount:I

    .line 337
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 340
    :cond_0
    return-void

    .line 337
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private usedInputBuffer()V
    .locals 1

    .prologue
    .line 525
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mNextInputBufferIndex:I

    .line 526
    return-void
.end method


# virtual methods
.method public drawTexture()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 474
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 476
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mTexturesToDelete:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 477
    invoke-static {v0}, Lf;->i(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 485
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 479
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mTexturesToDelete:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 483
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mHasRenderedFrames:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mUpdateGLTextureInfo:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 484
    :goto_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mHasRenderedFrames:Z

    .line 485
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 486
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    if-eqz v1, :cond_1

    .line 487
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v1}, Landroid/graphics/SurfaceTexture;->updateTexImage()V

    .line 489
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 483
    goto :goto_1
.end method

.method public getCurrentOutputFormat()Landroid/media/MediaFormat;
    .locals 2

    .prologue
    .line 498
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 499
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mCurrentOutputFormat:Landroid/media/MediaFormat;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 500
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public getManager()Lcom/google/android/libraries/hangouts/video/DecoderManager;
    .locals 1

    .prologue
    .line 463
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mDecoderManager:Lcom/google/android/libraries/hangouts/video/DecoderManager;

    return-object v0
.end method

.method public getOutputTextureName()I
    .locals 2

    .prologue
    .line 452
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 453
    :try_start_0
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mGLTextureId:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 454
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public initializeGLContext()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 423
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 426
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mUpdateGLTextureInfo:Z

    .line 427
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mOldSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-static {v0}, Lcwz;->a(Ljava/lang/Object;)V

    .line 428
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mOldGLTextureId:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v2}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 433
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mGLTextureId:I

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mOldGLTextureId:I

    .line 434
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mOldSurfaceTexture:Landroid/graphics/SurfaceTexture;

    .line 435
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mSurface:Landroid/view/Surface;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mOldSurface:Landroid/view/Surface;

    .line 437
    invoke-static {}, Lf;->B()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mGLTextureId:I

    .line 438
    new-instance v0, Landroid/graphics/SurfaceTexture;

    iget v2, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mGLTextureId:I

    invoke-direct {v0, v2}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    .line 439
    new-instance v0, Landroid/view/Surface;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-direct {v0, v2}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mSurface:Landroid/view/Surface;

    .line 440
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 441
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mDecodeHandler:Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mDecodeHandler:Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;

    invoke-virtual {v1, v3}, Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;->sendMessage(Landroid/os/Message;)Z

    .line 442
    return-void

    .line 440
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public release()V
    .locals 3

    .prologue
    .line 679
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mDecodeHandler:Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mDecodeHandler:Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/Decoder$DecodeHandler;->sendMessage(Landroid/os/Message;)Z

    .line 681
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mDecoderThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quitSafely()V

    .line 683
    return-void
.end method

.method public setSourceId(I)V
    .locals 2

    .prologue
    .line 536
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mSsrc:I

    if-ne p1, v0, :cond_0

    .line 547
    :goto_0
    return-void

    .line 539
    :cond_0
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mSsrc:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mSsrc:I

    if-eq v0, p1, :cond_1

    .line 540
    const-string v0, "vclib"

    const-string v1, "Can\'t modify the source of a decoder!"

    invoke-static {v0, v1}, Lcxc;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 546
    :cond_1
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/Decoder;->mSsrc:I

    goto :goto_0
.end method
