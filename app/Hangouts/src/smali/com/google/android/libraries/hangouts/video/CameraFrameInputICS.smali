.class final Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# instance fields
.field private final mCamera:Lcom/google/android/libraries/hangouts/video/CameraManagerICS;

.field private final mCameraInputData:Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS$CameraInputData;

.field private mCameraOutputTextureName:I

.field private mCameraRendererID:I

.field private final mCameraStopWatch:Lcxb;

.field private mCameraSurfaceTexture:Landroid/graphics/SurfaceTexture;

.field private final mCameraTransformMatrix:[F

.field private mLastCameraFrameTimeNs:J

.field private final mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

.field private mRotationTransform:[F

.field private volatile mSelfFrameReady:Z

.field private final mSelfFrameReadyLock:Ljava/lang/Object;

.field private final mSelfViewTransformMatrix:[F


# direct methods
.method constructor <init>(Lcom/google/android/libraries/hangouts/video/RendererManager;Lcom/google/android/libraries/hangouts/video/CameraManagerICS;)V
    .locals 3

    .prologue
    const/16 v2, 0x10

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mCameraRendererID:I

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mSelfFrameReady:Z

    .line 28
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mSelfFrameReadyLock:Ljava/lang/Object;

    .line 29
    new-instance v0, Lcxb;

    const-string v1, "SelfVideo.camera"

    invoke-direct {v0, v1}, Lcxb;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mCameraStopWatch:Lcxb;

    .line 31
    new-instance v0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS$CameraInputData;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS$CameraInputData;-><init>(Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS$1;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mCameraInputData:Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS$CameraInputData;

    .line 32
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mCameraTransformMatrix:[F

    .line 33
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mSelfViewTransformMatrix:[F

    .line 44
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    .line 45
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/RendererManager;->instantiateRenderer(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mCameraRendererID:I

    .line 48
    iput-object p2, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mCamera:Lcom/google/android/libraries/hangouts/video/CameraManagerICS;

    .line 49
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mSelfFrameReadyLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;Z)Z
    .locals 0

    .prologue
    .line 23
    iput-boolean p1, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mSelfFrameReady:Z

    return p1
.end method


# virtual methods
.method getCamera()Lcom/google/android/libraries/hangouts/video/CameraManager;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mCamera:Lcom/google/android/libraries/hangouts/video/CameraManagerICS;

    return-object v0
.end method

.method getMostRecentCameraFrameTimeNs()J
    .locals 2

    .prologue
    .line 127
    iget-wide v0, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mLastCameraFrameTimeNs:J

    return-wide v0
.end method

.method getOutputTextureName()I
    .locals 1

    .prologue
    .line 155
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mCameraOutputTextureName:I

    return v0
.end method

.method initializeGLContext()V
    .locals 4

    .prologue
    .line 52
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mSelfFrameReadyLock:Ljava/lang/Object;

    monitor-enter v1

    .line 53
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mSelfFrameReady:Z

    .line 54
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 55
    invoke-static {}, Lf;->B()I

    move-result v0

    .line 56
    new-instance v1, Landroid/graphics/SurfaceTexture;

    invoke-direct {v1, v0}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mCameraSurfaceTexture:Landroid/graphics/SurfaceTexture;

    .line 57
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mCameraSurfaceTexture:Landroid/graphics/SurfaceTexture;

    new-instance v2, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS$1;

    invoke-direct {v2, p0}, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS$1;-><init>(Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;)V

    invoke-virtual {v1, v2}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    .line 69
    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mLastCameraFrameTimeNs:J

    .line 71
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mCamera:Lcom/google/android/libraries/hangouts/video/CameraManagerICS;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mCameraSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/hangouts/video/CameraManagerICS;->setPreviewSurfaceTexture(Landroid/graphics/SurfaceTexture;)V

    .line 73
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v2, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mCameraRendererID:I

    const-string v3, "sub_intex"

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/libraries/hangouts/video/RendererManager;->setIntParam(ILjava/lang/String;I)V

    .line 76
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mCameraRendererID:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/RendererManager;->initializeGLContext(I)Z

    .line 78
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mCameraRendererID:I

    const-string v2, "sub_outtex"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/hangouts/video/RendererManager;->getIntParam(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mCameraOutputTextureName:I

    .line 80
    return-void

    .line 54
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method release()V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mCamera:Lcom/google/android/libraries/hangouts/video/CameraManagerICS;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/CameraManagerICS;->setPreviewSurfaceTexture(Landroid/graphics/SurfaceTexture;)V

    .line 84
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mCameraRendererID:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/RendererManager;->releaseRenderer(I)V

    .line 85
    return-void
.end method

.method render()Z
    .locals 5

    .prologue
    .line 89
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mSelfFrameReadyLock:Ljava/lang/Object;

    monitor-enter v1

    .line 90
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mSelfFrameReady:Z

    .line 91
    if-eqz v0, :cond_0

    .line 92
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mSelfFrameReady:Z

    .line 94
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    if-eqz v0, :cond_1

    .line 96
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mCameraStopWatch:Lcxb;

    .line 97
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mCameraSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v1}, Landroid/graphics/SurfaceTexture;->updateTexImage()V

    .line 98
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mCameraSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v1}, Landroid/graphics/SurfaceTexture;->getTimestamp()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mLastCameraFrameTimeNs:J

    .line 99
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mCameraSurfaceTexture:Landroid/graphics/SurfaceTexture;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mCameraTransformMatrix:[F

    invoke-virtual {v1, v2}, Landroid/graphics/SurfaceTexture;->getTransformMatrix([F)V

    .line 100
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mCameraTransformMatrix:[F

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mRotationTransform:[F

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mSelfViewTransformMatrix:[F

    invoke-static {v1, v2, v3}, Lcxd;->a([F[F[F)V

    .line 102
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mCameraInputData:Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS$CameraInputData;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mSelfViewTransformMatrix:[F

    iput-object v2, v1, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS$CameraInputData;->selfFrameTransform:[F

    .line 104
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v2, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mCameraRendererID:I

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mCameraInputData:Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS$CameraInputData;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/libraries/hangouts/video/RendererManager;->renderFrame(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 105
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mCameraStopWatch:Lcxb;

    .line 108
    :cond_1
    return v0

    .line 94
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method setFlipNeeded(Z)V
    .locals 4

    .prologue
    .line 116
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v2, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mCameraRendererID:I

    const-string v3, "sub_flipinput"

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/libraries/hangouts/video/RendererManager;->setIntParam(ILjava/lang/String;I)V

    .line 118
    return-void

    .line 116
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setOutputSize(Lcom/google/android/libraries/hangouts/video/Size;)V
    .locals 4

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mCameraRendererID:I

    const-string v2, "sub_outdims"

    .line 123
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/Size;->getEncodedDimensions()I

    move-result v3

    .line 121
    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/RendererManager;->setIntParam(ILjava/lang/String;I)V

    .line 124
    return-void
.end method

.method setRotation(I)V
    .locals 1

    .prologue
    .line 135
    sparse-switch p1, :sswitch_data_0

    .line 146
    invoke-static {}, Lcxd;->a()[F

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mRotationTransform:[F

    .line 149
    :goto_0
    return-void

    .line 137
    :sswitch_0
    invoke-static {}, Lcxd;->b()[F

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mRotationTransform:[F

    goto :goto_0

    .line 140
    :sswitch_1
    invoke-static {}, Lcxd;->c()[F

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mRotationTransform:[F

    goto :goto_0

    .line 143
    :sswitch_2
    invoke-static {}, Lcxd;->d()[F

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraFrameInputICS;->mRotationTransform:[F

    goto :goto_0

    .line 135
    nop

    :sswitch_data_0
    .sparse-switch
        0x5a -> :sswitch_0
        0xb4 -> :sswitch_1
        0x10e -> :sswitch_2
    .end sparse-switch
.end method
