.class Lcom/google/android/libraries/hangouts/video/SelfViewGB$1;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# instance fields
.field final synthetic this$0:Lcom/google/android/libraries/hangouts/video/SelfViewGB;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/hangouts/video/SelfViewGB;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/SelfViewGB$1;->this$0:Lcom/google/android/libraries/hangouts/video/SelfViewGB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 2

    .prologue
    .line 82
    const-string v0, "vclib"

    const-string v1, "surfaceChanged"

    invoke-static {v0, v1}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfViewGB$1;->this$0:Lcom/google/android/libraries/hangouts/video/SelfViewGB;

    # getter for: Lcom/google/android/libraries/hangouts/video/SelfViewGB;->mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/SelfViewGB;->access$000(Lcom/google/android/libraries/hangouts/video/SelfViewGB;)Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/SelfViewGB$1;->this$0:Lcom/google/android/libraries/hangouts/video/SelfViewGB;

    # getter for: Lcom/google/android/libraries/hangouts/video/SelfViewGB;->mDeviceOrientation:I
    invoke-static {v1}, Lcom/google/android/libraries/hangouts/video/SelfViewGB;->access$100(Lcom/google/android/libraries/hangouts/video/SelfViewGB;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->setRotation(I)V

    .line 84
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2

    .prologue
    .line 75
    const-string v0, "vclib"

    const-string v1, "surfaceCreated"

    invoke-static {v0, v1}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfViewGB$1;->this$0:Lcom/google/android/libraries/hangouts/video/SelfViewGB;

    # getter for: Lcom/google/android/libraries/hangouts/video/SelfViewGB;->mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/SelfViewGB;->access$000(Lcom/google/android/libraries/hangouts/video/SelfViewGB;)Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->setPreviewSurfaceHolder(Landroid/view/SurfaceHolder;)V

    .line 77
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfViewGB$1;->this$0:Lcom/google/android/libraries/hangouts/video/SelfViewGB;

    # getter for: Lcom/google/android/libraries/hangouts/video/SelfViewGB;->mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/SelfViewGB;->access$000(Lcom/google/android/libraries/hangouts/video/SelfViewGB;)Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/SelfViewGB$1;->this$0:Lcom/google/android/libraries/hangouts/video/SelfViewGB;

    # getter for: Lcom/google/android/libraries/hangouts/video/SelfViewGB;->mDeviceOrientation:I
    invoke-static {v1}, Lcom/google/android/libraries/hangouts/video/SelfViewGB;->access$100(Lcom/google/android/libraries/hangouts/video/SelfViewGB;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->setRotation(I)V

    .line 78
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2

    .prologue
    .line 69
    const-string v0, "vclib"

    const-string v1, "surfaceDestroyed"

    invoke-static {v0, v1}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfViewGB$1;->this$0:Lcom/google/android/libraries/hangouts/video/SelfViewGB;

    # getter for: Lcom/google/android/libraries/hangouts/video/SelfViewGB;->mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/SelfViewGB;->access$000(Lcom/google/android/libraries/hangouts/video/SelfViewGB;)Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->setPreviewSurfaceHolder(Landroid/view/SurfaceHolder;)V

    .line 71
    return-void
.end method
