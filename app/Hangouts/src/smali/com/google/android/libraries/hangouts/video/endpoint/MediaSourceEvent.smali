.class public Lcom/google/android/libraries/hangouts/video/endpoint/MediaSourceEvent;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/hangouts/video/endpoint/EndpointEvent;


# static fields
.field public static final AUDIO_SOURCE_ADDED:I = 0x0

.field public static final AUDIO_SOURCE_REMOVED:I = 0x1

.field public static final VIDEO_SOURCE_ADDED:I = 0x2

.field public static final VIDEO_SOURCE_REMOVED:I = 0x3


# instance fields
.field public final ssrc:I

.field public final type:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/endpoint/MediaSourceEvent;->type:I

    .line 19
    iput p2, p0, Lcom/google/android/libraries/hangouts/video/endpoint/MediaSourceEvent;->ssrc:I

    .line 20
    return-void
.end method
