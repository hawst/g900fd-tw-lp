.class Lcom/google/android/libraries/hangouts/video/VideoChat$1;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic this$0:Lcom/google/android/libraries/hangouts/video/VideoChat;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/hangouts/video/VideoChat;)V
    .locals 0

    .prologue
    .line 610
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/VideoChat$1;->this$0:Lcom/google/android/libraries/hangouts/video/VideoChat;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 1

    .prologue
    .line 613
    instance-of v0, p2, Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;

    if-eqz v0, :cond_0

    .line 614
    check-cast p2, Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;

    .line 615
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChat$1;->this$0:Lcom/google/android/libraries/hangouts/video/VideoChat;

    # invokes: Lcom/google/android/libraries/hangouts/video/VideoChat;->onSoftVideoChatServiceBound(Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;)V
    invoke-static {v0, p2}, Lcom/google/android/libraries/hangouts/video/VideoChat;->access$000(Lcom/google/android/libraries/hangouts/video/VideoChat;Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;)V

    .line 620
    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 627
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChat$1;->this$0:Lcom/google/android/libraries/hangouts/video/VideoChat;

    # invokes: Lcom/google/android/libraries/hangouts/video/VideoChat;->stopListening()V
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->access$100(Lcom/google/android/libraries/hangouts/video/VideoChat;)V

    .line 628
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChat$1;->this$0:Lcom/google/android/libraries/hangouts/video/VideoChat;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/libraries/hangouts/video/VideoChat;->mSoftServiceBinder:Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;
    invoke-static {v0, v1}, Lcom/google/android/libraries/hangouts/video/VideoChat;->access$202(Lcom/google/android/libraries/hangouts/video/VideoChat;Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;)Lcom/google/android/libraries/hangouts/video/VideoChatService$SoftBinder;

    .line 629
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoChat$1;->this$0:Lcom/google/android/libraries/hangouts/video/VideoChat;

    # invokes: Lcom/google/android/libraries/hangouts/video/VideoChat;->startListening()V
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/VideoChat;->access$300(Lcom/google/android/libraries/hangouts/video/VideoChat;)V

    .line 630
    return-void
.end method
