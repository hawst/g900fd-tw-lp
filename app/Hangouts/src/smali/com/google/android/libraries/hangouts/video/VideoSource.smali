.class public abstract Lcom/google/android/libraries/hangouts/video/VideoSource;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract getHeight()I
.end method

.method abstract getTextureName()I
.end method

.method abstract getWidth()I
.end method

.method abstract isDirty()Z
.end method

.method abstract isExternalTexture()Z
.end method

.method abstract processFrame()V
.end method

.method public abstract release()V
.end method

.method abstract shouldFlipTexture()Z
.end method
