.class public Lcom/google/android/libraries/hangouts/video/VideoSpecification;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static sAllSpecificationsSet:Z

.field private static sIncomingPrimaryVideo:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

.field private static sIncomingSecondaryVideo:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

.field private static final sLock:Ljava/lang/Object;

.field private static sOutgoingNoEffectsVideo:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

.field private static sOutgoingWithEffectsVideo:Lcom/google/android/libraries/hangouts/video/VideoSpecification;


# instance fields
.field private final mFrameRate:I

.field private final mSize:Lcom/google/android/libraries/hangouts/video/Size;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->sLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/libraries/hangouts/video/Size;I)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->mSize:Lcom/google/android/libraries/hangouts/video/Size;

    .line 28
    iput p2, p0, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->mFrameRate:I

    .line 29
    return-void
.end method

.method private static checkIfAllSpecified()V
    .locals 1

    .prologue
    .line 126
    sget-object v0, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->sIncomingPrimaryVideo:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->sIncomingSecondaryVideo:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->sOutgoingNoEffectsVideo:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->sOutgoingWithEffectsVideo:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    if-eqz v0, :cond_0

    .line 129
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->sAllSpecificationsSet:Z

    .line 130
    sget-object v0, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->sLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 132
    :cond_0
    return-void
.end method

.method public static getFromString(Ljava/lang/String;)Lcom/google/android/libraries/hangouts/video/VideoSpecification;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 157
    if-nez p0, :cond_0

    .line 177
    :goto_0
    return-object v0

    .line 160
    :cond_0
    const-string v1, "x"

    invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 161
    array-length v2, v1

    const/4 v3, 0x3

    if-eq v2, v3, :cond_1

    .line 163
    const-string v1, "vclib"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "VideoSpecification can\'t parse "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcxc;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 170
    :cond_1
    const/4 v2, 0x0

    :try_start_0
    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 171
    const/4 v3, 0x1

    aget-object v3, v1, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 172
    const/4 v4, 0x2

    aget-object v1, v1, v4

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 177
    new-instance v0, Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    new-instance v4, Lcom/google/android/libraries/hangouts/video/Size;

    invoke-direct {v4, v2, v3}, Lcom/google/android/libraries/hangouts/video/Size;-><init>(II)V

    invoke-direct {v0, v4, v1}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;-><init>(Lcom/google/android/libraries/hangouts/video/Size;I)V

    goto :goto_0

    .line 174
    :catch_0
    move-exception v1

    const-string v1, "vclib"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "VideoSpecification can\'t parse "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcxc;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getIncomingPrimaryVideoSpec()Lcom/google/android/libraries/hangouts/video/VideoSpecification;
    .locals 1

    .prologue
    .line 50
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->waitForVideoSpecs()V

    .line 51
    sget-object v0, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->sIncomingPrimaryVideo:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    return-object v0
.end method

.method public static getIncomingSecondaryVideoSpec()Lcom/google/android/libraries/hangouts/video/VideoSpecification;
    .locals 1

    .prologue
    .line 59
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->waitForVideoSpecs()V

    .line 60
    sget-object v0, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->sIncomingSecondaryVideo:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    return-object v0
.end method

.method public static getOutgoingNoEffectsVideoSpec()Lcom/google/android/libraries/hangouts/video/VideoSpecification;
    .locals 1

    .prologue
    .line 68
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->waitForVideoSpecs()V

    .line 69
    sget-object v0, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->sOutgoingNoEffectsVideo:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    return-object v0
.end method

.method public static getOutgoingWithEffectsVideoSpec()Lcom/google/android/libraries/hangouts/video/VideoSpecification;
    .locals 1

    .prologue
    .line 77
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->waitForVideoSpecs()V

    .line 78
    sget-object v0, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->sOutgoingWithEffectsVideo:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    return-object v0
.end method

.method public static setMaxIncomingPrimary(Lcom/google/android/libraries/hangouts/video/VideoSpecification;)V
    .locals 2

    .prologue
    .line 85
    sget-object v1, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->sLock:Ljava/lang/Object;

    monitor-enter v1

    .line 86
    :try_start_0
    sput-object p0, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->sIncomingPrimaryVideo:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    .line 87
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->checkIfAllSpecified()V

    .line 88
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static setMaxIncomingSecondary(Lcom/google/android/libraries/hangouts/video/VideoSpecification;)V
    .locals 2

    .prologue
    .line 95
    sget-object v1, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->sLock:Ljava/lang/Object;

    monitor-enter v1

    .line 96
    :try_start_0
    sput-object p0, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->sIncomingSecondaryVideo:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    .line 97
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->checkIfAllSpecified()V

    .line 98
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static setMaxOutgoingNoEffects(Lcom/google/android/libraries/hangouts/video/VideoSpecification;)V
    .locals 2

    .prologue
    .line 105
    sget-object v1, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->sLock:Ljava/lang/Object;

    monitor-enter v1

    .line 106
    :try_start_0
    sput-object p0, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->sOutgoingNoEffectsVideo:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    .line 107
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->checkIfAllSpecified()V

    .line 108
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static setMaxOutgoingWithEffects(Lcom/google/android/libraries/hangouts/video/VideoSpecification;)V
    .locals 2

    .prologue
    .line 115
    sget-object v1, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->sLock:Ljava/lang/Object;

    monitor-enter v1

    .line 116
    :try_start_0
    sput-object p0, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->sOutgoingWithEffectsVideo:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    .line 117
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->checkIfAllSpecified()V

    .line 118
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static waitForVideoSpecs()V
    .locals 3

    .prologue
    .line 138
    sget-object v1, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->sLock:Ljava/lang/Object;

    monitor-enter v1

    .line 139
    :try_start_0
    sget-boolean v0, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->sAllSpecificationsSet:Z

    if-eqz v0, :cond_0

    .line 140
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 148
    :goto_0
    return-void

    .line 143
    :cond_0
    :try_start_1
    sget-object v0, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->sLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 148
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 144
    :catch_0
    move-exception v0

    .line 146
    :try_start_3
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 33
    check-cast p1, Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    .line 34
    if-eqz p1, :cond_0

    .line 35
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->mSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget-object v2, p1, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->mSize:Lcom/google/android/libraries/hangouts/video/Size;

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->mFrameRate:I

    iget v2, p1, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->mFrameRate:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 37
    :cond_0
    return v0
.end method

.method public getFrameRate()I
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->mFrameRate:I

    return v0
.end method

.method public getSize()Lcom/google/android/libraries/hangouts/video/Size;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->mSize:Lcom/google/android/libraries/hangouts/video/Size;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->mSize:Lcom/google/android/libraries/hangouts/video/Size;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/Size;->hashCode()I

    move-result v0

    mul-int/lit16 v0, v0, 0x115

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->mFrameRate:I

    add-int/2addr v0, v1

    return v0
.end method
