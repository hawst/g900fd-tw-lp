.class Lcom/google/android/libraries/hangouts/video/CallManager$WifiStateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# static fields
.field private static final RECONNECT_TIMEOUT:J = 0x3e8L


# instance fields
.field private mConnected:Z

.field final synthetic this$0:Lcom/google/android/libraries/hangouts/video/CallManager;


# direct methods
.method private constructor <init>(Lcom/google/android/libraries/hangouts/video/CallManager;)V
    .locals 1

    .prologue
    .line 1604
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CallManager$WifiStateReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 1608
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager$WifiStateReceiver;->mConnected:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/libraries/hangouts/video/CallManager;Lcom/google/android/libraries/hangouts/video/CallManager$1;)V
    .locals 0

    .prologue
    .line 1604
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/CallManager$WifiStateReceiver;-><init>(Lcom/google/android/libraries/hangouts/video/CallManager;)V

    return-void
.end method

.method static synthetic access$2300(Lcom/google/android/libraries/hangouts/video/CallManager$WifiStateReceiver;)Z
    .locals 1

    .prologue
    .line 1604
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager$WifiStateReceiver;->mConnected:Z

    return v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 1612
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager$WifiStateReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    # invokes: Lcom/google/android/libraries/hangouts/video/CallManager;->isPreparingOrInCall()Z
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallManager;->access$1700(Lcom/google/android/libraries/hangouts/video/CallManager;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1655
    :cond_0
    :goto_0
    return-void

    .line 1617
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager$WifiStateReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalState:Lcom/google/android/libraries/hangouts/video/LocalState;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallManager;->access$1800(Lcom/google/android/libraries/hangouts/video/CallManager;)Lcom/google/android/libraries/hangouts/video/LocalState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/LocalState;->getSignalingNetworkType()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 1621
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.net.wifi.supplicant.STATE_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1622
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.net.wifi.supplicant.CONNECTION_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1623
    :cond_2
    invoke-static {p1}, Lf;->e(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1624
    iput-boolean v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager$WifiStateReceiver;->mConnected:Z

    .line 1625
    const-string v0, "vclib"

    const-string v1, "wifi connected."

    invoke-static {v0, v1}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1628
    :cond_3
    const-string v0, "vclib"

    const-string v1, "lost wifi connection"

    invoke-static {v0, v1}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1629
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager$WifiStateReceiver;->mConnected:Z

    if-eqz v0, :cond_0

    .line 1631
    const-string v0, "vclib"

    const-string v1, "We lost our wifi connection. Give it some time to recover then  terminate the call if it can\'t."

    invoke-static {v0, v1}, Lcxc;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1633
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager$WifiStateReceiver;->mConnected:Z

    .line 1634
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager$WifiStateReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    # invokes: Lcom/google/android/libraries/hangouts/video/CallManager;->shouldAttemptReconnect()Z
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallManager;->access$2000(Lcom/google/android/libraries/hangouts/video/CallManager;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1637
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager$WifiStateReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallManager;->access$1900(Lcom/google/android/libraries/hangouts/video/CallManager;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/libraries/hangouts/video/CallManager$WifiStateReceiver$1;

    invoke-direct {v1, p0}, Lcom/google/android/libraries/hangouts/video/CallManager$WifiStateReceiver$1;-><init>(Lcom/google/android/libraries/hangouts/video/CallManager$WifiStateReceiver;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 1650
    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager$WifiStateReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    # invokes: Lcom/google/android/libraries/hangouts/video/CallManager;->handleNetworkDisconnect()V
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallManager;->access$2200(Lcom/google/android/libraries/hangouts/video/CallManager;)V

    goto :goto_0
.end method
