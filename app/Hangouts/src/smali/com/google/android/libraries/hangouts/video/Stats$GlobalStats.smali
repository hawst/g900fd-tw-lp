.class public Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;
.super Lcom/google/android/libraries/hangouts/video/Stats;
.source "PG"


# instance fields
.field public batteryLevel:I

.field public currentCpuSpeedMHz:I

.field public isOnBattery:Z

.field public onlineCpuCores:I

.field public final processCpuUsage:I

.field public final systemCpuUsage:I

.field public utilizationPerCpu:I


# direct methods
.method public constructor <init>(II)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 622
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/Stats;-><init>()V

    .line 616
    iput v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;->onlineCpuCores:I

    .line 617
    iput v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;->currentCpuSpeedMHz:I

    .line 618
    iput v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;->utilizationPerCpu:I

    .line 619
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;->isOnBattery:Z

    .line 620
    iput v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;->batteryLevel:I

    .line 623
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;->processCpuUsage:I

    .line 624
    iput p2, p0, Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;->systemCpuUsage:I

    .line 625
    return-void
.end method

.method static printLegend(Ljava/io/PrintWriter;)V
    .locals 1

    .prologue
    .line 659
    const-string v0, "  GlobalStats -- pcpu, tcpu, online cores, util per cpu, cpu freq, on battery, battery level"

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 661
    return-void
.end method


# virtual methods
.method public addTo(Ldoe;)V
    .locals 1

    .prologue
    .line 649
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;->processCpuUsage:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Ldoe;->e:Ljava/lang/Integer;

    .line 650
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;->systemCpuUsage:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Ldoe;->i:Ljava/lang/Integer;

    .line 651
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;->onlineCpuCores:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Ldoe;->j:Ljava/lang/Integer;

    .line 652
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;->currentCpuSpeedMHz:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Ldoe;->n:Ljava/lang/Integer;

    .line 653
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;->utilizationPerCpu:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Ldoe;->o:Ljava/lang/Integer;

    .line 654
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;->isOnBattery:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Ldoe;->p:Ljava/lang/Boolean;

    .line 655
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;->batteryLevel:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Ldoe;->q:Ljava/lang/Integer;

    .line 656
    return-void
.end method

.method public print(Ljava/io/PrintWriter;Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;)V
    .locals 2

    .prologue
    .line 665
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, " -- GlobalStats -- "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;->processCpuUsage:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;->systemCpuUsage:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;->onlineCpuCores:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;->utilizationPerCpu:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;->currentCpuSpeedMHz:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;->isOnBattery:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;->batteryLevel:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 673
    return-void
.end method

.method public setBatteryLevel(I)V
    .locals 0

    .prologue
    .line 644
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;->batteryLevel:I

    .line 645
    return-void
.end method

.method public setCurrentCpuSpeedMHz(I)V
    .locals 0

    .prologue
    .line 632
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;->currentCpuSpeedMHz:I

    .line 633
    return-void
.end method

.method public setIsOnBattery(Z)V
    .locals 0

    .prologue
    .line 640
    iput-boolean p1, p0, Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;->isOnBattery:Z

    .line 641
    return-void
.end method

.method public setOnlineCpuCores(I)V
    .locals 0

    .prologue
    .line 628
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;->onlineCpuCores:I

    .line 629
    return-void
.end method

.method public setUtilizationPerCpu(I)V
    .locals 0

    .prologue
    .line 636
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;->utilizationPerCpu:I

    .line 637
    return-void
.end method
