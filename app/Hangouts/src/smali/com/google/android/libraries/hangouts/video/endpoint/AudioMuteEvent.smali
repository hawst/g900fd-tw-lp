.class public Lcom/google/android/libraries/hangouts/video/endpoint/AudioMuteEvent;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/hangouts/video/endpoint/EndpointEvent;


# instance fields
.field private final mIsMuted:Z

.field private final mMuter:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;


# direct methods
.method public constructor <init>(ZLcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    if-nez p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 18
    iput-boolean p1, p0, Lcom/google/android/libraries/hangouts/video/endpoint/AudioMuteEvent;->mIsMuted:Z

    .line 19
    iput-object p2, p0, Lcom/google/android/libraries/hangouts/video/endpoint/AudioMuteEvent;->mMuter:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 20
    return-void

    .line 17
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getMuter()Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/endpoint/AudioMuteEvent;->mMuter:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    return-object v0
.end method

.method public isMuted()Z
    .locals 1

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/endpoint/AudioMuteEvent;->mIsMuted:Z

    return v0
.end method

.method public isRemoteAction()Z
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/endpoint/AudioMuteEvent;->mMuter:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
