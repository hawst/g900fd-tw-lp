.class public Lcom/google/android/libraries/hangouts/video/Registration;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final KEY_APIARY_URI:Ljava/lang/String; = "apiary_uri"

.field private static final KEY_CHECK_CONNECTIVITY:Ljava/lang/String; = "check_connectivity"

.field private static final KEY_CLIENT_VERSION:Ljava/lang/String; = "clver"

.field private static final KEY_JID_RESOURCE_PREFIX:Ljava/lang/String; = "respre"

.field private static final KEY_MAX_INCOMING_VIDEO:Ljava/lang/String; = "miv"

.field private static final KEY_MAX_OUTGOING_VIDEO:Ljava/lang/String; = "mov"

.field private static final KEY_MAX_OUTGOING_VIDEO_WITH_EFFECTS:Ljava/lang/String; = "movwe"

.field private static final KEY_NATIVE_CRASH_RECEIVER:Ljava/lang/String; = "native_crash"

.field private static final KEY_OUTGOING_RECEIVER:Ljava/lang/String; = "or"

.field private static final KEY_OVERIDE_MAP:Ljava/lang/String; = "overides"

.field private static final KEY_RAW_LOG_DIRECTORY:Ljava/lang/String; = "raw_log_directory"

.field private static final KEY_USE_HARDWARE_DECODE:Ljava/lang/String; = "use_hardware_decode"

.field private static final KEY_USE_HARDWARE_ENCODE:Ljava/lang/String; = "use_hardware_encode"

.field private static final SHARED_PREFS_NAME:Ljava/lang/String; = "vclib_registration"

.field private static final TAG:Ljava/lang/String; = "vclib"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    return-void
.end method

.method private static deserializeComponentNameString(Ljava/lang/String;)Landroid/content/ComponentName;
    .locals 4

    .prologue
    .line 254
    if-nez p0, :cond_0

    .line 255
    const-string v0, "vclib"

    const-string v1, "null ComponentName serialized String"

    invoke-static {v0, v1}, Lcxc;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    const/4 v0, 0x0

    .line 259
    :goto_0
    return-object v0

    .line 258
    :cond_0
    const-string v0, "_"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 259
    new-instance v0, Landroid/content/ComponentName;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    const/4 v3, 0x1

    aget-object v1, v1, v3

    invoke-direct {v0, v2, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static deserializeOverideMap(Ljava/lang/String;)[[Ljava/lang/String;
    .locals 6

    .prologue
    .line 276
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 277
    if-eqz p0, :cond_0

    .line 278
    const-string v0, ","

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 279
    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 280
    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 279
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 283
    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [[Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Ljava/lang/String;

    return-object v0
.end method

.method private static deserializeVideoSpecString(Ljava/lang/String;)Lcom/google/android/libraries/hangouts/video/VideoSpecification;
    .locals 5

    .prologue
    .line 235
    if-nez p0, :cond_0

    .line 236
    const-string v0, "vclib"

    const-string v1, "null VideoSpecification serialized String"

    invoke-static {v0, v1}, Lcxc;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    const/4 v0, 0x0

    .line 245
    :goto_0
    return-object v0

    .line 240
    :cond_0
    const-string v0, "_"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 241
    new-instance v0, Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    new-instance v2, Lcom/google/android/libraries/hangouts/video/Size;

    const/4 v3, 0x1

    aget-object v3, v1, v3

    .line 243
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    const/4 v4, 0x2

    aget-object v4, v1, v4

    .line 244
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-direct {v2, v3, v4}, Lcom/google/android/libraries/hangouts/video/Size;-><init>(II)V

    const/4 v3, 0x0

    aget-object v1, v1, v3

    .line 245
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-direct {v0, v2, v1}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;-><init>(Lcom/google/android/libraries/hangouts/video/Size;I)V

    goto :goto_0
.end method

.method static getRegisteredComponents(Landroid/content/Context;)Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;
    .locals 17

    .prologue
    .line 204
    const-string v1, "vclib_registration"

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v14

    .line 208
    new-instance v1, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;

    const-string v2, "respre"

    const/4 v3, 0x0

    .line 209
    invoke-interface {v14, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "clver"

    const/4 v4, 0x0

    .line 210
    invoke-interface {v14, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "miv"

    const/4 v5, 0x0

    .line 211
    invoke-interface {v14, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/libraries/hangouts/video/Registration;->deserializeVideoSpecString(Ljava/lang/String;)Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v4

    const-string v5, "mov"

    const/4 v6, 0x0

    .line 212
    invoke-interface {v14, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/libraries/hangouts/video/Registration;->deserializeVideoSpecString(Ljava/lang/String;)Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v5

    const-string v6, "movwe"

    const/4 v7, 0x0

    .line 213
    invoke-interface {v14, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/libraries/hangouts/video/Registration;->deserializeVideoSpecString(Ljava/lang/String;)Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v6

    const-string v7, "or"

    const/4 v8, 0x0

    .line 216
    invoke-interface {v14, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 215
    invoke-static {v7}, Lcom/google/android/libraries/hangouts/video/Registration;->deserializeComponentNameString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v7

    const-string v8, "native_crash"

    const/4 v9, 0x0

    .line 217
    invoke-interface {v14, v8, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/libraries/hangouts/video/Registration;->deserializeComponentNameString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v8

    const-string v9, "overides"

    const/4 v10, 0x0

    .line 219
    invoke-interface {v14, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/libraries/hangouts/video/Registration;->deserializeOverideMap(Ljava/lang/String;)[[Ljava/lang/String;

    move-result-object v9

    const-string v10, "use_hardware_decode"

    const/4 v11, 0x0

    .line 220
    invoke-interface {v14, v10, v11}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v10

    const-string v11, "use_hardware_encode"

    const/4 v12, 0x0

    .line 221
    invoke-interface {v14, v11, v12}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v11

    const-string v12, "raw_log_directory"

    const/4 v13, 0x0

    .line 222
    invoke-interface {v14, v12, v13}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "check_connectivity"

    const/4 v15, 0x0

    .line 223
    invoke-interface {v14, v13, v15}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v13

    const-string v15, "apiary_uri"

    const/16 v16, 0x0

    .line 224
    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-direct/range {v1 .. v14}, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/libraries/hangouts/video/VideoSpecification;Lcom/google/android/libraries/hangouts/video/VideoSpecification;Lcom/google/android/libraries/hangouts/video/VideoSpecification;Landroid/content/ComponentName;Landroid/content/ComponentName;[[Ljava/lang/String;ZZLjava/lang/String;ILjava/lang/String;)V

    return-object v1
.end method

.method static register(Landroid/content/Context;Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;)V
    .locals 3

    .prologue
    .line 169
    const-string v0, "vclib_registration"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 171
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 173
    iget-object v1, p1, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->jidResourcePrefix:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->jidResourcePrefix:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 174
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "jidResourcePrefix cannot be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 176
    :cond_1
    iget-object v1, p1, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->clientVersion:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->clientVersion:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 177
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "clientVersion cannot be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 180
    :cond_3
    const-string v1, "respre"

    iget-object v2, p1, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->jidResourcePrefix:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 181
    const-string v1, "clver"

    iget-object v2, p1, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->clientVersion:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 182
    const-string v1, "miv"

    iget-object v2, p1, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->maxIncomingVideoSpec:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    .line 183
    invoke-static {v2}, Lcom/google/android/libraries/hangouts/video/Registration;->serializeVideoSpec(Lcom/google/android/libraries/hangouts/video/VideoSpecification;)Ljava/lang/String;

    move-result-object v2

    .line 182
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 184
    const-string v1, "mov"

    iget-object v2, p1, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->maxOutgoingSpecNoEffects:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    .line 185
    invoke-static {v2}, Lcom/google/android/libraries/hangouts/video/Registration;->serializeVideoSpec(Lcom/google/android/libraries/hangouts/video/VideoSpecification;)Ljava/lang/String;

    move-result-object v2

    .line 184
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 186
    const-string v1, "movwe"

    iget-object v2, p1, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->maxOutgoingSpecWithEffects:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    .line 187
    invoke-static {v2}, Lcom/google/android/libraries/hangouts/video/Registration;->serializeVideoSpec(Lcom/google/android/libraries/hangouts/video/VideoSpecification;)Ljava/lang/String;

    move-result-object v2

    .line 186
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 188
    const-string v1, "or"

    iget-object v2, p1, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->outgoingVideoChatReceiver:Landroid/content/ComponentName;

    .line 189
    invoke-static {v2}, Lcom/google/android/libraries/hangouts/video/Registration;->serializeComponentName(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v2

    .line 188
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 190
    const-string v1, "native_crash"

    iget-object v2, p1, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->nativeCrashReceiver:Landroid/content/ComponentName;

    .line 191
    invoke-static {v2}, Lcom/google/android/libraries/hangouts/video/Registration;->serializeComponentName(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v2

    .line 190
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 192
    const-string v1, "overides"

    iget-object v2, p1, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->overides:[[Ljava/lang/String;

    .line 193
    invoke-static {v2}, Lcom/google/android/libraries/hangouts/video/Registration;->serializeOverideMap([[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 192
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 194
    const-string v1, "use_hardware_decode"

    iget-boolean v2, p1, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->useHardwareDecode:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 195
    const-string v1, "use_hardware_encode"

    iget-boolean v2, p1, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->useHardwareEncode:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 196
    const-string v1, "raw_log_directory"

    iget-object v2, p1, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->rawLogDirectory:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 197
    const-string v1, "check_connectivity"

    iget v2, p1, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->checkConnectivity:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 198
    const-string v1, "apiary_uri"

    iget-object v2, p1, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->apiaryUri:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 199
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 200
    return-void
.end method

.method private static serializeComponentName(Landroid/content/ComponentName;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 250
    const-string v0, "%s_%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static serializeOverideMap([[Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 263
    const/4 v0, 0x0

    .line 264
    array-length v4, p0

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v3, p0, v1

    .line 265
    if-eqz v0, :cond_0

    .line 266
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ","

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 270
    :goto_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v5, v3, v2

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ":"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v5, 0x1

    aget-object v3, v3, v5

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 264
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move-object v0, v3

    goto :goto_0

    .line 268
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    goto :goto_1

    .line 272
    :cond_1
    return-object v0
.end method

.method private static serializeVideoSpec(Lcom/google/android/libraries/hangouts/video/VideoSpecification;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 229
    const-string v0, "%d_%d_%d"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 231
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->getFrameRate()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->getSize()Lcom/google/android/libraries/hangouts/video/Size;

    move-result-object v3

    iget v3, v3, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->getSize()Lcom/google/android/libraries/hangouts/video/Size;

    move-result-object v3

    iget v3, v3, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 229
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
