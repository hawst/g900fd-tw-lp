.class public Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;
.super Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;
.source "PG"


# instance fields
.field private final mObfuscatedGaiaId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ZZ)V
    .locals 6

    .prologue
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p5

    move v5, p6

    .line 13
    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;-><init>(Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 14
    iput-object p4, p0, Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;->mObfuscatedGaiaId:Ljava/lang/String;

    .line 15
    return-void
.end method


# virtual methods
.method public getObfuscatedGaiaId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/endpoint/GaiaEndpoint;->mObfuscatedGaiaId:Ljava/lang/String;

    return-object v0
.end method
