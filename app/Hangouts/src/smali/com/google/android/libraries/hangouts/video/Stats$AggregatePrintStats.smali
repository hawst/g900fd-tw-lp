.class Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final frameRateReceiveList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private final frameRateSendList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mostRecentlyUsingRelayServer:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;->frameRateSendList:Ljava/util/ArrayList;

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;->frameRateReceiveList:Ljava/util/ArrayList;

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;->mostRecentlyUsingRelayServer:Z

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;->frameRateSendList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;->frameRateReceiveList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;Z)Z
    .locals 0

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;->mostRecentlyUsingRelayServer:Z

    return p1
.end method


# virtual methods
.method print(Ljava/io/PrintWriter;)V
    .locals 2

    .prologue
    .line 38
    const-string v0, "Aggregate statistics"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 39
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "               send FPS: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;->frameRateSendList:Ljava/util/ArrayList;

    .line 40
    # invokes: Lcom/google/android/libraries/hangouts/video/Stats;->calculateMedian(Ljava/util/ArrayList;)F
    invoke-static {v1}, Lcom/google/android/libraries/hangouts/video/Stats;->access$000(Ljava/util/ArrayList;)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 39
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "  Median video receive FPS: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;->frameRateReceiveList:Ljava/util/ArrayList;

    .line 42
    # invokes: Lcom/google/android/libraries/hangouts/video/Stats;->calculateMedian(Ljava/util/ArrayList;)F
    invoke-static {v1}, Lcom/google/android/libraries/hangouts/video/Stats;->access$000(Ljava/util/ArrayList;)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 41
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "  using relay: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;->mostRecentlyUsingRelayServer:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 44
    return-void
.end method
