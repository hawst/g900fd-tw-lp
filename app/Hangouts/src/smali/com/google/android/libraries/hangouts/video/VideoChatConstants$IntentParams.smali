.class public Lcom/google/android/libraries/hangouts/video/VideoChatConstants$IntentParams;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final ACTION_ADD_LOG_COMMENT:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.ACTION_ADD_LOG_COMMENT"

.field public static final ACTION_BLOCK_MEDIA:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.ACTION_BLOCK_MEDIA"

.field public static final ACTION_DISCONNECT_AND_SIGN_OUT:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.ACTION_DISCONNECT_AND_SIGN_OUT"

.field public static final ACTION_INVITE_PSTN:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.ACTION_INVITE_PSTN"

.field public static final ACTION_INVITE_USERS:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.ACTION_INVITE_USERS"

.field public static final ACTION_NATIVE_CRASH:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.ACTION_NATIVE_CRASH"

.field public static final ACTION_ONGOING_NOTIFICATION_REQUEST:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.ACTION_ONGOING_NOTIFICATION_REQUEST"

.field public static final ACTION_ONGOING_NOTIFICATION_RESPONSE:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.ACTION_ONGOING_NOTIFICATION_RESPONSE"

.field public static final ACTION_PREPARE_CALL:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.ACTION_PREPARE_CALL"

.field public static final ACTION_PUBLISH_AUDIO_MUTE_STATE:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.ACTION_PUBLISH_AUDIO_MUTE_STATE"

.field public static final ACTION_PUBLISH_VIDEO_MUTE_STATE:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.ACTION_PUBLISH_VIDEO_MUTE_STATE"

.field public static final ACTION_PUSH_NOTIFICATION:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.ACTION_PUSH_NOTIFICATION"

.field public static final ACTION_REMOTE_MUTE:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.ACTION_REMOTE_MUTE"

.field public static final ACTION_SEND_DTMF:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.ACTION_SEND_DTMF"

.field public static final ACTION_SET_AUDIO_DEVICE:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.ACTION_SET_AUDIO_DEVICE"

.field public static final ACTION_SET_CALL_TAG:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.ACTION_SET_CALL_TAG"

.field public static final ACTION_SET_MUTE:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.ACTION_SET_MUTE"

.field public static final ACTION_SIGN_IN:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.ACTION_SIGN_IN"

.field public static final ACTION_START_CALL:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.ACTION_START_CALL"

.field public static final ACTION_TERMINATE_CALL:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.ACTION_TERMINATE_CALL"

.field public static final EXTRA_ACCOUNT_NAME:Ljava/lang/String; = "account_name"

.field public static final EXTRA_ANDROID_ID:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.android_id"

.field public static final EXTRA_AUDIO_DEVICE:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.audio_device"

.field public static final EXTRA_CALL_OPTIONS:Ljava/lang/String; = "call_options"

.field public static final EXTRA_CALL_TAG:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.tag"

.field public static final EXTRA_COMPRESSED_LOG_FILE:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.compressed_log_file"

.field public static final EXTRA_CREATE_ACTIVITY:Ljava/lang/String; = "create_activity"

.field public static final EXTRA_DTMF_CODE:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.dtmf_code"

.field public static final EXTRA_DURATION_MILLIS:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.duration_millis"

.field public static final EXTRA_GCM_REGISTRATION_ID:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.gcm_registration"

.field public static final EXTRA_HANGOUT_REQUEST:Ljava/lang/String; = "hangout_request"

.field public static final EXTRA_HANGOUT_START_CONTEXT_BASE64:Ljava/lang/String; = "hangout_start_context"

.field public static final EXTRA_INVITED_CIRCLE_IDS:Ljava/lang/String; = "invited_circle_ids"

.field public static final EXTRA_INVITED_USER_IDS:Ljava/lang/String; = "invited_user_ids"

.field public static final EXTRA_INVITE_CONVERSATION_PARTICIPANTS:Ljava/lang/String; = "invite_conversation"

.field public static final EXTRA_INVITE_TYPE:Ljava/lang/String; = "invite_type"

.field public static final EXTRA_IS_VIDEO:Ljava/lang/String; = "is_video"

.field public static final EXTRA_KEEP_ALIVE:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.keep_alive"

.field public static final EXTRA_LOG_COMMENT:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.log_comment"

.field public static final EXTRA_MEDIA_BLOCKEE:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.media_blockee"

.field public static final EXTRA_MESSAGE_ACTION:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.message_action"

.field public static final EXTRA_MUTE:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.mute"

.field public static final EXTRA_NO_WIFI:Ljava/lang/String; = "no_wifi"

.field public static final EXTRA_ONGOING_NOTIFICATION:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.ongoing_notif"

.field public static final EXTRA_OUTPUT_RECEIVER:Ljava/lang/String; = "output_receiver"

.field public static final EXTRA_PHONE_NUMBER:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.phone_number"

.field public static final EXTRA_PSTN_JID:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.pstn_jid"

.field public static final EXTRA_PUSH_PROTO:Ljava/lang/String; = "push_proto"

.field public static final EXTRA_REMOTE_BARE_JID:Ljava/lang/String; = "remote_bare_jid"

.field public static final EXTRA_REMOTE_JID:Ljava/lang/String; = "remote_jid"

.field public static final EXTRA_REMOTE_MUC_JID:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.remote_muc_jid"

.field public static final EXTRA_REMOTE_MUTEE:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.remote_mutee"

.field public static final EXTRA_RING_INVITEES:Ljava/lang/String; = "ring_invitees"

.field public static final EXTRA_SESSION_ID:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.sessionid"

.field public static final EXTRA_SHOULD_MANAGE_AUDIO:Ljava/lang/String; = "should_manage_audio"

.field public static final EXTRA_TERMINATE_REASON:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.terminate_reason"

.field public static final EXTRA_VIDEO_MUTE_STATE:Ljava/lang/String; = "com.google.android.libraries.hangouts.video.video_mute"

.field public static final PACKAGE_PREFIX:Ljava/lang/String; = "com.google.android.libraries.hangouts.video."


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
