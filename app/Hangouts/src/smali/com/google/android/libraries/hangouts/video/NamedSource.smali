.class public Lcom/google/android/libraries/hangouts/video/NamedSource;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final name:Ljava/lang/String;

.field public final nick:Ljava/lang/String;

.field public final ssrc:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/NamedSource;->nick:Ljava/lang/String;

    .line 18
    iput-object p2, p0, Lcom/google/android/libraries/hangouts/video/NamedSource;->name:Ljava/lang/String;

    .line 19
    iput p3, p0, Lcom/google/android/libraries/hangouts/video/NamedSource;->ssrc:I

    .line 20
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 29
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/google/android/libraries/hangouts/video/NamedSource;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/libraries/hangouts/video/NamedSource;

    iget v0, p1, Lcom/google/android/libraries/hangouts/video/NamedSource;->ssrc:I

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/NamedSource;->ssrc:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/NamedSource;->ssrc:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/NamedSource;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/NamedSource;->nick:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/NamedSource;->ssrc:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
