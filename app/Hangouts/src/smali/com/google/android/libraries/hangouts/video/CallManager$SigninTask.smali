.class Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;
.super Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/hangouts/video/SafeAsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/util/Pair",
        "<",
        "Ljava/lang/String;",
        "Landroid/content/Intent;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final mAccountName:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/libraries/hangouts/video/CallManager;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/hangouts/video/CallManager;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 670
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    .line 671
    sget-wide v0, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;->MAX_NETWORK_OPERATION_TIME_MILLIS:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;-><init>(J)V

    .line 672
    iput-object p2, p0, Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;->mAccountName:Ljava/lang/String;

    .line 673
    return-void
.end method

.method private getAccount()Landroid/accounts/Account;
    .locals 5

    .prologue
    .line 714
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallManager;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallManager;->access$500(Lcom/google/android/libraries/hangouts/video/CallManager;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 715
    invoke-virtual {v0}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v1

    .line 716
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 717
    aget-object v2, v1, v0

    .line 718
    iget-object v3, v2, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string v4, "com.google"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    aget-object v3, v1, v0

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;->mAccountName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 719
    return-object v2

    .line 716
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 723
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No account found for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;->mAccountName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private getAuthScope(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 676
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 679
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x80

    .line 678
    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    .line 680
    const-string v2, "com.google.android.hangouts.video.USE_NATIVE_SCOPE"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 686
    :goto_0
    if-eqz v0, :cond_0

    const-string v0, "oauth2:https://www.googleapis.com/auth/chat https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/hangouts https://www.googleapis.com/auth/identity.plus.page.impersonation https://www.googleapis.com/auth/chat.native"

    :goto_1
    return-object v0

    :cond_0
    const-string v0, "oauth2:https://www.googleapis.com/auth/chat https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/hangouts https://www.googleapis.com/auth/identity.plus.page.impersonation"

    goto :goto_1

    .line 684
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method protected varargs doInBackgroundTimed([Ljava/lang/Void;)Landroid/util/Pair;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 691
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    .line 693
    :try_start_0
    const-string v2, "vclib"

    const-string v3, "SigninTask.doInBackgroundTimed"

    invoke-static {v2, v3}, Lcxc;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 694
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallManager;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/libraries/hangouts/video/CallManager;->access$500(Lcom/google/android/libraries/hangouts/video/CallManager;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 698
    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 699
    invoke-direct {p0, v2}, Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;->getAuthScope(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 698
    invoke-static {v2, v1, v3, v4}, Lcfn;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    .line 700
    const-string v2, "vclib"

    const-string v3, "Got authToken for hangouts"

    invoke-static {v2, v3}, Lcxc;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 701
    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    :try_end_0
    .catch Lcfq; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcfm; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 709
    :goto_0
    return-object v0

    .line 702
    :catch_0
    move-exception v1

    .line 703
    const-string v2, "vclib"

    const-string v3, "Got authException"

    invoke-static {v2, v3, v1}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 704
    invoke-virtual {v1}, Lcfq;->b()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0

    .line 705
    :catch_1
    move-exception v1

    .line 706
    const-string v2, "vclib"

    const-string v3, "Error in getToken"

    invoke-static {v2, v3, v1}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 709
    :catch_2
    move-exception v1

    goto :goto_0
.end method

.method protected bridge synthetic doInBackgroundTimed([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 667
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;->doInBackgroundTimed([Ljava/lang/Void;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/util/Pair;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Landroid/content/Intent;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 728
    const-string v0, "vclib"

    const-string v1, "SigninTask.onPostExecute"

    invoke-static {v0, v1}, Lcxc;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 729
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 730
    const-string v0, "vclib"

    const-string v1, "Signin cancelled"

    invoke-static {v0, v1}, Lcxc;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 750
    :cond_0
    :goto_0
    return-void

    .line 734
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    # setter for: Lcom/google/android/libraries/hangouts/video/CallManager;->mSigninTask:Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;
    invoke-static {v0, v2}, Lcom/google/android/libraries/hangouts/video/CallManager;->access$602(Lcom/google/android/libraries/hangouts/video/CallManager;Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;)Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;

    .line 736
    if-nez p1, :cond_2

    .line 737
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/hangouts/video/CallManager;->handleCallEnd(ILjava/lang/String;)V

    goto :goto_0

    .line 738
    :cond_2
    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-eqz v0, :cond_3

    .line 739
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    # setter for: Lcom/google/android/libraries/hangouts/video/CallManager;->mAuthToken:Ljava/lang/String;
    invoke-static {v1, v0}, Lcom/google/android/libraries/hangouts/video/CallManager;->access$702(Lcom/google/android/libraries/hangouts/video/CallManager;Ljava/lang/String;)Ljava/lang/String;

    .line 740
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallManager;->mLibjingle:Lcom/google/android/libraries/hangouts/video/Libjingle;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallManager;->access$1100(Lcom/google/android/libraries/hangouts/video/CallManager;)Lcom/google/android/libraries/hangouts/video/Libjingle;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;->mAccountName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallManager;->mRegistrationComponents:Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;
    invoke-static {v2}, Lcom/google/android/libraries/hangouts/video/CallManager;->access$800(Lcom/google/android/libraries/hangouts/video/CallManager;)Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->apiaryUri:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    .line 741
    # getter for: Lcom/google/android/libraries/hangouts/video/CallManager;->mAuthToken:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/libraries/hangouts/video/CallManager;->access$700(Lcom/google/android/libraries/hangouts/video/CallManager;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallManager;->mGcmRegistration:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/libraries/hangouts/video/CallManager;->access$900(Lcom/google/android/libraries/hangouts/video/CallManager;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallManager;->mAndroidId:Ljava/lang/String;
    invoke-static {v5}, Lcom/google/android/libraries/hangouts/video/CallManager;->access$1000(Lcom/google/android/libraries/hangouts/video/CallManager;)Ljava/lang/String;

    move-result-object v5

    .line 740
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/libraries/hangouts/video/Libjingle;->signIn(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 742
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallManager;->mApiaryClient:Lcom/google/android/libraries/hangouts/video/ApiaryClient;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallManager;->access$1200(Lcom/google/android/libraries/hangouts/video/CallManager;)Lcom/google/android/libraries/hangouts/video/ApiaryClient;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallManager;->mAuthToken:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/libraries/hangouts/video/CallManager;->access$700(Lcom/google/android/libraries/hangouts/video/CallManager;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/ApiaryClient;->setAuthToken(Ljava/lang/String;)V

    .line 743
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallManager;->mApiaryClient:Lcom/google/android/libraries/hangouts/video/ApiaryClient;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallManager;->access$1200(Lcom/google/android/libraries/hangouts/video/CallManager;)Lcom/google/android/libraries/hangouts/video/ApiaryClient;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/ApiaryClient;->setListener(Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestListener;)V

    goto :goto_0

    .line 745
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    # invokes: Lcom/google/android/libraries/hangouts/video/CallManager;->reset()V
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallManager;->access$1300(Lcom/google/android/libraries/hangouts/video/CallManager;)V

    .line 746
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    # invokes: Lcom/google/android/libraries/hangouts/video/CallManager;->getClonedListeners()Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallManager;->access$1400(Lcom/google/android/libraries/hangouts/video/CallManager;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStateListener;

    .line 747
    iget-object v1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Landroid/content/Intent;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/hangouts/video/CallStateListener;->onAuthUserActionRequired(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 667
    check-cast p1, Landroid/util/Pair;

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/hangouts/video/CallManager$SigninTask;->onPostExecute(Landroid/util/Pair;)V

    return-void
.end method
