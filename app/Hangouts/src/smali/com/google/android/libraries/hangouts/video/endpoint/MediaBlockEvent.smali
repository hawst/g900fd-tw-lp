.class public Lcom/google/android/libraries/hangouts/video/endpoint/MediaBlockEvent;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/hangouts/video/endpoint/EndpointEvent;


# instance fields
.field private final mBlocker:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/endpoint/MediaBlockEvent;->mBlocker:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 13
    return-void
.end method


# virtual methods
.method public getBlocker()Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/endpoint/MediaBlockEvent;->mBlocker:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    return-object v0
.end method
