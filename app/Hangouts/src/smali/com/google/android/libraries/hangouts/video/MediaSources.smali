.class public Lcom/google/android/libraries/hangouts/video/MediaSources;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final audioAdded:[Lcom/google/android/libraries/hangouts/video/NamedSource;

.field public final audioRemoved:[Lcom/google/android/libraries/hangouts/video/NamedSource;

.field public final videoAdded:[Lcom/google/android/libraries/hangouts/video/NamedSource;

.field public final videoRemoved:[Lcom/google/android/libraries/hangouts/video/NamedSource;


# direct methods
.method public constructor <init>([Lcom/google/android/libraries/hangouts/video/NamedSource;[Lcom/google/android/libraries/hangouts/video/NamedSource;[Lcom/google/android/libraries/hangouts/video/NamedSource;[Lcom/google/android/libraries/hangouts/video/NamedSource;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/MediaSources;->audioAdded:[Lcom/google/android/libraries/hangouts/video/NamedSource;

    .line 20
    iput-object p2, p0, Lcom/google/android/libraries/hangouts/video/MediaSources;->audioRemoved:[Lcom/google/android/libraries/hangouts/video/NamedSource;

    .line 21
    iput-object p3, p0, Lcom/google/android/libraries/hangouts/video/MediaSources;->videoAdded:[Lcom/google/android/libraries/hangouts/video/NamedSource;

    .line 22
    iput-object p4, p0, Lcom/google/android/libraries/hangouts/video/MediaSources;->videoRemoved:[Lcom/google/android/libraries/hangouts/video/NamedSource;

    .line 23
    return-void
.end method
