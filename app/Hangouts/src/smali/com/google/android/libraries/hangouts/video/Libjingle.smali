.class final Lcom/google/android/libraries/hangouts/video/Libjingle;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final AVAILABLE:I = 0x1

.field private static final CALL_FLAG_ABUSE_TOS_ACCEPTED:I = 0x8

.field private static final CALL_FLAG_ALLOW_ON_AIR:I = 0x4

.field private static final CALL_FLAG_HAS_VIDEO:I = 0x1

.field private static final CALL_FLAG_MINOR_USER:I = 0x2

.field public static final HAS_CAMERA_V1:I = 0x8

.field public static final HAS_VIDEO_V1:I = 0x4

.field public static final HAS_VOICE_V1:I = 0x2

.field private static final LIBJINGLE_LS_ERROR:I = 0x4

.field private static final LIBJINGLE_LS_INFO:I = 0x2

.field private static final LIBJINGLE_LS_VERBOSE:I = 0x1

.field private static final LIBJINGLE_LS_WARNING:I = 0x3

.field private static final LIBJINGLE_VIDEO_ENCODE_CORES:Ljava/lang/String; = "VIDEO_ENCODE_CORES"

.field private static final LIBJINGLE_VIDEO_MAX_FRAMERATE:Ljava/lang/String; = "VIDEO_MAX_FRAMERATE"

.field private static final LIBJINGLE_VIDEO_MAX_HEIGHT:Ljava/lang/String; = "VIDEO_MAX_HEIGHT"

.field private static final LIBJINGLE_VIDEO_MAX_WIDTH:Ljava/lang/String; = "VIDEO_MAX_WIDTH"

.field public static final STR1_KEY:Ljava/lang/String; = "str1"

.field public static final STR2_KEY:Ljava/lang/String; = "str2"

.field public static final STR3_KEY:Ljava/lang/String; = "str3"

.field public static final STR4_KEY:Ljava/lang/String; = "str4"

.field private static final TAG:Ljava/lang/String; = "vclib"

.field public static final UNAVAILABLE:I


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private mInitialized:Z

.field private mNativeContext:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 48
    const-string v0, "videochat_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 51
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-ge v0, v1, :cond_1

    const/4 v0, 0x1

    .line 52
    :goto_0
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/Libjingle;->nativeInit(Z)V

    .line 61
    invoke-static {}, Lcxc;->a()I

    move-result v0

    .line 62
    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 63
    const/4 v0, 0x5

    .line 65
    :cond_0
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/Libjingle;->nativeSetLoggingLevel(I)V

    .line 66
    return-void

    .line 51
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/Libjingle;->mContext:Landroid/content/Context;

    .line 114
    iput-object p2, p0, Lcom/google/android/libraries/hangouts/video/Libjingle;->mHandler:Landroid/os/Handler;

    .line 115
    return-void
.end method

.method private static dispatchNativeEvent(Ljava/lang/Object;IIILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 500
    invoke-static {}, Lcwz;->e()V

    .line 503
    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/Libjingle;

    .line 509
    if-eqz v0, :cond_0

    iget v1, v0, Lcom/google/android/libraries/hangouts/video/Libjingle;->mNativeContext:I

    if-eqz v1, :cond_0

    .line 511
    iget-object v1, v0, Lcom/google/android/libraries/hangouts/video/Libjingle;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1, p2, p3, p8}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 512
    new-instance v2, Landroid/os/Bundle;

    const/4 v3, 0x2

    invoke-direct {v2, v3}, Landroid/os/Bundle;-><init>(I)V

    .line 513
    const-string v3, "str1"

    check-cast p4, Ljava/lang/String;

    invoke-virtual {v2, v3, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    const-string v3, "str2"

    check-cast p5, Ljava/lang/String;

    invoke-virtual {v2, v3, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    const-string v3, "str3"

    check-cast p6, Ljava/lang/String;

    invoke-virtual {v2, v3, p6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 516
    const-string v3, "str4"

    check-cast p7, Ljava/lang/String;

    invoke-virtual {v2, v3, p7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 517
    invoke-virtual {v1, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 520
    iget-object v0, v0, Lcom/google/android/libraries/hangouts/video/Libjingle;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 522
    :cond_0
    return-void
.end method

.method private getCorrectedLibjingleMediaLogLevel(I)I
    .locals 0

    .prologue
    .line 139
    packed-switch p1, :pswitch_data_0

    .line 144
    :goto_0
    return p1

    .line 142
    :pswitch_0
    add-int/lit8 p1, p1, -0x1

    goto :goto_0

    .line 139
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private getLibjingleLogLevel(Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x3

    const/4 v0, 0x2

    .line 123
    invoke-static {p1, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 124
    const/4 v0, 0x1

    .line 132
    :cond_0
    :goto_0
    return v0

    .line 126
    :cond_1
    invoke-static {p1, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 129
    invoke-static {p1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 130
    goto :goto_0

    :cond_2
    move v0, v2

    .line 132
    goto :goto_0
.end method

.method private initGservicesOverrides([[Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x1

    .line 183
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Libjingle;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 184
    array-length v3, p1

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_4

    aget-object v4, p1, v0

    .line 185
    aget-object v5, v4, v1

    .line 186
    aget-object v4, v4, v7

    .line 188
    sget-object v6, Lcom/google/android/libraries/hangouts/video/VideoChat;->USE_DEFAULT_NETWORKS_ONLY:Ljava/lang/String;

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 195
    invoke-static {v2, v5, v7}, Lcww;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    .line 196
    if-ne v4, v7, :cond_0

    invoke-static {}, Lcxg;->a()Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    .line 197
    :cond_1
    sget-object v4, Lcom/google/android/libraries/hangouts/video/VideoChat;->USE_DEFAULT_NETWORKS_ONLY:Ljava/lang/String;

    const-string v5, "true"

    invoke-direct {p0, v4, v5}, Lcom/google/android/libraries/hangouts/video/Libjingle;->nativeSetGServicesOverride(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 201
    :cond_3
    invoke-static {v2, v5}, Lcww;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 202
    if-eqz v5, :cond_2

    .line 203
    invoke-direct {p0, v4, v5}, Lcom/google/android/libraries/hangouts/video/Libjingle;->nativeSetGServicesOverride(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 209
    :cond_4
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->getIncomingPrimaryVideoSpec()Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v0

    .line 210
    const-string v1, "VIDEO_MAX_WIDTH"

    .line 211
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->getSize()Lcom/google/android/libraries/hangouts/video/Size;

    move-result-object v2

    iget v2, v2, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 210
    invoke-direct {p0, v1, v2}, Lcom/google/android/libraries/hangouts/video/Libjingle;->nativeSetGServicesOverride(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    const-string v1, "VIDEO_MAX_HEIGHT"

    .line 213
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->getSize()Lcom/google/android/libraries/hangouts/video/Size;

    move-result-object v2

    iget v2, v2, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 212
    invoke-direct {p0, v1, v2}, Lcom/google/android/libraries/hangouts/video/Libjingle;->nativeSetGServicesOverride(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    const-string v1, "VIDEO_MAX_FRAMERATE"

    .line 215
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->getFrameRate()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 214
    invoke-direct {p0, v1, v0}, Lcom/google/android/libraries/hangouts/video/Libjingle;->nativeSetGServicesOverride(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    const-string v0, "VIDEO_ENCODE_CORES"

    .line 217
    invoke-static {}, Lcxl;->b()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 216
    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/hangouts/video/Libjingle;->nativeSetGServicesOverride(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    return-void
.end method

.method public static load()V
    .locals 0

    .prologue
    .line 71
    return-void
.end method

.method private log(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 535
    const-string v0, "vclib"

    invoke-static {v0, p1}, Lcxc;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    return-void
.end method

.method private static final native nativeAddLogComment(Ljava/lang/String;)V
.end method

.method private final native nativeBlockMedia(Ljava/lang/String;)V
.end method

.method private final native nativeCallHangout(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
.end method

.method private final native nativeCheckConnectivity()V
.end method

.method private final native nativeEndCall(Ljava/lang/String;Z)V
.end method

.method private final native nativeEndCallAndSignOut(Ljava/lang/String;)V
.end method

.method private final native nativeFinalize()V
.end method

.method private static native nativeInit(Z)V
.end method

.method private final native nativeInitializeRenderer()V
.end method

.method private final native nativeInvitePstn(Ljava/lang/String;Ljava/lang/String;Z)V
.end method

.method private final native nativeInviteUsers(Z[Ljava/lang/String;[Ljava/lang/String;IZZLjava/lang/String;)V
.end method

.method private final native nativeIsSecure(Ljava/lang/String;)Z
.end method

.method private final native nativeIsVideo(Ljava/lang/String;)Z
.end method

.method private final native nativePublishAudioMuteState(Z)V
.end method

.method private final native nativePublishVideoMuteState(Z)V
.end method

.method private final native nativeRelease()V
.end method

.method private final native nativeRemoteMute(Ljava/lang/String;)V
.end method

.method private final native nativeRequestVideoViews([Lcom/google/android/libraries/hangouts/video/VideoViewRequest;)V
.end method

.method private final native nativeSendDtmf(CILjava/lang/String;)V
.end method

.method private final native nativeSendStanza(Ljava/lang/String;)V
.end method

.method private final native nativeSetGServicesOverride(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private static final native nativeSetLoggingLevel(I)V
.end method

.method private final native nativeSetSelfViewFrameParameters(ZIII)V
.end method

.method private final native nativeSetup(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZ)V
.end method

.method private final native nativeSignIn(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private static reportNativeCrash()V
    .locals 3

    .prologue
    .line 527
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.libraries.hangouts.video.ACTION_NATIVE_CRASH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 528
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getInstance()Lcom/google/android/libraries/hangouts/video/VideoChat;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/VideoChat;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 529
    invoke-static {v1}, Lcom/google/android/libraries/hangouts/video/Registration;->getRegisteredComponents(Landroid/content/Context;)Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;

    move-result-object v2

    .line 530
    iget-object v2, v2, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->nativeCrashReceiver:Landroid/content/ComponentName;

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 531
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 532
    return-void
.end method


# virtual methods
.method public addLogComment(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 387
    invoke-static {p1}, Lcom/google/android/libraries/hangouts/video/Libjingle;->nativeAddLogComment(Ljava/lang/String;)V

    .line 388
    return-void
.end method

.method public final native bindRenderer(III)V
.end method

.method public blockMedia(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 323
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/Libjingle;->nativeBlockMedia(Ljava/lang/String;)V

    .line 324
    return-void
.end method

.method public endCallAndSignOut(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 335
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/Libjingle;->nativeEndCallAndSignOut(Ljava/lang/String;)V

    .line 336
    return-void
.end method

.method protected finalize()V
    .locals 0

    .prologue
    .line 251
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/Libjingle;->nativeFinalize()V

    .line 252
    return-void
.end method

.method public final native handleApiaryResponse(J[B)V
.end method

.method public final native handlePushNotification([B)V
.end method

.method public init(Ljava/lang/String;Ljava/lang/String;[[Ljava/lang/String;ZLjava/lang/String;)V
    .locals 10

    .prologue
    .line 156
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/Libjingle;->mInitialized:Z

    if-eqz v0, :cond_0

    .line 157
    const-string v0, "init: already initialized"

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/Libjingle;->log(Ljava/lang/String;)V

    .line 179
    :goto_0
    return-void

    .line 161
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/Libjingle;->mInitialized:Z

    .line 163
    invoke-direct {p0, p3}, Lcom/google/android/libraries/hangouts/video/Libjingle;->initGservicesOverrides([[Ljava/lang/String;)V

    .line 167
    const-string v0, "init: call nativeSetup"

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/Libjingle;->log(Ljava/lang/String;)V

    .line 169
    const-string v0, "vclib:videoLogging"

    .line 170
    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/Libjingle;->getLibjingleLogLevel(Ljava/lang/String;)I

    move-result v0

    .line 169
    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/Libjingle;->getCorrectedLibjingleMediaLogLevel(I)I

    move-result v7

    .line 171
    const-string v0, "vclib:audioLogging"

    .line 172
    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/Libjingle;->getLibjingleLogLevel(Ljava/lang/String;)I

    move-result v0

    .line 171
    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/Libjingle;->getCorrectedLibjingleMediaLogLevel(I)I

    move-result v8

    .line 174
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    .line 175
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/Libjingle;->mContext:Landroid/content/Context;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v6, p5

    move v9, p4

    invoke-direct/range {v0 .. v9}, Lcom/google/android/libraries/hangouts/video/Libjingle;->nativeSetup(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZ)V

    .line 178
    const-string v0, "init: nativeSetup returned"

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/Libjingle;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public initializeRenderer()V
    .locals 2

    .prologue
    .line 375
    const-string v0, "vclib"

    const-string v1, "initializeRenderer"

    invoke-static {v0, v1}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/Libjingle;->nativeInitializeRenderer()V

    .line 377
    return-void
.end method

.method public initiateCheckConnectivity()V
    .locals 0

    .prologue
    .line 391
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/Libjingle;->nativeCheckConnectivity()V

    .line 392
    return-void
.end method

.method public initiateHangoutCall(Lcom/google/android/libraries/hangouts/video/CallOptions;Lcom/google/android/libraries/hangouts/video/HangoutRequest;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 289
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/CallOptions;->hasVideo()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    or-int/lit8 v2, v0, 0x0

    .line 291
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/CallOptions;->userIsChild()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    :goto_1
    or-int/2addr v2, v0

    .line 292
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/CallOptions;->allowOnAir()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    :goto_2
    or-int/2addr v0, v2

    .line 293
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/CallOptions;->abuseTosAccepted()Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x8

    :cond_0
    or-int v7, v0, v1

    .line 296
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/CallOptions;->getSessionId()Ljava/lang/String;

    move-result-object v1

    .line 297
    invoke-virtual {p2}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getExternalKeyType()Ljava/lang/String;

    move-result-object v2

    .line 298
    invoke-virtual {p2}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getExternalKey()Ljava/lang/String;

    move-result-object v3

    .line 299
    invoke-virtual {p2}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getDomain()Ljava/lang/String;

    move-result-object v4

    .line 300
    invoke-virtual {p2}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getHangoutId()Ljava/lang/String;

    move-result-object v5

    .line 301
    invoke-virtual {p2}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getInviteeNick()Ljava/lang/String;

    move-result-object v6

    .line 303
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/CallOptions;->getCompressedLogFile()Ljava/lang/String;

    move-result-object v8

    move-object v0, p0

    .line 295
    invoke-direct/range {v0 .. v8}, Lcom/google/android/libraries/hangouts/video/Libjingle;->nativeCallHangout(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 304
    return-void

    :cond_1
    move v0, v1

    .line 289
    goto :goto_0

    :cond_2
    move v0, v1

    .line 291
    goto :goto_1

    :cond_3
    move v0, v1

    .line 292
    goto :goto_2
.end method

.method public invitePstn(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 363
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/hangouts/video/Libjingle;->nativeInvitePstn(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 364
    return-void
.end method

.method public inviteUsers(Z[Ljava/lang/String;[Ljava/lang/String;IZZLjava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 356
    if-eqz p4, :cond_0

    if-ne p4, v0, :cond_1

    :cond_0
    :goto_0
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 358
    invoke-direct/range {p0 .. p7}, Lcom/google/android/libraries/hangouts/video/Libjingle;->nativeInviteUsers(Z[Ljava/lang/String;[Ljava/lang/String;IZZLjava/lang/String;)V

    .line 360
    return-void

    .line 356
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 234
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/Libjingle;->mInitialized:Z

    return v0
.end method

.method public isSecure(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 245
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/Libjingle;->nativeIsSecure(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isVideo(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 238
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/Libjingle;->nativeIsVideo(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public publishAudioMuteState(Z)V
    .locals 0

    .prologue
    .line 311
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/Libjingle;->nativePublishAudioMuteState(Z)V

    .line 312
    return-void
.end method

.method public publishVideoMuteState(Z)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/Libjingle;->nativePublishVideoMuteState(Z)V

    .line 320
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    .line 222
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/Libjingle;->mInitialized:Z

    if-nez v0, :cond_0

    .line 223
    const-string v0, "release: already released"

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/Libjingle;->log(Ljava/lang/String;)V

    .line 231
    :goto_0
    return-void

    .line 227
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/Libjingle;->mInitialized:Z

    .line 229
    const-string v0, "Release: call nativeRelease"

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/Libjingle;->log(Ljava/lang/String;)V

    .line 230
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/Libjingle;->nativeRelease()V

    goto :goto_0
.end method

.method public remoteMute(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 315
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/Libjingle;->nativeRemoteMute(Ljava/lang/String;)V

    .line 316
    return-void
.end method

.method public requestVideoViews([Lcom/google/android/libraries/hangouts/video/VideoViewRequest;)V
    .locals 0

    .prologue
    .line 307
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/Libjingle;->nativeRequestVideoViews([Lcom/google/android/libraries/hangouts/video/VideoViewRequest;)V

    .line 308
    return-void
.end method

.method public sendDtmf(CILjava/lang/String;)V
    .locals 0

    .prologue
    .line 367
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/hangouts/video/Libjingle;->nativeSendDtmf(CILjava/lang/String;)V

    .line 368
    return-void
.end method

.method public sendStanza(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 349
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/Libjingle;->nativeSendStanza(Ljava/lang/String;)V

    .line 350
    return-void
.end method

.method public setPlayoutSampleRate(I)V
    .locals 2

    .prologue
    .line 273
    sget-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->AUDIO_PLAYBACK_SAMPLING_RATE:Ljava/lang/String;

    .line 274
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 273
    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/hangouts/video/Libjingle;->nativeSetGServicesOverride(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    return-void
.end method

.method public setRecordingDevice(I)V
    .locals 2

    .prologue
    .line 278
    sget-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->AUDIO_RECORDING_DEVICE:Ljava/lang/String;

    .line 279
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 278
    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/hangouts/video/Libjingle;->nativeSetGServicesOverride(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    return-void
.end method

.method public setRecordingSampleRate(I)V
    .locals 2

    .prologue
    .line 268
    sget-object v0, Lcom/google/android/libraries/hangouts/video/VideoChat;->AUDIO_RECORDING_SAMPLING_RATE:Ljava/lang/String;

    .line 269
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 268
    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/hangouts/video/Libjingle;->nativeSetGServicesOverride(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    return-void
.end method

.method public setSelfViewFrameParameters(ZIII)V
    .locals 0

    .prologue
    .line 371
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/libraries/hangouts/video/Libjingle;->nativeSetSelfViewFrameParameters(ZIII)V

    .line 372
    return-void
.end method

.method public setUseWebRTCAcousticEchoCanceler(Z)V
    .locals 2

    .prologue
    .line 260
    const-string v1, "ENABLE_ECHO_CANCELLATION"

    if-eqz p1, :cond_0

    const-string v0, "true"

    :goto_0
    invoke-direct {p0, v1, v0}, Lcom/google/android/libraries/hangouts/video/Libjingle;->nativeSetGServicesOverride(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    return-void

    .line 260
    :cond_0
    const-string v0, "false"

    goto :goto_0
.end method

.method public setUseWebRTCAutomaticGainControl(Z)V
    .locals 2

    .prologue
    .line 264
    const-string v1, "ENABLE_AUTO_GAIN_CONTROL"

    if-eqz p1, :cond_0

    const-string v0, "true"

    :goto_0
    invoke-direct {p0, v1, v0}, Lcom/google/android/libraries/hangouts/video/Libjingle;->nativeSetGServicesOverride(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    return-void

    .line 264
    :cond_0
    const-string v0, "false"

    goto :goto_0
.end method

.method public setUseWebRTCNoiseSuppressor(Z)V
    .locals 2

    .prologue
    .line 256
    const-string v1, "ENABLE_NOISE_SUPPRESSION"

    if-eqz p1, :cond_0

    const-string v0, "true"

    :goto_0
    invoke-direct {p0, v1, v0}, Lcom/google/android/libraries/hangouts/video/Libjingle;->nativeSetGServicesOverride(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    return-void

    .line 256
    :cond_0
    const-string v0, "false"

    goto :goto_0
.end method

.method public signIn(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 330
    invoke-static {p1}, Lcwz;->b(Ljava/lang/Object;)V

    .line 331
    invoke-direct/range {p0 .. p5}, Lcom/google/android/libraries/hangouts/video/Libjingle;->nativeSignIn(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    return-void
.end method

.method public terminateCall(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 345
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/hangouts/video/Libjingle;->nativeEndCall(Ljava/lang/String;Z)V

    .line 346
    return-void
.end method

.method public final native unbindRenderer(I)V
.end method
