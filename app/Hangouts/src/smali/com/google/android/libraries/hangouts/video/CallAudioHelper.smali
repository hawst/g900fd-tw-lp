.class Lcom/google/android/libraries/hangouts/video/CallAudioHelper;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x9
.end annotation


# static fields
.field private static final BLUETOOTH_SCO_TIMEOUT_MS:I = 0x1388

.field private static final TAG:Ljava/lang/String; = "vclib"


# instance fields
.field private mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

.field private final mAudioDevices:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/libraries/hangouts/video/AudioDevice;",
            ">;"
        }
    .end annotation
.end field

.field private final mAudioManager:Landroid/media/AudioManager;

.field private mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

.field private mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

.field private mBluetoothReceiver:Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;

.field private final mContext:Landroid/content/Context;

.field private mDefaultConnectedDevice:Lcom/google/android/libraries/hangouts/video/AudioDevice;

.field private final mHandler:Landroid/os/Handler;

.field private final mInitializationLock:Ljava/lang/Object;

.field private mIsInitialized:Z

.field private final mOnAudioStateChangedListener:Ljava/lang/Runnable;

.field private mPendingAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

.field private mSavedMuteState:Z

.field private mSavedSpeakerphoneState:Z

.field private final mWiredHeadsetReceiver:Landroid/content/BroadcastReceiver;

.field private final onBluetoothTimeoutRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mIsInitialized:Z

    .line 49
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mInitializationLock:Ljava/lang/Object;

    .line 58
    sget-object v0, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->SPEAKERPHONE_ON:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    .line 59
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDevices:Ljava/util/Set;

    .line 87
    new-instance v0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$1;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$1;-><init>(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->onBluetoothTimeoutRunnable:Ljava/lang/Runnable;

    .line 225
    new-instance v0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$2;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$2;-><init>(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mWiredHeadsetReceiver:Landroid/content/BroadcastReceiver;

    .line 279
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mContext:Landroid/content/Context;

    .line 280
    iput-object p2, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mHandler:Landroid/os/Handler;

    .line 281
    iput-object p3, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mOnAudioStateChangedListener:Ljava/lang/Runnable;

    .line 282
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioManager:Landroid/media/AudioManager;

    .line 285
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->hasEarpiece()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 286
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDevices:Ljava/util/Set;

    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDevice;->EARPIECE:Lcom/google/android/libraries/hangouts/video/AudioDevice;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 288
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDevices:Ljava/util/Set;

    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDevice;->SPEAKERPHONE:Lcom/google/android/libraries/hangouts/video/AudioDevice;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 289
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->onBluetoothTimeout()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->usePendingAudioDeviceState()V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)Z
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->hasEarpiece()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)Lcom/google/android/libraries/hangouts/video/AudioDevice;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mDefaultConnectedDevice:Lcom/google/android/libraries/hangouts/video/AudioDevice;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;Landroid/bluetooth/BluetoothHeadset;)Landroid/bluetooth/BluetoothHeadset;
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)Lcom/google/android/libraries/hangouts/video/AudioDeviceState;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;Lcom/google/android/libraries/hangouts/video/AudioDeviceState;)Lcom/google/android/libraries/hangouts/video/AudioDeviceState;
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->startBluetoothSco()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->stopBluetoothSco()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)Landroid/bluetooth/BluetoothDevice;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    return-object p1
.end method

.method static synthetic access$700(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDevices:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->reportUpdate()V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->cancelBluetoothTimer()V

    return-void
.end method

.method private cancelBluetoothTimer()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 647
    const-string v0, "Canceling bluetooth timer"

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->log(Ljava/lang/String;)V

    .line 648
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->onBluetoothTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 649
    return-void
.end method

.method private deinitBluetoothAudio()V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 409
    const-string v0, "deinitBluetoothAudio"

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->log(Ljava/lang/String;)V

    .line 411
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_0

    .line 413
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->stopBluetoothSco()V

    .line 415
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->cancelBluetoothTimer()V

    .line 417
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothReceiver:Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 418
    iput-object v3, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothReceiver:Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;

    .line 420
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    invoke-virtual {v0, v1, v2}, Landroid/bluetooth/BluetoothAdapter;->closeProfileProxy(ILandroid/bluetooth/BluetoothProfile;)V

    .line 421
    iput-object v3, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    .line 422
    iput-object v3, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    .line 423
    iput-object v3, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 425
    :cond_0
    return-void
.end method

.method private deinitWiredHeadsetAudio()V
    .locals 2

    .prologue
    .line 437
    const-string v0, "deinitWiredHeadsetAudio"

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->log(Ljava/lang/String;)V

    .line 438
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mWiredHeadsetReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 439
    return-void
.end method

.method private hasEarpiece()Z
    .locals 2

    .prologue
    .line 750
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mContext:Landroid/content/Context;

    const-string v1, "phone"

    .line 751
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 752
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private initBluetoothAudio()Z
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 376
    const-string v2, "initBluetoothAudio"

    invoke-direct {p0, v2}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->log(Ljava/lang/String;)V

    .line 379
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 380
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez v2, :cond_1

    .line 404
    :cond_0
    :goto_0
    return v0

    .line 384
    :cond_1
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mContext:Landroid/content/Context;

    new-instance v4, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothServiceListener;

    invoke-direct {v4, p0, v5}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothServiceListener;-><init>(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;Lcom/google/android/libraries/hangouts/video/CallAudioHelper$1;)V

    invoke-virtual {v2, v3, v4, v1}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    .line 387
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 389
    const-string v3, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 391
    const-string v3, "android.bluetooth.headset.profile.action.AUDIO_STATE_CHANGED"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 392
    new-instance v3, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;

    invoke-direct {v3, p0, v5}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;-><init>(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;Lcom/google/android/libraries/hangouts/video/CallAudioHelper$1;)V

    iput-object v3, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothReceiver:Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;

    .line 393
    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothReceiver:Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothReceiver;

    invoke-virtual {v3, v4, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 395
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 396
    invoke-virtual {v3, v1}, Landroid/bluetooth/BluetoothAdapter;->getProfileConnectionState(I)I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 397
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDevices:Ljava/util/Set;

    sget-object v2, Lcom/google/android/libraries/hangouts/video/AudioDevice;->BLUETOOTH_HEADSET:Lcom/google/android/libraries/hangouts/video/AudioDevice;

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 398
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->startBluetoothTimer()V

    .line 399
    sget-object v0, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->BLUETOOTH_TURNING_ON:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    .line 400
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->reportUpdate()V

    move v0, v1

    .line 402
    goto :goto_0
.end method

.method private initWiredHeadsetAudio()V
    .locals 3

    .prologue
    .line 428
    const-string v0, "initWiredHeadsetAudio"

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->log(Ljava/lang/String;)V

    .line 429
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 430
    const-string v1, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 431
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mWiredHeadsetReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 434
    return-void
.end method

.method private log(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 756
    const-string v0, "vclib"

    invoke-static {v0, p1}, Lcxc;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 757
    return-void
.end method

.method private loge(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 760
    const-string v0, "vclib"

    invoke-static {v0, p1}, Lcxc;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 761
    return-void
.end method

.method private onBluetoothTimeout()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 653
    const-string v0, "Starting or stopping Bluetooth timed out"

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->log(Ljava/lang/String;)V

    .line 656
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->cancelBluetoothTimer()V

    .line 658
    sget-object v0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$3;->$SwitchMap$com$google$android$libraries$hangouts$video$AudioDeviceState:[I

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 683
    :goto_0
    return-void

    .line 661
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    .line 662
    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothHeadset;->isAudioConnected(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 663
    const-string v0, "We thought BT had timed out, but it\'s actually on; updating state."

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->loge(Ljava/lang/String;)V

    .line 664
    sget-object v0, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->BLUETOOTH_ON:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    .line 670
    :goto_1
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->reportUpdate()V

    goto :goto_0

    .line 667
    :cond_0
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->stopBluetoothSco()V

    .line 668
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->usePendingAudioDeviceState()V

    goto :goto_1

    .line 674
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    .line 675
    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothHeadset;->isAudioConnected(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 676
    :cond_1
    const-string v0, "We thought BT had timed out, but it\'s actually off; updating state."

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->loge(Ljava/lang/String;)V

    .line 677
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->usePendingAudioDeviceState()V

    .line 682
    :goto_2
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->reportUpdate()V

    goto :goto_0

    .line 680
    :cond_2
    sget-object v0, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->BLUETOOTH_ON:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    goto :goto_2

    .line 658
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private reportUpdate()V
    .locals 2

    .prologue
    .line 479
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "reportUpdate: state="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", devices="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDevices:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->log(Ljava/lang/String;)V

    .line 480
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mOnAudioStateChangedListener:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 481
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mOnAudioStateChangedListener:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 483
    :cond_0
    return-void
.end method

.method private setSpeakerphoneOn(Z)V
    .locals 2

    .prologue
    .line 454
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setSpeakerphoneOn("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "), wasOn="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->isSpeakerphoneOn()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->log(Ljava/lang/String;)V

    .line 455
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    .line 456
    return-void
.end method

.method private startBluetoothSco()V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 502
    const-string v0, "startBluetoothSco"

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->log(Ljava/lang/String;)V

    .line 503
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->cancelBluetoothTimer()V

    .line 505
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    if-nez v0, :cond_1

    .line 562
    :cond_0
    :goto_0
    return-void

    .line 510
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    if-nez v0, :cond_2

    .line 512
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothHeadset;->getConnectedDevices()Ljava/util/List;

    move-result-object v0

    .line 513
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 514
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    .line 518
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    if-eqz v0, :cond_0

    .line 523
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_4

    .line 524
    const-string v0, "startBluetoothSco : JBMR2+ Workaround"

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->log(Ljava/lang/String;)V

    .line 526
    const/4 v0, 0x1

    :try_start_0
    new-array v0, v0, [Ljava/lang/Class;

    .line 527
    const/4 v1, 0x0

    const-class v2, Landroid/bluetooth/BluetoothDevice;

    aput-object v2, v0, v1

    .line 528
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 529
    const-string v2, "startScoUsingVirtualVoiceCall"

    .line 530
    invoke-virtual {v1, v2, v0}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 533
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 534
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 537
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 538
    sget-object v0, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->BLUETOOTH_TURNING_ON:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3

    .line 549
    :cond_3
    :goto_1
    const-string v0, "done"

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->log(Ljava/lang/String;)V

    .line 557
    :goto_2
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->BLUETOOTH_TURNING_ON:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    if-ne v0, v1, :cond_0

    .line 559
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->reportUpdate()V

    .line 560
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->startBluetoothTimer()V

    goto :goto_0

    .line 540
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 542
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 544
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 546
    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 551
    :cond_4
    const-string v0, "startBluetoothSco : pre-JBMR2"

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->log(Ljava/lang/String;)V

    .line 553
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->startBluetoothSco()V

    .line 554
    sget-object v0, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->BLUETOOTH_TURNING_ON:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    goto :goto_2
.end method

.method private startBluetoothTimer()V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 641
    const-string v0, "Starting bluetooth timer"

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->log(Ljava/lang/String;)V

    .line 642
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->onBluetoothTimeoutRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 643
    return-void
.end method

.method private stopBluetoothSco()V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 585
    const-string v0, "stopBluetoothSco"

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->log(Ljava/lang/String;)V

    .line 586
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->cancelBluetoothTimer()V

    .line 588
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    if-nez v0, :cond_1

    .line 637
    :cond_0
    :goto_0
    return-void

    .line 593
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    if-eqz v0, :cond_0

    .line 598
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_3

    .line 601
    :try_start_0
    const-string v0, "stopBluetoothSco : JBMR2+ Workaround"

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->log(Ljava/lang/String;)V

    .line 602
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    .line 603
    const/4 v1, 0x0

    const-class v2, Landroid/bluetooth/BluetoothDevice;

    aput-object v2, v0, v1

    .line 604
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 605
    const-string v2, "stopScoUsingVirtualVoiceCall"

    .line 606
    invoke-virtual {v1, v2, v0}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 609
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 610
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 612
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 613
    sget-object v0, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->BLUETOOTH_TURNING_OFF:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3

    .line 624
    :cond_2
    :goto_1
    const-string v0, "done"

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->log(Ljava/lang/String;)V

    .line 632
    :goto_2
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->BLUETOOTH_TURNING_OFF:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    if-ne v0, v1, :cond_0

    .line 634
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->reportUpdate()V

    .line 635
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->startBluetoothTimer()V

    goto :goto_0

    .line 615
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 617
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 619
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 621
    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 626
    :cond_3
    const-string v0, "stopBluetoothSco : pre-JBMR2"

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->log(Ljava/lang/String;)V

    .line 628
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->stopBluetoothSco()V

    .line 629
    sget-object v0, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->BLUETOOTH_TURNING_OFF:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    goto :goto_2
.end method

.method private turnOffBluetooth()Z
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 573
    const-string v0, "turnOffBluetooth"

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->log(Ljava/lang/String;)V

    .line 574
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->BLUETOOTH_ON:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->BLUETOOTH_TURNING_ON:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    if-eq v0, v1, :cond_0

    .line 576
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "turnOffBluetooth: state is already "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", cannot turn off"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->log(Ljava/lang/String;)V

    .line 577
    const/4 v0, 0x0

    .line 580
    :goto_0
    return v0

    .line 579
    :cond_0
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->stopBluetoothSco()V

    .line 580
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private turnOnBluetooth()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 491
    const-string v0, "turnOnBluetooth"

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->log(Ljava/lang/String;)V

    .line 492
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->BLUETOOTH_ON:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->BLUETOOTH_TURNING_ON:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    if-ne v0, v1, :cond_1

    .line 494
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "turnOnBluetooth: state is already "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", cannot turn on"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->log(Ljava/lang/String;)V

    .line 498
    :goto_0
    return-void

    .line 497
    :cond_1
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->startBluetoothSco()V

    goto :goto_0
.end method

.method private usePendingAudioDeviceState()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 693
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mPendingAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mPendingAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->WIRED_HEADSET_ON:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDevices:Ljava/util/Set;

    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDevice;->WIRED_HEADSET:Lcom/google/android/libraries/hangouts/video/AudioDevice;

    .line 695
    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 696
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "usePendingAudioDeviceState: there\'s no pending state or it was WH, but has beenunplugged; using default device. Pending state was "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mPendingAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->loge(Ljava/lang/String;)V

    .line 699
    iput-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mPendingAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    .line 700
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mDefaultConnectedDevice:Lcom/google/android/libraries/hangouts/video/AudioDevice;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->setAudioDevice(Lcom/google/android/libraries/hangouts/video/AudioDevice;)V

    .line 708
    :goto_0
    return-void

    .line 704
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "usePendingAudioDeviceState: using "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mPendingAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->log(Ljava/lang/String;)V

    .line 705
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mPendingAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    .line 706
    iput-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mPendingAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    .line 707
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->SPEAKERPHONE_ON:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->setSpeakerphoneOn(Z)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public deinitAudio()V
    .locals 3

    .prologue
    .line 345
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mInitializationLock:Ljava/lang/Object;

    monitor-enter v1

    .line 346
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mIsInitialized:Z

    if-nez v0, :cond_0

    .line 347
    monitor-exit v1

    .line 366
    :goto_0
    return-void

    .line 350
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v0, v2, :cond_1

    .line 351
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->deinitBluetoothAudio()V

    .line 353
    :cond_1
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->deinitWiredHeadsetAudio()V

    .line 356
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioManager:Landroid/media/AudioManager;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->setMode(I)V

    .line 357
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioManager:Landroid/media/AudioManager;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 360
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "deinitAudio: set mute back to "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mSavedMuteState:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", speakerphone back to "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mSavedSpeakerphoneState:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->log(Ljava/lang/String;)V

    .line 362
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mSavedMuteState:Z

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->setMute(Z)V

    .line 363
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mSavedSpeakerphoneState:Z

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->setSpeakerphoneOn(Z)V

    .line 365
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mIsInitialized:Z

    .line 366
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public getAudioDeviceState()Lcom/google/android/libraries/hangouts/video/AudioDeviceState;
    .locals 1

    .prologue
    .line 466
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    return-object v0
.end method

.method public getAudioDevices()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/libraries/hangouts/video/AudioDevice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 471
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDevices:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public initAudio(Z)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 297
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mInitializationLock:Ljava/lang/Object;

    monitor-enter v2

    .line 298
    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mIsInitialized:Z

    if-eqz v1, :cond_0

    .line 299
    monitor-exit v2

    .line 337
    :goto_0
    return-void

    .line 303
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->isMute()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mSavedMuteState:Z

    .line 304
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->isSpeakerphoneOn()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mSavedSpeakerphoneState:Z

    .line 305
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "initAudio: Saved mute = "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mSavedMuteState:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", speakerphone = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v3, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mSavedSpeakerphoneState:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->log(Ljava/lang/String;)V

    .line 310
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioManager:Landroid/media/AudioManager;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x2

    invoke-virtual {v1, v3, v4, v5}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 315
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioManager:Landroid/media/AudioManager;

    const/4 v3, 0x3

    invoke-virtual {v1, v3}, Landroid/media/AudioManager;->setMode(I)V

    .line 317
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioManager:Landroid/media/AudioManager;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/media/AudioManager;->setMicrophoneMute(Z)V

    .line 318
    const/4 v1, 0x0

    invoke-static {v1}, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->setMicrophoneMute(Z)V

    .line 320
    if-nez p1, :cond_1

    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->hasEarpiece()Z

    move-result v1

    if-nez v1, :cond_4

    :cond_1
    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDevice;->SPEAKERPHONE:Lcom/google/android/libraries/hangouts/video/AudioDevice;

    :goto_1
    iput-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mDefaultConnectedDevice:Lcom/google/android/libraries/hangouts/video/AudioDevice;

    .line 322
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->initWiredHeadsetAudio()V

    .line 326
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v1, v3, :cond_2

    .line 327
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->initBluetoothAudio()Z

    move-result v0

    .line 330
    :cond_2
    if-nez v0, :cond_3

    .line 333
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mDefaultConnectedDevice:Lcom/google/android/libraries/hangouts/video/AudioDevice;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->setAudioDevice(Lcom/google/android/libraries/hangouts/video/AudioDevice;)V

    .line 336
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mIsInitialized:Z

    .line 337
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 320
    :cond_4
    :try_start_1
    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDevice;->EARPIECE:Lcom/google/android/libraries/hangouts/video/AudioDevice;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public isMute()Z
    .locals 1

    .prologue
    .line 459
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMicrophoneMute()Z

    move-result v0

    return v0
.end method

.method public setAudioDevice(Lcom/google/android/libraries/hangouts/video/AudioDevice;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 711
    sget-object v0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$3;->$SwitchMap$com$google$android$libraries$hangouts$video$AudioDevice:[I

    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/AudioDevice;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 742
    :goto_0
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->reportUpdate()V

    .line 743
    return-void

    .line 714
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mPendingAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    .line 715
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->turnOnBluetooth()V

    goto :goto_0

    .line 718
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->turnOffBluetooth()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 719
    sget-object v0, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->SPEAKERPHONE_ON:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mPendingAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    .line 723
    :goto_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->setSpeakerphoneOn(Z)V

    goto :goto_0

    .line 721
    :cond_0
    sget-object v0, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->SPEAKERPHONE_ON:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    goto :goto_1

    .line 726
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->turnOffBluetooth()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 727
    sget-object v0, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->WIRED_HEADSET_ON:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mPendingAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    .line 731
    :goto_2
    invoke-direct {p0, v2}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->setSpeakerphoneOn(Z)V

    goto :goto_0

    .line 729
    :cond_1
    sget-object v0, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->WIRED_HEADSET_ON:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    goto :goto_2

    .line 734
    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->turnOffBluetooth()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 735
    sget-object v0, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->EARPIECE_ON:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mPendingAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    .line 739
    :goto_3
    invoke-direct {p0, v2}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->setSpeakerphoneOn(Z)V

    goto :goto_0

    .line 737
    :cond_2
    sget-object v0, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->EARPIECE_ON:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    goto :goto_3

    .line 711
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setMute(Z)V
    .locals 2

    .prologue
    .line 442
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setMute: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", wasMute="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->isMute()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->log(Ljava/lang/String;)V

    .line 445
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->setMicrophoneMute(Z)V

    .line 448
    invoke-static {p1}, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->setMicrophoneMute(Z)V

    .line 450
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->reportUpdate()V

    .line 451
    return-void
.end method
