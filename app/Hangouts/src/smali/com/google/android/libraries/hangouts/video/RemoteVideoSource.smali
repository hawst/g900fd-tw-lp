.class public Lcom/google/android/libraries/hangouts/video/RemoteVideoSource;
.super Lcom/google/android/libraries/hangouts/video/VideoSource;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/hangouts/video/Renderer$RendererThreadCallback;


# instance fields
.field private final endpoint:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

.field private final frameOutputData:Lcom/google/android/libraries/hangouts/video/RemoteRenderer$RendererFrameOutputData;

.field private final gl:Lcom/google/android/libraries/hangouts/video/GlManager;

.field private initialized:Z

.field private isDirty:Z

.field private final renderer:Lcom/google/android/libraries/hangouts/video/Renderer;

.field private textureName:I

.field private videoHeight:I

.field private videoWidth:I


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/hangouts/video/RendererManager;Lcom/google/android/libraries/hangouts/video/GlManager;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 23
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/VideoSource;-><init>()V

    .line 13
    new-instance v0, Lcom/google/android/libraries/hangouts/video/RemoteRenderer$RendererFrameOutputData;

    invoke-direct {v0}, Lcom/google/android/libraries/hangouts/video/RemoteRenderer$RendererFrameOutputData;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/RemoteVideoSource;->frameOutputData:Lcom/google/android/libraries/hangouts/video/RemoteRenderer$RendererFrameOutputData;

    .line 18
    iput-boolean v1, p0, Lcom/google/android/libraries/hangouts/video/RemoteVideoSource;->isDirty:Z

    .line 19
    iput-boolean v1, p0, Lcom/google/android/libraries/hangouts/video/RemoteVideoSource;->initialized:Z

    .line 24
    invoke-virtual {p1, p0}, Lcom/google/android/libraries/hangouts/video/RendererManager;->createRemoteRenderer(Lcom/google/android/libraries/hangouts/video/Renderer$RendererThreadCallback;)Lcom/google/android/libraries/hangouts/video/RemoteRenderer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/RemoteVideoSource;->renderer:Lcom/google/android/libraries/hangouts/video/Renderer;

    .line 25
    iput-object p2, p0, Lcom/google/android/libraries/hangouts/video/RemoteVideoSource;->gl:Lcom/google/android/libraries/hangouts/video/GlManager;

    .line 26
    iput-object p3, p0, Lcom/google/android/libraries/hangouts/video/RemoteVideoSource;->endpoint:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 27
    return-void
.end method


# virtual methods
.method getHeight()I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/RemoteVideoSource;->videoHeight:I

    return v0
.end method

.method getTextureName()I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/RemoteVideoSource;->textureName:I

    return v0
.end method

.method getWidth()I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/RemoteVideoSource;->videoWidth:I

    return v0
.end method

.method isDirty()Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/RemoteVideoSource;->isDirty:Z

    return v0
.end method

.method isExternalTexture()Z
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    return v0
.end method

.method public maybeAddVideoViewRequest(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/hangouts/video/VideoViewRequest;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 90
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->getIncomingSecondaryVideoSpec()Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v5

    .line 92
    new-instance v0, Lcom/google/android/libraries/hangouts/video/VideoViewRequest;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/RemoteVideoSource;->endpoint:Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;

    .line 93
    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getVideoSsrcs()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/RemoteVideoSource;->renderer:Lcom/google/android/libraries/hangouts/video/Renderer;

    .line 95
    invoke-virtual {v5}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->getSize()Lcom/google/android/libraries/hangouts/video/Size;

    move-result-object v3

    iget v3, v3, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    .line 96
    invoke-virtual {v5}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->getSize()Lcom/google/android/libraries/hangouts/video/Size;

    move-result-object v4

    iget v4, v4, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    .line 97
    invoke-virtual {v5}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->getFrameRate()I

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/hangouts/video/VideoViewRequest;-><init>(ILcom/google/android/libraries/hangouts/video/Renderer;III)V

    .line 92
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    return-void
.end method

.method public onOutputTextureNameChanged(I)V
    .locals 0

    .prologue
    .line 107
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/RemoteVideoSource;->textureName:I

    .line 108
    return-void
.end method

.method processFrame()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 36
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/RemoteVideoSource;->initialized:Z

    if-nez v0, :cond_2

    .line 37
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/RemoteVideoSource;->renderer:Lcom/google/android/libraries/hangouts/video/Renderer;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/Renderer;->initializeGLContext()V

    .line 38
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/RemoteVideoSource;->renderer:Lcom/google/android/libraries/hangouts/video/Renderer;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/Renderer;->getOutputTextureName()I

    move-result v0

    if-nez v0, :cond_1

    .line 51
    :cond_0
    :goto_0
    return-void

    .line 41
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/RemoteVideoSource;->renderer:Lcom/google/android/libraries/hangouts/video/Renderer;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/Renderer;->getOutputTextureName()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/RemoteVideoSource;->textureName:I

    .line 42
    iput-boolean v3, p0, Lcom/google/android/libraries/hangouts/video/RemoteVideoSource;->initialized:Z

    .line 44
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/RemoteVideoSource;->renderer:Lcom/google/android/libraries/hangouts/video/Renderer;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/RemoteVideoSource;->frameOutputData:Lcom/google/android/libraries/hangouts/video/RemoteRenderer$RendererFrameOutputData;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/hangouts/video/Renderer;->drawTexture(Lcom/google/android/libraries/hangouts/video/Renderer$DrawInputParams;Lcom/google/android/libraries/hangouts/video/Renderer$DrawOutputParams;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    iput-boolean v3, p0, Lcom/google/android/libraries/hangouts/video/RemoteVideoSource;->isDirty:Z

    .line 46
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/RemoteVideoSource;->frameOutputData:Lcom/google/android/libraries/hangouts/video/RemoteRenderer$RendererFrameOutputData;

    iget-boolean v0, v0, Lcom/google/android/libraries/hangouts/video/RemoteRenderer$RendererFrameOutputData;->frameSizeChanged:Z

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/RemoteVideoSource;->frameOutputData:Lcom/google/android/libraries/hangouts/video/RemoteRenderer$RendererFrameOutputData;

    iget v0, v0, Lcom/google/android/libraries/hangouts/video/RemoteRenderer$RendererFrameOutputData;->frameWidth:I

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/RemoteVideoSource;->videoWidth:I

    .line 48
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/RemoteVideoSource;->frameOutputData:Lcom/google/android/libraries/hangouts/video/RemoteRenderer$RendererFrameOutputData;

    iget v0, v0, Lcom/google/android/libraries/hangouts/video/RemoteRenderer$RendererFrameOutputData;->frameHeight:I

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/RemoteVideoSource;->videoHeight:I

    goto :goto_0
.end method

.method public queueEventForGLThread(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/RemoteVideoSource;->gl:Lcom/google/android/libraries/hangouts/video/GlManager;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/GlManager;->queueEvent(Ljava/lang/Runnable;)V

    .line 103
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/RemoteVideoSource;->renderer:Lcom/google/android/libraries/hangouts/video/Renderer;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/Renderer;->release()V

    .line 32
    return-void
.end method

.method shouldFlipTexture()Z
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    return v0
.end method
