.class public Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;
.super Lcom/google/android/libraries/hangouts/video/Stats;
.source "PG"


# instance fields
.field public final bytesRcvd:I

.field public final firsSent:I

.field public final fractionLost:F

.field public final frameHeight:I

.field public final frameRateDecoded:I

.field public final frameRateRcvd:I

.field public final frameRateRenderInput:F

.field public final frameRateRenderOutput:F

.field public final frameWidth:I

.field public final nacksSent:I

.field public final packetsLost:I

.field public final packetsRcvd:I

.field public final ssrc:I


# direct methods
.method public constructor <init>(IIIIFIIIIIIFF)V
    .locals 0

    .prologue
    .line 363
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/Stats;-><init>()V

    .line 364
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->ssrc:I

    .line 365
    iput p2, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->bytesRcvd:I

    .line 366
    iput p3, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->packetsRcvd:I

    .line 367
    iput p4, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->packetsLost:I

    .line 368
    iput p5, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->fractionLost:F

    .line 369
    iput p6, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->firsSent:I

    .line 370
    iput p7, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->nacksSent:I

    .line 371
    iput p8, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->frameWidth:I

    .line 372
    iput p9, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->frameHeight:I

    .line 373
    iput p10, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->frameRateRcvd:I

    .line 374
    iput p11, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->frameRateDecoded:I

    .line 375
    iput p12, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->frameRateRenderInput:F

    .line 376
    iput p13, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->frameRateRenderOutput:F

    .line 377
    return-void
.end method

.method static printLegend(Ljava/io/PrintWriter;)V
    .locals 1

    .prologue
    .line 403
    const-string v0, "  VideoReceiver -- ssrc, rcvd, lost, firs, size, rcvd, dec, rendIn, rendOut"

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 405
    return-void
.end method


# virtual methods
.method public addTo(Ldoe;)V
    .locals 4

    .prologue
    .line 381
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/Stats;->createEmptyMediaProto(I)Ldoh;

    move-result-object v1

    .line 382
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->l:Ljava/lang/Integer;

    .line 383
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->ssrc:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->m:Ljava/lang/Integer;

    .line 384
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->bytesRcvd:I

    int-to-long v2, v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v1, Ldoh;->j:Ljava/lang/Long;

    .line 385
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->packetsRcvd:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->k:Ljava/lang/Integer;

    .line 386
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->packetsLost:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->d:Ljava/lang/Integer;

    .line 387
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->fractionLost:F

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v0, v2

    float-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->c:Ljava/lang/Integer;

    .line 388
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->firsSent:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->o:Ljava/lang/Integer;

    .line 389
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->nacksSent:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->p:Ljava/lang/Integer;

    .line 390
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->frameWidth:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->P:Ljava/lang/Integer;

    .line 391
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->frameHeight:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->Q:Ljava/lang/Integer;

    .line 392
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->frameRateRcvd:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->r:Ljava/lang/Integer;

    .line 393
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->frameRateDecoded:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->s:Ljava/lang/Integer;

    .line 394
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->frameRateRenderInput:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v1, Ldoh;->t:Ljava/lang/Float;

    .line 395
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->frameRateRenderOutput:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v1, Ldoh;->v:Ljava/lang/Float;

    .line 397
    iget-object v0, p1, Ldoe;->c:[Ldoh;

    array-length v2, v0

    .line 398
    iget-object v0, p1, Ldoe;->c:[Ldoh;

    add-int/lit8 v3, v2, 0x1

    invoke-static {v0, v3}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldoh;

    iput-object v0, p1, Ldoe;->c:[Ldoh;

    .line 399
    iget-object v0, p1, Ldoe;->c:[Ldoh;

    aput-object v1, v0, v2

    .line 400
    return-void
.end method

.method public print(Ljava/io/PrintWriter;Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;)V
    .locals 2

    .prologue
    .line 409
    # getter for: Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;->frameRateReceiveList:Ljava/util/ArrayList;
    invoke-static {p2}, Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;->access$200(Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;)Ljava/util/ArrayList;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->frameRateRcvd:I

    int-to-float v1, v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 410
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, " -- VideoReceiver -- "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->ssrc:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->packetsRcvd:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->bytesRcvd:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "), "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->packetsLost:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->fractionLost:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "), "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->firsSent:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->nacksSent:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "), "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->frameWidth:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->frameHeight:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->frameRateRcvd:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->frameRateDecoded:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->frameRateRenderInput:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->frameRateRenderOutput:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 420
    return-void
.end method
