.class public Lcom/google/android/libraries/hangouts/video/ConnectivityReporter;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    return-void
.end method

.method static synthetic access$000([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    invoke-static {p0}, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private static varargs toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)[TT;"
        }
    .end annotation

    .prologue
    .line 147
    return-object p0
.end method

.method public static toLogEntry([Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;)Ldon;
    .locals 4

    .prologue
    .line 135
    if-eqz p0, :cond_0

    array-length v0, p0

    if-nez v0, :cond_1

    .line 136
    :cond_0
    const/4 v0, 0x0

    .line 143
    :goto_0
    return-object v0

    .line 138
    :cond_1
    new-instance v1, Ldon;

    invoke-direct {v1}, Ldon;-><init>()V

    .line 139
    array-length v0, p0

    new-array v0, v0, [Ldoo;

    iput-object v0, v1, Ldon;->b:[Ldoo;

    .line 140
    const/4 v0, 0x0

    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_2

    .line 141
    iget-object v2, v1, Ldon;->b:[Ldoo;

    aget-object v3, p0, v0

    invoke-virtual {v3}, Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;->toProto()Ldoo;

    move-result-object v3

    aput-object v3, v2, v0

    .line 140
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 143
    goto :goto_0
.end method
