.class Lcom/google/android/libraries/hangouts/video/SelfRendererGB$1;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/hardware/Camera$PreviewCallback;


# instance fields
.field final synthetic this$0:Lcom/google/android/libraries/hangouts/video/SelfRendererGB;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/hangouts/video/SelfRendererGB;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB$1;->this$0:Lcom/google/android/libraries/hangouts/video/SelfRendererGB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreviewFrame([BLandroid/hardware/Camera;)V
    .locals 4

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB$1;->this$0:Lcom/google/android/libraries/hangouts/video/SelfRendererGB;

    # getter for: Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mSelfFrameReadyLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->access$100(Lcom/google/android/libraries/hangouts/video/SelfRendererGB;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 63
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB$1;->this$0:Lcom/google/android/libraries/hangouts/video/SelfRendererGB;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    # setter for: Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mLastCameraFrameTimeNs:J
    invoke-static {v0, v2, v3}, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->access$202(Lcom/google/android/libraries/hangouts/video/SelfRendererGB;J)J

    .line 64
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB$1;->this$0:Lcom/google/android/libraries/hangouts/video/SelfRendererGB;

    # getter for: Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mPendingFrame:[B
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->access$300(Lcom/google/android/libraries/hangouts/video/SelfRendererGB;)[B

    move-result-object v0

    .line 65
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB$1;->this$0:Lcom/google/android/libraries/hangouts/video/SelfRendererGB;

    # setter for: Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mPendingFrame:[B
    invoke-static {v2, p1}, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->access$302(Lcom/google/android/libraries/hangouts/video/SelfRendererGB;[B)[B

    .line 66
    if-eqz v0, :cond_0

    .line 67
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/SelfRendererGB$1;->this$0:Lcom/google/android/libraries/hangouts/video/SelfRendererGB;

    # getter for: Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;
    invoke-static {v2}, Lcom/google/android/libraries/hangouts/video/SelfRendererGB;->access$400(Lcom/google/android/libraries/hangouts/video/SelfRendererGB;)Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->returnBuffer([B)V

    .line 69
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
