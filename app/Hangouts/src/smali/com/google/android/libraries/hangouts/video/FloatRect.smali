.class public Lcom/google/android/libraries/hangouts/video/FloatRect;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public bottom:F

.field public left:F

.field public right:F

.field public top:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/FloatRect;->left:F

    .line 24
    iput p2, p0, Lcom/google/android/libraries/hangouts/video/FloatRect;->top:F

    .line 25
    iput p3, p0, Lcom/google/android/libraries/hangouts/video/FloatRect;->right:F

    .line 26
    iput p4, p0, Lcom/google/android/libraries/hangouts/video/FloatRect;->bottom:F

    .line 27
    return-void
.end method

.method public constructor <init>(Lcom/google/android/libraries/hangouts/video/FloatRect;)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iget v0, p1, Lcom/google/android/libraries/hangouts/video/FloatRect;->left:F

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/FloatRect;->left:F

    .line 17
    iget v0, p1, Lcom/google/android/libraries/hangouts/video/FloatRect;->top:F

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/FloatRect;->top:F

    .line 18
    iget v0, p1, Lcom/google/android/libraries/hangouts/video/FloatRect;->right:F

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/FloatRect;->right:F

    .line 19
    iget v0, p1, Lcom/google/android/libraries/hangouts/video/FloatRect;->bottom:F

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/FloatRect;->bottom:F

    .line 20
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 46
    check-cast p1, Lcom/google/android/libraries/hangouts/video/FloatRect;

    .line 47
    if-eqz p1, :cond_0

    .line 48
    iget v1, p0, Lcom/google/android/libraries/hangouts/video/FloatRect;->left:F

    iget v2, p1, Lcom/google/android/libraries/hangouts/video/FloatRect;->left:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/FloatRect;->top:F

    iget v2, p1, Lcom/google/android/libraries/hangouts/video/FloatRect;->top:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/FloatRect;->right:F

    iget v2, p1, Lcom/google/android/libraries/hangouts/video/FloatRect;->right:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/FloatRect;->bottom:F

    iget v2, p1, Lcom/google/android/libraries/hangouts/video/FloatRect;->bottom:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 51
    :cond_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 31
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 32
    const-string v1, "FloatRect("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    iget v1, p0, Lcom/google/android/libraries/hangouts/video/FloatRect;->left:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 34
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    iget v1, p0, Lcom/google/android/libraries/hangouts/video/FloatRect;->top:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 36
    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    iget v1, p0, Lcom/google/android/libraries/hangouts/video/FloatRect;->right:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 38
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    iget v1, p0, Lcom/google/android/libraries/hangouts/video/FloatRect;->bottom:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 40
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
