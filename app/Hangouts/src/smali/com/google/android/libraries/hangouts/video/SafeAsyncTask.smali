.class public abstract Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Params:",
        "Ljava/lang/Object;",
        "Progress:",
        "Ljava/lang/Object;",
        "Result:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/os/AsyncTask",
        "<TParams;TProgress;TResult;>;"
    }
.end annotation


# static fields
.field private static final DEFAULT_MAX_EXECUTION_TIME_MILLIS:J = 0x2710L

.field public static MAX_NETWORK_OPERATION_TIME_MILLIS:J = 0x0L

.field public static final UNBOUNDED_TIME:J = 0x7fffffffffffffffL

.field private static volatile sExecutorForGB:Ljava/util/concurrent/ExecutorService;


# instance fields
.field private final mMaxExecutionTimeMillis:J

.field private mThreadPoolRequested:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    const-wide/32 v0, 0x9c40

    sput-wide v0, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;->MAX_NETWORK_OPERATION_TIME_MILLIS:J

    .line 36
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;->sExecutorForGB:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 42
    const-wide/16 v0, 0x2710

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;-><init>(J)V

    .line 43
    return-void
.end method

.method public constructor <init>(J)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 55
    invoke-static {}, Lcwz;->a()V

    .line 56
    iput-wide p1, p0, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;->mMaxExecutionTimeMillis:J

    .line 57
    return-void
.end method

.method public static executeOnThreadPool(Ljava/lang/Runnable;)V
    .locals 8

    .prologue
    .line 116
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 117
    sget-object v0, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-interface {v0, p0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 129
    :goto_0
    return-void

    .line 119
    :cond_0
    sget-object v0, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;->sExecutorForGB:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_1

    .line 121
    new-instance v0, Ljava/util/concurrent/ThreadPoolExecutor;

    const/4 v1, 0x5

    const/16 v2, 0x80

    const-wide/16 v3, 0x1

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v6, Ljava/util/concurrent/LinkedBlockingQueue;

    const/16 v7, 0xa

    invoke-direct {v6, v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    invoke-direct/range {v0 .. v6}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    sput-object v0, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;->sExecutorForGB:Ljava/util/concurrent/ExecutorService;

    .line 127
    :cond_1
    sget-object v0, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;->sExecutorForGB:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static setNetworkOperationTimeout(J)V
    .locals 2

    .prologue
    .line 102
    const-wide/16 v0, 0x2710

    add-long/2addr v0, p0

    sput-wide v0, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;->MAX_NETWORK_OPERATION_TIME_MILLIS:J

    .line 103
    return-void
.end method


# virtual methods
.method protected final varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TParams;)TResult;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 78
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;->mThreadPoolRequested:Z

    invoke-static {v0}, Lcwz;->a(Z)V

    .line 80
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 82
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;->doInBackgroundTimed([Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 84
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    sub-long/2addr v3, v0

    .line 85
    iget-wide v0, p0, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;->mMaxExecutionTimeMillis:J

    cmp-long v0, v3, v0

    if-lez v0, :cond_0

    .line 86
    const-string v1, "vclib"

    const-string v5, "%s TOOK TOO LONG! (%dms > %dms)"

    new-array v6, v6, [Ljava/lang/Object;

    .line 87
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->isAnonymousClass()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    aput-object v0, v6, v7

    .line 88
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v6, v8

    iget-wide v3, p0, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;->mMaxExecutionTimeMillis:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v6, v9

    .line 86
    invoke-static {v1, v5, v6}, Lcxc;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 90
    :cond_0
    return-object v2

    .line 87
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 84
    :catchall_0
    move-exception v2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    sub-long/2addr v3, v0

    .line 85
    iget-wide v0, p0, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;->mMaxExecutionTimeMillis:J

    cmp-long v0, v3, v0

    if-lez v0, :cond_2

    .line 86
    const-string v1, "vclib"

    const-string v5, "%s TOOK TOO LONG! (%dms > %dms)"

    new-array v6, v6, [Ljava/lang/Object;

    .line 87
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->isAnonymousClass()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    aput-object v0, v6, v7

    .line 88
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v6, v8

    iget-wide v3, p0, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;->mMaxExecutionTimeMillis:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v6, v9

    .line 86
    invoke-static {v1, v5, v6}, Lcxc;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 90
    :cond_2
    throw v2

    .line 87
    :cond_3
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public varargs abstract doInBackgroundTimed([Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TParams;)TResult;"
        }
    .end annotation
.end method

.method public final varargs executeOnThreadPool([Ljava/lang/Object;)Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TParams;)",
            "Lcom/google/android/libraries/hangouts/video/SafeAsyncTask",
            "<TParams;TProgress;TResult;>;"
        }
    .end annotation

    .prologue
    .line 60
    invoke-static {}, Lcwz;->a()V

    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;->mThreadPoolRequested:Z

    .line 62
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 63
    sget-object v0, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 69
    :goto_0
    return-object p0

    .line 67
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public onPostExecute(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TResult;)V"
        }
    .end annotation

    .prologue
    .line 97
    const-string v0, "Use SafeAsyncTask.executeOnThreadPool"

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    .line 98
    return-void
.end method
