.class public Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final apiaryUri:Ljava/lang/String;

.field public final checkConnectivity:I

.field public final clientVersion:Ljava/lang/String;

.field public final jidResourcePrefix:Ljava/lang/String;

.field public final maxIncomingVideoSpec:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

.field public final maxOutgoingSpecNoEffects:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

.field public final maxOutgoingSpecWithEffects:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

.field public final nativeCrashReceiver:Landroid/content/ComponentName;

.field public final outgoingVideoChatReceiver:Landroid/content/ComponentName;

.field public final overides:[[Ljava/lang/String;

.field public final rawLogDirectory:Ljava/lang/String;

.field public final useHardwareDecode:Z

.field public final useHardwareEncode:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/libraries/hangouts/video/VideoSpecification;Lcom/google/android/libraries/hangouts/video/VideoSpecification;Lcom/google/android/libraries/hangouts/video/VideoSpecification;Landroid/content/ComponentName;Landroid/content/ComponentName;[[Ljava/lang/String;ZZLjava/lang/String;ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 141
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-static {p12, v0, v1}, Lcwz;->a(III)V

    .line 143
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->jidResourcePrefix:Ljava/lang/String;

    .line 144
    iput-object p2, p0, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->clientVersion:Ljava/lang/String;

    .line 145
    iput-object p3, p0, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->maxIncomingVideoSpec:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    .line 146
    iput-object p4, p0, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->maxOutgoingSpecNoEffects:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    .line 147
    iput-object p5, p0, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->maxOutgoingSpecWithEffects:Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    .line 148
    iput-object p6, p0, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->outgoingVideoChatReceiver:Landroid/content/ComponentName;

    .line 149
    iput-object p7, p0, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->nativeCrashReceiver:Landroid/content/ComponentName;

    .line 150
    iput-object p8, p0, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->overides:[[Ljava/lang/String;

    .line 151
    iput-boolean p9, p0, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->useHardwareDecode:Z

    .line 152
    iput-boolean p10, p0, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->useHardwareEncode:Z

    .line 153
    iput-object p11, p0, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->rawLogDirectory:Ljava/lang/String;

    .line 154
    iput p12, p0, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->checkConnectivity:I

    .line 155
    iput-object p13, p0, Lcom/google/android/libraries/hangouts/video/Registration$RegistrationComponents;->apiaryUri:Ljava/lang/String;

    .line 156
    return-void
.end method
