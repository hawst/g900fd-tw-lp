.class abstract Lcom/google/android/libraries/hangouts/video/CameraManager;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x9
.end annotation


# static fields
.field protected static final CAMERA_STATE_CLOSED:I = 0x0

.field protected static final MASK_CAMERA_RUNNING:I = 0x4

.field protected static final MASK_HAS_CAMERA_SPEC:I = 0x2

.field protected static final MASK_HAS_PREVIEW_SURFACE:I = 0x1

.field protected static final TAG:Ljava/lang/String; = "vclib"


# instance fields
.field protected mCallback:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraManagerCallback;

.field private mCamera:Landroid/hardware/Camera;

.field protected final mCameraLock:Ljava/lang/Object;

.field private final mCameraSettings:[Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

.field private mCameraState:I

.field private mCurrentCameraIndex:I

.field private final mCurrentCameraLock:Ljava/lang/Object;

.field private mCurrentCameraSettings:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

.field private mDefaultCamera:Lcom/google/android/libraries/hangouts/video/CameraSpecification;

.field private final mFrontCameraCount:I

.field private mLastSourceTimeNs:J

.field private mLastTranslatedTimeNs:J

.field private mLastWallClockTimeNs:J

.field private final mNumAvailableCameras:I

.field private mRequestedOutputParams:Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;


# direct methods
.method protected constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraLock:Ljava/lang/Object;

    .line 68
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraLock:Ljava/lang/Object;

    .line 164
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mLastWallClockTimeNs:J

    .line 165
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mLastTranslatedTimeNs:J

    .line 167
    const/4 v0, 0x0

    .line 169
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CameraManager;->getCameraInfo()Landroid/util/Pair;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v1, v0

    .line 176
    :goto_0
    if-nez v1, :cond_0

    .line 177
    iput v5, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mNumAvailableCameras:I

    .line 178
    new-array v0, v5, [Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraSettings:[Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    .line 179
    iput v5, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mFrontCameraCount:I

    .line 187
    :goto_1
    return-void

    .line 170
    :catch_0
    move-exception v1

    .line 173
    const-string v2, "vclib"

    const-string v3, "Exception in getCameraInfo"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, Lcxc;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v1, v0

    goto :goto_0

    .line 181
    :cond_0
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    .line 182
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    iput v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mNumAvailableCameras:I

    .line 183
    iget v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mNumAvailableCameras:I

    new-array v2, v2, [Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    iput-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraSettings:[Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    .line 184
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraSettings:[Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 185
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mFrontCameraCount:I

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/google/android/libraries/hangouts/video/CameraManager;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CameraManager;->startCamera()V

    return-void
.end method

.method private chooseDefaultCamera()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 254
    iget v1, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mNumAvailableCameras:I

    if-nez v1, :cond_0

    .line 286
    :goto_0
    return-void

    .line 263
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mDefaultCamera:Lcom/google/android/libraries/hangouts/video/CameraSpecification;

    if-eqz v1, :cond_4

    .line 264
    :goto_1
    iget v1, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mNumAvailableCameras:I

    if-ge v0, v1, :cond_1

    .line 265
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraSettings:[Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->cameraId:I

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mDefaultCamera:Lcom/google/android/libraries/hangouts/video/CameraSpecification;

    iget v2, v2, Lcom/google/android/libraries/hangouts/video/CameraSpecification;->cameraId:I

    if-ne v1, v2, :cond_2

    .line 266
    iput v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraIndex:I

    .line 267
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mDefaultCamera:Lcom/google/android/libraries/hangouts/video/CameraSpecification;

    iget-object v1, v1, Lcom/google/android/libraries/hangouts/video/CameraSpecification;->previewSize:Lcom/google/android/libraries/hangouts/video/Size;

    if-eqz v1, :cond_1

    .line 269
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraSettings:[Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mDefaultCamera:Lcom/google/android/libraries/hangouts/video/CameraSpecification;

    iget-object v2, v2, Lcom/google/android/libraries/hangouts/video/CameraSpecification;->previewSize:Lcom/google/android/libraries/hangouts/video/Size;

    iput-object v2, v1, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->previewSize:Lcom/google/android/libraries/hangouts/video/Size;

    .line 270
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraSettings:[Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    aget-object v0, v1, v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->previewSizeIsValid:Z

    .line 285
    :cond_1
    :goto_2
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraSettings:[Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraIndex:I

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraSettings:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    goto :goto_0

    .line 264
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 277
    :cond_3
    add-int/lit8 v0, v0, 0x1

    :cond_4
    iget v1, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mNumAvailableCameras:I

    if-ge v0, v1, :cond_1

    .line 278
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraSettings:[Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    aget-object v1, v1, v0

    iget-boolean v1, v1, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->cameraIsFrontFacing:Z

    if-eqz v1, :cond_3

    .line 279
    iput v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraIndex:I

    goto :goto_2
.end method

.method private ensureCameraChosen()V
    .locals 2

    .prologue
    .line 236
    invoke-static {}, Lcwz;->j()V

    .line 237
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraLock:Ljava/lang/Object;

    monitor-enter v1

    .line 238
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraSettings:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    if-nez v0, :cond_0

    .line 239
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CameraManager;->chooseDefaultCamera()V

    .line 241
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private frameTimeBasisChanged()V
    .locals 2

    .prologue
    .line 123
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mLastSourceTimeNs:J

    .line 124
    return-void
.end method

.method private getBestSupportedFocusMode(Landroid/hardware/Camera$Parameters;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 421
    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v1

    .line 422
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v0

    move v2, v0

    move v4, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 423
    const-string v6, "continuous-video"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v4, v3

    .line 424
    goto :goto_0

    .line 425
    :cond_0
    const-string v6, "continuous-picture"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    move v2, v3

    .line 426
    goto :goto_0

    .line 427
    :cond_1
    const-string v6, "edof"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v3

    :goto_1
    move v1, v0

    .line 430
    goto :goto_0

    .line 431
    :cond_2
    if-eqz v4, :cond_3

    .line 432
    const-string v0, "continuous-video"

    .line 438
    :goto_2
    return-object v0

    .line 433
    :cond_3
    if-eqz v2, :cond_4

    .line 434
    const-string v0, "continuous-picture"

    goto :goto_2

    .line 435
    :cond_4
    if-eqz v1, :cond_5

    .line 436
    const-string v0, "edof"

    goto :goto_2

    .line 438
    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method private getBestSupportedPictureSize(Landroid/hardware/Camera$Parameters;Lcom/google/android/libraries/hangouts/video/Size;)Lcom/google/android/libraries/hangouts/video/Size;
    .locals 11

    .prologue
    const/4 v6, 0x0

    const v1, 0x7fffffff

    .line 329
    iget v0, p2, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    mul-int/lit16 v0, v0, 0x3e8

    iget v2, p2, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    div-int v7, v0, v2

    .line 330
    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getSupportedPictureSizes()Ljava/util/List;

    move-result-object v0

    .line 334
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v3, v1

    move-object v5, v6

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    .line 335
    iget v2, v0, Landroid/hardware/Camera$Size;->width:I

    iget v4, p2, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    if-lt v2, v4, :cond_0

    iget v2, v0, Landroid/hardware/Camera$Size;->height:I

    iget v4, p2, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    if-lt v2, v4, :cond_0

    .line 337
    iget v2, v0, Landroid/hardware/Camera$Size;->width:I

    iget v4, p2, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    if-ne v2, v4, :cond_3

    iget v2, v0, Landroid/hardware/Camera$Size;->height:I

    iget v4, p2, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    if-ne v2, v4, :cond_3

    .line 361
    :goto_1
    invoke-static {}, Lcxc;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 362
    const-string v1, "vclib"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "best picture size: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, v0, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    :cond_1
    if-eqz v0, :cond_2

    .line 365
    new-instance v6, Lcom/google/android/libraries/hangouts/video/Size;

    iget v1, v0, Landroid/hardware/Camera$Size;->width:I

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    invoke-direct {v6, v1, v0}, Lcom/google/android/libraries/hangouts/video/Size;-><init>(II)V

    .line 367
    :cond_2
    return-object v6

    .line 345
    :cond_3
    iget v2, v0, Landroid/hardware/Camera$Size;->width:I

    mul-int/lit16 v2, v2, 0x3e8

    iget v4, v0, Landroid/hardware/Camera$Size;->height:I

    div-int/2addr v2, v4

    .line 347
    sub-int/2addr v2, v7

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v4

    .line 349
    iget v2, v0, Landroid/hardware/Camera$Size;->width:I

    iget v9, v0, Landroid/hardware/Camera$Size;->height:I

    mul-int/2addr v2, v9

    .line 353
    if-lt v4, v3, :cond_4

    if-ne v4, v3, :cond_5

    if-ge v2, v1, :cond_5

    :cond_4
    move v1, v4

    move v10, v2

    move-object v2, v0

    move v0, v10

    :goto_2
    move v3, v1

    move-object v5, v2

    move v1, v0

    .line 360
    goto :goto_0

    :cond_5
    move v0, v1

    move-object v2, v5

    move v1, v3

    goto :goto_2

    :cond_6
    move-object v0, v5

    goto :goto_1
.end method

.method private getBestSupportedPreviewFpsRange(Landroid/hardware/Camera$Parameters;I)[I
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 395
    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewFpsRange()Ljava/util/List;

    move-result-object v0

    .line 396
    const/4 v3, 0x0

    .line 397
    const v2, 0x7fffffff

    .line 398
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    .line 400
    aget v1, v0, v6

    mul-int/lit16 v5, p2, 0x3e8

    if-gt v1, v5, :cond_2

    aget v1, v0, v7

    mul-int/lit16 v5, p2, 0x3e8

    if-lt v1, v5, :cond_2

    .line 401
    aget v1, v0, v7

    aget v5, v0, v6

    sub-int/2addr v1, v5

    .line 402
    if-ge v1, v2, :cond_2

    move v8, v1

    move-object v1, v0

    move v0, v8

    :goto_1
    move v2, v0

    move-object v3, v1

    .line 407
    goto :goto_0

    .line 408
    :cond_0
    invoke-static {}, Lcxc;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz v3, :cond_1

    .line 409
    const-string v0, "vclib"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "best fps range: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget v2, v3, v6

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget v2, v3, v7

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    :cond_1
    return-object v3

    :cond_2
    move v0, v2

    move-object v1, v3

    goto :goto_1
.end method

.method private getBestSupportedPreviewFrameRate(Landroid/hardware/Camera$Parameters;I)I
    .locals 6

    .prologue
    const/high16 v3, -0x80000000

    .line 375
    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewFrameRates()Ljava/util/List;

    move-result-object v1

    .line 377
    const v0, 0x7fffffff

    .line 378
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v0

    move v2, v3

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 379
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    sub-int v4, p2, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    .line 380
    if-ge v4, v1, :cond_2

    .line 381
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move v0, v4

    :goto_1
    move v2, v1

    move v1, v0

    .line 384
    goto :goto_0

    .line 385
    :cond_0
    invoke-static {}, Lcxc;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eq v2, v3, :cond_1

    .line 386
    const-string v0, "vclib"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "best fps rate: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    :cond_1
    return v2

    :cond_2
    move v0, v1

    move v1, v2

    goto :goto_1
.end method

.method private getBestSupportedPreviewSize(Landroid/hardware/Camera$Parameters;Lcom/google/android/libraries/hangouts/video/Size;)Lcom/google/android/libraries/hangouts/video/Size;
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 292
    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v0

    .line 294
    const v2, 0x7fffffff

    .line 295
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-object v3, v4

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    .line 296
    iget v1, v0, Landroid/hardware/Camera$Size;->width:I

    iget v5, p2, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    sub-int v5, v1, v5

    .line 297
    iget v1, v0, Landroid/hardware/Camera$Size;->height:I

    iget v7, p2, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    sub-int/2addr v1, v7

    .line 299
    if-gez v5, :cond_0

    .line 300
    mul-int/lit8 v5, v5, -0x4

    .line 302
    :cond_0
    if-gez v1, :cond_1

    .line 303
    mul-int/lit8 v1, v1, -0x4

    .line 306
    :cond_1
    add-int/2addr v1, v5

    .line 307
    if-ge v1, v2, :cond_5

    move v8, v1

    move-object v1, v0

    move v0, v8

    :goto_1
    move v2, v0

    move-object v3, v1

    .line 312
    goto :goto_0

    .line 313
    :cond_2
    invoke-static {}, Lcxc;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz v3, :cond_3

    .line 314
    const-string v0, "vclib"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "best preview size: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v3, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v3, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    :cond_3
    if-eqz v3, :cond_4

    .line 317
    new-instance v4, Lcom/google/android/libraries/hangouts/video/Size;

    iget v0, v3, Landroid/hardware/Camera$Size;->width:I

    iget v1, v3, Landroid/hardware/Camera$Size;->height:I

    invoke-direct {v4, v0, v1}, Lcom/google/android/libraries/hangouts/video/Size;-><init>(II)V

    .line 319
    :cond_4
    return-object v4

    :cond_5
    move v0, v2

    move-object v1, v3

    goto :goto_1
.end method

.method private getCameraInfo()Landroid/util/Pair;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;",
            ">;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 190
    invoke-static {}, Lcxl;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 191
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Simulated Camera API failure"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 195
    :cond_0
    new-instance v5, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v5}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 196
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v6

    .line 197
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 200
    const-string v0, "rear_camera_only"

    .line 201
    invoke-static {v0}, Lcxc;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 200
    invoke-static {v0}, Lcxc;->a(Ljava/lang/String;)Z

    move-result v8

    move v4, v3

    move v0, v3

    .line 202
    :goto_0
    if-ge v4, v6, :cond_4

    .line 203
    invoke-static {v4, v5}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 204
    iget v1, v5, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v1, v2, :cond_3

    move v1, v2

    .line 205
    :goto_1
    if-eqz v1, :cond_1

    if-nez v8, :cond_2

    .line 206
    :cond_1
    new-instance v9, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    invoke-direct {v9}, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;-><init>()V

    .line 210
    iput v4, v9, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->cameraId:I

    .line 211
    iput-boolean v1, v9, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->cameraIsFrontFacing:Z

    .line 212
    iget v10, v5, Landroid/hardware/Camera$CameraInfo;->orientation:I

    iput v10, v9, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->orientation:I

    .line 213
    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 214
    if-eqz v1, :cond_2

    .line 215
    add-int/lit8 v0, v0, 0x1

    .line 202
    :cond_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    :cond_3
    move v1, v3

    .line 204
    goto :goto_1

    .line 219
    :cond_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v7, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance()Lcom/google/android/libraries/hangouts/video/CameraManager;
    .locals 2

    .prologue
    .line 155
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 156
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/CameraManagerICS;->getInstance()Lcom/google/android/libraries/hangouts/video/CameraManager;

    move-result-object v0

    .line 158
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/CameraManagerGingerbread;->getInstance()Lcom/google/android/libraries/hangouts/video/CameraManager;

    move-result-object v0

    goto :goto_0
.end method

.method private openCameraIfNeededLocked()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 448
    invoke-static {}, Lcwz;->g()V

    .line 450
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCamera:Landroid/hardware/Camera;

    if-nez v0, :cond_d

    .line 453
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCallback:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraManagerCallback;

    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 458
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CameraManager;->ensureCameraChosen()V

    .line 460
    const-string v0, "vclib"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Opening Camera "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraSettings:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    iget v2, v2, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->cameraId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcxc;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraSettings:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    iget v0, v0, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->cameraId:I

    invoke-static {v0}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCamera:Landroid/hardware/Camera;

    .line 463
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    .line 465
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraSettings:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/libraries/hangouts/video/CameraManager;->initializeCameraLocked(Landroid/hardware/Camera$Parameters;Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;)V

    .line 467
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCallback:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraManagerCallback;

    invoke-interface {v0}, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraManagerCallback;->getDesiredCaptureVideoSpec()Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v0

    .line 470
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraSettings:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    iget-boolean v2, v2, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->previewSizeIsValid:Z

    if-nez v2, :cond_0

    .line 471
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraSettings:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    .line 472
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->getSize()Lcom/google/android/libraries/hangouts/video/Size;

    move-result-object v3

    .line 471
    invoke-direct {p0, v1, v3}, Lcom/google/android/libraries/hangouts/video/CameraManager;->getBestSupportedPreviewSize(Landroid/hardware/Camera$Parameters;Lcom/google/android/libraries/hangouts/video/Size;)Lcom/google/android/libraries/hangouts/video/Size;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->previewSize:Lcom/google/android/libraries/hangouts/video/Size;

    .line 473
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraSettings:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    iput-boolean v6, v2, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->previewSizeIsValid:Z

    .line 475
    :cond_0
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraSettings:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    iget-boolean v2, v2, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->pictureSizeIsValid:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraSettings:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    iget-object v2, v2, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->previewSize:Lcom/google/android/libraries/hangouts/video/Size;

    if-eqz v2, :cond_1

    .line 477
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraSettings:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraSettings:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    iget-object v3, v3, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->previewSize:Lcom/google/android/libraries/hangouts/video/Size;

    invoke-direct {p0, v1, v3}, Lcom/google/android/libraries/hangouts/video/CameraManager;->getBestSupportedPictureSize(Landroid/hardware/Camera$Parameters;Lcom/google/android/libraries/hangouts/video/Size;)Lcom/google/android/libraries/hangouts/video/Size;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->pictureSize:Lcom/google/android/libraries/hangouts/video/Size;

    .line 479
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraSettings:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    iput-boolean v6, v2, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->pictureSizeIsValid:Z

    .line 484
    :cond_1
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraSettings:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    iget-object v2, v2, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->pictureSize:Lcom/google/android/libraries/hangouts/video/Size;

    if-eqz v2, :cond_3

    .line 485
    invoke-static {}, Lcxc;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 486
    const-string v2, "vclib"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Setting camera picture size to "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraSettings:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    iget-object v4, v4, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->pictureSize:Lcom/google/android/libraries/hangouts/video/Size;

    .line 487
    invoke-virtual {v4}, Lcom/google/android/libraries/hangouts/video/Size;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 486
    invoke-static {v2, v3}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 489
    :cond_2
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraSettings:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    iget-object v2, v2, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->pictureSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v2, v2, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraSettings:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    iget-object v3, v3, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->pictureSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v3, v3, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    invoke-virtual {v1, v2, v3}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    .line 494
    :cond_3
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraSettings:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    iget-object v2, v2, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->previewSize:Lcom/google/android/libraries/hangouts/video/Size;

    if-eqz v2, :cond_5

    .line 495
    invoke-static {}, Lcxc;->b()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 496
    const-string v2, "vclib"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Setting camera preview size to "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraSettings:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    iget-object v4, v4, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->previewSize:Lcom/google/android/libraries/hangouts/video/Size;

    .line 497
    invoke-virtual {v4}, Lcom/google/android/libraries/hangouts/video/Size;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 496
    invoke-static {v2, v3}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    :cond_4
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraSettings:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    iget-object v2, v2, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->previewSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v2, v2, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraSettings:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    iget-object v3, v3, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->previewSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget v3, v3, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    invoke-virtual {v1, v2, v3}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    .line 503
    :cond_5
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->getFrameRate()I

    move-result v2

    .line 506
    invoke-direct {p0, v1, v2}, Lcom/google/android/libraries/hangouts/video/CameraManager;->getBestSupportedPreviewFrameRate(Landroid/hardware/Camera$Parameters;I)I

    move-result v3

    .line 507
    const v0, 0x7fffffff

    .line 508
    const/high16 v4, -0x80000000

    if-eq v3, v4, :cond_7

    .line 509
    invoke-static {}, Lcxc;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 510
    const-string v0, "vclib"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "setPreviewFrameRate: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    :cond_6
    invoke-virtual {v1, v3}, Landroid/hardware/Camera$Parameters;->setPreviewFrameRate(I)V

    .line 513
    sub-int v0, v2, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    .line 517
    :cond_7
    invoke-direct {p0, v1, v2}, Lcom/google/android/libraries/hangouts/video/CameraManager;->getBestSupportedPreviewFpsRange(Landroid/hardware/Camera$Parameters;I)[I

    move-result-object v2

    .line 518
    if-eqz v2, :cond_9

    aget v3, v2, v6

    aget v4, v2, v7

    sub-int/2addr v3, v4

    if-gt v3, v0, :cond_9

    .line 519
    invoke-static {}, Lcxc;->b()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 520
    const-string v0, "vclib"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "setPreviewFpsRange: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget v4, v2, v7

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget v4, v2, v6

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    :cond_8
    aget v0, v2, v7

    aget v2, v2, v6

    invoke-virtual {v1, v0, v2}, Landroid/hardware/Camera$Parameters;->setPreviewFpsRange(II)V

    .line 526
    :cond_9
    invoke-direct {p0, v1}, Lcom/google/android/libraries/hangouts/video/CameraManager;->getBestSupportedFocusMode(Landroid/hardware/Camera$Parameters;)Ljava/lang/String;

    move-result-object v0

    .line 527
    if-eqz v0, :cond_a

    .line 528
    invoke-virtual {v1, v0}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    .line 532
    :cond_a
    invoke-static {}, Lcxc;->b()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 533
    const-string v0, "vclib"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "focusMode: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getFocusMode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    :cond_b
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoCompatibility;->isNexus4()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 539
    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getSupportedPictureSizes()Ljava/util/List;

    move-result-object v0

    .line 540
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-eqz v2, :cond_c

    .line 541
    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    .line 542
    iget v2, v0, Landroid/hardware/Camera$Size;->width:I

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v1, v2, v0}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    .line 546
    :cond_c
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    .line 548
    :cond_d
    return-void
.end method

.method private releaseCamera()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 555
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraLock:Ljava/lang/Object;

    monitor-enter v1

    .line 556
    :try_start_0
    const-string v2, "vclib"

    const-string v3, "releaseCamera"

    invoke-static {v2, v3}, Lcxc;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 557
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCamera:Landroid/hardware/Camera;

    if-eqz v2, :cond_0

    .line 558
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->stopPreview()V

    .line 559
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->release()V

    .line 560
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCamera:Landroid/hardware/Camera;

    .line 562
    :cond_0
    iget v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraState:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraState:I

    .line 564
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraSettings:[Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    array-length v3, v2

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 565
    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->previewSizeIsValid:Z

    .line 566
    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->pictureSizeIsValid:Z

    .line 564
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 568
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static requestOutputParametersNative(III)V
    .locals 1

    .prologue
    .line 844
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/CameraManager;->getInstance()Lcom/google/android/libraries/hangouts/video/CameraManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Lcom/google/android/libraries/hangouts/video/CameraManager;->requestOutputParameters(III)V

    .line 845
    return-void
.end method

.method private startCamera()V
    .locals 5

    .prologue
    .line 735
    invoke-static {}, Lcwz;->g()V

    .line 736
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraLock:Ljava/lang/Object;

    monitor-enter v1

    .line 737
    :try_start_0
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraState:I

    and-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_0

    .line 739
    monitor-exit v1

    .line 792
    :goto_0
    return-void

    .line 742
    :cond_0
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraState:I

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_1

    .line 748
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 792
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 751
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCamera:Landroid/hardware/Camera;

    if-eqz v0, :cond_2

    .line 752
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    .line 755
    :cond_2
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraState:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    .line 756
    :goto_1
    iget v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraState:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraState:I

    .line 758
    if-eqz v0, :cond_4

    .line 760
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCamera:Landroid/hardware/Camera;

    invoke-static {v2}, Lcwz;->b(Ljava/lang/Object;)V

    .line 771
    :goto_2
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {p0, v2}, Lcom/google/android/libraries/hangouts/video/CameraManager;->setSurfaceOnCameraLocked(Landroid/hardware/Camera;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-eqz v2, :cond_7

    .line 773
    :try_start_2
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->startPreview()V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 779
    :try_start_3
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/CameraManager;->restorePreviewCallback()Z

    move-result v2

    if-nez v2, :cond_5

    .line 780
    const-string v0, "vclib"

    const-string v2, "restorePreviewCallback error"

    invoke-static {v0, v2}, Lcxc;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 781
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    .line 782
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCallback:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraManagerCallback;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraManagerCallback;->onCameraOpenError(Ljava/lang/Exception;)V

    .line 783
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 755
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 763
    :cond_4
    :try_start_4
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CameraManager;->openCameraIfNeededLocked()V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 764
    :catch_0
    move-exception v0

    .line 765
    :try_start_5
    const-string v2, "vclib"

    const-string v3, "openCamera exception"

    invoke-static {v2, v3, v0}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 766
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCallback:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraManagerCallback;

    invoke-interface {v2, v0}, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraManagerCallback;->onCameraOpenError(Ljava/lang/Exception;)V

    .line 767
    monitor-exit v1

    goto :goto_0

    .line 774
    :catch_1
    move-exception v0

    .line 775
    const-string v2, "vclib"

    const-string v3, "startPreview exception"

    invoke-static {v2, v3, v0}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 776
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCallback:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraManagerCallback;

    invoke-interface {v2, v0}, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraManagerCallback;->onCameraOpenError(Ljava/lang/Exception;)V

    .line 777
    monitor-exit v1

    goto :goto_0

    .line 785
    :cond_5
    if-nez v0, :cond_6

    .line 786
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCallback:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraManagerCallback;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraSettings:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    iget-object v2, v2, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->previewSize:Lcom/google/android/libraries/hangouts/video/Size;

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraSettings:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    iget v3, v3, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->orientation:I

    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraSettings:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    iget-boolean v4, v4, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->cameraIsFrontFacing:Z

    invoke-interface {v0, v2, v3, v4}, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraManagerCallback;->onCameraOpen(Lcom/google/android/libraries/hangouts/video/Size;IZ)V

    .line 790
    :cond_6
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraState:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraState:I

    .line 792
    :cond_7
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0
.end method

.method private startCameraOnCameraOpenThread()V
    .locals 3

    .prologue
    .line 725
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/google/android/libraries/hangouts/video/CameraManager$1;

    invoke-direct {v1, p0}, Lcom/google/android/libraries/hangouts/video/CameraManager$1;-><init>(Lcom/google/android/libraries/hangouts/video/CameraManager;)V

    const-string v2, "CameraOpenThread"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 730
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 731
    return-void
.end method

.method private useCameraInternal(ILcom/google/android/libraries/hangouts/video/Size;)V
    .locals 4

    .prologue
    .line 662
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraLock:Ljava/lang/Object;

    monitor-enter v1

    .line 663
    :try_start_0
    const-string v0, "vclib"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "useCameraInternal cameraState: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcxc;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 665
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CameraManager;->releaseCamera()V

    .line 667
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraIndex:I

    .line 668
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraLock:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 669
    :try_start_1
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraSettings:[Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    iget v3, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraIndex:I

    aget-object v0, v0, v3

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraSettings:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    .line 670
    if-eqz p2, :cond_0

    .line 671
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraSettings:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    iput-object p2, v0, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->previewSize:Lcom/google/android/libraries/hangouts/video/Size;

    .line 672
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraSettings:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    const/4 v3, 0x1

    iput-boolean v3, v0, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->previewSizeIsValid:Z

    .line 674
    :cond_0
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 676
    :try_start_2
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraState:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraState:I

    .line 677
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraState:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 678
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CameraManager;->startCameraOnCameraOpenThread()V

    .line 680
    :cond_1
    monitor-exit v1

    return-void

    .line 674
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 680
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public arePreview3ALocksSupported()Z
    .locals 2

    .prologue
    .line 830
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraLock:Ljava/lang/Object;

    monitor-enter v1

    .line 831
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraSettings:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/hangouts/video/CameraManager;->arePreview3ALocksSupportedLocked(Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;)Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 832
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected abstract arePreview3ALocksSupportedLocked(Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;)Z
.end method

.method getCameraCount()I
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraSettings:[Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    array-length v0, v0

    return v0
.end method

.method protected getCameraState()I
    .locals 1

    .prologue
    .line 654
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraState:I

    return v0
.end method

.method protected getCurrentCamera()Landroid/hardware/Camera;
    .locals 1

    .prologue
    .line 658
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCamera:Landroid/hardware/Camera;

    return-object v0
.end method

.method public getCurrentCameraId()I
    .locals 2

    .prologue
    .line 645
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraIndex:I

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraSettings:[Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraSettings:[Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraIndex:I

    aget-object v0, v0, v1

    iget v0, v0, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->cameraId:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method protected getCurrentCameraSettings()Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;
    .locals 1

    .prologue
    .line 650
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraSettings:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    return-object v0
.end method

.method getFrontCameraCount()I
    .locals 1

    .prologue
    .line 227
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mFrontCameraCount:I

    return v0
.end method

.method public getRequestedOutputParameters()Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;
    .locals 1

    .prologue
    .line 598
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mRequestedOutputParams:Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;

    return-object v0
.end method

.method hasFrontCamera()Z
    .locals 1

    .prologue
    .line 231
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mFrontCameraCount:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract initializeCameraLocked(Landroid/hardware/Camera$Parameters;Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;)V
.end method

.method public onNewPreviewSurfaceSet()V
    .locals 2

    .prologue
    .line 712
    invoke-static {}, Lcwz;->i()V

    .line 713
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraLock:Ljava/lang/Object;

    monitor-enter v1

    .line 714
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CameraManager;->frameTimeBasisChanged()V

    .line 715
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraState:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraState:I

    .line 716
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraState:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    .line 717
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CameraManager;->startCameraOnCameraOpenThread()V

    .line 719
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public onPreviewSurfaceCleared()V
    .locals 2

    .prologue
    .line 702
    invoke-static {}, Lcwz;->a()V

    .line 703
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraLock:Ljava/lang/Object;

    monitor-enter v1

    .line 704
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CameraManager;->frameTimeBasisChanged()V

    .line 705
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/CameraManager;->suspendCamera()V

    .line 706
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraState:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraState:I

    .line 707
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected requestOutputParameters(III)V
    .locals 3

    .prologue
    .line 612
    const-string v0, "vclib"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Camera requestOutputParameters state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " fps"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 614
    invoke-static {}, Lcwz;->f()V

    .line 616
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraLock:Ljava/lang/Object;

    monitor-enter v1

    .line 617
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCallback:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraManagerCallback;

    if-eqz v0, :cond_0

    .line 618
    new-instance v0, Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;

    new-instance v2, Lcom/google/android/libraries/hangouts/video/Size;

    invoke-direct {v2, p1, p2}, Lcom/google/android/libraries/hangouts/video/Size;-><init>(II)V

    invoke-direct {v0, v2, p3}, Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;-><init>(Lcom/google/android/libraries/hangouts/video/Size;I)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mRequestedOutputParams:Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;

    .line 620
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCallback:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraManagerCallback;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mRequestedOutputParams:Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;

    invoke-interface {v0, v2}, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraManagerCallback;->onFrameOutputSet(Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;)V

    .line 622
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method resetCurrentCamera()V
    .locals 4

    .prologue
    .line 576
    invoke-static {}, Lcwz;->a()V

    .line 577
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraLock:Ljava/lang/Object;

    monitor-enter v1

    .line 578
    :try_start_0
    const-string v0, "vclib"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "reset state: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcxc;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 579
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraState:I

    .line 580
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CameraManager;->releaseCamera()V

    .line 581
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CameraManager;->chooseDefaultCamera()V

    .line 582
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mRequestedOutputParams:Lcom/google/android/libraries/hangouts/video/CameraManager$FrameOutputParameters;

    .line 583
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected abstract restorePreviewCallback()Z
.end method

.method public setApplicationCallback(Lcom/google/android/libraries/hangouts/video/CameraManager$CameraManagerCallback;)V
    .locals 2

    .prologue
    .line 592
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraLock:Ljava/lang/Object;

    monitor-enter v1

    .line 593
    :try_start_0
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCallback:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraManagerCallback;

    .line 594
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public setDefaultCamera(Lcom/google/android/libraries/hangouts/video/CameraSpecification;)V
    .locals 0

    .prologue
    .line 248
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mDefaultCamera:Lcom/google/android/libraries/hangouts/video/CameraSpecification;

    .line 249
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CameraManager;->ensureCameraChosen()V

    .line 250
    return-void
.end method

.method public setPreview3ALocks(Z)V
    .locals 3

    .prologue
    .line 812
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraLock:Ljava/lang/Object;

    monitor-enter v1

    .line 813
    :try_start_0
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraState:I

    and-int/lit8 v0, v0, 0x4

    if-nez v0, :cond_0

    .line 814
    const-string v0, "vclib"

    const-string v2, "Can\'t lock AE/AWB when camera not running!"

    invoke-static {v0, v2}, Lcxc;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 815
    monitor-exit v1

    .line 818
    :goto_0
    return-void

    .line 817
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCamera:Landroid/hardware/Camera;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCurrentCameraSettings:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    invoke-virtual {p0, p1, v0, v2}, Lcom/google/android/libraries/hangouts/video/CameraManager;->setPreview3ALocksLocked(ZLandroid/hardware/Camera;Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;)V

    .line 818
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected abstract setPreview3ALocksLocked(ZLandroid/hardware/Camera;Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;)V
.end method

.method protected abstract setSurfaceOnCameraLocked(Landroid/hardware/Camera;)Z
.end method

.method public suspendCamera()V
    .locals 4

    .prologue
    .line 688
    invoke-static {}, Lcwz;->a()V

    .line 689
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraLock:Ljava/lang/Object;

    monitor-enter v1

    .line 690
    :try_start_0
    const-string v0, "vclib"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "suspendCamera state: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcxc;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 691
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraState:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    .line 692
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CameraManager;->releaseCamera()V

    .line 696
    :cond_0
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraState:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraState:I

    .line 697
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public translateFrameTime(J)J
    .locals 8

    .prologue
    .line 136
    iget-wide v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mLastTranslatedTimeNs:J

    .line 137
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    .line 138
    iget-wide v4, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mLastSourceTimeNs:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_0

    .line 139
    iget-wide v4, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mLastSourceTimeNs:J

    sub-long v4, p1, v4

    add-long/2addr v0, v4

    .line 144
    :goto_0
    iput-wide v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mLastTranslatedTimeNs:J

    .line 145
    iput-wide v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mLastWallClockTimeNs:J

    .line 146
    return-wide v0

    .line 142
    :cond_0
    iget-wide v4, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mLastWallClockTimeNs:J

    sub-long v4, v2, v4

    add-long/2addr v0, v4

    goto :goto_0
.end method

.method public useCamera(Lcom/google/android/libraries/hangouts/video/CameraSpecification;)V
    .locals 5

    .prologue
    const/4 v1, -0x1

    .line 626
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraLock:Ljava/lang/Object;

    monitor-enter v2

    .line 628
    const/4 v0, 0x0

    :goto_0
    :try_start_0
    iget v3, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mNumAvailableCameras:I

    if-ge v0, v3, :cond_2

    .line 629
    iget v3, p1, Lcom/google/android/libraries/hangouts/video/CameraSpecification;->cameraId:I

    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/CameraManager;->mCameraSettings:[Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;

    aget-object v4, v4, v0

    iget v4, v4, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->cameraId:I

    if-ne v3, v4, :cond_0

    .line 635
    :goto_1
    if-ne v0, v1, :cond_1

    .line 636
    const-string v0, "vclib"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Can\'t switch to camera "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p1, Lcom/google/android/libraries/hangouts/video/CameraSpecification;->cameraId:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ". Invalid id."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcxc;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 637
    monitor-exit v2

    .line 641
    :goto_2
    return-void

    .line 628
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 640
    :cond_1
    iget-object v1, p1, Lcom/google/android/libraries/hangouts/video/CameraSpecification;->previewSize:Lcom/google/android/libraries/hangouts/video/Size;

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/hangouts/video/CameraManager;->useCameraInternal(ILcom/google/android/libraries/hangouts/video/Size;)V

    .line 641
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_2
    move v0, v1

    goto :goto_1
.end method
