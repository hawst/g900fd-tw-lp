.class Lcom/google/android/libraries/hangouts/video/CallAudioHelper$2;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# static fields
.field private static final EXTRA_STATE:Ljava/lang/String; = "state"

.field private static final STATE_PLUGGED:I = 0x1

.field private static final STATE_UNPLUGGED:I


# instance fields
.field final synthetic this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)V
    .locals 0

    .prologue
    .line 225
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$2;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 232
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 233
    const-string v1, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 234
    const-string v0, "state"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 235
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$2;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "WiredHeadsetReceiver.onReceive: state="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", isInitialStickyBroadcast="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 236
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$2;->isInitialStickyBroadcast()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 235
    # invokes: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->log(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$100(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;Ljava/lang/String;)V

    .line 237
    packed-switch v0, :pswitch_data_0

    .line 263
    :cond_0
    :goto_0
    return-void

    .line 239
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$2;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDevices:Ljava/util/Set;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$700(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDevice;->WIRED_HEADSET:Lcom/google/android/libraries/hangouts/video/AudioDevice;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 240
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$2;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    # invokes: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->hasEarpiece()Z
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$1100(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 241
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$2;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDevices:Ljava/util/Set;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$700(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDevice;->EARPIECE:Lcom/google/android/libraries/hangouts/video/AudioDevice;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 244
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$2;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDevices:Ljava/util/Set;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$700(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDevice;->SPEAKERPHONE:Lcom/google/android/libraries/hangouts/video/AudioDevice;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 247
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$2;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$300(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->WIRED_HEADSET_ON:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    if-ne v0, v1, :cond_2

    .line 248
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$2;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$2;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mDefaultConnectedDevice:Lcom/google/android/libraries/hangouts/video/AudioDevice;
    invoke-static {v1}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$1200(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)Lcom/google/android/libraries/hangouts/video/AudioDevice;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->setAudioDevice(Lcom/google/android/libraries/hangouts/video/AudioDevice;)V

    goto :goto_0

    .line 250
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$2;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    # invokes: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->reportUpdate()V
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$800(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)V

    goto :goto_0

    .line 255
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$2;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDevices:Ljava/util/Set;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$700(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDevice;->WIRED_HEADSET:Lcom/google/android/libraries/hangouts/video/AudioDevice;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 256
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$2;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDevices:Ljava/util/Set;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$700(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDevice;->EARPIECE:Lcom/google/android/libraries/hangouts/video/AudioDevice;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 258
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$2;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDevices:Ljava/util/Set;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$700(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDevice;->SPEAKERPHONE:Lcom/google/android/libraries/hangouts/video/AudioDevice;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 259
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$2;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDevice;->WIRED_HEADSET:Lcom/google/android/libraries/hangouts/video/AudioDevice;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->setAudioDevice(Lcom/google/android/libraries/hangouts/video/AudioDevice;)V

    goto :goto_0

    .line 237
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
