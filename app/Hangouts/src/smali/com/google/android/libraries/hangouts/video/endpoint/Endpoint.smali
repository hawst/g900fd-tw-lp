.class public Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final CONNECTION_TIME_UNSET:J = -0x1L

.field public static final STATUS_CONNECTED:I = 0x5

.field public static final STATUS_CONNECTING:I = 0x3

.field public static final STATUS_CONNECTING_WITH_MEDIA:I = 0x2

.field public static final STATUS_DIALING:I = 0x1

.field public static final STATUS_HANGUP:I = 0x6

.field public static final STATUS_JOINING:I = 0x4

.field public static final STATUS_UKNOWN:I


# instance fields
.field private volatile mAttachedData:Ljava/lang/Object;

.field private mAudioMuted:Z

.field private final mAudioSsrcs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mConnectionStatus:I

.field private mConnectionTime:J

.field private final mDisplayName:Ljava/lang/String;

.field private final mEndpointMucJid:Ljava/lang/String;

.field private final mIsSelfEndpoint:Z

.field private final mIsSelfUser:Z

.field private mMediaBlocked:Z

.field private mVideoMuted:Z

.field private final mVideoSsrcs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;IZZ)V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mAudioSsrcs:Ljava/util/ArrayList;

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mVideoSsrcs:Ljava/util/ArrayList;

    .line 51
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mEndpointMucJid:Ljava/lang/String;

    .line 52
    iput-object p2, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mDisplayName:Ljava/lang/String;

    .line 53
    iput p3, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mConnectionStatus:I

    .line 54
    iput-boolean p4, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mIsSelfEndpoint:Z

    .line 55
    iput-boolean p5, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mIsSelfUser:Z

    .line 56
    const/4 v0, 0x5

    if-ne p3, v0, :cond_0

    .line 57
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    :goto_0
    iput-wide v0, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mConnectionTime:J

    .line 58
    return-void

    .line 57
    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public addAudioSsrc(I)V
    .locals 2

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mAudioSsrcs:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 168
    return-void
.end method

.method public addVideoSsrc(I)V
    .locals 2

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mVideoSsrcs:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 190
    return-void
.end method

.method public getAttachedData()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mAttachedData:Ljava/lang/Object;

    return-object v0
.end method

.method public getAudioSsrcs()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mAudioSsrcs:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getConnectionTime()J
    .locals 2

    .prologue
    .line 126
    iget-wide v0, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mConnectionTime:J

    return-wide v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mAttachedData:Ljava/lang/Object;

    .line 88
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint$EndpointDataProvider;

    if-eqz v1, :cond_0

    .line 89
    check-cast v0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint$EndpointDataProvider;

    invoke-interface {v0}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint$EndpointDataProvider;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    .line 91
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mDisplayName:Ljava/lang/String;

    goto :goto_0
.end method

.method public getEndpointMucJid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mEndpointMucJid:Ljava/lang/String;

    return-object v0
.end method

.method public getJidNickname()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mEndpointMucJid:Ljava/lang/String;

    invoke-static {v0}, Lf;->r(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVideoSsrcs()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mVideoSsrcs:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public hasSameMucJid(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)Z
    .locals 2

    .prologue
    .line 255
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mEndpointMucJid:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mEndpointMucJid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAudioMuted()Z
    .locals 1

    .prologue
    .line 207
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mAudioMuted:Z

    return v0
.end method

.method public isConnected()Z
    .locals 2

    .prologue
    .line 138
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mConnectionStatus:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isConnecting()Z
    .locals 2

    .prologue
    .line 107
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mConnectionStatus:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isRinging()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isConnectingWithMedia()Z
    .locals 2

    .prologue
    .line 134
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mConnectionStatus:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDialing()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 97
    iget v1, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mConnectionStatus:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMediaBlocked()Z
    .locals 1

    .prologue
    .line 232
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mMediaBlocked:Z

    return v0
.end method

.method public isRinging()Z
    .locals 2

    .prologue
    .line 102
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mConnectionStatus:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->isDialing()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSelfEndpoint()Z
    .locals 1

    .prologue
    .line 145
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mIsSelfEndpoint:Z

    return v0
.end method

.method public isSelfUser()Z
    .locals 1

    .prologue
    .line 153
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mIsSelfUser:Z

    return v0
.end method

.method public isVideoMuted()Z
    .locals 1

    .prologue
    .line 218
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mVideoMuted:Z

    return v0
.end method

.method public removeAudioSsrc(I)V
    .locals 2

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mAudioSsrcs:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 176
    return-void
.end method

.method public removeVideoSsrc(I)V
    .locals 2

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mVideoSsrcs:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 198
    return-void
.end method

.method public setAttachedData(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 240
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mAttachedData:Ljava/lang/Object;

    .line 241
    return-void
.end method

.method public setAudioMuted(Z)V
    .locals 0

    .prologue
    .line 211
    iput-boolean p1, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mAudioMuted:Z

    .line 212
    return-void
.end method

.method public setConnectionStatus(I)Z
    .locals 6

    .prologue
    const/4 v0, 0x5

    .line 112
    iget v1, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mConnectionStatus:I

    .line 114
    const/4 v2, 0x4

    if-eq p1, v2, :cond_1

    :goto_0
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mConnectionStatus:I

    .line 115
    iget-wide v2, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mConnectionTime:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget v2, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mConnectionStatus:I

    if-ne v2, v0, :cond_0

    .line 116
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mConnectionTime:J

    .line 118
    :cond_0
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mConnectionStatus:I

    if-eq v1, v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    move p1, v0

    .line 114
    goto :goto_0

    .line 118
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public setMediaBlocked(Z)V
    .locals 0

    .prologue
    .line 236
    iput-boolean p1, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mMediaBlocked:Z

    .line 237
    return-void
.end method

.method public setVideoMuted(Z)V
    .locals 0

    .prologue
    .line 222
    iput-boolean p1, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mVideoMuted:Z

    .line 223
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 249
    const-string v0, "%s (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mDisplayName:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->mEndpointMucJid:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
