.class public Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public cameraId:I

.field cameraIsFrontFacing:Z

.field cameraSupports3ALocks:Z

.field haveReadCameraParameters:Z

.field orientation:I

.field public pictureSize:Lcom/google/android/libraries/hangouts/video/Size;

.field pictureSizeIsValid:Z

.field public previewSize:Lcom/google/android/libraries/hangouts/video/Size;

.field previewSizeIsValid:Z


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->haveReadCameraParameters:Z

    return-void
.end method
