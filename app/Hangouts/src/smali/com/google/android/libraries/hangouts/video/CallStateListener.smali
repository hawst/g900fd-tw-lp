.class public interface abstract Lcom/google/android/libraries/hangouts/video/CallStateListener;
.super Ljava/lang/Object;
.source "PG"


# virtual methods
.method public abstract onAudioUpdated(Lcom/google/android/libraries/hangouts/video/AudioDeviceState;Ljava/util/Set;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/hangouts/video/AudioDeviceState;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/libraries/hangouts/video/AudioDevice;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onAuthUserActionRequired(Landroid/content/Intent;)V
.end method

.method public abstract onBoundToService()V
.end method

.method public abstract onBroadcastProducerChange(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onBroadcastSessionStateChange(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onBroadcastStreamStateChange(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onBroadcastStreamViewerCountChange(I)V
.end method

.method public abstract onCallEnded(Lcom/google/android/libraries/hangouts/video/CallState;)V
.end method

.method public abstract onCommonNotificationReceived(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onCommonNotificationRetracted(Ljava/lang/String;)V
.end method

.method public abstract onConversationIdChanged(Ljava/lang/String;)V
.end method

.method public abstract onEndpointEvent(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/EndpointEvent;)V
.end method

.method public abstract onHangoutIdResolved(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;ZZ)V
.end method

.method public abstract onLoudestSpeakerUpdate(Ljava/lang/String;I)V
.end method

.method public abstract onMediaStarted(Lcom/google/android/libraries/hangouts/video/CallState;)V
.end method

.method public abstract onPresenterUpdate(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)V
.end method

.method public abstract onSignedIn()V
.end method
