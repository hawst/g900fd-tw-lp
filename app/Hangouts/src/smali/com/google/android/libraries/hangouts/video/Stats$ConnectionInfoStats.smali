.class public Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;
.super Lcom/google/android/libraries/hangouts/video/Stats;
.source "PG"


# instance fields
.field public flags:I

.field public localAddress:Ljava/lang/String;

.field public localProtocol:I

.field public localType:I

.field public mediaNetworkType:I

.field public mediaType:I

.field public receivedBitrate:I

.field public receivedBytes:I

.field public remoteAddress:Ljava/lang/String;

.field public remoteProtocol:I

.field public remoteType:I

.field public rtt:I

.field public sentBitrate:I

.field public sentBytes:I

.field public signalStrength:Ldog;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIIII)V
    .locals 1

    .prologue
    .line 499
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/Stats;-><init>()V

    .line 482
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->mediaNetworkType:I

    .line 483
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->signalStrength:Ldog;

    .line 500
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->mediaType:I

    .line 501
    # invokes: Lcom/google/android/libraries/hangouts/video/Stats;->parseConnectionType(Ljava/lang/String;)I
    invoke-static {p2}, Lcom/google/android/libraries/hangouts/video/Stats;->access$300(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->localType:I

    .line 502
    iput-object p3, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->localAddress:Ljava/lang/String;

    .line 503
    # invokes: Lcom/google/android/libraries/hangouts/video/Stats;->parseConnectionProtocol(Ljava/lang/String;)I
    invoke-static {p4}, Lcom/google/android/libraries/hangouts/video/Stats;->access$400(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->localProtocol:I

    .line 504
    # invokes: Lcom/google/android/libraries/hangouts/video/Stats;->parseConnectionType(Ljava/lang/String;)I
    invoke-static {p5}, Lcom/google/android/libraries/hangouts/video/Stats;->access$300(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->remoteType:I

    .line 505
    iput-object p6, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->remoteAddress:Ljava/lang/String;

    .line 506
    # invokes: Lcom/google/android/libraries/hangouts/video/Stats;->parseConnectionProtocol(Ljava/lang/String;)I
    invoke-static {p7}, Lcom/google/android/libraries/hangouts/video/Stats;->access$400(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->remoteProtocol:I

    .line 507
    iput p8, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->receivedBytes:I

    .line 508
    iput p9, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->receivedBitrate:I

    .line 509
    iput p10, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->sentBytes:I

    .line 510
    iput p11, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->sentBitrate:I

    .line 511
    iput p12, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->rtt:I

    .line 512
    iput p13, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->flags:I

    .line 513
    return-void
.end method

.method static printLegend(Ljava/io/PrintWriter;)V
    .locals 1

    .prologue
    .line 577
    const-string v0, "  Connection -- media type, local address, type, protocol, remote address, type, protocol, received bitrate, sent bitrate, media network type, wifi signal strength, cell signal type, cell level, cell asu level"

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 581
    return-void
.end method


# virtual methods
.method public addTo(Ldoe;)V
    .locals 6

    .prologue
    .line 527
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->mediaType:I

    packed-switch v0, :pswitch_data_0

    .line 535
    const/4 v0, 0x0

    .line 540
    :goto_0
    new-instance v1, Ldob;

    invoke-direct {v1}, Ldob;-><init>()V

    .line 542
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->localAddress:Ljava/lang/String;

    iput-object v2, v1, Ldob;->b:Ljava/lang/String;

    .line 543
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Ldob;->c:Ljava/lang/Integer;

    .line 544
    iget v2, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->localType:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Ldob;->d:Ljava/lang/Integer;

    .line 545
    iget v2, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->localProtocol:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Ldob;->e:Ljava/lang/Integer;

    .line 548
    new-instance v2, Ldob;

    invoke-direct {v2}, Ldob;-><init>()V

    .line 550
    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->localAddress:Ljava/lang/String;

    iput-object v3, v2, Ldob;->b:Ljava/lang/String;

    .line 551
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Ldob;->c:Ljava/lang/Integer;

    .line 552
    iget v3, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->remoteType:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Ldob;->d:Ljava/lang/Integer;

    .line 553
    iget v3, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->remoteProtocol:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Ldob;->e:Ljava/lang/Integer;

    .line 555
    new-instance v3, Ldof;

    invoke-direct {v3}, Ldof;-><init>()V

    .line 556
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v3, Ldof;->b:Ljava/lang/Integer;

    .line 557
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->flags:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v3, Ldof;->c:Ljava/lang/Integer;

    .line 558
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->rtt:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v3, Ldof;->d:Ljava/lang/Integer;

    .line 559
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->sentBytes:I

    int-to-long v4, v0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v3, Ldof;->e:Ljava/lang/Long;

    .line 560
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->receivedBytes:I

    int-to-long v4, v0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v3, Ldof;->g:Ljava/lang/Long;

    .line 561
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->sentBitrate:I

    div-int/lit8 v0, v0, 0x8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v3, Ldof;->f:Ljava/lang/Integer;

    .line 562
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->receivedBitrate:I

    div-int/lit8 v0, v0, 0x8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v3, Ldof;->h:Ljava/lang/Integer;

    .line 563
    iput-object v1, v3, Ldof;->i:Ldob;

    .line 564
    iput-object v2, v3, Ldof;->j:Ldob;

    .line 566
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->mediaNetworkType:I

    if-lez v0, :cond_0

    .line 567
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->mediaNetworkType:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v3, Ldof;->k:Ljava/lang/Integer;

    .line 569
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->signalStrength:Ldog;

    iput-object v0, v3, Ldof;->l:Ldog;

    .line 571
    iget-object v0, p1, Ldoe;->d:[Ldof;

    array-length v1, v0

    .line 572
    iget-object v0, p1, Ldoe;->d:[Ldof;

    add-int/lit8 v2, v1, 0x1

    invoke-static {v0, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldof;

    iput-object v0, p1, Ldoe;->d:[Ldof;

    .line 573
    iget-object v0, p1, Ldoe;->d:[Ldof;

    aput-object v3, v0, v1

    .line 574
    return-void

    .line 529
    :pswitch_0
    const/4 v0, 0x1

    .line 530
    goto/16 :goto_0

    .line 532
    :pswitch_1
    const/4 v0, 0x2

    .line 533
    goto/16 :goto_0

    .line 527
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public print(Ljava/io/PrintWriter;Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;)V
    .locals 3

    .prologue
    const/4 v1, 0x3

    .line 585
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->localType:I

    if-eq v1, v0, :cond_0

    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->remoteType:I

    if-ne v1, v0, :cond_1

    .line 587
    :cond_0
    const/4 v0, 0x1

    # setter for: Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;->mostRecentlyUsingRelayServer:Z
    invoke-static {p2, v0}, Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;->access$502(Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;Z)Z

    .line 591
    :goto_0
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->mediaType:I

    if-nez v0, :cond_2

    const-string v0, "video"

    .line 593
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " -- Connection -- "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->localAddress:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->localType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->localProtocol:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->remoteAddress:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->remoteType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->remoteProtocol:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->receivedBitrate:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->sentBitrate:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->mediaNetworkType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->signalStrength:Ldog;

    iget-object v1, v1, Ldog;->b:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->signalStrength:Ldog;

    iget-object v1, v1, Ldog;->c:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->signalStrength:Ldog;

    iget-object v1, v1, Ldog;->d:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->signalStrength:Ldog;

    iget-object v1, v1, Ldog;->e:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 608
    return-void

    .line 589
    :cond_1
    const/4 v0, 0x0

    # setter for: Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;->mostRecentlyUsingRelayServer:Z
    invoke-static {p2, v0}, Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;->access$502(Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;Z)Z

    goto/16 :goto_0

    .line 591
    :cond_2
    const-string v0, "audio"

    goto/16 :goto_1
.end method

.method public setMediaNetworkType(I)V
    .locals 0

    .prologue
    .line 516
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->mediaNetworkType:I

    .line 517
    return-void
.end method

.method public setSignalStrength(Ldog;)V
    .locals 0

    .prologue
    .line 520
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->signalStrength:Ldog;

    .line 521
    return-void
.end method
