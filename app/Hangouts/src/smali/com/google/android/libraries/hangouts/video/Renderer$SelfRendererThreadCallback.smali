.class public interface abstract Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/hangouts/video/Renderer$RendererThreadCallback;


# virtual methods
.method public abstract getIdealCaptureSize()Lcom/google/android/libraries/hangouts/video/Size;
.end method

.method public abstract onCameraOpenError(Ljava/lang/Exception;)V
.end method

.method public abstract onCameraOpened(Z)V
.end method

.method public abstract onFrameGeometryChanged(IIII)V
.end method
