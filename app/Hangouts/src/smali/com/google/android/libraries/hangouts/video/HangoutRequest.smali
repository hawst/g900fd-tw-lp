.class public Lcom/google/android/libraries/hangouts/video/HangoutRequest;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/io/Serializable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/libraries/hangouts/video/HangoutRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final EXT_KEY_TYPE_CONVERSATION:Ljava/lang/String; = "conversation"

.field private static final HANGOUT_REQUEST_TYPE:Ljava/lang/String; = "vnd.android.item/hangout"

.field private static final PLUS_ONE_INTENT_EXTRA_ACCOUNT_NAME:Ljava/lang/String; = "account_name"

.field private static final PLUS_ONE_INTENT_EXTRA_MEETING_DOMAIN:Ljava/lang/String; = "hangout_room_domain"

.field private static final PLUS_ONE_INTENT_EXTRA_ROOM_NAME:Ljava/lang/String; = "hangout_room_name"

.field private static final PREF_ACCOUNT_NAME:Ljava/lang/String; = "ACCOUNT_NAME"

.field private static final PREF_CALL_MEDIA_TYPE:Ljava/lang/String; = "CALL_MEDIA_TYPE"

.field private static final PREF_CONVERSATION_ID:Ljava/lang/String; = "CONVERSATION_ID"

.field private static final PREF_EXTERNAL_KEY:Ljava/lang/String; = "EXTERNAL_KEY"

.field private static final PREF_EXTERNAL_KEY_TYPE:Ljava/lang/String; = "EXTERNAL_KEY_TYPE"

.field private static final PREF_HANGOUT_DOMAIN:Ljava/lang/String; = "INFO_HANGOUT_DOMAIN"

.field private static final PREF_HANGOUT_ID:Ljava/lang/String; = "INFO_HANGOUT_ID"

.field private static final PREF_HANGOUT_TYPE:Ljava/lang/String; = "HANGOUT_TYPE"

.field private static final PREF_INVITEE_NICK:Ljava/lang/String; = "INFO_INVITEE_NICK"

.field private static final PREF_PENDING_HANGOUT_ID_KEY:Ljava/lang/String; = "PENDING_HANGOUT_ID_KEY"

.field private static final sNamedHangoutDomains:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mAccountName:Ljava/lang/String;

.field private final transient mCallCompleteIntent:Landroid/app/PendingIntent;

.field private final mCallMediaType:I

.field private mConversationId:Ljava/lang/String;

.field private mExternalKey:Ljava/lang/String;

.field private final mExternalKeyType:Ljava/lang/String;

.field private final mHangoutDomain:Ljava/lang/String;

.field private final mHangoutId:Ljava/lang/String;

.field private final mHangoutType:I

.field private final mInviteeNick:Ljava/lang/String;

.field private final mPendingIdKey:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 45
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "9thavesurf.com"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "google.com"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "goredteam.com"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "hp.com"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "motorola.com"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "motorolatest.com"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->sNamedHangoutDomains:Ljava/util/List;

    .line 566
    new-instance v0, Lcom/google/android/libraries/hangouts/video/HangoutRequest$1;

    invoke-direct {v0}, Lcom/google/android/libraries/hangouts/video/HangoutRequest$1;-><init>()V

    sput-object v0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 579
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 580
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mAccountName:Ljava/lang/String;

    .line 581
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 582
    const-string v1, "conversation"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "conversation"

    :cond_0
    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKeyType:Ljava/lang/String;

    .line 584
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKey:Ljava/lang/String;

    .line 585
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mConversationId:Ljava/lang/String;

    .line 586
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutDomain:Ljava/lang/String;

    .line 587
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutId:Ljava/lang/String;

    .line 588
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mPendingIdKey:Ljava/lang/String;

    .line 589
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutType:I

    .line 590
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mCallMediaType:I

    .line 591
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mInviteeNick:Ljava/lang/String;

    .line 592
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mCallCompleteIntent:Landroid/app/PendingIntent;

    .line 593
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/libraries/hangouts/video/HangoutRequest$1;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 12

    .prologue
    .line 107
    const/4 v6, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v11}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;)V

    .line 109
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;)V
    .locals 11

    .prologue
    .line 114
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;)V

    .line 116
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;)V
    .locals 12

    .prologue
    .line 121
    const/4 v10, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;)V

    .line 123
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;)V
    .locals 2

    .prologue
    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    if-nez p1, :cond_0

    .line 130
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "accountName cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 133
    :cond_0
    if-eqz p4, :cond_1

    if-nez p5, :cond_1

    .line 134
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "externalKeyType specified but not externalKey"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 137
    :cond_1
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mAccountName:Ljava/lang/String;

    .line 138
    iput p3, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mCallMediaType:I

    .line 139
    iput p2, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutType:I

    .line 140
    iput-object p5, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKey:Ljava/lang/String;

    .line 141
    const-string v0, "conversation"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 142
    if-eqz p6, :cond_2

    invoke-virtual {p6, p5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 146
    const-string v0, "conversation"

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKeyType:Ljava/lang/String;

    .line 147
    iput-object p5, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mConversationId:Ljava/lang/String;

    .line 152
    :goto_1
    iput-object p7, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutDomain:Ljava/lang/String;

    .line 153
    iput-object p8, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutId:Ljava/lang/String;

    .line 154
    iput-object p9, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mInviteeNick:Ljava/lang/String;

    .line 155
    iput-object p11, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mCallCompleteIntent:Landroid/app/PendingIntent;

    .line 156
    iput-object p10, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mPendingIdKey:Ljava/lang/String;

    .line 157
    return-void

    .line 142
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 149
    :cond_4
    iput-object p4, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKeyType:Ljava/lang/String;

    .line 150
    iput-object p6, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mConversationId:Ljava/lang/String;

    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 12

    .prologue
    const/4 v4, 0x0

    .line 95
    const/4 v3, 0x2

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    move-object v8, v4

    move-object v9, v4

    move-object v10, p3

    move-object v11, v4

    invoke-direct/range {v0 .. v11}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;)V

    .line 97
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 11

    .prologue
    .line 101
    const/4 v3, 0x1

    const/4 v6, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    invoke-direct/range {v0 .. v10}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;)V

    .line 103
    return-void
.end method

.method public static fromIntent(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/hangouts/video/HangoutRequest;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 426
    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 427
    if-eqz v0, :cond_0

    .line 429
    invoke-static {v0, p2, v3}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->fromUri(Landroid/net/Uri;Ljava/lang/String;Landroid/app/PendingIntent;)Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v0

    .line 444
    :goto_0
    return-object v0

    .line 430
    :cond_0
    const-string v0, "vnd.android.item/hangout"

    invoke-virtual {p0}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 432
    const-string v0, "account_name"

    .line 433
    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 432
    invoke-static {v0}, Lf;->q(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 434
    new-instance v0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    const/4 v2, 0x1

    const-string v4, "hangout_room_domain"

    .line 439
    invoke-virtual {p0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v4, "hangout_room_name"

    .line 440
    invoke-virtual {p0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object v4, v3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 441
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0, p1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 442
    invoke-virtual {p0, p1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    goto :goto_0

    :cond_2
    move-object v0, v3

    .line 444
    goto :goto_0
.end method

.method public static fromUri(Landroid/net/Uri;Ljava/lang/String;Landroid/app/PendingIntent;)Lcom/google/android/libraries/hangouts/video/HangoutRequest;
    .locals 10

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v8, 0x0

    .line 450
    if-nez p0, :cond_0

    .line 529
    :goto_0
    return-object v8

    .line 460
    :cond_0
    invoke-virtual {p0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v5

    .line 461
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v9

    .line 462
    if-ne v9, v3, :cond_1

    .line 463
    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 464
    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 465
    const-string v3, "hangouts"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    :goto_1
    move-object v7, v1

    move-object v6, v8

    move-object v5, v8

    move-object v4, v8

    .line 525
    :goto_2
    if-nez v7, :cond_9

    if-nez v5, :cond_9

    .line 526
    const-string v0, "vclib"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not parse "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcxc;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 469
    :cond_1
    if-ne v9, v4, :cond_3

    .line 470
    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 471
    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 472
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 473
    const-string v4, "talkgadget"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "hangout"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    move-object v7, v3

    move-object v6, v8

    move-object v5, v8

    move-object v4, v8

    .line 475
    goto :goto_2

    .line 476
    :cond_2
    const-string v4, "hangouts"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    const-string v0, "_"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    :goto_3
    move-object v7, v3

    move-object v6, v8

    move-object v5, v8

    move-object v4, v8

    .line 480
    goto :goto_2

    :cond_3
    const/4 v0, 0x4

    if-ne v9, v0, :cond_8

    .line 481
    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 482
    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 483
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 484
    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 485
    const-string v5, "hangouts"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    const-string v5, "_"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 486
    const-string v0, "lite"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move-object v7, v4

    move-object v6, v8

    move-object v5, v8

    move-object v4, v8

    .line 488
    goto/16 :goto_2

    .line 489
    :cond_4
    sget-object v0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->sNamedHangoutDomains:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 493
    invoke-static {v4}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object v6, v3

    move-object v5, v8

    move-object v4, v8

    goto/16 :goto_2

    :cond_5
    move-object v7, v8

    move-object v6, v8

    move-object v5, v4

    move-object v4, v3

    .line 497
    goto/16 :goto_2

    .line 499
    :cond_6
    const-string v5, "hangouts"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    const-string v5, "extras"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 503
    invoke-static {v4}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object v6, v3

    move-object v5, v8

    move-object v4, v8

    goto/16 :goto_2

    .line 504
    :cond_7
    const-string v5, "talk"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    const-string v0, "meet"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 508
    invoke-static {v4}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_4
    move-object v7, v0

    move-object v6, v3

    move-object v5, v8

    move-object v4, v8

    .line 510
    goto/16 :goto_2

    :cond_8
    const/4 v0, 0x5

    if-ne v9, v0, :cond_a

    .line 511
    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 512
    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 513
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 514
    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 515
    const/4 v6, 0x4

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 516
    const-string v6, "hangouts"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "extras"

    .line 517
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 521
    invoke-static {v5}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v7, v3

    move-object v6, v4

    move-object v5, v8

    move-object v4, v8

    goto/16 :goto_2

    .line 529
    :cond_9
    new-instance v0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-object v1, p1

    move v3, v2

    move-object v9, p2

    invoke-direct/range {v0 .. v9}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;)V

    move-object v8, v0

    goto/16 :goto_0

    :cond_a
    move-object v7, v8

    move-object v6, v8

    move-object v5, v8

    move-object v4, v8

    goto/16 :goto_2

    :cond_b
    move-object v0, v8

    move-object v3, v8

    goto :goto_4

    :cond_c
    move-object v3, v8

    goto/16 :goto_3

    :cond_d
    move-object v1, v8

    goto/16 :goto_1
.end method

.method static get(Landroid/content/SharedPreferences;)Lcom/google/android/libraries/hangouts/video/HangoutRequest;
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v12, 0x0

    .line 394
    const-string v0, "ACCOUNT_NAME"

    invoke-interface {p0, v0, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 395
    const-string v0, "CALL_MEDIA_TYPE"

    invoke-interface {p0, v0, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 397
    const-string v0, "EXTERNAL_KEY"

    invoke-interface {p0, v0, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 398
    const-string v0, "EXTERNAL_KEY_TYPE"

    invoke-interface {p0, v0, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 399
    const-string v0, "CONVERSATION_ID"

    invoke-interface {p0, v0, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 400
    const-string v0, "INFO_HANGOUT_DOMAIN"

    invoke-interface {p0, v0, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 401
    const-string v0, "INFO_HANGOUT_ID"

    invoke-interface {p0, v0, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 402
    const-string v0, "PENDING_HANGOUT_ID_KEY"

    invoke-interface {p0, v0, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 404
    const-string v0, "HANGOUT_TYPE"

    invoke-interface {p0, v0, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 405
    const-string v0, "INFO_INVITEE_NICK"

    invoke-interface {p0, v0, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 407
    :try_start_0
    new-instance v0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    const/4 v11, 0x0

    invoke-direct/range {v0 .. v11}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 411
    :goto_0
    return-object v0

    .line 410
    :catch_0
    move-exception v0

    const-string v0, "Unexpected"

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    move-object v0, v12

    .line 411
    goto :goto_0
.end method


# virtual methods
.method public clone()Lcom/google/android/libraries/hangouts/video/HangoutRequest;
    .locals 12

    .prologue
    .line 289
    new-instance v0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mAccountName:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutType:I

    iget v3, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mCallMediaType:I

    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKeyType:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKey:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mConversationId:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutDomain:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutId:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mInviteeNick:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mPendingIdKey:Ljava/lang/String;

    iget-object v11, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mCallCompleteIntent:Landroid/app/PendingIntent;

    invoke-direct/range {v0 .. v11}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;)V

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->clone()Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    move-result-object v0

    return-object v0
.end method

.method public cloneForCallTransferReceive(Ljava/lang/String;)Lcom/google/android/libraries/hangouts/video/HangoutRequest;
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 241
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKey:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKeyType:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mCallCompleteIntent:Landroid/app/PendingIntent;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mPendingIdKey:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 243
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mAccountName:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 246
    :goto_1
    return-object p0

    .line 241
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 246
    :cond_1
    new-instance v0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    iget v2, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutType:I

    iget v3, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mCallMediaType:I

    iget-object v6, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutDomain:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutId:Ljava/lang/String;

    move-object v1, p1

    move-object v5, v4

    move-object v8, v4

    move-object v9, v4

    invoke-direct/range {v0 .. v9}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;)V

    move-object p0, v0

    goto :goto_1
.end method

.method public cloneForCallTransferSend(Ljava/lang/String;)Lcom/google/android/libraries/hangouts/video/HangoutRequest;
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 236
    new-instance v0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mAccountName:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutType:I

    iget v3, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mCallMediaType:I

    iget-object v6, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutDomain:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutId:Ljava/lang/String;

    .line 237
    invoke-static {p1}, Lf;->r(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    move-object v5, v4

    move-object v9, v4

    invoke-direct/range {v0 .. v9}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;)V

    return-object v0
.end method

.method public cloneWithConversationId(Ljava/lang/String;)Lcom/google/android/libraries/hangouts/video/HangoutRequest;
    .locals 11

    .prologue
    .line 264
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutId:Ljava/lang/String;

    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 265
    const-string v0, "conversation"

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKeyType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 268
    if-nez p1, :cond_1

    .line 269
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutId:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKeyType:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, "conversation"

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKeyType:Ljava/lang/String;

    .line 270
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 271
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Erasing the only data of conv id"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 274
    :cond_1
    new-instance v0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mAccountName:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutType:I

    iget v3, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mCallMediaType:I

    if-nez p1, :cond_2

    const/4 v4, 0x0

    :goto_0
    iget-object v7, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutDomain:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutId:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mInviteeNick:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mCallCompleteIntent:Landroid/app/PendingIntent;

    move-object v5, p1

    move-object v6, p1

    invoke-direct/range {v0 .. v10}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;)V

    .line 280
    :goto_1
    return-object v0

    .line 274
    :cond_2
    const-string v4, "conversation"

    goto :goto_0

    .line 280
    :cond_3
    new-instance v0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mAccountName:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutType:I

    iget v3, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mCallMediaType:I

    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKeyType:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKey:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutDomain:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutId:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mInviteeNick:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mCallCompleteIntent:Landroid/app/PendingIntent;

    move-object v6, p1

    invoke-direct/range {v0 .. v10}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;)V

    goto :goto_1
.end method

.method public cloneWithHangoutId(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/hangouts/video/HangoutRequest;
    .locals 10

    .prologue
    .line 252
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutDomain:Ljava/lang/String;

    invoke-static {v0}, Lcwz;->a(Ljava/lang/Object;)V

    .line 253
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutId:Ljava/lang/String;

    invoke-static {v0}, Lcwz;->a(Ljava/lang/Object;)V

    .line 254
    new-instance v0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mAccountName:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutType:I

    iget v3, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mCallMediaType:I

    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKeyType:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKey:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mInviteeNick:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mCallCompleteIntent:Landroid/app/PendingIntent;

    move-object v6, p1

    move-object v7, p2

    invoke-direct/range {v0 .. v9}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;)V

    return-object v0
.end method

.method public cloneWithInviteeNick(Ljava/lang/String;)Lcom/google/android/libraries/hangouts/video/HangoutRequest;
    .locals 10

    .prologue
    .line 259
    new-instance v0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mAccountName:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutType:I

    iget v3, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mCallMediaType:I

    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKeyType:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKey:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutDomain:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutId:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mCallCompleteIntent:Landroid/app/PendingIntent;

    move-object v8, p1

    invoke-direct/range {v0 .. v9}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 548
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 336
    if-ne p0, p1, :cond_1

    move v2, v1

    .line 377
    :cond_0
    :goto_0
    return v2

    .line 339
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v0, v3, :cond_0

    .line 343
    check-cast p1, Lcom/google/android/libraries/hangouts/video/HangoutRequest;

    .line 344
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mAccountName:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mAccountName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 348
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutType:I

    if-ne v0, v4, :cond_5

    move v0, v1

    .line 350
    :goto_1
    iget v3, p1, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutType:I

    if-ne v3, v4, :cond_6

    move v3, v1

    .line 353
    :goto_2
    if-ne v0, v3, :cond_0

    .line 359
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKeyType:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKeyType:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKeyType:Ljava/lang/String;

    .line 360
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKey:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKey:Ljava/lang/String;

    .line 361
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    .line 362
    :goto_3
    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mConversationId:Ljava/lang/String;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mConversationId:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mConversationId:Ljava/lang/String;

    .line 363
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    move v3, v1

    .line 364
    :goto_4
    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutId:Ljava/lang/String;

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutId:Ljava/lang/String;

    iget-object v5, p1, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutId:Ljava/lang/String;

    .line 365
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutDomain:Ljava/lang/String;

    if-nez v4, :cond_2

    iget-object v4, p1, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutDomain:Ljava/lang/String;

    if-eqz v4, :cond_3

    :cond_2
    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutDomain:Ljava/lang/String;

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutDomain:Ljava/lang/String;

    iget-object v5, p1, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutDomain:Ljava/lang/String;

    .line 367
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    :cond_3
    move v4, v1

    .line 368
    :goto_5
    iget-object v5, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mPendingIdKey:Ljava/lang/String;

    if-eqz v5, :cond_a

    iget-object v5, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mPendingIdKey:Ljava/lang/String;

    iget-object v6, p1, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mPendingIdKey:Ljava/lang/String;

    .line 369
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    move v5, v1

    .line 371
    :goto_6
    if-nez v0, :cond_4

    if-nez v3, :cond_4

    if-nez v4, :cond_4

    if-eqz v5, :cond_0

    :cond_4
    move v2, v1

    .line 377
    goto/16 :goto_0

    .line 348
    :cond_5
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutType:I

    goto :goto_1

    .line 350
    :cond_6
    iget v3, p1, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutType:I

    goto :goto_2

    :cond_7
    move v0, v2

    .line 361
    goto :goto_3

    :cond_8
    move v3, v2

    .line 363
    goto :goto_4

    :cond_9
    move v4, v2

    .line 367
    goto :goto_5

    :cond_a
    move v5, v2

    .line 369
    goto :goto_6
.end method

.method public getAccountName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mAccountName:Ljava/lang/String;

    return-object v0
.end method

.method public getCallCompletePendingIntent()Landroid/app/PendingIntent;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mCallCompleteIntent:Landroid/app/PendingIntent;

    return-object v0
.end method

.method public getCallMediaType()I
    .locals 1

    .prologue
    .line 197
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mCallMediaType:I

    return v0
.end method

.method public getConversationId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mConversationId:Ljava/lang/String;

    return-object v0
.end method

.method public getDomain()Ljava/lang/String;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutDomain:Ljava/lang/String;

    return-object v0
.end method

.method public getExternalKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKey:Ljava/lang/String;

    return-object v0
.end method

.method public getExternalKeyType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKeyType:Ljava/lang/String;

    return-object v0
.end method

.method public getHangoutId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutId:Ljava/lang/String;

    return-object v0
.end method

.method public getHangoutType()I
    .locals 1

    .prologue
    .line 201
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutType:I

    return v0
.end method

.method public getInviteeNick()Ljava/lang/String;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mInviteeNick:Ljava/lang/String;

    return-object v0
.end method

.method public hasExternalKey()Z
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKey:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasHangoutId()Z
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutId:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasInformationForCall()Z
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutId:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKey:Ljava/lang/String;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 322
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mAccountName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit8 v0, v0, 0x1f

    .line 325
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutType:I

    add-int/2addr v0, v1

    .line 331
    return v0
.end method

.method public isNewHangoutRequest()Z
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutId:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKeyType:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method put(Landroid/content/SharedPreferences$Editor;)V
    .locals 2

    .prologue
    .line 381
    const-string v0, "ACCOUNT_NAME"

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mAccountName:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 382
    const-string v0, "CALL_MEDIA_TYPE"

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mCallMediaType:I

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 383
    const-string v0, "EXTERNAL_KEY"

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKey:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 384
    const-string v0, "EXTERNAL_KEY_TYPE"

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKeyType:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 385
    const-string v0, "CONVERSATION_ID"

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mConversationId:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 386
    const-string v0, "INFO_HANGOUT_DOMAIN"

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutDomain:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 387
    const-string v0, "INFO_HANGOUT_ID"

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutId:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 388
    const-string v0, "PENDING_HANGOUT_ID_KEY"

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mPendingIdKey:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 389
    const-string v0, "HANGOUT_TYPE"

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutType:I

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 390
    const-string v0, "INFO_INVITEE_NICK"

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mInviteeNick:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 391
    return-void
.end method

.method public sendCallCompleteIntent()V
    .locals 5

    .prologue
    .line 536
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mCallCompleteIntent:Landroid/app/PendingIntent;

    if-eqz v0, :cond_0

    .line 538
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mCallCompleteIntent:Landroid/app/PendingIntent;

    invoke-virtual {v0}, Landroid/app/PendingIntent;->send()V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    .line 544
    :cond_0
    :goto_0
    return-void

    .line 539
    :catch_0
    move-exception v0

    .line 540
    const-string v1, "vclib"

    const-string v2, "HangoutRequest call complete intent could not be sent"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Lcxc;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public setExternalKey(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 177
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKey:Ljava/lang/String;

    .line 178
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v4, 0x3a

    const/16 v3, 0x20

    .line 296
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 297
    const-string v1, "HangoutRequest{ account="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mAccountName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 298
    const-string v1, "callMediaType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mCallMediaType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 299
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKeyType:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 300
    const-string v1, "conversation"

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKeyType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 301
    const-string v1, "extKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKeyType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKey:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    .line 302
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 304
    :cond_0
    const-string v1, "convId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mConversationId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 306
    :cond_1
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutId:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 307
    const-string v1, "hangoutId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutDomain:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    .line 308
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 310
    :cond_2
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mPendingIdKey:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 311
    const-string v1, "pendingIdKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mPendingIdKey:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 313
    :cond_3
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mInviteeNick:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 314
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mInviteeNick:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 316
    :cond_4
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 317
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 553
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mAccountName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 554
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKeyType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 555
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mExternalKey:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 556
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mConversationId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 557
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutDomain:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 558
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 559
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mPendingIdKey:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 560
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mHangoutType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 561
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mCallMediaType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 562
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mInviteeNick:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 563
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->mCallCompleteIntent:Landroid/app/PendingIntent;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 564
    return-void
.end method
