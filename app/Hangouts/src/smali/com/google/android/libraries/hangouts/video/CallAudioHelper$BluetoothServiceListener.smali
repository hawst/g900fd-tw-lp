.class Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothServiceListener;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/bluetooth/BluetoothProfile$ServiceListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;


# direct methods
.method private constructor <init>(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothServiceListener;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;Lcom/google/android/libraries/hangouts/video/CallAudioHelper$1;)V
    .locals 0

    .prologue
    .line 100
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothServiceListener;-><init>(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)V

    return-void
.end method


# virtual methods
.method public onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V
    .locals 2

    .prologue
    .line 103
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothServiceListener;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    const-string v1, "BluetoothProfile.ServiceListener : onServiceConnected"

    # invokes: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->log(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$100(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;Ljava/lang/String;)V

    .line 105
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothServiceListener;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    check-cast p2, Landroid/bluetooth/BluetoothHeadset;

    # setter for: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;
    invoke-static {v0, p2}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$202(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;Landroid/bluetooth/BluetoothHeadset;)Landroid/bluetooth/BluetoothHeadset;

    .line 107
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothServiceListener;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$300(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDeviceState;->BLUETOOTH_TURNING_ON:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    if-ne v0, v1, :cond_0

    .line 108
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothServiceListener;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    # invokes: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->startBluetoothSco()V
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$400(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)V

    .line 111
    :cond_0
    return-void
.end method

.method public onServiceDisconnected(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 115
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothServiceListener;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    const-string v1, "BluetoothProfile.ServiceListener : onServiceDisconnected"

    # invokes: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->log(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$100(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;Ljava/lang/String;)V

    .line 117
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothServiceListener;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    # invokes: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->stopBluetoothSco()V
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$500(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)V

    .line 118
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothServiceListener;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    # setter for: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothDevice:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v0, v2}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$602(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;

    .line 119
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothServiceListener;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    # setter for: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;
    invoke-static {v0, v2}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$202(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;Landroid/bluetooth/BluetoothHeadset;)Landroid/bluetooth/BluetoothHeadset;

    .line 122
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothServiceListener;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDevices:Ljava/util/Set;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$700(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDevice;->BLUETOOTH_HEADSET:Lcom/google/android/libraries/hangouts/video/AudioDevice;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothServiceListener;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->mAudioDevices:Ljava/util/Set;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$700(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/hangouts/video/AudioDevice;->BLUETOOTH_HEADSET:Lcom/google/android/libraries/hangouts/video/AudioDevice;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 124
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallAudioHelper$BluetoothServiceListener;->this$0:Lcom/google/android/libraries/hangouts/video/CallAudioHelper;

    # invokes: Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->reportUpdate()V
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallAudioHelper;->access$800(Lcom/google/android/libraries/hangouts/video/CallAudioHelper;)V

    .line 127
    :cond_0
    return-void
.end method
