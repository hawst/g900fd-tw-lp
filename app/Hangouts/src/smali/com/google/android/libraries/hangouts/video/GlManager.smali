.class public Lcom/google/android/libraries/hangouts/video/GlManager;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final TAG:Ljava/lang/String; = "vclib"


# instance fields
.field private final eventQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final glHelper:Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;

.field private final glThread:Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;

.field private final outputRenderers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/libraries/hangouts/video/SurfaceInfo;",
            "Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;",
            ">;"
        }
    .end annotation
.end field

.field private final videoSources:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/hangouts/video/VideoSource;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager;->outputRenderers:Ljava/util/Map;

    .line 34
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager;->videoSources:Ljava/util/List;

    .line 35
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager;->eventQueue:Ljava/util/Queue;

    .line 40
    new-instance v0, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;-><init>(Lcom/google/android/libraries/hangouts/video/GlManager;Lcom/google/android/libraries/hangouts/video/GlManager$1;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager;->glHelper:Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;

    .line 41
    new-instance v0, Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;-><init>(Lcom/google/android/libraries/hangouts/video/GlManager;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager;->glThread:Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;

    .line 42
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/libraries/hangouts/video/GlManager;)Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager;->glHelper:Lcom/google/android/libraries/hangouts/video/GlManager$GlHelper;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/libraries/hangouts/video/GlManager;)Ljava/util/Queue;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager;->eventQueue:Ljava/util/Queue;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/libraries/hangouts/video/GlManager;)Ljava/util/List;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager;->videoSources:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/libraries/hangouts/video/GlManager;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager;->outputRenderers:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public addRenderingTarget(Lcom/google/android/libraries/hangouts/video/SurfaceInfo;Lcom/google/android/libraries/hangouts/video/VideoSource;)V
    .locals 2

    .prologue
    .line 59
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/SurfaceInfo;->getWidth()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/SurfaceInfo;->getHeight()I

    move-result v0

    if-nez v0, :cond_1

    .line 60
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid surface"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 62
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager;->outputRenderers:Ljava/util/Map;

    new-instance v1, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;-><init>(Lcom/google/android/libraries/hangouts/video/GlManager;Lcom/google/android/libraries/hangouts/video/SurfaceInfo;Lcom/google/android/libraries/hangouts/video/VideoSource;)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    return-void
.end method

.method public addVideoSource(Lcom/google/android/libraries/hangouts/video/VideoSource;)V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager;->videoSources:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    return-void
.end method

.method public queueEvent(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager;->eventQueue:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 89
    return-void
.end method

.method public removeRenderingTarget(Lcom/google/android/libraries/hangouts/video/SurfaceInfo;)V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager;->outputRenderers:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;

    .line 70
    if-eqz v0, :cond_0

    .line 71
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/GlManager$OutputRenderer;->release()V

    .line 74
    :cond_0
    return-void
.end method

.method public removeVideoSource(Lcom/google/android/libraries/hangouts/video/VideoSource;)V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager;->videoSources:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 82
    return-void
.end method

.method public start()V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager;->glThread:Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;->start()V

    .line 46
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/GlManager;->glThread:Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/GlManager$GlThread;->interrupt()V

    .line 53
    return-void
.end method
