.class public Lcom/google/android/libraries/hangouts/video/LocalVideoSource;
.super Lcom/google/android/libraries/hangouts/video/VideoSource;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;


# instance fields
.field private cameraIsOpened:Z

.field private captureHeight:I

.field private captureWidth:I

.field private final gl:Lcom/google/android/libraries/hangouts/video/GlManager;

.field private initialized:Z

.field private isDirty:Z

.field private isExternalTexture:Z

.field private orientation:I

.field private final selfRenderer:Lcom/google/android/libraries/hangouts/video/SelfRendererICS;

.field private shouldFlipTexture:Z

.field private textureName:I

.field private final wm:Landroid/view/WindowManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/libraries/hangouts/video/RendererManager;Lcom/google/android/libraries/hangouts/video/GlManager;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 31
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/VideoSource;-><init>()V

    .line 21
    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->initialized:Z

    .line 22
    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->cameraIsOpened:Z

    .line 23
    iput-boolean v2, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->isDirty:Z

    .line 24
    iput v0, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->textureName:I

    .line 25
    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->shouldFlipTexture:Z

    .line 26
    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->isExternalTexture:Z

    .line 29
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->orientation:I

    .line 32
    iput-object p3, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->gl:Lcom/google/android/libraries/hangouts/video/GlManager;

    .line 33
    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->wm:Landroid/view/WindowManager;

    .line 35
    invoke-virtual {p2, p0, v3}, Lcom/google/android/libraries/hangouts/video/RendererManager;->createSelfRenderer(Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;Lcom/google/android/libraries/hangouts/video/CameraSpecification;)Lcom/google/android/libraries/hangouts/video/SelfRendererICS;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->selfRenderer:Lcom/google/android/libraries/hangouts/video/SelfRendererICS;

    .line 37
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->selfRenderer:Lcom/google/android/libraries/hangouts/video/SelfRendererICS;

    new-instance v1, Lcom/google/android/libraries/hangouts/video/CameraSpecification;

    invoke-direct {v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/CameraSpecification;-><init>(ILcom/google/android/libraries/hangouts/video/Size;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->useCamera(Lcom/google/android/libraries/hangouts/video/CameraSpecification;)V

    .line 38
    return-void
.end method

.method private getOrientation()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 71
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->wm:Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getRotation()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 81
    const-string v1, "vclib"

    const-string v2, "Bad rotation"

    invoke-static {v1, v2}, Lcxc;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    :goto_0
    :pswitch_0
    return v0

    .line 75
    :pswitch_1
    const/16 v0, 0x5a

    goto :goto_0

    .line 77
    :pswitch_2
    const/16 v0, 0xb4

    goto :goto_0

    .line 79
    :pswitch_3
    const/16 v0, 0x10e

    goto :goto_0

    .line 71
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method getHeight()I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->captureHeight:I

    return v0
.end method

.method public getIdealCaptureSize()Lcom/google/android/libraries/hangouts/video/Size;
    .locals 1

    .prologue
    .line 152
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->getOutgoingNoEffectsVideoSpec()Lcom/google/android/libraries/hangouts/video/VideoSpecification;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/VideoSpecification;->getSize()Lcom/google/android/libraries/hangouts/video/Size;

    move-result-object v0

    return-object v0
.end method

.method getTextureName()I
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->textureName:I

    return v0
.end method

.method getWidth()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->captureWidth:I

    return v0
.end method

.method isDirty()Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->isDirty:Z

    return v0
.end method

.method isExternalTexture()Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->isExternalTexture:Z

    return v0
.end method

.method public onCameraOpenError(Ljava/lang/Exception;)V
    .locals 0

    .prologue
    .line 148
    return-void
.end method

.method public onCameraOpened(Z)V
    .locals 1

    .prologue
    .line 141
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->cameraIsOpened:Z

    .line 142
    iput-boolean p1, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->shouldFlipTexture:Z

    .line 143
    return-void
.end method

.method public onFrameGeometryChanged(IIII)V
    .locals 0

    .prologue
    .line 135
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->captureWidth:I

    .line 136
    iput p2, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->captureHeight:I

    .line 137
    return-void
.end method

.method public onOutputTextureNameChanged(I)V
    .locals 1

    .prologue
    .line 128
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->textureName:I

    .line 129
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->isDirty:Z

    .line 130
    return-void
.end method

.method processFrame()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 42
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->initialized:Z

    if-nez v0, :cond_2

    .line 43
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->selfRenderer:Lcom/google/android/libraries/hangouts/video/SelfRendererICS;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->initializeGLContext()V

    .line 44
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->selfRenderer:Lcom/google/android/libraries/hangouts/video/SelfRendererICS;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->getOutputTextureName()I

    move-result v0

    if-nez v0, :cond_1

    .line 68
    :cond_0
    :goto_0
    return-void

    .line 47
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->selfRenderer:Lcom/google/android/libraries/hangouts/video/SelfRendererICS;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->setUseMaxSizeForCameraBuffer(Z)V

    .line 48
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->selfRenderer:Lcom/google/android/libraries/hangouts/video/SelfRendererICS;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->getOutputTextureName()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->textureName:I

    .line 49
    iput-boolean v2, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->initialized:Z

    .line 52
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->cameraIsOpened:Z

    if-eqz v0, :cond_0

    .line 56
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->getOrientation()I

    move-result v0

    .line 57
    iget v1, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->orientation:I

    if-eq v0, v1, :cond_3

    .line 58
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->selfRenderer:Lcom/google/android/libraries/hangouts/video/SelfRendererICS;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->setDeviceOrientation(I)V

    .line 59
    iput v0, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->orientation:I

    .line 62
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->selfRenderer:Lcom/google/android/libraries/hangouts/video/SelfRendererICS;

    invoke-virtual {v0, v3, v3}, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->drawTexture(Lcom/google/android/libraries/hangouts/video/Renderer$DrawInputParams;Lcom/google/android/libraries/hangouts/video/Renderer$DrawOutputParams;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 63
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->selfRenderer:Lcom/google/android/libraries/hangouts/video/SelfRendererICS;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->encodeFrame()V

    .line 64
    iput-boolean v2, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->isDirty:Z

    goto :goto_0

    .line 66
    :cond_4
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->isDirty:Z

    goto :goto_0
.end method

.method public queueEventForGLThread(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->gl:Lcom/google/android/libraries/hangouts/video/GlManager;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/GlManager;->queueEvent(Ljava/lang/Runnable;)V

    .line 124
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->selfRenderer:Lcom/google/android/libraries/hangouts/video/SelfRendererICS;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/SelfRendererICS;->release()V

    .line 89
    return-void
.end method

.method shouldFlipTexture()Z
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/LocalVideoSource;->shouldFlipTexture:Z

    return v0
.end method
