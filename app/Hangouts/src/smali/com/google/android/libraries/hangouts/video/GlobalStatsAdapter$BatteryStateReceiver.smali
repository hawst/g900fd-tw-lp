.class Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter$BatteryStateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# instance fields
.field private mBatteryLevel:I

.field private mIsOnBattery:Z

.field final synthetic this$0:Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter;)V
    .locals 1

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter$BatteryStateReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter$BatteryStateReceiver;->mIsOnBattery:Z

    .line 56
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter$BatteryStateReceiver;->mBatteryLevel:I

    .line 57
    return-void
.end method


# virtual methods
.method public getBatteryLevel()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter$BatteryStateReceiver;->mBatteryLevel:I

    return v0
.end method

.method public getIsOnBattery()Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter$BatteryStateReceiver;->mIsOnBattery:Z

    return v0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 69
    const-string v0, "plugged"

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 70
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter$BatteryStateReceiver;->mIsOnBattery:Z

    .line 72
    const-string v0, "scale"

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 73
    const-string v2, "level"

    invoke-virtual {p2, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 74
    if-eqz v0, :cond_1

    .line 75
    mul-int/lit8 v1, v1, 0x64

    div-int v0, v1, v0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter$BatteryStateReceiver;->mBatteryLevel:I

    .line 79
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 70
    goto :goto_0

    .line 77
    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/GlobalStatsAdapter$BatteryStateReceiver;->mBatteryLevel:I

    goto :goto_1
.end method
