.class public Lcom/google/android/libraries/hangouts/video/CameraInterface;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final CAMERA_OPEN_THREAD_NAME:Ljava/lang/String; = "CameraOpenThread"

.field public static final INVALID_CAMERA_ID:I = -0x1

.field private static volatile sInstance:Lcom/google/android/libraries/hangouts/video/CameraInterface;

.field private static final sInstanceLock:Ljava/lang/Object;


# instance fields
.field private final mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/libraries/hangouts/video/CameraInterface;->sInstanceLock:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/libraries/hangouts/video/CameraManager;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CameraInterface;->mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManager;

    .line 24
    return-void
.end method

.method public static getInstance()Lcom/google/android/libraries/hangouts/video/CameraInterface;
    .locals 3

    .prologue
    .line 27
    sget-object v1, Lcom/google/android/libraries/hangouts/video/CameraInterface;->sInstanceLock:Ljava/lang/Object;

    monitor-enter v1

    .line 28
    :try_start_0
    sget-object v0, Lcom/google/android/libraries/hangouts/video/CameraInterface;->sInstance:Lcom/google/android/libraries/hangouts/video/CameraInterface;

    if-nez v0, :cond_0

    .line 29
    new-instance v0, Lcom/google/android/libraries/hangouts/video/CameraInterface;

    invoke-static {}, Lcom/google/android/libraries/hangouts/video/CameraManager;->getInstance()Lcom/google/android/libraries/hangouts/video/CameraManager;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/libraries/hangouts/video/CameraInterface;-><init>(Lcom/google/android/libraries/hangouts/video/CameraManager;)V

    sput-object v0, Lcom/google/android/libraries/hangouts/video/CameraInterface;->sInstance:Lcom/google/android/libraries/hangouts/video/CameraInterface;

    .line 31
    :cond_0
    sget-object v0, Lcom/google/android/libraries/hangouts/video/CameraInterface;->sInstance:Lcom/google/android/libraries/hangouts/video/CameraInterface;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 32
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public arePreview3ALocksSupported()Z
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraInterface;->mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManager;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CameraManager;->arePreview3ALocksSupported()Z

    move-result v0

    return v0
.end method

.method public getCameraCount()I
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraInterface;->mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManager;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CameraManager;->getCameraCount()I

    move-result v0

    return v0
.end method

.method getCameraManager()Lcom/google/android/libraries/hangouts/video/CameraManager;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraInterface;->mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManager;

    return-object v0
.end method

.method public getCurrentCameraId()I
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraInterface;->mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManager;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CameraManager;->getCurrentCameraId()I

    move-result v0

    return v0
.end method

.method public getFrontCameraCount()I
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraInterface;->mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManager;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CameraManager;->getFrontCameraCount()I

    move-result v0

    return v0
.end method

.method public hasFrontCamera()Z
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraInterface;->mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManager;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CameraManager;->hasFrontCamera()Z

    move-result v0

    return v0
.end method

.method public resetCurrentCamera()V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraInterface;->mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManager;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CameraManager;->resetCurrentCamera()V

    .line 78
    return-void
.end method

.method public setPreview3ALocks(Z)V
    .locals 1

    .prologue
    .line 89
    invoke-static {}, Lcwz;->c()V

    .line 90
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraInterface;->mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManager;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/CameraManager;->setPreview3ALocks(Z)V

    .line 91
    return-void
.end method

.method public suspendCamera()V
    .locals 1

    .prologue
    .line 72
    invoke-static {}, Lcwz;->a()V

    .line 73
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraInterface;->mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManager;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CameraManager;->suspendCamera()V

    .line 74
    return-void
.end method

.method public useCamera(Lcom/google/android/libraries/hangouts/video/CameraSpecification;)V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraInterface;->mCameraManager:Lcom/google/android/libraries/hangouts/video/CameraManager;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/CameraManager;->useCamera(Lcom/google/android/libraries/hangouts/video/CameraSpecification;)V

    .line 58
    return-void
.end method
