.class public Lcom/google/android/libraries/hangouts/video/endpoint/ExitEvent;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/hangouts/video/endpoint/EndpointEvent;


# instance fields
.field private final mExitEventErrorCode:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Ljava/lang/Integer;)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/endpoint/ExitEvent;->mExitEventErrorCode:Ljava/lang/Integer;

    .line 13
    return-void
.end method


# virtual methods
.method public getExitEventErrorCode()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/endpoint/ExitEvent;->mExitEventErrorCode:Ljava/lang/Integer;

    return-object v0
.end method
