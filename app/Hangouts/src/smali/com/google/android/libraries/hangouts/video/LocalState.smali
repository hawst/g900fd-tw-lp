.class public final Lcom/google/android/libraries/hangouts/video/LocalState;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final SIGNIN_CONNECTED:I = 0x2

.field public static final SIGNIN_CONNECTING:I = 0x1

.field public static final SIGNIN_DISCONNECTED:I = 0x0

.field public static final SIGNIN_DISCONNECTING:I = 0x3


# instance fields
.field private mAccountName:Ljava/lang/String;

.field mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

.field mAvailableAudioDevices:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/libraries/hangouts/video/AudioDevice;",
            ">;"
        }
    .end annotation
.end field

.field private mJid:Ljava/lang/String;

.field private mSignalingNetworkType:I

.field private mSigninState:I

.field private mTokenInvalidated:Z


# direct methods
.method constructor <init>(Lcom/google/android/libraries/hangouts/video/AudioDeviceState;Ljava/util/Set;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/hangouts/video/AudioDeviceState;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/libraries/hangouts/video/AudioDevice;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 38
    const/16 v7, 0x8

    move-object v0, p0

    move-object v2, v1

    move-object v4, p1

    move-object v5, p2

    move v6, v3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/libraries/hangouts/video/LocalState;-><init>(Ljava/lang/String;Ljava/lang/String;ILcom/google/android/libraries/hangouts/video/AudioDeviceState;Ljava/util/Set;ZI)V

    .line 40
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;ILcom/google/android/libraries/hangouts/video/AudioDeviceState;Ljava/util/Set;ZI)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Lcom/google/android/libraries/hangouts/video/AudioDeviceState;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/libraries/hangouts/video/AudioDevice;",
            ">;ZI)V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/16 v0, 0x8

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/LocalState;->mSignalingNetworkType:I

    .line 45
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/LocalState;->mAccountName:Ljava/lang/String;

    .line 46
    iput-object p2, p0, Lcom/google/android/libraries/hangouts/video/LocalState;->mJid:Ljava/lang/String;

    .line 47
    iput p3, p0, Lcom/google/android/libraries/hangouts/video/LocalState;->mSigninState:I

    .line 48
    iput-object p4, p0, Lcom/google/android/libraries/hangouts/video/LocalState;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    .line 49
    iput-object p5, p0, Lcom/google/android/libraries/hangouts/video/LocalState;->mAvailableAudioDevices:Ljava/util/Set;

    .line 50
    iput-boolean p6, p0, Lcom/google/android/libraries/hangouts/video/LocalState;->mTokenInvalidated:Z

    .line 51
    iput p7, p0, Lcom/google/android/libraries/hangouts/video/LocalState;->mSignalingNetworkType:I

    .line 52
    return-void
.end method


# virtual methods
.method getAccountName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/LocalState;->mAccountName:Ljava/lang/String;

    return-object v0
.end method

.method public getAudioDeviceState()Lcom/google/android/libraries/hangouts/video/AudioDeviceState;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/LocalState;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    return-object v0
.end method

.method public getAvailableAudioDevices()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/libraries/hangouts/video/AudioDevice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/LocalState;->mAvailableAudioDevices:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getJidResource()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/LocalState;->mJid:Ljava/lang/String;

    invoke-static {v0}, Lf;->r(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getSignalingNetworkType()I
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/LocalState;->mSignalingNetworkType:I

    return v0
.end method

.method getSigninState()I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/LocalState;->mSigninState:I

    return v0
.end method

.method setAccountName(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/LocalState;->mAccountName:Ljava/lang/String;

    invoke-static {v0}, Lcwz;->a(Ljava/lang/Object;)V

    .line 82
    invoke-static {p1}, Lf;->q(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 83
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/LocalState;->mAccountName:Ljava/lang/String;

    .line 84
    return-void
.end method

.method setAudioState(Lcom/google/android/libraries/hangouts/video/AudioDeviceState;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/hangouts/video/AudioDeviceState;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/libraries/hangouts/video/AudioDevice;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 56
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/LocalState;->mAudioDeviceState:Lcom/google/android/libraries/hangouts/video/AudioDeviceState;

    .line 57
    iput-object p2, p0, Lcom/google/android/libraries/hangouts/video/LocalState;->mAvailableAudioDevices:Ljava/util/Set;

    .line 58
    return-void
.end method

.method setFullJid(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/LocalState;->mAccountName:Ljava/lang/String;

    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 91
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/LocalState;->mAccountName:Ljava/lang/String;

    invoke-static {p1}, Lf;->q(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/LocalState;->mAccountName:Ljava/lang/String;

    const-string v1, "gmail.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Lcwz;->b(Z)V

    .line 95
    :cond_0
    const-string v0, "vclib"

    const-string v1, "Setting jid for %s to %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/LocalState;->mAccountName:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/LocalState;->mJid:Ljava/lang/String;

    .line 97
    return-void
.end method

.method setSignalingNetworkType(I)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v0, 0x0

    .line 125
    const/16 v1, 0x9

    invoke-static {p1, v0, v1}, Lcwz;->a(III)V

    .line 127
    iget v1, p0, Lcom/google/android/libraries/hangouts/video/LocalState;->mSigninState:I

    if-nez v1, :cond_0

    if-ne p1, v3, :cond_1

    :cond_0
    iget v1, p0, Lcom/google/android/libraries/hangouts/video/LocalState;->mSigninState:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    if-ne p1, v3, :cond_2

    :cond_1
    const/4 v0, 0x1

    :cond_2
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 131
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/LocalState;->mSignalingNetworkType:I

    .line 132
    return-void
.end method

.method setSigninState(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 108
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-static {p1, v0, v1}, Lcwz;->a(III)V

    .line 109
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/LocalState;->mSigninState:I

    .line 110
    if-nez p1, :cond_0

    .line 111
    iput-object v2, p0, Lcom/google/android/libraries/hangouts/video/LocalState;->mAccountName:Ljava/lang/String;

    .line 112
    iput-object v2, p0, Lcom/google/android/libraries/hangouts/video/LocalState;->mJid:Ljava/lang/String;

    .line 114
    :cond_0
    return-void
.end method

.method setTokenInvalidated()V
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/LocalState;->mTokenInvalidated:Z

    .line 140
    return-void
.end method

.method wasTokenInvalidated()Z
    .locals 1

    .prologue
    .line 135
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/LocalState;->mTokenInvalidated:Z

    return v0
.end method
