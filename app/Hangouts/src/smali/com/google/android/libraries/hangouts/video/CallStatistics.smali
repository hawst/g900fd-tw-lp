.class public Lcom/google/android/libraries/hangouts/video/CallStatistics;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final MAX_STATS_UPDATES_TO_KEEP:I = 0x4b0

.field private static final MMS_PER_INCH:F = 25.4f


# instance fields
.field private mCallAccepted:Z

.field private mCallElapsedRealtimeAtStart:J

.field private mCallStartTime:J

.field private final mSessionId:Ljava/lang/String;

.field private final mStatsUpdates:Lcxa;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcxa",
            "<",
            "Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CallStatistics;->mSessionId:Ljava/lang/String;

    .line 75
    new-instance v0, Lcxa;

    const/16 v1, 0x4b0

    invoke-direct {v0, v1}, Lcxa;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallStatistics;->mStatsUpdates:Lcxa;

    .line 76
    return-void
.end method

.method private static endCauseMapping(I)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x2

    .line 401
    const/4 v0, 0x6

    .line 402
    sparse-switch p0, :sswitch_data_0

    .line 462
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected endCause:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcwz;->a(Ljava/lang/String;)V

    .line 465
    :goto_0
    return v0

    .line 404
    :sswitch_0
    const-string v1, "endCause is not set"

    invoke-static {v1}, Lcwz;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 407
    :sswitch_1
    const/16 v0, 0x1d

    .line 408
    goto :goto_0

    .line 411
    :sswitch_2
    const/16 v0, 0x2f

    .line 412
    goto :goto_0

    .line 414
    :sswitch_3
    const/16 v0, 0x2e

    .line 415
    goto :goto_0

    .line 417
    :sswitch_4
    const/16 v0, 0xa

    .line 418
    goto :goto_0

    .line 420
    :sswitch_5
    const/16 v0, 0x16

    .line 421
    goto :goto_0

    .line 423
    :sswitch_6
    const/16 v0, 0x25

    .line 424
    goto :goto_0

    :sswitch_7
    move v0, v1

    .line 427
    goto :goto_0

    .line 429
    :sswitch_8
    const/16 v0, 0x12

    .line 430
    goto :goto_0

    .line 432
    :sswitch_9
    const/16 v0, 0x3d

    .line 433
    goto :goto_0

    .line 435
    :sswitch_a
    const/16 v0, 0x3e

    .line 436
    goto :goto_0

    .line 439
    :sswitch_b
    const/16 v0, 0xf

    .line 440
    goto :goto_0

    .line 442
    :sswitch_c
    const/16 v0, 0x1f

    .line 443
    goto :goto_0

    :sswitch_d
    move v0, v2

    .line 446
    goto :goto_0

    :sswitch_e
    move v0, v2

    .line 450
    goto :goto_0

    :sswitch_f
    move v0, v2

    .line 454
    goto :goto_0

    .line 456
    :sswitch_10
    const/16 v0, 0x3c

    .line 457
    goto :goto_0

    :sswitch_11
    move v0, v1

    .line 460
    goto :goto_0

    .line 402
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x3 -> :sswitch_2
        0xa -> :sswitch_2
        0x15 -> :sswitch_4
        0x16 -> :sswitch_5
        0x18 -> :sswitch_6
        0x3e8 -> :sswitch_3
        0x3eb -> :sswitch_c
        0x3ec -> :sswitch_7
        0x3ed -> :sswitch_9
        0x3ee -> :sswitch_a
        0x3ef -> :sswitch_b
        0x3f1 -> :sswitch_8
        0x3f2 -> :sswitch_d
        0x3f3 -> :sswitch_e
        0x3f4 -> :sswitch_10
        0x3f6 -> :sswitch_f
        0x3f7 -> :sswitch_11
    .end sparse-switch
.end method

.method private getCallPerf(Ljava/lang/String;II)Ldoc;
    .locals 14

    .prologue
    .line 469
    new-instance v6, Ldoc;

    invoke-direct {v6}, Ldoc;-><init>()V

    .line 470
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallStatistics;->mSessionId:Ljava/lang/String;

    iput-object v0, v6, Ldoc;->b:Ljava/lang/String;

    .line 471
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "EEE MMM d HH:mm:ss yyyy"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 472
    const-string v1, "GMT"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 473
    iget-wide v1, p0, Lcom/google/android/libraries/hangouts/video/CallStatistics;->mCallStartTime:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Ldoc;->c:Ljava/lang/String;

    .line 474
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/libraries/hangouts/video/CallStatistics;->mCallElapsedRealtimeAtStart:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    .line 475
    div-int/lit16 v0, v0, 0x3e8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v6, Ldoc;->d:Ljava/lang/Integer;

    .line 476
    iput-object p1, v6, Ldoc;->e:Ljava/lang/String;

    .line 477
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/CallStatistics;->mCallAccepted:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v6, Ldoc;->h:Ljava/lang/Boolean;

    .line 479
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v6, Ldoc;->i:Ljava/lang/Integer;

    .line 481
    invoke-static/range {p2 .. p2}, Lcom/google/android/libraries/hangouts/video/CallStatistics;->endCauseMapping(I)I

    move-result v0

    .line 482
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v6, Ldoc;->j:Ljava/lang/Integer;

    .line 484
    iget-object v7, p0, Lcom/google/android/libraries/hangouts/video/CallStatistics;->mStatsUpdates:Lcxa;

    monitor-enter v7

    .line 485
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallStatistics;->mStatsUpdates:Lcxa;

    invoke-virtual {v0}, Lcxa;->a()I

    move-result v8

    .line 487
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 489
    const/4 v3, 0x0

    .line 490
    const-wide/16 v1, -0x1

    .line 492
    const/4 v0, 0x0

    move-object v4, v3

    move-wide v12, v1

    move-wide v2, v12

    move v1, v0

    :goto_0
    if-ge v1, v8, :cond_2

    .line 493
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallStatistics;->mStatsUpdates:Lcxa;

    invoke-virtual {v0, v1}, Lcxa;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;

    .line 495
    # getter for: Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;->mSecondsSinceCallStart:J
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;->access$200(Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;)J

    move-result-wide v10

    cmp-long v5, v10, v2

    if-nez v5, :cond_0

    move-object v5, v4

    .line 507
    :goto_1
    # getter for: Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;->mStatsObject:Lcom/google/android/libraries/hangouts/video/Stats;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;->access$100(Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;)Lcom/google/android/libraries/hangouts/video/Stats;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/libraries/hangouts/video/Stats;->addTo(Ldoe;)V

    .line 492
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 498
    :cond_0
    if-eqz v4, :cond_1

    .line 501
    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 503
    :cond_1
    new-instance v4, Ldoe;

    invoke-direct {v4}, Ldoe;-><init>()V

    .line 504
    # getter for: Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;->mSecondsSinceCallStart:J
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;->access$200(Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;)J

    move-result-wide v2

    long-to-int v2, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v4, Ldoe;->b:Ljava/lang/Integer;

    .line 505
    # getter for: Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;->mSecondsSinceCallStart:J
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;->access$200(Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;)J

    move-result-wide v2

    move-object v5, v4

    goto :goto_1

    .line 510
    :cond_2
    if-nez v4, :cond_4

    const/4 v0, 0x1

    move v1, v0

    :goto_2
    if-nez v8, :cond_5

    const/4 v0, 0x1

    :goto_3
    if-ne v1, v0, :cond_6

    const/4 v0, 0x1

    :goto_4
    invoke-static {v0}, Lcwz;->a(Z)V

    .line 511
    if-eqz v4, :cond_3

    .line 512
    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 514
    :cond_3
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ldoe;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldoe;

    iput-object v0, v6, Ldoc;->g:[Ldoe;

    .line 515
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 517
    return-object v6

    .line 510
    :cond_4
    const/4 v0, 0x0

    move v1, v0

    goto :goto_2

    :cond_5
    const/4 v0, 0x0

    goto :goto_3

    :cond_6
    const/4 v0, 0x0

    goto :goto_4

    .line 515
    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0
.end method

.method public static getMappedEndCause(I)I
    .locals 1

    .prologue
    .line 195
    invoke-static {p0}, Lcom/google/android/libraries/hangouts/video/CallStatistics;->endCauseMapping(I)I

    move-result v0

    return v0
.end method

.method private getMobileDeviceInfo(Landroid/content/Context;)Ldot;
    .locals 6

    .prologue
    const v5, 0x41cb3333    # 25.4f

    .line 214
    new-instance v1, Ldot;

    invoke-direct {v1}, Ldot;-><init>()V

    .line 215
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 218
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v1, Ldot;->b:Ljava/lang/Boolean;

    .line 220
    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 222
    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    .line 223
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x11

    if-lt v3, v4, :cond_1

    .line 224
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Display;->getRealMetrics(Landroid/util/DisplayMetrics;)V

    .line 228
    :goto_1
    iget v0, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v0, v0

    iget v3, v2, Landroid/util/DisplayMetrics;->xdpi:F

    div-float/2addr v0, v3

    .line 229
    iget v3, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v3, v3

    iget v2, v2, Landroid/util/DisplayMetrics;->ydpi:F

    div-float v2, v3, v2

    .line 230
    mul-float/2addr v0, v5

    float-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldot;->c:Ljava/lang/Integer;

    .line 231
    mul-float v0, v2, v5

    float-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldot;->d:Ljava/lang/Integer;

    .line 233
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/CameraInterface;->getInstance()Lcom/google/android/libraries/hangouts/video/CameraInterface;

    move-result-object v0

    .line 234
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CameraInterface;->getFrontCameraCount()I

    move-result v2

    .line 235
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v1, Ldot;->e:Ljava/lang/Integer;

    .line 236
    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/CameraInterface;->getCameraCount()I

    move-result v0

    sub-int/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldot;->f:Ljava/lang/Integer;

    .line 238
    return-object v1

    .line 218
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 226
    :cond_1
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    goto :goto_1
.end method

.method private getStartupEntry(Lcom/google/android/libraries/hangouts/video/HangoutRequest;IILjava/lang/String;IZ)Ldok;
    .locals 5

    .prologue
    const/16 v0, 0x131

    const/16 v2, 0xdb

    const/4 v1, -0x1

    .line 255
    new-instance v3, Ldok;

    invoke-direct {v3}, Ldok;-><init>()V

    .line 257
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, v3, Ldok;->f:Ljava/lang/Integer;

    .line 260
    if-eqz p6, :cond_3

    .line 261
    const/4 v0, 0x0

    .line 363
    :goto_0
    :sswitch_0
    if-eq v0, v1, :cond_0

    .line 364
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v3, Ldok;->b:Ljava/lang/Integer;

    .line 367
    :cond_0
    new-instance v0, Ldor;

    invoke-direct {v0}, Ldor;-><init>()V

    .line 368
    if-eqz p1, :cond_1

    .line 369
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->hasExternalKey()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 370
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getExternalKeyType()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Ldor;->b:Ljava/lang/String;

    .line 371
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getExternalKey()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Ldor;->c:Ljava/lang/String;

    .line 379
    :cond_1
    :goto_1
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Ldor;->h:Ljava/lang/Integer;

    .line 381
    iput-object v0, v3, Ldok;->c:Ldor;

    .line 383
    if-eqz p4, :cond_2

    .line 384
    invoke-static {p4}, Lf;->q(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 385
    iput-object v0, v3, Ldok;->d:Ljava/lang/String;

    .line 388
    :cond_2
    return-object v3

    .line 263
    :cond_3
    sparse-switch p5, :sswitch_data_0

    .line 358
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected endCause:"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    .line 265
    :sswitch_1
    const-string v0, "endCause is not set"

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    move v0, v1

    .line 266
    goto :goto_0

    .line 268
    :sswitch_2
    const/16 v0, 0x12e

    .line 269
    goto :goto_0

    .line 271
    :sswitch_3
    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->hasExternalKey()Z

    move-result v0

    if-nez v0, :cond_4

    .line 272
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getDomain()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 273
    :cond_4
    const/16 v0, 0x132

    goto :goto_0

    .line 275
    :cond_5
    const/16 v0, 0xd9

    .line 277
    goto :goto_0

    .line 280
    :sswitch_4
    const/16 v0, 0x130

    .line 281
    goto :goto_0

    .line 283
    :sswitch_5
    const/16 v0, 0x134

    .line 284
    goto :goto_0

    .line 286
    :sswitch_6
    const/16 v0, 0x135

    .line 287
    goto :goto_0

    .line 289
    :sswitch_7
    const/16 v0, 0x136

    .line 290
    goto :goto_0

    .line 292
    :sswitch_8
    const/16 v0, 0x138

    .line 293
    goto :goto_0

    .line 295
    :sswitch_9
    const/16 v0, 0x139

    .line 296
    goto :goto_0

    .line 298
    :sswitch_a
    const/16 v0, 0x13a

    .line 299
    goto :goto_0

    .line 301
    :sswitch_b
    const/16 v0, 0x13c

    .line 302
    goto :goto_0

    .line 304
    :sswitch_c
    const/16 v0, 0xc9

    .line 305
    goto/16 :goto_0

    .line 307
    :sswitch_d
    const/16 v0, 0xca

    .line 308
    goto/16 :goto_0

    .line 310
    :sswitch_e
    const/16 v0, 0xd8

    .line 311
    goto/16 :goto_0

    .line 313
    :sswitch_f
    const/16 v0, 0x133

    .line 314
    goto/16 :goto_0

    .line 316
    :sswitch_10
    const/16 v0, 0x12f

    .line 317
    goto/16 :goto_0

    .line 320
    :sswitch_11
    const/16 v0, 0xd4

    .line 321
    goto/16 :goto_0

    .line 324
    :sswitch_12
    const-string v0, "Should not happen"

    invoke-static {v0}, Lcwz;->a(Ljava/lang/String;)V

    move v0, v1

    .line 325
    goto/16 :goto_0

    .line 327
    :sswitch_13
    const/16 v0, 0xce

    .line 328
    goto/16 :goto_0

    .line 334
    :sswitch_14
    const/16 v0, 0x12c

    .line 335
    goto/16 :goto_0

    .line 337
    :sswitch_15
    const/16 v0, 0xd2

    .line 338
    goto/16 :goto_0

    .line 340
    :sswitch_16
    const/16 v0, 0xd0

    .line 341
    goto/16 :goto_0

    :sswitch_17
    move v0, v2

    .line 347
    goto/16 :goto_0

    .line 349
    :sswitch_18
    const/16 v0, 0xd1

    .line 350
    goto/16 :goto_0

    .line 352
    :sswitch_19
    const/16 v0, 0xde

    .line 353
    goto/16 :goto_0

    :sswitch_1a
    move v0, v2

    .line 356
    goto/16 :goto_0

    .line 372
    :cond_6
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getDomain()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 373
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getDomain()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Ldor;->e:Ljava/lang/String;

    .line 374
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getHangoutId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Ldor;->f:Ljava/lang/String;

    goto/16 :goto_1

    .line 376
    :cond_7
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/HangoutRequest;->getHangoutId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Ldor;->g:Ljava/lang/String;

    goto/16 :goto_1

    .line 263
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x1 -> :sswitch_2
        0x2 -> :sswitch_3
        0x3 -> :sswitch_4
        0x4 -> :sswitch_5
        0x5 -> :sswitch_6
        0x6 -> :sswitch_7
        0x7 -> :sswitch_8
        0x8 -> :sswitch_9
        0x9 -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_18
        0x13 -> :sswitch_12
        0x14 -> :sswitch_13
        0x15 -> :sswitch_0
        0x16 -> :sswitch_0
        0x19 -> :sswitch_12
        0x1a -> :sswitch_19
        0x3e8 -> :sswitch_14
        0x3e9 -> :sswitch_15
        0x3ea -> :sswitch_16
        0x3eb -> :sswitch_0
        0x3ec -> :sswitch_17
        0x3f5 -> :sswitch_1a
    .end sparse-switch
.end method

.method private getSystemInfo(Landroid/content/Context;Ljava/lang/String;)Ldos;
    .locals 2

    .prologue
    .line 199
    new-instance v0, Ldos;

    invoke-direct {v0}, Ldos;-><init>()V

    .line 200
    invoke-static {}, Lcxl;->b()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Ldos;->g:Ljava/lang/Integer;

    .line 201
    invoke-static {}, Lcxl;->d()I

    move-result v1

    .line 202
    if-ltz v1, :cond_0

    .line 203
    div-int/lit16 v1, v1, 0x3e8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Ldos;->h:Ljava/lang/Integer;

    .line 206
    :cond_0
    const-string v1, "android"

    iput-object v1, v0, Ldos;->b:Ljava/lang/String;

    .line 207
    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    iput-object v1, v0, Ldos;->o:Ljava/lang/String;

    .line 208
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/CallStatistics;->getMobileDeviceInfo(Landroid/content/Context;)Ldot;

    move-result-object v1

    iput-object v1, v0, Ldos;->v:Ldot;

    .line 209
    iput-object p2, v0, Ldos;->w:Ljava/lang/String;

    .line 210
    return-object v0
.end method


# virtual methods
.method dump(Ljava/io/PrintWriter;)V
    .locals 8

    .prologue
    .line 526
    new-instance v2, Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;

    invoke-direct {v2}, Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;-><init>()V

    .line 527
    const-string v0, "Stats history"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 529
    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/CallStatistics;->mStatsUpdates:Lcxa;

    monitor-enter v3

    .line 530
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallStatistics;->mStatsUpdates:Lcxa;

    invoke-virtual {v0}, Lcxa;->a()I

    move-result v4

    .line 531
    if-lez v4, :cond_0

    .line 532
    const-string v0, "Legend:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 533
    invoke-static {p1}, Lcom/google/android/libraries/hangouts/video/Stats$VoiceSenderStats;->printLegend(Ljava/io/PrintWriter;)V

    .line 534
    invoke-static {p1}, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->printLegend(Ljava/io/PrintWriter;)V

    .line 535
    invoke-static {p1}, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;->printLegend(Ljava/io/PrintWriter;)V

    .line 536
    invoke-static {p1}, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->printLegend(Ljava/io/PrintWriter;)V

    .line 537
    invoke-static {p1}, Lcom/google/android/libraries/hangouts/video/Stats$BandwidthEstimationStats;->printLegend(Ljava/io/PrintWriter;)V

    .line 538
    invoke-static {p1}, Lcom/google/android/libraries/hangouts/video/Stats$ConnectionInfoStats;->printLegend(Ljava/io/PrintWriter;)V

    .line 539
    invoke-static {p1}, Lcom/google/android/libraries/hangouts/video/Stats$GlobalStats;->printLegend(Ljava/io/PrintWriter;)V

    .line 541
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    .line 542
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallStatistics;->mStatsUpdates:Lcxa;

    invoke-virtual {v0, v1}, Lcxa;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;

    .line 543
    new-instance v5, Ljava/util/Date;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;->mTime:J
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;->access$300(Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;)J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    .line 544
    invoke-virtual {v5}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 545
    # getter for: Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;->mStatsObject:Lcom/google/android/libraries/hangouts/video/Stats;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;->access$100(Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;)Lcom/google/android/libraries/hangouts/video/Stats;

    move-result-object v0

    invoke-virtual {v0, p1, v2}, Lcom/google/android/libraries/hangouts/video/Stats;->print(Ljava/io/PrintWriter;Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;)V

    .line 541
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 547
    :cond_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 549
    invoke-virtual {v2, p1}, Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;->print(Ljava/io/PrintWriter;)V

    .line 550
    return-void

    .line 547
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method

.method public getEndpointVideoStats(Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;)Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;
    .locals 4

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallStatistics;->mStatsUpdates:Lcxa;

    invoke-virtual {v0}, Lcxa;->a()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    .line 177
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/endpoint/Endpoint;->getVideoSsrcs()Ljava/util/List;

    move-result-object v0

    .line 178
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 179
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 180
    :goto_0
    if-ltz v1, :cond_1

    .line 181
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallStatistics;->mStatsUpdates:Lcxa;

    invoke-virtual {v0, v1}, Lcxa;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;->mStatsObject:Lcom/google/android/libraries/hangouts/video/Stats;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;->access$100(Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;)Lcom/google/android/libraries/hangouts/video/Stats;

    move-result-object v0

    .line 182
    instance-of v3, v0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;

    if-eqz v3, :cond_0

    .line 183
    check-cast v0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;

    .line 184
    iget v3, v0, Lcom/google/android/libraries/hangouts/video/Stats$VideoReceiverStats;->ssrc:I

    if-ne v3, v2, :cond_0

    .line 191
    :goto_1
    return-object v0

    .line 188
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    .line 189
    goto :goto_0

    .line 191
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getLocalVideoStats()Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;
    .locals 3

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallStatistics;->mStatsUpdates:Lcxa;

    invoke-virtual {v0}, Lcxa;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    .line 160
    :goto_0
    if-ltz v1, :cond_1

    .line 161
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallStatistics;->mStatsUpdates:Lcxa;

    invoke-virtual {v0, v1}, Lcxa;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;->mStatsObject:Lcom/google/android/libraries/hangouts/video/Stats;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;->access$100(Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;)Lcom/google/android/libraries/hangouts/video/Stats;

    move-result-object v0

    .line 162
    instance-of v2, v0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;

    if-eqz v2, :cond_0

    .line 163
    check-cast v0, Lcom/google/android/libraries/hangouts/video/Stats$VideoSenderStats;

    .line 167
    :goto_1
    return-object v0

    .line 165
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    .line 166
    goto :goto_0

    .line 167
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method getLogData(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/libraries/hangouts/video/HangoutRequest;IILjava/lang/String;Ljava/lang/String;ZII)Ldzj;
    .locals 10

    .prologue
    .line 120
    new-instance v9, Ldzj;

    invoke-direct {v9}, Ldzj;-><init>()V

    .line 121
    const/16 v2, 0x3b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v9, Ldzj;->n:Ljava/lang/Integer;

    .line 123
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/hangouts/video/CallStatistics;->getSystemInfo(Landroid/content/Context;Ljava/lang/String;)Ldos;

    move-result-object v2

    iput-object v2, v9, Ldzj;->j:Ldos;

    move-object v2, p0

    move-object v3, p3

    move v4, p4

    move v5, p5

    move-object/from16 v6, p7

    move/from16 v7, p9

    move/from16 v8, p8

    .line 125
    invoke-direct/range {v2 .. v8}, Lcom/google/android/libraries/hangouts/video/CallStatistics;->getStartupEntry(Lcom/google/android/libraries/hangouts/video/HangoutRequest;IILjava/lang/String;IZ)Ldok;

    move-result-object v2

    iput-object v2, v9, Ldzj;->k:Ldok;

    .line 130
    if-eqz p6, :cond_0

    .line 131
    move-object/from16 v0, p6

    iput-object v0, v9, Ldzj;->b:Ljava/lang/String;

    .line 134
    :cond_0
    if-nez p7, :cond_2

    .line 135
    invoke-static/range {p8 .. p8}, Lcwz;->b(Z)V

    .line 152
    :cond_1
    :goto_0
    return-object v9

    .line 140
    :cond_2
    invoke-static/range {p7 .. p7}, Lf;->q(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 141
    iput-object v2, v9, Ldzj;->c:Ljava/lang/String;

    .line 143
    invoke-static/range {p7 .. p7}, Lf;->r(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 144
    iput-object v3, v9, Ldzj;->e:Ljava/lang/String;

    .line 146
    if-eqz p8, :cond_1

    .line 148
    move/from16 v0, p9

    move/from16 v1, p10

    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/libraries/hangouts/video/CallStatistics;->getCallPerf(Ljava/lang/String;II)Ldoc;

    move-result-object v2

    iput-object v2, v9, Ldzj;->f:Ldoc;

    goto :goto_0
.end method

.method initializeStats()V
    .locals 2

    .prologue
    .line 79
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/libraries/hangouts/video/CallStatistics;->mCallStartTime:J

    .line 80
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/libraries/hangouts/video/CallStatistics;->mCallElapsedRealtimeAtStart:J

    .line 81
    return-void
.end method

.method onCallAccepted()V
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/CallStatistics;->mCallAccepted:Z

    .line 85
    return-void
.end method

.method update(Lcom/google/android/libraries/hangouts/video/Stats;)V
    .locals 7

    .prologue
    .line 89
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/libraries/hangouts/video/CallStatistics;->mCallElapsedRealtimeAtStart:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long v3, v0, v2

    .line 90
    new-instance v0, Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const/4 v6, 0x0

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/libraries/hangouts/video/CallStatistics$StatsUpdate;-><init>(JJLcom/google/android/libraries/hangouts/video/Stats;Lcom/google/android/libraries/hangouts/video/CallStatistics$1;)V

    .line 93
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallStatistics;->mStatsUpdates:Lcxa;

    monitor-enter v1

    .line 94
    :try_start_0
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CallStatistics;->mStatsUpdates:Lcxa;

    invoke-virtual {v2, v0}, Lcxa;->a(Ljava/lang/Object;)V

    .line 95
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
