.class Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# static fields
.field private static final RECONNECT_TIMEOUT:J = 0x1388L


# instance fields
.field private mConnected:Z

.field private mMobileHipriConnectedRunnable:Ljava/lang/Runnable;

.field final synthetic this$0:Lcom/google/android/libraries/hangouts/video/CallManager;


# direct methods
.method private constructor <init>(Lcom/google/android/libraries/hangouts/video/CallManager;)V
    .locals 1

    .prologue
    .line 1532
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 1534
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;->mConnected:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/libraries/hangouts/video/CallManager;Lcom/google/android/libraries/hangouts/video/CallManager$1;)V
    .locals 0

    .prologue
    .line 1532
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;-><init>(Lcom/google/android/libraries/hangouts/video/CallManager;)V

    return-void
.end method

.method static synthetic access$2100(Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;)Z
    .locals 1

    .prologue
    .line 1532
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;->mConnected:Z

    return v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 1546
    invoke-static {}, Lcwz;->a()V

    .line 1547
    const-string v0, "networkInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    .line 1548
    if-nez v0, :cond_1

    .line 1601
    :cond_0
    :goto_0
    return-void

    .line 1552
    :cond_1
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    # invokes: Lcom/google/android/libraries/hangouts/video/CallManager;->isPreparingOrInCall()Z
    invoke-static {v1}, Lcom/google/android/libraries/hangouts/video/CallManager;->access$1700(Lcom/google/android/libraries/hangouts/video/CallManager;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1557
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalState:Lcom/google/android/libraries/hangouts/video/LocalState;
    invoke-static {v1}, Lcom/google/android/libraries/hangouts/video/CallManager;->access$1800(Lcom/google/android/libraries/hangouts/video/CallManager;)Lcom/google/android/libraries/hangouts/video/LocalState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/LocalState;->getSignalingNetworkType()I

    move-result v1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 1561
    invoke-static {}, Lcxc;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1562
    const-string v1, "vclib"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Connection state of "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " changed to: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1563
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1562
    invoke-static {v1, v2}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1566
    :cond_2
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v0

    sget-object v1, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v0, v1, :cond_3

    .line 1567
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;->mConnected:Z

    .line 1569
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;->mMobileHipriConnectedRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 1570
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalState:Lcom/google/android/libraries/hangouts/video/LocalState;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallManager;->access$1800(Lcom/google/android/libraries/hangouts/video/CallManager;)Lcom/google/android/libraries/hangouts/video/LocalState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/LocalState;->getSignalingNetworkType()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x5

    .line 1571
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 1570
    invoke-static {v0, v1}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1572
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallManager;->access$1900(Lcom/google/android/libraries/hangouts/video/CallManager;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;->mMobileHipriConnectedRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 1575
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;->mConnected:Z

    if-eqz v0, :cond_0

    .line 1577
    const-string v0, "vclib"

    const-string v1, "We lost our connection. Give it some time to recover then  terminate the call if it can\'t."

    invoke-static {v0, v1}, Lcxc;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1579
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;->mConnected:Z

    .line 1581
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    # invokes: Lcom/google/android/libraries/hangouts/video/CallManager;->shouldAttemptReconnect()Z
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallManager;->access$2000(Lcom/google/android/libraries/hangouts/video/CallManager;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1584
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    # getter for: Lcom/google/android/libraries/hangouts/video/CallManager;->mLocalHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallManager;->access$1900(Lcom/google/android/libraries/hangouts/video/CallManager;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver$1;

    invoke-direct {v1, p0}, Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver$1;-><init>(Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;)V

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 1597
    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;->this$0:Lcom/google/android/libraries/hangouts/video/CallManager;

    # invokes: Lcom/google/android/libraries/hangouts/video/CallManager;->handleNetworkDisconnect()V
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/CallManager;->access$2200(Lcom/google/android/libraries/hangouts/video/CallManager;)V

    goto/16 :goto_0
.end method

.method setMobileHipriConnectedRunnable(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 1541
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CallManager$NetworkStateReceiver;->mMobileHipriConnectedRunnable:Ljava/lang/Runnable;

    .line 1542
    return-void
.end method
