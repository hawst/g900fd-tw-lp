.class public abstract Lcom/google/android/libraries/hangouts/video/Renderer;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field protected mRendererID:I

.field protected mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    return-void
.end method


# virtual methods
.method public abstract drawTexture(Lcom/google/android/libraries/hangouts/video/Renderer$DrawInputParams;Lcom/google/android/libraries/hangouts/video/Renderer$DrawOutputParams;)Z
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 0

    .prologue
    .line 142
    return-void
.end method

.method public abstract getOutputTextureName()I
.end method

.method public abstract initializeGLContext()V
.end method

.method public isExternalTexture()Z
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x0

    return v0
.end method

.method public abstract release()V
.end method
