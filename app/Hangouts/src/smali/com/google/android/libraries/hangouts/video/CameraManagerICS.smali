.class final Lcom/google/android/libraries/hangouts/video/CameraManagerICS;
.super Lcom/google/android/libraries/hangouts/video/CameraManager;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# static fields
.field private static volatile sInstance:Lcom/google/android/libraries/hangouts/video/CameraManagerICS;

.field private static final sInstanceLock:Ljava/lang/Object;


# instance fields
.field private mSurfaceTexture:Landroid/graphics/SurfaceTexture;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/libraries/hangouts/video/CameraManagerICS;->sInstanceLock:Ljava/lang/Object;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/CameraManager;-><init>()V

    return-void
.end method

.method private static getICSInstance()Lcom/google/android/libraries/hangouts/video/CameraManagerICS;
    .locals 2

    .prologue
    .line 34
    sget-object v0, Lcom/google/android/libraries/hangouts/video/CameraManagerICS;->sInstance:Lcom/google/android/libraries/hangouts/video/CameraManagerICS;

    if-nez v0, :cond_1

    .line 35
    sget-object v1, Lcom/google/android/libraries/hangouts/video/CameraManagerICS;->sInstanceLock:Ljava/lang/Object;

    monitor-enter v1

    .line 36
    :try_start_0
    sget-object v0, Lcom/google/android/libraries/hangouts/video/CameraManagerICS;->sInstance:Lcom/google/android/libraries/hangouts/video/CameraManagerICS;

    if-nez v0, :cond_0

    .line 37
    new-instance v0, Lcom/google/android/libraries/hangouts/video/CameraManagerICS;

    invoke-direct {v0}, Lcom/google/android/libraries/hangouts/video/CameraManagerICS;-><init>()V

    sput-object v0, Lcom/google/android/libraries/hangouts/video/CameraManagerICS;->sInstance:Lcom/google/android/libraries/hangouts/video/CameraManagerICS;

    .line 39
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 41
    :cond_1
    sget-object v0, Lcom/google/android/libraries/hangouts/video/CameraManagerICS;->sInstance:Lcom/google/android/libraries/hangouts/video/CameraManagerICS;

    return-object v0

    .line 39
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getInstance()Lcom/google/android/libraries/hangouts/video/CameraManager;
    .locals 1

    .prologue
    .line 30
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/CameraManagerICS;->getICSInstance()Lcom/google/android/libraries/hangouts/video/CameraManagerICS;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public arePreview3ALocksSupportedLocked(Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 89
    if-nez p1, :cond_0

    .line 90
    const-string v1, "vclib"

    const-string v2, "Calling arePreview3ALocksSupported without a current camera!"

    invoke-static {v1, v2}, Lcxc;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    :goto_0
    return v0

    .line 96
    :cond_0
    iget-boolean v1, p1, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->haveReadCameraParameters:Z

    if-nez v1, :cond_1

    .line 97
    const-string v1, "vclib"

    const-string v2, "Calling arePreview3ALocksSupported before reading camera parameters!"

    invoke-static {v1, v2}, Lcxc;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 103
    :cond_1
    iget-boolean v0, p1, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->cameraSupports3ALocks:Z

    goto :goto_0
.end method

.method protected initializeCameraLocked(Landroid/hardware/Camera$Parameters;Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 48
    const-string v0, "video-stabilization-supported"

    invoke-virtual {p1, v0}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 49
    const-string v2, "true"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    const-string v0, "video-stabilization"

    const-string v2, "false"

    invoke-virtual {p1, v0, v2}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    :cond_0
    iget-boolean v0, p2, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->haveReadCameraParameters:Z

    if-nez v0, :cond_1

    .line 55
    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->isAutoExposureLockSupported()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 56
    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->isAutoWhiteBalanceLockSupported()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p2, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->cameraSupports3ALocks:Z

    .line 57
    iput-boolean v1, p2, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->haveReadCameraParameters:Z

    .line 59
    :cond_1
    return-void

    .line 56
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected restorePreviewCallback()Z
    .locals 1

    .prologue
    .line 143
    const/4 v0, 0x1

    return v0
.end method

.method public setPreview3ALocksLocked(ZLandroid/hardware/Camera;Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;)V
    .locals 2

    .prologue
    .line 72
    iget-boolean v0, p3, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraSpecificSettings;->cameraSupports3ALocks:Z

    if-nez v0, :cond_0

    .line 73
    const-string v0, "vclib"

    const-string v1, "Current camera does not support AE/AWB locks."

    invoke-static {v0, v1}, Lcxc;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    :goto_0
    return-void

    .line 76
    :cond_0
    invoke-virtual {p2}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    .line 77
    invoke-virtual {v0, p1}, Landroid/hardware/Camera$Parameters;->setAutoExposureLock(Z)V

    .line 78
    invoke-virtual {v0, p1}, Landroid/hardware/Camera$Parameters;->setAutoWhiteBalanceLock(Z)V

    .line 79
    invoke-virtual {p2, v0}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    goto :goto_0
.end method

.method public setPreviewSurfaceTexture(Landroid/graphics/SurfaceTexture;)V
    .locals 3

    .prologue
    .line 126
    invoke-static {}, Lcxc;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    const-string v0, "vclib"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setPreviewSurfaceTexture "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " state="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 128
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/CameraManagerICS;->getCameraState()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 127
    invoke-static {v0, v1}, Lcxc;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    :cond_0
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CameraManagerICS;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    .line 131
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManagerICS;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_1

    .line 132
    invoke-static {}, Lcwz;->c()V

    .line 133
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/CameraManagerICS;->onNewPreviewSurfaceSet()V

    .line 138
    :goto_0
    return-void

    .line 135
    :cond_1
    invoke-static {}, Lcwz;->a()V

    .line 136
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/CameraManagerICS;->onPreviewSurfaceCleared()V

    goto :goto_0
.end method

.method protected setSurfaceOnCameraLocked(Landroid/hardware/Camera;)Z
    .locals 3

    .prologue
    .line 110
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CameraManagerICS;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {p1, v0}, Landroid/hardware/Camera;->setPreviewTexture(Landroid/graphics/SurfaceTexture;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 111
    const/4 v0, 0x1

    .line 115
    :goto_0
    return v0

    .line 112
    :catch_0
    move-exception v0

    .line 113
    const-string v1, "vclib"

    const-string v2, "setPreviewTexture failed"

    invoke-static {v1, v2}, Lcxc;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CameraManagerICS;->mCallback:Lcom/google/android/libraries/hangouts/video/CameraManager$CameraManagerCallback;

    invoke-interface {v1, v0}, Lcom/google/android/libraries/hangouts/video/CameraManager$CameraManagerCallback;->onCameraOpenError(Ljava/lang/Exception;)V

    .line 115
    const/4 v0, 0x0

    goto :goto_0
.end method
