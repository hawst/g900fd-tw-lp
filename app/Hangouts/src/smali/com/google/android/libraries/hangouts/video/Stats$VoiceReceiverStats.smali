.class public Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;
.super Lcom/google/android/libraries/hangouts/video/Stats;
.source "PG"


# instance fields
.field public final audioLevel:I

.field public final bytesRcvd:I

.field public final delayEstimateMillis:I

.field public final expandRate:F

.field public final extSeqNum:I

.field public final fractionLost:F

.field public final jitterBufferMillis:I

.field public final jitterBufferPreferredMillis:I

.field public final jitterMillis:I

.field public final packetsLost:I

.field public final packetsRcvd:I

.field public final ssrc:I


# direct methods
.method public constructor <init>(IIIIFIIIIII)V
    .locals 1

    .prologue
    .line 180
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/Stats;-><init>()V

    .line 181
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->ssrc:I

    .line 182
    iput p2, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->bytesRcvd:I

    .line 183
    iput p3, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->packetsRcvd:I

    .line 184
    iput p4, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->packetsLost:I

    .line 185
    iput p5, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->fractionLost:F

    .line 186
    iput p6, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->extSeqNum:I

    .line 187
    iput p7, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->jitterMillis:I

    .line 188
    iput p8, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->jitterBufferMillis:I

    .line 189
    iput p9, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->jitterBufferPreferredMillis:I

    .line 190
    iput p10, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->delayEstimateMillis:I

    .line 191
    iput p11, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->audioLevel:I

    .line 192
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->expandRate:F

    .line 193
    return-void
.end method

.method public constructor <init>(IIIIFIIIIIIF)V
    .locals 0

    .prologue
    .line 198
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/Stats;-><init>()V

    .line 199
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->ssrc:I

    .line 200
    iput p2, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->bytesRcvd:I

    .line 201
    iput p3, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->packetsRcvd:I

    .line 202
    iput p4, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->packetsLost:I

    .line 203
    iput p5, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->fractionLost:F

    .line 204
    iput p6, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->extSeqNum:I

    .line 205
    iput p7, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->jitterMillis:I

    .line 206
    iput p8, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->jitterBufferMillis:I

    .line 207
    iput p9, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->jitterBufferPreferredMillis:I

    .line 208
    iput p10, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->delayEstimateMillis:I

    .line 209
    iput p11, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->audioLevel:I

    .line 210
    iput p12, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->expandRate:F

    .line 211
    return-void
.end method

.method static printLegend(Ljava/io/PrintWriter;)V
    .locals 1

    .prologue
    .line 240
    const-string v0, "  VoiceReceiver -- ssrc, rcvd, lost, seq, jitter, jbuf, delay, level, expand"

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 242
    return-void
.end method


# virtual methods
.method public addTo(Ldoe;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v4, -0x1

    .line 215
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/Stats;->createEmptyMediaProto(I)Ldoh;

    move-result-object v1

    .line 216
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->l:Ljava/lang/Integer;

    .line 217
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->ssrc:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->m:Ljava/lang/Integer;

    .line 218
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->bytesRcvd:I

    int-to-long v2, v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v1, Ldoh;->j:Ljava/lang/Long;

    .line 219
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->packetsRcvd:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->k:Ljava/lang/Integer;

    .line 220
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->packetsLost:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->d:Ljava/lang/Integer;

    .line 221
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->fractionLost:F

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v0, v2

    float-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->c:Ljava/lang/Integer;

    .line 222
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->extSeqNum:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->e:Ljava/lang/Integer;

    .line 223
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->jitterMillis:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->f:Ljava/lang/Integer;

    .line 224
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->jitterBufferMillis:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->x:Ljava/lang/Integer;

    .line 225
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->expandRate:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v1, Ldoh;->X:Ljava/lang/Float;

    .line 227
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->delayEstimateMillis:I

    if-eq v0, v4, :cond_0

    .line 228
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->delayEstimateMillis:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->z:Ljava/lang/Integer;

    .line 230
    :cond_0
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->audioLevel:I

    if-eq v0, v4, :cond_1

    .line 231
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->audioLevel:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Ldoh;->n:Ljava/lang/Integer;

    .line 234
    :cond_1
    iget-object v0, p1, Ldoe;->c:[Ldoh;

    array-length v2, v0

    .line 235
    iget-object v0, p1, Ldoe;->c:[Ldoh;

    add-int/lit8 v3, v2, 0x1

    invoke-static {v0, v3}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldoh;

    iput-object v0, p1, Ldoe;->c:[Ldoh;

    .line 236
    iget-object v0, p1, Ldoe;->c:[Ldoh;

    aput-object v1, v0, v2

    .line 237
    return-void
.end method

.method public print(Ljava/io/PrintWriter;Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;)V
    .locals 5

    .prologue
    .line 246
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, " -- VoiceReceiver -- "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->ssrc:I

    int-to-long v1, v1

    const-wide v3, 0xffffffffL

    and-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->packetsRcvd:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->bytesRcvd:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "), "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->packetsLost:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->fractionLost:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "), "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->extSeqNum:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->jitterMillis:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->jitterBufferMillis:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->jitterBufferPreferredMillis:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "), "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->delayEstimateMillis:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->audioLevel:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/Stats$VoiceReceiverStats;->expandRate:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 256
    return-void
.end method
