.class public Lcom/google/android/libraries/hangouts/video/VideoChatConstants;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final ASYNC_INVITE:I = 0x0

.field public static final AUTH_SCOPE:Ljava/lang/String; = "oauth2:https://www.googleapis.com/auth/chat https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/hangouts https://www.googleapis.com/auth/identity.plus.page.impersonation https://www.googleapis.com/auth/chat.native"

.field public static final BASE_AUTH_SCOPE:Ljava/lang/String; = "oauth2:https://www.googleapis.com/auth/chat https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/hangouts https://www.googleapis.com/auth/identity.plus.page.impersonation"

.field public static final CALL_END_AUTO_EXIT_ON_EMPTY_HANGOUT:I = 0x3ed

.field public static final CALL_END_ERROR_INSUFFICIENT_FUNDS:I = 0x3f6

.field public static final CALL_END_HANDOFF_TO_CELLULAR:I = 0x3f7

.field public static final CALL_END_KICKED:I = 0x18

.field public static final CALL_END_LOCAL_USER_ENDED:I = 0x3ec

.field public static final CALL_END_MAX:I = 0x3f7

.field public static final CALL_END_MISSING_CODEC:I = 0x3f0

.field public static final CALL_END_NETWORK_DISCONNECTED:I = 0x3eb

.field public static final CALL_END_NONE:I = 0x0

.field public static final CALL_END_NOT_GPLUS_USER:I = 0x1a

.field public static final CALL_END_NOT_ONGOING_AS_EXPECTED:I = 0x3f4

.field public static final CALL_END_PHONE_CALL:I = 0x3ee

.field public static final CALL_END_REMOTE_USER_ENDED:I = 0x3f1

.field public static final CALL_END_REMOTE_USER_UNAVAILABLE:I = 0x3ef

.field public static final CALL_END_RING_DECLINED:I = 0x3f3

.field public static final CALL_END_RING_UNANSWERED:I = 0x3f2

.field public static final CALL_END_SIG_CHANNEL_TIMEOUT:I = 0x16

.field public static final CALL_END_SIG_SOCKET_CLOSED:I = 0x15

.field public static final CALL_END_UNKNOWN:I = 0x1

.field public static final CALL_ENTER_ERROR_BLOCKED:I = 0xc

.field public static final CALL_ENTER_ERROR_BLOCKING:I = 0xd

.field public static final CALL_ENTER_ERROR_HANGOUT_OVER:I = 0x2

.field public static final CALL_ENTER_ERROR_MATURE_CONTENT:I = 0x14

.field public static final CALL_ENTER_ERROR_MAX_USERS:I = 0xe

.field public static final CALL_ENTER_ERROR_MEDIA_SESSION_REJECTED:I = 0x10

.field public static final CALL_ENTER_ERROR_NOT_ACCEPTABLE:I = 0x11

.field public static final CALL_ENTER_ERROR_NO_NETWORK:I = 0x3e9

.field public static final CALL_ENTER_ERROR_ONGOING_PHONE_CALL:I = 0x3f5

.field public static final CALL_ENTER_ERROR_ROOM_LOCKED:I = 0x12

.field public static final CALL_ENTER_ERROR_SERVER:I = 0xf

.field public static final CALL_ENTER_ERROR_SIGNIN_FAILED:I = 0x3e8

.field public static final CALL_ENTER_ERROR_UNEXPECTED_ABUSE_RECORDABLE:I = 0x19

.field public static final CALL_ENTER_ERROR_UNEXPECTED_HOA:I = 0x13

.field public static final CALL_ENTER_ERROR_WIFI_REQUIRED:I = 0x3ea

.field public static final CALL_ENTER_TIMEOUT:I = 0x3

.field public static final CALL_ENTER_TIMEOUT_CREATE_CONV:I = 0x4

.field public static final CALL_ENTER_TIMEOUT_CREATE_HANGOUT_ID:I = 0xb

.field public static final CALL_ENTER_TIMEOUT_ENTER_CHATROOM:I = 0x9

.field public static final CALL_ENTER_TIMEOUT_INITIATE_SESSION:I = 0xa

.field public static final CALL_ENTER_TIMEOUT_JID_LOOKUP:I = 0x6

.field public static final CALL_ENTER_TIMEOUT_ROOM_CONFIG:I = 0x8

.field public static final CALL_ENTER_TIMEOUT_ROOM_DISCOVERY:I = 0x7

.field public static final CALL_ENTER_TIMEOUT_SIGN_IN:I = 0x5

.field public static final CALL_ERROR_MEDIA_CONNECTIVITY_FAILURE:I = 0x17

.field public static final SYNC_INVITE:I = 0x1

.field public static final UNSUPPORTED_NOTIFICATION_CONFIRM:I = 0x1

.field public static final UNSUPPORTED_NOTIFICATION_FYI:I = 0x0

.field public static final UNSUPPORTED_NOTIFICATION_INTERRUPT:I = 0x2


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x19

    .line 259
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcwz;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 260
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    return-void
.end method

.method public static isConnectivityRelatedIssue(I)Z
    .locals 1

    .prologue
    .line 272
    sparse-switch p0, :sswitch_data_0

    .line 292
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 290
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 272
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_0
        0x4 -> :sswitch_0
        0x5 -> :sswitch_0
        0x6 -> :sswitch_0
        0x7 -> :sswitch_0
        0x8 -> :sswitch_0
        0x9 -> :sswitch_0
        0xa -> :sswitch_0
        0xb -> :sswitch_0
        0x15 -> :sswitch_0
        0x16 -> :sswitch_0
        0x17 -> :sswitch_0
        0x3e8 -> :sswitch_0
        0x3e9 -> :sswitch_0
        0x3ea -> :sswitch_0
        0x3eb -> :sswitch_0
    .end sparse-switch
.end method

.method public static isNetworkError(I)Z
    .locals 1

    .prologue
    .line 263
    const/16 v0, 0x15

    if-eq p0, v0, :cond_0

    const/16 v0, 0x16

    if-eq p0, v0, :cond_0

    const/16 v0, 0x3eb

    if-eq p0, v0, :cond_0

    const/16 v0, 0x3f7

    if-eq p0, v0, :cond_0

    const/16 v0, 0x17

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
