.class public Lcom/google/android/libraries/hangouts/video/endpoint/EnterEvent;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/hangouts/video/endpoint/EndpointEvent;


# instance fields
.field private final mMayBePreExistingEndpoint:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-boolean p1, p0, Lcom/google/android/libraries/hangouts/video/endpoint/EnterEvent;->mMayBePreExistingEndpoint:Z

    .line 13
    return-void
.end method


# virtual methods
.method public mayBePreExistingEndpoint()Z
    .locals 1

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/endpoint/EnterEvent;->mMayBePreExistingEndpoint:Z

    return v0
.end method
