.class public Lcom/google/android/libraries/hangouts/video/ApiaryClient;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final CONTENT_TYPE:Ljava/lang/String; = "application/x-protobuf"

.field private static final DEFAULT_SDK_VERSION:Ljava/lang/String; = "1.0.0"

.field public static final HTTP_HEADER_CONTAINER_URL:Ljava/lang/String; = "X-Container-Url"

.field public static final HTTP_HEADER_DEVICE_ID:Ljava/lang/String; = "X-Device-ID"

.field public static final HTTP_HEADER_NETWORK_ID:Ljava/lang/String; = "X-Network-ID"

.field public static final HTTP_HEADER_OAUTH_TIME:Ljava/lang/String; = "X-Auth-Time"

.field public static final X_GOOG_SAFETY_ENCODING:Ljava/lang/String; = "X-Goog-Safety-Encoding"


# instance fields
.field private final mApiaryUri:Ljava/lang/String;

.field private mAuthToken:Ljava/lang/String;

.field private mExecutor:Lcom/google/android/libraries/hangouts/video/ApiaryClient$HttpRequestExecutor;

.field private mListener:Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestListener;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 195
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 196
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient;->mApiaryUri:Ljava/lang/String;

    .line 197
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/libraries/hangouts/video/ApiaryClient;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient;->mAuthToken:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/libraries/hangouts/video/ApiaryApiInfo;Ljava/lang/String;Ljava/lang/Long;I)Lcom/google/api/client/http/HttpRequestInitializer;
    .locals 1

    .prologue
    .line 37
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/libraries/hangouts/video/ApiaryClient;->createHttpRequestInitializer(Lcom/google/android/libraries/hangouts/video/ApiaryApiInfo;Ljava/lang/String;Ljava/lang/Long;I)Lcom/google/api/client/http/HttpRequestInitializer;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/libraries/hangouts/video/ApiaryClient;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient;->mApiaryUri:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/libraries/hangouts/video/ApiaryClient;)Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestListener;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient;->mListener:Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestListener;

    return-object v0
.end method

.method private static createHttpRequestInitializer(Lcom/google/android/libraries/hangouts/video/ApiaryApiInfo;Ljava/lang/String;Ljava/lang/Long;I)Lcom/google/api/client/http/HttpRequestInitializer;
    .locals 2

    .prologue
    .line 227
    new-instance v0, Lczi;

    invoke-direct {v0}, Lczi;-><init>()V

    .line 228
    invoke-virtual {v0, p1}, Lczi;->c(Ljava/lang/String;)Lczi;

    .line 229
    new-instance v1, Lcom/google/android/libraries/hangouts/video/ApiaryClient$1;

    invoke-direct {v1, p2, p1, v0, p3}, Lcom/google/android/libraries/hangouts/video/ApiaryClient$1;-><init>(Ljava/lang/Long;Ljava/lang/String;Lczi;I)V

    return-object v1
.end method

.method public static getContainerUrl(Lcom/google/android/libraries/hangouts/video/ApiaryApiInfo;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 274
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/ApiaryApiInfo;->getSourceInfo()Lcom/google/android/libraries/hangouts/video/ApiaryApiInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 275
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/ApiaryApiInfo;->getSourceInfo()Lcom/google/android/libraries/hangouts/video/ApiaryApiInfo;

    move-result-object p0

    .line 279
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/ApiaryApiInfo;->getCertificate()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/ApiaryApiInfo;->getCertificate()Ljava/lang/String;

    move-result-object v0

    .line 280
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/ApiaryApiInfo;->getClientId()Ljava/lang/String;

    move-result-object v1

    .line 281
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/ApiaryApiInfo;->getApiKey()Ljava/lang/String;

    move-result-object v2

    .line 282
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/ApiaryApiInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 284
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "https://"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ".apps.googleusercontent.com"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 285
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 286
    if-eqz v1, :cond_1

    .line 287
    const-string v4, "client_id"

    invoke-virtual {v0, v4, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 289
    :cond_1
    if-eqz v2, :cond_2

    .line 290
    const-string v1, "api_key"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 292
    :cond_2
    if-eqz v3, :cond_3

    .line 293
    const-string v1, "pkg"

    invoke-virtual {v0, v1, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 295
    :cond_3
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 279
    :cond_4
    const-string v0, "0"

    goto :goto_0
.end method


# virtual methods
.method getExecutor()Lcom/google/android/libraries/hangouts/video/ApiaryClient$HttpRequestExecutor;
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient;->mExecutor:Lcom/google/android/libraries/hangouts/video/ApiaryClient$HttpRequestExecutor;

    if-nez v0, :cond_0

    .line 319
    new-instance v0, Lcom/google/android/libraries/hangouts/video/ApiaryClient$2;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/hangouts/video/ApiaryClient$2;-><init>(Lcom/google/android/libraries/hangouts/video/ApiaryClient;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient;->mExecutor:Lcom/google/android/libraries/hangouts/video/ApiaryClient$HttpRequestExecutor;

    .line 327
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient;->mExecutor:Lcom/google/android/libraries/hangouts/video/ApiaryClient$HttpRequestExecutor;

    return-object v0
.end method

.method public makeRequest(JLjava/lang/String;[BI)V
    .locals 7

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient;->mListener:Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestListener;

    invoke-static {v0}, Lcwz;->b(Ljava/lang/Object;)V

    .line 215
    new-instance v0, Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestTask;

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestTask;-><init>(Lcom/google/android/libraries/hangouts/video/ApiaryClient;JLjava/lang/String;[BI)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestTask;->executeOnThreadPool([Ljava/lang/Object;)Lcom/google/android/libraries/hangouts/video/SafeAsyncTask;

    .line 216
    return-void
.end method

.method makeRequestSync(JLjava/lang/String;[BI)[B
    .locals 7

    .prologue
    .line 209
    new-instance v0, Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestTask;

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestTask;-><init>(Lcom/google/android/libraries/hangouts/video/ApiaryClient;JLjava/lang/String;[BI)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestTask;->doInBackgroundTimed([Ljava/lang/Void;)[B

    move-result-object v0

    return-object v0
.end method

.method setAuthToken(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 200
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient;->mAuthToken:Ljava/lang/String;

    .line 201
    return-void
.end method

.method public setExecutor(Lcom/google/android/libraries/hangouts/video/ApiaryClient$HttpRequestExecutor;)V
    .locals 0

    .prologue
    .line 312
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient;->mExecutor:Lcom/google/android/libraries/hangouts/video/ApiaryClient$HttpRequestExecutor;

    .line 313
    return-void
.end method

.method setListener(Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestListener;)V
    .locals 0

    .prologue
    .line 204
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/ApiaryClient;->mListener:Lcom/google/android/libraries/hangouts/video/ApiaryClient$RequestListener;

    .line 205
    return-void
.end method
