.class Lcom/google/android/libraries/hangouts/video/MobileHipriManager;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final MOBILE_HIPRI_TIMER:I = 0x61a8

.field private static final RTP_CAN_RESTART_OVER_WIFI:Z = false

.field private static final TAG:Ljava/lang/String; = "vclib"


# instance fields
.field private final mConnectivityManager:Landroid/net/ConnectivityManager;

.field private final mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private mMobileHipriRunnable:Lcom/google/android/libraries/hangouts/video/MobileHipriManager$MobileHipriRunnable;

.field private mRequestHipri:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/MobileHipriManager;->mContext:Landroid/content/Context;

    .line 52
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/MobileHipriManager;->mHandler:Landroid/os/Handler;

    .line 53
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/MobileHipriManager;->mContext:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/MobileHipriManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    .line 55
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/libraries/hangouts/video/MobileHipriManager;)Z
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/MobileHipriManager;->requestMobileHipriNetwork()Z

    move-result v0

    return v0
.end method

.method private log(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 133
    const-string v0, "vclib"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[NetConMgr] "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcxc;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    return-void
.end method

.method private requestMobileHipriNetwork()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 111
    monitor-enter p0

    .line 112
    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/libraries/hangouts/video/MobileHipriManager;->mRequestHipri:Z

    if-nez v1, :cond_1

    .line 113
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 129
    :cond_0
    :goto_0
    return v0

    .line 115
    :cond_1
    monitor-exit p0

    .line 117
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/MobileHipriManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    const-string v2, "enableHIPRI"

    invoke-virtual {v1, v0, v2}, Landroid/net/ConnectivityManager;->startUsingNetworkFeature(ILjava/lang/String;)I

    move-result v1

    .line 122
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, ">>> requestMobileHipriNetwork: enableHIPRI result="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/libraries/hangouts/video/MobileHipriManager;->log(Ljava/lang/String;)V

    .line 125
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/MobileHipriManager;->mMobileHipriRunnable:Lcom/google/android/libraries/hangouts/video/MobileHipriManager$MobileHipriRunnable;

    if-nez v2, :cond_2

    .line 126
    new-instance v2, Lcom/google/android/libraries/hangouts/video/MobileHipriManager$MobileHipriRunnable;

    invoke-direct {v2, p0}, Lcom/google/android/libraries/hangouts/video/MobileHipriManager$MobileHipriRunnable;-><init>(Lcom/google/android/libraries/hangouts/video/MobileHipriManager;)V

    iput-object v2, p0, Lcom/google/android/libraries/hangouts/video/MobileHipriManager;->mMobileHipriRunnable:Lcom/google/android/libraries/hangouts/video/MobileHipriManager$MobileHipriRunnable;

    .line 128
    :cond_2
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/MobileHipriManager;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/MobileHipriManager;->mMobileHipriRunnable:Lcom/google/android/libraries/hangouts/video/MobileHipriManager$MobileHipriRunnable;

    const-wide/16 v4, 0x61a8

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 129
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 115
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public startUsingMobileHipriIfOnMobileNetwork()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 66
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/MobileHipriManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 67
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    .line 69
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "startUsingMobileHipriIfOnMobileNetwork: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/libraries/hangouts/video/MobileHipriManager;->log(Ljava/lang/String;)V

    .line 71
    if-eqz v0, :cond_0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    .line 80
    :cond_0
    return v4

    .line 67
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public stopUsingMobileHipri()V
    .locals 2

    .prologue
    .line 95
    monitor-enter p0

    .line 96
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "stopUsingMobileHipri: mRequestHipri="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/google/android/libraries/hangouts/video/MobileHipriManager;->mRequestHipri:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/MobileHipriManager;->log(Ljava/lang/String;)V

    .line 97
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/MobileHipriManager;->mRequestHipri:Z

    if-eqz v0, :cond_1

    .line 98
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/MobileHipriManager;->mRequestHipri:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 102
    monitor-exit p0

    .line 104
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/MobileHipriManager;->mMobileHipriRunnable:Lcom/google/android/libraries/hangouts/video/MobileHipriManager$MobileHipriRunnable;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/MobileHipriManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/MobileHipriManager;->mMobileHipriRunnable:Lcom/google/android/libraries/hangouts/video/MobileHipriManager$MobileHipriRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 106
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/MobileHipriManager;->mMobileHipriRunnable:Lcom/google/android/libraries/hangouts/video/MobileHipriManager$MobileHipriRunnable;

    .line 108
    :cond_0
    :goto_0
    return-void

    .line 100
    :cond_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 102
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
