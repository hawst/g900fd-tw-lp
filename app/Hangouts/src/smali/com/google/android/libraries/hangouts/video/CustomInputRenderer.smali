.class public Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;
.super Lcom/google/android/libraries/hangouts/video/Renderer;
.source "PG"


# static fields
.field private static final TAG:Ljava/lang/String; = "vclib"


# instance fields
.field private final mCallback:Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;

.field private mEncodeRendererID:I

.field private mEncodeSize:Lcom/google/android/libraries/hangouts/video/Size;

.field private final mEncodeStopWatch:Lcxb;

.field private final mFrameLock:Ljava/lang/Object;

.field private mInputSize:Lcom/google/android/libraries/hangouts/video/Size;

.field private mNewFrameReady:Z

.field private final mRendererFrameInputData:Lcom/google/android/libraries/hangouts/video/CustomInputRenderer$RendererFrameInputData;

.field private mSurfaceTexture:Landroid/graphics/SurfaceTexture;

.field private mTextureName:I


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/hangouts/video/RendererManager;Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;)V
    .locals 5

    .prologue
    const/16 v4, 0x140

    const/16 v3, 0xf0

    const/4 v2, 0x0

    .line 49
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/Renderer;-><init>()V

    .line 26
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mFrameLock:Ljava/lang/Object;

    .line 27
    new-instance v0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer$RendererFrameInputData;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer$RendererFrameInputData;-><init>(Lcom/google/android/libraries/hangouts/video/CustomInputRenderer$1;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mRendererFrameInputData:Lcom/google/android/libraries/hangouts/video/CustomInputRenderer$RendererFrameInputData;

    .line 28
    new-instance v0, Lcxb;

    const-string v1, "CustomInputRenderer.encode"

    invoke-direct {v0, v1}, Lcxb;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mEncodeStopWatch:Lcxb;

    .line 31
    new-instance v0, Lcom/google/android/libraries/hangouts/video/Size;

    invoke-direct {v0, v4, v3}, Lcom/google/android/libraries/hangouts/video/Size;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mInputSize:Lcom/google/android/libraries/hangouts/video/Size;

    .line 32
    new-instance v0, Lcom/google/android/libraries/hangouts/video/Size;

    invoke-direct {v0, v4, v3}, Lcom/google/android/libraries/hangouts/video/Size;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mEncodeSize:Lcom/google/android/libraries/hangouts/video/Size;

    .line 37
    iput v2, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mTextureName:I

    .line 38
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mEncodeRendererID:I

    .line 40
    iput-boolean v2, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mNewFrameReady:Z

    .line 50
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    .line 51
    iput-object p2, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mCallback:Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;

    .line 52
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/hangouts/video/RendererManager;->instantiateRenderer(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mEncodeRendererID:I

    .line 54
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/hangouts/video/RendererManager;->registerRendererForStats(Lcom/google/android/libraries/hangouts/video/Renderer;)V

    .line 55
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mFrameLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;Z)Z
    .locals 0

    .prologue
    .line 23
    iput-boolean p1, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mNewFrameReady:Z

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;)I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mTextureName:I

    return v0
.end method

.method static synthetic access$302(Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;I)I
    .locals 0

    .prologue
    .line 23
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mTextureName:I

    return p1
.end method


# virtual methods
.method public drawTexture(Lcom/google/android/libraries/hangouts/video/Renderer$DrawInputParams;Lcom/google/android/libraries/hangouts/video/Renderer$DrawOutputParams;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 174
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mFrameLock:Ljava/lang/Object;

    monitor-enter v1

    .line 175
    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mNewFrameReady:Z

    if-eqz v2, :cond_0

    .line 176
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->updateTexImage()V

    .line 177
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mNewFrameReady:Z

    .line 178
    const/4 v0, 0x1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 180
    :goto_0
    return v0

    :cond_0
    monitor-exit v1

    goto :goto_0

    .line 181
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 0

    .prologue
    .line 197
    return-void
.end method

.method public encodeFrame()V
    .locals 4

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mEncodeStopWatch:Lcxb;

    .line 190
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mRendererFrameInputData:Lcom/google/android/libraries/hangouts/video/CustomInputRenderer$RendererFrameInputData;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer$RendererFrameInputData;->lastCameraFrameTimeNs:J

    .line 191
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mEncodeRendererID:I

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mRendererFrameInputData:Lcom/google/android/libraries/hangouts/video/CustomInputRenderer$RendererFrameInputData;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/RendererManager;->renderFrame(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 192
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mEncodeStopWatch:Lcxb;

    .line 193
    return-void
.end method

.method public getOutputTextureName()I
    .locals 1

    .prologue
    .line 163
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mTextureName:I

    return v0
.end method

.method public getSurfaceTexture()Landroid/graphics/SurfaceTexture;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    return-object v0
.end method

.method public initializeGLContext()V
    .locals 4

    .prologue
    .line 68
    invoke-static {}, Lcwz;->c()V

    .line 70
    invoke-static {}, Lcxc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    const-string v0, "vclib"

    const-string v1, "CustomInputRenderer.initializeGLContext"

    invoke-static {v0, v1}, Lcxc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    :cond_0
    invoke-static {}, Lf;->B()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mTextureName:I

    .line 75
    new-instance v0, Landroid/graphics/SurfaceTexture;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mTextureName:I

    invoke-direct {v0, v1}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    .line 79
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    new-instance v1, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer$1;

    invoke-direct {v1, p0}, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer$1;-><init>(Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;)V

    invoke-virtual {v0, v1}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    .line 92
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mEncodeRendererID:I

    const-string v2, "sub_intextype"

    const v3, 0x8d65

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/RendererManager;->setIntParam(ILjava/lang/String;I)V

    .line 96
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mEncodeRendererID:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/RendererManager;->initializeGLContext(I)Z

    .line 98
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mEncodeRendererID:I

    const-string v2, "sub_intex"

    iget v3, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mTextureName:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/RendererManager;->setIntParam(ILjava/lang/String;I)V

    .line 103
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mInputSize:Lcom/google/android/libraries/hangouts/video/Size;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->setInputSize(Lcom/google/android/libraries/hangouts/video/Size;)V

    .line 104
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mEncodeSize:Lcom/google/android/libraries/hangouts/video/Size;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->setEncodeSize(Lcom/google/android/libraries/hangouts/video/Size;)V

    .line 105
    return-void
.end method

.method public isExternalTexture()Z
    .locals 1

    .prologue
    .line 168
    const/4 v0, 0x1

    return v0
.end method

.method public release()V
    .locals 2

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mEncodeRendererID:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/RendererManager;->releaseRenderer(I)V

    .line 137
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mEncodeRendererID:I

    .line 138
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    .line 139
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mCallback:Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;

    new-instance v1, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer$2;

    invoke-direct {v1, p0}, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer$2;-><init>(Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;)V

    invoke-interface {v0, v1}, Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;->queueEventForGLThread(Ljava/lang/Runnable;)V

    .line 148
    return-void
.end method

.method public setEncodeSize(Lcom/google/android/libraries/hangouts/video/Size;)V
    .locals 4

    .prologue
    .line 126
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mEncodeSize:Lcom/google/android/libraries/hangouts/video/Size;

    .line 128
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mEncodeRendererID:I

    const-string v2, "sub_inclip"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/RendererManager;->setIntParam(ILjava/lang/String;I)V

    .line 130
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mEncodeRendererID:I

    const-string v2, "sub_outdims"

    .line 131
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/Size;->getEncodedDimensions()I

    move-result v3

    .line 130
    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/RendererManager;->setIntParam(ILjava/lang/String;I)V

    .line 132
    return-void
.end method

.method public setInputSize(Lcom/google/android/libraries/hangouts/video/Size;)V
    .locals 5

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    iget v1, p1, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    iget v2, p1, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/SurfaceTexture;->setDefaultBufferSize(II)V

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mEncodeRendererID:I

    const-string v2, "sub_indims"

    .line 115
    invoke-virtual {p1}, Lcom/google/android/libraries/hangouts/video/Size;->getEncodedDimensions()I

    move-result v3

    .line 114
    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/RendererManager;->setIntParam(ILjava/lang/String;I)V

    .line 116
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mInputSize:Lcom/google/android/libraries/hangouts/video/Size;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/Size;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 117
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mCallback:Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;

    iget v1, p1, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    iget v2, p1, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    iget v3, p1, Lcom/google/android/libraries/hangouts/video/Size;->width:I

    iget v4, p1, Lcom/google/android/libraries/hangouts/video/Size;->height:I

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/libraries/hangouts/video/Renderer$SelfRendererThreadCallback;->onFrameGeometryChanged(IIII)V

    .line 119
    :cond_1
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mInputSize:Lcom/google/android/libraries/hangouts/video/Size;

    .line 120
    return-void
.end method

.method public setIsScreencast(Z)V
    .locals 4

    .prologue
    .line 157
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mRendererManager:Lcom/google/android/libraries/hangouts/video/RendererManager;

    iget v2, p0, Lcom/google/android/libraries/hangouts/video/CustomInputRenderer;->mEncodeRendererID:I

    const-string v3, "sub_screencast"

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/libraries/hangouts/video/RendererManager;->setIntParam(ILjava/lang/String;I)V

    .line 159
    return-void

    .line 157
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
