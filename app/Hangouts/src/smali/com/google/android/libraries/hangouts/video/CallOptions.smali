.class public Lcom/google/android/libraries/hangouts/video/CallOptions;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private final mAbuseTosAccepted:Z

.field private final mAllowOnAir:Z

.field private final mCompressedLogFile:Ljava/lang/String;

.field private final mHasVideo:Z

.field private final mPreferSpeakerphone:Z

.field private final mSessionId:Ljava/lang/String;

.field private final mShouldManagePlatformInteraction:Z

.field private final mUserIsChild:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;ZZZZZZLjava/lang/String;)V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    if-nez p1, :cond_0

    .line 30
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "sessionId"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 32
    :cond_0
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/CallOptions;->mSessionId:Ljava/lang/String;

    .line 33
    iput-boolean p2, p0, Lcom/google/android/libraries/hangouts/video/CallOptions;->mHasVideo:Z

    .line 34
    iput-boolean p3, p0, Lcom/google/android/libraries/hangouts/video/CallOptions;->mShouldManagePlatformInteraction:Z

    .line 35
    iput-boolean p4, p0, Lcom/google/android/libraries/hangouts/video/CallOptions;->mPreferSpeakerphone:Z

    .line 36
    iput-boolean p5, p0, Lcom/google/android/libraries/hangouts/video/CallOptions;->mUserIsChild:Z

    .line 37
    iput-boolean p6, p0, Lcom/google/android/libraries/hangouts/video/CallOptions;->mAllowOnAir:Z

    .line 38
    iput-boolean p7, p0, Lcom/google/android/libraries/hangouts/video/CallOptions;->mAbuseTosAccepted:Z

    .line 39
    iput-object p8, p0, Lcom/google/android/libraries/hangouts/video/CallOptions;->mCompressedLogFile:Ljava/lang/String;

    .line 40
    return-void
.end method


# virtual methods
.method abuseTosAccepted()Z
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/CallOptions;->mAbuseTosAccepted:Z

    return v0
.end method

.method allowOnAir()Z
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/CallOptions;->mAllowOnAir:Z

    return v0
.end method

.method getCompressedLogFile()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallOptions;->mCompressedLogFile:Ljava/lang/String;

    return-object v0
.end method

.method getSessionId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/CallOptions;->mSessionId:Ljava/lang/String;

    return-object v0
.end method

.method hasVideo()Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/CallOptions;->mHasVideo:Z

    return v0
.end method

.method preferSpeakerphone()Z
    .locals 1

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/CallOptions;->mPreferSpeakerphone:Z

    return v0
.end method

.method shouldManagePlatformInteraction()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/CallOptions;->mShouldManagePlatformInteraction:Z

    return v0
.end method

.method userIsChild()Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/CallOptions;->mUserIsChild:Z

    return v0
.end method
