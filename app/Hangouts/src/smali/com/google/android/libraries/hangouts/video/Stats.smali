.class public abstract Lcom/google/android/libraries/hangouts/video/Stats;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final CONNECTION_INFO_STATS_VIDEO:I = 0x0

.field public static final CONNECTION_INFO_STATS_VOICE:I = 0x1

.field private static final DEFAULT_FOR_REQUIRED_PROTO_FIELD:I = -0x1

.field private static final RELAY_CONNECTION_TYPE:I = 0x3


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 611
    return-void
.end method

.method static synthetic access$000(Ljava/util/ArrayList;)F
    .locals 1

    .prologue
    .line 24
    invoke-static {p0}, Lcom/google/android/libraries/hangouts/video/Stats;->calculateMedian(Ljava/util/ArrayList;)F

    move-result v0

    return v0
.end method

.method static synthetic access$300(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 24
    invoke-static {p0}, Lcom/google/android/libraries/hangouts/video/Stats;->parseConnectionType(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic access$400(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 24
    invoke-static {p0}, Lcom/google/android/libraries/hangouts/video/Stats;->parseConnectionProtocol(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private static calculateMedian(Ljava/util/ArrayList;)F
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;)F"
        }
    .end annotation

    .prologue
    .line 710
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 711
    if-nez v2, :cond_0

    .line 712
    const/4 v0, 0x0

    .line 727
    :goto_0
    return v0

    .line 715
    :cond_0
    new-array v3, v2, [F

    .line 716
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_1

    .line 717
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    aput v0, v3, v1

    .line 716
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 720
    :cond_1
    invoke-static {v3}, Ljava/util/Arrays;->sort([F)V

    .line 721
    div-int/lit8 v0, v2, 0x2

    .line 722
    rem-int/lit8 v1, v2, 0x2

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 724
    aget v0, v3, v0

    goto :goto_0

    .line 727
    :cond_2
    add-int/lit8 v1, v0, -0x1

    aget v1, v3, v1

    aget v0, v3, v0

    add-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method static createEmptyMediaProto(I)Ldoh;
    .locals 5

    .prologue
    const-wide/16 v3, -0x1

    const/4 v2, -0x1

    .line 53
    new-instance v0, Ldoh;

    invoke-direct {v0}, Ldoh;-><init>()V

    .line 55
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Ldoh;->b:Ljava/lang/Integer;

    .line 56
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Ldoh;->c:Ljava/lang/Integer;

    .line 57
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Ldoh;->d:Ljava/lang/Integer;

    .line 58
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Ldoh;->e:Ljava/lang/Integer;

    .line 59
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Ldoh;->f:Ljava/lang/Integer;

    .line 60
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Ldoh;->g:Ljava/lang/Integer;

    .line 61
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Ldoh;->h:Ljava/lang/Long;

    .line 62
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Ldoh;->i:Ljava/lang/Integer;

    .line 63
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Ldoh;->j:Ljava/lang/Long;

    .line 64
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Ldoh;->k:Ljava/lang/Integer;

    .line 65
    return-object v0
.end method

.method private static parseConnectionProtocol(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 692
    const-string v0, "udp"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 693
    const/4 v0, 0x1

    .line 699
    :goto_0
    return v0

    .line 694
    :cond_0
    const-string v0, "tcp"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 695
    const/4 v0, 0x2

    goto :goto_0

    .line 696
    :cond_1
    const-string v0, "ssltcp"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 697
    const/4 v0, 0x3

    goto :goto_0

    .line 699
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static parseConnectionType(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 678
    const-string v0, "local"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 679
    const/4 v0, 0x1

    .line 685
    :goto_0
    return v0

    .line 680
    :cond_0
    const-string v0, "stun"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 681
    const/4 v0, 0x2

    goto :goto_0

    .line 682
    :cond_1
    const-string v0, "relay"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 683
    const/4 v0, 0x3

    goto :goto_0

    .line 685
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract addTo(Ldoe;)V
.end method

.method public abstract print(Ljava/io/PrintWriter;Lcom/google/android/libraries/hangouts/video/Stats$AggregatePrintStats;)V
.end method
