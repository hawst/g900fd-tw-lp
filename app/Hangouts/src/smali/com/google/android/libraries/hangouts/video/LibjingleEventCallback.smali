.class interface abstract Lcom/google/android/libraries/hangouts/video/LibjingleEventCallback;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final CHATROOM_ENTERED:I = 0x0

.field public static final ENDPOINT_AUDIO_MUTE_STATE_CHANGED:I = 0x4

.field public static final ENDPOINT_CHANGED:I = 0x3

.field public static final ENDPOINT_ENTERED:I = 0x1

.field public static final ENDPOINT_EXITED:I = 0x2

.field public static final ENDPOINT_MEDIA_BLOCKED:I = 0x6

.field public static final ENDPOINT_VIDEO_MUTE_STATE_CHANGED:I = 0x5


# virtual methods
.method public abstract handleBroadcastProducerChange(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract handleBroadcastSessionStateChange(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract handleBroadcastStreamStateChange(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract handleBroadcastStreamViewerCountChange(I)V
.end method

.method public abstract handleCallEnd(ILjava/lang/String;)V
.end method

.method public abstract handleCheckConnectivity([Lcom/google/android/libraries/hangouts/video/ConnectivityReporter$NicInfo;)V
.end method

.method public abstract handleCommonNotificationReceived(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract handleCommonNotificationRetracted(Ljava/lang/String;)V
.end method

.method public abstract handleConversationIdChanged(Ljava/lang/String;)V
.end method

.method public abstract handleEndpointEvent(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract handleHangoutIdResolved(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;ZZ)V
.end method

.method public abstract handleLoudestSpeakerUpdate(Ljava/lang/String;I)V
.end method

.method public abstract handleMediaSourcesUpdate(Ljava/lang/String;Lcom/google/android/libraries/hangouts/video/MediaSources;)V
.end method

.method public abstract handleMediaStateChanged(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract handleSignedInStateUpdate(Ljava/lang/String;Z)V
.end method

.method public abstract handleStatsUpdate(Lcom/google/android/libraries/hangouts/video/Stats;)V
.end method

.method public abstract makeApiaryRequest(JLjava/lang/String;[BI)V
.end method
